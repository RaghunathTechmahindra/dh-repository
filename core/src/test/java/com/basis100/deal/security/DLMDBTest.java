package com.basis100.deal.security;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;

public class DLMDBTest extends FXDBTestCase {
	private static boolean ready = false;
	private static DLM instance;
	private static final int DELAY = 1000;
	private static final int DELAY_TIMEOUT = 100;
	DLM dlm = null;
	private IDataSet dataSetTest;

	public DLMDBTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				DLM.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		DLM.init(srk, false);
		dlm = DLM.getInstance();
	}

	public void testGetLockedTime() throws Exception {
		ITable testGetLockedTime = dataSetTest.getTable("testGetLockedTime");
		String dealID = (String) testGetLockedTime.getValue(0, "DEALID");
		String userProfileId = (String) testGetLockedTime.getValue(0,
				"USERPROFILEID");
		String str = dlm.getLockedTime(dealID, userProfileId, srk);
		assert str.length() != 0;
		assertNotNull(str);
	}

	public void testIsLockedByUser() throws Exception {
		ITable testIsLockedByUser = dataSetTest.getTable("testIsLockedByUser");
		int dealId = Integer.parseInt(testIsLockedByUser.getValue(0, "dealId")
				.toString());
		int userProfileId = Integer.parseInt(testIsLockedByUser.getValue(0,
				"userProfileId").toString());
		srk.getExpressState().setDealInstitutionId(2);
		boolean result = dlm.isLockedByUser(dealId, userProfileId, srk);
		assertSame(true, result);

	}

	public void testIsLockedByUserFailure() throws Exception {
		ITable testIsLockedByUser = dataSetTest
				.getTable("testIsLockedByUserFailure");
		int dealId = Integer.parseInt(testIsLockedByUser.getValue(0, "dealId")
				.toString());
		int userProfileId = Integer.parseInt(testIsLockedByUser.getValue(0,
				"userProfileId").toString());
		boolean result = dlm.isLockedByUser(dealId, userProfileId, srk);
		assertSame(false, result);

	}

	public void testGetLockStatus() throws Exception {
		int resultLock = 0;
		ITable testIsLockedByUser = dataSetTest.getTable("testGetLockStatus");
		int dealId = Integer.parseInt(testIsLockedByUser.getValue(0, "dealId")
				.toString());
		int userProfileId = Integer.parseInt(testIsLockedByUser.getValue(0,
				"userProfileId").toString());
		srk.getExpressState().setDealInstitutionId(0);
		resultLock = dlm.getLockStatus(dealId, userProfileId, srk);
		assertNotSame(0, resultLock);

	}

	public void testGetLockStatusFailure() throws Exception {
		ITable testIsLockedByUser = dataSetTest
				.getTable("testGetLockStatusFailure");
		int dealId = Integer.parseInt(testIsLockedByUser.getValue(0, "dealId")
				.toString());
		int userProfileId = Integer.parseInt(testIsLockedByUser.getValue(0,
				"userProfileId").toString());
		int result = dlm.getLockStatus(dealId, userProfileId, srk);
		assertSame(0, result);

	}

	public void testGetDealIdOfLockedDeal() throws Exception {
		ITable testIsLockedByUser = dataSetTest
				.getTable("testGetDealIdOfLockedDeal");
		int userProfileId = Integer.parseInt(testIsLockedByUser.getValue(0,
				"userProfileId").toString());
		int result = dlm.getDealIdOfLockedDeal(userProfileId, srk);		
		assertNotSame(-1, result);

	}

	public void testGetDealIdOfLockedDealFailure() throws Exception {
		ITable testIsLockedByUser = dataSetTest
				.getTable("testGetDealIdOfLockedDealFailure");
		int userProfileId = Integer.parseInt(testIsLockedByUser.getValue(0,
				"userProfileId").toString());
		int result = dlm.getDealIdOfLockedDeal(userProfileId, srk);
		assertSame(-1, result);

	}

	public void testGetUserIdOfLockedDeal() throws Exception {

		ITable testIsLockedByUser = dataSetTest
				.getTable("testGetUserIdOfLockedDeal");
		int dealId = Integer.parseInt((String) testIsLockedByUser.getValue(0,
				"dealId"));
		int result = dlm.getUserIdOfLockedDeal(dealId, srk);

		assertNotSame(-1, result);

	}

	public void testGetUserIdOfLockedDealFailure() throws Exception {

		ITable testIsLockedByUser = dataSetTest
				.getTable("testGetUserIdOfLockedDealFailure");
		int dealId = Integer.parseInt(testIsLockedByUser.getValue(0, "dealId")
				.toString());
		int result = dlm.getUserIdOfLockedDeal(dealId, srk);
		assertSame(-1, result);

	}

	@Test
	public void testIsLocked() throws Exception {
		ITable testIsLockedByUser = dataSetTest.getTable("testIsLocked");
		int dealId = Integer.parseInt(testIsLockedByUser.getValue(0, "dealId")
				.toString());
		int userProfileId = Integer.parseInt(testIsLockedByUser.getValue(0,
				"userProfileId").toString());
		boolean result = dlm.isLocked(dealId, userProfileId, srk);
		assertTrue(result);
		assertSame(true, result);

	}

	public void testIsLockedFailure() throws Exception {
		ITable testIsLockedByUser = dataSetTest.getTable("testIsLockedFailure");
		int dealId = Integer.parseInt(testIsLockedByUser.getValue(0, "dealId")
				.toString());
		int userProfileId = Integer.parseInt(testIsLockedByUser.getValue(0,
				"userProfileId").toString());
		boolean result = dlm.isLocked(dealId, userProfileId, srk);
		assertSame(false, result);
	}
	
	
	public void testUnlockFiveArgs() throws Exception {
		ITable testUnlock = dataSetTest.getTable("testUnlockFiveArgs");
		try {
			srk.beginTransaction();
			String page = (String) testUnlock.getValue(0, "DEALID");
			int dealId = Integer.parseInt(page);
			String userProfile = (String) testUnlock.getValue(0,
					"USERPROFILEID");
			int userProfileId = Integer.parseInt(userProfile);			
			dlm.unlock(dealId, userProfileId, srk,true, true);
			assertNotNull(dlm);
			srk.rollbackTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public void testUnlockFiveArgsTransFalse() throws Exception {
		ITable testUnlock = dataSetTest.getTable("testUnlockFiveArgsTransFalse");
		try {
			srk.beginTransaction();
			String page = (String) testUnlock.getValue(0, "DEALID");
			int dealId = Integer.parseInt(page);
			String userProfile = (String) testUnlock.getValue(0,
					"USERPROFILEID");
			int userProfileId = Integer.parseInt(userProfile);			
			dlm.unlock(dealId, userProfileId, srk,true, false);
			assertNotNull(dlm);
			srk.rollbackTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
	public void testUnlockFiveArgsForceToOverrideFalse() throws Exception {
		ITable testUnlock = dataSetTest.getTable("testUnlockFiveArgsForceToOverrideFalse");
		try {
			srk.beginTransaction();
			String page = (String) testUnlock.getValue(0, "DEALID");
			int dealId = Integer.parseInt(page);
			String userProfile = (String) testUnlock.getValue(0,
					"USERPROFILEID");
			int userProfileId = Integer.parseInt(userProfile);			
			dlm.unlock(dealId, userProfileId, srk,false, true);
			assertNotNull(dlm);
			srk.rollbackTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void testUnlock() throws Exception {
		ITable testUnlock = dataSetTest.getTable("testUnlock");
		try {
			srk.beginTransaction();
			String page = (String) testUnlock.getValue(0, "DEALID");
			int dealId = Integer.parseInt(page);
			String userProfile = (String) testUnlock.getValue(0,
					"USERPROFILEID");
			int userProfileId = Integer.parseInt(userProfile);			
			dlm.unlock(dealId, userProfileId, srk,true);
			assertNotNull(dlm);
			srk.rollbackTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void testUnlockTransFalse() throws Exception {
		ITable testUnlock = dataSetTest.getTable("testUnlockTransFalse");
		try {
			srk.beginTransaction();
			String page = (String) testUnlock.getValue(0, "DEALID");
			int dealId = Integer.parseInt(page);
			String userProfile = (String) testUnlock.getValue(0,
					"USERPROFILEID");
			int userProfileId = Integer.parseInt(userProfile);			
			dlm.unlock(dealId, userProfileId, srk,false);
			assertNotNull(dlm);
			srk.rollbackTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void testUnlockAnyBySessionID() throws Exception {
		ITable testUnlockAnyBySessionID = dataSetTest.getTable("testUnlockAnyBySessionID");
		try {
			srk.beginTransaction();
			String SessionID = (String) testUnlockAnyBySessionID.getValue(0, "SESSIONID");			
			dlm.unlockAnyBySessionID(srk,true,SessionID);
			assertNotNull(dlm);
			srk.rollbackTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void testUnlockAnyBySessionIDTransFalse() throws Exception {
		ITable testUnlockAnyBySessionID = dataSetTest.getTable("testUnlockAnyBySessionIDTransFalse");
		try {
			srk.beginTransaction();
			String SessionID = (String) testUnlockAnyBySessionID.getValue(0, "SESSIONID");			
			dlm.unlockAnyBySessionID(srk,false,SessionID);
			assertNotNull(dlm);
			srk.rollbackTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void testUnlockAnyByUser() throws Exception {
		ITable testUnlockAnyByUser = dataSetTest.getTable("testUnlockAnyByUser");
		try {
			srk.beginTransaction();
			int userProfileId = Integer.parseInt((String) testUnlockAnyByUser.getValue(0, "USERPROFILEID"));			
			dlm.unlockAnyByUser(userProfileId,srk,true);
			assertNotNull(dlm);
			srk.rollbackTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
	public void testUnlockAnyByUserTransFalse() throws Exception {
		ITable testUnlockAnyByUser = dataSetTest.getTable("testUnlockAnyByUserTransFalse");
		try {
			srk.beginTransaction();
			int userProfileId = Integer.parseInt((String) testUnlockAnyByUser.getValue(0, "USERPROFILEID"));			
			dlm.unlockAnyByUser(userProfileId,srk,false);
			assertNotNull(dlm);
			srk.rollbackTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}
