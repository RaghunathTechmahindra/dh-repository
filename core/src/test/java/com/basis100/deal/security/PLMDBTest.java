package com.basis100.deal.security;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;

public class PLMDBTest extends FXDBTestCase {
	PLM plm = null;
	private IDataSet dataSetTest;

	public PLMDBTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				PLM.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;

	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();

		plm = PLM.getInstance();
	}

	public void testGetPageLockedUserDetails() throws Exception {
		ITable testGetPageLockedUserDetails = dataSetTest
				.getTable("testGetPageLockedUserDetails");
		int pageID = Integer.parseInt((String) testGetPageLockedUserDetails
				.getValue(0, "PAGEID"));
		int userProfileId = Integer
				.parseInt((String) testGetPageLockedUserDetails.getValue(0,
						"USERPROFILEID"));
		String str = plm.getPageLockedUserDetails(pageID, userProfileId, srk);
		assert plm.getInstance() != null;
		assertNotNull(str);

	}

	public void testGetPageLockTimeStamp() throws Exception {
		ITable testGetPageLockTimeStamp = dataSetTest
				.getTable("testGetPageLockTimeStamp");
		String page = (String) testGetPageLockTimeStamp.getValue(0, "PAGEID");
		int pageID = Integer.parseInt(page);
		String userProfile = (String) testGetPageLockTimeStamp.getValue(0,
				"USERPROFILEID");
		int userProfileId = Integer.parseInt(userProfile);
		String str = plm.getPageLockTimeStamp(pageID, userProfileId, srk);
		assert plm.getInstance() != null;
		assertNotNull(str);
	}

	public void testIsLocked() throws Exception {
		ITable testIsLocked = dataSetTest.getTable("testIsLocked");
		String page = (String) testIsLocked.getValue(0, "PAGEID");
		int pageID = Integer.parseInt(page);
		String userProfile = (String) testIsLocked.getValue(0, "USERPROFILEID");
		int userProfileId = Integer.parseInt(userProfile);
		boolean str = plm.isLocked(pageID, userProfileId, srk);
		assertTrue(str);
		
	}

	public void testIsLockedByUser() throws Exception {
		ITable testIsLockedByUser = dataSetTest.getTable("testIsLockedByUser");
		String page = (String) testIsLockedByUser.getValue(0, "PAGEID");
		int pageID = Integer.parseInt(page);
		String userProfile = (String) testIsLockedByUser.getValue(0,
				"USERPROFILEID");
		int userProfileId = Integer.parseInt(userProfile);
		boolean str = plm.isLockedByUser(pageID, userProfileId, srk);
		assertTrue(str);
	}

	public void testUnlock() throws Exception {
		ITable testUnlock = dataSetTest.getTable("testUnlock");
		try {
			srk.beginTransaction();
			String page = (String) testUnlock.getValue(0, "PAGEID");
			int pageID = Integer.parseInt(page);
			String userProfile = (String) testUnlock.getValue(0,
					"USERPROFILEID");
			int userProfileId = Integer.parseInt(userProfile);
			String sessionId = (String) testUnlock.getValue(0, "SESSIONID");
			plm.unlock(pageID, userProfileId, srk, true, sessionId);
			assertNotNull(plm);
			srk.rollbackTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void testUnlockTransFalse() throws Exception {
		ITable testUnlock = dataSetTest.getTable("testUnlockTransFalse");
		try {
			srk.beginTransaction();
			String page = (String) testUnlock.getValue(0, "PAGEID");
			int pageID = Integer.parseInt(page);
			String userProfile = (String) testUnlock.getValue(0,
					"USERPROFILEID");
			int userProfileId = Integer.parseInt(userProfile);
			String sessionId = (String) testUnlock.getValue(0, "SESSIONID");
			plm.unlock(pageID, userProfileId, srk, false, sessionId);
			assertNotNull(plm);
			srk.rollbackTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void testUnlockAnyByUse() throws Exception {
		ITable testUnlockAnyByUse = dataSetTest.getTable("testUnlockAnyByUse");
		try {
			srk.beginTransaction();
			String userProfile = (String) testUnlockAnyByUse.getValue(0,
					"USERPROFILEID");
			int userProfileId = Integer.parseInt(userProfile);
			plm.unlockAnyByUser(userProfileId, srk, true);
			assertNotNull(plm);
			srk.rollbackTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void testUnlockAnyByUseTransFalse() throws Exception {
		ITable testUnlockAnyByUse = dataSetTest
				.getTable("testUnlockAnyByUseTransFalse");
		try {
			srk.beginTransaction();
			String userProfile = (String) testUnlockAnyByUse.getValue(0,
					"USERPROFILEID");
			int userProfileId = Integer.parseInt(userProfile);
			plm.unlockAnyByUser(userProfileId, srk, false);
			assertNotNull(plm);
			srk.rollbackTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void testUnlockAnyBySessionID() throws Exception {
		ITable testUnlockAnyBySessionID = dataSetTest
				.getTable("testUnlockAnyBySessionID");
		try {
			srk.beginTransaction();
			String sessionId = (String) testUnlockAnyBySessionID.getValue(0,
					"SESSIONID");
			plm.unlockAnyBySessionID(srk, true, sessionId);
			assertNotNull(plm);
			srk.rollbackTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void testUnlockAnyBySessionIDTransFalse() throws Exception {
		ITable testUnlockAnyBySessionID = dataSetTest
				.getTable("testUnlockAnyBySessionIDTransFalse");
		try {
			srk.beginTransaction();
			String sessionId = (String) testUnlockAnyBySessionID.getValue(0,
					"SESSIONID");
			plm.unlockAnyBySessionID(srk, false, sessionId);
			assertNotNull(plm);
			srk.rollbackTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
