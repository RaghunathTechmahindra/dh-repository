package com.basis100.deal.conditions;

import java.io.IOException;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.FXDBTestCase;


public class ConditionHandlerTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	ConditionHandler conditionHandler=null;
	DealPK dealPK=null;
	Deal deal=null;
	
	public ConditionHandlerTest(String name) throws IOException,DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("ConditionHandlerDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource("ConditionHandlerDataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		conditionHandler=new ConditionHandler(srk);
		 deal=new Deal();
	}
	
	/*public void testGetVariableVerbiagebyDealPK() throws Exception 
	{
		ITable testIsLockedByUser = dataSetTest.getTable("testGetVariableVerbiagebyDealPK");
		int userId=Integer.parseInt(testIsLockedByUser.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		dealPK=new DealPK(userId,copyId);
		String  label=testIsLockedByUser.getValue(0, "label").toString();
		int languagePreferenceId=Integer.parseInt(testIsLockedByUser.getValue(0, "languagePreferenceId").toString());
		String result=conditionHandler.getVariableVerbiage(dealPK,label,languagePreferenceId);
		assertEquals("TBD", result);
		
	}*/

	public void testGetVariableVerbiagebyDealPKFailure() throws Exception 
	{
		ITable testIsLockedByUser = dataSetTest.getTable("testGetVariableVerbiagebyDealPKFailure");
		int userId=Integer.parseInt(testIsLockedByUser.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		dealPK=new DealPK(userId,copyId);
		String  label=testIsLockedByUser.getValue(0, "label").toString();
		int languagePreferenceId=Integer.parseInt(testIsLockedByUser.getValue(0, "languagePreferenceId").toString());
		String result=conditionHandler.getVariableVerbiage(dealPK,label,languagePreferenceId);
		assertEquals(null, result);
	}
	/*public void testGetVariableVerbiagebyDeal() throws Exception 
	{
		ITable testIsLockedByUser = dataSetTest.getTable("testGetVariableVerbiagebyDeal");
		int userId=Integer.parseInt(testIsLockedByUser.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		dealPK=new DealPK(userId,copyId);
		String  label=testIsLockedByUser.getValue(0, "label").toString();
		int languagePreferenceId=Integer.parseInt(testIsLockedByUser.getValue(0, "languagePreferenceId").toString());
		String result=conditionHandler.getVariableVerbiage(deal,label,languagePreferenceId);
		assertEquals("TBD", result);
		
	}*/
	public void testGetVariableVerbiagebyDealFailure() throws Exception 
	{
		ITable testIsLockedByUser = dataSetTest.getTable("testGetVariableVerbiagebyDealFailure");
		int userId=Integer.parseInt(testIsLockedByUser.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		dealPK=new DealPK(userId,copyId);
		String  label=testIsLockedByUser.getValue(0, "label").toString();
		int languagePreferenceId=Integer.parseInt(testIsLockedByUser.getValue(0, "languagePreferenceId").toString());
		String result=conditionHandler.getVariableVerbiage(deal,null,languagePreferenceId);
		assertEquals(null, result);
	}
	
	/*public void testGetElectronicVerbiage() throws Exception 
	{
		ITable testIsLockedByUser = dataSetTest.getTable("testGetElectronicVerbiage");
		int userId=Integer.parseInt(testIsLockedByUser.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		dealPK=new DealPK(userId,copyId);
		String  label=testIsLockedByUser.getValue(0, "label").toString();
		int languagePreferenceId=Integer.parseInt(testIsLockedByUser.getValue(0, "languagePreferenceId").toString());
		String result=conditionHandler.getElectronicVerbiage(deal,label,languagePreferenceId);
		assertEquals("TBD", result);
		
	}*/
	public void testGetElectronicVerbiageFailure() throws Exception 
	{
		ITable testIsLockedByUser = dataSetTest.getTable("testGetElectronicVerbiageFailure");
		int userId=Integer.parseInt(testIsLockedByUser.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		dealPK=new DealPK(userId,copyId);
		String  label=testIsLockedByUser.getValue(0, "label").toString();
		int languagePreferenceId=Integer.parseInt(testIsLockedByUser.getValue(0, "languagePreferenceId").toString());
		String result=conditionHandler.getElectronicVerbiage(deal,null,languagePreferenceId);
		assertEquals(null, result);
	}
	public void testGetElectronicVerbiageByCondition() throws Exception {
		ITable testIsLockedByUser = dataSetTest.getTable("testGetElectronicVerbiageByCondition");
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		int languagePreferenceId=Integer.parseInt(testIsLockedByUser.getValue(0, "languagePreferenceId").toString());
		String result=conditionHandler.getElectronicVerbiage(deal,copyId,languagePreferenceId);
		
		assertEquals(null, result);
	}
	public void testGetElectronicVerbiageByConditionFailure() throws Exception {
		ITable testIsLockedByUser = dataSetTest.getTable("testGetElectronicVerbiageByConditionFailure");
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		int languagePreferenceId=Integer.parseInt(testIsLockedByUser.getValue(0, "languagePreferenceId").toString());
		String result=conditionHandler.getElectronicVerbiage(deal,copyId,languagePreferenceId);
		assertEquals(null, result);
	}
	
	//DocumentTracking buildFreeFormCondition(int conditionId, DealPK pk, String text, String label)
	
	public void testBuildFreeFormCondition() throws Exception {
		ITable testIsLockedByUser = dataSetTest.getTable("testBuildFreeFormCondition");
		int conditionId=Integer.parseInt(testIsLockedByUser.getValue(0, "conditionId").toString());
		String text=testIsLockedByUser.getValue(0, "text").toString();
		String label=testIsLockedByUser.getValue(0, "label").toString();
		int userId=Integer.parseInt(testIsLockedByUser.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		dealPK=new DealPK(userId,copyId);
		boolean result=true;
	    try{
		DocumentTracking resultS=conditionHandler.buildFreeFormCondition(conditionId,dealPK,text,label);
		result=false;
	    }catch(Exception e){
	    	result=true;
	    }
		assertEquals(true, result);
	}
	

	public void testBuildFreeFormConditionFailure() throws Exception {
		ITable testIsLockedByUser = dataSetTest.getTable("testBuildFreeFormConditionFailure");
		int conditionId=Integer.parseInt(testIsLockedByUser.getValue(0, "conditionId").toString());
		String text=testIsLockedByUser.getValue(0, "text").toString();
		String label=testIsLockedByUser.getValue(0, "label").toString();
		int userId=Integer.parseInt(testIsLockedByUser.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		dealPK=new DealPK(userId,copyId);
		boolean result=false;
	    try{
		DocumentTracking resultS=conditionHandler.buildFreeFormCondition(conditionId,dealPK,text,label);
		result=true;
	    }catch(Exception e){
	    	result=false;
	    }
		assertEquals(false, result);
	}
	
	/*public void testRetrieveCommitmentLetterDocTracking() throws Exception{
		
		//retrieveCommitmentLetterDocTracking(Deal deal,int languagePreference)
		ITable testIsLockedByUser = dataSetTest.getTable("testRetrieveCommitmentLetterDocTracking");
		int languagePreference=Integer.parseInt(testIsLockedByUser.getValue(0, "languagePreference").toString());
		int userId=Integer.parseInt(testIsLockedByUser.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		deal=new Deal(srk,null,userId,copyId);
		int result=conditionHandler.retrieveCommitmentLetterDocTracking(deal, languagePreference).size();
    	assertNotSame(0, result);
		
	}*/
	
      public void testRetrieveCommitmentLetterDocTrackingFailure() throws Exception{
    	  boolean status=false;
    	  try{
		ITable testIsLockedByUser = dataSetTest.getTable("testRetrieveCommitmentLetterDocTrackingFailure");
		int languagePreference=Integer.parseInt(testIsLockedByUser.getValue(0, "languagePreference").toString());
		int userId=Integer.parseInt(testIsLockedByUser.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		deal=new Deal(srk,null,userId,copyId);
		int result=conditionHandler.retrieveCommitmentLetterDocTracking(deal, languagePreference).size();
    	  }catch (Exception e) {
			// TODO: handle exception
    		  status=false;
		}
    	assertSame(false, status);
		
	}
	/*public void testGetCustomDocumentTracking()throws Exception{
		ITable testIsLockedByUser = dataSetTest.getTable("testGetCustomDocumentTracking");
		int userId=Integer.parseInt(testIsLockedByUser.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		deal=new Deal(srk,null,userId,copyId);
		int result=conditionHandler.getSystemGeneratedDocumentTracking(deal).size();
    	assertNotSame(0, result);
		
	}*/
	public void testGetCustomDocumentTrackingFailure()throws Exception{
		boolean result=true;
		try{
		ITable testIsLockedByUser = dataSetTest.getTable("testGetCustomDocumentTrackingFailure");
		int userId=Integer.parseInt(testIsLockedByUser.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		deal=new Deal(srk,null,userId,copyId);
		int size=conditionHandler.getSystemGeneratedDocumentTracking(deal).size();
		result=true;
		}catch(Exception e){
			 result=false;
		}
    	assertSame(false, result);
		
	}
	
	public void testUpdateForApproval()  throws Exception{
		
		ITable testIsLockedByUser = dataSetTest.getTable("testUpdateForApproval");
		int userId=Integer.parseInt(testIsLockedByUser.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		deal=new Deal(srk,null,userId,copyId);
		conditionHandler.updateForApproval(deal);
	}
	
    public void testUpdateForApprovalFailure()  throws Exception{
    	boolean result=true;
		try{
		ITable testIsLockedByUser = dataSetTest.getTable("testUpdateForApprovalFailure");
		int userId=Integer.parseInt(testIsLockedByUser.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		deal=new Deal(srk,null,userId,copyId);
		conditionHandler.updateForApproval(deal);
		result=false;
		}catch(Exception e){
			result=true;
		}
		assertSame(true, result);
	}
	
    public void updateForPDecision() throws Exception{
    	ITable testIsLockedByUser = dataSetTest.getTable("updateForPDecision");
    	int userId=Integer.parseInt(testIsLockedByUser.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		deal=new Deal(srk,null,userId,copyId);
		conditionHandler.updateForPDecision(deal);	
    }
	
    public void updateForPDecisionFailure() throws Exception{
    	boolean result=true;
		try{
    	ITable testIsLockedByUser = dataSetTest.getTable("updateForPDecisionFailure");
    	int userId=Integer.parseInt(testIsLockedByUser.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		deal=new Deal(srk,null,userId,copyId);
		conditionHandler.updateForPDecision(deal);	
		}catch(Exception e){
			result=true;
		}
		assertSame(true, result);
    }
    
    //updateForCommitmentCancelled
    
    public void updateForCommitmentCancelled() throws Exception{
    	
    	ITable testIsLockedByUser = dataSetTest.getTable("updateForCommitmentCancelled");
    	int userId=Integer.parseInt(testIsLockedByUser.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		deal=new Deal(srk,null,userId,copyId);
		conditionHandler.updateForCommitmentCancelled(deal,true);
    }
    
    public void updateForCommitmentCancelledFailure() throws Exception{
    	
    	boolean result=true;
		try{
    	ITable testIsLockedByUser = dataSetTest.getTable("updateForPDecisionFailure");
    	int userId=Integer.parseInt(testIsLockedByUser.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testIsLockedByUser.getValue(0, "copyId").toString());
		deal=new Deal(srk,null,userId,copyId);
		conditionHandler.updateForCommitmentCancelled(deal,true);	
		}catch(Exception e){
			result=true;
		}
		assertSame(true, result);
    }
    
    
}
