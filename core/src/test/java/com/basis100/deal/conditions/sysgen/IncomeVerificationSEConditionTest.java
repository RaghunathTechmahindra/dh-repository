package com.basis100.deal.conditions.sysgen;

import java.io.IOException;
import java.sql.Date;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.docprep.xsl.XSLDataFactory;
import com.basis100.deal.docprep.xsl.XSLMerger;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.log.SysLogger;
import com.basis100.workflow.pk.AssignedTaskBeanPK;


/**
 * <p>DealQueryTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class IncomeVerificationSEConditionTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private IncomeVerificationSECondition incomeVerificationSECondition;
	// session resource kit
    //private SessionResourceKit _srk;

	public IncomeVerificationSEConditionTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(IncomeVerificationSECondition.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(IncomeVerificationSECondition.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.incomeVerificationSECondition = new IncomeVerificationSECondition();
	}
	
	public void testExtract() throws Exception {
		// get input data from xml repository
		FMLQueryResult fml=new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");			   	   
		String id=(String)testExtract.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);		
	    Deal deal=new Deal(srk,null,dealId, copyId);	    
	    DealEntity entity=(DealEntity)deal;
	    deal.setFunderProfileId(1);    
	   fml=incomeVerificationSECondition.extract(entity, srk);  
	   assertNotNull(fml);
    }
	
}