package com.basis100.deal.conditions;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.Condition;
import com.basis100.deal.pk.DealPK;

public class ConditionParserTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	ConditionParser conditionParser = null;
	Deal deal=null;;
	DealEntity parseCtx=null;
	Condition condition=null;
	
	public ConditionParserTest(String name) throws IOException,DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("ConditionParserDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		
		return new FlatXmlDataSet(this.getClass().getResource("ConditionParserDataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		conditionParser = new ConditionParser();
	}

	public void testParseText() throws Exception {

		ITable testParseText = dataSetTest.getTable("testParseText");
		String in = testParseText.getValue(0, "in").toString();
		int language = Integer.parseInt(testParseText.getValue(0, "language").toString());
		String result = conditionParser.parseText(in, language, null, srk);
		assertEquals(in, result);
	}

	public void testParseTextFailure() throws Exception {
		ITable testParseTextFailure = dataSetTest.getTable("testParseTextFailure");
		//String in = testParseTextFailure.getValue(0, "in").toString();
		int language = Integer.parseInt(testParseTextFailure.getValue(0,"language").toString());
		String result = conditionParser.parseText(null, language, null, srk);
		assertEquals(null, result);
	}
	
	public void testcreateSystemGeneratedDocTracking() throws Exception
	{
		boolean status=true;
		try{
		ITable testcreateSystemGeneratedDocTracking = dataSetTest.getTable("testcreateSystemGeneratedDocTracking");
		int userId=Integer.parseInt(testcreateSystemGeneratedDocTracking.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testcreateSystemGeneratedDocTracking.getValue(0, "copyId").toString());
		deal =new Deal(srk,null,userId,copyId);
		parseCtx=new DealEntity(srk);
		condition=new Condition(srk);
		conditionParser.createSystemGeneratedDocTracking(condition, deal, parseCtx, srk);
     	 status=false;
	     }catch(Exception e){
	    	 status=true;
	   }
	     assertEquals(true,status);
	}

	public void testcreateSystemGeneratedDocTrackingFailure() throws Exception
	{
		boolean status=false;
		try{
		ITable testcreateSystemGeneratedDocTracking = dataSetTest.getTable("testcreateSystemGeneratedDocTrackingFailure");
		int userId=Integer.parseInt(testcreateSystemGeneratedDocTracking.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testcreateSystemGeneratedDocTracking.getValue(0, "copyId").toString());
		deal =new Deal(srk,null,userId,copyId);
		parseCtx=new DealEntity(srk);
		condition=new Condition(srk);
		conditionParser.createSystemGeneratedDocTracking(condition, deal, parseCtx, srk);
		 status=true;
	     }catch(Exception e){
	    	 status=false;
	   }
	     assertEquals(false,status);
	}
	
	
}
