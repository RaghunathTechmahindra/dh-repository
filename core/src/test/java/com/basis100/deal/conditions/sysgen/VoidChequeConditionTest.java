package com.basis100.deal.conditions.sysgen;

import java.io.IOException;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.duplicate.PropertySearchResult;
import com.basis100.deal.duplicate.SearchResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.Income;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.Property;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.workflow.entity.Routing;
import com.basis100.workflow.pk.RoutingPK;

public class VoidChequeConditionTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private VoidChequeCondition voidChequeCondition;
	FMLQueryResult fml =null; 
	
	public VoidChequeConditionTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(VoidChequeCondition.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.INSERT;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.voidChequeCondition = new VoidChequeCondition();	
	}
	
	public void testExtract() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException{
		fml=new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");			   	   
		String id=(String)testExtract.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);		
	    Deal deal=new Deal(srk,null,dealId, copyId);	    
	    DealEntity entity=(DealEntity)deal;	   
	    deal.setSourceOfBusinessProfileId(18315);
	    srk.getExpressState().setDealInstitutionId(1);
	   fml=voidChequeCondition.extract(entity, srk);  
	   assertNotNull(fml);
} 
	

}
