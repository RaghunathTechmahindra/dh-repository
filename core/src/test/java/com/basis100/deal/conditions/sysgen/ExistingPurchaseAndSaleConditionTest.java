package com.basis100.deal.conditions.sysgen;

import java.io.IOException;
import java.sql.Date;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.docprep.xsl.XSLDataFactory;
import com.basis100.deal.docprep.xsl.XSLMerger;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.log.SysLogger;
import com.basis100.workflow.pk.AssignedTaskBeanPK;


/**
 * <p>DealQueryTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class ExistingPurchaseAndSaleConditionTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private ExistingPurchaseAndSaleCondition existingPurchaseAndSaleCondition;
	// session resource kit
    //private SessionResourceKit _srk;

	public ExistingPurchaseAndSaleConditionTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ExistingPurchaseAndSaleCondition.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(ExistingPurchaseAndSaleCondition.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.existingPurchaseAndSaleCondition = new ExistingPurchaseAndSaleCondition();
	}
	
	public void testExtract() throws Exception {
    	
		// get input data from xml repository
    	ITable expectedAssignedTask = dataSetTest.getTable("testExtract");
    	FMLQueryResult fml=new FMLQueryResult();   
		    Deal deal=new Deal(srk,null);
		    deal.setDealId(1054864);
			deal.setCopyId(4);
			deal.setInstitutionProfileId(1);
			deal.setMIIndicatorId(1);
			DealEntity de=(DealEntity)deal;	
			   
			fml = existingPurchaseAndSaleCondition.extract(de, srk);
			assertNotNull(fml);
    }
	
}