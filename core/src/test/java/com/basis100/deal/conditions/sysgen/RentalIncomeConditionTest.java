package com.basis100.deal.conditions.sysgen;

import java.io.IOException;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.duplicate.PropertySearchResult;
import com.basis100.deal.duplicate.SearchResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.Income;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.workflow.entity.Routing;
import com.basis100.workflow.pk.RoutingPK;

public class RentalIncomeConditionTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private RentalIncomeCondition rentalIncomeCondition;
	FMLQueryResult fml =null; 
	
	public RentalIncomeConditionTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(RentalIncomeCondition.class.getSimpleName() + "DataSetTest.xml"));
	}



	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.INSERT;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.rentalIncomeCondition = new RentalIncomeCondition();	
	}
	
	public void testExtract() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException{
		ITable testExtract = dataSetTest.getTable("testExtract");
		fml=new FMLQueryResult();	   	   
		String id=(String)testExtract.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);		
	    Deal deal=new Deal(srk,null,dealId, copyId);	    
	    DealEntity entity=(DealEntity)deal;
	    Income income=new Income(srk,null);
	    DealEntity entityIncome=(DealEntity)income;
	    income.setIncomeTypeId(10);
	   fml=rentalIncomeCondition.extract(entity, srk);  
	   assertNotNull(fml);
} 
	

}
