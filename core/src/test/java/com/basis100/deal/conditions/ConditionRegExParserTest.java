package com.basis100.deal.conditions;

import java.io.IOException;
import java.util.ArrayList;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.Asset;
import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentTracking;

public class ConditionRegExParserTest extends FXDBTestCase  {
	
	private IDataSet dataSetTest;
	ConditionRegExParser conditionRegExParser=null;
	
	public ConditionRegExParserTest(String name) throws IOException,DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("ConditionRegExParserDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		
		return new FlatXmlDataSet(this.getClass().getResource("ConditionRegExParserDataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		conditionRegExParser=new ConditionRegExParser(srk);
	}
	
	/*public void testCreateSystemGeneratedDocTracking() throws Exception {

		ITable testCreateSystemGeneratedDocTracking = dataSetTest.getTable("testCreateSystemGeneratedDocTracking");
		
		int userId=Integer.parseInt(testCreateSystemGeneratedDocTracking.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testCreateSystemGeneratedDocTracking.getValue(0, "copyId").toString());
		int auserId=Integer.parseInt(testCreateSystemGeneratedDocTracking.getValue(0, "aid").toString());
		int acopyId=Integer.parseInt(testCreateSystemGeneratedDocTracking.getValue(0, "acopyId").toString());
		
		Condition c1 = new Condition(srk);
		ArrayList<Condition> conditionList = new ArrayList();
		conditionList.add(c1);
		Deal deal = new Deal(srk, null, userId, copyId);
		DealEntity entity = new Asset(srk, null, auserId, acopyId);
		ArrayList resultList = new ArrayList();
		conditionRegExParser.createSystemGeneratedDocTracking(conditionList,deal, entity, resultList);
	}
	
     public void testCreateSystemGeneratedDocTrackingFailure() throws Exception {
         boolean result=true;   
    		ITable testCreateSystemGeneratedDocTracking = dataSetTest.getTable("testCreateSystemGeneratedDocTrackingFailure");
    		int userId=Integer.parseInt(testCreateSystemGeneratedDocTracking.getValue(0, "id").toString());
    		int copyId=Integer.parseInt(testCreateSystemGeneratedDocTracking.getValue(0, "copyId").toString());
    		int auserId=Integer.parseInt(testCreateSystemGeneratedDocTracking.getValue(0, "aid").toString());
    		int acopyId=Integer.parseInt(testCreateSystemGeneratedDocTracking.getValue(0, "acopyId").toString());
    		Condition c1 = new Condition(srk);
    		ArrayList<Condition> conditionList = new ArrayList();
    		conditionList.add(c1);
    		Deal deal = new Deal(srk, null, userId, copyId);
    		DealEntity entity = new Asset(srk, null, auserId, acopyId);
    		ArrayList resultList = new ArrayList();
    		conditionRegExParser.createSystemGeneratedDocTracking(conditionList,deal, entity, resultList);
    		result=false; 
            assertEquals(false,result);
   }
     
     public void testparseAndSetText() throws Exception{
    	 ITable testparseAndSetText = dataSetTest.getTable("testparseAndSetText");
     	 int auserId=Integer.parseInt(testparseAndSetText.getValue(0, "aid").toString());
 	     int acopyId=Integer.parseInt(testparseAndSetText.getValue(0, "acopyId").toString());
    	 Condition cond = new Condition(srk);
    	 DocumentTracking dt=new DocumentTracking(srk);
    	 DealEntity entity = new Asset(srk, null, auserId, acopyId);
    	 conditionRegExParser.parseAndSetText(cond, dt, entity);
     }

     public void testparseAndSetTextFailure() throws Exception{
    	 boolean result=false;
    	 ITable testparseAndSetText = dataSetTest.getTable("testparseAndSetTextFailure");
     	 int auserId=Integer.parseInt(testparseAndSetText.getValue(0, "aid").toString());
 	     int acopyId=Integer.parseInt(testparseAndSetText.getValue(0, "acopyId").toString());
    	 Condition cond = new Condition(srk);
    	 DocumentTracking dt=new DocumentTracking(srk);
    	 DealEntity entity = new Asset(srk, null, auserId, acopyId);
    	 conditionRegExParser.parseAndSetText(cond, dt, entity);
    	 result=true;
    	  assertSame(true,result);
     }
     
     public void testReplaceTages() throws Exception{
    	 
    	 //String conditionsText, DealEntity entity, int language
    	 ITable testReplaceTages = dataSetTest.getTable("testReplaceTages");
    	 String  conditionsText=testReplaceTages.getValue(0, "conditionsText").toString();
    	 int language=Integer.parseInt(testReplaceTages.getValue(0, "language").toString());
    	 int auserId=Integer.parseInt(testReplaceTages.getValue(0, "aid").toString());
 	     int acopyId=Integer.parseInt(testReplaceTages.getValue(0, "acopyId").toString());
    	 DealEntity entity = new Asset(srk, null, auserId, acopyId);
    	 String result=conditionRegExParser.replaceTages(conditionsText, entity, language);
    	 assertSame(conditionsText,result);
     }

     public void testReplaceTagesFailure() throws Exception{
    	 
    	 //String conditionsText, DealEntity entity, int language
    	 boolean status=false;
    	 ITable testReplaceTages = dataSetTest.getTable("testReplaceTagesFailure");
    	 int language=Integer.parseInt(testReplaceTages.getValue(0, "language").toString());
    	 int auserId=Integer.parseInt(testReplaceTages.getValue(0, "aid").toString());
 	     int acopyId=Integer.parseInt(testReplaceTages.getValue(0, "acopyId").toString());
    	 DealEntity entity = new Asset(srk, null, auserId, acopyId);
    	 String result=conditionRegExParser.replaceTages("test", entity, language);
    	 status=true;
    	 assertSame(true,status);
     }*/
     
    /* public void testGetTagReplaceValue() throws Exception{
    	 //getTagReplaceValue(String, DealEntity, int)

    	 ITable testReplaceTages = dataSetTest.getTable("testGetTagReplaceValue");
    	 String  tag=testReplaceTages.getValue(0, "tag").toString();
    	 int auserId=Integer.parseInt(testReplaceTages.getValue(0, "aid").toString());
 	     int acopyId=Integer.parseInt(testReplaceTages.getValue(0, "acopyId").toString());
 	     int language=Integer.parseInt(testReplaceTages.getValue(0, "language").toString());
    	 DealEntity entity = new Asset(srk, null, auserId, acopyId);
    	 String result=conditionRegExParser.getTagReplaceValue(tag, entity, language);
    	 System.out.println("Result:"+result);
    	 assertSame(tag,result);
     }*/
     
     public void testCreateAllMissingDocumentVerbiage() throws Exception{
    	 ITable testCreateAllMissingDocumentVerbiage = dataSetTest.getTable("testCreateAllMissingDocumentVerbiage");
    	 int userId=Integer.parseInt(testCreateAllMissingDocumentVerbiage.getValue(0, "id").toString());
 		 int copyId=Integer.parseInt(testCreateAllMissingDocumentVerbiage.getValue(0, "copyId").toString());
 		 int languagePref=Integer.parseInt(testCreateAllMissingDocumentVerbiage.getValue(0, "languagePref").toString());
    	 Deal deal = new Deal(srk, null, userId, copyId);
    	 conditionRegExParser.createAllMissingDocumentVerbiage(languagePref, deal);
    	 
     }
     public void testCreateAllMissingDocumentVerbiageFailure() throws Exception{
    	 boolean status=false;
    	 ITable testCreateAllMissingDocumentVerbiage = dataSetTest.getTable("testCreateAllMissingDocumentVerbiageFailure");
    	 int userId=Integer.parseInt(testCreateAllMissingDocumentVerbiage.getValue(0, "id").toString());
 		 int copyId=Integer.parseInt(testCreateAllMissingDocumentVerbiage.getValue(0, "copyId").toString());
 		 int languagePref=Integer.parseInt(testCreateAllMissingDocumentVerbiage.getValue(0, "languagePref").toString());
    	 Deal deal = new Deal(srk, null, userId, copyId);
    	 conditionRegExParser.createAllMissingDocumentVerbiage(languagePref, deal);
    	 status=true;
    	
    	 assertSame(true, status);
     }
     
     
}
