package com.basis100.deal.conditions;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;



import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.pk.DealPK;
import com.sun.org.apache.xerces.internal.impl.xs.opti.DefaultDocument;
import com.sun.org.apache.xerces.internal.impl.xs.opti.DefaultNode;

public class BrokerApprovalConditionHandlerTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private BrokerApprovalConditionHandler brokerApprovalConditionHandler; 
	Node node=null;
	Document dom=null;
	public BrokerApprovalConditionHandlerTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(BrokerApprovalConditionHandler.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(BrokerApprovalConditionHandler.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.INSERT;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.brokerApprovalConditionHandler = new BrokerApprovalConditionHandler(srk);	
	}
	
	/*public void testBuildSystemGeneratedDocumentTracking() throws Exception{
		dom=new DefaultDocument();		
		node=new DefaultNode();
		ITable testBuildSystemGeneratedDocumentTracking = dataSetTest.getTable("testBuildSystemGeneratedDocumentTracking");
		String id=(String)testBuildSystemGeneratedDocumentTracking.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testBuildSystemGeneratedDocumentTracking.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);	 
	    Deal deal=new Deal(srk,null,dealId,copyId);	   
	    node=(Node)brokerApprovalConditionHandler.buildBrokerApprovalConditionNodes(deal,dom,node,null);	     
	   assertNotNull(node);
} 

	public void testExtractBrokerApprovalConditionNodes() throws Exception{
		Map map=null;
		ITable testExtractBrokerApprovalConditionNodes = dataSetTest.getTable("testExtractBrokerApprovalConditionNodes");
		String id=(String)testExtractBrokerApprovalConditionNodes.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testExtractBrokerApprovalConditionNodes.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);	 
	    Deal deal=new Deal(srk,null,dealId,copyId);	   
	    map=brokerApprovalConditionHandler.extractBrokerApprovalConditionNodes(deal,null);
	    int size=0;
	    if(map!=null){
	    	size=map.size()	;
	    }
	    assertNotSame(0,size);
	    
} */
	public void testTBR(){
        System.out.println("test");
}

	
}
