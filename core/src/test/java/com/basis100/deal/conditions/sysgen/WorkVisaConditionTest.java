package com.basis100.deal.conditions.sysgen;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.duplicate.PropertySearchResult;
import com.basis100.deal.duplicate.SearchResult;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.Income;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.Property;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.workflow.entity.Routing;
import com.basis100.workflow.pk.RoutingPK;

public class WorkVisaConditionTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private WorkVisaCondition workVisaCondition;
	FMLQueryResult fml =null; 
	
	public WorkVisaConditionTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(WorkVisaCondition.class.getSimpleName() + "DataSetTest.xml"));
	}
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.INSERT;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.workVisaCondition = new WorkVisaCondition();	
	}
	
	public void testExtract() throws Exception{
		fml=new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");			   	   
		String id=(String)testExtract.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);		
	    Deal deal=new Deal(srk,null,dealId, copyId);	    
	    DealEntity entity=(DealEntity)deal;	  
	    srk.getExpressState().setDealInstitutionId(1);
	   fml=workVisaCondition.extract(entity, srk);  
	   assertNotNull(fml);
} 
	

}
