package com.basis100.deal.conditions.sysgen;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;


/**
 * <p>DealQueryTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class GECapitalInsuranceConditionTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private GECapitalInsuranceCondition gECapitalInsuranceCondition;
	// session resource kit
    //private SessionResourceKit _srk;

	public GECapitalInsuranceConditionTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(GECapitalInsuranceCondition.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(GECapitalInsuranceCondition.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.gECapitalInsuranceCondition = new GECapitalInsuranceCondition();
	}
	
	public void testExtract() throws Exception {
    	
		// get input data from xml repository
    	ITable expectedAssignedTask = dataSetTest.getTable("testExtract");
    	FMLQueryResult fml=new FMLQueryResult();   
		    Deal deal=new Deal(srk,null);
		    deal.setDealId(1054864);
			deal.setCopyId(4);
			deal.setInstitutionProfileId(1);
			deal.setMIIndicatorId(1);
			DealEntity de=(DealEntity)deal;	
			   
			fml = gECapitalInsuranceCondition.extract(de, srk);
			assertNotNull(fml);
    }
	
}