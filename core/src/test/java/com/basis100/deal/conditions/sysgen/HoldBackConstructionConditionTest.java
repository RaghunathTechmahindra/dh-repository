package com.basis100.deal.conditions.sysgen;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;


/**
 * <p>DealQueryTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class HoldBackConstructionConditionTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private HoldBackConstructionCondition holdBackConstructionCondition;
	// session resource kit
    //private SessionResourceKit _srk;

	public HoldBackConstructionConditionTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(HoldBackConstructionCondition.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(HoldBackConstructionCondition.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.holdBackConstructionCondition = new HoldBackConstructionCondition();
	}
	
	public void testExtract() throws Exception {
    	
		// get input data from xml repository
    	ITable expectedAssignedTask = dataSetTest.getTable("testExtract");
    	int dealId=Integer.parseInt((String)expectedAssignedTask.getValue(0,"dealId"));
 	    int copyId=Integer.parseInt((String)expectedAssignedTask.getValue(0,"copyId"));
    	FMLQueryResult fml=new FMLQueryResult();   
		    Deal deal=new Deal(srk,null,dealId,copyId);
			DealEntity de=(DealEntity)deal;	
			   
			fml = holdBackConstructionCondition.extract(de, srk);
			assertNotNull(fml);
    }
	
}