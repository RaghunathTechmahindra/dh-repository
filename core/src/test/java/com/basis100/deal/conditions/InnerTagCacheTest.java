package com.basis100.deal.conditions;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.DealEntity;

public class InnerTagCacheTest extends FXDBTestCase  {


	IDataSet dataSetTest;
	InnerTagCache innerTagCache=null;
	DealEntity de=null;
	
	public InnerTagCacheTest(String name) throws IOException,DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("InnerTagCacheDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		
		return new FlatXmlDataSet(this.getClass().getResource("InnerTagCacheDataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		innerTagCache = InnerTagCache.getInstance();
	}
	
	public void testGetResult() throws Exception{
		
		ITable testGetResult = dataSetTest.getTable("testGetResult");
		String tagname=testGetResult.getValue(0, "tagname").toString();
		int lang=Integer.parseInt(testGetResult.getValue(0, "lang").toString());
		de=new DealEntity(srk);
		innerTagCache.getResult(srk,de,tagname,lang);
		
	}
	
	
}
