package com.basis100.deal.conditions;

import java.io.IOException;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.Deal;

public class SysGenCacheTest extends FXDBTestCase {

	IDataSet dataSetTest;
	SysGenCache sysGenCache=null;
	Deal deal=null;
	
	public SysGenCacheTest(String name) throws IOException,DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("SysGenCacheDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		
		return new FlatXmlDataSet(this.getClass().getResource("SysGenCacheDataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		sysGenCache = SysGenCache.getInstance();
	}
	//generateDocumentTracking
	
	public void testGenerateDocumentTracking()  throws Exception{
		
		ITable testGetResult = dataSetTest.getTable("testGenerateDocumentTracking");
		int userId=Integer.parseInt(testGetResult.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testGetResult.getValue(0, "copyId").toString());
		deal =new Deal(srk,null,userId,copyId);
		Collection result=sysGenCache.generateDocumentTracking(srk,deal);
		int resultSize=result.size();
		assertSame(0,resultSize);
		
	}
	
      public void testGenerateDocumentTrackingFailure()  throws Exception{
		
    	  boolean result=true;
    	  try{
		ITable testGetResult = dataSetTest.getTable("testGenerateDocumentTrackingFailure");
		int userId=Integer.parseInt(testGetResult.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testGetResult.getValue(0, "copyId").toString());
		deal =new Deal(srk,null,userId,copyId);
		sysGenCache.generateDocumentTracking(srk,deal);
		
    	  }catch(Exception e){
    		  result=true;
    	  }
		assertSame(true,result);
		
	}
	
	
}
