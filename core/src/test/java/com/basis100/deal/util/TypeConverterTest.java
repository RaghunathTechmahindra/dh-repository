package com.basis100.deal.util;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TypeConverterTest {

	// @BeforeClass
	// public static void setUpBeforeClass() throws Exception {
	// }
	//
	// @AfterClass
	// public static void tearDownAfterClass() throws Exception {
	// }

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDateFrom_existing() {

		// make sure existing function works
		// = {"ddMMyyyy", "dd/MM/yyyy", "yyyy-MM-dd HH:mm:ss" };
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");

		// test of "ddMMyyyy"
		Date d = TypeConverter.dateFrom("11122006");
		assertEquals("2006.12.11 00:00:00", sdf.format(d));

		// test of "dd/MM/yyyy"
		d = TypeConverter.dateFrom("12/01/2007");
		assertEquals("2007.01.12 00:00:00", sdf.format(d));

		// test of "yyyy-MM-dd HH:mm:ss"
		d = TypeConverter.dateFrom("2008-02-09 14:32:45");
		assertEquals("2008.02.09 14:32:45", sdf.format(d));

	}

	@Test
	public void testDateFrom_W3CDTF() {

		// make sure existing function works
		// = {"ddMMyyyy", "dd/MM/yyyy", "yyyy-MM-dd HH:mm:ss" };
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		
		// test of "ddMMyyyy"

		// Year:
		// YYYY (eg 1997)
		
		Date d = TypeConverter.dateFrom("1997");
		System.out.println(sdf.format(d));
		assertNotSame("1997.01.01", sdf.format(d));
		
		// Year and month:
		// YYYY-MM (eg 1997-07)
		d = TypeConverter.dateFrom("1997-07");
		System.out.println("**********"+sdf.format(d));
		//assertSame("1997.06.30", sdf.format(d));
		//assertNotNull(d);
		// Complete date:
		// YYYY-MM-DD (eg 1997-07-16)
		d = TypeConverter.dateFrom("1997-07-16");
		System.out.println("**********"+sdf.format(d));
		assertNotSame("1997.07.16", sdf.format(d));
		//assertNotNull(d);
		
		
		sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss.SS Z");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		// Complete date plus hours and minutes:
		// YYYY-MM-DDThh:mmTZD (eg 1997-07-16T19:20+01:00)
		d = TypeConverter.dateFrom("1997-07-16T19:20+01:00");
		System.out.println(sdf.format(d));
		assertEquals("1997.07.16 18:20:00.00 +0000", sdf.format(d));
		
		// Complete date plus hours, minutes and seconds:
		// YYYY-MM-DDThh:mm:ssTZD (eg 1997-07-16T19:20:30+01:00)
		d = TypeConverter.dateFrom("1997-07-16T19:20:30+01:00");
		System.out.println(sdf.format(d));
		assertEquals("1997.07.16 18:20:30.00 +0000", sdf.format(d));
		
		// Complete date plus hours, minutes, seconds and a decimal fraction of a second
		// YYYY-MM-DDThh:mm:ss.sTZD (eg 1997-07-16T19:20:30.45+01:00)
		d = TypeConverter.dateFrom("1997-07-16T19:20:30.45+01:00");
		System.out.println(sdf.format(d));
		assertEquals("1997.07.16 18:20:30.45 +0000", sdf.format(d));

		// YYYY-MM-DDThh:mm:ss.sZ (eg 1997-07-16T19:20:30.45+0100)
		d = TypeConverter.dateFrom("1997-07-16T19:20:30+0100");
		System.out.println(sdf.format(d));
		assertEquals("1997.07.16 18:20:30.00 +0000", sdf.format(d));
		
		d = TypeConverter.dateFrom("1997-07-16T19:20-30+0100");
		System.out.println(d);
		assertNull(d);

		// test of W3CDTF "yyyy-MM-dd HH:mm:ss"

	}

}
