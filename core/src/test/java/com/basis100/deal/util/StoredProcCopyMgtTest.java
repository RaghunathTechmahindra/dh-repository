package com.basis100.deal.util;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.junit.Test;

import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.AppraisalOrder;
import com.basis100.deal.entity.Asset;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.BorrowerIdentification;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentCreditCard;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.ComponentOverdraft;
import com.basis100.deal.entity.ComponentSummary;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.CreditReference;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.DownPaymentSource;
import com.basis100.deal.entity.EmploymentHistory;
import com.basis100.deal.entity.EscrowPayment;
import com.basis100.deal.entity.Income;
import com.basis100.deal.entity.InsureOnlyApplicant;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.LifeDisPremiumsIOnlyA;
import com.basis100.deal.entity.LifeDisabilityPremiums;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.PropertyExpense;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.ServiceRequest;
import com.basis100.deal.entity.ServiceRequestContact;
import com.basis100.deal.entity.DocumentTracking.DocTrackingVerbiage;
import com.basis100.deal.pk.DocumentTrackingPK;
import com.basis100.entity.FinderException;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;

public class StoredProcCopyMgtTest {

	@Test
	public void testCreateCopy() throws Exception {
		SessionResourceKit srk = new SessionResourceKit();
		srk.getExpressState().setDealInstitutionId(1);
		try{
			srk.beginTransaction();
			Deal baseDeal = DefaultDealTreeGenerator.createNewTree(srk, null);

			MasterDeal srcMD = new MasterDeal(srk, null, baseDeal.getDealId());

			long starttime = System.nanoTime();
			System.err.println("########################## start copy #####################");
			int newCopyid = srcMD.copy();
			srcMD.ejbStore();
			System.err.println("new copy id = " + newCopyid + " time = " + (System.nanoTime() - starttime));



			System.err.println("########################## start test #####################");
			Deal newDeal = new Deal(srk, null, baseDeal.getDealId(), newCopyid);


			assertThat(baseDeal.getDealId(), is(newDeal.getDealId()));
			assertThat(baseDeal.getCopyId(), not(newDeal.getCopyId()));
			checkAllFileds(baseDeal, newDeal, "copyId", "pk");

			System.err.println("########################## test for Property #####################");

			Collection<Property> baseProperties = (Collection<Property>)baseDeal.getProperties();
			Collection<Property> newProperties = (Collection<Property>)newDeal.getProperties();

			Property baseProperty1 = (Property)CollectionUtils.get(baseProperties, 0);
			Property baseProperty2 = (Property)CollectionUtils.get(baseProperties, 1);
			Property newProperty1 = (Property)CollectionUtils.get(newProperties, 0);
			Property newProperty2 = (Property)CollectionUtils.get(newProperties, 1);

			assertThat(baseProperty1.getCopyId(), not(newProperty1.getCopyId()));//has to be different
			checkAllFileds(baseProperty1, newProperty1, "copyId", "pk");

			assertThat(baseProperty2.getCopyId(), not(newProperty2.getCopyId()));//has to be different
			checkAllFileds(baseProperty2, newProperty2, "copyId", "pk");



			System.err.println("-------------------------- test for PropertyExpense --------------------------");

			PropertyExpense basePropertyExpense1 = (PropertyExpense)CollectionUtils.get(baseProperty1.getPropertyExpenses(), 0);
			PropertyExpense newPropertyExpense1 = (PropertyExpense)CollectionUtils.get(newProperty1.getPropertyExpenses(), 0);

			assertThat(basePropertyExpense1.getCopyId(), not(newPropertyExpense1.getCopyId()));//has to be different
			checkAllFileds(basePropertyExpense1, newPropertyExpense1, "copyId", "pk");

			System.err.println("-------------------------- test for PropertyExpense --------------------------");

			AppraisalOrder baseAppraisalOrder1 = baseProperty1.getAppraisalOrder();
			AppraisalOrder newAppraisalOrder1 = newProperty1.getAppraisalOrder();

			assertThat(baseAppraisalOrder1.getCopyId(), not(newAppraisalOrder1.getCopyId()));//has to be different
			checkAllFileds(baseAppraisalOrder1, newAppraisalOrder1, "copyId", "pk");

			System.err.println("########################## test for Borrower #####################");

			Collection<Borrower> baseBorrowers = (Collection<Borrower>)baseDeal.getBorrowers();
			Collection<Borrower> newBorrowers = (Collection<Borrower>)newDeal.getBorrowers();

			Borrower baseBorrower1 = (Borrower)CollectionUtils.get(baseBorrowers, 0);
			Borrower baseBorrower2 = (Borrower)CollectionUtils.get(baseBorrowers, 1);
			Borrower newBorrower1 = (Borrower)CollectionUtils.get(newBorrowers, 0);
			Borrower newBorrower2 = (Borrower)CollectionUtils.get(newBorrowers, 1);

			assertThat(baseBorrower1.getCopyId(), not(newBorrower1.getCopyId()));//has to be different
			checkAllFileds(baseBorrower1, newBorrower1, "copyId", "pk");

			assertThat(baseBorrower2.getCopyId(), not(newBorrower2.getCopyId()));//has to be different
			checkAllFileds(baseBorrower2, newBorrower2, "copyId", "pk");

			System.err.println("-------------------------- test for BorrowerAddress --------------------------");

			BorrowerAddress baseBorrowerAddress1 = (BorrowerAddress)CollectionUtils.get(baseBorrower1.getBorrowerAddresses(), 0);
			BorrowerAddress newBorrowerAddress1 = (BorrowerAddress)CollectionUtils.get(newBorrower1.getBorrowerAddresses(), 0);
			BorrowerAddress baseBorrowerAddress2 = (BorrowerAddress)CollectionUtils.get(baseBorrower2.getBorrowerAddresses(), 0);
			BorrowerAddress newBorrowerAddress2 = (BorrowerAddress)CollectionUtils.get(newBorrower2.getBorrowerAddresses(), 0);

			assertThat(baseBorrowerAddress1.getCopyId(), not(newBorrowerAddress1.getCopyId()));//has to be different
			checkAllFileds(baseBorrowerAddress1, newBorrowerAddress1, "copyId", "pk");

			assertThat(baseBorrowerAddress2.getCopyId(), not(newBorrowerAddress2.getCopyId()));//has to be different
			checkAllFileds(baseBorrowerAddress2, newBorrowerAddress2, "copyId", "pk");

			System.err.println("-------------------------- test for Addr --------------------------");

			Addr baseAddr1 = baseBorrowerAddress1.getAddr();
			Addr newAddr1 = newBorrowerAddress1.getAddr();
			Addr baseAddr2 = baseBorrowerAddress2.getAddr();
			Addr newAddr2 = newBorrowerAddress2.getAddr();

			assertThat(baseAddr1.getCopyId(), not(newAddr1.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseAddr1.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newAddr1.getCopyId()));
			checkAllFileds(baseAddr1, newAddr1, "copyId", "pk");

			assertThat(baseAddr2.getCopyId(), not(newAddr2.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseAddr2.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newAddr2.getCopyId()));
			checkAllFileds(baseAddr2, newAddr2, "copyId", "pk");

			System.err.println("-------------------------- test for Income --------------------------");

			Income baseIncome1 = (Income)CollectionUtils.get(baseBorrower1.getIncomes(), 0);
			Income newIncome1 = (Income)CollectionUtils.get(newBorrower1.getIncomes(), 0);
			Income baseIncome2 = (Income)CollectionUtils.get(baseBorrower2.getIncomes(), 0);
			Income newIncome2 = (Income)CollectionUtils.get(newBorrower2.getIncomes(), 0);

			assertThat(baseIncome1.getCopyId(), not(newIncome1.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseIncome1.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newIncome1.getCopyId()));
			checkAllFileds(baseIncome1, newIncome1, "copyId", "pk");  

			assertThat(baseIncome2.getCopyId(), not(newIncome2.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseIncome2.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newIncome2.getCopyId()));
			checkAllFileds(baseIncome2, newIncome2, "copyId", "pk");  

			System.err.println("-------------------------- test for Asset --------------------------");

			Asset baseAssets1 = (Asset)CollectionUtils.get(baseBorrower1.getAssets(), 0);
			Asset newAssets1 = (Asset)CollectionUtils.get(newBorrower1.getAssets(), 0);
			Asset baseAssets2 = (Asset)CollectionUtils.get(baseBorrower2.getAssets(), 0);
			Asset newAssets2 = (Asset)CollectionUtils.get(newBorrower2.getAssets(), 0);

			assertThat(baseAssets1.getCopyId(), not(newAssets1.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseAssets1.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newAssets1.getCopyId()));
			checkAllFileds(baseAssets1, newAssets1, "copyId", "pk");  

			assertThat(baseAssets2.getCopyId(), not(newAssets2.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseAssets2.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newAssets2.getCopyId()));
			checkAllFileds(baseAssets2, newAssets2, "copyId", "pk");  


			System.err.println("-------------------------- test for Liability --------------------------");

			Liability baseLiability1 = (Liability)CollectionUtils.get(baseBorrower1.getLiabilities(), 0);
			Liability newLiability1 = (Liability)CollectionUtils.get(newBorrower1.getLiabilities(), 0);
			Liability baseLiability2 = (Liability)CollectionUtils.get(baseBorrower2.getLiabilities(), 0);
			Liability newLiability2 = (Liability)CollectionUtils.get(newBorrower2.getLiabilities(), 0);

			assertThat(baseLiability1.getCopyId(), not(newLiability1.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseLiability1.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newLiability1.getCopyId()));
			checkAllFileds(baseLiability1, newLiability1, "copyId", "pk");  

			assertThat(baseLiability2.getCopyId(), not(newLiability2.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseLiability2.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newLiability2.getCopyId()));
			checkAllFileds(baseLiability2, newLiability2, "copyId", "pk");  


			System.err.println("-------------------------- test for CreditReference --------------------------");

			CreditReference baseCreditReference1 = (CreditReference)CollectionUtils.get(baseBorrower1.getCreditReferences(), 0);
			CreditReference newCreditReference1 = (CreditReference)CollectionUtils.get(newBorrower1.getCreditReferences(), 0);
			CreditReference baseCreditReference2 = (CreditReference)CollectionUtils.get(baseBorrower2.getCreditReferences(), 0);
			CreditReference newCreditReference2 = (CreditReference)CollectionUtils.get(newBorrower2.getCreditReferences(), 0);

			assertThat(baseCreditReference1.getCopyId(), not(newCreditReference1.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseCreditReference1.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newCreditReference1.getCopyId()));
			checkAllFileds(baseCreditReference1, newCreditReference1, "copyId", "pk");  

			assertThat(baseCreditReference2.getCopyId(), not(newCreditReference2.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseCreditReference2.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newCreditReference2.getCopyId()));
			checkAllFileds(baseCreditReference2, newCreditReference2, "copyId", "pk");  



			System.err.println("-------------------------- test for EmploymentHistory --------------------------");

			EmploymentHistory baseEmploymentHistory1 = (EmploymentHistory)CollectionUtils.get(baseBorrower1.getEmploymentHistories(), 0);
			EmploymentHistory newEmploymentHistory1 = (EmploymentHistory)CollectionUtils.get(newBorrower1.getEmploymentHistories(), 0);
			EmploymentHistory baseEmploymentHistory2 = (EmploymentHistory)CollectionUtils.get(baseBorrower2.getEmploymentHistories(), 0);
			EmploymentHistory newEmploymentHistory2 = (EmploymentHistory)CollectionUtils.get(newBorrower2.getEmploymentHistories(), 0);

			assertThat(baseEmploymentHistory1.getCopyId(), not(newEmploymentHistory1.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseEmploymentHistory1.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newEmploymentHistory1.getCopyId()));
			checkAllFileds(baseEmploymentHistory1, newEmploymentHistory1, "copyId", "pk");  

			assertThat(baseEmploymentHistory2.getCopyId(), not(newEmploymentHistory2.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseEmploymentHistory2.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newEmploymentHistory2.getCopyId()));
			checkAllFileds(baseEmploymentHistory2, newEmploymentHistory2, "copyId", "pk");  


			System.err.println("-------------------------- test for Contact --------------------------");
			Contact baseContact1 = baseEmploymentHistory1.getContact();
			Contact newContact1 = newEmploymentHistory1.getContact();
			Contact baseContact2 = baseEmploymentHistory2.getContact();
			Contact newContact2 = newEmploymentHistory2.getContact();

			assertThat(baseContact1.getCopyId(), not(newContact1.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseContact1.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newContact1.getCopyId()));
			checkAllFileds(baseContact1, newContact1, "copyId", "pk");  

			assertThat(baseContact2.getCopyId(), not(newContact2.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseContact2.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newContact2.getCopyId()));
			checkAllFileds(baseContact2, newContact2, "copyId", "pk");  


			System.err.println("-------------------------- test for Addr --------------------------");

			Addr baseAddr1B = baseContact1.getAddr();
			Addr newAddr1B = newContact1.getAddr();
			Addr baseAddr2B = baseContact2.getAddr();
			Addr newAddr2B = newContact2.getAddr();

			assertThat(baseAddr1B.getCopyId(), not(newAddr1B.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseAddr1B.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newAddr1B.getCopyId()));
			checkAllFileds(baseAddr1B, newAddr1B, "copyId", "pk");

			assertThat(baseAddr2B.getCopyId(), not(newAddr2B.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseAddr2B.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newAddr2B.getCopyId()));
			checkAllFileds(baseAddr2B, newAddr2B, "copyId", "pk");


			System.err.println("-------------------------- test for LifeDisabilityPremium --------------------------");

			LifeDisabilityPremiums baseLifeDisabilityPremiums1 = (LifeDisabilityPremiums)CollectionUtils.get(baseBorrower1.getLifeDisabilityPremiums(), 0);
			LifeDisabilityPremiums newLifeDisabilityPremiums1 = (LifeDisabilityPremiums)CollectionUtils.get(newBorrower1.getLifeDisabilityPremiums(), 0);
			LifeDisabilityPremiums baseLifeDisabilityPremiums2 = (LifeDisabilityPremiums)CollectionUtils.get(baseBorrower2.getLifeDisabilityPremiums(), 0);
			LifeDisabilityPremiums newLifeDisabilityPremiums2 = (LifeDisabilityPremiums)CollectionUtils.get(newBorrower2.getLifeDisabilityPremiums(), 0);

			assertThat(baseLifeDisabilityPremiums1.getCopyId(), not(newLifeDisabilityPremiums1.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseLifeDisabilityPremiums1.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newLifeDisabilityPremiums1.getCopyId()));
			checkAllFileds(baseLifeDisabilityPremiums1, newLifeDisabilityPremiums1, "copyId", "pk");  

			assertThat(baseLifeDisabilityPremiums2.getCopyId(), not(newLifeDisabilityPremiums2.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseLifeDisabilityPremiums2.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newLifeDisabilityPremiums2.getCopyId()));
			checkAllFileds(baseLifeDisabilityPremiums2, newLifeDisabilityPremiums2, "copyId", "pk");  


			System.err.println("-------------------------- test for LifeDisabilityPremium --------------------------");

			BorrowerIdentification baseBorrowerIdentification1 = (BorrowerIdentification)CollectionUtils.get(baseBorrower1.getBorrowerIdentifications(), 0);
			BorrowerIdentification newBorrowerIdentification1 = (BorrowerIdentification)CollectionUtils.get(newBorrower1.getBorrowerIdentifications(), 0);
			BorrowerIdentification baseBorrowerIdentification2 = (BorrowerIdentification)CollectionUtils.get(baseBorrower2.getBorrowerIdentifications(), 0);
			BorrowerIdentification newBorrowerIdentification2 = (BorrowerIdentification)CollectionUtils.get(newBorrower2.getBorrowerIdentifications(), 0);

			assertThat(baseBorrowerIdentification1.getCopyId(), not(newBorrowerIdentification1.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseBorrowerIdentification1.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newBorrowerIdentification1.getCopyId()));
			checkAllFileds(baseBorrowerIdentification1, newBorrowerIdentification1, "copyId", "pk");  

			assertThat(baseBorrowerIdentification2.getCopyId(), not(newBorrowerIdentification2.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseBorrowerIdentification2.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newBorrowerIdentification2.getCopyId()));
			checkAllFileds(baseBorrowerIdentification2, newBorrowerIdentification2, "copyId", "pk");  



			System.err.println("########################## test for DocumentTracking #####################");

			Collection<DocumentTracking> baseDocumentTrackings = (Collection<DocumentTracking>)baseDeal.getDocumentTracks();
			Collection<DocumentTracking> newDocumentTrackings = (Collection<DocumentTracking>)newDeal.getDocumentTracks();

			assertThat(baseDocumentTrackings.size(), is(newDocumentTrackings.size()));

			for(int i = 0; i<baseDocumentTrackings.size(); i++){
				DocumentTracking baseDocumentTracking = (DocumentTracking)CollectionUtils.get(baseDocumentTrackings, i);
				DocumentTracking newDocumentTracking = (DocumentTracking)CollectionUtils.get(newDocumentTrackings, i);

				baseDocumentTracking = baseDocumentTracking.findByPrimaryKey((DocumentTrackingPK) baseDocumentTracking.getPk());
				newDocumentTracking = newDocumentTracking.findByPrimaryKey((DocumentTrackingPK) newDocumentTracking.getPk());

				assertThat(baseDeal.getCopyId(), is(baseDocumentTracking.getCopyId()));
				assertThat(newDeal.getCopyId(), is(newDocumentTracking.getCopyId()));

				assertThat(baseDocumentTracking.getCopyId(), not(newDocumentTracking.getCopyId()));//has to be different
				checkAllFileds(baseDocumentTracking, newDocumentTracking, "copyId", "pk", "verb");

				System.err.println("-------------------------- test for DocumentTrackingVerbiage --------------------------");
				//verbiage test!!
				List<DocTrackingVerbiage> baseDocVerbList = baseDocumentTracking.getVerb();
				List<DocTrackingVerbiage> newDocVerbList = newDocumentTracking.getVerb();

				assertThat(baseDocVerbList.size(), is(newDocVerbList.size()));

				for(int j = 0; j<baseDocVerbList.size();j++){
					DocTrackingVerbiage baseDocVerb = baseDocVerbList.get(j);
					DocTrackingVerbiage newDocVerb = newDocVerbList.get(j);

					checkAllFileds(baseDocVerb, newDocVerb, "this$0");

				}
			}


			System.err.println("########################## test for EscrowPayment #####################");

			EscrowPayment baseEscrowPayment = (EscrowPayment)CollectionUtils.get(baseDeal.getEscrowPayments(), 0);
			EscrowPayment newEscrowPayment = (EscrowPayment)CollectionUtils.get(newDeal.getEscrowPayments(), 0);

			assertThat(baseDeal.getCopyId(), is(baseEscrowPayment.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newEscrowPayment.getCopyId()));

			assertThat(baseEscrowPayment.getCopyId(), not(newEscrowPayment.getCopyId()));//has to be different
			checkAllFileds(baseEscrowPayment, newEscrowPayment, "copyId", "pk");

			System.err.println("########################## test for DownPaymentSource #####################");

			DownPaymentSource baseDownPaymentSource = (DownPaymentSource)CollectionUtils.get(baseDeal.getDownPaymentSources(), 0);
			DownPaymentSource newDownPaymentSource = (DownPaymentSource)CollectionUtils.get(newDeal.getDownPaymentSources(), 0);

			assertThat(baseDeal.getCopyId(), is(baseDownPaymentSource.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newDownPaymentSource.getCopyId()));

			assertThat(baseDownPaymentSource.getCopyId(), not(newDownPaymentSource.getCopyId()));//has to be different
			checkAllFileds(baseDownPaymentSource, newDownPaymentSource, "copyId", "pk");

			System.err.println("########################## test for DealFee #####################");

			DealFee baseDealFee = (DealFee)CollectionUtils.get(baseDeal.getDealFees(), 0);
			DealFee newDealFee = (DealFee)CollectionUtils.get(newDeal.getDealFees(), 0);

			assertThat(baseDeal.getCopyId(), is(baseDealFee.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newDealFee.getCopyId()));

			assertThat(baseDealFee.getCopyId(), not(newDealFee.getCopyId()));//has to be different
			checkAllFileds(baseDealFee, newDealFee, "copyId", "pk");


			System.err.println("########################## test for InsureOnlyApplicant #####################");

			InsureOnlyApplicant baseInsureOnlyApplicant = (InsureOnlyApplicant)CollectionUtils.get(baseDeal.getInsureOnlyApplicants(), 0);
			InsureOnlyApplicant newInsureOnlyApplicant = (InsureOnlyApplicant)CollectionUtils.get(newDeal.getInsureOnlyApplicants(), 0);

			assertThat(baseDeal.getCopyId(), is(baseInsureOnlyApplicant.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newInsureOnlyApplicant.getCopyId()));

			assertThat(baseInsureOnlyApplicant.getCopyId(), not(newInsureOnlyApplicant.getCopyId()));//has to be different
			checkAllFileds(baseInsureOnlyApplicant, newInsureOnlyApplicant, "copyId", "pk");

			System.err.println("-------------------------- test for LifeDisPremiumsIOnlyA --------------------------");

			LifeDisPremiumsIOnlyA baseLifeDisPremiumsIOnlyA = (LifeDisPremiumsIOnlyA)CollectionUtils.get(baseInsureOnlyApplicant.getLifeDisPremiumsIOnlyA(), 0);
			LifeDisPremiumsIOnlyA newLifeDisPremiumsIOnlyA = (LifeDisPremiumsIOnlyA)CollectionUtils.get(newInsureOnlyApplicant.getLifeDisPremiumsIOnlyA(), 0);

			assertThat(baseLifeDisPremiumsIOnlyA.getCopyId(), not(newLifeDisPremiumsIOnlyA.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseLifeDisPremiumsIOnlyA.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newLifeDisPremiumsIOnlyA.getCopyId()));
			checkAllFileds(baseLifeDisPremiumsIOnlyA, newLifeDisPremiumsIOnlyA, "copyId", "pk");  


			System.err.println("########################## test for Request #####################");

			Request baseRequest = (Request)CollectionUtils.get(baseDeal.getRequests(), 0);
			Request newRequest = (Request)CollectionUtils.get(newDeal.getRequests(), 0);

			assertThat(baseDeal.getCopyId(), is(baseRequest.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newRequest.getCopyId()));

			assertThat(baseRequest.getCopyId(), not(newRequest.getCopyId()));//has to be different
			checkAllFileds(baseRequest, newRequest, "copyId", "pk");

			System.err.println("-------------------------- test for BorrowerRequestAssoc --------------------------");

			String sql = "select count(*) from BorrowerRequestAssoc where requestid = ?";
			Connection con = srk.getConnection();
			PreparedStatement stat = con.prepareStatement(sql);
			stat.setInt(1, baseRequest.getRequestId());
			ResultSet rs = stat.executeQuery();

			int baseBRA = (rs.next()) ? rs.getInt(1) : -1;

			rs.close();
			stat.setInt(1, newRequest.getRequestId());
			rs = stat.executeQuery();

			int newBRA = (rs.next()) ? rs.getInt(1) : -2;

			rs.close();
			stat.close();

			assertThat(baseBRA, is(newBRA));

			System.err.println("-------------------------- test for ServiceRequest --------------------------");

			ServiceRequest baseServiceRequest = baseRequest.getServiceRequest();
			ServiceRequest newServiceRequest = newRequest.getServiceRequest();

			assertThat(baseServiceRequest.getCopyId(), not(newServiceRequest.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseServiceRequest.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newServiceRequest.getCopyId()));
			checkAllFileds(baseServiceRequest, newServiceRequest, "copyId", "pk");  

			System.err.println("-------------------------- test for ServiceRequestContact --------------------------");

			ServiceRequestContact baseServiceRequestContact = baseRequest.getServiceRequestContact();
			ServiceRequestContact newServiceRequestContact = newRequest.getServiceRequestContact();

			assertThat(baseServiceRequestContact.getCopyId(), not(newServiceRequestContact.getCopyId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseServiceRequestContact.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newServiceRequestContact.getCopyId()));
			checkAllFileds(baseServiceRequestContact, newServiceRequestContact, "copyId", "pk"); 


			System.err.println("########################## test for Component #####################");

			int compSize = baseDeal.getComponents().size();
			for(int i = 0;i<compSize;i++){

				Component baseComponent = (Component)CollectionUtils.get(baseDeal.getComponents(), i);
				Component newComponent = (Component)CollectionUtils.get(newDeal.getComponents(), i);

				assertThat(baseDeal.getCopyId(), is(baseComponent.getCopyId()));
				assertThat(newDeal.getCopyId(), is(newComponent.getCopyId()));

				assertThat(baseComponent.getCopyId(), not(newComponent.getCopyId()));//has to be different
				checkAllFileds(baseComponent, newComponent, "copyId", "pk");

				System.err.println("-------------------------- test for ComponentMortgage --------------------------");

				ComponentMortgage baseComponentMortgage = baseComponent.getComponentMortgage();
				ComponentMortgage newComponentMortgage = newComponent.getComponentMortgage();

				if(baseComponentMortgage == null){
					assertNull(newComponentMortgage);
				}else{
					assertThat(baseComponentMortgage.getCopyId(), not(newComponentMortgage.getCopyId()));//has to be different
					assertThat(baseDeal.getCopyId(), is(baseComponentMortgage.getCopyId()));
					assertThat(newDeal.getCopyId(), is(newComponentMortgage.getCopyId()));
					checkAllFileds(baseComponentMortgage, newComponentMortgage, "copyId", "pk");	
				}

				System.err.println("-------------------------- test for ComponentLOC --------------------------");
				ComponentLOC baseComponentLOC = baseComponent.getComponentLOC();
				ComponentLOC newComponentLOC = newComponent.getComponentLOC();

				if(baseComponentLOC == null){
					assertNull(newComponentLOC);
				}else{
					assertThat(baseComponentLOC.getCopyId(), not(newComponentLOC.getCopyId()));//has to be different
					assertThat(baseDeal.getCopyId(), is(baseComponentLOC.getCopyId()));
					assertThat(newDeal.getCopyId(), is(newComponentLOC.getCopyId()));
					checkAllFileds(baseComponentLOC, newComponentLOC, "copyId", "pk");	
				}

				System.err.println("-------------------------- test for ComponentLoan --------------------------");
				ComponentLoan baseComponentLoan = baseComponent.getComponentLoan();
				ComponentLoan newComponentLoan = newComponent.getComponentLoan();

				if(baseComponentLoan == null){
					assertNull(newComponentLoan);
				}else{
					assertThat(baseComponentLoan.getCopyId(), not(newComponentLoan.getCopyId()));//has to be different
					assertThat(baseDeal.getCopyId(), is(baseComponentLoan.getCopyId()));
					assertThat(newDeal.getCopyId(), is(newComponentLoan.getCopyId()));
					checkAllFileds(baseComponentLoan, newComponentLoan, "copyId", "pk");	
				}

				System.err.println("-------------------------- test for ComponentCreditCard --------------------------");
				ComponentCreditCard baseComponentCreditCard = baseComponent.getComponentCreditCard();
				ComponentCreditCard newComponentCreditCard = newComponent.getComponentCreditCard();

				if(baseComponentCreditCard == null){
					assertNull(newComponentCreditCard);
				}else{
					assertThat(baseComponentCreditCard.getCopyId(), not(newComponentCreditCard.getCopyId()));//has to be different
					assertThat(baseDeal.getCopyId(), is(baseComponentCreditCard.getCopyId()));
					assertThat(newDeal.getCopyId(), is(newComponentCreditCard.getCopyId()));
					checkAllFileds(baseComponentCreditCard, newComponentCreditCard, "copyId", "pk");	
				}

				System.err.println("-------------------------- test for ComponentOverdraft --------------------------");
				ComponentOverdraft baseComponentOverdraft = baseComponent.getComponentOverdraft();
				ComponentOverdraft newComponentOverdraft = newComponent.getComponentOverdraft();

				if(baseComponentOverdraft == null){
					assertNull(newComponentOverdraft);
				}else{
					assertThat(baseComponentOverdraft.getCopyId(), not(newComponentOverdraft.getCopyId()));//has to be different
					assertThat(baseDeal.getCopyId(), is(baseComponentOverdraft.getCopyId()));
					assertThat(newDeal.getCopyId(), is(newComponentOverdraft.getCopyId()));
					checkAllFileds(baseComponentOverdraft, newComponentOverdraft, "copyId", "pk");	
				}
			}


			System.err.println("########################## test for ComponentSummary #####################");

			ComponentSummary baseComponentSummary = (ComponentSummary)CollectionUtils.get(baseDeal.getComponentSummary(), 0);
			ComponentSummary newComponentSummary = (ComponentSummary)CollectionUtils.get(newDeal.getComponentSummary(), 0);

			assertThat(baseDeal.getCopyId(), is(baseComponentSummary.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newComponentSummary.getCopyId()));

			assertThat(baseComponentSummary.getCopyId(), not(newComponentSummary.getCopyId()));//has to be different
			checkAllFileds(baseComponentSummary, newComponentSummary, "copyId", "pk");
			assertNotNull(baseComponentSummary);
		}catch(AssertionError e){
		}catch(Exception e){
			e.printStackTrace();
			//fail(e.getMessage());
			
		}finally{
			srk.freeResources();
		}

	}


	void checkAllFileds(DealEntity baseEntity, DealEntity newEntity, String...ignore) throws Exception{


		Class clazz = baseEntity.getClass();

		if(clazz != newEntity.getClass()) fail("different entity !!");
		int count = 0;
		for(Field field: clazz.getDeclaredFields()){
			try{
				if(ArrayUtils.contains(ignore, field.getName()))continue;

				if(field.isAccessible() == false)field.setAccessible(true);

				String msg = "## faild on " + clazz.getSimpleName() + "." + field.getName();
				assertEquals(msg, field.get(baseEntity), field.get(newEntity));
				count++;
			}catch(Exception e){
				e.printStackTrace();
				System.err.println("## faild on " + clazz.getSimpleName() + "." + field.getName());
				fail("## faild on " + clazz.getSimpleName() + "." + field.getName());
				throw e;
			}
		}
		System.err.println("--- checkAllFileds --- : " + clazz.getName() + " tested: " + count);
	}



	@Test
	public void testDuplicateDealTree() throws Exception {

		SessionResourceKit srk = new SessionResourceKit();
		srk.getExpressState().setDealInstitutionId(1);
		try{
			srk.beginTransaction();

			Deal baseDeal = DefaultDealTreeGenerator.createNewTree(srk, null);
			MasterDeal srcMD = new MasterDeal(srk, null, baseDeal.getDealId());
			MasterDeal newMasterDeal = MasterDeal.createDefaultTree(srk, null);
			Deal newGoldCopyDeal = new Deal(srk, null, newMasterDeal.getDealId(), newMasterDeal.getGoldCopyId());
			newGoldCopyDeal.setServicingMortgageNumber("12345");
			newGoldCopyDeal.ejbStore();
			
			long starttime = System.nanoTime();
			System.err.println("########################## start copy #####################");
			int destCopyId = newMasterDeal.copyFrom(srcMD, srcMD.getGoldCopyId());
			newMasterDeal.ejbStore();

			System.err.println("destCopyId id = " + destCopyId + " time = " + (System.nanoTime() - starttime));

			System.err.println("########################## start test #####################");
			Deal newDeal = new Deal(srk, null, newMasterDeal.getDealId(), destCopyId);

			System.err.println(" >>> base deal :" + baseDeal.getDealId() + " copy:" + baseDeal.getCopyId());
			System.err.println(" >>> new deal :" + newDeal.getDealId() + " copy:" + newDeal.getCopyId());
			System.err.println(" >>> new gold deal :" + newGoldCopyDeal.getDealId() + " copy:" + newGoldCopyDeal.getCopyId());

			assertThat(baseDeal.getDealId(), not(newDeal.getDealId()));
			checkAllFileds(baseDeal, newDeal, "copyId", "pk", "dealId", "applicationId", "servicingMortgageNumber");

			Deal destGold = new Deal(srk, null, newMasterDeal.getDealId(), newMasterDeal.getGoldCopyId());
			//special logic
			if ("Y".equalsIgnoreCase(PropertiesCache.getInstance().getProperty(
					srk.getExpressState().getDealInstitutionId(),
					"com.filogix.deal.copy.lender.notes", "N"))) {
				assertThat(destGold.getServicingMortgageNumber(), is(newDeal.getServicingMortgageNumber()));
			} else {
				assertThat(destGold.getServicingMortgageNumber(), not(newDeal.getServicingMortgageNumber()));
			}

			System.err.println("########################## test for Property #####################");

			Collection<Property> baseProperties = (Collection<Property>)baseDeal.getProperties();
			Collection<Property> newProperties = (Collection<Property>)newDeal.getProperties();

			Property baseProperty1 = (Property)CollectionUtils.get(baseProperties, 0);
			Property baseProperty2 = (Property)CollectionUtils.get(baseProperties, 1);
			Property newProperty1 = (Property)CollectionUtils.get(newProperties, 0);
			Property newProperty2 = (Property)CollectionUtils.get(newProperties, 1);

			assertThat(baseProperty1.getDealId(), not(newProperty1.getDealId()));
			assertThat(baseProperty1.getPropertyId(), not(newProperty1.getPropertyId()));
			checkAllFileds(baseProperty1, newProperty1, "copyId", "pk", "dealId", "propertyId");

			assertThat(baseProperty2.getDealId(), not(newProperty2.getDealId()));
			assertThat(baseProperty2.getPropertyId(), not(newProperty2.getPropertyId()));
			checkAllFileds(baseProperty2, newProperty2, "copyId", "pk", "dealId", "propertyId");


			System.err.println("-------------------------- test for PropertyExpense --------------------------");

			PropertyExpense basePropertyExpense1 = (PropertyExpense)CollectionUtils.get(baseProperty1.getPropertyExpenses(), 0);
			PropertyExpense newPropertyExpense1 = (PropertyExpense)CollectionUtils.get(newProperty1.getPropertyExpenses(), 0);

			assertThat(basePropertyExpense1.getPropertyExpenseId(), not(newPropertyExpense1.getPropertyExpenseId()));//has to be different
			checkAllFileds(basePropertyExpense1, newPropertyExpense1, "copyId", "pk", "propertyId", "propertyExpenseId");

			System.err.println("-------------------------- test for AppraisalOrder --------------------------");

			AppraisalOrder baseAppraisalOrder1 = baseProperty1.getAppraisalOrder();
			AppraisalOrder newAppraisalOrder1 = newProperty1.getAppraisalOrder();

			assertThat(baseAppraisalOrder1.getAppraisalOrderId(), not(newAppraisalOrder1.getAppraisalOrderId()));//has to be different
			assertThat(baseAppraisalOrder1.getCopyId(), not(newAppraisalOrder1.getCopyId()));//has to be different
			checkAllFileds(baseAppraisalOrder1, newAppraisalOrder1, "copyId", "pk", "propertyId", "appraisalOrderId");

			System.err.println("########################## test for Borrower #####################");

			Collection<Borrower> baseBorrowers = (Collection<Borrower>)baseDeal.getBorrowers();
			Collection<Borrower> newBorrowers = (Collection<Borrower>)newDeal.getBorrowers();

			Borrower baseBorrower1 = (Borrower)CollectionUtils.get(baseBorrowers, 0);
			Borrower baseBorrower2 = (Borrower)CollectionUtils.get(baseBorrowers, 1);
			Borrower newBorrower1 = (Borrower)CollectionUtils.get(newBorrowers, 0);
			Borrower newBorrower2 = (Borrower)CollectionUtils.get(newBorrowers, 1);

			assertThat(baseBorrower1.getDealId(), not(newBorrower1.getDealId()));
			assertThat(baseBorrower1.getBorrowerId(), not(newBorrower1.getBorrowerId()));
			checkAllFileds(baseBorrower1, newBorrower1, "copyId", "pk", "dealId", "borrowerId");

			assertThat(baseBorrower2.getDealId(), not(newBorrower2.getDealId()));
			assertThat(baseBorrower2.getBorrowerId(), not(newBorrower2.getBorrowerId()));
			checkAllFileds(baseBorrower2, newBorrower2, "copyId", "pk", "dealId", "borrowerId");

			System.err.println("-------------------------- test for BorrowerAddress --------------------------");

			BorrowerAddress baseBorrowerAddress1 = (BorrowerAddress)CollectionUtils.get(baseBorrower1.getBorrowerAddresses(), 0);
			BorrowerAddress newBorrowerAddress1 = (BorrowerAddress)CollectionUtils.get(newBorrower1.getBorrowerAddresses(), 0);
			BorrowerAddress baseBorrowerAddress2 = (BorrowerAddress)CollectionUtils.get(baseBorrower2.getBorrowerAddresses(), 0);
			BorrowerAddress newBorrowerAddress2 = (BorrowerAddress)CollectionUtils.get(newBorrower2.getBorrowerAddresses(), 0);

			assertThat(baseBorrowerAddress1.getBorrowerId(), not(newBorrowerAddress1.getBorrowerId()));
			assertThat(baseBorrowerAddress1.getBorrowerAddressId(), not(newBorrowerAddress1.getBorrowerAddressId()));
			checkAllFileds(baseBorrowerAddress1, newBorrowerAddress1, "copyId", "pk", "borrowerId", "borrowerAddressId", "addrId");

			assertThat(baseBorrowerAddress2.getBorrowerId(), not(newBorrowerAddress2.getBorrowerId()));
			assertThat(baseBorrowerAddress2.getBorrowerAddressId(), not(newBorrowerAddress2.getBorrowerAddressId()));
			checkAllFileds(baseBorrowerAddress2, newBorrowerAddress2, "copyId", "pk", "borrowerId", "borrowerAddressId", "addrId");



			System.err.println("-------------------------- test for Addr --------------------------");

			Addr baseAddr1 = baseBorrowerAddress1.getAddr();
			Addr newAddr1 = newBorrowerAddress1.getAddr();
			Addr baseAddr2 = baseBorrowerAddress2.getAddr();
			Addr newAddr2 = newBorrowerAddress2.getAddr();

			assertThat(baseDeal.getCopyId(), is(baseAddr1.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newAddr1.getCopyId()));
			assertThat(baseAddr1.getAddrId(), not(newAddr1.getAddrId()));
			checkAllFileds(baseAddr1, newAddr1, "copyId", "pk", "addrId");

			assertThat(baseDeal.getCopyId(), is(baseAddr2.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newAddr2.getCopyId()));
			assertThat(baseAddr2.getAddrId(), not(newAddr2.getAddrId()));
			checkAllFileds(baseAddr2, newAddr2, "copyId", "pk", "addrId");

			System.err.println("-------------------------- test for Income --------------------------");

			Income baseIncome1 = (Income)CollectionUtils.get(baseBorrower1.getIncomes(), 0);
			Income newIncome1 = (Income)CollectionUtils.get(newBorrower1.getIncomes(), 0);
			Income baseIncome2 = (Income)CollectionUtils.get(baseBorrower2.getIncomes(), 0);
			Income newIncome2 = (Income)CollectionUtils.get(newBorrower2.getIncomes(), 0);

			assertThat(baseIncome1.getIncomeId(), not(newIncome1.getIncomeId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseIncome1.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newIncome1.getCopyId()));
			checkAllFileds(baseIncome1, newIncome1, "copyId", "pk", "incomeId", "borrowerId");  

			assertThat(baseIncome2.getIncomeId(), not(newIncome2.getIncomeId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseIncome2.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newIncome2.getCopyId()));
			checkAllFileds(baseIncome2, newIncome2, "copyId", "pk", "incomeId", "borrowerId");  

			System.err.println("-------------------------- test for Asset --------------------------");

			Asset baseAssets1 = (Asset)CollectionUtils.get(baseBorrower1.getAssets(), 0);
			Asset newAssets1 = (Asset)CollectionUtils.get(newBorrower1.getAssets(), 0);
			Asset baseAssets2 = (Asset)CollectionUtils.get(baseBorrower2.getAssets(), 0);
			Asset newAssets2 = (Asset)CollectionUtils.get(newBorrower2.getAssets(), 0);

			assertThat(baseAssets1.getAssetId(), not(newAssets1.getAssetId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseAssets1.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newAssets1.getCopyId()));
			checkAllFileds(baseAssets1, newAssets1, "copyId", "pk", "borrowerId", "assetId");  

			assertThat(baseAssets2.getAssetId(), not(newAssets2.getAssetId()));//has to be different
			assertThat(baseDeal.getCopyId(), is(baseAssets2.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newAssets2.getCopyId()));
			checkAllFileds(baseAssets2, newAssets2, "copyId", "pk", "borrowerId", "assetId");  


			System.err.println("-------------------------- test for Liability --------------------------");

			Liability baseLiability1 = (Liability)CollectionUtils.get(baseBorrower1.getLiabilities(), 0);
			Liability newLiability1 = (Liability)CollectionUtils.get(newBorrower1.getLiabilities(), 0);
			Liability baseLiability2 = (Liability)CollectionUtils.get(baseBorrower2.getLiabilities(), 0);
			Liability newLiability2 = (Liability)CollectionUtils.get(newBorrower2.getLiabilities(), 0);

			assertThat(baseLiability1.getLiabilityId(), not(newLiability1.getLiabilityId()));
			assertThat(baseDeal.getCopyId(), is(baseLiability1.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newLiability1.getCopyId()));
			checkAllFileds(baseLiability1, newLiability1, "copyId", "pk", "borrowerId", "liabilityId");  

			assertThat(baseLiability2.getLiabilityId(), not(newLiability2.getLiabilityId()));
			assertThat(baseDeal.getCopyId(), is(baseLiability2.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newLiability2.getCopyId()));
			checkAllFileds(baseLiability2, newLiability2, "copyId", "pk", "borrowerId", "liabilityId");  


			System.err.println("-------------------------- test for CreditReference --------------------------");

			CreditReference baseCreditReference1 = (CreditReference)CollectionUtils.get(baseBorrower1.getCreditReferences(), 0);
			CreditReference newCreditReference1 = (CreditReference)CollectionUtils.get(newBorrower1.getCreditReferences(), 0);
			CreditReference baseCreditReference2 = (CreditReference)CollectionUtils.get(baseBorrower2.getCreditReferences(), 0);
			CreditReference newCreditReference2 = (CreditReference)CollectionUtils.get(newBorrower2.getCreditReferences(), 0);

			assertThat(baseCreditReference1.getCreditReferenceId(), not(newCreditReference1.getCreditReferenceId()));
			assertThat(baseDeal.getCopyId(), is(baseCreditReference1.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newCreditReference1.getCopyId()));
			checkAllFileds(baseCreditReference1, newCreditReference1, "copyId", "pk", "borrowerId", "creditReferenceId");  

			assertThat(baseCreditReference2.getCreditReferenceId(), not(newCreditReference2.getCreditReferenceId()));
			assertThat(baseDeal.getCopyId(), is(baseCreditReference2.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newCreditReference2.getCopyId()));
			checkAllFileds(baseCreditReference2, newCreditReference2, "copyId", "pk", "borrowerId", "creditReferenceId");  



			System.err.println("-------------------------- test for EmploymentHistory --------------------------");

			EmploymentHistory baseEmploymentHistory1 = (EmploymentHistory)CollectionUtils.get(baseBorrower1.getEmploymentHistories(), 0);
			EmploymentHistory newEmploymentHistory1 = (EmploymentHistory)CollectionUtils.get(newBorrower1.getEmploymentHistories(), 0);
			EmploymentHistory baseEmploymentHistory2 = (EmploymentHistory)CollectionUtils.get(baseBorrower2.getEmploymentHistories(), 0);
			EmploymentHistory newEmploymentHistory2 = (EmploymentHistory)CollectionUtils.get(newBorrower2.getEmploymentHistories(), 0);

			assertThat(baseEmploymentHistory1.getEmploymentHistoryId(), not(newEmploymentHistory1.getEmploymentHistoryId()));
			assertThat(baseEmploymentHistory1.getIncome(), not(newEmploymentHistory1.getIncome()));
			assertThat(baseEmploymentHistory1.getContactId(), not(newEmploymentHistory1.getContactId()));
			assertThat(baseDeal.getCopyId(), is(baseEmploymentHistory1.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newEmploymentHistory1.getCopyId()));
			checkAllFileds(baseEmploymentHistory1, newEmploymentHistory1, "copyId", "pk", "borrowerId", "employmentHistoryId", "contactId", "incomeId");  

			assertThat(baseEmploymentHistory2.getEmploymentHistoryId(), not(newEmploymentHistory2.getEmploymentHistoryId()));
			assertThat(baseEmploymentHistory2.getContactId(), not(newEmploymentHistory2.getContactId()));
			assertThat(baseEmploymentHistory2.getIncome(), not(newEmploymentHistory2.getIncome()));
			assertThat(baseDeal.getCopyId(), is(baseEmploymentHistory2.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newEmploymentHistory2.getCopyId()));
			checkAllFileds(baseEmploymentHistory2, newEmploymentHistory2, "copyId", "pk", "borrowerId", "employmentHistoryId", "contactId", "incomeId");  


			System.err.println("-------------------------- test for Contact --------------------------");
			Contact baseContact1 = baseEmploymentHistory1.getContact();
			Contact newContact1 = newEmploymentHistory1.getContact();
			Contact baseContact2 = baseEmploymentHistory2.getContact();
			Contact newContact2 = newEmploymentHistory2.getContact();

			assertThat(baseContact1.getContactId(), not(newContact1.getContactId()));
			assertThat(baseContact1.getAddrId(), not(newContact1.getAddrId()));
			assertThat(baseDeal.getCopyId(), is(baseContact1.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newContact1.getCopyId()));
			checkAllFileds(baseContact1, newContact1, "copyId", "pk", "contactId", "borrowerId", "addrId");  

			assertThat(baseContact2.getContactId(), not(newContact2.getContactId()));
			assertThat(baseContact2.getAddrId(), not(newContact2.getAddrId()));
			assertThat(baseDeal.getCopyId(), is(baseContact2.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newContact2.getCopyId()));
			checkAllFileds(baseContact2, newContact2, "copyId", "pk", "contactId", "borrowerId", "addrId");  

			System.err.println("-------------------------- test for Addr --------------------------");

			Addr baseAddr1B = baseContact1.getAddr();
			Addr newAddr1B = newContact1.getAddr();
			Addr baseAddr2B = baseContact2.getAddr();
			Addr newAddr2B = newContact2.getAddr();

			assertThat(baseAddr1B.getAddrId(), not(newAddr1B.getAddrId()));
			assertThat(baseDeal.getCopyId(), is(baseAddr1B.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newAddr1B.getCopyId()));
			checkAllFileds(baseAddr1B, newAddr1B, "copyId", "pk", "addrId");

			assertThat(baseAddr2B.getAddrId(), not(newAddr2B.getAddrId()));
			assertThat(baseDeal.getCopyId(), is(baseAddr2B.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newAddr2B.getCopyId()));
			checkAllFileds(baseAddr2B, newAddr2B, "copyId", "pk", "addrId");

			System.err.println("-------------------------- test for LifeDisabilityPremium --------------------------");

			LifeDisabilityPremiums baseLifeDisabilityPremiums1 = (LifeDisabilityPremiums)CollectionUtils.get(baseBorrower1.getLifeDisabilityPremiums(), 0);
			LifeDisabilityPremiums newLifeDisabilityPremiums1 = (LifeDisabilityPremiums)CollectionUtils.get(newBorrower1.getLifeDisabilityPremiums(), 0);
			LifeDisabilityPremiums baseLifeDisabilityPremiums2 = (LifeDisabilityPremiums)CollectionUtils.get(baseBorrower2.getLifeDisabilityPremiums(), 0);
			LifeDisabilityPremiums newLifeDisabilityPremiums2 = (LifeDisabilityPremiums)CollectionUtils.get(newBorrower2.getLifeDisabilityPremiums(), 0);

			assertThat(baseLifeDisabilityPremiums1.getLifeDisabilityPremiumsId(), not(newLifeDisabilityPremiums1.getLifeDisabilityPremiumsId()));
			assertThat(baseDeal.getCopyId(), is(baseLifeDisabilityPremiums1.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newLifeDisabilityPremiums1.getCopyId()));
			checkAllFileds(baseLifeDisabilityPremiums1, newLifeDisabilityPremiums1, "copyId", "pk", "borrowerId", "lifeDisabilityPremiumsId");  

			assertThat(baseLifeDisabilityPremiums2.getLifeDisabilityPremiumsId(), not(newLifeDisabilityPremiums2.getLifeDisabilityPremiumsId()));
			assertThat(baseDeal.getCopyId(), is(baseLifeDisabilityPremiums2.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newLifeDisabilityPremiums2.getCopyId()));
			checkAllFileds(baseLifeDisabilityPremiums2, newLifeDisabilityPremiums2, "copyId", "pk", "borrowerId", "lifeDisabilityPremiumsId");  


			System.err.println("-------------------------- test for BorrowerIdentification --------------------------");

			BorrowerIdentification baseBorrowerIdentification1 = (BorrowerIdentification)CollectionUtils.get(baseBorrower1.getBorrowerIdentifications(), 0);
			BorrowerIdentification newBorrowerIdentification1 = (BorrowerIdentification)CollectionUtils.get(newBorrower1.getBorrowerIdentifications(), 0);
			BorrowerIdentification baseBorrowerIdentification2 = (BorrowerIdentification)CollectionUtils.get(baseBorrower2.getBorrowerIdentifications(), 0);
			BorrowerIdentification newBorrowerIdentification2 = (BorrowerIdentification)CollectionUtils.get(newBorrower2.getBorrowerIdentifications(), 0);

			//FIXME: current express cannot handle this
			//assertThat(baseBorrowerIdentification1.getIdentificationId(), not(newBorrowerIdentification1.getIdentificationId()));
			assertThat(baseDeal.getCopyId(), is(baseBorrowerIdentification1.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newBorrowerIdentification1.getCopyId()));
			checkAllFileds(baseBorrowerIdentification1, newBorrowerIdentification1, "copyId", "pk", "identificationId", "borrowerId");  

			//FIXME: current express cannot handle this
			//assertThat(baseBorrowerIdentification2.getIdentificationId(), not(newBorrowerIdentification2.getIdentificationId()));
			assertThat(baseDeal.getCopyId(), is(baseBorrowerIdentification2.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newBorrowerIdentification2.getCopyId()));
			checkAllFileds(baseBorrowerIdentification2, newBorrowerIdentification2, "copyId", "pk", "identificationId", "borrowerId");  


			System.err.println("########################## test for DocumentTracking #####################");

			Collection<DocumentTracking> baseDocumentTrackings = (Collection<DocumentTracking>)baseDeal.getDocumentTracks();
			Collection<DocumentTracking> newDocumentTrackings = (Collection<DocumentTracking>)newDeal.getDocumentTracks();

			assertThat(baseDocumentTrackings.size(), is(newDocumentTrackings.size()));

			for(int i = 0; i<baseDocumentTrackings.size(); i++){
				DocumentTracking baseDocumentTracking = (DocumentTracking)CollectionUtils.get(baseDocumentTrackings, i);
				DocumentTracking newDocumentTracking = (DocumentTracking)CollectionUtils.get(newDocumentTrackings, i);

				baseDocumentTracking = baseDocumentTracking.findByPrimaryKey((DocumentTrackingPK) baseDocumentTracking.getPk());
				newDocumentTracking = newDocumentTracking.findByPrimaryKey((DocumentTrackingPK) newDocumentTracking.getPk());

				assertThat(baseDeal.getCopyId(), is(baseDocumentTracking.getCopyId()));
				assertThat(newDeal.getCopyId(), is(newDocumentTracking.getCopyId()));

				assertThat(baseDeal.getDealId(), is(baseDocumentTracking.getApplicationId()));
				assertThat(newDeal.getDealId(), is(newDocumentTracking.getApplicationId()));

				assertThat(baseDocumentTracking.getDocumentTrackingId(), not(newDocumentTracking.getDocumentTrackingId()));
				checkAllFileds(baseDocumentTracking, newDocumentTracking, "copyId", "pk", "verb", "dealId", "documentTrackingId", "applicationId");

				System.err.println("-------------------------- test for DocumentTrackingVerbiage --------------------------");
				//verbiage test!!
				List<DocTrackingVerbiage> baseDocVerbList = baseDocumentTracking.getVerb();
				List<DocTrackingVerbiage> newDocVerbList = newDocumentTracking.getVerb();

				assertThat(baseDocVerbList.size(), is(newDocVerbList.size()));

				for(int j = 0; j<baseDocVerbList.size();j++){
					DocTrackingVerbiage baseDocVerb = baseDocVerbList.get(j);
					DocTrackingVerbiage newDocVerb = newDocVerbList.get(j);

					checkAllFileds(baseDocVerb, newDocVerb, "this$0", "documentTrackingId");

				}
			}

			System.err.println("########################## test for EscrowPayment #####################");

			EscrowPayment baseEscrowPayment = (EscrowPayment)CollectionUtils.get(baseDeal.getEscrowPayments(), 0);
			EscrowPayment newEscrowPayment = (EscrowPayment)CollectionUtils.get(newDeal.getEscrowPayments(), 0);

			assertThat(baseDeal.getCopyId(), is(baseEscrowPayment.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newEscrowPayment.getCopyId()));

			assertThat(baseEscrowPayment.getEscrowPaymentId(), not(newEscrowPayment.getEscrowPaymentId()));
			checkAllFileds(baseEscrowPayment, newEscrowPayment, "copyId", "pk", "escrowPaymentId", "dealId");


			System.err.println("########################## test for DownPaymentSource #####################");

			DownPaymentSource baseDownPaymentSource = (DownPaymentSource)CollectionUtils.get(baseDeal.getDownPaymentSources(), 0);
			DownPaymentSource newDownPaymentSource = (DownPaymentSource)CollectionUtils.get(newDeal.getDownPaymentSources(), 0);

			assertThat(baseDeal.getCopyId(), is(baseDownPaymentSource.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newDownPaymentSource.getCopyId()));

			assertThat(baseDownPaymentSource.getDownPaymentSourceId(), not(newDownPaymentSource.getDownPaymentSourceId()));

			checkAllFileds(baseDownPaymentSource, newDownPaymentSource, "copyId", "pk", "downPaymentSourceId", "dealId");


			System.err.println("########################## test for DealFee #####################");

			DealFee baseDealFee = (DealFee)CollectionUtils.get(baseDeal.getDealFees(), 0);
			DealFee newDealFee = (DealFee)CollectionUtils.get(newDeal.getDealFees(), 0);

			assertThat(baseDeal.getCopyId(), is(baseDealFee.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newDealFee.getCopyId()));

			assertThat(baseDealFee.getDealFeeId(), not(newDealFee.getDealFeeId()));
			checkAllFileds(baseDealFee, newDealFee, "copyId", "pk", "dealId", "dealFeeId");


			System.err.println("########################## test for InsureOnlyApplicant #####################");

			InsureOnlyApplicant baseInsureOnlyApplicant = (InsureOnlyApplicant)CollectionUtils.get(baseDeal.getInsureOnlyApplicants(), 0);
			InsureOnlyApplicant newInsureOnlyApplicant = (InsureOnlyApplicant)CollectionUtils.get(newDeal.getInsureOnlyApplicants(), 0);

			assertThat(baseDeal.getCopyId(), is(baseInsureOnlyApplicant.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newInsureOnlyApplicant.getCopyId()));

			assertThat(baseInsureOnlyApplicant.getInsureOnlyApplicantId(), not(newInsureOnlyApplicant.getInsureOnlyApplicantId()));
			checkAllFileds(baseInsureOnlyApplicant, newInsureOnlyApplicant, "copyId", "pk", "dealId", "insureOnlyApplicantId");


			System.err.println("-------------------------- test for LifeDisPremiumsIOnlyA --------------------------");

			LifeDisPremiumsIOnlyA baseLifeDisPremiumsIOnlyA = (LifeDisPremiumsIOnlyA)CollectionUtils.get(baseInsureOnlyApplicant.getLifeDisPremiumsIOnlyA(), 0);
			LifeDisPremiumsIOnlyA newLifeDisPremiumsIOnlyA = (LifeDisPremiumsIOnlyA)CollectionUtils.get(newInsureOnlyApplicant.getLifeDisPremiumsIOnlyA(), 0);

			assertThat(baseLifeDisPremiumsIOnlyA.getLifeDisPremiumsIOnlyAId(), not(newLifeDisPremiumsIOnlyA.getLifeDisPremiumsIOnlyAId()));
			assertThat(baseDeal.getCopyId(), is(baseLifeDisPremiumsIOnlyA.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newLifeDisPremiumsIOnlyA.getCopyId()));
			checkAllFileds(baseLifeDisPremiumsIOnlyA, newLifeDisPremiumsIOnlyA, "copyId", "pk", "dealId", "lifeDisPremiumsIOnlyAId", "insureOnlyApplicantId");  


			System.err.println("########################## test for Request #####################");

			Request baseRequest = (Request)CollectionUtils.get(baseDeal.getRequests(), 0);
			Request newRequest = (Request)CollectionUtils.get(newDeal.getRequests(), 0);

			assertThat(baseDeal.getCopyId(), is(baseRequest.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newRequest.getCopyId()));

			assertThat(baseRequest.getRequestId(), not(newRequest.getRequestId()));
			checkAllFileds(baseRequest, newRequest, "copyId", "pk", "dealId", "requestId");

			System.err.println("-------------------------- test for BorrowerRequestAssoc --------------------------");

			String sql = "select count(*) from BorrowerRequestAssoc where requestid = ?";
			Connection con = srk.getConnection();
			PreparedStatement stat = con.prepareStatement(sql);
			stat.setInt(1, baseRequest.getRequestId());
			ResultSet rs = stat.executeQuery();

			int baseBRA = (rs.next()) ? rs.getInt(1) : -1;

			rs.close();
			stat.setInt(1, newRequest.getRequestId());
			rs = stat.executeQuery();

			int newBRA = (rs.next()) ? rs.getInt(1) : -2;

			rs.close();
			stat.close();

			assertThat(baseBRA, is(newBRA));

			System.err.println("-------------------------- test for ServiceRequest --------------------------");

			ServiceRequest baseServiceRequest = baseRequest.getServiceRequest();
			ServiceRequest newServiceRequest = newRequest.getServiceRequest();

			assertThat(baseServiceRequest.getRequestId(), not(newServiceRequest.getRequestId()));
			assertThat(baseDeal.getCopyId(), is(baseServiceRequest.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newServiceRequest.getCopyId()));
			checkAllFileds(baseServiceRequest, newServiceRequest, "copyId", "pk", "requestId");  

			System.err.println("-------------------------- test for ServiceRequestContact --------------------------");

			ServiceRequestContact baseServiceRequestContact = baseRequest.getServiceRequestContact();
			ServiceRequestContact newServiceRequestContact = newRequest.getServiceRequestContact();

			assertThat(baseServiceRequestContact.getServiceRequestContactId(), 
					not(newServiceRequestContact.getServiceRequestContactId()));
			assertThat(baseDeal.getCopyId(), is(baseServiceRequestContact.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newServiceRequestContact.getCopyId()));
			checkAllFileds(baseServiceRequestContact, newServiceRequestContact, "copyId", "pk", "requestId","serviceRequestContactId"); 


			System.err.println("########################## test for Component #####################");

			int compSize = baseDeal.getComponents().size();
			for(int i = 0;i<compSize;i++){

				Component baseComponent = (Component)CollectionUtils.get(baseDeal.getComponents(), i);
				Component newComponent = (Component)CollectionUtils.get(newDeal.getComponents(), i);

				assertThat(baseDeal.getCopyId(), is(baseComponent.getCopyId()));
				assertThat(newDeal.getCopyId(), is(newComponent.getCopyId()));

				assertThat(baseComponent.getComponentId(), not(newComponent.getComponentId()));
				checkAllFileds(baseComponent, newComponent, "copyId", "pk", "componentId", "dealId");

				System.err.println("-------------------------- test for ComponentMortgage --------------------------");

				ComponentMortgage baseComponentMortgage = baseComponent.getComponentMortgage();
				ComponentMortgage newComponentMortgage = newComponent.getComponentMortgage();

				if(baseComponentMortgage == null){
					assertNull(newComponentMortgage);
				}else{
					assertThat(baseComponentMortgage.getComponentId(), not(newComponentMortgage.getComponentId()));
					assertThat(baseDeal.getCopyId(), is(baseComponentMortgage.getCopyId()));
					assertThat(newDeal.getCopyId(), is(newComponentMortgage.getCopyId()));
					checkAllFileds(baseComponentMortgage, newComponentMortgage, "copyId", "pk", "componentId");	
				}

				System.err.println("-------------------------- test for ComponentLOC --------------------------");
				ComponentLOC baseComponentLOC = baseComponent.getComponentLOC();
				ComponentLOC newComponentLOC = newComponent.getComponentLOC();

				if(baseComponentLOC == null){
					assertNull(newComponentLOC);
				}else{
					assertThat(baseComponentLOC.getComponentId(), not(newComponentLOC.getComponentId()));//has to be different
					assertThat(baseDeal.getCopyId(), is(baseComponentLOC.getCopyId()));
					assertThat(newDeal.getCopyId(), is(newComponentLOC.getCopyId()));
					checkAllFileds(baseComponentLOC, newComponentLOC, "copyId", "pk", "componentId");	
				}

				System.err.println("-------------------------- test for ComponentLoan --------------------------");
				ComponentLoan baseComponentLoan = baseComponent.getComponentLoan();
				ComponentLoan newComponentLoan = newComponent.getComponentLoan();

				if(baseComponentLoan == null){
					assertNull(newComponentLoan);
				}else{
					assertThat(baseComponentLoan.getComponentId(), not(newComponentLoan.getComponentId()));//has to be different
					assertThat(baseDeal.getCopyId(), is(baseComponentLoan.getCopyId()));
					assertThat(newDeal.getCopyId(), is(newComponentLoan.getCopyId()));
					checkAllFileds(baseComponentLoan, newComponentLoan, "copyId", "pk", "componentId");	
				}

				System.err.println("-------------------------- test for ComponentCreditCard --------------------------");
				ComponentCreditCard baseComponentCreditCard = baseComponent.getComponentCreditCard();
				ComponentCreditCard newComponentCreditCard = newComponent.getComponentCreditCard();

				if(baseComponentCreditCard == null){
					assertNull(newComponentCreditCard);
				}else{
					assertThat(baseComponentCreditCard.getComponentId(), not(newComponentCreditCard.getComponentId()));//has to be different
					assertThat(baseDeal.getCopyId(), is(baseComponentCreditCard.getCopyId()));
					assertThat(newDeal.getCopyId(), is(newComponentCreditCard.getCopyId()));
					checkAllFileds(baseComponentCreditCard, newComponentCreditCard, "copyId", "pk", "componentId");	
				}

				System.err.println("-------------------------- test for ComponentOverdraft --------------------------");
				ComponentOverdraft baseComponentOverdraft = baseComponent.getComponentOverdraft();
				ComponentOverdraft newComponentOverdraft = newComponent.getComponentOverdraft();

				if(baseComponentOverdraft == null){
					assertNull(newComponentOverdraft);
				}else{
					assertThat(baseComponentOverdraft.getComponentId(), not(newComponentOverdraft.getComponentId()));//has to be different
					assertThat(baseDeal.getCopyId(), is(baseComponentOverdraft.getCopyId()));
					assertThat(newDeal.getCopyId(), is(newComponentOverdraft.getCopyId()));
					checkAllFileds(baseComponentOverdraft, newComponentOverdraft, "copyId", "pk", "componentId");	
				}
			}


			System.err.println("########################## test for ComponentSummary #####################");

			ComponentSummary baseComponentSummary = (ComponentSummary)CollectionUtils.get(baseDeal.getComponentSummary(), 0);
			ComponentSummary newComponentSummary = (ComponentSummary)CollectionUtils.get(newDeal.getComponentSummary(), 0);

			assertThat(baseDeal.getCopyId(), is(baseComponentSummary.getCopyId()));
			assertThat(newDeal.getCopyId(), is(newComponentSummary.getCopyId()));

			assertThat(baseComponentSummary.getDealId(), not(newComponentSummary.getDealId()));//has to be different
			checkAllFileds(baseComponentSummary, newComponentSummary, "copyId", "pk", "dealId");
			assertNotNull(baseComponentSummary);
		}catch(AssertionError e){
		}catch(Exception e){
			e.printStackTrace();
			//fail(e.getMessage());
		}finally{
			srk.freeResources();
		}
	}

	@Test
	public void testDeleteCopy() throws Exception {

		SessionResourceKit srk = new SessionResourceKit();
		srk.getExpressState().setDealInstitutionId(1);
		try{
			srk.beginTransaction();

			Deal baseDeal = DefaultDealTreeGenerator.createNewTree(srk, null);

			System.err.println(" >>> base deal :" + baseDeal.getDealId() + " copy:" + baseDeal.getCopyId());

			MasterDeal srcMD = new MasterDeal(srk, null, baseDeal.getDealId());

			long starttime = System.nanoTime();
			srcMD.deleteCopy(baseDeal.getCopyId());
			System.err.println("deleted time = " + (System.nanoTime() - starttime));
			srcMD.ejbStore();

			try{
				srcMD = new MasterDeal(srk, null, baseDeal.getDealId());
				fail("finder exception has to be thrown");
			}catch(FinderException fe){
				// no MasterDeal : good
				System.err.println("MasterDeal was successfuly deleted");
			}
			assertNotNull(srcMD);
		}catch(AssertionError e){
		}catch(Exception e){
			e.printStackTrace();
			//fail(e.getMessage());
		}finally{
			srk.freeResources();
		}
	}

}
