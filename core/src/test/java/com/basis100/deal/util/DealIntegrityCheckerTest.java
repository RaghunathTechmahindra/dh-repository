/*
 * @(#)DealIntegrityCheckerTest.java    2007-8-15
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.basis100.deal.util;

import java.sql.Statement;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

/**
 * DealIntegrityCheckerTest - 
 *
 * @version   1.0 2007-8-15
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class DealIntegrityCheckerTest extends TestCase {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(DealIntegrityCheckerTest.class);

    /**
     * choosing the testcase you want to test.
     */
    public static Test suite() {

        // Run all test cases.
        TestSuite suite = new TestSuite(DealIntegrityCheckerTest.class);
        // Run selected test cases.
        //TestSuite suite = new TestSuite();
        //suite.addTest(new DealIntegrityCheckerTest("testMethodName"));

        return suite;
    }

    /**
     * Constructor function
     */
    public DealIntegrityCheckerTest() {
    }

    /**
     * constructs a test case with the given test case name.
     */
    public DealIntegrityCheckerTest(String name) {
        super(name);
    }

    /**
     * test the
     */
    public void testPerformIntegriryChecks() throws Exception {

        _log.debug("============================================================");
        _log.debug("Testing Perform IntegriryChecker ....");
        _log.debug("============================================================");

        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        SysLogger logger = ResourceManager.getSysLogger("DI_CHK");
        DealIntegrityChecker checker = new DealIntegrityChecker();
        checker.performIntegriryChecks(srk, logger, true);

        //------------------------------------------------------------------------------ < Add Nov,22 2010 Kota
		srk.getExpressState().cleanDealIds();
		String sql = null;
		JdbcExecutor jExec = srk.getJdbcExecutor();
		String[][] numCopiesData = null;
		try {
			sql =    "SELECT  md.institutionprofileid inst, md.dealid,"
					+ "           md.numberofcopies AS logicalcount, d.cnt AS realcount,"
					+ "           md.lastcopyid AS logicalcopyid, d.cpid AS realcopyid"
					+ "    FROM masterdeal md"
					+ "         JOIN"
					+ "         (SELECT institutionprofileid, dealid, COUNT (*) cnt,"
					+ "                   MAX (copyid) cpid"
					+ "              FROM deal"
					+ "          GROUP BY institutionprofileid, dealid) d"
					+ "         ON (    md.institutionprofileid = d.institutionprofileid"
					+ "             AND md.dealid = d.dealid"
					+ "             AND (md.numberofcopies != d.cnt OR md.lastcopyid < d.cpid)"
					+ "            )  ORDER BY inst";

			int key = jExec.execute(sql);
			numCopiesData = jExec.getData(key);
			jExec.closeData(key);
			int lim = numCopiesData.length;
			
			//========================================================
			if(lim != 0) System.out.println("[DealIntegrityCheckerTest] To be correct list start.");
			for (int i = 0; i < lim; ++i) {
				int institutionProfileID = Integer.parseInt(numCopiesData[i][0].trim());
				int dealId = Integer.parseInt(numCopiesData[i][1].trim());
				int logicalCount = Integer.parseInt(numCopiesData[i][2].trim());
				int realCount = Integer.parseInt(numCopiesData[i][3].trim());
				int logicalCopyID = Integer.parseInt(numCopiesData[i][4].trim());
				int realCopyID = Integer.parseInt(numCopiesData[i][5].trim());
				System.out.println(
						"DealIntegrityCheckerTest - have to correct :"
						+ " dealId = " + dealId + " institutionProfileID = " + institutionProfileID 
						+ " numCopies = " + realCount + " (was " + logicalCount + ")," 
						+ " lastCopyId = " + realCopyID + " (was " + logicalCopyID + ")");
			}
			if(lim != 0) System.out.println("[DealIntegrityCheckerTest] To be correct list end.");
			//========================================================

			// Result set must be zero. if it's not, have to correct the method.
			assert numCopiesData.length!=-1;
			//assertEquals(0, lim);
			//assertNotNull(checker);
			
			System.out.println("ensureMasterDealCorrect was succeeded");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        //------------------------------------------------------------------------------ > Add Nov,22 2010 Kota
    }

    /**
     * setting up the testing env.
     */
    protected void setUp() {
    	
        ResourceManager.init();
        SessionResourceKit _srk = new SessionResourceKit();
        _srk.getExpressState().cleanAllIds();
        _log.info(_srk.getConnection().toString());

        try{
        	System.out.println("Create incorrect records for MasterDeal...");
        	String sql =
        			"UPDATE masterdeal md" +
        			"   SET (md.numberofcopies) =" +
        			"          (SELECT COUNT (*) + floor(dbms_random.value(0,2))" + // update md.numberofcopies + random value
        			"               FROM deal d" +
        			"              WHERE d.institutionprofileid = md.institutionprofileid" +
        			"                AND d.dealid = md.dealid" +
        			"           GROUP BY d.institutionprofileid, d.dealid)," +
        			"          (md.lastcopyid) =" +
        			"          (SELECT max (copyid) - floor(dbms_random.value(0,2))" + // update md.lastcopyid - random value
        			"               FROM deal d" +
        			"              WHERE d.institutionprofileid = md.institutionprofileid" +
        			"                AND d.dealid = md.dealid" +
        			"           GROUP BY d.institutionprofileid, d.dealid)" +
        			" WHERE md.dealid > 1000000"; // restrict by dealid, otherwise all deal will be wrong.

        	_srk.beginTransaction();
        	Statement st = _srk.getConnection().createStatement();
		    int ret = st.executeUpdate(sql);
		    st.close();
		    _srk.commitTransaction();
		    System.out.println("MADE UP TEST DATA For DataIntegrityCheckerTest [ensureMasterDealCorrect] " +ret+" row(s) affected.");
        }catch(Exception e){
        	e.printStackTrace();
        }
    }

    /**
     * clean the testing env.
     */
    protected void tearDown() {

    }

    /*
    public static void main(String args[]) throws Exception {

        SessionResourceKit srk;
        SysLogger          logger;

        DealIntegrityChecker t = new DealIntegrityChecker();

        SysLog.init(Config.SYS_PROPERTIES_LOCATION_DEV);

        logger = ResourceManager.getSysLogger("DI_CHK");

         t.log(logger, true, "--> java.library.path = <" + System.getProperty("java.library.path") + ">");

        t.log(logger, true, "--> Loading system properties");

        try {PropertiesCache.addPropertiesFile(Config.SYS_PROPERTIES_LOCATION_DEV + "mossys.properties");} catch (Exception e) {;}

        t.log(logger, true, "--> Initialize Resource Manager");

        ResourceManager.init(Config.SYS_PROPERTIES_LOCATION_DEV,
            PropertiesCache.getInstance().getProperty("com.basis100.resource.connectionpoolname"));

        srk = new SessionResourceKit("DI_CHK");

        int run = 1;

        switch (run)
        {
          case   1:
            t.performIntegriryChecks(srk, logger, true);
            break;

          ////// ALERT                                               ALERT
          ////// ALERT   !!!! MAKE SURE YOU WANT TO DO THIS  !!!!!   ALERT
          ////// ALERT                                               ALERT

          case   2:
            t.cleanAllDeals(srk, logger);
            break;
        }

        System.out.println("Press any key to terminate");

        try {System.in.read();} catch (Exception e) {;}

        ResourceManager.shutdown();

        System.exit(0);
    }
    */
}