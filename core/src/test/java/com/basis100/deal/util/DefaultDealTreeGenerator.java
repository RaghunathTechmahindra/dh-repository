package com.basis100.deal.util;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Collection;
import java.util.Date;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.AppraisalOrder;
import com.basis100.deal.entity.Asset;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.BorrowerIdentification;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentCreditCard;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.ComponentOverdraft;
import com.basis100.deal.entity.ComponentSummary;
import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.CreditBureauReport;
import com.basis100.deal.entity.CreditReference;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.DealHistory;
import com.basis100.deal.entity.DealNotes;
import com.basis100.deal.entity.DisclosureQuestion;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.entity.DocumentQueue;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.DownPaymentSource;
import com.basis100.deal.entity.EmploymentHistory;
import com.basis100.deal.entity.EscrowPayment;
import com.basis100.deal.entity.Income;
import com.basis100.deal.entity.InsureOnlyApplicant;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.LifeDisPremiumsIOnlyA;
import com.basis100.deal.entity.LifeDisabilityPremiums;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.PartyProfile;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.PropertyExpense;
import com.basis100.deal.entity.QualifyDetail;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.ServiceRequest;
import com.basis100.deal.entity.ServiceRequestContact;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.DocumentProfilePK;
import com.basis100.deal.pk.IncomePK;
import com.basis100.deal.pk.InsureOnlyApplicantPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.core.ExpressRuntimeException;

public class DefaultDealTreeGenerator {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SessionResourceKit srk = new SessionResourceKit();
		srk.getExpressState().setDealInstitutionId(1);

		try{
			srk.beginTransaction();
			System.out.println("-- start creating new deal tree ");
			Deal deal = createNewTree(srk, null);
			System.out.println("-- new deal:  dealid = " + deal.getDealId() + " copyid =" + deal.getCopyId());

			srk.commitTransaction();
		}catch(Exception e){
			e.printStackTrace();
			srk.cleanTransaction();
		}finally{
			srk.freeResources();
		}
	}

	public static Deal createNewTree(SessionResourceKit srk, CalcMonitor dcm)throws Exception
	{
		MasterDeal root = new MasterDeal(srk,dcm);
		MasterDealPK pk = root.createPrimaryKey(srk.getExpressState().getDealInstitutionId());

		//----- Deal ---------
		Deal deal = root.create(pk);
		final DealPK dealPK = (DealPK)deal.getPk();
		int dealid = dealPK.getId();
		int copyid = dealPK.getCopyId();

		deal.setAmortizationTerm(300);
		deal.setActualPaymentTerm(60);
		deal.setNumberOfBorrowers(1);
		deal.setNumberOfProperties(1);
		deal.setCopyType("G");
		deal.setScenarioRecommended("Y");
		deal.setScenarioNumber(1);
		deal.setServicingMortgageNumber("ABC123");
		deal.ejbStore();

		//----- Property  ---------
		Property property1 = new Property(srk,dcm);
		property1 = property1.create(dealPK);

		property1.setPrimaryPropertyFlag("Y");
		property1.setPropertyOccurenceNumber(1);
		property1.ejbStore();

		addDefaultChildrenProperty(srk, dcm, property1);

		Property property2 = new Property(srk,dcm);
		property2 = property2.create(dealPK);

		property2.setPrimaryPropertyFlag("N");
		property2.setPropertyOccurenceNumber(2);
		property2.ejbStore();

		addDefaultChildrenProperty(srk, dcm, property2);

		//----- Borrower ---------
		Borrower borrower1 = new Borrower(srk,dcm);
		borrower1 = borrower1.create(dealPK);

		borrower1.setPrimaryBorrowerFlag("Y");
		borrower1.setBorrowerNumber(1);
		borrower1.ejbStore();

		addDefaultChildrenBorrower(srk, dcm, borrower1);


		Borrower borrower2 = new Borrower(srk,dcm);
		borrower2 = borrower2.create(dealPK);

		borrower2.setPrimaryBorrowerFlag("N");
		borrower2.setBorrowerNumber(1);
		borrower2.ejbStore();

		addDefaultChildrenBorrower(srk, dcm, borrower2);

		//Other Deal sub entities

		//		Bridge bridge = new Bridge(srk, dcm);
		//		bridge.create(dealPK);

		addDefaultChildrenDocTrack(srk, dcm, deal);

		EscrowPayment ep = new EscrowPayment(srk, dcm);
		ep.create(dealPK);

		DownPaymentSource dps = new DownPaymentSource(srk,dcm);
		dps = dps.create(dealPK);

		DealFee df = new DealFee(srk, dcm);
		df.create(dealPK);

		InsureOnlyApplicant ioa = new InsureOnlyApplicant(srk, dcm);
		ioa.create(dealPK);
		addDefaultChildrenInsureOnlyApplicant(srk, dcm, ioa);

		Request request1 = new Request(srk);
		request1.create(dealPK, 0, new Date(), 0);

		Request request2 = new Request(srk);
		request2.create(dealPK, 0, new Date(), 0);

		createBorrowerRequestAssoc(srk, dcm, request1, borrower1);
		createBorrowerRequestAssoc(srk, dcm, request1, borrower2);
		createBorrowerRequestAssoc(srk, dcm, request2, borrower1);
		createBorrowerRequestAssoc(srk, dcm, request2, borrower2);

		addDefaultChildrenRequest(srk, dcm, request1);
		addDefaultChildrenRequest(srk, dcm, request2);

		Component component1 = new Component(srk, dcm);
		component1.create(dealPK.getId(), dealPK.getCopyId(), Mc.COMPONENT_TYPE_MORTGAGE, 0);
		addDefaultChildrenComponent(srk, dcm, component1);

		Component component2 = new Component(srk, dcm);
		component2.create(dealPK.getId(), dealPK.getCopyId(), Mc.COMPONENT_TYPE_LOC, 0);
		addDefaultChildrenComponent(srk, dcm, component2);

		Component component3 = new Component(srk, dcm);
		component3.create(dealPK.getId(), dealPK.getCopyId(), Mc.COMPONENT_TYPE_CREDITCARD, 0);
		addDefaultChildrenComponent(srk, dcm, component3);

		Component component4 = new Component(srk, dcm);
		component4.create(dealPK.getId(), dealPK.getCopyId(), Mc.COMPONENT_TYPE_LOAN, 0);
		addDefaultChildrenComponent(srk, dcm, component4);

		Component component5 = new Component(srk, dcm);
		component5.create(dealPK.getId(), dealPK.getCopyId(), Mc.COMPONENT_TYPE_OVERDRAFT, 0);
		addDefaultChildrenComponent(srk, dcm, component5);

		ComponentSummary componentSummary = new ComponentSummary(srk, dcm);
		componentSummary.create(dealPK.getId(), dealPK.getCopyId());

		if(dealid != dealPK.getId()) throw new ExpressRuntimeException("dealid has been changed!!!");
		if(copyid != dealPK.getCopyId()) throw new ExpressRuntimeException("copy has been changed!!!");


		//etc
		DealNotes dealNotes = new DealNotes(srk);
		dealNotes.create(dealPK);

		CreditBureauReport creditBureauReport = new CreditBureauReport(srk);
		creditBureauReport.create(dealPK);

		DealHistory dealHistory = new DealHistory(srk);
		dealHistory.create(dealPK.getId(), 0, 0, 1, "transactionText", new Date());

		PartyProfile partyProfile = new PartyProfile(srk);
		partyProfile.create(0, 0);

		Connection con = srk.getConnection();
		Statement stat = con.createStatement();
		String sql = "insert into partydealassoc " +
		"(DEALID, PARTYPROFILEID, PROPERTYID, INSTITUTIONPROFILEID ) values ("
		+ dealPK.getId()
		+ ","
		+ partyProfile.getPartyProfileId()
		+ ","
		+ property1.getPropertyId()
		+ ","
		+ srk.getExpressState().getDealInstitutionId() + " )";

		stat.execute(sql);		
		stat.close();

		DisclosureQuestion disclosureQuestion = new DisclosureQuestion(srk);
		disclosureQuestion.create(dealPK, 1, "020E", "Y");

		//DocumentProfile
//		DocumentProfile documentProfile = new DocumentProfile(srk);
//		documentProfile = documentProfile.findByDocumentTypeId(1);
		DocumentProfilePK dppk = new DocumentProfilePK("1", 0, 1, 0,"format");
		
		DocumentQueue documentQueue = new DocumentQueue(srk);
		documentQueue.create(new Date(), 0, "emailAddress", "emailFullName",
				"emailSubject", dealPK.getId(), dealPK.getCopyId(), "1",
				dppk, "pFaxNumbers");

		QualifyDetail qualifyDetail = new QualifyDetail(srk);
		qualifyDetail.create(dealPK.getId(), 0, 0);

		return deal;
	}


	private static void addDefaultChildrenComponent(SessionResourceKit srk, CalcMonitor dcm, Component component)throws Exception
	{

		switch (component.getComponentTypeId()) {
		case Mc.COMPONENT_TYPE_MORTGAGE:
			ComponentMortgage cm = new ComponentMortgage(srk, dcm);
			cm.create(component.getComponentId(), component.getCopyId());
			break;
		case Mc.COMPONENT_TYPE_LOC:

			ComponentLOC cl = new ComponentLOC(srk, dcm);
			cl.create(component.getComponentId(), component.getCopyId());
			break;
		case Mc.COMPONENT_TYPE_CREDITCARD:

			ComponentCreditCard ccc = new ComponentCreditCard(srk, dcm);
			ccc.create(component.getComponentId(), component.getCopyId());
			break;
		case Mc.COMPONENT_TYPE_LOAN:
			ComponentLoan clo = new ComponentLoan(srk, dcm);
			clo.create(component.getComponentId(), component.getCopyId());
			break;
		case Mc.COMPONENT_TYPE_OVERDRAFT:
			ComponentOverdraft cod = new ComponentOverdraft(srk, dcm);
			cod.create(component.getComponentId(), component.getCopyId());
			break;

		default:
			throw new IllegalArgumentException("ComponentTypeId is invalid");
		}
	}

	private static void createBorrowerRequestAssoc(SessionResourceKit srk,
			CalcMonitor dcm, Request request, Borrower borrower)
	throws Exception {

		if (request == null)
			throw new IllegalArgumentException("request is null");
		if (borrower == null)
			throw new IllegalArgumentException("borrower is null");
		if (request.getCopyId() != borrower.getCopyId())
			throw new IllegalArgumentException("different copy id !!");

		String sql = "INSERT INTO BorrowerRequestAssoc "
			+ "(requestId, copyId, borrowerId, institutionProfileId) VALUES ("
			+ request.getRequestId() + ", " 
			+ request.getCopyId() + ", "
			+ borrower.getBorrowerId() + ", "
			+ +srk.getExpressState().getDealInstitutionId() + ")";

		srk.getJdbcExecutor().execute(sql);
	}

	private static void addDefaultChildrenRequest(SessionResourceKit srk, CalcMonitor dcm, Request request)throws Exception
	{
		RequestPK rpk = (RequestPK)request.getPk();



		ServiceRequest sr = new ServiceRequest(srk);
		sr.create(rpk.getId(), rpk.getCopyId());

		ServiceRequestContact src = new ServiceRequestContact(srk);
		src.create(rpk.getId(), rpk.getCopyId());


	}

	private static void addDefaultChildrenInsureOnlyApplicant(SessionResourceKit srk, CalcMonitor dcm, InsureOnlyApplicant ioa)throws Exception
	{
		InsureOnlyApplicantPK ioaPK = (InsureOnlyApplicantPK)ioa.getPk();

		LifeDisPremiumsIOnlyA ldpioa = new LifeDisPremiumsIOnlyA(srk, dcm);
		ldpioa.create(ioaPK, 1);
	}

	private static void addDefaultChildrenProperty(SessionResourceKit srk, CalcMonitor dcm, Property property)throws Exception
	{
		PropertyPK ppk = (PropertyPK)property.getPk();

		PropertyExpense pexp = new PropertyExpense(srk,dcm);
		pexp = pexp.create(ppk);

		AppraisalOrder apprOrd = new AppraisalOrder(srk, dcm);
		apprOrd = apprOrd.create(ppk);
	}

	private static void addDefaultChildrenBorrower(SessionResourceKit srk, CalcMonitor dcm, Borrower borrower)throws Exception
	{
		BorrowerPK bpk = (BorrowerPK)borrower.getPk();

		BorrowerAddress baddr = new BorrowerAddress(srk,dcm);
		baddr = baddr.create(bpk);

		Asset asset = new Asset(srk,dcm);
		asset = asset.create(bpk);

		Liability liab = new Liability(srk, dcm);
		liab = liab.create(bpk);

		CreditReference cr = new CreditReference(srk, dcm);
		cr.create(bpk);

		Income inc = new Income(srk,dcm);
		inc = inc.create(bpk);

		EmploymentHistory eh = new EmploymentHistory(srk);
		eh = eh.create((IncomePK)inc.getPk());

		LifeDisabilityPremiums ldp = new LifeDisabilityPremiums(srk, dcm);
		ldp.create(bpk, 1); //LDInsuranceType: 1:Life, 2:Disability

		BorrowerIdentification bi = new BorrowerIdentification(srk, dcm);
		bi.create(bpk);

	}

	private static void addDefaultChildrenDocTrack(SessionResourceKit srk, CalcMonitor dcm, Deal deal)throws Exception
	{

		Condition condition = new Condition(srk);
		Collection<Condition> collection = condition.findBySqlColl(
		"select * from condition where conditiontypeid = 0 order by conditionid");
		int count = 0;
		for(Condition cond: collection){
			if(count++ > 10) break;

			DocumentTracking dt = new DocumentTracking(srk);
			dt.create(cond, deal.getDealId(), deal.getCopyId(), 
					0/* dt status*/, 0 /* dt source, 0=system gen */);
		}


	}

}
