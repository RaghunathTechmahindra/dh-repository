/*
 * @(#)BusinessCalendarTest.java    2005-3-15
 * 
 */


package com.basis100.deal.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.text.SimpleDateFormat;
import java.text.ParseException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import junit.framework.TestCase;

import config.Config;
import com.basis100.log.SysLog;
import com.basis100.resources.SessionResourceKit;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;

// only for testing.
import com.basis100.TestingEnvironmentInitializer;

/**
 * BusinessCalendarTest - 
 *
 * @version   1.0 2005-3-15
 * @author    <A HREF="mailto:chyxiang@yahoo.com">Chen Xiang (Sean)</A>
 */
public class BusinessCalendarTest extends TestCase {

    // the logger
    private static Log _log = LogFactory.getLog(BusinessCalendarTest.class);

    // minutes in a day.
    public static final int MINUTES_IN_ADAY = 8 * 60;

    private SessionResourceKit _srk;
    private BusinessCalendar _bCalc;

    // a list of potential holiday.
    protected List _holidayCases = new ArrayList();
    protected List _startDateCases = new ArrayList();

    // a list of province cases.
    protected List _provinceCases = new ArrayList();

    // a list of possible time zone.
    protected List _timezoneCases = new ArrayList();

    // intervals in minutes.
    protected int[] _taskDuration = {3600 / 60, 22200 / 60, 30600 / 60, 153000 / 60, 306000 / 60};

    /**
     * initializing the holiday cases.
     */
    protected void setUp() {

        _log.debug("Initializing the test cases ...");

        SimpleDateFormat formater = new SimpleDateFormat("yyyy.MM.dd");
        try {
            // the holiday cases.
            _holidayCases.add(formater.parse("2005.01.01"));
            _holidayCases.add(formater.parse("2005.01.02"));
            _holidayCases.add(formater.parse("2005.02.21"));
            _holidayCases.add(formater.parse("2005.03.25"));
            _holidayCases.add(formater.parse("2005.03.28"));
            _holidayCases.add(formater.parse("2005.05.23"));
            _holidayCases.add(formater.parse("2005.06.20"));
            _holidayCases.add(formater.parse("2005.06.24"));
            _holidayCases.add(formater.parse("2005.07.01"));
            _holidayCases.add(formater.parse("2005.07.09"));
            _holidayCases.add(formater.parse("2005.08.01"));
            _holidayCases.add(formater.parse("2005.08.15"));
            _holidayCases.add(formater.parse("2005.09.05"));
            _holidayCases.add(formater.parse("2005.10.10"));
            _holidayCases.add(formater.parse("2005.11.11"));
            _holidayCases.add(formater.parse("2005.12.26"));
            _holidayCases.add(formater.parse("2005.12.25"));

            // Business day cases.
            _startDateCases.add(formater.parse("2004.12.30"));
            _startDateCases.add(formater.parse("2004.12.31"));
            _startDateCases.add(formater.parse("2005.01.01"));
            _startDateCases.add(formater.parse("2005.01.02"));

            _startDateCases.add(formater.parse("2005.02.19"));
            _startDateCases.add(formater.parse("2005.02.20"));
            _startDateCases.add(formater.parse("2005.02.21"));

            _startDateCases.add(formater.parse("2005.03.23"));
            _startDateCases.add(formater.parse("2005.03.24"));
            _startDateCases.add(formater.parse("2005.03.25"));

            _startDateCases.add(formater.parse("2005.03.26"));
            _startDateCases.add(formater.parse("2005.03.27"));
            _startDateCases.add(formater.parse("2005.03.28"));

            _startDateCases.add(formater.parse("2005.05.21"));
            _startDateCases.add(formater.parse("2005.05.22"));
            _startDateCases.add(formater.parse("2005.05.23"));

            _startDateCases.add(formater.parse("2005.06.18"));
            _startDateCases.add(formater.parse("2005.06.19"));
            _startDateCases.add(formater.parse("2005.06.20"));

            _startDateCases.add(formater.parse("2005.06.22"));
            _startDateCases.add(formater.parse("2005.06.23"));
            _startDateCases.add(formater.parse("2005.06.24"));

            _startDateCases.add(formater.parse("2005.06.29"));
            _startDateCases.add(formater.parse("2005.06.30"));
            _startDateCases.add(formater.parse("2005.07.01"));

            _startDateCases.add(formater.parse("2005.07.07"));
            _startDateCases.add(formater.parse("2005.07.08"));
            _startDateCases.add(formater.parse("2005.07.09"));

            _startDateCases.add(formater.parse("2005.07.30"));
            _startDateCases.add(formater.parse("2005.07.31"));
            _startDateCases.add(formater.parse("2005.08.01"));

            _startDateCases.add(formater.parse("2005.08.13"));
            _startDateCases.add(formater.parse("2005.08.14"));
            _startDateCases.add(formater.parse("2005.08.15"));

            _startDateCases.add(formater.parse("2005.09.03"));
            _startDateCases.add(formater.parse("2005.09.04"));
            _startDateCases.add(formater.parse("2005.09.05"));

            _startDateCases.add(formater.parse("2005.10.08"));
            _startDateCases.add(formater.parse("2005.10.09"));
            _startDateCases.add(formater.parse("2005.10.10"));

            _startDateCases.add(formater.parse("2005.11.09"));
            _startDateCases.add(formater.parse("2005.11.10"));
            _startDateCases.add(formater.parse("2005.11.12"));

            _startDateCases.add(formater.parse("2005.12.23"));
            _startDateCases.add(formater.parse("2005.12.24"));
            _startDateCases.add(formater.parse("2005.12.25"));
            _startDateCases.add(formater.parse("2005.12.26"));
            
        } catch (ParseException pe) {
            _log.error("Setting up error: ", pe);
        }

        // the province cases.
        _provinceCases.add(new Province(0, "Other                  "));
        _provinceCases.add(new Province(1, "Alberta                "));
        _provinceCases.add(new Province(2, "British Columbia       "));
        _provinceCases.add(new Province(3, "Manitoba               "));
        _provinceCases.add(new Province(4, "New Brunswick          "));
        _provinceCases.add(new Province(5, "Newfoundland & Labrador"));
        _provinceCases.add(new Province(6, "Northwest Territories  "));
        _provinceCases.add(new Province(7, "Nova Scotia            "));
        _provinceCases.add(new Province(8, "Nunavut                "));
        _provinceCases.add(new Province(9, "Ontario                "));
        _provinceCases.add(new Province(10, "Prince Edward Island   "));
        _provinceCases.add(new Province(11, "Quebec                 "));
        _provinceCases.add(new Province(12, "Saskatchewan           "));
        _provinceCases.add(new Province(13, "Yukon Territories      "));

        // the time zone.
        _timezoneCases.add(new TimeZone(3, "AST - HAL  "));
        _timezoneCases.add(new TimeZone(5, "EST - EST  "));
        _timezoneCases.add(new TimeZone(7, "CST - CST  "));
        _timezoneCases.add(new TimeZone(8, "CST - SASK "));
        _timezoneCases.add(new TimeZone(9, "MST - MST  "));
        _timezoneCases.add(new TimeZone(10, "MST - PNT  "));
        _timezoneCases.add(new TimeZone(11, "PST - PST  "));

        // initializing the project enviornment.
        TestingEnvironmentInitializer.initTestingEnv();

        _srk = new SessionResourceKit("Testing");
        _bCalc = new BusinessCalendar(_srk, 5);
    }

    /**
     * test is holiday
     */
    public void testIsHoliday() {

        _log.info("--------Testing Province Cases--------");
        for (Iterator i = _provinceCases.iterator(); i.hasNext(); ) {

            Province province = (Province) i.next();
            _log.info(province.getName() + " - " + province.getId());
            for (Iterator h = _holidayCases.iterator(); h.hasNext(); ) {
                Date aDay = (Date) h.next();
                _bCalc.setTime(aDay);
                BusinessCalendar.HolidayHolder hh = _bCalc.new HolidayHolder();
                int test = _bCalc.isHoliday(5, province.getId(), hh);
                if (test == BusinessCalendar.NOT_HOLIDAY) {
                    _log.info("    " + aDay.toString() + " - is");
                } else {
                    _log.info("    " + aDay.toString() + " - is " + hh.h.getHolidayName());
                }
            }
        }
    }

    /**
     * test the method: calcBusDate
     */
    public void testCalcBusDate() {

        _log.info("------------Testing Calculate Business Day......");

        for (Iterator i = _startDateCases.iterator(); i.hasNext(); ) {
            // for each start date.
            Date startDate = (Date) i.next();
            _log.info("::::::::::::: Start Date: " + startDate.toString());
            for (int t = 0; t < _taskDuration.length; t ++) {
                // for each task.
                _log.info("    Task: " + _taskDuration[t] + "(M) " +
                          (_taskDuration[t] / MINUTES_IN_ADAY) + "(D)");
                for (Iterator p = _provinceCases.iterator(); p.hasNext(); ) {
                    // for each province.
                    Province aProvince = (Province) p.next();
                    TimeZone timeZone = getTimeZone(aProvince);
                    _log.info("        Province: " + aProvince.toString() +
                              " (" + timeZone.toString() + ")");
                    // caculate the due date.
                    Date dueDate = _bCalc.calcBusDate(startDate, timeZone.getId(),
                                                      _taskDuration[t],
                                                      aProvince.getId());
                    _log.info("            Due Date: " + dueDate.toString());
                }
            }
        }
    }

    public void tearDown() {

        _log.debug("Cleaning test cases ...");
        _holidayCases = null;
    }

    /**
     * get the province's timezone.
     */
    private TimeZone getTimeZone(Province province) {

        if (province.getId() == 0) {
            return (TimeZone) _timezoneCases.get(3);
        } else if (province.getId() == 1) {
            return (TimeZone) _timezoneCases.get(4);
        } else if (province.getId() == 2) {
            return (TimeZone) _timezoneCases.get(6);
        } else if (province.getId() == 3) {
            return (TimeZone) _timezoneCases.get(5);
        } else if (province.getId() == 4) {
            return (TimeZone) _timezoneCases.get(1);
        } else if (province.getId() == 5) {
            return (TimeZone) _timezoneCases.get(4);
        } else if (province.getId() == 6) {
            return (TimeZone) _timezoneCases.get(4);
        } else if (province.getId() == 7) {
            return (TimeZone) _timezoneCases.get(0);
        } else if (province.getId() == 8) {
            return (TimeZone) _timezoneCases.get(4);
        } else if (province.getId() == 9) {
            return (TimeZone) _timezoneCases.get(2);
        } else if (province.getId() == 10) {
            return (TimeZone) _timezoneCases.get(0);
        } else if (province.getId() == 11) {
            return (TimeZone) _timezoneCases.get(2);
        } else if (province.getId() == 12) {
            return (TimeZone) _timezoneCases.get(3);
        } else {
            return (TimeZone) _timezoneCases.get(0);
        }
    }

    /**
     * the province class for testing.
     */
    protected class Province {

        public Province(int id, String name) {
            _id = id;
            _name = name;
        }

        /**
         * the province id
         */
        private int _id;

        /**
         * Get the value of the province id
         */
        public int getId() {
            return _id;
        }

        /**
         * Set the value of the province id
         */
        public void setId(int id) {
            _id = id;
        }

        /**
         * the province name.
         */
        private String _name;

        /**
         * Get the value of the province name.
         */
        public String getName() {
            return _name;
        }

        /**
         * Set the value of the province name.
         */
        public void setName(String name) {
            _name = name;
        }

        /**
         * to string
         */
        public String toString() {

            return "[Name: " + getName() + " ID: " + getId() + "]";
        }
    }

    /**
     * the time zone class for testing.
     */
    protected class TimeZone {

        public TimeZone(int id, String name) {
            _id = id;
            _name = name;
        }

        /**
         * the time zone id.
         */
        private int _id;

        /**
         * Get the value of the time zone id.
         */
        public int getId() {
            return _id;
        }

        /**
         * Set the value of the time zone id.
         */
        public void setId(int id) {
            _id = id;
        }

        /**
         * the time zone name.
         */
        private String _name;

        /**
         * Get the value of the time zone name.
         */
        public String getName() {
            return _name;
        }

        /**
         * Set the value of the time zone name.
         */
        public void setName(String name) {
            _name = name;
        }

        /**
         * to string.
         */
        public String toString() {

            return "[Name: " + getName() + " ID: " + getId() + "]";
        }
    }
}
