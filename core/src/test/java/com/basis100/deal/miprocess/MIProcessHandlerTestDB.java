package com.basis100.deal.miprocess;

import java.io.IOException;
import java.util.Date;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Ignore;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealHistory;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.pk.DealPK;
import com.basis100.workflow.notification.WFNotificationEvent;

/**
 * <p>
 * MIProcessHandlerTestDB
 * </p>
 * Express Entity class unit test: ServiceProvider
 */
public class MIProcessHandlerTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;	
	private MIProcessHandler processHandler;
	private MasterDeal mDeal;
	private Deal deal;
	private WFNotificationEvent event;
	private DealHistory history;
	private DealPK pk;

	public MIProcessHandlerTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				MIProcessHandler.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		processHandler = MIProcessHandler.getInstance();

	}

	@Test
	public void testDetermineRcmCommStatus() throws Exception {
		// get input data from xml repository
		ITable testDetermineRcmCommStatus = dataSetTest
				.getTable("testDetermineRcmCommStatus");
		int dealId = Integer.parseInt((String) testDetermineRcmCommStatus
				.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String) testDetermineRcmCommStatus
				.getValue(0, "COPYID"));
		deal = new Deal(srk, null, dealId, copyId);
		processHandler.determineRcmCommStatus(deal, srk);
		assertEquals(dealId, deal.getDealId());
		assertNotNull(processHandler);
	}

	@Test
	public void testWorkflowNotified() throws Exception {
		// get input data from xml repository
		ITable testWorkflowNotified = dataSetTest
				.getTable("testWorkflowNotified");
		int dealId = Integer.parseInt((String) testWorkflowNotified.getValue(0,
				"DEALID"));
		int workflowNotificationType = Integer
				.parseInt((String) testWorkflowNotified.getValue(0,
						"WORKFLOWNOTIFICATIONTYPEID"));
		int institutionId = Integer.parseInt((String) testWorkflowNotified
				.getValue(0, "INSTITUTIONPROFILEID"));
		mDeal = new MasterDeal(srk, null, dealId);
		event = new WFNotificationEvent(dealId, workflowNotificationType, 0,
				institutionId);
		processHandler.workflowNotified(event);
		assertEquals(dealId, mDeal.getDealId());
		assertNotNull(processHandler);
	}

	@Test
	public void testPropogateDealFeeRemoval() throws Exception {
		// get input data from xml repository
		srk.beginTransaction();
		ITable testPropogateDealFeeRemoval = dataSetTest
				.getTable("testPropogateDealFeeRemoval");
		int dealId = Integer.parseInt((String) testPropogateDealFeeRemoval
				.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String) testPropogateDealFeeRemoval
				.getValue(0, "COPYID"));
		String reqType = (String) testPropogateDealFeeRemoval.getValue(0,
				"reqTypes");
		int reqTypes[] = new int[reqType.length()];
		deal = new Deal(srk, null, dealId, copyId);
		processHandler.propogateDealFeeRemoval(srk, deal, reqTypes);
		assertEquals(dealId, deal.getDealId());
		assertNotNull(processHandler);
		srk.rollbackTransaction();
	}

	@Test
	public void testPropogateDealFeeRemovalTwoArgs() throws Exception {
		// get input data from xml repository
		srk.beginTransaction();
		ITable testPropogateDealFeeRemoval = dataSetTest
				.getTable("testPropogateDealFeeRemovalTwoArgs");
		int dealId = Integer.parseInt((String) testPropogateDealFeeRemoval
				.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String) testPropogateDealFeeRemoval
				.getValue(0, "COPYID"));
		deal = new Deal(srk, null, dealId, copyId);
		processHandler.propogateDealFeeRemoval(srk, deal);
		assertEquals(dealId, deal.getDealId());
		assertNotNull(processHandler);
		srk.rollbackTransaction();
	}

	@Test
	public void testUpdateOnReply() throws Exception {
		// get input data from xml repository
		srk.beginTransaction();
		ITable testUpdateOnReply = dataSetTest.getTable("testUpdateOnReply");
		int dealId = Integer.parseInt((String) testUpdateOnReply.getValue(0,
				"DEALID"));
		int copyId = Integer.parseInt((String) testUpdateOnReply.getValue(0,
				"COPYID"));
		int dealHistoryId = Integer.parseInt((String) testUpdateOnReply
				.getValue(0, "DEALHISTORYID"));
		int userProfileId = Integer.parseInt((String) testUpdateOnReply
				.getValue(0, "USERPROFILEID"));
		int transactionTypeId = Integer.parseInt((String) testUpdateOnReply
				.getValue(0, "TRANSACTIONTYPEID"));
		int statusId = Integer.parseInt((String) testUpdateOnReply.getValue(0,
				"STATUSID"));
		String transactionText = (String) testUpdateOnReply.getValue(0,
				"TRANSACTIONTEXT");
		srk.getExpressState().setDealInstitutionId(1);
		deal = new Deal(srk, null);
		pk = new DealPK(dealId, copyId);
		history = new DealHistory(srk);
		history.setDealHistoryId(dealHistoryId);
		history.setUserProfileId(userProfileId);
		history.setTransactionTypeId(transactionTypeId);
		history.setStatusId(statusId);
		history.setTransactionDate(new Date());
		history.setTransactionText(transactionText);
		processHandler.updateOnReply(pk, srk);
		assertEquals(dealId, pk.getId());
		assertNotNull(processHandler);
		srk.rollbackTransaction();

	}

	@Test
	public void testUpdateOnSubmission() throws Exception {
		// get input data from xml repository
		srk.beginTransaction();
		ITable testUpdateOnSubmission = dataSetTest
				.getTable("testUpdateOnSubmission");
		int dealId = Integer.parseInt((String) testUpdateOnSubmission.getValue(
				0, "DEALID"));
		int copyId = Integer.parseInt((String) testUpdateOnSubmission.getValue(
				0, "COPYID"));
		int dealHistoryId = Integer.parseInt((String) testUpdateOnSubmission
				.getValue(0, "DEALHISTORYID"));
		int userProfileId = Integer.parseInt((String) testUpdateOnSubmission
				.getValue(0, "USERPROFILEID"));
		int transactionTypeId = Integer
				.parseInt((String) testUpdateOnSubmission.getValue(0,
						"TRANSACTIONTYPEID"));
		int statusId = Integer.parseInt((String) testUpdateOnSubmission
				.getValue(0, "STATUSID"));
		String transactionText = (String) testUpdateOnSubmission.getValue(0,
				"TRANSACTIONTEXT");
		srk.getExpressState().setDealInstitutionId(1);
		deal = new Deal(srk, CalcMonitor.getMonitor(srk), dealId, copyId);
		history = new DealHistory(srk);
		history.setDealHistoryId(dealHistoryId);
		history.setUserProfileId(userProfileId);
		history.setTransactionTypeId(transactionTypeId);
		history.setStatusId(statusId);
		history.setTransactionDate(new Date());
		history.setTransactionText(transactionText);
		processHandler.updateOnSubmission(deal, srk);
		assertEquals(dealId, deal.getDealId());
		assertNotNull(processHandler);
		srk.rollbackTransaction();

	}

	@Test
	public void testRemoveFeeFromDeal() throws Exception {
		// get input data from xml repository
		srk.beginTransaction();
		ITable testRemoveFeeFromDeal = dataSetTest
				.getTable("testRemoveFeeFromDeal");
		int dealId = Integer.parseInt((String) testRemoveFeeFromDeal.getValue(
				0, "DEALID"));
		int copyId = Integer.parseInt((String) testRemoveFeeFromDeal.getValue(
				0, "COPYID"));
		srk.getExpressState().setDealInstitutionId(1);
		deal = new Deal(srk, null, dealId, copyId);
		processHandler.removeFeeFromDeal(srk, deal);
		assertEquals(dealId, deal.getDealId());
		assertNotNull(processHandler);
		srk.rollbackTransaction();

	}

	
	@Test
	public void testRemoveFeeFromDealFailure() throws Exception {
		// get input data from xml repository
		ITable testRemoveFeeFromDeal = dataSetTest.getTable("testRemoveFeeFromDealFailure");
		try{
		srk.beginTransaction();		
		int dealId = Integer.parseInt((String) testRemoveFeeFromDeal.getValue(
				0, "DEALID"));
		int copyId = Integer.parseInt((String) testRemoveFeeFromDeal.getValue(
				0, "COPYID"));
		double miPremiumAmount = Double
				.parseDouble((String) testRemoveFeeFromDeal.getValue(0,
						"MIPREMIUMAMOUNT"));
		srk.getExpressState().setDealInstitutionId(1);
		deal = new Deal(srk, CalcMonitor.getMonitor(srk), dealId, copyId);
		//deal.setScenarioRecommended("Y");
		processHandler.preservePreviousMIAmount(deal, miPremiumAmount);
		assertEquals(dealId, deal.getDealId());
		assertNotNull(processHandler);
		srk.rollbackTransaction();
	}catch(Exception e){
		e.printStackTrace();
	}
	}

}