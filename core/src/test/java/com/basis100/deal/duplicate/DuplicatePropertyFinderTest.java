package com.basis100.deal.duplicate;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.Property;

public class DuplicatePropertyFinderTest extends FXDBTestCase {
	private IDataSet dataSetTest;
	private DuplicatePropertyFinder duplicatePropertyFinder = null;
	DealEntity d1;
	Deal deal = null;
	Property p1;
	ClassId classId;

	public DuplicatePropertyFinderTest(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"DuplicatePropertyFinderDataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.duplicatePropertyFinder = new DuplicatePropertyFinder(srk);
		this.p1 = new Property(srk, null);

	}

	// testFindDealListMatch
	public void testFindDealListMatch() throws Exception {
		Collection cList = null;
		ITable findByDupeCheckCriteria = dataSetTest
				.getTable("testFindDealListMatch");
		String dealId = findByDupeCheckCriteria.getValue(0, "DealId")
				.toString();
		String copyId = findByDupeCheckCriteria.getValue(0, "CopyId")
				.toString();
		deal = new Deal(srk, CalcMonitor.getMonitor(srk),
				Integer.parseInt(dealId), Integer.parseInt(copyId));
		cList = duplicatePropertyFinder.findDealListMatch(deal, new Date());
		assert cList.size() == 0;

	}

	// testFindPropertyListMatch
	public void testFindPropertyListMatch() throws Exception {
		Collection cList = null;
		ITable findByDupeCheckCriteria = dataSetTest
				.getTable("testFindPropertyListMatch");
		String dealId = findByDupeCheckCriteria.getValue(0, "DealId")
				.toString();
		String copyId = findByDupeCheckCriteria.getValue(0, "CopyId")
				.toString();
		String propertyId = findByDupeCheckCriteria.getValue(0, "PropertyId")
				.toString();
		Property property = new Property(srk, CalcMonitor.getMonitor(srk),
				Integer.parseInt(propertyId), Integer.parseInt(copyId));
		DealEntity entity = (DealEntity) property;
		cList = duplicatePropertyFinder.findDealListMatch(entity, new Date());
		assert cList.size() != 0;
		assertNotNull(cList);
	}

	public void testFindDealListMatchFailure() {
		boolean result = false;
		try {
			ITable findByDupeCheckCriteria = dataSetTest
					.getTable("testFindDealListMatch");
			String dealId = findByDupeCheckCriteria.getValue(0, "DealId")
					.toString();
			String copyId = findByDupeCheckCriteria.getValue(0, "CopyId")
					.toString();
			d1 = new Deal(srk, CalcMonitor.getMonitor(srk),
					Integer.parseInt(dealId), Integer.parseInt(copyId));
			duplicatePropertyFinder.findDealListMatch(d1, null);
		} catch (Exception e) {
			result = false;
		}
		System.out.println("Result Size:" + result);
		assertEquals(false, result);
	}

	public void testDealSearch() throws Exception {
		SearchResult result = null;
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testDealSearch");
		String dealId = findByDupeCheckCriteria.getValue(0, "DealId")
				.toString();
		String copyId = findByDupeCheckCriteria.getValue(0, "CopyId")
				.toString();
		d1 = new Deal(srk, CalcMonitor.getMonitor(srk),
				Integer.parseInt(dealId), Integer.parseInt(copyId));
		result = duplicatePropertyFinder.search(d1, new Date());
		assert result.getDuplicateId() != 0;
		assertNotNull(result);
	}

	public void testPropertySearch() throws Exception {
		SearchResult result = null;
		ITable findByDupeCheckCriteria = dataSetTest
				.getTable("testPropertySearch");
		String copyId = findByDupeCheckCriteria.getValue(0, "CopyId")
				.toString();
		String propertyId = findByDupeCheckCriteria.getValue(0, "PropertyId")
				.toString();
		Property property = new Property(srk, CalcMonitor.getMonitor(srk),
				Integer.parseInt(propertyId), Integer.parseInt(copyId));
		DealEntity entity = (DealEntity) property;
		result = duplicatePropertyFinder.search(entity, new Date());
		assert result.getDuplicateId() != 0;
		assertNotNull(result);
	}

	public void testMatchProperty() throws Exception {
		Set result = null;
		ITable testMatchProperty = dataSetTest.getTable("testMatchProperty");
		String PropertyStreetNumber = testMatchProperty.getValue(0,
				"PropertyStreetNumber").toString();
		String PropertyStreetName = testMatchProperty.getValue(0,
				"PropertyStreetName").toString();
		String PropertyCity = testMatchProperty.getValue(0, "PropertyCity")
				.toString();
		String StreetTypeId = testMatchProperty.getValue(0, "StreetTypeId")
				.toString();
		String StreetDirectionId = testMatchProperty.getValue(0,
				"StreetDirectionId").toString();
		String ProvinceId = testMatchProperty.getValue(0, "ProvinceId")
				.toString();
		String copyId = testMatchProperty.getValue(0, "CopyId").toString();
		String propertyId = testMatchProperty.getValue(0, "PropertyId")
				.toString();
		p1 = new Property(srk, CalcMonitor.getMonitor(srk),
				Integer.parseInt(propertyId), Integer.parseInt(copyId));
		p1.setPropertyStreetNumber(PropertyStreetNumber);
		p1.setPropertyStreetName(PropertyStreetName);
		p1.setPropertyCity(PropertyCity);
		p1.setStreetTypeId(Integer.parseInt(StreetTypeId));
		p1.setStreetDirectionId(Integer.parseInt(StreetDirectionId));
		p1.setProvinceId(Integer.parseInt(ProvinceId));
		result = duplicatePropertyFinder.matchProperty(p1, null);
		assert result.size() != 0;
		assertNotNull(result);

	}

	public void testMatchPropertyFailure() throws Exception {
		Set result = null;
		ITable testMatchProperty = dataSetTest
				.getTable("testMatchPropertyFailure");
		String PropertyStreetNumber = testMatchProperty.getValue(0,
				"PropertyStreetNumber").toString();
		String PropertyStreetName = testMatchProperty.getValue(0,
				"PropertyStreetName").toString();
		String PropertyCity = testMatchProperty.getValue(0, "PropertyCity")
				.toString();
		String StreetTypeId = testMatchProperty.getValue(0, "StreetTypeId")
				.toString();
		String StreetDirectionId = testMatchProperty.getValue(0,
				"StreetDirectionId").toString();
		String ProvinceId = testMatchProperty.getValue(0, "ProvinceId")
				.toString();
		String copyId = testMatchProperty.getValue(0, "CopyId").toString();
		String propertyId = testMatchProperty.getValue(0, "PropertyId")
				.toString();
		int dealId = Integer.parseInt((String) testMatchProperty.getValue(0,
				"DealId"));
		p1 = new Property(srk, CalcMonitor.getMonitor(srk),
				Integer.parseInt(propertyId), Integer.parseInt(copyId));
		p1.setPropertyStreetNumber(PropertyStreetNumber);
		p1.setPropertyStreetName(PropertyStreetName);
		p1.setPropertyCity(PropertyCity);
		p1.setStreetTypeId(Integer.parseInt(StreetTypeId));
		p1.setStreetDirectionId(Integer.parseInt(StreetDirectionId));
		p1.setProvinceId(Integer.parseInt(ProvinceId));
		p1.setDealId(dealId);
		result = duplicatePropertyFinder.matchProperty(p1, null);
		
		assert result.size() == 0;
		assertNull(result);

	}

}
