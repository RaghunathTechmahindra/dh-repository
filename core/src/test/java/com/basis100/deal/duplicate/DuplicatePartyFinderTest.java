package com.basis100.deal.duplicate;

import java.io.IOException;
import java.util.Date;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.PartyProfile;
import com.basis100.deal.entity.Property;

public class DuplicatePartyFinderTest extends FXDBTestCase {
	private IDataSet dataSetTest;
	private DuplicatePartyFinder duplicatePartyFinder = null;
	Property property = null;
	private DealEntity dealEntity = null;
	private PartyProfile pProfile = null;

	public DuplicatePartyFinderTest(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"DuplicatePartyFinderDataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.duplicatePartyFinder = new DuplicatePartyFinder(srk);

	}

	public void testSearch() throws Exception {
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testSearch");
		String partyProfileId = findByDupeCheckCriteria.getValue(0,
				"partyProfileId").toString();
		int contactId = Integer.parseInt((String) findByDupeCheckCriteria
				.getValue(0, "contactId"));
		int copyId = Integer.parseInt((String) findByDupeCheckCriteria
				.getValue(0, "copyId"));
		pProfile = new PartyProfile(srk, Integer.parseInt(partyProfileId));
		dealEntity = (DealEntity) pProfile;
		Contact contact = new Contact(srk, contactId, copyId);
		SearchResult searchResult = duplicatePartyFinder.search(dealEntity,
				new Date());
		assertEquals(contactId, contact.getContactId());
		assertNotNull(searchResult);
	}

	public void testSearchFailure() throws Exception {
		try {
			ITable findByDupeCheckCriteria = dataSetTest
					.getTable("testSearchFailure");
			String partyProfileId = findByDupeCheckCriteria.getValue(0,
					"partyProfileId").toString();
			int contactId = Integer.parseInt((String) findByDupeCheckCriteria
					.getValue(0, "contactId"));
			int copyId = Integer.parseInt((String) findByDupeCheckCriteria
					.getValue(0, "copyId"));
			pProfile = new PartyProfile(srk, Integer.parseInt(partyProfileId));
			dealEntity = (DealEntity) pProfile;
			Contact contact = new Contact(srk, contactId, copyId);
			SearchResult searchResult = duplicatePartyFinder.search(dealEntity,
					new Date());
			assertEquals(contactId, contact.getContactId());
			assertNotNull(searchResult);
		} catch (Exception e) {
			e.getMessage();
		}
	}
}
