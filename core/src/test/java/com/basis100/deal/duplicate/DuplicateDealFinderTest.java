package com.basis100.deal.duplicate;

import java.io.IOException;
import java.util.Date;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;

public class DuplicateDealFinderTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private DuplicateDealFinder duplicateDealFinder;
	private DealEntity dealEntity = null;
	private Deal deal = null;

	public DuplicateDealFinderTest(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				DuplicateDealFinder.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		duplicateDealFinder = new DuplicateDealFinder(srk);
	}	

	public void testsearch() throws Exception {
		SearchResult searchResult = null;
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testsearch");
		int dealId = Integer.parseInt((String) findByDupeCheckCriteria
				.getValue(0, "DealId"));
		int copyId = Integer.parseInt((String) findByDupeCheckCriteria
				.getValue(0, "copyId"));
		deal = new Deal(srk, CalcMonitor.getMonitor(srk), dealId, copyId);
		deal.setSpecialFeatureId(1);
		searchResult = duplicateDealFinder.search(deal, new Date());
		assertNotNull(searchResult);
	}

	public void testsearchFailure() throws Exception {

		ITable findByDupeCheckCriteria = dataSetTest
				.getTable("testsearchFailure");
		String dealId = findByDupeCheckCriteria.getValue(0, "DealId")
				.toString();
		String copyId = findByDupeCheckCriteria.getValue(0, "CopyId")
				.toString();
		boolean result = false;
		try {
			duplicateDealFinder = new DuplicateDealFinder(srk);
			dealEntity = new Deal(srk, CalcMonitor.getMonitor(srk),
					Integer.parseInt(dealId), Integer.parseInt(copyId));
			duplicateDealFinder.search(dealEntity, new Date());
			result = true;
		} catch (Exception e) {
			result = false;
		}
		assertSame(false, result);
	}

}
