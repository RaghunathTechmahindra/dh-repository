package com.basis100.deal.duplicate;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;

public class DuplicateSourceFinderTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private DuplicateSourceFinder duplicateSourceFinder = null;
	private Deal deal = null;
	private SearchResult result = null;
	private Borrower borrower = null;
	private DealEntity entity = null;

	public DuplicateSourceFinderTest(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"DuplicateSourceFinderDataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.duplicateSourceFinder = new DuplicateSourceFinder(srk);
	}

	
	  public void testSearch() throws Exception { srk.beginTransaction();
	  
	  ITable testSearch = dataSetTest.getTable("testSearch"); String dealId =
	  testSearch.getValue(0,"DealId").toString(); String copyId =
	  testSearch.getValue(0,"CopyId").toString(); deal =new
	  Deal(srk,CalcMonitor.getMonitor(srk),Integer.parseInt(dealId),Integer.parseInt(copyId));
	  String application = testSearch.getValue(0,"applicationDate").toString();
	  SimpleDateFormat formate=new SimpleDateFormat("dd-MMM-yy"); Date
	  applicationDate=formate.parse(application); entity=(DealEntity)deal;
	  result=duplicateSourceFinder.search(entity,applicationDate);
	  assertNotNull(result); srk.rollbackTransaction(); }
	 

	// get Deal
	public void testFindDealListMatchSearch() throws Exception {
		srk.beginTransaction();
		ITable findByDupeCheckCriteria = dataSetTest
				.getTable("testFindDealListMatchSearch");
		String dealId = findByDupeCheckCriteria.getValue(0, "DealId")
				.toString();
		String copyId = findByDupeCheckCriteria.getValue(0, "CopyId")
				.toString();
		String borrowerBirthDate = (String) findByDupeCheckCriteria.getValue(0,
				"BORROWERBIRTHDATE");
		SimpleDateFormat formate = new SimpleDateFormat("dd-MMM-yy");
		Date applicationDate = formate.parse(borrowerBirthDate);
		deal = new Deal(srk, CalcMonitor.getMonitor(srk),
				Integer.parseInt(dealId), Integer.parseInt(copyId));
		entity = (DealEntity) deal;
		result = duplicateSourceFinder.search(entity, applicationDate);
		assertNotSame(0, result);
		srk.rollbackTransaction();
	}

}
