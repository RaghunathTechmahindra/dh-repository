package com.basis100.deal.duplicate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.CautionProperty;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.Property;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class CautionPropertyFinderDBTest extends FXDBTestCase {

	// The logger
	private final static Logger _logger = LoggerFactory
			.getLogger(CautionPropertyFinderDBTest.class);
	private IDataSet dataSetTest;
	private CautionPropertyFinder cautionPropertyFinder;
	public int institutionProfileId = -1;
	private Deal deal = null;
	private Property property = null;

	public CautionPropertyFinderDBTest(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass()
				.getResource(
						CautionPropertyFinder.class.getSimpleName()
								+ "DataSetTest.xml"));
	}

	protected DatabaseOperation getSetUpOperation() throws Exception {
		return DatabaseOperation.INSERT;
	}

	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.cautionPropertyFinder = new CautionPropertyFinder(srk);
	}

	/**
	 * test search.
	 * 
	 * @throws FinderException
	 * @throws RemoteException
	 * @throws DataSetException
	 */
	@Test
	public void testDealSearch() throws RemoteException, FinderException,
			DataSetException {
		srk = new SessionResourceKit("CautionPropertyFinderTest");
		srk.getExpressState().setDealInstitutionId(1);
		deal = new Deal(srk, null);
		ITable testSearch = dataSetTest.getTable("testDealSearch");
		int rowCount = testSearch.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			try {
				String dealSearch = (String) testSearch.getValue(i, "id");
				int dealid = Integer.parseInt(dealSearch);
				String copy = (String) testSearch.getValue(i, "copyId");
				int copyid = Integer.parseInt(copy);
				DealPK dealPK = new DealPK(dealid, copyid);
				deal = deal.findByPrimaryKey(dealPK);
				CautionPropertyFinder propFinder = new CautionPropertyFinder(
						srk);
				SearchResult appProperties = propFinder
						.search(deal, new Date());
				assertEquals(dealSearch, appProperties.searchDeal.getDealId());
			}

			catch (Exception ex) {
				String msg = "DuplicatePropertyFinder - Failed to fetch properties for: "
						+ deal;
				_logger.error(msg);
			}
		}

	}

	@Test
	public void testPropertySearch() throws RemoteException, FinderException,
			DataSetException {
		srk = new SessionResourceKit("CautionPropertyFinderTest");
		srk.getExpressState().setDealInstitutionId(1);
		ITable testSearch = dataSetTest.getTable("testPropertySearch");
		int rowCount = testSearch.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			try {
				String dealSearch = (String) testSearch.getValue(i, "id");
				int propertyId = Integer.parseInt(dealSearch);
				String copy = (String) testSearch.getValue(i, "copyId");
				int copyid = Integer.parseInt(copy);
				property = new Property(srk, null, propertyId, copyid);
				PropertyPK pk = new PropertyPK(propertyId, copyid);
				property = property.findByPrimaryKey(pk);
				DealEntity entity = (DealEntity) property;
				CautionPropertyFinder propFinder = new CautionPropertyFinder(
						srk);
				SearchResult appProperties = propFinder.search(entity,
						new Date());
				assertEquals(dealSearch, appProperties.searchDeal.getDealId());
			}

			catch (Exception ex) {
				String msg = "DuplicatePropertyFinder - Failed to fetch properties for: "
						+ deal;
				_logger.error(msg);
			}
		}

	}

	/**
	 * test matchCP.
	 */
	@Test
	public void testMatchCP() throws Exception {
		ITable testMatchCP = dataSetTest.getTable("testMatchCP");
		List<SearchResult> allDeals = new ArrayList<SearchResult>();
		String deal = (String) testMatchCP.getValue(0, "dealId");
		int dealId = Integer.parseInt(deal);
		String dealSearch = (String) testMatchCP.getValue(0, "id");
		int propertyId = Integer.parseInt(dealSearch);
		String copy = (String) testMatchCP.getValue(0, "copyId");
		int copyid = Integer.parseInt(copy);
		String cautionProperties = (String) testMatchCP.getValue(0,
				"CAUTIONPROPERTIESID");
		int cautionPropertiesId = Integer.parseInt(cautionProperties);
		srk.getExpressState().setDealInstitutionId(1);
		Deal de = new Deal(srk, null);
		de.setDealId(dealId);
		property = new Property(srk, null);
		property.setDealId(propertyId);
		CautionProperty cautionProperty = new CautionProperty(srk);
		cautionProperty.setCautionPropertiesId(cautionPropertiesId);
		SearchResult searchResult = new CautionPropertySearchResult(srk,
				cautionProperty, de);
		searchResult.match = false;
		searchResult.action = -1;
		allDeals.add(searchResult);
		cautionPropertyFinder.matchCP(property);
		assertNotNull(searchResult);
	}
}
