package com.basis100.deal.duplicate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;

public class DupCheckActionTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private DupCheckAction dupCheckAction;

	public DupCheckActionTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(DupCheckAction.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(DupCheckAction.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.dupCheckAction = new DupCheckAction(srk);		
	}
	
	public void testExecute() throws DataSetException, DupCheckActionException, RemoteException, FinderException{
		ITable testExecute = dataSetTest.getTable("testExecute");
		List<SearchResult> allDeals=new ArrayList<SearchResult>();		
		Deal de=new Deal(srk,null);
		de.setSourceApplicationId("QA-57884");
    	de.setDealId(1054864); 
    	de.setApplicationId("1054864");    	
    	SearchResult searchResult=new PropertySearchResult(srk,de,null,allDeals);
    	searchResult.match=false;
    	searchResult.action=-1;
    	PassiveMessage pm=dupCheckAction.execute(searchResult, this.srk.getLanguageId());
    	assertNotNull(pm);
	}

}
