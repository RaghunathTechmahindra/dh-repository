package com.basis100.deal.duplicate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;

import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Deal;


public class DuplicateFinderTest extends  FXDBTestCase  {

private IDataSet dataSetTest;
private DuplicateApplicationFinder  duplicateApplicationFinder=null;
Property p1=null;
Deal d1=null,ex=null,in=null;


public DuplicateFinderTest(String name) throws IOException, DataSetException {
	super(name);
	dataSetTest = new FlatXmlDataSet(this.getClass().getResource("DuplicateFinderDataSetTest.xml"));
}



@Override
protected DatabaseOperation getTearDownOperation() throws Exception {
	return DatabaseOperation.NONE;
}

@Override
protected void setUp() throws Exception {
	super.setUp();
	this.duplicateApplicationFinder = new DuplicateApplicationFinder(srk);
	this.p1=new Property(srk);
	this.d1=new Deal(srk,null);
	this.in=new Deal(srk,null);
	this.ex=new Deal(srk,null);

}


public void testPropertyDeltaType() throws Exception {
	
	ITable streetDetails = dataSetTest.getTable("testPropertyDeltaType");
	String streetNo=streetDetails.getValue(0,"StreetNumber").toString();
	String streetName=streetDetails.getValue(0,"StreetName").toString();
	Collection<Property> ps=new ArrayList<Property>();
	p1.setPropertyStreetNumber(streetNo);
	p1.setPropertyStreetName(streetName);
	ps.add(p1);
	int result=duplicateApplicationFinder.propertyDeltaType(ps);
	assertEquals(2,result);

}



public void testPropertyDeltaTypeFailure() throws Exception {
	ITable streetDetails = dataSetTest.getTable("testPropertyDeltaTypeFailure");
	String streetNo=streetDetails.getValue(0,"StreetNumber").toString();
	String streetName=streetDetails.getValue(0,"StreetName").toString();
	Collection<Property> ps=new ArrayList<Property>();
	p1.setPropertyStreetNumber(streetNo);
	p1.setPropertyStreetName(streetName);
	ps.add(p1);
	int result=duplicateApplicationFinder.propertyDeltaType(ps);
	assertEquals(0,result);
}

public void testfindStatusCategory() throws Exception{
	ITable streetDetails = dataSetTest.getTable("testfindStatusCategory");
	String StatusCategoryId=streetDetails.getValue(0,"StatusCategoryId").toString();
	d1.setStatusId(Integer.parseInt(StatusCategoryId));
	int result=duplicateApplicationFinder.findStatusCategory(d1);
	System.out.println("Result in success :"+result);
	assertNotSame(-1,result);
	
}

public void testfindStatusCategoryFailure() throws Exception{
	ITable streetDetails = dataSetTest.getTable("testfindStatusCategoryFailure");
	String StatusCategoryId=streetDetails.getValue(0,"StatusCategoryId").toString();
	d1.setStatusId(Integer.parseInt(StatusCategoryId));
	int result=duplicateApplicationFinder.findStatusCategory(d1);
	System.out.println("Result in Failure :"+result);
	assertSame(0,result);
	
}

public void testDetectDealFieldDelta() throws Exception{
	
	ITable dealAmountDetails = dataSetTest.getTable("testDetectDealFieldDelta");
	String dealTotalAmount1=dealAmountDetails.getValue(0,"DealTotalAmount1").toString();
	String dealTotalAmount2=dealAmountDetails.getValue(0,"DealTotalAmount2").toString();
	ex.setTotalLoanAmount(Integer.parseInt(dealTotalAmount1));
	in.setTotalLoanAmount(Integer.parseInt(dealTotalAmount2));
	int result=duplicateApplicationFinder.detectDealFieldDelta(ex,in);
	System.out.println("Result"+result);
	assertSame(4,result);
}

public void testDetectDealFieldDeltaFailure() throws Exception{
	
	ITable findByDupeCheckCriteria = dataSetTest.getTable("testDetectDealFieldDeltaFailure");
	String  dealId = findByDupeCheckCriteria.getValue(0,"DealId").toString();
	String  copyId = findByDupeCheckCriteria.getValue(0,"CopyId").toString();
	ex =new Deal(srk,CalcMonitor.getMonitor(srk),Integer.parseInt(dealId),Integer.parseInt(copyId));
	in =new Deal(srk,CalcMonitor.getMonitor(srk),Integer.parseInt(dealId),Integer.parseInt(copyId));
	int result=duplicateApplicationFinder.detectDealFieldDelta(ex,in);
	System.out.println("Result"+result);
	assertSame(0,result);
	
}






}