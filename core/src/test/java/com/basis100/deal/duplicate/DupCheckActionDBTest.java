package com.basis100.deal.duplicate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.CautionProperty;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.PartyProfile;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;

public class DupCheckActionDBTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private DupCheckAction dupCheckAction;
	private SearchResult searchResult = null;
	private PassiveMessage pm = null;
	private Deal de = null;

	public DupCheckActionDBTest(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				DupCheckAction.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.dupCheckAction = new DupCheckAction(srk);
	}

	// Test for PropertySearchResult
	public void testExecutePropertySearch() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testExecute = dataSetTest.getTable("testExecutePropertySearch");
		List<SearchResult> allDeals = new ArrayList<SearchResult>();
		int dealId = Integer.parseInt((String) testExecute
				.getValue(0, "DEALID"));
		String applicationId = (String) testExecute
				.getValue(0, "APPLICATIONID");
		srk.getExpressState().setDealInstitutionId(1);
		de = new Deal(srk, null);
		de.setDealId(dealId);
		de.setApplicationId(applicationId);
		searchResult = new PropertySearchResult(srk, de, de, allDeals);
		searchResult.match = false;
		searchResult.action = -1;
		allDeals.add(searchResult);
		pm = dupCheckAction.execute(searchResult, this.srk.getLanguageId());
		assert dupCheckAction.getStoredDealPK() != null;
		assertNotNull(pm);
	}

	// Test for PartySearchResult
	public void testExecutePartySearch() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testExecute = dataSetTest.getTable("testExecutePartySearch");
		List<SearchResult> allDeals = new ArrayList<SearchResult>();
		int partyProfileId = Integer.parseInt((String) testExecute.getValue(0,
				"PARTYPROFILEID"));
		int dealId = Integer.parseInt((String) testExecute
				.getValue(0, "DEALID"));
		srk.getExpressState().setDealInstitutionId(1);
		de = new Deal(srk, null);
		de.setDealId(dealId);
		PartyProfile partyProfile = new PartyProfile(srk, partyProfileId);
		partyProfile.setPartyProfileId(partyProfileId);
		searchResult = new PartySearchResult(srk, partyProfile, partyProfile);
		searchResult.match = false;
		searchResult.action = -1;
		allDeals.add(searchResult);
		pm = dupCheckAction.execute(searchResult, this.srk.getLanguageId());
		assert dupCheckAction.getStoredDealPK() != null;
		assertNotNull(pm);
	}

	// Test for BorrowerSearchResult
	public void testExecuteBorrowerSearch() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testExecute = dataSetTest.getTable("testExecuteBorrowerSearch");
		List<SearchResult> allDeals = new ArrayList<SearchResult>();
		int dealId = Integer.parseInt((String) testExecute
				.getValue(0, "DEALID"));
		srk.getExpressState().setDealInstitutionId(1);
		de = new Deal(srk, null);
		de.setDealId(dealId);
		searchResult = new BorrowerSearchResult(srk, de, de, allDeals);
		searchResult.match = false;
		searchResult.action = -1;
		allDeals.add(searchResult);
		pm = dupCheckAction.execute(searchResult, this.srk.getLanguageId());
		assert dupCheckAction.getStoredDealPK() != null;
		assertNotNull(pm);
	}

	// Test for CautionPropertySearchResult
	public void testExecuteCautionProperty() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testExecute = dataSetTest.getTable("testExecuteCautionProperty");
		List<SearchResult> allDeals = new ArrayList<SearchResult>();
		int dealId = Integer.parseInt((String) testExecute
				.getValue(0, "DEALID"));
		int cautionPropertiesId = Integer.parseInt((String) testExecute
				.getValue(0, "CAUTIONPROPERTIESID"));
		srk.getExpressState().setDealInstitutionId(1);
		de = new Deal(srk, null);
		de.setDealId(dealId);
		CautionProperty cautionProperty = new CautionProperty(srk);
		cautionProperty.setCautionPropertiesId(cautionPropertiesId);
		searchResult = new CautionPropertySearchResult(srk, cautionProperty, de);
		searchResult.match = false;
		searchResult.action = -1;
		allDeals.add(searchResult);
		pm = dupCheckAction.execute(searchResult, this.srk.getLanguageId());
		assert dupCheckAction.getStoredDealPK() != null;
		assertNotNull(pm);
	}

	// Test for CautionPropertySearchResultFailure
	public void testExecuteCautionPropertyFailure() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		try {
			ITable testExecute = dataSetTest
					.getTable("testExecuteCautionPropertyFailure");
			List<SearchResult> allDeals = new ArrayList<SearchResult>();
			int dealId = Integer.parseInt((String) testExecute.getValue(0,
					"DEALID"));
			int cautionPropertiesId = Integer.parseInt((String) testExecute
					.getValue(0, "CAUTIONPROPERTIESID"));
			srk.getExpressState().setDealInstitutionId(1);
			de = new Deal(srk, null);
			de.setDealId(dealId);
			CautionProperty cautionProperty = new CautionProperty(srk);
			cautionProperty.setCautionPropertiesId(cautionPropertiesId);
			searchResult = new CautionPropertySearchResult(srk,
					cautionProperty, de);
			searchResult.match = true;
			searchResult.action = -1;
			searchResult.searchDeal = de;
			allDeals.add(searchResult);
			pm = dupCheckAction.execute(searchResult, this.srk.getLanguageId());
			assert dupCheckAction.getStoredDealPK() != null;
			assertNotNull(pm);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	// Test for DealSearchResult
	public void testExecuteDealSearchResult() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		ITable testExecute = dataSetTest
				.getTable("testExecuteDealSearchResult");
		List<SearchResult> allDeals = new ArrayList<SearchResult>();
		int dealId = Integer.parseInt((String) testExecute
				.getValue(0, "DEALID"));
		srk.getExpressState().setDealInstitutionId(1);
		de = new Deal(srk, null);
		de.setDealId(dealId);
		searchResult = new DealSearchResult(srk, de, de, 2, 2);
		searchResult.match = false;
		searchResult.action = -1;
		searchResult.deltaType = -1;
		searchResult.levelOneCode = 1;
		allDeals.add(searchResult);
		pm = dupCheckAction.execute(searchResult, this.srk.getLanguageId());
		assert dupCheckAction.getStoredDealPK() != null;
		assertNotNull(pm);
	}

	// Test for DealSearchResultFailure
	public void testExecuteDealSearchResultFailure() throws DataSetException,
			DupCheckActionException, RemoteException, FinderException {
		try {
			srk.beginTransaction();
			ITable testExecute = dataSetTest
					.getTable("testExecuteDealSearchResultFailure");
			List<SearchResult> allDeals = new ArrayList<SearchResult>();
			int dealId = Integer.parseInt((String) testExecute.getValue(0,
					"DEALID"));
			srk.getExpressState().setDealInstitutionId(1);
			de = new Deal(srk, null);
			de.setDealId(dealId);
			searchResult = new DealSearchResult(srk, de, de, 2, 2);
			searchResult.match = false;
			searchResult.action = -1;
			searchResult.deltaType = -1;
			searchResult.levelOneCode = 1;
			searchResult.dsc = 1;
			searchResult.softDenial = true;
			allDeals.add(searchResult);
			pm = dupCheckAction.execute(searchResult, this.srk.getLanguageId());
			assert dupCheckAction.getStoredDealPK() != null;
			assertNotNull(pm);

		} catch (Exception e) {
			e.getMessage();

		}
		try {
			srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {
			e.printStackTrace();
		}
	}
}
