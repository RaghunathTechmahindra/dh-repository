package com.basis100.deal.duplicate;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;

public class DuplicateApplicationFinderTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private DuplicateApplicationFinder duplicateApplicationFinder;
	private DealEntity entity = null;

	public DuplicateApplicationFinderTest(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				DuplicateApplicationFinder.class.getSimpleName()
						+ "DataSetTest.xml"));
	}


	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
		// return DatabaseOperation.UPDATE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.duplicateApplicationFinder = new DuplicateApplicationFinder(srk);
	}

	public void testSearch() throws Exception {
		ITable testSearch = dataSetTest.getTable("testSearch");
		SearchResult searchResult = new PropertySearchResult(srk);
		int dealId = Integer
				.parseInt((String) testSearch.getValue(0, "dealId"));
		int copyId = Integer
				.parseInt((String) testSearch.getValue(0, "copyId"));
		String sourceApplicationId = (String) testSearch.getValue(0,
				"sourceApplicationId");
		String indexEffectiveDate = (String) testSearch.getValue(0,
				"INDEXEFFECTIVEDATE");
		SimpleDateFormat formate = new SimpleDateFormat("dd-MMM-yy");
		Date date = formate.parse(indexEffectiveDate);
		Deal de = new Deal(srk, CalcMonitor.getMonitor(srk), dealId, copyId);
		de.setSourceApplicationId(sourceApplicationId);
		de.setSystemTypeId(58);
		searchResult = duplicateApplicationFinder.search(de, date);
		assertNotNull(searchResult);
	}

	public void testSearchFailure() throws Exception {
		try {
			ITable testSearch = dataSetTest.getTable("testSearchFailure");
			SearchResult searchResult = new PropertySearchResult(srk);
			int dealId = Integer.parseInt((String) testSearch.getValue(0,
					"dealId"));
			String sourceApplicationId = (String) testSearch.getValue(0,
					"sourceApplicationId");
			String indexEffectiveDate = (String) testSearch.getValue(0,
					"INDEXEFFECTIVEDATE");
			SimpleDateFormat formate = new SimpleDateFormat("dd-MMM-yy");
			Date date = formate.parse(indexEffectiveDate);
			Deal deal = new Deal(srk, null);
			deal.setSourceApplicationId(sourceApplicationId);
			deal.setDealId(dealId);
			deal.setSystemTypeId(100);
			entity = (DealEntity) deal;
			searchResult = duplicateApplicationFinder.search(entity, date);
			assertNotNull(searchResult);
		} catch (Exception e) {
			e.getMessage();
		}
	}

}
