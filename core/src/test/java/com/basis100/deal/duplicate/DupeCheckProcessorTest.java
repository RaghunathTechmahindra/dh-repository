package com.basis100.deal.duplicate;


import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.pk.ConditionPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.UnitTestLogging;

/**
 * DupeCheckProcessor Test 
 * 
 * @author maida
 * @since Express 4.3GR 
 *
 */
public class DupeCheckProcessorTest extends Assert implements UnitTestLogging {

    public static final String INSTITUTIONPROFILE = "INSTITUTIONPROFILE";
    public static final String DUPECHECK_NAME = "com.basis100.deal.duplicate.GENXDupeCheckProcessor";
    public static final int CONDITIONTYPE_DC_AND_COMMITMENT = 0;
    public static final int NumberOfConditions = 5;
    public static final int DOC_SOURCE_SYSTEM = 0;
    
    public static int[] LOB_LIST = {0,1,2};
    public int institutionProfileId = -1; 
    public Deal oldDeal;
    public Deal newDeal;
    public DupeCheckProcessor dupcheckP = new DupeCheckProcessor();
    
    boolean isReassignOgirinalUser = false;
    boolean isLOBMatchedOnly = false;
    boolean isPreAppCondReset = false;
    
    // The logger
    private final static Logger _logger = LoggerFactory
        .getLogger(DupeCheckProcessorTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public DupeCheckProcessorTest() {
	
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    
	
        ResourceManager.init();
        _srk = new SessionResourceKit("DupeCheckProcessorTest");
        // this method 
        institutionProfileId = BXResources.getDefaultInstitutionId();
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);
        _srk.beginTransaction();
        MasterDeal oldmd = MasterDeal.createDefaultTree(_srk, null);
        oldDeal = new Deal(_srk, null);
        oldDeal = oldDeal.findByPrimaryKey(new DealPK(oldmd.getDealId(), 1));
        
        MasterDeal newmd = MasterDeal.createDefaultTree(_srk, null);
        newDeal = new Deal(_srk, null);
        newDeal = newDeal.findByPrimaryKey(new DealPK(newmd.getDealId(), 1));
        
        isReassignOgirinalUser  = PropertiesCache.getInstance( ).getProperty(institutionProfileId,
		  "ingestion.route.declined.resubmission.to.original.uw" , "N" ).equalsIgnoreCase( "Y" );
        isLOBMatchedOnly = PropertiesCache.getInstance( ).getProperty(institutionProfileId,
		  "ingestion.route.declined.resubmission.to.original.uw.lob" , "Y" ).equalsIgnoreCase( "Y" );
        
	isPreAppCondReset = PropertiesCache.getInstance()
	      .getProperty(institutionProfileId, 
	    		  "com.filogix.ingestion.preappfirm.conditions.reset" , "N" )
	      .equalsIgnoreCase( "Y" );
        
    }

    /**
     * test case for Denied Deal Routing
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        _srk.rollbackTransaction();
        _srk.freeResources();
    }

    @Test 
    public void createXRefRoutingTest() throws Exception {/*
	
	String sourceApplicationId = "DupeCheckTest1";
	oldDeal.setSourceApplicationId(sourceApplicationId);
	newDeal.setSourceApplicationId(sourceApplicationId);
        setLineOfBusiness(oldDeal, 0);
        setUnderWriter(oldDeal);
        
        // check if the original Deal is Denied.
        setStatus(oldDeal, Mc.DEAL_DENIED);
        
        Class processorClass = Class.forName(DUPECHECK_NAME);
    	DupeCheckProcessor dupeCheckProcessor = (DupeCheckProcessor) processorClass.newInstance();
    	dupeCheckProcessor.srk = _srk;
	setLineOfBusiness(newDeal, 1);

    	dupeCheckProcessor.deal = newDeal;
    	dupeCheckProcessor.existingDeal = oldDeal;
	dupeCheckProcessor.createXRef();
	
	if (isReassignOgirinalUser) {
		if (isLOBMatchedOnly) {
		    assertNotSame(oldDeal.getUnderwriterUserId(), newDeal.getUnderwriterUserId());		    
		} else {
		    assertSame(oldDeal.getUnderwriterUserId(), newDeal.getUnderwriterUserId());		    		    
		}	    
	} else {
	    assertNotSame(oldDeal.getUnderwriterUserId(), newDeal.getUnderwriterUserId()); 
	}
	
        // check if the original Deal is NOT Denied.
	setStatus(oldDeal, Mc.DEAL_COMMIT_OFFERED);
    	dupeCheckProcessor.deal = newDeal;
    	dupeCheckProcessor.existingDeal = oldDeal;
	dupeCheckProcessor.createXRef();
	
	assertNotSame(oldDeal.getUnderwriterUserId(), newDeal.getUnderwriterUserId());		    
	
    */}
    
    
    
    
    /**
     * test case for clean up conditions Deal Routing
     * @throws Exception
     */
    @Test 
    public void performCreateWithExistingTest() throws Exception {/*
	
	String sourceApplicationId = "DupeCheckTest1";
	oldDeal.setSourceApplicationId(sourceApplicationId);
	newDeal.setSourceApplicationId(sourceApplicationId);
        setLineOfBusiness(oldDeal, 0);
        setUnderWriter(oldDeal);
        addCondition(oldDeal);
        
        boolean isReassignOgirinalUser  = PropertiesCache.getInstance( ).getProperty(newDeal.getInstitutionProfileId(),
  		  "ingestion.route.declined.resubmission.to.original.uw" , "N" ).equalsIgnoreCase( "Y" );
        boolean isLOBMatchedOnly = PropertiesCache.getInstance( ).getProperty(newDeal.getInstitutionProfileId(),
  		  "ingestion.route.declined.resubmission.to.original.uw.lob" , "Y" ).equalsIgnoreCase( "Y" );
 
        Class processorClass = Class.forName(DUPECHECK_NAME);
    	DupeCheckProcessor dupeCheckProcessor = (DupeCheckProcessor) processorClass.newInstance();
    	dupeCheckProcessor.srk = _srk;
	setLineOfBusiness(newDeal, 1);

    	dupeCheckProcessor.deal = newDeal;
    	dupeCheckProcessor.existingDeal = oldDeal;
	dupeCheckProcessor.createXRef();
	
	if (isReassignOgirinalUser) {
		if (isLOBMatchedOnly) {
		    assertNotSame(oldDeal.getUnderwriterUserId(), newDeal.getUnderwriterUserId());		    
		} else {
		    assertSame(oldDeal.getUnderwriterUserId(), newDeal.getUnderwriterUserId());		    		    
		}	    
	} else {
	    assertNotSame(oldDeal.getUnderwriterUserId(), newDeal.getUnderwriterUserId()); 
	}
    */}
    

    
    private void addCondition(Deal deal) throws Exception{
	
        JdbcExecutor jExec = _srk.getJdbcExecutor();

	for (int i=0; i<NumberOfConditions; i++ ){
	    Condition condition = insertTestCondition(jExec);
	    DocumentTracking dt = new DocumentTracking(_srk);
	    dt.createWithNoVerbiage(condition, oldDeal.getDealId(), 1, Mc.DOC_STATUS_REQUESTED, DOC_SOURCE_SYSTEM);
	}	
    }
    
    private void setLineOfBusiness(Deal deal, int lobIndex) throws Exception {
	
	deal.setLineOfBusinessId(LOB_LIST[lobIndex]);
	deal.ejbStore();
	
    }
    
    private void setStatus(Deal deal, int statisId) throws Exception {
	
	deal.setStatusId(statisId);
	deal.ejbStore();
	
    }
    
    
    private void setUnderWriter(Deal deal) throws Exception {
	
	String sql = "select up.userprofileId from userProfile up, usersecurityPolicy usp where up.userTypeID = " 
	    + Mc.USER_TYPE_SR_UNDERWRITER + " and up.USERLOGIN = usp.USERLOGIN "
	    + " and USERSECURITYPOLICYSTATUS = " + Sc.USERSECUTITYPOLICY_STATUS_CURRENT 
	    + " AND PROFILESTATUSID =  " + Sc.PROFILE_STATUS_ACTIVE;
	
        JdbcExecutor jExec = _srk.getJdbcExecutor();
        int upId = 0;
        int key = jExec.execute(sql);

        for (; jExec.next(key);  ) {
            upId = jExec.getInt(key,1);  // get the 1st record
            break;
        }

        jExec.closeData(key);
        if( upId == -1 ) throw new Exception();
        
        deal.setUnderwriterUserId(upId);
        deal.ejbStore();
    }
    
    private Condition insertTestCondition(JdbcExecutor jExec) throws Exception {
	
        int conditionId = -1;
        int conditonTypeId = -1;
        int conditionResponsibilityRoleId = -1;
        int conditionBasedonDateId = -1;
        int conditionCalcTypeId = -1;

	// add temp condition for conditon table
	String sql = "select max(conditionId) from condition";
        int key = jExec.execute(sql);

        // add conditionType
        for (; jExec.next(key);  ) {
            conditionId = jExec.getInt(key,1);  // get the 1st record
        }
        jExec.closeData(key);
        if( conditionId == -1 ) throw new Exception();
        conditionId++;
        
        sql = "select max(CONDITIONTYPEID) from conditionType";
        key = jExec.execute(sql);
        for (; jExec.next(key);  ) {
            conditonTypeId = jExec.getInt(key,1);  // get the 1st record
        }
        jExec.closeData(key);
        if( conditonTypeId == -1 ) throw new Exception();
        conditonTypeId++;
        sql = "insert into conditionType (CONDITIONTYPEID, CONDITIONDESCRIPTION) values ( " 
            + conditonTypeId + ", 'test conditionType')"; 
        jExec.execute(sql);

        // add CONDITIONRESPONSIBILITYROLE
        sql = "select max(CONDITIONRESPONSIBILITYROLEID) from CONDITIONRESPONSIBILITYROLE";
        key = jExec.execute(sql);
        for (; jExec.next(key);  ) {
            conditionResponsibilityRoleId = jExec.getInt(key,1);  // get the 1st record
        }
        jExec.closeData(key);
        if( conditionResponsibilityRoleId == -1 ) throw new Exception();
        conditionResponsibilityRoleId++;
        sql = "insert into CONDITIONRESPONSIBILITYROLE (CONDITIONRESPONSIBILITYROLEID, CRRDESCRIPTION) values ( " 
            + conditionResponsibilityRoleId + ", 'test CONDITIONRESPONSIBILITYROLE')"; 
        jExec.execute(sql);
        
        sql = "select max(conditionBasedonDateId) from CONDITIONBASEDONDATE";
        key = jExec.execute(sql);
        for (; jExec.next(key);  ) {
            conditionBasedonDateId = jExec.getInt(key,1);  // get the 1st record
        }
        jExec.closeData(key);
        if( conditionBasedonDateId == -1 ) throw new Exception();
        conditionBasedonDateId++;
        sql = "insert into CONDITIONBASEDONDATE (CONDITIONBASEDONDATEID, CBODDESCRIPTION) values ( " 
            + conditionBasedonDateId + ", 'test CONDITIONBASEDONDATE')"; 
        jExec.execute(sql);

        // add CONDITIONCALCULATIONTYPE
        sql = "select max(CONDITIONCALCULATIONTYPEID) from CONDITIONCALCULATIONTYPE";
        key = jExec.execute(sql);
        for (; jExec.next(key);  ) {
            conditionCalcTypeId = jExec.getInt(key,1);  // get the 1st record
        }
        jExec.closeData(key);
        if( conditionCalcTypeId == -1 ) throw new Exception();
        conditionCalcTypeId++;
        sql = "insert into CONDITIONCALCULATIONTYPE (CONDITIONCALCULATIONTYPEID, DURATIONDAYSDESCRIPTION) values ( " 
            + conditionCalcTypeId + ", 'test CONDITIONCALCULATIONTYPE')"; 
        jExec.execute(sql);
        
        // add condition
        sql = "insert into condition (CONDITIONID,  CONDITIONTYPEID, CONDITIONRESPONSIBILITYROLEID, "
            + "CONDITIONBASEDONDATEID, CONDITIONCALCULATIONTYPEID, INSTITUTIONPROFILEID) values "
            + "(" + conditionId + "," + conditonTypeId + "," + conditionResponsibilityRoleId + "," 
            + conditionBasedonDateId + "," + conditionCalcTypeId + "," + institutionProfileId + ")";
        jExec.execute(sql);

        Condition cond = new Condition(_srk);
        cond = cond.findByPrimaryKey(new ConditionPK(conditionId));
        return cond;

    }
    
    private void insertDocumentTracking(JdbcExecutor jExec, Condition cond) 
    	throws Exception {
	
	
    }

}
