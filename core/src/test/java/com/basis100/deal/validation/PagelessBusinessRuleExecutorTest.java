package com.basis100.deal.validation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.security.PassiveMessage;

public class PagelessBusinessRuleExecutorTest extends FXDBTestCase  {
	

	IDataSet dataSetTest;
	PagelessBusinessRuleExecutor pagelessBusinessRuleExecutor=null;
	Deal deal=null;
	PassiveMessage passiveMessage=null;

	public PagelessBusinessRuleExecutorTest(String name) throws IOException,DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("PagelessBusinessRuleExecutorDataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		pagelessBusinessRuleExecutor = new PagelessBusinessRuleExecutor();
	}

	public void testValidateByRulePrefix() throws Exception{	
		ITable testGetResult = dataSetTest.getTable("testValidateByRulePrefix");
		int userId=Integer.parseInt(testGetResult.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testGetResult.getValue(0, "copyId").toString());
		String rulePrefix=testGetResult.getValue(0, "rulePrefix").toString();
		deal =new Deal(srk,CalcMonitor.getMonitor(srk),userId,copyId);
		passiveMessage = new PassiveMessage();
		passiveMessage.addMsg("billdesk",1);
		passiveMessage.addMsg("IBM",1);
		passiveMessage.addMsg("MSAT",1);		
		passiveMessage=pagelessBusinessRuleExecutor.validate(deal, srk, passiveMessage, rulePrefix);
		assertNotNull(passiveMessage);
	}	
	public void testValidateByRulePrefixFailure() throws Exception{		
			ITable testGetResult = dataSetTest.getTable("testValidateByRulePrefixFailure");
			try{
			int userId=Integer.parseInt(testGetResult.getValue(0, "id").toString());
			int copyId=Integer.parseInt(testGetResult.getValue(0, "copyId").toString());
			String rulePrefix=testGetResult.getValue(0, "rulePrefix").toString();
			deal =new Deal(srk,null,userId,copyId);
			passiveMessage=pagelessBusinessRuleExecutor.validate(null, srk, null, rulePrefix);
			assertNull(passiveMessage);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public void testValidateByRulePrefixLanguageId() throws Exception{		
		ITable testGetResult = dataSetTest.getTable("testValidateByRulePrefixLanguageId");
		int userId=Integer.parseInt(testGetResult.getValue(0, "id").toString());
		int copyId=Integer.parseInt(testGetResult.getValue(0, "copyId").toString());
		int languageId=Integer.parseInt(testGetResult.getValue(0, "languageId").toString());		
		String rulePrefix=testGetResult.getValue(0, "rulePrefix").toString();		
		deal =new Deal(srk,CalcMonitor.getMonitor(srk),userId,copyId);
		passiveMessage = new PassiveMessage();
		passiveMessage.addMsg("billdesk",1);
		passiveMessage.addMsg("IBM",1);
		passiveMessage.addMsg("MSAT",1);		
		passiveMessage=pagelessBusinessRuleExecutor.validate(deal, srk, passiveMessage, rulePrefix,languageId);
		assertNotNull(passiveMessage);
	}
	
	
	
}
