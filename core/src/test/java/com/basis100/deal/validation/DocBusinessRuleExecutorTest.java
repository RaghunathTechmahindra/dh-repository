package com.basis100.deal.validation;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.Deal;

public class DocBusinessRuleExecutorTest extends FXDBTestCase {

	IDataSet dataSetTest;
	DocBusinessRuleExecutor docBusinessRuleExecutor = null;
	Deal deal = null;

	public DocBusinessRuleExecutorTest(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"DocBusinessRuleExecutorDataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		docBusinessRuleExecutor = new DocBusinessRuleExecutor(srk);
	}

	public void testEvaluateRequest() throws Exception {
		ITable testGetResult = dataSetTest.getTable("testEvaluateRequest");
		int userId = Integer.parseInt(testGetResult.getValue(0, "id")
				.toString());
		int copyId = Integer.parseInt(testGetResult.getValue(0, "copyId")
				.toString());
		int docProfileNum = Integer.parseInt(testGetResult.getValue(0,
				"docProfileNum").toString());
		deal = new Deal(srk, null, userId, copyId);
		DocBusinessRuleResponse docBusinessRuleResponse = docBusinessRuleExecutor
				.evaluateRequest(deal, docProfileNum);
		assertNotNull(docBusinessRuleResponse);
		assert docBusinessRuleResponse.getDestinationType() != 0;

	}

	public void testEvaluateRequestFailure() throws Exception {
		boolean status = false;
		ITable testGetResult = dataSetTest
				.getTable("testEvaluateRequestFailure");
		int userId = Integer.parseInt(testGetResult.getValue(0, "id")
				.toString());
		int copyId = Integer.parseInt(testGetResult.getValue(0, "copyId")
				.toString());
		int docProfileNum = Integer.parseInt(testGetResult.getValue(0,
				"docProfileNum").toString());
		try {
			deal = new Deal(srk, null, userId, copyId);
			DocBusinessRuleResponse docBusinessRuleResponse = docBusinessRuleExecutor
					.evaluateRequest(deal, docProfileNum);
			status = docBusinessRuleResponse.isCreate();
		} catch (Exception e) {
			status = false;
		}
		assertEquals(false, status);
	}

	public void testEvaluateRequestByLang() throws Exception {
		ITable testGetResult = dataSetTest
				.getTable("testEvaluateRequestByLang");
		int userId = Integer.parseInt(testGetResult.getValue(0, "id")
				.toString());
		int copyId = Integer.parseInt(testGetResult.getValue(0, "copyId")
				.toString());
		int docProfileNum = Integer.parseInt(testGetResult.getValue(0,
				"docProfileNum").toString());
		int lang = Integer.parseInt(testGetResult.getValue(0, "lang")
				.toString());
		deal = new Deal(srk, null, userId, copyId);
		DocBusinessRuleResponse docBusinessRuleResponse = docBusinessRuleExecutor
				.evaluateRequest(deal, docProfileNum, lang);
		assertNotNull(docBusinessRuleResponse);
		assert docBusinessRuleResponse.getLanguage() != 0;
	}

	public void testEvaluateRequestByLangFailure() throws Exception {
		boolean status = false;
		try {
			ITable testGetResult = dataSetTest
					.getTable("testEvaluateRequestByLangFailure");
			int userId = Integer.parseInt(testGetResult.getValue(0, "id")
					.toString());
			int copyId = Integer.parseInt(testGetResult.getValue(0, "copyId")
					.toString());
			int docProfileNum = Integer.parseInt(testGetResult.getValue(0,
					"docProfileNum").toString());
			int lang = Integer.parseInt(testGetResult.getValue(0, "lang")
					.toString());
			deal = new Deal(srk, null, userId, copyId);
			DocBusinessRuleResponse docBusinessRuleResponse = docBusinessRuleExecutor
					.evaluateRequest(deal, docProfileNum, lang);
			status = docBusinessRuleResponse.isCreate();
		} catch (Exception e) {
			status = false;
		}
		assertEquals(false, status);
	}
}
