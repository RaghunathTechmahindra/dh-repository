package com.basis100.deal.validation;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.BREngine.BRInterpretor;
import com.basis100.BREngine.BusinessRule;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;


/**
 * <p>DealQueryTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class BCBusinessRuleExecutorTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private BCBusinessRuleExecutor bCBusinessRuleExecutor;
	private  BusinessRule rule=null;
	 BRInterpretor interpretor = null;
	 Statement queryStmt=null;
	// session resource kit
    //private SessionResourceKit _srk;

	public BCBusinessRuleExecutorTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(BCBusinessRuleExecutor.class.getSimpleName() + "DataSetTest.xml"));
	}
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.bCBusinessRuleExecutor = new BCBusinessRuleExecutor(srk);
		 //queryStmt = interpretor.getConnection().createStatement();
		
		
	}
	
	public void testExtract() throws Exception {
    	
		try{
		// get input data from xml repository
		ITable testExtract = dataSetTest.getTable("testExtract");			   	   
		String id=(String)testExtract.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		int workflowId=Integer.parseInt((String)testExtract.getValue(0,"WORKFLOWID"));
		int taskId=Integer.parseInt((String)testExtract.getValue(0,"TASKID"));
		String taskEvent=(String)testExtract.getValue(0,"TASKEVENT");
		String ruleName=(String)testExtract.getValue(0,"RULENAME");
		int instId=Integer.parseInt((String)testExtract.getValue(0,"INSTITUTIONPROFILEID"));
	    Deal dealObj=new Deal(srk,CalcMonitor.getMonitor(srk),dealId, copyId);	  
	    rule=new BusinessRule(workflowId,taskId,taskEvent,ruleName,instId);	
	    Collection out=bCBusinessRuleExecutor.extractConditions(dealObj);  
	    assert out.size()==0; 
    }catch(Exception e){
    	e.printStackTrace();
    }
	}
}