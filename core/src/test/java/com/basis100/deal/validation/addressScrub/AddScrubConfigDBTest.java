package com.basis100.deal.validation.addressScrub;

import java.io.IOException;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;

import com.basis100.FXDBTestCase;

public class AddScrubConfigDBTest extends FXDBTestCase {

	IDataSet dataSetTest;
	AddScrubConfig addScrubConfig = null;

	public AddScrubConfigDBTest(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"AddScrubConfigDataSetTest.xml"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		addScrubConfig = new AddScrubConfig(srk);
	}

	public void testFindByPrimaryKeys() throws Exception {
		ITable testFindByPrimaryKeys = dataSetTest
				.getTable("testFindByPrimaryKeys");
		srk.beginTransaction();
		int addressTypeId = Integer.parseInt(testFindByPrimaryKeys.getValue(0,
				"addressTypeId").toString());
		int addressSourceId = Integer.parseInt(testFindByPrimaryKeys.getValue(
				0, "addressSourceId").toString());
		int institutionId = Integer.parseInt(testFindByPrimaryKeys.getValue(0,
				"institutionId").toString());
		srk.getExpressState().setDealInstitutionId(institutionId);
		addScrubConfig = addScrubConfig.findByPrimaryKeys(addressTypeId,
				addressSourceId);
		assertNotNull(addScrubConfig);
		srk.rollbackTransaction();
	}

	public void testFindByPrimaryKeysFailure() throws Exception {
		ITable testCreate = dataSetTest
				.getTable("testFindByPrimaryKeysFailure");
		try {
			srk.beginTransaction();
			int addressTypeId = Integer.parseInt(testCreate.getValue(0,
					"addressTypeId").toString());
			int addressSourceId = Integer.parseInt(testCreate.getValue(0,
					"addressSourceId").toString());
			int institutionId = Integer.parseInt(testCreate.getValue(0,
					"institutionId").toString());
			srk.getExpressState().setDealInstitutionId(institutionId);
			addScrubConfig = addScrubConfig.findByPrimaryKeys(addressTypeId,
					addressSourceId);
			assertNotNull(addScrubConfig);
			srk.rollbackTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void testFindByAddressSourceId() throws Exception {
		Vector vList = null;
		ITable testFindByAddressSourceId = dataSetTest
				.getTable("testFindByAddressSourceId");
		srk.beginTransaction();
		int addressSourceId = Integer.parseInt(testFindByAddressSourceId
				.getValue(0, "addressSourceId").toString());
		int institutionId = Integer.parseInt(testFindByAddressSourceId
				.getValue(0, "institutionId").toString());
		srk.getExpressState().setDealInstitutionId(institutionId);
		vList = addScrubConfig.findByAddressSourceId(addressSourceId);
		assertNotNull(addScrubConfig);
		srk.rollbackTransaction();
	}

	public void testFindByAddressSourceIdFailure() throws Exception {
		Vector vList = null;
		ITable testFindByAddressSourceId = dataSetTest
				.getTable("testFindByAddressSourceIdFailure");
		try {
			srk.beginTransaction();
			int addressSourceId = Integer.parseInt(testFindByAddressSourceId
					.getValue(0, "addressSourceId").toString());
			int institutionId = Integer.parseInt(testFindByAddressSourceId
					.getValue(0, "institutionId").toString());
			srk.getExpressState().setDealInstitutionId(institutionId);
			vList = addScrubConfig.findByAddressSourceId(addressSourceId);
			assertNotNull(addScrubConfig);
			srk.rollbackTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void testFindByAddressTypeId() throws Exception {
		Vector vList = null;
		ITable testFindByAddressTypeId = dataSetTest
				.getTable("testFindByAddressTypeId");
		srk.beginTransaction();
		int addressTypeId = Integer.parseInt(testFindByAddressTypeId.getValue(
				0, "addressTypeId").toString());
		int institutionId = Integer.parseInt(testFindByAddressTypeId.getValue(
				0, "institutionId").toString());
		srk.getExpressState().setDealInstitutionId(institutionId);
		vList = addScrubConfig.findByAddressTypeId(addressTypeId);
		assertNotNull(addScrubConfig);
		srk.rollbackTransaction();
	}

	public void testFindByAddressTypeIdFailure() throws Exception {
		Vector vList = null;
		ITable testFindByAddressTypeId = dataSetTest
				.getTable("testFindByAddressTypeIdFailure");
		try {
			srk.beginTransaction();
			int addressTypeId = Integer.parseInt(testFindByAddressTypeId
					.getValue(0, "addressTypeId").toString());
			int institutionId = Integer.parseInt(testFindByAddressTypeId
					.getValue(0, "institutionId").toString());
			srk.getExpressState().setDealInstitutionId(institutionId);
			vList = addScrubConfig.findByAddressTypeId(addressTypeId);
			assertNotNull(addScrubConfig);
			srk.rollbackTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
