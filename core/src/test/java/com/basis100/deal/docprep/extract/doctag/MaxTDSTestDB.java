package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.entity.PricingProfile;
import com.basis100.resources.SessionResourceKit;

public class MaxTDSTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;
	private MaxTDS maxTDS;
	private Deal deal;
	private DealEntity entity;
	private DocumentProfile profile;
	private DocumentFormat format;

	public MaxTDSTestDB(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				MaxTDS.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.maxTDS = new MaxTDS();
	}


	public void testExtract() throws Exception {
		FMLQueryResult fml = new FMLQueryResult();
		SessionResourceKit srk= new SessionResourceKit();
		ITable testExtract = dataSetTest.getTable("testExtract");
		int dealId = Integer
				.valueOf((String) testExtract.getValue(0, "DEALID"));
		int copyId = Integer
				.valueOf((String) testExtract.getValue(0, "COPYID"));
		int documentTypeId = Integer.valueOf((String) testExtract.getValue(0,
				"DOCUMENTTYPEID"));
		int pricingrateinventoryid = Integer.valueOf((String) testExtract
				.getValue(0, "PRICINGRATEINVENTORYID"));
		int pricingProfile = Integer.valueOf((String) testExtract.getValue(0,
				"PRICINGPROFILEID"));
		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		profile = new DocumentProfile(srk);
		profile.findByDocumentTypeId(documentTypeId);
		format = new DocumentFormat(profile);
		srk.getExpressState().setDealInstitutionId(0);
		fml = maxTDS.extract(entity, documentTypeId, format, srk);
		assertNotNull(fml);
		assert fml.getEntities().size()>0;
			
	}

}
