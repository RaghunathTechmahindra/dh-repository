package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.entity.Income;

public class OtherIncomeSectionTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;
	private OtherIncomeSection otherIncomeSection;
	private DealEntity entity;
	private DocumentProfile profile;
	private DocumentFormat format;
	private FMLQueryResult fml;
	private Borrower bor;
	private Income income;

	public OtherIncomeSectionTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				OtherIncomeSection.class.getSimpleName() + "DataSetTest.xml"));
	}
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.otherIncomeSection = new OtherIncomeSection();
	}

	

	public void testExtract() throws Exception {
		fml = new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");
		int borrowerId = Integer.valueOf((String) testExtract.getValue(0,
				"BORROWERID"));
		int copyId = Integer
				.valueOf((String) testExtract.getValue(0, "COPYID"));
		int documentTypeId = Integer.valueOf((String) testExtract.getValue(0,
				"DOCUMENTTYPEID"));
		int incomeId = Integer.valueOf((String) testExtract.getValue(0,
				"INCOMEID"));
		bor = new Borrower(srk, null, borrowerId, copyId);
		income = new Income(srk, null, incomeId, copyId);
		entity = (DealEntity) bor;
		profile = new DocumentProfile(srk);
		profile.findByDocumentTypeId(documentTypeId);
		format = new DocumentFormat(profile);
		srk.getExpressState().setDealInstitutionId(1);
		fml = otherIncomeSection.extract(entity, documentTypeId, format, srk);
		if (fml != null) {
			assertEquals(incomeId, income.getIncomeId());
			assertNotNull(fml);
		}
	}
}
