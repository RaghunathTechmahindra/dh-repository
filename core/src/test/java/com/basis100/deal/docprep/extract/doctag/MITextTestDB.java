package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;

public class MITextTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;
	private MIText miText;
	private Deal deal;
	private DealEntity entity;
	private DocumentProfile profile;
	private DocumentFormat format;

	public MITextTestDB(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				MIText.class.getSimpleName() + "DataSetTest.xml"));
	}

	/*@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(
				MIText.class.getSimpleName() + "DataSet.xml"));
	}*/

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.miText = new MIText();
	}




	public void testExtract() throws Exception {
		FMLQueryResult fml = new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");
		int dealId = Integer
				.valueOf((String) testExtract.getValue(0, "DEALID"));
		int copyId = Integer
				.valueOf((String) testExtract.getValue(0, "COPYID"));
		int documentTypeId = Integer.valueOf((String) testExtract.getValue(0,
				"DOCUMENTTYPEID"));
		int miIndicatorId = Integer.valueOf((String) testExtract.getValue(0,
				"MIINDICATORID"));
		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;

		profile = new DocumentProfile(srk);
		profile.findByDocumentTypeId(documentTypeId);
		format = new DocumentFormat(profile);
		srk.getExpressState().setDealInstitutionId(1);
		fml = miText.extract(entity, documentTypeId, format, srk);
		System.out.println("indicator id--"+deal.getMIIndicatorId());
		if (fml != null) {
			assertEquals(miIndicatorId, deal.getMIIndicatorId());
			assertNotNull(fml);
		}
	}

}
