package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.entity.PricingProfile;

public class LoanNotesTestDB extends FXDBTestCase{

	private IDataSet dataSetTest;
	private LoanNotes loanNotes;	
	private Deal deal;
	private DealEntity entity;
	private DocumentProfile profile;
	private DocumentFormat format;
	 
	
	public LoanNotesTestDB(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(LoanNotes.class.getSimpleName() + "DataSetTest.xml"));
	}

/*	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(LoanNotes.class.getSimpleName() + "DataSet.xml"));
	}
	*/
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();		
		this.loanNotes = new LoanNotes();		
	}	
	
	

public void testExtract() throws Exception {	
		FMLQueryResult fml=new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");
		int dealId=Integer.valueOf((String)testExtract.getValue(0,"DEALID"));
		int copyId=Integer.valueOf((String)testExtract.getValue(0,"COPYID"));
		int documentTypeId=Integer.valueOf((String)testExtract.getValue(0,"DOCUMENTTYPEID"));		
		int pricingProfileId=Integer.valueOf((String)testExtract.getValue(0,"PRICINGPROFILEID"));
		deal=new Deal(srk,null,dealId, copyId);	    
	    entity=(DealEntity)deal;	    
	    profile = new DocumentProfile(srk);	    
	    profile.findByDocumentTypeId(documentTypeId);	    
	    format = new DocumentFormat(profile);	  	   
	    srk.getExpressState().setDealInstitutionId(1);
	    PricingProfile pp = new PricingProfile(srk);
	      pp = pp.findByPricingRateInventory(pricingProfileId );	    
	    fml=loanNotes.extract(entity,documentTypeId, format, srk);  
	    if(fml!=null){
	    assertEquals(pricingProfileId,deal.getPricingProfileId());
	    assertNotNull(fml);
	    }
    }
	
}
