package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.entity.Fee;

public class CommitmentFeeAmountTest extends FXDBTestCase
{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(CommitmentFeeAmountTest.class);
    
    //Initial data to be loaded before each test case.
	private final static String INIT_XML_DATA = "CommitmentFeeAmountDataSet.xml";
	private IDataSet dataSetTest;
	CommitmentFeeAmount commitmentFeeAmt;
	
	/**
     * Constructor function
     */
    public CommitmentFeeAmountTest(String name)  throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource("CommitmentFeeAmountDataSetTest.xml"));
    }
      
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
    	commitmentFeeAmt = new CommitmentFeeAmount();
    	return DatabaseOperation.UPDATE;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.NONE;
    }
    
    

    public void testExtract() throws Exception 
    {
    	_logger.info(" Start testExtract");
    	
    	// get input data
		ITable testExtract = dataSetTest.getTable("testExtract");
		int  feeId = Integer.parseInt((String)testExtract.getValue(0,"feeId"));
		int  dealFeeId = Integer.parseInt((String)testExtract.getValue(0,"dealFeeId"));
		int copyId=Integer.parseInt((String)testExtract.getValue(0,"copyId"));
		int lang=Integer.parseInt((String)testExtract.getValue(0,"lang"));
		int  dealFeeId1 = Integer.parseInt((String)testExtract.getValue(0,"dealFeeId1"));
		int copyId1=Integer.parseInt((String)testExtract.getValue(0,"copyId1"));
		int  dealFeeId2 = Integer.parseInt((String)testExtract.getValue(0,"dealFeeId2"));
		int copyId2=Integer.parseInt((String)testExtract.getValue(0,"copyId2"));
		
		
		Fee fee = new Fee(srk, null, feeId);
	    DealEntity entity=(DealEntity)fee;
	    
	    DocumentProfile profile = new DocumentProfile(srk);
	    profile.setDocumentFormat("rtf");
	    
	    DocumentFormat format = new DocumentFormat(profile);
	    
	    FMLQueryResult fml=commitmentFeeAmt.extract(entity, lang, format, srk);  
	    assertNotNull(fml);
	    assert fml.getValues() != null;
	    assert fml.getValues().size() > 0;
	    
	    DealFee dealFee = new DealFee(srk, null, dealFeeId, copyId);
	    DealEntity entity1=(DealEntity)dealFee;
	    FMLQueryResult fml1=commitmentFeeAmt.extract(entity1, lang, format, srk);  
	    assertNotNull(fml1);
	    assert fml1.getValues() != null;
	    assert fml1.getValues().size() > 0;
	    
	    DealFee dealFee1 = new DealFee(srk, null, dealFeeId1, copyId1);
	    DealEntity entity2=(DealEntity)dealFee1;
	    FMLQueryResult fml2=commitmentFeeAmt.extract(entity2, lang, format, srk);  
	    assertNotNull(fml2);
	    assert fml2.getValues() != null;
	    assert fml2.getValues().size() > 0;
	    
	    DealFee dealFee2 = new DealFee(srk, null, dealFeeId2, copyId2);
	    DealEntity entity3=(DealEntity)dealFee2;
	    FMLQueryResult fml3=commitmentFeeAmt.extract(entity3, lang, format, srk);  
	    assertNotNull(fml3);
	    assert fml3.getValues() != null;
	    assert fml3.getValues().size() > 0;
	    
	    _logger.info(" End testExtract");
    }

}
