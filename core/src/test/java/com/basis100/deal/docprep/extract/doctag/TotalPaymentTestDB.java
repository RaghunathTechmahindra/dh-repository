package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;

/**
 * <p>
 * BranchCurrentTimeTest
 * </p>
 * Express Entity class unit test: ServiceProvider
 */
public class TotalPaymentTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;
	private TotalPayment totalPayment;
	private Deal deal;
	private DealEntity entity;
	private DocumentProfile profile;
	private DocumentFormat format;
	private FMLQueryResult fml;

	public TotalPaymentTestDB(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				TotalPayment.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.totalPayment = new TotalPayment();
	}

	public void testExtract() throws Exception {
		// get input data from xml repository
		fml = new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");
		int dealId = Integer.parseInt((String) testExtract
				.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String) testExtract
				.getValue(0, "COPYID"));
		String documentFormat = (String) testExtract.getValue(0,
				"documentFormat");
		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		srk.getExpressState().setDealInstitutionId(1);
		profile = new DocumentProfile(srk);
		profile.setDocumentFormat(documentFormat);
		format = new DocumentFormat(profile);
		fml = totalPayment.extract(entity, 1, format, srk);
		if (fml != null) {
			assertEquals(dealId, deal.getDealId());
		}
	}
}