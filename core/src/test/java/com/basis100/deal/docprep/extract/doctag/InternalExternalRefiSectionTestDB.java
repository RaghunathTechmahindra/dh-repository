package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;

public class InternalExternalRefiSectionTestDB extends FXDBTestCase{

	private IDataSet dataSetTest;
	private InternalExternalRefiSection refiSection;
	private DocumentProfile profile;
	private DocumentFormat format;
	private DealEntity entity;
	private Deal deal;
	
	public InternalExternalRefiSectionTestDB(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(InternalExternalRefiSection.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(InternalExternalRefiSection.class.getSimpleName() + "DataSet.xml"));
	}
	
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();		
		this.refiSection = new InternalExternalRefiSection();	
	}	
	

	
public void testExtract() throws Exception {		
		FMLQueryResult fml=new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");
		int dealId=Integer.valueOf((String)testExtract.getValue(0,"DEALID"));
		int copyId=Integer.valueOf((String)testExtract.getValue(0,"COPYID"));
		int documentTypeId=Integer.valueOf((String)testExtract.getValue(0,"DOCUMENTTYPEID"));
		int dealPurposeId=Integer.valueOf((String)testExtract.getValue(0,"DEALPURPOSEID"));		
		deal=new Deal(srk,null,dealId, copyId);	    
	    entity=(DealEntity)deal;	   
	    profile = new DocumentProfile(srk);	    
	    profile.findByDocumentTypeId(documentTypeId);	    
	    format = new DocumentFormat(profile);	   
	    fml=refiSection.extract(entity,documentTypeId, format, srk);  
	    if(fml!=null){
	    assertEquals(dealPurposeId,deal.getDealPurposeId());
	    assertNotNull(fml);
	    }
    }
	
}
