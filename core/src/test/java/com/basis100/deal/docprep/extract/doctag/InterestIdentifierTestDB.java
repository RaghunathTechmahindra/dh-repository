package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.entity.LenderProfile;

public class InterestIdentifierTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;
	private InterestIdentifier interestIdentifier;
	private DocumentProfile profile;
	private DocumentFormat format;
	private DealEntity entity;
	private Deal deal;

	public InterestIdentifierTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				InterestIdentifier.class.getSimpleName() + "DataSetTest.xml"));
	}
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.interestIdentifier = new InterestIdentifier();
	}

	

	public void testExtract() throws Exception {
		FMLQueryResult fml = new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");
		int dealId = Integer
				.valueOf((String) testExtract.getValue(0, "DEALID"));
		int copyId = Integer
				.valueOf((String) testExtract.getValue(0, "COPYID"));
		int documentTypeId = Integer.valueOf((String) testExtract.getValue(0,
				"DOCUMENTTYPEID"));
		int lenderProfileId = Integer.valueOf((String) testExtract.getValue(0,
				"LENDERPROFILEID"));
		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		profile = new DocumentProfile(srk);
		profile.findByDocumentTypeId(documentTypeId);
		format = new DocumentFormat(profile);
		fml = interestIdentifier.extract(entity, documentTypeId, format, srk);
		if (fml != null) {
			assertEquals(lenderProfileId, deal.getLenderProfileId());
			assertNotNull(fml);
		}
	}

}
