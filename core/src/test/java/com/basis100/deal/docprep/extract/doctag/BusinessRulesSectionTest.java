package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;

public class BusinessRulesSectionTest extends FXDBTestCase
{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(BusinessRulesSectionTest.class);
    
    //Initial data to be loaded before each test case.
	private final static String INIT_XML_DATA = "BusinessRulesSectionDataSet.xml";
	private IDataSet dataSetTest;
	private BusinessRulesSection businessRulesSection;
	
	/**
     * Constructor function
     */
    public BusinessRulesSectionTest(String name)  throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource("BusinessRulesSectionDataSetTest.xml"));
    }
      
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
    	businessRulesSection = new BusinessRulesSection();
    	return DatabaseOperation.INSERT;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.DELETE;
    }
    

    public void testExtract() throws Exception 
    {
    	_logger.info(" Start testExtract");
    	
    	// get input data
		ITable testExtract = dataSetTest.getTable("testExtract");
		int  dealId = Integer.parseInt((String)testExtract.getValue(0,"dealId"));
		int copyId=Integer.parseInt((String)testExtract.getValue(0,"copyId"));
		int lang=Integer.parseInt((String)testExtract.getValue(0,"lang"));
		
		
		Deal deal=new Deal(srk,null,dealId, copyId);	    
	    DealEntity entity=(DealEntity)deal;
	    
	    deal.setFunderProfileId(1); 
	    DocumentProfile profile = new DocumentProfile(srk);
	    profile.setDocumentFormat("rtf");
	    
	    DocumentFormat format = new DocumentFormat(profile);
	    
	    FMLQueryResult fml=businessRulesSection.extract(entity, lang, format, srk);  
	    assertNotNull(fml);
	    assert fml.getEntities() != null;
	    assert fml.getEntities().size() > 0;
	    
	    _logger.info(" End testExtract");
    }

}
