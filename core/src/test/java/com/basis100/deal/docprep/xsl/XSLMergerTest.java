package com.basis100.deal.docprep.xsl;

import java.util.Vector;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.pk.DealPK;
import com.basis100.log.SysLogger;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

public class XSLMergerTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    int dealNumber;
    int instituionNumber;
    int docTypeId;
    int copyId;
    Deal deal;
    DocumentProfile documentProfile;
    SessionResourceKit _srk;
    
    public XSLMergerTest() {
        
    }
    
    @Before
    public void setUp() throws Exception {
        
        ResourceManager.init();
        _srk = new SessionResourceKit();
        instituionNumber = _dataRepository.getInt("DEAL",
                "INSTITUTIONPROFILEID", 0);
        _srk.getExpressState().setDealInstitutionId(instituionNumber);
        dealNumber = _dataRepository.getInt("DEAL", "DEALID", 0);
        copyId = _dataRepository.getInt("DEAL", "COPYID", 0);
        
        docTypeId =  _dataRepository.getInt("DOCUMENTPROFILE", "DOCUMENTTYPEID", 0);
        documentProfile = new DocumentProfile(_srk)
                .findByDocumentTypeId(docTypeId);
        
        deal = new Deal(_srk, CalcMonitor.getMonitor(_srk))
                .findByPrimaryKey(new DealPK(dealNumber, copyId));
    }
    
    /**
     * test the merger.
     */
    @Ignore
    public void testMerger() throws Exception {
        SysLogger logger = new SysLogger(this, "oc_uw_2");
        XSLMerger merger = new XSLMerger(deal, _srk, documentProfile);
        String mXmlString = null;
        
        XSLDataFactory xslDataFactory = new XSLDataFactory(deal, documentProfile, _srk);
        mXmlString = xslDataFactory.ExtractDataForXsl();
        
        Vector out = merger.getPDFDocuments(mXmlString);
        
        assertTrue(out.size() > 0);
    }    
}
