package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.entity.Liability;

public class CBLiabilitiesSectionTest extends FXDBTestCase
{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(CBLiabilitiesSectionTest.class);
    
    //Initial data to be loaded before each test case.
	private final static String INIT_XML_DATA = "CBLiabilitiesSectionDataSet.xml";
	private IDataSet dataSetTest;
	private CBLiabilitiesSection cbLiabilitiesSection;
	
	/**
     * Constructor function
     */
    public CBLiabilitiesSectionTest(String name)  throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource("CBLiabilitiesSectionDataSetTest.xml"));
    }
      
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
    	cbLiabilitiesSection = new CBLiabilitiesSection();
    	return DatabaseOperation.INSERT;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.DELETE;
    }

    
    public void testExtract() throws Exception 
    {
    	_logger.info(" Start testExtract");
    	
    	// get input data
		ITable testExtract = dataSetTest.getTable("testExtract");
		int  dealId = Integer.parseInt((String)testExtract.getValue(0,"dealId"));
		int  borrowerId = Integer.parseInt((String)testExtract.getValue(0,"borrowerId"));
		int copyId=Integer.parseInt((String)testExtract.getValue(0,"copyId"));
		int lang=Integer.parseInt((String)testExtract.getValue(0,"lang"));
		
	    DocumentProfile profile = new DocumentProfile(srk);
	    profile.setDocumentFormat("rtf");
	    
	    DocumentFormat format = new DocumentFormat(profile);
	    
	    
	    Deal deal=new Deal(srk,null,dealId, copyId);	    
	    DealEntity entity=(DealEntity)deal;
	    
	    FMLQueryResult fml=cbLiabilitiesSection.extract(entity, lang, format, srk);  
	    assertNotNull(fml);
	    assert fml.getEntities() != null;
	    assert fml.getEntities().size() > 0;
	    
	    
	    Borrower borrower = new Borrower(srk, null, borrowerId, copyId);
	    DealEntity entity1=(DealEntity)borrower;
	    
	    FMLQueryResult fml1=cbLiabilitiesSection.extract(entity1, lang, format, srk);  
	    assertNotNull(fml1);
	    assert fml1.getEntities() != null;
	    assert fml1.getEntities().size() > 0;
	    
	    Liability liability = new Liability(srk, null);
	    DealEntity entity2=(DealEntity)liability;
	    
	    FMLQueryResult fml2=cbLiabilitiesSection.extract(entity2, lang, format, srk);  
	    assertNotNull(fml2);
	    assert fml2.getEntities() != null;
	    assert fml2.getEntities().size() == 0;
	    
	    
	    _logger.info(" End testExtract");
    }


}
