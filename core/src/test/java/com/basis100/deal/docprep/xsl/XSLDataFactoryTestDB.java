package com.basis100.deal.docprep.xsl;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;

/**
 * <p>
 * BranchCurrentTimeTest
 * </p>
 * Express Entity class unit test: ServiceProvider
 */
public class XSLDataFactoryTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;
	private XSLDataFactory dataFactory;
	private Deal deal;
	private DealEntity entity;
	private DocumentProfile profile;
	private DocumentFormat format;

	public XSLDataFactoryTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				XSLDataFactory.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();

	}

	public void testExtract() throws Exception {
		// get input data from xml repository
		String str = null;
		ITable testExtract = dataSetTest.getTable("testExtract");
		int dealId = Integer.parseInt((String) testExtract
				.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String) testExtract
				.getValue(0, "COPYID"));
		int documentTypeId = Integer.parseInt((String) testExtract.getValue(0,
				"DOCUMENTTYPEID"));
		int languagePreferenceId = Integer.parseInt((String) testExtract
				.getValue(0, "LANGUAGEPREFERENCEID"));
		int lenderProfileId = Integer.parseInt((String) testExtract.getValue(0,
				"LENDERPROFILEID"));
		String documentFormat = (String) testExtract.getValue(0,
				"documentFormat");

		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		profile = new DocumentProfile(srk);
		profile.setDocumentTypeId(documentTypeId);
		profile.setDocumentFormat(documentFormat);
		profile.setDocumentVersion("GenX1.0");
		profile.setDocumentSchemaPath("com.basis100.deal.docprep.extract.data.GENXCommitmentExtractor");
		format = new DocumentFormat(profile);
		dataFactory = new XSLDataFactory(deal, profile, srk);
		srk.getExpressState().setDealInstitutionId(1);
		str = dataFactory.ExtractDataForXsl();
		if (str != null) {
			assertEquals(lenderProfileId, deal.getLenderProfileId());
		}

	}
}