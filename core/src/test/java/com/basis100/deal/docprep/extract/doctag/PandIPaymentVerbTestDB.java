package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;

public class PandIPaymentVerbTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;
	private PandIPaymentVerb pandIPaymentVerb;
	private DealEntity entity;
	private DocumentProfile profile;
	private DocumentFormat format;
	private FMLQueryResult fml;
	private Deal deal;
	private Condition cond;

	public PandIPaymentVerbTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				PandIPaymentVerb.class.getSimpleName() + "DataSetTest.xml"));
	}
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.pandIPaymentVerb = new PandIPaymentVerb();
	}
	
	

	public void testExtract() throws Exception {
		fml = new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");
		int dealId = Integer
				.valueOf((String) testExtract.getValue(0, "DEALID"));
		int copyId = Integer
				.valueOf((String) testExtract.getValue(0, "COPYID"));
		int documentTypeId = Integer.valueOf((String) testExtract.getValue(0,
				"DOCUMENTTYPEID"));
		int conditionTypeId = Integer.valueOf((String) testExtract.getValue(0,
				"CONDITIONTYPEID"));
		int conditionId = Integer.valueOf((String) testExtract.getValue(0,
				"CONDITIONID"));
		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		cond = new Condition(srk, conditionId);
		profile = new DocumentProfile(srk);
		profile.findByDocumentTypeId(documentTypeId);
		format = new DocumentFormat(profile);
		srk.getExpressState().setDealInstitutionId(1);
		fml = pandIPaymentVerb.extract(entity, documentTypeId, format, srk);
		if (fml != null) {
			assert fml.getEntities().size()>0;
			assertNotNull(fml);
		}
	}
}
