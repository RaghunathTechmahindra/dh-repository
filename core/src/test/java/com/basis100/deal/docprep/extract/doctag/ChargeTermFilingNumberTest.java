package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;

public class ChargeTermFilingNumberTest extends FXDBTestCase
{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(ChargeTermFilingNumberTest.class);
    
    //Initial data to be loaded before each test case.
	private final static String INIT_XML_DATA = "ChargeTermFilingNumberDataSet.xml";
	private IDataSet dataSetTest;
	ChargeTermFilingNumber termFilingNum;
	
	/**
     * Constructor function
     */
    public ChargeTermFilingNumberTest(String name)  throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource("ChargeTermFilingNumberDataSetTest.xml"));
    }
    
     
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
    	termFilingNum = new ChargeTermFilingNumber();
    	return DatabaseOperation.INSERT;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.DELETE;
    }
    
    

    public void testExtract() throws Exception 
    {
    	_logger.info(" Start testExtract");
    	
    	// get input data
		ITable testExtract = dataSetTest.getTable("testExtract");
		int  dealId = Integer.parseInt((String)testExtract.getValue(0,"dealId"));
		int copyId=Integer.parseInt((String)testExtract.getValue(0,"copyId"));
		int lang=Integer.parseInt((String)testExtract.getValue(0,"lang"));
		int  dealId1 = Integer.parseInt((String)testExtract.getValue(0,"dealId1"));
		int copyId1=Integer.parseInt((String)testExtract.getValue(0,"copyId1"));
		int  dealId2 = Integer.parseInt((String)testExtract.getValue(0,"dealId2"));
		int copyId2=Integer.parseInt((String)testExtract.getValue(0,"copyId2"));
		int  dealId3 = Integer.parseInt((String)testExtract.getValue(0,"dealId3"));
		int copyId3=Integer.parseInt((String)testExtract.getValue(0,"copyId3"));
		int  dealId4 = Integer.parseInt((String)testExtract.getValue(0,"dealId4"));
		int copyId4=Integer.parseInt((String)testExtract.getValue(0,"copyId4"));
		
		// Province Ontario
		Deal deal = new Deal(srk, null, dealId, copyId);
	    DealEntity entity=(DealEntity)deal;
	    
	    DocumentProfile profile = new DocumentProfile(srk);
	    profile.setDocumentFormat("rtf");
	    
	    DocumentFormat format = new DocumentFormat(profile);
	    
	    FMLQueryResult fml=termFilingNum.extract(entity, lang, format, srk);  
	    assertNotNull(fml);
	    assert fml.getValues() != null;
	    assert fml.getValues().size() > 0;
	    
	    // Province British Columbia
	    Deal deal1 = new Deal(srk, null, dealId1, copyId1);
	    DealEntity entity1=(DealEntity)deal1;
	    
	    FMLQueryResult fml1=termFilingNum.extract(entity1, lang, format, srk);  
	    assertNotNull(fml1);
	    assert fml1.getValues() != null;
	    assert fml1.getValues().size() > 0;
	    
	    // Province Alberta
	    Deal deal2 = new Deal(srk, null, dealId2, copyId2);
	    DealEntity entity2=(DealEntity)deal2;
	    
	    FMLQueryResult fml2=termFilingNum.extract(entity2, lang, format, srk);  
	    assertNotNull(fml2);
	    assert fml2.getValues() != null;
	    assert fml2.getValues().size() > 0;
	    
	    
	    // Province Manitoba
	    Deal deal3 = new Deal(srk, null, dealId3, copyId3);
	    DealEntity entity3=(DealEntity)deal3;
	    
	    FMLQueryResult fml3=termFilingNum.extract(entity3, lang, format, srk);  
	    assertNotNull(fml3);
	    assert fml3.getValues() != null;
	    assert fml3.getValues().size() > 0;
	    
	    // Province New Brunswick
	    Deal deal4 = new Deal(srk, null, dealId4, copyId4);
	    DealEntity entity4=(DealEntity)deal4;
	    
	    FMLQueryResult fml4=termFilingNum.extract(entity4, lang, format, srk);  
	    assertNotNull(fml4);
	    assert fml4.getValues() != null;
	    assert fml4.getValues().size() > 0;
	    _logger.info(" End testExtract");
    }

}
