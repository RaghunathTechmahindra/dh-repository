package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.entity.Liability;

public class LiabilityVerbTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;
	private LiabilityVerb liabilityVerb;
	private Deal deal;
	private DealEntity entity;
	private DocumentProfile profile;
	private DocumentFormat format;
	private Liability liability;

	public LiabilityVerbTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				LiabilityVerb.class.getSimpleName() + "DataSetTest.xml"));
	}

	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.liabilityVerb = new LiabilityVerb();

	}

	

	public void testExtract() throws Exception {
		FMLQueryResult fml = new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");
		int dealId = Integer
				.valueOf((String) testExtract.getValue(0, "DEALID"));
		int copyId = Integer
				.valueOf((String) testExtract.getValue(0, "COPYID"));
		int documentTypeId = Integer.valueOf((String) testExtract.getValue(0,
				"DOCUMENTTYPEID"));
		int liabilityId = Integer.valueOf((String) testExtract.getValue(0,
				"LIABILITYID"));
		int liabilityPayOffTypeId = Integer.valueOf((String) testExtract
				.getValue(0, "LIABILITYPAYOFFTYPEID"));
		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		profile = new DocumentProfile(srk);
		profile.findByDocumentTypeId(documentTypeId);
		format = new DocumentFormat(profile);
		liability = new Liability(srk, null, liabilityId, copyId);
		entity = (DealEntity) liability;
		srk.getExpressState().setDealInstitutionId(2);
		fml = liabilityVerb.extract(entity, documentTypeId, format, srk);
		if (fml != null) {
			assertEquals(liabilityPayOffTypeId,
					liability.getLiabilityPayOffTypeId());
			assertNotNull(fml);
		}
	}

}
