package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;

public class LenderNameTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;
	private LenderName lenderName;
	private Deal deal;
	Borrower primary;

	public LenderNameTestDB(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				LenderName.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(
				LenderName.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.lenderName = new LenderName();

	}

	

	public void testExtract() throws Exception {
		FMLQueryResult fml = new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");
		int dealId = Integer
				.valueOf((String) testExtract.getValue(0, "DEALID"));
		int copyId = Integer
				.valueOf((String) testExtract.getValue(0, "COPYID"));
		int documentTypeId = Integer.valueOf((String) testExtract.getValue(0,
				"DOCUMENTTYPEID"));
		int lenderProfileId = Integer.valueOf((String) testExtract.getValue(0,
				"LENDERPROFILEID"));
		deal = new Deal(srk, null, dealId, copyId);
		DealEntity entity = (DealEntity) deal;
		DocumentProfile profile = new DocumentProfile(srk);
		profile.findByDocumentTypeId(documentTypeId);
		DocumentFormat format = new DocumentFormat(profile);		
		srk.getExpressState().setDealInstitutionId(2);
		fml = lenderName.extract(entity, documentTypeId, format, srk);
		if (fml != null) {
			assertEquals(lenderProfileId, deal.getLenderProfileId());
			assertNotNull(fml);
		}
	}

}
