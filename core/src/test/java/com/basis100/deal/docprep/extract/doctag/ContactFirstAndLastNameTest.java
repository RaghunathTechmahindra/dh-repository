package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;


/**
 * <p>ContactFirstAndLastNameTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class ContactFirstAndLastNameTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private ContactFirstAndLastName contactFirstAndLastName;

	public ContactFirstAndLastNameTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ContactFirstAndLastName.class.getSimpleName() + "DataSetTest.xml"));
	}

	

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.contactFirstAndLastName = new ContactFirstAndLastName();
	}
	

	public void testExtract() throws Exception {
    	
		// get input data from xml repository
		FMLQueryResult fml=new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");			   	   
		String id=(String)testExtract.getValue(0,"CONTACTID");
		int contactId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);		
	    Contact contact=new Contact(srk,contactId, copyId);	    
	    DealEntity entity=contact;
	    DocumentProfile profile = new DocumentProfile(srk);
	    profile.findByDocumentTypeId(1);
	    DocumentFormat format = new DocumentFormat(profile);
	    fml=contactFirstAndLastName.extract(entity,1, format, srk);  
	    assertNotNull(fml);
    }
	
}