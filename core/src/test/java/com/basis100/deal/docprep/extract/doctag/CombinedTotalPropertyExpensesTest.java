package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;

public class CombinedTotalPropertyExpensesTest extends FXDBTestCase
{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(CombinedTotalPropertyExpensesTest.class);
    
    //Initial data to be loaded before each test case.
	private final static String INIT_XML_DATA = "CombinedTotalPropertyExpensesDataSet.xml";
	private IDataSet dataSetTest;
	private CombinedTotalPropertyExpenses combinedTotalPropertyExpenses;
	
	/**
     * Constructor function
     */
    public CombinedTotalPropertyExpensesTest(String name)  throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource("CombinedTotalPropertyExpensesDataSetTest.xml"));
    }
      
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
    	combinedTotalPropertyExpenses = new CombinedTotalPropertyExpenses();
    	return DatabaseOperation.INSERT;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.DELETE;
    }
    
    public void testExtract() throws Exception 
    {
    	_logger.info(" Start testExtract");
    	
    	// get input data
		ITable testExtract = dataSetTest.getTable("testExtract");
		int  dealId = Integer.parseInt((String)testExtract.getValue(0,"dealId"));
		int copyId=Integer.parseInt((String)testExtract.getValue(0,"copyId"));
		int lang=Integer.parseInt((String)testExtract.getValue(0,"lang"));
		
	    DocumentProfile profile = new DocumentProfile(srk);
	    profile.setDocumentFormat("rtf");
	    
	    DocumentFormat format = new DocumentFormat(profile);
	    
	    Deal deal=new Deal(srk,null,dealId, copyId);	    
	    DealEntity entity=(DealEntity)deal;
	    
	    FMLQueryResult fml=combinedTotalPropertyExpenses.extract(entity, lang, format, srk);  
	    assertNotNull(fml);
	    assert fml.getValues() != null;
	    assert fml.getValues().size() > 0;
	    
	    _logger.info(" End testExtract");
    }


}
