package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.entity.Liability;

public class MortgageLiabilityAmountTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;
	private MortgageLiabilityAmount mLiabilityAmount;
	private DealEntity entity;
	private DocumentProfile profile;
	private DocumentFormat format;
	private Liability liability;

	public MortgageLiabilityAmountTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				MortgageLiabilityAmount.class.getSimpleName()
						+ "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.mLiabilityAmount = new MortgageLiabilityAmount();
	}
	


	public void testExtract() throws Exception {
		FMLQueryResult fml = new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");
		int copyId = Integer
				.valueOf((String) testExtract.getValue(0, "COPYID"));
		int documentTypeId = Integer.valueOf((String) testExtract.getValue(0,
				"DOCUMENTTYPEID"));
		int liabilityPayOffTypeId = Integer.valueOf((String) testExtract
				.getValue(0, "LIABILITYPAYOFFTYPEID"));
		int liabilityId = Integer.valueOf((String) testExtract.getValue(0,
				"LIABILITYID"));
		liability = new Liability(srk, null, liabilityId, copyId);
		entity = (DealEntity) liability;
		profile = new DocumentProfile(srk);
		profile.findByDocumentTypeId(documentTypeId);
		format = new DocumentFormat(profile);
		srk.getExpressState().setDealInstitutionId(1);
		fml = mLiabilityAmount.extract(entity, documentTypeId, format, srk);
		if (fml != null) {
			assertEquals(liabilityPayOffTypeId,
					liability.getLiabilityPayOffTypeId());
			assertNotNull(fml);
		}
	}
}
