package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.entity.Property;

public class LoanToValueTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;
	private LoanToValue loanToValue;
	private DealEntity entity;
	private DocumentProfile profile;
	private DocumentFormat format;
	private Property property;

	public LoanToValueTestDB(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				LoanToValue.class.getSimpleName() + "DataSetTest.xml"));
	}


	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.loanToValue = new LoanToValue();
	}
	


	public void testExtract() throws Exception {
		FMLQueryResult fml = new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");
		int propertyId = Integer.valueOf((String) testExtract.getValue(0,
				"PROPERTYID"));
		int copyId = Integer
				.valueOf((String) testExtract.getValue(0, "COPYID"));
		int documentTypeId = Integer.valueOf((String) testExtract.getValue(0,
				"DOCUMENTTYPEID"));
		double loanValue = Double.valueOf((String) testExtract.getValue(0,
				"LOANTOVALUE"));
		property = new Property(srk, null, propertyId, copyId);
		entity = (DealEntity) property;
		profile = new DocumentProfile(srk);
		profile.findByDocumentTypeId(documentTypeId);
		format = new DocumentFormat(profile);
		srk.getExpressState().setDealInstitutionId(1);
		fml = loanToValue.extract(entity, documentTypeId, format, srk);
		if (fml != null) {
			assertEquals(loanValue, property.getLoanToValue());
			assertNotNull(fml);
		}
	}

}
