package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.entity.Property;

public class NewConstructionTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;
	private NewConstruction newConstruction;
	private DealEntity entity;
	private DocumentProfile profile;
	private DocumentFormat format;
	private FMLQueryResult fml;
	private Property property;

	public NewConstructionTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				NewConstruction.class.getSimpleName() + "DataSetTest.xml"));
	}
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.newConstruction = new NewConstruction();
	}

	

	public void testExtract() throws Exception {
		fml = new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");
		int propertyId = Integer.valueOf((String) testExtract.getValue(0,
				"PROPERTYID"));
		int copyId = Integer
				.valueOf((String) testExtract.getValue(0, "COPYID"));
		int documentTypeId = Integer.valueOf((String) testExtract.getValue(0,
				"DOCUMENTTYPEID"));
		int newConstructionId = Integer.valueOf((String) testExtract.getValue(
				0, "NEWCONSTRUCTIONID"));
		property = new Property(srk, null, propertyId, copyId);
		entity = (DealEntity) property;
		profile = new DocumentProfile(srk);
		profile.findByDocumentTypeId(documentTypeId);
		format = new DocumentFormat(profile);
		srk.getExpressState().setDealInstitutionId(1);
		fml = newConstruction.extract(entity, documentTypeId, format, srk);
		if (fml != null) {
			assert fml.getEntities().size()>0;
			assertNotNull(fml);
		}
	}
}
