package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;


/**
 * <p>EmployerPhoneTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class EmployerPhoneTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private EmployerPhone employerPhone;

	public EmployerPhoneTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(EmployerPhone.class.getSimpleName() + "DataSetTest.xml"));
	}
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.employerPhone = new EmployerPhone();
	}
	
	

	public void testExtract() throws Exception {
    	
		// get input data from xml repository
		FMLQueryResult fml=new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");			   	   
		String id=(String)testExtract.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);		
	    Deal deal=new Deal(srk,null,dealId, copyId);	    
	    DealEntity entity=(DealEntity)deal;
	    deal.setFunderProfileId(1); 
	    DocumentProfile profile = new DocumentProfile(srk);
	    profile.findByDocumentTypeId(1);
	    //profile.findByPrimaryKey(new DocumentProfilePK() );
	    DocumentFormat format = new DocumentFormat(profile);
	    fml=employerPhone.extract(entity,1, format, srk);  
	    assert fml.getEntities().size()>0;
    }
	
}