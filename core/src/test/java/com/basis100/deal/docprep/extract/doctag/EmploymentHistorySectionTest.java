package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;


/**
 * <p>EmploymentHistorySectionTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class EmploymentHistorySectionTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private EmploymentHistorySection employmentHistorySection;

	public EmploymentHistorySectionTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(EmploymentHistorySection.class.getSimpleName() + "DataSetTest.xml"));
	}

	/*@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(EmploymentHistorySection.class.getSimpleName() + "DataSet.xml"));
	}*/

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.employmentHistorySection = new EmploymentHistorySection();
	}
	

	
	public void testExtract() throws Exception {
    	
		// get input data from xml repository
		FMLQueryResult fml=new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");			   	   
		String id=(String)testExtract.getValue(0,"BORROWERID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);		
	    Borrower borrower=new Borrower(srk,null,dealId, copyId);
	    DealEntity entity=borrower;
	    DocumentProfile profile = new DocumentProfile(srk);
	    profile.findByDocumentTypeId(1);
	    //profile.findByPrimaryKey(new DocumentProfilePK() );
	    DocumentFormat format = new DocumentFormat(profile);
	    fml=employmentHistorySection.extract(entity,1, format, srk);  
	    assertNotNull(fml);
    }
	
}