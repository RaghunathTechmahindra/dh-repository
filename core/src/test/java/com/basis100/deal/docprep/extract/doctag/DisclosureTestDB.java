package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.pk.AppraisalOrderPK;
import com.basis100.deal.pk.ArchivedLoanApplicationPK;
import com.basis100.deal.pk.ArchivedLoanDecisionPK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PricingProfilePK;
import com.basis100.deal.pk.PricingRateInventoryPK;
import com.basis100.deal.pk.PrimeIndexRateInventoryPK;
import com.basis100.deal.pk.PrimeIndexRateProfilePK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.deal.pk.SourceOfBusinessProfilePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.util.EntityTestUtil;

public class DisclosureTestDB extends FXDBTestCase{

	private IDataSet dataSetTest;
	private Disclosure disclosure;	
	private Deal deal;
	public DisclosureTestDB(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(Disclosure.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(Disclosure.class.getSimpleName() + "DataSet.xml"));
	}
	
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	
	@Override
	protected void setUp() throws Exception {
		super.setUp();		
		this.disclosure = new Disclosure();
	}	
	
	

   public void testExtract() throws Exception {		
		FMLQueryResult fml=new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");
		int dealId=Integer.valueOf((String)testExtract.getValue(0,"DEALID"));
		int copyId=Integer.valueOf((String)testExtract.getValue(0,"COPYID"));
		int documentTypeId=Integer.valueOf((String)testExtract.getValue(0,"DOCUMENTTYPEID"));
	    Deal deal=new Deal(srk,null,dealId, copyId);	    
	    DealEntity entity=(DealEntity)deal;
	    deal.setFunderProfileId(documentTypeId); 
	    DocumentProfile profile = new DocumentProfile(srk);
	    profile.findByDocumentTypeId(documentTypeId);	    
	    DocumentFormat format = new DocumentFormat(profile);
	    fml=disclosure.extract(entity,documentTypeId, format, srk);  
	    assertNotNull(fml);
    }
	
}
