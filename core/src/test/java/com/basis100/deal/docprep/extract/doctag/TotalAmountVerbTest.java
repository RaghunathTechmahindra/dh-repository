package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;

/**
 * <p>
 * BranchCurrentTimeTest
 * </p>
 * Express Entity class unit test: ServiceProvider
 */
public class TotalAmountVerbTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private TotalAmountVerb totalAmountVerb;
	private DealEntity entity;
	private DocumentProfile profile;
	private DocumentFormat format;
	private Deal deal;

	public TotalAmountVerbTest(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				TotalAmountVerb.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.totalAmountVerb = new TotalAmountVerb();
	}
	


	public void testExtract() throws Exception {

		// get input data from xml repository
		FMLQueryResult fml = new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");
		int dealId = Integer.parseInt((String) testExtract
				.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String) testExtract
				.getValue(0, "COPYID"));
		int miIndicatorId = Integer.parseInt((String) testExtract.getValue(0,
				"MIINDICATORID"));
		String documentFormat = (String) testExtract.getValue(0,
				"documentFormat");
		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		srk.getExpressState().setDealInstitutionId(1);
		profile = new DocumentProfile(srk);
		profile.setDocumentFormat(documentFormat);
		format = new DocumentFormat(profile);
		fml = totalAmountVerb.extract(entity, 1, format, srk);
		if (fml != null) {
			assertEquals(miIndicatorId, deal.getMIIndicatorId());
		}
	}
}