package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;

/**
 * <p>
 * BorrowerAddressSectionTest
 * </p>
 * Express Entity class unit test: ServiceProvider
 */
public class BorrowerAddressSectionTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private BorrowerAddressSection borrowerAddressSection;
	private DealEntity entity;
	private DocumentProfile profile;
	private DocumentFormat format;
	private Borrower bor;

	public BorrowerAddressSectionTest(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				BorrowerAddressSection.class.getSimpleName()
						+ "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.borrowerAddressSection = new BorrowerAddressSection();
	}

	

	public void testExtract() throws Exception {

		// get input data from xml repository
		FMLQueryResult fml = new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");
		int borrowerId = Integer.parseInt((String) testExtract.getValue(0,
				"BORROWERID"));
		int copyId = Integer.parseInt((String) testExtract
				.getValue(0, "COPYID"));
		bor = new Borrower(srk, null, borrowerId, copyId);
		entity = (DealEntity) bor;
		profile = new DocumentProfile(srk);
		profile.findByDocumentTypeId(1);
		format = new DocumentFormat(profile);
		fml = borrowerAddressSection.extract(entity, 1, format, srk);
		if(fml!=null){
		assertEquals(borrowerId, bor.getBorrowerId());		
		}
	}

}