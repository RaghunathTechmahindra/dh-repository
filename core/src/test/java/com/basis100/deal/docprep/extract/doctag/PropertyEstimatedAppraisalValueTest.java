package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;


/**
 * <p>PropertyEstimatedAppraisalValueTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class PropertyEstimatedAppraisalValueTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private PropertyEstimatedAppraisalValue propertyEstimatedAppraisalValue;

	public PropertyEstimatedAppraisalValueTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(PropertyEstimatedAppraisalValue.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(PropertyEstimatedAppraisalValue.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.propertyEstimatedAppraisalValue = new PropertyEstimatedAppraisalValue();
	}
	

	
	public void testExtract() throws Exception {
    	
		// get input data from xml repository
		FMLQueryResult fml=new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");			   	   
		String id=(String)testExtract.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);		
	    Deal deal=new Deal(srk,null,dealId, copyId);	    
	    DealEntity entity=(DealEntity)deal;
	    deal.setFunderProfileId(1); 
	    DocumentProfile profile = new DocumentProfile(srk);
	    profile.findByDocumentTypeId(1);
	    DocumentFormat format = new DocumentFormat(profile);
	    fml=propertyEstimatedAppraisalValue.extract(entity,1, format, srk);  
	    assertNotNull(fml);
    }
	
}