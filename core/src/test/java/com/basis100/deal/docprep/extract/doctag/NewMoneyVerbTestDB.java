package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;

public class NewMoneyVerbTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;
	private NewMoneyVerb newMoneyVerb;
	private Deal deal;
	private DealEntity entity;
	private DocumentProfile profile;
	private DocumentFormat format;
	private FMLQueryResult fml;

	public NewMoneyVerbTestDB(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				NewMoneyVerb.class.getSimpleName() + "DataSetTest.xml"));
	}
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.newMoneyVerb = new NewMoneyVerb();
	}


	public void testExtract() throws Exception {
		fml = new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");
		int dealId = Integer
				.valueOf((String) testExtract.getValue(0, "DEALID"));
		int copyId = Integer
				.valueOf((String) testExtract.getValue(0, "COPYID"));
		int documentTypeId = Integer.valueOf((String) testExtract.getValue(0,
				"DOCUMENTTYPEID"));
		int conditionId = Integer.valueOf((String) testExtract.getValue(0,
				"CONDITIONID"));
		deal = new Deal(srk, null, dealId, copyId);
		entity = (DealEntity) deal;
		profile = new DocumentProfile(srk);
		Condition cond = new Condition(srk, conditionId);
		profile.findByDocumentTypeId(documentTypeId);
		format = new DocumentFormat(profile);
		srk.getExpressState().setDealInstitutionId(1);
		fml = newMoneyVerb.extract(entity, documentTypeId, format, srk);
		if (fml != null) {
			assertEquals(conditionId, cond.getConditionId());
			assertNotNull(fml);
		}
	}
}
