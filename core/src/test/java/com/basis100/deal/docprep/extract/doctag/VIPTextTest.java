package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;

public class VIPTextTest extends FXDBTestCase
{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(VIPTextTest.class);
    
    //Initial data to be loaded before each test case.
	private final static String INIT_XML_DATA = "VIPTextDataSet.xml";
	private IDataSet dataSetTest;
	private VIPText vipText;
	
	/**
     * Constructor function
     */
    public VIPTextTest(String name)  throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource("VIPTextDataSetTest.xml"));
    }
    
 
    
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
    	vipText = new VIPText();
    	return DatabaseOperation.UPDATE;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.NONE;
    }
    
    

    public void testExtract() throws Exception 
    {
    	_logger.info(" Start testExtract");
    	
    	// get input data
		ITable testExtract = dataSetTest.getTable("testExtract");
		int  dealId = Integer.parseInt((String)testExtract.getValue(0,"dealId"));
		int copyId=Integer.parseInt((String)testExtract.getValue(0,"copyId"));
		int lang=Integer.parseInt((String)testExtract.getValue(0,"lang"));
		int  dealId1 = Integer.parseInt((String)testExtract.getValue(0,"dealId1"));
		int copyId1=Integer.parseInt((String)testExtract.getValue(0,"copyId1"));
		
		
		Deal deal=new Deal(srk,null,dealId, copyId);	    
	    DealEntity entity=(DealEntity)deal;
	    
	    DocumentProfile profile = new DocumentProfile(srk);
	    profile.setDocumentFormat("rtf");
	    
	    DocumentFormat format = new DocumentFormat(profile);
	    
	    FMLQueryResult fml=vipText.extract(entity, lang, format, srk);  
	    assertNotNull(fml);
	    assert fml.getValues() != null;
	    assert fml.getValues().size() > 0;
	    
	    Deal deal1=new Deal(srk,null,dealId1, copyId1);	    
	    DealEntity entity1=(DealEntity)deal1;
	    
	    FMLQueryResult fml1=vipText.extract(entity1, lang, format, srk);  
	    assertNotNull(fml1);
	    assert fml1.getValues() != null;
	    assert fml1.getValues().size() == 0;
	    
	    _logger.info(" End testExtract");
    }

}
