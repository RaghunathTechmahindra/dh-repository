package com.basis100.deal.docprep.extract.doctag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.DocumentFormat;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentProfile;
import com.basis100.deal.entity.EmploymentHistory;

public class MIIndicatorMonthsTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;
	private MIIndicatorMonths miIndicatorMonths;
	private DealEntity entity;
	private DocumentProfile profile;
	private DocumentFormat format;

	public MIIndicatorMonthsTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				MIIndicatorMonths.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.miIndicatorMonths = new MIIndicatorMonths();
	}


	public void testExtract() throws Exception {
		FMLQueryResult fml = new FMLQueryResult();
		ITable testExtract = dataSetTest.getTable("testExtract");
		int employmentHistoryId = Integer.valueOf((String) testExtract
				.getValue(0, "EMPLOYMENTHISTORYID"));
		int copyId = Integer
				.valueOf((String) testExtract.getValue(0, "COPYID"));
		int documentTypeId = Integer.valueOf((String) testExtract.getValue(0,
				"DOCUMENTTYPEID"));
		EmploymentHistory eh = new EmploymentHistory(srk, employmentHistoryId,
				copyId);
		entity = (DealEntity) eh;
		profile = new DocumentProfile(srk);
		profile.findByDocumentTypeId(documentTypeId);
		format = new DocumentFormat(profile);
		srk.getExpressState().setDealInstitutionId(1);
		fml = miIndicatorMonths.extract(entity, documentTypeId, format, srk);
		if (fml != null) {
			assertEquals(employmentHistoryId, eh.getEmploymentHistoryId());
			assertNotNull(fml);
		}
	}

}
