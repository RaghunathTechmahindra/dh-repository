package com.basis100.deal.aag;

import java.io.IOException;
import java.util.*;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;

import MosSystem.Mc;
import MosSystem.Sc;
import com.filogix.express.legacy.mosapp.ISessionStateModel;
import mosApp.MosSystem.PageEntry;
//import mosApp.MosSystem.SessionStateModelImpl;

import com.basis100.FXDBTestCase;
import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.jdbcservices.*;
import com.basis100.jdbcservices.jdbcexecutor.*;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.workflow.*;
import com.basis100.workflow.entity.*;
import com.basis100.workflow.pk.*;
import com.basis100.util.thread.*;
import com.basis100.deal.validation.*;
import com.basis100.deal.security.*;
import com.basis100.picklist.BXResources;

public class AAGuidelineTest extends FXDBTestCase {

	IDataSet dataSetTest;
	AAGuideline aaGuideline;
	private String INIT_XML_DATA = "AAGuidelineDataSet.xml";

	public AAGuidelineTest(String name) throws DataSetException, IOException {
		super(name);

		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"AAGuidelineDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		aaGuideline = new AAGuideline(srk);

	}

	public void testpagelessQualifyApproval() throws Exception {

		int userId = Integer.parseInt(dataSetTest
				.getTable("testpagelessQualifyApproval").getValue(0, "id")
				.toString());
		int copyId = Integer.parseInt(dataSetTest
				.getTable("testpagelessQualifyApproval").getValue(0, "copyId")
				.toString());
		int dealInstitutionId = Integer.parseInt(dataSetTest
				.getTable("testpagelessQualifyApproval")
				.getValue(0, "dealInstitutionId").toString());
		boolean isDealAPPOrRec = false;
		Deal deal = new Deal(srk, CalcMonitor.getMonitor(srk), userId, copyId);
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		int dealApproval = aaGuideline.pagelessQualifyApproval(srk, deal);		
		assertNotNull(dealApproval);		

		/*
		 * deal.setTotalLoanAmount(1000000); deal.setLineOfBusinessId(1);
		 * UserProfile appProfile = aaGuideline.getApproverProfile(0,
		 * srk.getExpressState().getDealInstitutionId()); boolean
		 * sufficientLOBAuthority; boolean aagRulesMessages;
		 * 
		 * boolean recommended = true; UserProfileQuery upq = new
		 * UserProfileQuery(); UserProfile nextAppProfile = null; double
		 * totalLoanAmount = deal.getTotalLoanAmount(); int lineOfBusinessId =
		 * deal.getLineOfBusinessId(); sufficientLOBAuthority =true;
		 * PassiveMessage pm = new PassiveMessage(); BusinessRuleExecutor brExec
		 * = new BusinessRuleExecutor();
		 * 
		 * pm = brExec.BREValidator((DealPK)deal.getPk(), srk, pm,
		 * Mc.AAG_RULE_PREFIX + "-%");
		 * 
		 * aagRulesMessages = pm.getNumMessages() > 0;
		 * 
		 * if (appProfile == null || deal.getJointApproverId() != 0){
		 * 
		 * assertEquals(-1, dealApproval); recommended=false;
		 * 
		 * }
		 * 
		 * if (sufficientLOBAuthority == true && aagRulesMessages == false) { if
		 * (null != brExec.BREValidator((DealPK)deal.getPk(), srk, null,
		 * Mc.AGP_RULE_PREFIX + "-%")) { assertEquals(10, dealApproval); }
		 * assertEquals(11, dealApproval); recommended=false; }
		 * 
		 * if (deal.higherApproverAllowed() &&
		 * appProfile.getSecondApproverUserId() > 0) {
		 * 
		 * 
		 * nextAppProfile = upq.get2ndApproverUserProfile(appProfile
		 * .getSecondApproverUserId(), deal.getInstitutionProfileId(), true,
		 * true, srk);
		 * 
		 * if (nextAppProfile != null && nextAppProfile.getProfileStatusId() ==
		 * Sc.PROFILE_STATUS_ACTIVE) {
		 * 
		 * assertEquals(8, dealApproval); recommended=false;
		 * 
		 * } } if (nextAppProfile != null &&
		 * nextAppProfile.getJointApproverUserId() > 0) {
		 * 
		 * nextAppProfile = upq.getJointApproverUserProfile(nextAppProfile
		 * .getJointApproverUserId(), deal.getInstitutionProfileId(), true,
		 * true, srk);
		 * 
		 * if (nextAppProfile != null && nextAppProfile.getProfileStatusId() ==
		 * Sc.PROFILE_STATUS_ACTIVE) {
		 * 
		 * assertEquals(8, dealApproval); recommended=false; } }
		 * if(recommended){ assertEquals(8, dealApproval); }
		 */

	}

	/*
	 * public void testpagelessQualifyApprovalFailure() throws Exception {
	 * 
	 * int userId = Integer.parseInt(dataSetTest
	 * .getTable("testpagelessQualifyApprovalFailure") .getValue(0,
	 * "id").toString()); int copyId = Integer.parseInt(dataSetTest
	 * .getTable("testpagelessQualifyApprovalFailure") .getValue(0,
	 * "copyId").toString()); int dealInstitutionId =
	 * Integer.parseInt(dataSetTest
	 * .getTable("testpagelessQualifyApprovalFailure") .getValue(0,
	 * "dealInstitutionId").toString()); boolean isDealAPPOrRec = false; Deal
	 * deal = new Deal(srk, CalcMonitor.getMonitor(srk), userId, copyId);
	 * //srk.getExpressState().setDealInstitutionId(dealInstitutionId); int
	 * dealApproval = aaGuideline.pagelessQualifyApproval(srk, null);
	 * System.out.println("DealApproval is....." + dealApproval);
	 * assertNotNull(dealApproval);
	 * 
	 * assertEquals(-1, dealApproval);
	 * 
	 * }
	 */
}
