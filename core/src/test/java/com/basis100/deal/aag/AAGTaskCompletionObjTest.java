package com.basis100.deal.aag;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import MosSystem.Sc;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.CreditReference;
import com.basis100.deal.entity.Deal;
import com.basis100.workflow.DealUserProfiles;
import com.basis100.workflow.entity.AssignedTask;

public class AAGTaskCompletionObjTest extends FXDBTestCase {
	AAGTaskCompletionObj aagTaskCompletionObj;
	private String INIT_XML_DATA = "AAGTaskCompletionObjDataSet.xml";
	IDataSet dataSetTest;

	public AAGTaskCompletionObjTest(String name) throws DataSetException,
			IOException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"AAGTaskCompletionObjDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		

	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	public void testgetTaskCompletionCode() throws Exception {
		int dealInstitutionId = Integer.parseInt(dataSetTest
				.getTable("getTaskCompletionCode")
				.getValue(0, "dealInstitutionId").toString());
		int dealId = Integer.parseInt(dataSetTest
				.getTable("getTaskCompletionCode").getValue(0, "dealId")
				.toString());
		String copyType = dataSetTest.getTable("getTaskCompletionCode")
				.getValue(0, "copyType").toString();
		int underwriterUserId = Integer.parseInt(dataSetTest
				.getTable("getTaskCompletionCode")
				.getValue(0, "underwriterUserId").toString());
		int secondApproverId = Integer.parseInt(dataSetTest
				.getTable("getTaskCompletionCode")
				.getValue(0, "secondApproverId").toString());
		int jointApproverId = Integer.parseInt(dataSetTest
				.getTable("getTaskCompletionCode")
				.getValue(0, "jointApproverId").toString());
		int nextStatus = Integer.parseInt(dataSetTest
				.getTable("getTaskCompletionCode").getValue(0, "nextStatus")
				.toString());
		int nextApprover = Integer.parseInt(dataSetTest
				.getTable("getTaskCompletionCode").getValue(0, "nextApprover")
				.toString());
		
		aagTaskCompletionObj = new AAGTaskCompletionObj(nextStatus,
				nextApprover);
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		AssignedTask aTask = new AssignedTask(srk);
		Deal deal = new Deal();
		deal.setDealId(dealId);
		deal.setCopyType(copyType);
		deal.setUnderwriterUserId(underwriterUserId);
		deal.setSecondApproverId(secondApproverId);
		deal.setJointApproverId(jointApproverId);

		DealUserProfiles dealUserProfiles = new DealUserProfiles(deal, srk);
		int code = aagTaskCompletionObj.getTaskCompletionCode(srk, aTask, deal,
				dealUserProfiles);
		
			assertEquals(5, code);
		

	}
	public void testgetTaskCompletionCodeFailure() throws Exception {
		int dealInstitutionId = Integer.parseInt(dataSetTest
				.getTable("getTaskCompletionCodeFailure")
				.getValue(0, "dealInstitutionId").toString());
		int dealId = Integer.parseInt(dataSetTest
				.getTable("getTaskCompletionCodeFailure").getValue(0, "dealId")
				.toString());
		String copyType = dataSetTest.getTable("getTaskCompletionCodeFailure")
				.getValue(0, "copyType").toString();
		int underwriterUserId = Integer.parseInt(dataSetTest
				.getTable("getTaskCompletionCodeFailure")
				.getValue(0, "underwriterUserId").toString());
		int secondApproverId = Integer.parseInt(dataSetTest
				.getTable("getTaskCompletionCodeFailure")
				.getValue(0, "secondApproverId").toString());
		int jointApproverId = Integer.parseInt(dataSetTest
				.getTable("getTaskCompletionCodeFailure")
				.getValue(0, "jointApproverId").toString());
		int nextStatus = Integer.parseInt(dataSetTest
				.getTable("getTaskCompletionCodeFailure").getValue(0, "nextStatus")
				.toString());
		int nextApprover = Integer.parseInt(dataSetTest
				.getTable("getTaskCompletionCodeFailure").getValue(0, "nextApprover")
				.toString());
		
		aagTaskCompletionObj = new AAGTaskCompletionObj(nextStatus,
				nextApprover);

		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		AssignedTask aTask = new AssignedTask(srk);
		Deal deal = new Deal();
		deal.setDealId(dealId);
		deal.setCopyType(copyType);
		deal.setUnderwriterUserId(underwriterUserId);
		deal.setSecondApproverId(secondApproverId);
		deal.setJointApproverId(jointApproverId);

		DealUserProfiles dealUserProfiles = new DealUserProfiles(deal, srk);
		int code = aagTaskCompletionObj.getTaskCompletionCode(srk, aTask, deal,
				dealUserProfiles);
		
			assertEquals(0, code);
		

	}
}
