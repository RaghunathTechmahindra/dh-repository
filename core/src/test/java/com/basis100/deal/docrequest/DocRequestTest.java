/**
 * <p>@(#)DocRequestTest.java Oct 4, 2007</p>
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.basis100.deal.docrequest;

import java.io.File;
import java.util.Date;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.docprep.Dc;
import com.basis100.deal.docprep.DocPrepLanguage;
import com.basis100.deal.docprep.xsl.XSLMerger;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentQueue;
import com.basis100.deal.pk.DealPK;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.core.ExpressCoreTestContext;
import com.filogix.express.test.UnitTestDataRepository;
import com.filogix.express.test.UnitTestLogging;

import config.Config;

public class DocRequestTest extends Assert 
    implements UnitTestLogging {
    
    // the unit test data repository.
    protected static UnitTestDataRepository _dataRepository;

    // The logger
    private final static Logger _logger = 
        LoggerFactory.getLogger(DocRequestTest.class);
    
    public static final String ENTITY = "DOCUMENTQUEUE";
    public static final String ENTITY_DEAL = "DEAL";
    public static final String ENTITY_DOCUMENTPROFILE = "DOCUMENTPROFILE";
    public static final String ENTITY_MESSAGE = "MESSAGE";
    
    public static final int DEAL_DATA_COUNT = 1;
    public static final int DOCPROF_DATA_COUNT = 20;

    private SessionResourceKit srk = null;
    
    protected int documentQueueId;
    protected Date timeRequested;
    protected int requestorUserId;
    protected String documentFullName;
    protected String emailAddress;
    protected String emailFullName;
    protected String emailSubject;
    protected int languagePreferenceId;
    protected int lenderProfileId;
    protected int documentTypeId;
    protected String documentFormat;
    protected String documentVersion;
    protected int dealId;
    protected int dealCopyId;
    protected String scenarioNumber;
    protected int requestStatusId;
    protected int documentStatus;
    protected String emailText;
    protected String faxNumbers;
    protected int instProfileId;

    protected int deal_dealId;
    protected int deal_copyId;
    protected int deal_scenarioNumber;
    protected int deal_instituionProfileId;
    protected int deal_unserWriterUserId;
    
    protected String  documnetProfile_documentFullName;
    protected String  documnetProfile_documentDescription;
    protected String  documnetProfile_documentVersion ;
    protected String  documnetProfile_documentTemplatePath;
    protected String  documnetProfile_documentSchemaPath;
    protected int     documnetProfile_languagePreferenceId;
    protected int     documnetProfile_lenderProfileId;
    protected int     documnetProfile_documentTypeId;
    protected String  documnetProfile_documentFormat;
    protected int  documnetProfile_documentFormatId;
    protected int     documnetProfile_documentProfileStatus;
    protected String  documnetProfile_replyFlag;
    protected String  documnetProfile_digestKey;
    protected int     documnetProfile_mailDestinationTypeId;
    protected int     documnetProfile_instProfileId;

    protected int     message_id;
    
    public DocRequestTest() {
        
    }

    /**
     * setting up the testing env.
     */
    @BeforeClass
    public static void beforeClass() throws Exception {

        _dataRepository = ExpressCoreTestContext.getUnitTestDataRepository();
        // init ResouceManager
        ResourceManager.init();
        String path = Config.getProperty("SYS_PROPERTIES_LOCATION");
        path +=  "mossys.properties";

        PropertiesCache.addPropertiesFile(path);
        PropertiesCache resources = PropertiesCache.getInstance();
        
        File locate = new File("admin"); 

        String rootDir = locate.getAbsolutePath(); 

            if(rootDir.endsWith("/") || rootDir.endsWith("\\"))  
                   path = rootDir + path;  
             else  
         
               path = rootDir + "/"+ path;
            
        String initLocation = resources.getProperty(-1, "path");

        
    }

    /**
     * <p>setUp</p>
     * Setting up test case.
     */
    @Before
    public void setUp() throws Exception {
      
        _logger.info(BORDER_START, "DocRequestTest setting up ...");
        
        Double indexD = new Double(Math.random() * DEAL_DATA_COUNT);
        int testSeqForGen = indexD.intValue();
        
        //      get test Deal data from repository
        deal_dealId 
            = _dataRepository.getInt(ENTITY_DEAL,  "DEALID", testSeqForGen);
        deal_copyId 
            = _dataRepository.getInt(ENTITY_DEAL,  "COPYID", testSeqForGen);
        deal_scenarioNumber
            = _dataRepository.getInt(ENTITY_DEAL,  "SCENARIONUMBER", testSeqForGen);
        deal_instituionProfileId
            = _dataRepository.getInt(ENTITY_DEAL,  "INSTITUTIONPROFILEID", testSeqForGen);
        deal_unserWriterUserId
            = _dataRepository.getInt(ENTITY_DEAL,  "UNDERWRITERUSERID", testSeqForGen);
      
        indexD = new Double(Math.random() * DOCPROF_DATA_COUNT);
        testSeqForGen = indexD.intValue();
//        testSeqForGen = 11;
        
        // any DocumentProfile type should be ok now for test
        documnetProfile_documentTypeId 
            = _dataRepository.getInt(ENTITY_DOCUMENTPROFILE,  "DOCUMENTTYPEID", 0);
        
//        System.out.println("rowNum: " + testSeqForGen);
//        System.out.println("DocumentTypeId: " + documnetProfile_documentTypeId);
        
        documnetProfile_documentFullName
            = _dataRepository.getString(ENTITY_DOCUMENTPROFILE,  "DOCUMENTFULLNAME", 0);
        documnetProfile_documentFormat
            = _dataRepository.getString(ENTITY_DOCUMENTPROFILE,  "DOCUMENTFORMAT", 0);
        if ("RFT".equalsIgnoreCase(documnetProfile_documentFormat)) {
            documnetProfile_documentFormatId = XSLMerger.TYPE_RTF;
        }
        else if ("PDF".equalsIgnoreCase(documnetProfile_documentFormat)) {
            documnetProfile_documentFormatId = XSLMerger.TYPE_PDF;            
        }
        else if ("HTML".equalsIgnoreCase(documnetProfile_documentFormat)) {
            documnetProfile_documentFormatId = XSLMerger.TYPE_HTML;            
        }
        else if ("CSV".equalsIgnoreCase(documnetProfile_documentFormat)) {
            documnetProfile_documentFormatId = XSLMerger.TYPE_CSV;            
        }
        else {
            documnetProfile_documentFormatId = XSLMerger.TYPE_RTF;            
        }
        
        documnetProfile_documentVersion
            = _dataRepository.getString(ENTITY_DOCUMENTPROFILE,  "DOCUMENTVERSION", 0);

        message_id = _dataRepository.getInt(ENTITY_MESSAGE, "MESSAGEID", 0);

        //  init session resource kit.
        srk = new SessionResourceKit("" + deal_unserWriterUserId);
        srk.getExpressState().setDealInstitutionId(deal_instituionProfileId);
        _logger.info(BORDER_END, "Setting up Done");
    }
    
    @After
    public void tearDown() {

      //      free resources
      srk.freeResources( );

    }
  
    /**
     * test requestDocByBusRules.
     */
    /*@Ignore
    public void requestDocByBusRules() throws Exception
    {
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey( new DealPK(deal_dealId, deal_copyId));
        
        DocumentRequest request 
            = DocumentRequest.requestDocByBusRules(srk, deal, 
                                                   documnetProfile_documentTypeId, 
                                                   "" + deal_scenarioNumber);
        DocumentQueue document = request.getRequest();
        assertEquals(document.getDealId(), deal_dealId);
        assertEquals(document.getDocumentTypeId(), documnetProfile_documentTypeId);
        assertEquals(document.getDocumentFormat(), documnetProfile_documentFormat);
    } 

    *//**
     * test requestDocByBusRules.
     *//*
    @Ignore
    public void requestDocByBusRuleslang() throws Exception {
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey( new DealPK(deal_dealId, deal_copyId));
        int lang = DocPrepLanguage.ENGLISH;
        
        int message_Id
            = _dataRepository.getInt("MESSAGE",  "MESSAGEID", 0);
        int message_languageId
            = _dataRepository.getInt("MESSAGE",  "LANGUAGEPREFERENCEID", 0);

        DocumentRequest request 
            = DocumentRequest.requestDocByBusRuleslang(srk, deal, 
                                                   documnetProfile_documentTypeId, 
                                                   "" + deal_scenarioNumber, message_Id);
        DocumentQueue document = request.getRequest();
        assertEquals(document.getDealId(), deal_dealId);
        assertEquals(document.getDocumentTypeId(), documnetProfile_documentTypeId);
        assertEquals(document.getDocumentFormat(), documnetProfile_documentFormat);
    } 

    *//**
     * test requestDisbursmentLetter.
     *//*
    @Test
    public void requestDisbursmentLetter() throws Exception
    {
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey( new DealPK(deal_dealId, deal_copyId));
        
        DocumentRequest request 
            = DocumentRequest.requestDisbursmentLetter(srk, deal);
        DocumentQueue document = request.getRequest();
        assertEquals(document.getDealId(), deal_dealId);
    }
    
    *//**
     * test requestDisbursmentLetter.
     *//*
    @Test
    public void requestConditionsOutstanding() throws Exception
    {
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey( new DealPK(deal_dealId, deal_copyId));
        
        DocumentRequest request 
            = DocumentRequest.requestConditionsOutstanding(srk, deal, 
                                                           documnetProfile_documentFormatId, 
                                                           message_id);
        DocumentQueue document = request.getRequest();
        assertEquals(document.getDealId(), deal_dealId);
    }
    *//**
     * test requestCommitment.
     *//*
    @Test
    public void requestCommitment() throws Exception
    {
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey( new DealPK(deal_dealId, deal_copyId));
        
        DocumentRequest request 
            = DocumentRequest.requestCommitment(srk, deal, documnetProfile_documentFormatId);
        DocumentQueue document = request.getRequest();
        assertEquals(document.getDealId(), deal_dealId);
    }
    
    *//**
     * test requestSolicitorsPackage.
     *//*
    @Test
    public void requestSolicitorsPackage() throws Exception
    {
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey( new DealPK(deal_dealId, deal_copyId));
        
        DocumentRequest request 
            = DocumentRequest.requestSolicitorsPackage(srk, deal, 
                                                       documnetProfile_documentFormatId);
        DocumentQueue document = request.getRequest();
        assertEquals(document.getDealId(), deal_dealId);
    }
    
    *//**
     * test requestNBCSolicitorsPackage.
     *//*
    @Ignore
    public void requestNBCSolicitorsPackage() throws Exception
    {
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey( new DealPK(deal_dealId, deal_copyId));
        
        DocumentRequest request 
            = DocumentRequest.requestSolicitorsPackage(srk, deal, 
                                                       documnetProfile_documentTypeId);
        DocumentQueue document = request.getRequest();
        assertEquals(document.getDealId(), deal_dealId);
    }
    
    *//**
     * test requestDealSummary.
     *//*
    @Test
    public void requestDealSummary() throws Exception
    {
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey( new DealPK(deal_dealId, deal_copyId));
        
        int message_Id
            = _dataRepository.getInt("MESSAGE",  "MESSAGEID", 0);

        DocumentRequest request 
            = DocumentRequest.requestDealSummary(srk, deal, 
                                                 documnetProfile_documentTypeId, 
                                                 documnetProfile_documentFormatId, 
                                                 message_Id);
        DocumentQueue document = request.getRequest();
        assertEquals(document.getDealId(), deal_dealId);
    }
    
    *//**
     * test requestDealSummaryLite.
     *//*
    @Test
    public void requestDealSummaryLite() throws Exception
    {
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey( new DealPK(deal_dealId, deal_copyId));
        
        int message_Id
            = _dataRepository.getInt("MESSAGE",  "MESSAGEID", 0);

        DocumentRequest request 
            = DocumentRequest.requestDealSummaryLite(srk, deal);
        DocumentQueue document = request.getRequest();
        assertEquals(document.getDealId(), deal_dealId);
    }
    
    *//**
     * test requestWireTransfer.
     *//*
    @Test
    public void requestWireTransfer() throws Exception
    {
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey( new DealPK(deal_dealId, deal_copyId));
        
        int message_Id
            = _dataRepository.getInt("MESSAGE",  "MESSAGEID", 0);

        DocumentRequest request 
            = DocumentRequest.requestWireTransfer(srk, deal);
        DocumentQueue document = request.getRequest();
        assertEquals(document.getDealId(), deal_dealId);
    }
    
    *//**
     * test requestDataEntry.
     *//*
    @Test
    public void requestDataEntry() throws Exception
    {
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey( new DealPK(deal_dealId, deal_copyId));
        
        int message_Id
            = _dataRepository.getInt("MESSAGE",  "MESSAGEID", 0);

        DocumentRequest request 
            = DocumentRequest.requestDataEntry(srk, deal);
        DocumentQueue document = request.getRequest();
        assertEquals(document.getDealId(), deal_dealId);
    }
    
    *//**
     * test requestDealSummaryPackage.
     *//*
    @Test
    public void requestDealSummaryPackage() throws Exception
    {
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey( new DealPK(deal_dealId, deal_copyId));
        
        int message_Id
            = _dataRepository.getInt("MESSAGE",  "MESSAGEID", 0);

        DocumentRequest request 
            = DocumentRequest.requestDealSummaryPackage(srk, deal);
        DocumentQueue document = request.getRequest();
        assertEquals(document.getDealId(), deal_dealId);
    }
        
    *//**
     * test requestTransferPackage.
     * this method is used by only DJ
     * PartyProfileAssoc doesnot have required data
     *//*
    @Ignore
    public void requestTransferPackage() throws Exception
    {
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey( new DealPK(deal_dealId, deal_copyId));
        
        int message_Id
            = _dataRepository.getInt("MESSAGE",  "MESSAGEID", 0);

        DocumentRequest request 
            = DocumentRequest.requestTransferPackage(srk, deal);
        DocumentQueue document = request.getRequest();
        assertEquals(document.getDealId(), deal_dealId);
    }
    
    *//**
     * test requestBridgePackage.
     * this method is used by only DJ
     * PartyProfileAssoc doesnot have required data
     *//*
    @Ignore
    public void requestBridgePackage() throws Exception
    {
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey( new DealPK(deal_dealId, deal_copyId));
        
        DocumentRequest request 
            = DocumentRequest.requestBridgePackage(srk, deal);
        DocumentQueue document = request.getRequest();
        assertEquals(document.getDealId(), deal_dealId);
    }
    
    *//**
     * test requestPreAppCertificate.
     *//*
    @Test
    public void requestPreAppCertificate() throws Exception
    {
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey( new DealPK(deal_dealId, deal_copyId));
        
        int message_Id
            = _dataRepository.getInt("MESSAGE",  "MESSAGEID", 0);

        DocumentRequest request 
            = DocumentRequest.requestPreAppCertificate(srk, deal);
        DocumentQueue document = request.getRequest();
        assertEquals(document.getDealId(), deal_dealId);
    }
    */
    /**
     * test requestMortgageInsurance with DOCUMENT_GENERAL_TYPE_MI_REQUIRED
     */
    @Test
    public void requestMortgageInsurance1() throws Exception
    {
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey( new DealPK(deal_dealId, deal_copyId));
        
        int message_Id
            = _dataRepository.getInt("MESSAGE",  "MESSAGEID", 0);

        DocumentRequest request = DocumentRequest.requestMortgageInsurance
            (srk, deal, "" + deal.getScenarioNumber(), deal.getUnderwriterUserId(),  
             Dc.DOCUMENT_GENERAL_TYPE_MI_REQUIRED);

        DocumentQueue document = request.getRequest();
        assertEquals(document.getDealId(), deal_dealId);
    }
    /**
     * test requestMortgageInsurance with DOCUMENT_GENERAL_TYPE_MI_CANCELLED
     */
    @Test
    public void requestMortgageInsurance2() throws Exception
    {
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey( new DealPK(deal_dealId, deal_copyId));
        
        int message_Id
            = _dataRepository.getInt("MESSAGE",  "MESSAGEID", 0);

        DocumentRequest request = DocumentRequest.requestMortgageInsurance
            (srk, deal, "" + deal.getScenarioNumber(), deal.getUnderwriterUserId(),  
             Dc.DOCUMENT_GENERAL_TYPE_MI_CANCELLED);

        DocumentQueue document = request.getRequest();
        assertEquals(document.getDealId(), deal_dealId);
    }
    /**
     * test requestUpload 
     */
    @Test
    public void requestUpload() throws Exception
    {
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey( new DealPK(deal_dealId, deal_copyId));

        DocumentRequest request = DocumentRequest.requestUpload(srk, deal);

        DocumentQueue document = request.getRequest();
        assertEquals(document.getDealId(), deal_dealId);
    }
    
}
