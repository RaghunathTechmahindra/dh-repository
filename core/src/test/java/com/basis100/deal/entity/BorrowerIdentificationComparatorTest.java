package com.basis100.deal.entity;

import java.io.IOException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.BorrowerIdentification;
import com.basis100.deal.util.collections.BorrowerIdentificationComparator;
import com.basis100.resources.SessionResourceKit;

public class BorrowerIdentificationComparatorTest extends FXDBTestCase {
	
	private IDataSet dataSetTest;
	private BorrowerIdentificationComparator bic = null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public BorrowerIdentificationComparatorTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(BorrowerIdentificationComparator.class.getSimpleName()+"DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected DatabaseOperation getSetUpOperation() throws Exception
	    {
		bic = new BorrowerIdentificationComparator();
	    	return DatabaseOperation.INSERT;
	    }
	
	public void testCompare() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCompare");
		
		int id1 = Integer.parseInt((String)testExtract.getValue(0, "IDENTIFICATIONID1"));
		int id2 = Integer.parseInt((String)testExtract.getValue(0, "IDENTIFICATIONID2"));
		int copyid = Integer.parseInt((String)testExtract.getValue(0, "copyid"));
		
		BorrowerIdentification bi1 = new BorrowerIdentification(srk, id1, copyid);
		BorrowerIdentification bi2 = new BorrowerIdentification(srk, id1, copyid);
		
		com.basis100.deal.entity.DealEntity o1 = (com.basis100.deal.entity.DealEntity )bi1;
		com.basis100.deal.entity.DealEntity o2 = (com.basis100.deal.entity.DealEntity )bi2;
		
		int result = bic.compare(o1, o2);
		
		assert result==0 || result==1||result==-1;
		
		srk.rollbackTransaction();
	}
	

}
