package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;

public class MortgageInsurerTest1 extends FXDBTestCase {
	private String INIT_XML_DATA = "MortgageInsurerDataSet.xml";
	private MortgageInsurer mortgageInsurer;
	private IDataSet dataSetTest;

	public MortgageInsurerTest1(String name) throws DataSetException,
			IOException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"MortgageInsurerDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		// TODO Auto-generated method stub
		return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();

		mortgageInsurer = new MortgageInsurer(srk);

	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}
	
	public void testFindMortgageInsurersNotUsedFailure() throws Exception {
		int dealId = Integer.parseInt(dataSetTest
				.getTable("testFindMortgageInsurersNotUsedFailure")
				.getValue(0, "dealId").toString());
		Collection list=mortgageInsurer.findMortgageInsurersNotUsed(dealId);
		assertNotNull(list);
		assertEquals(true, list.size()<=0);
		
	}
	public void testFindByMPBusinessId(){
		//mortgageInsurer.fi
	}

}
