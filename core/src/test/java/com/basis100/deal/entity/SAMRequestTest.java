/*
 * @(#)SAMRequestTest.java    2007-9-7
 *
 * Copyright (C) 2007 Filogix Limited Partnership All rights reserved.
 */


package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.SAMRequestPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>SAMRequestTest</p>
 * Express Entity class unit test: SAMRequest
 */
public class SAMRequestTest extends ExpressEntityTestCase 
    implements UnitTestLogging {
  
    private final static Logger _logger = 
        LoggerFactory.getLogger(RegionProfileTest.class);
    private final static String ENTITY_NAME = "SAMREQUEST"; 
    
    // retreived data count that is stored in UnitTestDataRepository.xml
    private static final int DATA_COUNT = 1;
  
    private SessionResourceKit _srk;
  
    // use this value for random sequence number data
    private int testSeqForGen = 0;
  
    // properties
    protected int           requestId;
    protected int           copyId; // this is missing in entity class
    protected int         samStepId;
    protected int           samApplicationTypeId;
    protected int           samPropertyTypeId;
    protected int           samProductTypeId;
    protected String        samRequestTypeCode;
    protected String        samCreditFileRequired;
    protected String        samGuarantorOccupancy;
    protected int        instProfId;
  
    /**
     * <p>setUp</p>
     * Setting up test case.
     */
    @Before
    public void setUp() throws Exception {

        _logger.info(BORDER_START, ENTITY_NAME + " TestCase setting up ...");
  
        //  init session resource kit.
        ResourceManager.init();
        //  init session resource kit.
        _srk = new SessionResourceKit();
  
//        // get random seqence number 
//        Double indexD = new Double(Math.random() * DATA_COUNT);
//        testSeqForGen = indexD.intValue();
  
        // retreive data from test data file
        requestId 
            = _dataRepository.getInt( ENTITY_NAME, "REQUESTID", testSeqForGen);
        copyId 
            = _dataRepository.getInt( ENTITY_NAME, "COPYID", testSeqForGen);
        samStepId 
            = _dataRepository.getInt( ENTITY_NAME, "SAMSTEPID", testSeqForGen);
        samApplicationTypeId 
            = _dataRepository.getInt( ENTITY_NAME, "SAMAPPLICATIONTYPEID", testSeqForGen);
        samPropertyTypeId 
            = _dataRepository.getInt( ENTITY_NAME, "SAMPROPERTYTYPEID", testSeqForGen);
        samProductTypeId 
            = _dataRepository.getInt( ENTITY_NAME, "samProductTypeId", testSeqForGen);
        samRequestTypeCode 
            = _dataRepository.getString( ENTITY_NAME, "SAMREQUESTTYPECODE", testSeqForGen);
        samCreditFileRequired 
            = _dataRepository.getString( ENTITY_NAME, "SAMCREDITFILEREQUIRED", testSeqForGen);
        samGuarantorOccupancy 
            = _dataRepository.getString( ENTITY_NAME, "SAMGUARANTOROCCUPANCY", testSeqForGen);
        instProfId 
            = _dataRepository.getInt( ENTITY_NAME, "INSTITUTIONPROFILEID", testSeqForGen);
        
        _logger.info(BORDER_END, "Setting up Done");
    }
                  
    @After
    public void tearDown() {
      
        //      free resources
        _srk.freeResources( );
  
    }

    /**
     * test findByPrimaryKey.
     */
    @Ignore @Test
    public void testFindByPrimaryKey()  throws Exception {
    
        _logger.info(BORDER_START, "findByPrimaryKey");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
        
        // excute method
        SAMRequest entity = new SAMRequest(_srk);
        entity = entity.findByPrimaryKey(new SAMRequestPK(requestId));
              
        // assertEquals
        assertEquals(entity.getRequestId(), requestId);
//        assertEquals(entity.getCopyId(), copyId);
        assertEquals(entity.getRequestId(), samStepId);
        assertEquals(entity.getRequestId(), samApplicationTypeId);
        assertEquals(entity.getRequestId(), samPropertyTypeId);
        assertEquals(entity.getRequestId(), samProductTypeId);
        assertEquals(entity.getRequestId(), samRequestTypeCode);
        assertEquals(entity.getRequestId(), samCreditFileRequired);
        assertEquals(entity.getRequestId(), samGuarantorOccupancy);

        _logger.info(BORDER_END, "findByPrimaryKey");
    }
}
