package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.RegionProfilePK;

public class RegionProfileTest1 extends  FXDBTestCase  {
	
	private IDataSet dataSetTest;
	RegionProfile regionProfile=null;
	public RegionProfileTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("RegionProfileDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource("RegionProfileDataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		regionProfile=new RegionProfile(srk); 
	}
	//createPrimaryKey()
	//create(RegionProfilePK)

    public void testCreatePrimaryKey() throws Exception{
    	RegionProfilePK regionProfilePK=regionProfile.createPrimaryKey();
    	assertNotNull(regionProfilePK);
    }
   /* //create(RegionProfilePK)
   public void testCreate() throws Exception{
    	srk.beginTransaction();
    	ITable testFindByPrimaryKey = dataSetTest.getTable("testCreate");
		int regionProfileId =Integer.parseInt(testFindByPrimaryKey.getValue(0,"RegionProfileId").toString());
		int DealInstitutionId =Integer.parseInt(testFindByPrimaryKey.getValue(0,"DealInstitutionId").toString());
    	RegionProfilePK regionProfilePK=new RegionProfilePK(regionProfileId);
    	srk.getExpressState().setDealInstitutionId(DealInstitutionId);
    	RegionProfile regionProfile1=regionProfile.create(regionProfilePK);
    	assertNotNull(regionProfile1);
    	srk.rollbackTransaction();
    }*/
   
    
    
}
