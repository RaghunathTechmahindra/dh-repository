package com.basis100.deal.entity;

import java.io.IOException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.conditions.ConditionHandler;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.docprep.extract.doctag.SolicitorsPackEmployerPhone;
import com.basis100.resources.SessionResourceKit;

public class SolicitorsPackEmployerPhoneTest extends FXDBTestCase {
	private IDataSet dataSetTest;
	private SolicitorsPackEmployerPhone solicitorsPackEmployerPhone = null;
	private ConditionHandler ch = null;
    CalcMonitor dcm =null;
    FMLQueryResult fmlQueryResult =null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public SolicitorsPackEmployerPhoneTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(SolicitorsPackEmployerPhone.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	

	@Override
	protected DatabaseOperation getSetUpOperation() throws Exception
	    {
		solicitorsPackEmployerPhone = new SolicitorsPackEmployerPhone();
	    	return DatabaseOperation.INSERT;
	    }
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}
	
	public void testExtract() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testExtract");
		
		int institutionProfileId = Integer.parseInt((String)testExtract.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String)testExtract.getValue(0, "dealid"));
		int copyid = Integer.parseInt((String)testExtract.getValue(0, "copyid"));
		int conditionId = Integer.parseInt((String)testExtract.getValue(0, "CONDITIONID"));
	
		Deal deal = new Deal(srk, null, id, copyid);
		com.basis100.deal.entity.DealEntity de = (com.basis100.deal.entity.DealEntity )deal;
		deal.setInstitutionProfileId(institutionProfileId);
		
		ch = new ConditionHandler(srk);
		
		int lang = 0;
		fmlQueryResult = solicitorsPackEmployerPhone.extract(de, lang, null, srk);
		
		//assertTrue(ch.getVariableVerbiage(deal, conditionId, 1)!=null);
		assertNotNull(fmlQueryResult);
		assert fmlQueryResult.getValues().size()>0;
		
		srk.rollbackTransaction();
	}
	
}
