package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.ServiceProductPk;


/**
 * <p>ServiceProductDbTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class ServiceProductDbTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private ServiceProduct serviceProduct;
	
	public ServiceProductDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ServiceProduct.class.getSimpleName() + "DataSetTest.xml"));
	}

	

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.serviceProduct = new ServiceProduct(srk);
		
	}
	
	public void testFindByPrimaryKey() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByPrimaryKey");
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "SERVICEPRODUCTID"));
    	
    	serviceProduct.findByPrimaryKey(new ServiceProductPk(id));
	    assertNotNull(serviceProduct);
    }
}