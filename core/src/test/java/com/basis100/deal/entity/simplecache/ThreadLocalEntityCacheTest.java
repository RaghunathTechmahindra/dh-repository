package com.basis100.deal.entity.simplecache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.junit.BeforeClass;
import org.junit.Test;

import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.core.ExpressRuntimeException;


public class ThreadLocalEntityCacheTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println(ThreadLocalEntityCache.class);
		Field f = ThreadLocalEntityCache.class.getDeclaredField("ENABLED");
		f.setAccessible(true);
		Object obj = f.get(ThreadLocalEntityCache.class);
		if(((Boolean)obj).booleanValue() == false) {
			System.err.println("Not Enabled, you have to set 'Y' to SysProperty table: com.basis100.deal.entity.simpleentitycache");
			//assertNotNull(obj);
			fail("Not Enabled, you have to set 'Y' to SysProperty table: com.basis100.deal.entity.simpleentitycache");
		}
	}

	
	@Test
	public void testWriteToCache() throws IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchFieldException, RemoteException, FinderException {
		SessionResourceKit srk = new SessionResourceKit();
		srk.getExpressState().setDealInstitutionId(0);

		Deal d1 = new Deal(srk, null);
		Deal d2 = new Deal(srk, null);
		Deal d3 = new Deal(srk, null);

		DealPK pk = new DealPK(100, 23);

		Field refPKField = Deal.class.getDeclaredField("pk");
		refPKField.setAccessible(true);

		refPKField.set(d1, pk);

		d1.setActualPaymentTerm(100);

		ThreadLocalEntityCache.writeToCache(d1, pk);
		ThreadLocalEntityCache.readFromCache(d2, pk);
         //assertNotNull(refPKField);
		assertEquals(100, d2.getActualPaymentTerm());
		
		d3.setActualPaymentTerm(200);
		DealPK pk2 = new DealPK(100, 23);
		refPKField.set(d3, pk2);

		ThreadLocalEntityCache.writeToCache(d3, pk);
		ThreadLocalEntityCache.readFromCache(d2, pk);
		 //assertNotNull(pk2);
		assertEquals(100, d1.getActualPaymentTerm());
		assertEquals(200, d2.getActualPaymentTerm());

	}

	@Test
	public void testRemoveFromCache() throws IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchFieldException, RemoteException, FinderException {

		SessionResourceKit srk = new SessionResourceKit();
		srk.getExpressState().setDealInstitutionId(1);
		Deal d1 = new Deal(srk, null);
		Deal d2 = new Deal(srk, null);

		DealPK pk = new DealPK(101, 23);
		Field refPKField = Deal.class.getDeclaredField("pk");
		refPKField.setAccessible(true);

		refPKField.set(d1, pk);
		d1.setActualPaymentTerm(100);

		ThreadLocalEntityCache.writeToCache(d1, pk);
		ThreadLocalEntityCache.readFromCache(d2, pk);
		 //assertNotNull(refPKField);
	     assertEquals(100, d2.getActualPaymentTerm());

		ThreadLocalEntityCache.removeFromCache(d2, pk);
		assertFalse(ThreadLocalEntityCache.readFromCache(d2, pk));

	}

	@Test
	public void testRemoveFromCache2() throws IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchFieldException, RemoteException, FinderException {

		SessionResourceKit srk = new SessionResourceKit();
		srk.getExpressState().setDealInstitutionId(1);
		Deal d1 = new Deal(srk, null);
		Deal d2 = new Deal(srk, null);
		Borrower b1 = new Borrower(srk, null);
		Borrower b2 = new Borrower(srk, null);
		
		//set values to deal
		DealPK pk = new DealPK(101, 23);
		Field refPKField = Deal.class.getDeclaredField("pk");
		refPKField.setAccessible(true);
		refPKField.set(d1, pk);
		d1.setActualPaymentTerm(100);
		
		//set values to borrower
		BorrowerPK bpk = new BorrowerPK(222, 23);
		refPKField = Borrower.class.getDeclaredField("pk");
		refPKField.setAccessible(true);
		refPKField.set(b1, bpk);
		b1.setBorrowerFirstName("test");
		
		
		ThreadLocalEntityCache.writeToCache(d1, pk);

		ThreadLocalEntityCache.writeToCache(b1, bpk);
		
		//delete Deal class
		ThreadLocalEntityCache.removeFromCache(Deal.class);
		//so, deal has to be dorroped
		assertFalse(ThreadLocalEntityCache.readFromCache(d2, pk));
		//but, this operation doesn't change borrower
		assertTrue(ThreadLocalEntityCache.readFromCache(b2, bpk));
		assertEquals("test", b2.getBorrowerFirstName());
		//assertNotNull(bpk);

	}
	@Test
	public void testReadFromCache() throws IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchFieldException, RemoteException, FinderException {

		SessionResourceKit srk = new SessionResourceKit();
		srk.getExpressState().setDealInstitutionId(1);

		Deal d1 = new Deal(srk, null);
		Deal d2 = new Deal(srk, null);
		DealPK pk = new DealPK(100, 23);

		Field refPKField = Deal.class.getDeclaredField("pk");
		refPKField.setAccessible(true);

		refPKField.set(d1, pk);

		d1.setDealId(100);
		d1.setCopyId(23);
		d1.setScenarioNumber(3);
		d1.setScenarioRecommended("Y");
		Date date = new Date();
		d1.setApplicationDate(new Date());
		d1.setAddCreditcostInsIncurred(12.34);

		ThreadLocalEntityCache.writeToCache(d1, pk);
		ThreadLocalEntityCache.readFromCache(d2,pk);

		assertEquals(pk, refPKField.get(d2));
		assertEquals(100, d2.getDealId());
		assertEquals(23, d2.getCopyId());
		assertEquals(3, d2.getScenarioNumber());
		assertEquals("Y", d2.getScenarioRecommended());
		assertEquals(date, d2.getApplicationDate());
		assertNotSame(date, d2.getApplicationDate()); //date must be deep cloned
	     assertEquals(12.34, d2.getAddCreditCostInsIncurred(), 0.001);

		assertNotNull(d1.getChangeValueMap());
		assertNotNull(d2.getChangeValueMap());

		System.out.println("System.identityHashCode(d1.getChangeValueMap()) = " + System.identityHashCode(d1.getChangeValueMap()));
		System.out.println("System.identityHashCode(d2.getChangeValueMap()) = " + System.identityHashCode(d2.getChangeValueMap()));

		  assertEquals(d1.getChangeValueMap(), d2.getChangeValueMap());
	      assertNotSame(d1.getChangeValueMap(), d2.getChangeValueMap()); //cvm must be deeply cloned
		//assertNotNull(refPKField);
	}

	@Test
	public void testCopyFields() throws IllegalArgumentException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException, SecurityException, NoSuchFieldException, ParseException {
		Deal d1 = new Deal();
		Deal d2 = new Deal();

		Field refPKField = Deal.class.getDeclaredField("pk");
		refPKField.setAccessible(true);

		//		DealPK pk = new DealPK(100, 23);
		//		refPKField.set(d1, pk);

		d1.setDealId(100);
		d1.setCopyId(23);
		d1.setScenarioNumber(3);
		d1.setScenarioRecommended("Y");
		Date date = new Date();
		d1.setApplicationDate(new Date());
		d1.setAddCreditcostInsIncurred(12.34);		
		
		String indexEffectiveDate="16-AUG-06";
		SimpleDateFormat formate=new SimpleDateFormat("dd-MMM-yy");
		Date dateDeal2=formate.parse(indexEffectiveDate);
		d2.setApplicationDate(dateDeal2);

		ThreadLocalEntityCache.copyFields(d1, d2);

		//		assertEquals(pk, refPKField.get(d2));
		assertEquals(100, d2.getDealId());
		assertEquals(23, d2.getCopyId());
		assertEquals(3, d2.getScenarioNumber());
		assertEquals("Y", d2.getScenarioRecommended());
		assertEquals(date, d2.getApplicationDate());
		assertNotSame(date, d1.getApplicationDate()); //date must be deep cloned
		assertEquals(12.34, d2.getAddCreditCostInsIncurred(), 0.001);

		assertEquals(d1.getChangeValueMap(), d2.getChangeValueMap());
		assertNotSame(d1.getChangeValueMap(), d2.getChangeValueMap()); //cvm must be deeply cloned

		DummyForMutable dummy1 = new DummyForMutable();
		DummyForMutable dummy2 = new DummyForMutable();
		dummy1.StringBufferField = new StringBuffer();
		dummy1.StringBufferField.append("I don't think Express uses SB!");
		try {
			ThreadLocalEntityCache.copyFields(dummy1, dummy2);
			fail("ExpressRuntimeException should happen");
		} catch (ExpressRuntimeException e) {
			assertTrue(true);
		}

	}

	@Test
	public void testCheckMutable() throws SecurityException, NoSuchFieldException {

		Field f = DummyForMutable.class.getDeclaredField("intField");
		assertFalse(ThreadLocalEntityCache.checkMutable(f.getType()));
		f = DummyForMutable.class.getDeclaredField("booleanField");
		assertFalse(ThreadLocalEntityCache.checkMutable(f.getType()));
		f = DummyForMutable.class.getDeclaredField("longField");
		assertFalse(ThreadLocalEntityCache.checkMutable(f.getType()));
		f = DummyForMutable.class.getDeclaredField("doubleField");
		assertFalse(ThreadLocalEntityCache.checkMutable(f.getType()));
		f = DummyForMutable.class.getDeclaredField("StringField");
		assertFalse(ThreadLocalEntityCache.checkMutable(f.getType()));
		f = DummyForMutable.class.getDeclaredField("IntegerField");
		assertFalse(ThreadLocalEntityCache.checkMutable(f.getType()));
		f = DummyForMutable.class.getDeclaredField("LongField");
		assertFalse(ThreadLocalEntityCache.checkMutable(f.getType()));
		f = DummyForMutable.class.getDeclaredField("DoubleField");
		assertFalse(ThreadLocalEntityCache.checkMutable(f.getType()));
		f = DummyForMutable.class.getDeclaredField("BooleanField");
		assertFalse(ThreadLocalEntityCache.checkMutable(f.getType()));
		f = DummyForMutable.class.getDeclaredField("DateField");
		assertTrue(ThreadLocalEntityCache.checkMutable(f.getType()));
		f = DummyForMutable.class.getDeclaredField("StringBufferField");
		assertTrue(ThreadLocalEntityCache.checkMutable(f.getType()));

	}

	@Test
	public void testClearCache() throws Exception {

		SessionResourceKit srk = new SessionResourceKit();
		srk.getExpressState().setDealInstitutionId(1);

		Deal d1 = new Deal(srk, null);
		DealPK pk = new DealPK(100, 25);
		Deal d2 = new Deal(srk, null);

		ThreadLocalEntityCache.writeToCache(d1, pk);

		ThreadLocalEntityCache.clearCache();

		assertFalse(ThreadLocalEntityCache.readFromCache(d2, pk));

	}


	@Test
	public void testAttachPK() throws Exception {

		Deal d1 = new Deal();
		DealPK pk = new DealPK(100, 24);

		ThreadLocalEntityCache.attachPK(d1, pk);

		assertSame(d1.getPk(), pk);
	}

	@Test
	public void testGetInstId() throws Exception {

		SessionResourceKit srk = new SessionResourceKit();
		srk.getExpressState().setDealInstitutionId(1);
		Deal d1 = new Deal(srk, null);

		assertEquals(1, ThreadLocalEntityCache.getInstId(d1));

		srk.getExpressState().setDealInstitutionId(2);
		assertEquals(2, ThreadLocalEntityCache.getInstId(d1));

		srk.getExpressState().cleanAllIds();
		assertEquals(-1, ThreadLocalEntityCache.getInstId(d1));

		Deal d2 = new Deal();
		assertEquals(-1, ThreadLocalEntityCache.getInstId(d2));

		SessionResourceKit srk2 = new SessionResourceKit();
		d2.setSessionResourceKit(srk2);
		assertEquals(-1, ThreadLocalEntityCache.getInstId(d2));

	}

	@SuppressWarnings("serial")
	class DummyForMutable extends DealEntity{

		int intField;
		boolean booleanField;
		long longField;
		double doubleField;
		String StringField;
		Integer IntegerField;
		Long LongField;
		Double DoubleField;
		Boolean BooleanField;
		Date DateField;
		StringBuffer StringBufferField;
		BorrowerPK pk;
	}
	
	
	@Test
	public void testCloneObject() throws Exception {
		
		Object expected = null;
		String str = "abc";
		
		expected = ThreadLocalEntityCache.cloneObject(str);
		assertSame(expected, str);
		assertEquals(expected, str);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("1", new Date());
		map.put("2", "abc");
		map.put("3", 12345);
		
		expected = ThreadLocalEntityCache.cloneObject(map);
		
		assertFalse(expected == map);
		assertEquals(expected.toString(), map.toString());
		assertFalse(((Map)expected).get("1") == map.get("1"));
		
		Map<String, Object> map2 = new HashMap<String, Object>();
		map2.put("11", new Date());
		map2.put("12", "abc");
		map2.put("13", 12345);
		
		map.put("map", map2);
		
		expected = ThreadLocalEntityCache.cloneObject(map);
		assertFalse(expected == map);
		assertEquals(expected.toString(), map.toString());
		assertFalse(((Map)expected).get("map") == map.get("map"));
		
		
		List<Object> list = new Vector<Object>();
		
		list.add("123");
		list.add(new Date());
		list.add(map);
		
		expected = ThreadLocalEntityCache.cloneObject(list);
		System.out.println(list);
		assertFalse(expected == list);
		assertEquals(expected.toString(), list.toString());
	}

}
