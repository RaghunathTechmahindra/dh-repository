package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;

public class MbmlRedirectTest extends FXDBTestCase{
	private String INIT_XML_DATA = "MbmlRedirectDataSet.xml";
	private MbmlRedirect mbmlRedirect;
	private IDataSet dataSetTest;

	public MbmlRedirectTest(String name) throws DataSetException,
			IOException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"MbmlRedirectDataSetTest.xml"));
	}

/*	@Override
	protected IDataSet getDataSet() throws Exception {
		// TODO Auto-generated method stub
		return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}
*/
	@Override
	protected void setUp() throws Exception {
		super.setUp();

		mbmlRedirect = new MbmlRedirect(srk);

	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}
	public void testFindBySourceApplicationId(){
		MbmlRedirect mbmlRedirect1 = null;
		try{
			mbmlRedirect1 = mbmlRedirect.findBySourceApplicationId("1");
		}
		catch(Exception e){}
		assertNotNull(mbmlRedirect1);
	}
	
}
