package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.LenderProfilePK;
import com.basis100.resources.SessionResourceKit;

public class LenderProfileTest1 extends FXDBTestCase {
	private IDataSet dataSetTest;
    private LenderProfile lenderProfile = null;
    @SuppressWarnings("unused")
	private LenderProfilePK pk = null;
    @SuppressWarnings("rawtypes")
	private Collection collection = null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public LenderProfileTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(LenderProfile.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(LenderProfile.class.getSimpleName()+"DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.lenderProfile = new LenderProfile(srk);
	}
	
	public void testCreate() throws Exception
	{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int institutionProfileId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String)testCreate.getValue(0, "LENDERPROFILEID"));
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		LenderProfilePK pk = new LenderProfilePK(id);
		
		LenderProfile aLenderProfile = lenderProfile.create(pk);
		LenderProfile new_LenderProfile = new LenderProfile(srk, id);
		
		assertEquals(new_LenderProfile, aLenderProfile);
		
		srk.rollbackTransaction();
	}
	
	/*
	 *Issue in DB Query - Insert into LDInsuranceRates(LDInsuranceRateId ) Values ( 50) inserts only one column where as 
	 *it is supposed to insert 2 other not nullable columns of LDINSURANCETYPEID,INSTITUTIONPROFILEID also.
	 */
	public void testCreatePrimaryKey() throws Exception
	{
		srk.beginTransaction();
		@SuppressWarnings("unused")
		ITable testCreatePrimaryKey = dataSetTest.getTable("testCreatePrimaryKey");
		
		LenderProfilePK pk = null;
		pk = lenderProfile.createPrimaryKey();
		
		assertNotNull(pk);
		assert pk.getId() > 0;
		srk.rollbackTransaction();
	} 

	/*
	 * Failure case not applicable as the create method returns an entity and sending unqualified data will result
	 * in improper end of test class and thus it is avoid.
	 */
	public void testfindByDefaultLender() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByDefaultLender = dataSetTest.getTable("testfindByDefaultLender");
		String defaultLender = testfindByDefaultLender.getValue(0, "LENDERNAME").toString();
		
		LenderProfile alenderProfile=lenderProfile.findByDefaultLender();
		
		assertTrue(alenderProfile.getLenderName().equals(defaultLender));
		
		srk.rollbackTransaction();
	}
	
	public void testfindByDefaultLenderFailure() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByDefaultLenderFailure = dataSetTest.getTable("testfindByDefaultLenderFailure");
		String defaultLender = testfindByDefaultLenderFailure.getValue(0, "LENDERNAME").toString();
		
		LenderProfile alenderProfile=lenderProfile.findByDefaultLender();
		
		assertFalse(alenderProfile.getLenderName().equals(defaultLender));
		srk.rollbackTransaction();
	}
	
	public void testfindByShortName() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByShortName = dataSetTest.getTable("testfindByShortName");
		String shortName = testfindByShortName.getValue(0, "LPSHORTNAME").toString();
		
		collection = lenderProfile.findByShortName(shortName);
		assert collection.size() > 0;
		srk.rollbackTransaction();
	}
	
	public void testfindByShortNameFailure() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByShortName = dataSetTest.getTable("testfindByShortName");
		String shortName = testfindByShortName.getValue(0, "LPSHORTNAME").toString();
		
		collection = lenderProfile.findByShortName(shortName);
		assert collection.size() == 0;
		srk.rollbackTransaction();
	}
}
