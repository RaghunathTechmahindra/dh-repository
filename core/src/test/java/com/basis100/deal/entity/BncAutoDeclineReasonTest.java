/*
 * @(#)BncAutoDeclineReasonTest.java Sep 21, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.BncAutoDeclineReasonPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * @author amalhi
 * 
 */
public class BncAutoDeclineReasonTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(BncAutoDeclineReasonTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public BncAutoDeclineReasonTest() {

    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.BncAutoDeclineReason#findByPrimaryKey(com.basis100.deal.pk.BncAutoDeclineReasonPK)}.
     */
    @Ignore("no data for associated table")
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int responseId = _dataRepository.getInt("BncAutoDeclineReason",
                "responseId", 0);
        int instProfId = _dataRepository.getInt("BncAutoDeclineReason",
                "INSTITUTIONPROFILEID", 0);
        int autoDeclineReasonId = _dataRepository.getInt(
                "BncAutoDeclineReason", "autoDeclineReasonId", 0);

        _logger.info(BORDER_START,
                "BncAutoDeclineReason - testFindByPrimaryKey");
        _logger.info("Got Values from BncAutoDeclineReason found::: [{}]",
                responseId + "::" + instProfId + "::" + autoDeclineReasonId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        BncAutoDeclineReason _bncAutoDeclineReason = new BncAutoDeclineReason(
                _srk);

        // Find by primary key
        _bncAutoDeclineReason = _bncAutoDeclineReason
                .findByPrimaryKey(new BncAutoDeclineReasonPK(responseId,
                        autoDeclineReasonId));

        _logger
                .info(
                        "Got Values from BncAdjudicationApplicantResponse found::: [{}]",
                        _bncAutoDeclineReason.getResponseId()
                                + "::"
                                + _bncAutoDeclineReason
                                        .getAutoDeclineReasonId());

        // Check if the data retrieved matches the DB
        assertEquals(_bncAutoDeclineReason.getResponseId(), responseId);
        assertEquals(_bncAutoDeclineReason.getAutoDeclineReasonId(),
                autoDeclineReasonId);

        _logger.info(BORDER_END, "BncAutoDeclineReason - testFindByPrimaryKey");
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.BncAutoDeclineReason#create(int, java.lang.String, java.lang.String)}.
     */
    @Ignore("no data for associated table")
    @Test
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create - BncAutoDeclineReason");

        // get input data from repository
        int instProfId = _dataRepository.getInt("BncAutoDeclineReason",
                "INSTITUTIONPROFILEID", 0);
        int responseId = _dataRepository.getInt("BNCADJUDICATIONRESPONSE",
                "responseId", 0);
        String autoDeclineReasonCode = _dataRepository.getString(
                "BncAutoDeclineReason", "autoDeclineReasonCode", 0);
        String autoDeclineReasonDesc = _dataRepository.getString(
                "BncAutoDeclineReason", "autoDeclineReasonDesc", 0);

        _logger.info("Got Values from new BncAutoDeclineReason ::: [{}]",
                instProfId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // begin transaction
        _srk.beginTransaction();

        // Create the initial entity
        BncAutoDeclineReason newEntity = new BncAutoDeclineReason(_srk);

        // call create method of entity
        newEntity = newEntity.create(responseId, autoDeclineReasonCode,
                autoDeclineReasonDesc);

        _logger.info("CREATED BncAutoDeclineReason ::: [{}]", newEntity
                .getResponseId()
                + "::" + newEntity.getAutoDeclineReasonCode());

        // check create correctly by calling findByPrimaryKey
        BncAutoDeclineReason foundEntity = new BncAutoDeclineReason(_srk);

        foundEntity = foundEntity.findByPrimaryKey(new BncAutoDeclineReasonPK(
                newEntity.getResponseId(), newEntity.getAutoDeclineReasonId()));

        // verifying the created entity

        assertEquals(foundEntity.getResponseId(), newEntity.getResponseId());

        assertEquals(foundEntity.getAutoDeclineReasonCode(), newEntity
                .getAutoDeclineReasonCode());

        assertEquals(foundEntity.getAutoDeclineReasonDesc(), newEntity
                .getAutoDeclineReasonDesc());

        _logger.info(BORDER_END, "Create - BncAutoDeclineReason");

        // roll-back transaction.
        _srk.rollbackTransaction();
    }
}
