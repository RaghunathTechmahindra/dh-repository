/*
 * @(#)ChargeTermTest.java    2005-3-16
 *
 */

package com.basis100.deal.entity;

import java.sql.Connection;
import java.sql.Statement;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;

import com.basis100.TestingEnvironmentInitializer;
import com.basis100.deal.pk.AddrPK;
import com.basis100.deal.pk.ChargeTermPK;
import com.basis100.deal.pk.ContactPK;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.externallinks.framework.ServiceConst;

/**
 * ChargeTermTest -
 * 
 * @version 1.0 2007-3-16
 * @author <A HREF="mailto:chyxiang@yahoo.com">Chen Xiang (Sean)</A>
 */
public class ChargeTermTest extends ExpressEntityTestCase {

    // The logger
    private static Log _log = LogFactory.getLog(ChargeTermTest.class);

    // the Session Resource Kit
    private SessionResourceKit _srk;
    
    // charge term entity
    ChargeTerm _chargeTermDAO;

    // institution id for deal
    private int dealInstitutionId;

    /*
     * setup
     */
    @Before
    public void setUp() throws Exception {
        
        // initialize resource
        TestingEnvironmentInitializer.initTestingEnv();
        _srk = new SessionResourceKit("Testing");
        _chargeTermDAO = new ChargeTerm(_srk);
        
        // get value from database
        dealInstitutionId = _dataRepository.getInt("ChargeTerm",
                "InstitutionProfileId", 0);
    }

    /*
     * test findby primary key
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {
        
        // set vpd context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        
        //get values from database
        int chargeTermId = 
            _dataRepository.getInt("ChargeTerm", "ChargeTermId", 0);
        int provinceId = 
            _dataRepository.getInt("ChargeTerm", "ProvinceId", 0);
        
        // find a charge term by primary key
        ChargeTermPK chargeTermPK = new ChargeTermPK(chargeTermId);
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        _chargeTermDAO.findByPrimaryKey(chargeTermPK);
        assertEquals(provinceId, _chargeTermDAO.getProvinceId());
        _log.info("Charge Term Found ");
        _log.info("ChargeTerm Description: "
                + _chargeTermDAO.getChargeTermDescription());
        _log.info("ChargeTerm Qualifier: "
                + _chargeTermDAO.getChargeTermQualifier());
        _log.info("ChargeTerm FillingNumber: "
                + _chargeTermDAO.getChargeTermFillingNumber());


    }

    /*
     * test find by LenderProvinceAndQualifier
     */
    @Test
    public void testFindFirstByLenderProvinceAndQualifier() throws Exception {

        // set vpd context
        try {
            _srk.beginTransaction();
            dealInstitutionId = _dataRepository.getInt("LenderProfile",
                    "institutionProfileId", 0);
            int lenderProfileId = _dataRepository.getInt("LenderProfile",
                    "lenderProfileId", 0);
            int contactId = _dataRepository.getInt("LenderProfile",
                    "contactid", 0);
            _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
            Contact contact = new Contact(_srk);
            ContactPK contactPk = new ContactPK(contactId, 1);
            contact = contact.findByPrimaryKey(contactPk);
            int addrId = contact.getAddrId();
            Addr address = new Addr(_srk);
            AddrPK addressPk = new AddrPK(addrId, 1);
            address = address.findByPrimaryKey(addressPk);
            int provinceId = address.getProvinceId();

            String qualifier = "test";
            insertData(provinceId, Mc.LANGUAGE_PREFERENCE_ENGLISH,
                    lenderProfileId, dealInstitutionId, qualifier);

            // find a charge term by lender privince and qualifier
            _chargeTermDAO.findFirstByLenderProvinceAndQualifier(
                    lenderProfileId, provinceId, qualifier);

            // verify result
            assertNotNull(_chargeTermDAO);

            _log.info("Charge Term FindFirstByLenderProvinceAndQualifier Found ");
            _log.info("     Description: "
                    + _chargeTermDAO.getChargeTermDescription());
            _log.info("       Qualifier: "
                    + _chargeTermDAO.getChargeTermQualifier());
            _log.info("   FillingNumber: "
                    + _chargeTermDAO.getChargeTermFillingNumber());
        } catch (Exception e) {
            _srk.rollbackTransaction();
        }

    }

    /*
     * test Create
     */
    @Test
    public void testCreate() throws Exception {

        // set vpd context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        
        // get values from database
        int provinceId = 
            _dataRepository.getInt("ChargeTerm", "ProvinceId", 0);
        int languageId = 
            _dataRepository.getInt("ChargeTerm", 
                                   "ConditionLanguagePreferenceId", 0);
        int lenderProfileId = 
            _dataRepository.getInt("ChargeTerm", "LenderProfileId", 0);
        
        // create a charge term
        _chargeTermDAO.create(provinceId, languageId, lenderProfileId);
        
        // verify result
        assertNotNull(_chargeTermDAO);
        assertEquals(provinceId, _chargeTermDAO.getProvinceId());
        _log.info("Charge Term FindFirstByLenderProvinceAndQualifier Found ");
        _log.info("         Province Id: " + _chargeTermDAO.getProvinceId());
        _log.info("   Lender Profile Id: "
                + _chargeTermDAO.getLenderProfileId());
        _log.info("          Languge Id: "
                + _chargeTermDAO.getConditionLanguagePreferenceId());

    }
    
    /**
     * clean the testing env.
     * @throws JdbcTransactionException 
     */
    @After
    public void tearDown() throws JdbcTransactionException {
        //free the srk
        
        _srk.freeResources();
    }
    
    /**
     * @param provinceId
     * @param conditionLanguagePreferenceId
     * @param lenderProfileId
     * @param institutionProfileId
     * @param qualifier
     * @throws Exception
     */
    private void insertData(int provinceId, int conditionLanguagePreferenceId,
            int lenderProfileId, int institutionProfileId, String qualifier)
            throws Exception {

        Connection con = _srk.getConnection();
        Statement stat = con.createStatement();
        String sql = "Insert into ChargeTerm( chargeTermId, provinceId,  "
                + "conditionLanguagePreferenceId, lenderProfileId, institutionProfileId, " 
                + "chargeTermQualifier) Values ("
                + "ChargeTermseq.nextval, " + provinceId + ", "
                + conditionLanguagePreferenceId + ", " + lenderProfileId + ", "
                + institutionProfileId + ", '" + qualifier + "' ) ";
        stat.execute(sql);
    }
    
}
