/*
 * @(#) DisclosureQuestionTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */


package com.basis100.deal.entity;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.DisclosureQuestionPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>DisclosureQuestionTest</p>
 * Express Entity class unit test: DisclosureQuestion
 */
public class DisclosureQuestionTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(DocumentProfileTest.class);

    /**
     * Constructor function
     */
    public DisclosureQuestionTest() {
    }

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int disclosureQuestionId = _dataRepository.getInt("DISCLOSUREQUESTION",
                "DISCLOSUREQUESTIONID", 0);
        int dealId = _dataRepository.getInt("DISCLOSUREQUESTION", "DEALID", 0);
        int instProfId = _dataRepository.getInt("DISCLOSUREQUESTION",
                "INSTITUTIONPROFILEID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed the input data and run the program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        DisclosureQuestion entity = new DisclosureQuestion(srk);
        DisclosureQuestionPK dpk = new DisclosureQuestionPK(
                disclosureQuestionId);
        entity = entity.findByPrimaryKey(dpk);

        // verify the running result.
        assertEquals(disclosureQuestionId, entity.getDisclosureQuestionId());
        assertEquals(dealId, entity.getDealId());

        // free resources
        srk.freeResources();
    }

    /**
     * test create.
     */
    @Test
    public void testCreate() throws Exception {

        // get input data from repository
        int disclosureQuestionId = _dataRepository.getInt("DISCLOSUREQUESTION",
                "DISCLOSUREQUESTIONID", 0);
        int dealId = _dataRepository.getInt("DISCLOSUREQUESTION", "DEALID", 0);
        String questionCode = _dataRepository.getString("DISCLOSUREQUESTION",
                "QUESTIONCODE", 0);
        int documentTypeId = _dataRepository.getInt("DISCLOSUREQUESTION",
                "DOCUMENTTYPEID", 0);
        String isResourceBundleKey = _dataRepository.getString(
                "DISCLOSUREQUESTION", "ISRESOURCEBUNDLEKEY", 0);

        int instProfId = _dataRepository.getInt("DISCLOSUREQUESTION",
                "INSTITUTIONPROFILEID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed input data and run program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        srk.beginTransaction();
        DisclosureQuestion newEntity = new DisclosureQuestion(srk);
        DealPK dppk = new DealPK(dealId, 0);
        newEntity = newEntity.create(dppk, documentTypeId, questionCode,
                isResourceBundleKey);
        newEntity.ejbStore();

        // check create correctlly.
        DisclosureQuestion foundEntity = new DisclosureQuestion(srk);
        DisclosureQuestionPK dqpk = new DisclosureQuestionPK(
                disclosureQuestionId);
        foundEntity = foundEntity.findByPrimaryKey(dqpk);

        // verifying
        assertEquals(foundEntity.getDocumentTypeId(), newEntity
                .getDocumentTypeId());
        // rollback transaction.
        srk.rollbackTransaction();
        // free resources.
        srk.freeResources();
    }
}
