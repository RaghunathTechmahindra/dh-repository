/*
 * @(#) HolidaysTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */


package com.basis100.deal.entity;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.HolidaysPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>HolidaysTest</p>
 * Express Entity class unit test: Holidays
 */
public class HolidaysTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(HolidaysTest.class);

    /**
     * Constructor function
     */
    public HolidaysTest() {
    }

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int holidaysId = _dataRepository.getInt("HOLIDAYS", "HOLIDAYID", 0);
        int expectedProvinceId = _dataRepository.getInt("HOLIDAYS", "PROVINCEID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed the input data and run the program.
        Holidays entity = new Holidays(srk);
        entity = entity.findByPrimaryKey(new HolidaysPK(holidaysId));

        // verify the running result.
        assertEquals(expectedProvinceId, entity.getProvinceId());

        // free resources
        srk.freeResources();
    }

    /**
     * test create.
     */

    // @Ignore("no data for associated table") @Test
    public void testCreate() throws Exception {

        // get input data from repository
        int holidayId = _dataRepository.getInt("HOLIDAYS", "HOLIDAYID", 0);
        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();

        // feed input data and run program.
        srk.beginTransaction();
        Holidays newEntity = new Holidays(srk);
        HolidaysPK hpk = new HolidaysPK(holidayId);
        newEntity = newEntity.findByPrimaryKey(hpk);
        newEntity.ejbRemove();
        newEntity = newEntity.create(holidayId);

        newEntity.ejbStore();

        // check create correctlly.
        Holidays foundEntity = new Holidays(srk);
        foundEntity = foundEntity.findByPrimaryKey(new HolidaysPK(holidayId));

        // verifying
        assertEquals(foundEntity.getHolidayId(), holidayId);

        // rollback transaction.
        srk.rollbackTransaction();
        // free resources.
        srk.freeResources();
    }

}
