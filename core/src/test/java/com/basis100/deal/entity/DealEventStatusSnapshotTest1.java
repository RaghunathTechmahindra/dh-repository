package com.basis100.deal.entity;

import java.io.File;
import java.io.IOException;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.AppraisalOrderPK;
import com.basis100.deal.pk.DealEventStatusSnapshotPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.ltx.unittest.util.EntityTestUtil;

public class DealEventStatusSnapshotTest1 extends FXDBTestCase{

	private IDataSet dataSetTest;	
	private DealEventStatusSnapshot entityStatusSnapshot;
	Clob clob=null;
	
	
	public DealEventStatusSnapshotTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(DealEventStatusSnapshot.class.getSimpleName() + "DataSetTest.xml"));
	}

	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
	}
	
	public void testFindByPrimaryKeySuccess() throws DataSetException,IOException, SQLException, RemoteException, FinderException{
	
		ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKeySuccess");
		 int dealId=Integer.valueOf((String)testFindByPrimaryKey.getValue(0,"DEALID"));	
		 int typeId=Integer.valueOf((String)testFindByPrimaryKey.getValue(0,"DEALEVENTSTATUSTYPEID"));			
		entityStatusSnapshot = new DealEventStatusSnapshot(srk,dealId,typeId);
		DealEventStatusSnapshotPK pk=new DealEventStatusSnapshotPK(dealId,typeId);
		entityStatusSnapshot=entityStatusSnapshot.findByPrimaryKey(pk);
	    assertEquals(dealId, pk.getId());
} 
		
	public void testCreate() throws DataSetException,IOException, SQLException, RemoteException, FinderException, CreateException{
		 ITable testCreate = dataSetTest.getTable("testCreate");
		 int statusNameReplacementId=Integer.valueOf((String)testCreate.getValue(0,"STATUSNAMEREPLACEMENTID"));	
		 int dealEventStatusTypeId=Integer.valueOf((String)testCreate.getValue(0,"DEALEVENTSTATUSTYPEID"));
		 int dealId=Integer.valueOf((String)testCreate.getValue(0,"DEALID"));	
		 int copyId=Integer.valueOf((String)testCreate.getValue(0,"COPYID"));
		 srk.getExpressState().setDealInstitutionId(1);
		 
		 try {
			srk.beginTransaction();
		
		 entityStatusSnapshot = new DealEventStatusSnapshot(srk);
		 Deal deal=new Deal(srk,null,dealId, copyId);	
		 entityStatusSnapshot=entityStatusSnapshot.create(deal,dealEventStatusTypeId,statusNameReplacementId);
	    assertEquals(dealEventStatusTypeId, entityStatusSnapshot.getDealEventStatusTypeId());
	    
	    srk.rollbackTransaction();
		 } catch (JdbcTransactionException e) {				
				e.printStackTrace();
			}
	}  
	
	public void testFindByEventStatusType() throws DataSetException,IOException, SQLException, RemoteException, FinderException, CreateException{
		 ITable testFindByEventStatusType = dataSetTest.getTable("testFindByEventStatusType");
		  int dealId=Integer.valueOf((String)testFindByEventStatusType.getValue(0,"DEALID"));	
		 int typeId=Integer.valueOf((String)testFindByEventStatusType.getValue(0,"DEALEVENTSTATUSTYPEID"));
		 DealEventStatusSnapshotPK pk=new DealEventStatusSnapshotPK(dealId,typeId);
		 srk.getExpressState().setDealInstitutionId(1);
		 entityStatusSnapshot = new DealEventStatusSnapshot(srk);		 	
		 entityStatusSnapshot=entityStatusSnapshot.findByEventStatusType(pk,typeId);
	    assertEquals(dealId, pk.getId());
	} 
	
	
}
