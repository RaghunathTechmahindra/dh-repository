package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import MosSystem.Mc;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.deal.pk.ComponentSummaryPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.util.EntityTestUtil;

public class ComponentSummaryDBTest extends FXDBTestCase {
	private IDataSet dataSetTest;
    private ComponentSummary componentSummary = null;
    private Collection collection =null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public ComponentSummaryDBTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ComponentSummary.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.componentSummary = new ComponentSummary(srk);
	}
	public void testCreate() throws Exception
	{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int dealInstitutionId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));
		int dealid = Integer.parseInt((String)testCreate.getValue(0, "DEALID"));
		int copyid = Integer.parseInt((String)testCreate.getValue(0, "COPYID"));
		
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);

		ComponentSummary aComponentSummary = componentSummary.create(dealid, copyid);
		//find a ComponentLoan to verify
		ComponentSummary new_ComponentSummary = new ComponentSummary(srk, dealid, copyid);
		assertEquals(aComponentSummary, new_ComponentSummary);
		
		srk.rollbackTransaction();
	}
	/*
	 * Failure case not applicable as the create method returns an entity and sending unqualified data will result
	 * in improper end of test class and thus it is avoid.
	 */
	/*public void testfindByPrimaryKey() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByPrimaryKey = dataSetTest.getTable("testfindByPrimaryKey");
		int institutionId = Integer.parseInt((String)testfindByPrimaryKey.getValue(0, "INSTITUTIONPROFILEID"));
		int dealId = Integer.parseInt((String)testfindByPrimaryKey.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String)testfindByPrimaryKey.getValue(0, "COPYID"));
		
        srk.getExpressState().setDealInstitutionId(institutionId);
        ComponentSummary component = new ComponentSummary(srk);

        // Find by primary Key
        componentSummary = component.findByPrimaryKey(new ComponentSummaryPK(dealId, copyId));

        // Check if the data retrieved
        assertEquals(componentSummary.dealId, dealId);

        // roll-back transaction.
        srk.rollbackTransaction();
	}*/
	
	public void testfindByDeal() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByComponent = dataSetTest.getTable("testfindByDeal");
		int institutionId = Integer.parseInt((String)testfindByComponent.getValue(0, "INSTITUTIONPROFILEID"));
		int dealId = Integer.parseInt((String)testfindByComponent.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String)testfindByComponent.getValue(0, "COPYID"));
		
        srk.getExpressState().setDealInstitutionId(institutionId);
        ComponentSummary component = new ComponentSummary(srk);

        // Find by primary Key
        collection = component.findByDeal(new DealPK(dealId, copyId));

        // Check if the data retrieved
        assertNotNull(collection);

        // roll-back transaction.
        srk.rollbackTransaction();
	}
	
	@SuppressWarnings("finally")
	public boolean testfindByDealFailure() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByDealFailure = dataSetTest.getTable("testfindByDealFailure");
		int institutionId = Integer.parseInt((String)testfindByDealFailure.getValue(0, "INSTITUTIONPROFILEID"));
		int dealId = Integer.parseInt((String)testfindByDealFailure.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String)testfindByDealFailure.getValue(0, "COPYID"));
		
        srk.getExpressState().setDealInstitutionId(institutionId);

        // create the entity
        ComponentSummary component = new ComponentSummary(srk);
        try{
        // Find by primary Key
        Collection collection = component.findByDeal(new DealPK(dealId, copyId));
        }catch (Exception e) {
        	// Check if the data retrieved matches the DB
            assertNull(collection);
            return true; 
		}
        finally{
        	assertNull(collection);
        // roll-back transaction.
        srk.rollbackTransaction();
        return true;
        }
	}
}
