/*
 * @(#)ComponentMortgageTest.java May 14, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * 
 */
public class ComponentMortgageTest extends ExpressEntityTestCase implements UnitTestLogging {

 // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(ComponentLoanTest.class);

    // session resource kit
    private SessionResourceKit  _srk;
    private int dealId;
    private int copyId;
    private int instProfId;

    /**
     * Constructor function
     */
    public ComponentMortgageTest() {

    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
        dealId = _dataRepository.getInt("DEAL", "DEALId", 0);
        copyId = _dataRepository.getInt("DEAL", "COPYID", 0);
        instProfId = _dataRepository.getInt("DEAL", "INSTITUTIONPROFILEID", 0);
        _srk.getExpressState().setDealInstitutionId(instProfId);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }
    
    /**
     * Test method for {@link com.basis100.deal.entity.ComponentMortgage#findByPrimaryKey(com.basis100.deal.pk.ComponentMortgagePK)}.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        _srk.beginTransaction();
        Deal deal = new Deal(_srk, null, dealId, copyId);
        int mtgProdId = deal.getMtgProdId();
        Component comp = new Component(_srk, null);
        comp = comp.create(dealId, copyId, Mc.COMPONENT_TYPE_MORTGAGE,
                mtgProdId);
        ComponentMortgage componentMortgage = new ComponentMortgage(_srk, null);
        componentMortgage.create(comp.getComponentId(), comp.getCopyId());
        _logger.info(BORDER_START, "findByPrimaryKey");
        _logger.info("Got Values from new ComponentMortgage ::: [{}]",
                comp.getComponentId() + "::" + instProfId + "::" + copyId);

        // create the entity
        ComponentMortgage _component = new ComponentMortgage(_srk);
        // Find by primary Key
        _component = _component.findByPrimaryKey(new ComponentMortgagePK(comp.getComponentId(),
                copyId));

        _logger.info("Got Values from ComponentMortgage found::: [{}]",
                _component.getComponentId() + "::" + _component.getCopyId());

        // Check if the data retrieved matches the DB
        assertEquals(_component.getComponentId(), comp.getComponentId());
        assertEquals(_component.getCopyId(), copyId);

        _logger.info(BORDER_END, "findByPrimaryKey");
        _srk.rollbackTransaction();
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.ComponentMortgage#create(int, int, int, int, int, java.lang.String, java.lang.String)}.
     */
    @Test
    public void testCreate() throws Exception{
        _logger.info(BORDER_START, "Create - ComponentMortgage");

        // begin transaction
        _srk.beginTransaction();
        
        Deal deal = new Deal(_srk, null, dealId, copyId);
        int mtgProdId = deal.getMtgProdId();
        Component comp = new Component(_srk, null);
        comp = comp.create(dealId, copyId, Mc.COMPONENT_TYPE_MORTGAGE,
                mtgProdId);

        //create entity
        ComponentMortgage newEntity = new ComponentMortgage(_srk);
        newEntity = newEntity.create(comp.getComponentId(), copyId);
        
        
        _logger.info("Created a new ComponentMortgage ::" + newEntity.getComponentId());
        
        //create the holder entity
        ComponentMortgage foundEntity = new ComponentMortgage(_srk);
        
        //Find by primary Key
        foundEntity = foundEntity.findByPrimaryKey(new ComponentMortgagePK(newEntity.getComponentId(), copyId));
        
        // verifying the created entity
        assertEquals(foundEntity.getComponentId(), newEntity.getComponentId());
        assertEquals(foundEntity.getCopyId(), newEntity.getCopyId());
        
        _logger.info(BORDER_END, "Create - ComponentMortgage");

        // roll-back transaction.
        _srk.rollbackTransaction();

    }

}
