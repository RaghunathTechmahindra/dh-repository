/**
 * <p>Title: ProvinceTest.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2007</p>
 *
 * <p>Company: Filogix Limited Partnership
</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � Aug 29, 2007)
 *
 */
package com.basis100.deal.entity;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.basis100.deal.pk.ProvincePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>ProvinceTest</p>
 * Express Entity class unit test: Province
 */
public class ProvinceTest extends ExpressEntityTestCase
    implements UnitTestLogging {

    private final static Logger _logger = 
        LoggerFactory.getLogger(ProvinceTest.class);
    private final static String ENTITY_NAME = "PROVINCE"; 
    
    // retreived data count that is stored in UnitTestDataRepository.xml
    private static final int DATA_COUNT = 14;
    // use this value for random sequence number data
    private int testSeqForGen = 0;

    private SessionResourceKit _srk;
    
    // entity/table properties
    private int provinceId;
    private String provinceName;
    private String provinceAbbr;
    private double provinceTaxRate;

    /**
     * <p>setUp</p>
     * Setting up test case.
     */
    @Before
    public void setUp() throws Exception {
      
        _logger.info(ENTITY_NAME + " TestCase setting up ...");

        // init ResouceManager
        ResourceManager.init();
        //  init session resource kit.
        _srk = new SessionResourceKit();

        // get random seqence number 
        Double indexD = new Double(Math.random() * DATA_COUNT);
        testSeqForGen = indexD.intValue();


        //      get input data from repository
        provinceId = _dataRepository.getInt( ENTITY_NAME, "PROVINCEID", testSeqForGen);
        provinceName = _dataRepository.getString( ENTITY_NAME, "PROVINCENAME", testSeqForGen);
        provinceAbbr = _dataRepository.getString( ENTITY_NAME, "PROVINCEABBREVIATION", testSeqForGen);
        provinceTaxRate = _dataRepository.getDouble( ENTITY_NAME, "PROVINCETAXRATE", testSeqForGen);

        _logger.info("Setup done: ");
    }
    
    @After
    public void tearDown() {

      //      free resources
      _srk.freeResources( );

    }

    /**
     * test findByPrimaryKey.
     */
    @Test
    public void testFindbyPrimaryKey() throws Exception{
      
        _logger.info(BORDER_START, "findByPrimaryKey");

  
        // excute method
        Province entity = new Province(_srk);
        entity = entity.findByPrimaryKey(new ProvincePK(provinceId));
        
        // assert
        assertEqualsProperteis(entity);

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * test findByName.
     */
    @Test
    public void testFindbyName() throws Exception {
      
        _logger.info(BORDER_START, "findByName");
        
        // excute method
        Province entity = new Province(_srk);
        entity = entity.findByName(provinceName);
                  
        // assert
        assertEqualsProperteis(entity);

        _logger.info(BORDER_END, "findByName");
    }
    

    /**
     * test findByName.
     */
    @Test
    public void testGetAllProvince() throws Exception {
      
        _logger.info(BORDER_START, "getAllProvince");
        
        // get testData
        // unitTestDataRepository.xml should have
        // <entry key="Province" value="14" />
        List provincesFromTestData = new ArrayList();
        for(int i=0; i < DATA_COUNT; i++) {
            Province province = new Province(_srk);
            province.setProvinceId(_dataRepository
                                   .getInt(ENTITY_NAME, "PROVINCEID", i));
            province.setProvinceName(_dataRepository
                                     .getString( ENTITY_NAME, "PROVINCENAME", i));
            province.setProvinceAbbreviation(_dataRepository
                                             .getString(ENTITY_NAME, 
                                                        "PROVINCEABBREVIATION",
                                                        i));
            province.setProvinceTaxRate(_dataRepository
                                        .getDouble(ENTITY_NAME, 
                                                   "PROVINCETAXRATE", 
                                                   i));
            provincesFromTestData.add(province);
        }
        
        // excute method
        Province entity = new Province(_srk);
        List provinces = entity.getAllProvince();
                  
        // assert
        for(int i=0; i < DATA_COUNT; i++)
        {
            Province provinceFromTestData = (Province) provincesFromTestData.get(i);
            Province province = (Province)provinces.get(i);
            
            assertEquals(provinceFromTestData.getProvinceId(),
                         province.getProvinceId());
            assertEquals(provinceFromTestData.getProvinceName(),
                         province.getProvinceName());
            assertEquals(provinceFromTestData.getProvinceAbbreviation(),
                         province.getProvinceAbbreviation());
            assertEquals(provinceFromTestData.getProvinceTaxRate(),
                         province.getProvinceTaxRate(), Double.MIN_VALUE);
        }

        _logger.info(BORDER_END, "getAllProvince");
    }
    
    /**
     * test findbyAbbreviation.
     */
    @Test
    public void testFindbyAbbreviation() throws Exception {
      
        _logger.info(BORDER_START, "FindbyAbbreviation");

        // excute method
        Province entity = new Province(_srk);
        entity = entity.findByAbbreviation(provinceAbbr);
                  
        // assert
        assertEqualsProperteis(entity);

        _logger.info(BORDER_END, "FindbyAbbreviation");
    }
    
    /**
     * <p>assertEqualsProperteis</p>
     * <p>compare retreived entity data with preloaded xml data</p>
     * 
     * @param entity Province
     * @throws Exception
     */
    private void assertEqualsProperteis(Province entity) throws Exception {
      
        assertEquals(provinceId, entity.getProvinceId());
        assertEquals(provinceName, entity.getProvinceName());
        assertEquals(provinceAbbr, entity.getProvinceAbbreviation());
        assertEquals(provinceTaxRate, entity.getProvinceTaxRate(), Double.MIN_VALUE);

    }
  
}
