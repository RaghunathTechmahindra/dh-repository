/**
 * <p>Title: RegionProfileTest.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2007</p>
 *
 * <p>Company: Filogix Limited Partnership
</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � Aug 29, 2007)
 *
 */
package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.basis100.deal.pk.RegionProfilePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>RegionProfileTest</p>
 * Express Entity class unit test: RegionProfile
 */
public class RegionProfileTest extends ExpressEntityTestCase 
    implements UnitTestLogging {
  
    private final static Logger _logger = 
        LoggerFactory.getLogger(RegionProfileTest.class);
    private final static String ENTITY_NAME = "REGIONPROFILE"; 
    
    // retreived data count that is stored in UnitTestDataRepository.xml
    private static final int DATA_COUNT = 6;

    private SessionResourceKit _srk;

    // use this value for random sequence number data
    private int testSeqForGen = 0;

    // properties
    private int _rerionProfileId;
    private String _regionName;
    private int _contactId;
    private int _regionMangerUserId;
    private int _profileStatusId;
    private String _rpShortName;
    private String _rpBusinessId;
    private int instProfId;

    /**
     * <p>setUp</p>
     * Setting up test case.
     */
    @Before
    public void setUp() throws Exception {

        _logger.info(BORDER_START, ENTITY_NAME + " TestCase setting up ...");

        //  init session resource kit.
        ResourceManager.init();
        //  init session resource kit.
        _srk = new SessionResourceKit();

        
        // get random seqence number 
        Double indexD = new Double(Math.random() * DATA_COUNT);
        testSeqForGen = indexD.intValue();

        // retreive data from test data file
        _rerionProfileId 
            = _dataRepository.getInt( ENTITY_NAME, "REGIONPROFILEID", testSeqForGen);
        _regionName = _dataRepository.getString( ENTITY_NAME, "REGIONNAME", testSeqForGen);
        _regionMangerUserId 
            = _dataRepository.getInt( ENTITY_NAME, "REGIONMANAGERUSERID", testSeqForGen);
        _contactId = _dataRepository.getInt( ENTITY_NAME, "CONTACTID", testSeqForGen);
        _profileStatusId 
            = _dataRepository.getInt( ENTITY_NAME, "PROFILESTATUSID", testSeqForGen);
        _rpShortName = _dataRepository.getString( ENTITY_NAME, "RPSHORTNAME", testSeqForGen);
        _rpBusinessId 
            = _dataRepository.getString( ENTITY_NAME, "RPBUSINESSID", testSeqForGen);
        instProfId 
            = _dataRepository.getInt( ENTITY_NAME, "INSTITUTIONPROFILEID", testSeqForGen);
  
        _logger.info(BORDER_END, "Setting up Done");
    }

    @After
    public void tearDown() {
      
      //      free resources
      _srk.freeResources( );

    }

    /**
     * test findByPrimaryKey.
     */
    /*@Test
    public void testFindByPrimaryKey()  throws Exception {
      
        _logger.info(BORDER_START, "findByPrimaryKey");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
        
        // excute method
        RegionProfile entity = new RegionProfile(_srk);
        entity = entity.findByPrimaryKey(new RegionProfilePK(_rerionProfileId));
              
        // assertEquqls
        assertEquals(entity.getRegionProfileId(), _rerionProfileId);
        assertEquals(entity.getRegionName(), _regionName);
        assertEquals(entity.getRegionManagerUserId(), _regionMangerUserId);
        assertEquals(entity.getContactId(), _contactId);
        assertEquals(entity.getProfileStatusId(), _profileStatusId);
        assertEquals(entity.getRPShortName(), _rpShortName);
        assertEquals(entity.getRPBusinessId(), _rpBusinessId);
        
        _logger.info(BORDER_END, "findByPrimaryKey");
    }*/
    @Test
    public void testTBR(){
        System.out.println("test");
} 
  
}
