package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;

public class UserProfileTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private UserProfile userProfile;

	public UserProfileTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(UserProfile.class.getSimpleName() + "DataSetTest.xml"));
	}

	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
		//return DatabaseOperation.UPDATE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.userProfile = new UserProfile(srk);		
	}

	public void testValidateNewPassword() throws DataSetException {
		ITable testValidateNewPassword = dataSetTest.getTable("testValidateNewPassword");
		int rowCount = testValidateNewPassword.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String password = (String) testValidateNewPassword.getValue(i, "password");
			String returned = userProfile.validateNewPassword(password, this.srk.getLanguageId());
			assertEquals("", returned);
		}
	}

	public void testValidateNewPasswordFailure() throws DataSetException {
		ITable testValidateNewPassword = dataSetTest.getTable("testValidateNewPasswordFailure");
		int rowCount = testValidateNewPassword.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String password = (String) testValidateNewPassword.getValue(i, "password");
			String returned = userProfile.validateNewPassword(password, this.srk.getLanguageId());
			assertFalse("".equals(returned));
		}
	}
		
		public void testCreate() throws Exception{	
			srk.beginTransaction();
			ITable testCreate = dataSetTest.getTable("testCreate");
			int userTypeId = Integer.parseInt((String)testCreate.getValue(0,"userTypeId"));	
			int userProfileId = Integer.parseInt((String)testCreate.getValue(0,"userProfileId"));
			int groupProfileId = Integer.parseInt((String)testCreate.getValue(0,"groupProfileId"));
			String userLogin=(String)testCreate.getValue(0,"userLogin");
			String standardAccess=(String)testCreate.getValue(0,"standardAccess");
			String bilingual=(String)testCreate.getValue(0,"bilingual");
			int secondApproverUserId = Integer.parseInt((String)testCreate.getValue(0,"secondApproverUserId"));	
			int jointApproverUserId = Integer.parseInt((String)testCreate.getValue(0,"jointApproverUserId"));	
			int contactId = Integer.parseInt((String)testCreate.getValue(0,"contactId"));	
			int profileStatusId = Integer.parseInt((String)testCreate.getValue(0,"profileStatusId"));	
			int partnerUserId = Integer.parseInt((String)testCreate.getValue(0,"partnerUserId"));	
			int standIdUserId = Integer.parseInt((String)testCreate.getValue(0,"standIdUserId"));
			String taskAlert=(String)testCreate.getValue(0,"taskAlert");
			int institutionId = Integer.parseInt((String)testCreate.getValue(0,"institutionId"));	
			srk.getExpressState().setDealInstitutionId(0);		
			userProfile=userProfile.create(userProfileId,userTypeId,groupProfileId,userLogin,standardAccess,bilingual,secondApproverUserId,
					jointApproverUserId,contactId,profileStatusId,partnerUserId,standIdUserId,taskAlert,institutionId);			
			srk.rollbackTransaction();			
		}
		
		public void testFindByAdministratorsInGroup() throws DataSetException, DupCheckActionException, RemoteException, FinderException{
			ITable testFindByAdministratorsInGroup = dataSetTest.getTable("testFindByAdministratorsInGroup");
			Vector vector = null;
			int groupProfileId = Integer.parseInt((String)testFindByAdministratorsInGroup.getValue(0,"groupProfileId"));
			int adminType = Integer.parseInt((String)testFindByAdministratorsInGroup.getValue(0,"adminType"));
			int institutionId = Integer.parseInt((String)testFindByAdministratorsInGroup.getValue(0,"institutionId"));
		    vector=userProfile.findByAdministratorsInGroup(groupProfileId,adminType,true,institutionId);	  	    
		    assertNotNull(vector);
		} 
		
		public void testFindByContactId() throws DataSetException, DupCheckActionException, RemoteException, FinderException{
			ITable testFindByAdministratorsInGroup = dataSetTest.getTable("testFindByContactId");
			int contactId = Integer.parseInt((String)testFindByAdministratorsInGroup.getValue(0,"contactId"));
			int institutionId = Integer.parseInt((String)testFindByAdministratorsInGroup.getValue(0,"institutionId"));
			userProfile=userProfile.findByContactId(contactId,institutionId);	  	    
		    assertEquals(contactId, userProfile.getContactId());
		} 	

		public void testFindByContactIdFailure() throws DataSetException, DupCheckActionException, RemoteException, FinderException{
			ITable testFindByAdministratorsInGroup = dataSetTest.getTable("testFindByContactIdFailure");
			boolean status=false;
			try{
			int contactId = Integer.parseInt((String)testFindByAdministratorsInGroup.getValue(0,"contactId"));
			int institutionId = Integer.parseInt((String)testFindByAdministratorsInGroup.getValue(0,"institutionId"));
			userProfile=userProfile.findByContactId(contactId,institutionId);	
			status=true;
			}catch(Exception e){
				status=false;
			}
		    assertEquals(false,status);
		} 	
		
		public void testFindByLoginId() throws DataSetException, DupCheckActionException, RemoteException, FinderException{
			ITable testFindByLoginId = dataSetTest.getTable("testFindByLoginId");
			String loginId=(String)testFindByLoginId.getValue(0,"loginId");
			userProfile=userProfile.findByLoginId(loginId);
			assertEquals(loginId, userProfile.getUserLogin());
		} 
		
		public void testFindByLoginIdAllData() throws DataSetException, DupCheckActionException, RemoteException, FinderException{
			ITable testFindByLoginIdAllData = dataSetTest.getTable("testFindByLoginIdAllData");
			List list= null;
			String loginId=(String)testFindByLoginIdAllData.getValue(0,"loginId");
			list=userProfile.findByLoginIdAllData(loginId);
			assertNotNull(list);
		} 
		
		public void testFindPreviousRecordByUserId() throws DataSetException, DupCheckActionException, RemoteException, FinderException{
			ITable testFindPreviousRecordByUserId = dataSetTest.getTable("testFindPreviousRecordByUserId");
			Collection  records= null;
			String userLogin=(String)testFindPreviousRecordByUserId.getValue(0,"userLogin");
			UserProfile.UserSecurityPolicy oi=new UserProfile(srk).new UserSecurityPolicy(srk);
			records=oi.findPreviousRecordByUserId(userLogin);
			assertNotNull(records);
		} 
		
		public void testFindByChangeUserProfile() throws DataSetException, DupCheckActionException, RemoteException, FinderException{
			ITable testFindByChangeUserProfile = dataSetTest.getTable("testFindByChangeUserProfile");
			Collection  records= null;
			String userLogin=(String)testFindByChangeUserProfile.getValue(0,"userLogin");
			int institutionId = Integer.parseInt((String)testFindByChangeUserProfile.getValue(0,"institutionId"));
			userProfile=userProfile.findByChangeUserProfile(userLogin,institutionId);
			assertEquals(userLogin, userProfile.getUserLogin());
		} 
}
	
	


