package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;

public class MtgProdTest1 extends FXDBTestCase {
	private String INIT_XML_DATA = "MtgProdDataSet.xml";
	private MtgProd mtgProd;
	private IDataSet dataSetTest;

	public MtgProdTest1(String name) throws DataSetException, IOException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"MtgProdDataSetTest.xml"));
	}


	@Override
	protected void setUp() throws Exception {
		super.setUp();
		int id = Integer.parseInt(dataSetTest.getTable("mtgProdTest1")
				.getValue(0, "id").toString());

		mtgProd = new MtgProd(srk, CalcMonitor.getMonitor(srk), id);

	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	public void testFindByMPBusinessId() throws RemoteException,
			FinderException, DataSetException {
		String mpBusinessId = dataSetTest.getTable("testFindByMPBusinessId")
				.getValue(0, "mpBusinessId").toString();
		MtgProd prod = mtgProd.findByMPBusinessId(mpBusinessId);
		assertNotNull(prod);
		assertEquals(mpBusinessId, prod.getMPBusinessId());
	}

	public void testFindByPrimaryKey() throws RemoteException, FinderException,
			NumberFormatException, DataSetException {
		int mtgProdId = Integer.parseInt(dataSetTest
				.getTable("testFindByPrimaryKey").getValue(0, "mtgProdId")
				.toString());
		MtgProdPK mtgProdPK = new MtgProdPK(mtgProdId);
		MtgProd prod = mtgProd.findByPrimaryKey(mtgProdPK);
		assertNotNull(prod);
		assertEquals(mtgProdId, prod.mtgProdId);
	}

	public void testFindByPrimaryKeyAndDealId() throws RemoteException,
			FinderException, NumberFormatException, DataSetException {
		int dealId = Integer.parseInt(dataSetTest
				.getTable("testFindByPrimaryKeyAndDealId")
				.getValue(0, "dealId").toString());
		int dealInstitutionId = Integer.parseInt(dataSetTest
				.getTable("testFindByPrimaryKeyAndDealId")
				.getValue(0, "dealInstitutionId").toString());
		MtgProd prod = mtgProd.findByPrimaryKey(dealId, dealInstitutionId);
		assertNotNull(prod);
		assertEquals(dealId, prod.pk.getId());
	}

	public void testFindByLenderAndPaymentTerm() throws Exception {
		int lenderProfileId = Integer.parseInt(dataSetTest
				.getTable("testFindByLenderAndPaymentTerm")
				.getValue(0, "lenderProfileId").toString());
		int paymentTermId = Integer.parseInt(dataSetTest
				.getTable("testFindByLenderAndPaymentTerm")
				.getValue(0, "paymentTermId").toString());
		Collection list = mtgProd.findByLenderAndPaymentTerm(lenderProfileId,
				paymentTermId);
		assertNotNull(list);
		assertEquals(true, list.size() > 0);
		for (Object obj : list) {
			MtgProd prod = (MtgProd) obj;
			assertEquals(paymentTermId, prod.getPaymentTermId());
		}
	}

	public void testFindByLenderAndPaymentTermFailure() throws Exception {
		int lenderProfileId = Integer.parseInt(dataSetTest
				.getTable("testFindByLenderAndPaymentTermFailure")
				.getValue(0, "lenderProfileId").toString());
		int paymentTermId = Integer.parseInt(dataSetTest
				.getTable("testFindByLenderAndPaymentTermFailure")
				.getValue(0, "paymentTermId").toString());
		Collection list = mtgProd.findByLenderAndPaymentTerm(lenderProfileId,
				paymentTermId);
		assertNotNull(list);
		assertEquals(true, list.size() <= 0);
	}

	public void testFindByPricingProfile() throws Exception {
		int pricingProfileId = Integer.parseInt(dataSetTest
				.getTable("testFindByPricingProfile")
				.getValue(0, "pricingProfileId").toString());

		Collection list = mtgProd.findByPricingProfile(pricingProfileId);
		assertNotNull(list);
		assertEquals(true, list.size() > 0);
		for (Object obj : list) {
			MtgProd prod = (MtgProd) obj;
			assertEquals(pricingProfileId, prod.getPricingProfileId());
		}
	}

	public void testFindByPricingProfileFailure() throws Exception {
		int pricingProfileId = Integer.parseInt(dataSetTest
				.getTable("testFindByPricingProfileFailure")
				.getValue(0, "pricingProfileId").toString());

		Collection list = mtgProd.findByPricingProfile(pricingProfileId);
		assertNotNull(list);
		assertEquals(true, list.size() <= 0);
	}

	public void testFindByLenderAndInterestType() throws Exception {
		// int lenderProfileId, int intType
		int lenderProfileId = Integer.parseInt(dataSetTest
				.getTable("testFindByLenderAndInterestType")
				.getValue(0, "lenderProfileId").toString());
		int intType = Integer.parseInt(dataSetTest
				.getTable("testFindByLenderAndInterestType")
				.getValue(0, "intType").toString());

		Collection list = mtgProd.findByLenderAndInterestType(lenderProfileId,
				intType);
		assertNotNull(list);
		assertEquals(true, list.size() > 0);
		for (Object obj : list) {
			MtgProd prod = (MtgProd) obj;
			assertEquals(intType, prod.getInterestTypeId());
		}
	}

	public void testFindByLenderAndInterestTypeFailure() throws Exception {
		int lenderProfileId = Integer.parseInt(dataSetTest
				.getTable("testFindByLenderAndInterestTypeFailure")
				.getValue(0, "lenderProfileId").toString());
		int intType = Integer.parseInt(dataSetTest
				.getTable("testFindByLenderAndInterestTypeFailure")
				.getValue(0, "intType").toString());

		Collection list = mtgProd.findByLenderAndInterestType(lenderProfileId,
				intType);
		assertNotNull(list);
		assertEquals(true, list.size() <= 0);
	}

	public void testFindDefaultFor() throws RemoteException, FinderException {
		LenderProfile profile = new LenderProfile(srk);
		int id = mtgProd.getMtgProdId();
		profile.setInstitutionProfileId(2);
		MtgProd prod = mtgProd.findDefaultFor(profile);
		assertNotNull(prod);
	}

	public void testFindRfdTypeByPricingProfileId() throws Exception {
		int rfdtype = Integer.parseInt(dataSetTest
				.getTable("testFindRfdTypeByPricingProfileId")
				.getValue(0, "rfdtype").toString());

		rfdtype = mtgProd.findRfdTypeByPricingProfileId(rfdtype);
		assertNotNull(rfdtype);
	}

	public void testFindRfdTypeByPricingProfileIdFailure() throws Exception {
		int rfdtype = Integer.parseInt(dataSetTest
				.getTable("testFindRfdTypeByPricingProfileIdFailure")
				.getValue(0, "rfdtype").toString());
		rfdtype = mtgProd.findRfdTypeByPricingProfileId(rfdtype);
		assertEquals(0, rfdtype);
	}

	public void testCreatePrimaryKey() throws CreateException,
			JdbcTransactionException {
		srk.beginTransaction();
		MtgProdPK mtgProdPK = mtgProd.createPrimaryKey();
		assertNotNull(mtgProdPK);
		srk.rollbackTransaction();
	}

}
