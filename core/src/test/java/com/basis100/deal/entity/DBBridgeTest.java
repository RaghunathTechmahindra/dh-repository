package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.DealPK;

public class DBBridgeTest extends FXDBTestCase{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(DBBridgeTest.class);
    
    //Initial data to be loaded before each test case.
	private final static String INIT_XML_DATA = "DBBridgeTestDataSet.xml";
	private IDataSet dataSetTest;
	
	/**
     * Constructor function
     */
    public DBBridgeTest(String name)  throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource(DBBridgeTest.class.getSimpleName() + "DataSetTest.xml"));
    }
    
    @Override
	protected IDataSet getDataSet() throws Exception {
    	return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}
    
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
        return DatabaseOperation.INSERT;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.DELETE;
    }

    /**
     * Test method for Bbridge#findByDeal
     */
    @Test
    public void testFindByDeal() throws Exception {

    	 _logger.info(" Start testFindByDeal");
    	 
    	 ITable testFindByDeal = dataSetTest.getTable("testFindByDeal");
    	int  dealId = Integer.parseInt((String)testFindByDeal.getValue(0,"dealId"));
     	int  copyId = Integer.parseInt((String)testFindByDeal.getValue(0,"copyId"));
     	int  bridgeId = Integer.parseInt((String)testFindByDeal.getValue(0,"bridgeId"));
  		int  instProfId = Integer.parseInt((String)testFindByDeal.getValue(0,"instProfId"));
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
    	 
        Bridge bridge = new Bridge(srk, null);
        DealPK dealPK = new DealPK(dealId, copyId);
        
        bridge = bridge.findByDeal(dealPK);

        assert bridge != null;
        assertEquals(dealId, bridge.getDealId());
        assertEquals(copyId, bridge.getCopyId());
        assertEquals(bridgeId, bridge.getBridgeId());
        
        _logger.info(" End testFindByDeal");
    }
    
}
