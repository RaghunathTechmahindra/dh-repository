package com.basis100.deal.entity;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.AppraisalOrderPK;
import com.basis100.deal.pk.ArchivedLoanApplicationPK;
import com.basis100.deal.pk.ArchivedLoanDecisionPK;
import com.basis100.deal.pk.AssetPK;
import com.basis100.deal.pk.AssetTypePK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.util.EntityTestUtil;

public class AssetTypeDbTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private AssetType assetType;	
	public AssetTypeDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(AssetType.class.getSimpleName() + "DataSetTest.xml"));
	}


	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
		}

	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		assetType = new AssetType(srk);		
	}
   public void testCreate() throws Exception{
		try {			
			srk.beginTransaction();
			ITable testCreate = dataSetTest.getTable("testCreate");	
		    int assetId=Integer.valueOf((String)testCreate.getValue(0,"ASSETTYPEID"));
		    String assetDescrption=(String)testCreate.getValue(0,"ASSETTYPEDESCRIPTION");
		    int netWorth=Integer.valueOf((String)testCreate.getValue(0,"NETWORTHINCLUSION"));
		    srk.getExpressState().setDealInstitutionId(0);		    
		    assetType=assetType.create(assetId,assetDescrption,netWorth);		   
		    assertEquals(assetId,assetType.getAssetTypeId());
		    srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
	public void testFindByPrimaryKey() throws Exception{		
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByPrimaryKey");
		int assetTypeId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"assetTypeId"));	
		srk.getExpressState().setDealInstitutionId(1);		
		AssetTypePK  pk = new AssetTypePK(assetTypeId);
		assetType=assetType.findByPrimaryKey(pk);		
		assertEquals(assetTypeId, pk.getId());		
	}   
  public void testFindByPrimaryKeyFailure() throws Exception{		
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByPrimaryKeyFailure");
		boolean status=false;
		try{
		int assetTypeId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"assetTypeId"));	
		srk.getExpressState().setDealInstitutionId(1);		
		AssetTypePK  pk = new AssetTypePK(assetTypeId);
		assetType=assetType.findByPrimaryKey(pk);	
		status=true;
		}catch(Exception e){
			status=false;
		}
		assertEquals(false,status);		
	}  	
	
}
