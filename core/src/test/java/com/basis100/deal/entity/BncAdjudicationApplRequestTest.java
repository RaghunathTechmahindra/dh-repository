/*
 * @(#)BncAdjudicationApplRequestTest.java Sep 21, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.BncAdjudicationApplRequestPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * 
 * @author amalhi
 * 
 * Junit for entity BncAdjudicationApplRequest
 * 
 */
public class BncAdjudicationApplRequestTest extends ExpressEntityTestCase
        implements UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(BncAdjudicationApplRequestTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public BncAdjudicationApplRequestTest() {

    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.BncAdjudicationApplRequest#findByPrimaryKey(com.basis100.deal.pk.BncAdjudicationApplRequestPK)}.
     */
    @Ignore("no data for associated table")
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int requestId = _dataRepository.getInt("BncAdjudicationApplRequest",
                "requestId", 0);
        int instProfId = _dataRepository.getInt("BncAdjudicationApplRequest",
                "INSTITUTIONPROFILEID", 0);
        int applicantNumber = _dataRepository.getInt(
                "BncAdjudicationApplRequest", "applicantNumber", 0);
        int copyId = _dataRepository.getInt("BncAdjudicationApplRequest",
                "copyId", 0);

        _logger.info(BORDER_START,
                "BncAdjudicationApplRequest - testFindByPrimaryKey");
        _logger.info("Got Values from new BncAdjudicationApplRequest ::: [{}]",
                requestId + "::" + instProfId + "::" + applicantNumber + "::"
                        + copyId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        BncAdjudicationApplRequest _bncAdjudicationApplRequest = new BncAdjudicationApplRequest(
                _srk);

        // Find by primary key
        _bncAdjudicationApplRequest = _bncAdjudicationApplRequest
                .findByPrimaryKey(new BncAdjudicationApplRequestPK(requestId,
                        applicantNumber, copyId));

        _logger
                .info(
                        "Got Values from new BncAdjudicationApplicantResponse ::: [{}]",
                        _bncAdjudicationApplRequest.getRequestId()
                                + "::"
                                + _bncAdjudicationApplRequest
                                        .getApplicantNumber() + "::"
                                + _bncAdjudicationApplRequest.getCopyId());

        // Check if the data retrieved matches the DB
        assertEquals(_bncAdjudicationApplRequest.getRequestId(), requestId);
        assertEquals(_bncAdjudicationApplRequest.getApplicantNumber(),
                applicantNumber);
        assertEquals(_bncAdjudicationApplRequest.getCopyId(), copyId);

        _logger.info(BORDER_END,
                "BncAdjudicationApplRequest - testFindByPrimaryKey");
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.BncAdjudicationApplRequest#create(com.basis100.deal.pk.BncAdjudicationApplRequestPK, int)}.
     */
    @Ignore("no data for associated table")
    @Test
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create - BncAdjudicationApplRequest");

        // get input data from repository
        int instProfId = _dataRepository.getInt("BncAdjudicationApplRequest",
                "INSTITUTIONPROFILEID", 0);
        int requestId = _dataRepository.getInt("AdjudicationApplicantRequest",
                "requestId", 0);
        int applicantNumber = _dataRepository.getInt(
                "AdjudicationApplicantRequest", "applicantNumber", 0);
        int copyId = _dataRepository.getInt("AdjudicationApplicantRequest",
                "copyId", 0);
        int requestedCreditBureauNameId = 1;

        _logger.info("Got Values from new BncAdjudicationApplRequest ::: [{}]",
                instProfId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // begin transaction
        _srk.beginTransaction();

        // Create the initial entity
        BncAdjudicationApplRequest newEntity = new BncAdjudicationApplRequest(
                _srk);

        // call create method of entity
        newEntity = newEntity.create(new BncAdjudicationApplRequestPK(
                requestId, applicantNumber, copyId),
                requestedCreditBureauNameId);

        _logger.info("CREATED BncAdjudicationApplRequest ::: [{}]", newEntity
                .getRequestId()
                + "::"
                + newEntity.getApplicantNumber()
                + "::"
                + newEntity.getCopyId());

        // check create correctly by calling findByPrimaryKey
        BncAdjudicationApplRequest foundEntity = new BncAdjudicationApplRequest(
                _srk);
        foundEntity = foundEntity
                .findByPrimaryKey(new BncAdjudicationApplRequestPK(newEntity
                        .getRequestId(), newEntity.getApplicantNumber(),
                        newEntity.getCopyId()));

        // verifying the created entity
        assertEquals(foundEntity.getRequestId(), newEntity.getRequestId());
        assertEquals(foundEntity.getApplicantNumber(), newEntity
                .getApplicantNumber());
        assertEquals(foundEntity.getCopyId(), newEntity.getCopyId());

        _logger.info(BORDER_END, "Create - BncAdjudicationApplRequest");

        // roll-back transaction.
        _srk.rollbackTransaction();
    }

}
