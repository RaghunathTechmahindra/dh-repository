package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.LenderProfilePK;
import com.basis100.deal.pk.LiabilityPK;
import com.basis100.deal.pk.PricingProfilePK;
import com.basis100.resources.SessionResourceKit;

public class PricingProfileTest1 extends FXDBTestCase {
	private IDataSet dataSetTest;
    private PricingProfile pricingProfile = null;
    private PricingProfilePK pricingProfilepk = null;
    CalcMonitor dcm =null;
    SessionResourceKit srk= new SessionResourceKit();
    List l = null;
	
	public PricingProfileTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(PricingProfile.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(PricingProfile.class.getSimpleName()+"DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.pricingProfile = new PricingProfile(srk);
	}
	
	public void testCreate() throws Exception
	{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int institutionProfileId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String)testCreate.getValue(0, "PRICINGPROFILEID"));
		int psid = Integer.parseInt((String)testCreate.getValue(0, "PRICINGSTATUSID"));
		int profileStatusId = Integer.parseInt((String)testCreate.getValue(0, "PROFILESTATUSID"));
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		pricingProfilepk = new PricingProfilePK(id);
		
		PricingProfile aPricingProfile = pricingProfile.create(pricingProfilepk);
		
		assertTrue(pricingProfile.getInstitutionProfileId() == institutionProfileId);;
		assertNotNull(aPricingProfile);
		
		srk.rollbackTransaction();
	}
	
	public void testCreatePrimaryKey() throws Exception
	{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreatePrimaryKey");
		int institutionProfileId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String)testCreate.getValue(0, "PRICINGPROFILEID"));
		
		pricingProfile = new PricingProfile(srk);
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		pricingProfilepk = pricingProfile.createPrimaryKey();
		assertNotNull(pricingProfilepk);
		
		srk.rollbackTransaction();
	}
	
	public void testfindLatestInventoryByPricingProfileId() throws Exception{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testfindLatestInventoryByPricingProfileId");
		int institutionProfileId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String)testCreate.getValue(0, "PRICINGPROFILEID"));
		PricingRateInventory pri = null;
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		
		pri = pricingProfile.findLatestInventoryByPricingProfileId(id);
		
		assertTrue(pricingProfile.getInstitutionProfileId() == institutionProfileId);
		assertNotNull(pri);
		
		srk.rollbackTransaction();
		
	}
	public void testfindByPrimeIndexRateProfileId() throws Exception{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testfindByPrimeIndexRateProfileId");
		int institutionProfileId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String)testCreate.getValue(0, "PRIMEINDEXRATEPROFILEID"));
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		
		l = pricingProfile.findByPrimeIndexRateProfileId(id);
		
		assert(l.size()>0);
		srk.rollbackTransaction();
		
	}
	public void testfindByRateCodeDescription() throws Exception{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testfindByRateCodeDescription");
		int institutionProfileId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));
		String rateCodeDesc = (String)testCreate.getValue(0, "RATECODEDESCRIPTION");
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		
		l = pricingProfile.findByRateCodeDescription(rateCodeDesc);
		
		assert(l.size()>0);
		srk.rollbackTransaction();
		
	}
	public void testfindByRateCode() throws Exception{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testfindByRateCode");
		int institutionProfileId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));
		String ratecode = (String)testCreate.getValue(0, "RATECODE");
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		
		l = pricingProfile.findByRateCode(ratecode);
		
		assert(l.size()>0);
		srk.rollbackTransaction();
		
	}
	public void testfindByPricingRateInventory() throws Exception{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testfindByPricingRateInventory");
		int institutionProfileId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));
		int priid = Integer.parseInt((String)testCreate.getValue(0, "PRICINGRATEINVENTORYID"));
		PricingProfile pricingProfile1 =null;
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		
		pricingProfile1 = pricingProfile.findByPricingRateInventory(priid);
		
		assertNotNull(pricingProfile1);
		srk.rollbackTransaction();
		
	}
	
	
	
}
