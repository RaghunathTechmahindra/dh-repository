package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.LenderProfilePK;
import com.basis100.deal.pk.LiabilityPK;
import com.basis100.resources.SessionResourceKit;

public class LiabilityTest1 extends FXDBTestCase {
	private IDataSet dataSetTest;
    private Liability liability = null;
    @SuppressWarnings("unused")
	private BorrowerPK pk = null;
    @SuppressWarnings("rawtypes")
	private Collection collection = null;
    private Collection collection1 = null;
    CalcMonitor dcm =null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public LiabilityTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(Liability.class.getSimpleName()+"DataSetTest.xml"));
	}
	

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		dcm = CalcMonitor.getMonitor(srk);
		this.liability = new Liability(srk, dcm);
	}
	
	public void testCreate() throws Exception
	{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int institutionProfileId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String)testCreate.getValue(0, "BORROWERID"));
		int copyId = Integer.parseInt((String)testCreate.getValue(0, "COPYID"));
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		BorrowerPK pk = new BorrowerPK(id, copyId);
		
		Liability aLiability = liability.create(pk);
		
		assertTrue(aLiability.getBorrowerId() == id);
		assertTrue(aLiability.getCopyId()== copyId);
		
		srk.rollbackTransaction();
	}
	
	
	public void testfindNonMortgageTypes() throws Exception
	{
		srk.beginTransaction();
		ITable testfindNonMortgageTypes = dataSetTest.getTable("testfindNonMortgageTypes");
		int id = Integer.parseInt((String)testfindNonMortgageTypes.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String)testfindNonMortgageTypes.getValue(0, "COPYID"));
		
		DealPK pk = new DealPK(id, copyId);
		collection = liability.findNonMortgageTypes(pk);
		assertTrue(collection.size()>1);
		srk.rollbackTransaction();
	}
	
	public void testfindNonMortgageTypesFailure() throws Exception
	{
		srk.beginTransaction();
		ITable testfindNonMortgageTypesFailure = dataSetTest.getTable("testfindNonMortgageTypesFailure");
		int id = Integer.parseInt((String)testfindNonMortgageTypesFailure.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String)testfindNonMortgageTypesFailure.getValue(0, "COPYID"));
		
		DealPK pk = new DealPK(id, copyId);
		
		collection = liability.findNonMortgageTypes(pk);
		assertTrue(collection.size()==0);
		srk.rollbackTransaction();
	}
	

	public void findByDealAndPayoffType() throws Exception
	{
		srk.beginTransaction();
		ITable findByDealAndPayoffType = dataSetTest.getTable("findByDealAndPayoffType");
		int id = Integer.parseInt((String)findByDealAndPayoffType.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String)findByDealAndPayoffType.getValue(0, "COPYID"));
		int payofftype =  Integer.parseInt((String)findByDealAndPayoffType.getValue(0, "LIABILITYPAYOFFTYPEID"));
		
		DealPK pk = new DealPK(id, copyId);
		
		collection = liability.findByDealAndPayoffType(pk, payofftype);
		
		assertTrue(collection.size()>1);
		
		srk.rollbackTransaction();
	}
	
	
	public void testfindByDealAndPayoffTypeFailure() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByDealAndPayoffTypeFailure = dataSetTest.getTable("testfindByDealAndPayoffTypeFailure");
		int id = Integer.parseInt((String)testfindByDealAndPayoffTypeFailure.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String)testfindByDealAndPayoffTypeFailure.getValue(0, "COPYID"));
		int payofftype =  Integer.parseInt((String)testfindByDealAndPayoffTypeFailure.getValue(0, "LIABILITYPAYOFFTYPEID"));
		
		DealPK pk = new DealPK(id, copyId);
		
		collection = liability.findByDealAndPayoffType(pk, payofftype);
		
		assertTrue(collection.size()==0);
		
		srk.rollbackTransaction();
	}
	
	public void testfindByDealAndPayoffTypes() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByDealAndPayoffTypes = dataSetTest.getTable("testfindByDealAndPayoffTypes");
		int id = Integer.parseInt((String)testfindByDealAndPayoffTypes.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String)testfindByDealAndPayoffTypes.getValue(0, "COPYID"));
		int payofftype0 = Integer.parseInt((String)testfindByDealAndPayoffTypes.getValue(0, "LIABILITYPAYOFFTYPEID0"));
		int payofftype1 = Integer.parseInt((String)testfindByDealAndPayoffTypes.getValue(0, "LIABILITYPAYOFFTYPEID1"));
		int payofftype2 = Integer.parseInt((String)testfindByDealAndPayoffTypes.getValue(0, "LIABILITYPAYOFFTYPEID2"));
			int payofftypes[]={payofftype0,payofftype1,payofftype2};
		DealPK pk = new DealPK(id, copyId);
		
		collection1 = liability.findByDealAndPayoffTypes(pk, payofftypes);
		
		assertTrue(collection1.size()>1);
		
		srk.rollbackTransaction();
	}
	
}
