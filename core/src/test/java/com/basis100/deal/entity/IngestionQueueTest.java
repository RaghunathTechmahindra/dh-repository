package com.basis100.deal.entity;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.dbunit.Assertion;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.NoSuchColumnException;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.datatype.TypeCastException;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.IngestionQueuePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;

public class IngestionQueueTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private IngestionQueue ingestionQueue;
    
    /**
     * Constructor function
     */
	public IngestionQueueTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(IngestionQueue.class.getSimpleName() + "DataSetTest.xml"));
	}

	

	protected DatabaseOperation getSetUpOperation() throws Exception
    {
		ingestionQueue = new IngestionQueue(srk);
		return DatabaseOperation.INSERT;
    }
	
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	private void assertEquals(DataType dataType, Object expected, Object actual) throws TypeCastException {
		assertEquals(dataType.typeCast(expected), dataType.typeCast(actual));
	}

	private void assertIngestionQueue(ITable expected, IngestionQueue actual) throws DataSetException {
		//TODO: to specify data type for the dataset xml file?
		//ITableMetaData itm = expected.getTableMetaData();

		try {
			assertEquals(DataType.INTEGER, expected.getValue(0, IngestionQueue.COLUMN_NAME_INGESTIONQUEUEID), actual.getIngestionQueueId());
		} catch (NoSuchColumnException ignore) { //for newly created entity
		}
		assertEquals(DataType.VARCHAR, expected.getValue(0, IngestionQueue.COLUMN_NAME_SOURCEAPPLICATIONID), actual.getSourceApplicationId());
		assertEquals(DataType.VARCHAR, expected.getValue(0, IngestionQueue.COLUMN_NAME_ECNILENDERID), actual.getEcniLenderId());
		assertEquals(DataType.VARCHAR, expected.getValue(0, IngestionQueue.COLUMN_NAME_SENDERCHANNEL), actual.getSenderChannel());
		assertEquals(DataType.VARCHAR, expected.getValue(0, IngestionQueue.COLUMN_NAME_RECEIVERCHANNEL), actual.getReceiverChannel());
		assertEquals(DataType.TIMESTAMP, expected.getValue(0, IngestionQueue.COLUMN_NAME_RECEIVEDTIMESTAMP), actual.getReceivedTimestamp());
		assertEquals(DataType.INTEGER, expected.getValue(0, IngestionQueue.COLUMN_NAME_INGESTIONQUEUESTATUSID), actual.getIngestionQueueStatusId());
		assertEquals(DataType.TIMESTAMP, expected.getValue(0, IngestionQueue.COLUMN_NAME_STATUSDATETIMESTAMP), actual.getStatusDateTimestamp());
		assertEquals(DataType.INTEGER, expected.getValue(0, IngestionQueue.COLUMN_NAME_RETRYCOUNTER), actual.getRetryCounter());
		assertEquals(DataType.CLOB, expected.getValue(0, IngestionQueue.COLUMN_NAME_DEALXML), actual.getDealXml());
	}

	public void testFindByPrimaryKey() throws FinderException, RemoteException, DataSetException {
		ITable expectedIngestionQueue = dataSetTest.getTable("testFindByPrimaryKey");
		Integer expectedIngestionQueueId = (Integer) DataType.INTEGER.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_INGESTIONQUEUEID));

		IngestionQueuePK actualIngestionQueuePK = new IngestionQueuePK(expectedIngestionQueueId);
		IngestionQueue actualIngestionQueue = ingestionQueue.findByPrimaryKey(actualIngestionQueuePK);

		assertIngestionQueue(expectedIngestionQueue, actualIngestionQueue);
	}

	//TODO: dbunit not support junit 4? @Test(expected = FinderException.class)
	public void testFindByPrimaryKeyNotFound() throws RemoteException, DataSetException {
		ITable expectedIngestionQueue = dataSetTest.getTable("testFindByPrimaryKeyNotFound");
		Integer expectedIngestionQueueId = (Integer) DataType.INTEGER.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_INGESTIONQUEUEID));

		IngestionQueuePK actualIngestionQueuePK = new IngestionQueuePK(expectedIngestionQueueId);
		try {
			ingestionQueue.findByPrimaryKey(actualIngestionQueuePK);
			fail();
		} catch (FinderException fe) {
		}
	}

	public void testCreate() throws CreateException, RemoteException, DataSetException, Exception {
		ITable expectedIngestionQueue = dataSetTest.getTable("testCreate");
		String expectedIngestionQueueSourceApplicationId = (String) DataType.VARCHAR.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_SOURCEAPPLICATIONID));
		String expectedIngestionQueueEcniLenderId = (String) DataType.VARCHAR.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_ECNILENDERID));
		String expectedIngestionQueueSenderChannel = (String) DataType.VARCHAR.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_SENDERCHANNEL));
		String expectedIngestionQueueReceiverChannel = (String) DataType.VARCHAR.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_RECEIVERCHANNEL));
		Timestamp expectedIngestionQueueReceivedTimestamp = (Timestamp) DataType.TIMESTAMP.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_RECEIVEDTIMESTAMP));
		Integer expectedIngestionQueueStatusId = (Integer) DataType.INTEGER.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_INGESTIONQUEUESTATUSID));
		Timestamp expectedIngestionQueueStatusDateTimestamp = (Timestamp) DataType.TIMESTAMP.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_STATUSDATETIMESTAMP));
		Integer expectedIngestionQueueRetryCounter = (Integer) DataType.INTEGER.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_RETRYCOUNTER));
		String expectedIngestionQueueDealXml = (String) DataType.CLOB.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_DEALXML));

		IngestionQueue actualIngestionQueue = ingestionQueue.create(expectedIngestionQueueSourceApplicationId, expectedIngestionQueueEcniLenderId, expectedIngestionQueueSenderChannel, expectedIngestionQueueReceiverChannel, expectedIngestionQueueReceivedTimestamp, expectedIngestionQueueStatusId, expectedIngestionQueueStatusDateTimestamp, expectedIngestionQueueRetryCounter, expectedIngestionQueueDealXml);
		//TODO: dbunit seq/genkey?
		//ITable seqid = this.getConnection().createQueryTable("seqid", "select ingestionqueuesequence.currval from dual");
		//ITable actualIngestionQueueTable = this.getConnection().createTable(IngestionQueue.TABLE_NAME);
		//ITable actualIngestionQueueTable = this.getConnection().
		ITable actualIngestionQueueTable = this.getFilteredITableFromDB(IngestionQueue.TABLE_NAME, IngestionQueue.COLUMN_NAME_INGESTIONQUEUEID + "=" + actualIngestionQueue.getIngestionQueueId(), expectedIngestionQueue);

		Assertion.assertEquals(expectedIngestionQueue, actualIngestionQueueTable);
		assertIngestionQueue(expectedIngestionQueue, actualIngestionQueue);
	}

	public void testCreateFailure() throws RemoteException, DataSetException, Exception {
		ITable expectedIngestionQueue = dataSetTest.getTable("testCreateFailure");
		String expectedIngestionQueueSourceApplicationId = (String) DataType.VARCHAR.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_SOURCEAPPLICATIONID));
		String expectedIngestionQueueEcniLenderId = (String) DataType.VARCHAR.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_ECNILENDERID));
		String expectedIngestionQueueSenderChannel = (String) DataType.VARCHAR.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_SENDERCHANNEL));
		String expectedIngestionQueueReceiverChannel = (String) DataType.VARCHAR.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_RECEIVERCHANNEL));
		Timestamp expectedIngestionQueueReceivedTimestamp = (Timestamp) DataType.TIMESTAMP.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_RECEIVEDTIMESTAMP));
		Integer expectedIngestionQueueStatusId = (Integer) DataType.INTEGER.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_INGESTIONQUEUESTATUSID));
		Timestamp expectedIngestionQueueStatusDateTimestamp = (Timestamp) DataType.TIMESTAMP.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_STATUSDATETIMESTAMP));
		Integer expectedIngestionQueueRetryCounter = (Integer) DataType.INTEGER.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_RETRYCOUNTER));
		String expectedIngestionQueueDealXml = (String) DataType.CLOB.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_DEALXML));

		try {
			ingestionQueue.create(expectedIngestionQueueSourceApplicationId, expectedIngestionQueueEcniLenderId, expectedIngestionQueueSenderChannel, expectedIngestionQueueReceiverChannel, expectedIngestionQueueReceivedTimestamp, expectedIngestionQueueStatusId, expectedIngestionQueueStatusDateTimestamp, expectedIngestionQueueRetryCounter, expectedIngestionQueueDealXml);
			fail();
		} catch (CreateException e) {
		}

	}

	public void testSetters() throws FinderException, RemoteException, DataSetException, Exception {
		ITable expectedIngestionQueue = dataSetTest.getTable("testSetters");
		Integer expectedIngestionQueueId = (Integer) DataType.INTEGER.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_INGESTIONQUEUEID));
		String expectedIngestionQueueSourceApplicationId = (String) DataType.VARCHAR.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_SOURCEAPPLICATIONID));
		String expectedIngestionQueueEcniLenderId = (String) DataType.VARCHAR.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_ECNILENDERID));
		String expectedIngestionQueueSenderChannel = (String) DataType.VARCHAR.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_SENDERCHANNEL));
		String expectedIngestionQueueReceiverChannel = (String) DataType.VARCHAR.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_RECEIVERCHANNEL));
		Timestamp expectedIngestionQueueReceivedTimestamp = (Timestamp) DataType.TIMESTAMP.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_RECEIVEDTIMESTAMP));
		Integer expectedIngestionQueueStatusId = (Integer) DataType.INTEGER.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_INGESTIONQUEUESTATUSID));
		Timestamp expectedIngestionQueueStatusDateTimestamp = (Timestamp) DataType.TIMESTAMP.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_STATUSDATETIMESTAMP));
		Integer expectedIngestionQueueRetryCounter = (Integer) DataType.INTEGER.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_RETRYCOUNTER));
		String expectedIngestionQueueDealXml = (String) DataType.CLOB.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_DEALXML));

		IngestionQueuePK actualIngestionQueuePK = new IngestionQueuePK(expectedIngestionQueueId.intValue());
		//TODO: design issue; force to assume constructor works and findByPrimaryKey works
		IngestionQueue actualIngestionQueue = new IngestionQueue(srk).findByPrimaryKey(actualIngestionQueuePK);

		actualIngestionQueue.setSourceApplicationId(expectedIngestionQueueSourceApplicationId);
		actualIngestionQueue.setEcniLenderId(expectedIngestionQueueEcniLenderId);
		actualIngestionQueue.setSenderChannel(expectedIngestionQueueSenderChannel);
		actualIngestionQueue.setReceiverChannel(expectedIngestionQueueReceiverChannel);
		actualIngestionQueue.setReceivedTimestamp(expectedIngestionQueueReceivedTimestamp);
		actualIngestionQueue.setIngestionQueueStatusId(expectedIngestionQueueStatusId);
		actualIngestionQueue.setStatusDateTimestamp(expectedIngestionQueueStatusDateTimestamp);
		//actualIngestionQueue.setStatusDateTimestamp(expectedIngestionQueueReceivedTimestamp);
		actualIngestionQueue.setRetryCounter(expectedIngestionQueueRetryCounter);
		actualIngestionQueue.setDealXml(expectedIngestionQueueDealXml);
		actualIngestionQueue.ejbStore();
		ITable actualIngestionQueueTable = this.getFilteredITableFromDB(IngestionQueue.TABLE_NAME, IngestionQueue.COLUMN_NAME_INGESTIONQUEUEID + "=" + expectedIngestionQueueId, expectedIngestionQueue);

		Assertion.assertEquals(expectedIngestionQueue, actualIngestionQueueTable);
		assertIngestionQueue(expectedIngestionQueue, actualIngestionQueue);
	}

	public void testSettersFailure() throws FinderException, DataSetException, Exception {
		ITable expectedIngestionQueue = dataSetTest.getTable("testSettersFailure");
		Integer expectedIngestionQueueId = (Integer) DataType.INTEGER.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_INGESTIONQUEUEID));
		Integer expectedIngestionQueueStatusId = (Integer) DataType.INTEGER.typeCast(expectedIngestionQueue.getValue(0, IngestionQueue.COLUMN_NAME_INGESTIONQUEUESTATUSID));

		IngestionQueuePK actualIngestionQueuePK = new IngestionQueuePK(expectedIngestionQueueId.intValue());
		//TODO: design issue; force to assume constructor works and findByPrimaryKey works
		IngestionQueue actualIngestionQueue = new IngestionQueue(srk).findByPrimaryKey(actualIngestionQueuePK);
		actualIngestionQueue.setIngestionQueueStatusId(expectedIngestionQueueStatusId);
		try {
			actualIngestionQueue.ejbStore();
			fail();
		} catch (RemoteException re) {
		}
	}
	
	
	public void testFindByStatus() throws FinderException, RemoteException, DataSetException {
		ITable testFindByStatus = dataSetTest.getTable("testFindByStatus");
		Integer ingestionQueueStatusId = (Integer) DataType.INTEGER.typeCast(testFindByStatus.getValue(0, IngestionQueue.COLUMN_NAME_INGESTIONQUEUESTATUSID));
		Integer ingestionQueueStatusId1 = (Integer) DataType.INTEGER.typeCast(testFindByStatus.getValue(0, "ingestionQueueStatusId1"));

		List<IngestionQueue> ingestionQueues = (List<IngestionQueue>)ingestionQueue.findByStatus(ingestionQueueStatusId);

		assert ingestionQueues != null;
		assert ingestionQueues.size() > 0;
		
		
		//failure case
		List<IngestionQueue> ingestionQueues1 = (List<IngestionQueue>)ingestionQueue.findByStatus(ingestionQueueStatusId1);

		assert ingestionQueues1 != null;
		assert ingestionQueues1.size() == 0;
	}
	
	public void testFindBySourceApplicationId() throws FinderException, RemoteException, DataSetException {
		ITable testFindBySourceApplicationId = dataSetTest.getTable("testFindBySourceApplicationId");
		String sourceApplicationId = (String)testFindBySourceApplicationId.getValue(0, IngestionQueue.COLUMN_NAME_SOURCEAPPLICATIONID);
		Integer ingestionQueueStatusId = (Integer) DataType.INTEGER.typeCast(testFindBySourceApplicationId.getValue(0, IngestionQueue.COLUMN_NAME_INGESTIONQUEUESTATUSID));
		Integer ingestionQueueStatusId1 = (Integer) DataType.INTEGER.typeCast(testFindBySourceApplicationId.getValue(0, "ingestionQueueStatusId1"));
		Date prior = new Date();
		
		List<IngestionQueue> ingestionQueues = (List<IngestionQueue>)ingestionQueue.findBySourceApplicationId(sourceApplicationId, ingestionQueueStatusId, prior);

		assert ingestionQueues != null;
		assert ingestionQueues.size() > 0;
		
		
		//failure case
		List<IngestionQueue> ingestionQueues1 = (List<IngestionQueue>)ingestionQueue.findBySourceApplicationId(sourceApplicationId, ingestionQueueStatusId1, prior);

		assert ingestionQueues1 != null;
		assert ingestionQueues1.size() == 0;
	}
	
	public void testCreatePrimaryKey() throws FinderException, RemoteException, DataSetException, CreateException {
		
		
		IngestionQueuePK ingestionQueuePK = ingestionQueue.createPrimaryKey();
		
		assert ingestionQueuePK != null;
		assert ingestionQueuePK.getId() != 0;
		assertEquals(IngestionQueue.COLUMN_NAME_INGESTIONQUEUEID, ingestionQueuePK.getName());
		
	}
	
	public void testCreate1() throws CreateException, RemoteException, DataSetException, Exception {
		
		ITable testCreate1 = dataSetTest.getTable("testCreate1");
		String expectedIngestionQueueSourceApplicationId = (String) DataType.VARCHAR.typeCast(testCreate1.getValue(0, IngestionQueue.COLUMN_NAME_SOURCEAPPLICATIONID));
		String expectedIngestionQueueEcniLenderId = (String) DataType.VARCHAR.typeCast(testCreate1.getValue(0, IngestionQueue.COLUMN_NAME_ECNILENDERID));
		String expectedIngestionQueueSenderChannel = (String) DataType.VARCHAR.typeCast(testCreate1.getValue(0, IngestionQueue.COLUMN_NAME_SENDERCHANNEL));
		String expectedIngestionQueueReceiverChannel = (String) DataType.VARCHAR.typeCast(testCreate1.getValue(0, IngestionQueue.COLUMN_NAME_RECEIVERCHANNEL));
		Timestamp expectedIngestionQueueReceivedTimestamp = (Timestamp) DataType.TIMESTAMP.typeCast(testCreate1.getValue(0, IngestionQueue.COLUMN_NAME_RECEIVEDTIMESTAMP));
		String expectedIngestionQueueDealXml = (String) DataType.CLOB.typeCast(testCreate1.getValue(0, IngestionQueue.COLUMN_NAME_DEALXML));
		
		srk.beginTransaction();
		
		IngestionQueue actualIngestionQueue = ingestionQueue.create(expectedIngestionQueueSourceApplicationId, expectedIngestionQueueEcniLenderId, expectedIngestionQueueSenderChannel, expectedIngestionQueueReceiverChannel, expectedIngestionQueueReceivedTimestamp, expectedIngestionQueueDealXml);
	
		assert actualIngestionQueue != null;
		assertEquals(expectedIngestionQueueSourceApplicationId, actualIngestionQueue.getSourceApplicationId());
		assertEquals(expectedIngestionQueueEcniLenderId, actualIngestionQueue.getEcniLenderId());
		assertEquals(expectedIngestionQueueSenderChannel, actualIngestionQueue.getSenderChannel());
		
		srk.rollbackTransaction();
	}
	
	public void testFindNextAvailable() throws CreateException, RemoteException, DataSetException, Exception {
		
		ITable testFindNextAvailable = dataSetTest.getTable("testFindNextAvailable");
		Integer lockedDealRetryWaitInSeconds = (Integer) DataType.INTEGER.typeCast(testFindNextAvailable.getValue(0, "lockedDealRetryWaitInSeconds"));
		
		IngestionQueue actualIngestionQueue = ingestionQueue.findNextAvailable(lockedDealRetryWaitInSeconds);
	
		assert actualIngestionQueue != null;
		
	}
	

}
