package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.DealPK;


/**
 * <p>EscrowPaymentDbTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class EscrowPaymentDbTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private EscrowPayment escrowPayment;

	public EscrowPaymentDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(EscrowPayment.class.getSimpleName() + "DataSetTest.xml"));
	}

	

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.escrowPayment = new EscrowPayment(srk);
	}
	
	public void testCreatePrimaryKey() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreatePrimaryKey");
    	int escrowPaymentId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "ESCROWPAYMENTID"));
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	
    	escrowPayment.createPrimaryKey(copyId);
    	assertEquals(escrowPaymentId, escrowPayment.getEscrowPaymentId());
    }
	
	public void testCreate() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreate");
    	int escrowPaymentId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "ESCROWPAYMENTID"));
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	srk.beginTransaction();
    	CalcMonitor dcm = null;
    	escrowPayment = escrowPayment.create(new DealPK(id,copyId));
    	EscrowPayment escrowPayment1 = new EscrowPayment(srk,dcm,escrowPayment.getEscrowPaymentId(),escrowPayment.getCopyId());
    	assertNotNull(escrowPayment1);
    	srk.rollbackTransaction();
    }
	
	public void testFindByName() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByName");
    	int escrowPaymentId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "ESCROWPAYMENTID"));
    	String description = (String)(expected.getValue(0, "ESCROWPAYMENTDESCRIPTION"));
    	
    	escrowPayment.findByName(description);
    	assertEquals(escrowPaymentId, escrowPayment.getEscrowPaymentId());
    }
	
	public void testFindByName1() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByName1");
    	int escrowPaymentId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "ESCROWPAYMENTID"));
    	String description = (String)(expected.getValue(0, "ESCROWPAYMENTDESCRIPTION"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	
    	escrowPayment.findByName(description, copyId);
    	assertEquals(escrowPaymentId, escrowPayment.getEscrowPaymentId());
    }
	
	public void testDeepCopy() throws Exception {
    	ITable expected = dataSetTest.getTable("testDeepCopy");
    	int escrowPaymentId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "ESCROWPAYMENTID"));
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	
    	escrowPayment.deepCopy();
    	assertEquals(escrowPaymentId, escrowPayment.getEscrowPaymentId());
    }
	
	public void testFindByDealAndType() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByDealAndType");
    	int escrowPaymentId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "ESCROWPAYMENTID"));
    	int pType = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "ESCROWTYPEID"));
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	
    	escrowPayment.findByDealAndType(new DealPK(id,copyId), pType);
    	assertEquals(escrowPaymentId, escrowPayment.getEscrowPaymentId());
    }
	
	public void testFindByDeal() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByDeal");
    	int escrowPaymentId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "ESCROWPAYMENTID"));
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	
    	escrowPayment.findByDeal(new DealPK(id,copyId));
    	assertEquals(escrowPaymentId, escrowPayment.getEscrowPaymentId());
    }
	
}