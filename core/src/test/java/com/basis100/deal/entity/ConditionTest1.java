package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.doctag.PrivacyClause;
import com.basis100.deal.pk.ConditionPK;
import com.basis100.resources.SessionResourceKit;
import com.sun.org.apache.bcel.internal.generic.INSTANCEOF;

public class ConditionTest1 extends FXDBTestCase {
	private IDataSet dataSetTest;
    private Condition condition = null;
    private ConditionPK conditionPK = null;
    private Collection<String> collection=null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public ConditionTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(Condition.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.condition = new Condition(srk);
	}
	public void testCreate() throws Exception
	{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int dealInstitutionId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));
		int conditionId = Integer.parseInt((String)testCreate.getValue(0, "CONDITIONID"));
		
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		conditionPK = new ConditionPK(conditionId);
		Condition aCondition = condition.create(conditionPK);
		//find a Condition to verify
		Condition new_Condition = new Condition(srk, conditionId);
		assertEquals(aCondition, new_Condition);
		
		srk.rollbackTransaction();
	}
	/*
	 * Failure case not applicable as the create method returns an entity and sending unqualified data will result
	 * in improper end of test class and thus it is avoid.
	 */
	
	/*public void testCreatePrimaryKey() throws Exception
	{
		srk.beginTransaction();
		
		conditionPK = condition.createPrimaryKey();
		
		assertNotNull(conditionPK);
		
		srk.rollbackTransaction();
	}*/
	
	/*
	 * Failure case not applicable as the create method returns an entity and sending unqualified data will result
	 * in improper end of test class and thus it is avoid.
	 */
	
	public void testfindByTypeAndDescription() throws Exception
	{
		srk.beginTransaction();
		ITable findByTypeAndDescription = dataSetTest.getTable("testfindByTypeAndDescription");
		int conditionID = Integer.parseInt((String)findByTypeAndDescription.getValue(0, "CONDITIONID"));
		String conditionDesc = (String)findByTypeAndDescription.getValue(0, "CONDITIONDESC");
		
		condition = condition.findByTypeAndDescription(conditionID, conditionDesc);
		
		assertNotNull(condition);
		
		srk.rollbackTransaction();
	}
	
	@SuppressWarnings("finally")
	public boolean testfindByTypeAndDescriptionFailure() throws Exception
	{
		srk.beginTransaction();
		ITable findByTypeAndDescription = dataSetTest.getTable("testfindByTypeAndDescriptionFailure");
		int conditionID = Integer.parseInt((String)findByTypeAndDescription.getValue(0, "CONDITIONID"));
		String conditionDesc = (String)findByTypeAndDescription.getValue(0, "CONDITIONDESC");
		try{
		condition = condition.findByTypeAndDescription(conditionID, conditionDesc);
		}catch (Exception e) {
			assertNull(condition);
			return true;
		}
		finally{		
		srk.rollbackTransaction();
		assertNull(condition);
		return true;
		}
	}
	
	public void testfindBySql() throws Exception
	{
		srk.beginTransaction();
		ITable findBySql = dataSetTest.getTable("testfindBySql");
		String sql = (String)findBySql.getValue(0, "SQL");
		
		condition = condition.findBySql(sql);
		
		assertNotNull(condition);
		
		srk.rollbackTransaction();
	}
	
	@SuppressWarnings("finally")
	public boolean testfindBySqlFailure() throws Exception
	{
		srk.beginTransaction();
		ITable findBySql = dataSetTest.getTable("testfindBySqlFailure");
		String sql = (String)findBySql.getValue(0, "SQL");
		try{
		condition = condition.findBySql(sql);
		}catch (Exception e) {
			assertNull(condition);
			return true;
		}
		finally{
			srk.rollbackTransaction();
			assertNull(condition);
			return true;
		}
	}
	
	@SuppressWarnings("unchecked")
	public void testfindByConditionType() throws Exception
	{
		srk.beginTransaction();
		ITable findByConditionType = dataSetTest.getTable("testfindByConditionType");
		int conditionId = Integer.parseInt((String)findByConditionType.getValue(0, "CONDITIONID"));
		
		collection = condition.findByConditionType(conditionId);
		
		assertNotNull(collection);
		
		srk.rollbackTransaction();
	}
	
	@SuppressWarnings({ "unchecked", "finally" })
	public boolean testfindByConditionTypeFailure() throws Exception
	{
		srk.beginTransaction();
		ITable findByConditionType = dataSetTest.getTable("testfindByConditionTypeFailure");
		int conditionId = Integer.parseInt((String)findByConditionType.getValue(0, "SQL"));
		try{
		collection = condition.findByConditionType(conditionId);
		}catch (Exception e) {
			assertNull(collection);
			return true;
		}
		finally{
		srk.rollbackTransaction();
		assertNull(collection);
		return true;
		}
	}
	
	@SuppressWarnings("unchecked")
	public void testfindAllExceptType() throws Exception
	{
		srk.beginTransaction();
		ITable findAllExceptType = dataSetTest.getTable("testfindAllExceptType");
		int conditionId = Integer.parseInt((String)findAllExceptType.getValue(0, "CONDITIONID"));
		
		collection = condition.findAllExceptType(conditionId);
		
		assertNotNull(collection);
		
		srk.rollbackTransaction();
	}
	
	@SuppressWarnings({ "unchecked", "finally" })
	public boolean testfindAllExceptTypeFailure() throws Exception
	{
		srk.beginTransaction();
		ITable findAllExceptType = dataSetTest.getTable("testfindAllExceptTypeFailure");
		int conditionId = Integer.parseInt((String)findAllExceptType.getValue(0, "CONDITIONID"));
		try{
		collection = condition.findAllExceptType(conditionId);
		}catch (Exception e) {
			assertNull(collection);
			return true;
		}finally{
			srk.rollbackTransaction();
			assertNull(collection);
			return true;
		}
	}
	
	@SuppressWarnings("unchecked")
	public void testfindBySqlColl() throws Exception
	{
		srk.beginTransaction();
		ITable findBySqlColl = dataSetTest.getTable("testfindBySqlColl");
		String sql = (String)findBySqlColl.getValue(0, "SQL");
		
		collection = condition.findBySqlColl(sql);
		
		assertNotNull(collection);
		
		srk.rollbackTransaction();
	}
	
	@SuppressWarnings({ "unchecked", "finally" })
	public boolean testfindBySqlCollFailure() throws Exception
	{
		srk.beginTransaction();
		ITable findBySqlColl = dataSetTest.getTable("testfindBySqlCollFailure");
		String sql = (String)findBySqlColl.getValue(0, "SQL");
		try{
		collection = condition.findBySqlColl(sql);
		}catch (Exception e) {
			assertNull(collection);
			return true;
		}
		finally{
		srk.rollbackTransaction();
		assertNull(collection);
		return true;
		}
	}
	
}
