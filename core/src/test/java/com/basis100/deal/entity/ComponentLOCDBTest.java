package com.basis100.deal.entity;

import java.io.IOException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import MosSystem.Mc;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.util.EntityTestUtil;

public class ComponentLOCDBTest extends FXDBTestCase {
	
	private IDataSet dataSetTest;
    private ComponentLOC componentLOC=null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public ComponentLOCDBTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ComponentLOC.class.getSimpleName()+"DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.componentLOC = new ComponentLOC(srk);
	}
	public void testCreate() throws Exception
	{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int dealInstitutionId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));
		int componentid = Integer.parseInt((String)testCreate.getValue(0, "COMPONENTID"));
		int copyid = Integer.parseInt((String)testCreate.getValue(0, "COPYID"));
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		
		ComponentLOC aComponentLOC = componentLOC.create(componentid, copyid);
		
		//find a ComponentLOC to verify
		ComponentLOC new_ComponentLOC = new ComponentLOC(srk, componentid, copyid);
		assertEquals(aComponentLOC, new_ComponentLOC);
		srk.rollbackTransaction();
	}
	/*
	 * Failure case not applicable as the create method returns an entity and sending unqualified data will result
	 * in improper end of test class and thus it is omitted.
	 */
	
	public void testfindByComponent() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByComponent = dataSetTest.getTable("testfindByComponent");
		int dealId = Integer.parseInt((String)testfindByComponent.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String)testfindByComponent.getValue(0, "COPYID"));
		int institutionId = Integer.parseInt((String)testfindByComponent.getValue(0, "INSTITUTIONPROFILEID"));
        Deal deal = new Deal(srk, null, dealId, copyId);
        srk.getExpressState().setDealInstitutionId(institutionId);
        int mtgProdId = deal.getMtgProdId();
        Component comp = new Component(srk, null);
        comp = comp.create(dealId, copyId, Mc.COMPONENT_TYPE_LOC,
                mtgProdId);

        ComponentLOC componentLoc = new ComponentLOC(srk, null);
        componentLoc.create(comp.getComponentId(), comp.getCopyId());

        // create the entity
        ComponentLOC component = new ComponentLOC(srk);

        // Find by primary Key
        component = component.findByPrimaryKey(new ComponentLOCPK(comp
                .getComponentId(), copyId));

        // Check if the data retrieved matches the DB
        assertEquals(component.getComponentId(), comp.getComponentId());
        assertEquals(component.getCopyId(), copyId);

        // roll-back transaction.
        srk.rollbackTransaction();
	}
	@SuppressWarnings("finally")
	public boolean testfindByComponentFailure() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByComponentFailure = dataSetTest.getTable("testfindByComponentFailure");
		int institutionId = Integer.parseInt((String)testfindByComponentFailure.getValue(0, "INSTITUTIONPROFILEID"));
		int dealId = Integer.parseInt((String)testfindByComponentFailure.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String)testfindByComponentFailure.getValue(0, "COPYID"));
		
        Deal deal = new Deal(srk, null, dealId, copyId);
        srk.getExpressState().setDealInstitutionId(institutionId);
        
        int mtgProdId = deal.getMtgProdId();
        Component comp = new Component(srk, null);
        comp = comp.create(dealId, copyId, Mc.COMPONENT_TYPE_LOC,
                mtgProdId);

        ComponentLOC componentLoc = new ComponentLOC(srk, null);
        componentLoc.create(comp.getComponentId(), comp.getCopyId());

        // create the entity
        ComponentLOC component = new ComponentLOC(srk);
        try{
        // Find by primary Key
        component = component.findByPrimaryKey(new ComponentLOCPK(comp
                .getComponentId(), copyId));
        }catch (Exception e) {
        	// Check if the data retrieved matches the DB
            assertNull(component);
            return true; 
		}
        finally{
        	assertNull(component);
        // roll-back transaction.
        srk.rollbackTransaction();
        return true;
        }
	}
	
}
