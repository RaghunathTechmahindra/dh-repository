package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.EmploymentHistoryPK;
import com.basis100.deal.pk.IncomePK;


/**
 * <p>EmploymentHistoryDbTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class EmploymentHistoryDbTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private EmploymentHistory employmentHistory;

	public EmploymentHistoryDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(EmploymentHistory.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(EmploymentHistory.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.employmentHistory = new EmploymentHistory(srk, 1019, 2);
	}
	
	public void testFindByCurrentFullTimeEmployment() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByCurrentFullTimeEmployment");
    	int employmentHistoryId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "EMPLOYMENTHISTORYID"));
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "BORROWERID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	employmentHistory.findByCurrentFullTimeEmployment(new BorrowerPK(id, copyId));
    	assertEquals(employmentHistoryId, employmentHistory.getEmploymentHistoryId());
    }
	
	public void testFindByCurrentEmployment() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByCurrentEmployment");
    	int employmentHistoryId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "EMPLOYMENTHISTORYID"));
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "BORROWERID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	employmentHistory.findByCurrentEmployment(new BorrowerPK(id, copyId));
    	assertEquals(employmentHistoryId, employmentHistory.getEmploymentHistoryId());
    }
	
	public void testFindByCurrentEmployment1() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByCurrentEmployment1");
    	int employmentHistoryId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "EMPLOYMENTHISTORYID"));
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "INCOMEID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	employmentHistory.findByCurrentEmployment(new IncomePK(id, copyId));
    	assertEquals(employmentHistoryId, employmentHistory.getEmploymentHistoryId());
    }
	
	public void testFindByBorrower() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByBorrower");
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "BORROWERID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	employmentHistory.findByBorrower(new BorrowerPK(id, copyId));
    	assertEquals(id, employmentHistory.getBorrowerId());
    }
	
	public void testFindByDeal() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByDeal");
    	int employmentHistoryId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "EMPLOYMENTHISTORYID"));
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	employmentHistory.findByDeal(new DealPK(id, copyId));
    	assertEquals(employmentHistoryId, employmentHistory.getEmploymentHistoryId());
    }
	
	public void testFindAllCurrent() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindAllCurrent");
    	int employmentHistoryId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "EMPLOYMENTHISTORYID"));
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	employmentHistory.findAllCurrent(new DealPK(id, copyId));
    	assertEquals(employmentHistoryId, employmentHistory.getEmploymentHistoryId());
    }
	
	public void testFindByCurrentEmployments() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByCurrentEmployments");
    	int employmentHistoryId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "EMPLOYMENTHISTORYID"));
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "BORROWERID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	employmentHistory.findByCurrentEmployments(new BorrowerPK(id, copyId));
    	assertEquals(employmentHistoryId, employmentHistory.getEmploymentHistoryId());
    }
	
	public void testFindBySqlColl() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindBySqlColl");
    	int employmentHistoryId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "EMPLOYMENTHISTORYID"));
    	String sql = "Select * from EmploymentHistory where employmenthistoryid = 2022 and EmploymentHistoryStatusId = 1";
    	employmentHistory.findBySqlColl(sql, null);
    	assertEquals(employmentHistoryId, employmentHistory.getEmploymentHistoryId());
    }
	
	public void testFindByIncome() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByIncome");
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "INCOMEID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	employmentHistory.findByIncome(new IncomePK(id, copyId));
    	assertEquals(id, employmentHistory.getIncomeId());
    }
	
	/*public void testCreate() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreate");
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "BORROWERID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	srk.beginTransaction();
    	employmentHistory = employmentHistory.create(new BorrowerPK(id, copyId));
    	EmploymentHistoryPK employmentHistoryPK = new EmploymentHistoryPK(employmentHistory.getEmploymentHistoryId(),employmentHistory.getCopyId());
    	EmploymentHistory employmentHistorynew = employmentHistory.findByPrimaryKey(employmentHistoryPK);
    	assertNotNull(employmentHistorynew);    	
    	srk.rollbackTransaction();
    }
	
	public void testCreate1() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreate1");
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "BORROWERID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	srk.beginTransaction();
    	employmentHistory = employmentHistory.create(new BorrowerPK(id, copyId));
    	EmploymentHistoryPK employmentHistoryPK = new EmploymentHistoryPK(employmentHistory.getEmploymentHistoryId(),employmentHistory.getCopyId());
    	EmploymentHistory employmentHistorynew = employmentHistory.findByPrimaryKey(employmentHistoryPK);
    	assertNotNull(employmentHistorynew);    	
    	srk.rollbackTransaction();
    }*/
	
	public void testFindByMyCopies() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByMyCopies");
    	int employmentHistoryId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "EMPLOYMENTHISTORYID"));
    	employmentHistory.findByMyCopies();
    	assertEquals(employmentHistoryId, employmentHistory.getEmploymentHistoryId());
    }
}