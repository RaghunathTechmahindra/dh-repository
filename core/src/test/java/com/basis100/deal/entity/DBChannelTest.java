package com.basis100.deal.entity;

import java.io.IOException;
import java.util.List;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.ChannelPK;

public class DBChannelTest extends FXDBTestCase{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(DBChannelTest.class);
    
    //Initial data to be loaded before each test case.
	private final static String INIT_XML_DATA = "DBChannelDataSet.xml";
	private IDataSet dataSetTest;
	
	/**
     * Constructor function
     */
    public DBChannelTest(String name)  throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource("DBChannelDataSetTest.xml"));
    }
    
    @Override
	protected IDataSet getDataSet() throws Exception {
    	return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}
    
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
        return DatabaseOperation.INSERT;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.DELETE;
    }
    
    /**
     * Test method for Channel#findByAddressComponents
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

    	_logger.info(" Start testFindByPrimaryKey");
    	 
    	ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKey");
    	int  channelId = Integer.parseInt((String)testFindByPrimaryKey.getValue(0,"channelId"));
    	int  channelTypeId = Integer.parseInt((String)testFindByPrimaryKey.getValue(0,"channelTypeId"));
  		int  instProfId = Integer.parseInt((String)testFindByPrimaryKey.getValue(0,"instProfId"));
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
    	 
        Channel channel = new Channel(srk);
        ChannelPK pk = new ChannelPK(channelId);
        channel = channel.findByPrimaryKey(pk);
        
        assert channel != null;
        assertEquals(channelId, channel.getChannelId());
        assertEquals(channelTypeId, channel.getChannelTypeId());
        
        _logger.info(" End testFindByPrimaryKey");
    }

}
