/*
 * @(#)AdjudicationApplicantTest.java Sep 18, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.AdjudicationApplicantPK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * AdjudicationApplicantTest
 * 
 * @version 1.0 Sep 18, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class AdjudicationApplicantTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(AdjudicationApplicantTest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test the find by primary key
     */
    @Ignore("No Data") @Test
    public void testFindByPrimaryKey() throws Exception {
        // get input data from repository
        int dealInstitutionId = _dataRepository.getInt("AdjudicationApplicant",
                "InstitutionProfileID", 0);

        int copyID = _dataRepository.getInt("AdjudicationApplicant", "copyID",
                0);
        int requestID = _dataRepository.getInt("AdjudicationApplicant",
                "requestID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        AdjudicationApplicant adjudicationApp = new AdjudicationApplicant(_srk);

        // find by primary Key
        adjudicationApp.findByPrimaryKey(new AdjudicationApplicantPK(requestID,
                copyID));

    }

    /**
     * test Create
     * 
     * @throws Exception
     */
    @Ignore("Failing") @Test
    public void testCreate() throws Exception {
        int dealInstitutionId = _dataRepository.getInt("Request",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // get input data from repository
        int copyID = _dataRepository.getInt("Borrower", "copyID", 0);
        int borrowerID = _dataRepository.getInt("Borrower", "borrowerID", 0);
        int requestID = _dataRepository.getInt("Request", "requestID", 0);
        int appnum = 1;
        String appcurrind = "Test";

        // Start new transaction
        _srk.beginTransaction();

        _logger.info(BORDER_START, "testCreate");

        // Create the initial entity
        AdjudicationApplicant adjApplicant = new AdjudicationApplicant(_srk);

        // Create a new object
        adjApplicant.create(new RequestPK(requestID, copyID), new BorrowerPK(
                borrowerID, copyID), appnum, appcurrind);

        // Store it in the DB
        adjApplicant.ejbStore();

        // Rollback transaction
        _srk.rollbackTransaction();

        _logger.info(BORDER_END, "testCreate");

    }

}
