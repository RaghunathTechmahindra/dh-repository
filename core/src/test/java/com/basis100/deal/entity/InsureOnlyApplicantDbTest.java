package com.basis100.deal.entity;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Collection;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.docprep.extract.doctag.PrivacyClause;
import com.basis100.deal.pk.AddrPK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.ConditionPK;
import com.basis100.deal.pk.ContactPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.EmploymentHistoryPK;
import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.deal.pk.InsureOnlyApplicantPK;
import com.basis100.resources.SessionResourceKit;
import com.sun.org.apache.bcel.internal.generic.INSTANCEOF;

public class InsureOnlyApplicantDbTest extends FXDBTestCase {
	private IDataSet dataSetTest;
    private InsureOnlyApplicant insureOnlyApplicant = null;
    Collection ainsureOnlyApplicant=null;
    CalcMonitor dcm=null;
  
    SessionResourceKit srk= new SessionResourceKit();
	
	public InsureOnlyApplicantDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(InsureOnlyApplicant.class.getSimpleName()+"DataSetTest.xml"));
	}
	

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.dcm = CalcMonitor.getMonitor(srk);
		this.insureOnlyApplicant = new InsureOnlyApplicant(srk,dcm);
		
		
	}
	
	public void testCreate() throws Exception
	{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int institutionProfileId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));
		int copyId = Integer.parseInt((String)testCreate.getValue(0, "COPYID")); 
		
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		DealPK dpk = new DealPK(institutionProfileId, copyId);
	
		InsureOnlyApplicant ainsureOnlyApplicant = insureOnlyApplicant.create(dpk);
		
		InsureOnlyApplicant new_insureOnlyApplicant = new InsureOnlyApplicant(srk, dcm, ainsureOnlyApplicant.getInsureOnlyApplicantId(), copyId);
		
		assertEquals(new_insureOnlyApplicant, ainsureOnlyApplicant);
		
		srk.rollbackTransaction();
	}
	
	/*
	 * Failure case not applicable as the create method returns an entity and sending unqualified data will result
	 * in improper end of test class and thus it is avoid.
	 */
	
	
	public void testfindAllByDeal() throws Exception
	{
		srk.beginTransaction();
		ITable testfindAllByDeal = dataSetTest.getTable("testfindAllByDeal");
		int institutionProfileId = Integer.parseInt((String)testfindAllByDeal.getValue(0, "INSTITUTIONPROFILEID"));
		int copyId = Integer.parseInt((String)testfindAllByDeal.getValue(0, "COPYID")); 
		int dealId = Integer.parseInt((String)testfindAllByDeal.getValue(0, "DEALID")); 
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		DealPK dpk = new DealPK(dealId, copyId);
		
		Collection ainsureOnlyApplicant = insureOnlyApplicant.findByDeal(dpk);
		
		assert ainsureOnlyApplicant.size()>1;
		
		srk.rollbackTransaction();
	}
	
	public void testfindAllByDealFailure() throws Exception
	{
		srk.beginTransaction();
		ITable testfindAllByDealFailure = dataSetTest.getTable("testfindAllByDeal");
		int institutionProfileId = Integer.parseInt((String)testfindAllByDealFailure.getValue(0, "INSTITUTIONPROFILEID"));
		int copyId = Integer.parseInt((String)testfindAllByDealFailure.getValue(0, "COPYID")); 
		int dealId = Integer.parseInt((String)testfindAllByDealFailure.getValue(0, "DEALID")); 
		
		DealPK dpk = new DealPK(dealId, copyId);
		
		Collection ainsureOnlyApplicant = insureOnlyApplicant.findByDeal(dpk);
		
		assert ainsureOnlyApplicant.size()<=0;
		
		srk.rollbackTransaction();
	}
	
	public void testfindByPrimaryKey() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByPrimaryKey = dataSetTest.getTable("testfindByPrimaryKey");
		int institutionProfileId = Integer.parseInt((String)testfindByPrimaryKey.getValue(0, "INSTITUTIONPROFILEID"));
		int copyId = Integer.parseInt((String)testfindByPrimaryKey.getValue(0, "COPYID")); 
		int iaId = Integer.parseInt((String)testfindByPrimaryKey.getValue(0, "INSUREONLYAPPLICANTID")); 
		
		InsureOnlyApplicantPK ipk = new InsureOnlyApplicantPK(iaId, copyId);
		InsureOnlyApplicant aInsureOnlyApplicant = insureOnlyApplicant.findByPrimaryKey(ipk);
		
		assertNotNull(aInsureOnlyApplicant);
		
		srk.rollbackTransaction();
	}
	
	public void testfindByPrimaryKeyFailure() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByPrimaryKeyFailure = dataSetTest.getTable("testfindByPrimaryKeyFailure");
		int institutionProfileId = Integer.parseInt((String)testfindByPrimaryKeyFailure.getValue(0, "INSTITUTIONPROFILEID"));
		int copyId = Integer.parseInt((String)testfindByPrimaryKeyFailure.getValue(0, "COPYID")); 
		int iaId = Integer.parseInt((String)testfindByPrimaryKeyFailure.getValue(0, "INSUREONLYAPPLICANTID")); 
		
		InsureOnlyApplicantPK ipk = new InsureOnlyApplicantPK(iaId, copyId);
		InsureOnlyApplicant aInsureOnlyApplicant = null;
		try{
		aInsureOnlyApplicant = insureOnlyApplicant.findByPrimaryKey(ipk);
		}
		catch(Exception e){}
		assertNull(aInsureOnlyApplicant);
		
		srk.rollbackTransaction();
	}
	
	public void testfindByDeal() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByDeal = dataSetTest.getTable("testfindByDeal");
		int institutionProfileId = Integer.parseInt((String)testfindByDeal.getValue(0, "INSTITUTIONPROFILEID"));
		int copyId = Integer.parseInt((String)testfindByDeal.getValue(0, "COPYID")); 
		int dealId = Integer.parseInt((String)testfindByDeal.getValue(0, "DEALID")); 
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		DealPK dpk = new DealPK(dealId, copyId);
		
		Collection ainsureOnlyApplicant = insureOnlyApplicant.findByDeal(dpk);
		
		assert ainsureOnlyApplicant.size()>1;
		
		srk.rollbackTransaction();
	}
	
	public void testfindByDealFailure() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByDealFailure = dataSetTest.getTable("testfindByDealFailure");
		int institutionProfileId = Integer.parseInt((String)testfindByDealFailure.getValue(0, "INSTITUTIONPROFILEID"));
		int copyId = Integer.parseInt((String)testfindByDealFailure.getValue(0, "COPYID")); 
		int dealId = Integer.parseInt((String)testfindByDealFailure.getValue(0, "DEALID")); 
		
		DealPK dpk = new DealPK(dealId, copyId);
		
		Collection ainsureOnlyApplicant = insureOnlyApplicant.findByDeal(dpk);
		
		assert ainsureOnlyApplicant.size()<=0;
		
		srk.rollbackTransaction();
	}
	public void testFindByMyCopies() throws Exception{		
		Vector result=null;
		srk.getExpressState().setDealInstitutionId(1);		
		result=insureOnlyApplicant.findByMyCopies();	
		assertNotNull(result);
	}   
}
