/*
 * @(#) DocumentTrackingTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */


package com.basis100.deal.entity;

import java.sql.PreparedStatement;

import oracle.sql.CLOB;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.DocumentTrackingPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>DocumentTrackingTest</p>
 * Express Entity class unit test: DocumentTracking
 */
public class DocumentTrackingTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(DocumentTrackingTest.class);

    /**
     * Constructor function
     */
    public DocumentTrackingTest() {
    }

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int documentTrackingId = _dataRepository.getInt("DOCUMENTTRACKING",
                "DOCUMENTTRACKINGID", 0);
        int dealId = _dataRepository.getInt("DOCUMENTTRACKING", "DEALID", 0);
        int copyId = _dataRepository.getInt("DOCUMENTTRACKING", "COPYID", 0);
        int instProfId = _dataRepository.getInt("DOCUMENTTRACKING",
                "INSTITUTIONPROFILEID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed the input data and run the program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        DocumentTracking entity = new DocumentTracking(srk, documentTrackingId, copyId);
        entity = entity.findByPrimaryKey(new DocumentTrackingPK(
                documentTrackingId, copyId));

        // verify the running result.
        assertEquals(documentTrackingId, entity.getDocumentTrackingId());
        assertEquals(dealId, entity.getDealId());

        // free resources
        srk.freeResources();
    }

    /**
     * test create.
     */
    @Test
    public void testCreate() throws Exception {

        // get input data from repository
        int documentTrackingId = _dataRepository.getInt("DOCUMENTTRACKING",
                "DOCUMENTTRACKINGID", 0);
        int instProfId = _dataRepository.getInt("DOCUMENTTRACKING",
                "INSTITUTIONPROFILEID", 0);
        int conditionId = _dataRepository.getInt("DOCUMENTTRACKING", "CONDITIONID", 0);
        int documentstatus = _dataRepository.getInt("DOCUMENTTRACKING",
                "DOCUMENTSTATUSID", 0);
        int dealId = _dataRepository.getInt("DOCUMENTTRACKING", "DEALID", 0);
        int copyId = _dataRepository.getInt("DOCUMENTTRACKING", "COPYID", 0);
        int documentTrackingSource = _dataRepository.getInt("DOCUMENTTRACKING",
                "DOCUMENTTRACKINGSOURCEID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed input data and run program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        srk.beginTransaction();
        Condition con = new Condition(srk, conditionId);
        DocumentTracking newEntity = new DocumentTracking(srk, documentTrackingId, copyId);
        newEntity = newEntity.create(con, dealId, copyId, documentstatus,
                documentTrackingSource);
        newEntity.ejbStore();

        // check create correctlly.
        DocumentTracking foundEntity = new DocumentTracking(srk,
                documentTrackingId, copyId);
        foundEntity = foundEntity.findByPrimaryKey(new DocumentTrackingPK(
                newEntity.getDocumentTrackingId(), newEntity.getCopyId()));

        // verifying
        assertEquals(foundEntity.getDocumentTrackingId(), newEntity
                .getDocumentTrackingId());
        assertEquals(foundEntity.getDealId(), newEntity.getDealId());
        // rollback transaction.
        srk.rollbackTransaction();
        // free resources.
        srk.freeResources();
    }
    
    /**
     * test create in DocTrackingVerbiage.
     */
    /*@Test
    public void testCreateLargeText() throws Exception {

        //Prepare document tracking data.
        int documentTrackingId = _dataRepository.getInt("DOCUMENTTRACKING",
                "DOCUMENTTRACKINGID", 0);
        int instProfId = _dataRepository.getInt("DOCUMENTTRACKING",
                "INSTITUTIONPROFILEID", 0);
        int conditionId = _dataRepository.getInt("DOCUMENTTRACKING", "CONDITIONID", 0);
        int documentstatus = _dataRepository.getInt("DOCUMENTTRACKING",
                "DOCUMENTSTATUSID", 0);
        int dealId = _dataRepository.getInt("DOCUMENTTRACKING", "DEALID", 0);
        int copyId = _dataRepository.getInt("DOCUMENTTRACKING", "COPYID", 0);
        int documentTrackingSource = _dataRepository.getInt("DOCUMENTTRACKING",
                "DOCUMENTTRACKINGSOURCEID", 0);

        // Initialize srk.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();

        // feed input data and run program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        srk.beginTransaction();
        //Replace existing condition text with a large text.
        String largeConditionText = "The exemptions for auto dealers and community banks    defined as those with less than  10 billion in assets    reflect their vast influence in Washington. They succeeded even as lobbying by the nation s banking giants failed to protect those companies from the new bureau s strictest oversight.  The National Automobile Dealers Association began barraging congressional offices with phone calls and e mails as early as last fall. As a vote neared, dealers visited Washington to plead their case to lawmakers, according to Ed Tonkin, the NADA chairman.  They argued that auto dealers had been unfairly swept up in the zeal to rein in Wall Street s excesses. They noted that dealers merely arrange most auto loans, linking customers with lenders, and are already subject to regulations on auto financing. The lenders    including the auto finance arms of automakers like Ford and Toyota    will fall under the new bureau s authority.  This is a government overreach into private business,  Tonkin said.  Dealers are not banks, and we shouldn t be subject to bank rules. Yet because they act as a go between, consumer advocates say, auto dealers fill the same role as the mortgage brokers who fed the housing crisis by pushing high risk loans. Some auto dealers have been accused of misleading borrowers about financing terms and of pushing them into loans with higher interest rates than their credit scores warranted.  The NADA denies this. It says the money dealers make from arranging consumer loans from banks is slight. It notes that the minority of auto dealers that lend directly to consumers still would be regulated. And it warns that the cost to dealers of complying with new regulations would drive up the cost of cars.  The auto dealers  exemption ran into unexpected resistance when the Army secretary and other top Pentagon officials had urged Congress not to give dealers a pass. Reports had emerged of young, financially inexperienced troops being pushed into predatory car loans by dealerships near military bases.  The administration also lobbied against the exemption for auto dealers. President Barack Obama issued a rare public statement opposing it.   An auto loan is like any other loan,  said Lauren Saunders, managing attorney of the National Consumer Law Center, which has argued cases on behalf of minority car buyers who complained that auto dealers discriminated by giving them higher cost loans.  The auto dealers are a very powerful political lobby, and at the end of the day, that is what won.   Dealerships nationwide number about 18,000. And nearly 8,000 community banks dot the country.   They have that incredible grass roots power, and that s why they got the loopholes and the carve outs they did,  said Ed Mierzwinski, consumer program director at U.S. Public Interest Research Group.  The auto dealers  successful argument to Congress    don t punish Main Street for Wall Street s sins    was echoed by community banks. Community banks, which include thrifts and depositor owned banks, cater to homebuyers, developers, small businesses and farmers, among others.  Yet small banks don t necessarily mean small risks. A wave of failures has battered community banks: They account for nearly all the 226 banks in the U.S. that have failed since the start of 2009.  Losses have mounted on loans for commercial real estate, such as stores and office complexes. As more buildings sit vacant and builders default, regional and small banks could face  200 billion to  300 billion in losses over the next few years, according to the Congressional Oversight Panel, which is overseeing the financial bailout. Hundreds more banks could fail as a result.  The Independent Community Bankers of America spent nearly  1.1 million in the first quarter on lobbying on bank regulation, the new consumer protection bureau and other issues. That was 15 percent more than it spent in the first quarter of 2009. It spent  1.36 million on lobbying in the fourth quarter of last year.  The community banks  influence goes beyond those dollar figures. Local bankers are big donors in hundreds of congressional races that will be decided this fall. They supplemented their trade group s spending with visits and phone calls.  That pressure helped the community banks score separate victories in the legislation. Among them: Congress will raise fees that banks pay into the Federal Deposit Insurance Corp. s fund. But the rate increase will apply only to banks with assets greater than  10 billion. Smaller banks were spared.  The NADA has also increased its lobbying. It began lobbying on the dealer exemption in the third quarter of 2009. It spent a recent high of  896,000 in the fourth quarter, though that amount included lobbying on other issues, too.  As with community banks, pressure from individual auto dealers helped sway Congress. Such phone calls and visits aren t required to be reported as lobbying.  Consumer advocates hold out hope for extending the bureau s full oversight to auto dealers and community banks. They note that Congress preserved the Federal Trade Commission s authority to fight deceptive practices by dealers.  They also say they ve raised awareness of the issues with lawmakers    groundwork for future regulation. ";
        String sql = "Update conditionverbiage set conditiontext = ? " +
    	" where conditionid = " + conditionId + " and languagepreferenceid = 0";
        PreparedStatement pstmt = srk.getConnection().prepareStatement(sql);
        pstmt.setString(1, largeConditionText);
        pstmt.execute();
        
        Condition condition = new Condition(srk, conditionId);
        DocumentTracking newEntity = new DocumentTracking(srk);
        newEntity = newEntity.create(condition, dealId, copyId, documentstatus,
                documentTrackingSource);
        newEntity.ejbStore();

        //Find the newly created document tracking record.
        DocumentTracking foundEntity = new DocumentTracking(srk,
                newEntity.getDocumentTrackingId(), newEntity.getCopyId());
        foundEntity = foundEntity.findByPrimaryKey(new DocumentTrackingPK(
        		newEntity.getDocumentTrackingId(), newEntity.getCopyId()));

        // verifying
        assertEquals(foundEntity.getDocumentTrackingId(), newEntity
                .getDocumentTrackingId());
        assertEquals(foundEntity.getDealId(), newEntity.getDealId());
        assertEquals(foundEntity.getDocumentText(0), newEntity.getDocumentText(0));
        // rollback transaction.
        srk.rollbackTransaction();
        // free resources.
        srk.freeResources();
    }*/
}
