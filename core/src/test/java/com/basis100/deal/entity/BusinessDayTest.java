/**
 * @(#)BusinessDayTest.java    2007-3-16
 */

package com.basis100.deal.entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.deal.pk.BusinessDayPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;

/**
 * BusinessDayTest -
 * 
 * @version 1.0 2007-3-16
 * @author <A HREF="mailto:chyxiang@yahoo.com">Chen Xiang (Sean)</A>
 */
public class BusinessDayTest extends ExpressEntityTestCase {

    // The logger
    private static Log _log = LogFactory.getLog(BusinessDayTest.class);

    // The SessionResourceKit
    private SessionResourceKit _srk;

    // The business day entity
    private BusinessDay _businessDayDAO;

    // The institutionid for deal
    private int dealInstitutionId;

    /**
     * 
     */
    @Before
    public void setUp() throws Exception {
        _log.debug("Preparing the test cases ...");

        // init resource and srk
        ResourceManager.init();
        _srk = new SessionResourceKit("Testing");

        // get institutionId from database
        dealInstitutionId = _dataRepository.getInt("BusinessDays",
                "InstitutionProfileId", 0);

        _businessDayDAO = new BusinessDay(_srk);

    }

    /**
     * test get by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // set vpd context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // get values from database
        int businessDayId = _dataRepository.getInt("BusinessDays",
                "BusinessDayId", 0);
        String timeZone = _dataRepository.getString("BusinessDays",
                "BaseTimeZone", 0);

        // find a business day by primary key
        BusinessDayPK bpk = new BusinessDayPK(businessDayId);
        BusinessDay bd = _businessDayDAO.findByPrimaryKey(bpk);

        // verify the result
        assertEquals(timeZone, bd.getBaseTimeZone());
        _log.info("         Base Time Zone: " + bd.getBaseTimeZone());
        _log.info("           Day End Hour: " + bd.getEndHour());
        _log.info("         Day End Minute: " + bd.getEndMinute());
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        // free the srk
        _srk.freeResources();
    }

}
