package com.basis100.deal.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.duplicate.DuplicateApplicationFinder;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PropertyPK;

public class PropertyDBTest extends  FXDBTestCase {
	
	private IDataSet dataSetTest;
	private Property  property=null;
	Deal d1=null,ex=null,in=null;
	

	public PropertyDBTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("PropertyDataSetTest.xml"));
	}

/*	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource("PropertyDataSet.xml"));
	}
*/
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.property = new Property(srk);
	}

	public void testFindByDupeCheckCriteria() throws Exception {
		
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByDupeCheckCriteria");
		String  PropertyStreetNumber = findByDupeCheckCriteria.getValue(0,"PropertyStreetNumber").toString();
		String  PropertyStreetName = findByDupeCheckCriteria.getValue(0,"PropertyStreetName").toString();
		String  PropertyCity = findByDupeCheckCriteria.getValue(0,"PropertyCity").toString();
		String  StreetTypeId = findByDupeCheckCriteria.getValue(0,"StreetTypeId").toString();
		String  StreetDirectionId = findByDupeCheckCriteria.getValue(0,"StreetDirectionId").toString();
		String  ProvinceId =findByDupeCheckCriteria.getValue(0,"ProvinceId").toString();
		Collection  result=property.findByDupeCheckCriteria(PropertyStreetNumber, PropertyStreetName, Integer.parseInt(StreetTypeId), Integer.parseInt(StreetDirectionId), null, PropertyCity,
				Integer.parseInt(ProvinceId), null);
		int resultSize=result.size();
		assertNotSame(0,resultSize);
		
	}
	public void testFindByDupeCheckCriteriaFailure() throws Exception{
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByDupeCheckCriteriaFailure");
		String  PropertyStreetNumber = findByDupeCheckCriteria.getValue(0,"PropertyStreetNumber").toString();
		Collection  result=property.findByDupeCheckCriteria(null, null, 0,0, null, null,
				0, null);
		int resultSize = result.size();
		assertTrue(resultSize == 0);
		
	}
	
	
    public void testFindByGenXDupeCheck() throws Exception {
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByGenXDupeCheck");
		String  PropertyStreetNumber = findByDupeCheckCriteria.getValue(0,"PropertyStreetNumber").toString();
		String  PropertyStreetName = findByDupeCheckCriteria.getValue(0,"PropertyStreetName").toString();
		String  PropertyCity = findByDupeCheckCriteria.getValue(0,"PropertyCity").toString();
		String  StreetTypeId = findByDupeCheckCriteria.getValue(0,"StreetTypeId").toString();
		String  StreetDirectionId = findByDupeCheckCriteria.getValue(0,"StreetDirectionId").toString();
		String  ProvinceId =findByDupeCheckCriteria.getValue(0,"ProvinceId").toString();
		Collection  result=property.findByGenXDupeCheck(PropertyStreetNumber, PropertyStreetName, Integer.parseInt(StreetTypeId), Integer.parseInt(StreetDirectionId), null, PropertyCity,
				Integer.parseInt(ProvinceId), null);
		int resultSize=result.size();
		assertNotSame(0,resultSize);
	}
   
   
   public void testFindByGenXDupeCheckFailure() throws Exception {
		
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByGenXDupeCheckFailure");
		String  PropertyStreetNumber = findByDupeCheckCriteria.getValue(0,"PropertyStreetNumber").toString();
		Collection  result=property.findByGenXDupeCheck(null, null, 0,0, null, null,0, null);
		int resultSize=result.size();
		assertTrue(resultSize ==0);
		
	}
	
	
  
    public void testFindByDeal() throws Exception{
		Collection<Property> cList=null;
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByDeal");
		int dealId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"dealId"));	
		int copyId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"copyId"));
		srk.getExpressState().setDealInstitutionId(1);		
		DealPK pk = new DealPK(dealId,copyId);
		cList=property.findByDeal(pk);
		assert cList.size()!=0;
		assertEquals(dealId, pk.getId());		
	}
	 
  
    public void testFindByDealIdAndPrimaryFlag() throws Exception{
		Collection<Property> cList=null;
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByDealIdAndPrimaryFlag");
		int dealId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"dealId"));	
		int copyId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"copyId"));
		String primaryProperty=(String)findByDupeCheckCriteria.getValue(0,"primaryPropertyFlag");
		char primaryPropertyFlag = primaryProperty.charAt(0);
		srk.getExpressState().setDealInstitutionId(1);
		Deal deal=new Deal(srk,null,dealId,copyId);		
		cList=property.findByDealIdAndPrimaryFlag(dealId,primaryPropertyFlag);
		assert cList.size()!=0;
		assertEquals(dealId, deal.getDealId());		
	} 
	
	    
    
   public void testFindByPrimaryKey() throws Exception{		
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByPrimaryKey");
		int propertyId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"propertyId"));	
		int copyId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"copyId"));
		srk.getExpressState().setDealInstitutionId(1);		
		PropertyPK  pk = new PropertyPK(propertyId,copyId);
		property=property.findByPrimaryKey(pk);		
		assertEquals(propertyId, pk.getId());		
	} 
	 
  	   
    public void testFindByPrimaryKeyFailure() throws Exception{		
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByPrimaryKeyFailure");
		boolean status = false;
		try{
		int propertyId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"propertyId"));	
		int copyId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"copyId"));
		srk.getExpressState().setDealInstitutionId(1);				
		property.findByPrimaryKey(new PropertyPK(propertyId,copyId));
		status = true;
		} catch (Exception e) {
			status = false;
		}
		assertEquals(false, status);
    	}
    	
    
       public void testFindByPrimaryProperty() throws Exception{		
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByPrimaryProperty");
		int dealId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"dealId"));	
		int copyId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"copyId"));
		int institutionId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"institutionId"));
		srk.getExpressState().setDealInstitutionId(1);		
		//PropertyPK  pk = new PropertyPK(dealId,copyId);
		property=property.findByPrimaryProperty(dealId,copyId,institutionId);		
		//assertEquals(propertyId, pk.getId());		
	}   
     
    public void testFindCopyIdByPropertyId() throws Exception{
		Collection cList=null;
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindCopyIdByPropertyId");
		int propertyId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"propertyId"));					
		srk.getExpressState().setDealInstitutionId(1);			
		cList=property.findCopyIdByPropertyId(propertyId);
		int size=cList.size();
		boolean status=false;
		if(size>0){
			 status = true;
		}
		assertTrue(status);
	}
	
	  
   
	  public void testGetStreetDirectionDescription() throws Exception{
		  String sdDescription = null;
		  ITable testGetStreetDirectionDescription = dataSetTest.getTable("testGetStreetDirectionDescription");
			int streetDirectionId = Integer.parseInt((String)testGetStreetDirectionDescription.getValue(0,"streetDirectionId"));
			property.setStreetDirectionId(streetDirectionId);
			sdDescription= property.getStreetDirectionDescription();
		   assertNotNull(sdDescription);
		} 
	
	  
	  public void testFetchFirstAppraiserCompanyName() throws Exception{
		  String partyCompanyName = null;
		  ITable testFetchFirstAppraiserCompanyName = dataSetTest.getTable("testFetchFirstAppraiserCompanyName");
		  int propertyId = Integer.parseInt((String)testFetchFirstAppraiserCompanyName.getValue(0,"propertyId"));
		  property.setPropertyId(propertyId);
		  partyCompanyName = property.fetchFirstAppraiserCompanyName();
		  assertNotNull(partyCompanyName);
		} 
		
		 
	 
	  
	 public void testFormPropertyAddress() throws Exception{
		  String addr = null;
		  addr = property.formPropertyAddress();
		  assertNotNull(addr);
		} 
		

	public void testFindByMyCopies() throws Exception{
		  Vector v = null;
		  v=property.findByMyCopies();
		  assertNotNull(v);
		} 
	
}
