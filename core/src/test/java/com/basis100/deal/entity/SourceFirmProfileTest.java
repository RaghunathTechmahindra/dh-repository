/**
 * <p>@(#)SourceFirmProfileTest.java Sep 10, 2007</p>
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.basis100.deal.entity;

import java.util.Collection;
import java.util.Iterator;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.ContactPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.SourceFirmProfilePK;
import com.basis100.deal.pk.SourceOfBusinessProfilePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>SourceFirmProfileTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class SourceFirmProfileTest extends ExpressEntityTestCase 
    implements UnitTestLogging {

    private static final Logger _logger = 
        LoggerFactory.getLogger(ResponseTest.class);
    private static final String ENTITY_NAME = "SOURCEFIRMPROFILE"; 
  
    // retreived data count that is stored in UnitTestDataRepository.xml
    public static final int DATA_COUNT = 25;

    private int testSeqForGen = 0;

    private int[] okSeq = {1,2,4,7,9,14,20,24};
    private SessionResourceKit _srk;
    
    // properties
    protected String  sourceFirmName;
    protected int     systemTypeId;
    protected int     sourceFirmProfileId;
    protected int     contactId;
    protected int     profileStatusId;
    protected String  sfShortName;
    protected String  sfBusinessId;
    protected String  alternativeId;
    protected int     sfMOProfileId;
    protected String   sourceFirmCode;
    protected int     instProfId;
    
    /**
     * <p>setUp</p>
     * Setting up test case.
     */
    @Before
    public void setUp() throws Exception {
      
        _logger.info(BORDER_START, ENTITY_NAME + " TestCase setting up ...");
        
        // get random seqence number
        // for testFindByAssociatedSourceOfBusiness, it should be in OKseq 
        Double indexD = new Double(Math.random() * 8);
        testSeqForGen = okSeq[indexD.intValue()];
              
        //      get test data from repository
        sourceFirmName = _dataRepository.getString(ENTITY_NAME,  
                                    "SOURCEFIRMNAME", 
                                    testSeqForGen);
        systemTypeId = _dataRepository.getInt(ENTITY_NAME,  
                                    "SYSTEMTYPEID", 
                                    testSeqForGen);
        sourceFirmProfileId = _dataRepository.getInt(ENTITY_NAME,  
                                    "SOURCEFIRMPROFILEID", 
                                    testSeqForGen);
        contactId = _dataRepository.getInt(ENTITY_NAME,  
                                    "CONTACTID", 
                                    testSeqForGen);
        profileStatusId = _dataRepository.getInt(ENTITY_NAME,  
                                    "PROFILESTATUSID", 
                                    testSeqForGen);
        sfShortName = _dataRepository.getString(ENTITY_NAME,  
                                    "SFSHORTNAME", 
                                    testSeqForGen);
        sfBusinessId = _dataRepository.getString(ENTITY_NAME,  
                                    "SFBUSINESSID", 
                                    testSeqForGen);
        alternativeId = _dataRepository.getString(ENTITY_NAME,  
                                    "ALTERNATIVEID", 
                                    testSeqForGen);
        sfMOProfileId = _dataRepository.getInt(ENTITY_NAME,  
                                    "SFMOPROFILEID", 
                                    testSeqForGen);
        sourceFirmCode = _dataRepository.getString(ENTITY_NAME,  
                                    "SOURCEFIRMCODE", 
                                    testSeqForGen);
        instProfId = _dataRepository.getInt(ENTITY_NAME,  
                                    "INSTITUTIONPROFILEID", 
                                    testSeqForGen);

        // init ResouceManager
        ResourceManager.init();
        //  init session resource kit.
        _srk = new SessionResourceKit();

        _logger.info(BORDER_END, "Setting up Done");
    }
    
    @After
    public void tearDown() {

      //      free resources
      _srk.freeResources( );

    }
  
    /**
     * test findByPrimaryKey.
     */
    @Test
    public void testFindByPrimaryKey()  throws Exception {
      
        _logger.info(BORDER_START, "findByPrimaryKey");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        SourceFirmProfile sfProfile = new SourceFirmProfile(_srk);
        sfProfile  = sfProfile.findByPrimaryKey(new SourceFirmProfilePK(sourceFirmProfileId));
        
        assertEqualsProperties(sfProfile);

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * test FindByContactPK.
     */
    @Test
    public void testFindByContactPk()  throws Exception {
      
        _logger.info(BORDER_START, "FindByContactPK");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        SourceFirmProfile sfProfile = new SourceFirmProfile(_srk);
        // this should return collection
        sfProfile  = sfProfile.findByContactPK(new ContactPK(contactId, 0));
        
        _logger.info(BORDER_END, "FindByContactPK");
    }

   /* *//**
     * test findByName.
     *//*
    @Test
    public void testFindByName()  throws Exception {
      
        _logger.info(BORDER_START, "findByName");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        SourceFirmProfile sfProfile = new SourceFirmProfile(_srk);
        // this should return collection
        sfProfile  = sfProfile.findByName(sourceFirmName);
        assertEqualsProperties(sfProfile);

        _logger.info(BORDER_END, "findByName");
    }
*/
    /**
     * test findByAssociatedSourceOfBusiness.
     */
    /*@Test
    public void testFindByAssociatedSourceOfBusiness()  throws Exception {
      
        _logger.info(BORDER_START, "findByAssociatedSourceOfBusiness");
          
        // get sourceFirmBusinessId from test data
        int testSeq = 266;
        int testSOBSourceFirmProfileId = 0;
        int testSOBSOBProfileId = 0;
        int testSOBSOBInstId = 0;
        while((testSOBSourceFirmProfileId != sourceFirmProfileId 
                        || testSOBSOBInstId != instProfId)
            && testSeq < 275){
            testSOBSourceFirmProfileId 
                = _dataRepository.getInt("SOURCEOFBUSINESSPROFILE", 
                                         "SOURCEFIRMPROFILEID", 
                                         testSeq);
            testSOBSOBProfileId
                = _dataRepository.getInt("SOURCEOFBUSINESSPROFILE", 
                                         "SOURCEOFBUSINESSPROFILEID", 
                                         testSeq);
            testSOBSOBInstId
                = _dataRepository.getInt("SOURCEOFBUSINESSPROFILE", 
                                         "INSTITUTIONPROFILEID", 
                                         testSeq);
            testSeq++;
        }
        
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(testSOBSOBInstId);

        // excute method        
        SourceFirmProfile sfProfile = new SourceFirmProfile(_srk);
        Collection sfs  = sfProfile.
            findByAssociatedSourceOfBusiness(new SourceOfBusinessProfilePK(testSOBSOBProfileId));

        assertNotNull(sfs);
        
        // check assertEqual 
        Iterator profileIter = sfs.iterator();
        for (; profileIter.hasNext(); ) {
            assertEqualsProperties((SourceFirmProfile)profileIter.next());
        }
        
        _logger.info(BORDER_END, "findByAssociatedSourceOfBusiness");
    }*/

    /**
     * test findBySFBusinessIdAndSystemType.
     */
    @Test
    public void testFindBySFBusinessIdAndSystemType()  throws Exception {
      
        _logger.info(BORDER_START, "findBySFBusinessIdAndSystemType");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
        
        // excute method
        SourceFirmProfile sfProfile = new SourceFirmProfile(_srk);
        Collection sfs  
            = sfProfile.findBySFBusinessIdAndSystemType(sfBusinessId, systemTypeId);
        
        // check assertEqual 
        // check assertEqual 
        Iterator profileIter = sfs.iterator();
        for (; profileIter.hasNext(); ) {
            assertEqualsProperties((SourceFirmProfile)profileIter.next());
        }

        _logger.info(BORDER_END, "findBySFBusinessIdAndSystemType");
    }
    
    /**
     * test findBySFBusinessId.
     */
    @Test
    public void testFindBySFBusinessId()  throws Exception {
      
        _logger.info(BORDER_START, "findBySFBusinessId");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
        
        // excute method
        SourceFirmProfile sfProfile = new SourceFirmProfile(_srk);
        Collection sfs  
            = sfProfile.findBySFBusinessId(sfBusinessId);
        
        // check assertEqual 
        Iterator profileIter = sfs.iterator();
        for (; profileIter.hasNext(); ) {
            assertEqualsProperties((SourceFirmProfile)profileIter.next());
        }

        _logger.info(BORDER_END, "findBySFBusinessId");
    }

    /**
     * test findBySourceAndDealSystemType.
     * Deal seq starts with institutionid = 100 
     * but SourceFirmProfile starts with 315
     * so that can not test with out chaning test data retreive framework
     */
    @Ignore
    public void testFindBySourceAndDealSystemType()  throws Exception {
      
        _logger.info(BORDER_START, "findBySourceAndDealSystemType");
        
        
        // get desl info from Deal
        int testDealSystemId = _dataRepository.getInt("DEAL", 
                                                      "SYSTEMID",
                                                      0);
        int testDealDealId = _dataRepository.getInt("DEAL", "DEALID", 0);
        int testDealCopyId = _dataRepository.getInt("DEAL", "COPYID", 0);
        int testDealInstPId = _dataRepository.getInt("DEAL", "INSTITUTIONPROFILEID", 0);
        
        int testSFPSeq = 0;
        int testSystemId = 0;
        int testSourceFirmProfileId = 0;
        int testSOBProfileId = 0;
        int testInstitutionProfileId = 0;
        while (testSystemId != testDealSystemId
                        && testInstitutionProfileId != testDealInstPId)
        {
            // get random seqence number
            Double indexD = new Double(Math.random() * DATA_COUNT);
            testSFPSeq = indexD.intValue();

            testInstitutionProfileId
                = _dataRepository.getInt(SourceOfBusinessProfileTest.ENTITY_NAME,  
                                         "INSTITUTIONPROFILEID", 
                                         testSFPSeq);
            
            testSourceFirmProfileId 
                = _dataRepository.getInt(SourceOfBusinessProfileTest.ENTITY_NAME,  
                                         "SOURCEFIRMPROFILEID", 
                                         testSFPSeq);
            testSOBProfileId 
                = _dataRepository.getInt(SourceOfBusinessProfileTest.ENTITY_NAME, 
                                         "SOURCEOFBUSINESSPROFILEID", 
                                         testSFPSeq);
            testSystemId
                = _dataRepository.getInt(SourceOfBusinessProfileTest.ENTITY_NAME, 
                                         "SYSTEMTYPEID", 
                                         testSFPSeq);
            
        }
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(testInstitutionProfileId);

        // excute method
        SourceFirmProfile sfProfile = new SourceFirmProfile(_srk);
        Collection sfs  
            = sfProfile.findBySourceAndDealSystemType(new SourceOfBusinessProfilePK(testSOBProfileId), 
                                                      new DealPK(testDealDealId, testDealCopyId));
        
        // check assertEqual 
        Iterator profileIter = sfs.iterator();
        for (; profileIter.hasNext(); ) {
            assertEqualsProperties((SourceFirmProfile)profileIter.next());
        }

        _logger.info(BORDER_END, "findBySourceAndDealSystemType");
    }
    
    /**
     * test findBySystemTypeAndSFCode.
     */
    @Test
    public void testFindBySystemTypeAndSFCode()  throws Exception {
      
        _logger.info(BORDER_START, "findBySystemTypeAndSFCode");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
        
        // excute method
        SourceFirmProfile sfProfile = new SourceFirmProfile(_srk);
        sfProfile 
            = sfProfile.findBySystemTypeAndSFCode(systemTypeId, sourceFirmCode);
        
        // check assertEqual 
        assertEqualsProperties(sfProfile);

        _logger.info(BORDER_END, "findBySystemTypeAndSFCode");
    }

    
    /**
     * assertEqualProperties
     * 
     * check assertEquals for all properties of SourceFirmProfile with test Data
     * 
     * @param entity: SourceFirmProfile
     * @throws Exception
     */
    private void assertEqualsProperties(SourceFirmProfile entity) throws Exception
    {
        assertEquals(entity.getSourceFirmName(), sourceFirmName);
        assertEquals(entity.getSystemTypeId(), systemTypeId);
        assertEquals(entity.getSourceFirmProfileId(), sourceFirmProfileId);
        assertEquals(entity.getContactId(), contactId);
        assertEquals(entity.getProfileStatusId(), profileStatusId);
        assertEquals(entity.getSfShortName(), sfShortName);
        assertEquals(entity.getSfBusinessId(), sfBusinessId);
        assertEquals(entity.getAlternativeId(), alternativeId);
        assertEquals(entity.getSfMOProfileId(), sfMOProfileId);
        assertEquals(entity.getSourceFirmCode(), sourceFirmCode);
        assertEquals(entity.getInstitutionProfileId(), instProfId);

    }
    
    
    @Test
    public void testSFLicenseRegistrationNumber() throws Exception {
      
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        SourceFirmProfile sfProfile = new SourceFirmProfile(_srk);
        sfProfile  = sfProfile.findByPrimaryKey(new SourceFirmProfilePK(sourceFirmProfileId));
        
        String sfLicenseRegistrationNumber = "test";
        sfProfile.setSfLicenseRegistrationNumber(sfLicenseRegistrationNumber);
        assertEquals(sfLicenseRegistrationNumber, sfProfile.getSfLicenseRegistrationNumber());
        
    }

}
