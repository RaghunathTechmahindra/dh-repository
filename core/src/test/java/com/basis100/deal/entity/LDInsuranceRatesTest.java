package com.basis100.deal.entity;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.LDInsuranceRatesPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * 
 * LDInsuranceRatesTest
 * 
 * @version 1.0 Sep 5, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class LDInsuranceRatesTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(LDInsuranceRatesTest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test the find by primary key
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {
        // get input data from repository
        int insuranceRateID = _dataRepository.getInt("LDInsuranceRates",
                "LDINSURANCERATEID", 0);
        int dealInstitutionId = _dataRepository.getInt("LDInsuranceRates",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "findByPrimaryKey");

        // Create the initial entity
        LDInsuranceRates _lDInsuranceRates = new LDInsuranceRates(_srk);

        // find by primary key
        _lDInsuranceRates = _lDInsuranceRates
                .findByPrimaryKey(new LDInsuranceRatesPK(insuranceRateID));

        // Check if the data retrieved matches the DB
        assertTrue(_lDInsuranceRates.getLDRateCode().equals(
                _dataRepository.getString("LDInsuranceRates", "LDRATECODE", 0)));

        _logger.info(BORDER_END, "findByPrimaryKey");

    }

    /**
     * test the find by rate code
     */
    @Test
    public void testFindByRateCode() throws Exception {
        // get input data from repository
        String insuranceRateCode = _dataRepository.getString(
                "LDInsuranceRates", "LDRATECODE", 0);
        int dealInstitutionId = _dataRepository.getInt("LDInsuranceRates",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "findByRateCode");

        // Create the initial entity
        LDInsuranceRates _lDInsuranceRates = new LDInsuranceRates(_srk);

        // find by rate code
        List insuranceRates = _lDInsuranceRates
                .findByRateCode(insuranceRateCode);

        // Check if the data retrieved matches the DB
        assertTrue(((LDInsuranceRates) insuranceRates.get(0)).getLDRateCode()
                .equals(insuranceRateCode));

        _logger.info(BORDER_END, "findByRateCode");

    }

    /**
     * test the find by rate code desc
     */
    @Test
    public void testFindByRateCodeDesc() throws Exception {
        // get input data from repository
        String insuranceRateCodeDesc = _dataRepository.getString(
                "LDInsuranceRates", "LDRATEDESC", 0);
        int dealInstitutionId = _dataRepository.getInt("LDInsuranceRates",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "findByRateCodeDesc");

        // Create the initial entity
        LDInsuranceRates _lDInsuranceRates = new LDInsuranceRates(_srk);

        // find by rate code
        List insuranceRates = _lDInsuranceRates
                .findByRateCodeDescription(insuranceRateCodeDesc);

        // Check if the data retrieved matches the DB
        assertTrue(((LDInsuranceRates) insuranceRates.get(0))
                .getLDRateDescription().equals(insuranceRateCodeDesc));

        _logger.info(BORDER_END, "findByRateCodeDesc");

    }

}
