package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Date;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.ServiceResponseQueuePK;

/**
 * <p>ServiceResponseQueueDbTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class ServiceResponseQueueDbTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private ServiceResponseQueue serviceResponseQueue;
	
	public ServiceResponseQueueDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ServiceResponseQueue.class.getSimpleName() + "DataSetTest.xml"));
	}
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		 int id = 622;
		 int copyId = 1;
		this.serviceResponseQueue = new ServiceResponseQueue(srk,null,id,copyId);
		
	}
	
	public void testFindNextUnprocessedResponse() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindNextUnprocessedResponse");
    	int maxTry = 6;
    	int retryIntervalSec = 100;

    	serviceResponseQueue.findNextUnprocessedResponse(maxTry, retryIntervalSec);
	    assertNotNull(serviceResponseQueue);
    }
	
	public void testFindLatestByDealid() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindLatestByDealid");
    	int dealId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DEALID"));
    	int serviceProductId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "SERVICEPRODUCTID"));
    	
    	serviceResponseQueue.findLatestByDealid(dealId, serviceProductId);
	    assertNotNull(serviceResponseQueue);
    }
	
	public void testFindAvailableByDealIdAndServProdId() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindAvailableByDealIdAndServProdId");
    	int dealId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DEALID"));
    	int serviceProductId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "SERVICEPRODUCTID"));
    	
    	serviceResponseQueue.findAvailableByDealIdAndServProdId(dealId, serviceProductId);
	    assertNotNull(serviceResponseQueue);
    }
	
	public void testFindOpenByTransactionkey() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindOpenByTransactionkey");
    	String keyId = (String)(expected.getValue(0, "CHANNELTRANSACTIONKEY"));
    	String requestType = (String)(expected.getValue(0, "REQUESTTYPE"));
    	
    	serviceResponseQueue.findOpenByTransactionkey(keyId, requestType);
	    assertNotNull(serviceResponseQueue);
    }
	
	public void testCreatePrimaryKey() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreatePrimaryKey");
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	srk.beginTransaction();
    	serviceResponseQueue.createPrimaryKey(copyId);
    	srk.rollbackTransaction();
	    assertNotNull(serviceResponseQueue);
    }
	
	/*public void testCreate() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreate");
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "SERVICERESPONSEQUEUEID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	int serviceProductId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "SERVICEPRODUCTID"));
    	int processStatusId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "PROCESSSTATUSID"));
    	int retryCounter = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "RETRYCOUNTER"));
       	srk.beginTransaction();
    	serviceResponseQueue.create(new DealPK(id,copyId), serviceProductId, processStatusId, retryCounter, null, null);
	    srk.rollbackTransaction();
    	assertNotNull(serviceResponseQueue);
    }
	
		public void testCreate1() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreate1");
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "SERVICERESPONSEQUEUEID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	int serviceProductId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "SERVICEPRODUCTID"));
    	int processStatusId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "PROCESSSTATUSID"));
    	int retryCounter = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "RETRYCOUNTER"));
    	srk.beginTransaction();
    	serviceResponseQueue.create(new ServiceResponseQueuePK(id), new DealPK(id,copyId), copyId, serviceProductId, processStatusId, retryCounter, null, null);
	    srk.rollbackTransaction();
    	assertNotNull(serviceResponseQueue);
    }*/
}