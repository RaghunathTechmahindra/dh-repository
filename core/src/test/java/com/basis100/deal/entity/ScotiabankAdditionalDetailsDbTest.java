package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.DealPK;


/**
 * <p>ScotiabankAdditionalDetailsDbTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class ScotiabankAdditionalDetailsDbTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private ScotiabankAdditionalDetails scotiabankAdditionalDetails;
	
	public ScotiabankAdditionalDetailsDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ScotiabankAdditionalDetails.class.getSimpleName() + "DataSetTest.xml"));
	}

	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.scotiabankAdditionalDetails = new ScotiabankAdditionalDetails(srk);
		
	}
	
	/*public void testCreate() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreate");
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "SCOTIABANKAPPLICATIONNUMBER"));
    	int payor = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "RATEBUYDOWNPAYORID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	srk.beginTransaction();
    	scotiabankAdditionalDetails.create(new DealPK(id,copyId), payor);
    	srk.rollbackTransaction();
	    assertNotNull(scotiabankAdditionalDetails);
    }
	
	public void testEquals() throws Exception {
    	ITable expected = dataSetTest.getTable("testEquals");
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "SCOTIABANKAPPLICATIONNUMBER"));
    	
    	scotiabankAdditionalDetails.equals(o);
	    assertNotNull(scotiabankAdditionalDetails);
    }
	
	public void testFindByDeal() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByDeal");
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "SCOTIABANKAPPLICATIONNUMBER"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	
    	scotiabankAdditionalDetails.findByDeal(new DealPK(id,copyId));
	    assertNotNull(scotiabankAdditionalDetails);
    }
	
	public void testClone() throws Exception {
    	ITable expected = dataSetTest.getTable("testClone");
    	//int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "SCOTIABANKAPPLICATIONNUMBER"));
    	
    	scotiabankAdditionalDetails.clone();
	    assertNotNull(scotiabankAdditionalDetails);
    }*/
	
	public void testClone1() throws Exception {
    	ITable expected = dataSetTest.getTable("testClone1");
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "SCOTIABANKAPPLICATIONNUMBER"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	
    	scotiabankAdditionalDetails.clone(new DealPK(id,copyId));
	    assertNotNull(scotiabankAdditionalDetails);
    }
	
}