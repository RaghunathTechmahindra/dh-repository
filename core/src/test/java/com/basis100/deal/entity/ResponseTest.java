/*
 * ResponseTest.java    2007-9-7
 *
 * Copyright (C) 2007 Filogix Limited Partnership All rights reserved.
 * 
 * 
 */
package com.basis100.deal.entity;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>ResponseTest</p>
 * Express Entity class unit test: Response
 */
public class ResponseTest extends ExpressEntityTestCase 
    implements UnitTestLogging {

    private static final Logger _logger = 
        LoggerFactory.getLogger(ResponseTest.class);
    private static final String ENTITY_NAME = "RESPONSE"; 
  
    // retreived data count that is stored in UnitTestDataRepository.xml
    private static final int DATA_COUNT = 100;

    private int testSeqForGen = 0;
    
    private SessionResourceKit _srk;
    
    // properties     
    private int responseId;
    private Date responseDate;
    private int requestId;
    private int dealId;
    private int responseStatusId;
    private Date statusDate;
    private String statusMessage;
    private String channelTransactionKey;
    private int instProfId;
  

    /**
     * <p>setUp</p>
     * Setting up test case.
     */
    @Before
    public void setUp() throws Exception {
      
        _logger.info(BORDER_START, ENTITY_NAME + " TestCase setting up ...");
        
        // get random seqence number
        // channelTransactionKey should not be null
        while (channelTransactionKey == null) {
            Double indexD = new Double(Math.random() * DATA_COUNT);
            testSeqForGen = indexD.intValue();
            channelTransactionKey
              = _dataRepository.getString(ENTITY_NAME,  
                                          "CHANNELTRANSACTIONKEY", 
                                          testSeqForGen);
        }
      
        //      get test data from repository
        responseId 
            = _dataRepository.getInt(ENTITY_NAME,  "RESPONSEID", testSeqForGen);
        responseDate = _dataRepository.getDate(ENTITY_NAME,  
                                               "RESPONSEDATE", 
                                               testSeqForGen);
        requestId 
            = _dataRepository.getInt(ENTITY_NAME,  "REQUESTID", testSeqForGen);
        dealId 
            = _dataRepository.getInt(ENTITY_NAME,  "DEALID", testSeqForGen);
        responseStatusId = _dataRepository.getInt(ENTITY_NAME,  
                                                  "RESPONSESTATUSID", 
                                                  testSeqForGen);
        statusDate 
            = _dataRepository.getDate(ENTITY_NAME,  "STATUSDATE", testSeqForGen);
        statusMessage = _dataRepository.getString(ENTITY_NAME,  
                                                  "STATUSMESSAGE", 
                                                  testSeqForGen);
        channelTransactionKey = _dataRepository.getString(ENTITY_NAME,  
                                                          "CHANNELTRANSACTIONKEY", 
                                                          testSeqForGen);
        instProfId = _dataRepository.getInt(ENTITY_NAME,  
                                            "INSTITUTIONPROFILEID", 
                                            testSeqForGen);

        // init ResouceManager
        ResourceManager.init();
        //  init session resource kit.
        _srk = new SessionResourceKit();

  
        _logger.info(BORDER_END, "Setting up Done");
    }
    
    @After
    public void tearDown() {

      //      free resources
      _srk.freeResources( );

    }
  
   
  @Test
  /*  public void testFindByPrimaryKey()  throws Exception {
      
        _logger.info(BORDER_START, "findByPrimaryKey");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        Response entity = new Response(_srk);    
        entity = entity.findByPrimaryKey(new ResponsePK(responseId));
        
        assertEqualsProperties(entity);
        
        _logger.info(BORDER_END, "findByPrimaryKey");
    }
    
   /* *//**
     * test FindByRequestId.
     *//*
    @Test
    public void testFindByRequestId()  throws Exception {
        
        _logger.info(BORDER_START, "FindByRequestId");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        Response entity = new Response(_srk);
        Collection responses = entity.findByRequestId(requestId);
        
        // assertEquals only for insitutionProfileId, requestId and dealId
        Iterator responseIter = responses.iterator();
        for (; responseIter.hasNext(); )
        {
          Response aResponse = (Response) responseIter.next();
          assertEquals(aResponse.getInstitutionProfileId(), instProfId);
          assertEquals(aResponse.getRequestId(), requestId);
          assertEquals(aResponse.getDealId(), dealId);
        }

        _logger.info(BORDER_END, "FindByRequestId");
    }
 
    /**
     * test FindLastResponseByRequestId.
     */
  /*  @Test
    public void testFindLastResponseByRequestId()  throws Exception {
      
        _logger.info(BORDER_START, "FindLastResponseByRequestId");

        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
    
        // excute method
        Response resposne = new Response(_srk);
        resposne = resposne.findLastResponseByRequestId(requestId);
        
        //assertEquals
        // TODO: should test other fields
        // current test data structure has difficulties - Sep 6, 2007
        assertEquals(resposne.getRequestId(), requestId);
        assertEquals(resposne.getInstitutionProfileId(), instProfId);

        _logger.info(BORDER_END, "FindLastResponseByRequestId");
    }
*/
    /**
     * test findByDeal.
     * this method doesn't make sense
     * Entity Class bug
     *//*
    @Ignore
    public void testFindByDeal()  throws Exception {
      
        _logger.info(BORDER_START, "findByDeal");

        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
    
        // excute method
        Response resposne = new Response(_srk);
        Collection responses = resposne.findByDeal(new DealPK(dealId, 0));
                
        // assertEquals only for insitutionProfileId, requestId and dealId
        Iterator responseIter = responses.iterator();
        for (; responseIter.hasNext(); )
        {
          Response aResponse = (Response) responseIter.next();
          assertEquals(aResponse.getInstitutionProfileId(), instProfId);
          assertEquals(aResponse.getDealId(), dealId);
        }

        _logger.info(BORDER_END, "findByDeal");
    }*/
/*
    *//**
     * test findByChannelTransactionKey.
     * this method returne latest response with channelTransactionkey
     * Entity Class bug
     *//*
    @Test
    public void testFindByChannelTransactionKey()  throws Exception {
        
        _logger.info(BORDER_START, "findByChannelTransactionKey");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        Response entity = new Response(_srk);
        entity = entity.findByChannelTransactionKey(channelTransactionKey);
        
        // assertEquals only for insitutionProfileId, requestId and dealId
        assertEquals(entity.getInstitutionProfileId(), instProfId);
        assertEquals(entity.getChannelTransactionKey(), channelTransactionKey);
        assertEquals(entity.getRequestId(), requestId);

        _logger.info(BORDER_END, "findByChannelTransactionKey");
    }

    *//**
     * test findByRequest.
     * this method doesn't make sense
     *//*
    @Ignore
    public void testFindByRequest()  throws Exception {
        
        _logger.info(BORDER_START, "findByRequest");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        Response resposne = new Response(_srk);
        Collection responses = resposne.findByRequest(new RequestPK(requestId, 0));
                
        // assertEquals only for insitutionProfileId, requestId and dealId
        Iterator responseIter = responses.iterator();
        for (; responseIter.hasNext(); )
        {
          Response aResponse = (Response) responseIter.next();
          assertEquals(aResponse.getInstitutionProfileId(), instProfId);
          assertEquals(aResponse.getDealId(), dealId);
        }

        _logger.info(BORDER_END, "findByRequest");
    }
    
    *//**
     * test create.
     *//*
    @Test
    public void testCreate() throws Exception {
      
        _logger.info(BORDER_START, "create");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
        
        // set beginTransaction for auto commit off
        _srk.beginTransaction();

        Date now = new Date();
        // excute method
        Response entity = new Response(_srk);
        entity = entity.create(now, requestId, new DealPK(dealId, 0), now, responseStatusId);        
        
        // assertEquals
        assertEquals(entity.getRequestId(), requestId);
        assertEquals(entity.getResponseDate().toString(), now.toString());
        assertEquals(entity.getDealId(), dealId);
        assertEquals(entity.getResponseStatusId(), responseStatusId);
        assertEquals(entity.getResponseDate().toString(), now.toString());
        //assertEquals(entity.getStatusMessage(), statusMessage);
        assertEquals(entity.getInstitutionProfileId(), instProfId);

        // rollback
        _srk.rollbackTransaction();
        
        _logger.info(BORDER_END, "create");
    }

    
    *//**
     * assertEqualProperties
     * 
     * check assertEquals for all properties of Response with  test Data
     * 
     * @param entity: Response
     * @throws Exception
     *//*
    private void assertEqualsProperties(Response entity)
    {
      assertEquals(entity.getResponseId(), responseId);
      assertEquals(entity.getResponseDate(), responseDate);
      assertEquals(entity.getRequestId(), requestId);
      assertEquals(entity.getDealId(), dealId);
      assertEquals(entity.getResponseStatusId(), responseStatusId);
      assertEquals(entity.getStatusDate(), statusDate);
      assertEquals(entity.getStatusMessage(), statusMessage);
      assertEquals(entity.getChannelTransactionKey(), channelTransactionKey);
      assertEquals(entity.getInstitutionProfileId(), instProfId);
      
    }*/
    public void testTBR(){
        System.out.println("test");
}

}
