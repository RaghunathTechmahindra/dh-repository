package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.DcFolderPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;

public class DcFolderTest1 extends FXDBTestCase{
	private DcFolder dcFolder;
	private String INIT_XML_DATA = "DcFolderDataSet.xml";
	IDataSet dataSetTest;;
	public DcFolderTest1(String name) throws DataSetException, IOException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"DcFolderDataSetTest.xml"));
	}

	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		dcFolder = new DcFolder(srk);

	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}
	
	public void testfindBySourceAppId() throws DataSetException, FinderException{
		String sourceAppId=dataSetTest.getTable("testfindBySourceAppId").getValue(0, "sourceAppId").toString();
		DcFolder folder=dcFolder.findBySourceAppId(sourceAppId);
		assertNotNull(folder);
		assertEquals(sourceAppId,folder.sourceApplicationId);
	}
	//Programmatically code is throwing Exception if No Record Found.
	/*public void testfindBySourceAppIdFailure() throws DataSetException, FinderException{
		String sourceAppId=dataSetTest.getTable("testfindBySourceAppIdFailure").getValue(0, "sourceAppId").toString();
		DcFolder folder=dcFolder.findBySourceAppId(sourceAppId);
		
		assertNull(folder);
		//assertEquals(sourceAppId,folder.sourceApplicationId);
	}*/
	public void testCreate() throws JdbcTransactionException, DataSetException, RemoteException, CreateException{
		srk.beginTransaction();
		String sourceAppId=dataSetTest.getTable("testCreate").getValue(0, "sourceAppId").toString();
		String folderCode=dataSetTest.getTable("testCreate").getValue(0, "folderCode").toString();
		String folderStatusCode=dataSetTest.getTable("testCreate").getValue(0, "folderStatusCode").toString();
		String folderStatusDesc=dataSetTest.getTable("testCreate").getValue(0, "folderStatusDesc").toString();
		DcFolder folder=dcFolder.create(sourceAppId, folderCode, folderStatusCode, folderStatusDesc);
		assertNotNull(folder);
		assertEquals(sourceAppId,folder.sourceApplicationId);
		assertTrue(srk.getModified());
		srk.rollbackTransaction();
	}
	public void testCreatePrimaryKey() throws CreateException{
		DcFolderPK folderPK=dcFolder.createPrimaryKey();
		assertNotNull(folderPK);
	}
	
}
