package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.pk.ChargeTermPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.workflow.pk.TaskBeanPK;

public class ChargeTermDbTest extends FXDBTestCase {
	private ChargeTerm chargeTerm;
	private String INIT_XML_DATA = "ChargeTermDataSet.xml";
	IDataSet dataSetTest;

	public ChargeTermDbTest(String name) throws DataSetException, IOException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"ChargeTermDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}

	@Override
	public void setUp() throws Exception {
		super.setUp();
		// srk.getExpressState().setDealInstitutionId(0);
		chargeTerm = new ChargeTerm(srk);

	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	public void testCreate() throws JdbcTransactionException, RemoteException,
			CreateException, NumberFormatException, DataSetException {
		srk.beginTransaction();
		int provinceId = Integer.parseInt(dataSetTest.getTable("testCreate")
				.getValue(0, "provinceId").toString());
		int languagePrefId = Integer.parseInt(dataSetTest
				.getTable("testCreate").getValue(0, "languagePrefId")
				.toString());
		int lenderProfileId = Integer.parseInt(dataSetTest
				.getTable("testCreate").getValue(0, "lenderProfileId")
				.toString());
		int dealInstitutionId = Integer.parseInt(dataSetTest
				.getTable("testCreate").getValue(0, "dealInstitutionId")
				.toString());
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		chargeTerm.create(provinceId, languagePrefId, lenderProfileId);
		assertEquals(true, srk.getModified());
		srk.rollbackTransaction();

	}

	public void testFindFirstByLenderProvinceAndQualifier() throws RemoteException, FinderException, NumberFormatException, DataSetException {
		int provinceId = Integer.parseInt(dataSetTest.getTable("testFindFirstByLenderProvinceAndQualifier")
				.getValue(0, "provinceId").toString());
		String qualifier = dataSetTest
				.getTable("testFindFirstByLenderProvinceAndQualifier").getValue(0, "qualifier")
				.toString();
		int lenderProfileId = Integer.parseInt(dataSetTest
				.getTable("testFindFirstByLenderProvinceAndQualifier").getValue(0, "lenderProfileId")
				.toString());
		ChargeTerm chageTemByQualifier=chargeTerm.findFirstByLenderProvinceAndQualifier(lenderProfileId,provinceId,qualifier);
		assertEquals(0, chageTemByQualifier.getLenderProfileId());
	}
	
	public void testFindByPrimaryKey() throws RemoteException, FinderException, NumberFormatException, DataSetException {
		ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKey");
		int chargeTermId = Integer.parseInt((String)testFindByPrimaryKey.getValue(0,"chargeTermId"));
		ChargeTermPK pk = new ChargeTermPK(chargeTermId);
		chargeTerm= chargeTerm.findByPrimaryKey(pk);
		assertEquals(chargeTermId, pk.getId());
	}
	
	public void testFindByPrimaryKeyFailure() throws RemoteException, FinderException, NumberFormatException, DataSetException {
		ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKeyFailure");
		boolean status=false;
		try{
		int chargeTermId = Integer.parseInt((String)testFindByPrimaryKey.getValue(0,"chargeTermId"));
		ChargeTermPK pk = new ChargeTermPK(chargeTermId);
		chargeTerm= chargeTerm.findByPrimaryKey(pk);
		status=true;
		}catch(Exception e){
			status=false;
		}
		assertEquals(false, status);
	}
}
