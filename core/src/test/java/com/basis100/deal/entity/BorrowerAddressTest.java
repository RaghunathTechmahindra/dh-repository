/*
 * @(#)BorrowerAddressTest.java Sep 19, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.BorrowerAddressPK;
import com.basis100.deal.pk.AddrPK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * @author amalhi
 * 
 * JUnit test for entity BorrowerAddress
 * 
 */
public class BorrowerAddressTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(BorrowerAddressTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public BorrowerAddressTest() {

    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.BorrowerAddress#findByPrimaryKey(com.basis100.deal.pk.BorrowerAddressPK)}.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int borrowerAddressId = _dataRepository.getInt("BorrowerAddress",
                "borrowerAddressId", 0);
        int instProfId = _dataRepository.getInt("BorrowerAddress",
                "INSTITUTIONPROFILEID", 0);
        int copyId = _dataRepository.getInt("BorrowerAddress", "copyId", 0);
        int borrowerId = _dataRepository.getInt("BorrowerAddress",
                "borrowerId", 0);

        _logger.info(BORDER_START, "testFindByPrimaryKey");
        _logger.info("Got Values from new BorrowerAddress ::: [{}]",
                borrowerAddressId + "::" + instProfId + "::" + copyId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        BorrowerAddress _borrowerAddress = new BorrowerAddress(_srk);

        // Find by primary key
        _borrowerAddress = _borrowerAddress
                .findByPrimaryKey(new BorrowerAddressPK(borrowerAddressId,
                        copyId));

        _logger.info("Got Values from new BorrowerAddress ::: [{}]",
                _borrowerAddress.getBorrowerAddressId() + "::"
                        + _borrowerAddress.getCopyId());

        // Check if the data retrieved matches the DB
        assertEquals(_borrowerAddress.getBorrowerAddressId(), borrowerAddressId);
        assertEquals(_borrowerAddress.getCopyId(), copyId);
        assertEquals(_borrowerAddress.getBorrowerId(), borrowerId);

        _logger.info(BORDER_END, "testFindByPrimaryKey");
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.BorrowerAddress#findByAddr(com.basis100.deal.pk.AddrPK)}.
     */
    @Test
    public void testFindByAddr() throws Exception {

        // get input data from repository
        int addrId = _dataRepository.getInt("BorrowerAddress", "addrId", 0);
        int instProfId = _dataRepository.getInt("BorrowerAddress",
                "INSTITUTIONPROFILEID", 0);
        int copyId = _dataRepository.getInt("BorrowerAddress", "copyId", 0);
        int borrowerId = _dataRepository.getInt("BorrowerAddress",
                "borrowerId", 0);

        _logger.info(BORDER_START, "testFindByAddr");
        _logger.info("Got Values from new BorrowerAddress ::: [{}]", addrId
                + "::" + instProfId + "::" + copyId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        BorrowerAddress _borrowerAddress = new BorrowerAddress(_srk);

        // Find by primary key
        _borrowerAddress = _borrowerAddress.findByAddr(new AddrPK(addrId,
                copyId));

        _logger.info("Got Values from new BorrowerAddress ::: [{}]",
                _borrowerAddress.getBorrowerAddressId() + "::"
                        + _borrowerAddress.getCopyId());

        // Check if the data retrieved matches the DB
        assertEquals(_borrowerAddress.getAddrId(), addrId);
        assertEquals(_borrowerAddress.getCopyId(), copyId);
        assertEquals(_borrowerAddress.getBorrowerId(), borrowerId);

        _logger.info(BORDER_END, "testFindByAddr");
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.BorrowerAddress#findByCurrentAddress(int,int)}.
     */
    @Ignore
    public void testFindByCurrentAddress() throws Exception {

        // get input data from repository
        int borrowerAddressId = _dataRepository.getInt("BorrowerAddress",
                "borrowerAddressId", 0);
        int instProfId = _dataRepository.getInt("BorrowerAddress",
                "INSTITUTIONPROFILEID", 0);
        int copyId = _dataRepository.getInt("BorrowerAddress", "copyId", 0);
        int borrowerId = _dataRepository.getInt("BorrowerAddress",
                "borrowerId", 0);

        _logger.info(BORDER_START, "testFindByCurrentAddress");
        _logger.info("Got Values from new BorrowerAddress ::: [{}]",
                borrowerAddressId + "::" + instProfId + "::" + copyId + "::"
                        + borrowerId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        BorrowerAddress _borrowerAddress = new BorrowerAddress(_srk);

        // Find by primary key
        _borrowerAddress = _borrowerAddress.findByCurrentAddress(borrowerId,
                copyId);

        _logger.info("Got Values from new BorrowerAddress ::: [{}]",
                _borrowerAddress.getBorrowerAddressId() + "::"
                        + _borrowerAddress.getCopyId());

        // Check if the data retrieved matches the DB
        assertEquals(_borrowerAddress.getBorrowerAddressId(), borrowerAddressId);
        assertEquals(_borrowerAddress.getCopyId(), copyId);
        assertEquals(_borrowerAddress.getBorrowerId(), borrowerId);

        _logger.info(BORDER_END, "testFindByCurrentAddress");
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.BorrowerAddress#create(com.basis100.deal.pk.BorrowerPK)}.
     */
    @Test
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create - BorrowerAddress");

        // get input data from repository
        int instProfId = _dataRepository.getInt("BorrowerAddress",
                "INSTITUTIONPROFILEID", 0);
        int borrowerId = _dataRepository.getInt("Borrower", "borrowerId", 0);
        int copyId = _dataRepository.getInt("Borrower", "copyId", 0);
        int borrowerAddressId = _dataRepository.getInt("BorrowerAddress",
                "borrowerAddressId", 0);
        _logger
                .info("Got Values from new BorrowerAddress ::: [{}]",
                        instProfId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // begin transaction
        _srk.beginTransaction();

        // Create the initial entity
        BorrowerAddress newEntity = new BorrowerAddress(_srk);

        // call create method of entity
        newEntity = newEntity.create(new BorrowerPK(borrowerId, copyId));

        _logger.info("CREATED BorrowerAddress ::: [{}]", newEntity
                .getBorrowerId()
                + "::" + newEntity.getCopyId());

        // check create correctly by calling findByPrimaryKey
        BorrowerAddress foundEntity = new BorrowerAddress(_srk);
        foundEntity = foundEntity.findByPrimaryKey(new BorrowerAddressPK(
                newEntity.getBorrowerAddressId(), copyId));

        // verifying the created entity
        assertEquals(foundEntity.getBorrowerAddressId(), newEntity
                .getBorrowerAddressId());
        assertEquals(foundEntity.getCopyId(), newEntity.getCopyId());
        assertEquals(foundEntity.getBorrowerId(), newEntity.getBorrowerId());

        _logger.info(BORDER_END, "Create - BorrowerAddress");

        // roll-back transaction.
        _srk.rollbackTransaction();
    }

}
