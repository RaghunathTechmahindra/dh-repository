package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.AdjudicationResponsePK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.ResponsePK;

public class AdjudicationResponseTest1 extends  FXDBTestCase {

	private IDataSet dataSetTest;
	AdjudicationResponse adjudicationResponse=null;
	AdjudicationResponsePK adjudicationResponsePK=null;
	ResponsePK responsePK=null;
	DealPK dealPK=null;
	public AdjudicationResponseTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("AdjudicationResponseDataSetTest.xml"));
	}

	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		adjudicationResponse=new AdjudicationResponse(srk);
	}

	public void testFindByPrimaryKey() throws Exception {
		ITable testFindByRequest = dataSetTest.getTable("testFindByPrimaryKey");
		int  id = Integer.parseInt(testFindByRequest.getValue(0,"Id").toString());
		adjudicationResponsePK=new AdjudicationResponsePK(id);
		AdjudicationResponse adjudicationResponse1=adjudicationResponse.findByPrimaryKey(adjudicationResponsePK);
		assertNotNull(adjudicationResponse1);
	}

	public void testFindByPrimaryKeyFailure() throws Exception {
		
		boolean status=false;
		try
		{
		ITable testFindByPrimaryKeyFailure = dataSetTest.getTable("testFindByPrimaryKeyFailure");
		int  id = Integer.parseInt(testFindByPrimaryKeyFailure.getValue(0,"Id").toString());
		adjudicationResponsePK=new AdjudicationResponsePK(id);
		AdjudicationResponse adjudicationResponse1=adjudicationResponse.findByPrimaryKey(adjudicationResponsePK);
		}catch(Exception e){
			 status=false;
		}
		assertSame(false,status);
	}
	public void testEquales()  throws Exception{
		ITable testFindByPrimaryKeyFailure = dataSetTest.getTable("testEquales");
		int  id = Integer.parseInt(testFindByPrimaryKeyFailure.getValue(0,"Id").toString());
		adjudicationResponse.setResponseId(id);
		Object o= null;
    	o=new AdjudicationResponse(srk);
    	boolean status=adjudicationResponse.equals(o);
    	System.out.println("Status:"+status);
    	assertSame(true, status);
	}
	public void testEqualesFailure()  throws Exception{
		ITable testEqualesFailure = dataSetTest.getTable("testEqualesFailure");
		int  id = Integer.parseInt(testEqualesFailure.getValue(0,"Id").toString());
		adjudicationResponse.setResponseId(id);
		Object object=new AdjudicationResponse(srk);
    	boolean status=adjudicationResponse.equals(object);
    	assertSame(false, status);
	}
	
	public void testFindByResponse() throws Exception{
		ITable testEqualesFailure = dataSetTest.getTable("testFindByResponse");
		int  id = Integer.parseInt(testEqualesFailure.getValue(0,"Id").toString());
		responsePK=new ResponsePK(id);
		int size=adjudicationResponse.findByResponse(responsePK).size();
		assertNotSame(0,size);
	}

	public void testFindByResponseFailure() throws Exception{
		ITable testEqualesFailure = dataSetTest.getTable("testFindByResponseFailure");
		int  id = Integer.parseInt(testEqualesFailure.getValue(0,"Id").toString());
		responsePK=new ResponsePK(id);
		int size=adjudicationResponse.findByResponse(responsePK).size();
		assertSame(0,size);
	}

	public void testFindByDeal() throws Exception{
		ITable testEqualesFailure = dataSetTest.getTable("testFindByDeal");
		int  id = Integer.parseInt(testEqualesFailure.getValue(0,"Id").toString());
		int  copyId = Integer.parseInt(testEqualesFailure.getValue(0,"CopyId").toString());
		dealPK=new DealPK(id, copyId);
		int size=adjudicationResponse.findByDeal(dealPK).size();
		assertNotSame(0,size);
	}
	public void testFindByDealFailure() throws Exception{
		boolean status=false;
		try{
		ITable testEqualesFailure = dataSetTest.getTable("testFindByDealFailure");
		int  id = Integer.parseInt(testEqualesFailure.getValue(0,"Id").toString());
		int  copyId = Integer.parseInt(testEqualesFailure.getValue(0,"CopyId").toString());
		dealPK=new DealPK(id, copyId);
		int size=adjudicationResponse.findByDeal(dealPK).size();
		}catch(Exception e){
			status=false;
		}
		assertSame(false,status);
	}
	
	
	public void testFindByLastRecForThisDeal() throws Exception{
		ITable testEqualesFailure = dataSetTest.getTable("testFindByLastRecForThisDeal");
		int  id = Integer.parseInt(testEqualesFailure.getValue(0,"Id").toString());
		int  copyId = Integer.parseInt(testEqualesFailure.getValue(0,"CopyId").toString());
		dealPK=new DealPK(id, copyId);
		AdjudicationResponse adjudicationResponse1=adjudicationResponse.findByLastRecForThisDeal(dealPK);
		assertNotNull(adjudicationResponse1);
	}
	public void testFindByLastRecForThisDealFailure() throws Exception{
		ITable testEqualesFailure = dataSetTest.getTable("testtestFindByLastRecForThisDealFailure");
		int  id = Integer.parseInt(testEqualesFailure.getValue(0,"Id").toString());
		int  copyId = Integer.parseInt(testEqualesFailure.getValue(0,"CopyId").toString());
		dealPK=new DealPK(id, copyId);
		AdjudicationResponse adjudicationResponse1=adjudicationResponse.findByLastRecForThisDeal(dealPK);
		assertNull(adjudicationResponse1);
	}
}
