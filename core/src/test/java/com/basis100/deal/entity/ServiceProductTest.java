/**
 * <p>@(#)ServiceProductTest.java Sep 7, 2007</p>
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.basis100.deal.entity;

import java.util.Date;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.pk.ServiceProductPk;
import com.basis100.picklist.BXResources;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>ServiceProductTest</p>
 * Express Entity class unit test: ServiceProduct
 */
public class ServiceProductTest extends ExpressEntityTestCase 
    implements UnitTestLogging {

    private static final Logger _logger = 
        LoggerFactory.getLogger(ResponseTest.class);
    private static final String ENTITY_NAME = "SERVICEPRODUCT"; 
  
    // retreived data count that is stored in UnitTestDataRepository.xml
    private static final int DATA_COUNT = 57;

    private int testSeqForGen = 0;

    private SessionResourceKit _srk;
    
    // properties
    protected int     serviceProductId;
    protected String  serviceProductName;
    protected String  serviceProductVersion;
    protected String  serviceProductDescription;
    protected Date    startDate;
    protected Date    endDate;
    protected Date    disableDate ;
    protected int     serviceProviderId;
    protected int     serviceTypeId;
    protected int     serviceSubTypeId;
    protected int     serviceSubSubTypeId;
    private int instProfId;

    /**
     * <p>setUp</p>
     * Setting up test case.
     */
    @Before
    public void setUp() throws Exception {
      
        _logger.info(BORDER_START, ENTITY_NAME + " TestCase setting up ...");
        
        // get random seqence number
        Double indexD = new Double(Math.random() * DATA_COUNT);
        testSeqForGen = indexD.intValue();
      
        //      get test data from repository
        serviceProductId = _dataRepository.getInt(ENTITY_NAME,  
                                                  "SERVICEPRODUCTID", 
                                                  testSeqForGen);
        serviceProductName = _dataRepository.getString(ENTITY_NAME,  
                                                       "SERVICEPRODUCTNAME", 
                                                       testSeqForGen);
        serviceProductVersion = _dataRepository.getString(ENTITY_NAME,  
                                                          "SERVICEPRODUCTVERSION", 
                                                          testSeqForGen);
        serviceProductDescription = _dataRepository.getString(ENTITY_NAME,  
                                                              "SERVICEPRODUCTDESC",
                                                              testSeqForGen);
        startDate = _dataRepository.getDate(ENTITY_NAME,  
                                            "STARTDATE", 
                                            testSeqForGen);
        endDate = _dataRepository.getDate(ENTITY_NAME,  
                                          "ENDDATE", 
                                          testSeqForGen);
        disableDate = _dataRepository.getDate(ENTITY_NAME,  
                                              "DISABLEDATE", 
                                              testSeqForGen);
        serviceProviderId = _dataRepository.getInt(ENTITY_NAME,  
                                                   "SERVICEPROVIDERID", 
                                                   testSeqForGen);
        serviceTypeId = _dataRepository.getInt(ENTITY_NAME,  
                                               "SERVICETYPEID", 
                                               testSeqForGen);
        serviceSubTypeId = _dataRepository.getInt(ENTITY_NAME,  
                                                  "SERVICESUBTYPEID", 
                                                  testSeqForGen);
        serviceSubSubTypeId = _dataRepository.getInt(ENTITY_NAME, 
                                                     "SERVICESUBSUBTYPEID",
                                                     testSeqForGen);
        instProfId = _dataRepository.getInt(ENTITY_NAME,  
                                            "INSTITUTIONPROFILEID", 
                                            testSeqForGen);
           
        // init ResouceManager
        ResourceManager.init();
        //  init session resource kit.
        _srk = new SessionResourceKit();

  
        _logger.info(BORDER_END, "Setting up Done");
    }
    
    @After
    public void tearDown() {

      //      free resources
      _srk.freeResources( );

    }
  
    /**
     * test findByPrimaryKey.
     */
   /* @Test
    public void testFindByPrimaryKey()  throws Exception {
      
        _logger.info(BORDER_START, "findByPrimaryKey");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        ServiceProduct product = new ServiceProduct(_srk);
        product  = product.findByPrimaryKey(new ServiceProductPk(serviceProductId));
        
        assertEqualsProperty(product);

        _logger.info(BORDER_END, "findByPrimaryKey");
    }*/
    
    /**
     * test findByProviderAndTypeAndSubType.
     */
    /*@Test
    public void testFindByProviderAndTypeAndSubType()  throws Exception {
      
        _logger.info(BORDER_START, "findByProviderAndTypeAndSubType");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        ServiceProduct product = new ServiceProduct(_srk);
        product  = product.findByProviderAndTypeAndSubType(serviceProviderId, 
                                                           serviceTypeId, 
                                                           serviceSubTypeId, 
                                                           true);
        
        assertEqualsPropertyProvider(product);

        _logger.info(BORDER_END, "findByProviderAndTypeAndSubType");
        
    }*/

    /**
     * test findChannelIdByTransactionType.
     * 
     * although findChannelIdByTransactionType passsing serviceProviderId as the first parameter
     * checking serviceTransactionTypeId that is not attribute of ServiceProduct entity 
     * could not find any refernce of the method.
     *//*
    @Ignore
    public void testFindChannelIdByTransactionType()  throws Exception {
      
        _logger.info(BORDER_START, "findChannelIdByTransactionType");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        ServiceProduct product = new ServiceProduct(_srk);
        int channelId  = product.findChannelIdByTransactionType(serviceProviderId, 
                                                                serviceProductId);
        
        // get channelId from test Data
        int seq = 0;
        int testChannelId = 0;
        int testServiceTransactionTypeId = 0;
        while(true){
            if ( (serviceProductId == _dataRepository.getInt("SERVICEPRODUCTREQUESTASSOC",  
                                                      "SERVICEPRODUCTID", 
                                                      seq))
               &&
                 (serviceProductId == _dataRepository.getInt("SERVICEPRODUCTREQUESTASSOC",  
                                                           "SERVICETRANSACTIONTYPEID", 
                                                           seq))) {
                testChannelId = _dataRepository.getInt("SERVICEPRODUCTREQUESTASSOC",  
                                                       "CHANNELID", 
                                                       seq);
                break;
            }
            seq++;
        }
            
        assertEquals(channelId, testChannelId);
            
        _logger.info(BORDER_END, "findChannelIdByTransactionType");
        
    }*/
    
    /**
     * test getServiceSubTypeName.
     * 
     * although findChannelIdByTransactionType passsing serviceProviderId as the first parameter
     * checking serviceTransactionTypeId that is not attribute of ServiceProduct entity 
     * could not find any refernce of the method.
     */
    @Test
   /* public void testGetServiceSubTypeName()  throws Exception {
      
        _logger.info(BORDER_START, "getServiceSubTypeName");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        ServiceProduct product = new ServiceProduct(_srk);
        // get ServiceProduct with id
        product  = product.findByPrimaryKey(new ServiceProductPk(serviceProductId));
        String serviceSubTypeName = product.getServiceSubTypeName();
        
        
        assertNotNull(serviceSubTypeName);
        
    }
    */
    
  /*  *//**
     * assertEqualProperties
     * 
     * check assertEquals for all properties of ServiceProduct with test Data
     * 
     * @param entity: ServiceProduct
     * @throws Exception
     *//*
    public void assertEqualsProperty(ServiceProduct entity) throws Exception
    {
        assertEquals(entity.getServiceProductId(), serviceProductId);
        assertEquals(entity.getServiceProductName(), serviceProductName);
        assertEquals(entity.getServiceProductVersion(), serviceProductVersion);
        assertEquals(entity.getServiceProductDescription(), serviceProductDescription);
        assertEquals(entity.getStartDate(), startDate);
        assertEquals(entity.getEndDate(), endDate);
        assertEquals(entity.getDisableDate(), disableDate);
        assertEquals(entity.getServiceProviderId(), serviceProviderId);
        assertEquals(entity.getServiceTypeId(), serviceTypeId);
        assertEquals(entity.getServiceSubTypeId(), serviceSubTypeId);
        assertEquals(entity.getServiceSubSubTypeId(), serviceSubSubTypeId);

    }*/
    /*
    *//**
     * @param entity
     * @throws Exception
     *//*
    public void assertEqualsPropertyProvider(ServiceProduct entity) throws Exception
    {
        assertEquals(entity.getServiceProviderId(), serviceProviderId);
        assertEquals(entity.getServiceTypeId(), serviceTypeId);
        assertEquals(entity.getServiceSubTypeId(), serviceSubTypeId);
        assertEquals(entity.getServiceSubSubTypeId(), serviceSubSubTypeId);
    }*/
    public void testTBR(){
        System.out.println("test");
}

}
