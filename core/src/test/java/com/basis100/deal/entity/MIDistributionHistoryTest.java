package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.MIDistributionHistoryPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

public class MIDistributionHistoryTest extends ExpressEntityTestCase
    implements UnitTestLogging {

    // The logger
    private final static Logger _logger = 
        LoggerFactory.getLogger(InstitutionProfileTest.class);

    // the session resource kit.
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public MIDistributionHistoryTest() {
    }

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

        ResourceManager.init();
        _srk = new SessionResourceKit();
        _srk.getExpressState().setDealInstitutionId(11);
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
    }
    
    /**
     * test the find by primary key
     */
    @Ignore
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create");

        MIDistributionHistory distributionHistory = new MIDistributionHistory(_srk);

        _logger.info(SHORT_BREAK);
        
        distributionHistory.create(11, 100560, 2);
        
        _logger.info(BORDER_END, "Create");
    }

    /**
     * test the find by primary key
     */
    @Ignore
    public void testFindByPrimaryKey() throws Exception {

        _logger.info(BORDER_START, "findByPrimaryKey");

        MIDistributionHistory distributionHistory = new MIDistributionHistory(_srk);

        _logger.info(SHORT_BREAK);
        
        distributionHistory.findByPrimaryKey(new MIDistributionHistoryPK(5));        

        _logger.info(BORDER_END, "findByPrimaryKey");
    }
    
}
