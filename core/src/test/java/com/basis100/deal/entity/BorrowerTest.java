/*
 * @(#)BorrowerTest.java Sep 18, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * @author amalhi
 * 
 * JUnit test for entity Borrower
 * 
 */
public class BorrowerTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(BorrowerTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public BorrowerTest() {

    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.Borrower#findByPrimaryKey(com.basis100.deal.pk.BorrowerPK)}.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int borrowerId = _dataRepository.getInt("Borrower", "borrowerId", 0);
        int instProfId = _dataRepository.getInt("Borrower",
                "INSTITUTIONPROFILEID", 0);
        int copyId = _dataRepository.getInt("Borrower", "copyId", 0);
        int dealId = _dataRepository.getInt("Borrower", "dealId", 0);

        _logger.info(BORDER_START, "findByPrimaryKey");
        _logger.info("Got Values from new Borrower ::: [{}]", borrowerId + "::"
                + instProfId + "::" + copyId + "::" + dealId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        Borrower _borrower = new Borrower(_srk, CalcMonitor.getMonitor(_srk));

        // Find by primary key
        _borrower = _borrower.findByPrimaryKey(new BorrowerPK(borrowerId,
                copyId));

        _logger.info("Got Values from Borrower found::: [{}]", _borrower
                .getBorrowerId()
                + "::" + _borrower.getCopyId() + "::" + _borrower.getDealId());

        // Check if the data retrieved matches the DB
        assertEquals(_borrower.getBorrowerId(), borrowerId);
        assertEquals(_borrower.getDealId(), dealId);
        assertEquals(_borrower.getCopyId(), copyId);

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.Borrower#findByAsset(com.basis100.deal.entity.Asset)}.
     */
    @Test
    public void testFindByAsset() throws Exception {

        // get input data from repository
        int instProfId = _dataRepository.getInt("Borrower",
                "INSTITUTIONPROFILEID", 0);
        int copyId = _dataRepository.getInt("Asset", "copyId", 0);
        int assetId = _dataRepository.getInt("Asset", "assetId", 0);

        _logger.info(BORDER_START, "FindByAsset");
        _logger.info("Got Values from new Borrower ::: [{}]", instProfId + "::"
                + copyId + "::" + assetId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        Borrower _borrower = new Borrower(_srk, CalcMonitor.getMonitor(_srk));
        Asset _asset = new Asset(_srk, CalcMonitor.getMonitor(_srk), assetId,
                copyId);
        // Find by primary key
        _borrower = _borrower.findByAsset(_asset);

        _logger.info("Got Values from Borrower found::: [{}]", _borrower
                .getBorrowerId()
                + "::" + _borrower.getCopyId() + "::" + _borrower.getDealId());

        // Check if the data retrieved matches the DB
        assertEquals(_borrower.getBorrowerId(), _asset.getBorrowerId());
        assertEquals(_borrower.getCopyId(), copyId);

        _logger.info(BORDER_END, "FindByAsset");
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.Borrower#findByPrimaryBorrower}.
     */
    @Test
    public void testFindByPrimaryBorrower() throws Exception {

        // get input data from repository
        int borrowerId = _dataRepository.getInt("Borrower", "borrowerId", 0);
        int instProfId = _dataRepository.getInt("Borrower",
                "INSTITUTIONPROFILEID", 0);
        int copyId = _dataRepository.getInt("Borrower", "copyId", 0);
        int dealId = _dataRepository.getInt("Borrower", "dealId", 0);

        _logger.info(BORDER_START, "findByPrimaryBorrower");
        _logger.info("Got Values from new Borrower ::: [{}]", borrowerId + "::"
                + instProfId + "::" + copyId + "::" + dealId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        Borrower _borrower = new Borrower(_srk, CalcMonitor.getMonitor(_srk));

        // Find by primary key
        _borrower = _borrower.findByPrimaryBorrower(dealId, copyId);

        _logger.info("Got Values from Borrower found::: [{}]", _borrower
                .getBorrowerId()
                + "::" + _borrower.getCopyId() + "::" + _borrower.getDealId());

        // Check if the data retrieved matches the DB       
        assertEquals(_borrower.getDealId(), dealId);
        assertEquals(_borrower.getCopyId(), copyId);

        _logger.info(BORDER_END, "findByPrimaryBorrower");

    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.Borrower#findByFirstBorrower(int,int)}.
     */
    @Test
    public void testFindByFirstBorrower() throws Exception {

        // get input data from repository
        int instProfId = _dataRepository.getInt("Borrower",
                "INSTITUTIONPROFILEID", 0);
        int copyId = _dataRepository.getInt("Borrower", "copyId", 0);
        int dealId = _dataRepository.getInt("Borrower", "dealId", 0);
        int borrowerId = _dataRepository.getInt("Borrower", "borrowerId", 0);

        _logger.info(BORDER_START, "findByFirstBorrower");
        _logger.info("Got Values from new Borrower ::: [{}]", instProfId + "::"
                + copyId + "::" + dealId + "::" + borrowerId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        Borrower _borrower = new Borrower(_srk, CalcMonitor.getMonitor(_srk));

        // Find by primary key
        _borrower = _borrower.findByFirstBorrower(dealId, copyId);

        _logger.info("Got Values from Borrower found::: [{}]", _borrower
                .getBorrowerId()
                + "::" + _borrower.getCopyId() + "::" + _borrower.getDealId());

        // Check if the data retrieved matches the DB        
        assertEquals(_borrower.getDealId(), dealId);
        assertEquals(_borrower.getCopyId(), copyId);

        _logger.info(BORDER_END, "findByFirstBorrower");
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.Borrower#findByLiability(Liability)}.
     */
    @Test
    public void testFindByLiability() throws Exception {

        // get input data from repository
        int instProfId = _dataRepository.getInt("Borrower",
                "INSTITUTIONPROFILEID", 0);
        int copyId = _dataRepository.getInt("Liability", "copyId", 0);
        int liabilityId = _dataRepository.getInt("Liability", "liabilityId", 0);

        _logger.info(BORDER_START, "FindByLiability");
        _logger.info("Got Values from new Borrower ::: [{}]", instProfId + "::"
                + copyId + "::" + liabilityId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        Borrower _borrower = new Borrower(_srk, CalcMonitor.getMonitor(_srk));
        Liability _liability = new Liability(_srk,
                CalcMonitor.getMonitor(_srk), liabilityId, copyId);

        // Find by primary key
        _borrower = _borrower.findByLiability(_liability);

        _logger.info("Got Values from Borrower found::: [{}]", _borrower
                .getBorrowerId()
                + "::" + _borrower.getCopyId() + "::" + _borrower.getDealId());

        // Check if the data retrieved matches the DB
        assertEquals(_borrower.getBorrowerId(), _liability.getBorrowerId());
        assertEquals(_borrower.getCopyId(), copyId);

        _logger.info(BORDER_END, "FindByLiability");
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.Borrower#create(com.basis100.deal.pk.DealPK)}.
     */
    @Test
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create - Borrower");

        // get input data from repository
        int instProfId = _dataRepository.getInt("Borrower",
                "INSTITUTIONPROFILEID", 0);
        int dealId = _dataRepository.getInt("Deal", "dealId", 0);
        int copyId = _dataRepository.getInt("Deal", "copyId", 0);

        _logger.info("Got Values from new Borrower ::: [{}]", instProfId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // begin transaction
        _srk.beginTransaction();

        // Create the initial entity
        Borrower newEntity = new Borrower(_srk, CalcMonitor.getMonitor(_srk));

        // call create method of entity
        newEntity = newEntity.create(new DealPK(dealId, copyId));

        _logger.info("CREATED NEW Borrower ::: [{}]", newEntity.getBorrowerId()
                + "::" + newEntity.getDealId() + "::" + newEntity.getCopyId());

        // check create correctly by calling findByPrimaryKey
        Borrower foundEntity = new Borrower(_srk, CalcMonitor.getMonitor(_srk));
        foundEntity = foundEntity.findByPrimaryKey(new BorrowerPK(newEntity
                .getBorrowerId(), copyId));

        // verifying the created entity
        assertEquals(foundEntity.getBorrowerId(), newEntity.getBorrowerId());
        assertEquals(foundEntity.getDealId(), newEntity.getDealId());
        assertEquals(foundEntity.getCopyId(), newEntity.getCopyId());

        _logger.info(BORDER_END, "Create - Borrower");

        // roll-back transaction.
        _srk.rollbackTransaction();
    }
    
    //test for FXP26782
    @Test
    public void testExistingClient() throws Exception {
    	
        // get input data from repository
        int borrowerId = _dataRepository.getInt("Borrower", "borrowerId", 0);
        int instProfId = _dataRepository.getInt("Borrower", "INSTITUTIONPROFILEID", 0);
        int copyId = _dataRepository.getInt("Borrower", "copyId", 0);

//        _logger.insinstProfId + "::" + copyId + "::" + dealId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        Borrower _borrower = new Borrower(_srk, CalcMonitor.getMonitor(_srk));
        
        try{
            // Find by primary key
            _borrower = _borrower.findByPrimaryKey(
            		new BorrowerPK(borrowerId, copyId));
        	
            _logger.info("_borrower.getDealId() = " + _borrower.getDealId());
            _logger.info("_borrower.getBorrowerId() = " + _borrower.getBorrowerId());
            _logger.info("_borrower.getCopyId() = " + _borrower.getCopyId());
            
            _logger.info("_borrower.getExistingClient() = " + _borrower.getExistingClient());
            
            _borrower.setExistingClient("");
            _borrower.setExistingClient("Y");
            _borrower.setBorrowerFirstName("clear");
            _borrower.setBorrowerFirstName("ASA");
        	_borrower.ejbStore();
        	
        	_borrower = _borrower.findByPrimaryKey(
            		new BorrowerPK(borrowerId, copyId));
        	
        	assertEquals("Y", _borrower.getExistingClient());
        	assertEquals(true, _borrower.isExistingClient());

        	_borrower.setExistingClient("N");
        	
        	_borrower.ejbStore();
        	
        	_borrower = _borrower.findByPrimaryKey(
            		new BorrowerPK(borrowerId, copyId));
        	
        	assertEquals("N", _borrower.getExistingClient());
        	assertEquals(false, _borrower.isExistingClient());
        	
        	_borrower.setExistingClient("");
        	
        	_borrower.ejbStore();
        	
        	_borrower = _borrower.findByPrimaryKey(
            		new BorrowerPK(borrowerId, copyId));
        	String existingClient = _borrower.getExistingClient();
        	if("".equalsIgnoreCase(existingClient)){
        		existingClient = null;
        	}
        	
        	assertEquals(null, existingClient);
        	assertEquals(false, _borrower.isExistingClient());
        	
        	_borrower.setExistingClient(true);
        	
        	_borrower.ejbStore();
        	
        	_borrower = _borrower.findByPrimaryKey(
            		new BorrowerPK(borrowerId, copyId));
        	
        	assertEquals("Y", _borrower.getExistingClient());
        	assertEquals(true, _borrower.isExistingClient());
        	
        	_borrower.setExistingClient(false);
        	
        	_borrower.ejbStore();
        	
        	_borrower = _borrower.findByPrimaryKey(
            		new BorrowerPK(borrowerId, copyId));
        	
        	assertEquals("N", _borrower.getExistingClient());
        	assertEquals(false, _borrower.isExistingClient());
        	
        }finally{
        	_srk.cleanTransaction();
        	_srk.freeResources();
        }

    }

    
    @Test
    public void testSuffixId() throws Exception {

        // get input data from repository
        int borrowerId = _dataRepository.getInt("Borrower", "borrowerId", 0);
        int instProfId = _dataRepository.getInt("Borrower", "INSTITUTIONPROFILEID", 0);
        int copyId = _dataRepository.getInt("Borrower", "copyId", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        Borrower _borrower = new Borrower(_srk, CalcMonitor.getMonitor(_srk));
        
        // Find by primary key
        _borrower = _borrower.findByPrimaryKey(
                new BorrowerPK(borrowerId, copyId));
                
        int suffixId = 1;
        _borrower.setSuffixId(suffixId);
        assertEquals(suffixId, _borrower.getSuffixId());
        
    }
    
    @Test
    public void testCellPhoneNumber() throws Exception {
        // get input data from repository
        int borrowerId = _dataRepository.getInt("Borrower", "borrowerId", 0);
        int instProfId = _dataRepository.getInt("Borrower", "INSTITUTIONPROFILEID", 0);
        int copyId = _dataRepository.getInt("Borrower", "copyId", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        Borrower _borrower = new Borrower(_srk, CalcMonitor.getMonitor(_srk));
        
        // Find by primary key
        _borrower = _borrower.findByPrimaryKey(
                new BorrowerPK(borrowerId, copyId));
                
        String phoneNumber = "123456789";
        _borrower.setBorrowerCellPhoneNumber(phoneNumber);
        assertEquals(phoneNumber, _borrower.getBorrowerCellPhoneNumber());
    
    }

    @Test
    public void testCbAuthorizationDate() throws Exception {
        // get input data from repository
        int borrowerId = _dataRepository.getInt("Borrower", "borrowerId", 0);
        int instProfId = _dataRepository.getInt("Borrower", "INSTITUTIONPROFILEID", 0);
        int copyId = _dataRepository.getInt("Borrower", "copyId", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        Borrower _borrower = new Borrower(_srk, CalcMonitor.getMonitor(_srk));
        
        // Find by primary key
        _borrower = _borrower.findByPrimaryKey(
                new BorrowerPK(borrowerId, copyId));
                
        Date cbAuthorizationDate = new Date();
        _borrower.setCbAuthorizationDate(cbAuthorizationDate);
        assertEquals(cbAuthorizationDate, _borrower.getCbAuthorizationDate());
    
    }
    
    @Test
    public void testcCbAuthorizationMethod() throws Exception {
        // get input data from repository
        int borrowerId = _dataRepository.getInt("Borrower", "borrowerId", 0);
        int instProfId = _dataRepository.getInt("Borrower", "INSTITUTIONPROFILEID", 0);
        int copyId = _dataRepository.getInt("Borrower", "copyId", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        Borrower _borrower = new Borrower(_srk, CalcMonitor.getMonitor(_srk));
        
        // Find by primary key
        _borrower = _borrower.findByPrimaryKey(
                new BorrowerPK(borrowerId, copyId));
                
        String cbAuthorizationMethod = "test method";
        _borrower.setCbAuthorizationMethod(cbAuthorizationMethod);
        assertEquals(cbAuthorizationMethod, _borrower.getCbAuthorizationMethod());
    
    }
}
