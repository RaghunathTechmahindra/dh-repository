package com.basis100.deal.entity;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.SourceFirmProfilePK;
import com.basis100.workflow.notification.NotificationMonitor;
import com.basis100.workflow.notification.WFNotificationEvent;

/**
 * <p>
 * SourceFirmProfileTestDB
 * </p>
 * Express Entity class unit test: ServiceProvider
 */
public class SourceFirmProfileTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;	
	private SourceFirmProfile sourceFirmProfile;
	private SourceFirmProfilePK pk;
	


	public SourceFirmProfileTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				SourceFirmProfile.class.getSimpleName() + "DataSetTest.xml"));
	}

		
   protected DatabaseOperation getSetUpOperation() throws Exception   {    	
    	return DatabaseOperation.UPDATE;
    }
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}	

	@Test
	public void testCreateFirmUserAssoc() throws Exception {
		srk.beginTransaction();
		// get input data from xml repository
		ITable testCreateFirmUserAssoc = dataSetTest
				.getTable("testCreateFirmUserAssoc");
		int sourceFirmProfileId = Integer.parseInt((String) testCreateFirmUserAssoc
				.getValue(0, "SOURCEFIRMPROFILEID"));
		int sourceUserProfileId = Integer.parseInt((String) testCreateFirmUserAssoc
				.getValue(0, "USERPROFILEID"));		
		String effective = (String) testCreateFirmUserAssoc
				.getValue(0, "EFFECTIVEDATE");
		SimpleDateFormat formate=new SimpleDateFormat("dd-MMM-yy");
		Date effectiveDate=formate.parse(effective);		
		sourceFirmProfile = new SourceFirmProfile(srk,sourceFirmProfileId);
		sourceFirmProfile.setSourceFirmProfileId(sourceUserProfileId);
		srk.getExpressState().setDealInstitutionId(0);	
		sourceFirmProfile.createFirmUserAssoc(sourceUserProfileId,effectiveDate);
		assertEquals(sourceUserProfileId, sourceFirmProfile.getSourceFirmProfileId());
		assertNotNull(sourceFirmProfile);
		srk.rollbackTransaction();
	}

	@Test
	public void testUpdateFirmUserAssoc() throws Exception {
		srk.beginTransaction();
		// get input data from xml repository
		ITable testUpdateFirmUserAssoc = dataSetTest
				.getTable("testUpdateFirmUserAssoc");
		int sourceFirmProfileId = Integer.parseInt((String) testUpdateFirmUserAssoc
				.getValue(0, "SOURCEFIRMPROFILEID"));
		int sourceUserProfileId = Integer.parseInt((String) testUpdateFirmUserAssoc
				.getValue(0, "USERPROFILEID"));		
		String effective = (String) testUpdateFirmUserAssoc
				.getValue(0, "EFFECTIVEDATE");
		SimpleDateFormat formate=new SimpleDateFormat("dd-MMM-yy");
		Date effectiveDate=formate.parse(effective);		
		sourceFirmProfile = new SourceFirmProfile(srk,sourceFirmProfileId);
		sourceFirmProfile.setSourceFirmProfileId(sourceUserProfileId);
		srk.getExpressState().setDealInstitutionId(0);	
		sourceFirmProfile.updateFirmUserAssoc(sourceUserProfileId,effectiveDate);
		assertEquals(sourceUserProfileId, sourceFirmProfile.getSourceFirmProfileId());
		assertNotNull(sourceFirmProfile);
		srk.rollbackTransaction();
	}
	

	
	@Test
	public void testDeleteFirmUserAssoc() throws Exception {
		srk.beginTransaction();
		// get input data from xml repository
		ITable testDeleteFirmUserAssoc = dataSetTest
				.getTable("testDeleteFirmUserAssoc");
		int sourceFirmProfileId = Integer.parseInt((String) testDeleteFirmUserAssoc
				.getValue(0, "SOURCEFIRMPROFILEID"));			
		sourceFirmProfile = new SourceFirmProfile(srk,sourceFirmProfileId);	
		srk.getExpressState().setDealInstitutionId(0);	
		sourceFirmProfile.deleteFirmUserAssoc();
		assertEquals(sourceFirmProfileId, sourceFirmProfile.getSourceFirmProfileId());
		assertNotNull(sourceFirmProfile);
		srk.rollbackTransaction();
	}
		
	@Test
	public void testCreatePrimaryKey() throws Exception {
		srk.beginTransaction();
		// get input data from xml repository
		ITable testCreatePrimaryKey = dataSetTest
				.getTable("testCreatePrimaryKey");
		int sourceFirmProfileId = Integer.parseInt((String) testCreatePrimaryKey
				.getValue(0, "SOURCEFIRMPROFILEID"));			
		sourceFirmProfile = new SourceFirmProfile(srk,sourceFirmProfileId);	
		srk.getExpressState().setDealInstitutionId(0);	
		sourceFirmProfile.createPrimaryKey();
		assertEquals(sourceFirmProfileId, sourceFirmProfile.getSourceFirmProfileId());
		assertNotNull(sourceFirmProfile);
		srk.rollbackTransaction();
	}
			
          
	@Test   
	public void testCreate() throws Exception {
		srk.beginTransaction();
		// get input data from xml repository
		ITable testCreate = dataSetTest
				.getTable("testCreate");
		int sourceFirmProfileId = Integer.parseInt((String) testCreate
				.getValue(0, "SOURCEFIRMPROFILEID"));		
		sourceFirmProfile = new SourceFirmProfile(srk,sourceFirmProfileId);	
		srk.getExpressState().setDealInstitutionId(0);	
		sourceFirmProfile.create();			
		assertNotNull(sourceFirmProfile);
		srk.rollbackTransaction();
	}
	
	@Test
	public void testCreatePK() throws Exception {
		srk.beginTransaction();
		// get input data from xml repository
		ITable testCreatePK = dataSetTest
				.getTable("testCreatePK");
		int sourceFirmProfileId = Integer.parseInt((String) testCreatePK
				.getValue(0, "SOURCEFIRMPROFILEID"));		
		sourceFirmProfile = new SourceFirmProfile(srk,sourceFirmProfileId);	
		srk.getExpressState().setDealInstitutionId(1);
		pk=new SourceFirmProfilePK(sourceFirmProfileId);
		sourceFirmProfile.create(pk);	
		assertEquals(sourceFirmProfileId, pk.getId());
		assertNotNull(sourceFirmProfile);
		srk.rollbackTransaction();
	}
		
	@Test
	public void testISDupeBusId() throws Exception {
		srk.beginTransaction();
		// get input data from xml repository
		ITable testISDupeBusId = dataSetTest
				.getTable("testISDupeBusId");
		int sourceFirmProfileId = Integer.parseInt((String) testISDupeBusId
				.getValue(0, "SOURCEFIRMPROFILEID"));
		String sfbusinessid =(String) testISDupeBusId
				.getValue(0, "SFBUSINESSID");
		sourceFirmProfile = new SourceFirmProfile(srk,sourceFirmProfileId);	
		srk.getExpressState().setDealInstitutionId(0);	
		sourceFirmProfile.isDupeBusId(sfbusinessid,sourceFirmProfileId);	
		assertEquals(sourceFirmProfileId, sourceFirmProfile.getSourceFirmProfileId());
		assertNotNull(sourceFirmProfile);
		srk.rollbackTransaction();
	}
	
	@Test
	public void testUpdateFirmGroupAssoc() throws Exception {
		srk.beginTransaction();
		// get input data from xml repository
		ITable testUpdateFirmGroupAssoc = dataSetTest
				.getTable("testUpdateFirmGroupAssoc");
		int sourceFirmProfileId = Integer.parseInt((String) testUpdateFirmGroupAssoc
				.getValue(0, "SOURCEFIRMPROFILEID"));
		int groupProfileId = Integer.parseInt((String) testUpdateFirmGroupAssoc
				.getValue(0, "GROUPPROFILEID"));	
		String effective = (String) testUpdateFirmGroupAssoc
				.getValue(0, "EFFECTIVEDATE");
		SimpleDateFormat formate=new SimpleDateFormat("dd-MMM-yy");
		Date effectiveDate=formate.parse(effective);	
		sourceFirmProfile = new SourceFirmProfile(srk,sourceFirmProfileId);	
		srk.getExpressState().setDealInstitutionId(0);	
		sourceFirmProfile.updateFirmGroupAssoc(groupProfileId,effectiveDate);	
		assertEquals(sourceFirmProfileId, sourceFirmProfile.getSourceFirmProfileId());
		assertNotNull(sourceFirmProfile);
		srk.rollbackTransaction();
	}
	
	
	
	public void testDeleteFirmGroupAssoc() throws Exception {
		srk.beginTransaction();
		// get input data from xml repository
		ITable testDeleteFirmGroupAssoc = dataSetTest
				.getTable("testDeleteFirmGroupAssoc");
		int sourceFirmProfileId = Integer.parseInt((String) testDeleteFirmGroupAssoc
				.getValue(0, "SOURCEFIRMPROFILEID"));				
		sourceFirmProfile = new SourceFirmProfile(srk,sourceFirmProfileId);	
		srk.getExpressState().setDealInstitutionId(0);	
		sourceFirmProfile.deleteFirmGroupAssoc();	
		assertEquals(sourceFirmProfileId, sourceFirmProfile.getSourceFirmProfileId());
		assertNotNull(sourceFirmProfile);
		srk.rollbackTransaction();
	}
	
	public void testISDupeSystemCode() throws Exception {
		srk.beginTransaction();
		// get input data from xml repository
		ITable testISDupeSystemCode = dataSetTest
				.getTable("testISDupeSystemCode");
		int sourceFirmProfileId = Integer.parseInt((String) testISDupeSystemCode
				.getValue(0, "SOURCEFIRMPROFILEID"));	
		
		String sourceFirmCode=(String) testISDupeSystemCode
				.getValue(0, "SOURCEFIRMCODE");
		int SystemTypeId = Integer.parseInt((String) testISDupeSystemCode
				.getValue(0, "SYSTEMTYPEID"));
		sourceFirmProfile = new SourceFirmProfile(srk,sourceFirmProfileId);	
		srk.getExpressState().setDealInstitutionId(0);	
		sourceFirmProfile.isDupeSystemCode(sourceFirmCode,SystemTypeId);	
		assertEquals(sourceFirmCode, sourceFirmProfile.getSourceFirmCode());
		assertNotNull(sourceFirmProfile);
		srk.rollbackTransaction();
	}
	
	public void testISDupeSystemCodeThreeArg() throws Exception {
		srk.beginTransaction();
		// get input data from xml repository
		ITable testISDupeSystemCode = dataSetTest
				.getTable("testISDupeSystemCodeThreeArg");
		int sourceFirmProfileId = Integer.parseInt((String) testISDupeSystemCode
				.getValue(0, "SOURCEFIRMPROFILEID"));			
		String sourceFirmCode=(String) testISDupeSystemCode
				.getValue(0, "SOURCEFIRMCODE");
		int SystemTypeId = Integer.parseInt((String) testISDupeSystemCode
				.getValue(0, "SYSTEMTYPEID"));
		int mySFId = Integer.parseInt((String) testISDupeSystemCode
				.getValue(0, "MYSFID"));
		sourceFirmProfile = new SourceFirmProfile(srk,sourceFirmProfileId);	
		srk.getExpressState().setDealInstitutionId(0);	
		sourceFirmProfile.isDupeSystemCode(sourceFirmCode,SystemTypeId,mySFId);	
		assertEquals(sourceFirmCode, sourceFirmProfile.getSourceFirmCode());
		assertNotNull(sourceFirmProfile);
		srk.rollbackTransaction();
	}
	
	
	public void testCreateFirmGroupAssoc() throws Exception {
		srk.beginTransaction();
		// get input data from xml repository
		ITable testCreateFirmGroupAssoc = dataSetTest
				.getTable("testCreateFirmGroupAssoc");
		int sourceFirmProfileId = Integer.parseInt((String) testCreateFirmGroupAssoc
				.getValue(0, "SOURCEFIRMPROFILEID"));
		int groupProfileId = Integer.parseInt((String) testCreateFirmGroupAssoc
				.getValue(0, "GROUPPROFILEID"));	
		String effective = (String) testCreateFirmGroupAssoc
				.getValue(0, "EFFECTIVEDATE");
		SimpleDateFormat formate=new SimpleDateFormat("dd-MMM-yy");
		Date effectiveDate=formate.parse(effective);	
		sourceFirmProfile = new SourceFirmProfile(srk,sourceFirmProfileId);	
		srk.getExpressState().setDealInstitutionId(0);	
		sourceFirmProfile.createFirmGroupAssoc(groupProfileId, effectiveDate);
		assertNotNull(sourceFirmProfile);
		srk.rollbackTransaction();
	}
}