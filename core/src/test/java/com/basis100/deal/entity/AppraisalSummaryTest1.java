package com.basis100.deal.entity;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.AppraisalOrderPK;
import com.basis100.deal.pk.AppraisalSummaryPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.util.EntityTestUtil;

public class AppraisalSummaryTest1 extends FXDBTestCase{

	private IDataSet dataSetTest;
	private AppraisalSummary appraisalSummary;	
	private DealEntity entity;
	
	
	
	
	public AppraisalSummaryTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(AppraisalSummary.class.getSimpleName() + "DataSetTest.xml"));
	}


	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		appraisalSummary = new AppraisalSummary(srk);
		
	}
	
	public void testCreate() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, SQLException, CloneNotSupportedException, CreateException{
		try {
			srk.beginTransaction();
			ITable testCreate = dataSetTest.getTable("testCreate");		
			String id=(String)testCreate.getValue(0,"RESPONSEID");
		    int respondId=Integer.parseInt(id);		
			srk.getExpressState().setDealInstitutionId(1);
		    appraisalSummary=appraisalSummary.create(respondId);
		    assertEquals(respondId,appraisalSummary.getResponseId());
		    srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
} 
	public void testFindByResponseSuccess() throws Exception{   
	    Collection collection=null;
		ITable testFindByResponseSuccess = dataSetTest.getTable("testFindByResponseSuccess");		
		String id=(String)testFindByResponseSuccess.getValue(0,"RESPONSEID");
	    int respondId=Integer.parseInt(id);		
		srk.getExpressState().setDealInstitutionId(1);		 
		ResponsePK responsePK=new ResponsePK(respondId); 
		collection=appraisalSummary.findByResponse(responsePK);
	    assertEquals(respondId,responsePK.getId());
}
	
	public void testFindByResponseFailure() throws Exception{   
	    Collection collection=null;
		ITable testFindByResponseFailure = dataSetTest.getTable("testFindByResponseFailure");		
		String id=(String)testFindByResponseFailure.getValue(0,"RESPONSEID");
	    int respondId=Integer.parseInt(id);		
		srk.getExpressState().setDealInstitutionId(1);		 
		ResponsePK responsePK=new ResponsePK(respondId); 
		collection=appraisalSummary.findByResponse(responsePK);
	    assertEquals(respondId,responsePK.getId());
}	
	public void testFindByPrimaryKey() throws Exception{		
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByPrimaryKey");
		int responseId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"responseId"));	
		srk.getExpressState().setDealInstitutionId(1);		
		AppraisalSummaryPK  pk = new AppraisalSummaryPK(responseId);
		appraisalSummary=appraisalSummary.findByPrimaryKey(pk);		
		assertEquals(responseId, pk.getId());		
	} 
}
