package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.InvestorProfilePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * 
 * InvestorProfileTest.java
 * 
 * @version 1.0 Sep 5, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class InvestorProfileTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(InvestorProfileTest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test the find by primary key
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {
        //get input data from repository
        int investorProfileID = _dataRepository.getInt("InvestorProfile",
                "investorProfileID", 0);
        int dealInstitutionId = _dataRepository.getInt("InvestorProfile",
                "InstitutionProfileID", 0);
        _logger.info(BORDER_START, "findByPrimaryKey");
        
        //Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        
        //Create the initial entity
        InvestorProfile _investorProfile = new InvestorProfile(_srk);

        //Find by primary key
        _investorProfile = _investorProfile.findByPrimaryKey(new InvestorProfilePK(investorProfileID));
        
        //Check if the data retrieved matches the DB
        assertTrue(_investorProfile.getInvestorName().equals(
                _dataRepository.getString("InvestorProfile",
                        "investorProfileName", 0)));
        _logger.info(BORDER_END, "findByPrimaryKey");

    }

    /**
     * test create
     */
    @Test
    public void testCreate() throws Exception {
        _logger.info(BORDER_START, "Create");
        //get input data from repository
        int dealInstitutionId = _dataRepository.getInt("InvestorProfile",
                "INSTITUTIONPROFILEID", 0);
        
        //Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
       
        //Create the initial entity
        InvestorProfile _investorProfile = new InvestorProfile(_srk);
        
        //Start new transaction
        _srk.beginTransaction();
        
        InvestorProfilePK invProfilePK = _investorProfile.createPrimaryKey();
        
        //Create new Investor Profile
        _investorProfile.create(invProfilePK);
        
        //Store it in the DB
        _investorProfile.ejbStore();
        
        //Confirm that the profile in created correctly
        assertTrue(_investorProfile.getInvestorProfileId() == invProfilePK
                .getId());
        
        //Rollback transaction
        _srk.rollbackTransaction();

        //Free the resources
        _srk.freeResources();
        _logger.info(BORDER_END, "Create");

    }
}
