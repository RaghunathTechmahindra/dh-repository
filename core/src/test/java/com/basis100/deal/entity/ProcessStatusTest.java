/*
 * @(#) ProcessStatusTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.ProcessStatusPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * @author amalhi
 *
 * JUnit test for entity ProcessStatus
 *
 */
public class ProcessStatusTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(ProcessStatusTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public ProcessStatusTest() {

    }

    /**
     * To be executed before test
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env and free resources
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int processStatusId = _dataRepository.getInt("ProcessStatus",
                "processStatusId", 0);
        String processStatusDesc = _dataRepository.getString("ProcessStatus",
                "processStatusDesc", 0);

        _logger.info(BORDER_START, "findByPrimaryKey");
        _logger.info("Got Values from repository for ProcessStatus ::: [{}]",
                processStatusId + "::" + processStatusDesc);

        // Set VPD context
        _srk.getExpressState();

        // Create the initial entity
        ProcessStatus _processStatus = new ProcessStatus(_srk);

        // Find by primary key
        _processStatus = _processStatus.findByPrimaryKey(new ProcessStatusPK(
                processStatusId));

        _logger.info("Got Values from new found ProcessStatus ::: [{}]",
                _processStatus.getProcessStatusId() + "::"
                        + _processStatus.getProcessStatusDesc());

        // Check if the data retrieved matches the DB
        assertTrue(_processStatus.getProcessStatusId() == processStatusId);
        assertTrue(_processStatus.getProcessStatusDesc().equals(
                processStatusDesc));

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * test create method of entity
     */
    @Ignore /*it won't be used*/
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create");

        // get input data from repository
        String processStatusDesc = _dataRepository.getString("ProcessStatus",
                "processStatusDesc", 0);

        // Set VPD context
        _srk.getExpressState();

		//begin transaction
        _srk.beginTransaction();

		// Create the initial entity
        ProcessStatus newEntity = new ProcessStatus(_srk);
		
		//call create method of entity
        newEntity = newEntity.create(processStatusDesc);

        _logger.info("CREATED ProcessStatus ::: [{}]", newEntity
                .getProcessStatusId()
                + "::" + newEntity.getProcessStatusDesc());

        // check create correctly by calling findByPrimaryKey
        ProcessStatus foundEntity = new ProcessStatus(_srk);

        // Find by primary key
        foundEntity = foundEntity.findByPrimaryKey(new ProcessStatusPK(
                newEntity.getProcessStatusId()));

		// verifying the created entity
		assertEquals(foundEntity.getProcessStatusId(), newEntity
                .getProcessStatusId());
        assertEquals(foundEntity.getProcessStatusDesc(), newEntity
                .getProcessStatusDesc());

        _logger.info(BORDER_END, "Create");

        // roll-back transaction.
        _srk.rollbackTransaction();

    }
}
