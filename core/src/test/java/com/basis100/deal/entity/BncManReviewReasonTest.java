/*
 * @(#)BncManReviewReasonTest.java Sep 20, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.BncManReviewReasonPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * @author amalhi
 * 
 */
public class BncManReviewReasonTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(BncManReviewReasonTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public BncManReviewReasonTest() {

    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.BncManReviewReason#findByPrimaryKey(com.basis100.deal.pk.BncManReviewReasonPK)}.
     */
    @Ignore("no data for associated table")
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int responseId = _dataRepository.getInt("BncManReviewReason",
                "responseId", 0);
        int instProfId = _dataRepository.getInt("BncManReviewReason",
                "INSTITUTIONPROFILEID", 0);
        int manReviewReasonId = _dataRepository.getInt("BncManReviewReason",
                "manReviewReasonId", 0);
        String manReviewReasonDesc = _dataRepository.getString(
                "BncManReviewReason", "manReviewReasonDesc", 0);

        _logger.info(BORDER_START, "BncManReviewReason - testFindByPrimaryKey");
        _logger.info("Got Values from new BncManReviewReason ::: [{}]",
                responseId + "::" + responseId + "::" + manReviewReasonDesc);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        BncManReviewReason _bncManReviewReason = new BncManReviewReason(_srk);

        // Find by primary key
        _bncManReviewReason = _bncManReviewReason
                .findByPrimaryKey(new BncManReviewReasonPK(responseId,
                        manReviewReasonId));

        _logger.info("Got Values from BncManReviewReason found::: [{}]",
                _bncManReviewReason.getResponseId() + "::"
                        + _bncManReviewReason.getManReviewReasonId() + "::"
                        + _bncManReviewReason.getManReviewReasonDesc());

        // Check if the data retrieved matches the DB
        assertEquals(_bncManReviewReason.getResponseId(), responseId);
        assertEquals(_bncManReviewReason.getManReviewReasonId(),
                manReviewReasonId);
        assertEquals(_bncManReviewReason.getManReviewReasonDesc(),
                manReviewReasonDesc);

        _logger.info(BORDER_END, "BncManReviewReason - testFindByPrimaryKey");
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.BncManReviewReason#create(int, java.lang.String, java.lang.String)}.
     */
    @Ignore("no data for associated table")
    @Test
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create - BncManReviewReason");

        // get input data from repository
        int instProfId = _dataRepository.getInt("BncManReviewReason",
                "INSTITUTIONPROFILEID", 0);
        int responseId = _dataRepository.getInt("BNCADJUDICATIONRESPONSE",
                "responseId", 0);
        int code = _dataRepository.getInt("BncManReviewReason", "copyId", 0);

        _logger.info("Got Values from new BncManReviewReason ::: [{}]",
                instProfId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // begin transaction
        _srk.beginTransaction();

        // Create the initial entity
        BncManReviewReason newEntity = new BncManReviewReason(_srk);

        // call create method of entity
        newEntity = newEntity.create(responseId, "aa", "desc");

        _logger.info("CREATED BncManReviewReason ::: [{}]", newEntity
                .getResponseId()
                + "::"
                + newEntity.getManReviewReasonId()
                + "::"
                + newEntity.getManReviewReasonDesc());

        // check create correctly by calling findByPrimaryKey
        BncManReviewReason foundEntity = new BncManReviewReason(_srk);
        foundEntity = foundEntity.findByPrimaryKey(new BncManReviewReasonPK(
                newEntity.getResponseId(), newEntity.getManReviewReasonId()));

        // verifying the created entity
        assertEquals(foundEntity.getResponseId(), newEntity.getResponseId());
        assertEquals(foundEntity.getManReviewReasonId(), newEntity
                .getManReviewReasonId());
        assertEquals(foundEntity.getManReviewReasonDesc(), newEntity
                .getManReviewReasonDesc());

        _logger.info(BORDER_END, "Create - BncManReviewReason");

        // roll-back transaction.
        _srk.rollbackTransaction();
    }

}
