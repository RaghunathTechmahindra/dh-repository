package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.docprep.extract.doctag.CompoundingFrequency;
import com.basis100.resources.SessionResourceKit;

public class CompoundingFrequencyTest extends FXDBTestCase {
	private IDataSet dataSetTest;
    private CompoundingFrequency compoundingFrequency = null;
    private CalcMonitor dcm = null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public CompoundingFrequencyTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(CompoundingFrequency.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.compoundingFrequency = new CompoundingFrequency();
	}
	
	public void testExtract() throws Exception
	{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testExtract");
		int institutionProfileId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));
		int dealid = Integer.parseInt((String)testCreate.getValue(0, "DEALID"));
		int cid = Integer.parseInt((String)testCreate.getValue(0, "COPYID"));
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		
		Deal d= new Deal(srk,null);
		DealEntity de = (DealEntity)(d);
		
		
		FMLQueryResult fmlQueryResult = compoundingFrequency.extract(de, 0, null, srk);
		
		assertNotNull(fmlQueryResult);
		srk.rollbackTransaction();
	}
	
	
}
