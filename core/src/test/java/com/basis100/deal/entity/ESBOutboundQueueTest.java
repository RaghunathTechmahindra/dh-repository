package com.basis100.deal.entity;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.ESBOutboundQueuePK;
import com.basis100.entity.RemoteException;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.core.ExpressCoreTestContext;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;
import com.filogix.externallinks.framework.ServiceConst;

public class ESBOutboundQueueTest extends ExpressEntityTestCase implements
        UnitTestLogging 
{
    // The logger
    private final static Logger _logger = LoggerFactory.getLogger(ESBOutboundQueue.class);

    // session resource kit
    private SessionResourceKit  _srk;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }
    
    /**
     * Test method for {@link com.basis100.deal.entity.ESBOutboundQueue#create(int, int, int, int, int, int)}.
     */
    @Test
    public void testCreate() throws Exception {
        _logger.info(BORDER_START, "Create - ESBOutboundQueue");
        
        int dealId = _dataRepository.getInt("Deal", "dealId", 0);
        int institutionProfileId = _dataRepository.getInt("Deal", "institutionProfileId", 0);
        int serviceTypeId = ServiceConst.SERVICE_TYPE_CONDITION_STATUS_UPDATE;        
        int systemTypeId = _dataRepository.getInt("Deal", "systemTypeId", 0);
        int userProfileId = _dataRepository.getInt("Deal", "administratorId", 0);
        
        _logger.info("Got values for new ESBOutboundQueue ::: [{}]", institutionProfileId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);

        // begin transaction
        _srk.beginTransaction();
        
        //create entity
        ESBOutboundQueue newEntity = new ESBOutboundQueue(_srk);
        newEntity = newEntity.create(newEntity.createPrimaryKey(), serviceTypeId, dealId, systemTypeId, userProfileId );
        
        _logger.info("Created a new ESBOutboundQueue ::" + newEntity.getESBOutboundQueueId());
        
        //create the holder entity
        ESBOutboundQueue foundEntity = new ESBOutboundQueue(_srk);
        
        //Find by primary Key
        foundEntity = foundEntity.findByPrimaryKey(new ESBOutboundQueuePK(newEntity.getESBOutboundQueueId()));
        
        // verifying the created entity
        assertEquals(newEntity.getESBOutboundQueueId(), foundEntity.getESBOutboundQueueId());
        assertEquals(newEntity.getInstitutionProfileId(), foundEntity.getInstitutionProfileId());
        assertEquals(newEntity.getServiceTypeId(), foundEntity.getServiceTypeId());
        assertEquals(newEntity.getDealId(), foundEntity.getDealId());
        assertEquals(newEntity.getSystemTypeId(), foundEntity.getSystemTypeId());
        assertEquals(newEntity.getUserProfileId(), foundEntity.getUserProfileId());
        assertEquals(newEntity.getAttemptCounter(), foundEntity.getAttemptCounter());
        assertEquals(newEntity.getRetryTimeStamp(), foundEntity.getRetryTimeStamp());
        assertEquals(newEntity.getTransactionStatusId(), foundEntity.getTransactionStatusId());
        assertEquals(newEntity.getTransactionStatusDescription(), foundEntity.getTransactionStatusDescription());
        assertEquals(newEntity.getTimeStamp(), foundEntity.getTimeStamp());
        
        _logger.info(BORDER_END, "Create - ESBOutboundQueue");

        // roll-back transaction.
        _srk.rollbackTransaction();
    }
    
    
    /**
     * Test method for {@link com.basis100.deal.entity.ESBOutboundQueue#findByPrimaryKey(com.basis100.deal.pk.ESBOutboundQueuePK)}.
     */
   /* @Test
    public void testFindByPrimaryKey() throws Exception{
        _logger.info(BORDER_START, "FindByPrimaryKey - ESBOutboundQueue");
        
        int dealId = _dataRepository.getInt("Deal", "dealId", 0);
        int institutionProfileId = _dataRepository.getInt("Deal", "institutionProfileId", 0);
        int serviceTypeId = ServiceConst.SERVICE_TYPE_CONDITION_STATUS_UPDATE;        
        int systemTypeId = _dataRepository.getInt("Deal", "systemTypeId", 0);
        int userProfileId = _dataRepository.getInt("Deal", "administratorId", 0);
        
//        insertData(dealId, institutionProfileId, systemTypeId, serviceTypeId, 5, userProfileId);
        
        int esbOutboundQueueId = _dataRepository.getInt("ESBOutboundQueue", "esbOutboundQueueId", 0);
        int institutionProfileIdQ = _dataRepository.getInt("ESBOutboundQueue", "institutionProfileId", 0);
        int serviceTypeIdQ = _dataRepository.getInt("ESBOutboundQueue", "serviceTypeId", 0);
        int dealIdQ = _dataRepository.getInt("ESBOutboundQueue", "dealId", 0);
        int systemTypeIdQ = _dataRepository.getInt("ESBOutboundQueue", "systemTypeId", 0);
        int userProfileIdQ = _dataRepository.getInt("ESBOutboundQueue", "userProfileId", 0);
        int attemptCounter = _dataRepository.getInt("ESBOutboundQueue", "attemptCounter", 0);
        Date retryTimeStamp = _dataRepository.getDate("ESBOutboundQueue", "retryTimeStamp", 0);
        int transactionStatusId = _dataRepository.getInt("ESBOutboundQueue", "transactionStatusId", 0);
        String transactionStatusDescription = _dataRepository.getString("ESBOutboundQueue", "transactionStatusDescription", 0);
        Date timeStamp =_dataRepository.getDate("ESBOutboundQueue", "timeStamp", 0);
        
        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);
        
        //create the entity
        ESBOutboundQueue _esbOutboundQueue = new ESBOutboundQueue(_srk);
        
        //Find by primary Key
        _esbOutboundQueue = _esbOutboundQueue.findByPrimaryKey(new ESBOutboundQueuePK(esbOutboundQueueId));
        
        
        _logger.info("Got Values from ESBOutboundQueue found::: [{}]", _esbOutboundQueue.getESBOutboundQueueId());

        // Check if the data retrieved matches the DB
        assertEquals(_esbOutboundQueue.getESBOutboundQueueId(), esbOutboundQueueId);
        assertEquals(_esbOutboundQueue.getInstitutionProfileId(), institutionProfileIdQ);
        assertEquals(_esbOutboundQueue.getServiceTypeId(), serviceTypeIdQ);
        assertEquals(_esbOutboundQueue.getDealId(), dealIdQ);
        assertEquals(_esbOutboundQueue.getSystemTypeId(), systemTypeIdQ);
        assertEquals(_esbOutboundQueue.getUserProfileId(), userProfileIdQ);
        assertEquals(_esbOutboundQueue.getAttemptCounter(), attemptCounter);
        assertEquals(_esbOutboundQueue.getRetryTimeStamp(), retryTimeStamp);
        assertEquals(_esbOutboundQueue.getTransactionStatusId(), transactionStatusId);
        assertEquals(_esbOutboundQueue.getTransactionStatusDescription(), transactionStatusDescription);
        assertEquals(_esbOutboundQueue.getTimeStamp(), timeStamp);
        
//        deleteData(esbOutboundQueueId);

        _logger.info(BORDER_END, "findByPrimaryKey");

    }

    
    *//**
     * Test method for {@link com.basis100.deal.entity.ESBOutboundQueue#performUpdate(com.basis100.deal.pk.ESBOutboundQueuePK)}.
     *//*
    @Test
    public void testPerformUpdate() throws Exception{
        _logger.info(BORDER_START, "PerformUpdate - esbOutboundQueue");
        
        int esbOutboundQueueId = _dataRepository.getInt("ESBOutboundQueue", "esbOutboundQueueId", 0);
        int institutionProfileId = _dataRepository.getInt("ESBOutboundQueue", "institutionProfileId", 0);
        int serviceTypeId = _dataRepository.getInt("ESBOutboundQueue", "serviceTypeId", 0);
        int dealId = _dataRepository.getInt("ESBOutboundQueue", "dealId", 0);
        int systemTypeId = _dataRepository.getInt("ESBOutboundQueue", "systemTypeId", 0);
        int userProfileId = _dataRepository.getInt("ESBOutboundQueue", "userProfileId", 0);
        int attemptCounter = _dataRepository.getInt("ESBOutboundQueue", "attemptCounter", 0);        
        Date timeStamp =_dataRepository.getDate("ESBOutboundQueue", "timeStamp", 0);
        
        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(institutionProfileId);
        
        //create the entity
        ESBOutboundQueue _esbOutboundQueue = new ESBOutboundQueue(_srk);
        
        _logger.info("Find entry from ESBOutboundQueue by primary key::: [{}]", _esbOutboundQueue.getESBOutboundQueueId());
        
        //Find by primary Key
        _esbOutboundQueue = _esbOutboundQueue.findByPrimaryKey(new ESBOutboundQueuePK(esbOutboundQueueId));
        
        _logger.info("Update RetryTimeStamp, TransactionStatusDescription and TransactionStatusId field of the entry");
        
        Date updateRetryTimeStamp = new Date();
        String updateTransactionStatusDescription = "Hello World";
        int updateTransactionStatusId = 5;
        _esbOutboundQueue.setRetryTimeStamp(updateRetryTimeStamp);
        _esbOutboundQueue.setTransactionStatusDescription(updateTransactionStatusDescription);
        _esbOutboundQueue.setTransactionStatusId(updateTransactionStatusId);    
        
        _logger.info("Perform Update");
        _esbOutboundQueue.performUpdate();
        
        
        _esbOutboundQueue = _esbOutboundQueue.findByPrimaryKey(new ESBOutboundQueuePK(esbOutboundQueueId));
        _logger.info("Got Values from ESBOutboundQueue found::: [{}]", _esbOutboundQueue.getESBOutboundQueueId());

        // Check if the data retrieved matches the DB
        assertEquals(_esbOutboundQueue.getESBOutboundQueueId(), esbOutboundQueueId);
        assertEquals(_esbOutboundQueue.getInstitutionProfileId(), institutionProfileId);
        assertEquals(_esbOutboundQueue.getServiceTypeId(), serviceTypeId);
        assertEquals(_esbOutboundQueue.getDealId(), dealId);
        assertEquals(_esbOutboundQueue.getSystemTypeId(), systemTypeId);
        assertEquals(_esbOutboundQueue.getUserProfileId(), userProfileId);
        assertEquals(_esbOutboundQueue.getAttemptCounter(), attemptCounter);
        assertEquals(_esbOutboundQueue.getRetryTimeStamp().toString(), updateRetryTimeStamp.toString());
        assertEquals(_esbOutboundQueue.getTransactionStatusId(), updateTransactionStatusId);
        assertEquals(_esbOutboundQueue.getTransactionStatusDescription(), updateTransactionStatusDescription);
        assertEquals(_esbOutboundQueue.getTimeStamp(), timeStamp);
        

        _logger.info(BORDER_END, "performUpdate");

    }
    
    *//**
     * test findNextUnprocessedEvent
     * 
     * @throws Exception
     *//*
    @Test
    public void findNextUnprocessedEvent() throws Exception {

        ESBOutboundQueue _esbOutboundQueue = new ESBOutboundQueue(_srk);
        int esbOutboundQueueId = _dataRepository.getInt("ESBOutboundQueue", "esbOutboundQueueId", 0);
        _esbOutboundQueue.findNextUnprocessedEvent(5, 600);
        int institutionProfileId =_esbOutboundQueue.getInstitutionProfileId();

        _srk.getExpressState().setDealInstitutionId(institutionProfileId);

        assertEquals(_esbOutboundQueue.getTransactionStatusId(), 
                ServiceConst.ESBOUT_TRANSACTIONSTATUS_AVALIABLE);
    }
    
    private void insertData(int dealId, int institutionProfileId, int systemTypeId, 
            int dealEventStatusTypeId, int statusNameReplacementId, int userProfileId) 
        throws Exception {
       
        String sql = "INSERT INTO ESBOUTBOUNDQUEUE  values (" +
            "ESBOUTBOUNDQUEUESEQ.nextval, " + institutionProfileId + ", " 
            + ServiceConst.SERVICE_TYPE_DEALEVENT_STATUS_UPDATE+", " 
            + dealId + ", " + systemTypeId + ", " + userProfileId + ", 0, null, " 
            + " null, null, sysdate)";
        
        _dataRepository.executeSQL(sql);
        
    }*/
    
    private void deleteData(int qId) 
        throws Exception {
        
        String sql = "delete from ESBOUTBOUNDQUEUE  where ESBOUTBOUNDQUEUEid = " + qId;
         
        _dataRepository.executeSQL(sql);

        
    }


}
