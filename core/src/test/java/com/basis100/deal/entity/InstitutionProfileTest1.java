package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.docprep.extract.doctag.PrivacyClause;
import com.basis100.deal.pk.AddrPK;
import com.basis100.deal.pk.ConditionPK;
import com.basis100.deal.pk.ContactPK;
import com.basis100.deal.pk.EmploymentHistoryPK;
import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.resources.SessionResourceKit;
import com.sun.org.apache.bcel.internal.generic.INSTANCEOF;

public class InstitutionProfileTest1 extends FXDBTestCase {
	private IDataSet dataSetTest;
    private InstitutionProfile institutionProfile = null;
  
    SessionResourceKit srk= new SessionResourceKit();
	
	public InstitutionProfileTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(InstitutionProfile.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(InstitutionProfile.class.getSimpleName()+"DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.institutionProfile = new InstitutionProfile(srk);
	}
	
	/*public void testCreate() throws Exception
	{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int institutionProfileId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));          
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		institutionProfilePK = new InstitutionProfilePK(institutionProfileId);
	
		InstitutionProfile ainstitutionProfile = institutionProfile.create(institutionProfilePK);
		
		InstitutionProfile new_institutionProfile = new InstitutionProfile(srk, institutionProfileId);
		
		assertEquals(new_institutionProfile, ainstitutionProfile);
		
		srk.rollbackTransaction();
	}*/
	/*
	 * Failure case not applicable as the create method returns an entity and sending unqualified data will result
	 * in improper end of test class and thus it is avoid.
	 * Seems that InstitutionProfile table is associated with VPD so a VPD Error is thrown while inserting a row into this table.
	 * So this testcase is commented out
	 */
	
	public void testCreatePrimaryKey() throws Exception
	{
		srk.beginTransaction();
		ITable testCreatePrimaryKey = dataSetTest.getTable("testCreatePrimaryKey");
		int institutionProfileId = Integer.parseInt((String)testCreatePrimaryKey.getValue(0, "INSTITUTIONPROFILEID"));          
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		InstitutionProfilePK institutionProfilePK = new InstitutionProfilePK(institutionProfileId);
	
		InstitutionProfilePK ainstitutionProfilePK = institutionProfile.createPrimaryKey();
		
		//InstitutionProfile new_institutionProfile = new InstitutionProfile(srk, institutionProfileId);
		
		assertNotNull(ainstitutionProfilePK);
		
		srk.rollbackTransaction();
	}
	/*
	 * Failure case not applicable as the create method returns an entity and sending unqualified data will result
	 * in improper end of test class and thus it is avoid.
	 * Seems that InstitutionProfile table is associated with VPD so a VPD Error is thrown while inserting a row into this table.
	 * So this testcase is commented out
	 */
	public void testfindLowestId() throws Exception
	{
		srk.beginTransaction();
		ITable testfindLowestId = dataSetTest.getTable("testfindLowestId");
		int institutionProfileId = Integer.parseInt((String)testfindLowestId.getValue(0, "INSTITUTIONPROFILEID"));  
		//int lowestId = Integer.parseInt((String)testfindLowestId.getValue(0, "LOWESTID"));
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
			
		int lowestId = institutionProfile.findLowestId();
		
		//InstitutionProfile new_institutionProfile = new InstitutionProfile(srk, institutionProfileId);
		
		assertEquals(institutionProfileId, lowestId);
		
		srk.rollbackTransaction();
	}
	public void testfindByFirst() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByFirst = dataSetTest.getTable("testfindByFirst");
		//int institutionProfileId = Integer.parseInt((String)testfindLowestId.getValue(0, "INSTITUTIONPROFILEID"));  
		//int lowestId = Integer.parseInt((String)testfindLowestId.getValue(0, "LOWESTID"));
				
		institutionProfile = institutionProfile.findByFirst();
		
		//InstitutionProfile new_institutionProfile = new InstitutionProfile(srk, institutionProfileId);
		
		assertNotNull(institutionProfile);
		
		srk.rollbackTransaction();
	}
}
