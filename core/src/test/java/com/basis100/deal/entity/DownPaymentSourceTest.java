/*
 * @(#) DownPaymentSourceTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */


package com.basis100.deal.entity;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import com.basis100.deal.calc.CalcMonitor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.DownPaymentSourcePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>DownPaymentSourceTest</p>
 * Express Entity class unit test: DownPaymentSource
 */
public class DownPaymentSourceTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(DownPaymentSourceTest.class);

    /**
     * Constructor function
     */
    public DownPaymentSourceTest() {
    }

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int downpaymentSourceId = _dataRepository.getInt("DOWNPAYMENTSOURCE",
                "DOWNPAYMENTSOURCEID", 0);
        int dealId = _dataRepository.getInt("DOWNPAYMENTSOURCE", "DEALID", 0);
        int copyId = _dataRepository.getInt("DOWNPAYMENTSOURCE", "COPYID", 0);
        int instProfId = _dataRepository.getInt("DOWNPAYMENTSOURCE",
                "INSTITUTIONPROFILEID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");
        CalcMonitor cal = CalcMonitor.getMonitor(srk);

        // feed the input data and run the program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        DownPaymentSource entity = new DownPaymentSource(srk, cal);
        entity = entity.findByPrimaryKey(new DownPaymentSourcePK(
                downpaymentSourceId, copyId));

        // verify the running result.
        assertEquals(downpaymentSourceId, entity.getDownPaymentSourceId());
        assertEquals(dealId, entity.getDealId());

        // free resources
        srk.freeResources();
    }

    /**
     * test create.
     */
    @Test
    public void testCreate() throws Exception {

        // get input data from repository
        int instProfId = _dataRepository.getInt("DOWNPAYMENTSOURCE",
                "INSTITUTIONPROFILEID", 0);
        int dealId = _dataRepository.getInt("DOWNPAYMENTSOURCE", "DEALID", 0);
        int copyId = _dataRepository.getInt("DOWNPAYMENTSOURCE", "COPYID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");
        CalcMonitor cal = CalcMonitor.getMonitor(srk);

        // feed input data and run program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        srk.beginTransaction();
        DownPaymentSource newEntity = new DownPaymentSource(srk, cal);
        newEntity = newEntity.create(new DealPK(dealId, copyId));
        newEntity.ejbStore();

        // check create correctlly.
        DownPaymentSource foundEntity = new DownPaymentSource(srk, cal);
        foundEntity = foundEntity.findByPrimaryKey(new DownPaymentSourcePK(
                newEntity.getDownPaymentSourceId(), newEntity.getCopyId()));

        // verifying
        assertEquals(foundEntity.getDownPaymentSourceId(), newEntity
                .getDownPaymentSourceId());
        assertEquals(foundEntity.getDealId(), newEntity.getDealId());
        // rollback transaction.
        srk.rollbackTransaction();
        // free resources.
        srk.freeResources();
    }

}
