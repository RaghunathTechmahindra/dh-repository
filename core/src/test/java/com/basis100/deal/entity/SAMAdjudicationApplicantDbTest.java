package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.AdjudicationApplicantPK;
import com.basis100.deal.pk.RequestPK;

/**
 * <p>SAMAdjudicationApplicantDbTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class SAMAdjudicationApplicantDbTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private SAMAdjudicationApplicant sAMAdjudicationApplicant;
	
	public SAMAdjudicationApplicantDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(SAMAdjudicationApplicant.class.getSimpleName() + "DataSetTest.xml"));
	}


	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.sAMAdjudicationApplicant = new SAMAdjudicationApplicant(srk);
		
	}
	
	/*public void testCreate() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreate");
    	int requestId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "REQUESTID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	int appnum = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "APPLICANTNUMBER"));
    	srk.beginTransaction();
    	sAMAdjudicationApplicant.create(new RequestPK(requestId,copyId), appnum);
    	srk.rollbackTransaction();
	    assertNotNull(sAMAdjudicationApplicant);
    }
	
	public void testCreate1() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreate1");
    	int requestId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "REQUESTID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	int appnum = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "APPLICANTNUMBER"));
    	srk.beginTransaction();
    	sAMAdjudicationApplicant.create(new RequestPK(requestId,copyId), appnum);
    	srk.rollbackTransaction();
	    assertNotNull(sAMAdjudicationApplicant);
    }
	
	public void testCreate2() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreate2");
    	int requestId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "REQUESTID"));
    	int appnum = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "APPLICANTNUMBER"));
    	srk.beginTransaction();
    	sAMAdjudicationApplicant.create(new AdjudicationApplicantPK(requestId, appnum));
    	srk.rollbackTransaction();
	    assertNotNull(sAMAdjudicationApplicant);
    }
	*/
	public void testFindByAdjudicationApplicant() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByAdjudicationApplicant");
    	int requestId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "REQUESTID"));
    	int appnum = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "APPLICANTNUMBER"));
    	
    	sAMAdjudicationApplicant.findByAdjudicationApplicant(new AdjudicationApplicantPK(requestId, appnum));
	    assertNotNull(sAMAdjudicationApplicant);
    }
	
	/*public void testClone() throws Exception {
    	ITable expected = dataSetTest.getTable("testClone");
    	
    	sAMAdjudicationApplicant.clone();
	    assertNotNull(sAMAdjudicationApplicant);
    }*/
	
	public void testClone1() throws Exception {
    	ITable expected = dataSetTest.getTable("testClone1");
    	int requestId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "REQUESTID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	
    	sAMAdjudicationApplicant.clone(new RequestPK(requestId,copyId));
	    assertNotNull(sAMAdjudicationApplicant);
    }
}