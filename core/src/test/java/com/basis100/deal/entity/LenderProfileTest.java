/*
 * @(#)LenderProfileTest.java   Sep 6, 2007
 *
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.entity;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.LenderProfilePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * LenderProfileTest
 * 
 * @version 1.0 Sep 6, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class LenderProfileTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(LenderProfileTest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test the find by primary key
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {
        // get input data from repository
        int lenderProfileID = _dataRepository.getInt("LenderProfile",
                "lenderProfileID", 0);
        String lenderName = _dataRepository.getString("LenderProfile",
                "lenderName", 0);
        int dealInstitutionId = _dataRepository.getInt("LenderProfile",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "findByPrimaryKey");

        // Create the initial entity
        LenderProfile _lenderProfile = new LenderProfile(_srk);

        // find by primary key
        _lenderProfile = _lenderProfile.findByPrimaryKey(new LenderProfilePK(
                lenderProfileID));

        // Check that the data retrieved matches the DB
        assertTrue(_lenderProfile.getLenderName().equals(lenderName));

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * test the find by Default Lender
     */
    @Test
    public void testFindByDefaultLender() throws Exception {
        // get input data from repository
        String lenderName = _dataRepository.getString("LenderProfile",
                "lenderName", 0);
        int dealInstitutionId = _dataRepository.getInt("LenderProfile",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "findByDefaultLender");

        // Create the initial entity
        LenderProfile _lenderProfile = new LenderProfile(_srk);

        // find by Default Lender
        _lenderProfile = _lenderProfile.findByDefaultLender();

        // Check that the data retrieved matches the DB
        assertTrue(_lenderProfile.getLenderName().equals(lenderName));

        _logger.info(BORDER_END, "findByDefaultLender");
    }

    /**
     * test the find by Short Name
     */
    @Test
    public void testFindByShortName() throws Exception {
        // get input data from repository
        String shortName = _dataRepository.getString("LenderProfile",
                "LPSHORTNAME", 0);
        String lenderName = _dataRepository.getString("LenderProfile",
                "lenderName", 0);
        int dealInstitutionId = _dataRepository.getInt("LenderProfile",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "findByShortName");

        // Create the initial entity
        LenderProfile _lenderProfile = new LenderProfile(_srk);

        // find by Short Name
        List lenderProfiles = (List) _lenderProfile.findByShortName(shortName);

        // Check that the data retrieved matches the DB
        assertTrue(((LenderProfile) lenderProfiles.get(0)).getLenderName()
                .equals(lenderName));

        _logger.info(BORDER_END, "findByShortName");
    }

    /**
     * test the Create
     */
    @Test
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create");

        // get input data from repository
        int dealInstitutionId = _dataRepository.getInt("InvestorProfile",
                "INSTITUTIONPROFILEID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // Create initial entity
        LenderProfile _lenderProfile = new LenderProfile(_srk);

        // Start new transaction
        _srk.beginTransaction();

        LenderProfilePK lenderProfilePK = _lenderProfile.createPrimaryKey();

        // create the lender profile
        _lenderProfile = _lenderProfile.create(lenderProfilePK);
        
        //save the record
        _lenderProfile.ejbStore();
        
        // Confirm that the profile in created correctly
        assertTrue(_lenderProfile.getLenderProfileId() == lenderProfilePK
                .getId());

        // Rollback transaction
        _srk.rollbackTransaction();

        // Free the resources
        _srk.freeResources();
        
        _logger.info(BORDER_END, "Create");
    }

}
