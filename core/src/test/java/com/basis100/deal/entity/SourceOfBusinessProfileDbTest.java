/**
 * <p>@(#)SourceOfBusinessProfileTest.java Sep 10, 2007</p>
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */
package com.basis100.deal.entity;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.SourceFirmProfilePK;
import com.basis100.deal.pk.SourceOfBusinessProfilePK;
import com.basis100.resources.SessionResourceKit;


/**
 * <p>SourceOfBusinessProfileTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class SourceOfBusinessProfileDbTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private SourceOfBusinessProfile sourceOfBusinessProfile;
	// session resource kit
    private SessionResourceKit srk;
    private SourceOfBusinessProfilePK pk;
    private SourceFirmProfilePK sourceFirmProfilePK;
	public SourceOfBusinessProfileDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(SourceOfBusinessProfile.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		srk = new SessionResourceKit();
		
		this.sourceOfBusinessProfile = new SourceOfBusinessProfile(srk);
	}
	public void testfindByAssociatedSourceFirm() throws Exception {
    	srk.beginTransaction();
    	ITable testfindByAssociatedSourceFirm = dataSetTest.getTable("testfindByAssociatedSourceFirm");
    	int id = Integer.parseInt((String) testfindByAssociatedSourceFirm.getValue(0, "SOURCEFIRMPROFILEID"));
    	
    	sourceFirmProfilePK = new SourceFirmProfilePK(id);
    	Collection c  = sourceOfBusinessProfile.findByAssociatedSourceFirm(sourceFirmProfilePK);
		
	    assertTrue(c.size()>0);
		srk.rollbackTransaction();
    }
	
	public void testfindByMailBox() throws Exception {
    	srk.beginTransaction();
    	ITable testfindByMailBox = dataSetTest.getTable("testfindByMailBox");
    	String mailbox = (String) testfindByMailBox.getValue(0, "SOURCEOFBUSINESSCODE");
    	sourceOfBusinessProfile = sourceOfBusinessProfile.findByMailBox(mailbox);
		
	    assertEquals(sourceOfBusinessProfile.getSourceOfBusinessCode(),mailbox);
		srk.rollbackTransaction();
    }
    
    
	
	public void testfindByName() throws Exception {
    	srk.beginTransaction();
    	ITable testfindByName = dataSetTest.getTable("testfindByName");
    	String sobpShortName = (String) testfindByName.getValue(0, "SOBPSHORTNAME");
    	sourceOfBusinessProfile = sourceOfBusinessProfile.findByName(sobpShortName);
		assertEquals(sourceOfBusinessProfile.getSOBPShortName(),sobpShortName);
		srk.rollbackTransaction();
    }
 
	
	 public void testfindByPrimaryKey() throws Exception {
	    	srk.beginTransaction();
	    	ITable testfindByPrimaryKey = dataSetTest.getTable("testfindByPrimaryKey");
	    	int SourceOfBusinessProfileId = (Integer) DataType.INTEGER.typeCast(testfindByPrimaryKey.getValue(0, "SOURCEOFBUSINESSPROFILEID"));
			pk = new SourceOfBusinessProfilePK(SourceOfBusinessProfileId);
			sourceOfBusinessProfile = sourceOfBusinessProfile.findByPrimaryKey(pk);
			
		    assertEquals(sourceOfBusinessProfile.getSourceOfBusinessProfileId(),SourceOfBusinessProfileId);
			srk.rollbackTransaction();
	    }
	    
	   
	 
	 public void testfindBySystemTypeSFPAndSOBCode() throws Exception {
	    	srk.beginTransaction();
	    	ITable testfindBySystemTypeSFPAndSOBCode = dataSetTest.getTable("testfindBySystemTypeSFPAndSOBCode");
	    	int sourceFirmProfileId = (Integer) DataType.INTEGER.typeCast(testfindBySystemTypeSFPAndSOBCode.getValue(0, "SOURCEFIRMPROFILEID"));
	    	int systemType = (Integer) DataType.INTEGER.typeCast(testfindBySystemTypeSFPAndSOBCode.getValue(0, "SYSTEMTYPEID"));
	    	String sourceOfBusinessCode = (String) testfindBySystemTypeSFPAndSOBCode.getValue(0, "SOURCEOFBUSINESSCODE");
	    	
			sourceOfBusinessProfile = sourceOfBusinessProfile.findBySystemTypeSFPAndSOBCode(systemType,sourceFirmProfileId,sourceOfBusinessCode);
			
		    assertEquals(sourceOfBusinessProfile.getSourceOfBusinessCode(),sourceOfBusinessCode);
		    assertEquals(sourceOfBusinessProfile.getSystemTypeId(),systemType);
		    assertEquals(sourceOfBusinessProfile.getSourceFirmProfileId(),sourceFirmProfileId);
		    
			srk.rollbackTransaction();
	    }
	    
	
    public void testIsDupeSystemCode() throws Exception {
    	srk.beginTransaction();
    	ITable expectedsourceOfBusinessProfile = dataSetTest.getTable("testIsDupeSystemCode");
		String systemCode = (String)(expectedsourceOfBusinessProfile.getValue(0, "SOURCEOFBUSINESSCODE"));
		int SystemTypeId = (Integer) DataType.INTEGER.typeCast(expectedsourceOfBusinessProfile.getValue(0, "SYSTEMTYPEID"));
		
	    assertTrue(sourceOfBusinessProfile.isDupeSystemCode(systemCode, SystemTypeId, 0));
		srk.rollbackTransaction();
    }
    
    
   
    public void testIsDupeBusId() throws Exception {
    	srk.beginTransaction();
    	ITable expectedsourceOfBusinessProfile = dataSetTest.getTable("testIsDupeBusId");
		String SOBPBusinessId = (String)(expectedsourceOfBusinessProfile.getValue(0, "SOBPBUSINESSID"));
		int SourceOfBusinessProfileId = (Integer) DataType.INTEGER.typeCast(expectedsourceOfBusinessProfile.getValue(0, "SOURCEOFBUSINESSPROFILEID"));
	 
	    assertTrue(sourceOfBusinessProfile.isDupeBusId(SOBPBusinessId, SourceOfBusinessProfileId));
	    srk.rollbackTransaction();
    }
     
  
    public void testCreateSourceGroupAssoc() throws Exception {
    	srk.beginTransaction();
    	ITable testCreateSourceGroupAssoc = dataSetTest.getTable("testCreateSourceGroupAssoc");
		int groupProfileId = (Integer) DataType.INTEGER.typeCast(testCreateSourceGroupAssoc.getValue(0, "GROUPPROFILEID"));
		Date effectiveDate = (Date)DataType.DATE.typeCast (testCreateSourceGroupAssoc.getValue(0, "EFFECTIVEDATE"));
		srk.getExpressState().setDealInstitutionId(0);
		SourceOfBusinessProfile sourceOfBusinessProfile= new SourceOfBusinessProfile(srk, groupProfileId);
		sourceOfBusinessProfile.createSourceGroupAssoc(groupProfileId, effectiveDate);
	    //assertEquals(groupProfileId, sourceOfBusinessProfile.getSourceFirmProfileId());
	    assertNotNull(sourceOfBusinessProfile.getSourceFirmProfileId());
	    srk.rollbackTransaction();
    }
    
   
    public void testUpdateSourceGroupAssoc() throws Exception {
    	srk.beginTransaction();
    	ITable expectedsourceOfBusinessProfile = dataSetTest.getTable("SOURCETOGROUPASSOC");
    	int groupProfileId = (Integer) DataType.INTEGER.typeCast(expectedsourceOfBusinessProfile.getValue(0, "GROUPPROFILEID"));
		Date effectiveDate = (Date)DataType.DATE.typeCast (expectedsourceOfBusinessProfile.getValue(0, "EFFECTIVEDATE"));
		
    	//sourceOfBusinessProfile.updateSourceGroupAssoc(groupProfileId, effectiveDate);
    	assertNotNull(sourceOfBusinessProfile.getSourceFirmProfileId());
    	//assertEquals(groupProfileId, sourceOfBusinessProfile.getSourceFirmProfileId());
    	srk.rollbackTransaction();
    }
    
   
    public void testDeleteSourceGroupAssoc() throws Exception {
    	srk.beginTransaction();
    	ITable expectedsourceOfBusinessProfile = dataSetTest.getTable("SOURCETOGROUPASSOC");
    	int groupProfileId = (Integer) DataType.INTEGER.typeCast(expectedsourceOfBusinessProfile.getValue(0, "GROUPPROFILEID"));
		
		//.deleteSourceGroupAssoc();
		assertNotNull(sourceOfBusinessProfile.getSourceFirmProfileId());
		//assertEquals(groupProfileId, sourceOfBusinessProfile.getSourceFirmProfileId());
		srk.rollbackTransaction();
    }
 	}