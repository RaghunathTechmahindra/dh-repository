package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Properties;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;

public class SysPropertyDbTest extends FXDBTestCase {
	
		private IDataSet dataSetTest;
		private SysProperty sysProperty;
		// session resource kit
	    private SessionResourceKit srk;
	    
	    private int  _sysPropertyTypeId = -1;
	
		public SysPropertyDbTest(String name) throws IOException, DataSetException {
			super(name);
			dataSetTest = new FlatXmlDataSet(this.getClass().getResource(SysProperty.class.getSimpleName() + "DataSetTest.xml"));
		}
	
		@Override
		protected DatabaseOperation getTearDownOperation() throws Exception {
			return DatabaseOperation.NONE;
		}
	
		@Override
		protected void setUp() throws Exception {
			srk = new SessionResourceKit();
			this.sysProperty = new SysProperty(srk, _sysPropertyTypeId);
		}
	    
	    public void testFindByName() throws Exception {
	    	srk.beginTransaction();
	    	ITable expectedSysProperty = dataSetTest.getTable("testFindByName");
			
			int sysPropertyId = (Integer) DataType.INTEGER.typeCast(expectedSysProperty.getValue(0, "SYSPROPERTYTYPEID"));
			String name = (String)(expectedSysProperty.getValue(0, "NAME"));
			
	       sysProperty = sysProperty.findByName(name);
	       
		   assertEquals(name, sysProperty.getName());
		   srk.rollbackTransaction();
	    }
	    
	    @Test
	    public void testGetProperty() throws Exception {
	    	srk.beginTransaction();
	    	String propVal =null;
	    	ITable testGetProperty = dataSetTest.getTable("testGetProperty");
	    	String propertyName = (String)(testGetProperty.getValue(0, "NAME"));
	    	int sysPropertyId = Integer.parseInt((String)(testGetProperty.getValue(0, "SYSPROPERTYTYPEID")));

	    	// create the initial entity
	        SysProperty sysProperty = new SysProperty(srk, sysPropertyId);

	        // get The value for the property
	        propVal = sysProperty.getProperty(propertyName, "");
	        assertNotNull(propVal);
	        srk.rollbackTransaction();
	    }
	    
	    @Test
	    public void testGetProperties() throws Exception {
	    	srk.beginTransaction();
	    	ITable testGetProperties = dataSetTest.getTable("testGetProperties");
	    	int sysPropertyId = Integer.parseInt((String)(testGetProperties.getValue(0, "SYSPROPERTYTYPEID")));
	    	int insititutionId = Integer.parseInt((String)(testGetProperties.getValue(0, "INSTITUTIONPROFILEID")));
	    	
	    	// create the initial entity
	        SysProperty sysProperty = new SysProperty(srk, sysPropertyId);

	        // get The value for the property
	        Properties prop = sysProperty.getProperties(insititutionId);
	        assertNotNull(prop);
	        srk.rollbackTransaction();
	    }

}

