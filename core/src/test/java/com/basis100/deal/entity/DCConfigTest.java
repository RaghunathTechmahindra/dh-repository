/*
 * @(#)DCConfigTest.java    2005-3-16
 *
 */

package com.basis100.deal.entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.deal.pk.DCConfigPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;

/**
 * DCConfigTest -
 * 
 * @version 1.0 2005-3-16
 * @author <A HREF="mailto:chyxiang@yahoo.com">Chen Xiang (Sean)</A>
 */
public class DCConfigTest extends ExpressEntityTestCase {
    // The logger
    private static Log _log = LogFactory.getLog(DCConfigTest.class);

    // The Session Resource Kit
    private SessionResourceKit _srk;

    // The DCConfig entity
    private DCConfig _dcConfigDAO;

    // institutionId for deal
    private int dealInstitutionId;

    /*
     * setup
     */
    @Before
    public void setUp() throws Exception {
        _log.debug("Preparing the test cases ...");

        // init resources
        ResourceManager.init();
        _srk = new SessionResourceKit("Testing");
        _dcConfigDAO = new DCConfig(_srk);

        // get institutionid from database
        dealInstitutionId = _dataRepository.getInt("Contact",
                "InstitutionProfileId", 0);

    }

    /**
     * test get by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {
        // set institutionid
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        
        // get value from database
        int configId = _dataRepository.getInt("DCConfig", "DCConfigId", 0);
        String requestParty = _dataRepository.getString("DCConfig",
                "DCRequestingParty", 0);
        
        // get dcconfig find by primary key
        DCConfig aconfig = _dcConfigDAO.findByPrimaryKey(new DCConfigPK(
                configId));
        
        // verify result
        assertEquals(requestParty, aconfig.getDcRequestingParty());
        _log.info("Found DC Config: ");
        _log.info("   DC Request Party: " + aconfig.getDcRequestingParty());
        _log.info("   DC Folder Config: " + aconfig.getDcFolderConfigId());
        _log.info("      DC Account Id: " + aconfig.getDcAccountIdentifier());
        _log.info("  Dc Account Passwd: " + aconfig.getDcAccountPassword());
        _log.info("====================================");
    }

    /*
     * (non-Javadoc)
     * 
     * @see junit.framework.TestCase#tearDown()
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

}
