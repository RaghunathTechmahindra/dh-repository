package com.basis100.deal.entity;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Collection;
import java.util.List;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.docprep.extract.doctag.PrivacyClause;
import com.basis100.deal.pk.AddrPK;
import com.basis100.deal.pk.ConditionPK;
import com.basis100.deal.pk.ContactPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.EmploymentHistoryPK;
import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.deal.pk.InsureOnlyApplicantPK;
import com.basis100.deal.pk.InvestorProfilePK;
import com.basis100.deal.pk.LDInsuranceRatesPK;
import com.basis100.resources.SessionResourceKit;
import com.sun.org.apache.bcel.internal.generic.INSTANCEOF;

public class LDInsuranceRatesTest1 extends FXDBTestCase {
	private IDataSet dataSetTest;
    private LDInsuranceRates ldInsuranceRates = null;
    private LDInsuranceRatesPK pk=null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public LDInsuranceRatesTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(LDInsuranceRates.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(LDInsuranceRates.class.getSimpleName()+"DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.ldInsuranceRates = new LDInsuranceRates(srk);
		
		
	}
	
	/*public void testCreate() throws Exception
	{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int institutionProfileId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String)testCreate.getValue(0, "LDINSURANCERATEID"));
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		LDInsuranceRatesPK pk = new LDInsuranceRatesPK(id);
		
		LDInsuranceRates aLDInsuranceRates = ldInsuranceRates.create(pk);
		
		LDInsuranceRates new_LDInsuranceRates = new LDInsuranceRates(srk, id);
		
		assertEquals(new_LDInsuranceRates, aLDInsuranceRates);
		
		srk.rollbackTransaction();
	}*/
	
	/*
	 *Issue in DB Query - Insert into LDInsuranceRates(LDInsuranceRateId ) Values ( 50) inserts only one column where as 
	 *it is supposed to insert 2 other not nullable columns of LDINSURANCETYPEID,INSTITUTIONPROFILEID also.
	 */
	
	/*public void testCreatePrimaryKey() throws Exception
	{
		srk.beginTransaction();
		ITable testCreatePrimaryKey = dataSetTest.getTable("testCreatePrimaryKey");
		int institutionProfileId = Integer.parseInt((String)testCreatePrimaryKey.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String)testCreatePrimaryKey.getValue(0, "LDINSURANCERATEID"));
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		//LDInsuranceRatesPK pk = new LDInsuranceRatesPK(id);
		
		pk = ldInsuranceRates.createPrimaryKey();
		
		LDInsuranceRates new_LDInsuranceRates = new LDInsuranceRates(srk, id);
		
		assertSame(new_LDInsuranceRates, pk);
		
		srk.rollbackTransaction();
	} // sequence doesn't exist so commenting this method
*/
	/*
	 * Failure case not applicable as the create method returns an entity and sending unqualified data will result
	 * in improper end of test class and thus it is avoid.
	 */
	
	
	public void testfindByRateCode() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByRateCode = dataSetTest.getTable("testfindByRateCode");
		String rateCode = (String)testfindByRateCode.getValue(0, "LDRATECODE");
		
		List l = ldInsuranceRates.findByRateCode(rateCode);
		
		assert l.size() > 0;
		srk.rollbackTransaction();
	}
	
	public void testfindByRateCodeFailure() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByRateCodeFailure = dataSetTest.getTable("testfindByRateCodeFailure");
		String rateCode = (String)testfindByRateCodeFailure.getValue(0, "LDRATECODE");
		
		List l = ldInsuranceRates.findByRateCode(rateCode);
		
		assert l.size() == 0;
		srk.rollbackTransaction();
	}
	
	public void findByRateCodeDescription() throws Exception
	{
		srk.beginTransaction();
		ITable findByRateCodeDescription = dataSetTest.getTable("findByRateCodeDescription");
		String rateCodeDesc = (String)findByRateCodeDescription.getValue(0, "LDRATEDESC");
		
		List l = ldInsuranceRates.findByRateCode(rateCodeDesc);
		
		assert l.size() > 0;
		srk.rollbackTransaction();
	}
	
	public void findByRateCodeDescriptionFailure() throws Exception
	{
		srk.beginTransaction();
		ITable findByRateCodeDescriptionFailure = dataSetTest.getTable("findByRateCodeDescriptionFailure");
		String rateCodeDesc = (String)findByRateCodeDescriptionFailure.getValue(0, "LDRATEDESC");
		
		List l = ldInsuranceRates.findByRateCode(rateCodeDesc);
		
		assert l.size() == 0;
		srk.rollbackTransaction();
	}
}
