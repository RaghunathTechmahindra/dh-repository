package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.ProcessStatusPK;

public class ProcessStatusTest1 extends  FXDBTestCase {
	
	private IDataSet dataSetTest;
	private ProcessStatus processStatus=null,processStatus1=null;
	ProcessStatusPK processStatusPK=null;
	public ProcessStatusTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("ProcessStatusDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource("ProcessStatusDataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		processStatus=new ProcessStatus(srk);
	}
   //findByprocessStatusId(int)

	public void testFindByProcessStatusId() throws Exception{
		
		ITable testFindByRequest = dataSetTest.getTable("testFindByProcessStatusId");
		int  processstatusid = Integer.parseInt(testFindByRequest.getValue(0,"processStatusId").toString());
		int size=processStatus.findByprocessStatusId(processstatusid).size();
		assertNotSame(0, size);
		
	}
    public void testFindByProcessStatusIdFailure() throws Exception{
		
		ITable testFindByRequest = dataSetTest.getTable("testFindByProcessStatusIdFailure");
		int  processstatusid = Integer.parseInt(testFindByRequest.getValue(0,"processStatusId").toString());
		int size=processStatus.findByprocessStatusId(processstatusid).size();
		assertSame(0, size);
		
	}
    //createPrimaryKey()
      public void testCreatePrimaryKey() throws Exception{
    	  ProcessStatusPK processStatusPK=processStatus.createPrimaryKey();
          assertNotNull(processStatusPK);
	}
      //create(String)
      public void testCreate() throws Exception{
    	  
    	  ITable testCreate = dataSetTest.getTable("testCreate");
    	  String   processDesc = testCreate.getValue(0,"processDesc").toString();
    	  ProcessStatus processStatus1=processStatus.create(processDesc);
          assertNotNull(processStatus1);
	}
 
      //ProcessStatusPK
          public void testCreateWithProcessStatusPK() throws Exception{
          ProcessStatusPK processStatusPK=processStatus.createPrimaryKey();
          ITable testCreateWithProcessStatusPK = dataSetTest.getTable("testCreateWithProcessStatusPK");
    	  String   processDesc = testCreateWithProcessStatusPK.getValue(0,"processDesc").toString();
    	  ProcessStatus processStatus1=processStatus.create(processStatusPK,processDesc);
          assertNotNull(processStatus1);
	      }
          
          public void testCreateWithProcessStatusPKFailure() throws Exception{
        	  boolean status=false;
        	  try{
        	  ITable testCreateWithProcessStatusPKFailure = dataSetTest.getTable("testCreateWithProcessStatusPKFailure");
        	  String   processDesc = testCreateWithProcessStatusPKFailure.getValue(0,"processDesc").toString();
        	  int   id = Integer.parseInt(testCreateWithProcessStatusPKFailure.getValue(0,"id").toString());
        	  ProcessStatusPK processStatusPK=new ProcessStatusPK(id);
        	  ProcessStatus processStatus1=processStatus.create(processStatusPK,processDesc);
        	  status=true;
        	  }catch (Exception e) {
				// TODO: handle exception
        		  status=false;
			}
              assertSame(false,status);
    	  }
}
