package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;

public class ChangeMapValueTest1 extends  FXDBTestCase {
	
	private IDataSet dataSetTest;
    private ChangeMapValue changeMapValue=null;
    
    
	public ChangeMapValueTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("ChangeMapValueDataSetTest.xml"));
	}


	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		changeMapValue=new ChangeMapValue("","");
	}

	public void testDuplicate() throws Exception {
		
		changeMapValue.duplicate();
		Boolean result=changeMapValue.equals(changeMapValue);
		assertSame(true,result);
		
	}

public void testDuplicateFailure() throws Exception {
		
		changeMapValue.duplicate();
		Boolean result=changeMapValue.equals(null);
		assertSame(false,result);
		
	}

}
