package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;

public class DBIncomeTest  extends FXDBTestCase{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(DBIncomeTest.class);
    
    //Initial data to be loaded before each test case.
	private final static String INIT_XML_DATA = "DBIncomeDataSet.xml";
	private IDataSet dataSetTest;
	
	/**
     * Constructor function
     */
    public DBIncomeTest(String name)  throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource("DBIncomeDataSetTest.xml"));
    }
    
        
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
        return DatabaseOperation.INSERT;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.DELETE;
    }

    /**
     * test find by other income.
     */
    @Test
    public void testFindByOtherIncome() throws Exception {

    	_logger.info(" Start testFindByOtherIncome");
    	 
    	ITable testFindByOtherIncome = dataSetTest.getTable("testFindByOtherIncome");
    	int  instProfId = Integer.parseInt((String)testFindByOtherIncome.getValue(0,"instProfId"));
  		int  borrowerId = Integer.parseInt((String)testFindByOtherIncome.getValue(0,"borrowerId"));
  		int  copyId = Integer.parseInt((String)testFindByOtherIncome.getValue(0,"copyId"));
  		int  copyId1 = Integer.parseInt((String)testFindByOtherIncome.getValue(0,"copyId1"));
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
        
        Income income = new Income(srk, null);
        
        //success case
        BorrowerPK borrowerPK = new BorrowerPK(borrowerId, copyId);
        List<Income> incomes = (List<Income>)income.findByOtherIncome(borrowerPK);
        
        assert incomes != null;
        assert incomes.size() > 0;
        
        //failure case
        BorrowerPK borrowerPK1 = new BorrowerPK(borrowerId, copyId1);
        List<Income> incomes1 = (List<Income>)income.findByOtherIncome(borrowerPK1);
        
        assert incomes1 == null;
        
        _logger.info(" End testFindByOtherIncome");
    	
    }
    
    /**
     * test find by borrower.
     */
    @Test
    public void testFindByBorrower() throws Exception {

    	_logger.info(" Start testFindByBorrower");
    	 
    	ITable testFindByBorrower = dataSetTest.getTable("testFindByBorrower");
    	int  instProfId = Integer.parseInt((String)testFindByBorrower.getValue(0,"instProfId"));
  		int  borrowerId = Integer.parseInt((String)testFindByBorrower.getValue(0,"borrowerId"));
  		int  copyId = Integer.parseInt((String)testFindByBorrower.getValue(0,"copyId"));
  		int  copyId1 = Integer.parseInt((String)testFindByBorrower.getValue(0,"copyId1"));
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
        
        Income income = new Income(srk, null);
        
        //success case
        BorrowerPK borrowerPK = new BorrowerPK(borrowerId, copyId);
        List<Income> incomes = (List<Income>)income.findByBorrower(borrowerPK);
        
        assert incomes != null;
        assert incomes.size() > 0;
        
        //failure case
        BorrowerPK borrowerPK1 = new BorrowerPK(borrowerId, copyId1);
        List<Income> incomes1 = (List<Income>)income.findByBorrower(borrowerPK1);
        
        assert incomes1 == null;
        
        _logger.info(" End testFindByBorrower");
    	
    }
    
    /**
     * test find by deal.
     */
    @Test
    public void testFindByDeal() throws Exception {

    	_logger.info(" Start testFindByDeal");
    	 
    	ITable testFindByDeal = dataSetTest.getTable("testFindByDeal");
    	int  instProfId = Integer.parseInt((String)testFindByDeal.getValue(0,"instProfId"));
  		int  dealId = Integer.parseInt((String)testFindByDeal.getValue(0,"dealId"));
  		int  copyId = Integer.parseInt((String)testFindByDeal.getValue(0,"copyId"));
  		int  copyId1 = Integer.parseInt((String)testFindByDeal.getValue(0,"copyId1"));
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
        
        Income income = new Income(srk, null);
        
        //success case
        DealPK dealPK = new DealPK(dealId, copyId);
        List<Income> incomes = (List<Income>)income.findByDeal(dealPK);
        
        assert incomes != null;
        assert incomes.size() > 0;
        
        //failure case
        DealPK dealPK1 = new DealPK(dealId, copyId1);
        List<Income> incomes1 = (List<Income>)income.findByDeal(dealPK1);
        
        assert incomes1 == null;
        
        _logger.info(" End testFindByDeal");
    	
    }
    
    /**
     * test find by copies.
     */
    @Test
    public void testFindByMyCopies() throws Exception {

    	_logger.info(" Start testFindByMyCopies");
    	 
    	ITable testFindByMyCopies = dataSetTest.getTable("testFindByMyCopies");
    	int  instProfId = Integer.parseInt((String)testFindByMyCopies.getValue(0,"instProfId"));
  		int  incomeId = Integer.parseInt((String)testFindByMyCopies.getValue(0,"incomeId"));
  		int  incomeId1 = Integer.parseInt((String)testFindByMyCopies.getValue(0,"incomeId1"));
  		int  copyId = Integer.parseInt((String)testFindByMyCopies.getValue(0,"copyId"));
  		int  copyId1 = Integer.parseInt((String)testFindByMyCopies.getValue(0,"copyId1"));
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
        
        //success case
        Income income = new Income(srk, null, incomeId, copyId);
        Vector<Income> incomes = (Vector<Income>)income.findByMyCopies();
         
        assert incomes != null;
        assert incomes.size() > 0;
        
        //failure case
        Income income1 = new Income(srk, null, incomeId1, copyId1);
        Vector<Income> incomes1 = (Vector<Income>)income1.findByMyCopies();
        
        assert incomes1 != null;
        assert incomes1.size() == 0;
        
        _logger.info(" End testFindByMyCopies");
    	
    }
    
    /**
     * test create.
     */
    @Test
    public void testCreate() throws Exception {

    	_logger.info(" Start testCreate");
    	 
    	ITable testCreate = dataSetTest.getTable("testCreate");
    	int  instProfId = Integer.parseInt((String)testCreate.getValue(0,"instProfId"));
    	int  borrowerId = Integer.parseInt((String)testCreate.getValue(0,"borrowerId"));
  		int  copyId = Integer.parseInt((String)testCreate.getValue(0,"copyId"));
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
        
        srk.beginTransaction();
        //success case
        Income income = new Income(srk, null);
        BorrowerPK borrowerPK = new BorrowerPK(borrowerId, copyId);
        
        income = income.create(borrowerPK);
        
        assert income != null;
        assertEquals(copyId, income.getCopyId());
        assertEquals(borrowerId, income.getBorrowerId());
        
        srk.rollbackTransaction();
        
        _logger.info(" End testCreate");
    	
    }


}
