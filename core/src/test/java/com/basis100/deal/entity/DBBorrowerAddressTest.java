package com.basis100.deal.entity;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.BorrowerAddressPK;
import com.basis100.deal.pk.BorrowerPK;

public class DBBorrowerAddressTest extends FXDBTestCase{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(DBBorrowerAddressTest.class);
    
    //Initial data to be loaded before each test case.
	
	private IDataSet dataSetTest;
	
	/**
     * Constructor function
     */
    public DBBorrowerAddressTest(String name)  throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource(DBBorrowerAddressTest.class.getSimpleName() + "DataSetTest.xml"));
    }
    
 
    
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
        return DatabaseOperation.INSERT;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.DELETE;
    }
    
    /**
     * Test method for BorrowerAddress#findByPrimaryKey
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

    	 _logger.info(" Start testFindByPrimaryKey");
    	 
    	ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKey");
    	int  borrowerAddressId = Integer.parseInt((String)testFindByPrimaryKey.getValue(0,"borrowerAddressId"));
    	int  copyId = Integer.parseInt((String)testFindByPrimaryKey.getValue(0,"CopyId"));
 		int  instProfId = Integer.parseInt((String)testFindByPrimaryKey.getValue(0,"instProfId"));
 		int  borrowerId = Integer.parseInt((String)testFindByPrimaryKey.getValue(0,"borrowerId"));
 		

        // Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
        
        BorrowerAddress borrowerAddress = new BorrowerAddress(srk);

        // Find by primary key
        borrowerAddress = borrowerAddress.findByPrimaryKey(new BorrowerAddressPK(borrowerAddressId, copyId));

        _logger.info("Got Values from new BorrowerAddress ::: [{}]",
        		borrowerAddress.getBorrowerAddressId() + "::" + borrowerAddress.getCopyId());

        // Check if the data retrieved matches the DB
        assertEquals(borrowerAddress.getBorrowerAddressId(), borrowerAddressId);
        assertEquals(borrowerAddress.getCopyId(), copyId);
        assertEquals(borrowerAddress.getBorrowerId(), borrowerId);

        _logger.info(" End testFindByPrimaryKey");
    }
    
    /**
     * Test method for BorrowerAddress#findByBorrower
     */
    @Test
    public void testFindByBorrower() throws Exception {
    	
    	 _logger.info("Start testFindByBorrower");

    	ITable testFindByBorrower = dataSetTest.getTable("testFindByBorrower");
    	int  copyId = Integer.parseInt((String)testFindByBorrower.getValue(0,"CopyId"));
    	int  copyId1 = Integer.parseInt((String)testFindByBorrower.getValue(0,"CopyId1"));
 		int  instProfId = Integer.parseInt((String)testFindByBorrower.getValue(0,"instProfId"));
 		int  borrowerId = Integer.parseInt((String)testFindByBorrower.getValue(0,"borrowerId"));
    	
       
        _logger.info("Got Values from new BorrowerAddress ::: [{}]",  instProfId + "::" + copyId);

        // Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        BorrowerAddress borrowerAddress = new BorrowerAddress(srk);

        // Find by Borrower
        List<BorrowerAddress> borrowerAddresses = (List<BorrowerAddress>)borrowerAddress.findByBorrower(new BorrowerPK(borrowerId, copyId));

        assert borrowerAddresses.size() > 0;
        
        // Find by Borrower
        List<BorrowerAddress> borrowerAddresses1 = (List<BorrowerAddress>)borrowerAddress.findByBorrower(new BorrowerPK(borrowerId, copyId1));

        assert borrowerAddresses1.size() == 0;

        _logger.info("End testFindByBorrower");
    }

    
    /**
     * Test method for BorrowerAddress#isAuditable
     */
    @Test
    public void testDeepCopy() throws Exception {
    	
    	_logger.info("Start testDeepCopy");

    	ITable testDeepCopy = dataSetTest.getTable("testDeepCopy");
     	int  copyId = Integer.parseInt((String)testDeepCopy.getValue(0,"CopyId"));
  		int  instProfId = Integer.parseInt((String)testDeepCopy.getValue(0,"instProfId"));
  		int  borrowerId = Integer.parseInt((String)testDeepCopy.getValue(0,"borrowerId"));
  		int  borrowerAddressId = Integer.parseInt((String)testDeepCopy.getValue(0,"borrowerAddressId"));
    	 
        // Create the initial entity
        BorrowerAddress borrowerAddress = new BorrowerAddress(srk);
        borrowerAddress.setBorrowerAddressId(borrowerAddressId);
        borrowerAddress.setBorrowerId(borrowerId);
        borrowerAddress.setCopyId(copyId);
        borrowerAddress.setInstitutionProfileId(instProfId);

        BorrowerAddress borrowerAddrCopy = borrowerAddress.deepCopy();
        
        assertEquals(borrowerAddress.getBorrowerAddressId(), borrowerAddrCopy.getBorrowerAddressId());
        assertEquals(borrowerAddress.getCopyId(), borrowerAddrCopy.getCopyId());
        assertEquals(borrowerAddress.getBorrowerId(), borrowerAddrCopy.getBorrowerId());
        assertEquals(borrowerAddress.getBorrowerId(), borrowerAddrCopy.getBorrowerId());

        _logger.info("End testDeepCopy");
    }
    
    
    /**
     * Test method for BorrowerAddress#isAuditable
     */
    @Test
    public void testIsAuditable() throws Exception {
    	
    	 _logger.info("Start testIsAuditable");

        // Create the initial entity
        BorrowerAddress borrowerAddress = new BorrowerAddress(srk);

        assert borrowerAddress.isAuditable();

        _logger.info("End testIsAuditable");
    }
    
    /**
     * Test method for BorrowerAddress#findBySaleAddress
     */
    @Test
    public void testFindBySaleAddress() throws Exception {
    	
    	 _logger.info("Start testFindBySaleAddress");

    	ITable testFindBySaleAddress = dataSetTest.getTable("testFindBySaleAddress");
    	int  addrId = Integer.parseInt((String)testFindBySaleAddress.getValue(0,"addrId"));
    	int  copyId = Integer.parseInt((String)testFindBySaleAddress.getValue(0,"CopyId"));
 		int  instProfId = Integer.parseInt((String)testFindBySaleAddress.getValue(0,"instProfId"));
 		int  borrowerId = Integer.parseInt((String)testFindBySaleAddress.getValue(0,"borrowerId"));
    	
       
        _logger.info("Got Values from new BorrowerAddress ::: [{}]", addrId
                + "::" + instProfId + "::" + copyId);

        // Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        BorrowerAddress borrowerAddress = new BorrowerAddress(srk);

        // Find by primary key
        borrowerAddress = borrowerAddress.findBySaleAddress(borrowerId, copyId);

        _logger.info("Got Values from new BorrowerAddress ::: [{}]",
        		borrowerAddress.getBorrowerId() + "::" + borrowerAddress.getCopyId());

        // Check if the data retrieved matches the DB
       
        assertEquals(borrowerAddress.getCopyId(), copyId);
        assertEquals(borrowerAddress.getBorrowerId(), borrowerId);

        _logger.info("End testFindBySaleAddress");
    }
    
    /**
     * Test method for BorrowerAddress#create
     */
    @Test
    public void testCreate() throws Exception {

        _logger.info("Start Create - BorrowerAddress");

        
        ITable testCreate = dataSetTest.getTable("testCreate");
     	int  copyId = Integer.parseInt((String)testCreate.getValue(0,"CopyId"));
  		int  instProfId = Integer.parseInt((String)testCreate.getValue(0,"instProfId"));
  		int  borrowerId = Integer.parseInt((String)testCreate.getValue(0,"borrowerId"));

  		_logger.info("Got Values from new BorrowerAddress ::: [{}]", instProfId);

        // Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);

        // begin transaction
        srk.beginTransaction();

        // Create the initial entity
        BorrowerAddress newEntity = new BorrowerAddress(srk);

        // call create method of entity
        newEntity = newEntity.create(new BorrowerPK(borrowerId, copyId));

        _logger.info("CREATED BorrowerAddress ::: [{}]", newEntity.getBorrowerId() + "::" + newEntity.getCopyId());

        // check create correctly by calling findByPrimaryKey
        BorrowerAddress foundEntity = new BorrowerAddress(srk);
        foundEntity = foundEntity.findByPrimaryKey(new BorrowerAddressPK(
                newEntity.getBorrowerAddressId(), copyId));

        // verifying the created entity
        assertEquals(foundEntity.getBorrowerAddressId(), newEntity.getBorrowerAddressId());
        assertEquals(foundEntity.getCopyId(), newEntity.getCopyId());
        assertEquals(foundEntity.getBorrowerId(), newEntity.getBorrowerId());

        // roll-back transaction.
        srk.rollbackTransaction();
        
        _logger.info("End Create - BorrowerAddress");
    }
    
    
    /**
     * Test method for BorrowerAddress#findByBorrower
     */
    @Test
    public void testFindByMyCopies() throws Exception {
    	
    	 _logger.info("Start testFindByMyCopies");

    	ITable testFindByMyCopies = dataSetTest.getTable("testFindByMyCopies");
    	int  copyId = Integer.parseInt((String)testFindByMyCopies.getValue(0,"CopyId"));
    	int  copyId1 = Integer.parseInt((String)testFindByMyCopies.getValue(0,"CopyId1"));
 		int  instProfId = Integer.parseInt((String)testFindByMyCopies.getValue(0,"instProfId"));
 		int  borrowerAddressId = Integer.parseInt((String)testFindByMyCopies.getValue(0,"borrowerAddressId"));
 		int  borrowerAddressId1 = Integer.parseInt((String)testFindByMyCopies.getValue(0,"borrowerAddressId1"));
    	
       
        // Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        BorrowerAddress borrowerAddress = new BorrowerAddress(srk);
        borrowerAddress.borrowerAddressId = borrowerAddressId;
        borrowerAddress.copyId = copyId;

        // Find by Borrower
        Vector borrowerAddresses = (Vector)borrowerAddress.findByMyCopies();
        assert borrowerAddresses.size() > 0;
        
        
        // Create the initial entity
        BorrowerAddress borrowerAddress1 = new BorrowerAddress(srk);
        borrowerAddress1.borrowerAddressId = borrowerAddressId1;
        borrowerAddress1.copyId = copyId1;
        // Find by Borrower
        List<BorrowerAddress> borrowerAddresses1 = (List<BorrowerAddress>)borrowerAddress1.findByMyCopies();
        assert borrowerAddresses1.size() == 0;

        _logger.info("End testFindByMyCopies");
    }

}
