package com.basis100.deal.entity;

import java.io.IOException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.util.collections.BorrowerNumBorrowerComparator;
import com.basis100.deal.util.collections.CurrentBorrowerAddressComparator;
import com.basis100.deal.util.collections.CurrentEmploymentComparator;
import com.basis100.deal.util.collections.DealApplicationDateComparator;
import com.basis100.resources.SessionResourceKit;

public class DealApplicationDateComparatorTest extends FXDBTestCase {
	
	private IDataSet dataSetTest;
	private DealApplicationDateComparator dadc = null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public DealApplicationDateComparatorTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(DealApplicationDateComparator.class.getSimpleName()+"DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected DatabaseOperation getSetUpOperation() throws Exception
	    {
		dadc = new DealApplicationDateComparator();
	    	return DatabaseOperation.INSERT;
	    }
	
	public void testCompare() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCompare");
		
		int id1 = Integer.parseInt((String)testExtract.getValue(0, "dealid1"));
		int id2 = Integer.parseInt((String)testExtract.getValue(0, "dealid2"));
		int copyid1 = Integer.parseInt((String)testExtract.getValue(0, "copyid1"));
		int copyid2 = Integer.parseInt((String)testExtract.getValue(0, "copyid2"));
		
		Deal adeal = new Deal(srk, null, id1, copyid1);
		Deal bdeal = new Deal(srk, null, id2, copyid2);
		
		//com.basis100.deal.entity.DealEntity o1 = (com.basis100.deal.entity.DealEntity )e1;
		//com.basis100.deal.entity.DealEntity o2 = (com.basis100.deal.entity.DealEntity )e2;
		
		int result = dadc.compare(adeal, bdeal);
		
		assert result==0 || result==1 || result==-1;
		
		srk.rollbackTransaction();
	}
	

}
