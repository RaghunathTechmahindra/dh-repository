package com.basis100.deal.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.duplicate.DuplicateApplicationFinder;

public class PropertyTest1 extends  FXDBTestCase {
	
	private IDataSet dataSetTest;
	private Property  property=null;
	Deal d1=null,ex=null,in=null;
	

	public PropertyTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("PropertyDataSetTest.xml"));
	}


	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.property = new Property(srk);
	}

	public void testFindByDupeCheckCriteria() throws Exception {
		
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByDupeCheckCriteria");
		String  PropertyStreetNumber = findByDupeCheckCriteria.getValue(0,"PropertyStreetNumber").toString();
		String  PropertyStreetName = findByDupeCheckCriteria.getValue(0,"PropertyStreetName").toString();
		String  PropertyCity = findByDupeCheckCriteria.getValue(0,"PropertyCity").toString();
		String  StreetTypeId = findByDupeCheckCriteria.getValue(0,"StreetTypeId").toString();
		String  StreetDirectionId = findByDupeCheckCriteria.getValue(0,"StreetDirectionId").toString();
		String  ProvinceId =findByDupeCheckCriteria.getValue(0,"ProvinceId").toString();
		Collection  result=property.findByDupeCheckCriteria(PropertyStreetNumber, PropertyStreetName, Integer.parseInt(StreetTypeId), Integer.parseInt(StreetDirectionId), null, PropertyCity,
				Integer.parseInt(ProvinceId), null);
		int resultSize=result.size();
		assertNotSame(0,resultSize);
		
	}
	public void testFindByDupeCheckCriteriaFailure() throws Exception{
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByDupeCheckCriteriaFailure");
		String  PropertyStreetNumber = findByDupeCheckCriteria.getValue(0,"PropertyStreetNumber").toString();
		Collection  result=property.findByDupeCheckCriteria(PropertyStreetNumber, "", 0,0, null, null,
				0, null);
		int resultSize=result.size();
		assertSame(0,resultSize);
		
	}
	
    public void testFindByGenXDupeCheck() throws Exception {
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByGenXDupeCheck");
		String  PropertyStreetNumber = findByDupeCheckCriteria.getValue(0,"PropertyStreetNumber").toString();
		String  PropertyStreetName = findByDupeCheckCriteria.getValue(0,"PropertyStreetName").toString();
		String  PropertyCity = findByDupeCheckCriteria.getValue(0,"PropertyCity").toString();
		String  StreetTypeId = findByDupeCheckCriteria.getValue(0,"StreetTypeId").toString();
		String  StreetDirectionId = findByDupeCheckCriteria.getValue(0,"StreetDirectionId").toString();
		String  ProvinceId =findByDupeCheckCriteria.getValue(0,"ProvinceId").toString();
		Collection  result=property.findByGenXDupeCheck(PropertyStreetNumber, PropertyStreetName, Integer.parseInt(StreetTypeId), Integer.parseInt(StreetDirectionId), null, PropertyCity,
				Integer.parseInt(ProvinceId), null);
		int resultSize=result.size();
		assertNotSame(0,resultSize);
	}
    public void testFindByGenXDupeCheckFailure() throws Exception {
		
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByGenXDupeCheckFailure");
		String  PropertyStreetNumber = findByDupeCheckCriteria.getValue(0,"PropertyStreetNumber").toString();
		Collection  result=property.findByGenXDupeCheck(PropertyStreetNumber, "", 0,0, null, null,0, null);
		int resultSize=result.size();
		assertSame(0,resultSize);
		
	}
            
            
}
