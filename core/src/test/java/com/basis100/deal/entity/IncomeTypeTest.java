/*
 * @(#) IncomeTypeTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */


package com.basis100.deal.entity;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.IncomeTypePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>IncomeTypeTest</p>
 * Express Entity class unit test: IncomeType
 */
public class IncomeTypeTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(IncomeTypeTest.class);

    // the session resource kit.
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public IncomeTypeTest() {
    }

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
    }

    /**
     *  * test the find by primary key  
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {
        _logger.info(BORDER_START, "findByPrimaryKey");
        IncomeType incomeType = new IncomeType(_srk);
        _logger.info(SHORT_BREAK);

        doSearch(incomeType, _dataRepository.getInt("IncomeType",
                "IncomeTypeId", 0), _dataRepository.getInt("IncomeType",
                "Tdsinclusion", 0));

        _logger.info(BORDER_END, "findByPrimaryKey");
        _srk.freeResources();
    }

    /**
     * test create.
     */
    @Test
    public void testCreate() throws Exception {

        // get input data from repository
        int incomeTypeId = _dataRepository.getInt("IncomeType", "INCOMETYPEID", 0);
        String itdescription = _dataRepository.getString("IncomeType",
                "Itdescription", 0);
        String employmentrelated = _dataRepository.getString("IncomeType",
                "Employmentrelated", 0);
        double gdsinclusion = _dataRepository.getInt("IncomeType", "Gdsinclusion", 0);
        double tdsinclusion = _dataRepository.getInt("IncomeType", "Tdsinclusion", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();

        // feed input data and run program.
        srk.beginTransaction();
        IncomeType newEntity = new IncomeType(srk, incomeTypeId);
        newEntity = newEntity.create(43, itdescription, employmentrelated,
                gdsinclusion, tdsinclusion);
        newEntity.ejbStore();

        // check create correctlly.
        IncomeType foundEntity = new IncomeType(srk, 43);
        foundEntity = foundEntity.findByPrimaryKey(new IncomeTypePK(newEntity
                .getIncomeTypeId()));

        // verifying
        assertEquals(foundEntity.getIncomeTypeId(), newEntity.getIncomeTypeId());

        srk.rollbackTransaction();
        srk.freeResources();
    }

    /**
     *  * search all possible record institution profile table.  
     */

    protected void doSearch(IncomeType incomeType, int IncomeTypeId,
            int Tdsinclusion) throws Exception {

        _logger.info("Trying to search IncomeTypeId: [{}]", IncomeTypeId);
        _logger.info("Expecting the Tdsinclusion: [{}]", Tdsinclusion);
        IncomeTypePK ipk = new IncomeTypePK(IncomeTypeId);
        incomeType = incomeType.findByPrimaryKey(ipk);
        _logger.info("----------------------------------------------");
        _logger.info("Found Institution Profile[{}, {}]", incomeType
                .getIncomeTypeId(), incomeType.getITDescription());

        assertTrue(IncomeTypeId == incomeType.getIncomeTypeId());
        assertTrue(Tdsinclusion == incomeType.getTDSInclusion());
    }
}
