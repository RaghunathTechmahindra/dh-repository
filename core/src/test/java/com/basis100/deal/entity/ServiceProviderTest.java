/**
 * <p>@(#)ServiceProviderTest.java Sep 8, 2007</p>
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 * 
 * 
 */
package com.basis100.deal.entity;

import java.util.Date;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.basis100.deal.pk.ServiceProviderPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>ServiceProviderTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class ServiceProviderTest extends ExpressEntityTestCase 
    implements UnitTestLogging {

    private static final Logger _logger = 
        LoggerFactory.getLogger(ResponseTest.class);
    private static final String ENTITY_NAME = "SERVICEPROVIDER"; 
  
    // retreived data count that is stored in UnitTestDataRepository.xml
    private static final int DATA_COUNT = 27;

    private int testSeqForGen = 0;

    private SessionResourceKit _srk;
    
    // properties
    protected int     serviceProviderId;
    protected int     copyId;

    protected String  serviceProviderName;
    protected Date    startDate;
    protected Date    endDate;
    
    protected int     contactId;
    protected String  receivingPartyCode;
    protected int instProfId;
        
    /**
     * <p>setUp</p>
     * Setting up test case.
     */
    @Before
    public void setUp() throws Exception {
      
        _logger.info(BORDER_START, ENTITY_NAME + " TestCase setting up ...");
        
        // get random seqence number
        Double indexD = new Double(Math.random() * DATA_COUNT);
        testSeqForGen = indexD.intValue();
        
        // seq# 22 is serviceProviderId =  0 that is empty
        if (testSeqForGen == 22) testSeqForGen = 0;
      
        //      get test data from repository
        serviceProviderId = _dataRepository.getInt(ENTITY_NAME,  
                                                  "SERVICEPROVIDERID", 
                                                  testSeqForGen);
        contactId = _dataRepository.getInt(ENTITY_NAME,  
                                                  "CONTACTID", 
                                                  testSeqForGen);
        copyId = _dataRepository.getInt(ENTITY_NAME,  
                                                  "COPYID", 
                                                  testSeqForGen);
        serviceProviderName = _dataRepository.getString(ENTITY_NAME,  
                                                  "SERVICEPROVIDERNAME", 
                                                  testSeqForGen);
        startDate = _dataRepository.getDate(ENTITY_NAME,  
                                                  "STARTDATE", 
                                                  testSeqForGen);
        endDate = _dataRepository.getDate(ENTITY_NAME,  
                                                  "ENDDATE", 
                                                  testSeqForGen);
        receivingPartyCode = _dataRepository.getString(ENTITY_NAME,  
                                                  "RECEIVINGPARTYCODE", 
                                                  testSeqForGen);
        instProfId = _dataRepository.getInt(ENTITY_NAME,  
                                                  "INSTITUTIONPROFILEID", 
                                                  testSeqForGen);

        // init ResouceManager
        ResourceManager.init();
        //  init session resource kit.
        _srk = new SessionResourceKit();

        _logger.info(BORDER_END, "Setting up Done");
    }
    
    @After
    public void tearDown() {

      //      free resources
      _srk.freeResources( );

    }
  
    /**
     * test findByPrimaryKey.
     */
    @Test
    public void testFindByPrimaryKey()  throws Exception {
      
        _logger.info(BORDER_START, "findByPrimaryKey");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        ServiceProvider provider = new ServiceProvider(_srk);
        provider  = provider.findByPrimaryKey(new ServiceProviderPK(serviceProviderId));
        
        assertEqualsProperties(provider);

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * assertEqualProperties
     * 
     * check assertEquals for all properties of ServiceProvider with test Data
     * 
     * @param entity: ServiceProvider
     * @throws Exception
     */
    private void assertEqualsProperties(ServiceProvider entity) throws Exception
    {
        assertEquals(entity.getServiceProviderId(), serviceProviderId);
        assertEquals(entity.getCopyId(), copyId);
        assertEquals(entity.getServiceProviderName(), serviceProviderName);
        assertEquals(entity.getStartDate(), startDate);
        assertEquals(entity.getEndDate(), endDate);
        assertEquals(entity.getContactId(), contactId);
        assertEquals(entity.getReceivingPartyCode(), receivingPartyCode);      
    }
}