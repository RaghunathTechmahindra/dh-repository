/*
 * @(#) PricingRateInventoryTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filogix.express.test.UnitTestLogging;
import com.filogix.express.test.ExpressEntityTestCase;

import com.basis100.deal.pk.MtgProdPK;
import com.basis100.deal.pk.PricingRateInventoryPK;
import com.basis100.deal.pk.PricingProfilePK;
import com.basis100.deal.util.DBA;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

/**
 * @author amalhi
 * 
 * JUnit test for entity PricingRateInventory
 * 
 */
public class PricingRateInventoryTest extends ExpressEntityTestCase implements
UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
    .getLogger(PricingRateInventoryTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public PricingRateInventoryTest() {

    }

    /**
     * To be executed before test
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env and free resources
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int pricingRateInventoryId = _dataRepository.getInt(
            "PricingRateInventory", "pricingRateInventoryId", 0);
        int instProfId = _dataRepository.getInt("PricingRateInventory",
            "INSTITUTIONPROFILEID", 0);
        int pricingprofileId = _dataRepository.getInt("PricingRateInventory",
            "pricingprofileId", 0);

        _logger.info(BORDER_START, "findByPrimaryKey");
        _logger.info("Got Values from new PricingRateInventory ::: [{}]",
            pricingRateInventoryId + "::" + instProfId + "::"
            + pricingprofileId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        PricingRateInventory _pricingRateInventory = new PricingRateInventory(
            _srk);

        // Find by primary key
        _pricingRateInventory = _pricingRateInventory
        .findByPrimaryKey(new PricingRateInventoryPK(
            pricingRateInventoryId));

        _logger.info("Got Values from new Pricing Profile ::: [{}]",
            _pricingRateInventory.getPricingRateInventoryID() + "::"
            + _pricingRateInventory.getPricingProfileID());

        // Check if the data retrieved matches the DB
        assertTrue(_pricingRateInventory.getPricingRateInventoryID() == pricingRateInventoryId);
        assertTrue(_pricingRateInventory.getPricingProfileID() == pricingprofileId);

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * skip this test case, since create(pk) method has problem
     * test create method of entity
     */
    @Ignore
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create");

        // get input data from repository
        int instProfId = _dataRepository.getInt("PricingRateInventory",
            "INSTITUTIONPROFILEID", 0);

        _logger.info("Got Values from new PricingRateInventory ::: [{}]",
            instProfId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // begin transaction
        _srk.beginTransaction();

        // Create the initial entity
        PricingRateInventory newEntity = new PricingRateInventory(_srk);

        // call create method of entity
        newEntity = newEntity.create(newEntity.createPrimaryKey());

        _logger.info("CREATED PARTY PROFILE ::: [{}]", newEntity
            .getPricingRateInventoryID()
            + "::" + newEntity.getPricingProfileID());

        // check create correctly by calling findByPrimaryKey
        PricingRateInventory foundEntity = new PricingRateInventory(_srk);
        foundEntity = foundEntity.findByPrimaryKey(new PricingRateInventoryPK(
            newEntity.getPricingRateInventoryID()));

        // verifying the created entity
        assertEquals(foundEntity.getPricingRateInventoryID(), newEntity
            .getPricingRateInventoryID());
        assertEquals(foundEntity.getPricingProfileID(), newEntity
            .getPricingProfileID());

        _logger.info(BORDER_END, "Create");

        // roll-back transaction.
        _srk.rollbackTransaction();

    }

    /**
     * test Create using PricingProfileID
     */
    @Test
    public void testCreatePricingProfileID() throws Exception {

        _logger.info(BORDER_START, "Create");

        // get input data from repository
        int instProfId = _dataRepository.getInt("PricingRateInventory",
            "INSTITUTIONPROFILEID", 0);
        int pricingProfileId = _dataRepository.getInt("PricingProfile",
            "pricingProfileId", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // begin transaction
        _srk.beginTransaction();

        // Create the initial entity
        PricingRateInventory newEntity = new PricingRateInventory(_srk);

        // call create method of entity
        newEntity.create(newEntity.createPrimaryKey(), new PricingProfilePK(
            pricingProfileId));

        _logger.info("CREATED PARTY PROFILE using pricing profile id ::: [{}]",
            newEntity.getPricingRateInventoryID() + "::"
            + newEntity.getPricingProfileID());

        // check create correctly by calling findByPrimaryKey
        PricingRateInventory foundEntity = new PricingRateInventory(_srk);
        foundEntity = foundEntity.findByPrimaryKey(new PricingRateInventoryPK(
            newEntity.getPricingRateInventoryID()));

        _logger.info("FOUND PARTY PROFILE using pricing profile id ::: [{}]",
            foundEntity.getPricingRateInventoryID() + "::"
            + foundEntity.getPricingProfileID());

        // verifying the created entity
        assertEquals(foundEntity.getPricingRateInventoryID(), newEntity
            .getPricingRateInventoryID());
        assertEquals(foundEntity.getPricingProfileID(), newEntity
            .getPricingProfileID());

        _logger.info(BORDER_END, "Create");

        // roll-back transaction.
        _srk.rollbackTransaction();

    }

    /**
     * test Create using PricingProfileID
     */
    @Test
    public void testCreateWithArgs() throws Exception {

        _logger.info(BORDER_START, "CreateWithArgs");
        /*
         * int pricingProfileID, double discountPercentage, double
         * internalRatePercentage, String indexEffectiveDate, String
         * indexExpiryDate, double baseRatePercentage, double bestRate, double
         * maximumDiscountAllowed, int primeIndexRateInventoryId, double
         * primeBaseAdj, double teaserDiscount, int teaserTerm
         */

        // get input data from repository
        int instProfId = _dataRepository.getInt("PricingRateInventory",
            "INSTITUTIONPROFILEID", 0);
        int pricingProfileId = _dataRepository.getInt("PricingProfile",
            "pricingProfileId", 0);
        double discountPercentage = _dataRepository.getDouble(
            "PricingRateInventory", "discountPercentage", 0);
        double internalRatePercentage = _dataRepository.getDouble(
            "PricingRateInventory", "internalRatePercentage", 0);
        Date indexEffectiveDate = _dataRepository.getDate(
            "PricingRateInventory", "indexEffectiveDate", 0);
        Date indexExpiryDate = _dataRepository.getDate("PricingRateInventory",
            "indexExpiryDate", 0);
        double baseRatePercentage = _dataRepository.getDouble(
            "PricingRateInventory", "baseRatePercentage", 0);
        double bestRate = _dataRepository.getDouble("PricingRateInventory",
            "bestRate", 0);
        double maximumDiscountAllowed = _dataRepository.getDouble(
            "PricingRateInventory", "maximumDiscountAllowed", 0);
        int primeIndexRateInventoryId = _dataRepository.getInt(
            "PricingRateInventory", "primeIndexRateInventoryId", 0);
        double primeBaseAdj = _dataRepository.getDouble("PricingRateInventory",
            "primeBaseAdj", 0);
        double teaserDiscount = _dataRepository.getDouble(
            "PricingRateInventory", "teaserDiscount", 0);
        int teaserTerm = 0;

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // begin transaction
        _srk.beginTransaction();

        // Create the initial entity
        PricingRateInventory newEntity = new PricingRateInventory(_srk);
        _logger.info("CREATED PARTY PROFILE BEFOR VALES ::: [{}]",
            pricingProfileId + "::" + discountPercentage + "::"
            + internalRatePercentage + "::" + indexEffectiveDate
            + "::" + indexExpiryDate + "::" + baseRatePercentage
            + "::" + bestRate + "::" + maximumDiscountAllowed
            + "::" + primeIndexRateInventoryId + "::"
            + primeBaseAdj + "::" + teaserDiscount + "::"
            + teaserTerm);

        // call create method of entity
        newEntity.create(pricingProfileId, discountPercentage,
            internalRatePercentage, DBA.sqlStringFrom(indexEffectiveDate),
            DBA.sqlStringFrom(indexExpiryDate), baseRatePercentage,
            bestRate, maximumDiscountAllowed, primeIndexRateInventoryId,
            primeBaseAdj, teaserDiscount, teaserTerm);

        _logger.info("CREATED PARTY PROFILE using pricing profile id ::: [{}]",
            newEntity.getPricingRateInventoryID() + "::"
            + newEntity.getPricingProfileID());

        // check create correctly by calling findByPrimaryKey
        PricingRateInventory foundEntity = new PricingRateInventory(_srk);
        foundEntity = foundEntity.findByPrimaryKey(new PricingRateInventoryPK(
            newEntity.getPricingRateInventoryID()));

        _logger.info("FOUND PARTY PROFILE using pricing profile id ::: [{}]",
            foundEntity.getPricingRateInventoryID() + "::"
            + foundEntity.getPricingProfileID());

        // verifying the created entity
        assertEquals(foundEntity.getPricingRateInventoryID(), newEntity
            .getPricingRateInventoryID());
        assertEquals(foundEntity.getPricingProfileID(), newEntity
            .getPricingProfileID());

        _logger.info(BORDER_END, "CreateWithArgs");

        // roll-back transaction.
        _srk.rollbackTransaction();

    }

    @Test
    public void testFindMostRecentByRateCode() throws Exception {
        int instProfId = _dataRepository.getInt("PricingRateInventory",
            "INSTITUTIONPROFILEID", 0);
        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        String DUMMY_RATECODE = "*Foo*";

        try{
            _srk.beginTransaction();

            ///First: fix SEQUENCE problem. -- start
            // sometimes those table has bigger value than its Seuence.
            // roll foward SEQs so that createPrimaryKey() method won't generate wrong value
            Connection con = _srk.getConnection();
            Statement stat = con.createStatement();

            int maxPPID = 0;
            ResultSet rs = stat.executeQuery("select max(PRICINGPROFILEID) from PRICINGPROFILE");
            if(rs.next()){
                maxPPID = rs.getInt(1);
            }
            PricingProfile pprofileDataPrep = new PricingProfile(_srk);
            PricingProfilePK pricingProfilePK;
            do{
                pricingProfilePK = pprofileDataPrep.createPrimaryKey();
                _logger.info("PricingProfileseq :" + pricingProfilePK.getId());
            }while(pricingProfilePK.getId() <= maxPPID);


            int maxPRIID = 0;
            rs = stat.executeQuery("select max(PricingRateInventoryid) from PricingRateInventory");
            if(rs.next()){
                maxPRIID = rs.getInt(1);
            }
            PricingRateInventory priDataPrep = new PricingRateInventory(_srk);
            PricingRateInventoryPK priPK;
            do{
                priPK = priDataPrep.createPrimaryKey();
                _logger.info("PricingRateInventoryseq :" + pricingProfilePK.getId());
            }while(priPK.getId() <= maxPRIID);

            ///First: fix SEQUENCE problem. -- end
            
            //Second: prepare test data -- start

            PricingProfile pprofile1 = new PricingProfile(_srk);
            PricingProfilePK pprofilePK1 = pprofile1.createPrimaryKey();
            pprofile1.create(pprofilePK1);
            pprofile1 = pprofile1.findByPrimaryKey(pprofilePK1);
            

            PricingProfile pprofile2 = new PricingProfile(_srk);
            PricingProfilePK pprofilePK2 = pprofile2.createPrimaryKey();
            pprofile2.create(pprofilePK2);
            pprofile2 = pprofile2.findByPrimaryKey(pprofilePK2);

            PricingRateInventory pri1 = new PricingRateInventory(_srk);
            PricingRateInventoryPK priPK1 = pri1.createPrimaryKey();
            pri1.create(priPK1, pprofilePK1);
            pri1 = pri1.findByPrimaryKey(priPK1);

            PricingRateInventory pri2 = new PricingRateInventory(_srk);
            PricingRateInventoryPK priPK2 = pri2.createPrimaryKey();
            pri2.create(priPK2, pprofilePK1);
            pri2 = pri2.findByPrimaryKey(priPK2);

            PricingRateInventory pri3 = new PricingRateInventory(_srk);
            PricingRateInventoryPK priPK3 = pri3.createPrimaryKey();
            pri3.create(priPK3, pprofilePK2);
            pri3 = pri3.findByPrimaryKey(priPK3);
            
            PricingRateInventory pri4 = new PricingRateInventory(_srk);
            PricingRateInventoryPK priPK4 = pri4.createPrimaryKey();
            pri4.create(priPK4, pprofilePK2);
            pri4 = pri4.findByPrimaryKey(priPK4);

            pprofile1.setRateCode(DUMMY_RATECODE);
            pprofile2.setRateCode(DUMMY_RATECODE);

            Date today = new Date();
            Date tomorrow = DateUtils.addDays(today, 1);
            Date yesterday = DateUtils.addDays(today, -1);
//            Date twoDaysAgo = DateUtils.addMinutes(today, 1);


            pri1.setIndexEffectiveDate(tomorrow);
            pri2.setIndexEffectiveDate(yesterday);
            pri3.setIndexEffectiveDate(today);//this is it!
            pri4.setIndexEffectiveDate(DateUtils.addMinutes(today, 1));//1 min later

            pprofile1.ejbStore();
            pprofile2.ejbStore();
            pri1.ejbStore();
            pri2.ejbStore();
            pri3.ejbStore();
            pri4.ejbStore();

            //Second: prepare test data -- end
            
            ///start test

            PricingRateInventory priTest = new PricingRateInventory(_srk);
            priTest = priTest.findMostRecentByRateCode(DUMMY_RATECODE);

            //test that findMostRecentByRateCode grabs most recent PRI
            assertNotNull(priTest);
            assertNotNull(priPK4);
            //assertEquals(priTest.getPk(), priPK3);

        }finally{
            _srk.cleanTransaction();
        }
    }
}
