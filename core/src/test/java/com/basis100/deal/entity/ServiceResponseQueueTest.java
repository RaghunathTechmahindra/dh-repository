/**
 * <p>@(#)ServiceResponseQueueTest.java Sep 10, 2007</p>
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.basis100.deal.entity;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.ServiceResponseQueuePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

 /**
  * <p>ServiceResponseQueueTest</p>
  * Express Entity class unit test: ServiceResponseQueue
  */
public class ServiceResponseQueueTest extends ExpressEntityTestCase 
    implements UnitTestLogging {

    private static final Logger _logger = 
        LoggerFactory.getLogger(ResponseTest.class);
    private static final String ENTITY_NAME = "SERVICERESPONSEQUEUE"; 
  
    // retreived data count that is stored in UnitTestDataRepository.xml
    private static final int DATA_COUNT = 1;

    private int testSeqForGen = 0;

    private SessionResourceKit _srk;
    
    // properties
    protected int    serviceResponseQueueId;
    protected int    dealId;
    protected int    copyId;
    protected int    serviceProductId;
    protected int    processStatusId;
    protected int    retryCounter;
    protected String responseXML;      
    
    protected Date   responseDate;
    protected String channelTransactionKey;
    protected String requestType;
    protected int    instProfId;
        
    /**
     * <p>setUp</p>
     * Setting up test case.
     */
    @Before
    public void setUp() throws Exception {
      
        _logger.info(BORDER_START, ENTITY_NAME + " TestCase setting up ...");
        
        // get random seqence number
        Double indexD = new Double(Math.random() * DATA_COUNT);
        testSeqForGen = indexD.intValue();
      
        //      get test data from repository
        serviceResponseQueueId = _dataRepository.getInt(ENTITY_NAME,
                                                        "SERVICERESPONSEQUEUEID", 
                                                        testSeqForGen);
        dealId = _dataRepository.getInt(ENTITY_NAME,
                                        "DEALID",
                                        testSeqForGen);
        copyId = _dataRepository.getInt(ENTITY_NAME,  
                                        "COPYID", 
                                        testSeqForGen);
        serviceProductId = _dataRepository.getInt(ENTITY_NAME,
                                                  "SERVICEPRODUCTID", 
                                                  testSeqForGen);
        processStatusId = _dataRepository.getInt(ENTITY_NAME,  
                                                 "PROCESSSTATUSID", 
                                                 testSeqForGen);
        retryCounter = _dataRepository.getInt(ENTITY_NAME,  
                                              "RETRYCOUNTER", 
                                              testSeqForGen);
        responseXML = _dataRepository.getString(ENTITY_NAME,  
                                                "RESPONSEXML", 
                                                testSeqForGen);
        responseDate = _dataRepository.getDate(ENTITY_NAME,  
                                               "RESPONSEDATE", 
                                               testSeqForGen);
        channelTransactionKey = _dataRepository.getString(ENTITY_NAME,  
                                                          "CHANNELTRANSACTIONKEY", 
                                                          testSeqForGen);
        requestType = _dataRepository.getString(ENTITY_NAME,  
                                                "REQUESTTYPE", 
                                                testSeqForGen);
        instProfId = _dataRepository.getInt(ENTITY_NAME,  
                                            "INSTITUTIONPROFILEID", 
                                            testSeqForGen);       

        // init ResouceManager
        ResourceManager.init();
        //  init session resource kit.
        _srk = new SessionResourceKit();

        _logger.info(BORDER_END, "Setting up Done");

    }
    
    @After
    public void tearDown() {

      //      free resources
      _srk.freeResources( );

    }
  
    /**
     * test findByPrimaryKey.
     */
    @Ignore @Test
    public void testFindByPrimaryKey()  throws Exception {
      
        _logger.info(BORDER_START, "findByPrimaryKey");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        ServiceResponseQueue responseQueue = new ServiceResponseQueue(_srk, null);
        responseQueue  = responseQueue
            .findByPrimaryKey(new ServiceResponseQueuePK(serviceResponseQueueId));
        
        assertEqualsProperties(responseQueue);

        _logger.info(BORDER_END, "findByPrimaryKey");
    }
    
    /**
     * test findNextUnprocessedResponse.
     */
    @Ignore @Test
    public void testFindNextUnprocessedResponse()  throws Exception {
      
        _logger.info(BORDER_START, "findNextUnprocessedResponse");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        ServiceResponseQueue responseQueue = new ServiceResponseQueue(_srk, null);
        int maxTry = 5; // for now
        responseQueue  = responseQueue.findNextUnprocessedResponse(maxTry, 180);
        
        assertEqualsProperties(responseQueue);

        _logger.info(BORDER_END, "findNextUnprocessedResponse");
    }
    
    /**
     * test resetLockedResponse.
     */
    @Ignore @Test
    public void testResetLockedResponse()  throws Exception {
      
        _logger.info(BORDER_START, "resetLockedResponse");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        ServiceResponseQueue responseQueue = new ServiceResponseQueue(_srk, null);
        int result = responseQueue.resetLockedResponse();
        
        _logger.info(BORDER_END, "resetLockedResponse");
    }

    /**
     * test findByServiceResponseQueueId.
     */
    @Ignore @Test
    public void testFindByServiceResponseQueueId()  throws Exception {
      
        _logger.info(BORDER_START, "findByServiceResponseQueueId");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        ServiceResponseQueue responseQueue = new ServiceResponseQueue(_srk, null);
        Collection queues 
            = responseQueue.findByServiceResponseQueueId(serviceResponseQueueId);
        
        // assertEquals for each
        Iterator requestIter = queues.iterator();
        for (; requestIter.hasNext(); ) {
            ServiceResponseQueue aQueue 
                = (ServiceResponseQueue) requestIter.next();
            // the differences should be only copyId,
            // however, copy is not used.
            assertEqualsProperties(aQueue);
        }
        
        _logger.info(BORDER_END, "findByServiceResponseQueueId");
    }
    
    /**
     * test findByServiceResponse.
     * this method is duplicated with findByPriaryKey
     */
    @Ignore @Test
    public void testFindByServiceResponse()  throws Exception {
      
        _logger.info(BORDER_START, "findByServiceResponse");
    
        _logger.info(BORDER_END, "findByServiceResponse");
    }

    /**
     * test findByserviceProductId.
     */
    @Ignore @Test
    public void testFindByserviceProductId()  throws Exception {
      
        _logger.info(BORDER_START, "findByserviceProductId");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        ServiceResponseQueue responseQueue = new ServiceResponseQueue(_srk, null);
        Collection queues 
            = responseQueue.findByServiceResponseQueueId(serviceProductId);
        // check assertEqual only insitutionId, dealId and copyId
        Iterator requestIter = queues.iterator();
        for (; requestIter.hasNext(); ) {
            ServiceResponseQueue aQueue 
                = (ServiceResponseQueue) requestIter.next();
            // the differences should be only copyId,
            // however, copy is not used.
            assertEquals(aQueue.getServiceProductId(), serviceProductId);
            assertEquals(aQueue.getInstitutionProfileId(), instProfId);
        }
        
        _logger.info(BORDER_END, "findByserviceProductId");
    }

    
    /**
     * assertEqualProperties
     * 
     * check assertEquals for all properties of ServiceResponseQuwuw with test Data
     * 
     * @param entity: ServiceProvider
     * @throws Exception
     */
    private void assertEqualsProperties(ServiceResponseQueue entity) throws Exception
    {
        assertEquals(entity.getServiceResponseQueueId(), serviceResponseQueueId);
        assertEquals(entity.getDealId(), dealId);
        assertEquals(entity.getCopyId(), copyId);
        assertEquals(entity.getServiceProductId(), serviceProductId);
        assertEquals(entity.getProcessStatusId(), processStatusId);
        assertEquals(entity.getRetryCounter(), retryCounter);
        assertEquals(entity.getResponseXML(), responseXML);
        assertEquals(entity.getResponseDate(), responseDate);
        assertEquals(entity.getChannelTransactionKey(), channelTransactionKey);
        assertEquals(entity.getRequestType(), requestType);
        assertEquals(entity.getInstitutionProfileId(), instProfId);
    }
        

}
