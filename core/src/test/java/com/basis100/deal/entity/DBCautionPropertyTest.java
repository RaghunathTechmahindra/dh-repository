package com.basis100.deal.entity;

import java.io.IOException;
import java.util.List;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;

public class DBCautionPropertyTest  extends FXDBTestCase{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(DBCautionPropertyTest.class);
    
    //Initial data to be loaded before each test case.
	private final static String INIT_XML_DATA = "DBCautionPropertyDataSet.xml";
	private IDataSet dataSetTest;
	
	/**
     * Constructor function
     */
    public DBCautionPropertyTest(String name)  throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource("DBCautionPropertyDataSetTest.xml"));
    }
    
    @Override
	protected IDataSet getDataSet() throws Exception {
    	return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}
    
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
        return DatabaseOperation.INSERT;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.DELETE;
    }
    
    /**
     * Test method for CautionProperty#findByAddressComponents
     */
    @Test
    public void testFindByAddressComponents() throws Exception {

    	_logger.info(" Start findByAddressComponents");
    	 
    	ITable findByAddressComponents = dataSetTest.getTable("findByAddressComponents");
    	String  CPStreetNumber = (String)findByAddressComponents.getValue(0,"CPStreetNumber");
    	String  CPStreetName = (String)findByAddressComponents.getValue(0,"CPStreetName");
    	String  CPCity = (String)findByAddressComponents.getValue(0,"CPCity");
    	String  CPCity1 = (String)findByAddressComponents.getValue(0,"CPCity1");
  		int  instProfId = Integer.parseInt((String)findByAddressComponents.getValue(0,"instProfId"));
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
    	 
        CautionProperty cautionProp = new CautionProperty(srk);
        
        List<CautionProperty> cautionProps = cautionProp.findByAddressComponents(CPStreetNumber, CPStreetName, CPCity);
        assert cautionProps.size() > 0;
        
        List<CautionProperty> cautionProps1 = cautionProp.findByAddressComponents(CPStreetNumber, CPStreetName, CPCity1);
        assert cautionProps1.size() == 0;
        
        
        _logger.info(" End findByAddressComponents");
    }
    
    /**
     * Test method for CautionProperty#create
     */
    @Test
    public void testCreate() throws Exception {

    	_logger.info(" Start testCreate");
    	 
    	ITable testCreate = dataSetTest.getTable("testCreate");
    	int  cautionPropertiesId = Integer.parseInt((String)testCreate.getValue(0,"cautionPropertiesId"));
    	int  CPProvinceId = Integer.parseInt((String)testCreate.getValue(0,"CPProvinceId"));
    	String  CPStreetNumber = (String)testCreate.getValue(0,"CPStreetNumber");
    	String  CPStreetName = (String)testCreate.getValue(0,"CPStreetName");
    	String  CPCity = (String)testCreate.getValue(0,"CPCity");
    	String  CPPostalFSA = (String)testCreate.getValue(0,"CPPostalFSA");
    	String  CPPostalLDU = (String)testCreate.getValue(0,"CPPostalLDU");
    	int  CPStreetDirectionId = Integer.parseInt((String)testCreate.getValue(0,"CPStreetDirectionId"));
    	int  CPStreetTypeId = Integer.parseInt((String)testCreate.getValue(0,"CPStreetTypeId"));
    	String  cautionReason = (String)testCreate.getValue(0,"cautionReason");
  		int  instProfId = Integer.parseInt((String)testCreate.getValue(0,"instProfId"));
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
    	 
        srk.beginTransaction();
        CautionProperty cautionProp = new CautionProperty(srk);
        
        cautionProp.create(CPProvinceId, CPCity, CPPostalFSA, CPPostalLDU, CPStreetName, CPStreetNumber, CPStreetDirectionId, CPStreetTypeId, cautionReason, cautionPropertiesId);
        
        List<CautionProperty> foundCPs = cautionProp.findByAddressComponents(CPStreetNumber, CPStreetName, CPCity);
        CautionProperty foundCP = foundCPs.get(0);
        
        assert foundCP != null;
        assertEquals(cautionPropertiesId, foundCP.getCautionPropertiesId());
        assertEquals(CPProvinceId, foundCP.getCPProvinceId());
        assertEquals(CPStreetName, foundCP.getCPStreetName());
        		
        srk.rollbackTransaction();
        _logger.info(" End testCreate");
    }


}
