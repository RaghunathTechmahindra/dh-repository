/*
 * @(#)DealFeeTest.java    2005-3-16
 *
 */

package com.basis100.deal.entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.deal.pk.DealFeePK;
import com.basis100.deal.pk.DealPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.test.ExpressEntityTestCase;

/**
 * DealFeeTest -
 * 
 * @version 1.0 2005-3-16
 * @author <A HREF="mailto:chyxiang@yahoo.com">Chen Xiang (Sean)</A>
 */
public class DealFeeTest extends ExpressEntityTestCase {

    // The logger
    private static Log _log = LogFactory.getLog(DealFeeTest.class);

    // The Session Resource Kit
    private SessionResourceKit _srk;

    // The DealFee Entity
    private DealFee _dealFeeDAO;

    // institutionId for deal
    private int dealInstitutionId;

    /**
     * Constructor function
     */
    public DealFeeTest() {

    }

    /**
     * setup.
     * 
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {

        _log.debug("Preparing the test cases ...");

        // init resource
        ResourceManager.init();
        _srk = new SessionResourceKit("Testing");
        _dealFeeDAO = new DealFee(_srk, null);

        // get institutionId
        dealInstitutionId = _dataRepository.getInt("DealFee",
                "InstitutionProfileId", 0);
    }

    /**
     * test get by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // set institutionid
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // get values from database
        int dealFeeId = _dataRepository.getInt("DealFee", "DealFeeId", 0);
        int copyId = _dataRepository.getInt("DealFee", "CopyId", 0);

        // get DealFee find by primary key
        DealFee aDealFee = _dealFeeDAO.findByPrimaryKey(new DealFeePK(
                dealFeeId, copyId));

        // verify result
        assertEquals(dealFeeId, aDealFee.getDealFeeId());
        _log.info("Found Deal Fee PK: ");
        _log.info("       Fee Amount: " + aDealFee.getFeeAmount());
        _log.info("           Fee Id: " + aDealFee.getDealFeeId());
        _log.info("     Payment Date: " + aDealFee.getFeePaymentDate());
        _log.info("====================================");
    }

    /**
     * test create
     */
    @Test
    public void testCreate() throws Exception {

        // init vpd institutionId
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // get values from database
        int dealId = _dataRepository.getInt("DealFee", "DealId", 0);
        int copyId = _dataRepository.getInt("DealFee", "CopyId", 0);

        // create dealfee
        DealFee aDealFee = _dealFeeDAO.create(new DealPK(dealId, copyId));

        // verify result
        assertEquals(dealId, aDealFee.getDealId());
        _log.info("Create Deal Fee: ");
        _log.info("       Fee Amount: " + aDealFee.getFeeAmount());
        _log.info("           Fee Id: " + aDealFee.getDealFeeId());
        _log.info("     Payment Date: " + aDealFee.getFeePaymentDate());
        _log.info("====================================");
    }

    /*
     * teatdown
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

}
