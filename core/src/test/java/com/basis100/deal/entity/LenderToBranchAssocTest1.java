package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.BranchProfilePK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.LenderProfilePK;
import com.basis100.deal.pk.LiabilityPK;
import com.basis100.resources.SessionResourceKit;

public class LenderToBranchAssocTest1 extends FXDBTestCase {
	private IDataSet dataSetTest;
	private Collection collection =null;
    private LenderToBranchAssoc lenderToBranchAssoc = null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public LenderToBranchAssocTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(LenderToBranchAssoc.class.getSimpleName()+"DataSetTest.xml"));
	}
	


	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.lenderToBranchAssoc = new LenderToBranchAssoc(srk);
	}
	
	public void testCreate() throws Exception
	{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int institutionProfileId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String)testCreate.getValue(0, "BRANCHPROFILEID"));
		String branchName = (String)testCreate.getValue(0, "BRANCHNAME");
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		
		BranchProfilePK bpk = new BranchProfilePK(id);
		LenderProfilePK lpk = new LenderProfilePK(id); 
		
		LenderToBranchAssoc aLenderToBranchAssoc = lenderToBranchAssoc.create(lpk, bpk);
		
		assertTrue(aLenderToBranchAssoc.getBranchProfileId()== id);
		assertTrue(aLenderToBranchAssoc.getBranchProfileId()== id);
		
		srk.rollbackTransaction();
	}
	

}
