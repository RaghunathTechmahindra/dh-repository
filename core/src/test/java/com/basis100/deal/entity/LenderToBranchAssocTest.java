/*
 * @(#)LenderToBranchAssocTest.java   Sep 7, 2007
 *
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.entity;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.BranchProfilePK;
import com.basis100.deal.pk.LenderProfilePK;
import com.basis100.deal.pk.LenderToBranchAssocPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * LenderToBranchAssocTest
 * 
 * @version 1.0 Sep 6, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class LenderToBranchAssocTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(LenderToBranchAssocTest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test the find by primary key
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {
        // get input data from repository
        int lenderProfileID = _dataRepository.getInt("LenderToBranchAssoc",
                "lenderProfileID", 0);
        int branchProfileId = _dataRepository.getInt("LenderToBranchAssoc",
                "branchProfileId", 0);
        int contactId = _dataRepository.getInt("LenderToBranchAssoc",
                "contactId", 0);
        String elPhoneNumber = _dataRepository.getString("LenderToBranchAssoc",
                "ELPHONENUMBER", 0);
        int dealInstitutionId = _dataRepository.getInt("LenderToBranchAssoc",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "findByPrimaryKey");

        // Create the initial entity
        LenderToBranchAssoc _lenderToBranchAssoc = new LenderToBranchAssoc(_srk);

        // find by primary key
        _lenderToBranchAssoc = _lenderToBranchAssoc
                .findByPrimaryKey(new LenderToBranchAssocPK(lenderProfileID,
                        branchProfileId, contactId));

        // Check that the data retrieved matches the DB
        assertTrue(_lenderToBranchAssoc.getELPhoneNumber()
                .equals(elPhoneNumber));

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * test the find by Branch and Lender
     */
    @Test
    public void testFindByBranchAndLender() throws Exception {
        // get input data from repository
        int lenderProfileID = _dataRepository.getInt("LenderToBranchAssoc",
                "lenderProfileID", 0);
        int branchProfileId = _dataRepository.getInt("LenderToBranchAssoc",
                "branchProfileId", 0);
        String elPhoneNumber = _dataRepository.getString("LenderToBranchAssoc",
                "ELPHONENUMBER", 0);
        int dealInstitutionId = _dataRepository.getInt("LenderToBranchAssoc",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "FindByBranchAndLender");

        // Create the initial entity
        LenderToBranchAssoc _lenderToBranchAssoc = new LenderToBranchAssoc(_srk);

        // find by primary key
        List lenderToBranchAssocs = (List) _lenderToBranchAssoc
                .findByBranchAndLender(new BranchProfilePK(branchProfileId),
                        new LenderProfilePK(lenderProfileID));

        // Check that the data retrieved matches the DB
        assertTrue(((LenderToBranchAssoc) lenderToBranchAssocs.get(0))
                .getELPhoneNumber().equals(elPhoneNumber));

        _logger.info(BORDER_END, "FindByBranchAndLender");
    }

    /**
     * test the Create
     */
    @Test
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create");

        // get input data from repository
        int lenderProfileID = _dataRepository.getInt("LenderToBranchAssoc",
                "lenderProfileID", 0);
        int branchProfileId = _dataRepository.getInt("LenderToBranchAssoc",
                "branchProfileId", 0);
        int dealInstitutionId = _dataRepository.getInt("LenderToBranchAssoc",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // Create the initial entity
        LenderToBranchAssoc _lenderToBranchAssoc = new LenderToBranchAssoc(_srk);

        // Start new transaction
        _srk.beginTransaction();

        // create
        _lenderToBranchAssoc = _lenderToBranchAssoc.create(new LenderProfilePK(
                lenderProfileID), new BranchProfilePK(branchProfileId));

        // Save the changes
        _lenderToBranchAssoc.ejbStore();

        // get it from the DB to verify
        _lenderToBranchAssoc = _lenderToBranchAssoc
                .findByPrimaryKey(new LenderToBranchAssocPK(
                        _lenderToBranchAssoc.getLenderProfileId(),
                        _lenderToBranchAssoc.getBranchProfileId(),
                        _lenderToBranchAssoc.getContactId()));

        // Check that the object is OK
        assertNotNull(_lenderToBranchAssoc);

        // Rollback transaction
        _srk.rollbackTransaction();

        // Free the resources
        _srk.freeResources();

        _logger.info(BORDER_END, "Create");
    }

}
