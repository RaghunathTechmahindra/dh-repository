package com.basis100.deal.entity;

import java.io.IOException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.docprep.extract.doctag.Term;
import com.basis100.deal.docprep.extract.doctag.TimeAtJob;
import com.basis100.deal.docprep.extract.doctag.TotalAmount;
import com.basis100.deal.docprep.extract.doctag.TotalAmountPlusFees;
import com.basis100.resources.SessionResourceKit;

public class TotalAmountPlusFeesTest extends FXDBTestCase {
	
	private IDataSet dataSetTest;
	private TotalAmountPlusFees totalAmountPlusFees = null;
    FMLQueryResult fmlQueryResult = null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public TotalAmountPlusFeesTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(TotalAmountPlusFees.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected DatabaseOperation getSetUpOperation() throws Exception
	    {
		totalAmountPlusFees = new TotalAmountPlusFees();
	    	return DatabaseOperation.INSERT;
	    }
	
	public void testExtract() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testExtract");
		
		int institutionProfileId = Integer.parseInt((String)testExtract.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String)testExtract.getValue(0, "dealid"));
		int copyid = Integer.parseInt((String)testExtract.getValue(0, "copyid"));
		int dpid = Integer.parseInt((String)testExtract.getValue(0, "DEALPURPOSEID"));
		
		Deal deal = new Deal(srk, null, id, copyid);
		
		com.basis100.deal.entity.DealEntity de = (com.basis100.deal.entity.DealEntity )deal;
		deal.setInstitutionProfileId(institutionProfileId);
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		
		int lang = 0;
		fmlQueryResult = totalAmountPlusFees.extract(de, lang, null, srk);

		assert dpid == deal.getDealPurposeId();
		assertNotNull(fmlQueryResult);
		assert fmlQueryResult.getValues().size()>0;
		
		srk.rollbackTransaction();
	}
}
