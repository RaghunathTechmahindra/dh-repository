package com.basis100.deal.entity.simplecache;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.basis100.deal.pk.DealPK;

public class PKWrapperTest {

	@Test
	public void testHashCode() {
		DealPK pk = new DealPK(12345, 67);
		
		PKWrapper pkw1 = new PKWrapper(pk, 1);
		PKWrapper pkw2 = new PKWrapper(pk, 1);
		
		assertTrue(pkw1.hashCode() == pkw2.hashCode());

		pkw2 = new PKWrapper(pk, 0);
		assertFalse(pkw1.hashCode() == pkw2.hashCode());
	}


	@Test
	public void testEqualsObject() {
		DealPK pk1 = new DealPK(23456, 78);
		DealPK pk2 = new DealPK(23456, 78);
		
		PKWrapper pkw1 = new PKWrapper(pk1, 1);
		PKWrapper pkw2 = new PKWrapper(pk2, 1);
		
		assertTrue(pkw1.equals(pkw2));

		pkw2 = new PKWrapper(pk2, 0);
		assertFalse(pkw1.equals(pkw2));

		pk2 = new DealPK(23456, 79);
		pkw2 = new PKWrapper(pk2, 1);
		assertFalse(pkw1.equals(pkw2));
	
	}

}
