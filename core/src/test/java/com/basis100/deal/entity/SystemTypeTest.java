package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.SystemTypePK;
import com.basis100.resources.SessionResourceKit;

public class SystemTypeTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private SystemType systemType;
	// session resource kit
    private SessionResourceKit _srk;

	public SystemTypeTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(SystemType.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.systemType = new SystemType(srk);
	}
	
	public void testFindByPrimaryKey() throws Exception {
    	ITable expectedSystemType = dataSetTest.getTable("testFindByPrimaryKey");
		int systemTypeId = (Integer) DataType.INTEGER.typeCast(expectedSystemType.getValue(0, "SYSTEMTYPEID"));
		int instProfId = (Integer) DataType.INTEGER.typeCast(expectedSystemType.getValue(0, "INSTITUTIONPROFILEID"));
		
       systemType.findByPrimaryKey(new SystemTypePK(systemTypeId));
	   assertEquals(systemTypeId, systemType.getSystemTypeId());
    }
	
	   
}