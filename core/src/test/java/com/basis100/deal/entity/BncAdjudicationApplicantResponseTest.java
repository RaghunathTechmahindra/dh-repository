/*
 * @(#)BncAdjudicationApplicantResponseTest.java Sep 21, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.BncAdjudicationApplicantResponsePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * @author amalhi
 * 
 * Junit for entity BncAdjudicationApplicantResponse
 * 
 */
public class BncAdjudicationApplicantResponseTest extends ExpressEntityTestCase
        implements UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(BncAdjudicationApplicantResponseTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public BncAdjudicationApplicantResponseTest() {

    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.BncAdjudicationApplicantResponse#findByPrimaryKey(com.basis100.deal.pk.BncAdjudicationApplicantResponsePK)}.
     */
    @Ignore("no data in associated table")
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int responseId = _dataRepository.getInt(
                "BncAdjudicationApplicantResponse", "responseId", 0);
        int instProfId = _dataRepository.getInt(
                "BncAdjudicationApplicantResponse", "INSTITUTIONPROFILEID", 0);
        int applicantNumber = _dataRepository.getInt(
                "BncAdjudicationApplicantResponse", "applicantNumber", 0);

        _logger.info(BORDER_START,
                "BncAdjudicationApplicantResponse - testFindByPrimaryKey");
        _logger
                .info(
                        "Got Values from new BncAdjudicationApplicantResponse ::: [{}]",
                        responseId + "::" + instProfId + "::" + applicantNumber);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        BncAdjudicationApplicantResponse _bncAdjudicationApplicantResponse = new BncAdjudicationApplicantResponse(
                _srk);

        // Find by primary key
        _bncAdjudicationApplicantResponse = _bncAdjudicationApplicantResponse
                .findByPrimaryKey(new BncAdjudicationApplicantResponsePK(
                        responseId, applicantNumber));

        _logger
                .info(
                        "Got Values from new BncAdjudicationApplicantResponse ::: [{}]",
                        _bncAdjudicationApplicantResponse.getResponseId()
                                + "::"
                                + _bncAdjudicationApplicantResponse
                                        .getApplicantNumber());

        // Check if the data retrieved matches the DB
        assertEquals(_bncAdjudicationApplicantResponse.getResponseId(),
                responseId);
        assertEquals(_bncAdjudicationApplicantResponse.getApplicantNumber(),
                applicantNumber);

        _logger.info(BORDER_END,
                "BncAdjudicationApplicantResponse - testFindByPrimaryKey");
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.BncAdjudicationApplicantResponse#create(com.basis100.deal.pk.BncAdjudicationApplicantResponsePK)}.
     */
    @Ignore("no data in associated table")
    @Test
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create - BncAdjudicationApplicantResponse");

        // get input data from repository
        int instProfId = _dataRepository.getInt("Borrower",
                "INSTITUTIONPROFILEID", 0);
        int responseId = _dataRepository.getInt(
                "ADJUDICATIONAPPLICANTRESPONSE", "responseId", 0);
        int applicantNumber = _dataRepository.getInt(
                "ADJUDICATIONAPPLICANTRESPONSE", "applicantNumber", 0);

        _logger
                .info(
                        "Got Values from new BncAdjudicationApplicantResponse ::: [{}]",
                        instProfId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // begin transaction
        _srk.beginTransaction();

        // Create the initial entity
        BncAdjudicationApplicantResponse newEntity = new BncAdjudicationApplicantResponse(
                _srk);

        // call create method of entity
        newEntity = newEntity.create(new BncAdjudicationApplicantResponsePK(
                responseId, applicantNumber));

        _logger.info("CREATED BncAdjudicationApplicantResponse ::: [{}]",
                newEntity.getResponseId() + "::"
                        + newEntity.getApplicantNumber());

        // check create correctly by calling findByPrimaryKey
        BncAdjudicationApplicantResponse foundEntity = new BncAdjudicationApplicantResponse(
                _srk);
        foundEntity = foundEntity
                .findByPrimaryKey(new BncAdjudicationApplicantResponsePK(
                        newEntity.getResponseId(), newEntity
                                .getApplicantNumber()));

        // verifying the created entity
        assertEquals(foundEntity.getResponseId(), newEntity.getResponseId());
        assertEquals(foundEntity.getApplicantNumber(), newEntity
                .getApplicantNumber());

        _logger.info(BORDER_END, "Create - BncAdjudicationApplicantResponse");

        // roll-back transaction.
        _srk.rollbackTransaction();
    }

}
