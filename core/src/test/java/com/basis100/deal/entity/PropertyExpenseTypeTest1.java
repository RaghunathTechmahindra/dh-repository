package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;

public class PropertyExpenseTypeTest1  extends  FXDBTestCase{
	
	private IDataSet dataSetTest;
	PropertyExpenseType propertyExpenseType=null;
	public PropertyExpenseTypeTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("PropertyExpenseTypeDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource("PropertyExpenseTypeDataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		propertyExpenseType=new PropertyExpenseType(srk);
	}
	//create(int, String, double, double)

	public void testCreate() throws Exception{
		srk.beginTransaction();
		ITable create = dataSetTest.getTable("testCreate");
		int id =Integer.parseInt(create.getValue(0,"Id").toString());
		String Desc =create.getValue(0,"Desc").toString();
		double gdsInc =Double.parseDouble(create.getValue(0,"GdsInc").toString());
		double tdsInc =Double.parseDouble(create.getValue(0,"TdsInc").toString());
		PropertyExpenseType propertyExpenseType1=propertyExpenseType.create(id, Desc, gdsInc, tdsInc);
		assertNotNull(propertyExpenseType1);
		srk.rollbackTransaction();
	    
	}
	public void testCreateFailure() throws Exception{
		boolean status=false;
		try{
		ITable testCreateFailure = dataSetTest.getTable("testCreateFailure");
		int id =Integer.parseInt(testCreateFailure.getValue(0,"Id").toString());
		String Desc =testCreateFailure.getValue(0,"Desc").toString();
		double gdsInc =Double.parseDouble(testCreateFailure.getValue(0,"GdsInc").toString());
		double tdsInc =Double.parseDouble(testCreateFailure.getValue(0,"TdsInc").toString());
		PropertyExpenseType propertyExpenseType1=propertyExpenseType.create(id, Desc, gdsInc, tdsInc);
		}catch (Exception e) {
			// TODO: handle exception
			status=false;
		}
		assertSame(false,status);
	}
   
}
