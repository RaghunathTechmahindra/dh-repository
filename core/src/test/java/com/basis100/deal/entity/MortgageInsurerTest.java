package com.basis100.deal.entity;

import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.pk.MortgageInsurerPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

public class MortgageInsurerTest extends ExpressEntityTestCase
    implements UnitTestLogging {

    // The logger
    private final static Logger _logger = 
        LoggerFactory.getLogger(InstitutionProfileTest.class);

    // the session resource kit.
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public MortgageInsurerTest() {
    }

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

        ResourceManager.init();
        _srk = new SessionResourceKit();
//        _srk.getExpressState().setDealInstitutionId(11);
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
    }
   
    /**
     * test the find by primary key
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        _logger.info(BORDER_START, "findByPrimaryKey");

        MortgageInsurer mortgageInsurer = new MortgageInsurer(_srk);

        _logger.info(SHORT_BREAK);
        
        mortgageInsurer.findByPrimaryKey(new MortgageInsurerPK(3));        

        _logger.info(BORDER_END, "findByPrimaryKey");
    }
    
    @Test
    public void testFindMITypesByMortgageInsurer() throws Exception {
        
        _logger.info(BORDER_START, "testFindMITypesByMortgageInsurer");
        
        MortgageInsurer mortgageInsurer = new MortgageInsurer(_srk);
        
        List<Integer> milist = mortgageInsurer.findMITypesByMortgageInsurer(Mc.MI_INSURER_GE);
        
        assertFalse(milist.isEmpty());
        
        for(int mitype : milist){
            _logger.info("MIType =" + mitype);
        }
        _logger.info(BORDER_END, "testFindMITypesByMortgageInsurer");
    }
}
