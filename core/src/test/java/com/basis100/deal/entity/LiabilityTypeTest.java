/*
 * @(#)LiabilityTypeTest.java Sep 10, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.LiabilityTypePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * LiabilityTypeTest
 * 
 * @version 1.0 Sep 10, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class LiabilityTypeTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(LiabilityTypeTest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test the find by primary key
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {
        // get input data from repository
        int liabilityID = _dataRepository.getInt("LiabilityType",
                "LIABILITYTYPEID", 0);
        String liablityTypeDesc = _dataRepository.getString("LiabilityType",
                "LDESCRIPTION", 0);

        _logger.info(BORDER_START, "findByPrimaryKey");

        // Create the initial entity
        LiabilityType _liabilityType = new LiabilityType(_srk);

        // find by primary key
        _liabilityType = _liabilityType.findByPrimaryKey(new LiabilityTypePK(
                liabilityID));

        // Check that the data retrieved matches the DB
        assertTrue(liablityTypeDesc.equals(_liabilityType.getLDescription()));

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * test the create
     */
    @Test
    public void testCreate() throws Exception {
        // Need an institutionprofileID to set VPD context
        int dealInstitutionId = _dataRepository.getInt("Liability",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // Setting up dummy data
        int liablityTypeID = 50;
        String desc = "Test Data";
        double gdsInc = 100;
        double tdsInc = 100;
        String paymentQualifier = "F";

        // Start new transaction
        _srk.beginTransaction();

        _logger.info(BORDER_START, "testCreate");

        // Create the initial entity
        LiabilityType _liabilityType = new LiabilityType(_srk);

        // Create a new record
        _liabilityType.create(liablityTypeID, desc, gdsInc, tdsInc,
                paymentQualifier);
        
        //Check the inserted value in the DB
        _liabilityType = _liabilityType.findByPrimaryKey(new LiabilityTypePK(
                liablityTypeID));

        //assert on the retrieved value
        assertTrue(_liabilityType.getLDescription().equals(desc));

        // Rollback transaction
        _srk.rollbackTransaction();

        _logger.info(BORDER_END, "testCreate");

    }

}
