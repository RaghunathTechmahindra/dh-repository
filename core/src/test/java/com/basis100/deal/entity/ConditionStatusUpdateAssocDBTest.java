package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.doctag.PrivacyClause;
import com.basis100.deal.pk.ConditionPK;
import com.basis100.deal.pk.ConditionStatusUpdateAssocPK;
import com.basis100.resources.SessionResourceKit;
import com.sun.org.apache.bcel.internal.generic.INSTANCEOF;

public class ConditionStatusUpdateAssocDBTest extends FXDBTestCase {
	private IDataSet dataSetTest;
    private ConditionStatusUpdateAssoc conditionStatusUpdateAssoc =null;
    private Collection<ConditionStatusUpdateAssoc> collection=null;
    private Collection<Integer> collection1=null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public ConditionStatusUpdateAssocDBTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ConditionStatusUpdateAssoc.class.getSimpleName()+"DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.conditionStatusUpdateAssoc = new ConditionStatusUpdateAssoc(srk);
	}
	
	public void testfindByPrimaryKey() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByPrimaryKey = dataSetTest.getTable("testfindByPrimaryKey");
		int dealInstitutionId = Integer.parseInt((String)testfindByPrimaryKey.getValue(0, "INSTITUTIONPROFILEID"));
		int documentStatusId = Integer.parseInt((String)testfindByPrimaryKey.getValue(0, "DOCUMENTSTATUSID"));
		int systemtypeid = Integer.parseInt((String)testfindByPrimaryKey.getValue(0, "SYSTEMTYPEID"));
		
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		conditionStatusUpdateAssoc = conditionStatusUpdateAssoc.findByPrimaryKey(new ConditionStatusUpdateAssocPK(documentStatusId, systemtypeid));

		assertEquals(conditionStatusUpdateAssoc.documentStatusId, documentStatusId);
		
		srk.rollbackTransaction();
	}
	
	
	public void testfindBySystemType() throws Exception
	{
		srk.beginTransaction();
		ITable testfindBySystemType = dataSetTest.getTable("testfindBySystemType");
		int dealInstitutionId = Integer.parseInt((String)testfindBySystemType.getValue(0, "INSTITUTIONPROFILEID"));
		int systemtypeid = Integer.parseInt((String)testfindBySystemType.getValue(0, "SYSTEMTYPEID"));
		
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		collection = conditionStatusUpdateAssoc.findBySystemType(systemtypeid);

		assertNotNull(collection);
		
		srk.rollbackTransaction();
	}
	
	
	public void testfindBySystemTypeFailure() throws Exception
	{
		srk.beginTransaction();
		ITable testfindBySystemTypeFailure = dataSetTest.getTable("testfindBySystemTypeFailure");
		int dealInstitutionId = Integer.parseInt((String)testfindBySystemTypeFailure.getValue(0, "INSTITUTIONPROFILEID"));
		int systemtypeid = Integer.parseInt((String)testfindBySystemTypeFailure.getValue(0, "SYSTEMTYPEID"));
		
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		collection = conditionStatusUpdateAssoc.findBySystemType(systemtypeid);

		assert collection.size()>0;
		
		srk.rollbackTransaction();
	}
	
	
	public void testfinddocumentStatusIdBySystemType() throws Exception
	{
		srk.beginTransaction();
		ITable testfinddocumentStatusIdBySystemType = dataSetTest.getTable("testfinddocumentStatusIdBySystemType");
		int dealInstitutionId = Integer.parseInt((String)testfinddocumentStatusIdBySystemType.getValue(0, "INSTITUTIONPROFILEID"));
		int systemtypeid = Integer.parseInt((String)testfinddocumentStatusIdBySystemType.getValue(0, "SYSTEMTYPEID"));
		
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		collection1 = conditionStatusUpdateAssoc.finddocumentStatusIdBySystemType(systemtypeid);

		assertNotNull(collection1);
		
		srk.rollbackTransaction();
	}
	public void testfinddocumentStatusIdBySystemTypeFailure() throws Exception
	{
		srk.beginTransaction();
		ITable testfinddocumentStatusIdBySystemTypeFailure = dataSetTest.getTable("testfinddocumentStatusIdBySystemTypeFailure");
		int dealInstitutionId = Integer.parseInt((String)testfinddocumentStatusIdBySystemTypeFailure.getValue(0, "INSTITUTIONPROFILEID"));
		int systemtypeid = Integer.parseInt((String)testfinddocumentStatusIdBySystemTypeFailure.getValue(0, "SYSTEMTYPEID"));
		
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		collection1 = conditionStatusUpdateAssoc.finddocumentStatusIdBySystemType(systemtypeid);

		assert collection1.size()>0;
		
		srk.rollbackTransaction();
	}
	
}
