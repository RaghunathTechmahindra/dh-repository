package com.basis100.deal.entity;

import java.io.File;
import java.io.IOException;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.AppraisalOrderPK;
import com.basis100.deal.pk.DealEventStatusSnapshotPK;
import com.basis100.deal.pk.DealEventStatusUpdateAssocPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.ltx.unittest.util.EntityTestUtil;

public class DealEventStatusUpdateAssocTest1 extends FXDBTestCase{

	private IDataSet dataSetTest;	
	private DealEventStatusUpdateAssoc dealEventStatusUpdateAssoc;
	Clob clob=null;
	
	
	public DealEventStatusUpdateAssocTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(DealEventStatusUpdateAssoc.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(DealEventStatusUpdateAssoc.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		dealEventStatusUpdateAssoc = new DealEventStatusUpdateAssoc(srk);
	}
	
	public void testFindByPrimaryKeySuccess() throws DataSetException,IOException, SQLException, RemoteException, FinderException{
	
		ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKeySuccess");
		 int systemId=Integer.valueOf((String)testFindByPrimaryKey.getValue(0,"SYSTEMTYPEID"));	
		 int typeId=Integer.valueOf((String)testFindByPrimaryKey.getValue(0,"DEALEVENTSTATUSTYPEID"));	
		 int statusId=Integer.valueOf((String)testFindByPrimaryKey.getValue(0,"STATUSID"));	
		 srk.getExpressState().setDealInstitutionId(1);
		 dealEventStatusUpdateAssoc=dealEventStatusUpdateAssoc.findByPrimaryKey(systemId,typeId,statusId);
	    assertEquals(systemId, dealEventStatusUpdateAssoc.getSystemtypeId());
}  
	
	public void testFindByPrimaryKeyPkSuccess() throws DataSetException,IOException, SQLException, RemoteException, FinderException{
		
		ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKeyPkSuccess");
		 int systemId=Integer.valueOf((String)testFindByPrimaryKey.getValue(0,"SYSTEMTYPEID"));	
		 int typeId=Integer.valueOf((String)testFindByPrimaryKey.getValue(0,"DEALEVENTSTATUSTYPEID"));	
		 int statusId=Integer.valueOf((String)testFindByPrimaryKey.getValue(0,"STATUSID"));	
		 DealEventStatusUpdateAssocPK pk=new DealEventStatusUpdateAssocPK(systemId,typeId,statusId);
		 srk.getExpressState().setDealInstitutionId(1);
		 dealEventStatusUpdateAssoc=dealEventStatusUpdateAssoc.findByPrimaryKey(pk);
	    assertEquals(systemId, dealEventStatusUpdateAssoc.getSystemtypeId());
}  	
	
public void testFindBySystemTypeSuccess() throws DataSetException,IOException, SQLException, RemoteException, FinderException{
		Collection<DealEventStatusUpdateAssoc> dealEventList=null;
		ITable testFindBySystemType = dataSetTest.getTable("testFindBySystemTypeSuccess");
		 int systemId=Integer.valueOf((String)testFindBySystemType.getValue(0,"SYSTEMTYPEID"));			
		 srk.getExpressState().setDealInstitutionId(1);		
		 dealEventList=dealEventStatusUpdateAssoc.findBySystemType(systemId);
		 assertNotNull(dealEventList);
	     
}  	
	
public void testFindBySystemTypeFailure() throws DataSetException,IOException, SQLException, RemoteException, FinderException{
	Collection<DealEventStatusUpdateAssoc> dealEventList=null;
	ITable testFindBySystemType = dataSetTest.getTable("testFindBySystemTypeFailure");
	 int systemId=Integer.valueOf((String)testFindBySystemType.getValue(0,"SYSTEMTYPEID"));			
	 srk.getExpressState().setDealInstitutionId(1);
	 dealEventList=dealEventStatusUpdateAssoc.findBySystemType(systemId);
     assertEquals(0, dealEventList.size());
} 
}
