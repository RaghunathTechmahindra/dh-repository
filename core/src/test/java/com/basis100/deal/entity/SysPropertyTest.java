/*
 * @(#)SysPropertyTest.java Sep 28, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import java.util.Collection;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * SysPropertyTest
 * 
 * @version 1.0 Sep 28, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class SysPropertyTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(SysPropertyTest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test findByName
     * 
     * @throws Exception
     */
    @Test
    public void testFindByName() throws Exception {
        // get input data from repository
        int sysPropertyId = _dataRepository.getInt("SysProperty",
                "SYSPROPERTYTYPEID", 0);
        String propertyName = _dataRepository.getString("SysProperty", "Name",
                0);
        String propertyValue = _dataRepository.getString("SysProperty",
                "Value", 0);

        _logger.info(BORDER_START, "testFindByName");

        // create the initial entity
        SysProperty sysProperty = new SysProperty(_srk, sysPropertyId);

        // get The value for the property
        sysProperty = sysProperty.findByName(propertyName);

        // verify that the value is same as DB
        assertTrue(sysProperty.getValue().equals(propertyValue));

        _logger.info(BORDER_END, "testFindByName");
    }

    /**
     * test getProperty
     * 
     * @throws Exception
     */
    @Test
    public void testGetProperty() throws Exception {
        // get input data from repository
        int sysPropertyId = _dataRepository.getInt("SysProperty",
                "SYSPROPERTYTYPEID", 0);
        String propertyName = _dataRepository.getString("SysProperty", "Name",
                0);
        String propertyValue = _dataRepository.getString("SysProperty",
                "Value", 0);

        _logger.info(BORDER_START, "testGetProperty");

        // create the initial entity
        SysProperty sysProperty = new SysProperty(_srk, sysPropertyId);

        // get The value for the property
        String propVal = sysProperty.getProperty(propertyName, "");

        // verify that the value is same as DB
        assertTrue(propVal.equals(propertyValue));

        _logger.info(BORDER_END, "testGetProperty");
    }

    /**
     * test getProperties
     * 
     * @throws Exception
     */
    @Test
    public void testGetProperties() throws Exception {
        // get input data from repository
        int sysPropertyId = _dataRepository.getInt("SysProperty",
                "SYSPROPERTYTYPEID", 0);
        int institutionProfileId = _dataRepository.getInt("SysProperty",
                "institutionprofileid", 0);
        _logger.info(BORDER_START, "testGetProperties");

        // create the initial entity
        SysProperty sysProperty = new SysProperty(_srk, sysPropertyId);

        // get a list of the properties
        Properties sysProps = sysProperty.getProperties(institutionProfileId);

        // verify that the size is more than zero
        int collectionSize = sysProps.size();
        assertTrue(collectionSize > 0);
        _logger.info("Size " + collectionSize);
        _logger.info(BORDER_END, "testGetProperties");
    }

    /**
     * test getSysProperties
     * 
     * @throws Exception
     */
    @Test
    public void testGetSysProperties() throws Exception {
        // get input data from repository
        int sysPropertyId = _dataRepository.getInt("SysProperty",
                "SYSPROPERTYTYPEID", 0);
        _logger.info(BORDER_START, "testGetSysProperties");

        // create the initial entity
        SysProperty sysProperty = new SysProperty(_srk, sysPropertyId);

        // get a list of the properties
        Collection<SysProperty> sysProps = sysProperty.getSysProperties();

        // verify that the size is more than zero
        int collectionSize = sysProps.size();
        assertTrue(collectionSize > 0);
        _logger.info("Size " + collectionSize);
        _logger.info(BORDER_END, "testGetSysProperties");

    }
}
