/*
 * @(#) DocumentProfileTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */


package com.basis100.deal.entity;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.DocumentProfilePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>DocumentProfileTest</p>
 * Express Entity class unit test: DocumentProfile
 */
public class DocumentProfileTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(DocumentProfileTest.class);

    /**
     * Constructor function
     */
    public DocumentProfileTest() {
    }

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        String documentVersion = _dataRepository.getString("DOCUMENTPROFILE",
                "DOCUMENTVERSION", 0);
        int languagePreferenceId = _dataRepository.getInt("DOCUMENTPROFILE",
                "LANGUAGEPREFERENCEID", 0);
        int lenderProfileId = _dataRepository.getInt("DOCUMENTPROFILE",
                "LENDERPROFILEID", 0);
        int documentTypeId = _dataRepository.getInt("DOCUMENTPROFILE",
                "DOCUMENTTYPEID", 0);
        String documentFormat = _dataRepository.getString("DOCUMENTPROFILE",
                "DOCUMENTFORMAT", 0);
        int instProfId = _dataRepository.getInt("DOCUMENTPROFILE",
                "INSTITUTIONPROFILEID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        srk.beginTransaction();
        _logger.info("here ======");

        // feed the input data and run the program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        DocumentProfile entity = new DocumentProfile(srk);
        DocumentProfilePK dpk = new DocumentProfilePK(documentVersion,
                languagePreferenceId, lenderProfileId, documentTypeId,
                documentFormat);
        entity = entity.findByPrimaryKey(dpk);

        // verify the running result.
        assertEquals(documentVersion, entity.getDocumentVersion());
        assertEquals(documentTypeId, entity.getDocumentTypeId());

        srk.rollbackTransaction();
        // free resources
        srk.freeResources();
    }

    /**
     * test create.
     */
    @Test
    public void testCreate() throws Exception {

        // get input data from repository
        String documentVersion = _dataRepository.getString("DOCUMENTPROFILE",
                "DOCUMENTVERSION", 0);
        int languagePreferenceId = _dataRepository.getInt("DOCUMENTPROFILE",
                "LANGUAGEPREFERENCEID", 0);
        int lenderProfileId = _dataRepository.getInt("DOCUMENTPROFILE",
                "LENDERPROFILEID", 0);
        int documentTypeId = _dataRepository.getInt("DOCUMENTPROFILE",
                "DOCUMENTTYPEID", 0);
        String documentFormat = _dataRepository.getString("DOCUMENTPROFILE",
                "DOCUMENTFORMAT", 0);
        int instProfId = _dataRepository.getInt("DOCUMENTPROFILE",
                "INSTITUTIONPROFILEID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed input data and run program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        srk.beginTransaction();
        DocumentProfile newEntity = new DocumentProfile(srk);
        DocumentProfilePK dppk = new DocumentProfilePK(documentVersion,
                languagePreferenceId, lenderProfileId, documentTypeId,
                documentFormat);
        newEntity = newEntity.findByPrimaryKey(dppk);
        newEntity.ejbRemove();
        newEntity = newEntity.create(dppk);
        newEntity.ejbStore();

        // check create correctlly.
        DocumentProfile foundEntity = new DocumentProfile(srk);
        foundEntity = foundEntity.findByPrimaryKey(dppk);

        // verifying
        assertEquals(foundEntity.getDocumentVersion(), newEntity.getDocumentVersion());
        assertEquals(foundEntity.getDocumentTypeId(), newEntity.getDocumentTypeId());
        // rollback transaction.
        srk.rollbackTransaction();
        // free resources.
        srk.freeResources();
    }
}
