/*
 * @(#)AppraisalOrderTest.java Sep 19, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.AppraisalOrderPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * AppraisalOrderTest
 * 
 * @version 1.0 Sep 19, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class AppraisalOrderTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(AppraisalOrderTest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test the find by primary key
     */
    @Ignore("No Data") @Test
    public void testFindByPrimaryKey() throws Exception {
        // get input data from repository
        int copyID = _dataRepository.getInt("AppraisalOrder", "copyID", 0);
        int appraisalOrderID = _dataRepository.getInt("AppraisalOrder",
                "appraisalOrderID", 0);
        String propertyOwner = _dataRepository.getString("AppraisalOrder",
                "PROPERTYOWNERNAME", 0);

        int dealInstitutionId = _dataRepository.getInt("AppraisalOrder",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "testFindByPrimaryKey");

        AppraisalOrder appraisalOrder = new AppraisalOrder(_srk, CalcMonitor
                .getMonitor(_srk));
        // find by primary key
        appraisalOrder = appraisalOrder.findByPrimaryKey(new AppraisalOrderPK(
                appraisalOrderID, copyID));

        // verify data in the database
        assertEquals(appraisalOrder.getPropertyOwnerName(), propertyOwner);

        _logger.info(BORDER_END, "testFindByPrimaryKey");

    }

    /**
     * test Create
     */
    @Test
    public void testCreate() throws Exception {
        // get input data from repository
        int copyID = _dataRepository.getInt("Property", "copyID", 0);
        int propertyID = _dataRepository.getInt("Property", "propertyID", 0);
        int dealInstitutionId = _dataRepository.getInt("Property",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // Start new transaction
        _srk.beginTransaction();

        _logger.info(BORDER_START, "testCreate");

        AppraisalOrder appraisalOrder = new AppraisalOrder(_srk, CalcMonitor
                .getMonitor(_srk));

        // create a new Appraisal order
        appraisalOrder.create(new PropertyPK(propertyID, copyID));

        appraisalOrder.ejbStore();

        // roll back
        _srk.rollbackTransaction();
        _logger.info(BORDER_END, "Create");

    }

}
