/*
 * @(#)MailDestinationTypeTest.java Sep 12, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.MailDestinationTypePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * MailDestinationTypeTest
 * 
 * @version 1.0 Sep 12, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class MailDestinationTypeTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(MailDestinationTypeTest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test the find by primary key
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {
        // get input data from repository
        int mailDestinationTypeID = _dataRepository.getInt(
                "MailDestinationType", "MAILDESTINATIONTYPEID", 0);
        String mailDestinationTypeDesc = _dataRepository.getString(
                "MailDestinationType", "MAILDESTINATIONTYPEDESC", 0);

        _logger.info(BORDER_START, "findByPrimaryKey");

        // Create the initial entity
        MailDestinationType mailDestinationType = new MailDestinationType(_srk);

        // Search by primary key
        mailDestinationType = mailDestinationType
                .findByPrimaryKey(new MailDestinationTypePK(
                        mailDestinationTypeID));

        // Check that the data retrieved matches the DB
        assertTrue(mailDestinationType.getMailDestinationTypeDesc().equals(
                mailDestinationTypeDesc));

        _logger.info(BORDER_END, "findByPrimaryKey");

    }

    /**
     * test the Create
     */
    @Ignore
    public void testCreate() throws Exception {
        // get input data from repository
        int mailDestinationTypeID = 99;
        String mailDestinationTypeDesc = "Test Description";

        // Start new transaction
        _srk.beginTransaction();

        _logger.info(BORDER_START, "create");

        // Create the initial entity
        MailDestinationType mailDestinationType = new MailDestinationType(_srk);
        
        // Create a new record
        mailDestinationType = mailDestinationType.create(mailDestinationTypeID,
                mailDestinationTypeDesc);

        // Check that the data retrieved matches the DB
        mailDestinationType = mailDestinationType
                .findByPrimaryKey(new MailDestinationTypePK(
                        mailDestinationTypeID));
        assertTrue(mailDestinationType.getMailDestinationTypeDesc().equals(
                mailDestinationTypeDesc));

        // Rollback transaction
        _srk.rollbackTransaction();

        _logger.info(BORDER_END, "testCreate");

    }

}
