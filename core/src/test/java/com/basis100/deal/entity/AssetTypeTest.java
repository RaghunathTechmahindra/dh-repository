package com.basis100.deal.entity;

import javax.sql.DataSource;

import com.basis100.deal.pk.AssetTypePK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

import junit.framework.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * AssetTypeTest
 * 
 * @version 1.0 Sep 20, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class AssetTypeTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(AssetTypeTest.class);

    // the
    private SessionResourceKit  _srk;
    private AssetType           _assetTypeDAO;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * 
     * @throws Exception
     */
    @Test
    public void testFindByID() throws Exception {
        // get input data from repository
        int assetTypeID = _dataRepository.getInt("AssetType", "AssetTypeID", 0);

        _logger.debug("Trying to find the Asset Type with the assetTypeID: "
                + assetTypeID);

        _assetTypeDAO = new AssetType(_srk);
        AssetTypePK assetTypePK = new AssetTypePK(assetTypeID);
        _assetTypeDAO.findByPrimaryKey(assetTypePK);
        assertNotNull(_assetTypeDAO);
        assertEquals(assetTypeID, _assetTypeDAO.getAssetTypeId());
        _logger.info("Found [" + _assetTypeDAO + "] ");

        _logger.info("Found assetType:");
        _logger.info("   assetType ID: " + _assetTypeDAO.getAssetTypeId());
        _logger.info("       assetType desc: "
                + _assetTypeDAO.getAssetTypeDescription());

    }

    /**
     * Test the create
     * 
     * @throws Exception
     */
    @Test
    public void testCreate() throws Exception {
        // get input data from repository
        int assetTypeID = 99;
        String assetTypeDesc = "Test Asset";
        int netWorthInc = 100;

        // Not needed for this class but is used in a trigger
        int institutionId = _dataRepository.getInt("Asset",
                "InstitutionProfileID", 0);

        _srk.getExpressState().setDealIds(0, institutionId, 0);

        _logger.info(BORDER_START, "Create");

        // begin transaction
        _srk.beginTransaction();

        // create a new AssetType
        _assetTypeDAO = new AssetType(_srk);
        _assetTypeDAO = _assetTypeDAO.create(assetTypeID, assetTypeDesc,
                netWorthInc);
        _assetTypeDAO.ejbStore();

        // verrify the data in the database
        AssetTypePK assetTypePK = new AssetTypePK(assetTypeID);
        _assetTypeDAO.findByPrimaryKey(assetTypePK);
        assertNotNull(_assetTypeDAO);
        assertEquals(assetTypeDesc, _assetTypeDAO.getAssetTypeDescription());
        _logger.info("Found [" + _assetTypeDAO + "] ");

        _logger.info("Found created assetType:");
        _logger.info("   assetType ID: " + _assetTypeDAO.getAssetTypeId());
        _logger.info("       assetType desc: "
                + _assetTypeDAO.getAssetTypeDescription());

        // roll back
        _srk.rollbackTransaction();
        _logger.info(BORDER_END, "Create");

    }

}
