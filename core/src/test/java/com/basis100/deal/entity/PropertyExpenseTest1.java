package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PropertyPK;

public class PropertyExpenseTest1  extends  FXDBTestCase{
	
	private IDataSet dataSetTest;
	 PropertyExpense  propertyExpense=null;
	
	public PropertyExpenseTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("PropertyExpenseDataSetTest.xml"));
	}


	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		propertyExpense=new PropertyExpense(srk,null);
	}
	
	public void testFindByName() throws Exception{
		ITable testFindByName = dataSetTest.getTable("testFindByName");
		String description =testFindByName.getValue(0,"Description").toString();
		int CopyId =Integer.parseInt(testFindByName.getValue(0,"CopyId").toString());
		PropertyExpense propertyExpense1=propertyExpense.findByName(description, CopyId);
		assertNotNull(propertyExpense1);
	}

	public void testFindByNameFailure() throws Exception{
		
		ITable testFindByNameFailure = dataSetTest.getTable("testFindByNameFailure");
		String description =testFindByNameFailure.getValue(0,"Description").toString();
		int CopyId =Integer.parseInt(testFindByNameFailure.getValue(0,"CopyId").toString());
		PropertyExpense propertyExpense1=propertyExpense.findByName(description, CopyId);
		assertNull(propertyExpense1);
	}

	public void testFindByParentAndType() throws Exception{
		ITable testFindByName = dataSetTest.getTable("testFindByParentAndType");
		
		int Id =Integer.parseInt(testFindByName.getValue(0,"Id").toString());
		int CopyId =Integer.parseInt(testFindByName.getValue(0,"CopyId").toString());
		int type =Integer.parseInt(testFindByName.getValue(0,"type").toString());
		PropertyPK pk =new PropertyPK(Id,CopyId);
		int size = propertyExpense.findByParentAndType(pk, type).size();
		assertNotSame(0,size);
	}
	
	public void testFindByParentAndTypeFailure() throws Exception{
		ITable testFindByName = dataSetTest.getTable("testFindByParentAndTypeFailure");
		int Id =Integer.parseInt(testFindByName.getValue(0,"Id").toString());
		int CopyId =Integer.parseInt(testFindByName.getValue(0,"CopyId").toString());
		int type =Integer.parseInt(testFindByName.getValue(0,"type").toString());
		PropertyPK pk =new PropertyPK(Id,CopyId);
		int size = propertyExpense.findByParentAndType(pk, type).size();
		assertSame(0,size);
	}
	
	public void testFindByProperty() throws Exception{
		ITable testFindByName = dataSetTest.getTable("testFindByProperty");
		int Id =Integer.parseInt(testFindByName.getValue(0,"Id").toString());
		int CopyId =Integer.parseInt(testFindByName.getValue(0,"CopyId").toString());
		PropertyPK pk =new PropertyPK(Id,CopyId);
		int size = propertyExpense.findByProperty(pk).size();
		assertNotSame(0,size);
	}
	
	public void testFindByPropertyFailure() throws Exception{
		ITable testFindByName = dataSetTest.getTable("testFindByPropertyFailure");
		int Id =Integer.parseInt(testFindByName.getValue(0,"Id").toString());
		int CopyId =Integer.parseInt(testFindByName.getValue(0,"CopyId").toString());
		PropertyPK pk =new PropertyPK(Id,CopyId);
		int size = propertyExpense.findByProperty(pk).size();
		assertSame(0,size);
	}
	
	public void testFindByDeal() throws Exception{
		ITable testFindByDeal = dataSetTest.getTable("testFindByDeal");
		int Id =Integer.parseInt(testFindByDeal.getValue(0,"Id").toString());
		int CopyId =Integer.parseInt(testFindByDeal.getValue(0,"CopyId").toString());
		DealPK pk =new DealPK(Id,CopyId);
		int size = propertyExpense.findByDeal(pk).size();
		assertNotSame(0,size);
	}
	
	public void testFindByDealFailure() throws Exception{
		ITable testFindByDealFailure = dataSetTest.getTable("testFindByDealFailure");
		int Id =Integer.parseInt(testFindByDealFailure.getValue(0,"Id").toString());
		int CopyId =Integer.parseInt(testFindByDealFailure.getValue(0,"CopyId").toString());
		DealPK pk =new DealPK(Id,CopyId);
		int size = propertyExpense.findByDeal(pk).size();
		assertSame(0,size);
	}
	public void testCreate() throws Exception{
		ITable testFindByDeal = dataSetTest.getTable("testCreate");
		int Id =Integer.parseInt(testFindByDeal.getValue(0,"Id").toString());
		int CopyId =Integer.parseInt(testFindByDeal.getValue(0,"CopyId").toString());
		int DealInstitutionId =Integer.parseInt(testFindByDeal.getValue(0,"DealInstitutionId").toString());
		srk.getExpressState().setDealInstitutionId(DealInstitutionId);
		PropertyPK pk =new PropertyPK(Id,CopyId);
		PropertyExpense propertyExpense1= propertyExpense.create(pk);
		assertNotNull(propertyExpense1);
	}
	public void testCreateFailure() throws Exception{
		boolean status=false;
		try{
		ITable testFindByDeal = dataSetTest.getTable("testCreateFailure");
		int Id =Integer.parseInt(testFindByDeal.getValue(0,"Id").toString());
		int CopyId =Integer.parseInt(testFindByDeal.getValue(0,"CopyId").toString());
		int DealInstitutionId =Integer.parseInt(testFindByDeal.getValue(0,"DealInstitutionId").toString());
		srk.getExpressState().setDealInstitutionId(DealInstitutionId);
		PropertyPK pk =new PropertyPK(Id,CopyId);
		PropertyExpense propertyExpense1= propertyExpense.create(pk);
		}catch (Exception e) {
			// TODO: handle exception
			status=false;
		}
		assertSame(false,status);
	}
}
