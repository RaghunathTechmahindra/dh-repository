package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;

public class MailDestinationTypeTest1 extends FXDBTestCase {
	private String INIT_XML_DATA = "MailDestinationTypeDataSet.xml";
	private MailDestinationType mailDestinationType;
	private IDataSet dataSetTest;
	private int destinationTypeId;

	public MailDestinationTypeTest1(String name) throws DataSetException,
			IOException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"MailDestinationTypeDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		// TODO Auto-generated method stub
		return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		int destinationTypeId = Integer.parseInt(dataSetTest
				.getTable("testCreate").getValue(0, "destinationTypeId")
				.toString());
		mailDestinationType = new MailDestinationType(srk, destinationTypeId);

	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	public void testCreate() throws JdbcTransactionException, RemoteException,
			CreateException, NumberFormatException, DataSetException,
			FinderException {
		srk.beginTransaction();

		int id = Integer.parseInt(dataSetTest.getTable("testCreate")
				.getValue(0, "id").toString());
		mailDestinationType = new MailDestinationType(srk, destinationTypeId);
		MailDestinationType mDestinationType = mailDestinationType.create(id,
				"Banker");
		assertEquals(destinationTypeId, mDestinationType.mailDestinationTypeId);
		srk.rollbackTransaction();
	}
}
