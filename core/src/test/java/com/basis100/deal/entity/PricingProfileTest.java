/*
 * @(#) PricingProfileTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filogix.express.test.UnitTestLogging;
import com.filogix.express.test.ExpressEntityTestCase;

import com.basis100.deal.pk.PricingProfilePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

/**
 * @author amalhi
 *
 * JUnit test for entity PricingProfile
 *
 */
public class PricingProfileTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(PricingProfileTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public PricingProfileTest() {

    }

    /**
     * To be executed before test
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env and free resources
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int pricingProfileId = _dataRepository.getInt("PricingProfile",
                "pricingProfileId", 0);
        int instProfId = _dataRepository.getInt("PricingProfile",
                "INSTITUTIONPROFILEID", 0);
        int profileStatusId = _dataRepository.getInt("PricingProfile",
                "profileStatusId", 0);

        _logger.info(BORDER_START, "findByPrimaryKey");
        _logger.info("Got Values from new Party Profile ::: [{}]",
                pricingProfileId + "::" + instProfId + "::" + profileStatusId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        PricingProfile _pricingProfile = new PricingProfile(_srk);

        // Find by primary key
        _pricingProfile = _pricingProfile
                .findByPrimaryKey(new PricingProfilePK(pricingProfileId));

        _logger.info("Got Values from new Pricing Profile ::: [{}]",
                _pricingProfile.getPricingProfileId() + "::"
                        + _pricingProfile.getProfileStatusId());

        // Check if the data retrieved matches the DB
        assertTrue(_pricingProfile.getPricingProfileId() == pricingProfileId);
        assertTrue(_pricingProfile.getProfileStatusId() == profileStatusId);

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

   /* *//**
     * test create method of entity
     *//*
    @Test
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "CREATE");

        // get input data from repository
        int instProfId = _dataRepository.getInt("PricingProfile",
                "INSTITUTIONPROFILEID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);
        
		//begin trasaction
		_srk.beginTransaction();

		// inital new entity created
        PricingProfile newEntity = new PricingProfile(_srk);

		//calling create method of entity
        newEntity = newEntity.create(newEntity.createPrimaryKey());

        _logger.info("CREATED PRICING PROFILE ::: [{}]", newEntity
                .getPricingProfileId()
                + "::" + newEntity.getProfileStatusId());

        // check create correctly by calling findByPrimaryKey
        PricingProfile foundEntity = new PricingProfile(_srk);
        foundEntity = foundEntity.findByPrimaryKey(new PricingProfilePK(
                newEntity.getPricingProfileId()));

        _logger.info("FOUND SAME PRICING PROFILE ::: [{}]", foundEntity
                .getPricingProfileId()
                + "::" + foundEntity.getProfileStatusId());

        // verifying new entity created above
        assertEquals(foundEntity.getPricingProfileId(), newEntity
                .getPricingProfileId());
        assertEquals(foundEntity.getProfileStatusId(), newEntity
                .getProfileStatusId());

        _logger.info(BORDER_END, "Create");
        // roll-back transaction.
        _srk.rollbackTransaction();

    }*/

}
