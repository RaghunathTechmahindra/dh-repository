package com.basis100.deal.entity;

import java.io.IOException;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.AppraisalOrderPK;
import com.basis100.deal.pk.ArchivedLoanApplicationPK;
import com.basis100.deal.pk.ArchivedLoanDecisionPK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PrimeIndexRateInventoryPK;
import com.basis100.deal.pk.PrimeIndexRateProfilePK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.deal.pk.SourceOfBusinessProfilePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.util.EntityTestUtil;

public class PrimeIndexRateProfileTestDB extends FXDBTestCase{

	private IDataSet dataSetTest;
	private PrimeIndexRateProfile primeIndexRateProfile;	
	private DealEntity entity;
	public PrimeIndexRateProfileTestDB(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(PrimeIndexRateProfile.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;	
	}

	
	@Override
	protected void setUp() throws Exception {
		super.setUp();		
		primeIndexRateProfile = new PrimeIndexRateProfile(srk);
	}	
	
	
	public void testFindByPrimeIndexRateInventory() throws Exception{		
		try {			
			srk.beginTransaction();
			ITable testFindByPrime = dataSetTest.getTable("testFindByPrimeIndexRateInventory");					
			int primeIndexRateProfileId=Integer.valueOf((String)testFindByPrime.getValue(0,"PRIMEINDEXRATEPROFILEID"));	
			int primeindexrateinventoryid=Integer.valueOf((String)testFindByPrime.getValue(0,"PRIMEINDEXRATEINVENTORYID"));
			srk.getExpressState().setDealInstitutionId(1);			
			PrimeIndexRateProfilePK pk=new PrimeIndexRateProfilePK(primeIndexRateProfileId);
			primeIndexRateProfile=primeIndexRateProfile.findByPrimeIndexRateInventory(primeindexrateinventoryid);
			assertEquals(primeIndexRateProfileId, primeIndexRateProfile.getPrimeIndexRateProfileId());				
		    srk.rollbackTransaction();		    
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
}
