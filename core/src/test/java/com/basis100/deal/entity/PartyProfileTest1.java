package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.LenderProfilePK;
import com.basis100.deal.pk.LiabilityPK;
import com.basis100.deal.pk.PartyProfilePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.entity.RemoveException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;

public class PartyProfileTest1 extends FXDBTestCase {
	private IDataSet dataSetTest;
	private PartyProfile partyProfile = null;
	CalcMonitor dcm = null;
	SessionResourceKit srk = new SessionResourceKit();

	public PartyProfileTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				PartyProfile.class.getSimpleName() + "DataSetTest.xml"));
	}



	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.partyProfile = new PartyProfile(srk);
	}

	    public void testCreate() throws Exception {
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int institutionProfileId = Integer.parseInt((String) testCreate
				.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String) testCreate.getValue(0,
				"PARTYPROFILEID"));
		int pid = Integer.parseInt((String) testCreate.getValue(0,
				"PARTYTYPEID"));
		int profileStatusId = Integer.parseInt((String) testCreate.getValue(0,
				"PROFILESTATUSID"));

		srk.getExpressState().setDealInstitutionId(institutionProfileId);

		PartyProfile aPartyProfile = partyProfile.create(id, profileStatusId);

		assertTrue(aPartyProfile.getPartyTypeId() == pid);
		assertTrue(aPartyProfile.getProfileStatusId() == profileStatusId);

		srk.rollbackTransaction();
	}

	public void testCreate1() throws Exception {
		srk.beginTransaction();
		ITable testCreate1 = dataSetTest.getTable("testCreate1");
		int institutionProfileId = Integer.parseInt((String) testCreate1
				.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String) testCreate1.getValue(0,
				"PARTYPROFILEID"));
		int pid = Integer.parseInt((String) testCreate1.getValue(0,
				"PARTYTYPEID"));
		int profileStatusId = Integer.parseInt((String) testCreate1.getValue(0,
				"PROFILESTATUSID"));
		String partyName = (String) testCreate1.getValue(0, "PARTYNAME");
		int contactId = Integer.parseInt((String) testCreate1.getValue(0,
				"CONTACTID"));
		String ptPShortName = (String) testCreate1.getValue(0, "PTPSHORTNAME");
		String ptPBusinessId = (String) testCreate1
				.getValue(0, "PTPBUSINESSID");
		String partyCompanyName = (String) testCreate1.getValue(0,
				"PARTYCOMPANYNAME");
		String notes = (String) testCreate1.getValue(0, "NOTES");
		String GEBranchTransitNum = (String) testCreate1.getValue(0,
				"GEBRANCHTRANSITNUM");
		String CHMCBranchTransitNum = (String) testCreate1.getValue(0,
				"CHMCBRANCHTRANSITNUM");

		srk.getExpressState().setDealInstitutionId(institutionProfileId);

		PartyProfile aPartyProfile = partyProfile.create(partyName,
				profileStatusId, contactId, pid, ptPShortName, ptPBusinessId,
				partyCompanyName, notes, GEBranchTransitNum,
				CHMCBranchTransitNum);

		assertTrue(aPartyProfile.getPartyTypeId() == pid);
		assertTrue(aPartyProfile.getProfileStatusId() == profileStatusId);
		assertTrue(aPartyProfile.getPartyName().equals(partyName));
		assertTrue(aPartyProfile.getContactId() == contactId);
		assertTrue(aPartyProfile.getPtPBusinessId().equals(ptPBusinessId));

		srk.rollbackTransaction();
	}

	public void testCreateWithPropertyAssoc() throws RemoteException,
			CreateException, JdbcTransactionException, DataSetException {

		srk.beginTransaction();

		ITable testCreateWithPropertyAssoc = dataSetTest
				.getTable("testCreateWithPropertyAssoc");
		int dealInstitutionId = Integer
				.parseInt((String) testCreateWithPropertyAssoc.getValue(0,
						"dealInstitutionId"));
		int partyProfileId = Integer
				.parseInt((String) testCreateWithPropertyAssoc.getValue(0,
						"partyProfileId"));
		String partyName = (String) testCreateWithPropertyAssoc.getValue(0,
				"dealInstitutionId");
		int profileStatusId = Integer
				.parseInt((String) testCreateWithPropertyAssoc.getValue(0,
						"profileStatusId"));
		int contactId = Integer.parseInt((String) testCreateWithPropertyAssoc
				.getValue(0, "contactId"));
		int partyTypeId = Integer.parseInt((String) testCreateWithPropertyAssoc
				.getValue(0, "partyTypeId"));
		String ptPShortName = (String) testCreateWithPropertyAssoc.getValue(0,
				"ptPShortName");
		String ptPBusinessId = (String) testCreateWithPropertyAssoc.getValue(0,
				"ptPBusinessId");
		String partyCompanyName = (String) testCreateWithPropertyAssoc
				.getValue(0, "partyCompanyName");
		int dealId = Integer.parseInt((String) testCreateWithPropertyAssoc
				.getValue(0, "dealId"));
		int propertyId = Integer.parseInt((String) testCreateWithPropertyAssoc
				.getValue(0, "propertyId"));
		String GEBranchTransitNum = (String) testCreateWithPropertyAssoc
				.getValue(0, "GEBranchTransitNum");
		String CHMCBranchTransitNum = (String) testCreateWithPropertyAssoc
				.getValue(0, "CHMCBranchTransitNum");

		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		partyProfile.setPartyProfileId(partyProfileId);

		PartyProfile profile = partyProfile.createWithPropertyAssoc(partyName,
				profileStatusId, contactId, partyTypeId, ptPShortName,
				ptPBusinessId, partyCompanyName, dealId, propertyId,
				GEBranchTransitNum, CHMCBranchTransitNum);
		assertTrue(profile.getPartyTypeId() == partyTypeId);
		assertTrue(profile.getProfileStatusId() == profileStatusId);
		assertTrue(profile.getPartyName().equals(partyName));
		assertTrue(profile.getContactId() == contactId);
		assertTrue(profile.getPtPBusinessId().equals(ptPBusinessId));

		assertEquals(dealInstitutionId, profile.institutionProfileId);
		srk.rollbackTransaction();
	}

	public void testCreateWithDealAssoc() throws RemoteException,
			CreateException, JdbcTransactionException, DataSetException {
		srk.beginTransaction();

		ITable testCreateWithDealAssoc = dataSetTest
				.getTable("testCreateWithDealAssoc");

		int dealInstitutionId = Integer
				.parseInt((String) testCreateWithDealAssoc.getValue(0,
						"dealInstitutionId"));
		int partyProfileId = Integer.parseInt((String) testCreateWithDealAssoc
				.getValue(0, "partyProfileId"));
		String partyName = (String) testCreateWithDealAssoc.getValue(0,
				"dealInstitutionId");
		int profileStatusId = Integer.parseInt((String) testCreateWithDealAssoc
				.getValue(0, "profileStatusId"));
		int contactId = Integer.parseInt((String) testCreateWithDealAssoc
				.getValue(0, "contactId"));
		int partyTypeId = Integer.parseInt((String) testCreateWithDealAssoc
				.getValue(0, "partyTypeId"));
		String ptPShortName = (String) testCreateWithDealAssoc.getValue(0,
				"ptPShortName");
		String ptPBusinessId = (String) testCreateWithDealAssoc.getValue(0,
				"ptPBusinessId");
		String partyCompanyName = (String) testCreateWithDealAssoc.getValue(0,
				"partyCompanyName");
		int dealId = Integer.parseInt((String) testCreateWithDealAssoc
				.getValue(0, "dealId"));

		srk.getExpressState().setDealInstitutionId(dealInstitutionId);

		partyProfile.setPartyProfileId(partyProfileId);

		PartyProfile profile = partyProfile.createWithDealAssoc(partyName,
				profileStatusId, contactId, partyTypeId, ptPShortName,
				ptPBusinessId, partyCompanyName, dealId);
		assertTrue(profile.getPartyTypeId() == partyTypeId);
		assertTrue(profile.getProfileStatusId() == profileStatusId);
		assertTrue(profile.getPartyName().equals(partyName));
		assertTrue(profile.getContactId() == contactId);
		assertTrue(profile.getPtPBusinessId().equals(ptPBusinessId));
		srk.rollbackTransaction();
	}

	public void testCreatePrimaryKey() throws CreateException,
			JdbcTransactionException {
		srk.beginTransaction();
		partyProfile.createPrimaryKey();
		srk.rollbackTransaction();
	}

	public void testFindByDealId() throws RemoteException, FinderException,
			DataSetException {
		ITable testFindByDealId = dataSetTest.getTable("testFindByDealId");
		int dealId = Integer.parseInt((String) testFindByDealId.getValue(0,
				"dealId"));
		Map map = partyProfile.findByDealId(dealId);
		assertEquals(true, map.keySet().size() > 0);
	}

	public void testFindByDealIdFailure() throws RemoteException,
			FinderException, DataSetException {
		ITable testFindByDealIdFailure = dataSetTest
				.getTable("testFindByDealIdFailure");
		int dealId = Integer.parseInt((String) testFindByDealIdFailure
				.getValue(0, "dealId"));
		Map map = partyProfile.findByDealId(dealId);
		assertEquals(true, map.keySet().size() <= 0);
	}

	public void testFindByPrimaryKey() throws RemoteException, FinderException,
			DataSetException {
		ITable testFindByPrimaryKey = dataSetTest
				.getTable("testFindByPrimaryKey");
		int partyProfileId = Integer.parseInt((String) testFindByPrimaryKey
				.getValue(0, "partyProfileId"));

		PartyProfilePK pk = new PartyProfilePK(partyProfileId);
		PartyProfile profile = partyProfile.findByPrimaryKey(pk);
		assertNotNull(profile);
		assertEquals(partyProfileId, profile.getPartyProfileId());
	}
	public void testGetPartyType() throws FinderException {
		String partyType = partyProfile.getPartyType();
		assertNotNull(partyType);
		assertNotNull(partyType);
	}

	public void testFindByDealSolicitor() throws FinderException,
			DataSetException {
		ITable testFindByDealSolicitor = dataSetTest
				.getTable("testFindByDealSolicitor");
		int dealId = Integer.parseInt((String) testFindByDealSolicitor
				.getValue(0, "dealId"));
		PartyProfile profile = partyProfile.findByDealSolicitor(dealId);
		assertNotNull(profile);
		assertEquals(dealId, profile.getDealId());
	}

	public void testExtractIngestionProfileStatus()
			throws JdbcTransactionException, FinderException, DataSetException {
		ITable testExtractIngestionProfileStatus = dataSetTest
				.getTable("testExtractIngestionProfileStatus");
		int pSobCategory = Integer
				.parseInt((String) testExtractIngestionProfileStatus.getValue(
						0, "pSobCategory"));
		int pPartyType = Integer
				.parseInt((String) testExtractIngestionProfileStatus.getValue(
						0, "pPartyType"));
		int rv = partyProfile.extractIngestionProfileStatus(pSobCategory,
				pPartyType);
		assertEquals(true, rv != -1);
	}

	public void testExtractIngestionProfileStatusFailure()
			throws JdbcTransactionException, FinderException, DataSetException {
		ITable testExtractIngestionProfileStatusFailure = dataSetTest
				.getTable("testExtractIngestionProfileStatusFailure");
		int pSobCategory = Integer
				.parseInt((String) testExtractIngestionProfileStatusFailure
						.getValue(0, "pSobCategory"));
		int pPartyType = Integer
				.parseInt((String) testExtractIngestionProfileStatusFailure
						.getValue(0, "pPartyType"));
		int rv = partyProfile.extractIngestionProfileStatus(pSobCategory,
				pPartyType);
		assertEquals(true, rv == -1);
	}

	public void testFindByBusinessIdAndType() throws DataSetException,
			RemoteException, FinderException {
		ITable testFindByBusinessIdAndType = dataSetTest
				.getTable("testFindByBusinessIdAndType");
		int pType = Integer.parseInt((String) testFindByBusinessIdAndType
				.getValue(0, "pType"));
		String theBusinessId = (String) testFindByBusinessIdAndType.getValue(0,
				"theBusinessId");

		PartyProfile profile = partyProfile.findByBusinessIdAndType(
				theBusinessId, pType);
		assertNotNull(profile);
		assertEquals(pType, profile.getPartyTypeId());
		assertEquals(theBusinessId, profile.getPtPBusinessId());
	}

	public void testFindByShortName() throws DataSetException, RemoteException,
			FinderException {
		ITable testFindByShortName = dataSetTest
				.getTable("testFindByShortName");
		PartyProfile profile = partyProfile.findByShortName(null);
		assertNotNull(profile);
	   }

	public void testFindByDealAndType() throws RemoteException,
			FinderException, DataSetException {
		ITable testFindByDealAndType = dataSetTest
				.getTable("testFindByDealAndType");
		int id = Integer.parseInt((String) testFindByDealAndType.getValue(0,
				"id"));
		int copyId = Integer.parseInt((String) testFindByDealAndType.getValue(
				0, "copyId"));
		int typeId = Integer.parseInt((String) testFindByDealAndType.getValue(
				0, "typeId"));
		DealPK pk = new DealPK(id, copyId);
		Collection<PartyProfile> list = partyProfile.findByDealAndType(pk,
				typeId);
		assertTrue(list.size() > 0);
		for (Object obj : list) {
			PartyProfile profile = (PartyProfile) obj;
			assertEquals(id, profile.dealId);
		}

	}

	public void testFindByDealAndTypeFailure() throws RemoteException,
			FinderException, DatabaseUnitException {
		ITable testFindByDealAndTypeFailure = dataSetTest
				.getTable("testFindByDealAndTypeFailure");
		int id = Integer.parseInt((String) testFindByDealAndTypeFailure
				.getValue(0, "id"));
		int copyId = Integer.parseInt((String) testFindByDealAndTypeFailure
				.getValue(0, "copyId"));
		int typeId = Integer.parseInt((String) testFindByDealAndTypeFailure
				.getValue(0, "typeId"));
		DealPK pk = new DealPK(id, copyId);
		Collection<PartyProfile> list = partyProfile.findByDealAndType(pk,
				typeId);
		assertTrue(list.size() <= 0);
	}

	public void testFindByDealAndType1() throws RemoteException,
			FinderException, NumberFormatException, DataSetException {
		ITable testFindByDealAndType1 = dataSetTest
				.getTable("testFindByDealAndType1");
		int id = Integer.parseInt((String) testFindByDealAndType1.getValue(0,
				"id"));
		int typeId = Integer.parseInt((String) testFindByDealAndType1.getValue(
				0, "typeId"));
		Collection<PartyProfile> list = partyProfile.findByDealAndType(id,
				typeId);
		assertTrue(list.size() > 0);
		for (Object obj : list) {
			PartyProfile profile = (PartyProfile) obj;
			assertEquals(id, profile.dealId);
		}
	}

	public void testFindByDealAndTypeFailure1() throws RemoteException,
			FinderException, DataSetException {
		ITable testFindByDealAndTypeFailure1 = dataSetTest
				.getTable("testFindByDealAndTypeFailure1");
		int id = Integer.parseInt((String) testFindByDealAndTypeFailure1
				.getValue(0, "id"));
		int typeId = Integer.parseInt((String) testFindByDealAndTypeFailure1
				.getValue(0, "typeId"));
		Collection<PartyProfile> list = partyProfile.findByDealAndType(id,
				typeId);
		assertTrue(list.size() <= 0);
	}

	public void testFindByNameAndTypeId() throws RemoteException,
			FinderException, DataSetException {
		ITable testFindByNameAndTypeId = dataSetTest
				.getTable("testFindByNameAndTypeId");
		String pName = (String) testFindByNameAndTypeId.getValue(0, "pName");
		int pTypeId = Integer.parseInt((String) testFindByNameAndTypeId
				.getValue(0, "pTypeId"));

		Collection list = partyProfile.findByNameAndTypeId(pName, pTypeId);
		assertTrue(list.size() > 0);
		for (Object obj : list) {
			PartyProfile profile = (PartyProfile) obj;
			assertEquals(pTypeId, profile.partyTypeId);
		}
	}

	public void testFindByNameAndTypeIdFailure() throws RemoteException,
			FinderException, DataSetException {
		ITable testFindByNameAndTypeIdFailure = dataSetTest
				.getTable("testFindByNameAndTypeIdFailure");
		String pName = (String) testFindByNameAndTypeIdFailure.getValue(0,
				"pName");
		int pTypeId = Integer.parseInt((String) testFindByNameAndTypeIdFailure
				.getValue(0, "pTypeId"));
		Collection list = partyProfile.findByNameAndTypeId(pName, pTypeId);
		assertTrue(list.size() <= 0);
	}

	public void testFindByNameCompanyNameAndTypeId() throws RemoteException,
			FinderException, DataSetException {
		ITable testFindByNameCompanyNameAndTypeId = dataSetTest
				.getTable("testFindByNameCompanyNameAndTypeId");
		String pName = (String) testFindByNameCompanyNameAndTypeId.getValue(0,
				"pName");
		String pCompanyName = (String) testFindByNameCompanyNameAndTypeId
				.getValue(0, "pCompanyName");
		int pTypeId = Integer
				.parseInt((String) testFindByNameCompanyNameAndTypeId.getValue(
						0, "pTypeId"));
		Collection list = partyProfile.findByNameCompanyNameAndTypeId(pName,
				pCompanyName, pTypeId);
		assertTrue(list.size() > 0);
		for (Object obj : list) {
			PartyProfile profile = (PartyProfile) obj;
			assertEquals(pTypeId, profile.partyTypeId);
		}
	}

	public void testFindByNameCompanyNameAndTypeIdFailure()
			throws RemoteException, FinderException, DataSetException {
		ITable testFindByNameCompanyNameAndTypeIdFailure = dataSetTest
				.getTable("testFindByNameCompanyNameAndTypeIdFailure");
		String pName = (String) testFindByNameCompanyNameAndTypeIdFailure
				.getValue(0, "pName");
		String pCompanyName = (String) testFindByNameCompanyNameAndTypeIdFailure
				.getValue(0, "pCompanyName");
		int pTypeId = Integer
				.parseInt((String) testFindByNameCompanyNameAndTypeIdFailure
						.getValue(0, "pTypeId"));
		Collection list = partyProfile.findByNameCompanyNameAndTypeId(pName,
				pCompanyName, pTypeId);
		assertTrue(list.size() <= 0);
	}

	public void testFindByPropertyDealAndType() throws RemoteException,
			FinderException, DataSetException {
		ITable testFindByPropertyDealAndType = dataSetTest
				.getTable("testFindByPropertyDealAndType");
		Property property = new Property(srk, CalcMonitor.getMonitor(srk));
		int dealId = Integer.parseInt((String) testFindByPropertyDealAndType
				.getValue(0, "dealId"));
		int propertyId = Integer
				.parseInt((String) testFindByPropertyDealAndType.getValue(0,
						"propertyId"));
		int typeId = Integer.parseInt((String) testFindByPropertyDealAndType
				.getValue(0, "typeId"));
		property.setDealId(dealId);
		property.propertyId = propertyId;
		Collection list = partyProfile.findByPropertyDealAndType(property,
				typeId);
		assertTrue(list.size() > 0);
		for (Object obj : list) {
			PartyProfile profile = (PartyProfile) obj;
			assertEquals(typeId, profile.partyTypeId);
		}
	}

	public void testFindByPropertyDealAndTypeFailure() throws RemoteException,
			FinderException, DataSetException {
		ITable testFindByPropertyDealAndTypeFailure = dataSetTest
				.getTable("testFindByPropertyDealAndTypeFailure");

		int dealId = Integer
				.parseInt((String) testFindByPropertyDealAndTypeFailure
						.getValue(0, "dealId"));
		int propertyId = Integer
				.parseInt((String) testFindByPropertyDealAndTypeFailure
						.getValue(0, "propertyId"));
		int typeId = Integer
				.parseInt((String) testFindByPropertyDealAndTypeFailure
						.getValue(0, "typeId"));

		Property property = new Property(srk, CalcMonitor.getMonitor(srk));
		property.setDealId(dealId);
		property.propertyId = propertyId;
		Collection list = partyProfile.findByPropertyDealAndType(property,
				typeId);
		assertTrue(list.size() <= 0);

	}

	public void testFindByDupeCheckCriteria() throws RemoteException,
			FinderException, DataSetException {
		ITable testFindByDupeCheckCriteria = dataSetTest
				.getTable("testFindByDupeCheckCriteria");
		String firstName = (String) testFindByDupeCheckCriteria.getValue(0,
				"firstName");
		String lastName = (String) testFindByDupeCheckCriteria.getValue(0,
				"lastName");
		int partyTypeId = Integer.parseInt((String) testFindByDupeCheckCriteria
				.getValue(0, "partyTypeId"));
		Collection list = partyProfile.findByDupeCheckCriteria(firstName,
				lastName, partyTypeId);
		assertTrue(list.size() > 0);
		for (Object obj : list) {
			PartyProfile profile = (PartyProfile) obj;
			assertEquals(partyTypeId, profile.partyTypeId);
		}
	}

	public void testFindByDupeCheckCriteriaFailure() throws RemoteException,
			FinderException, DataSetException {
		ITable testFindByDupeCheckCriteriaFailure = dataSetTest
				.getTable("testFindByDupeCheckCriteriaFailure");
		String firstName = (String) testFindByDupeCheckCriteriaFailure
				.getValue(0, "firstName");
		String lastName = (String) testFindByDupeCheckCriteriaFailure.getValue(
				0, "lastName");
		int partyTypeId = Integer
				.parseInt((String) testFindByDupeCheckCriteriaFailure.getValue(
						0, "partyTypeId"));
		Collection list = partyProfile.findByDupeCheckCriteria(firstName,
				lastName, partyTypeId);
		assertTrue(list.size() <= 0);
	}

	
	 
	
	public void testAssociate() throws Exception,
			RemoteException {
		ITable testFindByDupeCheckCriteriaFailure = dataSetTest
		.getTable("testAssociate");
		int partyProfileId = Integer.parseInt((String) testFindByDupeCheckCriteriaFailure
		.getValue(0, "partyProfileId"));
		
		int dealId = Integer.parseInt((String) testFindByDupeCheckCriteriaFailure
		.getValue(0, "dealId"));
		
		int propertyId = Integer.parseInt((String) testFindByDupeCheckCriteriaFailure
		.getValue(0, "propertyId"));
		
		srk.beginTransaction();
		partyProfile.setPartyProfileId(partyProfileId);
		srk.getExpressState().setDealInstitutionId(0);
		partyProfile.associate(dealId, propertyId);
		srk.rollbackTransaction();
	}
	
	

	public void testDisassociate() throws JdbcTransactionException,
			RemoteException, DataSetException {

		ITable testFindByDupeCheckCriteriaFailure = dataSetTest
		.getTable("testAssociate");
		int partyProfileId = Integer.parseInt((String) testFindByDupeCheckCriteriaFailure
		.getValue(0, "partyProfileId"));
		
		int dealId = Integer.parseInt((String) testFindByDupeCheckCriteriaFailure
		.getValue(0, "dealId"));
		
		int propertyId = Integer.parseInt((String) testFindByDupeCheckCriteriaFailure
		.getValue(0, "propertyId"));
		
		srk.beginTransaction();
		partyProfile.setPartyProfileId(partyProfileId);
		srk.getExpressState().setDealInstitutionId(0);
		partyProfile.disassociate(dealId, propertyId);
		srk.rollbackTransaction();
		}

}
