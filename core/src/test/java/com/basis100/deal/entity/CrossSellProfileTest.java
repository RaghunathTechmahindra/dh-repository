package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.CrossSellProfilePK;
import com.basis100.deal.pk.DcFolderPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;

public class CrossSellProfileTest extends FXDBTestCase {

	private String INIT_XML_DATA = "CrossSellProfileDataSet.xml";
	private CrossSellProfile crossSellProfile;
	private IDataSet dataSetTest;

	public CrossSellProfileTest(String name) throws DataSetException,
			IOException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"CrossSellProfileDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		// TODO Auto-generated method stub
		return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
				crossSellProfile = new CrossSellProfile(srk,0);

	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

/*	public void testCreate() throws NumberFormatException, DataSetException,
			RemoteException, CreateException, JdbcTransactionException {
		//srk.beginTransaction();
		int crossSellProfileId = Integer.parseInt(dataSetTest
				.getTable("testCreate").getValue(0, "CrossSellProfileId")
				.toString());
		srk.getExpressState().setDealInstitutionId(1);
		CrossSellProfilePK pk = new CrossSellProfilePK(crossSellProfileId);
		crossSellProfile.create(pk);
		assertTrue(srk.getModified());
		//srk.rollbackTransaction();
	}*/
	/*public void testFindByPrimaryKey() throws RemoteException, FinderException, NumberFormatException, DataSetException {
		ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKey");
		int crossSellProfileId = Integer.parseInt((String)testFindByPrimaryKey.getValue(0,"crossSellProfileId"));
		CrossSellProfilePK pk = new CrossSellProfilePK(crossSellProfileId);
		crossSellProfile= crossSellProfile.findByPrimaryKey(pk);
		assertEquals(crossSellProfileId, pk.getId());
	}*/
	
	public void testFindByPrimaryKeyFailure() throws RemoteException, FinderException, NumberFormatException, DataSetException {
		ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKeyFailure");
		boolean status=false;
		try{
		int crossSellProfileId = Integer.parseInt((String)testFindByPrimaryKey.getValue(0,"crossSellProfileId"));
		CrossSellProfilePK pk = new CrossSellProfilePK(crossSellProfileId);
		crossSellProfile= crossSellProfile.findByPrimaryKey(pk);
		status=true;
		}catch(Exception e){
			status=false;
		}
		assertEquals(false, status);
	}

}
