package com.basis100.deal.entity;

import java.io.IOException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.ConditionHandler;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.docprep.extract.doctag.StatementDisclosure;
import com.basis100.resources.SessionResourceKit;

public class StatementDisclosureTest extends FXDBTestCase {
	private IDataSet dataSetTest;
	private StatementDisclosure statementDisclosure = null;
	private ConditionHandler ch = null;
    FMLQueryResult fmlQueryResult =null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public StatementDisclosureTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(StatementDisclosure.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	

	@Override
	protected DatabaseOperation getSetUpOperation() throws Exception
	    {
		statementDisclosure = new StatementDisclosure();
	    	return DatabaseOperation.INSERT;
	    }
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	/*@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.solicitorsPackEmployerPhone = new SolicitorsPackEmployerPhone();
	}*/
	
	public void testExtract() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testExtract");
		
		int institutionProfileId = Integer.parseInt((String)testExtract.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String)testExtract.getValue(0, "dealid"));
		int copyid = Integer.parseInt((String)testExtract.getValue(0, "copyid"));
		int conditionId = Integer.parseInt((String)testExtract.getValue(0, "CONDITIONID"));
	
		Deal deal = new Deal(srk, null, id, copyid);
		com.basis100.deal.entity.DealEntity de = (com.basis100.deal.entity.DealEntity )deal;
		deal.setInstitutionProfileId(institutionProfileId);
		
		ch = new ConditionHandler(srk);
		
		int lang = 0;
		fmlQueryResult = statementDisclosure.extract(de, lang, null, srk);
		
		//assertTrue(ch.getVariableVerbiage(deal, conditionId, 1)!=null);
		assertNotNull(fmlQueryResult);
		assert fmlQueryResult.getValues().size()>0;
		
		srk.rollbackTransaction();
	}
	
}
