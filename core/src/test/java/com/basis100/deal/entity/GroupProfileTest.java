/*
 * @(#) GroupProfileTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */


package com.basis100.deal.entity;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.GroupProfilePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>GroupProfileTest</p>
 * Express Entity class unit test: GroupProfile
 */
public class GroupProfileTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(GroupProfileTest.class);

    // the session resource kit.

    /**
     * Constructor function
     */
    public GroupProfileTest() {
    }

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int groupProfileId = _dataRepository.getInt("GROUPPROFILE", "GROUPPROFILEID", 0);
        // get input data from repository
        int instProfId = _dataRepository.getInt("GROUPPROFILE", "INSTITUTIONPROFILEID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed the input data and run the program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        GroupProfile entity = new GroupProfile(srk);
        entity = entity.findByPrimaryKey(new GroupProfilePK(groupProfileId));

        // verify the running result.
        assertEquals(groupProfileId, entity.getGroupProfileId());

        // free resources
        srk.freeResources();
    }

    /**
     * test create.
     */
    @Test
    public void testCreate() throws Exception {

        // get input data from repository
        int instProfId = _dataRepository.getInt("GROUPPROFILE", "INSTITUTIONPROFILEID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();

        // feed input data and run program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        srk.beginTransaction();
        GroupProfile newEntity = new GroupProfile(srk);
        newEntity = newEntity.create();
        newEntity.ejbStore();

        // check create correctlly.
        GroupProfile foundEntity = new GroupProfile(srk);
        foundEntity = foundEntity.findByPrimaryKey(new GroupProfilePK(newEntity
                .getGroupProfileId()));

        // verifying
        assertEquals(foundEntity.getGroupName(), newEntity.getGroupName());

        // rollback transaction.
        srk.rollbackTransaction();
        // free resources.
        srk.freeResources();
    }

}
