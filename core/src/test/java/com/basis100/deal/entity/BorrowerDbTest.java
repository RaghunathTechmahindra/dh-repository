package com.basis100.deal.entity;

import java.io.IOException;
import java.util.List;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PropertyPK;


/**
 * <p>BorrowerDbTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class BorrowerDbTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private Borrower borrower;
	
	public BorrowerDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(Borrower.class.getSimpleName() + "DataSetTest.xml"));
	}

	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.borrower = new Borrower(srk, null, 2282, 1);
		
	}
	
	public void testFindByIncome() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testFindByIncome");
    	int id = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "BORROWERID"));
    	Income income = new Income(srk, null);
    	income.setBorrowerId(id);
    	income.copyId = 1;
    	borrower.findByIncome(income);
	    assertNotNull(borrower);
    }
	
	public void testFindByGenXDupeCheck() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testFindByGenXDupeCheck");
    	String borrowerFirstName = (String)(expectedUserProfile.getValue(0, "BORROWERFIRSTNAME"));
    	String borrowerMiddleInitial = (String)(expectedUserProfile.getValue(0, "BORROWERMIDDLEINITIAL"));
    	String borrowerLastName = (String)(expectedUserProfile.getValue(0, "BORROWERLASTNAME"));
    	//Date borrowerBirthDate = (Date)(expectedUserProfile.getValue(0,"BORROWERBIRTHDATE"));
    	//Date minApplicationDate = (Date)(expectedUserProfile.getValue(0,"DATEOFCREDITBUREAUPROFILE"));
    	
    	borrower.findByGenXDupeCheck(borrowerFirstName, borrowerMiddleInitial, borrowerLastName, null,null);
	    assertNotNull(borrower);
    }
	
	public void testFindByDupeCheckCriteria() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testFindByDupeCheckCriteria");
    	String borrowerFirstName = (String)(expectedUserProfile.getValue(0, "BORROWERFIRSTNAME"));
    	String borrowerMiddleInitial = (String)(expectedUserProfile.getValue(0, "BORROWERMIDDLEINITIAL"));
    	String borrowerLastName = (String)(expectedUserProfile.getValue(0, "BORROWERLASTNAME"));
    	//Date borrowerBirthDate = (Date)(expectedUserProfile.getValue(0,"BORROWERBIRTHDATE"));
    	//Date minApplicationDate = (Date)(expectedUserProfile.getValue(0,"DATEOFCREDITBUREAUPROFILE"));
    	
    	borrower.findByDupeCheckCriteria(borrowerFirstName, borrowerMiddleInitial, borrowerLastName, null, null);
	    assertNotNull(borrower);
    }
	
	public void testFindByDeal() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testFindByDeal");
    	int id = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "COPYID"));
    	
    	borrower.findByDeal(new DealPK(id, copyId));
	    assertNotNull(borrower);
    }
	
	public void testFindByDealAndCurrentRequest() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testFindByDealAndCurrentRequest");
    	int id = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "COPYID"));
    	
    	borrower.findByDealAndCurrentRequest(new DealPK(id, copyId));
	    assertNotNull(borrower);
    }
	
	public void testFindByDealAndType() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testFindByDealAndType");
    	int borrowerTypeId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "BORROWERTYPEID"));
    	int id = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "COPYID"));
    	
    	borrower.findByDealAndType(new DealPK(id, copyId), borrowerTypeId);
	    assertNotNull(borrower);
    }
	public void testFindBySocialInsuranceNumber() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testFindBySocialInsuranceNumber");
    	String sin = (String)(expectedUserProfile.getValue(0, "SOCIALINSURANCENUMBER"));
    	
    	borrower.findBySocialInsuranceNumber(sin);
	    assertNotNull(borrower);
    }
	
	public void testFindByGenXDupeCheckSin() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testFindByGenXDupeCheckSin");
    	//int responseId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "RESPONSEID"));
    	String sin = (String)(expectedUserProfile.getValue(0, "SOCIALINSURANCENUMBER"));
    	//Date minDate = (Date)(expectedUserProfile.getValue(0,"DATEOFCREDITBUREAUPROFILE"));
    	
    	borrower.findByGenXDupeCheckSin(sin, null);
	    assertNotNull(borrower);
    }
	
	public void testFindByDupeCheckSin() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testFindByDupeCheckSin");
    	String sin = (String)(expectedUserProfile.getValue(0, "SOCIALINSURANCENUMBER"));
    	//Date minDate = (Date)(expectedUserProfile.getValue(0,"DATEOFCREDITBUREAUPROFILE"));
    	
    	borrower.findByDupeCheckSin(sin, null);
	    assertNotNull(borrower);
    }
	
	public void testFindByAllOnDeal() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testFindByAllOnDeal");
    	int dealId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "COPYID"));
    	
    	borrower.findByAllOnDeal(new DealPK(dealId, copyId));
	    assertNotNull(borrower);
    }
	
	public void testFindByPrimaryBorrower() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testFindByPrimaryBorrower");
    	int dealId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "COPYID"));
    	
    	borrower.findByPrimaryBorrower(dealId, copyId);
	    assertNotNull(borrower);
    }
	
	public void testFindByPrimaryBorrowerWithInstitutionId() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testFindByPrimaryBorrowerWithInstitutionId");
    	int institutionId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "INSTITUTIONPROFILEID"));
    	int dealId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "COPYID"));
    	
    	borrower.findByPrimaryBorrowerWithInstitutionId(dealId, copyId, institutionId);
	    assertNotNull(borrower);
    }
	
	public void testFindByExistingClient() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testFindByExistingClient");
    	int dealId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "COPYID"));
    	
    	borrower.findByExistingClient(dealId, copyId);
	    assertNotNull(borrower);
    }
	
	/*public void testCreate() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testCreate");
    	srk.beginTransaction();
    	int dealId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "COPYID"));
    	
    	borrower.create(new DealPK(dealId, copyId));
	    assertNotNull(borrower);
	    srk.rollbackTransaction();
    }*/
	
	public void testFindByMyCopies() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testFindByMyCopies");
    	
    	borrower.findByMyCopies();
	    assertNotNull(borrower);
    }
	
	public void testFindByAsset() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testFindByAsset");
    	int id = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "BORROWERID"));
    	Asset a = new Asset(srk, null);
    	a.setBorrowerId(id);
    	a.copyId = 1;
    	borrower.findByAsset(a);
	    assertNotNull(borrower);
    }
	public void testFindByLiability() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testFindByLiability");
    	int id = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "BORROWERID"));
    	Liability li = new Liability(srk, null);
    	li.setBorrowerId(id);
    	li.copyId = 1;
    	borrower.findByLiability(li);
	    assertNotNull(borrower);
    }
	
	public void testFindByFirstBorrower() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testFindByFirstBorrower");
    	int dealId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "COPYID"));
    	
    	borrower.findByFirstBorrower(dealId, copyId);
	    assertNotNull(borrower);
    }
	public void testFindByFirstBorrowerFailure() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testFindByFirstBorrowerFailure");
    	boolean status=false;
    	try{
    	int dealId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "COPYID"));
    	borrower.findByFirstBorrower(dealId, copyId);
    	status=true;
    	}catch(Exception e){
    		status=false;
    	}
	    assertEquals(false, status);
    }
	 public void testFindByPrimaryKey() throws Exception{		
			ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByPrimaryKey");
			int borrowerId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"borrowerId"));	
			int copyId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"copyId"));
			srk.getExpressState().setDealInstitutionId(1);		
			BorrowerPK  pk = new BorrowerPK(borrowerId,copyId);
			borrower=borrower.findByPrimaryKey(pk);		
			assertEquals(borrowerId, pk.getId());		
		}   
	 public void testFindByPrimaryKeyFailure() throws Exception{		
			ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByPrimaryKeyFailure");
			boolean status=false;
			try{
			int borrowerId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"borrowerId"));	
			int copyId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"copyId"));
			srk.getExpressState().setDealInstitutionId(1);		
			BorrowerPK  pk = new BorrowerPK(borrowerId,copyId);
			borrower=borrower.findByPrimaryKey(pk);		
			status=true;
			}catch(Exception e ){
				status = false;		
			}
			assertEquals(false, status);
		}   
	 public void testGetChildren() throws Exception{	
		  List l = null;
		  l=borrower.getChildren();
		  assertNotNull(l);
		} 
}