package com.basis100.deal.entity;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.pk.ResponsePK;


/**
 * <p>ResponseDbTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class ResponseDbTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private Response response;
	
	public ResponseDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(Response.class.getSimpleName() + "DataSetTest.xml"));
	}
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.response = new Response(srk);
		
	}
	
	public void testCreatePrimaryKey() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreatePrimaryKey");
    	int responseId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "RESPONSEID"));
    	
    	response.createPrimaryKey();
    	assertNotNull(response);
    	//assertEquals(responseId, response.getResponseId());
    }
	
	public void testCreate() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreate");
    	String rDate = (String)(expected.getValue(0, "RESPONSEDATE"));
    	
    	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy"); 
		Date responseDate = dateFormat.parse(rDate);
    	
    	int requestId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "REQUESTID"));
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	String sDate = (String)(expected.getValue(0, "STATUSDATE"));
    	
    	SimpleDateFormat dateFormatS = new SimpleDateFormat("dd-MMM-yyyy"); 
		Date statusDate = dateFormatS.parse(sDate);
    	
    	int responseStatusId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "RESPONSESTATUSID"));
    	srk.beginTransaction();
    	response.create(responseDate, requestId, new DealPK(id,copyId), statusDate, responseStatusId);
	    srk.rollbackTransaction();
    	assertNotNull(response);
    }
	
	public void testCreate1() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreate1");
    	int responseId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "RESPONSEID"));
    	String rDate = (String)(expected.getValue(0, "RESPONSEDATE"));
    	
    	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy"); 
		Date responseDate = dateFormat.parse(rDate);
    	
    	int requestId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "REQUESTID"));
    	int dealId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DEALID"));
    	String sDate = (String)(expected.getValue(0, "STATUSDATE"));
    	
    	SimpleDateFormat dateFormatS = new SimpleDateFormat("dd-MMM-yyyy"); 
		Date statusDate = dateFormatS.parse(sDate);
    	int responseStatusId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "RESPONSESTATUSID"));
    	srk.beginTransaction();
    	response.create(new ResponsePK(responseId), responseDate, requestId, dealId, statusDate, responseStatusId);
	    srk.rollbackTransaction();
    	assertNotNull(response);
    }
	
	/*public void testFindByRequest() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByRequest");
    	int requestId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "REQUESTID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	
    	response.findByRequest(new RequestPK(requestId,copyId));
	    assertNotNull(response);
    }*/
	
	public void testClone() throws Exception {
    	ITable expected = dataSetTest.getTable("testClone");
    	int responseId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "RESPONSEID"));
    	
    	response.clone();
	    assertNotNull(response);
    }
	
	/*public void testRemoveSons() throws Exception {
    	ITable expected = dataSetTest.getTable("testRemoveSons");
    	int responseId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "RESPONSEID"));
    	
    	response.removeSons();
	    assertNotNull(response);
    }*/
	
	public void testFindChannelTransactionData() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindChannelTransactionData");
    	String channelTransactionKey = (String)(expected.getValue(0, "CHANNELTRANSACTIONKEY"));
    	int dealId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DEALID"));
    	int serviceProductId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "SERVICEPRODUCTID"));
    	
    	response.findChannelTransactionData(channelTransactionKey, dealId, serviceProductId);
	    assertNotNull(response);
    }
	
}