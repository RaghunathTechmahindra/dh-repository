package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.pk.SAMRequestPK;


/**
 * <p>SAMRequestDbTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class SAMRequestDbTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private SAMRequest sAMRequest;
	
	public SAMRequestDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(SAMRequest.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.sAMRequest = new SAMRequest(srk);
		
	}
	
	/*public void testCreate() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreate");
    	int requestId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "REQUESTID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	srk.beginTransaction();
    	sAMRequest.create(new RequestPK(requestId,copyId));
    	srk.rollbackTransaction();
	    assertNotNull(sAMRequest);
    }
	*/
	public void testFindByPrimaryKey() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByPrimaryKey");
    	int requestId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "REQUESTID"));
    	
    	sAMRequest.findByPrimaryKey(new SAMRequestPK(requestId));
	    assertNotNull(sAMRequest);
    }
	/*
	public void testClone() throws Exception {
    	ITable expected = dataSetTest.getTable("testClone");
    	//int requestId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "REQUESTID"));
    	
    	sAMRequest.clone();
	    assertNotNull(sAMRequest);
    }
	*/
	public void testClone1() throws Exception {
    	ITable expected = dataSetTest.getTable("testClone1");
    	int requestId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "REQUESTID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	
    	sAMRequest.clone(new RequestPK(requestId,copyId));
	    assertNotNull(sAMRequest);
    }
	
}