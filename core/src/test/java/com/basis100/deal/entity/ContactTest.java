/*
 * @(#)ContactTest.java    2007-9-05
 *
 */

package com.basis100.deal.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.deal.pk.AddrPK;
import com.basis100.deal.pk.ContactPK;
import com.basis100.deal.pk.EmploymentHistoryPK;

import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * ContactTest -
 * 
 * @version 1.0 2007-9-05
 * @author <A HREF="mailto:chyxiang@yahoo.com">Chen Xiang (Sean)</A>
 */
public class ContactTest extends ExpressEntityTestCase implements 
         UnitTestLogging {

    private Contact _contactDAO;

    // The logger
    private static Logger _log = LoggerFactory.getLogger(ContactTest.class);

    // the Session Resource Kit
    private SessionResourceKit _srk;
    
    // the institutionId for deal
    private int dealInstitutionId;

    @Before
    public void setUp() throws Exception {
        
        // initiazile
        ResourceManager.init();
        _srk = new SessionResourceKit("Testing");
        
        _contactDAO = new Contact(_srk);
        
        // get value from database
        dealInstitutionId = _dataRepository.getInt("Contact",
                "InstitutionProfileId", 0);
    }

    @Test
    public void testFindByName() throws Exception {

        // get values from database
        String firstName = 
            _dataRepository
                .getString("Contact", "ContactFirstName", 0);
        String middleName = 
            _dataRepository.getString("Contact", "ContactMiddleName", 0);
        String lastName = 
            _dataRepository.getString("Contact", "ContactLastName", 0);

        // set vpd context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        
        // find a contact by name
        _contactDAO.findByName(firstName, middleName, lastName);
        
        // verify result
        assertNotNull(_contactDAO);
        assertEquals(lastName, _contactDAO.getContactLastName());
        
        _log.info("------------------------------");
        _log.info("Contact Found " + _contactDAO);
        _log.info("------------------------------");

    }

    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get values form database
        int contactID = _dataRepository.getInt("Contact",
                "ContactId", 0);
        int copyID = _dataRepository.getInt("Contact",
                "CopyId", 0);
        String firstName = _dataRepository.getString("Contact",
                "ContactFirstName", 0);

        // set vpd context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // find a contact by primary key
        ContactPK contactPK = new ContactPK(contactID, copyID);
        _contactDAO.findByPrimaryKey(contactPK);
        
        // verify result
        assertNotNull(_contactDAO);
        assertEquals(firstName, _contactDAO.getContactFirstName());
        _log.info("------------------------------");
        _log.info("Contact Found " + _contactDAO);
        _log.info("------------------------------");

    }

    /*
     * create with addr PK
     */
    @Test
    public void testCreateAddrPK() throws Exception {

        _log.info(BORDER_START, "Create");
        // set vpd context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // get result form database
        int addrID = 
            _dataRepository.getInt("Addr", "AddrId", 0);
        int copyID = 
            _dataRepository.getInt("Addr", "CopyId", 0);

        // create a contact
        AddrPK addrPK = new AddrPK(addrID, copyID);
        _srk.beginTransaction();
        _contactDAO.create(addrPK);
        
        // verify the result
        assertNotNull(_contactDAO);
        assertEquals(addrID, _contactDAO.getAddrId());
        _log.info(BORDER_END, "Create");
        _srk.rollbackTransaction();

    }

    /*
     * create employment history, this covers create(), createPrimaryKey()
     */
    @Test
    public void testCreateEmploymentHistory() throws Exception {
        
        _log.info(BORDER_START, "Create");
        // set vpd context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // get employmenthistory form database
        int historyId = 
            _dataRepository.getInt("EmploymentHistory", "EmploymentHistoryId", 0);
        int copyId = 
            _dataRepository.getInt("EmploymentHistory", "CopyId", 0);

        EmploymentHistoryPK epk = new EmploymentHistoryPK(historyId, copyId);
            
        _srk.beginTransaction();
        _contactDAO.create(epk);
        
        // verify the result
        assertNotNull(_contactDAO);
        assertEquals(copyId, _contactDAO.getCopyId());
        _log.info(BORDER_END, "Create");
        _srk.rollbackTransaction();

    }
    
    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        //free the srk
        _srk.freeResources();
    }
}
