/*
 * @(#)BncAdjudicationResponseTest.java Sep 21, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.BncAdjudicationResponsePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * @author amalhi
 * 
 * Junit for enitity BncAdjudicationResponse
 * 
 */
public class BncAdjudicationResponseTest extends ExpressEntityTestCase
        implements UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(BncAdjudicationResponseTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public BncAdjudicationResponseTest() {

    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.BncAdjudicationResponse#findByPrimaryKey(com.basis100.deal.pk.BncAdjudicationResponsePK)}.
     */
    @Ignore("no data for associated table")
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int responseId = _dataRepository.getInt("BncAdjudicationResponse",
                "responseId", 0);
        int instProfId = _dataRepository.getInt("BncAdjudicationResponse",
                "INSTITUTIONPROFILEID", 0);
        int recommendedCodeId = _dataRepository.getInt(
                "BncAdjudicationResponse", "RECOMMENDEDCODEID", 0);

        _logger.info(BORDER_START,
                "BncAdjudicationResponse - testFindByPrimaryKey");
        _logger.info("Got Values from new BncAdjudicationResponse ::: [{}]",
                responseId + "::" + instProfId + "::" + recommendedCodeId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        BncAdjudicationResponse _bncAdjudicationResponse = new BncAdjudicationResponse(
                _srk);

        // Find by primary key
        _bncAdjudicationResponse = _bncAdjudicationResponse
                .findByPrimaryKey(new BncAdjudicationResponsePK(responseId));

        _logger
                .info(
                        "Got Values from BncAdjudicationApplicantResponse found::: [{}]",
                        _bncAdjudicationResponse.getResponseId()
                                + "::"
                                + _bncAdjudicationResponse
                                        .getRecommendedCodeId());

        // Check if the data retrieved matches the DB
        assertEquals(_bncAdjudicationResponse.getResponseId(), responseId);
        assertEquals(_bncAdjudicationResponse.getRecommendedCodeId(),
                recommendedCodeId);

        _logger.info(BORDER_END,
                "BncAdjudicationResponse - testFindByPrimaryKey");
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.BncAdjudicationResponse#create(com.basis100.deal.pk.BncAdjudicationResponsePK, int)}.
     */
    @Ignore("no data for associated table")
    @Test
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create - BncAdjudicationResponse");

        // get input data from repository
        int instProfId = _dataRepository.getInt("BncAdjudicationResponse",
                "INSTITUTIONPROFILEID", 0);
        int responseId = _dataRepository.getInt("ADJUDICATIONRESPONSE",
                "responseId", 0);
        int recommendedCodeId = _dataRepository.getInt("RecommendCode",
                "RECOMMENDEDCODEID", 0);

        _logger.info("Got Values from new BncAdjudicationResponse ::: [{}]",
                instProfId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // begin transaction
        _srk.beginTransaction();

        // Create the initial entity
        BncAdjudicationResponse newEntity = new BncAdjudicationResponse(_srk);

        // call create method of entity
        newEntity = newEntity.create(new BncAdjudicationResponsePK(responseId),
                recommendedCodeId);

        _logger.info("CREATED BncAdjudicationResponse ::: [{}]", newEntity
                .getResponseId()
                + "::" + newEntity.getRecommendedCodeId());

        // check create correctly by calling findByPrimaryKey
        BncAdjudicationResponse foundEntity = new BncAdjudicationResponse(_srk);
        foundEntity = foundEntity
                .findByPrimaryKey(new BncAdjudicationResponsePK(newEntity
                        .getResponseId()));

        // verifying the created entity
        assertEquals(foundEntity.getResponseId(), newEntity.getResponseId());
        assertEquals(foundEntity.getRecommendedCodeId(), newEntity
                .getRecommendedCodeId());

        _logger.info(BORDER_END, "Create - BncAdjudicationResponse");

        // roll-back transaction.
        _srk.rollbackTransaction();
    }

}
