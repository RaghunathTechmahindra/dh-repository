package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.CreditReferencePK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.CreditBureauReportPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;

public class CreditBureauReportDbTest extends FXDBTestCase{
	CreditBureauReport creditBureauReport;
	IDataSet dataSetTest;

	public CreditBureauReportDbTest(String name) throws DataSetException,
			IOException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"CreditBureauReportDataSetTest.xml"));
	}

	

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		creditBureauReport = new CreditBureauReport(srk);

	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}
	@Test
    public void testfindByDealid() throws Exception{
    	// get values form database
		int dealId=Integer.parseInt(dataSetTest.getTable("testfindByDealid").getValue(0, "dealId").toString());
		int copyId=Integer.parseInt(dataSetTest.getTable("testfindByDealid").getValue(0, "copyId").toString());
        CreditBureauReport  newReport = new CreditBureauReport(srk);
        Collection reports = new Vector();
       reports= newReport.findByDealid(dealId);
       assertNotNull(reports);
       CreditBureauReport report= (CreditBureauReport) reports.iterator().next();
       assertEquals(dealId, report.getDealId());
    }
	@Test
    public void testfindByDealidFailure() throws Exception{
    	// get values form database
		int dealId=Integer.parseInt(dataSetTest.getTable("testfindByDealidFailure").getValue(0, "dealId").toString());
		int copyId=Integer.parseInt(dataSetTest.getTable("testfindByDealidFailure").getValue(0, "copyId").toString());
        CreditBureauReport  newReport = new CreditBureauReport(srk);
        Collection reports= newReport.findByDealid(dealId);
        assertSame(0, reports.size());
      }
@Test
public void testfindByDeal() throws Exception{
	int dealId=Integer.parseInt(dataSetTest.getTable("testfindByDeal").getValue(0, "dealId").toString());
	int copyId=Integer.parseInt(dataSetTest.getTable("testfindByDeal").getValue(0, "copyId").toString());
    CreditBureauReport  newReport = new CreditBureauReport(srk);
	DealPK pk=new DealPK(dealId, copyId);
	Collection Reports=newReport.findByDeal(pk);
	 assertNotNull(Reports);
	 CreditBureauReport report= (CreditBureauReport) Reports.iterator().next();
	 assertEquals(dealId, report.getDealId());
}
@Test
public void testfindByDealFailure() throws Exception{
	int dealId=Integer.parseInt(dataSetTest.getTable("testfindByDealFailure").getValue(0, "dealId").toString());
	int copyId=Integer.parseInt(dataSetTest.getTable("testfindByDealFailure").getValue(0, "copyId").toString());
    CreditBureauReport  newReport = new CreditBureauReport(srk);
	DealPK pk=new DealPK(dealId, copyId);
	Collection Reports=newReport.findByDeal(pk);
	
	assertSame(0, Reports.size());
	
	 
}
@Test
public void testCreate() throws RemoteException, FinderException, NumberFormatException, DataSetException, CreateException, JdbcTransactionException{
	srk.beginTransaction();
	int dealId=Integer.parseInt(dataSetTest.getTable("testCreate").getValue(0, "dealId").toString());
	int copyId=Integer.parseInt(dataSetTest.getTable("testCreate").getValue(0, "copyId").toString());
	  CreditBureauReport  newReport = new CreditBureauReport(srk);
	
	 srk.getExpressState().setDealInstitutionId(
				Integer.parseInt(dataSetTest.getTable("testCreate")
						.getValue(0, "dealInstitutionId").toString()));
	  CreditBureauReport report= newReport.create(new DealPK(dealId, copyId));
	 
	  assertNotNull(report);
	assertTrue(srk.getModified());
	srk.rollbackTransaction();
}
/*public void testFindByPrimaryKey() throws RemoteException, FinderException, NumberFormatException, DataSetException {
	ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKey");
	int creditBureauReportId = Integer.parseInt((String)testFindByPrimaryKey.getValue(0,"creditBureauReportId"));
	CreditBureauReportPK pk = new CreditBureauReportPK(creditBureauReportId);
	creditBureauReport= creditBureauReport.findByPrimaryKey(pk);
	assertEquals(creditBureauReportId, pk.getId());
}
*/
public void testFindByPrimaryKeyFailure() throws RemoteException, FinderException, NumberFormatException, DataSetException {
	ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKeyFailure");
	boolean status=false;
	try{
	int creditBureauReportId = Integer.parseInt((String)testFindByPrimaryKey.getValue(0,"creditBureauReportId"));
	CreditBureauReportPK pk = new CreditBureauReportPK(creditBureauReportId);
	creditBureauReport= creditBureauReport.findByPrimaryKey(pk);
	status=true;
	}catch(Exception e){
		status=false;
	}
	assertEquals(false, status);
}
}
