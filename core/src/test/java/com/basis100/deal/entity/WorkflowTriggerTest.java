package com.basis100.deal.entity;


import java.io.IOException;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;



public class WorkflowTriggerTest extends FXDBTestCase {
	

	private IDataSet dataSetTest;
    private WorkflowTrigger workflowTrigger=null;
    
	public WorkflowTriggerTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("WorkflowTriggerDataSetTest.xml"));
	}

	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		workflowTrigger=new WorkflowTrigger(srk,CalcMonitor.getMonitor(srk));
	}
	
	/*public void testFindByCompositeKey() throws Exception{
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByCompositeKey");
		String  dealId = findByDupeCheckCriteria.getValue(0,"dealId").toString();
		String  triggerKey = findByDupeCheckCriteria.getValue(0,"triggerKey").toString();
		WorkflowTrigger status=workflowTrigger.findByCompositeKey(Integer.parseInt(dealId),triggerKey);
		assertNotNull(status);
		
	}

	public void testFindByCompositeKeyFailure() throws Exception {
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByCompositeKeyFailure");
		boolean status = false;
		try {
			String dealId = findByDupeCheckCriteria.getValue(0, "dealId").toString();
			String triggerKey = findByDupeCheckCriteria.getValue(0,"triggerKey").toString();
			WorkflowTrigger statusWorkflowTrigger = workflowTrigger.findByCompositeKey(Integer.parseInt(dealId), triggerKey);
			status = true;
		} catch (Exception e) {
			status = false;
		}
		assertEquals(false, status);

	}
	
	public void testGetWorkflowTriggerValue() throws Exception{
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testGetWorkflowTriggerValue");
		String  dealId = findByDupeCheckCriteria.getValue(0,"dealId").toString();
		String  triggerKey = findByDupeCheckCriteria.getValue(0,"triggerKey").toString();
		String status=workflowTrigger.getWorkflowTriggerValue(Integer.parseInt(dealId), triggerKey);
		assertEquals("N", status);
	}
	
	
	public void testGetWorkflowTriggerValueFailure() throws Exception{
		
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testGetWorkflowTriggerValueFailure");
		String status = "";
		try {
			String dealId = findByDupeCheckCriteria.getValue(0, "dealId").toString();
			String triggerKey = findByDupeCheckCriteria.getValue(0,"triggerKey").toString();
			status = workflowTrigger.getWorkflowTriggerValue(Integer.parseInt(dealId), triggerKey);
			
		} catch (Exception e) {
			status = "";
		}
		assertEquals("", status);
		
	}
	
	public void testFindByCompositeKeyParems() throws Exception{
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByCompositeKeyParems");
		String  dealId = findByDupeCheckCriteria.getValue(0,"dealId").toString();
		String  triggerKey = findByDupeCheckCriteria.getValue(0,"triggerKey").toString();
		WorkflowTrigger status=workflowTrigger.findByCompositeKey(Integer.parseInt(dealId),triggerKey,"");
		String value=status.getValue();
		assertEquals("",value);
		
	}

	public void testFindByCompositeKeyParemsFailure() throws Exception {
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByCompositeKeyParemsFailure");
		boolean status = false;
		try {
			String dealId = findByDupeCheckCriteria.getValue(0, "dealId").toString();
			String triggerKey = findByDupeCheckCriteria.getValue(0,"triggerKey").toString();
			workflowTrigger.findByCompositeKey(Integer.parseInt(dealId), null,"");
			System.out.println("workflowTrigger:"+workflowTrigger);
			status = true;
		} catch (Exception e) {
			status = false;
		}
		assertEquals(false, status);

	}
	
	public void testFindByDealId() throws Exception{
		Collection cList=null;
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByDealId");
		int dealId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"dealId"));	
		int copyId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"copyId"));
		srk.getExpressState().setDealInstitutionId(1);
		Deal deal=new Deal(srk,null,dealId,copyId);
		cList=workflowTrigger.findByDealId(dealId);
		assert cList.size()!=0;
		assertEquals(dealId, deal.getDealId());		
	}

	
	public void testFindByDealIdFailure() throws Exception{		
		Collection cList=null;
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByDealIdFailure");
		boolean status = false;
		try{
		int dealId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"dealId"));	
		srk.getExpressState().setDealInstitutionId(1);
		cList=workflowTrigger.findByDealId(dealId);
		}catch(Exception e){
			status = false;
		}
		assertEquals(false, status);		
	}*/
	
	public void testCreate() throws Exception{	
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int dealId = Integer.parseInt((String)testCreate.getValue(0,"dealId"));	
		int copyId = Integer.parseInt((String)testCreate.getValue(0,"copyId"));
		String workflowtriggerkey = (String)testCreate.getValue(0,"workflowtriggerkey");
		String value = (String)testCreate.getValue(0,"value");		
		srk.getExpressState().setDealInstitutionId(0);
		workflowTrigger = workflowTrigger.create(dealId,workflowtriggerkey,value);		
		assert workflowTrigger.getDealId()!=0;
		srk.rollbackTransaction();
	}
}