package com.basis100.deal.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;

public class DBBorrowerIdentificationTest  extends FXDBTestCase{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(DBBridgeTest.class);
    
    //Initial data to be loaded before each test case.
	private final static String INIT_XML_DATA = "DBBorrowerIdentificationDataSet.xml";
	private IDataSet dataSetTest;
	
	/**
     * Constructor function
     */
    public DBBorrowerIdentificationTest(String name)  throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource("DBBorrowerIdentificationDataSetTest.xml"));
    }
    
    @Override
	protected IDataSet getDataSet() throws Exception {
    	return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}
    
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
        return DatabaseOperation.INSERT;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.DELETE;
    }
    
    /**
     * Test method for BorrowerIdentification#findByBorrower
     */
    @Test
    public void testFindByBorrower() throws Exception {

    	 _logger.info(" Start testFindByBorrower");
    	 
    	 ITable testFindByBorrower = dataSetTest.getTable("testFindByBorrower");
    	int  borrowerId = Integer.parseInt((String)testFindByBorrower.getValue(0,"borrowerId"));
     	int  copyId = Integer.parseInt((String)testFindByBorrower.getValue(0,"copyId"));
     	int  copyId1 = Integer.parseInt((String)testFindByBorrower.getValue(0,"copyId1"));
     	int  identificationId = Integer.parseInt((String)testFindByBorrower.getValue(0,"identificationId"));
  		int  instProfId = Integer.parseInt((String)testFindByBorrower.getValue(0,"instProfId"));
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
    	 
        // success case
        BorrowerIdentification borrowerIdent = new BorrowerIdentification(srk, null, identificationId, copyId);
        BorrowerPK borrowerPK = new BorrowerPK(borrowerId, copyId);
        
        List<BorrowerIdentification> borrowerIdentifications = (List<BorrowerIdentification>)borrowerIdent.findByBorrower(borrowerPK);
        
        assert borrowerIdentifications.size() > 0;
        
        // failure case
        BorrowerIdentification borrowerIdent1 = new BorrowerIdentification(srk, null);
        BorrowerPK borrowerPK1 = new BorrowerPK(borrowerId, copyId1);
        
        List<BorrowerIdentification> borrowerIdentifications1 = (List<BorrowerIdentification>)borrowerIdent1.findByBorrower(borrowerPK1);
        
        assert borrowerIdentifications1.size() == 0;
        
        _logger.info(" End testFindByBorrower");
    }

}
