package com.basis100.deal.entity;

import java.io.IOException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.util.collections.BorrowerNumBorrowerComparator;
import com.basis100.deal.util.collections.CurrentBorrowerAddressComparator;
import com.basis100.deal.util.collections.CurrentEmploymentComparator;
import com.basis100.resources.SessionResourceKit;

public class CurrentEmploymentComparatorTest extends FXDBTestCase {
	
	private IDataSet dataSetTest;
	private CurrentEmploymentComparator cec = null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public CurrentEmploymentComparatorTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(CurrentEmploymentComparator.class.getSimpleName()+"DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected DatabaseOperation getSetUpOperation() throws Exception
	    {
		cec = new CurrentEmploymentComparator();
	    	return DatabaseOperation.INSERT;
	    }

	public void testCompare() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCompare");
		
		int id1 = Integer.parseInt((String)testExtract.getValue(0, "EMPLOYMENTHISTORYID1"));
		int id2 = Integer.parseInt((String)testExtract.getValue(0, "EMPLOYMENTHISTORYID2"));
		int copyid = Integer.parseInt((String)testExtract.getValue(0, "copyid"));
		
		EmploymentHistory e1 = new EmploymentHistory(srk, null, id1, copyid);
		EmploymentHistory e2 = new EmploymentHistory(srk, null, id2, copyid);
		
		com.basis100.deal.entity.DealEntity o1 = (com.basis100.deal.entity.DealEntity )e1;
		com.basis100.deal.entity.DealEntity o2 = (com.basis100.deal.entity.DealEntity )e2;
		
		int result = cec.compare(o1, o2);
		
		assert result==0 || result==1 || result==-1;
		
		srk.rollbackTransaction();
	}

}
