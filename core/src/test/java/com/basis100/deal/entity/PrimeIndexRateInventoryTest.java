/*
 * @(#) PrimeIndexRateInventoryTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filogix.express.test.UnitTestLogging;
import com.filogix.express.test.ExpressEntityTestCase;

import com.basis100.deal.pk.PrimeIndexRateInventoryPK;
import com.basis100.deal.pk.PrimeIndexRateProfilePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

/**
 * @author amalhi
 * 
 * JUnit test for entity PrimeIndexRateInventory
 * 
 */
public class PrimeIndexRateInventoryTest extends ExpressEntityTestCase
        implements UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(PrimeIndexRateInventoryTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public PrimeIndexRateInventoryTest() {

    }

    /**
     * To be executed before test
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env and free resources
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int primeIndexRateInventoryId = _dataRepository.getInt(
                "PrimeIndexRateInventory", "primeIndexRateInventoryId", 0);

        int instProfId = _dataRepository.getInt("PrimeIndexRateInventory",
                "INSTITUTIONPROFILEID", 0);

        int primeIndexRateProfileId = _dataRepository.getInt(
                "PrimeIndexRateInventory", "primeIndexRateProfileId", 0);

        _logger.info(BORDER_START, "findByPrimaryKey");
        _logger.info(
                "Values from repository for PrimeIndexRateInventory ::: [{}]",
                primeIndexRateInventoryId + "::" + instProfId + "::"
                        + primeIndexRateProfileId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        PrimeIndexRateInventory _primeIndexRateInventory = new PrimeIndexRateInventory(
                _srk);

        // Find by primary key
        _primeIndexRateInventory = _primeIndexRateInventory
                .findByPrimaryKey(new PrimeIndexRateInventoryPK(
                        primeIndexRateInventoryId));

        _logger
                .info(
                        "Got Values from new Prime Index Rate Inventory ::: [{}]",
                        _primeIndexRateInventory.getPrimeIndexRateInventoryId()
                                + "::"
                                + _primeIndexRateInventory
                                        .getPrimeIndexRateProfileId());

        // Check if the data retrieved matches the DB
        assertTrue(_primeIndexRateInventory.getPrimeIndexRateInventoryId() == primeIndexRateInventoryId);
        assertTrue(_primeIndexRateInventory.getPrimeIndexRateProfileId() == primeIndexRateProfileId);

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * test create method of entity
     */
    @Ignore
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create");

        // get input data from repository
        int instProfId = _dataRepository.getInt("PrimeIndexRateInventory",
                "INSTITUTIONPROFILEID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // begin transaction
        _srk.beginTransaction();

        // Create the initial entity
        PrimeIndexRateInventory newEntity = new PrimeIndexRateInventory(_srk);

        // call create method of entity
        newEntity = newEntity.create(newEntity.createPrimaryKey());

        _logger.info("CREATED PRIME INDEX RATE INVENTORY ::: [{}]", newEntity
                .getPrimeIndexRateInventoryId()
                + "::" + newEntity.getPrimeIndexRateProfileId());

        // check create correctly by calling findByPrimaryKey
        PrimeIndexRateInventory foundEntity = new PrimeIndexRateInventory(_srk);
        foundEntity = foundEntity
                .findByPrimaryKey(new PrimeIndexRateInventoryPK(newEntity
                        .getPrimeIndexRateProfileId()));

        // verifying the created entity
        assertEquals(foundEntity.getPrimeIndexRateInventoryId(), newEntity
                .getPrimeIndexRateInventoryId());
        assertEquals(foundEntity.getPrimeIndexRateProfileId(), newEntity
                .getPrimeIndexRateProfileId());

        _logger.info(BORDER_END, "Create");

        // roll-back transaction.
        _srk.rollbackTransaction();

    }

    /**
     * test create method of entity
     */
    @Test
    public void testCreateWithArgs() throws Exception {

        _logger.info(BORDER_START, "Create");

        // get input data from repository
        int instProfId = _dataRepository.getInt("PrimeIndexRateInventory",
                "INSTITUTIONPROFILEID", 0);
        int primeIndexRateProfileId = _dataRepository.getInt(
                "PrimeIndexRateProfile", "primeIndexRateProfileId", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // begin transaction
        _srk.beginTransaction();

        // Create the initial entity
        PrimeIndexRateInventory newEntity = new PrimeIndexRateInventory(_srk);
        _logger.info("CREATED PRIME INDEX RATE INVENTORY - BEFORE  ::: [{}]",
                +primeIndexRateProfileId);

        // call create method of entity
        newEntity = newEntity.create(newEntity.createPrimaryKey(),
                new PrimeIndexRateProfilePK(primeIndexRateProfileId));

        _logger.info("CREATED PRIME INDEX RATE INVENTORY ::: [{}]", newEntity
                .getPrimeIndexRateInventoryId()
                + "::" + newEntity.getPrimeIndexRateProfileId());

        // check create correctly by calling findByPrimaryKey
        PrimeIndexRateInventory foundEntity = new PrimeIndexRateInventory(_srk);
        foundEntity = foundEntity
                .findByPrimaryKey(new PrimeIndexRateInventoryPK(newEntity
                        .getPrimeIndexRateInventoryId()));

        // verifying the created entity
        assertEquals(foundEntity.getPrimeIndexRateInventoryId(), newEntity
                .getPrimeIndexRateInventoryId());
        assertEquals(foundEntity.getPrimeIndexRateProfileId(),
                primeIndexRateProfileId);

        _logger.info(BORDER_END, "Create");

        // roll-back transaction.
        _srk.rollbackTransaction();

    }
}
