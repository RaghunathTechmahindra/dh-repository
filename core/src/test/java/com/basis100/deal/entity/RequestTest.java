/**
 * <p>Title: RequestTest.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2007</p>
 *
 * <p>Company: Filogix Limited Partnership
</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � Aug 29, 2007)
 *
 */
package com.basis100.deal.entity;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>ProvinceTest</p>
 * Express Entity class unit test: Province
 */
public class RequestTest extends ExpressEntityTestCase 
    implements UnitTestLogging{/*

    private static final Logger _logger = 
        LoggerFactory.getLogger(RequestTest.class);
    private static final String ENTITY_NAME = "REQUEST"; 
    
    // retreived data count that is stored in UnitTestDataRepository.xml
    private static final int DATA_COUNT = 200;
    
    private SessionResourceKit _srk;
    
    private int requestId;
    private Date requestDate;
    private int dealId;
    private int copyId;
    private int serviceProductId;
    private int channelId;
    private int payloadTypeId;
    private int serviceTransactionTypeId;
    private int requestStatusId;
    private Date statusDate;
    private int userProfileId;
    private String channelTransactionkey;
    private int instProfId;
    
    private int testSeqForGen = 0;
    
    *//**
     * <p>setUp</p>
     * Setting up test case.
     *//*
    @Before
    public void setUp() throws Exception {
      
        _logger.info(BORDER_START, ENTITY_NAME + " TestCase setting up ...");
  
        // get random seqence number
        // channelTransactionKey should not be null
        while (channelTransactionkey == null) {
          Double indexD = new Double(Math.random() * DATA_COUNT);
          testSeqForGen = indexD.intValue();
          channelTransactionkey 
              = _dataRepository.getString(ENTITY_NAME,  
                                          "CHANNELTRANSACTIONKEY", 
                                          testSeqForGen);
        }

        //      get test data from repository
        requestId 
            = _dataRepository.getInt(ENTITY_NAME,  "REQUESTID", testSeqForGen);
        requestDate = _dataRepository.getDate(ENTITY_NAME,  
                                              "REQUESTDATE", 
                                              testSeqForGen);
        dealId = _dataRepository.getInt(ENTITY_NAME,  "DEALID", testSeqForGen);
        copyId = _dataRepository.getInt(ENTITY_NAME,  "COPYID", testSeqForGen);
        serviceProductId = _dataRepository.getInt(ENTITY_NAME,  
                                                  "SERVICEPRODUCTID", 
                                                  testSeqForGen);
        channelId 
            = _dataRepository.getInt(ENTITY_NAME,  "CHANNELID", testSeqForGen);
        payloadTypeId = _dataRepository.getInt(ENTITY_NAME,  
                                               "PAYLOADTYPEID", 
                                               testSeqForGen);
        
        serviceTransactionTypeId 
            = _dataRepository.getInt(ENTITY_NAME,  
                                     "SERVICETRANSACTIONTYPEID", 
                                     testSeqForGen);
        requestStatusId = _dataRepository.getInt(ENTITY_NAME,  
                                                 "REQUESTSTATUSID", 
                                                 testSeqForGen);
        statusDate = _dataRepository.getDate(ENTITY_NAME,  
                                             "STATUSDATE",
                                             testSeqForGen);
        userProfileId 
            = _dataRepository.getInt(ENTITY_NAME, 
                                     "USERPROFILEID", 
                                     testSeqForGen);
        channelTransactionkey 
            = _dataRepository.getString(ENTITY_NAME,  
                                        "CHANNELTRANSACTIONKEY", 
                                        testSeqForGen);
        instProfId 
            = _dataRepository.getInt(ENTITY_NAME,  
                                     "INSTITUTIONPROFILEID", 
                                     testSeqForGen);
           
        // init ResouceManager
        ResourceManager.init();

        //  init session resource kit.
        _srk = new SessionResourceKit();

        _logger.info(BORDER_END, "Setting up Done");
    }

    @After
    public void tearDown() {

      //      free resources
      _srk.freeResources( );

    }
  
    *//**
     * test findByPrimaryKey.
     *//*
    @Test
    public void testFindByPrimaryKey()  throws Exception {
      
        _logger.info(BORDER_START, "findByPrimaryKey");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // excute method
        Request entity = new Request(_srk);    
        entity = entity.findByPrimaryKey(new RequestPK(requestId, copyId));
        
        assertEqualProperties(entity);
        
        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    *//**
     * <p>retreive Request record from Request table by channelTransationKey.
     * this method should not require VPD set since it will be used from WS, 
     * when asynchronous response comes back from Datx
     * Express doesn't know institutionId</p>
  
     * @throws Exception
     *//*
    @Test
    public void testFindByChannelTransactionKey() throws Exception {
        _logger.info(BORDER_START, "FindByChannelTransactionKey");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
        
        // excute method
        Request entity = new Request(_srk);
        entity = entity.findByChannelTransactionKey(channelTransactionkey);
        
        // assert
        assertEqualProperties(entity);

        _logger.info(BORDER_END, "testFindByChannelTransactionKey");
    }
  
    *//**
     * test findByDeal.
     *//*
    @Test
    public void testFindByDeal() throws Exception {
      
        _logger.info(BORDER_START, "FindByDeal");

        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // excute method
        Request entity = new Request(_srk);
        Collection requests = entity.findByDeal(new DealPK(dealId, copyId));
        
        // check assertEqual only insitutionId, dealId
        Iterator requestIter = requests.iterator();
        for (; requestIter.hasNext(); ) {
            Request aRequest = (Request) requestIter.next();
            assertEquals(aRequest.getInstitutionProfileId(), instProfId);
            assertEquals(aRequest.getDealId(), dealId);
        }
        
        _logger.info(BORDER_END, "testFindByDeal");
    }
  
    *//**
     * test findRequestsByRequestId.
     *//*
    @Test
    public void testFindRequestsByRequestId() throws Exception {
      
        _logger.info(BORDER_START, "FindRequestsByRequestId");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // excute method
        Request entity = new Request(_srk);
        Collection requests = entity.findRequestsByRequestId(requestId);

        // check assertEqual only insitutionId, and requestId
        Iterator requestIter = requests.iterator();
        for (; requestIter.hasNext(); ) {
            Request aRequest = (Request) requestIter.next();
            assertEquals(aRequest.getInstitutionProfileId(), instProfId);
            assertEquals(aRequest.getRequestId(), requestId);
        }

        _logger.info(BORDER_END, "FindRequestsByRequestId");
    }

    *//**
     * TODO: imcompleted
     * test findLastRequestsByRequestId.
     *//*
    @Ignore
    public void testFindLastRequestByRequestId() throws Exception {
      
        _logger.info(BORDER_START, "FindLastRequestByRequestId");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // excute method
        Request entity = new Request(_srk);
        
        _logger.info(BORDER_END, "FindLastRequestByRequestId");
    }
    
    *//**
     * TODO: imcompleted
     * test findLastRequestByServiceProductId.
     *//*
    @Ignore
    public void testFindLastRequestByServiceProductId() throws Exception {
      
      _logger.info(BORDER_START, "FindLastRequestByServiceProductId");
      
      //  set VPD
      _srk.getExpressState().setDealInstitutionId(instProfId);

      // excute method
      Request entity = new Request(_srk);
      
      _logger.info(BORDER_END, "FindLastRequestByServiceProductId");
    }

    *//**
     * TODO: imcompleted
     * test FindByDealIdAndCopyId.
     *//*
    @Ignore
    public void testFindByDealIdAndCopyId() throws Exception {
      
      _logger.info(BORDER_START, "FindByDealIdAndCopyId");
      
      //  init session resource kit.
      _srk = new SessionResourceKit();

      //  set VPD
      _srk.getExpressState().setDealInstitutionId(instProfId);

      // excute method
      Request entity = new Request(_srk);
      
      //      free resources
      _srk.freeResources( );

      _logger.info(BORDER_END, "FindByDealIdAndCopyId");
    }

    *//**
     * TODO: imcompleted
     * test FindContactId.
     *//*
    @Ignore
    public void testFindContactId() throws Exception {

        _logger.info(BORDER_START, "FindContactId");

        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // excute method
        Request entity = new Request(_srk);
        
        _logger.info(BORDER_END, "FindContactId");
    }

    *//**
     * TODO: imcompleted
     * test FindDatxTimeout.
     *//*
    @Ignore
    public void testFindDatxTimeout() throws Exception {
      
        _logger.info(BORDER_START, "FindDatxTimeout");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // excute method
        Request entity = new Request(_srk);
        
        _logger.info(BORDER_END, "FindDatxTimeout");
    }

    *//**
     * TODO: imcompleted
     * test findDocumentTypeId.
     *//*
    @Ignore
    public void testFindDocumentTypeId() throws Exception {
      
        _logger.info(BORDER_START, "findDocumentTypeId");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // excute method
        Request entity = new Request(_srk);
        
        _logger.info(BORDER_END, "findDocumentTypeId");
    }
    
    *//**
     * TODO: imcompleted
     * test getRequestTypeShortName.
     *//*
    @Ignore
    public void testGetRequestTypeShortName() throws Exception {
      
        _logger.info(BORDER_START, "getRequestTypeShortName");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // excute method
        Request entity = new Request(_srk);
        
        _logger.info(BORDER_END, "getRequestTypeShortName");
    }
    
    *//**
     * TODO: imcompleted
     * test getRequestTypeId.
     *//*
    @Ignore
    public void testGetRequestTypeId() throws Exception {
      
        _logger.info(BORDER_START, "getRequestTypeId");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // excute method
        Request entity = new Request(_srk);
        
        _logger.info(BORDER_END, "getRequestTypeId");
    }
    
    *//**
     * TODO: imcompleted
     * test isRequestForAnotherScenario.
     *//*
    @Ignore
    public void testIsRequestForAnotherScenario() throws Exception {
      
        _logger.info(BORDER_START, "isRequestForAnotherScenario");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // excute method
        Request entity = new Request(_srk);
        
        _logger.info(BORDER_END, "isRequestForAnotherScenario");
    }
    
    *//**
     * test create.
     *//*
    @Test
    public void testCreate() throws Exception {
      
        _logger.info(BORDER_START, "create");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // set beginTransaction for auto commit off
        _srk.beginTransaction();
        
        Date now = new Date();
        // excute method
        Request entity = new Request(_srk);
        entity = entity.create(new DealPK(dealId, copyId), requestStatusId, now, userProfileId);
        assertEquals(entity.getInstitutionProfileId(), instProfId);
        assertEquals(entity.getDealId(), dealId);
        assertEquals(entity.getCopyId(), copyId);
        assertEquals(entity.getStatusDate().toString(), now.toString());
        assertEquals(entity.getInstitutionProfileId(), instProfId);

        // rollback
        _srk.rollbackTransaction();
        
        _logger.info(BORDER_END, "create");
    }

    *//**
     * test create.
     *//*
    @Test
    public void createChildAssocs() throws Exception {
      
        _logger.info(BORDER_START, "createChildAssocs");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // set beginTransaction for auto commit off
        _srk.beginTransaction();
        
        // Create the initial entity
        Borrower borrower = new Borrower(_srk, CalcMonitor.getMonitor(_srk));
        
        // Find by primary key
        borrower = borrower.findByPrimaryBorrower(dealId, copyId);
        
        // excute method
        Request request = new Request(_srk);
        request.copyId = copyId;
        request.requestId = requestId;
        
        request.createChildAssocs(borrower.getBorrowerId());

        // Store it in the DB
        request.ejbStore();

        // rollback
        _srk.rollbackTransaction();
        
        _logger.info(BORDER_END, "createChildAssocs");
    }

    *//**
     * assertEqualProperties
     * 
     * check assertEquals for all properties of Request with  test Data
     * 
     * @param entity: Reqest
     * @throws Exception
     *//*
    private void assertEqualProperties(Request entity) throws Exception {
        assertEquals(entity.getRequestId(), requestId);
        assertEquals(entity.getRequestDate(), requestDate);
        assertEquals(entity.getDealId(), dealId);
        assertEquals(entity.getServiceProductId(), serviceProductId);
        assertEquals(entity.getChannelId(), channelId);
        assertEquals(entity.getPayloadTypeId(), payloadTypeId);
        assertEquals(entity.getServiceTransactionTypeId(), serviceTransactionTypeId);
        assertEquals(entity.getRequestStatusId(), requestStatusId);
        assertEquals(entity.getStatusDate(), statusDate);
        assertEquals(entity.getChannelTransactionKey(), channelTransactionkey);
        assertEquals(entity.getUserProfileId(), userProfileId);      
    }

    *//**
     * assertEqualProperties
     * 
     * check assertEquals for all properties of two Requests 
     * 
     * @param entity: Reqest
     * @throws Exception
     *//*
    private void assertEqualProperties(Request entity, Request request) throws Exception {
      assertEquals(entity.getRequestId(), request.getRequestId());
      assertEquals(entity.getRequestDate(), request.getRequestDate());
      assertEquals(entity.getDealId(), request.getDealId());
      assertEquals(entity.getServiceProductId(), request.getServiceProductId());
      assertEquals(entity.getChannelId(), request.getChannelId());
      assertEquals(entity.getPayloadTypeId(), request.getPayloadTypeId());
      assertEquals(entity.getServiceTransactionTypeId(), request.getServiceTransactionTypeId());
      assertEquals(entity.getRequestStatusId(), request.getRequestStatusId());
      assertEquals(entity.getStatusDate(), request.getStatusDate());
      assertEquals(entity.getChannelTransactionKey(), request.getChannelTransactionKey());
      assertEquals(entity.getUserProfileId(), request.getUserProfileId());      
  }
    
    
    
    
*/	@Test
	public void testTBR(){
        System.out.println("test");
}	

}
