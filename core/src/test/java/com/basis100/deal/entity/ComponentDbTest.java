package com.basis100.deal.entity;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.AppraisalOrderPK;
import com.basis100.deal.pk.ArchivedLoanApplicationPK;
import com.basis100.deal.pk.ArchivedLoanDecisionPK;
import com.basis100.deal.pk.BncManReviewReasonPK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.util.EntityTestUtil;

public class ComponentDbTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private Component component;	
	public ComponentDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(Component.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(Component.class.getSimpleName() + "DataSet.xml"));
	}
	
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		component = new Component(srk);		
	}
	
	public void testCreate() throws Exception{
		try {			
			srk.beginTransaction();
			ITable testCreate = dataSetTest.getTable("testCreate");	
		    int dealId=Integer.valueOf((String)testCreate.getValue(0,"DEALID"));		   
		    int copyId=Integer.valueOf((String)testCreate.getValue(0,"COPYID"));
		    int componentTypeId=Integer.valueOf((String)testCreate.getValue(0,"COMPONENTTYPEID"));
		    int mtgProdId=Integer.valueOf((String)testCreate.getValue(0,"MTGPRODID"));		    
		    srk.getExpressState().setDealInstitutionId(1);		    
		    component=component.create(dealId,copyId,componentTypeId,mtgProdId);		   
		    assertEquals(dealId,component.getDealId());
		    srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
	
	
	
	public void testFindByDealSuccess() throws Exception{
		Collection<Component> cList=null;
		try {			
			srk.beginTransaction();
			ITable testFindByDeal = dataSetTest.getTable("testFindByDealSuccess");	
		    int dealId=Integer.valueOf((String)testFindByDeal.getValue(0,"DEALID"));		   
		    int copyId=Integer.valueOf((String)testFindByDeal.getValue(0,"COPYID"));		        
		    srk.getExpressState().setDealInstitutionId(1);
		    DealPK pk=new DealPK(dealId,copyId);
		    cList=component.findByDeal(pk);		   
		    assertEquals(dealId,pk.getId());
		    srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
	
	public void testFindByDealFailure() throws Exception{
		Collection<Component> cList=null;
		try {			
			srk.beginTransaction();
			ITable testFindByDeal = dataSetTest.getTable("testFindByDealFailure");	
		    int dealId=Integer.valueOf((String)testFindByDeal.getValue(0,"DEALID"));		   
		    int copyId=Integer.valueOf((String)testFindByDeal.getValue(0,"COPYID"));		        
		    srk.getExpressState().setDealInstitutionId(1);
		    DealPK pk=new DealPK(dealId,copyId);
		    cList=component.findByDeal(pk);		   
		    assertEquals(0,cList.size());
		    srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
	
	/*public void testFindByPrimaryKey() throws Exception{		
		ITable findByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKey");
		int componentId = Integer.parseInt((String)findByPrimaryKey.getValue(0,"componentId"));	
		int copyId = Integer.parseInt((String)findByPrimaryKey.getValue(0,"copyId"));
		srk.getExpressState().setDealInstitutionId(1);		
		ComponentPK  pk = new ComponentPK(componentId,copyId);
		component=component.findByPrimaryKey(pk);		
		assertEquals(componentId, pk.getId());		
	} */  
 public void testFindByPrimaryKeyFailure() throws Exception{		
		ITable findByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKeyFailure");
		boolean status=false;
		try{
			int componentId = Integer.parseInt((String)findByPrimaryKey.getValue(0,"componentId"));	
			int copyId = Integer.parseInt((String)findByPrimaryKey.getValue(0,"copyId"));
			srk.getExpressState().setDealInstitutionId(1);		
			ComponentPK  pk = new ComponentPK(componentId,copyId);
			component=component.findByPrimaryKey(pk);		
		status=true;
		}catch(Exception e ){
			status = false;		
		}
		assertEquals(false, status);
	} 
 
 public void testGetPrimeIndex() throws Exception{		
		ITable findByPrimaryKey = dataSetTest.getTable("testGetPrimeIndex");
		String result=null;
		int componentId = Integer.parseInt((String)findByPrimaryKey.getValue(0,"componentId"));	
		int copyId = Integer.parseInt((String)findByPrimaryKey.getValue(0,"copyId"));
		srk.getExpressState().setDealInstitutionId(1);		
		result=component.getPrimeIndex(componentId,copyId);		
		assertNotNull(result);		
	}   
 

}
