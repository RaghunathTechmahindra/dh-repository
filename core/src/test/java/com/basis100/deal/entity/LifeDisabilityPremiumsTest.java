/*
 * @(#)LifeDisabilityPremiumsTest.java Sep 11, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.InsureOnlyApplicantPK;
import com.basis100.deal.pk.LifeDisabilityPremiumsPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * LifeDisabilityPremiumsTest
 * 
 * @version 1.0 Sep 11, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class LifeDisabilityPremiumsTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(LifeDisabilityPremiumsTest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test the find by primary key
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {
        // get input data from repository
        int lifeDisabilityPremiumsID = _dataRepository.getInt(
                "LifeDisabilityPremiums", "LIFEDISABILITYPREMIUMSID", 0);
        int copyID = _dataRepository.getInt("LifeDisabilityPremiums", "COPYID",
                0);
        int borrowerID = _dataRepository.getInt("LifeDisabilityPremiums",
                "borrowerID", 0);
        int dealInstitutionId = _dataRepository.getInt(
                "LifeDisabilityPremiums", "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "findByPrimaryKey");

        // Create the initial entity
        LifeDisabilityPremiums lifeDisabilityPremiums = new LifeDisabilityPremiums(
                _srk);

        // Find by primary key
        lifeDisabilityPremiums = lifeDisabilityPremiums
                .findByPrimaryKey(new LifeDisabilityPremiumsPK(
                        lifeDisabilityPremiumsID, copyID));

        // Check that the data retrieved matches the DB
        assertTrue(lifeDisabilityPremiums.getBorrowerId() == borrowerID);

        _logger.info(BORDER_END, "findByPrimaryKey");

    }

    /**
     * test the find by LD Insurance rate
     */
    @Test
    public void testFindByLDInsuranceRate() throws Exception {
        // get input data from repository
        int ldInsuranceRateID = _dataRepository.getInt(
                "LifeDisabilityPremiums", "LDINSURANCERATEID", 0);
        int borrowerID = _dataRepository.getInt("LifeDisabilityPremiums",
                "borrowerID", 0);
        int dealInstitutionId = _dataRepository.getInt(
                "LifeDisabilityPremiums", "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "testFindByLDInsuranceRate");

        // Create the initial entity
        LifeDisabilityPremiums lifeDisabilityPremiums = new LifeDisabilityPremiums(
                _srk);

        // Find by LD Insurance Rate ID
        List<LifeDisabilityPremiums> premiumsList = lifeDisabilityPremiums
                .findByLDInsuranceRateId(ldInsuranceRateID);

        // Check that the data retrieved matches the DB
        assertTrue(premiumsList.get(0).getBorrowerId() == borrowerID);

        _logger.info(BORDER_END, "testFindByLDInsuranceRate");

    }

    /**
     * test the find by Borrower
     */
    @Test
    public void testFindByBorrower() throws Exception {
        // get input data from repository
        int borrowerID = _dataRepository.getInt("LifeDisabilityPremiums",
                "borrowerID", 0);
        int copyID = _dataRepository.getInt("LifeDisabilityPremiums", "CopyID",
                0);
        int ldInsuranceRateID = _dataRepository.getInt(
                "LifeDisabilityPremiums", "LDINSURANCERATEID", 0);
        int dealInstitutionId = _dataRepository.getInt(
                "LifeDisabilityPremiums", "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "testFindByBorrower");

        // Create the initial entity
        LifeDisabilityPremiums lifeDisabilityPremiums = new LifeDisabilityPremiums(
                _srk);

        // Find by LD Insurance Rate ID
        List<LifeDisabilityPremiums> premiumsList = (List) lifeDisabilityPremiums
                .findByBorrower(new BorrowerPK(borrowerID, copyID));

        // Check that the data retrieved matches the DB
        assertTrue(premiumsList.get(0).getLDInsuranceRateId() == ldInsuranceRateID);

        _logger.info(BORDER_END, "testFindByBorrower");

    }

    /**
     * test the create
     */
    @Test
    public void testCreate() throws Exception {
        // get input data from repository
        int borrowerID = _dataRepository.getInt("LifeDisabilityPremiums",
                "borrowerID", 0);
        int copyID = _dataRepository.getInt("LifeDisabilityPremiums", "CopyID",
                0);
        int ldInsuranceRateID = _dataRepository.getInt(
                "LifeDisabilityPremiums", "LDINSURANCERATEID", 0);
        int ldInsuranceTypeID = _dataRepository.getInt(
                "LifeDisabilityPremiums", "LDINSURANCETYPEID", 0);
        int dealInstitutionId = _dataRepository.getInt(
                "LifeDisabilityPremiums", "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // Start new transaction
        _srk.beginTransaction();

        _logger.info(BORDER_START, "create");

        // Create the initial entity
        LifeDisabilityPremiums lifeDisabilityPremiums = new LifeDisabilityPremiums(
                _srk);

        // Create a new entity
        lifeDisabilityPremiums = lifeDisabilityPremiums.create(new BorrowerPK(
                borrowerID, copyID), ldInsuranceTypeID);

        // Store it in the DB
        lifeDisabilityPremiums.ejbStore();

        // verify database contents
        lifeDisabilityPremiums = lifeDisabilityPremiums
                .findByPrimaryKey(new LifeDisabilityPremiumsPK(
                        lifeDisabilityPremiums.getLifeDisabilityPremiumsId(),
                        copyID));

        assertTrue(lifeDisabilityPremiums.getLDInsuranceTypeId() == ldInsuranceTypeID);

        // Rollback transaction
        _srk.rollbackTransaction();

        _logger.info(BORDER_END, "testCreate");

    }
   
    /**
     * test the create with PK
     */
    @Test
    public void testCreate2() throws Exception {
        // get input data from repository
        int lifeDisabilityPremiumsID = 999;
        int copyID = 1;
        int dealInstitutionId = _dataRepository.getInt(
                "LifeDisabilityPremiums", "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // Start new transaction
        _srk.beginTransaction();

        _logger.info(BORDER_START, "testCreate2");

        // Create the initial entity
        LifeDisabilityPremiums lifeDisabilityPremiums = new LifeDisabilityPremiums(
                _srk);

        // Create a new entity
        lifeDisabilityPremiums = lifeDisabilityPremiums.create(new LifeDisabilityPremiumsPK(
                lifeDisabilityPremiumsID, copyID));

        // Store it in the DB
        lifeDisabilityPremiums.ejbStore();


        // Rollback transaction
        _srk.rollbackTransaction();

        _logger.info(BORDER_END, "testCreate2");

    }
}
