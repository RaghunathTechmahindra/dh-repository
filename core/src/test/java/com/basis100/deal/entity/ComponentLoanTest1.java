package com.basis100.deal.entity;

import java.io.IOException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.util.EntityTestUtil;

public class ComponentLoanTest1 extends FXDBTestCase {
	private IDataSet dataSetTest;
    private ComponentLoan componentLoan=null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public ComponentLoanTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ComponentLoan.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(ComponentLoan.class.getSimpleName()+"DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.componentLoan = new ComponentLoan(srk);
	}
	public void testCreate() throws Exception
	{
		ITable testCreate = dataSetTest.getTable("testCreate");
		String iId = (String)testCreate.getValue(0, "INSTITUTIONPROFILEID");
		int dealInstitutionId = Integer.parseInt(iId);
		String cid = (String)testCreate.getValue(0, "COMPONENTID");
		int componentid = Integer.parseInt(cid);
		String cpid = (String)testCreate.getValue(0, "COPYID");
		int copyid = Integer.parseInt(cpid);
		int paymentFrequencyId = Integer.parseInt((String)testCreate.getValue(0, "PAYMENTFREQUENCYID"));
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		srk.beginTransaction();
		
		ComponentLoan aComponentLoan = componentLoan.create(componentid, copyid);
		//find a ComponentLoan to verify
		ComponentLoan new_ComponentLoan = new ComponentLoan(srk, componentid, copyid);
		assertEquals(aComponentLoan, new_ComponentLoan);
		
		srk.rollbackTransaction();
	}
	/*
	 * Failure case not applicable as the create method returns an entity and sending unqualified data will result
	 * in improper end of test class and thus it is avoid.
	 */
}
