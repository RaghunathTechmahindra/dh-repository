package com.basis100.deal.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.DealPK;


/**
 * <p>DownPaymentSourceDbTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class DownPaymentSourceDbTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private DownPaymentSource downPaymentSource;

	public DownPaymentSourceDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(DownPaymentSource.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(DownPaymentSource.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.downPaymentSource = new DownPaymentSource(srk, null);
	}
	
	public void testFindByDeal() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByDeal");
    	int downpaymentSourceId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DOWNPAYMENTSOURCEID"));
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	List<DownPaymentSource> found = new ArrayList<DownPaymentSource>();
    	found = (List<DownPaymentSource>) downPaymentSource.findByDeal(new DealPK(id,copyId));
    	int downpaymentSourceId1 =0;
    	for(int i=0;i<=0;i++){
    		downpaymentSourceId1 =found.get(i).getDownPaymentSourceId();
    	}
    	assertEquals(downpaymentSourceId, downpaymentSourceId1);
    }
	
	public void testCreate() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreate");
    	//int downpaymentSourceId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DOWNPAYMENTSOURCEID"));
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	
    	downPaymentSource.create(new DealPK(id,copyId));
    	assertNotNull(downPaymentSource);
    }
	
	public void testFindByMyCopies() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByMyCopies");
    	int downpaymentSourceId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DOWNPAYMENTSOURCEID"));
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	
    	downPaymentSource.findByMyCopies();
    	assertNotNull(downPaymentSource);
    }
	
}