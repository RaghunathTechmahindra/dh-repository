package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.util.EntityTestUtil;

public class ComponentCreditCardDBTest extends FXDBTestCase {
	private IDataSet dataSetTest;
    private ComponentCreditCard componentCreditCard=null;
    Object obj =null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public ComponentCreditCardDBTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ComponentCreditCard.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(ComponentCreditCard.class.getSimpleName()+"DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.componentCreditCard = new ComponentCreditCard(srk);
	}
	public void testCreate() throws Exception
	{
		ITable testCreate = dataSetTest.getTable("testCreate");
		String iId = (String)testCreate.getValue(0, "INSTITUTIONPROFILEID");
		int dealInstitutionId = Integer.parseInt(iId);
		String cid = (String)testCreate.getValue(0, "COMPONENTID");
		int componentid = Integer.parseInt(cid);
		String cpid = (String)testCreate.getValue(0, "COPYID");
		int copyid = Integer.parseInt(cpid);
		Double creditAmt = Double.parseDouble((String)testCreate.getValue(0, "CREDITCARDAMOUNT"));
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		
		srk.beginTransaction();
		ComponentCreditCard aComponentCreditCard = componentCreditCard.create(componentid, copyid);
		
		
		//find a ComponentCreditCard to verify
		ComponentCreditCard new_ComponentCreditCard = new ComponentCreditCard(srk, componentid, copyid);
		assertEquals(aComponentCreditCard, new_ComponentCreditCard);
		srk.rollbackTransaction();
	}
	/*
	 * Failure case not applicable as the create method returns an entity and sending unqualified data will result
	 * in improper end of test class and thus it is avoid.
	 */
}
