package com.basis100.deal.entity;

import java.util.Collection;
import java.util.Iterator;
import java.util.HashSet;

import com.basis100.FXDBTestCase;

import org.dbunit.Assertion;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.NoSuchColumnException;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.deal.entity.FCXLoanAppQueue;
import com.basis100.deal.pk.FCXLoanAppQueuePK;

public class FCXLoanAppQueueTest extends FXDBTestCase {
	
		//XML file that contains all the expected data.
		private final static String TEST_XML_DATA = "FCXLoanAppQueueDataSetTest.xml";
		//Table name.
		private final static String TABLE_NAME = "FCXLOANAPPQUEUE";
		
		public FCXLoanAppQueueTest(String name){
			super(name);
	    }
	
		
/*
		protected DatabaseOperation getSetUpOperation() throws Exception
	    {
	        return DatabaseOperation.NONE;
	    }
*/
	    protected DatabaseOperation getTearDownOperation() throws Exception
	    {
	        return DatabaseOperation.DELETE_ALL;
	    }
	    
	    private void compareEntityToITable(FCXLoanAppQueue app, ITable it){
	    	//Determine if the Id field will be tested.
	    	boolean testId = true;
	    	try{
	    		it.getTableMetaData().getColumnIndex("FCXLOANAPPQUEUEID");
	    	} catch (NoSuchColumnException e) {
	    		testId = false;
	    	} catch (Exception e) {
	    		fail();
	    	} 
	    	try{
	    		if(testId)
	    			assertEquals(app.getFcxLoanAppQueueId(), super.getIntValue(it, 0, "FCXLOANAPPQUEUEID"));
	    		assertEquals(app.getSenderMessageId(), super.getStrValue(it, 0, "SENDERMESSAGEID"));
	    		assertEquals(app.getSenderChannel(), super.getStrValue(it, 0, "SENDERCHANNEL"));
	    		assertEquals(app.getReceiverChannel(), super.getStrValue(it, 0, "RECEIVERCHANNEL"));
	    		assertEquals(app.getSourceApplicationId(), super.getStrValue(it, 0, "SOURCEAPPLICATIONID"));
	    		assertEquals(app.getFormat(), super.getStrValue(it, 0, "FORMAT"));
	    		assertEquals(app.getVersion(), super.getStrValue(it, 0, "VERSION"));
	    		assertEquals(app.getReceivedTimeStamp(), super.getDateValue(it, 0, "RECEIVEDTIMESTAMP"));
	    		assertEquals(app.getMessage(), super.getStrValue(it, 0, "MESSAGE"));
	    		assertEquals(app.getFcxLoanAppStatusId(), super.getIntValue(it, 0, "FCXLOANAPPSTATUSID"));
	    		assertEquals(app.getRetryCounter(), super.getIntValue(it, 0, "RETRYCOUNTER"));
	    	} catch (Exception e) {
	    		e.printStackTrace();
	    		fail();
	    	}
	    }

		@Test
		public void testCreate() {
			int newRecordId = -1;
						
			try{
				//Gets the data to be inserted, which is also the expected data.
				ITable expected = super.getITableFromXml(TEST_XML_DATA, TABLE_NAME+"_CREATE");
				
				FCXLoanAppQueue loanapp = new FCXLoanAppQueue(srk);
				//Create the record (handling single record only).
				loanapp.create(getStrValue(expected, 0, "SENDERMESSAGEID"), 
								getStrValue(expected, 0, "SENDERCHANNEL"), 
								getStrValue(expected, 0, "RECEIVERCHANNEL"), 
								getStrValue(expected, 0, "SOURCEAPPLICATIONID"),
								getStrValue(expected, 0, "FORMAT"),
								getStrValue(expected, 0, "VERSION"),
								getDateValue(expected, 0, "RECEIVEDTIMESTAMP"), 
								getStrValue(expected, 0, "MESSAGE"), 
								getIntValue(expected, 0, "FCXLOANAPPSTATUSID"),
								getIntValue(expected, 0, "RETRYCOUNTER"));
				newRecordId = loanapp.getFcxLoanAppQueueId();
				
				//Find actual data from database.
				ITable actual_db = getFilteredITableFromDB(TABLE_NAME, "FCXLOANAPPQUEUEID=" + newRecordId, expected);
				
				//Verify that db data is in sync with expected data
		        Assertion.assertEquals(expected, actual_db);
		         
		        //Verify that loanapp record object is in sync with expected data.
		        this.compareEntityToITable(loanapp, expected);
		        
			} catch (Exception e) {
				e.printStackTrace();
				fail("Exception during assertion.");
			}
		}
		
		/*@Test
		public void testFindByPrimaryKey() {
			int testRecordId = 100;
			FCXLoanAppQueue actual = null;
			try{
				FCXLoanAppQueue loanapp = new FCXLoanAppQueue(srk);
				actual = loanapp.findByPrimaryKey(new FCXLoanAppQueuePK(testRecordId));
			} catch (Exception e){
				e.printStackTrace();
				fail("Exception during test.");
			}
			try{
		        ITable expected = getITableFromXml(TEST_XML_DATA, TABLE_NAME + "_FINDBYPRIMARYKEY_EXPECTED");
		        compareEntityToITable(actual, expected);
			} catch (Exception e) {
				e.printStackTrace();
				fail("Exception during assertion.");
			}
		}
		
		@Test
		public void testSetters() {
			int recordId = 102;
			
			try{
				//Obtain record from the database.
				FCXLoanAppQueue initApp = new FCXLoanAppQueue(srk);
				FCXLoanAppQueue loanAppRecord = initApp.findByPrimaryKey(new FCXLoanAppQueuePK(recordId));
				//Get the new data, which is will also be the expected data.
				ITable expected = getITableFromXml(TEST_XML_DATA, TABLE_NAME + "_SETTERS");
				
				//Invoke setter methods.
				loanAppRecord.setSenderMessageId(super.getStrValue(expected, 0, "SENDERMESSAGEID"));
				loanAppRecord.setSenderChannel(super.getStrValue(expected, 0, "SENDERCHANNEL"));
				loanAppRecord.setReceiverChannel(super.getStrValue(expected, 0, "RECEIVERCHANNEL"));
				loanAppRecord.setSourceApplicationId(super.getStrValue(expected, 0, "SOURCEAPPLICATIONID"));
				loanAppRecord.setFormat(super.getStrValue(expected, 0, "FORMAT"));
				loanAppRecord.setVersion(super.getStrValue(expected, 0, "VERSION"));
				loanAppRecord.setReceivedTimeStamp(super.getDateValue(expected, 0, "RECEIVEDTIMESTAMP"));
				loanAppRecord.setMessage(super.getStrValue(expected, 0, "MESSAGE"));
				loanAppRecord.setFcxLoanAppStatusId(super.getIntValue(expected, 0, "FCXLOANAPPSTATUSID"));
				loanAppRecord.setRetryCounter(super.getIntValue(expected, 0, "RETRYCOUNTER"));
				//Write the data to database.
				loanAppRecord.ejbStore();
				
				//Get the record data from database. This data has just been committed to database by previous statement.
				ITable actual = super.getITableFromDB(TABLE_NAME, "FCXLOANAPPQUEUEID=" + recordId);
				
				//Compare expected with actual.
				Assertion.assertEquals(expected, actual);
				
				//Verify that loanAppRecord object is in sync with expected data.
		        this.compareEntityToITable(loanAppRecord, expected);
				
			} catch (Exception e){
				e.printStackTrace();
				fail("Exception during test.");
			}
			
		}
		
		@Test
		public void testFindFailedBySourceApplicationId() {
			String sourceAppId = "QA-Test-44";
			try{
				//Obtain record from the database.
				FCXLoanAppQueue initApp = new FCXLoanAppQueue(srk);
				Collection<FCXLoanAppQueue> loanApps = initApp.findFailedBySourceApplicationId(sourceAppId);
				Iterator<FCXLoanAppQueue> it = loanApps.iterator();
				HashSet<Integer> actual = new HashSet<Integer>();
				while(it.hasNext()){
					actual.add(it.next().getPk().getId());
				}
				//Expected group of ID's.
				HashSet<Integer> expected = new HashSet<Integer>();
				expected.add(104);
				expected.add(105);
				
				//Compare expected with actual.
				assertEquals(expected, actual);
			} catch (Exception e){
				e.printStackTrace();
				fail("Exception during test.");
			}
		}*/
}

