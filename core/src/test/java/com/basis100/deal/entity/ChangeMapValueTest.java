package com.basis100.deal.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ChangeMapValueTest {

	@Test
	public void testEqualsObject() {
		ChangeMapValue cm1 = new ChangeMapValue("hoge", "abc");
		ChangeMapValue cm2 = new ChangeMapValue("hoge", "abc");
		
		assertTrue(cm1.equals(cm2));
		
		cm2.setChangeFlag(true);
		assertFalse(cm1.equals(cm2));
		
		cm1.setChangeFlag(true);
		assertTrue(cm1.equals(cm2));
		
		cm2.setCurValue("xyz");
		assertFalse(cm1.equals(cm2));
	}

	@Test
	public void testDuplicate() {
		ChangeMapValue cm1 = new ChangeMapValue("hoge", "abc");
		ChangeMapValue cm2 = cm1.duplicate();
		
		assertNotSame(cm1, cm2); //different object
		assertEquals(cm1, cm2);  //but the same values
	}

	@Test
	public void testToString() {
		ChangeMapValue cm1 = new ChangeMapValue("hoge", "abc");
		System.out.println(cm1.toString()); //just info. no test required
	}

}
