/*
 * @(#)SAMDispositionTest.java    2007-9-7
 *
 * Copyright (C) 2007 Filogix Limited Partnership All rights reserved.
 */


package com.basis100.deal.entity;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.SAMDispositionPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>SAMDispoisionTest</p>
 * Express Entity class unit test: SAMDispoision
 */
public class SAMDispositionTest extends ExpressEntityTestCase 
    implements UnitTestLogging {
  
    private final static Logger _logger = 
        LoggerFactory.getLogger(RegionProfileTest.class);
  
    private final static String ENTITY_NAME = "SAMDISPOSITION"; 
    
    // retreived data count that is stored in UnitTestDataRepository.xml
    private static final int DATA_COUNT = 1;
    
    private SessionResourceKit _srk;
    
    // use this value for random sequence number data
    private int testSeqForGen = 0;
    
    // properties
    protected int           responseId;
    protected int         samDispositionStatusId;
    protected int       samDispositionCodeId;
    protected int         userProfileId;
    protected Date          submitDateTimeStamp;
    protected Date          responseDateTimeStamp;
    protected int          instProfId;

    /**
     * <p>setUp</p>
     * Setting up test case.
     */
    @Before
    public void setUp() throws Exception {

        _logger.info(BORDER_START, ENTITY_NAME + " TestCase setting up ...");

        //  init session resource kit.
        ResourceManager.init();
        //  init session resource kit.
        _srk = new SessionResourceKit();
        
//        // get random seqence number 
//        Double indexD = new Double(Math.random() * DATA_COUNT);
//        testSeqForGen = indexD.intValue();

        // retreive data from test data file
        responseId 
            = _dataRepository.getInt( ENTITY_NAME, "RESPOINSEID", testSeqForGen);
        samDispositionStatusId = _dataRepository.getInt( ENTITY_NAME, "samDispositionStatusId", testSeqForGen);
        samDispositionCodeId 
            = _dataRepository.getInt( ENTITY_NAME, "samDispositionCodeId", testSeqForGen);
        userProfileId = _dataRepository.getInt( ENTITY_NAME, "userProfileId", testSeqForGen);
        submitDateTimeStamp 
            = _dataRepository.getDate(ENTITY_NAME, "submitDateTimeStamp", testSeqForGen);
        responseDateTimeStamp = _dataRepository.getDate( ENTITY_NAME, "responseDateTimeStamp", testSeqForGen);
        instProfId 
            = _dataRepository.getInt( ENTITY_NAME, "INSTITUTIONPROFILEID", testSeqForGen);
  
        _logger.info(BORDER_END, "Setting up Done");
    }

    @After
    public void tearDown() {
      
        //      free resources
        _srk.freeResources( );
    }

    /**
     * test findByPrimaryKey.
     */
    @Ignore
    @Test
    public void testFindByPrimaryKey()  throws Exception {
    
        _logger.info(BORDER_START, "findByPrimaryKey");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
        
        // excute method
        SAMDisposition entity 
            = new SAMDisposition(_srk);
        entity 
            = entity.findByPrimaryKey( new SAMDispositionPK(responseId)); 
        
        // assertEquals
        assertEquals(entity.getResponseId(), responseId);
        assertEquals(entity.getSAMDispositionStatusId(), samDispositionStatusId);
        assertEquals(entity.getSAMDispositionCodeId(), samDispositionCodeId);
        assertEquals(entity.getUserProfileId(), userProfileId);
        assertEquals(entity.getSubmitDateTimeStamp(), submitDateTimeStamp);
        assertEquals(entity.getResponseDateTimeStamp(), responseDateTimeStamp);
        assertEquals(entity.getInstitutionProfileId(), instProfId);
        
        _logger.info(BORDER_END, "findByPrimaryKey");
    }

}
