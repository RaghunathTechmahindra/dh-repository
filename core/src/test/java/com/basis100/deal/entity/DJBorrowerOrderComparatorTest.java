package com.basis100.deal.entity;

import java.io.IOException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.util.collections.DJBorrowerOrderComparator;
import com.basis100.resources.SessionResourceKit;

public class DJBorrowerOrderComparatorTest extends FXDBTestCase {
	
	private IDataSet dataSetTest;
	private DJBorrowerOrderComparator dj = null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public DJBorrowerOrderComparatorTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(DJBorrowerOrderComparator.class.getSimpleName()+"DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected DatabaseOperation getSetUpOperation() throws Exception
	    {
		dj = new DJBorrowerOrderComparator();
	    	return DatabaseOperation.INSERT;
	    }
	
	public void testCompare() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCompare");
		
		int id1 = Integer.parseInt((String)testExtract.getValue(0, "BORROWERID1"));
		int id2 = Integer.parseInt((String)testExtract.getValue(0, "BORROWERID2"));
		int copyid = Integer.parseInt((String)testExtract.getValue(0, "copyid"));
		
		Borrower b1 = new Borrower(srk, null, id1, copyid);
		Borrower b2 = new Borrower(srk, null, id2, copyid);
		
		com.basis100.deal.entity.DealEntity o1 = (com.basis100.deal.entity.DealEntity )b1;
		com.basis100.deal.entity.DealEntity o2 = (com.basis100.deal.entity.DealEntity )b2;
		
		int result = dj.compare(o1, o2);
		
		assert result==0 || result==1 || result==-1;
		
		srk.rollbackTransaction();
	}
}
