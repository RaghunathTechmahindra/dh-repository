package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.InsureOnlyApplicantPK;
import com.basis100.deal.pk.LifeDisPremiumsIOnlyAPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;

public class LifeDisPremiumsIOnlyATest1 extends FXDBTestCase {
	private String INIT_XML_DATA = "LifeDisPremiumsIOnlyADataSet.xml";
	private LifeDisPremiumsIOnlyA lifeDisPremiumsIOnlyA;
	private IDataSet dataSetTest;

	public LifeDisPremiumsIOnlyATest1(String name) throws DataSetException,
			IOException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"LifeDisPremiumsIOnlyADataSetTest.xml"));
	}

	

	@Override
	protected void setUp() throws Exception {
		super.setUp();

		lifeDisPremiumsIOnlyA = new LifeDisPremiumsIOnlyA(srk);

	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	public void testCreateLifeDisabilityPremiumsPK()
			throws NumberFormatException, DataSetException, RemoteException,
			CreateException, JdbcTransactionException {
		srk.beginTransaction();
		int lifeDisPremiumsIOnlyAId = Integer.parseInt(dataSetTest
				.getTable("testCreateLifeDisabilityPremiumsPK")
				.getValue(0, "LifeDisPremiumsIOnlyAId").toString());
		int copyId = Integer.parseInt(dataSetTest
				.getTable("testCreateLifeDisabilityPremiumsPK")
				.getValue(0, "copyId").toString());
		LifeDisPremiumsIOnlyAPK lifeDisPremiumsIOnlyAPK = new LifeDisPremiumsIOnlyAPK(
				lifeDisPremiumsIOnlyAId, copyId);
		LifeDisPremiumsIOnlyA disPremiumsIOnlyA = lifeDisPremiumsIOnlyA
				.create(lifeDisPremiumsIOnlyAPK);
		assertEquals(lifeDisPremiumsIOnlyAId,
				disPremiumsIOnlyA.getLifeDisPremiumsIOnlyAId());
		srk.rollbackTransaction();
	}

	public void testInsureOnlyApplicantPK() throws JdbcTransactionException,
			RemoteException, CreateException, NumberFormatException,
			DataSetException {
		srk.beginTransaction();
		int insureOnlyApplicantId = Integer.parseInt(dataSetTest
				.getTable("testInsureOnlyApplicantPK")
				.getValue(0, "insureOnlyApplicantId").toString());
		int copyId = Integer.parseInt(dataSetTest
				.getTable("testInsureOnlyApplicantPK").getValue(0, "copyId")
				.toString());
		int lifeDisabilityInsTypeId = Integer.parseInt(dataSetTest
				.getTable("testInsureOnlyApplicantPK")
				.getValue(0, "lifeDisabilityInsTypeId").toString());
		int dealInstitutionId = Integer.parseInt(dataSetTest
				.getTable("testInsureOnlyApplicantPK")
				.getValue(0, "dealInstitutionId").toString());
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		InsureOnlyApplicantPK insureOnlyApplicantPK = new InsureOnlyApplicantPK(
				insureOnlyApplicantId, copyId);
		LifeDisPremiumsIOnlyA disPremiumsIOnlyA = lifeDisPremiumsIOnlyA.create(
				insureOnlyApplicantPK, lifeDisabilityInsTypeId);
		assertEquals(insureOnlyApplicantId,
				disPremiumsIOnlyA.getInsureOnlyApplicantId());
		srk.rollbackTransaction();
	}

	public void testFindByPrimaryKey() throws RemoteException, FinderException,
			NumberFormatException, DataSetException {
		int lifeDisPremiumsIOnlyAId = Integer.parseInt(dataSetTest
				.getTable("testFindByPrimaryKey")
				.getValue(0, "lifeDisPremiumsIOnlyAId").toString());
		int copyId = Integer.parseInt(dataSetTest
				.getTable("testFindByPrimaryKey").getValue(0, "copyId")
				.toString());
		LifeDisPremiumsIOnlyAPK lifeDisPremiumsIOnlyAPK = new LifeDisPremiumsIOnlyAPK(
				lifeDisPremiumsIOnlyAId, copyId);
		LifeDisPremiumsIOnlyA disPremiumsIOnlyA = lifeDisPremiumsIOnlyA
				.findByPrimaryKey(lifeDisPremiumsIOnlyAPK);
		assertEquals(lifeDisPremiumsIOnlyAId,
				disPremiumsIOnlyA.getLifeDisPremiumsIOnlyAId());
	}

	public void testFindByLDInsuranceRateId() throws Exception {
		int ldInsuranceRateId = Integer.parseInt(dataSetTest
				.getTable("testFindByLDInsuranceRateId")
				.getValue(0, "ldInsuranceRateId").toString());
		Collection list = lifeDisPremiumsIOnlyA
				.findByLDInsuranceRateId(ldInsuranceRateId);
		assertEquals(true, list.size() > 0);
		for (Object obj : list) {

			LifeDisPremiumsIOnlyA disPremiumsIOnlyA = (LifeDisPremiumsIOnlyA) obj;
			assertEquals(ldInsuranceRateId,
					disPremiumsIOnlyA.getLDInsuranceRateId());

		}
	}

	public void testFindByLDInsuranceRateIdFailure() throws Exception {
		int ldInsuranceRateId = Integer.parseInt(dataSetTest
				.getTable("testFindByLDInsuranceRateIdFailure")
				.getValue(0, "ldInsuranceRateId").toString());
		Collection list = lifeDisPremiumsIOnlyA
				.findByLDInsuranceRateId(ldInsuranceRateId);
		assertEquals(true, list.size() <= 0);
	}

	public void testFindByInsureOnlyApplicant() throws Exception {
		int insureOnlyApplicantId = Integer.parseInt(dataSetTest
				.getTable("testFindByInsureOnlyApplicant")
				.getValue(0, "insureOnlyApplicantId").toString());
		int copyId = Integer.parseInt(dataSetTest
				.getTable("testFindByInsureOnlyApplicant")
				.getValue(0, "copyId").toString());
		InsureOnlyApplicantPK insureOnlyApplicantPK = new InsureOnlyApplicantPK(
				insureOnlyApplicantId, copyId);
		Collection list = lifeDisPremiumsIOnlyA
				.findByInsureOnlyApplicant(insureOnlyApplicantPK);
		assertEquals(true, list.size() > 0);
		for (Object obj : list) {

			LifeDisPremiumsIOnlyA disPremiumsIOnlyA = (LifeDisPremiumsIOnlyA) obj;
			assertEquals(insureOnlyApplicantId,
					disPremiumsIOnlyA.getInsureOnlyApplicantId());

		}
	}

	public void testFindByInsureOnlyApplicantFailure() throws Exception {
		int insureOnlyApplicantId = Integer.parseInt(dataSetTest
				.getTable("testFindByInsureOnlyApplicantFailure")
				.getValue(0, "insureOnlyApplicantId").toString());
		int copyId = Integer.parseInt(dataSetTest
				.getTable("testFindByInsureOnlyApplicantFailure")
				.getValue(0, "copyId").toString());
		InsureOnlyApplicantPK insureOnlyApplicantPK = new InsureOnlyApplicantPK(
				insureOnlyApplicantId, copyId);
		Collection list = lifeDisPremiumsIOnlyA
				.findByInsureOnlyApplicant(insureOnlyApplicantPK);
		assertEquals(true, list.size() <= 0);
	}

	public void testFindByMyCopies() throws RemoteException, FinderException,
			NumberFormatException, DataSetException {
		int insureOnlyApplicantId = Integer.parseInt(dataSetTest
				.getTable("testFindByMyCopies")
				.getValue(0, "insureOnlyApplicantId").toString());
		lifeDisPremiumsIOnlyA.setLifeDisPremiumsIOnlyAId(insureOnlyApplicantId);
		Collection list = lifeDisPremiumsIOnlyA.findByMyCopies();
		assertEquals(true, list.size() > 0);
		for (Object obj : list) {

			LifeDisPremiumsIOnlyA disPremiumsIOnlyA = (LifeDisPremiumsIOnlyA) obj;

		}
	}

	public void testFindByMyCopiesFailure() throws RemoteException,
			FinderException, NumberFormatException, DataSetException {
		int insureOnlyApplicantId = Integer.parseInt(dataSetTest
				.getTable("testFindByMyCopiesFailure")
				.getValue(0, "insureOnlyApplicantId").toString());
		lifeDisPremiumsIOnlyA.setLifeDisPremiumsIOnlyAId(insureOnlyApplicantId);
		Collection list = lifeDisPremiumsIOnlyA.findByMyCopies();
		assertEquals(true, list.size() <= 0);
		
	}
}
