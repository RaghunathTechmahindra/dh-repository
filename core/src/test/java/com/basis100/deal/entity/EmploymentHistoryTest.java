/*
 * @(#) EmploymentHistoryTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */


package com.basis100.deal.entity;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.EmploymentHistoryPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>EmploymentHistoryTest</p>
 * Express Entity class unit test: EmploymentHistory
 */
public class EmploymentHistoryTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(EmploymentHistoryTest.class);

    /**
     * Constructor function
     */
    public EmploymentHistoryTest() {
    }

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int employmentHistoryId = _dataRepository.getInt("EMPLOYMENTHISTORY",
                "EMPLOYMENTHISTORYID", 0);
        int copyId = _dataRepository.getInt("EMPLOYMENTHISTORY", "COPYID", 0);
        int instProfId = _dataRepository.getInt("EMPLOYMENTHISTORY",
                "INSTITUTIONPROFILEID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed the input data and run the program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        EmploymentHistory entity = new EmploymentHistory(srk);
        entity = entity.findByPrimaryKey(new EmploymentHistoryPK(
                employmentHistoryId, copyId));

        // verify the running result.
        assertEquals(employmentHistoryId, entity.getEmploymentHistoryId());

        // free resources
        srk.freeResources();
    }

    /**
     * test create.
     */
    @Test
    public void testCreate() throws Exception {

        // get input data from repository
        int instProfId = _dataRepository.getInt("EMPLOYMENTHISTORY",
                "INSTITUTIONPROFILEID", 0);
        int borrowerId = _dataRepository.getInt("EMPLOYMENTHISTORY", "BORROWERID", 0);
        int copyId = _dataRepository.getInt("EMPLOYMENTHISTORY", "COPYID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed input data and run program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        srk.beginTransaction();
        EmploymentHistory newEntity = new EmploymentHistory(srk);
        newEntity = newEntity.create(new BorrowerPK(borrowerId, copyId));
        newEntity.ejbStore();

        // check create correctlly.
        EmploymentHistory foundEntity = new EmploymentHistory(srk);
        foundEntity = foundEntity.findByPrimaryKey(new EmploymentHistoryPK(
                newEntity.getEmploymentHistoryId(), newEntity.getCopyId()));

        // verifying
        assertEquals(foundEntity.getEmploymentHistoryId(), newEntity
                .getEmploymentHistoryId());

        // rollback transaction.
        srk.rollbackTransaction();
        // free resources.
        srk.freeResources();
    }

}
