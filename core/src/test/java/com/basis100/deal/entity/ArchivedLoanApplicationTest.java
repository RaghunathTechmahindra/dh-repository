package com.basis100.deal.entity;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.AppraisalOrderPK;
import com.basis100.deal.pk.ArchivedLoanApplicationPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.util.EntityTestUtil;

public class ArchivedLoanApplicationTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private ArchivedLoanApplication archivedLoanApplication;	
	private DealEntity entity;
	
	
	
	
	public ArchivedLoanApplicationTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ArchivedLoanApplication.class.getSimpleName() + "DataSetTest.xml"));
	}


	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		archivedLoanApplication = new ArchivedLoanApplication(srk);
		
	}
	
	public void testCreate() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, SQLException, CloneNotSupportedException, CreateException{
		try {
			srk.beginTransaction();
			ITable testCreate = dataSetTest.getTable("testCreate");		
			String archivedLoanApp=(String)testCreate.getValue(0,"ARCHIVEDLOANAPPLICATIONID");
		    int archivedLoanAppId=Integer.parseInt(archivedLoanApp);	
		    String archivedLoanAppSourceId=(String)testCreate.getValue(0,"SOURCEAPPLICATIONID");		   	
			srk.getExpressState().setDealInstitutionId(1);
			archivedLoanApplication=archivedLoanApplication.create(archivedLoanAppId,archivedLoanAppSourceId);
		    assertEquals(archivedLoanAppSourceId,archivedLoanApplication.getSourceApplicationId());
		    srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
} 
	 	
	 public void testFindByPrimaryKeySuccess() throws Exception{	    
		ITable testFindByPrimaryKeySuccess = dataSetTest.getTable("testFindByPrimaryKeySuccess");		
		String id=(String)testFindByPrimaryKeySuccess.getValue(0,"ARCHIVEDLOANAPPLICATIONID");
	    int archivedLoanApplicationId=Integer.parseInt(id);	    
		srk.getExpressState().setDealInstitutionId(1);		 
		ArchivedLoanApplicationPK responsePK=new ArchivedLoanApplicationPK(archivedLoanApplicationId); 
		archivedLoanApplication=archivedLoanApplication.findByPrimaryKey(responsePK);
	    assertEquals(archivedLoanApplicationId,responsePK.getId());
}	
	public void testPerformUpdate() throws Exception{	    
		ITable testPerformUpdateFailure = dataSetTest.getTable("testPerformUpdateFailure");		
		String id=(String)testPerformUpdateFailure.getValue(0,"ARCHIVEDLOANAPPLICATIONID");
	    int archivedLoanApplicationId=Integer.parseInt(id);	    
	    ArchivedLoanApplication archivedLoanApplication = new ArchivedLoanApplication(srk, archivedLoanApplicationId);	  
	    archivedLoanApplication.dealXML="testing";
		srk.getExpressState().setDealInstitutionId(1);		
		int a =archivedLoanApplication.performUpdate();
	    assertEquals(0,a);
	    }
	 public void testFindByPrimaryKeyFailure() throws Exception{	    
			ITable testFindByPrimaryKeySuccess = dataSetTest.getTable("testFindByPrimaryKeyFailure");
			boolean status=false;
			try{
			String id=(String)testFindByPrimaryKeySuccess.getValue(0,"ARCHIVEDLOANAPPLICATIONID");
		    int archivedLoanApplicationId=Integer.parseInt(id);	    
			srk.getExpressState().setDealInstitutionId(1);		 
			ArchivedLoanApplicationPK responsePK=new ArchivedLoanApplicationPK(archivedLoanApplicationId); 
			archivedLoanApplication=archivedLoanApplication.findByPrimaryKey(responsePK);
			status=true;
			}catch(Exception e){
				status=false;
			}
		    assertEquals(false,status);
	}	
	
}
