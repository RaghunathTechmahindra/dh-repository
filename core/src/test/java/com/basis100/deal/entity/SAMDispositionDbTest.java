package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.deal.pk.SAMResponsePK;


/**
 * <p>SAMDispositionDbTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class SAMDispositionDbTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private SAMDisposition sAMDisposition;
	
	public SAMDispositionDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(SAMDisposition.class.getSimpleName() + "DataSetTest.xml"));
	}
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.sAMDisposition = new SAMDisposition(srk);
		
	}
	
	/*public void testCreate() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreate");
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "SAMDISPOSITIONSTATUSID"));
    	int responseId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "RESPONSEID"));
    	srk.beginTransaction();
    	sAMDisposition.create(new ResponsePK(responseId));
    	srk.rollbackTransaction();
	    assertNotNull(sAMDisposition);
    }*/
	
	public void testFindBySAMResponse() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindBySAMResponse");
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "SAMDISPOSITIONSTATUSID"));
    	int responseId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "RESPONSEID"));
    	
    	sAMDisposition.findBySAMResponse(new SAMResponsePK(responseId));
	    assertNotNull(sAMDisposition);
    }
}