package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;
import java.util.Formattable;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.DealNotesPK;
import com.basis100.deal.pk.DealPK;
public class DealNotesTestDB extends FXDBTestCase{

	private IDataSet dataSetTest;	
	private DealNotes dealNotes;
	
	
	
	public DealNotesTestDB(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(DealNotes.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(DealNotes.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		dealNotes = new DealNotes(srk);
	}	
	
	public void testCreate() throws Exception{		
			ITable testCreate = dataSetTest.getTable("testCreate");
			 int dealId=Integer.valueOf((String)testCreate.getValue(0,"DEALID"));	
			 int copyId=Integer.valueOf((String)testCreate.getValue(0,"COPYID"));	
			 DealPK pk=new DealPK(dealId,copyId);
			 srk.beginTransaction();
			 srk.getExpressState().setDealInstitutionId(1);
			 dealNotes=dealNotes.create(pk);
		     assertEquals(dealId, dealNotes.getDealId());
		     srk.rollbackTransaction();
	}  
	
	
	public void testCreatePk() throws Exception{		
		ITable testCreate = dataSetTest.getTable("testCreatePk");
		 int dealId=Integer.valueOf((String)testCreate.getValue(0,"DEALID"));	
		 int copyId=Integer.valueOf((String)testCreate.getValue(0,"COPYID"));	
		 int applicationId=Integer.valueOf((String)testCreate.getValue(0,"APPLICATIONID"));	
		 int category=Integer.valueOf((String)testCreate.getValue(0,"DEALNOTESCATEGORYID"));
		 int userId=Integer.valueOf((String)testCreate.getValue(0,"DEALNOTESUSERID"));	
		 String noteText =(String)testCreate.getValue(0,"DEALNOTESTEXT");
		 int langId=Integer.valueOf((String)testCreate.getValue(0,"LANGUAGEPREFERENCEID"));
		 DealPK pk=new DealPK(dealId,copyId);
		 srk.beginTransaction();
		 srk.getExpressState().setDealInstitutionId(1);
		 dealNotes=dealNotes.create(pk,applicationId,category,userId,noteText,langId);
	     assertEquals(dealId, dealNotes.getDealId());
	     srk.rollbackTransaction();
}
	
	
	public void testCreateSecond() throws Exception{		
		ITable testCreate = dataSetTest.getTable("testCreateSecond");
		 int dealId=Integer.valueOf((String)testCreate.getValue(0,"DEALID"));	
		 int copyId=Integer.valueOf((String)testCreate.getValue(0,"COPYID"));	
		 int applicationId=Integer.valueOf((String)testCreate.getValue(0,"APPLICATIONID"));	
		 int category=Integer.valueOf((String)testCreate.getValue(0,"DEALNOTESCATEGORYID"));
		 String noteText =(String)testCreate.getValue(0,"DEALNOTESTEXT");				 
		 int userId=Integer.valueOf((String)testCreate.getValue(0,"DEALNOTESUSERID"));
		 DealPK pk=new DealPK(dealId,copyId);
		 srk.beginTransaction();
		 srk.getExpressState().setDealInstitutionId(1);
		 dealNotes=dealNotes.create(pk,applicationId,category,noteText,null,userId);
	     assertEquals(dealId, dealNotes.getDealId());
	     srk.rollbackTransaction();
}
	
	public void testCreateThird() throws Exception{		
		ITable testCreate = dataSetTest.getTable("testCreateThird");
		 int dealId=Integer.valueOf((String)testCreate.getValue(0,"DEALID"));	
		 int copyId=Integer.valueOf((String)testCreate.getValue(0,"COPYID"));	
		 int applicationId=Integer.valueOf((String)testCreate.getValue(0,"APPLICATIONID"));	
		 int category=Integer.valueOf((String)testCreate.getValue(0,"DEALNOTESCATEGORYID"));
		 String noteText =(String)testCreate.getValue(0,"DEALNOTESTEXT");		
		 DealPK pk=new DealPK(dealId,copyId);
		 srk.beginTransaction();
		 srk.getExpressState().setDealInstitutionId(1);
		 dealNotes=dealNotes.create(pk,applicationId,category,noteText);
	     assertEquals(dealId, dealNotes.getDealId());
	     srk.rollbackTransaction();
}
	
	
	
	public void testCreateFourth() throws Exception{
		Formattable fmtTable=null;		
		ITable testCreate = dataSetTest.getTable("testCreateFourth");
		 int dealId=Integer.valueOf((String)testCreate.getValue(0,"DEALID"));	
		 int copyId=Integer.valueOf((String)testCreate.getValue(0,"COPYID"));		
		 int category=Integer.valueOf((String)testCreate.getValue(0,"DEALNOTESCATEGORYID"));
		 int userId=Integer.valueOf((String)testCreate.getValue(0,"DEALNOTESUSERID"));				
		 Deal deal=new Deal(srk,null,dealId,copyId);
		 srk.beginTransaction();
		 srk.getExpressState().setDealInstitutionId(1);
		 dealNotes.create(deal,category,userId,fmtTable);
	     assertEquals(dealId, dealNotes.getDealId());
	     srk.rollbackTransaction();
}
	
	
	public void testCreateFormattable() throws Exception{
		Formattable fmtTable=null;		
		ITable testCreate = dataSetTest.getTable("testCreateFormattable");
		 int dealId=Integer.valueOf((String)testCreate.getValue(0,"DEALID"));	
		 int copyId=Integer.valueOf((String)testCreate.getValue(0,"COPYID"));
		 int applicationId=Integer.valueOf((String)testCreate.getValue(0,"APPLICATIONID"));	
		 int categId=Integer.valueOf((String)testCreate.getValue(0,"DEALNOTESCATEGORYID"));
		 int userId=Integer.valueOf((String)testCreate.getValue(0,"DEALNOTESUSERID"));		
		 DealPK pk=new DealPK(dealId,copyId);
		 srk.beginTransaction();
		 srk.getExpressState().setDealInstitutionId(1);
		 dealNotes.create(pk,applicationId,categId,userId,fmtTable);
	     assertEquals(dealId, dealNotes.getDealId());
	     srk.rollbackTransaction();
}
	
	

	
	public void testFindByDealLang() throws Exception{
	 Collection cList=null;		
	ITable testFindByDealLang = dataSetTest.getTable("testFindByDealLang");
	 int dealId=Integer.valueOf((String)testFindByDealLang.getValue(0,"DEALID"));	
	 int copyId=Integer.valueOf((String)testFindByDealLang.getValue(0,"COPYID"));
	 int langId=Integer.valueOf((String)testFindByDealLang.getValue(0,"LANGUAGEPREFERENCEID"));
	 DealPK pk=new DealPK(dealId,copyId);
	 srk.getExpressState().setDealInstitutionId(1);
	 cList=dealNotes.findByDealLang(pk,langId);
     assertNotNull(cList);
}
	
	public void testFindByDealLangFailure() throws Exception{
		 Collection cList=null;		
		ITable testFindByDealLang = dataSetTest.getTable("testFindByDealLangFailure");
		 int dealId=Integer.valueOf((String)testFindByDealLang.getValue(0,"DEALID"));	
		 int copyId=Integer.valueOf((String)testFindByDealLang.getValue(0,"COPYID"));
		 int langId=Integer.valueOf((String)testFindByDealLang.getValue(0,"LANGUAGEPREFERENCEID"));
		 DealPK pk=new DealPK(dealId,copyId);
		 srk.getExpressState().setDealInstitutionId(1);
		 cList=dealNotes.findByDealLang(pk,langId);
		 assertEquals(0, cList.size());	     
	}
	
		
	public void testFindBySqlColl() throws Exception{
		 Collection cList=null;		
		ITable testFindBySqlColl = dataSetTest.getTable("testFindBySqlColl");	
		int dealNoteId=Integer.valueOf((String)testFindBySqlColl.getValue(0,"DEALNOTESID"));
		 String sql="Select * from DealNotes where DEALNOTESID="+dealNoteId;		 
		 srk.getExpressState().setDealInstitutionId(1);
		 cList=dealNotes.findBySqlColl(sql);
		 assertNotNull(cList);
	}
	
	
	public void testFindBySqlCollFailure() throws Exception{
		 Collection cList=null;		
		ITable testFindBySqlColl = dataSetTest.getTable("testFindBySqlCollFailure");	
		int dealNoteId=Integer.valueOf((String)testFindBySqlColl.getValue(0,"DEALNOTESID"));
		 String sql="Select * from DealNotes where DEALNOTESID="+dealNoteId;		 
		 srk.getExpressState().setDealInstitutionId(1);
		 cList=dealNotes.findBySqlColl(sql);
		 assertEquals(0, cList.size());
	}
	
	
	public void testFindByPrimaryKey() throws Exception{		
		ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKey");	
		int dealNoteId=Integer.valueOf((String)testFindByPrimaryKey.getValue(0,"DEALNOTESID"));		
		 srk.getExpressState().setDealInstitutionId(1);
		 DealNotesPK pk=new DealNotesPK(dealNoteId);
		 dealNotes=dealNotes.findByPrimaryKey(pk);
		 assertEquals(dealNoteId, pk.getId());
	}
}
