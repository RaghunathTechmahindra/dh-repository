package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.RequestPK;

public class AdjudicationRequestTest1 extends  FXDBTestCase{

	private IDataSet dataSetTest;
	AdjudicationRequest adjudicationRequest=null;
	RequestPK requestPK=null;
	DealPK dealPK=null;
	public AdjudicationRequestTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("AdjudicationRequestDataSetTest.xml"));
	}

	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		adjudicationRequest=new AdjudicationRequest(srk);
		
	}
	public void testFindByRequest() throws Exception {
		ITable testFindByRequest = dataSetTest.getTable("testFindByRequest");
		int  id = Integer.parseInt(testFindByRequest.getValue(0,"Id").toString());
		int  copyId = Integer.parseInt(testFindByRequest.getValue(0,"CopyId").toString());
		requestPK=new RequestPK(id, copyId);
		int size=adjudicationRequest.findByRequest(requestPK).size();	
		assertNotSame(0,size);
	}

	public void testFindByRequestFailure() throws Exception {
		ITable testFindByRequestFailure = dataSetTest.getTable("testFindByRequestFailure");
		int  id = Integer.parseInt(testFindByRequestFailure.getValue(0,"Id").toString());
		int  copyId = Integer.parseInt(testFindByRequestFailure.getValue(0,"CopyId").toString());
		requestPK=new RequestPK(id, copyId);
		int size=adjudicationRequest.findByRequest(requestPK).size();
		assertSame(0,size);
		
	}
	
	public void testGetSAMRequests() throws Exception {
		
		ITable testGetSAMRequests = dataSetTest.getTable("testGetSAMRequests");
		int  id = Integer.parseInt(testGetSAMRequests.getValue(0,"Id").toString());
		adjudicationRequest=new AdjudicationRequest(srk,id);
		int size=adjudicationRequest.getSAMRequests().size();		
		assertSame(1,size);
	}

	public void testGetSAMRequestsFailure() throws Exception {
		ITable testGetSAMRequests = dataSetTest.getTable("testGetSAMRequestsFailure");
		int  id = Integer.parseInt(testGetSAMRequests.getValue(0,"Id").toString());
		adjudicationRequest=new AdjudicationRequest(srk,id);
		int size=adjudicationRequest.getSAMRequests().size();
		assertNotSame(0, size);
	}

	public void testFindByDeal() throws Exception {
		ITable testFindByRequestFailure = dataSetTest.getTable("testFindByDeal");
		int  id = Integer.parseInt(testFindByRequestFailure.getValue(0,"Id").toString());
		int  copyId = Integer.parseInt(testFindByRequestFailure.getValue(0,"CopyId").toString());
		dealPK=new DealPK(id, copyId);
		int size=adjudicationRequest.findByDeal(dealPK).size();
		assertSame(1,size);
	}

	public void testFindByDealFailure() throws Exception {
		
		ITable testFindByRequestFailure = dataSetTest.getTable("testFindByDealFailure");
		int  id = Integer.parseInt(testFindByRequestFailure.getValue(0,"Id").toString());
		int  copyId = Integer.parseInt(testFindByRequestFailure.getValue(0,"CopyId").toString());
		dealPK=new DealPK(id, copyId);
		int size=adjudicationRequest.findByDeal(dealPK).size();
		assertNotSame(0,size);
	}

	public void testFindByLastRecForThisDeal() throws Exception {
		ITable testFindByLastRecForThisDeal = dataSetTest.getTable("testFindByLastRecForThisDeal");
		int  id = Integer.parseInt(testFindByLastRecForThisDeal.getValue(0,"Id").toString());
		int  copyId = Integer.parseInt(testFindByLastRecForThisDeal.getValue(0,"CopyId").toString());
		dealPK=new DealPK(id, copyId);
		AdjudicationRequest adjudicationRequest1=adjudicationRequest.findByLastRecForThisDeal(dealPK);
		assertNotNull(adjudicationRequest1);
	}
	public void testFindByLastRecForThisDealFailure() throws Exception {
		
		ITable testFindByLastRecForThisDealFailure = dataSetTest.getTable("testFindByLastRecForThisDealFailure");
		int  id = Integer.parseInt(testFindByLastRecForThisDealFailure.getValue(0,"Id").toString());
		int  copyId = Integer.parseInt(testFindByLastRecForThisDealFailure.getValue(0,"CopyId").toString());
		dealPK=new DealPK(id, copyId);
		AdjudicationRequest adjudicationRequest1=adjudicationRequest.findByLastRecForThisDeal(dealPK);
		assertNull(adjudicationRequest1);
		
	}

}
