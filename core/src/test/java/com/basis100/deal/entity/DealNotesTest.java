/*
 * @(#) DealNotesTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */


package com.basis100.deal.entity;

import oracle.sql.CLOB;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.docprep.DocPrepLanguage;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.DealNotesPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>DealNotesTest</p>
 * Express Entity class unit test: DealNotes
 */
public class DealNotesTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(DealNotesTest.class);

    /**
     * Constructor function
     */
    public DealNotesTest() {
    }

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        CLOB dealNotesText = null;
        
        // get input data from repository
        int dealNotesId = _dataRepository.getInt("DEALNOTES", "DEALNOTESID", 0);
        int dealNotesUserId = _dataRepository.getInt("DEALNOTES", "DEALNOTESUSERID", 0);
        int instProfId = _dataRepository.getInt("DEALNOTES",
                "INSTITUTIONPROFILEID", 0);
        dealNotesText = (CLOB)_dataRepository.getClob("DEALNOTES",
                "DEALNOTESTEXT", 0);


        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed the input data and run the program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        DealNotes entity = new DealNotes(srk);
        DealNotesPK dpk = new DealNotesPK(dealNotesId);
        entity = entity.findByPrimaryKey(dpk);

        // verify the running result.
        assertEquals(dealNotesId, entity.getDealNotesId());
        assertEquals(dealNotesUserId, entity.getDealNotesUserId());
        assertEquals(dealNotesText.getSubString(1, (int)dealNotesText.length()), entity.getDealNotesText());

        // free resources
        srk.freeResources();
    }

    /**
     * test create.
     */
    @Test
    public void testCreate1() throws Exception {

        // get input data from repository
        int dealNotesId = _dataRepository.getInt("DEALNOTES", "DEALNOTESID", 0);
        int dealId = _dataRepository.getInt("DEALNOTES", "DEALID", 0);
        int instProfId = _dataRepository.getInt("DEALNOTES",
                "INSTITUTIONPROFILEID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed input data and run program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        srk.beginTransaction();
        DealNotes newEntity = new DealNotes(srk);
        DealPK dpk = new DealPK(dealId, 0);
        newEntity = newEntity.create(dpk);
        newEntity.ejbStore();

        // check create correctlly.
        DealNotes foundEntity = new DealNotes(srk);
        DealNotesPK dnpk = new DealNotesPK(dealNotesId);
        foundEntity = foundEntity.findByPrimaryKey(dnpk);

        // verifying
        assertEquals(foundEntity.getDealId(), newEntity.getDealId());
        // rollback transaction.
        srk.rollbackTransaction();
        // free resources.
        srk.freeResources();
    }

    /**
     * test create.
     */
    @Test
    public void testCreate2() throws Exception {

        CLOB dealNotesText = null;
        
        // get input data from repository
        int dealNotesId = _dataRepository.getInt("DEALNOTES", "DEALNOTESID", 0);
        int dealId = _dataRepository.getInt("DEALNOTES", "DEALID", 0);
        int applicationId = _dataRepository.getInt("DEALNOTES", "APPLICATIONID", 0);
        int dealnotesCategoryId = _dataRepository.getInt("DEALNOTES",
                "DEALNOTESCATEGORYID", 0);
        dealNotesText = (CLOB)_dataRepository.getClob("DEALNOTES",
                "DEALNOTESTEXT", 0);
        int instProfId = _dataRepository.getInt("DEALNOTES",
                "INSTITUTIONPROFILEID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed input data and run program.
        srk.getExpressState().setDealInstitutionId(instProfId);

        srk.beginTransaction();
        DealNotes newEntity = new DealNotes(srk);
        DealPK dpk = new DealPK(dealId, 0);
        _logger.info("instituionid: " + instProfId);
        newEntity = newEntity.create(dpk, applicationId, dealnotesCategoryId, dealNotesText.getSubString(1, (int)dealNotesText.length()));
        newEntity.ejbStore();

        // check create correctlly.
        DealNotes foundEntity = new DealNotes(srk);
        DealNotesPK dnpk = new DealNotesPK(dealNotesId);
        foundEntity = foundEntity.findByPrimaryKey(dnpk);

        // verifying
        assertEquals(foundEntity.getDealId(), newEntity.getDealId());
        // rollback transaction.
        srk.rollbackTransaction();
        // free resources.
        srk.freeResources();
    }

    /**
     * test create.
     */
    @Test
    public void testCreate3() throws Exception {

        CLOB dealNotesText = null;
        // get input data from repository
        int dealNotesId = _dataRepository.getInt("DEALNOTES", "DEALNOTESID", 0);
        int dealNotesUserId = _dataRepository.getInt("DEALNOTES", "DEALNOTESUSERID", 0);
        int dealId = _dataRepository.getInt("DEALNOTES", "DEALID", 0);
        int applicationId = _dataRepository.getInt("DEALNOTES", "APPLICATIONID", 0);
        int dealnotesCategoryId = _dataRepository.getInt("DEALNOTES",
                "DEALNOTESCATEGORYID", 0);
        dealNotesText = (CLOB)_dataRepository.getClob("DEALNOTES",
                "DEALNOTESTEXT", 0);
        Date dealNotesDate = _dataRepository.getDate("DEALNOTES", "DEALNOTESDATE", 0);
        String dateStr = dealNotesDate.toString();
        int instProfId = _dataRepository.getInt("DEALNOTES",
                "INSTITUTIONPROFILEID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed input data and run program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        srk.beginTransaction();
        DealNotes newEntity = new DealNotes(srk);
        DealPK dpk = new DealPK(dealId, 0);
        newEntity = newEntity.create(dpk, applicationId, dealnotesCategoryId,
                dealNotesText.getSubString(1, (int)dealNotesText.length()), dateStr, dealNotesUserId);
        newEntity.ejbStore();

        // check create correctlly.
        DealNotes foundEntity = new DealNotes(srk);
        DealNotesPK dnpk = new DealNotesPK(dealNotesId);
        foundEntity = foundEntity.findByPrimaryKey(dnpk);

        // verifying
        assertEquals(foundEntity.getDealId(), newEntity.getDealId());
        // rollback transaction.
        srk.rollbackTransaction();
        // free resources.
        srk.freeResources();
    }
    
    /**
     * In this scenario, we leverage existing data in db.
     * @throws Exception
     *//*
    @Test
    public void testFindLastDealNotesByCategoryId() throws Exception {
    	int sequence = 0;
    	int categoryId = Mc.DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS;
    	int dealId = _dataRepository.getInt("DEALNOTES", "DEALID", sequence);
    	int copyId = _dataRepository.getInt("Deal", "CopyId", sequence);
        int instProfId = _dataRepository.getInt("DEALNOTES",
                "INSTITUTIONPROFILEID", sequence);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();

        // feed input data and run program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        
        //Get the deal.
        Deal deal = new Deal(srk, null, dealId, copyId);
        
        //Use the target method to find the targeted dealnotes id.
        DealNotes dnFinder = new DealNotes(srk);
        //Language Preference.
		DocPrepLanguage dpl = DocPrepLanguage.getInstance();
	    int languageId = dpl.findPreferredLanguageId(deal, deal.getSessionResourceKit());
        DealNotes dealNotesResult = dnFinder.
        findLastDealNotesByDealCategory(dealId, languageId, categoryId);
        int actualId = null==dealNotesResult?-1:dealNotesResult.getDealNotesId();
        
        //Get deal notes from deal and find the one with maximum dealNotesId.
        Collection<DealNotes> dealNotes = deal.getDealNotes();
        Iterator<DealNotes> dealNotesIt = dealNotes.iterator();
        int maxId = -1;
        while(dealNotesIt.hasNext()) {
        	DealNotes cur = dealNotesIt.next();
        	if(cur.getDealNotesId() > maxId 
        			&& (cur.getLanguagePreferenceId() == languageId
        					||cur.getLanguagePreferenceId() == Mc.LANGUAGE_UNKNOW)
        			&& cur.getDealNotesCategoryId() == categoryId)
        		maxId = cur.getDealNotesId(); 
        }
        assertEquals(actualId, maxId);
        srk.freeResources();
    }
    
    *//**
     * In this scenario, we manually insert a new dealNotes.
     * @throws Exception
     *//*
    @Test
    public void testFindLastDealNotesByCategoryId2() throws Exception {
    	int sequence = 0;
    	int categoryId = Mc.DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS;
    	int dealId = _dataRepository.getInt("DEALNOTES", "DEALID", sequence);
    	int copyId = _dataRepository.getInt("Deal", "CopyId", sequence);
        int instProfId = _dataRepository.getInt("DEALNOTES",
                "INSTITUTIONPROFILEID", sequence);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();

        // feed input data and run program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        
        srk.beginTransaction();
        //Get the deal.
        Deal deal = new Deal(srk, null, dealId, copyId);
        //Language Preference.
		DocPrepLanguage dpl = DocPrepLanguage.getInstance();
	    int languageId = dpl.findPreferredLanguageId(deal, deal.getSessionResourceKit());
        
        DealNotes dnFinder = new DealNotes(srk);
        
        //Create the new dealNotes.
        DealPK dpk = new DealPK(dealId, 0);
        DealNotes newEntity = dnFinder.create(dpk);
        newEntity.setDealNotesCategoryId(categoryId);
        newEntity.setDealNotesText("This notes should not persist in db.");
        newEntity.setLanguagePreferenceId(languageId);
        newEntity.ejbStore();
        int newEntityId = newEntity.getDealNotesId();
        
        //Use the target method to find the targeted dealnotes id.
        DealNotes dealNotesResult = dnFinder.
        findLastDealNotesByDealCategory(dealId, languageId, categoryId);
        int actualId = null==dealNotesResult?-1:dealNotesResult.getDealNotesId();

        srk.rollbackTransaction();
        assertEquals(actualId, newEntityId);
        srk.freeResources();
        
    }*/
}
