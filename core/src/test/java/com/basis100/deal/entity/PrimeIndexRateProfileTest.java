/*
 * @(#) PrimeIndexRateProfileTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filogix.express.test.UnitTestLogging;
import com.filogix.express.test.ExpressEntityTestCase;

import com.basis100.deal.pk.PrimeIndexRateProfilePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

/**
 * @author amalhi
 *
 * JUnit test for entity PrimeIndexRateProfile
 *
 */
public class PrimeIndexRateProfileTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(PrimeIndexRateProfileTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public PrimeIndexRateProfileTest() {

    }

    /**
     * To be executed before test
     */    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env and free resources
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int primeIndexRateProfileId = _dataRepository.getInt(
                "PrimeIndexRateProfile", "primeIndexRateProfileId", 0);
        int instProfId = _dataRepository.getInt("PrimeIndexRateProfile",
                "INSTITUTIONPROFILEID", 0);
        String primeIndexDesc = _dataRepository.getString(
                "PrimeIndexRateProfile", "primeIndexDescription", 0);

        _logger.info(BORDER_START, "findByPrimaryKey");
        _logger
                .info(
                        "Got Values from repository for PrimeIndexRateProfile ::: [{}]",
                        primeIndexRateProfileId + "::" + instProfId + "::"
                                + primeIndexDesc);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        PrimeIndexRateProfile _primeIndexRateProfile = new PrimeIndexRateProfile(
                _srk);

        // Find by primary key
        _primeIndexRateProfile = _primeIndexRateProfile
                .findByPrimaryKey(new PrimeIndexRateProfilePK(
                        primeIndexRateProfileId));

        _logger.info(
                "Got Values from new found PrimeIndexRateProfile ::: [{}]",
                _primeIndexRateProfile.getPrimeIndexRateProfileId() + "::"
                        + _primeIndexRateProfile.getPrimeIndexDescription());

        // Check if the data retrieved matches the DB
        assertTrue(_primeIndexRateProfile.getPrimeIndexRateProfileId() == primeIndexRateProfileId);
        assertTrue(_primeIndexRateProfile.getPrimeIndexDescription().equals(
                primeIndexDesc));

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * test create method of entity
     */
    @Ignore
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create");

        // get input data from repository
        int instProfId = _dataRepository.getInt("PrimeIndexRateProfile",
                "INSTITUTIONPROFILEID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

		//begin transaction
        _srk.beginTransaction();

        // Create the initial entity
        PrimeIndexRateProfile newEntity = new PrimeIndexRateProfile(_srk);
        
		//call create method of entity
		newEntity = newEntity.create(newEntity.createPrimaryKey());

        _logger.info("CREATED PrimeIndexRateProfile ::: [{}]", newEntity
                .getPrimeIndexRateProfileId()
                + "::" + newEntity.getPrimeIndexDescription());

        // check create correctly by calling findByPrimaryKey
        PrimeIndexRateProfile foundEntity = new PrimeIndexRateProfile(_srk);
        foundEntity = foundEntity.findByPrimaryKey(new PrimeIndexRateProfilePK(
                newEntity.getPrimeIndexRateProfileId()));

		// verifying the created entity
		assertEquals(foundEntity.getPrimeIndexRateProfileId(), newEntity
                .getPrimeIndexRateProfileId());
        assertEquals(foundEntity.getPrimeIndexDescription(), newEntity
                .getPrimeIndexDescription());

        _logger.info(BORDER_END, "Create");

        // roll-back transaction.
        _srk.rollbackTransaction();

    }

}
