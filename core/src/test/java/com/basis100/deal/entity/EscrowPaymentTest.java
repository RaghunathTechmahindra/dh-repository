/*
 * @(#) EscrowPaymentTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */


package com.basis100.deal.entity;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.EscrowPaymentPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>EscrowPaymentTest</p>
 * Express Entity class unit test: EscrowPayment
 */
public class EscrowPaymentTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(EscrowPaymentTest.class);

    /**
     * Constructor function
     */
    public EscrowPaymentTest() {
    }

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int escrowPaymentId = _dataRepository.getInt("ESCROWPAYMENT",
                "ESCROWPAYMENTID", 0);
        int copyId = _dataRepository.getInt("ESCROWPAYMENT", "COPYID", 0);
        int instProfId = _dataRepository.getInt("ESCROWPAYMENT",
                "INSTITUTIONPROFILEID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed the input data and run the program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        EscrowPayment entity = new EscrowPayment(srk);
        entity = entity.findByPrimaryKey(new EscrowPaymentPK(escrowPaymentId, copyId));

        // verify the running result.
        assertEquals(escrowPaymentId, entity.getEscrowPaymentId());

        // free resources
        srk.freeResources();
    }

    /**
     * test create.
     */
    @Test
    public void testCreate() throws Exception {

        // get input data from repository
        int instProfId = _dataRepository.getInt("ESCROWPAYMENT",
                "INSTITUTIONPROFILEID", 0);
        int dealId = _dataRepository.getInt("ESCROWPAYMENT", "DEALID", 0);
        int copyId = _dataRepository.getInt("ESCROWPAYMENT", "COPYID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed input data and run program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        srk.beginTransaction();
        EscrowPayment newEntity = new EscrowPayment(srk);
        newEntity = newEntity.create(new DealPK(dealId, copyId));
        newEntity.ejbStore();

        // check create correctlly.
        EscrowPayment foundEntity = new EscrowPayment(srk);
        foundEntity = foundEntity.findByPrimaryKey(new EscrowPaymentPK(
                newEntity.getEscrowPaymentId(), newEntity.getCopyId()));

        // verifying
        assertEquals(foundEntity.getEscrowPaymentId(), newEntity.getEscrowPaymentId());

        // rollback transaction.
        srk.rollbackTransaction();
        // free resources.
        srk.freeResources();
    }

}
