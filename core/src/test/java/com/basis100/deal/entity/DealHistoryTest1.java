package com.basis100.deal.entity;

import java.io.File;
import java.io.IOException;
import java.sql.Clob;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.AppraisalOrderPK;
import com.basis100.deal.pk.DealEventStatusSnapshotPK;
import com.basis100.deal.pk.DealEventStatusUpdateAssocPK;
import com.basis100.deal.pk.DealHistoryPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.ltx.unittest.util.EntityTestUtil;

public class DealHistoryTest1 extends FXDBTestCase{

	private IDataSet dataSetTest;	
	private DealHistory dealHistory;
	Clob clob=null;
	
	
	public DealHistoryTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(DealHistory.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(DealHistory.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		dealHistory = new DealHistory(srk);
	}	
	
	public void testCreate() throws Exception{		
			ITable testCreate = dataSetTest.getTable("testCreate");
			 int dealId=Integer.valueOf((String)testCreate.getValue(0,"DEALID"));	
			 int userProfileId=Integer.valueOf((String)testCreate.getValue(0,"USERPROFILEID"));
			 int transactionTypeId=Integer.valueOf((String)testCreate.getValue(0,"TRANSACTIONTYPEID"));
			 int statusId=Integer.valueOf((String)testCreate.getValue(0,"STATUSID"));
			 String transactionText=(String)testCreate.getValue(0,"TRANSACTIONTEXT");		
			 srk.beginTransaction();
			 srk.getExpressState().setDealInstitutionId(1);
			 dealHistory=dealHistory.create(dealId,userProfileId,transactionTypeId,statusId,transactionText,null);
		     assertEquals(dealId, dealHistory.getDealId());
		     srk.rollbackTransaction();
		     
	}  
	
	
	
	public void testFindLatestUpdate() throws Exception{		
		ITable testFindLatestUpdate = dataSetTest.getTable("testFindLatestUpdate");
		 int dealId=Integer.valueOf((String)testFindLatestUpdate.getValue(0,"DEALID"));		 
		 srk.getExpressState().setDealInstitutionId(1);
		 dealHistory=dealHistory.findLatestUpdate(dealId);
	     assertEquals(dealId, dealHistory.getDealId());
}  
	

	public void testFindByDeal() throws Exception{
		Vector vList=null;
		ITable testFindLatestUpdate = dataSetTest.getTable("testFindByDeal");
		 int dealId=Integer.valueOf((String)testFindLatestUpdate.getValue(0,"DEALID"));		 
		 srk.getExpressState().setDealInstitutionId(1);
		 vList=dealHistory.findByDeal(dealId);
	     assertNotNull(vList);
}
	
	public void testFindByDealPk() throws Exception{
		Collection cList=null;
		ITable testFindLatestUpdate = dataSetTest.getTable("testFindByDealPk");
		 int dealId=Integer.valueOf((String)testFindLatestUpdate.getValue(0,"DEALID"));		
		 int copyId=Integer.valueOf((String)testFindLatestUpdate.getValue(0,"COPYID"));	
		 srk.getExpressState().setDealInstitutionId(1);
		 DealPK pk=new DealPK(dealId,copyId);
		 cList=dealHistory.findByDeal(pk);
		 assertEquals(dealId, pk.getId());
}
	
		
	public void testCreatePrimaryKey() throws Exception{
		
		ITable testFindLatestUpdate = dataSetTest.getTable("testCreatePrimaryKey");
		 int dealHistoryId=Integer.valueOf((String)testFindLatestUpdate.getValue(0,"DEALHISTORYID"));			
		 srk.getExpressState().setDealInstitutionId(1);
		 DealHistoryPK pk=new DealHistoryPK(dealHistoryId);
		 dealHistory.setDealHistoryId(dealHistoryId);
		 pk=dealHistory.createPrimaryKey();
		 assertEquals("dealHistoryId",pk.getPKName());		
}
	
}
