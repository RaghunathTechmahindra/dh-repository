/*
 * @(#)BridgeTest.java    2005-3-16
 *
 */

package com.basis100.deal.entity;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.basis100.deal.pk.BridgePK;
import com.basis100.deal.pk.DealPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;

import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.test.ExpressEntityTestCase;

/**
 * BridgeTest -
 * 
 * @version 1.0 2005-3-16
 * @author <A HREF="mailto:chyxiang@yahoo.com">Chen Xiang (Sean)</A>
 */
public class BridgeTest extends ExpressEntityTestCase {

    // The logger
    private static Log _log = LogFactory.getLog(BridgeTest.class);

    // The SessionResourceKit
    private SessionResourceKit _srk;

    // The Bridge entity
    private Bridge _bridgeDAO;

    // The institutionId for deal to setup vpd context
    private int dealInstitutionId;
    private int dealId;
    private int copyId;

    /**
     * Constructor function
     */
    public BridgeTest() {
    }

    /**
     * setup.
     * 
     * @throws FinderException
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {

        _log.debug("Preparing the test cases ...");
        // initialize resource and srk
        ResourceManager.init();
        _srk = new SessionResourceKit("Testing");
        
        // get values from database
        dealInstitutionId = _dataRepository.getInt("Deal", 
                "InstitutionProfileId", 0);
        dealId = _dataRepository.getInt("Deal", 
                "DealId", 0);
        copyId = _dataRepository.getInt("Deal", 
                "CopyId", 0);

        _bridgeDAO = new Bridge(_srk, null);

    }

    /**
     * test get by primary key.
     */
    @Ignore("no sequence for this entity")
    public void testFindByPrimaryKey() throws Exception {

        // set institutionid
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        DealPK dpk = new DealPK(dealId, copyId);
        Bridge aBridge = _bridgeDAO.create(dpk);

        _log.info("Found Bridge: ");
        _log.info("      Net Interest Rate: " + aBridge.getNetInterestRate());
        _log.info("        Net Loan Amount: " + aBridge.getNetLoanAmount());
        _log.info("                Deal ID: " + aBridge.getDealId());
        _log.info("====================================");

        // verify the result
        assertEquals(dealId, aBridge.getDealId(), 0);
    }

    /**
     * test create
     */
    @Ignore("no seq for this entity")
    public void testCreate() throws Exception {

        // set institutionId
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // get values from database
        int dealId = _dataRepository.getInt("Deal", "DealId", 0);
        int copyId = _dataRepository.getInt("Deal", "CopyId", 0);

        // create a bridge enity and set values
        DealPK dealPk = new DealPK(dealId, copyId);
        Bridge aBridge = _bridgeDAO.create(dealPk);
        aBridge.setActualPaymentTerm(25);
        aBridge.setMaturityDate(new Date());
        aBridge.setNetLoanAmount(135);
        aBridge.ejbStore();

        // find a birdge to verify the result
        Bridge newBridge = new Bridge(_srk, null);
        newBridge = newBridge.findByDeal(dealPk);
        assertEquals(aBridge.getBridgeId(), newBridge.getBridgeId());
        _log.info("Create Bridge: ");
        _log.info("    Actual Payment Term: " + aBridge.getActualPaymentTerm());
        _log.info("          Maturity Date: " + aBridge.getMaturityDate());
        _log.info("        Net Loan Amount: " + aBridge.getNetLoanAmount());
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        // free the srk
        _srk.freeResources();
    }
    
    @Test
    public void test() {
        _log.info("test method");
    }
}
