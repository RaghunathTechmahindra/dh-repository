package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.InsureOnlyApplicantPK;
import com.basis100.deal.pk.LifeDisabilityPremiumsPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;

public class LifeDisabilityPremiumsTest1 extends FXDBTestCase {
	private String INIT_XML_DATA = "LifeDisabilityPremiumsDataSet.xml";
	private LifeDisabilityPremiums lifeDisabilityPremiums;
	private IDataSet dataSetTest;

	public LifeDisabilityPremiumsTest1(String name) throws DataSetException,
			IOException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"LifeDisabilityPremiumsDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		// TODO Auto-generated method stub
		return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();

		lifeDisabilityPremiums = new LifeDisabilityPremiums(srk);

	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	public void testFindByMyCopies() throws RemoteException, FinderException,
			NumberFormatException, DataSetException {
		int lifeDisabilityPremiumsId = Integer.parseInt(dataSetTest
				.getTable("testFindByMyCopies")
				.getValue(0, "ifeDisabilityPremiumsId").toString());
		lifeDisabilityPremiums
				.setLifeDisabilityPremiumsId(lifeDisabilityPremiumsId);
		Collection list = lifeDisabilityPremiums.findByMyCopies();

		for (Object obj : list) {

			LifeDisabilityPremiums lifeDisabilityPremiums = (LifeDisabilityPremiums) obj;
			assertNotNull(lifeDisabilityPremiums);
			assertEquals(lifeDisabilityPremiumsId,
					lifeDisabilityPremiums.getLifeDisabilityPremiumsId());
		}

	}

	public void testFindByMyCopiesFailure() throws RemoteException,
			FinderException, NumberFormatException, DataSetException {
		int lifeDisabilityPremiumsId = Integer.parseInt(dataSetTest
				.getTable("testFindByMyCopiesFailure")
				.getValue(0, "ifeDisabilityPremiumsId").toString());
		lifeDisabilityPremiums
				.setLifeDisabilityPremiumsId(lifeDisabilityPremiumsId);
		Collection list = lifeDisabilityPremiums.findByMyCopies();
		assertEquals(0, list.size());

	}

	public void testCreateByPK() throws NumberFormatException,
			DataSetException, RemoteException, CreateException,
			JdbcTransactionException {
		srk.beginTransaction();
		int lifeDisabilityPremiumsId = Integer.parseInt(dataSetTest
				.getTable("testCreateByPK")
				.getValue(0, "lifeDisabilityPremiumsId").toString());
		int copyId = Integer.parseInt(dataSetTest.getTable("testCreateByPK")
				.getValue(0, "copyId").toString()); // LifeDisabilityPremiumsPK
		LifeDisabilityPremiumsPK lifeDisabilityPremiumsPK = new LifeDisabilityPremiumsPK(
				lifeDisabilityPremiumsId, copyId);
		LifeDisabilityPremiums lifeDisabilityPremiumsByPK = lifeDisabilityPremiums
				.create(lifeDisabilityPremiumsPK);
		assertEquals(lifeDisabilityPremiumsId,
				lifeDisabilityPremiumsByPK.getLifeDisabilityPremiumsId());
		srk.rollbackTransaction();
	}

	public void testCreateBorrowerPK() throws NumberFormatException,
			DataSetException, RemoteException, CreateException,
			JdbcTransactionException {
		srk.beginTransaction();
		int dealInstitutionId = Integer.parseInt(dataSetTest
				.getTable("testCreateBorrowerPK")
				.getValue(0, "dealInstitutionId").toString());
		int LDInsuranceTypeId = Integer.parseInt(dataSetTest
				.getTable("testCreateBorrowerPK")
				.getValue(0, "LDInsuranceTypeId").toString());
		int borrowerId = Integer.parseInt(dataSetTest
				.getTable("testCreateBorrowerPK").getValue(0, "borrowerId")
				.toString());
		int copyId = Integer.parseInt(dataSetTest
				.getTable("testCreateBorrowerPK").getValue(0, "copyId")
				.toString());
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		BorrowerPK bpk = new BorrowerPK(borrowerId, copyId);
		LifeDisabilityPremiums lifeDisabilityPremiumsByPK = lifeDisabilityPremiums
				.create(bpk, LDInsuranceTypeId);
		assertEquals(borrowerId, lifeDisabilityPremiumsByPK.getBorrowerId());
		srk.rollbackTransaction();
	}

	public void testCreateByInsureOnlyApplicantPK()
			throws NumberFormatException, DataSetException, RemoteException,
			CreateException, JdbcTransactionException {
		srk.beginTransaction();
		int dealInstitutionId = Integer.parseInt(dataSetTest
				.getTable("testCreateByInsureOnlyApplicantPK")
				.getValue(0, "dealInstitutionId").toString());
		int insureOnlyApplicantId = Integer.parseInt(dataSetTest
				.getTable("testCreateByInsureOnlyApplicantPK")
				.getValue(0, "insureOnlyApplicantId").toString());
		int copyId = Integer.parseInt(dataSetTest
				.getTable("testCreateByInsureOnlyApplicantPK")
				.getValue(0, "copyId").toString());
		int LDInsuranceTypeId = Integer.parseInt(dataSetTest
				.getTable("testCreateByInsureOnlyApplicantPK")
				.getValue(0, "LDInsuranceTypeId").toString());
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		InsureOnlyApplicantPK insureOnlyApplicantPK = new InsureOnlyApplicantPK(
				insureOnlyApplicantId, copyId);
		LifeDisabilityPremiums lifeDisabilityPremiumsByPK = lifeDisabilityPremiums
				.create(insureOnlyApplicantPK, LDInsuranceTypeId);
		assertEquals(LDInsuranceTypeId,
				lifeDisabilityPremiumsByPK.getLDInsuranceTypeId());
		srk.rollbackTransaction();
	}

}
