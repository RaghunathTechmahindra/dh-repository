package com.basis100.deal.entity;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IncomePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;

public class DealTest1 extends FXDBTestCase {
	private Deal deal;
	IDataSet dataSetTest;
	private Income inc;
	private DealEntity entity;
	private Liability liability;
	public DealTest1(String name) throws DataSetException, IOException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"DealDataSetTest.xml"));
	}


	@Override
	public void setUp() throws Exception {
		super.setUp();
		// srk.getExpressState().setDealInstitutionId(0);
		deal = new Deal(srk, CalcMonitor.getMonitor(srk));

	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	/*
	 * public void testCreatePrimaryKey() throws NumberFormatException,
	 * DataSetException, CreateException, JdbcTransactionException {
	 * srk.beginTransaction(); int copyId = Integer.parseInt(dataSetTest
	 * .getTable("testCreatePrimaryKey").getValue(0, "copyId") .toString());
	 * deal.createPrimaryKey(copyId, institutionId); srk.rollbackTransaction();
	 * 
	 * }
	 */
	public void testCreate() throws JdbcTransactionException,
			NumberFormatException, DataSetException, RemoteException,
			CreateException {
		srk.beginTransaction();
		int dealId = Integer.parseInt(dataSetTest.getTable("testCreate")
				.getValue(0, "id").toString());
		int copyId = Integer.parseInt(dataSetTest.getTable("testCreate")
				.getValue(0, "copyId").toString());
		srk.getExpressState().setDealInstitutionId(0);
		DealPK dealPK = new DealPK(dealId, copyId);
		deal.create(dealPK);
		srk.rollbackTransaction();
	}

	public void testFindBySouAppIdAndSysType() throws Exception {
		String sourceAppId = dataSetTest
				.getTable("testFindBySouAppIdAndSysType")
				.getValue(0, "sourceAppId").toString();
		int pSysTypeId = Integer.parseInt(dataSetTest
				.getTable("testFindBySouAppIdAndSysType")
				.getValue(0, "pSysTypeId").toString());
		String minDate = dataSetTest.getTable("testFindBySouAppIdAndSysType")
				.getValue(0, "minDate").toString();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy");
		Date convertedDate = dateFormat.parse(minDate);
		Collection list = deal.findBySouAppIdAndSysType(sourceAppId,
				pSysTypeId, convertedDate);
		for (Object obj : list) {
			Deal deal = (Deal) obj;
			assertEquals(sourceAppId, deal.getSourceApplicationId());
		}

	}

	public void testFindBySouAppIdAndSysTypeFailure() throws Exception {
		String sourceAppId = dataSetTest
				.getTable("testFindBySouAppIdAndSysTypeFailure")
				.getValue(0, "sourceAppId").toString();
		int pSysTypeId = Integer.parseInt(dataSetTest
				.getTable("testFindBySouAppIdAndSysTypeFailure")
				.getValue(0, "pSysTypeId").toString());
		String minDate = dataSetTest
				.getTable("testFindBySouAppIdAndSysTypeFailure")
				.getValue(0, "minDate").toString();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy");
		Date convertedDate = dateFormat.parse(minDate);
		Collection list = deal.findBySouAppIdAndSysType(null, pSysTypeId,
				convertedDate);
		assertEquals(0, list.size());

	}

	public void testFindByDCSourceApplicationId() throws Exception {
		String sourceAppId = dataSetTest
				.getTable("testFindBySouAppIdAndSysTypeFailure")
				.getValue(0, "sourceAppId").toString();
		String minDate = dataSetTest
				.getTable("testFindBySouAppIdAndSysTypeFailure")
				.getValue(0, "minDate").toString();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy");
		Date convertedDate = dateFormat.parse(minDate);
		Collection list = deal.findByDCSourceApplicationId(sourceAppId,
				convertedDate);
		for (Object obj : list) {
			Deal deal = (Deal) obj;
			assertEquals(sourceAppId, deal.getSourceApplicationId());
		}

	}

	public void testFindByDCSourceApplicationIdFailure() throws Exception {
		String sourceAppId = dataSetTest
				.getTable("testFindBySouAppIdAndSysTypeFailure")
				.getValue(0, "sourceAppId").toString();
		String minDate = dataSetTest
				.getTable("testFindBySouAppIdAndSysTypeFailure")
				.getValue(0, "minDate").toString();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy");
		Date convertedDate = dateFormat.parse(minDate);
		Collection list = deal.findByDCSourceApplicationId(sourceAppId,
				convertedDate);
		assertEquals(0, list.size());

	}

	public void testFindByDCSourceMailBox() throws Exception {
		String mailBoxNbr = dataSetTest.getTable("testFindByDCSourceMailBox")
				.getValue(0, "mailBoxNbr").toString();

		String minDate = dataSetTest.getTable("testFindByDCSourceMailBox")
				.getValue(0, "minDate").toString();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy");
		Date convertedDate = dateFormat.parse(minDate);
		Collection list = deal.findByDCSourceMailBox(mailBoxNbr, convertedDate);
		assertEquals(true, list.size() > 0);
		
		  for (Object obj : list) { Deal deal = (Deal) obj; assertEquals(true,
		  list.size()>0); }
		 

	}

	public void testFindByDCSourceMailBoxFailure() throws Exception {
		String mailBoxNbr = dataSetTest
				.getTable("testFindByDCSourceMailBoxFailure")
				.getValue(0, "mailBoxNbr").toString();

		String minDate = dataSetTest
				.getTable("testFindByDCSourceMailBoxFailure")
				.getValue(0, "minDate").toString();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy");
		Date convertedDate = dateFormat.parse(minDate);
		Collection list = deal.findByDCSourceMailBox(mailBoxNbr, convertedDate);
		assertEquals(false, list.size() > 0);

	}

	public void testFindByDupeCheckCriteria() throws Exception {
		String sourceMailBox = dataSetTest
				.getTable("testFindByDupeCheckCriteria")
				.getValue(0, "sourceMailBox").toString();
		String sourceAppId = dataSetTest
				.getTable("testFindByDupeCheckCriteria")
				.getValue(0, "sourceAppId").toString();
		String minDate = dataSetTest.getTable("testFindByDupeCheckCriteria")
				.getValue(0, "minDate").toString();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy");
		Date convertedDate = dateFormat.parse(minDate);
		Collection list = deal.findByDupeCheckCriteria(sourceMailBox,
				sourceAppId, convertedDate);
		for (Object obj : list) {
			Deal deal = (Deal) obj;
			assertEquals(true, list.size() > 0);
			assertEquals(sourceAppId, deal.getSourceApplicationId());
		}
	}

	public void testFindByDupeCheckCriteriaFailure() throws Exception {
		String sourceMailBox = dataSetTest
				.getTable("testFindByDupeCheckCriteriaFailure")
				.getValue(0, "sourceMailBox").toString();
		String sourceAppId = dataSetTest
				.getTable("testFindByDupeCheckCriteriaFailure")
				.getValue(0, "sourceAppId").toString();
		String minDate = dataSetTest
				.getTable("testFindByDupeCheckCriteriaFailure")
				.getValue(0, "minDate").toString();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy");
		Date convertedDate = dateFormat.parse(minDate);
		Collection list = deal.findByDupeCheckCriteria(sourceMailBox,
				sourceAppId, convertedDate);
		assertEquals(false, list.size() > 0);
		
		  for (Object obj : list) { Deal deal = (Deal) obj; assertEquals(true,
		  list.size() > 0); assertEquals(sourceAppId,
		  deal.getSourceApplicationId()); }
		 
	}

	public void testFindByGoldCopy() throws NumberFormatException,
			DataSetException, RemoteException, FinderException {
		int dealId = Integer.parseInt(dataSetTest
				.getTable("testFindByGoldCopy").getValue(0, "id").toString());
		Deal dealByGoldCopy = deal.findByGoldCopy(dealId);
		assertNotNull(dealByGoldCopy);
		assertEquals(dealId, dealByGoldCopy.getDealId());

	}

	public void testFindByGoldCopyPK() throws RemoteException, FinderException,
			NumberFormatException, DataSetException {
		int dealId = Integer.parseInt(dataSetTest
				.getTable("testFindByGoldCopyPK").getValue(0, "id").toString());
		int copyId = Integer.parseInt(dataSetTest
				.getTable("testFindByGoldCopyPK").getValue(0, "copyId")
				.toString());

		DealPK dealPK = new DealPK(dealId, copyId);
		Deal dealByPK = deal.findByGoldCopy(dealPK);
		assertNotNull(dealByPK);
		assertEquals(dealId, dealByPK.getDealId());

	}

	public void testFindByUnlockedScenario() throws RemoteException,
			FinderException, NumberFormatException, DataSetException {
		int dealId = Integer.parseInt(dataSetTest
				.getTable("testFindByUnlockedScenario").getValue(0, "id")
				.toString());
		int copyId = Integer.parseInt(dataSetTest
				.getTable("testFindByUnlockedScenario").getValue(0, "copyId")
				.toString());

		DealPK dealPK = new DealPK(dealId, copyId);
		Deal dealByPK = deal.findByUnlockedScenario(dealPK);
		assertNotNull(dealByPK);
		assertEquals(dealId, dealByPK.getDealId());
	}

	public void testFindByNonTransactionalRecommendedScenario()
			throws RemoteException, FinderException, NumberFormatException,
			DataSetException {
		int dealId = Integer.parseInt(dataSetTest
				.getTable("testFindByNonTransactionalRecommendedScenario")
				.getValue(0, "id").toString());
		Deal dealBYNonTransRec = deal
				.findByNonTransactionalRecommendedScenario(dealId);
		assertNotNull(dealBYNonTransRec);
		assertEquals(dealId, dealBYNonTransRec.getDealId());
	}

	public void testFindByHighestScenario() throws NumberFormatException,
			DataSetException, RemoteException, FinderException {
		int dealId = Integer.parseInt(dataSetTest
				.getTable("testFindByHighestScenario").getValue(0, "id")
				.toString());
		int copyId = Integer.parseInt(dataSetTest
				.getTable("testFindByHighestScenario").getValue(0, "copyId")
				.toString());
		DealPK dealPK = new DealPK(dealId, copyId);
		Deal dealByHighestScenario = deal.findByHighestScenario(dealPK);
		assertNotNull(dealByHighestScenario);
		assertEquals(dealId, dealByHighestScenario.getDealId());
	}

	public void testFindByNonTransactionalScenario()
			throws NumberFormatException, DataSetException, RemoteException,
			FinderException {
		int dealId = Integer.parseInt(dataSetTest
				.getTable("testFindByNonTransactionalScenario")
				.getValue(0, "id").toString());
		int scenarioNumber = Integer.parseInt(dataSetTest
				.getTable("testFindByNonTransactionalScenario")
				.getValue(0, "scenarioNumber").toString());
		Deal dealByNonTranScenario = deal.findByNonTransactionalScenario(
				scenarioNumber, dealId);
		assertNotNull(dealByNonTranScenario);
		assertEquals(dealId, dealByNonTranScenario.getDealId());
	}

	public void testFindByNonTransactional() throws FinderException,
			NumberFormatException, DataSetException {
		int dealId = Integer.parseInt(dataSetTest
				.getTable("testFindByNonTransactional").getValue(0, "id")
				.toString());
		Deal dealByNonTrans = deal.findByNonTransactional(dealId);
		assertNotNull(dealByNonTrans);
		assertEquals(dealId, dealByNonTrans.getDealId());
	}

	public void testFindBySelectableScenarios() throws DataSetException,
			RemoteException, FinderException {
		int dealId = Integer.parseInt(dataSetTest
				.getTable("testFindBySelectableScenarios").getValue(0, "id")
				.toString());
		boolean includeGold = Boolean.parseBoolean(dataSetTest
				.getTable("testFindBySelectableScenarios")
				.getValue(0, "includeGold").toString());
		boolean includeLocked = Boolean.parseBoolean(dataSetTest
				.getTable("testFindBySelectableScenarios")
				.getValue(0, "includeLocked").toString());
		Collection list = deal.findBySelectableScenarios(dealId, includeGold,
				includeLocked);
		for (Object obj : list) {
			Deal deal = (Deal) obj;
			assertEquals(true, list.size() > 0);
			assertEquals(dealId, deal.getDealId());
		}
	}

	public void testFindBySelectableScenariosFailure() throws DataSetException,
			RemoteException, FinderException {
		int dealId = Integer.parseInt(dataSetTest
				.getTable("testFindBySelectableScenariosFailure")
				.getValue(0, "id").toString());
		boolean includeGold = Boolean.parseBoolean(dataSetTest
				.getTable("testFindBySelectableScenariosFailure")
				.getValue(0, "includeGold").toString());
		boolean includeLocked = Boolean.parseBoolean(dataSetTest
				.getTable("testFindBySelectableScenariosFailure")
				.getValue(0, "includeLocked").toString());
		Collection list = deal.findBySelectableScenarios(dealId, includeGold,
				includeLocked);
		assertEquals(true, list.size() == 0);
	}

	public void testFindByAllUnlockedScenarios() throws NumberFormatException,
			DataSetException, RemoteException, FinderException {
		int dealId = Integer.parseInt(dataSetTest
				.getTable("testFindByAllUnlockedScenarios").getValue(0, "id")
				.toString());
		int copyId = Integer.parseInt(dataSetTest
				.getTable("testFindByAllUnlockedScenarios")
				.getValue(0, "copyId").toString());
		DealPK dealPK = new DealPK(dealId, copyId);
		Deal dealByUnlockScen = deal.findByUnlockedScenario(dealPK);
		assertNotNull(dealByUnlockScen);
		assertEquals(dealId, dealByUnlockScen.getDealId());
	}

	public void testFindByAsset() throws RemoteException, FinderException,
			NumberFormatException, DataSetException {
		Asset asset = new Asset(srk, CalcMonitor.getMonitor(srk));
		int borrowerId = Integer.parseInt(dataSetTest
				.getTable("testFindByAsset").getValue(0, "borrowerId")
				.toString());
		int copyId = Integer.parseInt(dataSetTest.getTable("testFindByAsset")
				.getValue(0, "copyId").toString());
		asset.borrowerId =1;
		asset.copyId = 21;
		Deal dealByAsset = deal.findByAsset(asset);
		// assertEquals(borrowerId, dealByAsset.getB);

	}
	
	public void testFindByRecommendedScenario() throws Exception {
		
		int dealId =Integer.parseInt(dataSetTest.getTable("testFindByRecommendedScenario").getValue(0, "id").toString());
        int copyId = Integer.parseInt(dataSetTest.getTable("testFindByRecommendedScenario").getValue(0, "copyId").toString());

        DealPK dpk = new DealPK(dealId, copyId);
        Deal dealbyRecommendedScenario =deal.findByRecommendedScenario(dpk, true);
		assertEquals("Y", dealbyRecommendedScenario.getScenarioRecommended());
      }
	
	public void testFindByIncome() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByIncome");
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "INCOMEID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	inc = new Income(srk, null, id, copyId);
		entity = (DealEntity) inc;
		Deal dl = deal.findByIncome(inc);
		assertNotNull(dl);
	
    	 
    }
	
	 
	
	
}
