/*
 * @(#)DealTest.java    2005-5-5
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */

package com.basis100.deal.entity;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.deal.pk.DealPK;

import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;

/**
 * DealTest -
 * 
 * @version 1.0 2005-5-5
 * @author <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class DealTest extends ExpressEntityTestCase {

    // The logger
    private static Log _log = LogFactory.getLog(DealTest.class);

    // the session resource kit.
    private SessionResourceKit _srk;

    // deal entity
    private Deal _dealDAO;

    // institutionId for deal
    private int dealInstitutionId;

    /**
     * Constructor function
     */
    public DealTest() {
    }

    /**
     * constructs a test case with the given test case name.
     */
    public DealTest(String name) {

    }

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() throws Exception {

        // initializing the test environment.
        ResourceManager.init();
        _srk = new SessionResourceKit("Testing");
        _dealDAO = new Deal(_srk, null);

        // get institutionId
        dealInstitutionId = _dataRepository.getInt("Deal",
                "InstitutionProfileId", 0);
    }

    /**
     * Test mortgage insurance response
     */
    @Test
    public void testMIResponse() throws Exception {

        _log
                .debug("============================================================");
        _log.debug("Testing MI response column ...");
        _log
                .debug("============================================================");
        int dealId = _dataRepository.getInt("Deal", "DealId", 0);
        int copyId = _dataRepository.getInt("Deal", "CopyId", 0);
        _dealDAO = new Deal(_srk, null, dealId, copyId);
        _log.debug("MI response for deal: ");

        // set vpd institutionId
        _srk.getExpressState().setDealIds(375, dealInstitutionId, 186);
        _srk.beginTransaction();
        _log.info("Before Change MIResponse:"
                + _dealDAO.getMortgageInsuranceResponse());
        _dealDAO.setMortgageInsuranceResponse("Hello change");
        _dealDAO.ejbStore();
        _log.info("After Change MIResponse: "
                + _dealDAO.getMortgageInsuranceResponse());
        _srk.commitTransaction();
    }

    /**
     * Test Index change
     */
    @Test
    public void testDeal() throws Exception {

        _log
                .debug("============================================================");
        _log.debug("Testing change index to column name ...");
        _log
                .debug("============================================================");

        int dealId = _dataRepository.getInt("Deal", "DealId", 0);
        int copyId = _dataRepository.getInt("Deal", "CopyId", 0);
        _dealDAO = new Deal(_srk, null, dealId, copyId);
        _log.debug("MI response for deal: ");

        // set vpd institutionId
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        _srk.beginTransaction();
        _log.info("Deal id: " + _dealDAO.getDealId());
        _log.info("Copy id: " + _dealDAO.getCopyId());
        _log.info("Total loan amount: " + _dealDAO.getTotalLoanAmount());
        _srk.commitTransaction();

    }

    /*
     * test find by all unlocked
     */
    @Test
    public void testFindByAllUnlocked() throws Exception {

        // set vpd institutionId
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // set values from database
        int dealId = _dataRepository.getInt("Deal", "DealId", 0);
        int copyId = _dataRepository.getInt("Deal", "CopyId", 0);

        // get deals findby all unlocked
        _dealDAO = new Deal(_srk, null, dealId, copyId);
        Collection dealCol = _dealDAO.findByAllUnlocked(dealId);

        for (Iterator i = dealCol.iterator(); i.hasNext();) {
            Deal deal = (Deal) i.next();

            // verify result
            assertEquals("N", deal.getScenarioLocked());
            _log.info("  Found All Unlocked: ");
            _log.info("             Deal ID: " + deal.getDealId());
            _log.info("              Locked: " + deal.getScenarioLocked());
            _log.info("Scenario Recommended: " + deal.getScenarioRecommended());
        }
    }

    @Test
    public void testFindByPrimaryKey() throws Exception {

        // set vpd institutionId
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // get values from database
        int dealId = _dataRepository.getInt("Deal", "DealId", 0);
        int copyId = _dataRepository.getInt("Deal", "CopyId", 0);

        // get deal find by primay key
        DealPK dpk = new DealPK(dealId, copyId);
        _dealDAO.findByPrimaryKey(dpk);

        // verify result
        assertEquals(dealId, _dealDAO.getDealId());
        _log.info("     Find by Primary: ");
        _log.info("             Deal ID: " + _dealDAO.getDealId());
        _log.info("              Locked: " + _dealDAO.getScenarioLocked());
        _log.info("Scenario Recommended: " + _dealDAO.getScenarioRecommended());
    }

    @Test
    public void testFindByRecommendedScenario() throws Exception {

        // set vpd institutionid
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // get values from database
        int dealId = _dataRepository.getInt("Deal", "DealId", 0);
        int copyId = _dataRepository.getInt("Deal", "CopyId", 0);

        // get deal find by recommendedscenario
        DealPK dpk = new DealPK(dealId, copyId);
        _dealDAO = _dealDAO.findByRecommendedScenario(dpk, true);

        // verify result
        assertEquals("Y", _dealDAO.getScenarioRecommended());
        _log.info("Find by Scenareio Recom: ");
        _log.info("             Deal ID: " + _dealDAO.getDealId());
        _log.info("              Locked: " + _dealDAO.getScenarioLocked());
        _log.info("Scenario Recommended: " + _dealDAO.getScenarioRecommended());

    }
    
    @Test
    public void testFindAll() throws Exception {
        // set vpd institutionid
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // get values from database
        int dealId = _dataRepository.getInt("Deal", "DealId", 0);
        int copyId = _dataRepository.getInt("Deal", "CopyId", 0);

        // get deal find by recommendedscenario
        DealPK dpk = new DealPK(dealId, copyId);
        List<Deal> deals = _dealDAO.findAllCopies();

        // verify result
        assert deals.size()!=0;
        _log.info("Find by Scenareio Recom: ");
        _log.info("             Deal ID: " + _dealDAO.getDealId());
        _log.info("              Locked: " + _dealDAO.getScenarioLocked());
        _log.info("Scenario Recommended: " + _dealDAO.getScenarioRecommended());

	}

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }
}
