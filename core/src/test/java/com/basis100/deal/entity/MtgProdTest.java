/*
 * @(#)MtgProdTest.java Sep 12, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * MtgProdTest
 * 
 * @version 1.0 Sep 12, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class MtgProdTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(MtgProdTest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test the find by primary key
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int mtgProdID = _dataRepository.getInt("MtgProd", "MTGPRODID", 0);
        String mtgProdName = _dataRepository.getString("MtgProd",
                "MTGPRODNAME", 0);
        int dealInstitutionId = _dataRepository.getInt("MtgProd",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "findByPrimaryKey");

        // Create the initial entity
        MtgProd mtgProd = new MtgProd(_srk, CalcMonitor.getMonitor(_srk));

        // Find by primary key
        mtgProd = mtgProd.findByPrimaryKey(new MtgProdPK(mtgProdID));

        // Check that the data retrieved matches the DB
        assertTrue(mtgProd.getMtgProdName().equals(mtgProdName));

        _logger.info(BORDER_END, "findByPrimaryKey");

    }

    /**
     * test the find by MP business ID
     */
    @Test
    public void testFindByMPBusinessId() throws Exception {

        // get input data from repository
        String mpBusinessID = _dataRepository.getString("MtgProd",
                "MPBUSINESSID", 0);
        String mtgProdName = _dataRepository.getString("MtgProd",
                "MTGPRODNAME", 0);
        int dealInstitutionId = _dataRepository.getInt("MtgProd",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "testFindByMPBusinessId");

        // Create the initial entity
        MtgProd mtgProd = new MtgProd(_srk, CalcMonitor.getMonitor(_srk));

        // Find by primary key
        mtgProd = mtgProd.findByMPBusinessId(mpBusinessID);

        // Check that the data retrieved matches the DB
        assertTrue(mtgProd.getMtgProdName().equals(mtgProdName));

        _logger.info(BORDER_END, "testFindByMPBusinessId");

    }

    /**
     * test the find by Lender and Payment Term
     */
    @Test
    public void testFindByLenderAndPaymentTerm() throws Exception {

        // get input data from repository
        int lenderProfileId = _dataRepository.getInt(
                "mtgprodlenderinvestorassoc", "LENDERPROFILEID", 0);
        int paymentTermId = _dataRepository.getInt("MtgProd", "PAYMENTTERMID",
                0);
        String mtgProdName = _dataRepository.getString("MtgProd",
                "MTGPRODNAME", 0);
        int dealInstitutionId = _dataRepository.getInt("MtgProd",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "testFindByLenderAndPaymentTerm");

        // Create the initial entity
        MtgProd mtgProd = new MtgProd(_srk, CalcMonitor.getMonitor(_srk));

        // Find by primary key
        List<MtgProd> mtgProds = (List) mtgProd.findByLenderAndPaymentTerm(
                lenderProfileId, paymentTermId);

        // Check that the data retrieved matches the DB
        _logger.info("++++++++++++++++++++ Size " + mtgProds.size());

        _logger.info(BORDER_END, "testFindByLenderAndPaymentTerm");

    }

    /**
     * test the find by MP business ID
     */
    @Test
    public void testFindByPricingProfileID() throws Exception {

        // get input data from repository
        int pricingProfileId = _dataRepository.getInt("MtgProd",
                "PRICINGPROFILEID", 0);
        String mtgProdName = _dataRepository.getString("MtgProd",
                "MTGPRODNAME", 0);
        int dealInstitutionId = _dataRepository.getInt("MtgProd",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "testFindByPricingProfileID");

        // Create the initial entity
        MtgProd mtgProd = new MtgProd(_srk, CalcMonitor.getMonitor(_srk));

        // Find by primary key
        List<MtgProd> mtgProds = (List) mtgProd
                .findByPricingProfile(pricingProfileId);
        // Check that the data retrieved matches the DB
        _logger.info("++++++++++++++++++++ Size " + mtgProds.size());

        _logger.info(BORDER_END, "testFindByPricingProfileID");

    }

    /**
     * test the find by Lender and Interest Type
     */
    @Test
    public void testFindByLenderAndInterestType() throws Exception {

        // get input data from repository
        int lenderProfileId = _dataRepository.getInt(
                "mtgprodlenderinvestorassoc", "LENDERPROFILEID", 0);
        int interestTypeId = _dataRepository.getInt("MtgProd",
                "INTERESTTYPEID", 0);
        String mtgProdName = _dataRepository.getString("MtgProd",
                "MTGPRODNAME", 0);
        int dealInstitutionId = _dataRepository.getInt("MtgProd",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "testFindByLenderAndInterestType");

        // Create the initial entity
        MtgProd mtgProd = new MtgProd(_srk, CalcMonitor.getMonitor(_srk));

        // Find by primary key
        List<MtgProd> mtgProds = (List) mtgProd.findByLenderAndInterestType(
                lenderProfileId, interestTypeId);

        // Check that the data retrieved matches the DB
        _logger.info("++++++++++++++++++++ Size " + mtgProds.size());

        _logger.info(BORDER_END, "testFindByLenderAndInterestType");

    }

    /**
     * ignore this test case, since mtgProd.create(pk) method is invalid
     * COMPONENTTYPEID is not nullable
     * 
     * test the Create
     */
    @Ignore
    public void testCreate() throws Exception {
        // get input data from repository
        int mtgProdID = 99;
        String mtgProdName = "Test Desc";
        int dealInstitutionId = _dataRepository.getInt("MtgProd",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // Start new transaction
        _srk.beginTransaction();

        _logger.info(BORDER_START, "create");

        // Create the initial entity
        MtgProd mtgProd = new MtgProd(_srk, CalcMonitor.getMonitor(_srk));

        // Create the new entity
        mtgProd = mtgProd.create(new MtgProdPK(mtgProdID));
        mtgProd.setMtgProdName(mtgProdName);
        mtgProd.ejbStore();

        // Verify data from the database
        mtgProd = mtgProd.findByPrimaryKey(new MtgProdPK(mtgProdID));
        assertTrue(mtgProd.getMtgProdName().equals(mtgProdName));

        // Rollback transaction
        _srk.rollbackTransaction();

        _logger.info(BORDER_END, "create");

    }

}
