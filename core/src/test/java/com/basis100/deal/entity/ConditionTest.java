/*
 * @(#)ConditionTest.java    2005-3-16
 *
 */

package com.basis100.deal.entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.basis100.deal.pk.BranchProfilePK;
import com.basis100.deal.pk.ConditionPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;

/**
 * ConditionTest -
 * 
 * @version 1.0 2007-3-16
 * @author <A HREF="mailto:chyxiang@yahoo.com">Chen Xiang (Sean)</A>
 */
public class ConditionTest extends ExpressEntityTestCase {

    // The logger
    private static Log _log = LogFactory.getLog(ConditionTest.class);

    // The SessionResourceKit
    private SessionResourceKit _srk;

    // The condition entity
    private Condition _conditionDAO;

    // institutionid for deal
    private int dealInstitutionId;

    /*
     * setup
     */
    @Before
    public void setUp() throws Exception {

        // initialize resource
        ResourceManager.init();
        _srk = new SessionResourceKit("Testing");
        
        // get institutionid from database
        dealInstitutionId = _dataRepository.getInt("Condition",
                "InstitutionProfileId", 0);
        
        _conditionDAO = new Condition(_srk);
    }

    /**
     * test get by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // set vpd context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        
        // get values from database
        int conditionId = 
            _dataRepository.getInt("Condition", "ConditionId", 0);
        int conditionTypeId = 
            _dataRepository.getInt("Condition", "ConditionTypeId", 0);
        
        // find condition by primary key
        Condition condition = 
            _conditionDAO.findByPrimaryKey(new ConditionPK(conditionId));
        
        // verify the result
        assertEquals(conditionTypeId, condition.getConditionTypeId());
        
        _log.info("Found Conditon: ");
        _log.info("         Condition Desc: " + condition.getConditionDesc());
        _log.info("        Response RoleId: "
                + condition.getConditionResponsibilityRoleId());
        _log.info("      Condition Type ID: " + condition.getConditionTypeId());
        _log.info("====================================");
        

    }

    /**
     * test create. since sequence does not exist
     */
    @Ignore
    public void testCreate() throws Exception {

        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        
        //create a condition entity an set the values; 
        ConditionPK cPk = _conditionDAO.createPrimaryKey();
        Condition condition = _conditionDAO.create(cPk);
        condition.setConditionDesc("Condition Desc");
        condition.ejbStore();
        
        Condition foundCondition = new Condition(_srk);
        foundCondition = foundCondition
            .findByPrimaryKey(new ConditionPK(condition.getConditionId()));

        //verify result
        assertEquals(foundCondition.getConditionId(), 
                        condition.getConditionId());
    }
    
    
    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        //free the srk
        _srk.freeResources();
    }
}
