package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.docprep.extract.doctag.PrivacyClause;
import com.basis100.deal.pk.AddrPK;
import com.basis100.deal.pk.ConditionPK;
import com.basis100.deal.pk.ContactPK;
import com.basis100.deal.pk.EmploymentHistoryPK;
import com.basis100.resources.SessionResourceKit;
import com.sun.org.apache.bcel.internal.generic.INSTANCEOF;

public class ContactTest1 extends FXDBTestCase {
	private IDataSet dataSetTest;
    private Contact contact = null;
    private ContactPK contactPK = null;
    private AddrPK addrPK=null;
    private EmploymentHistoryPK employmentHistoryPK = null;
    private CalcMonitor calcMonitor =null;
    private Collection<String> collection=null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public ContactTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(Contact.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(Contact.class.getSimpleName()+"DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.contact = new Contact(srk);
	}
	
	public void testCreate() throws Exception
	{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int contactId = Integer.parseInt((String)testCreate.getValue(0, "CONTACTID"));     
		int copyId = Integer.parseInt((String)testCreate.getValue(0, "COPYID"));
		/*int addrId = Integer.parseInt((String)testCreate.getValue(0, "ADDRID"));      
		int salutationId = Integer.parseInt((String)testCreate.getValue(0, "SALUTATIONID"));   
		int langprefId = Integer.parseInt((String)testCreate.getValue(0, "LANGUAGEPREFERENCEID"));     
		int prefereddeliveryId = Integer.parseInt((String)testCreate.getValue(0, "PREFERREDDELIVERYMETHODID"));   */
		int institutionId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));          
		
		srk.getExpressState().setDealInstitutionId(institutionId);
		addrPK = new AddrPK(contactId, copyId);
		Contact aContact = contact.create(addrPK);
		
		/* Can not compare new_contact with aContact with assertEqual as each time the constructor is called 
		 * a new contactId is created with DB sequence hence we use assertNotNull() as below. 
		 */
		//Contact new_contact = new Contact( srk,contactId, copyId );
		assertNotNull(aContact);
		srk.rollbackTransaction();
	}
	/*
	 * Failure case not applicable as the create method returns an entity and sending unqualified data will result
	 * in improper end of test class and thus it is avoid.
	 */
	
	public void testCreate1() throws Exception
	{
		srk.beginTransaction();
		ITable testCreate1 = dataSetTest.getTable("testCreate1");
		int contactId = Integer.parseInt((String)testCreate1.getValue(0, "CONTACTID"));     
		int copyId = Integer.parseInt((String)testCreate1.getValue(0, "COPYID"));
		int institutionId = Integer.parseInt((String)testCreate1.getValue(0, "INSTITUTIONPROFILEID"));          
		
		srk.getExpressState().setDealInstitutionId(institutionId);
		addrPK = new AddrPK(contactId, copyId);
		Contact aContact = contact.create(addrPK);
		assertNotNull(aContact);
		
		srk.rollbackTransaction();
	}
	
	public void testCreate2() throws Exception
	{
		srk.beginTransaction();
		ITable testCreate2 = dataSetTest.getTable("testCreate2");
		int contactId = Integer.parseInt((String)testCreate2.getValue(0, "CONTACTID"));     
		int copyId = Integer.parseInt((String)testCreate2.getValue(0, "COPYID"));
		int institutionId = Integer.parseInt((String)testCreate2.getValue(0, "INSTITUTIONPROFILEID"));          
		
		srk.getExpressState().setDealInstitutionId(institutionId);
		addrPK = new AddrPK(contactId, copyId);
		Contact aContact = contact.create(addrPK);
		assertNotNull(aContact);
		
		srk.rollbackTransaction();
	}
	
	public void testCreate3() throws Exception
	{
		srk.beginTransaction();
		ITable testCreate3 = dataSetTest.getTable("testCreate3");
		int contactId = Integer.parseInt((String)testCreate3.getValue(0, "CONTACTID"));     
		int copyId = Integer.parseInt((String)testCreate3.getValue(0, "COPYID"));
		int institutionId = Integer.parseInt((String)testCreate3.getValue(0, "INSTITUTIONPROFILEID"));          
		
		srk.getExpressState().setDealInstitutionId(institutionId);
		employmentHistoryPK = new EmploymentHistoryPK(contactId, copyId);
		Contact aContact = contact.create(employmentHistoryPK);
		assertNotNull(aContact);
		
		srk.rollbackTransaction();
	}
	
   public void testfindByUserProfileId() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByUserProfileId = dataSetTest.getTable("testfindByUserProfileId");
		int userprofileId = Integer.parseInt((String)testfindByUserProfileId.getValue(0, "USERPROFILEID"));     
		
		Contact aContact = contact.findByUserProfileId(userprofileId);
		assertNotNull(aContact);
		
		srk.rollbackTransaction();
	}
	
	/*public void testfindByUserProfileIdFailure() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByUserProfileIdFailure = dataSetTest.getTable("testfindByUserProfileIdFailure");
		int userprofileId = Integer.parseInt((String)testfindByUserProfileIdFailure.getValue(0, "USERPROFILEID"));     
		
		Contact aContact = contact.findByUserProfileId(userprofileId);
		srk.getExpressState().setDealInstitutionId(0);
		
		Contact new_Contact = new Contact(srk);
		assertNotSame(new_Contact, aContact);
		
		srk.rollbackTransaction();
	}*/
	
	public void testfindByUserProfileIdWithInstitutionId() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByUserProfileIdWithInstitutionId = dataSetTest.getTable("testfindByUserProfileIdWithInstitutionId");
		int userprofileId = Integer.parseInt((String)testfindByUserProfileIdWithInstitutionId.getValue(0, "USERPROFILEID"));     
		int institutionId = Integer.parseInt((String)testfindByUserProfileIdWithInstitutionId.getValue(0, "INSTITUTIONPROFILEID"));   
		
		Contact aContact = contact.findByUserProfileIdWithInstitutionId(userprofileId, institutionId);
		assertNotNull(aContact);
		
		srk.rollbackTransaction();
	}
	
	@SuppressWarnings("finally")
	public boolean testfindByUserProfileIdWithInstitutionIdFailure() throws Exception
	{
		srk.beginTransaction();
		Contact aContact = null;
		ITable testfindByUserProfileIdWithInstitutionIdFailure = dataSetTest.getTable("testfindByUserProfileIdWithInstitutionIdFailure");
		int userprofileId = Integer.parseInt((String)testfindByUserProfileIdWithInstitutionIdFailure.getValue(0, "USERPROFILEID"));     
		int institutionId = Integer.parseInt((String)testfindByUserProfileIdWithInstitutionIdFailure.getValue(0, "INSTITUTIONPROFILEID"));   
		try{
		aContact = contact.findByUserProfileIdWithInstitutionId(userprofileId, institutionId);
		}catch (Exception e) {
			assertNull(aContact);
			return true;
		}
		finally{
		srk.rollbackTransaction();
		assertNull(aContact);
		return true;
		}
	}
	
	public void testfindByName() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByName = dataSetTest.getTable("testfindByName");
		String first = (String)testfindByName.getValue(0, "CONTACTFIRSTNAME");     
		String middle =(String)testfindByName.getValue(0, "CONTACTMIDDLEINITIAL");
		String last = (String)testfindByName.getValue(0, "CONTACTLASTNAME");          
		
		Contact aContact = contact.findByName(first, middle, last);
		assertNotNull(aContact);
		
		srk.rollbackTransaction();
	}
	public void testfindByNameFailure() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByNameFailure = dataSetTest.getTable("testfindByNameFailure");
		String first = (String)testfindByNameFailure.getValue(0, "CONTACTFIRSTNAME");     
		String middle =(String)testfindByNameFailure.getValue(0, "CONTACTMIDDLEINITIAL");
		String last = (String)testfindByNameFailure.getValue(0, "CONTACTLASTNAME");          
		
		Contact aContact = contact.findByName(first, middle, last);
		assertNull(aContact);
		
		srk.rollbackTransaction();
	}
	
}
