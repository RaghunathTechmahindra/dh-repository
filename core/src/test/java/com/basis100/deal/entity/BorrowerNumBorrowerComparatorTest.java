package com.basis100.deal.entity;

import java.io.IOException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.util.collections.BorrowerNumBorrowerComparator;
import com.basis100.resources.SessionResourceKit;

public class BorrowerNumBorrowerComparatorTest extends FXDBTestCase {
	
	private IDataSet dataSetTest;
	private BorrowerNumBorrowerComparator bnc = null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public BorrowerNumBorrowerComparatorTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(BorrowerNumBorrowerComparator.class.getSimpleName()+"DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected DatabaseOperation getSetUpOperation() throws Exception
	    {
		bnc = new BorrowerNumBorrowerComparator();
	    	return DatabaseOperation.INSERT;
	    }
	
	public void testCompare() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCompare");
		
		int bid1 = Integer.parseInt((String)testExtract.getValue(0, "BORROWERID1"));
		int bid2 = Integer.parseInt((String)testExtract.getValue(0, "BORROWERID2"));
		int dealid = Integer.parseInt((String)testExtract.getValue(0, "dealid"));
		int copyid = Integer.parseInt((String)testExtract.getValue(0, "copyid"));
		
		Borrower b1 = new Borrower(srk, null, bid1, copyid);
		Borrower b2 = new Borrower(srk, null, bid2, copyid);
		
		com.basis100.deal.entity.DealEntity o1 = (com.basis100.deal.entity.DealEntity )b1;
		com.basis100.deal.entity.DealEntity o2 = (com.basis100.deal.entity.DealEntity )b2;
		
		int result = bnc.compare(o1, o2);
		
		assert result==0 || result==1 || result==-1;
		
		srk.rollbackTransaction();
	}
}
