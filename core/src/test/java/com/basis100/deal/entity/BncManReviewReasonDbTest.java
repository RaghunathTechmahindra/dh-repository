package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.BncManReviewReasonPK;
import com.basis100.deal.pk.BorrowerPK;


/**
 * <p>BncAutoDeclineReasonDbTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class BncManReviewReasonDbTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private BncManReviewReason bncManReviewReason;
	
	public BncManReviewReasonDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(BncManReviewReason.class.getSimpleName() + "DataSetTest.xml"));
	}


	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.bncManReviewReason = new BncManReviewReason(srk);
	}
	

	
	public void testCreate() throws Exception {
    	ITable expectedUserProfile = dataSetTest.getTable("testCreate1");
    	int responseId = (Integer) DataType.INTEGER.typeCast(expectedUserProfile.getValue(0, "RESPONSEID"));
    	
    	bncManReviewReason.createPrimaryKey(responseId);
	    assertNotNull(bncManReviewReason);
    }
	
	 public void testFindByPrimaryKey() throws Exception{		
			ITable findByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKeyFailure");
			boolean status=false;
			try{
				int responseId = Integer.parseInt((String)findByPrimaryKey.getValue(0,"responseId"));	
				int copyId = Integer.parseInt((String)findByPrimaryKey.getValue(0,"copyId"));
				srk.getExpressState().setDealInstitutionId(1);		
				BncManReviewReasonPK  pk = new BncManReviewReasonPK(responseId,copyId);
				bncManReviewReason=bncManReviewReason.findByPrimaryKey(pk);		
			status=true;
			}catch(Exception e ){
				status = false;		
			}
			assertEquals(false, status);
		}   
	
	
}