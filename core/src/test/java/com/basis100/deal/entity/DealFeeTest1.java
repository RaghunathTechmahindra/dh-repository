package com.basis100.deal.entity;

import java.io.File;
import java.io.IOException;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.AppraisalOrderPK;
import com.basis100.deal.pk.DealEventStatusSnapshotPK;
import com.basis100.deal.pk.DealEventStatusUpdateAssocPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.ltx.unittest.util.EntityTestUtil;

public class DealFeeTest1 extends FXDBTestCase{

	private IDataSet dataSetTest;	
	private DealFee dealFee;
	Clob clob=null;
	
	
	public DealFeeTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(DealFee.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(DealFee.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		dealFee = new DealFee(srk,null);
	}
	
	public void testFindByDeal() throws Exception{
	 Collection cList=null;
		ITable testFindByDeal = dataSetTest.getTable("testFindByDeal");
		 int dealId=Integer.valueOf((String)testFindByDeal.getValue(0,"DEALID"));	
		 int copyId=Integer.valueOf((String)testFindByDeal.getValue(0,"COPYID"));		
		 srk.getExpressState().setDealInstitutionId(1);
		 DealPK pk=new DealPK(dealId,copyId);
		 cList=dealFee.findByDeal(pk);
	    assertEquals(dealId, pk.getId());
}  
	
	
	
	public void testFindByDealAndTypes() throws Exception{
		 Collection cList=null;
			ITable testFindByDeal = dataSetTest.getTable("testFindByDealAndTypes");
			 int dealId=Integer.valueOf((String)testFindByDeal.getValue(0,"DEALID"));	
			 int copyId=Integer.valueOf((String)testFindByDeal.getValue(0,"COPYID"));
			 String type=(String)testFindByDeal.getValue(0,"FEETYPEID");
			 int [] feeTypeId=new int[type.length()];			 
			 srk.getExpressState().setDealInstitutionId(1);
			 DealPK pk=new DealPK(dealId,copyId);
			 cList=dealFee.findByDealAndTypes(pk,feeTypeId);
		    assertEquals(dealId, pk.getId());
	}  
	
	
	public void testFindByDealAndType() throws Exception{
		 Collection<DealFee> cList=null;
			ITable testFindByDeal = dataSetTest.getTable("testFindByDealAndType");
			 int dealId=Integer.valueOf((String)testFindByDeal.getValue(0,"DEALID"));	
			 int copyId=Integer.valueOf((String)testFindByDeal.getValue(0,"COPYID"));
			 int feeTypeId=Integer.valueOf((String)testFindByDeal.getValue(0,"FEETYPEID"));					 
			 srk.getExpressState().setDealInstitutionId(1);
			 DealPK pk=new DealPK(dealId,copyId);
			 cList=dealFee.findByDealAndType(pk,feeTypeId);
		    assertEquals(dealId, pk.getId());
	}  	
	
	
	public void testFindFirstByDealAndType() throws Exception{		
			ITable testFindByDeal = dataSetTest.getTable("testFindFirstByDealAndType");
			 int dealId=Integer.valueOf((String)testFindByDeal.getValue(0,"DEALID"));	
			 int copyId=Integer.valueOf((String)testFindByDeal.getValue(0,"COPYID"));
			 int feeTypeId=Integer.valueOf((String)testFindByDeal.getValue(0,"FEETYPEID"));					 
			 srk.getExpressState().setDealInstitutionId(1);
			 DealPK pk=new DealPK(dealId,copyId);
			 dealFee=dealFee.findFirstByDealAndType(pk,feeTypeId);
			 assertEquals(dealId, pk.getId());
	}  
	
	public void testCreate() throws Exception{		
		ITable testCreate = dataSetTest.getTable("testCreate");
		 int dealId=Integer.valueOf((String)testCreate.getValue(0,"DEALID"));	
		 int copyId=Integer.valueOf((String)testCreate.getValue(0,"COPYID"));		 				 
		 srk.getExpressState().setDealInstitutionId(1);
		 DealPK pk=new DealPK(dealId,copyId);
		 dealFee=dealFee.create(pk);		 
		 assertEquals(dealId, dealFee.getDealId());
}
}

