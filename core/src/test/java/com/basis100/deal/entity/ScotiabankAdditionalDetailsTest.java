/*
 * @(#)ScotiabankAdditionalDetailsTest.java    2007-9-7
 *
 * Copyright (C) 2007 Filogix Limited Partnership All rights reserved.
 */


package com.basis100.deal.entity;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.ScotiabankAdditionalDetailsPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>ScotiaBankAdditionalDetailsTest</p>
 * Express Entity class unit test: ScotiaBankAdditionalDetails
 */
public class ScotiabankAdditionalDetailsTest extends ExpressEntityTestCase 
    implements UnitTestLogging {
  
    private final static Logger _logger = 
        LoggerFactory.getLogger(RegionProfileTest.class);
    private final static String ENTITY_NAME = "SCOTIABANKADDITIONALDETAILS"; 
    
    // retreived data count that is stored in UnitTestDataRepository.xml
    private static final int DATA_COUNT = 1;
  
    private SessionResourceKit _srk;
  
    // use this value for random sequence number data
    private int testSeqForGen = 0;
  
    // properties
    protected int           dealId;
    protected int         copyId;
    protected Date      approvalDate;
    protected int     scotiabankApplicationNumber;
    protected String    branchName;
    protected int     servicingBranchNumber;
    protected int     standaloneMortgageNumber;
    protected int     standaloneSCLNumber;
    protected String    healthCrisisInsuranceStatus;
    protected String    scotiaLifeInsuranceStatus;
    protected int     bankCode;
    protected double    postedRate;
    protected int     transitNumber;
    protected double    committedRate;
    protected int     bankAccount;
    protected int     amortization;
    protected double    findersFeeTotal;
    protected Date      termStartDate;
    protected double    rateBuydown;
    protected double    refinanceNewMoney;
    protected double    rateBuydownAmount;
    protected String    switchCode;
    protected double    cashbackPercentage;
    protected int     existingAccountNumber;
    protected double    cashbackAmount;
    protected int     rateBuydownPayorId;
    protected String    mdm;
    protected double    holdbackAmount;
    protected String    otherMortgageDeductions;
    protected String    ppDescription;
    protected String    otherDeductionsDescription;
    protected String    sfDescription;
    protected String    otherDescription;
    protected int     instProfId;
    
    /**
     * <p>setUp</p>
     * Setting up test case.
     */
    @Before
    public void setUp() throws Exception {

        _logger.info(BORDER_START, ENTITY_NAME + " TestCase setting up ...");

        //  init session resource kit.
        ResourceManager.init();
        //  init session resource kit.
        _srk = new SessionResourceKit();

//        // get random seqence number 
//        Double indexD = new Double(Math.random() * DATA_COUNT);
//        testSeqForGen = indexD.intValue();

        // retreive data from test data file
        dealId = _dataRepository.getInt( ENTITY_NAME, "DEALID", testSeqForGen);
        copyId = _dataRepository.getInt( ENTITY_NAME, "COPYID", testSeqForGen);
        approvalDate = _dataRepository.getDate( ENTITY_NAME, 
                                                "APPROVALDATE", 
                                                testSeqForGen);
        scotiabankApplicationNumber 
            = _dataRepository.getInt( ENTITY_NAME, 
                                      "SCOTIABANKAPPLICATIONNUMBER", 
                                      testSeqForGen);
        branchName = _dataRepository.getString( ENTITY_NAME, 
                                                "BRANCHNAME", 
                                                testSeqForGen);
        servicingBranchNumber = _dataRepository.getInt( ENTITY_NAME, 
                                                        "SERVICINGBRANCHNUMBER", 
                                                        testSeqForGen);
        standaloneMortgageNumber = _dataRepository.getInt( ENTITY_NAME, 
                                                           "STANDALONEMORTGAGENUMBER", 
                                                           testSeqForGen);
        standaloneSCLNumber = _dataRepository.getInt( ENTITY_NAME, 
                                                      "STANDALONESCLNUMBER", 
                                                      testSeqForGen);
        healthCrisisInsuranceStatus 
            = _dataRepository.getString( ENTITY_NAME, 
                                         "HEALTHCRISISINSURANCESTATUS", 
                                         testSeqForGen);
        scotiaLifeInsuranceStatus 
            = _dataRepository.getString( ENTITY_NAME, "SCOTIALIFEINSURANCESTATUS", 
                                         testSeqForGen);
        bankCode = _dataRepository.getInt( ENTITY_NAME, "BANKCODE", 
                                           testSeqForGen);
        postedRate = _dataRepository.getDouble( ENTITY_NAME, "POSTEDRATE", 
                                                testSeqForGen);
        transitNumber = _dataRepository.getInt( ENTITY_NAME, "TRANSITNUMBER", 
                                                testSeqForGen);
        committedRate = _dataRepository.getDouble( ENTITY_NAME, "COMMITTEDRATE", 
                                                   testSeqForGen);
        bankAccount = _dataRepository.getInt( ENTITY_NAME, "BANKACCOUNT", 
                                              testSeqForGen);
        amortization = _dataRepository.getInt( ENTITY_NAME, "AMORTIZATION", 
                                               testSeqForGen);
        findersFeeTotal = _dataRepository.getInt( ENTITY_NAME, "FINDERSFEETOTAL", 
                                                  testSeqForGen);
        termStartDate = _dataRepository.getDate( ENTITY_NAME, 
                                                 "TERMSTARTDATE", 
                                                 testSeqForGen);
        rateBuydown = _dataRepository.getDouble( ENTITY_NAME, "RATEBUYDOWN", 
                                                 testSeqForGen);
        refinanceNewMoney 
            = _dataRepository.getDouble( ENTITY_NAME, "REFINANCENEWMONEY", 
                                     testSeqForGen);
        rateBuydownAmount 
            = _dataRepository.getDouble( ENTITY_NAME, "RATEBUYDOWNAMOUNT", 
                                     testSeqForGen);
        switchCode 
            = _dataRepository.getString( ENTITY_NAME, "SWITCHCODE", testSeqForGen);
        cashbackPercentage 
            = _dataRepository.getDouble( ENTITY_NAME, "CASHBACKPERCENTAGE", 
                                     testSeqForGen);
        existingAccountNumber 
            = _dataRepository.getInt( ENTITY_NAME, "EXISTINGACCOUNTNUMBER", 
                                  testSeqForGen);
        cashbackAmount 
            = _dataRepository.getDouble( ENTITY_NAME, "CASHBACKAMOUNT", 
                                     testSeqForGen);
        rateBuydownPayorId 
        = _dataRepository.getInt( ENTITY_NAME, "RATEBUYDOWNPAYORID", 
                                  testSeqForGen);
        mdm 
            = _dataRepository.getString( ENTITY_NAME, "MDM", testSeqForGen);
        holdbackAmount 
            = _dataRepository.getDouble( ENTITY_NAME, "HOLDBACKAMOUNT", testSeqForGen);
        otherMortgageDeductions 
            = _dataRepository.getString( ENTITY_NAME, "OTHERMORTGAGEDEDUCTIONS", 
                                     testSeqForGen);
        ppDescription 
            = _dataRepository.getString( ENTITY_NAME, "PPDESCRIPTION", testSeqForGen);
        otherDeductionsDescription 
            = _dataRepository.getString( ENTITY_NAME, "OTHERDEDUCTIONSDESCRIPTION", 
                                     testSeqForGen);
        sfDescription 
            = _dataRepository.getString( ENTITY_NAME, "SFDESCRIPTION", testSeqForGen);
        otherDescription 
            = _dataRepository.getString( ENTITY_NAME, "OTHERDESCRIPTION", 
                                     testSeqForGen);
        instProfId 
            = _dataRepository.getInt( ENTITY_NAME, "INSTITUTIONPROFILEID", 
                                  testSeqForGen);

        _logger.info(BORDER_END, "Setting up Done");
    }
    
    @After
    public void tearDown() {
      
        //      free resources
        _srk.freeResources( );
  
    }

    /**
     * test findByPrimaryKey.
     */
    @Ignore @Test
    public void testFindByPrimaryKey()  throws Exception {
    
        _logger.info(BORDER_START, "findByPrimaryKey");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
        
        // excute method
        ScotiabankAdditionalDetails entity = new ScotiabankAdditionalDetails(_srk);
        entity = entity.findByPrimaryKey(new ScotiabankAdditionalDetailsPK(dealId, 
                                                                           copyId));
              
        // assert
        assertEquals(entity.getDealId(), dealId);
        assertEquals(entity.getCopyId(), copyId);
        assertEquals(entity.getApprovalDate(), approvalDate);
        assertEquals(entity.getScotiabankApplicationNumber(), 
                     scotiabankApplicationNumber);
        assertEquals(entity.getBranchName(), branchName);
        assertEquals(entity.getServicingBranchNumber(), servicingBranchNumber);
        assertEquals(entity.getStandaloneMortgageNumber(), 
                     standaloneMortgageNumber);
        assertEquals(entity.getStandaloneSCLNumber(), standaloneSCLNumber);
        assertEquals(entity.getHealthCrisisInsuranceStatus(), 
                     healthCrisisInsuranceStatus);
        assertEquals(entity.getScotiaLifeInsuranceStatus(), 
                     scotiaLifeInsuranceStatus);
        assertEquals(entity.getBankCode(), bankCode);
        assertEquals(entity.getPostedRate(), postedRate);
        assertEquals(entity.getTransitNumber(), transitNumber);
        assertEquals(entity.getCommittedRate(), committedRate);
        assertEquals(entity.getBankAccount(), bankAccount);
        assertEquals(entity.getAmortization(), amortization);
        assertEquals(entity.getFindersFeeTotal(), findersFeeTotal);
        assertEquals(entity.getTermStartDate(), termStartDate);
        assertEquals(entity.getRateBuydown(), rateBuydown);
        assertEquals(entity.getRefinanceNewMoney(), refinanceNewMoney);
        assertEquals(entity.getRateBuydownAmount(), rateBuydownAmount);
        assertEquals(entity.getSwitchCode(), switchCode);
        assertEquals(entity.getCashbackPercentage(), cashbackPercentage);
        assertEquals(entity.getExistingAccountNumber(), existingAccountNumber);
        assertEquals(entity.getCashbackAmount(), cashbackAmount);
        assertEquals(entity.getRateBuydownPayorId(), rateBuydownPayorId);
        assertEquals(entity.getMDM(), mdm);
        assertEquals(entity.getHoldbackAmount(), holdbackAmount);
        assertEquals(entity.getOtherMortgageDeductions(), 
                     otherMortgageDeductions);
        assertEquals(entity.getPPDescription(), ppDescription);
        assertEquals(entity.getOtherDeductionsDescription(), 
                     otherDeductionsDescription);
        assertEquals(entity.getSFDescription(), sfDescription);
        assertEquals(entity.getOtherDescription(), otherDescription);
        
        _logger.info(BORDER_END, "findByPrimaryKey");
    }
}
