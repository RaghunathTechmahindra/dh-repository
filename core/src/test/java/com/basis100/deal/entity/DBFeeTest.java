package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.FeePK;

public class DBFeeTest extends FXDBTestCase{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(DBFeeTest.class);
    
    //Initial data to be loaded before each test case.
	private IDataSet dataSetTest;
	
	/**
     * Constructor function
     */
    public DBFeeTest(String name)  throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource("DBFeeDataSetTest.xml"));
    }
    
  
    
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
        return DatabaseOperation.INSERT;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.NONE;
    }
    
    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

    	_logger.info(" Start testFindByPrimaryKey");
    	 
    	 ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKey");
    	int  feeId = Integer.parseInt((String)testFindByPrimaryKey.getValue(0,"feeId"));
  		int  instProfId = Integer.parseInt((String)testFindByPrimaryKey.getValue(0,"instProfId"));
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
        
        Fee fee = new Fee(srk, null);
        fee = fee.findByPrimaryKey(new FeePK(feeId));
        
        // verify the result.
        assert fee != null;
        assertEquals(feeId, fee.getFeeId());
        
        _logger.info(" End testFindByPrimaryKey");
    	
    }
    
    /**
     * test find first by Type.
     */
    @Test
    public void testFindFirstByType() throws Exception {

    	_logger.info(" Start testFindFirstByType");
    	 
    	 ITable testFindFirstByType = dataSetTest.getTable("testFindFirstByType");
    	int  feeTypeId = Integer.parseInt((String)testFindFirstByType.getValue(0,"feeTypeId"));
  		int  instProfId = Integer.parseInt((String)testFindFirstByType.getValue(0,"instProfId"));
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
        
        Fee fee = new Fee(srk, null);
        fee = fee.findFirstByType(feeTypeId);
        
        // verify the result.
        assert fee != null;
        assertEquals(feeTypeId, fee.getFeeTypeId());
        
        _logger.info(" End testFindFirstByType");
    	
    }
    
    /**
     * test create primary key.
     */
    @Test
    public void testCreatePrimaryKey() throws Exception {

    	_logger.info(" Start testCreatePrimaryKey");
    	 
    	 ITable testCreatePrimaryKey = dataSetTest.getTable("testCreatePrimaryKey");
  		int  instProfId = Integer.parseInt((String)testCreatePrimaryKey.getValue(0,"instProfId"));
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
        
        Fee fee = new Fee(srk, null);
        FeePK feePK = fee.createPrimaryKey();
        
        // verify the result.
        assert feePK != null;
        assert feePK.getId() != 0;
        assertEquals(feePK.getName(), "feeId");
        
        _logger.info(" End testCreatePrimaryKey");
    	
    }
    
    /**
     * test create.
     */
    @Test
    public void testCreate() throws Exception {

    	_logger.info(" Start testCreate");
    	 
    	 ITable testCreate = dataSetTest.getTable("testCreate");
  		int  instProfId = Integer.parseInt((String)testCreate.getValue(0,"instProfId"));
  		int  feeTypeId = Integer.parseInt((String)testCreate.getValue(0,"feeTypeId"));
  		int  offSetingFeeTypeId = Integer.parseInt((String)testCreate.getValue(0,"offSetingFeeTypeId"));
  		int  defaultPaymentMethodId = Integer.parseInt((String)testCreate.getValue(0,"defaultPaymentMethodId"));
  		int  feePayorTypeId = Integer.parseInt((String)testCreate.getValue(0,"feePayorTypeId"));
  		String  feeBusinessId = (String)testCreate.getValue(0,"feeBusinessId");
  		String GLAccount = (String)testCreate.getValue(0,"GLAccount");
  		String payableIndicator = (String)testCreate.getValue(0,"payableIndicator");
  		String MITypeFlag = (String)testCreate.getValue(0,"MITypeFlag");
  		String includeInTotalFeeAmount = (String)testCreate.getValue(0,"includeInTotalFeeAmount");
  		int  defaultFeePaymentDateTypeId = Integer.parseInt((String)testCreate.getValue(0,"defaultFeePaymentDateTypeId"));
  		int  feeGenerationTypeId = Integer.parseInt((String)testCreate.getValue(0,"feeGenerationTypeId"));
  		double  defaultFeeAmount = Double.parseDouble((String)testCreate.getValue(0,"defaultFeeAmount"));
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
        srk.beginTransaction();
        
        Fee fee = new Fee(srk, null);
        fee = fee.create(GLAccount, feeTypeId, offSetingFeeTypeId, defaultPaymentMethodId, feePayorTypeId, feeBusinessId, payableIndicator,
                 defaultFeeAmount, defaultFeePaymentDateTypeId, feeGenerationTypeId, MITypeFlag, includeInTotalFeeAmount);
        
        // verify the result.
        assert fee != null;
        assertEquals(feeTypeId, fee.getFeeTypeId());
        assertEquals(feeGenerationTypeId, fee.getFeeGenerationTypeId());
        assertEquals(defaultFeeAmount, fee.getDefaultFeeAmount());
        assertEquals(feePayorTypeId, fee.getFeePayorTypeId());
        
        srk.rollbackTransaction();
        
        _logger.info(" End testCreate");
    	
    }



}
