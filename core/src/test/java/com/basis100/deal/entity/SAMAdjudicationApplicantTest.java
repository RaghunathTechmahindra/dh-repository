/**
 * <p>Title: SAMAdjudicationApplicantTest.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2007</p>
 *
 * <p>Company: Filogix Limited Partnership</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � Sep 7, 2007)
 *
 */


package com.basis100.deal.entity;

import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.AdjudicationApplicantPK;
import com.basis100.deal.pk.SAMAdjudicationApplicantPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>SAMAdjudicationApplicantTest</p>
 * Express Entity class unit test: SAMAdjudicationApplican
 */
public class SAMAdjudicationApplicantTest extends ExpressEntityTestCase 
    implements UnitTestLogging {
  
    private final static Logger _logger = 
        LoggerFactory.getLogger(SAMAdjudicationApplicantTest.class);
    
    private final static String ENTITY_NAME = "SAMADJUDICATIONAPPLICANT"; 
    
    // retreived data count that is stored in UnitTestDataRepository.xml
    private static final int DATA_COUNT = 6;
    
    private SessionResourceKit _srk;
    
    // use this value for random sequence number data
    private int testSeqForGen = 0;

    // properties
    protected int         requestId;
    protected int         applicantNumber;
    protected double      samTotalMonthlyPayment;
    protected String      samCreditFileReport;
    private int instProfId;

    /**
     * <p>setUp</p>
     * Setting up test case.
     */
    @Before
    public void setUp() throws Exception {

        _logger.info(BORDER_START, ENTITY_NAME + " TestCase setting up ...");

        //  init session resource kit.
        ResourceManager.init();
        //  init session resource kit.
        _srk = new SessionResourceKit();

//        // get random seqence number 
//        Double indexD = new Double(Math.random() * DATA_COUNT);
//        testSeqForGen = indexD.intValue();
        
        // retreive data from test data file
        requestId 
            = _dataRepository.getInt( ENTITY_NAME, "REQUESTID", testSeqForGen);
        applicantNumber 
            = _dataRepository.getInt( ENTITY_NAME, "APPLICANTNUMBER", testSeqForGen);
        samTotalMonthlyPayment = _dataRepository.getInt( ENTITY_NAME, 
                                      "SAMTOTALMONTHLYPAYMENT", 
                                      testSeqForGen);
        // this cloumn is clob
        samCreditFileReport = _dataRepository.getString( ENTITY_NAME, 
                                                         "SAMCREDITFILEREPORT", 
                                                         testSeqForGen);
        instProfId 
            = _dataRepository.getInt( ENTITY_NAME, "INSTITUTIONPROFILEID", 0);
  
        _logger.info(BORDER_END, "Setting up Done");
    }

    @After
    public void tearDown() {
      
      //      free resources
      _srk.freeResources( );
    }
    
    /**
     * test findByPrimaryKey.
     */
    @Ignore @Test
    public void testFindByPrimaryKey() throws Exception {
      
        _logger.info(BORDER_START, "findByPrimaryKey");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
        
        // excute method
        SAMAdjudicationApplicant entity 
            = new SAMAdjudicationApplicant(_srk);
        entity 
            = entity.findByPrimaryKey( new SAMAdjudicationApplicantPK(requestId, 
                                                              applicantNumber));
        // assert
        assertEquals(entity.getRequestId(), requestId);
        assertEquals(entity.getApplicantNumber(), applicantNumber);
        assertEquals(entity.getSAMTotalMonthlyPayment(), samTotalMonthlyPayment);
        assertEquals(entity.getSAMCreditFileReport(), samCreditFileReport);
        
        _logger.info(BORDER_END, "findByPrimaryKey");
    }
    
    /**
     * test findByAdjudicationApplicant.
     */
    @Ignore @Test
    public void testFindByAdjudicationApplicant() throws Exception {
      
        _logger.info(BORDER_START, "findByAdjudicationApplicant");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
        
        // get AdjudicationApplication data
        // use 0 for now
        int adApReId = _dataRepository.getInt("ADJUDICATIONAPPLICANT", "REQUESTID", 0);
        int adApApCId = _dataRepository.getInt("ADJUDICATIONAPPLICANT", "COPYID", 0);
  
        // excute method
        SAMAdjudicationApplicant entity 
            = new SAMAdjudicationApplicant(_srk);
        Collection applicants 
            = entity.findByAdjudicationApplicant( new AdjudicationApplicantPK(adApReId, 
                                                                              adApApCId));
        // assert
        // TODO:
        
        _logger.info(BORDER_END, "findByAdjudicationApplicant");
    }
    
    
}
