package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.DCImagingReferencePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;

public class DCImagingReferenceTest1 extends FXDBTestCase {
	DCImagingReference dcImagingReference;
	IDataSet dataSetTest;

	public DCImagingReferenceTest1(String name) throws DataSetException,
			IOException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"DCImagingReferenceDataSetTest.xml"));
	}

	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		dcImagingReference = new DCImagingReference(srk);

	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	public void testfindByDcFolderId() throws NumberFormatException,
			DataSetException, FinderException {
		int dcFolderId = Integer.parseInt(dataSetTest
				.getTable("testfindByDcFolderId").getValue(0, "dcFolderId")
				.toString());
		DCImagingReference imgref = dcImagingReference
				.findByDcFolderId(dcFolderId);
		assertNotNull(imgref);
		assertEquals(dcFolderId, imgref.getDcFolderId());
	}

	/*public void testCreate() throws DataSetException, RemoteException,
			CreateException, JdbcTransactionException {
		srk.beginTransaction();
		int dcFolderId = Integer.parseInt(dataSetTest.getTable("testCreate")
				.getValue(0, "dcFolderId").toString());
		String folderCode = dataSetTest.getTable("testCreate")
				.getValue(0, "folderCode").toString();
		String folderStatusCode = dataSetTest.getTable("testCreate")
				.getValue(0, "folderStatusCode").toString();
		String folderStatusDesc = dataSetTest.getTable("testCreate")
				.getValue(0, "folderStatusDesc").toString();
		srk.getExpressState().setDealInstitutionId(
				Integer.parseInt(dataSetTest.getTable("testCreate")
						.getValue(0, "dealInstitutionId").toString()));
		DCImagingReference imgref=dcImagingReference.create(dcFolderId, folderCode, folderStatusCode,
				folderStatusDesc);
		assertNotNull(imgref);
		assertTrue(srk.getModified());
		srk.rollbackTransaction();
	}*/
	public void testCreatePrimaryKey() throws CreateException{
		DCImagingReferencePK imgrefPK=dcImagingReference.createPrimaryKey();
		assertNotNull(imgrefPK);
		
	}
}
