package com.basis100.deal.entity;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.docprep.extract.doctag.PrivacyClause;
import com.basis100.deal.pk.AddrPK;
import com.basis100.deal.pk.ConditionPK;
import com.basis100.deal.pk.ContactPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.EmploymentHistoryPK;
import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.deal.pk.InsureOnlyApplicantPK;
import com.basis100.deal.pk.InvestorProfilePK;
import com.basis100.resources.SessionResourceKit;
import com.sun.org.apache.bcel.internal.generic.INSTANCEOF;

public class InvestorProfileTest1 extends FXDBTestCase {
	private IDataSet dataSetTest;
    private InvestorProfile investorProfile = null;
  
    SessionResourceKit srk= new SessionResourceKit();
	
	public InvestorProfileTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(InvestorProfile.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(InvestorProfile.class.getSimpleName()+"DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.investorProfile = new InvestorProfile(srk);
		
		
	}
	
	public void testCreate() throws Exception
	{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int institutionProfileId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String)testCreate.getValue(0, "INVESTORPROFILEID"));
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		InvestorProfilePK pk = new InvestorProfilePK(id);
		
		InvestorProfile aInvestorProfile = investorProfile.create(pk);
		
		InvestorProfile new_InvestorProfile = new InvestorProfile(srk, id);
		
		assertEquals(new_InvestorProfile, aInvestorProfile);
		
		srk.rollbackTransaction();
	}
	
	/*
	 * Failure case not applicable as the create method returns an entity and sending unqualified data will result
	 * in improper end of test class and thus it is avoid.
	 */
	
	public void testCreatePrimaryKey() throws Exception
	{
		srk.beginTransaction();
		ITable testCreatePrimaryKey = dataSetTest.getTable("testCreatePrimaryKey");
		int institutionProfileId = Integer.parseInt((String)testCreatePrimaryKey.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String)testCreatePrimaryKey.getValue(0, "INVESTORPROFILEID"));
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		
		InvestorProfilePK newPk = investorProfile.createPrimaryKey();
		
		assertNotNull(newPk);
		srk.rollbackTransaction();
	}

	/*
	 * Failure case not applicable as the create method returns an entity and sending unqualified data will result
	 * in improper end of test class and thus it is avoid.
	 */
	
}
