/**
 * <p>Title: .java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Ltd. Partnership. (c) 2009</p>
 *
 * <p>Company: Filogix Ltd. Partnership</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � 5-May-09
 *
 */
package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

public class StatusNameReplacementTest extends ExpressEntityTestCase 
    implements UnitTestLogging {/*
    
    private static final int DATA_COUNT_NAMEREPLACEMENT = 24;
    private int testSeqForNameReplacement = 0;

    static Short enShort = new Short(""+Mc.LANGUAGE_PREFERENCE_ENGLISH);
    static Short frShort = new Short(""+Mc.LANGUAGE_PREFERENCE_FRENCH);
    
     // The logger
    private final static Logger _logger = LoggerFactory.getLogger(DealEventStatusSnapshot.class);
    private final static int INSTITUTIONPROFILEID = 0;
    // the session resource kit.
    private SessionResourceKit  _srk;
    
    int     statusNameReplacementId;
    String  statusNameReplacement;
    StatusNameReplacement    rep;

    @Before
    public void setup() throws Exception {
        
        // get random seqence number
        Double indexN = new Double(Math.random() * DATA_COUNT_NAMEREPLACEMENT);
        testSeqForNameReplacement = indexN.intValue();
        
        statusNameReplacementId  = _dataRepository.getInt("statusNameReplacement", "STATUSNAMEREPLACEMENTID", testSeqForNameReplacement);
        statusNameReplacement = _dataRepository.getString("statusNameReplacement", "STATUSNAMEREPLACEMENT", testSeqForNameReplacement);

        ResourceManager.init();
        _srk = new SessionResourceKit();
        rep = new StatusNameReplacement(_srk);
    }
    
    
    @After
    public void tearDown() throws Exception{
        _srk.freeResources( );
    }

    @Test
    public void findByPrimaryKeyTest() throws Exception {

        rep = rep.findByPrimaryKey(statusNameReplacementId);
        
        assertEquals(statusNameReplacementId, rep.getStatusNameReplacementId());
        assertEquals(statusNameReplacement, rep.getStatusNameReplacement());
    }
    
*/
	@Test
    public void testTBR(){
        System.out.println("test");
}	
}
