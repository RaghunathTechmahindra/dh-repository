package com.basis100.deal.entity;

import java.io.IOException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.docprep.extract.doctag.SurveyClause;
import com.basis100.deal.docprep.extract.doctag.SysGenCondition;
import com.basis100.deal.docprep.extract.doctag.Term;
import com.basis100.resources.SessionResourceKit;

public class SysGenConditionTest extends FXDBTestCase {
	
	private IDataSet dataSetTest;
	private SysGenCondition sgc = null;
    FMLQueryResult fmlQueryResult = null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public SysGenConditionTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(SysGenCondition.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(SysGenCondition.class.getSimpleName()+"DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected DatabaseOperation getSetUpOperation() throws Exception
	    {
			sgc = new SysGenCondition();
	    	return DatabaseOperation.INSERT;
	    }
	
	public void testExtract() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testExtract");
		
		int institutionProfileId = Integer.parseInt((String)testExtract.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String)testExtract.getValue(0, "dealid"));
		int copyid = Integer.parseInt((String)testExtract.getValue(0, "copyid"));
		int drid =  Integer.parseInt((String)testExtract.getValue(0, "DOCUMENTRESPONSIBILITYROLEID"));
		
		Deal deal = new Deal(srk, null, id, copyid);
		/*//com.basis100.deal.entity.DealEntity de = new com.basis100.deal.entity.DealEntity(srk);
		//DocumentTracking dt = (DocumentTracking)de;
		
		
		com.basis100.deal.entity.DealEntity de1 = (com.basis100.deal.entity.DealEntity )deal;
		deal.setInstitutionProfileId(institutionProfileId);
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		
		int lang = 0;
		fmlQueryResult = sgc.extract(de1, lang, null, srk);
		
		//assert drid == dt.getDocumentResponsibilityRoleId();
		assertNotNull(fmlQueryResult);
		assert fmlQueryResult.getValues().size()>0;
		
		srk.rollbackTransaction();*/
	}
}
