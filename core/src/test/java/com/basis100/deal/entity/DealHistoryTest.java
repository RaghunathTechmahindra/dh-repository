/*
 * @(#)DealHistoryTest.java    2005-3-16
 *
 */

package com.basis100.deal.entity;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import MosSystem.Mc;

import com.basis100.deal.pk.DealHistoryPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.test.ExpressEntityTestCase;

/**
 * DealHistoryTest -
 * 
 * @version 1.0 2005-3-16
 * @author <A HREF="mailto:chyxiang@yahoo.com">Chen Xiang (Sean)</A>
 */
public class DealHistoryTest extends ExpressEntityTestCase {

    // The logger
    private static Log _log = LogFactory.getLog(DealHistoryTest.class);

    // The Session Resource Kit
    private SessionResourceKit _srk;

    // The deal history
    private DealHistory _dealHistoryDAO;

    // institutionId for deal
    private int dealInstitutionId;
    private Deal deal;

    /**
     * Constructor function
     */
    public DealHistoryTest() {
    }

    /**
     * setup.
     */
    @Before
    public void setUp() throws Exception {

        _log.debug("Preparing the test cases ...");
        // initialize resource
        ResourceManager.init();
        _srk = new SessionResourceKit("Testing");

        // get institutionId
        dealInstitutionId = _dataRepository.getInt("Deal", "InstitutionProfileId", 0);
        int copyId = _dataRepository.getInt("Deal", "CopyId", 0);
        int dealId = _dataRepository.getInt("Deal", "DealId", 0);
        
        deal = new Deal(_srk, null, dealId, copyId);
        
        _dealHistoryDAO = new DealHistory(_srk);
    }

    /**
     * test get by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {
        // set institutionid
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        
        // get values from database
        int dealHistoryId = _dataRepository.getInt("DealHistory",
                "DealHistoryId", 0);
        String transactionText = _dataRepository.getString("DealHistory",
                "TransactionText", 0);
        
        // dealhistory find by primary key
        DealHistory dealHistory = _dealHistoryDAO
                .findByPrimaryKey(new DealHistoryPK(dealHistoryId));
        
        // verify result
        assertEquals(transactionText, dealHistory.getTransactionText());
        _log.info("Found Deal History: ");
        _log.info("    Dealhistory Id: " + dealHistory.getDealHistoryId());
        _log.info("           Deal Id: " + dealHistory.getDealId());
        _log.info("    Transaction text: " + dealHistory.getTransactionText());
        _log.info("====================================");
    }

    /**
     * test create
     */
   /* @Test
    public void testCreate() throws Exception {

        // set vpd institutionid
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        String tranText = "transaction text";

        // create dealhistory
        DealHistory dealHistory = _dealHistoryDAO.create(deal.getDealId(), deal
                .getUnderwriterUserId(), Mc.DEAL_TX_TYPE_DATA_CHANGE, deal
                .getStatusId(), tranText, new Date());
        
        // verify result
        assertEquals(tranText, dealHistory.getTransactionText());
        _log.info("Create Deal History: ");
        _log.info("    Dealhistory Id: " + dealHistory.getDealHistoryId());
        _log.info("           Deal Id: " + dealHistory.getDealId());
        _log.info("    Transaction text: " + dealHistory.getTransactionText());
        _log.info("====================================");
    }
*/
    /**
     * test find by deal
     */
    @Test
    public void testFindByDeal() throws Exception {

        // set vpd institutionId
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        
        // get values from database
        DealPK dpk = new DealPK(deal.getDealId(), deal.getCopyId());
        Collection dHistory = _dealHistoryDAO.findByDeal(dpk);
        for (Iterator i = dHistory.iterator(); i.hasNext();) {
            DealHistory dealHistory = (DealHistory) i.next();
            // verify result
            assertEquals(deal.getDealId(), dealHistory.getDealId());
            _log.info("Create Deal History: ");
            _log.info("    Dealhistory Id: " + dealHistory.getDealHistoryId());
            _log.info("           Deal Id: " + dealHistory.getDealId());
            _log.info("    Transaction text: "
                    + dealHistory.getTransactionText());
            _log.info("====================================");
        }
    }
    
    /*
     * teatdown
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }
}
