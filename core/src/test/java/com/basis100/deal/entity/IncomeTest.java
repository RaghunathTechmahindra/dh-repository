/*
 * @(#) IncomeTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */


package com.basis100.deal.entity;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import com.basis100.TestingEnvironmentInitializer;
import com.basis100.deal.calc.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.IncomePK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>IncomeTest</p>
 * Express Entity class unit test: Income
 */
public class IncomeTest extends ExpressEntityTestCase implements
		UnitTestLogging {
	// The logger
	private final static Logger _logger = LoggerFactory
			.getLogger(IncomeTest.class);

	/**
	 * Constructor function
	 */
	public IncomeTest() {
	}

	/**
	 * setting up the testing env.
	 */
	@Before
	public void setUp() {

	}

	/**
	 * clean the testing env.
	 */
	@After
	public void tearDown() {
	}

	/**
	 *  * test the find by primary key  
	 */
	@Test
	public void testFindByPrimaryKey() throws Exception {
		
		// get input data from repository
		int incomeId = _dataRepository.getInt("INCOME",
			"INCOMEID", 0);
		int copyId = _dataRepository.getInt("INCOME", "COPYID", 0);
		int instProfId = _dataRepository.getInt("INCOME",
			"INSTITUTIONPROFILEID", 0);

		// init session resource kit.
		ResourceManager.init();
		SessionResourceKit srk = new SessionResourceKit();
		CalcMonitor cal = CalcMonitor.getMonitor(srk); 
		_logger.info("here ======");

		// feed the input data and run the program.
		srk.getExpressState().setDealInstitutionId(instProfId);
		Income entity = new Income(srk, cal);
		entity = entity.findByPrimaryKey(new IncomePK(
			incomeId, copyId));

		// verify the running result.
		assertEquals(incomeId, entity.getIncomeId());
		assertEquals(copyId, entity.getCopyId());

		// free resources
		srk.freeResources();
	}

	/**
	 * test create.
	 */
	@Test
	public void testCreate() throws Exception {

		// get input data from repository
		int copyId = _dataRepository.getInt("INCOME", "COPYID", 0);
		int borrowerId = _dataRepository.getInt("INCOME", "BORROWERID", 0);
    	// get input data from repository
        int instProfId = _dataRepository.getInt("INCOME", "INSTITUTIONPROFILEID", 0);

		// init session resource kit.
		TestingEnvironmentInitializer.initTestingEnv();
		SessionResourceKit srk = new SessionResourceKit();
		CalcMonitor cal = CalcMonitor.getMonitor(srk); 

		// feed input data and run program.
		srk.getExpressState().setDealInstitutionId(instProfId);
		srk.beginTransaction();
		Income newEntity = new Income(srk, cal);
		BorrowerPK borrowerPK = new BorrowerPK(borrowerId, copyId);
		newEntity = newEntity.create(borrowerPK);
		newEntity.ejbStore();

		// check create correctlly.
		IncomePK incomePK = new IncomePK(newEntity.getIncomeId(), copyId);
		Income foundEntity = new Income(srk, cal);
		foundEntity = foundEntity.findByPrimaryKey(incomePK);

		// verifying
		assertEquals(foundEntity.getIncomeId(), newEntity.getIncomeId());
		srk.rollbackTransaction();
		srk.freeResources( );
	}

}
