package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.JobTitlePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;


/**
 * 
 * JobTitleTest
 *
 * @version 1.0 Sep 5, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */

public class JobTitleTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(JobTitleTest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test the find by primary key
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {
      //get input data from repository
        int jobTitleID = _dataRepository.getInt("JobTitle", "jobTitleID", 1);

        _logger.info(BORDER_START, "findByPrimaryKey");

        //Create the initial entity
        JobTitle _jobTitle = new JobTitle(_srk);
        
        //Find by primary key
        _jobTitle = _jobTitle.findByPrimaryKey(new JobTitlePK(jobTitleID));
        
        //Check if the data retrieved matches the DB
        assertTrue(_jobTitle.getJobTitleDescription()
                .equals(
                        _dataRepository.getString("JobTitle",
                                "JOBTITLEDESCRIPTION", 1)));
        _logger.info(BORDER_END, "findByPrimaryKey");

    }

    /**
     * test the find by Name
     */
    @Test
    public void testFindByName() throws Exception {
      //get input data from repository
        int jobTitleID = _dataRepository.getInt("JobTitle", "jobTitleID", 1);
        String jobTitleDesc = _dataRepository.getString("JobTitle",
                "JOBTITLEDESCRIPTION", 1);

        _logger.info(BORDER_START, "FindByName");

        //Create the initial entity
        JobTitle _jobTitle = new JobTitle(_srk);
        
        //Find by name
        _jobTitle = _jobTitle.findByName(jobTitleDesc);
        
        //Check if the data retrieved matches the DB
        assertTrue(_jobTitle.getJobTitleId() == jobTitleID);
        
        _logger.info(BORDER_END, "FindByName");

    }
}
