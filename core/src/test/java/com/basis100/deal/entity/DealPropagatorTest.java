package com.basis100.deal.entity;

import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;

public class DealPropagatorTest extends ExpressEntityTestCase {


    private final static Logger logger = LoggerFactory.getLogger(DealPropagatorTest.class);

    // the session resource kit.
    private SessionResourceKit _srk;


    // institutionId for deal
    private int dealInstitutionId;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() throws Exception {

        // initializing the test environment.
        ResourceManager.init();
        _srk = new SessionResourceKit("Testing");
        _srk.beginTransaction();
        // get institutionId
        dealInstitutionId = _dataRepository.getInt("Deal", "InstitutionProfileId", 0);
    }

    /**
     * Test Index change
     */
    @Test
    public void testUpdatePropagate() throws Exception {

        int dealId = 935439;//_dataRepository.getInt("Deal", "DealId", 4);
        int copyId = 8;//_dataRepository.getInt("Deal", "CopyId", 4);
        
        logger.info("dealid = " + dealId);
        logger.info("copyId = " + copyId);
        
        DealPropagator dealPropagator = new DealPropagator(_srk, null, dealId, copyId);
        
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        
        MasterDeal md = new MasterDeal(_srk, null, dealId);
        
        Collection<Integer> copyids = md.getCopyIds();
        
        logger.debug("number of copy = " + copyids.size());
        
        if(copyids.size() <= 1){
            fail("try this test with muitiple copy data");    
        }
        
        final double orgPremium = dealPropagator.getMIPremiumAmount();
        
        logger.debug("miPremiumAmount = " + orgPremium);
        
        final double newPremium = orgPremium + 2000d;
        
        logger.debug("miPremiumAmount new value = " + newPremium);
        
        dealPropagator.setMIPremiumAmount(newPremium);
        
        dealPropagator.ejbStore();
        
        
        for(Integer aCid : copyids){
            Deal deal = new Deal(_srk, null, dealId, aCid);
            logger.info("copyid = " + aCid + " miPremiumAmount=" + deal.getMIPremiumAmount());
            assertEquals(newPremium, deal.getMIPremiumAmount(), 0.001);
        }
        
        logger.debug(" test done");
        
    }
    
/*    @Test
    public void testRunningAllCalcs() throws Exception {

        int dealId = _dataRepository.getInt("Deal", "DealId", 0);
        int copyId = _dataRepository.getInt("Deal", "CopyId", 0);
        
        logger.info("dealid = " + dealId);
        logger.info("copyId = " + copyId);
        
        CalcMonitor dcm = CalcMonitor.getMonitor(_srk);
        DealPropagator dealPropagator = new DealPropagator(_srk, dcm, dealId, copyId);
        
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        
        MasterDeal md = new MasterDeal(_srk, null, dealId);
        
        Collection<Integer> copyids = md.getCopyIds();
        
        logger.debug("number of copy = " + copyids.size());
        
        if(copyids.size() <= 1){
            fail("try this test with muitiple copy data");    
        }
        
        //NewMoney calc :
        //New Money = deal.Total Loan Amount - deal.Existing Loan Amount
        
        logger.info("dealPropagator.getExistingLoanAmount = " + dealPropagator.getExistingLoanAmount());
        logger.info("dealPropagator.getTotalLoanAmount = " + dealPropagator.getTotalLoanAmount());
        logger.info("dealPropagator.getNewMoney = " + dealPropagator.getNewMoney());
        
        final double dummyExistingLoanAmount = dealPropagator.getExistingLoanAmount() + 837589;
        
        dealPropagator.setExistingLoanAmount(dummyExistingLoanAmount);
        
        dealPropagator.ejbStore();
        
//        List<CalcMonitor> dcms = dealPropagator.getCalcMonitors();
//        assertNotNull(dcms);
//        assertFalse(dcms.isEmpty());
        
        dcm.calc();
//        for(CalcMonitor dcm : dcms){
//            logger.info("executing calcMonitor");
//            dcm.calc();
//        }
        
        for(Integer aCid : copyids){
            Deal deal = new Deal(_srk, null, dealId, aCid);
            
            
            logger.info("copy = " + aCid);
            logger.info("getExistingLoanAmount = " + deal.getExistingLoanAmount());
            logger.info("getTotalLoanAmount = " + deal.getTotalLoanAmount());
            logger.info("getNewMoney = " + deal.getNewMoney());
            
            assertEquals(deal.getNewMoney(), deal.getTotalLoanAmount() - deal.getExistingLoanAmount(), 0.01);
        }
        
        logger.debug(" test done");
        
    }*/

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.cleanTransaction();
        _srk.freeResources();
    }
}
