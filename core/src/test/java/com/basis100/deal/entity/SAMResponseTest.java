/*
 * @(#)SAMResponseTest.java    2007-9-7
 *
 * Copyright (C) 2007 Filogix Limited Partnership All rights reserved.
 */


package com.basis100.deal.entity;

import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.AdjudicationResponsePK;
import com.basis100.deal.pk.SAMResponsePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>SAMResponseTest</p>
 * Express Entity class unit test: SAMResponse
 */
public class SAMResponseTest extends ExpressEntityTestCase 
    implements UnitTestLogging {
  
    private final static Logger _logger = 
        LoggerFactory.getLogger(RegionProfileTest.class);
    private final static String ENTITY_NAME = "SAMRESPONSE"; 
    
    // retreived data count that is stored in UnitTestDataRepository.xml
    private static final int DATA_COUNT = 1;
  
    private SessionResourceKit _srk;
  
    // use this value for random sequence number data
    private int testSeqForGen = 0;
  
    // properties
    protected int           responseId;
    protected int         samDecisionId;
    protected double      samAmountApproved;
    protected String    samResult;
    protected String      samXML;
    protected int     instProfId;
        
    /**
     * <p>setUp</p>
     * Setting up test case.
     */
    @Before
    public void setUp() throws Exception {

        _logger.info(BORDER_START, ENTITY_NAME + " TestCase setting up ...");
  
        //  init session resource kit.
        ResourceManager.init();
        //  init session resource kit.
        _srk = new SessionResourceKit();
  
//        // get random seqence number 
//        Double indexD = new Double(Math.random() * DATA_COUNT);
//        testSeqForGen = indexD.intValue();
  
        // retreive data from test data file
        responseId 
            = _dataRepository.getInt( ENTITY_NAME, "RESPONSEID", testSeqForGen);
        samDecisionId 
            = _dataRepository.getInt(ENTITY_NAME, "SAMDECISIONID", testSeqForGen);
        samAmountApproved 
            = _dataRepository.getDouble( ENTITY_NAME, "SAMAMOUNTAPPROVED", testSeqForGen);
        samResult 
            = _dataRepository.getString(ENTITY_NAME, "SAMRESULT", testSeqForGen);
        samXML 
            = _dataRepository.getString( ENTITY_NAME, "SAMXML", testSeqForGen);
        instProfId 
            = _dataRepository.getInt( ENTITY_NAME, "INSTITUTIONPROFILEID", testSeqForGen);
        
        _logger.info(BORDER_END, "Setting up Done");
    }
    
    @After
    public void tearDown() {
      
        //      free resources
        _srk.freeResources( );
  
    }

    /**
     * test findByPrimaryKey.
     */
    @Ignore @Test
    public void testFindByPrimaryKey()  throws Exception {
    
        _logger.info(BORDER_START, "findByPrimaryKey");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
        
        // excute method
        SAMResponse entity = new SAMResponse(_srk);
        entity = entity.findByPrimaryKey(new SAMResponsePK(responseId));
              
        // assertEquals
        assertEquals(entity.getResponseId(), responseId);
        assertEquals(entity.getSAMDecisionId(), samDecisionId);
        assertEquals(entity.getSAMAmountApproved(), samAmountApproved, Double.MIN_VALUE);
        assertEquals(entity.getSAMResult(), samResult);
        assertEquals(entity.getSAMXML(), samXML);
    }

    /**
     * test findByPrimaryKey.
     */
    @Ignore @Test
    public void testFindByAdjudicationResponse()  throws Exception {
    
        _logger.info(BORDER_START, "findByAdjudicationResponse");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
        
        // excute method
        SAMResponse entity = new SAMResponse(_srk);
        Collection responses 
            = entity.findByAdjudicationResponse(new AdjudicationResponsePK(responseId));
              
        // assert
        // TODO:
        _logger.info(BORDER_END, "findByAdjudicationResponse");
    }
    
}
