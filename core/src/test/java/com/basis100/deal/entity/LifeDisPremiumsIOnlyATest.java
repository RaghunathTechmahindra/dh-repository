/*
 * @(#)LifeDisPremiumsIOnlyA.java Sep 13, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.LifeDisPremiumsIOnlyAPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * LifeDisPremiumsIOnlyA
 * 
 * @version 1.0 Sep 13, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class LifeDisPremiumsIOnlyATest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(LifeDisPremiumsIOnlyATest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test the create with PK
     */
    @Test
    public void testCreate() throws Exception {
        // get input data from repository
        int lifeDisabilityPremiumsID = 999;
        int copyID = 1;

        // TODO: Change to this table once we have data
        int dealInstitutionId = _dataRepository.getInt(
                "LifeDisabilityPremiums", "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // Start new transaction
        _srk.beginTransaction();

        _logger.info(BORDER_START, "testCreate");

        // Create the initial entity
        LifeDisPremiumsIOnlyA lifeDisabilityPremiums = new LifeDisPremiumsIOnlyA(
                _srk);

        // Create a new entity
        lifeDisabilityPremiums = lifeDisabilityPremiums
                .create(new LifeDisPremiumsIOnlyAPK(lifeDisabilityPremiumsID,
                        copyID));

        // Store it in the DB
        lifeDisabilityPremiums.ejbStore();

        // Rollback transaction
        _srk.rollbackTransaction();

        _logger.info(BORDER_END, "testCreate");

    }

}
