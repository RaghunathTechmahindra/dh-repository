package com.basis100.deal.entity;

import java.io.IOException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.docprep.extract.doctag.SupportIncomeTotal;
import com.basis100.resources.SessionResourceKit;

public class SupportIncomeTotalTest extends FXDBTestCase {
	private IDataSet dataSetTest;
	private SupportIncomeTotal supportIncomeTotal = null;
	private Borrower b = null;
    CalcMonitor dcm =null;
    FMLQueryResult fmlQueryResult =null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public SupportIncomeTotalTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(SupportIncomeTotal.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	

	@Override
	protected DatabaseOperation getSetUpOperation() throws Exception
	    {
		supportIncomeTotal = new SupportIncomeTotal();
	    	return DatabaseOperation.INSERT;
	    }
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}
	
	public void testExtract() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testExtract");
		
		int institutionProfileId = Integer.parseInt((String)testExtract.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String)testExtract.getValue(0, "dealid"));
		int copyid = Integer.parseInt((String)testExtract.getValue(0, "copyid"));
	
		Deal deal = new Deal(srk, null, id, copyid);
		com.basis100.deal.entity.DealEntity de = (com.basis100.deal.entity.DealEntity )deal;
		deal.setInstitutionProfileId(institutionProfileId);
		
		///b = (Borrower)de;
				
		int lang = 0;
		fmlQueryResult = supportIncomeTotal.extract(de, lang, null, srk);
		
		//assertTrue(b.getIncomes().size() > 0);
		assertNotNull(fmlQueryResult);
		assert fmlQueryResult.getValues().size()>0;
		
		srk.rollbackTransaction();
	}
	
}
