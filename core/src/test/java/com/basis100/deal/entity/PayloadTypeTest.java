/*
 * @(#) PayloadTypeTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filogix.express.test.UnitTestLogging;
import com.filogix.express.test.ExpressEntityTestCase;

import com.basis100.deal.pk.PayloadTypePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

/**
 * @author amalhi
 *
 * JUnit test for entity PayloadType
 *
 */
public class PayloadTypeTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(PayloadTypeTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public PayloadTypeTest() {

    }

    /**
     * To be executed before test
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {

        // free resources
        _srk.freeResources();
    }

    /**
     * test FindByPrimaryKey method of entity
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int payloadTypeId = _dataRepository.getInt("PayloadType",
                "payloadtypeid", 0);
        int documentTypeId = _dataRepository.getInt("PayloadType",
                "documentTypeId", 0);
        int instProfId = _dataRepository.getInt("PayloadType",
                "INSTITUTIONPROFILEID", 0);
        String payloadTypeDesc = _dataRepository.getString("PayloadType",
                "payloadTypeDesc", 0);

        _logger.info(BORDER_START, "findByPrimaryKey");
        _logger.info("Got Values ::: [{}]", payloadTypeId + "::"
                + documentTypeId + "::" + instProfId + "::" + payloadTypeDesc);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        PayloadType _payloadType = new PayloadType(_srk);

        // Find by primary key
        _payloadType = _payloadType.findByPrimaryKey(new PayloadTypePK(
                payloadTypeId));

        _logger.info("Got Values from new PayloadType ::: [{}]", _payloadType
                .getPayloadTypeId()
                + "::" + _payloadType.getDocumentTypeId());

        // Check if the data retrieved matches the DB
        assertEquals(payloadTypeId, _payloadType.getPayloadTypeId());
        assertEquals(documentTypeId, _payloadType.getDocumentTypeId());
        assertEquals(payloadTypeDesc, _payloadType.getPayloadTypeDesc());

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

}
