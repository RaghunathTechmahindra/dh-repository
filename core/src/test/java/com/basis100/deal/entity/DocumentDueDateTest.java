package com.basis100.deal.entity;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.impl.DocumentDueDate;
import com.basis100.resources.SessionResourceKit;
import com.basis100.deal.calc.CalcEntityCache;
import com.ltx.unittest.util.EntityTestUtil;


		public class DocumentDueDateTest extends FXDBTestCase {
			private IDataSet dataSetTest;
			private DocumentDueDate documentDueDate = null;
			SessionResourceKit srk = new SessionResourceKit();
			private Condition condition;
			private CalcMonitor dcm;
			private Deal deal;
		
			public DocumentDueDateTest(String name) throws IOException,
					DataSetException {
				super(name);
				dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
						DocumentDueDate.class.getSimpleName() + "DataSetTest.xml"));
			}
		
			@Override
			protected DatabaseOperation getTearDownOperation() throws Exception {
				srk.freeResources();
				return DatabaseOperation.DELETE;
			}
		
			@Override
			protected DatabaseOperation getSetUpOperation() throws Exception {
				documentDueDate = new DocumentDueDate();
				return DatabaseOperation.INSERT;
			}
		
			public void testGetTarget() throws Exception {
				srk.beginTransaction();
				ITable testGetTarget = dataSetTest.getTable("testGetTarget");
		
				int conditionId = Integer.parseInt((String) testGetTarget.getValue(0,
						"CONDITIONID"));
				/*int copyId = Integer.parseInt((String) testGetTarget.getValue(0,
						"COPYID"));*/
				documentDueDate = new DocumentDueDate();
				srk.getExpressState().setDealInstitutionId(1);
		
				setEntityCacheAndSession();
				getDealCalc().getTarget(condition, dcm);
		
				assert documentDueDate.getTargets() != null;
				assert documentDueDate.getTargets().size() >= 1;
				srk.rollbackTransaction();
			}
		
			/*public void testDoCalc() throws Exception {
				try {
					srk.beginTransaction();
					ITable testDoCalc = dataSetTest.getTable("testDoCalc");
		
					int dealId = Integer.parseInt((String) testDoCalc.getValue(0,
							"DEALID"));
					int copyId = Integer.parseInt((String) testDoCalc.getValue(0,
							"COPYID"));
					deal = new Deal(srk, dcm, dealId, copyId);
					// mtgProd = new MtgProd(srk, dcm, mtgProdId);
					srk.getExpressState().setDealInstitutionId(1);
					setEntityCacheAndSession();
					// Trigger the Calculation
					getDealCalc().doCalc(condition);
					assert documentDueDate.getCalcNumber() != null;
					assert documentDueDate.getCalcNumber().length() >= 0;
					srk.rollbackTransaction();
				} catch (Exception ex) {
					throw ex;
				}
		
			}
		*/
			public void setEntityCacheAndSession() {
				Class classCalc = documentDueDate.getClass();
				Class superClassofCalc = classCalc.getSuperclass();
				Field ff[] = superClassofCalc.getDeclaredFields();
				for (Field field : ff) {
					field.setAccessible(true);
					if (field.getName().equalsIgnoreCase("entityCache")) {
						Class fieldClasstest = field.getType();
						System.out.println(fieldClasstest.getName());
						Constructor con[] = fieldClasstest.getDeclaredConstructors();
						for (Constructor conss : con) {
							conss.setAccessible(true);
							System.out.println(conss.getModifiers() + " "
									+ conss.getName());
		
							conss.setAccessible(true);
							CalcEntityCache cache;
							try {
								cache = (CalcEntityCache) conss
										.newInstance(EntityTestUtil.getCalcMonitor());
								field.set(documentDueDate, cache);
							} catch (Exception e) {
								e.printStackTrace();
							}
		
						}
						break;
					}
				}
		
				documentDueDate.setResourceKit(EntityTestUtil.getSessionResourceKit());
			}
		
			public DocumentDueDate getDealCalc() {
				return documentDueDate;
			}
		
		}
