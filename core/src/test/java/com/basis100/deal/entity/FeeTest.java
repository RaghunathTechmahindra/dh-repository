/*
 * @(#) FeeTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */


package com.basis100.deal.entity;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import com.basis100.deal.calc.CalcMonitor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.FeePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>FeeTest</p>
 * Express Entity class unit test: Fee
 */
public class FeeTest extends ExpressEntityTestCase implements UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(FeeTest.class);

    /**
     * Constructor function
     */
    public FeeTest() {
    }

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int feeId = _dataRepository.getInt("FEE", "FEEID", 0);
        // get input data from repository
        int instProfId = _dataRepository.getInt("FEE", "INSTITUTIONPROFILEID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");
        CalcMonitor cal = CalcMonitor.getMonitor(srk);

        // feed the input data and run the program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        Fee entity = new Fee(srk, cal);
        entity = entity.findByPrimaryKey(new FeePK(feeId));

        // verify the running result.
        assertEquals(feeId, entity.getFeeId());

        // free resources
        srk.freeResources();
    }

    /**
     * test create.
     */
   /* @Test
    public void testCreate() throws Exception {
        String glaccount = _dataRepository.getString("FEE", "GLACCOUNT", 0);
        int feeTypeId = _dataRepository.getInt("FEE", "FEETYPEID", 0);
        int offsetingFeeTypeId = _dataRepository.getInt("FEE", "OFFSETINGFEETYPEID", 0);
        int defaultPaymentMethodId = _dataRepository.getInt("FEE",
                "DEFAULTPAYMENTMETHODID", 0);
        int feePayorTypeId = _dataRepository.getInt("FEE", "FEEPAYORTYPEID", 0);
        String feeBusinessId = _dataRepository.getString("FEE", "FEEBUSINESSID", 0);
        String payableIndicator = _dataRepository.getString("FEE", "PAYABLEINDICATOR", 0);
        double defaultFeeAmount = _dataRepository.getDouble("FEE",
                "DEFAULTFEEAMOUNT", 0);
        int defaultFeePaymentDateTypeId = _dataRepository.getInt("FEE",
                "DEFAULTFEEPAYMENTDATETYPEID", 0);
        int feeGenerationTypeId = _dataRepository.getInt("FEE", "FEEGENERATIONTYPEID", 0);
        String miTypeFlag = _dataRepository.getString("FEE", "MITYPEFLAG", 0);
        String includeInTotalFeeAmount = _dataRepository.getString("FEE",
                "INCLUDEINTOTALFEEAMOUNT", 0);
        int instProfId = _dataRepository.getInt("FEE", "INSTITUTIONPROFILEID", 0);
        
        Integer FCXFeeTypeId = new Integer(5);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");
        CalcMonitor cal = CalcMonitor.getMonitor(srk);

        // feed input data and run program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        srk.beginTransaction();
        Fee newEntity = new Fee(srk, cal);
        newEntity = newEntity.create(glaccount, feeTypeId, offsetingFeeTypeId,
                defaultPaymentMethodId, feePayorTypeId, feeBusinessId,
                payableIndicator, defaultFeeAmount,
                defaultFeePaymentDateTypeId, feeGenerationTypeId, miTypeFlag,
                includeInTotalFeeAmount);
        newEntity.setFCXFeeTypeID(FCXFeeTypeId);
        newEntity.ejbStore();

        // check create correctlly.
        Fee foundEntity = new Fee(srk, cal);
        foundEntity = foundEntity.findByPrimaryKey(new FeePK(newEntity.getFeeId()));

        // verifying
        assertEquals(foundEntity.getFeeId(), newEntity.getFeeId());
        assertEquals(foundEntity.getFCXFeeTypeID(), newEntity.getFCXFeeTypeID());

        // rollback transaction.
        srk.rollbackTransaction();
        // free resources.
        srk.freeResources();
    }*/

}
