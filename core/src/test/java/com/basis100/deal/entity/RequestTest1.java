package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Date;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.RequestPK;

public class RequestTest1 extends  FXDBTestCase{
	
	private IDataSet dataSetTest;
	Request request=null;
	public RequestTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("RequestDataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		request=new Request(srk);
	}
	
	//createPrimaryKey(int)

	public void testCreatePrimaryKey() throws Exception{
		ITable testFindByPrimaryKey = dataSetTest.getTable("testCreatePrimaryKey");
		int copyId =Integer.parseInt(testFindByPrimaryKey.getValue(0,"CopyId").toString());
		RequestPK requestPK=request.createPrimaryKey(copyId);
		assertNotNull(requestPK);
	}
   //create(DealPK, int, Date, int)
	public void testCreate() throws Exception{
		srk.beginTransaction();
		ITable testFindByPrimaryKey = dataSetTest.getTable("testCreate");
		int requestStatusId =Integer.parseInt(testFindByPrimaryKey.getValue(0,"RequestStatusId").toString());
		int userProfileId =Integer.parseInt(testFindByPrimaryKey.getValue(0,"UserProfileId").toString());
		int id =Integer.parseInt(testFindByPrimaryKey.getValue(0,"Id").toString());
		int copyId =Integer.parseInt(testFindByPrimaryKey.getValue(0,"CopyId").toString());
		int dealInstitutionId=Integer.parseInt(testFindByPrimaryKey.getValue(0,"DealInstitutionId").toString());
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		DealPK dpk=new DealPK(id, copyId);
		Date statusDate=new Date();
		//DealPK dpk, int requestStatusId, Date statusDate,int userProfileId
		Request request1=request.create(dpk,requestStatusId,statusDate,userProfileId);
		assertNotNull(request1);
		srk.rollbackTransaction();
	}
	public void testCreateFailure() throws Exception{
		boolean status=false;
		try{
		srk.beginTransaction();
		ITable testFindByPrimaryKey = dataSetTest.getTable("testCreateFailure");
		int requestStatusId =Integer.parseInt(testFindByPrimaryKey.getValue(0,"RequestStatusId").toString());
		int userProfileId =Integer.parseInt(testFindByPrimaryKey.getValue(0,"UserProfileId").toString());
		int id =Integer.parseInt(testFindByPrimaryKey.getValue(0,"Id").toString());
		int copyId =Integer.parseInt(testFindByPrimaryKey.getValue(0,"CopyId").toString());
		int dealInstitutionId=Integer.parseInt(testFindByPrimaryKey.getValue(0,"DealInstitutionId").toString());
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		DealPK dpk=new DealPK(id, copyId);
		Date statusDate=new Date();
		//DealPK dpk, int requestStatusId, Date statusDate,int userProfileId
		Request request1=request.create(dpk,requestStatusId,statusDate,userProfileId);
		
		srk.rollbackTransaction();
		}catch (Exception e) {
			// TODO: handle exception
			status=false;
		}
		assertSame(false,status);
	}
	
	//findByChannelTranKeyAndServProduct(String, String)
	public void testFindByChannelTranKeyAndServProduct() throws Exception{
		ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByChannelTranKeyAndServProduct");
		String ctk =testFindByPrimaryKey.getValue(0,"ctk").toString();
		String serviceProductName = testFindByPrimaryKey.getValue(0,"ServiceProductName").toString();
		//String ctk,String serviceProductName
		Request request1=request.findByChannelTranKeyAndServProduct(ctk,serviceProductName);
		assertNotNull(request1);
	}
	
	public void testFindByChannelTranKeyAndServProductFailure() throws Exception{
		boolean status=false;
		try{
		ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByChannelTranKeyAndServProductFailure");
		String ctk =testFindByPrimaryKey.getValue(0,"ctk").toString();
		String serviceProductName = testFindByPrimaryKey.getValue(0,"ServiceProductName").toString();
		//String ctk,String serviceProductName
		Request request1=request.findByChannelTranKeyAndServProduct(ctk,serviceProductName);
		}catch (Exception e) {
			// TODO: handle exception
			status=false;
		}
		assertSame(false,status);
	}
	//findRequestsByDealIdAndCopyId(DealPK)

	public void testFindRequestsByDealIdAndCopyId() throws Exception{
	
		ITable testFindRequestsByDealIdAndCopyId = dataSetTest.getTable("testFindRequestsByDealIdAndCopyId");
		int id =Integer.parseInt(testFindRequestsByDealIdAndCopyId.getValue(0,"Id").toString());
		int copyId =Integer.parseInt(testFindRequestsByDealIdAndCopyId.getValue(0,"CopyId").toString());
		DealPK dpk=new DealPK(id, copyId);
		int size=request.findRequestsByDealIdAndCopyId(dpk).size();
		assertNotSame(0, size);
	}
	
	public void testFindRequestsByDealIdAndCopyIdFailure() throws Exception{
		boolean status=false;
		try{
		ITable testFindRequestsByDealIdAndCopyIdFailure = dataSetTest.getTable("testFindRequestsByDealIdAndCopyIdFailure");
		int id =Integer.parseInt(testFindRequestsByDealIdAndCopyIdFailure.getValue(0,"Id").toString());
		int copyId =Integer.parseInt(testFindRequestsByDealIdAndCopyIdFailure.getValue(0,"CopyId").toString());
		DealPK dpk=new DealPK(id, copyId);
		int size=request.findRequestsByDealIdAndCopyId(dpk).size();
		}catch (Exception e) {
			status=false;
		}
		assertSame(false,status);
	}
	
	//findLastCollectionOfRequest(DealPK)

	public void testFindLastCollectionOfRequest() throws Exception{
		
		ITable testFindLastCollectionOfRequest = dataSetTest.getTable("testFindLastCollectionOfRequest");
		int id =Integer.parseInt(testFindLastCollectionOfRequest.getValue(0,"Id").toString());
		int copyId =Integer.parseInt(testFindLastCollectionOfRequest.getValue(0,"CopyId").toString());
		DealPK dpk=new DealPK(id, copyId);
		int size=request.findLastCollectionOfRequest(dpk).size();
		assertNotSame(0, size);
	}
	
        public void testFindLastCollectionOfRequestFailure() throws Exception{
		
        	boolean status=false;
    		try{
    		ITable testFindLastCollectionOfRequestFailure = dataSetTest.getTable("testFindLastCollectionOfRequestFailure");
    		int id =Integer.parseInt(testFindLastCollectionOfRequestFailure.getValue(0,"Id").toString());
    		int copyId =Integer.parseInt(testFindLastCollectionOfRequestFailure.getValue(0,"CopyId").toString());
    		DealPK dpk=new DealPK(id, copyId);
    		int size=request.findRequestsByDealIdAndCopyId(dpk).size();
    		}catch (Exception e) {
    			status=false;
    		}
    		assertSame(false,status);
	}
        
        //findLastRequest(DealPK)


    	public void testFindLastRequest() throws Exception{
    		ITable testFindLastRequest = dataSetTest.getTable("testFindLastRequest");
    		int id =Integer.parseInt(testFindLastRequest.getValue(0,"Id").toString());
    		int copyId =Integer.parseInt(testFindLastRequest.getValue(0,"CopyId").toString());
    		DealPK dpk=new DealPK(id, copyId);
    		Request Request1=request.findLastRequest(dpk);
    		assertNotNull(Request1);
    	}
    	

    	public void testFindLastRequestFailure() throws Exception{
    		boolean status=false;
    		try{
    		ITable testFindLastRequestFailure = dataSetTest.getTable("testFindLastRequestFailure");
    		int id =Integer.parseInt(testFindLastRequestFailure.getValue(0,"Id").toString());
    		int copyId =Integer.parseInt(testFindLastRequestFailure.getValue(0,"CopyId").toString());
    		DealPK dpk=new DealPK(id, copyId);
    		int size=request.findRequestsByDealIdAndCopyId(dpk).size();
    		}catch (Exception e) {
    			status=false;
    		}
    		assertSame(false,status);
    	}
    	//findLastRequestByRequestId(int)

    	public void testLastRequestByRequestId() throws Exception{
    		ITable testFindLastRequest = dataSetTest.getTable("testLastRequestByRequestId");
    		int RequestId =Integer.parseInt(testFindLastRequest.getValue(0,"RequestId").toString());
    		Request Request1=request.findLastRequestByRequestId(RequestId);
    		assertNotNull(Request1);
    	}
    	public void testLastRequestByRequestIdFailure() throws Exception{
    		boolean status=false;
    		try{
    		ITable testFindLastRequest = dataSetTest.getTable("testLastRequestByRequestIdFailure");
    		int RequestId =Integer.parseInt(testFindLastRequest.getValue(0,"RequestId").toString());
    		Request Request1=request.findLastRequestByRequestId(RequestId);
    		status=true;
    		}catch (Exception e) {
				// TODO: handle exception
    			status=false;
			}
    		assertSame(false,status);
    	}
    	//findLastRequestByDealIdandCopyIdandServiceSubTypeId(int, int, int)

    	public void testLastRequestByDealIdandCopyIdandServiceSubTypeId() throws Exception{
    		ITable testLastRequestByDealIdandCopyIdandServiceSubTypeId = dataSetTest.getTable("testLastRequestByDealIdandCopyIdandServiceSubTypeId");
    		int id =Integer.parseInt(testLastRequestByDealIdandCopyIdandServiceSubTypeId.getValue(0,"Id").toString());
    		int CopyId =Integer.parseInt(testLastRequestByDealIdandCopyIdandServiceSubTypeId.getValue(0,"CopyId").toString());
    		int ServiceSubTypeId =Integer.parseInt(testLastRequestByDealIdandCopyIdandServiceSubTypeId.getValue(0,"ServiceSubTypeId").toString());
    		//int dealId, int copyId, int serviceSubTypeId
    		Request Request1=request.findLastRequestByDealIdandCopyIdandServiceSubTypeId(id,CopyId,ServiceSubTypeId);
    		assertNotNull(Request1);
    	}
    	
    	public void testLastRequestByDealIdandCopyIdandServiceSubTypeIdFailure() throws Exception{
    		boolean status=false;
    		try{
    		ITable testLastRequestByDealIdandCopyIdandServiceSubTypeIdFailure = dataSetTest.getTable("testLastRequestByDealIdandCopyIdandServiceSubTypeIdFailure");
    		int id =Integer.parseInt(testLastRequestByDealIdandCopyIdandServiceSubTypeIdFailure.getValue(0,"Id").toString());
    		int CopyId =Integer.parseInt(testLastRequestByDealIdandCopyIdandServiceSubTypeIdFailure.getValue(0,"CopyId").toString());
    		int ServiceSubTypeId =Integer.parseInt(testLastRequestByDealIdandCopyIdandServiceSubTypeIdFailure.getValue(0,"ServiceSubTypeId").toString());
    		//int dealId, int copyId, int serviceSubTypeId
    		Request Request1=request.findLastRequestByDealIdandCopyIdandServiceSubTypeId(id,CopyId,ServiceSubTypeId);
    		status=true;
    		}catch (Exception e) {
				// TODO: handle exception
    			status=false;
			}
    		assertSame(status, false);
    	}
    	
    	//findContactId()
    	public void testFindContactId() throws Exception{
    		ITable testFindContactId = dataSetTest.getTable("testFindContactId");
    		int id =Integer.parseInt(testFindContactId.getValue(0,"Id").toString());
    		int CopyId =Integer.parseInt(testFindContactId.getValue(0,"CopyId").toString());
    		request=new Request(srk,id,CopyId);
    		int  result=request.findContactId();
    		assertNotSame(0,result);
    	}
    	
    	public void testFindContactIdFailure() throws Exception{
    		boolean status=false;
    		try{
    		ITable testFindContactIdFailure = dataSetTest.getTable("testFindContactIdFailure");
    		int id =Integer.parseInt(testFindContactIdFailure.getValue(0,"Id").toString());
    		int CopyId =Integer.parseInt(testFindContactIdFailure.getValue(0,"CopyId").toString());
    		request=new Request(srk,id,CopyId);
    		int  result=request.findContactId();
    		}catch (Exception e) {
				// TODO: handle exception
    			status=false;
			}
    		assertSame(status,false);
    	}
    	//findDatxTimeout(int, int)
    	public void testFindDatxTimeout() throws Exception{
    		ITable testFindDatxTimeout = dataSetTest.getTable("testFindDatxTimeout");
    		int serviceProductId =Integer.parseInt(testFindDatxTimeout.getValue(0,"ServiceProductId").toString());
    		int channelId =Integer.parseInt(testFindDatxTimeout.getValue(0,"ChannelId").toString());
    		int  result=request.findDatxTimeout(serviceProductId,channelId);
    		assertSame(120,result);
    	}
    	
    	public void testFindDatxTimeoutFailure() throws Exception{
    		boolean status=false;
    		try{
    		ITable testFindDatxTimeout = dataSetTest.getTable("testFindDatxTimeoutFailure");
    		int serviceProductId =Integer.parseInt(testFindDatxTimeout.getValue(0,"ServiceProductId").toString());
    		int channelId =Integer.parseInt(testFindDatxTimeout.getValue(0,"ChannelId").toString());
    		int  result=request.findDatxTimeout(serviceProductId,channelId);
    		}catch (Exception e) {
				// TODO: handle exception
    			status=false;
			}
    		assertSame(false,status);
    	}
    	//findDocumentTypeId(int)
    	public void testFindDocumentTypeId() throws Exception{
    		Request request=null;
    		ITable testFindDatxTimeout = dataSetTest.getTable("testFindDocumentTypeId");
    		int payloadTypeId =Integer.parseInt(testFindDatxTimeout.getValue(0,"PayloadTypeId").toString());
    		int id =Integer.parseInt(testFindDatxTimeout.getValue(0,"Id").toString());
    		int copyId =Integer.parseInt(testFindDatxTimeout.getValue(0,"CopyId").toString());
    		request=new Request(srk,id,copyId);
    		int  result=request.findDocumentTypeId(payloadTypeId);
    		assertSame(90,result);
    	}
    
    	public void testFindDocumentTypeIdFailure() throws Exception{
    		
    		boolean status=false;
    		try{
    		Request request=null;
    		ITable testFindDatxTimeout = dataSetTest.getTable("testFindDocumentTypeId");
    		int payloadTypeId =Integer.parseInt(testFindDatxTimeout.getValue(0,"PayloadTypeId").toString());
    		int id =Integer.parseInt(testFindDatxTimeout.getValue(0,"Id").toString());
    		int copyId =Integer.parseInt(testFindDatxTimeout.getValue(0,"CopyId").toString());
    		request=new Request(srk,id,copyId);
    		int  result=request.findDocumentTypeId(payloadTypeId);
    		}catch (Exception e) {
				// TODO: handle exception
    			status=false;
			}
    		assertSame(false,status);
    	}
    	//createChildAssocs(int)
    	/*public void testCreateChildAssocs() throws Exception{
    		Request request=null;
    		ITable testFindDatxTimeout = dataSetTest.getTable("testFindDocumentTypeId");
    		int payloadTypeId =Integer.parseInt(testFindDatxTimeout.getValue(0,"PayloadTypeId").toString());
    		int id =Integer.parseInt(testFindDatxTimeout.getValue(0,"Id").toString());
    		int copyId =Integer.parseInt(testFindDatxTimeout.getValue(0,"CopyId").toString());
    		srk.getExpressState().setDealInstitutionId(0);
    		request=new Request(srk,8384,1);
    		request.createChildAssocs(1);
    		
    	}*/

    	//clone()
    	public void testClone() throws Exception{
    		
    		srk.beginTransaction();
    		ITable testFindByPrimaryKey = dataSetTest.getTable("testClone");
    		int requestStatusId =Integer.parseInt(testFindByPrimaryKey.getValue(0,"RequestStatusId").toString());
    		int userProfileId =Integer.parseInt(testFindByPrimaryKey.getValue(0,"UserProfileId").toString());
    		int id =Integer.parseInt(testFindByPrimaryKey.getValue(0,"Id").toString());
    		int copyId =Integer.parseInt(testFindByPrimaryKey.getValue(0,"CopyId").toString());
    		int dealInstitutionId=Integer.parseInt(testFindByPrimaryKey.getValue(0,"DealInstitutionId").toString());
    		
    		request.setDealId(id);
    		request.setCopyId(copyId);
    		request.setRequestStatusId(requestStatusId);
    		request.setStatusDate(new Date());
    		request.setUserProfileId(userProfileId);
    		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
    		//DealPK dpk, int requestStatusId, Date statusDate,int userProfileId
    		Object  result=request.clone();
    		boolean status=srk.getModified();
    		assertSame(true,status);
    		srk.rollbackTransaction();
    	}
    	
         public void testCloneFailure() throws Exception{
    		
        	 boolean status=false;
        	 try{
    		ITable testCloneFailure = dataSetTest.getTable("testCloneFailure");
    		int requestStatusId =Integer.parseInt(testCloneFailure.getValue(0,"RequestStatusId").toString());
    		int userProfileId =Integer.parseInt(testCloneFailure.getValue(0,"UserProfileId").toString());
    		int id =Integer.parseInt(testCloneFailure.getValue(0,"Id").toString());
    		int copyId =Integer.parseInt(testCloneFailure.getValue(0,"CopyId").toString());
    		int dealInstitutionId=Integer.parseInt(testCloneFailure.getValue(0,"DealInstitutionId").toString());
    		request.setDealId(id);
    		request.setCopyId(copyId);
    		request.setRequestStatusId(requestStatusId);
    		request.setUserProfileId(userProfileId);
    		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
    		//DealPK dpk, int requestStatusId, Date statusDate,int userProfileId
    		status=srk.getModified();
    		
        	 }catch (Exception e) {
				// TODO: handle exception
        		 status=false;
			}
    		assertSame(false,status);
    	}
        //isRequestForAnotherScenario(int, int)

        /* public void testIsRequestForAnotherScenario() throws Exception{
     		
     		ITable testFindByPrimaryKey = dataSetTest.getTable("testIsRequestForAnotherScenario");
     		int id =Integer.parseInt(testFindByPrimaryKey.getValue(0,"Id").toString());
     		int copyId =Integer.parseInt(testFindByPrimaryKey.getValue(0,"CopyId").toString());
     		//int dealInstitutionId=Integer.parseInt(testFindByPrimaryKey.getValue(0,"DealInstitutionId").toString());
     		//srk.getExpressState().setDealInstitutionId(dealInstitutionId);
     		boolean status=request.isRequestForAnotherScenario(id,copyId);
     		assertSame(true,status);
     		
     	}*/
         
    }
