/*
 * @(#)CautionPropertyTest.java    2005-3-16
 *
 */

package com.basis100.deal.entity;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;

/**
 * CautionPropertyTest -
 * 
 * @version 1.0 2005-3-16
 * @author <A HREF="mailto:chyxiang@yahoo.com">Chen Xiang (Sean)</A>
 */
public class CautionPropertyTest extends ExpressEntityTestCase {

    // The logger
    private static Log _log = LogFactory.getLog(CautionPropertyTest.class);

    // The SessionResourceKit
    private SessionResourceKit _srk;

    // The caution property entity
    private CautionProperty _cautionPropertyDAO;

    // institutionId for deal
    private int dealInstitutionId = 100;

    /*
     * setup
     */
    @Before
    public void setUp() throws Exception {
        _log.debug("Preparing the test cases ...");
        
        // initialize resource and srk
        ResourceManager.init();
        _srk = new SessionResourceKit("Testing");
        
        _cautionPropertyDAO = new CautionProperty(_srk);

    }

    /*
     * Test Create
     */
    @Ignore("caution property id sequence? ")
    @Test
    public void testCreate() throws Exception {

        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        CautionProperty cp = _cautionPropertyDAO.create(9, "Toronto", "ABC",
                "CDE", "Yonge St", "1", 2, 3, "canada", 2);

        List cpList = _cautionPropertyDAO.findByAddressComponents("1",
                "Yonge St", "Toronto");

        for (Iterator i = cpList.iterator(); i.hasNext();) {
            CautionProperty cProp = (CautionProperty) i.next();
            _log.info("Create Caution property: ");
            _log.info("             Profile ID: " + cProp.getCPPostalFSA());
            _log.info("            Branch Name: " + cProp.getCPCity());
            _log.info("            Street Name: " + cProp.getCPStreetName());
            _log.info("         Caution Reason: " + cProp.getCautionReason());
        }
    }
    
    
    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        //free the srk
        _srk.freeResources();
    }
}
