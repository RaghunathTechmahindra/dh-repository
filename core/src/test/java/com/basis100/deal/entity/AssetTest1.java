package com.basis100.deal.entity;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.AppraisalOrderPK;
import com.basis100.deal.pk.ArchivedLoanApplicationPK;
import com.basis100.deal.pk.ArchivedLoanDecisionPK;
import com.basis100.deal.pk.AssetPK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.util.EntityTestUtil;

public class AssetTest1 extends FXDBTestCase{

	private IDataSet dataSetTest;
	private Asset asset;	
	private DealEntity entity;
	public AssetTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(Asset.class.getSimpleName() + "DataSetTest.xml"));
	}

	
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		asset = new Asset(srk,null);
		
	}
	
	public void testFindByNameSuccess() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, SQLException, CloneNotSupportedException, CreateException{
		try {
			srk.beginTransaction();
			ITable testFindByName = dataSetTest.getTable("testFindByNameSuccess");	
			String assetDescription=(String)testFindByName.getValue(0,"ASSETDESCRIPTION");			
			int copyId=Integer.valueOf((String)testFindByName.getValue(0,"COPYID"));		   		
		    asset=asset.findByName(assetDescription,copyId);
		    assertEquals(copyId,asset.getCopyId());
		    srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
	
	
	public void testFindByBorrowerSuccess() throws Exception{
		try {
			Collection<Asset> collection=null;
			srk.beginTransaction();
			ITable testFindByBorrower = dataSetTest.getTable("testFindByBorrowerSuccess");	
			int borrowerId=Integer.valueOf((String)testFindByBorrower.getValue(0,"BORROWERID"));		    	
		    int copyId=Integer.valueOf((String)testFindByBorrower.getValue(0,"COPYID"));
		    BorrowerPK pk=new BorrowerPK(borrowerId, copyId);
		   collection=asset.findByBorrower(pk);
		    assertEquals(borrowerId,pk.getId());
		    srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
	
	
	public void testFindByBorrowerFailure() throws Exception{
		try {
			Collection<Asset> collection=null;
			srk.beginTransaction();
			ITable testFindByBorrower = dataSetTest.getTable("testFindByBorrowerFailure");	
		    int borrowerId=Integer.valueOf((String)testFindByBorrower.getValue(0,"BORROWERID"));
		    int copyId=Integer.valueOf((String)testFindByBorrower.getValue(0,"COPYID"));
		    BorrowerPK pk=new BorrowerPK(borrowerId, copyId);
		   collection=asset.findByBorrower(pk);
		    assertEquals(0,collection.size());
		    srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
	
	
	public void testFindByDealSuccess() throws Exception{
		try {
			Collection collection=null;
			srk.beginTransaction();
			ITable testFindByDeal = dataSetTest.getTable("testFindByDealSuccess");	
		    int dealId=Integer.valueOf((String)testFindByDeal.getValue(0,"DEALID"));
		    int copyId=Integer.valueOf((String)testFindByDeal.getValue(0,"COPYID"));
		    DealPK pk=new DealPK(dealId, copyId);
		    collection=asset.findByDeal(pk);
		    assertEquals(dealId,pk.getId());
		    srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
	
	
	public void testFindByDealFailure() throws Exception{
		try {
			Collection collection=null;
			srk.beginTransaction();
			ITable testFindByDeal = dataSetTest.getTable("testFindByDealFailure");	
		    int dealId=Integer.valueOf((String)testFindByDeal.getValue(0,"DEALID"));
		    int copyId=Integer.valueOf((String)testFindByDeal.getValue(0,"COPYID"));
		    DealPK pk=new DealPK(dealId, copyId);
		    collection=asset.findByDeal(pk);
		    assertEquals(0,collection.size());
		    srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
	
	
	  
	
	public void testCreate() throws Exception{
		try {			
			srk.beginTransaction();
			ITable testCreate = dataSetTest.getTable("testCreate");	
		    int borrowerId=Integer.valueOf((String)testCreate.getValue(0,"ASSETID"));
		    int copyId=Integer.valueOf((String)testCreate.getValue(0,"COPYID"));
		    srk.getExpressState().setDealInstitutionId(0);
		    BorrowerPK pk=new BorrowerPK(borrowerId, copyId);
		    asset=asset.create(pk);		   
		    assertEquals(borrowerId,pk.getId());
		    srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
	
	public void testFindByMyCopiesSuccess() throws Exception{  
		Vector assetCopies=null;
		ITable testFindByMyCopiesSuccess = dataSetTest.getTable("testFindByMyCopiesSuccess");		
	    int assetId=Integer.valueOf((String)testFindByMyCopiesSuccess.getValue(0,"ASSETID"));		
		int copyId=Integer.valueOf((String)testFindByMyCopiesSuccess.getValue(0,"COPYID"));
		srk.getExpressState().setDealInstitutionId(0);
		asset.setAssetId(assetId);
		asset.copyId=copyId;
		assetCopies=asset.findByMyCopies(); 
		
	    assertEquals(assetId,asset.getAssetId());
	} 
	
	public void testFindByMyCopiesFailure() throws Exception{  
		Vector assetCopies=null;
		ITable testFindByMyCopiesFailure = dataSetTest.getTable("testFindByMyCopiesFailure");		
	    int assetId=Integer.valueOf((String)testFindByMyCopiesFailure.getValue(0,"ASSETID"));		
		int copyId=Integer.valueOf((String)testFindByMyCopiesFailure.getValue(0,"COPYID"));
		srk.getExpressState().setDealInstitutionId(0);
		asset.setAssetId(assetId);
		asset.copyId=copyId;
		assetCopies=asset.findByMyCopies();	
	   assertEquals(0, assetCopies.size());
		
	}
	
	 public void testFindByPrimaryKey() throws Exception{		
			ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByPrimaryKey");
			int assetId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"assetId"));	
			int copyId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"copyId"));
			srk.getExpressState().setDealInstitutionId(1);		
			AssetPK  pk = new AssetPK(assetId,copyId);
			asset=asset.findByPrimaryKey(pk);		
			assertEquals(assetId, pk.getId());		
		}   
	  public void testFindByPrimaryKeyFailure() throws Exception{		
			ITable findByDupeCheckCriteria = dataSetTest.getTable("testFindByPrimaryKeyFailure");
			boolean status=false;
			try{
			int assetId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"assetId"));	
			int copyId = Integer.parseInt((String)findByDupeCheckCriteria.getValue(0,"copyId"));
			srk.getExpressState().setDealInstitutionId(1);		
			AssetPK  pk = new AssetPK(assetId,copyId);
			asset=asset.findByPrimaryKey(pk);	
			status=true;
			}catch(Exception e){
				status=false;
			}
			assertEquals(false,status);		
		}  
}
