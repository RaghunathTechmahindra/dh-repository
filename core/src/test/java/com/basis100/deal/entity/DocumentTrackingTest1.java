package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.DealPK;

public class DocumentTrackingTest1 extends  FXDBTestCase{

	private IDataSet dataSetTest;
	DocumentTracking documentTracking=null;
	DealPK dealPK=null;
	
	public DocumentTrackingTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("DocumentTrackingDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource("DocumentTrackingDataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		documentTracking=new DocumentTracking(srk);
	}
	//findByLabel(DealPK, String)

	public void testFindByLabel() throws Exception{
		ITable testFindByRequest = dataSetTest.getTable("testFindByLabel");
		String  label = testFindByRequest.getValue(0,"label").toString();
		int size=documentTracking.findByLabel(null,label).size();
		assertNotSame(0,size);
	}
	public void testFindByLabelFailure() throws Exception{
		int size=documentTracking.findByLabel(null,null).size();
		assertSame(0,size);
	}
	//findByDeal(DealPK)

	public void testFindByDeal() throws Exception{
		ITable testFindByRequest = dataSetTest.getTable("testFindByDeal");
		int  DealId = Integer.parseInt(testFindByRequest.getValue(0,"DealId").toString());
		int  CopyId = Integer.parseInt(testFindByRequest.getValue(0,"CopyId").toString());
		dealPK=new DealPK(DealId,CopyId);
		int size=documentTracking.findByDeal(dealPK).size();
		assertNotSame(0,size);
	}
	public void testFindByDealFailure() throws Exception{
		ITable testFindByRequest = dataSetTest.getTable("testFindByDealFailure");
		int  DealId = Integer.parseInt(testFindByRequest.getValue(0,"DealId").toString());
		int  CopyId = Integer.parseInt(testFindByRequest.getValue(0,"CopyId").toString());
		dealPK=new DealPK(DealId,CopyId);
		int size=documentTracking.findByDeal(dealPK).size();
		assertSame(0,size);
	}
	//findByDealAndSource(DealPK, int)
	public void testFindByDealAndSource() throws Exception{
		ITable testFindByRequest = dataSetTest.getTable("testFindByDealAndSource");
		int  DealId = Integer.parseInt(testFindByRequest.getValue(0,"DealId").toString());
		int  CopyId = Integer.parseInt(testFindByRequest.getValue(0,"CopyId").toString());
		int  source = Integer.parseInt(testFindByRequest.getValue(0,"source").toString());
		dealPK=new DealPK(DealId,CopyId);
		int size=documentTracking.findByDealAndSource(dealPK,source).size();
		assertNotSame(0,size);
	}
	public void testFindByDealAndSourceFailure() throws Exception{
		ITable testFindByRequest = dataSetTest.getTable("testFindByDealAndSourceFailure");
		int  DealId = Integer.parseInt(testFindByRequest.getValue(0,"DealId").toString());
		int  CopyId = Integer.parseInt(testFindByRequest.getValue(0,"CopyId").toString());
		int  source = Integer.parseInt(testFindByRequest.getValue(0,"source").toString());
		dealPK=new DealPK(DealId,CopyId);
		int size=documentTracking.findByDealAndSource(dealPK,source).size();
		assertSame(0,size);
	}
   //findForCommitmentLetter(DealPK)

	/*public void testFindForCommitmentLetter() throws Exception{
		ITable testFindByRequest = dataSetTest.getTable("testFindForCommitmentLetter");
		int  DealId = Integer.parseInt(testFindByRequest.getValue(0,"DealId").toString());
		int  CopyId = Integer.parseInt(testFindByRequest.getValue(0,"CopyId").toString());
		dealPK=new DealPK(DealId,CopyId);
		int size=documentTracking.findForCommitmentLetter(dealPK).size();
		assertNotSame(0,size);
	}*/
	public void testFindForCommitmentLetterFailure() throws Exception{
		ITable testFindByRequest = dataSetTest.getTable("testFindForCommitmentLetterFailure");
		int  DealId = Integer.parseInt(testFindByRequest.getValue(0,"DealId").toString());
		int  CopyId = Integer.parseInt(testFindByRequest.getValue(0,"CopyId").toString());
		dealPK=new DealPK(DealId,CopyId);
		int size=documentTracking.findForCommitmentLetter(dealPK).size();
		assertSame(0,size);
	}
	//findBySqlColl(String)
	public void testFindBySqlColl() throws Exception{
		ITable testFindByRequest = dataSetTest.getTable("testFindBySqlColl");
		String  sqlQuery = testFindByRequest.getValue(0,"sql").toString();
		int size=documentTracking.findBySqlColl(sqlQuery).size();
		assertNotSame(0,size);
	}
	
	public void testFindBySqlCollFailure() throws Exception{
		ITable testFindByRequest = dataSetTest.getTable("testFindBySqlCollFailure");
		String  sqlQuery = testFindByRequest.getValue(0,"sql").toString();
		int size=documentTracking.findBySqlColl(sqlQuery).size();
		assertSame(0,size);
	}
	//loadSectionCode()
	public void testLoadSectionCode() throws Exception{
		String size=documentTracking.loadSectionCode();
		assertNotNull(size);
	}

	public void testLoadSectionCodeFailure() throws Exception{
		ITable testFindByRequest = dataSetTest.getTable("testLoadSectionCodeFailure");
		int  conditionId = Integer.parseInt(testFindByRequest.getValue(0,"conditionId").toString());
		documentTracking.setConditionId(conditionId);
		String size=documentTracking.loadSectionCode();
		assertNull(size);
	}
	//findForConditionUpdate(DealPK)

	public void testFindForConditionUpdate() throws Exception{
		ITable testFindByRequest = dataSetTest.getTable("testFindForConditionUpdate");
		int  DealId = Integer.parseInt(testFindByRequest.getValue(0,"DealId").toString());
		int  CopyId = Integer.parseInt(testFindByRequest.getValue(0,"CopyId").toString());
		dealPK=new DealPK(DealId,CopyId);
		int size=documentTracking.findForConditionUpdate(dealPK).size();
		assertNotSame(0,size);
	}
	public void testFindForConditionUpdateFailure() throws Exception{
		ITable testFindByRequest = dataSetTest.getTable("testFindForConditionUpdateFailure");
		int  DealId = Integer.parseInt(testFindByRequest.getValue(0,"DealId").toString());
		int  CopyId = Integer.parseInt(testFindByRequest.getValue(0,"CopyId").toString());
		dealPK=new DealPK(DealId,CopyId);
		int size=documentTracking.findForConditionUpdate(dealPK).size();
		assertSame(0,size);
	}
	//findForConditionUpdate(DealPK, String)

	public void testFindForConditionUpdateByConditionTypes() throws Exception{
		ITable testFindForConditionUpdateByConditionTypes = dataSetTest.getTable("testFindForConditionUpdateByConditionTypes");
		int  DealId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"DealId").toString());
		int  CopyId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"CopyId").toString());
		String conditionTypes=testFindForConditionUpdateByConditionTypes.getValue(0,"ConditionTypes").toString();
		dealPK=new DealPK(DealId,CopyId);
		int size=documentTracking.findForConditionUpdate(dealPK,conditionTypes).size();
		assertNotSame(0,size);
	}
	public void testFindForConditionUpdateByConditionTypesFailure() throws Exception{
		ITable testFindForConditionUpdateByConditionTypesFailure = dataSetTest.getTable("testFindForConditionUpdateByConditionTypesFailure");
		int  DealId = Integer.parseInt(testFindForConditionUpdateByConditionTypesFailure.getValue(0,"DealId").toString());
		int  CopyId = Integer.parseInt(testFindForConditionUpdateByConditionTypesFailure.getValue(0,"CopyId").toString());
		String conditionTypes=testFindForConditionUpdateByConditionTypesFailure.getValue(0,"ConditionTypes").toString();
		dealPK=new DealPK(DealId,CopyId);
		int size=documentTracking.findForConditionUpdate(dealPK,conditionTypes).size();
		assertSame(0,size);
	}
	
	//needStatusUpdate(DealPK, String, boolean)
	public void testNeedStatusUpdate() throws Exception{
		ITable testFindForConditionUpdateByConditionTypes = dataSetTest.getTable("testNeedStatusUpdate");
		int  DealId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"DealId").toString());
		int  CopyId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"CopyId").toString());
		String conditionTypes=testFindForConditionUpdateByConditionTypes.getValue(0,"ConditionTypes").toString();
		dealPK=new DealPK(DealId,CopyId);
		boolean status=documentTracking.needStatusUpdate(dealPK,conditionTypes,true);
		assertSame(true,status);
	}
	public void testNeedStatusUpdateFailure() throws Exception{
		ITable testFindForConditionUpdateByConditionTypes = dataSetTest.getTable("testNeedStatusUpdateFailure");
		int  DealId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"DealId").toString());
		int  CopyId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"CopyId").toString());
		String conditionTypes=testFindForConditionUpdateByConditionTypes.getValue(0,"ConditionTypes").toString();
		dealPK=new DealPK(DealId,CopyId);
		boolean status=documentTracking.needStatusUpdate(dealPK,conditionTypes,true);
		assertSame(false,status);
	}
	//createWithNoVerbiage(Condition, int, int, int, int)

	public void testCreateWithNoVerbiage() throws Exception{
		ITable testFindForConditionUpdateByConditionTypes = dataSetTest.getTable("testCreateWithNoVerbiage");
		int  DealId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"DealId").toString());
		int  CopyId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"CopyId").toString());
		int docStatusId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"DocStatusId").toString());
		int dtsourceId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"DtsourceId").toString());
		int ConditionId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"ConditionId").toString());
		int ConditionResponsibilityRoleId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"ConditionResponsibilityRoleId").toString());
		Condition condition=new Condition(srk);
		condition.setConditionId(ConditionId);
		condition.setConditionResponsibilityRoleId(ConditionResponsibilityRoleId);
		srk.getExpressState().setDealInstitutionId(0);
		//Condition condition,int dealId, int copyId, int docStatusId, int dtsourceId
		DocumentTracking status=documentTracking.createWithNoVerbiage(condition,DealId,CopyId,docStatusId,dtsourceId);
		assertNotNull(status);
	}
	public void testCreateWithNoVerbiageFailure() throws Exception{
		boolean status=false;
		try{
		ITable testFindForConditionUpdateByConditionTypes = dataSetTest.getTable("testCreateWithNoVerbiageFailure");
		int  DealId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"DealId").toString());
		int  CopyId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"CopyId").toString());
		int docStatusId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"DocStatusId").toString());
		int dtsourceId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"DtsourceId").toString());
		int ConditionId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"ConditionId").toString());
		int ConditionResponsibilityRoleId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"ConditionResponsibilityRoleId").toString());
		Condition condition=new Condition(srk);
		condition.setConditionId(ConditionId);
		condition.setConditionResponsibilityRoleId(ConditionResponsibilityRoleId);
		srk.getExpressState().setDealInstitutionId(0);
		documentTracking.createWithNoVerbiage(condition,DealId,CopyId,docStatusId,dtsourceId);
		 status=true;
		}catch (Exception e) {
			// TODO: handle exception
			 status=false;
		}
		assertSame(false, status);
	}
	//create(Condition condition, int dealId, int copyId,  int docStatusId, int dtsourceId)
	
	public void testCreate() throws Exception{
		ITable testFindForConditionUpdateByConditionTypes = dataSetTest.getTable("testCreate");
		int  DealId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"DealId").toString());
		int  CopyId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"CopyId").toString());
		int docStatusId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"DocStatusId").toString());
		int dtsourceId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"DtsourceId").toString());
		int ConditionId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"ConditionId").toString());
		int ConditionResponsibilityRoleId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"ConditionResponsibilityRoleId").toString());
		Condition condition=new Condition(srk);
		condition.setConditionId(ConditionId);
		condition.setConditionResponsibilityRoleId(ConditionResponsibilityRoleId);
		srk.getExpressState().setDealInstitutionId(0);
		//Condition condition,int dealId, int copyId, int docStatusId, int dtsourceId
		DocumentTracking status=documentTracking.create(condition,DealId,CopyId,docStatusId,dtsourceId);
		assertNotNull(status);
	}
	
	public void testCreateFailure() throws Exception{
		boolean status=false;
		try{
		ITable testFindForConditionUpdateByConditionTypes = dataSetTest.getTable("testCreateFailure");
		int  DealId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"DealId").toString());
		int  CopyId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"CopyId").toString());
		int docStatusId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"DocStatusId").toString());
		int dtsourceId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"DtsourceId").toString());
		int ConditionId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"ConditionId").toString());
		int ConditionResponsibilityRoleId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"ConditionResponsibilityRoleId").toString());
		Condition condition=new Condition(srk);
		condition.setConditionId(ConditionId);
		condition.setConditionResponsibilityRoleId(ConditionResponsibilityRoleId);
		srk.getExpressState().setDealInstitutionId(0);
		documentTracking.createWithNoVerbiage(condition,DealId,CopyId,docStatusId,dtsourceId);
		 status=true;
		}catch (Exception e) {
			// TODO: handle exception
			 status=false;
		}
		assertSame(false, status);
	}
	
	//createMissingDTVerbiage(Condition)

	/*public void testCreateMissingDTVerbiage() throws Exception{
		ITable testFindForConditionUpdateByConditionTypes = dataSetTest.getTable("testCreateMissingDTVerbiage");
		int  DealId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"DealId").toString());
		int  CopyId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"CopyId").toString());
		int docStatusId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"DocStatusId").toString());
		int dtsourceId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"DtsourceId").toString());
		int ConditionId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"ConditionId").toString());
		int ConditionResponsibilityRoleId = Integer.parseInt(testFindForConditionUpdateByConditionTypes.getValue(0,"ConditionResponsibilityRoleId").toString());
		Condition condition=new Condition(srk);
		condition.setConditionId(ConditionId);
		condition.setConditionResponsibilityRoleId(ConditionResponsibilityRoleId);
		srk.getExpressState().setDealInstitutionId(0);
		//Condition condition,int dealId, int copyId, int docStatusId, int dtsourceId
		documentTracking.createMissingDTVerbiage(condition);
		assertNotNull(null);
	}*/
}
