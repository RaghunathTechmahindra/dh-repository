package com.basis100.deal.entity;

import java.io.IOException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import MosSystem.Mc;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.util.EntityTestUtil;

public class ComponentMortgageDBTest extends FXDBTestCase {
	private IDataSet dataSetTest;
    private ComponentMortgage componentMortgage = null;
    Object obj =null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public ComponentMortgageDBTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ComponentMortgage.class.getSimpleName()+"DataSetTest.xml"));
	}
	

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.componentMortgage = new ComponentMortgage(srk);
	}
	public void testCreate() throws Exception
	{
		ITable testCreate = dataSetTest.getTable("testCreate");
		String iId = (String)testCreate.getValue(0, "INSTITUTIONPROFILEID");
		int dealInstitutionId = Integer.parseInt(iId);
		String cid = (String)testCreate.getValue(0, "COMPONENTID");
		int componentid = Integer.parseInt(cid);
		String cpid = (String)testCreate.getValue(0, "COPYID");
		int copyid = Integer.parseInt(cpid);
		/*int paymentFrequencyId = Integer.parseInt((String)testCreate.getValue(0, "PAYMENTFREQUENCYID"));
		int prepaymentOptionsId = Integer.parseInt((String)testCreate.getValue(0, "PREPAYMENTOPTIONSID"));
		int privilegesPaymentId = Integer.parseInt((String)testCreate.getValue(0, "PRIVILEGEPAYMENTID"));
		char cashbackAmountOverride = testCreate.getValue(0, "CASHBACKAMOUNTOVERRIDE").toString().charAt(0);
		char ratelock = testCreate.getValue(0, "RATELOCK").toString().charAt(0);*/
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		srk.beginTransaction();
		
		ComponentMortgage aComponentMortgage = componentMortgage.create(componentid, copyid);
		//find a ComponentLoan to verify
		ComponentMortgage new_ComponentMortgage = new ComponentMortgage(srk, componentid, copyid);
		assertEquals(aComponentMortgage, new_ComponentMortgage);
		
		srk.rollbackTransaction();
	}
	/*
	 * Failure case not applicable as the create method returns an entity and sending unqualified data will result
	 * in improper end of test class and thus it is avoid.
	 */
	public void testfindByComponent() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByComponent = dataSetTest.getTable("testfindByComponent");
		int dealId = Integer.parseInt((String)testfindByComponent.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String)testfindByComponent.getValue(0, "COPYID"));
		int institutionId = Integer.parseInt((String)testfindByComponent.getValue(0, "INSTITUTIONPROFILEID"));
        Deal deal = new Deal(srk, null, dealId, copyId);
        srk.getExpressState().setDealInstitutionId(institutionId);
        int mtgProdId = deal.getMtgProdId();
        Component comp = new Component(srk, null);
        comp = comp.create(dealId, copyId, Mc.COMPONENT_TYPE_LOC,
                mtgProdId);

        ComponentMortgage componentMortgage = new ComponentMortgage(srk, null);
        componentMortgage.create(comp.getComponentId(), comp.getCopyId());

        // create the entity
        ComponentMortgage component = new ComponentMortgage(srk);

        // Find by primary Key
        component = component.findByPrimaryKey(new ComponentMortgagePK(comp
                .getComponentId(), copyId));

        // Check if the data retrieved matches the DB
        assertEquals(component.getComponentId(), comp.getComponentId());
        assertEquals(component.getCopyId(), copyId);

        // roll-back transaction.
        srk.rollbackTransaction();
	}
	@SuppressWarnings("finally")
	public boolean testfindByComponentFailure() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByComponentFailure = dataSetTest.getTable("testfindByComponentFailure");
		int institutionId = Integer.parseInt((String)testfindByComponentFailure.getValue(0, "INSTITUTIONPROFILEID"));
		int dealId = Integer.parseInt((String)testfindByComponentFailure.getValue(0, "DEALID"));
		int copyId = Integer.parseInt((String)testfindByComponentFailure.getValue(0, "COPYID"));
		
        Deal deal = new Deal(srk, null, dealId, copyId);
        srk.getExpressState().setDealInstitutionId(institutionId);
        
        int mtgProdId = deal.getMtgProdId();
        Component comp = new Component(srk, null);
        comp = comp.create(dealId, copyId, Mc.COMPONENT_TYPE_LOC,
                mtgProdId);

        ComponentMortgage componentMortgage = new ComponentMortgage(srk, null);
        componentMortgage.create(comp.getComponentId(), comp.getCopyId());

        // create the entity
        ComponentMortgage component = new ComponentMortgage(srk);
        try{
        // Find by primary Key
        component = component.findByPrimaryKey(new ComponentMortgagePK(comp
                .getComponentId(), copyId));
        }catch (Exception e) {
        	// Check if the data retrieved matches the DB
            assertNull(component);
            return true; 
		}
        finally{
        	assertNull(component);
        // roll-back transaction.
        srk.rollbackTransaction();
        return true;
        }
	}
}
