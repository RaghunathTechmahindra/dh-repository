package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.BusinessDayPK;
import com.basis100.entity.CreateException;

public class DBBusinessDayTest  extends FXDBTestCase{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(DBBorrowerAddressTest.class);
    
    //Initial data to be loaded before each test case.
	private final static String INIT_XML_DATA = "DBBusinessDayTestDataSet.xml";
	private IDataSet dataSetTest;
	
	/**
     * Constructor function
     */
    public DBBusinessDayTest(String name)  throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource(DBBusinessDayTest.class.getSimpleName() + "DataSetTest.xml"));
    }
    
    @Override
	protected IDataSet getDataSet() throws Exception {
    	return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}
    
    @Override
	protected void setUp() throws Exception {
		super.setUp();
	}
    
    /**
     * test FindByName.
     */
    @Test
    public void testFindByName() throws Exception {

    	_logger.info("Start testFindByName");
    	
    	ITable testFindByName = dataSetTest.getTable("testFindByName");
 		int  instProfId = Integer.parseInt((String)testFindByName.getValue(0,"instProfId"));
 		String  weekStart = (String)testFindByName.getValue(0,"weekStart");
 		String  weekEnd = (String)testFindByName.getValue(0,"weekEnd");
    	
    	// set vpd context
        srk.getExpressState().setDealInstitutionId(instProfId);

        // find a business day by primary key
        BusinessDay businessDay = new BusinessDay(srk);
        
        BusinessDay bd = businessDay.findByName();
        
        // verify the result
        assert bd != null;
        assert bd.getWeekStart().equalsIgnoreCase(weekStart);
        assert bd.getWeekEnd().equalsIgnoreCase(weekEnd);
        
        _logger.info("End testFindByName");
    }
    

}
