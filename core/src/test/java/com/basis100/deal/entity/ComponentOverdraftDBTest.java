package com.basis100.deal.entity;

import java.io.IOException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import MosSystem.Mc;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentMortgagePK;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.util.EntityTestUtil;

public class ComponentOverdraftDBTest extends FXDBTestCase {
	private IDataSet dataSetTest;
    private ComponentOverdraft componentOverdraft = null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public ComponentOverdraftDBTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ComponentOverdraft.class.getSimpleName()+"DataSetTest.xml"));
	}
	

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.componentOverdraft = new ComponentOverdraft(srk);
	}
	public void testCreate() throws Exception
	{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int dealInstitutionId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));
		int componentid = Integer.parseInt((String)testCreate.getValue(0, "COMPONENTID"));
		int copyid = Integer.parseInt((String)testCreate.getValue(0, "COPYID"));
		
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);

		ComponentOverdraft aComponentOverdraft = componentOverdraft.create(componentid, copyid);
		//find a ComponentLoan to verify
		ComponentOverdraft new_ComponentOverdraft = new ComponentOverdraft(srk, componentid, copyid);
		assertEquals(aComponentOverdraft, new_ComponentOverdraft);
		
		srk.rollbackTransaction();
	}
	/*
	 * Failure case not applicable as the create method returns an entity and sending unqualified data will result
	 * in improper end of test class and thus it is avoid.
	 */
}
