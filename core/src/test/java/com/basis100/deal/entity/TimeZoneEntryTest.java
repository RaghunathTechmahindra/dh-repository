package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.TimeZoneEntryPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;


public class TimeZoneEntryTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private TimeZoneEntry timeZoneEntry;
	// The Session Resource Kit
    private SessionResourceKit _srk;
    private Deal deal;
    
	public TimeZoneEntryTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(TimeZoneEntry.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		// init resource
        ResourceManager.init();
        _srk = new SessionResourceKit("Testing");
		this.timeZoneEntry = new TimeZoneEntry(srk);
	}

    public void testCreate() throws Exception {
    	srk.beginTransaction();
    	ITable expectedTimeZoneEntry = dataSetTest.getTable("testCreate");
		int timeZoneEntryId = (Integer) DataType.INTEGER.typeCast(expectedTimeZoneEntry.getValue(0, "TIMEZONEENTRYID"));
		String tzeDescription = (String)(expectedTimeZoneEntry.getValue(0, "TZEDESCRIPTION"));
		String tzeSysId = (String)(expectedTimeZoneEntry.getValue(0, "TZESYSID"));
		String tzeCustomId = (String)(expectedTimeZoneEntry.getValue(0, "TZECUSTOMID"));

		//TimeZoneEntry timeZoneEntry = new TimeZoneEntry(_srk);
        timeZoneEntry.create(timeZoneEntryId, tzeDescription, tzeSysId, tzeCustomId);
	    assertEquals(timeZoneEntryId, timeZoneEntry.getTimeZoneEntryId());
	    srk.rollbackTransaction();
    }
    
    public void testRemove() throws Exception {
    	srk.beginTransaction();
    	ITable expectedTimeZoneEntry = dataSetTest.getTable("testRemove");
    	int timeZoneEntryId = (Integer) DataType.INTEGER.typeCast(expectedTimeZoneEntry.getValue(0, "TIMEZONEENTRYID"));
		String tzeDescription = (String)(expectedTimeZoneEntry.getValue(0, "TZEDESCRIPTION"));

		/*TimeZoneEntry timeZoneEntry = new TimeZoneEntry(_srk);*/
        timeZoneEntry.remove();
        assertNotNull(timeZoneEntryId);
        srk.rollbackTransaction();
    }
    
    public void testFindByPrimaryKey() throws Exception {
    	srk.beginTransaction();
    	ITable expectedTimeZoneEntry = dataSetTest.getTable("testFindByPrimaryKey");
    	int timeZoneEntryId = (Integer) DataType.INTEGER.typeCast(expectedTimeZoneEntry.getValue(0, "TIMEZONEENTRYID"));
		
      // TimeZoneEntry timeZoneEntry = new TimeZoneEntry(_srk);
       timeZoneEntry.findByPrimaryKey(new TimeZoneEntryPK(timeZoneEntryId));
       assertEquals(timeZoneEntryId, timeZoneEntry.getTimeZoneEntryId());
       srk.rollbackTransaction();
    }
    
    // The Sql query involving this method has error
  /*public void testFindByDeal() throws Exception {
    	srk.beginTransaction();
    	ITable expectedTimeZoneEntry = dataSetTest.getTable("testFindByDeal");
    	int id = (Integer) DataType.INTEGER.typeCast(expectedTimeZoneEntry.getValue(0, "ID"));
		int copyId = (Integer) DataType.INTEGER.typeCast(expectedTimeZoneEntry.getValue(0, "COPYID"));


		Deal deal = new Deal(srk, null, id, copyId);
        timeZoneEntry.findByDeal(new DealPK(deal.getDealId(), deal.getCopyId()));
        assertEquals(id, deal.getDealId());
        
        srk.rollbackTransaction();
    }
*/
    public void testFindByAll() throws Exception {
    	srk.beginTransaction();
    	ITable expectedTimeZoneEntry = dataSetTest.getTable("testFindByAll");
    	int timeZoneEntryId = (Integer) DataType.INTEGER.typeCast(expectedTimeZoneEntry.getValue(0, "TIMEZONEENTRYID"));
		String tzeDescription = (String)(expectedTimeZoneEntry.getValue(0, "TZEDESCRIPTION"));
		String tzeSysId = (String)(expectedTimeZoneEntry.getValue(0, "TZESYSID"));
		String tzeCustomId = (String)(expectedTimeZoneEntry.getValue(0, "TZECUSTOMID"));
		
       //TimeZoneEntry timeZoneEntry = new TimeZoneEntry(_srk);
        timeZoneEntry.findByAll();
		assertNotNull(timeZoneEntryId);
		srk.rollbackTransaction();
    }
    
    
    
}