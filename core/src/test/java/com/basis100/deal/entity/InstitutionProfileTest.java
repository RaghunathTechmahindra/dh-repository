/*
 * @(#)InstitutionProfileTest.java    2007-8-29
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.basis100.deal.entity;

import java.util.HashMap;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * InstitutionProfileTest - 
 *
 * @version   1.0 2007-8-29
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class InstitutionProfileTest extends ExpressEntityTestCase
    implements UnitTestLogging {

    // The logger
    private final static Logger _logger = 
        LoggerFactory.getLogger(InstitutionProfileTest.class);

    // the session resource kit.
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public InstitutionProfileTest() {
    }

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
    }

    /**
     * test the find by primary key
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        _logger.info(BORDER_START, "findByPrimaryKey");

        InstitutionProfile instProfile = new InstitutionProfile(_srk);

        _logger.info(SHORT_BREAK);
        doSearch(instProfile,
                 _dataRepository.getInt("InstitutionProfile",
                                        "InstitutionProfileId", 0),
                 _dataRepository.getString("InstitutionProfile",
                                           "InstitutionAbbr", 0)
                 );

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    @Test
    public void testFindByPrimaryKeyAgain() throws Exception {

        _logger.info(BORDER_START, "findByPrimaryKeyAgain");

        InstitutionProfile instProfile = new InstitutionProfile(_srk);

        _logger.info(SHORT_BREAK);
        doSearch(instProfile,
                 _dataRepository.getInt("InstitutionProfile",
                                        "InstitutionProfileId", 0),
                 _dataRepository.getString("InstitutionProfile",
                                           "InstitutionAbbr", 0)
                 );

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * search all possible record institution profile table.
     */
    protected void doSearch(InstitutionProfile instProfile,
                            int id, String abbr) throws Exception {

        _logger.info("Trying to search institution: [{}]", id);
        _logger.info("Expecting the abbreviation: [{}]", abbr);
        instProfile = instProfile.
            findByPrimaryKey(new InstitutionProfilePK(id));

        _logger.info("Found Institution Profile[{}, {}]",
                     instProfile.getInstitutionProfileId(),
                     instProfile.getInstitutionAbbr());

        assertTrue(id == instProfile.getInstitutionProfileId());
        assertTrue(abbr.equals(instProfile.getInstitutionAbbr()));
    }
    
    /**
     * 
     * @throws Exception
     */
    @Test
    public void testGetInstitutionNames()throws Exception
    {
        _logger.info(BORDER_START, "testGetInstitutionNames");

        InstitutionProfile instProfile = new InstitutionProfile(_srk);

        _logger.info(SHORT_BREAK);
        
        HashMap<Integer, String> insts = instProfile.getInstitutionNames();
        
        _logger.info("Found Institutions : " + insts);
        
        _logger.info(SHORT_BREAK);
        
        assertTrue(insts.size() > 0);
        
        _logger.info(BORDER_END, "testGetInstitutionNames");
    }
    
    /**
     * 
     * @throws Exception
     */
    @Test
    public void testPmiid()throws Exception
    {
        _logger.info(BORDER_START, "testPmiid");

        InstitutionProfile instProfile = new InstitutionProfile(_srk);
        
        instProfile = instProfile.
        	findByPrimaryKey(new InstitutionProfilePK(
        			_dataRepository.getInt("InstitutionProfile",
                            "InstitutionProfileId", 0)
        			));

        _logger.info(SHORT_BREAK);
        
        String pmiid = instProfile.getPMIId();
        
        assertTrue(pmiid !=  null);
        
        _logger.info(BORDER_END, "testPmiid");
    }
}