package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.GroupProfilePK;

public class DBGroupProfileTest  extends FXDBTestCase{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(DBGroupProfileTest.class);
    
    //Initial data to be loaded before each test case.
	private final static String INIT_XML_DATA = "DBGroupProfileDataSet.xml";
	private IDataSet dataSetTest;
	
	/**
     * Constructor function
     */
    public DBGroupProfileTest(String name)  throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource("DBGroupProfileDataSetTest.xml"));
    }
      
    
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
        return DatabaseOperation.INSERT;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.DELETE;
    }
    
    /**
     * test create primary key.
     */
    @Test
    public void testCreatePrimaryKey() throws Exception {

    	_logger.info(" Start testCreatePrimaryKey");
    	 
    	 ITable testCreatePrimaryKey = dataSetTest.getTable("testCreatePrimaryKey");
  		int  instProfId = Integer.parseInt((String)testCreatePrimaryKey.getValue(0,"instProfId"));
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
        
        GroupProfile grpProfile = new GroupProfile(srk);
        GroupProfilePK grpProfilePk = grpProfile.createPrimaryKey();
        
        // verify the result.
        assert grpProfilePk != null;
        assert grpProfilePk.getId() != 0;
        assertEquals(grpProfilePk.getName(), "groupProfileId");
        
        _logger.info(" End testCreatePrimaryKey");
    	
    }
    
    /**
     * test create & its overridden methods
     */
    @Test
    public void testCreate() throws Exception {

    	_logger.info(" Start testCreate");
    	 
    	 ITable testCreate = dataSetTest.getTable("testCreate");
  		int  instProfId = Integer.parseInt((String)testCreate.getValue(0,"instProfId"));
  		int  branchId = Integer.parseInt((String)testCreate.getValue(0,"branchId"));
  		String  gpname = (String)testCreate.getValue(0,"gpname");
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
        
        // test create
        srk.beginTransaction();
        GroupProfile grpProfile = new GroupProfile(srk);
        grpProfile = grpProfile.create();
        
        GroupProfilePK pk = new GroupProfilePK(grpProfile.getGroupProfileId());
        GroupProfile foundGrpProfile = grpProfile.findByPrimaryKey(pk);
        
        // verify the result.
        assert grpProfile != null;
        assertEquals(grpProfile.getGroupProfileId(), foundGrpProfile.getGroupProfileId());
        srk.rollbackTransaction();
        
        
        // test create with parameters
        srk.beginTransaction();
        GroupProfile grpProfile1 = new GroupProfile(srk);
        grpProfile1 = grpProfile1.create(branchId, gpname, instProfId);
        
        GroupProfilePK pk1 = new GroupProfilePK(grpProfile1.getGroupProfileId());
        GroupProfile foundGrpProfile1 = grpProfile1.findByPrimaryKey(pk1);
        
        // verify the result.
        assert grpProfile1 != null;
        assertEquals(grpProfile1.getGroupProfileId(), foundGrpProfile1.getGroupProfileId());
        srk.rollbackTransaction();
        
        _logger.info(" End testCreate");
    	
    }
    
    /**
     * test isDupGroupName
     */
    @Test
    public void testIsDupGroupName() throws Exception {

    	_logger.info(" Start testIsDupGroupName");
    	 
    	 ITable testIsDupGroupName = dataSetTest.getTable("testIsDupGroupName");
  		int  instProfId = Integer.parseInt((String)testIsDupGroupName.getValue(0,"instProfId"));
  		int  branchId = Integer.parseInt((String)testIsDupGroupName.getValue(0,"branchId"));
  		String  gpName = (String)testIsDupGroupName.getValue(0,"gpName");
  		String  gpName1 = (String)testIsDupGroupName.getValue(0,"gpName1");
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
        
        GroupProfile grpProfile = new GroupProfile(srk);
        boolean dup = grpProfile.isDupGroupName(gpName, branchId);
        
        // verify the result.
        assert dup;
        
        // failure case
        dup = grpProfile.isDupGroupName(gpName1, branchId);
        assert !dup;
        
        
        _logger.info(" End testIsDupGroupName");
    	
    }
    
}
