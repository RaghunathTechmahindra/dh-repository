package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.ESBOutboundQueuePK;


/**
 * <p>ESBOutboundQueueDbTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class ESBOutboundQueueDbTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private ESBOutboundQueue eSBOutboundQueue;

	public ESBOutboundQueueDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ESBOutboundQueue.class.getSimpleName() + "DataSetTest.xml"));
	}

	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.eSBOutboundQueue = new ESBOutboundQueue(srk);
	}
	
	public void testCreatePrimaryKey() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreatePrimaryKey");
    	int esbOutboundQueueId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "ESBOUTBOUNDQUEUEID"));
    	
    	eSBOutboundQueue.createPrimaryKey();
    	assertEquals(esbOutboundQueueId, eSBOutboundQueue.getESBOutboundQueueId());
    }
	
	public void testCreate() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreate");
    	int esbOutboundQueueId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "ESBOUTBOUNDQUEUEID"));
    	int serviceTypeId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "SERVICETYPEID"));
    	int dealId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DEALID"));
    	int systemTypeId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "SYSTEMTYPEID"));
    	int userProfileId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "USERPROFILEID"));
    	int institutionProfileId =(Integer) DataType.INTEGER.typeCast(expected.getValue(0, "INSTITUTIONPROFILEID"));  
		//int lowestId = Integer.parseInt((String)testfindLowestId.getValue(0, "LOWESTID"));
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
    	srk.beginTransaction();
    	eSBOutboundQueue = eSBOutboundQueue.create(new ESBOutboundQueuePK(esbOutboundQueueId), serviceTypeId, dealId, systemTypeId, userProfileId);
    	ESBOutboundQueue eSBOutboundQueue1 = new ESBOutboundQueue(srk,eSBOutboundQueue.getESBOutboundQueueId());
    	assertNotNull(eSBOutboundQueue1);
    	srk.rollbackTransaction();
    }
	
	public void testExistUnprocessed() throws Exception {
    	ITable expected = dataSetTest.getTable("testExistUnprocessed");
    	int esbOutboundQueueId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "ESBOUTBOUNDQUEUEID"));
    	int dealId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "DEALID"));
    	int serviceTypeId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "SERVICETYPEID"));
    	
    	eSBOutboundQueue.existUnprocessed(dealId, serviceTypeId);
    	assertEquals(esbOutboundQueueId, eSBOutboundQueue.getESBOutboundQueueId());
    }
	
}