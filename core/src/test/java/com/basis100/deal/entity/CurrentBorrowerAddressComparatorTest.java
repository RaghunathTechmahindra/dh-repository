package com.basis100.deal.entity;

import java.io.IOException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.util.collections.BorrowerNumBorrowerComparator;
import com.basis100.deal.util.collections.CurrentBorrowerAddressComparator;
import com.basis100.resources.SessionResourceKit;

public class CurrentBorrowerAddressComparatorTest extends FXDBTestCase {
	
	private IDataSet dataSetTest;
	private CurrentBorrowerAddressComparator cbac = null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public CurrentBorrowerAddressComparatorTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(CurrentBorrowerAddressComparator.class.getSimpleName()+"DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected DatabaseOperation getSetUpOperation() throws Exception
	    {
		cbac = new CurrentBorrowerAddressComparator();
	    	return DatabaseOperation.INSERT;
	    }
	
	public void testCompare() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCompare");
		
		int id1 = Integer.parseInt((String)testExtract.getValue(0, "BORROWERADDRESSID1"));
		int id2 = Integer.parseInt((String)testExtract.getValue(0, "BORROWERADDRESSID2"));
		int copyid = Integer.parseInt((String)testExtract.getValue(0, "copyid"));
		
		BorrowerAddress ba1 = new BorrowerAddress(srk, null, id1, copyid);
		BorrowerAddress ba2 = new BorrowerAddress(srk, null, id2, copyid);
		
		com.basis100.deal.entity.DealEntity o1 = (com.basis100.deal.entity.DealEntity )ba1;
		com.basis100.deal.entity.DealEntity o2 = (com.basis100.deal.entity.DealEntity )ba2;
		
		int result = cbac.compare(o1, o2);
		
		assert result==0 || result==1 || result==-1;
		
		srk.rollbackTransaction();
	}
	

}
