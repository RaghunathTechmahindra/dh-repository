/*
 * @(#) PartyProfileTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filogix.express.test.UnitTestLogging;
import com.filogix.express.test.ExpressEntityTestCase;

import com.basis100.deal.pk.PartyProfilePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

/**
 * @author amalhi
 * 
 * JUnit test for entity PartyProfile
 * 
 */
public class PartyProfileTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(PartyProfileTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public PartyProfileTest() {

    }

    /**
     * To be executed before test
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {

        // free resources
        _srk.freeResources();
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int partyProfileId = _dataRepository.getInt("PARTYPROFILE",
                "partyProfileId", 0);
        int copyId = _dataRepository.getInt("PARTYPROFILE", "copyId", 0);
        int instProfId = _dataRepository.getInt("PARTYPROFILE",
                "INSTITUTIONPROFILEID", 0);
        String partyName = _dataRepository.getString("PARTYPROFILE",
                "partyName", 0);

        _logger.info(BORDER_START, "findByPrimaryKey");
        _logger.info("Got Values ::: [{}]", partyProfileId + "::" + copyId
                + "::" + instProfId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        PartyProfile entity = new PartyProfile(_srk);

        // Find by primary key
        entity = entity.findByPrimaryKey(new PartyProfilePK(partyProfileId));

        _logger.info(SHORT_BREAK);

        // verify the running result.
        assertEquals(copyId, entity.getCopyId());
        assertEquals(partyName, entity.getPartyName());
        assertEquals(partyProfileId, entity.getPartyProfileId());

        _logger.info(BORDER_END, "findByPrimaryKey");

    }

    /**
     * test create method of entity
     */
    @Test
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create");

        // get input data from repository
        int instProfId = _dataRepository.getInt("PartyProfile",
                "INSTITUTIONPROFILEID", 0);
        int partyTypeId = _dataRepository.getInt("PartyProfile", "partyTypeId",
                0);
        int profileStatusId = _dataRepository.getInt("PartyProfile",
                "profileStatusId", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // begin transaction
        _srk.beginTransaction();

        // inital new entity created
        PartyProfile newEntity = new PartyProfile(_srk);

        _logger.info("CREATED PARTY PROFILE BEFORE::: [{}]", partyTypeId + "::"
                + profileStatusId);

        // calling create method of entity
        newEntity = newEntity.create(partyTypeId, profileStatusId);

        _logger.info("CREATED PARTY PROFILE ::: [{}]", newEntity
                .getPartyTypeId()
                + "::" + newEntity.getPartyName());

        // check create correctly by calling findByPrimaryKey
        PartyProfile foundEntity = new PartyProfile(_srk);
        foundEntity = foundEntity.findByPrimaryKey(new PartyProfilePK(newEntity
                .getPartyProfileId()));

        // verifying new entity
        assertEquals(foundEntity.getPartyProfileId(), newEntity
                .getPartyProfileId());
        assertEquals(foundEntity.getPartyName(), newEntity.getPartyName());

        _logger.info(BORDER_END, "Create");

        // roll-back transaction.
        _srk.rollbackTransaction();

    }

    /**
     * test other create method of entity
     */
    @Test
    public void testCreateWithArgs() throws Exception {

        _logger.info(BORDER_START, "Create");

        // get input data from repository
        int instProfId = _dataRepository.getInt("PartyProfile",
                "INSTITUTIONPROFILEID", 0);
        String partyName = _dataRepository.getString("PartyProfile",
                "partyName", 0);
        int profileStatusId = _dataRepository.getInt("PartyProfile",
                "profileStatusId", 0);
        int contactId = _dataRepository.getInt("PartyProfile", "contactId", 0);
        int partyTypeId = _dataRepository.getInt("PartyProfile", "partyTypeId",
                0);
        String ptPShortName = _dataRepository.getString("PartyProfile",
                "ptPShortName", 0);
        String ptPBusinessId = _dataRepository.getString("PartyProfile",
                "ptPBusinessId", 0);
        String partyCompanyName = _dataRepository.getString("PartyProfile",
                "partyCompanyName", 0);
        String notes = _dataRepository.getString("PartyProfile", "notes", 0);
        String GEBranchTransitNum = _dataRepository.getString("PartyProfile",
                "GEBranchTransitNum", 0);
        String CHMCBranchTransitNum = _dataRepository.getString("PartyProfile",
                "CHMCBranchTransitNum", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // begin transaction
        _srk.beginTransaction();

        // inital new entity created
        PartyProfile newEntity = new PartyProfile(_srk);

        _logger.info("CREATED PARTY PROFILE with following values::: [{}]",
                partyName + "::" + profileStatusId + "::" + contactId + "::"
                        + partyTypeId + "::" + ptPShortName + "::"
                        + ptPBusinessId + "::" + partyCompanyName + "::"
                        + notes + "::" + GEBranchTransitNum + "::"
                        + CHMCBranchTransitNum);

        // calling create method of entity
        newEntity = newEntity.create(partyName, profileStatusId, contactId,
                partyTypeId, ptPShortName, ptPBusinessId, partyCompanyName,
                notes, GEBranchTransitNum, CHMCBranchTransitNum);

        _logger.info("CREATED PARTY PROFILE ::: [{}]", newEntity
                .getPartyTypeId()
                + "::" + newEntity.getPartyName());

        // check create correctly by calling findByPrimaryKey
        PartyProfile foundEntity = new PartyProfile(_srk);
        foundEntity = foundEntity.findByPrimaryKey(new PartyProfilePK(newEntity
                .getPartyProfileId()));

        // verifying new entity
        assertEquals(foundEntity.getPartyProfileId(), newEntity
                .getPartyProfileId());
        assertEquals(foundEntity.getPartyName(), newEntity.getPartyName());

        _logger.info(BORDER_END, "Create");

        // roll-back transaction.
        _srk.rollbackTransaction();

    }
}
