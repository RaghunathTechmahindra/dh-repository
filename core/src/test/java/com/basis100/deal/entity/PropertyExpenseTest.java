/*
 * @(#) PropertyExpenseTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.pk.PropertyExpensePK;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * @author amalhi
 * 
 * JUnit test for entity PropertyExpense
 * 
 */
public class PropertyExpenseTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(PropertyExpenseTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public PropertyExpenseTest() {

    }

    /**
     * To be executed before test
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env and free resources
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int propertyExpenseId = _dataRepository.getInt("PropertyExpense",
                "propertyExpenseId", 0);
        int instProfId = _dataRepository.getInt("PropertyExpense",
                "INSTITUTIONPROFILEID", 0);
        int propertyId = _dataRepository.getInt("PropertyExpense",
                "propertyId", 0);
        int copyId = _dataRepository.getInt("PropertyExpense", "copyId", 0);

        _logger.info(BORDER_START, "findByPrimaryKey");
        _logger.info("Got Values from repository for PropertyExpense ::: [{}]",
                propertyExpenseId + "::" + propertyId + "::" + copyId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        PropertyExpense _propertyExpense = new PropertyExpense(_srk,
                CalcMonitor.getMonitor(_srk));

        // Find by primary key
        _propertyExpense = _propertyExpense
                .findByPrimaryKey(new PropertyExpensePK(propertyExpenseId,
                        copyId));

        _logger.info("Got Values from new found Property ::: [{}]",
                _propertyExpense.getPropertyExpenseId() + "::"
                        + _propertyExpense.getPropertyId() + "::"
                        + _propertyExpense.getCopyId());

        // Check if the data retrieved matches the DB
        assertTrue(_propertyExpense.getPropertyExpenseId() == propertyExpenseId);
        assertTrue(_propertyExpense.getPropertyId() == propertyId);
        assertTrue(_propertyExpense.getCopyId() == copyId);

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * test create method of entity
     */
    @Test
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create");

        // get input data from repository
        int instProfId = _dataRepository.getInt("PropertyExpense",
                "INSTITUTIONPROFILEID", 0);
        int propertyId = _dataRepository.getInt("Property", "propertyId", 0);
        int copyId = _dataRepository.getInt("Property", "copyId", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // begin transaction
        _srk.beginTransaction();

        // Create the initial entity
        PropertyExpense newEntity = new PropertyExpense(_srk, CalcMonitor
                .getMonitor(_srk));

        _logger.info("CREATED NEW PropertyExpense ENTITY - BEFORE ::: [{}]",
                +propertyId + "::" + copyId);

        // call create method of entity
        newEntity = newEntity.create(new PropertyPK(propertyId, copyId));
        _logger.info("CREATED NEW PropertyExpense ENTITY ::: [{}]", newEntity
                .getPropertyExpenseId()
                + "::"
                + newEntity.getPropertyId()
                + "::"
                + newEntity.getCopyId());

        // check create correctly by calling findByPrimaryKey
        PropertyExpense foundEntity = new PropertyExpense(_srk, CalcMonitor
                .getMonitor(_srk));
        foundEntity = foundEntity.findByPrimaryKey(new PropertyExpensePK(
                newEntity.getPropertyExpenseId(), newEntity.getCopyId()));

        // verifying the created entity
        assertEquals(foundEntity.getPropertyExpenseId(), newEntity
                .getPropertyExpenseId());
        assertEquals(foundEntity.getPropertyId(), newEntity.getPropertyId());
        assertEquals(foundEntity.getCopyId(), newEntity.getCopyId());

        _logger.info(BORDER_END, "Create");

        // roll-back transaction.
        _srk.rollbackTransaction();

    }

}
