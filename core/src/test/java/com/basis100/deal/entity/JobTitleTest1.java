package com.basis100.deal.entity;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.docprep.extract.doctag.PrivacyClause;
import com.basis100.deal.pk.AddrPK;
import com.basis100.deal.pk.ConditionPK;
import com.basis100.deal.pk.ContactPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.EmploymentHistoryPK;
import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.deal.pk.InsureOnlyApplicantPK;
import com.basis100.deal.pk.InvestorProfilePK;
import com.basis100.resources.SessionResourceKit;
import com.sun.org.apache.bcel.internal.generic.INSTANCEOF;

public class JobTitleTest1 extends FXDBTestCase {
	private IDataSet dataSetTest;
    private JobTitle jobTitle = null;
  
    SessionResourceKit srk= new SessionResourceKit();
	
	public JobTitleTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(JobTitle.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(JobTitle.class.getSimpleName()+"DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.jobTitle = new JobTitle(srk);
		
		
	}
	
	public void testfindByName() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByName = dataSetTest.getTable("testfindByName");
		String description = (String)testfindByName.getValue(0, "JOBTITLEDESCRIPTION");
		
		JobTitle aJobTitle = jobTitle.findByName(description);
		
		assertNotNull(aJobTitle);
		
		srk.rollbackTransaction();
	}
	
	public void testfindByNameFailure() throws Exception
	{
		srk.beginTransaction();
		ITable testfindByNameFailure = dataSetTest.getTable("testfindByNameFailure");
		String description = (String)testfindByNameFailure.getValue(0, "JOBTITLEDESCRIPTION");
		
		JobTitle aJobTitle = jobTitle.findByName(description);
		
		assertNull(aJobTitle);
		
		srk.rollbackTransaction();
	}
	
}
