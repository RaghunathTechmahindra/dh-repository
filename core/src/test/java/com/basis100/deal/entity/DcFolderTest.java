/*
 * @(#)DcFolderTest.java    2005-3-16
 *
 */

package com.basis100.deal.entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.deal.pk.DcFolderPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;

/**
 * DCFolderTest -
 * 
 * @version 1.0 2005-3-16
 * @author <A HREF="mailto:chyxiang@yahoo.com">Chen Xiang (Sean)</A>
 */
public class DcFolderTest extends ExpressEntityTestCase {

    // The logger
    private static Log _log = LogFactory.getLog(DcFolderTest.class);

    // The Session Resource Kit
    private SessionResourceKit _srk;

    // DcFolder entity
    private DcFolder _dcFolderDAO;

    // institutionId for deal
    private int dealInstitutionId = 100;

    /*
     * setup
     */
    @Before
    public void setUp() throws Exception {

        // init resource
        ResourceManager.init();
        _srk = new SessionResourceKit("Testing");
        _dcFolderDAO = new DcFolder(_srk);
        
        // get institutionid
        dealInstitutionId = 
            _dataRepository.getInt("DcFolder", "InstitutionProfileId", 0);
    }

    /**
     * test get by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // set vpd institutionid
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        
        // get values from database
        int folderId = 
            _dataRepository.getInt("DcFolder", "DcFolderId", 0);
        String desc = 
            _dataRepository.getString("DcFolder", "DcFolderStatusDescription", 0);
        
        // get dcfolder by primary key
        DcFolder dcFolder = 
            _dcFolderDAO.findByPrimaryKey(new DcFolderPK(folderId));
        
        // verify result
        assertEquals(desc, dcFolder.getDcFolderStatusDescription());
        _log.info("Found Dc Folder: ");
        _log.info("     Dc Folder AppId: " + dcFolder.getSourceApplicationId());
        _log.info("      Dc Folder Desc: "
                + dcFolder.getDcFolderStatusDescription());
        _log.info("      Dc Folder Code: " + dcFolder.getDcFolderCode());
        _log.info("====================================");

    }

    /*
     * test find by source app Id
     */
    @Test
    public void testFindBySourceAppId() throws Exception {
        
        // set vpd institutionId
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        
        // get values from database
        String appId = 
            _dataRepository.getString("DcFolder", "SourceApplicationId", 0);
        String desc = 
            _dataRepository.getString("DcFolder", "DcFolderStatusDescription", 0);
        
        // get dcfolder by source app id
        DcFolder dcFolder = _dcFolderDAO.findBySourceAppId(appId);
        
        // verify result
        assertEquals(desc, dcFolder.getDcFolderStatusDescription());
        _log.info("Found Dc Folder SourceAppId: ");
        _log.info("     Dc Folder AppId: " + dcFolder.getSourceApplicationId());
        _log.info("      Dc Folder Desc: "
                + dcFolder.getDcFolderStatusDescription());
        _log.info("      Dc Folder Code: " + dcFolder.getDcFolderCode());
        _log.info("====================================");
    }

    /*
     * test Create
     */
    @Test
    public void testCreate() throws Exception {
        
        // set vpd instituionId
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        
        // create a dcFolder
        String testFolder = "Test Folder";
        DcFolder dcFolder = _dcFolderDAO.create(testFolder, "1234", "3",
                "status desc");
        
        // verify result
        assertEquals(testFolder, dcFolder.getSourceApplicationId());
        _log.info("Found Dc Folder SourceAppId: ");
        _log.info("     Dc Folder AppId: " + dcFolder.getSourceApplicationId());
        _log.info("      Dc Folder Desc: "
                + dcFolder.getDcFolderStatusDescription());
        _log.info("      Dc Folder Code: " + dcFolder.getDcFolderCode());
        _log.info("====================================");
    }
    
    /*
     * teatdown
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

}
