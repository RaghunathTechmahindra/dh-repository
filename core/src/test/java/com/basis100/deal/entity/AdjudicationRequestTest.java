/*
 * @(#)AdjudicationRequestTest.java Sep 21, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.AdjudicationRequestPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * AdjudicationRequestTest
 * 
 * @version 1.0 Sep 21, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class AdjudicationRequestTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(AdjudicationRequestTest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test the find by primary key
     */
    @Ignore("No Data") @Test
    public void testFindByPrimaryKey() throws Exception {
        // get input data from repository
        int dealInstitutionId = _dataRepository.getInt("AdjudicationRequest",
                "InstitutionProfileID", 0);
        int copyID = _dataRepository.getInt("AdjudicationRequest", "copyID", 0);
        int requestID = _dataRepository.getInt("AdjudicationRequest",
                "requestID", 0);

        _logger.info(BORDER_START, "testFindByPrimaryKey");

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // initial entity
        AdjudicationRequest adjRequest = new AdjudicationRequest(_srk);

        adjRequest = adjRequest.findByPrimaryKey(new AdjudicationRequestPK(
                requestID));

        _logger.info(BORDER_END, "testFindByPrimaryKey");
    }

    /**
     * test Create
     * 
     * @throws Exception
     */
    @Ignore("Failing") @Test
    public void testCreate() throws Exception {
        int dealInstitutionId = _dataRepository.getInt("Request",
                "InstitutionProfileID", 0);
        // get input data from repository
        int copyId = _dataRepository.getInt("Request", "copyID", 0);
        int requestID = _dataRepository.getInt("Request", "requestID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // Start new transaction
        _srk.beginTransaction();

        _logger.info(BORDER_START, "testCreate");

        // Create the initial entity
        AdjudicationRequest adjRequest = new AdjudicationRequest(_srk);

        adjRequest = adjRequest.create(new RequestPK(requestID, copyId));

        adjRequest.ejbStore();

        // roll back
        _srk.rollbackTransaction();
        _logger.info(BORDER_END, "Create");

    }

    /**
     * test Create
     * 
     * @throws Exception
     */
    @Ignore("Failing") @Test
    public void testCreate2() throws Exception {
        int dealInstitutionId = _dataRepository.getInt("Request",
                "InstitutionProfileID", 0);
        // get input data from repository
        int copyId = _dataRepository.getInt("Request", "copyID", 0);
        int requestID = _dataRepository.getInt("Request", "requestID", 0);
        int scenarioNum = 1;
        int numofApps = 1;

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // Start new transaction
        _srk.beginTransaction();

        _logger.info(BORDER_START, "testCreate");

        // Create the initial entity
        AdjudicationRequest adjRequest = new AdjudicationRequest(_srk);

        adjRequest = adjRequest.create(new RequestPK(requestID, copyId),
                scenarioNum, numofApps);

        adjRequest.ejbStore();

        // roll back
        _srk.rollbackTransaction();
        _logger.info(BORDER_END, "Create");

    }

}
