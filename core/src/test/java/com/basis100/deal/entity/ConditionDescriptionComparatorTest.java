package com.basis100.deal.entity;

import java.io.IOException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.util.collections.BorrowerNumBorrowerComparator;
import com.basis100.deal.util.collections.ConditionDescriptionComparator;
import com.basis100.resources.SessionResourceKit;

public class ConditionDescriptionComparatorTest extends FXDBTestCase {
	
	private IDataSet dataSetTest;
	private ConditionDescriptionComparator cdc = null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public ConditionDescriptionComparatorTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ConditionDescriptionComparator.class.getSimpleName()+"DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected DatabaseOperation getSetUpOperation() throws Exception
	    {
		cdc = new ConditionDescriptionComparator();
	    	return DatabaseOperation.INSERT;
	    }
	
	public void testCompare() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCompare");
		
		int doctrackid1 = Integer.parseInt((String)testExtract.getValue(0, "DOCUMENTTRACKINGID1"));
		int doctrackid2 = Integer.parseInt((String)testExtract.getValue(0, "DOCUMENTTRACKINGID2"));
		int copyid = Integer.parseInt((String)testExtract.getValue(0, "copyid"));
		
		DocumentTracking dt1 = new DocumentTracking(srk, doctrackid1, copyid);
		DocumentTracking dt2 = new DocumentTracking(srk, doctrackid2, copyid);
		
		com.basis100.deal.entity.DealEntity o1 = (com.basis100.deal.entity.DealEntity )dt1;
		com.basis100.deal.entity.DealEntity o2 = (com.basis100.deal.entity.DealEntity )dt2;
		
		int result = cdc.compare(o1, o2);
		
		assert result==0 || result==1 || result==-1;
		
		srk.rollbackTransaction();
	}
	
	
}
