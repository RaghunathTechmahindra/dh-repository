/*
 * @(#)ChannelTest.java    2005-3-16
 *
 */

package com.basis100.deal.entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.deal.pk.ChannelPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;

/**
 * ChannelTest -
 * 
 * @version 1.0 2007-3-16
 * @author <A HREF="mailto:chyxiang@yahoo.com">Chen Xiang (Sean)</A>
 */
public class ChannelTest extends ExpressEntityTestCase {

    // The logger
    private static Log _log = LogFactory.getLog(ChannelTest.class);

    // the Session Resource Kit
    private SessionResourceKit _srk;

    // the channel entity
    Channel _channelDAO;
    
    // institution id for deal
    private int dealInstitutionId;

    /*
     * setup
     */
    @Before
    public void setUp() throws Exception {
        
        // init resource and srk
        ResourceManager.init();
        _srk = new SessionResourceKit("Testing");
        
        // get institutionid from database
        dealInstitutionId = _dataRepository.getInt("Channel",
                "InstitutionProfileId", 0);
        
        _channelDAO = new Channel(_srk);
    }

    /*
     * test find by primarykey
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // set institution id
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        
        // get value from database
        int channelId = _dataRepository.getInt("Channel", "ChannelId", 0);
        int channelTypeId = _dataRepository
            .getInt("Channel", "ChannelTypeId", 0);
        
        // find a channel by primary key
        ChannelPK channelPK = new ChannelPK(channelId);
        _channelDAO.findByPrimaryKey(channelPK);
        
        // verify result
        assertEquals(channelTypeId, _channelDAO.getChannelTypeId());
        _log.info("Channel Found " + _channelDAO);
        _log.info("Channel ID " + _channelDAO.getChannelId());
        _log.info("Channel Type ID " + _channelDAO.getChannelTypeId());
        _log.info("Channel Path " + _channelDAO.getPath());
    }

    /*
     * test FindByServiceProductAndTransactionType
     */
    @Test
    public void testFindByServiceProductAndTransactionType() throws Exception {

        // set vpd context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // get values from database
        int productId = _dataRepository
            .getInt("SERVICEPRODUCTREQUESTASSOC", "ServiceProductId", 0);
        int transactionTypeId = _dataRepository
            .getInt("SERVICEPRODUCTREQUESTASSOC", "ServiceTransactionTypeId", 0);
        
        // find a channel by serviceproductId and trasactiontypeId
        _channelDAO
            .findByServiceProductAndTransactionType(productId, transactionTypeId);
        //assert(_channelDAO.getChannelTypeId() == _dataRepository
        //        .getInt("Channel", "ChannelTypeId", 0));
        //TODO: need to verify result,
        _log.info("Channel Found " + _channelDAO);
        _log.info("Channel ID " + _channelDAO.getChannelId());
        _log.info("Channel Type ID " + _channelDAO.getChannelTypeId());
        _log.info("Channel Path " + _channelDAO.getPath());

    }
    
    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        //free the srk
        _srk.freeResources();
    }
}
