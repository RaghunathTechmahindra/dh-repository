/*
 * @(#)AdjudicationApplicantRequestTest.java Sep 21, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.AdjudicationApplicantRequestPK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * AdjudicationApplicantRequestTest
 * 
 * @version 1.0 Sep 21, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class AdjudicationApplicantRequestTest extends ExpressEntityTestCase
        implements UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(AdjudicationApplicantRequestTest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test the find by primary key
     */
    @Ignore("No Data") @Test
    public void testFindByPrimaryKey() throws Exception {
        // get input data from repository
        int dealInstitutionId = _dataRepository.getInt(
                "AdjudicationApplicantRequest", "InstitutionProfileID", 0);
        int applicantNumber = _dataRepository.getInt(
                "AdjudicationApplicantRequest", "applicantNumber", 0);
        int copyID = _dataRepository.getInt("AdjudicationApplicantRequest",
                "copyID", 0);
        int requestID = _dataRepository.getInt("AdjudicationApplicantRequest",
                "requestID", 0);

        _logger.info(BORDER_START, "testFindByPrimaryKey");

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        AdjudicationApplicantRequest appRequest = new AdjudicationApplicantRequest(
                _srk);
        // Find by primary key
        appRequest = appRequest
                .findByPrimaryKey(new AdjudicationApplicantRequestPK(requestID,
                        applicantNumber, copyID));

        _logger.info(BORDER_END, "testFindByPrimaryKey");

    }

    /**
     * test Create
     * 
     * @throws Exception
     */
    @Ignore("Failing") @Test
    public void testCreate() throws Exception {
        int dealInstitutionId = _dataRepository.getInt("Request",
                "InstitutionProfileID", 0);
        // get input data from repository
        int copyID = _dataRepository.getInt("Borrower", "copyID", 0);
        int borrowerID = _dataRepository.getInt("Borrower", "borrowerID", 0);
        int requestID = _dataRepository.getInt("Request", "requestID", 0);
        int appnum = 1;
        String appcurrind = "A";

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // Start new transaction
        _srk.beginTransaction();

        _logger.info(BORDER_START, "testCreate");

        // Create the initial entity
        AdjudicationApplicantRequest appRequest = new AdjudicationApplicantRequest(
                _srk);

        // create a new Adjudication Applicant Request
        appRequest = appRequest.create(new RequestPK(requestID, copyID),
                new BorrowerPK(borrowerID, copyID), appnum, appcurrind);

        appRequest.ejbStore();
        
        // roll back
        _srk.rollbackTransaction();
        _logger.info(BORDER_END, "Create");

    }

}
