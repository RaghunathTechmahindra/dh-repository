/*
 * @(#)LiabilityTest.java Sep 7, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.entity;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.LiabilityPK;

/**
 * LiabilityTest
 * 
 * @version 1.0 Sep 7, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class LiabilityTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(LiabilityTest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test the find by primary key
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {
        // get input data from repository
        int liabilityID = _dataRepository.getInt("Liability", "liabilityID", 0);
        int copyID = _dataRepository.getInt("Liability", "copyID", 0);
        int borrowerID = _dataRepository.getInt("Liability", "borrowerID", 0);
        int dealInstitutionId = _dataRepository.getInt("Liability",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "findByPrimaryKey");

        // Create the initial entity
        Liability _liability = new Liability(_srk, CalcMonitor.getMonitor(_srk));

        // find by primary key
        _liability = _liability.findByPrimaryKey(new LiabilityPK(liabilityID,
                copyID));

        // Check that the data retrieved matches the DB
        assertTrue(_liability.getBorrowerId() == borrowerID);

        _logger.info(BORDER_END, "findByPrimaryKey");

    }

    /**
     * test the find by Borrower
     */
    @Test
    public void testFindByBorrower() throws Exception {
        // get input data from repository
        int liabilityID = _dataRepository.getInt("Liability", "liabilityID", 0);
        int copyID = _dataRepository.getInt("Liability", "copyID", 0);
        int borrowerID = _dataRepository.getInt("Liability", "borrowerID", 0);
        int dealInstitutionId = _dataRepository.getInt("Liability",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "testFindByBorrower");

        // Create the initial entity
        Liability _liability = new Liability(_srk, CalcMonitor.getMonitor(_srk));

        // find by Borrower
        List<Liability> liablities = (List) _liability
                .findByBorrower(new BorrowerPK(borrowerID, copyID));

        // Check that the data retrieved matches the DB
        assertTrue(liabilityID == liablities.get(0).getLiabilityId());

        _logger.info(BORDER_END, "testFindByBorrower");

    }

   /**
     * test the find by Deal
     */
    /*@Test
    public void testFindByDeal() throws Exception {
        // get input data from repository
        int dealID = _dataRepository.getInt("Deal", "dealID", 0);
        int copyID = _dataRepository.getInt("Deal", "copyID", 0);
        int dealInstitutionId = _dataRepository.getInt("Liability",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "testFindByDeal");

        // Create the initial entity
        Liability _liability = new Liability(_srk, CalcMonitor.getMonitor(_srk));

        // find by Deal
        List<Liability> liablities = (List) _liability.findByDeal(new DealPK(
                dealID, copyID));

        // Check that the data retrieved matches the DB
        Borrower borrower = new Borrower(_srk, CalcMonitor.getMonitor(_srk));
        borrower.findByPrimaryKey(new BorrowerPK(liablities.get(0)
                .getBorrowerId(), copyID));
        assertTrue(dealID == borrower.getDealId());

        _logger.info(BORDER_END, "testFindByDeal");

    }*/

    /**
     * test the find by Deal
     */
    @Test
    public void testFindNonMortgageTypes() throws Exception {
        // get input data from repository
        int dealID = _dataRepository.getInt("Deal", "dealID", 0);
        int copyID = _dataRepository.getInt("Deal", "copyID", 0);
        int liabilityID = _dataRepository.getInt("Liability", "liabilityID", 0);
        int dealInstitutionId = _dataRepository.getInt("Liability",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        _logger.info(BORDER_START, "testFindByDeal");

        // Create the initial entity
        Liability _liability = new Liability(_srk, CalcMonitor.getMonitor(_srk));

        // find by Deal
        List<Liability> liablities = (List) _liability
                .findNonMortgageTypes(new DealPK(dealID, copyID));

        // Check that the data retrieved matches the DB
        // TODO: Figure out an assert
        // assertTrue(liablities.size() > 0);

        _logger.info(BORDER_END, "testFindByDeal");

    }

    /**
     * test Create
     */
    @Test
    public void testCreate() throws Exception {
        // get input data from repository
        int copyID = _dataRepository.getInt("Liability", "copyID", 0);
        int borrowerID = _dataRepository.getInt("Liability", "borrowerID", 0);
        int dealInstitutionId = _dataRepository.getInt("Liability",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // Start new transaction
        _srk.beginTransaction();

        _logger.info(BORDER_START, "testCreate");

        // Create the initial entity
        Liability _liability = new Liability(_srk, CalcMonitor.getMonitor(_srk));

        // find by Deal
        _liability = _liability.create(new BorrowerPK(borrowerID, copyID));

        // Store it
        _liability.ejbStore();

        // Check that the data retrieved matches the DB
        List<Liability> liablities = (List) _liability
                .findByBorrower(new BorrowerPK(borrowerID, copyID));
        int newLiabilityID = _liability.getLiabilityId();

        // Check the list to see if any Liability matched the ones retrieved
        boolean assertFlag = false;
        for (int index = 0; index < liablities.size(); index++) {
            if (liablities.get(index).getLiabilityId() == newLiabilityID) {
                // Found the Liability
                assertFlag = true;
            }

        }
        // Assert on the outcome of the comparison above
        assertTrue(assertFlag);

        // Rollback transaction
        _srk.rollbackTransaction();

        _logger.info(BORDER_END, "testCreate");

    }

    @Test
    public void testCreditBureauRecordIndicator() throws Exception {
        // get input data from repository
        int copyID = _dataRepository.getInt("Liability", "copyID", 0);
        int borrowerID = _dataRepository.getInt("Liability", "borrowerID", 0);
        int dealInstitutionId = _dataRepository.getInt("Liability",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // Start new transaction
        _srk.beginTransaction();

        _logger.info(BORDER_START, "testCreate");

        // Create the initial entity
        Liability _liability = new Liability(_srk, CalcMonitor.getMonitor(_srk));

        // find by Deal
        _liability = _liability.create(new BorrowerPK(borrowerID, copyID));

                
        String indicator = "Y";
        _liability.setCreditBureauRecordIndicator(indicator);
        assertEquals(indicator, _liability.getCreditBureauRecordIndicator());
    
    }
    
    @Test
    public void testCreditLimit() throws Exception {
        // get input data from repository
        int copyID = _dataRepository.getInt("Liability", "copyID", 0);
        int borrowerID = _dataRepository.getInt("Liability", "borrowerID", 0);
        int dealInstitutionId = _dataRepository.getInt("Liability",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // Start new transaction
        _srk.beginTransaction();

        _logger.info(BORDER_START, "testCreate");

        // Create the initial entity
        Liability _liability = new Liability(_srk, CalcMonitor.getMonitor(_srk));

        // find by Deal
        _liability = _liability.create(new BorrowerPK(borrowerID, copyID));

                
        double creditLimit = 1000;
        _liability.setCreditLimit(creditLimit);
        assertEquals(creditLimit, _liability.getCreditLimit(), 0);
    
    }
    
    @Test
    public void testMaturityDate() throws Exception {
        // get input data from repository
        int copyID = _dataRepository.getInt("Liability", "copyID", 0);
        int borrowerID = _dataRepository.getInt("Liability", "borrowerID", 0);
        int dealInstitutionId = _dataRepository.getInt("Liability",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // Start new transaction
        _srk.beginTransaction();

        _logger.info(BORDER_START, "testCreate");

        // Create the initial entity
        Liability _liability = new Liability(_srk, CalcMonitor.getMonitor(_srk));

        // find by Deal
        _liability = _liability.create(new BorrowerPK(borrowerID, copyID));

                
        Date date = new Date();
        _liability.setMaturityDate(date);
        assertEquals(date, _liability.getMaturityDate());
    
    }
}
