/*
 * @(#)CreditBureauReportTest.java    2005-9-19
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */

package com.basis100.deal.entity;

import java.util.Collection;
import java.util.Iterator;

import oracle.sql.CLOB;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.deal.pk.CreditBureauReportPK;
import com.basis100.deal.pk.DealPK;
import com.filogix.express.test.ExpressEntityTestCase;

/**
 * CreditBureauReportTest -
 * 
 * @version 1.0 2005-9-19
 * @author <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class CreditBureauReportTest extends ExpressEntityTestCase {

    // The logger
    private static Log _log = LogFactory.getLog(CreditBureauReportTest.class);

    // Session Resource kits.
    private SessionResourceKit _srk;

    // creditbureaureport entity
    private CreditBureauReport _reportDAO;

    // institutionId for deal
    private int dealInstitutionId;

    /**
     * Constructor function
     */
    public CreditBureauReportTest() {
    }

    /**
     * constructs a test case with the given test case name.
     */
    public CreditBureauReportTest(String name) {

    }

    /**
     * setting up the testing env.
     * 
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {

        // init resources
        ResourceManager.init();
        _srk = new SessionResourceKit("Testing");
        _reportDAO = new CreditBureauReport(_srk);

        // get institutionId from database
        dealInstitutionId = _dataRepository.getInt("CreditBureauReport",
                "InstitutionProfileId", 0);

    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
    }

  /*  *//**
     * test read the report! it is a CLOB column.
     *//*
    @Test
    public void testReadReport() throws Exception {

        // set vpd context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // get dealId from database
        int dealId = _dataRepository.getInt("CreditBureauReport", "DealId", 0);

        // find report by dealId
        Collection reports = _reportDAO.findByDealid(dealId);
        _log.info("Found [" + reports.size() + "] credit report(s).");

        for (Iterator i = reports.iterator(); i.hasNext();) {

            CreditBureauReport report = (CreditBureauReport) i.next();
            _log.info("Credit Report ID: " + report.getCreditBureauReportId());
            _log.info("          Report: ");
            _log.info(report.getCreditReport());
        }

    }
*/
  /*  *//**
     * copying the report.
     *//*
    @Test
    public void testCopyReport() throws Exception {

        // get values from database
        int institutionId = _dataRepository.getInt("Deal",
                "InstitutionProfileId", 0);
        int fromid = _dataRepository.getInt("CreditBureauReport", "DealId", 0);
        int toid = _dataRepository.getInt("Deal", "DealId", 0);
        int toCopyId = _dataRepository.getInt("Deal", "DealId", 0);
        
        // set vpd context
        _srk.getExpressState().setDealInstitutionId(institutionId);

        _log.info("Reading credit reports for deal: " + fromid);
        Collection reports = _reportDAO.findByDealid(fromid);
        _log.info("Found [" + reports.size() + "] credit report(s).");

        for (Iterator i = reports.iterator(); i.hasNext();) {

            CreditBureauReport report = (CreditBureauReport) i.next();
            _log.info("Credit Report ID: " + report.getCreditBureauReportId());
            _log.info("          Report: ");
            _log.info(report.getCreditReport());
            CreditBureauReport newReport = new CreditBureauReport(_srk);
            newReport = newReport.create(new DealPK(toid, toCopyId));
            newReport.setCreditReport(report.getCreditReport());
            newReport.ejbStore();
            
            //verify result
            assertEquals(report.getCreditReport(), newReport.getCreditReport());
        }
    }*/

    /*
     * test findby primary key
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // set institutionId
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // get values from database
        int id = _dataRepository.getInt("CreditBureauReport",
                "CreditBureauReportId", 0);
        CLOB value = _dataRepository.getClob("CreditBureauReport",
                "CreditReport", 0);
        
        // find report by primary key
        CreditBureauReport report = 
            _reportDAO.findByPrimaryKey(new CreditBureauReportPK(id));
        
        // verify result
        assertNotNull(value);
        _log.info("Found CreditBureauReport: ");
        _log.info("           Deal Id: " + report.getDealId());
        _log.info("  Credit Report Id: " + report.getCreditBureauReportId());
        _log.info("            Report: " + value);
        _log.info("====================================");
    }

    /*
     * test create
     */
    @Test
    public void testCreate() throws Exception {
        
        // get values form database
        int dealId = _dataRepository.getInt("Deal", "DealId", 0);
        int copyId = _dataRepository.getInt("Deal", "CopyId", 0);

        // set vpd context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // create a report for a deal
        CreditBureauReport newReport;
        newReport = new CreditBureauReport(_srk);
        newReport = newReport.create(new DealPK(dealId, copyId));
        
        // verify result
        assertEquals(dealId, newReport.getDealId());
        _log.info("Create CreditBureauReport: ");
        _log.info("           Deal Id: " + newReport.getDealId());
        _log.info("  Credit Report Id: " + newReport.getCreditBureauReportId());
        _log.info("            Report: " + newReport.getCreditReport());
        _log.info("====================================");
    }

}
