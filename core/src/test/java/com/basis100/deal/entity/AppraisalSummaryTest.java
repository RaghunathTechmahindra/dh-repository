/*
 * @(#)AppraisalSummaryTest.java Sep 21, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.AppraisalSummaryPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * AppraisalSummaryTest
 * 
 * @version 1.0 Sep 21, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class AppraisalSummaryTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(AppraisalOrderTest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test the find by primary key
     */
    @Ignore("No Data") @Test
    public void testFindByPrimaryKey() throws Exception {
        // get input data from repository
        int responseID = _dataRepository.getInt("AppraisalSummary",
                "responseID", 0);
        int appraisalValue = _dataRepository.getInt("AppraisalSummary",
                "ACTUALAPPRAISALVALUE", 0);

        int dealInstitutionId = _dataRepository.getInt("AppraisalSummary",
                "InstitutionProfileID", 0);

        _logger.info(BORDER_START, "testFindByPrimaryKey");

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        AppraisalSummary appraisalSummary = new AppraisalSummary(_srk);

        // find by primary key
        appraisalSummary = appraisalSummary
                .findByPrimaryKey(new AppraisalSummaryPK(responseID));

        // verify data in the database
        assertEquals(appraisalSummary.getActualAppraisalValue(), appraisalValue);

        _logger.info(BORDER_END, "testFindByPrimaryKey");

    }

  /*  *//**
     * test Create
     *//*
    @Test
    public void testCreate() throws Exception {
        // get input data from repository
        int responseId = _dataRepository.getInt("Response",
                "responseID", 0);

        int dealInstitutionId = _dataRepository.getInt("Response",
                "InstitutionProfileID", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // Start new transaction
        _srk.beginTransaction();

        _logger.info(BORDER_START, "testCreate");

        AppraisalSummary appraisalSummary = new AppraisalSummary(_srk);

        // create a new Appraisal summary
        appraisalSummary = appraisalSummary.create(responseId);

        appraisalSummary.ejbStore();

        // roll back
        _srk.rollbackTransaction();
        _logger.info(BORDER_END, "Create");

    }
*/
}
