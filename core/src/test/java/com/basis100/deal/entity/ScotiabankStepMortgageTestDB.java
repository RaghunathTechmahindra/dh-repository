package com.basis100.deal.entity;

import java.io.File;
import java.io.IOException;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.AppraisalOrderPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.ltx.unittest.util.EntityTestUtil;

public class ScotiabankStepMortgageTestDB extends FXDBTestCase{

	private IDataSet dataSetTest;	
	private ScotiabankStepMortgage scotiabankStepMortgage;
	
	
	
	public ScotiabankStepMortgageTestDB(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ScotiabankStepMortgage.class.getSimpleName() + "DataSetTest.xml"));
	}
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		scotiabankStepMortgage = new ScotiabankStepMortgage(srk);
	}
	
	public void testCreate() throws DataSetException,IOException, SQLException, RemoteException, CreateException, JdbcTransactionException{	
		ITable testCreate = dataSetTest.getTable("testCreate");	
		srk.beginTransaction();
		int dealId=Integer.valueOf((String)testCreate.getValue(0,"DEALID"));		   
	    int copyId=Integer.valueOf((String)testCreate.getValue(0,"COPYID"));
	    int stepMortgageId=Integer.valueOf((String)testCreate.getValue(0,"STEPMORTGAGEID"));
	    int mtgtypeid=Integer.valueOf((String)testCreate.getValue(0,"STEPMORTGAGETYPEID"));	
	    int prodid=Integer.valueOf((String)testCreate.getValue(0,"STEPPRODUCTID"));
	    int mtgnumber=Integer.valueOf((String)testCreate.getValue(0,"STEPMORTGAGENUMBER"));
	    double amtadv=Double.valueOf((String)testCreate.getValue(0,"STEPMORTGAGEAMOUNTADVANCED"));
	    int term=Integer.valueOf((String)testCreate.getValue(0,"STEPMORTGAGETERM"));
	    int amort=Integer.valueOf((String)testCreate.getValue(0,"STEPMORTGAGEAMORTIZATION"));
	    int userid=Integer.valueOf((String)testCreate.getValue(0,"USERPROFILEID"));	    
	    DealPK dpk=new DealPK(dealId,copyId);
	    srk.getExpressState().setDealInstitutionId(1);	
	    scotiabankStepMortgage=scotiabankStepMortgage.create(dpk,stepMortgageId,mtgtypeid,prodid,mtgnumber,amtadv,term,amort,userid);		   
	    assertEquals(dealId,dpk.getId());
	   srk.rollbackTransaction();
}  
	
	public void testEquals() throws DataSetException,IOException, SQLException, RemoteException, CreateException, JdbcTransactionException, FinderException{	
		
		boolean equals=true;
		ITable testEquals = dataSetTest.getTable("testEquals");	
		srk.beginTransaction();
		int dealId=Integer.valueOf((String)testEquals.getValue(0,"DEALID"));		   
	    int copyId=Integer.valueOf((String)testEquals.getValue(0,"COPYID"));
	    int stepMortgageId=Integer.valueOf((String)testEquals.getValue(0,"STEPMORTGAGEID"));	    
	    scotiabankStepMortgage = new ScotiabankStepMortgage(srk,stepMortgageId,dealId,copyId);	   
	    srk.getExpressState().setDealInstitutionId(1);	
	    equals = scotiabankStepMortgage.equals(scotiabankStepMortgage);		   
	    assertEquals(equals,equals);	   
}  
	
public void testClone() throws DataSetException,IOException, SQLException, RemoteException, CreateException,  CloneNotSupportedException{	
	   try {
		srk.beginTransaction();	
		Object obj=null;
		ITable testClone = dataSetTest.getTable("testClone");			
		int dealId=Integer.valueOf((String)testClone.getValue(0,"DEALID"));		   
	    int copyId=Integer.valueOf((String)testClone.getValue(0,"COPYID"));
	    int stepMortgageId=Integer.valueOf((String)testClone.getValue(0,"STEPMORTGAGEID"));
	    int mtgtypeid=Integer.valueOf((String)testClone.getValue(0,"STEPMORTGAGETYPEID"));	
	    int prodid=Integer.valueOf((String)testClone.getValue(0,"STEPPRODUCTID"));
	    int mtgnumber=Integer.valueOf((String)testClone.getValue(0,"STEPMORTGAGENUMBER"));
	    double amtadv=Double.valueOf((String)testClone.getValue(0,"STEPMORTGAGEAMOUNTADVANCED"));
	    int term=Integer.valueOf((String)testClone.getValue(0,"STEPMORTGAGETERM"));
	    int amort=Integer.valueOf((String)testClone.getValue(0,"STEPMORTGAGEAMORTIZATION"));
	    int userid=Integer.valueOf((String)testClone.getValue(0,"USERPROFILEID"));	   	       
	    DealPK dpk=new DealPK(dealId,copyId);
	    scotiabankStepMortgage.setStepMortgageId(stepMortgageId);
	    scotiabankStepMortgage.setStepMortgageTypeId(mtgtypeid);
	    scotiabankStepMortgage.setStepProductId(prodid);
	    scotiabankStepMortgage.setStepMortgageNumber(mtgnumber);
	    scotiabankStepMortgage.setStepMortgageAmountAdvanced(amtadv);
	    scotiabankStepMortgage.setStepMortgageTerm(term);
	    scotiabankStepMortgage.setStepMortgageAmortization(amort);
	    scotiabankStepMortgage.setUserProfileId(userid);
	    srk.getExpressState().setDealInstitutionId(1);	
	    scotiabankStepMortgage.clone(dpk);		   
	    assertEquals(dealId, dpk.getId());
	    srk.rollbackTransaction();
	   } catch (JdbcTransactionException e) {			
			e.printStackTrace();
		}
}  
	
	
}
