package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.doctag.PrivacyClause;
import com.basis100.deal.pk.ConditionPK;
import com.basis100.resources.SessionResourceKit;
import com.sun.org.apache.bcel.internal.generic.INSTANCEOF;

public class ConditionFactoryTest extends FXDBTestCase {
	private IDataSet dataSetTest;
    private ConditionFactory conditionFactory = null;
    private ConditionPK conditionPK = null;
    private Collection<String> collection=null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public ConditionFactoryTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ConditionFactory.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.conditionFactory = conditionFactory.getInstance();
	}
	public void testgetCondition() throws Exception
	{
		srk.beginTransaction();
		ITable testgetCondition = dataSetTest.getTable("testgetCondition");
		int dealInstitutionId = Integer.parseInt((String)testgetCondition.getValue(0, "INSTITUTIONPROFILEID"));
		int conditionId = Integer.parseInt((String)testgetCondition.getValue(0, "CONDITIONID"));
		
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		
		Condition condition = conditionFactory.getCondition(srk, conditionId);
	
		assertNotNull(condition);
		
		srk.rollbackTransaction();
	}
	/*
	 * Failure case not applicable 
	 */
	
	
}
