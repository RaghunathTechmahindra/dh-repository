package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.BncAdjudicationApplRequestPK;
import com.basis100.deal.pk.BncAdjudicationResponsePK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.RequestPK;


/**
 * <p>BncAdjudicationApplRequestDbTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class BncAdjudicationApplRequestDbTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private BncAdjudicationApplRequest bncAdjudicationApplRequest;
	
	public BncAdjudicationApplRequestDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(BncAdjudicationApplRequest.class.getSimpleName() + "DataSetTest.xml"));
	}

	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.bncAdjudicationApplRequest = new BncAdjudicationApplRequest(srk);
	}
	
	public void testFindAllByRequest() throws Exception {
    	ITable expectedBncAdjudicationApplRequest = dataSetTest.getTable("testFindAllByRequest");
    	int id = (Integer) DataType.INTEGER.typeCast(expectedBncAdjudicationApplRequest.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expectedBncAdjudicationApplRequest.getValue(0, "COPYID"));
    	bncAdjudicationApplRequest.findAllByRequest(new RequestPK(id, copyId));
    	assertNotNull(bncAdjudicationApplRequest);
    }
	
	/*public void testCreate() throws Exception {
    	ITable expectedBncAdjudicationApplRequest = dataSetTest.getTable("testCreate");
    	srk.beginTransaction();
    	int id = (Integer) DataType.INTEGER.typeCast(expectedBncAdjudicationApplRequest.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expectedBncAdjudicationApplRequest.getValue(0, "COPYID"));
    	int applicantNumber = (Integer) DataType.INTEGER.typeCast(expectedBncAdjudicationApplRequest.getValue(0, "APPLICANTNUMBER"));
    	int requestedCreditBureauNameId = (Integer) DataType.INTEGER.typeCast(expectedBncAdjudicationApplRequest.getValue(0, "REQUESTEDCREDITBUREAUNAMEID"));
    	srk.getExpressState().setDealInstitutionId(1);
    	bncAdjudicationApplRequest.create(new BncAdjudicationApplRequestPK(id,copyId,applicantNumber), requestedCreditBureauNameId);
	    assertNull(bncAdjudicationApplRequest);
	    srk.rollbackTransaction();
    }
	
	public void testCreate1() throws Exception {
    	ITable expectedBncAdjudicationApplRequest = dataSetTest.getTable("testCreate1");
    	srk.beginTransaction();
    	int id = (Integer) DataType.INTEGER.typeCast(expectedBncAdjudicationApplRequest.getValue(0, "DEALID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expectedBncAdjudicationApplRequest.getValue(0, "COPYID"));
    	int applicantNumber = (Integer) DataType.INTEGER.typeCast(expectedBncAdjudicationApplRequest.getValue(0, "APPLICANTNUMBER"));
    	String appCurrInd = (String)(expectedBncAdjudicationApplRequest.getValue(0, "APPLICURRIND"));
    	int requestedCreditBureauNameId = (Integer) DataType.INTEGER.typeCast(expectedBncAdjudicationApplRequest.getValue(0, "REQUESTEDCREDITBUREAUNAMEID"));
    	BncAdjudicationApplRequestPK bncAdjudicationApplRequestPK = new BncAdjudicationApplRequestPK(id,copyId,applicantNumber);
    	RequestPK requestPK = new RequestPK(id,copyId);
    	BorrowerPK borrowerPK = new BorrowerPK(id,copyId);
    	srk.getExpressState().setDealInstitutionId(1);
    	bncAdjudicationApplRequest.createWithAdjudicationApplRequest(bncAdjudicationApplRequestPK, requestPK, borrowerPK, applicantNumber, appCurrInd, requestedCreditBureauNameId);
    	assertNull(bncAdjudicationApplRequest);
    	srk.rollbackTransaction();
    	
	}*/
	
}