/*
 * @(#)CreditREferenceTest.java    2005-3-16
 *
 */

package com.basis100.deal.entity;

import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.CreditReferencePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;

/**
 * CreditReferenceTest -
 * 
 * @version 1.0 2005-3-16
 * @author <A HREF="mailto:chyxiang@yahoo.com">Chen Xiang (Sean)</A>
 */
/**
 * @author JDai
 * 
 */
public class CreditReferenceTest extends ExpressEntityTestCase {

    // The logger
    private static Log _log = LogFactory.getLog(CreditReferenceTest.class);

    // The session resource kit
    private SessionResourceKit _srk;

    // The credit reference entity
    private CreditReference _creditReferenceDAO;

    // institutionId for deal
    private int dealInstitutionId;

    /**
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {

        // init resource
        ResourceManager.init();
        _srk = new SessionResourceKit("Testing");

        // get institutionId
        dealInstitutionId = _dataRepository.getInt("CreditReference",
                "InstitutionProfileId", 0);
        _creditReferenceDAO = new CreditReference(_srk);

    }

    /**
     * test get by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // set vpd institutionid
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // get values from database
        int creditId = _dataRepository.getInt("CreditReference",
                "CreditReferenceId", 0);
        int copyId = _dataRepository.getInt("CreditReference", "CopyId", 0);
        String desc = _dataRepository.getString("CreditReference",
                "CreditReferenceDescription", 0);

        // find credit reference by primary key
        CreditReference creditRef = _creditReferenceDAO
                .findByPrimaryKey(new CreditReferencePK(creditId, copyId));

        // verify result
        assertEquals(desc, creditRef.getCreditReferenceDescription());

        _log.info("Found CreditReference: ");
        _log.info("      Credit Ref Desc: "
                + creditRef.getCreditReferenceDescription());
        _log.info("        Response RoleId: " + creditRef.getCreditRefTypeId());
        _log.info("      Condition Type ID: " + creditRef.getAccountNumber());
        _log.info("====================================");

    }

    /**
     * test find by borrower
     */
    @Test
    public void testFindByBorrower() throws Exception {
        
        // set vpd context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        
        // read values from database
        int borrowerId = _dataRepository.getInt("CreditReference",
                "BorrowerId", 0);
        int copyId = _dataRepository.getInt("CreditReference", "CopyId", 0);
        
        // find a credit reference by borrower
        Collection creditRefs = _creditReferenceDAO
                .findByBorrower(new BorrowerPK(borrowerId, copyId));
        
        for (Iterator i = creditRefs.iterator(); i.hasNext();) {
            CreditReference creditRef = (CreditReference) i.next();
            
            //verify result
            assertEquals(borrowerId, creditRef.getBorrowerId());
            _log.info("Found CreditReference: ");
            _log.info("      Credit Ref Desc: "
                    + creditRef.getCreditReferenceDescription());
            _log.info("        Response RoleId: "
                    + creditRef.getCreditRefTypeId());
            _log.info("      Condition Type ID: "
                    + creditRef.getAccountNumber());
            _log.info("====================================");
        }
    }

    /*
     * test create
     */
    @Test
    public void testCreate() throws Exception {
        
        // set vpd context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        
        // get values from database
        int borrowerId = _dataRepository.getInt("Borrower", "BorrowerId", 0);
        int copyId = _dataRepository.getInt("Borrower", "CopyId", 0);
        
        // credit a credit reference
        CreditReference creditRef = _creditReferenceDAO.create(new BorrowerPK(
                borrowerId, copyId));
        
        // verify result
        assertEquals(borrowerId, creditRef.getBorrowerId());
        _log.info("Create CreditReference: ");
        _log.info("      Credit Ref Id: " + creditRef.getCreditReferenceId());
        _log.info("       Response RoleId: " + creditRef.getBorrowerId());
        _log.info("     Condition Type ID: " + creditRef.getCopyId());
        _log.info("====================================");
    }
    
    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        //free the srk
        _srk.freeResources();
    }
}
