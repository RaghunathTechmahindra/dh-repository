package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;

public class DBIncomeTypeTest extends FXDBTestCase{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(DBIncomeTypeTest.class);
    
    //Initial data to be loaded before each test case.
	private final static String INIT_XML_DATA = "DBIncomeTypeDataSet.xml";
	private IDataSet dataSetTest;
	
	/**
     * Constructor function
     */
    public DBIncomeTypeTest(String name) throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource("DBIncomeTypeDataSetTest.xml"));
    }
   
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
        return DatabaseOperation.INSERT;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.DELETE;
    }
    
    /**
     * test find first matched.
     */
    @Test
    public void testFindFirstMatched() throws Exception {

    	_logger.info(" Start testFindFirstMatched");
    	 
    	 ITable testFindFirstMatched = dataSetTest.getTable("testFindFirstMatched");
    	String  whereClause = (String)testFindFirstMatched.getValue(0,"whereClause");
    	String  incTypeDesc = (String)testFindFirstMatched.getValue(0,"incTypeDesc");
    	int  incTypeId = Integer.parseInt((String)testFindFirstMatched.getValue(0,"incTypeId"));
  		int  instProfId = Integer.parseInt((String)testFindFirstMatched.getValue(0,"instProfId"));
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
        
        IncomeType incomeType = new IncomeType(srk);
        incomeType = incomeType.findFirstMatched(whereClause);
        
        // verify the result.
        assert incomeType != null;
        assertEquals(incTypeId, incomeType.getIncomeTypeId());
        assertEquals(incTypeDesc, incomeType.getITDescription());
        
        _logger.info(" End testFindFirstMatched");
    	
    }
    
    /**
     * test create.
     */
    /*@Test
    public void testCreate() throws Exception {

    	_logger.info(" Start testCreate");
    	 
    	 ITable testCreate = dataSetTest.getTable("testCreate");
    	String  incTypeDesc = (String)testCreate.getValue(0,"incTypeDesc");
    	String  erel = (String)testCreate.getValue(0,"erel");
    	int  incTypeId = Integer.parseInt((String)testCreate.getValue(0,"incTypeId"));
    	int  incTypeId1 = Integer.parseInt((String)testCreate.getValue(0,"incTypeId1"));
    	double  gdsInc = Double.parseDouble((String)testCreate.getValue(0,"gdsInc"));
    	double  tdsInc = Double.parseDouble((String)testCreate.getValue(0,"gdsInc"));
  		 
    	srk.beginTransaction();
        IncomeType incomeType = new IncomeType(srk, incTypeId1);
        
        incomeType = incomeType.create(incTypeId, incTypeDesc, erel, gdsInc, tdsInc);
        
        IncomeType foundEntity = new IncomeType(srk, incTypeId);
        foundEntity = foundEntity.findByPrimaryKey(new IncomeTypePK(incomeType.getIncomeTypeId()));

        // verify the result.
        assert incomeType != null;
        assertEquals(foundEntity.getIncomeTypeId(), incomeType.getIncomeTypeId());
        
        srk.rollbackTransaction();
        
        _logger.info(" End testCreate");
    	
    }
*/

}
