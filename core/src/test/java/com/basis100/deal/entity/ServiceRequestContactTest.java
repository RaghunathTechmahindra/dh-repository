/**
 * <p>@(#)ServiceRequestContactTest.java Sep 11, 2007</p>
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.basis100.deal.entity;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.ServiceRequestContactPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>ServiceRequestContactTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class ServiceRequestContactTest extends ExpressEntityTestCase 
    implements UnitTestLogging {

    private static final Logger _logger = 
        LoggerFactory.getLogger(ResponseTest.class);
    private static final String ENTITY_NAME = "SERVICEREQUESTCONTACT"; 
  
    // retreived data count that is stored in UnitTestDataRepository.xml
    private static final int DATA_COUNT = 20;

    private int testSeqForGen = 0;

    private SessionResourceKit _srk;
    
    // properties
    protected int     serviceRequestContactId;
    protected int     requestId;
    protected String nameFirst;
    protected String nameLast;
    protected String emailAddress;
    protected String workAreaCode;
    protected String workPhoneNumber;
    protected String workExtension;
    protected String cellAreaCode;
    protected String cellPhoneNumber;
    protected String homeAreaCode;
    protected String homePhoneNumber;
    protected String propertyOwnerName;
    protected String comments;

    protected boolean haveAssoc;
    protected int borrowerId;
    protected int copyId;
    protected int instProfId;
    
    /**
     * <p>setUp</p>
     * Setting up test case.
     */
    @Before
    public void setUp() throws Exception {
      
        _logger.info("**** " + ENTITY_NAME + " TestCase setting up ...");
        
        // get random seqence number
        Double indexD = new Double(Math.random() * DATA_COUNT);
        testSeqForGen = indexD.intValue();
        
        //      get test data from repository
        requestId = _dataRepository.getInt(ENTITY_NAME, "REQUESTID", testSeqForGen);
        serviceRequestContactId = _dataRepository.getInt(ENTITY_NAME, 
                                                         "SERVICEREQUESTCONTACTID", 
                                                         testSeqForGen);
        nameFirst = _dataRepository.getString(ENTITY_NAME, 
                                              "NAMEFIRST", 
                                              testSeqForGen);
        nameLast = _dataRepository.getString(ENTITY_NAME,  
                                             "NAMELAST", 
                                             testSeqForGen);
        emailAddress = _dataRepository.getString(ENTITY_NAME, 
                                                 "EMAILADDRESS", 
                                                 testSeqForGen);
        workAreaCode = _dataRepository.getString(ENTITY_NAME,  
                                                 "WORKAREACODE", 
                                                 testSeqForGen);
        workPhoneNumber = _dataRepository.getString(ENTITY_NAME,  
                                                    "WORKPHONENUMBER", 
                                                    testSeqForGen);
        workExtension = _dataRepository.getString(ENTITY_NAME,  
                                                  "WORKEXTENSION",
                                                  testSeqForGen);
        cellAreaCode = _dataRepository.getString(ENTITY_NAME,  
                                                 "CELLAREACODE", 
                                                 testSeqForGen);
        cellPhoneNumber = _dataRepository.getString(ENTITY_NAME,  
                                                    "CELLPHONENUMBER", 
                                                    testSeqForGen);
        homeAreaCode = _dataRepository.getString(ENTITY_NAME,  
                                                 "HOMEAREACODE", 
                                                 testSeqForGen);
        homePhoneNumber = _dataRepository.getString(ENTITY_NAME,  
                                                 "HOMEPHONENUMBER", 
                                                 testSeqForGen);
        propertyOwnerName = _dataRepository.getString(ENTITY_NAME,  
                                                      "PROPERTYOWNERNAME", 
                                                      testSeqForGen);
        comments = _dataRepository.getString(ENTITY_NAME,  
                                             "COMMENTS", 
                                             testSeqForGen);
        borrowerId = _dataRepository.getInt(ENTITY_NAME,  
                                            "BORROWERID", 
                                            testSeqForGen);
        copyId = _dataRepository.getInt(ENTITY_NAME, 
                                        "COPYID", 
                                        testSeqForGen);
        instProfId = _dataRepository.getInt(ENTITY_NAME, 
                                            "INSTITUTIONPROFILEID", 
                                            testSeqForGen);
        
        // init ResouceManager
        ResourceManager.init();
        //  init session resource kit.
        _srk = new SessionResourceKit();

        _logger.info(BORDER_END, "Setting up Done");
    }
    
    @After
    public void tearDown() {

      //      free resources
      _srk.freeResources( );

    }
  
    /**
     * test findByPrimaryKey.
     * Test DB data is empaty
     */
    @Ignore @Test
    public void testFindByPrimaryKey()  throws Exception {
      
        _logger.info(BORDER_START, "findByPrimaryKey");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        ServiceRequestContact contact = new ServiceRequestContact(_srk);
        contact 
            = contact.findByPrimaryKey(new ServiceRequestContactPK(serviceRequestContactId,
                                                                   requestId,
                                                                   copyId));
        
        assertEqualsProperties(contact);

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * test findByBorrowerIdAndCopyId.
     * Test DB data is empaty
     */
    @Ignore @Test
    public void findByBorrowerIdAndCopyId()  throws Exception {
      
        _logger.info(BORDER_START, "findByBorrowerIdAndCopyId");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        ServiceRequestContact contact = new ServiceRequestContact(_srk);
        contact 
            = contact.findByBorrowerIdAndCopyId(borrowerId, copyId);
        
        assertEqualsProperties(contact);

        _logger.info(BORDER_END, "findByBorrowerIdAndCopyId");
    }

    /**
     * test findLastByRequestId.
     * Test DB data is empaty
     */
    @Ignore @Test
    public void findLastByRequestId()  throws Exception {
      
        _logger.info(BORDER_START, "findLastByRequestId");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        ServiceRequestContact contact = new ServiceRequestContact(_srk);
        contact 
            = contact.findLastByRequestId(requestId);
        
        assertEqualsProperties(contact);

        _logger.info(BORDER_END, "findLastByRequestId");
    }

    /**
     * test create.
     */
    @Ignore @Test
    public void testCreate() throws Exception {
      
        _logger.info(BORDER_START, "create");
                
        // excute method
        // 1. get request info from Test data
        int requestDealId = _dataRepository.getInt("REQUEST",  "DEALID", 0);
        int requestCopyId = _dataRepository.getInt("REQUEST",  "COPYID", 0);
        int requestStatusId = _dataRepository.getInt("REQUEST",  "REQUESTSTATUSID", 0);
        int requestUserProfileId = _dataRepository.getInt("REQUEST",  "USERPROFILEID", 0);
        int reqeustInstId = _dataRepository.getInt("REQUEST",  "INSTITUTIONPROFILEID", 0);

        //  set VPD
        _srk.getExpressState().setDealInstitutionId(reqeustInstId);

        // set beginTransaction for auto commit off
        _srk.beginTransaction();

        // 2. create Request
        Date now = new Date();
        Request request = new Request(_srk);
        request = request.create(new DealPK(requestDealId, requestCopyId), requestStatusId, now, requestUserProfileId);

        // 3. create ServiceRequestContact
        ServiceRequestContact entity = new ServiceRequestContact(_srk);
        entity = entity.create(request.getRequestId(), requestCopyId);
        assertEquals(entity.getInstitutionProfileId(), reqeustInstId);
        assertEquals(entity.getRequestId(), request.getRequestId());
        assertEquals(entity.getCopyId(), requestCopyId);
        assertEquals(entity.getInstitutionProfileId(), reqeustInstId);

        // rollback
        _srk.rollbackTransaction();
        
        _logger.info(BORDER_END, "create");
    }

    
    /**
     * assertEqualProperties
     * 
     * check assertEquals for all properties of ServiceRequestContact with test Data
     * 
     * @param entity: ServiceRequestContact
     * @throws Exception
     */
    private void assertEqualsProperties(ServiceRequestContact entity) throws Exception
    {
        assertEquals(entity.getServiceRequestContactId(), serviceRequestContactId);
        assertEquals(entity.getRequestId(), requestId);
        assertEquals(entity.getFirstName(), nameFirst);
        assertEquals(entity.getLastName(), nameLast);
        assertEquals(entity.getEmailAddress(), emailAddress);
        assertEquals(entity.getWorkAreaCode(), workAreaCode);
        assertEquals(entity.getWorkPhoneNumber(), workPhoneNumber);
        assertEquals(entity.getWorkExtension(), workExtension);
        assertEquals(entity.getCellAreaCode(), cellAreaCode);
        assertEquals(entity.getCellPhoneNumber(), cellPhoneNumber);
        assertEquals(entity.getHomeAreaCode(), homeAreaCode);
        assertEquals(entity.getHomePhoneNumber(), homePhoneNumber);
        assertEquals(entity.getPropertyOwnerName(), propertyOwnerName);
        assertEquals(entity.getComments(), comments);
        assertEquals(entity.getBorrowerId(), borrowerId);
        assertEquals(entity.getCopyId(), copyId);
        assertEquals(entity.getInstitutionProfileId(), instProfId);
    }
}