package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.DocumentProfilePK;

public class DocumentProfileTest1 extends  FXDBTestCase{

	private IDataSet dataSetTest;
	DocumentProfile documentProfile=null;
	public DocumentProfileTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("DocumentProfileDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource("DocumentProfileDataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		documentProfile=new DocumentProfile(srk);
		
	}
	public void testFindByDocumentTypeId() throws Exception{
		ITable testFindByDocumentTypeId = dataSetTest.getTable("testFindByDocumentTypeId");
		int  docTypeId = Integer.parseInt(testFindByDocumentTypeId.getValue(0,"docTypeId").toString());	
		DocumentProfile documentProfile1=documentProfile.findByDocumentTypeId(docTypeId);
		assertNotNull(documentProfile1);
	}
	
	public void testFindByDocumentTypeIdFailure() throws Exception{
		ITable testFindByDocumentTypeId = dataSetTest.getTable("testFindByDocumentTypeIdFailure");
		int  docTypeId = Integer.parseInt(testFindByDocumentTypeId.getValue(0,"docTypeId").toString());	
		DocumentProfile documentProfile1=documentProfile.findByDocumentTypeId(docTypeId);
		assertNull(documentProfile1);
	}

	public void testFindByDocumentTypeIdAndLanguageId() throws Exception{
		ITable testFindByDocumentTypeIdAndLanguageId = dataSetTest.getTable("testFindByDocumentTypeIdAndLanguageId");
		int  docTypeId = Integer.parseInt(testFindByDocumentTypeIdAndLanguageId.getValue(0,"docTypeId").toString());	
		int languageId = Integer.parseInt(testFindByDocumentTypeIdAndLanguageId.getValue(0,"languageId").toString());
		DocumentProfile documentProfile1=documentProfile.findByDocumentTypeIdAndLanguageId(docTypeId,languageId);
		assertNotNull(documentProfile1);
	}
	public void testFindByDocumentTypeIdAndLanguageIdFailure() throws Exception{
		ITable testFindByDocumentTypeIdAndLanguageId = dataSetTest.getTable("testFindByDocumentTypeIdAndLanguageIdFailure");
		int  docTypeId = Integer.parseInt(testFindByDocumentTypeIdAndLanguageId.getValue(0,"docTypeId").toString());	
		int languageId = Integer.parseInt(testFindByDocumentTypeIdAndLanguageId.getValue(0,"languageId").toString());
		DocumentProfile documentProfile1=documentProfile.findByDocumentTypeIdAndLanguageId(docTypeId,languageId);
		assertNull(documentProfile1);
	}
	public void testFindByClassName() throws Exception{
		ITable testFindByClassName = dataSetTest.getTable("testFindByClassName");
		String  className = testFindByClassName.getValue(0,"className").toString();	
		int lang = Integer.parseInt(testFindByClassName.getValue(0,"lang").toString());
		DocumentProfile documentProfile1=documentProfile.findByClassName(className,lang);
		assertNotNull(documentProfile1);
	}
	public void testFindByClassNameFailure() throws Exception{
		ITable testFindByClassName = dataSetTest.getTable("testFindByClassNameFailure");
		String  className = testFindByClassName.getValue(0,"className").toString();	
		int lang = Integer.parseInt(testFindByClassName.getValue(0,"lang").toString());
		DocumentProfile documentProfile1=documentProfile.findByClassName(className,lang);
		assertNull(documentProfile1);
	}
		//create(DocumentProfilePK)

	/*public void testCreate() throws Exception{
	
		ITable testFindByClassName = dataSetTest.getTable("testCreate");
		int dealInstitutionId = Integer.parseInt(testFindByClassName.getValue(0,"dealInstitutionId").toString());
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		String  version = testFindByClassName.getValue(0,"version").toString();	
		int language = Integer.parseInt(testFindByClassName.getValue(0,"language").toString());
		int lender = Integer.parseInt(testFindByClassName.getValue(0,"lender").toString());
		int type = Integer.parseInt(testFindByClassName.getValue(0,"type").toString());
		String  format = testFindByClassName.getValue(0,"format").toString();	
		DocumentProfilePK documentProfilePK=new DocumentProfilePK(version,language,lender,type,format);
		srk.beginTransaction();
		DocumentProfile documentProfile1=documentProfile.create(documentProfilePK);
		assertNotNull(documentProfile1);
		srk.rollbackTransaction();
	}
	*/
	public void testCreateFailure() throws Exception{
		boolean status=false;
		try{
		ITable testFindByClassName = dataSetTest.getTable("testCreateFailure");
		int dealInstitutionId = Integer.parseInt(testFindByClassName.getValue(0,"dealInstitutionId").toString());
		String  version = testFindByClassName.getValue(0,"version").toString();	
		int language = Integer.parseInt(testFindByClassName.getValue(0,"language").toString());
		int lender = Integer.parseInt(testFindByClassName.getValue(0,"lender").toString());
		int type = Integer.parseInt(testFindByClassName.getValue(0,"type").toString());
		String  format = testFindByClassName.getValue(0,"format").toString();	
		DocumentProfilePK documentProfilePK=new DocumentProfilePK(version,language,lender,type,format);
		DocumentProfile documentProfile1=documentProfile.create(documentProfilePK);
		}catch (Exception e) {
			// TODO: handle exception
			status=false;
		}
		assertSame(false, status);
	}

}
