package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.MIDistributionHistoryPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;

public class MIDistributionHistoryTest1 extends FXDBTestCase{
	private String INIT_XML_DATA = "MIDistributionHistoryDataSet.xml";
	private MIDistributionHistory miDistributionHistory;
	private IDataSet dataSetTest;

	public MIDistributionHistoryTest1(String name) throws DataSetException,
			IOException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"MIDistributionHistoryDataSetTest.xml"));
	}

	
	@Override
	protected void setUp() throws Exception {
		super.setUp();

		miDistributionHistory = new MIDistributionHistory(srk);

	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}
	public void testCreate() throws JdbcTransactionException, RemoteException, CreateException, NumberFormatException, DataSetException{
		srk.beginTransaction();
		
		int institutionProfileId = Integer.parseInt(dataSetTest
				.getTable("testCreate")
				.getValue(0, "institutionProfileId").toString());
		int dealId = Integer.parseInt(dataSetTest
				.getTable("testCreate")
				.getValue(0, "dealId").toString());
		int mortgageInsurerId = Integer.parseInt(dataSetTest
				.getTable("testCreate")
				.getValue(0, "mortgageInsurerId").toString());
		MIDistributionHistory distributionHistory=miDistributionHistory.create(institutionProfileId, dealId, mortgageInsurerId);
		assertNotNull(distributionHistory);
		assertEquals(mortgageInsurerId, distributionHistory.getMortgageInsurerId());
		srk.rollbackTransaction();
	}
	public void testCreatePrimaryKey() throws JdbcTransactionException, CreateException{
		srk.beginTransaction();
		MIDistributionHistoryPK miDistributionHistoryPK=miDistributionHistory.createPrimaryKey();
		assertNotNull(miDistributionHistoryPK);
		srk.rollbackTransaction();
	}
}
