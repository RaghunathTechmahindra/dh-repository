package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.ResponsePK;


/**
 * <p>SAMResponseDbTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class SAMResponseDbTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private SAMResponse sAMResponse;
	
	public SAMResponseDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(SAMResponse.class.getSimpleName() + "DataSetTest.xml"));
	}

	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.sAMResponse = new SAMResponse(srk);
		
	}
	
	/*public void testCreate() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreate");
    	int responseId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "RESPONSEID"));
    	srk.beginTransaction();
    	sAMResponse.create(new ResponsePK(responseId));
    	srk.rollbackTransaction();
	    assertNotNull(sAMResponse);
    }
	
	public void testFindByResponse() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByResponse");
    	int responseId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "RESPONSEID"));
    	
    	sAMResponse.findByResponse(new ResponsePK(responseId));
	    assertNotNull(sAMResponse);
    }
	
	public void testFindByResponse1() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByResponse1");
    	int responseId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "RESPONSEID"));
    	
    	sAMResponse.findByResponseId(responseId);
	    assertNotNull(sAMResponse);
    }
	
	public void testFindByDeal() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByDeal");
    	int responseId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "RESPONSEID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	
    	sAMResponse.findByDeal(new DealPK(responseId,copyId));
	    assertNotNull(sAMResponse);
    }
	
	public void testClone() throws Exception {
    	ITable expected = dataSetTest.getTable("testClone");
    	//int responseId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "RESPONSEID"));
    	
    	sAMResponse.clone();
	    assertNotNull(sAMResponse);
    }*/
	
	public void testClone1() throws Exception {
    	ITable expected = dataSetTest.getTable("testClone1");
    	int responseId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "RESPONSEID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	
    	sAMResponse.clone(new ResponsePK(responseId));
	    assertNotNull(sAMResponse);
    }
	
	/*public void testRemoveSons() throws Exception {
    	ITable expected = dataSetTest.getTable("testRemoveSons");
    	srk.beginTransaction();
    	sAMResponse.removeSons();
    	srk.rollbackTransaction();
	    assertNotNull(sAMResponse);
    }*/
	
}