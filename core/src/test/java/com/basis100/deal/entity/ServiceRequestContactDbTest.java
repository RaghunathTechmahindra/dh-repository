package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.pk.ServiceRequestContactPK;

/**
 * <p>ServiceRequestContactDbTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class ServiceRequestContactDbTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private ServiceRequestContact serviceRequestContact;
	
	public ServiceRequestContactDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ServiceRequestContact.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		 int id = 207;
		 int requestId = 9556;
		 int copyId = 1;
		this.serviceRequestContact = new ServiceRequestContact(srk);
		
	}
	
	
	public void testCreatePrimaryKey() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreatePrimaryKey");
    	int requestId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "REQUESTID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	ServiceRequestContactPK pk =null;
    	srk.beginTransaction();
    	pk = serviceRequestContact.createPrimaryKey(requestId, copyId);
    	srk.rollbackTransaction();
	    assertNotNull(pk);
    }
	
	public void testCreate() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreate");
    	int requestId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "REQUESTID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	ServiceRequestContact serviceRequestContact1 = null;
    	srk.beginTransaction();
    	serviceRequestContact1 = serviceRequestContact.create(requestId, copyId);
    	srk.rollbackTransaction();
	    assertNotNull(serviceRequestContact1);
    }
   
	
	public void testCreate1() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreate1");
    	int borrowerId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "BORROWERID"));
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "SERVICEREQUESTCONTACTID"));
    	int requestId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "REQUESTID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	srk.beginTransaction();
    	serviceRequestContact.create(new ServiceRequestContactPK(id,requestId,copyId), borrowerId, copyId);
	    srk.rollbackTransaction();
    	assertNotNull(serviceRequestContact);
    }
     
		
	
	public void testCreateWithBorrowerAssoc() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreateWithBorrowerAssoc");
    	int requestId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "REQUESTID"));
    	int borrowerId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "BORROWERID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	srk.beginTransaction();
    	serviceRequestContact.createWithBorrowerAssoc(requestId, borrowerId, copyId);
    	srk.rollbackTransaction();
	    assertNotNull(serviceRequestContact);
    }
	
	
	public void testFindByRequest() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByRequest");
    	int requestId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "REQUESTID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	
    	serviceRequestContact.findByRequest(new RequestPK(requestId,copyId));
	    assertNotNull(serviceRequestContact);
    }
	
	public void testCutBorrowerAssoc() throws Exception {
    	ITable expected = dataSetTest.getTable("testCutBorrowerAssoc");
    	int requestId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "REQUESTID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	serviceRequestContact.requestId = requestId;
    	serviceRequestContact.copyId = copyId;
    	srk.beginTransaction();
    	serviceRequestContact.cutBorrowerAssoc();
    	srk.rollbackTransaction();
	    assertNotNull(serviceRequestContact);
    }
}