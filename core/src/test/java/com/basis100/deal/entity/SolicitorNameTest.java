package com.basis100.deal.entity;

import java.io.IOException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.docprep.extract.doctag.RefExistingMtgNumber;
import com.basis100.deal.docprep.extract.doctag.Refinance;
import com.basis100.deal.docprep.extract.doctag.SolicitorName;
import com.basis100.resources.SessionResourceKit;

public class SolicitorNameTest extends FXDBTestCase {
	private IDataSet dataSetTest;
	private SolicitorName solicitorName = null;
    CalcMonitor dcm =null;
    FMLQueryResult fmlQueryResult =null;
    SessionResourceKit srk= new SessionResourceKit();
	
	public SolicitorNameTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(SolicitorName.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.solicitorName = new SolicitorName();
	}
	
	public void testExtract() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testExtract");
		
		int institutionProfileId = Integer.parseInt((String)testExtract.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String)testExtract.getValue(0, "dealid"));
		int copyid = Integer.parseInt((String)testExtract.getValue(0, "copyid"));
		
		Deal deal = new Deal(srk, null, id, copyid);
		com.basis100.deal.entity.DealEntity de = (com.basis100.deal.entity.DealEntity )deal;
		deal.setInstitutionProfileId(institutionProfileId);
		
		int lang = 0;
		fmlQueryResult = solicitorName.extract(de, lang, null, srk);
		
		assertNotNull(fmlQueryResult);
		assert fmlQueryResult.getValues().size()>0;
		
		srk.rollbackTransaction();
	}
	
}
