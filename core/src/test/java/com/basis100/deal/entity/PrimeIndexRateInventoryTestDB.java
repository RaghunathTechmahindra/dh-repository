package com.basis100.deal.entity;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.pk.PrimeIndexRateInventoryPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;

public class PrimeIndexRateInventoryTestDB extends FXDBTestCase{

	private IDataSet dataSetTest;
	private PrimeIndexRateInventory primeIndexRateInventory;	
	private DealEntity entity;
	public PrimeIndexRateInventoryTestDB(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(PrimeIndexRateInventory.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(PrimeIndexRateInventory.class.getSimpleName() + "DataSet.xml"));
	}
	
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	
	@Override
	protected void setUp() throws Exception {
		super.setUp();		
		primeIndexRateInventory = new PrimeIndexRateInventory(srk);
	}	
	
	public void testFindByPrimeIndexRateProfileIdSuccess() throws Exception{
		try {
			Collection cList=null;
			srk.beginTransaction();
			ITable testPrimeIndexRate = dataSetTest.getTable("testFindByPrimeIndexRateProfileIdSuccess");					
			int primeIndexRateProfileId=Integer.valueOf((String)testPrimeIndexRate.getValue(0,"PRIMEINDEXRATEPROFILEID"));	
			srk.getExpressState().setDealInstitutionId(1);
			cList=primeIndexRateInventory.findByPrimeIndexRateProfileId(primeIndexRateProfileId);
			if(cList!=null){
		    assertNotNull(cList);	
			}			
		    srk.rollbackTransaction();
		    
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
		public void testFindByPrimeIndexRateProfileIdFailure() throws Exception{
			try {
				Collection cList=null;
				srk.beginTransaction();
				ITable testPrimeIndexRate = dataSetTest.getTable("testFindByPrimeIndexRateProfileIdFailure");					
				int primeIndexRateProfileId=Integer.valueOf((String)testPrimeIndexRate.getValue(0,"PRIMEINDEXRATEPROFILEID"));	
				srk.getExpressState().setDealInstitutionId(1);
				cList=primeIndexRateInventory.findByPrimeIndexRateProfileId(primeIndexRateProfileId);
				if(cList!=null){
			    assertEquals(0, cList.size());	
				}			
			    srk.rollbackTransaction();
			    
			} catch (JdbcTransactionException e) {			
				e.printStackTrace();			
			}
	}
	
		public void testCreatePrimaryKeySuccess() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, SQLException, CloneNotSupportedException, CreateException{
			try {
				srk.beginTransaction();
				ITable testCreatePrimaryKey = dataSetTest.getTable("testCreatePrimaryKeySuccess");					
				int primeIndexRateInventoryId=Integer.valueOf((String)testCreatePrimaryKey.getValue(0,"PRIMEINDEXRATEINVENTORYID"));
				srk.getExpressState().setDealInstitutionId(1);
				PrimeIndexRateInventoryPK pk=new PrimeIndexRateInventoryPK(primeIndexRateInventoryId);				
				pk=primeIndexRateInventory.createPrimaryKey();
			    assertNotNull(pk);			   
			    srk.rollbackTransaction();
			} catch (JdbcTransactionException e) {			
				e.printStackTrace();			
			}
		}
		
		public void testCreatePrimaryKeyFailure() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, SQLException, CloneNotSupportedException, CreateException{
			try {
				srk.beginTransaction();
				ITable testCreatePrimaryKey = dataSetTest.getTable("testCreatePrimaryKeyFailure");					
				int primeIndexRateInventoryId=Integer.valueOf((String)testCreatePrimaryKey.getValue(0,"PRIMEINDEXRATEINVENTORYID"));			
				PrimeIndexRateInventoryPK pk=new PrimeIndexRateInventoryPK(primeIndexRateInventoryId);				
				pk=primeIndexRateInventory.createPrimaryKey();
			    assertNotNull(pk);
			    srk.rollbackTransaction();
			} catch (JdbcTransactionException e) {			
				e.printStackTrace();			
			}
		}
}
