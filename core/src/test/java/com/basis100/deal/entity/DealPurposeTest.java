/*
 * @(#) DealPurposeTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */


package com.basis100.deal.entity;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.DealPurposePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>DealPurposeTest</p>
 * Express Entity class unit test: DealPurpose
 */
public class DealPurposeTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(DealPurposeTest.class);

    /**
     * Constructor function
     */
    public DealPurposeTest() {
    }

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int dealPurposeId = _dataRepository.getInt("DEALPURPOSE", "DEALPURPOSEID", 0);
        String dpDescription = _dataRepository.getString("DEALPURPOSE", "DPDESCRIPTION", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed the input data and run the program.
        DealPurpose entity = new DealPurpose(srk);
        DealPurposePK dpk = new DealPurposePK(dealPurposeId);
        entity = entity.findByPrimaryKey(dpk);

        // verify the running result.
        assertEquals(dealPurposeId, entity.getDealPurposeId());
        assertEquals(dpDescription, entity.getDpDescription());

        // free resources
        srk.freeResources();
    }

    /**
     * There is not create function in DealPurpose class.
     */

}
