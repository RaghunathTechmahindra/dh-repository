package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.PropertyPK;

/**
 * <p>ServiceRequestDbTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class ServiceRequestDbTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private ServiceRequest serviceRequest;
	
	public ServiceRequestDbTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ServiceRequest.class.getSimpleName() + "DataSetTest.xml"));
	}


	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.serviceRequest = new ServiceRequest(srk);
		
	}
	
	public void testCreate() throws Exception {
    	ITable expected = dataSetTest.getTable("testCreate");
    	int rid = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "REQUESTID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	srk.beginTransaction();
    	serviceRequest.create(rid, copyId);
    	srk.rollbackTransaction();
	    assertNotNull(serviceRequest);
    }
	
	public void testFindByPropertyIdAndCopyId() throws Exception {
    	ITable expected = dataSetTest.getTable("testFindByPropertyIdAndCopyId");
    	int id = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "PROPERTYID"));
    	int copyId = (Integer) DataType.INTEGER.typeCast(expected.getValue(0, "COPYID"));
    	
    	serviceRequest.findByPropertyIdAndCopyId(new PropertyPK(id,copyId));
	    assertNotNull(serviceRequest);
    }
	
}