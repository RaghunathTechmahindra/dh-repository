/**
 * <p>@(#)SourceOfBusinessProfileTest.java Sep 10, 2007</p>
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.basis100.deal.entity;

import java.util.Collection;
import java.util.Iterator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.basis100.deal.pk.SourceFirmProfilePK;
import com.basis100.deal.pk.SourceOfBusinessProfilePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>SourceOfBusinessProfileTest</p>
 * Express Entity class unit test: ServiceProvider
 */
public class SourceOfBusinessProfileTest extends ExpressEntityTestCase 
    implements UnitTestLogging {

    private static final Logger _logger = 
        LoggerFactory.getLogger(ResponseTest.class);
    public static final String ENTITY_NAME = "SOURCEOFBUSINESSPROFILE"; 
  
    // retreived data count that is stored in UnitTestDataRepository.xml
    public static final int DATA_COUNT = 300;

    private int testSeqForGen = 0;

    private SessionResourceKit _srk;
    
    // properties
    protected int      sourceOfBusinessProfileId=6000;
    protected int      lengthOfService=0;
    protected int      contactId=69053;
    protected int      profileStatusId=3;
    protected int      tierLevel=0;
    protected double   compensationFactor=0;
    protected String   SOBPShortName="BDyck";
    protected String   SOBPBusinessId="";
    protected int      sourceOfBusinessCategoryId=0;
    protected String   alternativeId="";
    protected int      partyProfileId=0;
    protected String   notes="";
    protected int      SOBRegionId=0;
    protected int      sourceFirmProfileId=1644;
    protected int      systemTypeId=1;
    protected String   sourceOfBusinessCode="BDKB001B001";
    protected int      sourceOfBusinessPriorityId=0;
    protected int      instProfId=0;

    /**
     * <p>setUp</p>
     * Setting up test case.
     */
    @Before
    public void setUp() throws Exception {
      
        _logger.info(BORDER_START, ENTITY_NAME + " TestCase setting up ...");
        
        // get random seqence number
        Double indexD = new Double(Math.random() * DATA_COUNT);
        testSeqForGen = indexD.intValue();
      
        //      get test data from repository
       /* sourceOfBusinessProfileId 
            = _dataRepository.getInt(ENTITY_NAME, 
                                     "SOURCEOFBUSINESSPROFILEID", 
                                     testSeqForGen);
        lengthOfService = _dataRepository.getInt(ENTITY_NAME,  
                                                 "LENGTHOFSERVICE", 
                                                 testSeqForGen);
        contactId = _dataRepository.getInt(ENTITY_NAME,  
                                           "CONTACTID", 
                                           testSeqForGen);
        profileStatusId = _dataRepository.getInt(ENTITY_NAME,  
                                                 "PROFILESTATUSID", 
                                                 testSeqForGen);
        tierLevel = _dataRepository.getInt(ENTITY_NAME, 
                                           "TIERLEVEL", 
                                           testSeqForGen);
        compensationFactor = _dataRepository.getDouble(ENTITY_NAME,  
                                                    "COMPENSATIONFACTOR", 
                                                    testSeqForGen);
        SOBPShortName = _dataRepository.getString(ENTITY_NAME, 
                                                  "SOBPSHORTNAME", 
                                                  testSeqForGen);
        SOBPBusinessId = _dataRepository.getString(ENTITY_NAME,  
                                                   "SOBPBUSINESSID", 
                                                   testSeqForGen);
        sourceOfBusinessCategoryId 
            = _dataRepository.getInt(ENTITY_NAME, 
                                     "SOURCEOFBUSINESSCATEGORYID", 
                                     testSeqForGen);
        alternativeId = _dataRepository.getString(ENTITY_NAME,  
                                                  "ALTERNATIVEID",
                                                  testSeqForGen);
        partyProfileId = _dataRepository.getInt(ENTITY_NAME,  
                                                "PARTYPROFILEID", 
                                                testSeqForGen);
        notes = _dataRepository.getString(ENTITY_NAME,  
                                          "NOTES", 
                                          testSeqForGen);
        SOBRegionId = _dataRepository.getInt(ENTITY_NAME,  
                                             "SOBREGIONID", 
                                             testSeqForGen);
        sourceFirmProfileId = _dataRepository.getInt(ENTITY_NAME,  
                                                     "SOURCEFIRMPROFILEID", 
                                                     testSeqForGen);
        systemTypeId = _dataRepository.getInt(ENTITY_NAME,  
                                              "SYSTEMTYPEID", 
                                              testSeqForGen);
        sourceOfBusinessCode 
            = _dataRepository.getString(ENTITY_NAME,
                                        "SOURCEOFBUSINESSCODE", 
                                        testSeqForGen);
        sourceOfBusinessPriorityId 
            = _dataRepository.getInt(ENTITY_NAME, 
                                     "SOURCEOFBUSINESSPRIORITYID", 
                                     testSeqForGen);
        instProfId = _dataRepository.getInt(ENTITY_NAME, 
                                            "INSTITUTIONPROFILEID", 
                                            testSeqForGen);*/

        // init ResouceManager
        ResourceManager.init();
        //  init session resource kit.
        _srk = new SessionResourceKit();

        _logger.info(BORDER_END, "Setting up Done");
    }
    
    @After
    public void tearDown() {

      //      free resources
      _srk.freeResources( );

    }
    /**
     * test findByPrimaryKey.
     */
    @Test
    public void findByPrimaryKey()  throws Exception {
      
        _logger.info(BORDER_START, "findByPrimaryKey");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        SourceOfBusinessProfile sob = new SourceOfBusinessProfile(_srk);
        sob = sob.findByPrimaryKey(new SourceOfBusinessProfilePK(sourceOfBusinessProfileId));
        
       assertNotNull(sob);
        // assertEqualsProperties(sob);

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * test findBySystemTypeSFPAndSOBCode.
     */
    @Test
    public void findBySystemTypeSFPAndSOBCode()  throws Exception {
      
        _logger.info(BORDER_START, "findBySystemTypeSFPAndSOBCode");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        SourceOfBusinessProfile sob = new SourceOfBusinessProfile(_srk);
        sob  = sob.findBySystemTypeSFPAndSOBCode(systemTypeId, 
                                                 sourceFirmProfileId,
                                                 sourceOfBusinessCode);
        assertNotNull(sob);
        //assertEqualsProperties(sob);

        _logger.info(BORDER_END, "findBySystemTypeSFPAndSOBCode");
    }
    
    /**
     * test findByName.
     */
    @Test
    public void findByName()  throws Exception {
      
        _logger.info(BORDER_START, "findByName");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        SourceOfBusinessProfile sob = new SourceOfBusinessProfile(_srk);
        sob  = sob.findByName(SOBPShortName);
        
        // SOBPShotName is not uniqu
        // But this method returns only one record
        assertEquals(sob.getSOBPShortName(), SOBPShortName);

        _logger.info(BORDER_END, "findByName");
    }
    
    /**
     * test findByMailBox.
     */
    @Test
    public void findByMailBox()  throws Exception {
      
        _logger.info(BORDER_START, "findByMailBox");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        SourceOfBusinessProfile sob = new SourceOfBusinessProfile(_srk);
        sob  = sob.findByMailBox(sourceOfBusinessCode);
        
        assertEqualsPropertiesMailBox(sob);

        _logger.info(BORDER_END, "findByMailBox");
    }
    
    /**
     * test findByAssociatedSourceFirm.
     */
    @Test
    public void findByAssociatedSourceFirm()  throws Exception {
      
        _logger.info(BORDER_START, "findByAssociatedSourceFirm");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        SourceOfBusinessProfile sob = new SourceOfBusinessProfile(_srk);
        Collection sobs  
            = sob.findByAssociatedSourceFirm(new SourceFirmProfilePK (sourceFirmProfileId));
        // assertEquals for each
        Iterator sobsIter = sobs.iterator();
        for (; sobsIter.hasNext(); ) {
            SourceOfBusinessProfile aSOB 
                = (SourceOfBusinessProfile) sobsIter.next();

            // assertEquals only sourceFirmProfileId
            assertEquals(aSOB.getSourceFirmProfileId(), sourceFirmProfileId);
        }
        

        _logger.info(BORDER_END, "findByAssociatedSourceFirm");
    }
    
    
    
    /**
     * assertEqualProperties
     * 
     * check assertEquals for all properties of SourceFirmProfile with test Data
     * 
     * @param entity: SourceFirmProfile
     * @throws Exception
     */
    private void assertEqualsProperties(SourceOfBusinessProfile entity) throws Exception
    {
        assertEquals(entity.getSourceOfBusinessProfileId(), sourceOfBusinessProfileId);
        assertEquals(entity.getLengthOfService(), lengthOfService);
        assertEquals(entity.getContactId(), contactId);
        assertEquals(entity.getProfileStatusId(), profileStatusId);
        assertEquals(entity.getTierLevel(), tierLevel);
        assertEquals(entity.getCompensationFactor(), compensationFactor, Double.MIN_VALUE);
        assertEquals(entity.getSOBPShortName(), SOBPShortName);
        assertEquals(entity.getSOBPBusinessId(), SOBPBusinessId);
        assertEquals(entity.getSourceOfBusinessCategoryId(), sourceOfBusinessCategoryId);
        assertEquals(entity.getAlternativeId(), alternativeId);
        assertEquals(entity.getPartyProfileId(), partyProfileId);
        assertEquals(entity.getNotes(), notes);
        assertEquals(entity.getSOBRegionId(), SOBRegionId);
        assertEquals(entity.getSourceFirmProfileId(), sourceFirmProfileId);
        assertEquals(entity.getSystemTypeId(), systemTypeId);
        assertEquals(entity.getSourceOfBusinessCode(), sourceOfBusinessCode);
        assertEquals(entity.getSourceOfBusinessPriorityId(), sourceOfBusinessPriorityId);
        assertEquals(entity.getInstitutionProfileId(), instProfId);

    }

    
    /**
     * @param entity
     * @throws Exception
     */
    private void assertEqualsPropertiesMailBox(SourceOfBusinessProfile entity) throws Exception {
        assertEquals(entity.getInstitutionProfileId(), instProfId);
        assertEquals(entity.getSourceOfBusinessCode(), sourceOfBusinessCode);
    }
    
    
    @Test
    public void testSobLicenseRegistrationNumber() throws Exception {
      
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // excute method
        SourceOfBusinessProfile sob = new SourceOfBusinessProfile(_srk);
        sob = sob.findByPrimaryKey(new SourceOfBusinessProfilePK(sourceOfBusinessProfileId));
          
        String licenseRegistrationNumber = "test";
        sob.setSobLicenseRegistrationNumber(licenseRegistrationNumber);
        assertEquals(licenseRegistrationNumber, sob.getSobLicenseRegistrationNumber());
        
    }

}
