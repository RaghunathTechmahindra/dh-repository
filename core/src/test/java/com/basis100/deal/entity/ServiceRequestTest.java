/**
 * <p>@(#)ServiceRequestTest.java Sep 8, 2007</p>
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.basis100.deal.entity;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.pk.ServiceRequestPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>ServiceRequestTest</p>
 * Express Entity class unit test: ServiceRequest
 */
public class ServiceRequestTest extends ExpressEntityTestCase 
    implements UnitTestLogging {

    private static final Logger _logger = 
        LoggerFactory.getLogger(ResponseTest.class);
    private static final String ENTITY_NAME = "SERVICEREQUEST"; 
  
    // retreived data count that is stored in UnitTestDataRepository.xml
    private static final int DATA_COUNT = 50;

    private int testSeqForGen = 0;

    private SessionResourceKit _srk;
    
    // properties
    protected int    requestId; 
    protected String specialInstructions;
    protected String serviceProviderRefNo;
    protected int    propertyId;
    protected int    copyId;
    protected int   instProfId;
    
    /**
     * <p>setUp</p>
     * Setting up test case.
     */
    @Before
    public void setUp() throws Exception {
      
        _logger.info(BORDER_START, ENTITY_NAME + " TestCase setting up ...");
        
        // get random seqence number
//        Double indexD = new Double(Math.random() * DATA_COUNT);
//        testSeqForGen = indexD.intValue();
              
        //      get test data from repository
        requestId = _dataRepository.getInt(ENTITY_NAME,  
                                           "REQUESTID", 
                                           testSeqForGen);
        propertyId = _dataRepository.getInt(ENTITY_NAME,  
                                           "PROPERTYID", 
                                           testSeqForGen);
        copyId = _dataRepository.getInt(ENTITY_NAME,  
                                           "COPYID", 
                                           testSeqForGen);
        specialInstructions = _dataRepository.getString(ENTITY_NAME,  
                                           "SPECIALINSTRUCTIONS", 
                                           testSeqForGen);
        serviceProviderRefNo = _dataRepository.getString(ENTITY_NAME,  
                                           "SERVICEPROVIDERREFNO", 
                                           testSeqForGen);
        instProfId = _dataRepository.getInt(ENTITY_NAME,  
                                           "INSTITUTIONPROFILEID", 
                                           testSeqForGen);
        // init ResouceManager
        ResourceManager.init();
        //  init session resource kit.
        _srk = new SessionResourceKit();

        _logger.info(BORDER_END, "Setting up Done");
    }
    
    @After
    public void tearDown() {

      //      free resources
      _srk.freeResources( );

    }
  
    /**
     * test findByPrimaryKey.
     */
    @Test
    public void testFindByPrimaryKey()  throws Exception {
      
        _logger.info(BORDER_START, "findByPrimaryKey");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        ServiceRequest request = new ServiceRequest(_srk);
        request  = request.findByPrimaryKey(new ServiceRequestPK(requestId, copyId));
        
        assertEqualsProperties(request);

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * test findByPropertyId.
     */
    @Test
    public void testFindByPropertyId()  throws Exception {
      
        _logger.info(BORDER_START, "findByPropertyId");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        ServiceRequest request = new ServiceRequest(_srk);
        Collection requests  = request.findByPropertyId(propertyId);
        
        // check assertEqual only requestId
        Iterator requestIter = requests.iterator();
        for (; requestIter.hasNext(); ) {
            ServiceRequest aRequest = (ServiceRequest) requestIter.next();
            assertEquals(aRequest.getRequestId(), requestId);
            assertEquals(aRequest.getInstitutionProfileId(), instProfId);
        }

        _logger.info(BORDER_END, "findByPropertyId");
    }
    /**
     * test findByRequest.
     */
    @Test
    public void testFindByRequest()  throws Exception {
      
        _logger.info(BORDER_START, "findByRequest");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
  
        // excute method
        ServiceRequest request = new ServiceRequest(_srk);
        Collection requests  = request.findByRequest(new RequestPK(requestId, copyId));
        
        // check assertEqual only requestId
        Iterator requestIter = requests.iterator();
        for (; requestIter.hasNext(); ) {
            ServiceRequest aRequest = (ServiceRequest) requestIter.next();
            assertEquals(aRequest.getRequestId(), requestId);
            assertEquals(aRequest.getCopyId(), copyId);
            assertEquals(aRequest.getInstitutionProfileId(), instProfId);
        }

        _logger.info(BORDER_END, "findByRequest");
    }

    /**
     * test create.
     */
    @Test
    public void testCreate() throws Exception {
      
        _logger.info(BORDER_START, "create");
                
        // excute method
        // 1. get request info from Test data
        int requestDealId = _dataRepository.getInt("REQUEST",  "DEALID", 0);
        int requestCopyId = _dataRepository.getInt("REQUEST",  "COPYID", 0);
        int requestStatusId = _dataRepository.getInt("REQUEST",  "REQUESTSTATUSID", 0);
        int requestUserProfileId = _dataRepository.getInt("REQUEST",  "USERPROFILEID", 0);
        int reqeustInstId = _dataRepository.getInt("REQUEST",  "INSTITUTIONPROFILEID", 0);

        //  set VPD
        _srk.getExpressState().setDealInstitutionId(reqeustInstId);

        // set beginTransaction for auto commit off
        _srk.beginTransaction();

        // 2. create Request
        Date now = new Date();
        Request request = new Request(_srk);
        request = request.create(new DealPK(requestDealId, requestCopyId), requestStatusId, now, requestUserProfileId);

        // 3. create ServiceRequest
        ServiceRequest entity = new ServiceRequest(_srk);
        entity = entity.create(request.getRequestId(), requestCopyId);
        assertEquals(entity.getInstitutionProfileId(), reqeustInstId);
        assertEquals(entity.getRequestId(), request.getRequestId());
        assertEquals(entity.getCopyId(), requestCopyId);
        assertEquals(entity.getInstitutionProfileId(), reqeustInstId);

        // rollback
        _srk.rollbackTransaction();
        
        _logger.info(BORDER_END, "create");
    }

    /**
     * assertEqualProperties
     * 
     * check assertEquals for all properties of ServiceProvider with test Data
     * 
     * @param entity: ServiceProvider
     * @throws Exception
     */
    private void assertEqualsProperties(ServiceRequest entity) throws Exception
    {
        assertEquals(entity.getRequestId(), requestId);
        assertEquals(entity.getSpecialInstructions(), specialInstructions);
        assertEquals(entity.getServiceProviderRefNo(), serviceProviderRefNo);
        assertEquals(entity.getPropertyId(), propertyId);
        assertEquals(entity.getCopyId(), copyId);
        assertEquals(entity.getInstitutionProfileId(), instProfId);
    }
    
}
