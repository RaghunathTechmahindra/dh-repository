/*
 * @(#)DcImagingReferenceTest.java    2005-3-16
 *
 */

package com.basis100.deal.entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.basis100.deal.pk.DCImagingReferencePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;

/**
 * DcImagingReferenceTest -
 * 
 * @version 1.0 2005-3-16
 * @author <A HREF="mailto:chyxiang@yahoo.com">Chen Xiang (Sean)</A>
 */
public class DcImagingReferenceTest extends ExpressEntityTestCase {

    // The logger
    private static Log _log = LogFactory.getLog(DcImagingReferenceTest.class);

    // The SessionResource Kit
    private SessionResourceKit _srk;

    // DcImagingReference entity
    private DCImagingReference _dcImagingRefDAO;

    // institutionId for deal
    private int dealInstitutionId;

    /*
     * setup
     */
    @Before
    public void setUp() throws Exception {

        // init resource
        ResourceManager.init();
        _srk = new SessionResourceKit("Testing");
        _dcImagingRefDAO = new DCImagingReference(_srk);

        dealInstitutionId = _dataRepository.getInt("Deal",
                "InstitutionProfileId", 0);
        // set vpd institutionId
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        
    }

    /*
     * teardown
     */
    @After
    public void tearDown() throws Exception {
        _srk.freeResources();
    }

  /*  
     * test Create
     
    @Test
    public void testCreate() throws Exception {

        _srk.beginTransaction();
        String testDesc = "folder status desc";

        // create DCImagingReference
        DCImagingReference dcImage = _dcImagingRefDAO.create(1, "foldercode",
                "statuscode", testDesc);

        // verify result
        assertEquals(testDesc, dcImage.getDcFolderStatusDescription());
        _log.info("Create Dc Imaging reference: ");
        _log.info("    Dc Image Folder Id: " + dcImage.getDcFolderId());
        _log.info("        Dc Folder Desc: "
                + dcImage.getDcFolderStatusDescription());
        _log.info(" Dc Folder Status Code: " + dcImage.getDcFolderStatusCode());
        _log.info("====================================");
        _srk.rollbackTransaction();
    }

    *//**
     * test get by primary key.
     *//*
    @Test
    public void testFindByPrimaryKey() throws Exception {

        _srk.beginTransaction();
        String testDesc = "folder status desc";
        // get DCImagingReference by primary key
        _dcImagingRefDAO = _dcImagingRefDAO.create(1, "foldercode",
                "statuscode", testDesc);
        int refId = _dcImagingRefDAO.getDcImagingReferenceId();
        
        DCImagingReferencePK pk = new DCImagingReferencePK(refId);
        _dcImagingRefDAO.findByPrimaryKey(pk);

        // verify result
        assertEquals(testDesc, _dcImagingRefDAO.getDcFolderStatusDescription());
        _log.info("Found Dc Imaging reference primay key: ");
        _log.info("    Dc Image Folder Id: " + _dcImagingRefDAO.getDcFolderId());
        _log.info("        Dc Folder Desc: "
                + _dcImagingRefDAO.getDcFolderStatusDescription());
        _log.info(" Dc Folder Status Code: " + _dcImagingRefDAO.getDcFolderStatusCode());
        _log.info("====================================");
        _srk.rollbackTransaction();

    }

    */
     /** test find by Dc folder Id**/
     
    @Test
    public void testFindByDcFolderId() throws Exception {

        _srk.beginTransaction();
        // get values from database
        String testDesc = "Y";
        int folderId = 1211;
        // get DCImagingReference by primary key
        DCImagingReferencePK pk = _dcImagingRefDAO.createPrimaryKey();
        _dcImagingRefDAO = _dcImagingRefDAO.create(folderId, "foldercode",
                "statuscode", testDesc);
        
        DCImagingReference dcImage = _dcImagingRefDAO
                .findByDcFolderId(folderId);
        _log.info("DcImagingRef Test description: " + testDesc);

        // verify result
        assertEquals(testDesc, dcImage.getDcFolderStatusDescription());
        _log.info("Found Dc Imaging reference by Dc FolderId: ");
        _log.info("    Dc Image Folder Id: " + dcImage.getDcFolderId());
        _log.info("        Dc Folder Desc: "
                + dcImage.getDcFolderStatusDescription());
        _log.info(" Dc Folder Status Code: " + dcImage.getDcFolderStatusCode());
        _log.info("====================================");
        
        _srk.rollbackTransaction();
    }
   
}
