package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.aag.AAGuideline;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.CreditReferencePK;
import com.basis100.deal.pk.CrossSellProfilePK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;

public class CreditReferenceDbTest extends FXDBTestCase {
	CreditReference creditReference;
	private String INIT_XML_DATA = "CreditReferenceDataSet.xml";
	IDataSet dataSetTest;

	public CreditReferenceDbTest(String name) throws DataSetException,
			IOException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"CreditReferenceDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		creditReference = new CreditReference(srk);

	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	public void testFindByBorrower() throws NumberFormatException,
			DataSetException, Exception {
		boolean hasEle = false;
		int borrowerId = Integer.parseInt(dataSetTest
				.getTable("testFindByBorrower").getValue(0, "borrowerId")
				.toString());
		int copyId = Integer.parseInt(dataSetTest
				.getTable("testFindByBorrower").getValue(0, "copyId")
				.toString());
		// find a credit reference by borrower
		Collection creditRefs = creditReference.findByBorrower(new BorrowerPK(
				borrowerId, copyId));

		if (creditRefs.size() > 0)
			hasEle = true;
		assertTrue(hasEle);
		for (Iterator i = creditRefs.iterator(); i.hasNext();) {
			CreditReference creditRef = (CreditReference) i.next();
			// verify result
			assertNotNull(creditRef);
			assertEquals(borrowerId, creditRef.getBorrowerId());

		}

	}

	public void testFindByBorrowerFailure() throws NumberFormatException,
			DataSetException, Exception {
		int borrowerId = Integer.parseInt(dataSetTest
				.getTable("testFindByBorrowerFailure")
				.getValue(0, "borrowerId").toString());
		int copyId = Integer.parseInt(dataSetTest
				.getTable("testFindByBorrowerFailure").getValue(0, "copyId")
				.toString());
		// find a credit reference by borrower
		Collection creditRefs = creditReference.findByBorrower(new BorrowerPK(
				borrowerId, copyId));
		assertEquals(0, creditRefs.size());

	}

	public void testFindByMyCopies() throws RemoteException, FinderException,
			NumberFormatException, DataSetException {
		creditReference.setCreditReferenceId(Integer.parseInt(dataSetTest
				.getTable("testFindByMyCopies")
				.getValue(0, "creditReferenceId").toString()));
		boolean hasRefs = false;
		Collection creditrefs = creditReference.findByMyCopies();
		if (creditrefs.size() > 1)
			hasRefs = true;

		assertTrue(hasRefs);

	}

	public void testFindByMyCopiesFailure() throws RemoteException,
			FinderException, NumberFormatException, DataSetException {
		creditReference.setCreditReferenceId(Integer.parseInt(dataSetTest
				.getTable("testFindByMyCopiesFailure")
				.getValue(0, "creditReferenceId").toString()));
		boolean hasRefs = false;
		Collection creditrefs = creditReference.findByMyCopies();

		assertSame(0, creditrefs.size());

	}

	public void testCreate() throws NumberFormatException, DataSetException,
			Exception {
		srk.beginTransaction();
		int borrowerId = Integer.parseInt(dataSetTest.getTable("testCreate")
				.getValue(0, "borrowerId").toString());
		int copyId = Integer.parseInt(dataSetTest.getTable("testCreate")
				.getValue(0, "copyId").toString());
		srk.getExpressState().setDealInstitutionId(
				Integer.parseInt(dataSetTest.getTable("testCreate")
						.getValue(0, "dealInstitutionId").toString()));
		BorrowerPK pk = new BorrowerPK(borrowerId, copyId);
		CreditReference creditRef = creditReference.create(pk);
		assertEquals(borrowerId, creditRef.getBorrowerId());
		assertTrue(srk.getModified());
		srk.rollbackTransaction();

	}
	
	/*public void testFindByPrimaryKey() throws RemoteException, FinderException, NumberFormatException, DataSetException {
		ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKey");
		int creditReferenceId = Integer.parseInt((String)testFindByPrimaryKey.getValue(0,"creditReferenceId"));
		int copyId = Integer.parseInt((String)testFindByPrimaryKey.getValue(0,"copyId"));
		CreditReferencePK pk = new CreditReferencePK(creditReferenceId,copyId);
		creditReference= creditReference.findByPrimaryKey(pk);
		assertEquals(creditReferenceId, pk.getId());
	}
	*/
	public void testFindByPrimaryKeyFailure() throws RemoteException, FinderException, NumberFormatException, DataSetException {
		ITable testFindByPrimaryKey = dataSetTest.getTable("testFindByPrimaryKeyFailure");
		boolean status=false;
		try{
		int creditReferenceId = Integer.parseInt((String)testFindByPrimaryKey.getValue(0,"creditReferenceId"));
		int copyId = Integer.parseInt((String)testFindByPrimaryKey.getValue(0,"copyId"));
		CreditReferencePK pk = new CreditReferencePK(creditReferenceId,copyId);
		creditReference= creditReference.findByPrimaryKey(pk);
		status=true;
		}catch(Exception e){
			status=false;
		}
		assertEquals(false, status);
	}
	
}
