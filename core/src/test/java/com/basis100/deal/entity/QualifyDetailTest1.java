package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.DealPK;

public class QualifyDetailTest1 extends  FXDBTestCase{
	
	private IDataSet dataSetTest;
	QualifyDetail qualifyDetail=null;
	public QualifyDetailTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("QualifyDetailDataSetTest.xml"));
	}



	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		qualifyDetail=new QualifyDetail(srk);
	}
	
	//findByDeal(DealPK)

	public void testFindByDeal() throws Exception{
		ITable testFindByDeal = dataSetTest.getTable("testFindByDeal");
		int Id =Integer.parseInt(testFindByDeal.getValue(0,"Id").toString());
		int CopyId =Integer.parseInt(testFindByDeal.getValue(0,"CopyId").toString());
		DealPK pk =new DealPK(Id,CopyId);
		int size=qualifyDetail.findByDeal(pk).size();
		assertNotSame(0, size);
	}

	public void testFindByDealFailure() throws Exception{
		ITable testFindByDeal = dataSetTest.getTable("testFindByDealFailure");
		int Id =Integer.parseInt(testFindByDeal.getValue(0,"Id").toString());
		int CopyId =Integer.parseInt(testFindByDeal.getValue(0,"CopyId").toString());
		DealPK pk =new DealPK(Id,CopyId);
		int size=qualifyDetail.findByDeal(pk).size();
		assertSame(0, size);
	}
	// Create
	

		public void testCreate() throws Exception{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int dealId =Integer.parseInt(testCreate.getValue(0,"DealId").toString());
		int interestCompoundingId =Integer.parseInt(testCreate.getValue(0,"InterestCompoundingId").toString());
		int repaymentTypeId =Integer.parseInt(testCreate.getValue(0,"RepaymentTypeId").toString());
		srk.getExpressState().setDealInstitutionId(1);
		QualifyDetail qualifyDetail1=qualifyDetail.create(dealId, interestCompoundingId, repaymentTypeId);
		assertNotNull(qualifyDetail1);
		srk.rollbackTransaction();
	}
	public void testCreateFailure() throws Exception{
		boolean status=false;
		try{
		ITable testCreateFailure = dataSetTest.getTable("testCreateFailure");
		int dealId =Integer.parseInt(testCreateFailure.getValue(0,"DealId").toString());
		int interestCompoundingId =Integer.parseInt(testCreateFailure.getValue(0,"InterestCompoundingId").toString());
		int repaymentTypeId =Integer.parseInt(testCreateFailure.getValue(0,"RepaymentTypeId").toString());
		QualifyDetail qualifyDetail1=qualifyDetail.create(dealId, interestCompoundingId, repaymentTypeId);
		}catch (Exception e) {
			// TODO: handle exception
			status=false;
		}
		assertSame(false,status);
	}
}
