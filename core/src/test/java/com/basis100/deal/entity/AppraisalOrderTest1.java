package com.basis100.deal.entity;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.AppraisalOrderPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.ltx.unittest.util.EntityTestUtil;

public class AppraisalOrderTest1 extends FXDBTestCase{

	private IDataSet dataSetTest;
	private AppraisalOrder appraisalOrder;	
	private DealEntity entity;
	
	
	
	public AppraisalOrderTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(AppraisalOrder.class.getSimpleName() + "DataSetTest.xml"));
	}


	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		appraisalOrder = new AppraisalOrder(srk,null);
	}
	
	public void testCreate() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, SQLException, CloneNotSupportedException, CreateException{
		
		ITable testCreate = dataSetTest.getTable("testCreate");		
		String id=(String)testCreate.getValue(0,"PROPERTYID");
	    int propertyId=Integer.parseInt(id);
		String copy=(String)testCreate.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		srk.getExpressState().setDealInstitutionId(1);
	    PropertyPK pk=new PropertyPK(propertyId,copyId); 
	    appraisalOrder=appraisalOrder.create(pk);
	    assertEquals(appraisalOrder.getPropertyId(),pk.getId());
} 
 public void testFindByDealSuccess() throws Exception{
	    Collection collection=null;
		ITable testFindByDealSuccess = dataSetTest.getTable("testFindByDealSuccess");		
		String id=(String)testFindByDealSuccess.getValue(0,"DEALID");
	    int dealId=Integer.parseInt(id);
		String copy=(String)testFindByDealSuccess.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);
		srk.getExpressState().setDealInstitutionId(1);		 
		DealPK dealPK=new DealPK(dealId,copyId); 
		collection=appraisalOrder.findByDeal(dealPK);
	    assertEquals(dealId,dealPK.getId());
} 
	
public void testFindByDealFailure() throws Exception{
    Collection collection=null;
	ITable testFindByDealFailure = dataSetTest.getTable("testFindByDealFailure");		
	String id=(String)testFindByDealFailure.getValue(0,"DEALID");
    int dealId=Integer.parseInt(id);
	String copy=(String)testFindByDealFailure.getValue(0,"COPYID");
	int copyId=Integer.parseInt(copy);
	srk.getExpressState().setDealInstitutionId(1);		 
	DealPK dealPK=new DealPK(dealId,copyId); 
	collection=appraisalOrder.findByDeal(dealPK);
    assertEquals(dealId,dealPK.getId());
} 


public void testFindByPrimaryKeySuccess() throws Exception{    
	ITable testFindByPrimaryKeySuccess = dataSetTest.getTable("testFindByPrimaryKeySuccess");		
	String id=(String)testFindByPrimaryKeySuccess.getValue(0,"APPRAISALORDERID");
    int appraisalId=Integer.parseInt(id);
	String copy=(String)testFindByPrimaryKeySuccess.getValue(0,"COPYID");
	int copyId=Integer.parseInt(copy);
	srk.getExpressState().setDealInstitutionId(1);		 
	AppraisalOrderPK appraisalPK=new AppraisalOrderPK(appraisalId,copyId); 
	appraisalOrder=appraisalOrder.findByPrimaryKey(appraisalPK);
    assertEquals(appraisalId,appraisalPK.getId());
} 

public void testFindByPrimaryKeyFailure() throws Exception{    
	ITable testFindByPrimaryKeyFailure = dataSetTest.getTable("testFindByPrimaryKeyFailure");		
	String id=(String)testFindByPrimaryKeyFailure.getValue(0,"APPRAISALORDERID");
    int appraisalId=Integer.parseInt(id);
	String copy=(String)testFindByPrimaryKeyFailure.getValue(0,"COPYID");
	int copyId=Integer.parseInt(copy);
	srk.getExpressState().setDealInstitutionId(1);		 
	AppraisalOrderPK appraisalPK=new AppraisalOrderPK(appraisalId,copyId); 
	appraisalOrder=appraisalOrder.findByPrimaryKey(appraisalPK);
    assertEquals(appraisalId,appraisalPK.getId());
} 

public void testFindByMyCopiesSuccess() throws Exception{  
	Vector appraisal=null;
	ITable testFindByMyCopiesSuccess = dataSetTest.getTable("testFindByMyCopiesSuccess");		
	String id=(String)testFindByMyCopiesSuccess.getValue(0,"APPRAISALORDERID");
    int appraisalId=Integer.parseInt(id);
	String copy=(String)testFindByMyCopiesSuccess.getValue(0,"COPYID");
	int copyId=Integer.parseInt(copy);
	srk.getExpressState().setDealInstitutionId(1);	
	appraisalOrder.setAppraisalOrderId(appraisalId);
	appraisalOrder.copyId=copyId;	 
	appraisal=appraisalOrder.findByMyCopies();
    assertEquals(appraisalId,appraisalOrder.getAppraisalOrderId());
} 

public void testFindByMyCopiesFailure() throws Exception{  
	Vector appraisal=null;
	ITable testFindByMyCopiesFailure = dataSetTest.getTable("testFindByMyCopiesFailure");		
	String id=(String)testFindByMyCopiesFailure.getValue(0,"APPRAISALORDERID");
    int appraisalId=Integer.parseInt(id);
	String copy=(String)testFindByMyCopiesFailure.getValue(0,"COPYID");
	int copyId=Integer.parseInt(copy);
	srk.getExpressState().setDealInstitutionId(1);	
	appraisalOrder.setAppraisalOrderId(appraisalId);
	appraisalOrder.copyId=copyId;	
	appraisal=appraisalOrder.findByMyCopies();
    assertEquals(0,appraisal.size());
} 

public void testFindByProperty() throws Exception{    
	ITable testFindByPrimaryKeySuccess = dataSetTest.getTable("testFindByProperty");		
	String id=(String)testFindByPrimaryKeySuccess.getValue(0,"propertyId");
    int propertyId=Integer.parseInt(id);
	String copy=(String)testFindByPrimaryKeySuccess.getValue(0,"copyId");
	int copyId=Integer.parseInt(copy);
	srk.getExpressState().setDealInstitutionId(1);		 
	PropertyPK propertyPK=new PropertyPK(propertyId,copyId); 
	appraisalOrder=appraisalOrder.findByProperty(propertyPK);
    assertEquals(propertyId,propertyPK.getId());
}

public void testFindByPropertyFailure() throws Exception{    
	ITable testFindByPrimaryKeySuccess = dataSetTest.getTable("testFindByPropertyFailure");	
	boolean status=false;
	try{
	String id=(String)testFindByPrimaryKeySuccess.getValue(0,"propertyId");
    int propertyId=Integer.parseInt(id);
	String copy=(String)testFindByPrimaryKeySuccess.getValue(0,"copyId");
	int copyId=Integer.parseInt(copy);
	srk.getExpressState().setDealInstitutionId(1);		 
	PropertyPK propertyPK=new PropertyPK(propertyId,copyId); 
	appraisalOrder=appraisalOrder.findByProperty(propertyPK);
	status=true;
	}catch(Exception e){
		status=false;
	}
   assertNull(appraisalOrder);
} 
}
