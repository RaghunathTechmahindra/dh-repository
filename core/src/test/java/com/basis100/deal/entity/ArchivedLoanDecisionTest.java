package com.basis100.deal.entity;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.AppraisalOrderPK;
import com.basis100.deal.pk.ArchivedLoanApplicationPK;
import com.basis100.deal.pk.ArchivedLoanDecisionPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.util.EntityTestUtil;

public class ArchivedLoanDecisionTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private ArchivedLoanDecision archivedLoanDecision;	
	private DealEntity entity;
	
	
	
	
	public ArchivedLoanDecisionTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ArchivedLoanDecision.class.getSimpleName() + "DataSetTest.xml"));
	}

	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		archivedLoanDecision = new ArchivedLoanDecision(srk);
		
	}
	
	public void testCreatePrimaryKey() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, SQLException, CloneNotSupportedException, CreateException{
		try {
			srk.beginTransaction();
			ITable testCreatePrimaryKey = dataSetTest.getTable("testCreatePrimaryKey");		
			String archivedLoanApp=(String)testCreatePrimaryKey.getValue(0,"ARCHIVEDLOANDECISIONID");
		    int archivedLoanDecisionId=Integer.parseInt(archivedLoanApp);		    
			ArchivedLoanDecisionPK archivedLoanDecisionPK=new ArchivedLoanDecisionPK(archivedLoanDecisionId);
			//archivedLoanDecisionPK=archivedLoanDecision.createPrimaryKey();
		    assertNotNull(archivedLoanDecisionPK);
		    srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
} 
	 	
	 public void testCreate() throws Exception{	
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");		
		String archivedLoanId=(String)testCreate.getValue(0,"ARCHIVEDLOANDECISIONID");
	    int archivedLoanDecisionId=Integer.parseInt(archivedLoanId);	
	    String id=(String)testCreate.getValue(0,"COPYID");
	    int copyId=Integer.parseInt(id);	
	    String sourceAppId=(String)testCreate.getValue(0,"SOURCEAPPLICATIONID");
		srk.getExpressState().setDealInstitutionId(1);
		archivedLoanDecision=archivedLoanDecision.create(archivedLoanDecisionId,copyId,sourceAppId);
	    assertEquals(sourceAppId,archivedLoanDecision.getSourceApplicationId());
	    srk.rollbackTransaction();
}
	 

	public void testFindByPrimaryKeySuccess() throws Exception{	    
		ITable testFindByPrimaryKeySuccess = dataSetTest.getTable("testFindByPrimaryKeySuccess");		
		String id=(String)testFindByPrimaryKeySuccess.getValue(0,"ARCHIVEDLOANDECISIONID");
	    int archivedLoanDecisionId=Integer.parseInt(id);
		srk.getExpressState().setDealInstitutionId(1);	
		ArchivedLoanDecisionPK pk=new ArchivedLoanDecisionPK(archivedLoanDecisionId);
		archivedLoanDecision=archivedLoanDecision.findByPrimaryKey(pk);
	    assertEquals(archivedLoanDecisionId,archivedLoanDecision.getArchivedLoanDecisionId());
	  }
}
