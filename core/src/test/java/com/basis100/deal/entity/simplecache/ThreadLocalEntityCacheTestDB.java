package com.basis100.deal.entity.simplecache;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.DealPK;

/**
 * <p>
 * ThreadLocalEntityCacheTestDB
 * </p>
 * Express Entity class unit test: ServiceProvider
 */
public class ThreadLocalEntityCacheTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;
	public ThreadLocalEntityCacheTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				ThreadLocalEntityCache.class.getSimpleName()
						+ "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(
				ThreadLocalEntityCache.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();		
	}

	@Test
	public void testAttachPK() throws Exception {
		// get input data from xml repository
		ITable testAttachPK = dataSetTest.getTable("testAttachPK");
		int dealId = Integer.parseInt((String) testAttachPK.getValue(0,
				"DEALID"));
		int copyId = Integer.parseInt((String) testAttachPK.getValue(0,
				"COPYID"));
		Deal deal = new Deal(srk, null);
		DealEntity entity = (DealEntity) deal;
		DealPK pk = new DealPK(dealId, copyId);
		ThreadLocalEntityCache.attachPK(entity, pk);
		assertSame(deal.getPk(), pk);
	}

}