/*
 * @(#)ComponentLOCTest.java May 13, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.pk.ComponentLOCPK;
import com.basis100.deal.pk.ComponentPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * 
 */
public class ComponentLOCTest extends ExpressEntityTestCase implements UnitTestLogging {
 // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(ComponentLoanTest.class);

    // session resource kit
    private SessionResourceKit  _srk;
    private int dealId;
    private int copyId;
    private int institutionId;

    /**
     * Constructor function
     */
    public ComponentLOCTest() {

    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
        dealId = _dataRepository.getInt("DEAL", "DEALId", 0);
        copyId = _dataRepository.getInt("DEAL", "COPYID", 0);
        institutionId = _dataRepository.getInt("DEAL", "INSTITUTIONPROFILEID", 0);
        _srk.getExpressState().setDealInstitutionId(institutionId);
        
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }

    /**
     * Test method for {@link com.basis100.deal.entity.ComponentLOC#findByPrimaryKey(com.basis100.deal.pk.ComponentLOCPK)}.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception{

        _srk.beginTransaction();

        Deal deal = new Deal(_srk, null, dealId, copyId);
        int mtgProdId = deal.getMtgProdId();
        Component comp = new Component(_srk, null);
        comp = comp.create(dealId, copyId, Mc.COMPONENT_TYPE_LOC,
                mtgProdId);

        ComponentLOC componentLoc = new ComponentLOC(_srk, null);
        componentLoc.create(comp.getComponentId(), comp.getCopyId());

        _logger.info(BORDER_START, "findByPrimaryKey");
        _logger.info("Got Values from new ComponentLOC ::: [{}]", comp
                .getComponentId()
                + "::" + institutionId + "::" + copyId);

        // create the entity
        ComponentLOC _component = new ComponentLOC(_srk);

        // Find by primary Key
        _component = _component.findByPrimaryKey(new ComponentLOCPK(comp
                .getComponentId(), copyId));
        _logger.info("Got Values from ComponentLOC found::: [{}]", _component
                .getComponentId()
                + "::" + _component.getCopyId());

        // Check if the data retrieved matches the DB
        assertEquals(_component.getComponentId(), comp.getComponentId());
        assertEquals(_component.getCopyId(), copyId);

        _logger.info(BORDER_END, "findByPrimaryKey");
        // roll-back transaction.
        _srk.rollbackTransaction();
    }

    /**
     * Test method for {@link com.basis100.deal.entity.ComponentLOC#create(int, int, int, int, int)}.
     */
    @Test
    public void testCreate() throws Exception{
        _logger.info(BORDER_START, "Create - ComponentLOC");

        // begin transaction
        _srk.beginTransaction();
        Deal deal = new Deal(_srk, null, dealId, copyId);
        int mtgProdId = deal.getMtgProdId();

        Component comp = new Component(_srk, null);
        comp = comp.create(dealId, copyId, Mc.COMPONENT_TYPE_LOC,
                mtgProdId);

        _logger.info("Got Values from new ComponentLOC ::: [{}]", institutionId);
        
        //create entity
        ComponentLOC newEntity = new ComponentLOC(_srk);
        newEntity = newEntity.create(comp.getComponentId(), copyId);
        
        _logger.info("Created a new ComponentLOC ::" + newEntity.getComponentId());
        
        //create the holder entity
        ComponentLOC foundEntity = new ComponentLOC(_srk);
        
        //Find by primary Key
        foundEntity = foundEntity.findByPrimaryKey(new ComponentLOCPK(newEntity
                .getComponentId(), copyId));
        
        // verifying the created entity
        assertEquals(foundEntity.getComponentId(), newEntity.getComponentId());
        assertEquals(foundEntity.getCopyId(), newEntity.getCopyId());
        
        _logger.info(BORDER_END, "Create - ComponentLOC");

        // roll-back transaction.
        _srk.rollbackTransaction();
    }

}
