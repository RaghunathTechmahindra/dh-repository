package com.basis100.deal.entity;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.duplicate.DuplicateBorrowerFinder;
import com.basis100.deal.duplicate.SearchResult;

public class DuplicateBorrowerFinderTest extends FXDBTestCase {

	private IDataSet dataSetTest;
	private DuplicateBorrowerFinder duplicateBorrowerFinder = null;
	private Borrower borrower = null;
	private DealEntity dealEntity = null;
	private Deal deal = null;

	public DuplicateBorrowerFinderTest(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"DuplicateBorrowerFinderDataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		duplicateBorrowerFinder = new DuplicateBorrowerFinder(srk);
		borrower = new Borrower(srk, CalcMonitor.getMonitor(srk));
	}

	public void testMatchBorrower() throws Exception {
		ITable findByDupeCheckCriteria = dataSetTest
				.getTable("testMatchBorrower");
		String socialInsuranceNumber = findByDupeCheckCriteria.getValue(0,
				"SocialInsuranceNumber").toString();
		String borrowerBirthDate = (String) findByDupeCheckCriteria.getValue(0,
				"applicationDate");
		SimpleDateFormat formate = new SimpleDateFormat("dd-MMM-yy");
		Date date = formate.parse(borrowerBirthDate);
		borrower.setSocialInsuranceNumber(socialInsuranceNumber);
		int resultSize = duplicateBorrowerFinder.matchBorrower(borrower, date)
				.size();
		assertNotSame(0, resultSize);
	}

	// get Deal
	public void testFindDealListMatch() throws Exception {
		ITable findByDupeCheckCriteria = dataSetTest
				.getTable("testFindDealListMatch");
		String dealId = findByDupeCheckCriteria.getValue(0, "DealId")
				.toString();
		String copyId = findByDupeCheckCriteria.getValue(0, "CopyId")
				.toString();
		String borrowerBirthDate = (String) findByDupeCheckCriteria.getValue(0,
				"BORROWERBIRTHDATE");
		SimpleDateFormat formate = new SimpleDateFormat("dd-MMM-yy");
		Date date = formate.parse(borrowerBirthDate);
		deal = new Deal(srk, CalcMonitor.getMonitor(srk),
				Integer.parseInt(dealId), Integer.parseInt(copyId));
		dealEntity = (DealEntity) deal;
		int resultSize = duplicateBorrowerFinder.findDealListMatch(dealEntity,
				date).size();
		
		System.out.println("SIZE" +resultSize);
		assertNotSame(0, resultSize);
	}

	// get Borrower
	public void testFindBorrowerListMatch() throws Exception {
		ITable findByDupeCheckCriteria = dataSetTest
				.getTable("testFindBorrowerListMatch");
		String borrowerId = findByDupeCheckCriteria.getValue(0, "BORROWERID")
				.toString();
		String copyId = findByDupeCheckCriteria.getValue(0, "COPYID")
				.toString();
		String borrowerBirthDate = (String) findByDupeCheckCriteria.getValue(0,
				"BORROWERBIRTHDATE");
		SimpleDateFormat formate = new SimpleDateFormat("dd-MMM-yy");
		Date date = formate.parse(borrowerBirthDate);
		borrower = new Borrower(srk, CalcMonitor.getMonitor(srk),
				Integer.parseInt(borrowerId), Integer.parseInt(copyId));
		dealEntity = (DealEntity) borrower;
		int resultSize = duplicateBorrowerFinder.findDealListMatch(dealEntity,
				date).size();
		assertEquals(0, resultSize);
	}

	// deal search

	public void testDealSearch() throws Exception {
		ITable findByDupeCheckCriteria = dataSetTest.getTable("testDealSearch");
		String dealId = findByDupeCheckCriteria.getValue(0, "DealId")
				.toString();
		String copyId = findByDupeCheckCriteria.getValue(0, "CopyId")
				.toString();
		String borrowerBirthDate = (String) findByDupeCheckCriteria.getValue(0,
				"BORROWERBIRTHDATE");
		SimpleDateFormat formate = new SimpleDateFormat("dd-MMM-yy");
		Date date = formate.parse(borrowerBirthDate);
		deal = new Deal(srk, CalcMonitor.getMonitor(srk),
				Integer.parseInt(dealId), Integer.parseInt(copyId));
		dealEntity = (DealEntity) deal;
		SearchResult result = duplicateBorrowerFinder.search(dealEntity, date);
		assertNotNull(result);
	}

	// borrower search
	public void testBorrowerSearch() throws Exception {
		ITable findByDupeCheckCriteria = dataSetTest
				.getTable("testBorrowerSearch");
		String borrowerId = findByDupeCheckCriteria.getValue(0, "BORROWERID")
				.toString();
		String copyId = findByDupeCheckCriteria.getValue(0, "COPYID")
				.toString();
		String borrowerBirthDate = (String) findByDupeCheckCriteria.getValue(0,
				"BORROWERBIRTHDATE");
		SimpleDateFormat formate = new SimpleDateFormat("dd-MMM-yy");
		Date date = formate.parse(borrowerBirthDate);
		borrower = new Borrower(srk, CalcMonitor.getMonitor(srk),
				Integer.parseInt(borrowerId), Integer.parseInt(copyId));
		dealEntity = (DealEntity) borrower;
		SearchResult result = duplicateBorrowerFinder.search(dealEntity, date);
		assertNotNull(result);
	}
}
