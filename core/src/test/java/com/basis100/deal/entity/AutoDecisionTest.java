/*
 * @(#)AutoDecisionTest.java Sep 18, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * @author amalhi
 * 
 * JUnit test for entity AutoDecision
 * 
 */
public class AutoDecisionTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(AutoDecisionTest.class);

    // session resource kit
    private SessionResourceKit _srk;
    private int dealId;
    private int copyId;
    private int instProfId;

    /**
     * Constructor function
     */
    public AutoDecisionTest() {

    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
        dealId = _dataRepository.getInt("DEAL", "DEALId", 0);
        copyId = _dataRepository.getInt("DEAL", "COPYID", 0);
        instProfId = _dataRepository.getInt("DEAL", "INSTITUTIONPROFILEID", 0);
        _srk.getExpressState().setDealInstitutionId(instProfId);

    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.AutoDecision#findByDealId(int)}.
     */
    @Test
    public void testFindByDealId() throws Exception {

        // begin transaction
        _srk.beginTransaction();
        
        // get input data from repository
        AutoDecision autoDecision = new AutoDecision(_srk);
        autoDecision.create(dealId);
        int status = autoDecision.getStatus();

        _logger.info(BORDER_START, "findByDealId");
        _logger.info("Got Values from new AutoDecision ::: [{}]", dealId + "::"
                + instProfId + "::" + status);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        AutoDecision _autoDecision = new AutoDecision(_srk);

        // Find by primary key
        _autoDecision = _autoDecision.findByDealId(dealId);

        _logger.info("Got Values from new AutoDecision ::: [{}]", _autoDecision
                .getDealId()
                + "::" + _autoDecision.getStatus());

        // Check if the data retrieved matches the DB
        assertEquals(_autoDecision.getDealId(), dealId);
        assertEquals(_autoDecision.getStatus(), status);

        _logger.info(BORDER_END, "findByDealId");
        _srk.rollbackTransaction();
    }

    /**
     * Test method for {@link com.basis100.deal.entity.AutoDecision#create(int)}.
     */
    @Test
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create");

        // begin transaction
        _srk.beginTransaction();

        // Create the initial entity
        AutoDecision newEntity = new AutoDecision(_srk);

        // call create method of entity
        newEntity = newEntity.create(dealId);

        _logger.info("CREATED AUTO DECISION ::: [{}]", newEntity.getDealId()
                + "::" + newEntity.getStatus());

        // check create correctly by calling findByPrimaryKey
        AutoDecision foundEntity = new AutoDecision(_srk);
        foundEntity = foundEntity.findByDealId(dealId);

        // verifying the created entity
        assertEquals(foundEntity.getDealId(), newEntity.getDealId());
        assertEquals(foundEntity.getStatus(), newEntity.getStatus());

        _logger.info(BORDER_END, "Create");

        // roll-back transaction.
        _srk.rollbackTransaction();

    }

}
