package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.DealPK;

public class DisclosureQuestionTest1  extends  FXDBTestCase{
	
	private IDataSet dataSetTest;
	private DisclosureQuestion disclosureQuestion=null;
	DealPK dealPK=null;
	
	public DisclosureQuestionTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("DisclosureQuestionDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource("DisclosureQuestionDataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		disclosureQuestion=new DisclosureQuestion(srk);
		
	}
	//findByDealIdDocumentTypeIdAndQuestionCode(int, int, String)

	public void testFindByDealIdDocumentTypeIdAndQuestionCode() throws Exception{
		
		ITable testFindByRequest = dataSetTest.getTable("testFindByDealIdDocumentTypeIdAndQuestionCode");
		int  dealId = Integer.parseInt(testFindByRequest.getValue(0,"dealId").toString());
		int  documentTypeId = Integer.parseInt(testFindByRequest.getValue(0,"documentTypeId").toString());
		String  questionCode = testFindByRequest.getValue(0,"questionCode").toString();	
		DisclosureQuestion disclosureQuestion1=disclosureQuestion.findByDealIdDocumentTypeIdAndQuestionCode(dealId, documentTypeId, questionCode);
		assertNotNull(disclosureQuestion1);
     	}
	
      public void testFindByDealIdDocumentTypeIdAndQuestionCodeFailure() throws Exception{
		
    	  boolean status=false;
    	try{  
		ITable testFindByRequest = dataSetTest.getTable("testFindByDealIdDocumentTypeIdAndQuestionCodeFailure");
		int  dealId = Integer.parseInt(testFindByRequest.getValue(0,"dealId").toString());
		int  documentTypeId = Integer.parseInt(testFindByRequest.getValue(0,"documentTypeId").toString());
		String  questionCode = testFindByRequest.getValue(0,"questionCode").toString();	
		DisclosureQuestion disclosureQuestion1=disclosureQuestion.findByDealIdDocumentTypeIdAndQuestionCode(dealId, documentTypeId, questionCode);
		status=true;
    	}catch (Exception e) {
			// TODO: handle exception
    		status=false;
		}
		assertEquals(false, status);
	}
      
       public void testFindByDealIdAndQuestionCode() throws Exception{
    	  ITable testFindByDealIdAndQuestionCode = dataSetTest.getTable("testFindByDealIdAndQuestionCode");
    	  int  dealId = Integer.parseInt(testFindByDealIdAndQuestionCode.getValue(0,"dealId").toString());
  		String  questionCode = testFindByDealIdAndQuestionCode.getValue(0,"questionCode").toString();	
  		DisclosureQuestion disclosureQuestion1=  disclosureQuestion.findByDealIdAndQuestionCode(dealId, questionCode);
    	  assertNotNull(disclosureQuestion1);
      }
      public void testFindByDealIdAndQuestionCodeFailure() throws Exception{
    	  boolean status=false;
      	try{  
    	  ITable testFindByDealIdAndQuestionCodeFailure = dataSetTest.getTable("testFindByDealIdAndQuestionCodeFailure");
    	  int  dealId = Integer.parseInt(testFindByDealIdAndQuestionCodeFailure.getValue(0,"dealId").toString());
  		  String  questionCode = testFindByDealIdAndQuestionCodeFailure.getValue(0,"questionCode").toString();	
  	      DisclosureQuestion disclosureQuestion1=  disclosureQuestion.findByDealIdAndQuestionCode(dealId, questionCode);
  		  status=true;
    	}catch (Exception e) {
			status=false;
		}
		assertEquals(false, status);
      }
      
      //create(DealPK, int, String, String)

      public void testCreate() throws Exception{
    	  
     	  ITable testCreate = dataSetTest.getTable("testCreate");
    	  int  dealId = Integer.parseInt(testCreate.getValue(0,"dealId").toString());
    	  int copyId = Integer.parseInt(testCreate.getValue(0,"copyId").toString());
    	  int dealInstitutionId = Integer.parseInt(testCreate.getValue(0,"dealInstitutionId").toString());
    	  int documentTypeId = Integer.parseInt(testCreate.getValue(0,"documentTypeId").toString());
    	  String questionCode = testCreate.getValue(0,"questionCode").toString();
    	  String isResourceBundleKey = testCreate.getValue(0,"isResourceBundleKey").toString();
  		  dealPK=new DealPK(dealId,copyId);
    	  srk.getExpressState().setDealInstitutionId(dealInstitutionId);
    	  DisclosureQuestion disclosureQuestion1 = disclosureQuestion.create(dealPK, documentTypeId, questionCode, isResourceBundleKey);
    	  assertNotNull(disclosureQuestion1);
      }
      
       public void testCreateFailure() throws Exception{
    	  
    	   boolean status=false;
    	   try{
     	  ITable testCreateFailure = dataSetTest.getTable("testCreateFailure");
    	  int  dealId = Integer.parseInt(testCreateFailure.getValue(0,"dealId").toString());
    	  int copyId = Integer.parseInt(testCreateFailure.getValue(0,"copyId").toString());
    	  int documentTypeId = Integer.parseInt(testCreateFailure.getValue(0,"documentTypeId").toString());
    	  String questionCode = testCreateFailure.getValue(0,"questionCode").toString();
    	  String isResourceBundleKey = testCreateFailure.getValue(0,"isResourceBundleKey").toString();
  		  dealPK=new DealPK(dealId,copyId);
    	  DisclosureQuestion disclosureQuestion1 = disclosureQuestion.create(dealPK, documentTypeId, questionCode, isResourceBundleKey);
    	   }catch (Exception e) {
			// TODO: handle exception
    		   status=false;
		}
    	  assertSame(false,status);
      }
}
