/**
 * 
 */
package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.AssetPK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * 
 * AssetTest
 * 
 * @version 1.0 Sep 19, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class AssetTest extends ExpressEntityTestCase implements UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(AssetTest.class);

    // seesion resource kit
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * Find by primary key
     * 
     * @throws Exception
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        int institutionId = _dataRepository.getInt("Asset",
                "InstitutionProfileID", 0);

        _srk.getExpressState().setDealIds(0, institutionId, 0);

        int assetID = _dataRepository.getInt("Asset", "AssetID", 0);
        int copyID = _dataRepository.getInt("Asset", "CopyID", 0);
        String assetDescription = _dataRepository.getString("Asset",
                "AssetDescription", 0);
        double assetValue = _dataRepository.getDouble("Asset", "AssetValue", 0);
        int borrowerID = _dataRepository.getInt("Asset", "borrowerID", 0);
        int assetTypeID = _dataRepository.getInt("Asset", "assetTypeID", 0);

        _logger.info(BORDER_START, "testFindByPrimaryKey");

        // create the initial entity
        Asset asset = new Asset(_srk, CalcMonitor.getMonitor(_srk));

        // find by primary key
        asset = asset.findByPrimaryKey(new AssetPK(assetID, copyID));

        assertEquals(asset.getAssetId(), assetID);
        assertTrue(asset.getAssetValue() == assetValue);
        assertEquals(asset.getAssetDescription(), assetDescription);
        assertEquals(asset.getBorrowerId(), borrowerID);
        assertEquals(asset.getAssetTypeId(), assetTypeID);

        _logger.info(BORDER_END, "testFindByPrimaryKey");

    }

    /**
     * test create
     * 
     * @throws Exception
     */
    @Test
    public void testCreate() throws Exception {
        _logger.info(BORDER_START, "Create");
        // get input data from repository
        int institutionId = _dataRepository.getInt("Asset",
                "InstitutionProfileID", 0);
        int borrowerID = _dataRepository.getInt("Asset", "borrowerID", 0);
        int copyID = _dataRepository.getInt("Asset", "CopyID", 0);

        // set VPD state
        _srk.getExpressState().setDealIds(0, institutionId, 0);

        // Create the initial entity
        Asset asset = new Asset(_srk, CalcMonitor.getMonitor(_srk));

        // Start new transaction
        _srk.beginTransaction();

        // create the entity
        asset.create(new BorrowerPK(borrowerID, copyID));

        asset.ejbStore();
        // Rollback transaction
        _srk.rollbackTransaction();

        // Free the resources
        _srk.freeResources();
        _logger.info(BORDER_END, "Create");

    }

}
