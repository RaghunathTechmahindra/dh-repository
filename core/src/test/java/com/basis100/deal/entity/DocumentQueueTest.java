/*
 * @(#) DocumentQueueTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */


package com.basis100.deal.entity;

import java.util.Date;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.DocumentProfilePK;
import com.basis100.deal.pk.DocumentQueuePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>DocumentQueueTest</p>
 * Express Entity class unit test: DocumentQueue
 */
public class DocumentQueueTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(DocumentQueueTest.class);

    /**
     * Constructor function
     */
    public DocumentQueueTest() {
    }

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {

    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int documentQueueId = _dataRepository.getInt("DOCUMENTQUEUE",
                "DOCUMENTQUEUEID", 0);
        int dealId = _dataRepository.getInt("DOCUMENTQUEUE", "DEALID", 0);
        int instProfId = _dataRepository.getInt("DOCUMENTQUEUE",
                "INSTITUTIONPROFILEID", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed the input data and run the program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        DocumentQueue entity = new DocumentQueue(srk, documentQueueId);
        entity = entity.findByPrimaryKey(new DocumentQueuePK(documentQueueId));

        // verify the running result.
        assertEquals(documentQueueId, entity.getDocumentQueueId());
        assertEquals(dealId, entity.getDealId());

        // free resources
        srk.freeResources();
    }

    /**
     * test create.
     */
    @Test
    public void testCreate1() throws Exception {

        // get input data from repository
        int documentQueueId = _dataRepository.getInt("DOCUMENTQUEUE",
                "DOCUMENTQUEUEID", 0);
        int instProfId = _dataRepository.getInt("DOCUMENTQUEUE",
                "INSTITUTIONPROFILEID", 0);
        Date timeRequested = _dataRepository.getDate("DOCUMENTQUEUE",
                "TIMEREQUESTED", 0);
        int requestorUserId = _dataRepository.getInt("DOCUMENTQUEUE",
                "REQUESTORUSERID", 0);
        String emailAddress = _dataRepository.getString("DOCUMENTQUEUE",
                "EMAILADDRESS", 0);
        String emailSubject = _dataRepository.getString("DOCUMENTQUEUE",
                "EMAILSUBJECT", 0);
        int languagePreferenceId = _dataRepository.getInt("DOCUMENTQUEUE",
                "LANGUAGEPREFERENCEID", 0);
        int lenderProfileId = _dataRepository.getInt("DOCUMENTQUEUE",
                "LENDERPROFILEID", 0);
        int documentTypeId = _dataRepository.getInt("DOCUMENTQUEUE",
                "DOCUMENTTYPEID", 0);
        String documentFormat = _dataRepository.getString("DOCUMENTQUEUE",
                "DOCUMENTFORMAT", 0);
        String documentVersion = _dataRepository.getString("DOCUMENTQUEUE",
                "DOCUMENTVERSION", 0);
        String emailText = _dataRepository.getString("DOCUMENTQUEUE",
                "EMAILTEXT", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed input data and run program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        srk.beginTransaction();
        DocumentQueue newEntity = new DocumentQueue(srk, documentQueueId);
        DocumentProfilePK dppk = new DocumentProfilePK(documentVersion,
                languagePreferenceId, lenderProfileId, documentTypeId,
                documentFormat);

        newEntity = newEntity.create(timeRequested, requestorUserId,
                emailAddress, emailSubject, emailText, dppk);
        newEntity.ejbStore();
        documentQueueId = newEntity.getDocumentQueueId();
        int dealId = newEntity.getDealId();

        // check create correctlly.
        DocumentQueue foundEntity = new DocumentQueue(srk, documentQueueId);
        DocumentQueuePK documentQueuePK = new DocumentQueuePK(documentQueueId);
        foundEntity = foundEntity.findByPrimaryKey(documentQueuePK);

        // verifying
        assertEquals(foundEntity.getDocumentQueueId(), documentQueueId);
        // rollback transaction.
        srk.rollbackTransaction();
        // free resources.
        srk.freeResources();
    }

    /**
     * test create.
     */
    @Test
    public void testCreate2() throws Exception {

        // get input data from repository
        int documentQueueId = _dataRepository.getInt("DOCUMENTQUEUE",
                "DOCUMENTQUEUEID", 0);
        int instProfId = _dataRepository.getInt("DOCUMENTQUEUE",
                "INSTITUTIONPROFILEID", 0);
        Date timeRequested = _dataRepository.getDate("DOCUMENTQUEUE",
                "TIMEREQUESTED", 0);
        int requestorUserId = _dataRepository.getInt("DOCUMENTQUEUE",
                "REQUESTORUSERID", 0);
        String emailFullName = _dataRepository.getString("DOCUMENTQUEUE",
                "EMAILFULLNAME", 0);
        String emailAddress = _dataRepository.getString("DOCUMENTQUEUE",
                "EMAILADDRESS", 0);
        String emailSubject = _dataRepository.getString("DOCUMENTQUEUE",
                "EMAILSUBJECT", 0);
        int languagePreferenceId = _dataRepository.getInt("DOCUMENTQUEUE",
                "LANGUAGEPREFERENCEID", 0);
        int lenderProfileId = _dataRepository.getInt("DOCUMENTQUEUE",
                "LENDERPROFILEID", 0);
        int documentTypeId = _dataRepository.getInt("DOCUMENTQUEUE",
                "DOCUMENTTYPEID", 0);
        String documentFormat = _dataRepository.getString("DOCUMENTQUEUE",
                "DOCUMENTFORMAT", 0);
        String scenarioNumber = _dataRepository.getString("DOCUMENTQUEUE",
                "SCENARIONUMBER", 0);
        String documentVersion = _dataRepository.getString("DOCUMENTQUEUE",
                "DOCUMENTVERSION", 0);
        int dealId = _dataRepository.getInt("DOCUMENTQUEUE",
                "DEALID", 0);
        int dealCopyId = _dataRepository.getInt("DOCUMENTQUEUE",
                "DEALCOPYID", 0);
        String emailText = _dataRepository.getString("DOCUMENTQUEUE",
                "EMAILTEXT", 0);
        String pFaxNumbers = _dataRepository.getString("DOCUMENTQUEUE",
                "FAXNUMBERS", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed input data and run program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        srk.beginTransaction();
        DocumentQueue newEntity = new DocumentQueue(srk, documentQueueId);
        DocumentQueuePK dqpk = new DocumentQueuePK(documentQueueId);
        DocumentProfilePK dppk = new DocumentProfilePK(documentVersion,
                languagePreferenceId, lenderProfileId, documentTypeId, documentFormat);
        newEntity = newEntity.findByPrimaryKey(dqpk);
        newEntity.ejbRemove();
        newEntity = newEntity.create(timeRequested, requestorUserId,
                emailAddress, emailFullName, emailSubject, emailText, dealId, dealCopyId,
                scenarioNumber, dppk, pFaxNumbers);
        newEntity.ejbStore();
        documentQueueId = newEntity.getDocumentQueueId();

        // check create correctlly.
        DocumentQueue foundEntity = new DocumentQueue(srk, documentQueueId);
        DocumentQueuePK documentQueuePK = new DocumentQueuePK(documentQueueId);
        foundEntity = foundEntity.findByPrimaryKey(documentQueuePK);

        // verifying
        assertEquals(foundEntity.getDocumentQueueId(), documentQueueId);
        // rollback transaction.
        srk.rollbackTransaction();
        // free resources.
        srk.freeResources();
    }

    /**
     * test create.
     */
    @Test
    public void testCreate3() throws Exception {

        // get input data from repository
        int documentQueueId = _dataRepository.getInt("DOCUMENTQUEUE",
                "DOCUMENTQUEUEID", 0);
        int instProfId = _dataRepository.getInt("DOCUMENTQUEUE",
                "INSTITUTIONPROFILEID", 0);
        Date timeRequested = _dataRepository.getDate("DOCUMENTQUEUE",
                "TIMEREQUESTED", 0);
        int requestorUserId = _dataRepository.getInt("DOCUMENTQUEUE",
                "REQUESTORUSERID", 0);
        String emailFullName = _dataRepository.getString("DOCUMENTQUEUE",
                "EMAILFULLNAME", 0);
        String emailAddress = _dataRepository.getString("DOCUMENTQUEUE",
                "EMAILADDRESS", 0);
        String emailSubject = _dataRepository.getString("DOCUMENTQUEUE",
                "EMAILSUBJECT", 0);
        String emailText = _dataRepository.getString("DOCUMENTQUEUE",
                "EMAILTEXT", 0);
        int dealId = _dataRepository.getInt("DOCUMENTQUEUE", "dealId", 0);
        int copyId = _dataRepository.getInt("DOCUMENTQUEUE", "copyId", 0);

        // init session resource kit.
        ResourceManager.init();
        SessionResourceKit srk = new SessionResourceKit();
        _logger.info("here ======");

        // feed input data and run program.
        srk.getExpressState().setDealInstitutionId(instProfId);
        srk.beginTransaction();
        DocumentQueue newEntity = new DocumentQueue(srk);
        DocumentQueuePK pk = newEntity.createPrimaryKey();
        newEntity = newEntity.create(timeRequested, requestorUserId,
                emailAddress, emailFullName, emailSubject, emailText, dealId, copyId);
        newEntity.ejbStore();
        documentQueueId = newEntity.getDocumentQueueId();

        // check create correctlly.
        DocumentQueue foundEntity = new DocumentQueue(srk, documentQueueId);
        DocumentQueuePK documentQueuePK = new DocumentQueuePK(documentQueueId);
        foundEntity = foundEntity.findByPrimaryKey(documentQueuePK);

        // verifying
        assertEquals(foundEntity.getDocumentQueueId(), documentQueueId);
        // rollback transaction.
        srk.rollbackTransaction();
        // free resources.
        srk.freeResources();
    }
}
