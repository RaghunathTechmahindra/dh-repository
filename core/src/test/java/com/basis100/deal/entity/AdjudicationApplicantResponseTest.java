/*
 * @(#)AdjudicationApplicantResponseTest.java Sep 21, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.AdjudicationApplicantResponsePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * AdjudicationApplicantResponseTest
 * 
 * @version 1.0 Sep 21, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class AdjudicationApplicantResponseTest extends ExpressEntityTestCase
        implements UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(AdjudicationApplicantResponseTest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test the find by primary key
     */
    @Ignore("No Data") @Test
    public void testFindByPrimaryKey() throws Exception {
        // get input data from repository
        int dealInstitutionId = _dataRepository.getInt(
                "AdjudicationApplicantResponse", "InstitutionProfileID", 0);
        int applicantNumber = _dataRepository.getInt(
                "AdjudicationApplicantRequest", "applicantNumber", 0);
        int copyID = _dataRepository.getInt("AdjudicationApplicantResponse",
                "copyID", 0);
        int responseID = _dataRepository.getInt("AdjudicationApplicantResponse",
                "responseID", 0);

        _logger.info(BORDER_START, "testFindByPrimaryKey");

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // initial entity
        AdjudicationApplicantResponse adjAppResponse = new AdjudicationApplicantResponse(
                _srk);

        adjAppResponse = adjAppResponse
                .findByPrimaryKey(new AdjudicationApplicantResponsePK(
                        responseID, applicantNumber));

        _logger.info(BORDER_END, "testFindByPrimaryKey");
    }

    /**
     * test Create
     * 
     * @throws Exception
     */
    @Ignore("Failing - No data") @Test
    public void testCreate() throws Exception {
        int dealInstitutionId = _dataRepository.getInt("BorrowerResponseAssoc",
                "InstitutionProfileID", 0);
        // get input data from repository
        int copyId = _dataRepository.getInt("BorrowerResponseAssoc", "copyID", 0);
        int borrowerId = _dataRepository.getInt("BorrowerResponseAssoc", "borrowerID", 0);
        int responseID = _dataRepository.getInt("BorrowerResponseAssoc", "responseID", 0);
        int appnum = 1;
        String applicantCurrentInd = "A";

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(dealInstitutionId);

        // Start new transaction
        _srk.beginTransaction();

        _logger.info(BORDER_START, "testCreate");

        // Create the initial entity
        AdjudicationApplicantResponse adjAppResponse = new AdjudicationApplicantResponse(
                _srk);

        // create a new Adjudication Applicant response
        adjAppResponse = adjAppResponse.create(
                new AdjudicationApplicantResponsePK(responseID, appnum), copyId,
                borrowerId, applicantCurrentInd);
        adjAppResponse.ejbStore();

        // roll back
        _srk.rollbackTransaction();
        _logger.info(BORDER_END, "Create");

    }
}
