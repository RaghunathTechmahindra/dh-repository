package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;

public class MasterDealTest extends FXDBTestCase {
	private String INIT_XML_DATA = "MasterDealDataSet.xml";
	private MasterDeal masterDeal;
	private IDataSet dataSetTest;

	public MasterDealTest(String name) throws DataSetException, IOException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"MasterDealDataSetTest.xml"));
	}



	@Override
	protected void setUp() throws Exception {
		super.setUp();
		srk.getExpressState().setDealInstitutionId(2);
		masterDeal = new MasterDeal(srk, CalcMonitor.getMonitor(srk));

	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}
	public void testFindByPrimaryKey() throws RemoteException, FinderException,
			NumberFormatException, DataSetException {
		int dealId = Integer.parseInt(dataSetTest
				.getTable("testFindByPrimaryKey").getValue(0, "id").toString());
		MasterDealPK masterDealPK = new MasterDealPK(dealId);
		MasterDeal mdeal = masterDeal.findByPrimaryKey(masterDealPK);
		assertNotNull(mdeal);
		assertEquals(dealId, mdeal.getDealId());
	}

	public void testNextScenarioNumber() throws JdbcTransactionException,
			RemoteException {
		srk.beginTransaction();
		int scenarioNumber = masterDeal.nextScenarioNumber();
		assertNotNull(scenarioNumber);
		srk.rollbackTransaction();
	}

	public void testCreate() throws JdbcTransactionException, RemoteException,
			CreateException, NumberFormatException, DataSetException {
		srk.beginTransaction();
		int dealId = Integer.parseInt(dataSetTest.getTable("testCreate")
				.getValue(0, "id").toString());
		int dealInstitutionId = Integer.parseInt(dataSetTest
				.getTable("testCreate").getValue(0, "dealInstitutionId")
				.toString());
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		MasterDealPK masterDealPK = new MasterDealPK(dealId);
		Deal deal = masterDeal.create(masterDealPK);
		assertEquals(dealId, deal.getDealId());
		srk.rollbackTransaction();
	}

	public void testCreatePrimaryKey() throws JdbcTransactionException,
			CreateException, NumberFormatException, DataSetException {
		srk.beginTransaction();
		int institutionProfileId = Integer.parseInt(dataSetTest
				.getTable("testCreatePrimaryKey")
				.getValue(0, "institutionProfileId").toString());
		MasterDealPK masterDealPK = masterDeal
				.createPrimaryKey(institutionProfileId);
		assertNotNull(masterDealPK);
		srk.rollbackTransaction();
	}
	
}
