package com.basis100.deal.entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.TestingEnvironmentInitializer;
import com.basis100.deal.pk.AddrPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;
import com.filogix.express.util.TestDataUtil;

import junit.framework.Assert;
import junit.framework.TestCase;
/**
 * 
 * AddrTest
 *
 * @version 1.0 Sep 25, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class AddrTest extends ExpressEntityTestCase implements UnitTestLogging {
	
 // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(AddrTest.class);
    // the session resource kit.
    private SessionResourceKit  _srk;

    int                         addrId;
    int                         copyId;
    int                         instProfId;
    int                         provinceId;
    String                      addrLine1;
    String                      addrLine2;
    String                      city;


    /**
     * setting up the testing env.
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
        
        // get input data from repository
        addrId = _dataRepository.getInt("ADDR", "ADDRID", 0);
        copyId = _dataRepository.getInt("ADDR", "copyId", 0);
        instProfId = _dataRepository.getInt("ADDR", "INSTITUTIONPROFILEID", 0);
        provinceId = _dataRepository.getInt("ADDR", "PROVINCEID", 0);
        addrLine1 = _dataRepository.getString("ADDR", "ADDRESSLINE1", 0);
        addrLine2 = _dataRepository.getString("ADDR", "ADDRESSLINE2", 0);
        city = _dataRepository.getString("ADDR", "CITY", 0);

    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test the find by primary key
     * @throws Exception
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        _logger.info(BORDER_START, "findByPrimaryKey");

        _srk.getExpressState().setDealInstitutionId(instProfId);
        Addr entity = new Addr(_srk);

        entity = entity.findByPrimaryKey(new AddrPK(addrId, copyId));

        assertEquals(addrId, entity.getAddrId());
        assertEquals(copyId, entity.getCopyId());
        assertEquals(instProfId, entity.getInstProfileId());
        assertEquals(provinceId, entity.getProvinceId());
        assertEquals(addrLine1, entity.getAddressLine1());
        assertEquals(addrLine2, entity.getAddressLine2());
        assertEquals(city, entity.getCity());
        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * Test the create
     * @throws Exception
     */
    @Test
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create");
        _srk.getExpressState().setDealInstitutionId(instProfId);

        Addr entity = new Addr(_srk);
        _srk.beginTransaction();
        entity = entity.create();
        entity.ejbStore();

        // assertEquals(addrId, entity.getAddrId());
        // assertEquals(copyId, entity.getCopyId());
        // assertEquals(instProfId, entity.getInstProfileId());
        // assertEquals(provinceId, entity.getProvinceId());
        // assertEquals(addrLine1, entity.getAddressLine1());
        // assertEquals(addrLine2, entity.getAddressLine2());
        // assertEquals(city, entity.getCity());
        _srk.rollbackTransaction();
        _logger.info(BORDER_END, "Create");
    }

}
