/*
 * @(#) PropertyExpenseTypeTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.PropertyExpenseTypePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * @author amalhi
 * 
 * JUnit test for entity PropertyExpenseType
 * 
 */
public class PropertyExpenseTypeTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(PropertyExpenseTypeTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public PropertyExpenseTypeTest() {

    }

    /**
     * To be executed before test
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env and free resources
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository

        int propertyExpenseTypeId = _dataRepository.getInt(
                "PropertyExpenseType", "propertyExpenseTypeId", 0);
        String petDescription = _dataRepository.getString(
                "PropertyExpenseType", "petDescription", 0);

        _logger.info(BORDER_START, "findByPrimaryKey");
        _logger.info(
                "Got Values from repository for PropertyExpenseType ::: [{}]",
                propertyExpenseTypeId + "::" + petDescription);

        // Set VPD context
        _srk.getExpressState();

        // Create the initial entity
        PropertyExpenseType _propertyExpenseType = new PropertyExpenseType(_srk);

        // Find by primary key
        _propertyExpenseType = _propertyExpenseType
                .findByPrimaryKey(new PropertyExpenseTypePK(
                        propertyExpenseTypeId));

        _logger.info("Got Values from new found PropertyExpenseType ::: [{}]",
                _propertyExpenseType.getPropertyExpenseTypeId() + "::"
                        + _propertyExpenseType.getPETDescription());

        // Check if the data retrieved matches the DB
        assertTrue(_propertyExpenseType.getPropertyExpenseTypeId() == propertyExpenseTypeId);
        assertTrue(_propertyExpenseType.getPETDescription().equals(
                petDescription));

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * test create method of entity
     */
    @Ignore
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create");

        // get value from data repository
        String petDescription = _dataRepository.getString(
                "PropertyExpenseType", "petDescription", 0);
        float GDSInclusion = _dataRepository.getInt("PropertyExpenseType",
                "GDSInclusion", 0);
        float TDSInclusion = _dataRepository.getInt("PropertyExpenseType",
                "TDSInclusion", 0);
        int tempPropertyExpenseTypeId = 7;

        // Set VPD context
        _srk.getExpressState();

        // begin transaction
        _srk.beginTransaction();

        // Create the initial entity
        PropertyExpenseType newEntity = new PropertyExpenseType(_srk);
        // newEntity.setPropertyExpenseTypeId(tempPropertyExpenseTypeId);

        _logger.info(" PropertyExpenseType ID IS ::: [{}]", newEntity
                .getPropertyExpenseTypeId());

        // call create method of entity
        newEntity = newEntity.create(tempPropertyExpenseTypeId, petDescription,
                GDSInclusion, TDSInclusion);

        _logger.info("CREATED PropertyExpenseType ::: [{}]", newEntity
                .getPropertyExpenseTypeId()
                + "::" + newEntity.getPETDescription());

        // check create correctly by calling findByPrimaryKey
        PropertyExpenseType foundEntity = new PropertyExpenseType(_srk);

        // Find by primary key
        foundEntity = foundEntity.findByPrimaryKey(new PropertyExpenseTypePK(
                tempPropertyExpenseTypeId));

        // verifying the created entity
        assertEquals(foundEntity.getPropertyExpenseTypeId(), newEntity
                .getPropertyExpenseTypeId());
        assertEquals(foundEntity.getPETDescription(), newEntity
                .getPETDescription());

        _logger.info(BORDER_END, "Create");

        // roll-back transaction.
        _srk.rollbackTransaction();

    }
}
