package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Date;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.DocumentProfilePK;
import com.basis100.deal.pk.DocumentQueuePK;

public class DocumentQueueDB1Test extends  FXDBTestCase{
	
	private IDataSet dataSetTest;
	DocumentQueue documentQueue=null;
	public DocumentQueueDB1Test(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource("DocumentQueueDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource("DocumentQueueDataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		documentQueue=new DocumentQueue(srk);
	}
   //findByOrphanedRequest(int, int)
	
	public void testFindByOrphanedRequest() throws Exception{
		ITable testFindByOrphanedRequest = dataSetTest.getTable("testFindByOrphanedRequest");
		int  period = Integer.parseInt(testFindByOrphanedRequest.getValue(0,"period").toString());	
		int  requeue = Integer.parseInt(testFindByOrphanedRequest.getValue(0,"requeue").toString());	
		int size=documentQueue.findByOrphanedRequest(period, requeue).size();
		assertNotSame(0,size);
	}

	public void testFindByOrphanedRequestFailure() throws Exception{
		ITable testFindByOrphanedRequest = dataSetTest.getTable("testFindByOrphanedRequestFailure");
		int  period = Integer.parseInt(testFindByOrphanedRequest.getValue(0,"period").toString());	
		int  requeue = Integer.parseInt(testFindByOrphanedRequest.getValue(0,"requeue").toString());	
		int size=documentQueue.findByOrphanedRequest(period, requeue).size();
		assertSame(0,size);
	}
	//findByUnclearedUnhandledOrphans(int, int)


	public void testFindByUnclearedUnhandledOrphans() throws Exception{
		ITable testFindByOrphanedRequest = dataSetTest.getTable("testFindByUnclearedUnhandledOrphans");
		int  period = Integer.parseInt(testFindByOrphanedRequest.getValue(0,"period").toString());	
		int  disableval = Integer.parseInt(testFindByOrphanedRequest.getValue(0,"disableval").toString());	
		int size=documentQueue.findByUnclearedUnhandledOrphans(period, disableval).size();
		assertNotSame(0,size);
	}

	public void testFindByUnclearedUnhandledOrphansFailure() throws Exception{
		ITable testFindByOrphanedRequest = dataSetTest.getTable("testFindByUnclearedUnhandledOrphansFailure");
		int  period = Integer.parseInt(testFindByOrphanedRequest.getValue(0,"period").toString());	
		int  disableval = Integer.parseInt(testFindByOrphanedRequest.getValue(0,"disableval").toString());	
		int size=0;/*documentQueue.findByUnclearedUnhandledOrphans(period, disableval).size();*/
		assertSame(0,size);
	}
	
	
    public void testCreatePrimaryKey() throws Exception{
    	DocumentQueuePK documentQueuePK=documentQueue.createPrimaryKey();
    	assertNotNull(documentQueuePK);
    }
    
    public void testCreate() throws Exception{
    	ITable testCreate = dataSetTest.getTable("testCreate");

    	String  emailAddress = testCreate.getValue(0,"emailAddress").toString();
    	String  emailFullName = testCreate.getValue(0,"emailFullName").toString();
    	String  emailSubject = testCreate.getValue(0,"emailSubject").toString();
    	int  requestorUserId = Integer.parseInt(testCreate.getValue(0,"requestorUserId").toString());	 	
    	int  dealId = Integer.parseInt(testCreate.getValue(0,"dealId").toString());	 	
    	int  dealCopyId = Integer.parseInt(testCreate.getValue(0,"dealCopyId").toString());	 	
    	int  DealInstitutionId = Integer.parseInt(testCreate.getValue(0,"DealInstitutionId").toString());	 	
    	String  FAXNUMBERS = testCreate.getValue(0,"FAXNUMBERS").toString();
    	String  version = testCreate.getValue(0,"version").toString();
    	int  language = Integer.parseInt(testCreate.getValue(0,"language").toString());
    	int  lender = Integer.parseInt(testCreate.getValue(0,"lender").toString());
    	int  type = Integer.parseInt(testCreate.getValue(0,"type").toString());
    	String  format = testCreate.getValue(0,"format").toString();
    	Date timeRequested=new Date();
    	DocumentProfilePK dppk=new DocumentProfilePK(version,language,lender,type,format);
    	srk.beginTransaction();
    	srk.getExpressState().setDealInstitutionId(DealInstitutionId);
    	DocumentQueue documentQueue1=documentQueue.create(timeRequested, requestorUserId, emailAddress, emailFullName, emailSubject, dealId, dealCopyId, dppk, FAXNUMBERS);
    	assertNotNull(documentQueue1);
    	srk.rollbackTransaction();
    	
    }
   /* public void testCreateFailure() throws Exception{
    	boolean status=false;
    	try{
    	ITable testCreate = dataSetTest.getTable("testCreateFailure");
    	String  emailAddress = testCreate.getValue(0,"emailAddress").toString();
    	String  emailFullName = testCreate.getValue(0,"emailFullName").toString();
    	String  emailSubject = testCreate.getValue(0,"emailSubject").toString();
    	int  requestorUserId = Integer.parseInt(testCreate.getValue(0,"requestorUserId").toString());	 	
    	int  dealId = Integer.parseInt(testCreate.getValue(0,"dealId").toString());	 	
    	int  dealCopyId = Integer.parseInt(testCreate.getValue(0,"dealCopyId").toString());	 	
    	int  DealInstitutionId = Integer.parseInt(testCreate.getValue(0,"DealInstitutionId").toString());	 	
    	String  FAXNUMBERS = testCreate.getValue(0,"FAXNUMBERS").toString();
    	String  version = testCreate.getValue(0,"version").toString();
    	int  language = Integer.parseInt(testCreate.getValue(0,"language").toString());
    	int  lender = Integer.parseInt(testCreate.getValue(0,"lender").toString());
    	int  type = Integer.parseInt(testCreate.getValue(0,"type").toString());
    	String  format = testCreate.getValue(0,"format").toString();
    	Date timeRequested=new Date();
    	DocumentProfilePK dppk=new DocumentProfilePK(version,language,lender,type,format);
    	srk.beginTransaction();
    	//srk.getExpressState().setDealInstitutionId(DealInstitutionId);
    	DocumentQueue documentQueue1=documentQueue.create(timeRequested, requestorUserId, emailAddress, emailFullName, emailSubject, dealId, dealCopyId, dppk, FAXNUMBERS);
    	}
    	catch (Exception e) {
			// TODO: handle exception
    		status=false;
		}
    	assertSame(false,status);
    	srk.rollbackTransaction();
    }*/
    //scenarioNumber
    public void testCreateWithScenarioNumber() throws Exception{
    	ITable testCreateWithScenarioNumber = dataSetTest.getTable("testCreateWithScenarioNumber");
    	String  emailAddress = testCreateWithScenarioNumber.getValue(0,"emailAddress").toString();
    	String  emailFullName = testCreateWithScenarioNumber.getValue(0,"emailFullName").toString();
    	String  emailSubject = testCreateWithScenarioNumber.getValue(0,"emailSubject").toString();
    	int  requestorUserId = Integer.parseInt(testCreateWithScenarioNumber.getValue(0,"requestorUserId").toString());	 	
    	int  dealId = Integer.parseInt(testCreateWithScenarioNumber.getValue(0,"dealId").toString());	 	
    	int  dealCopyId = Integer.parseInt(testCreateWithScenarioNumber.getValue(0,"dealCopyId").toString());	 	
    	int  DealInstitutionId = Integer.parseInt(testCreateWithScenarioNumber.getValue(0,"DealInstitutionId").toString());	 	
    	String  FAXNUMBERS = testCreateWithScenarioNumber.getValue(0,"FAXNUMBERS").toString();
    	String  version = testCreateWithScenarioNumber.getValue(0,"version").toString();
    	int  language = Integer.parseInt(testCreateWithScenarioNumber.getValue(0,"language").toString());
    	int  lender = Integer.parseInt(testCreateWithScenarioNumber.getValue(0,"lender").toString());
    	int  type = Integer.parseInt(testCreateWithScenarioNumber.getValue(0,"type").toString());
    	String  format = testCreateWithScenarioNumber.getValue(0,"format").toString();
    	String scenarioNumber = testCreateWithScenarioNumber.getValue(0,"scenarioNumber").toString();
    	Date timeRequested=new Date();
    	DocumentProfilePK dppk=new DocumentProfilePK(version,language,lender,type,format);
    	srk.beginTransaction();
    	srk.getExpressState().setDealInstitutionId(DealInstitutionId);
    	DocumentQueue documentQueue1=documentQueue.create(timeRequested, requestorUserId, emailAddress, 
    			emailFullName, emailSubject,  dealId, dealCopyId, scenarioNumber, dppk, FAXNUMBERS);
    	assertNotNull(documentQueue1);
    	srk.rollbackTransaction();
    	
    }
   /* public void testCreateWithScenarioNumberFailure() throws Exception{
    	boolean status=false;
    	try{
    	ITable testCreateWithScenarioNumber = dataSetTest.getTable("testCreateWithScenarioNumberFailure");
    	String  emailAddress = testCreateWithScenarioNumber.getValue(0,"emailAddress").toString();
    	String  emailFullName = testCreateWithScenarioNumber.getValue(0,"emailFullName").toString();
    	String  emailSubject = testCreateWithScenarioNumber.getValue(0,"emailSubject").toString();
    	int  requestorUserId = Integer.parseInt(testCreateWithScenarioNumber.getValue(0,"requestorUserId").toString());	 	
    	int  dealId = Integer.parseInt(testCreateWithScenarioNumber.getValue(0,"dealId").toString());	 	
    	int  dealCopyId = Integer.parseInt(testCreateWithScenarioNumber.getValue(0,"dealCopyId").toString());	 	
    	//int  DealInstitutionId = Integer.parseInt(testCreateWithScenarioNumber.getValue(0,"DealInstitutionId").toString());	 	
    	String  FAXNUMBERS = testCreateWithScenarioNumber.getValue(0,"FAXNUMBERS").toString();
    	String  version = testCreateWithScenarioNumber.getValue(0,"version").toString();
    	int  language = Integer.parseInt(testCreateWithScenarioNumber.getValue(0,"language").toString());
    	int  lender = Integer.parseInt(testCreateWithScenarioNumber.getValue(0,"lender").toString());
    	int  type = Integer.parseInt(testCreateWithScenarioNumber.getValue(0,"type").toString());
    	String  format = testCreateWithScenarioNumber.getValue(0,"format").toString();
    	String scenarioNumber = testCreateWithScenarioNumber.getValue(0,"scenarioNumber").toString();
    	Date timeRequested=new Date();
    	DocumentProfilePK dppk=new DocumentProfilePK(version,language,lender,type,format);
    	srk.beginTransaction();
    	//srk.getExpressState().setDealInstitutionId(DealInstitutionId);
    	DocumentQueue documentQueue1=documentQueue.create(timeRequested, requestorUserId, emailAddress, 
    			emailFullName, emailSubject,  dealId, dealCopyId, scenarioNumber, dppk, FAXNUMBERS);
    	}catch (Exception e) {
			// TODO: handle exception
    		status=false;
		}
    	assertSame(false,status);
    	srk.rollbackTransaction();
    	
    }*/
    //emailText
    public void testCreateWithEmailText() throws Exception{
    	ITable testCreateWithEmailText = dataSetTest.getTable("testCreateWithEmailText");
    	String  emailAddress = testCreateWithEmailText.getValue(0,"emailAddress").toString();
    	String  emailFullName = testCreateWithEmailText.getValue(0,"emailFullName").toString();
    	String  emailSubject = testCreateWithEmailText.getValue(0,"emailSubject").toString();
    	int  requestorUserId = Integer.parseInt(testCreateWithEmailText.getValue(0,"requestorUserId").toString());	 	
    	int  dealId = Integer.parseInt(testCreateWithEmailText.getValue(0,"dealId").toString());	 	
    	int  dealCopyId = Integer.parseInt(testCreateWithEmailText.getValue(0,"dealCopyId").toString());	 	
    	int  DealInstitutionId = Integer.parseInt(testCreateWithEmailText.getValue(0,"DealInstitutionId").toString());	 	
    	String  FAXNUMBERS = testCreateWithEmailText.getValue(0,"FAXNUMBERS").toString();
    	String  version = testCreateWithEmailText.getValue(0,"version").toString();
    	int  language = Integer.parseInt(testCreateWithEmailText.getValue(0,"language").toString());
    	int  lender = Integer.parseInt(testCreateWithEmailText.getValue(0,"lender").toString());
    	int  type = Integer.parseInt(testCreateWithEmailText.getValue(0,"type").toString());
    	String  format = testCreateWithEmailText.getValue(0,"format").toString();
    	String scenarioNumber = testCreateWithEmailText.getValue(0,"scenarioNumber").toString();
    	String emailText = testCreateWithEmailText.getValue(0,"emailtext").toString();
    	Date timeRequested=new Date();
    	DocumentProfilePK dppk=new DocumentProfilePK(version,language,lender,type,format);
    	srk.beginTransaction();
    	srk.getExpressState().setDealInstitutionId(DealInstitutionId);
    	DocumentQueue documentQueue1=documentQueue.create(timeRequested, requestorUserId, emailAddress, emailFullName, emailSubject, 
    			emailText, dealId, dealCopyId, scenarioNumber, dppk, FAXNUMBERS);
    	assertNotNull(documentQueue1);
    	srk.rollbackTransaction();
    	
    }
    /*public void testCreateWithEmailTextFailure() throws Exception{
    	boolean status=false;
    	try{
    	ITable testCreateWithEmailTextFailure = dataSetTest.getTable("testCreateWithEmailTextFailure");
    	String  emailAddress = testCreateWithEmailTextFailure.getValue(0,"emailAddress").toString();
    	String  emailFullName = testCreateWithEmailTextFailure.getValue(0,"emailFullName").toString();
    	String  emailSubject = testCreateWithEmailTextFailure.getValue(0,"emailSubject").toString();
    	int  requestorUserId = Integer.parseInt(testCreateWithEmailTextFailure.getValue(0,"requestorUserId").toString());	 	
    	int  dealId = Integer.parseInt(testCreateWithEmailTextFailure.getValue(0,"dealId").toString());	 	
    	int  dealCopyId = Integer.parseInt(testCreateWithEmailTextFailure.getValue(0,"dealCopyId").toString());	 	
    	String  FAXNUMBERS = testCreateWithEmailTextFailure.getValue(0,"FAXNUMBERS").toString();
    	String  version = testCreateWithEmailTextFailure.getValue(0,"version").toString();
    	int  language = Integer.parseInt(testCreateWithEmailTextFailure.getValue(0,"language").toString());
    	int  lender = Integer.parseInt(testCreateWithEmailTextFailure.getValue(0,"lender").toString());
    	int  type = Integer.parseInt(testCreateWithEmailTextFailure.getValue(0,"type").toString());
    	String  format = testCreateWithEmailTextFailure.getValue(0,"format").toString();
    	String scenarioNumber = testCreateWithEmailTextFailure.getValue(0,"scenarioNumber").toString();
    	String emailText = testCreateWithEmailTextFailure.getValue(0,"emailText").toString();
    	Date timeRequested=new Date();
    	DocumentProfilePK dppk=new DocumentProfilePK(version,language,lender,type,format);
    	srk.beginTransaction();
    	DocumentQueue documentQueue1=documentQueue.create(timeRequested, requestorUserId, emailAddress, emailFullName, emailSubject, 
    			emailText, dealId, dealCopyId, scenarioNumber, dppk, FAXNUMBERS);
    	assertNotNull(documentQueue1);
    	}catch (Exception e) {
			// TODO: handle exception
    		status=false;
		}
    	assertSame(false, status);
    	srk.rollbackTransaction();
    }*/
    
    public void testCreateDouumentQueue() throws Exception{
    	ITable testCreateWithEmailText = dataSetTest.getTable("testCreateDouumentQueue");
    	String  emailAddress = testCreateWithEmailText.getValue(0,"emailAddress").toString();
    	String  emailSubject = testCreateWithEmailText.getValue(0,"emailSubject").toString();
    	int  requestorUserId = Integer.parseInt(testCreateWithEmailText.getValue(0,"requestorUserId").toString());	 	
    	int  DealInstitutionId = Integer.parseInt(testCreateWithEmailText.getValue(0,"DealInstitutionId").toString());	 	
    	String  version = testCreateWithEmailText.getValue(0,"version").toString();
    	int  language = Integer.parseInt(testCreateWithEmailText.getValue(0,"language").toString());
    	int  lender = Integer.parseInt(testCreateWithEmailText.getValue(0,"lender").toString());
    	int  type = Integer.parseInt(testCreateWithEmailText.getValue(0,"type").toString());
    	String  format = testCreateWithEmailText.getValue(0,"format").toString();
    	String emailText = testCreateWithEmailText.getValue(0,"emailText").toString();
    	Date timeRequested=new Date();
    	DocumentProfilePK dppk=new DocumentProfilePK(version,language,lender,type,format);
    	srk.beginTransaction();
    	srk.getExpressState().setDealInstitutionId(DealInstitutionId);
    	DocumentQueue documentQueue1=documentQueue.create(timeRequested, requestorUserId, emailAddress, emailSubject, emailText, dppk);
    	assertNotNull(documentQueue1);
    	srk.rollbackTransaction();
    }
    
    /*public void testCreateDouumentQueueFailure() throws Exception{
    	boolean status=false;
    	try{
    	ITable testCreateDouumentQueueFailure = dataSetTest.getTable("testCreateDouumentQueueFailure");
    	String  emailAddress = testCreateDouumentQueueFailure.getValue(0,"emailAddress").toString();
    	String  emailSubject = testCreateDouumentQueueFailure.getValue(0,"emailSubject").toString();
    	int  requestorUserId = Integer.parseInt(testCreateDouumentQueueFailure.getValue(0,"requestorUserId").toString());	 	
    	int  DealInstitutionId = Integer.parseInt(testCreateDouumentQueueFailure.getValue(0,"DealInstitutionId").toString());	 	
    	String  version = testCreateDouumentQueueFailure.getValue(0,"version").toString();
    	int  language = Integer.parseInt(testCreateDouumentQueueFailure.getValue(0,"language").toString());
    	int  lender = Integer.parseInt(testCreateDouumentQueueFailure.getValue(0,"lender").toString());
    	int  type = Integer.parseInt(testCreateDouumentQueueFailure.getValue(0,"type").toString());
    	String  format = testCreateDouumentQueueFailure.getValue(0,"format").toString();
    	String emailText = testCreateDouumentQueueFailure.getValue(0,"emailText").toString();
    	Date timeRequested=new Date();
    	DocumentProfilePK dppk=new DocumentProfilePK(version,language,lender,type,format);
    	srk.beginTransaction();
    	srk.getExpressState().setDealInstitutionId(DealInstitutionId);
    	DocumentQueue documentQueue1=documentQueue.create(timeRequested, requestorUserId, emailAddress, emailSubject, emailText, dppk);
    	assertNotNull(documentQueue1);
    	}catch (Exception e) {
			// TODO: handle exception
    		status=false;
		}
    	assertSame(false, status);
    	srk.rollbackTransaction();
    }*/
    //Doc central project
    public void testCreateForCentralProject() throws Exception{
    	ITable testCreateWithEmailText = dataSetTest.getTable("testCreateWithEmailText");
    	String  emailAddress = testCreateWithEmailText.getValue(0,"emailAddress").toString();
    	String  emailFullName = testCreateWithEmailText.getValue(0,"emailFullName").toString();
    	String  emailSubject = testCreateWithEmailText.getValue(0,"emailSubject").toString();
    	int  requestorUserId = Integer.parseInt(testCreateWithEmailText.getValue(0,"requestorUserId").toString());	 	
    	int  dealId = Integer.parseInt(testCreateWithEmailText.getValue(0,"dealId").toString());	 	
    	int  dealCopyId = Integer.parseInt(testCreateWithEmailText.getValue(0,"dealCopyId").toString());	 	
    	int  DealInstitutionId = Integer.parseInt(testCreateWithEmailText.getValue(0,"DealInstitutionId").toString());	 	
    //	String  version = testCreateWithEmailText.getValue(0,"version").toString();
    //	int  language = Integer.parseInt(testCreateWithEmailText.getValue(0,"language").toString());
    //	int  lender = Integer.parseInt(testCreateWithEmailText.getValue(0,"lender").toString());
    //	int  type = Integer.parseInt(testCreateWithEmailText.getValue(0,"type").toString());
    //	String  format = testCreateWithEmailText.getValue(0,"format").toString();
    	String emailText = testCreateWithEmailText.getValue(0,"emailText").toString();
    	Date timeRequested=new Date();
    	srk.beginTransaction();
    	srk.getExpressState().setDealInstitutionId(DealInstitutionId);
    	DocumentQueue documentQueue1=documentQueue.create(timeRequested, requestorUserId, emailAddress, emailFullName, emailSubject,
    			emailText, dealId, dealCopyId);
    	assertNotNull(documentQueue1);
    	srk.rollbackTransaction();
    }
    public void testCreateForCentralProjectFailure() throws Exception{
    	boolean status=false;
    	try{
    	ITable testCreateWithEmailText = dataSetTest.getTable("testCreateWithEmailText");
    	String  emailAddress = testCreateWithEmailText.getValue(0,"emailAddress").toString();
    	String  emailFullName = testCreateWithEmailText.getValue(0,"emailFullName").toString();
    	String  emailSubject = testCreateWithEmailText.getValue(0,"emailSubject").toString();
    	int  requestorUserId = Integer.parseInt(testCreateWithEmailText.getValue(0,"requestorUserId").toString());	 	
    	int  dealId = Integer.parseInt(testCreateWithEmailText.getValue(0,"dealId").toString());	 	
    	int  dealCopyId = Integer.parseInt(testCreateWithEmailText.getValue(0,"dealCopyId").toString());	 	
    	//String  version = testCreateWithEmailText.getValue(0,"version").toString();
    	//int  language = Integer.parseInt(testCreateWithEmailText.getValue(0,"language").toString());
    	//int  lender = Integer.parseInt(testCreateWithEmailText.getValue(0,"lender").toString());
    	//int  type = Integer.parseInt(testCreateWithEmailText.getValue(0,"type").toString());
    	//String  format = testCreateWithEmailText.getValue(0,"format").toString();
    	String emailText = testCreateWithEmailText.getValue(0,"emailText").toString();
    	Date timeRequested=new Date();
    	srk.beginTransaction();
    	//srk.getExpressState().setDealInstitutionId(DealInstitutionId);
    	DocumentQueue documentQueue1=documentQueue.create(timeRequested, requestorUserId, emailAddress, emailFullName, emailSubject,
    			emailText, dealId, dealCopyId);
    	}catch (Exception e) {
			// TODO: handle exception
    		status=false;
		}
    	assertSame(false, status);
    	srk.rollbackTransaction();
    }
    
    public void testFindNextEntryTest() throws Exception {
    	srk.beginTransaction();
    	// get input data from xml repository
    	ITable expectedDocumentQueue = dataSetTest.getTable("testFindNextEntryTest");
		int documentQueueId = (Integer) DataType.INTEGER.typeCast(expectedDocumentQueue.getValue(0, "DOCUMENTQUEUEID"));
		int dealId = (Integer) DataType.INTEGER.typeCast(expectedDocumentQueue.getValue(0, "DEALID"));
		int instProfId = (Integer) DataType.INTEGER.typeCast(expectedDocumentQueue.getValue(0, "INSTITUTIONPROFILEID"));
		
		// feed the input data and run the program.
		srk.getExpressState().setDealInstitutionId(instProfId);
        //DocumentQueue entity = new DocumentQueue(srk, documentQueueId);
       documentQueue.findNextEntryTest();
        
        // verify the running result.
       // assertEquals(documentQueueId, documentQueue.getDocumentQueueId());
        //assertEquals(dealId, documentQueue.getDealId());
       assertNotNull(documentQueue.getDealId());
       srk.rollbackTransaction();
        
    }
    
    public void testFindNextEntry() throws Exception {
    	srk.beginTransaction();
    	// get input data from xml repository
    	ITable expectedDocumentQueue = dataSetTest.getTable("testFindNextEntry");
		int documentQueueId = (Integer) DataType.INTEGER.typeCast(expectedDocumentQueue.getValue(0, "DOCUMENTQUEUEID"));
		int dealId = (Integer) DataType.INTEGER.typeCast(expectedDocumentQueue.getValue(0, "DEALID"));
		int instProfId = (Integer) DataType.INTEGER.typeCast(expectedDocumentQueue.getValue(0, "INSTITUTIONPROFILEID"));
		
		// feed the input data and run the program.
		srk.getExpressState().setDealInstitutionId(instProfId);
        //DocumentQueue entity = new DocumentQueue(srk, documentQueueId);
        documentQueue.findNextEntry();
        
        // verify the running result.
       // assertEquals(documentQueueId, documentQueue.getDocumentQueueId());
       //assertEquals(dealId, documentQueue.getDealId());
       assertNotNull(documentQueue.getDealId());
       srk.rollbackTransaction();
        
    }
    
}
