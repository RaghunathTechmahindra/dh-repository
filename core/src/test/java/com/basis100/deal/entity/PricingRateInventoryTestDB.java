package com.basis100.deal.entity;

import java.io.IOException;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.AppraisalOrderPK;
import com.basis100.deal.pk.ArchivedLoanApplicationPK;
import com.basis100.deal.pk.ArchivedLoanDecisionPK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PricingProfilePK;
import com.basis100.deal.pk.PricingRateInventoryPK;
import com.basis100.deal.pk.PrimeIndexRateInventoryPK;
import com.basis100.deal.pk.PrimeIndexRateProfilePK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.deal.pk.SourceOfBusinessProfilePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.util.EntityTestUtil;

public class PricingRateInventoryTestDB extends FXDBTestCase{

	private IDataSet dataSetTest;
	private PricingRateInventory pricingRateInventory;
	private Component component;
	private Deal deal;
	public PricingRateInventoryTestDB(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(PricingRateInventory.class.getSimpleName() + "DataSetTest.xml"));
	}


	
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	
	@Override
	protected void setUp() throws Exception {
		super.setUp();		
		pricingRateInventory = new PricingRateInventory(srk);
	}	
	
	public void testFindByPricingProfileIdSuccess() throws Exception{
		try {
			Collection cList=null;
			srk.beginTransaction();
			ITable testFindByPricingProfile = dataSetTest.getTable("testFindByPricingProfileIdSuccess");					
			int pricingProfileId=Integer.valueOf((String)testFindByPricingProfile.getValue(0,"PRICINGPROFILEID"));	
			srk.getExpressState().setDealInstitutionId(1);
			cList=pricingRateInventory.findByPricingProfileId(pricingProfileId);
			if(cList!=null){
		    assertNotNull(cList);	
			}			
		    srk.rollbackTransaction();
		    
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
		
	public void testFindByPricingProfileIdFailure() throws Exception{
		try {
			Collection cList=null;
			srk.beginTransaction();
			ITable testFindByPricingProfile = dataSetTest.getTable("testFindByPricingProfileIdFailure");					
			int pricingProfileId=Integer.valueOf((String)testFindByPricingProfile.getValue(0,"PRICINGPROFILEID"));	
			srk.getExpressState().setDealInstitutionId(1);
			cList=pricingRateInventory.findByPricingProfileId(pricingProfileId);
			if(cList!=null){
		    assertEquals(0, cList.size());				
			}			
		    srk.rollbackTransaction();		    
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
	
	public void testFindByComponentAndDateSuccess() throws Exception{
		try {			
			srk.beginTransaction();
			ITable testFindByComponent = dataSetTest.getTable("testFindByComponentAndDateSuccess");					
			int pricingProfile=Integer.valueOf((String)testFindByComponent.getValue(0,"PRICINGRATEINVENTORYID"));
			int componentId=Integer.valueOf((String)testFindByComponent.getValue(0,"COMPONENTID"));
			int copyId=Integer.valueOf((String)testFindByComponent.getValue(0,"COPYID"));
			String indexEffectiveDate=(String)testFindByComponent.getValue(0,"INDEXEFFECTIVEDATE");
			SimpleDateFormat formate=new SimpleDateFormat("dd-MMM-yy");
			Date date=formate.parse(indexEffectiveDate);			
		    component=new Component(srk,null,componentId,copyId);
			srk.getExpressState().setDealInstitutionId(1);
			pricingRateInventory= pricingRateInventory.findByComponentAndDate(component,date);
			assertEquals(componentId, component.getComponentId());
		    srk.rollbackTransaction();
		    
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
	
	public void testFindByComponentAndDateFailure() throws Exception{
		try {			
			srk.beginTransaction();
			ITable testFindByComponent = dataSetTest.getTable("testFindByComponentAndDateFailure");					
			int pricingProfile=Integer.valueOf((String)testFindByComponent.getValue(0,"PRICINGRATEINVENTORYID"));
			int componentId=Integer.valueOf((String)testFindByComponent.getValue(0,"COMPONENTID"));
			int copyId=Integer.valueOf((String)testFindByComponent.getValue(0,"COPYID"));
			String indexEffectiveDate=(String)testFindByComponent.getValue(0,"INDEXEFFECTIVEDATE");
			SimpleDateFormat formate=new SimpleDateFormat("dd-MMM-yy");
			Date date=formate.parse(indexEffectiveDate);			
		    component=new Component(srk,null,componentId,copyId);
			srk.getExpressState().setDealInstitutionId(1);
			pricingRateInventory= pricingRateInventory.findByComponentAndDate(component,date);
			assertEquals(componentId, component.getComponentId());
		    srk.rollbackTransaction();		    
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}	
	
	public void testFindByDealAndDate() throws Exception{
		try {			
			srk.beginTransaction();
			ITable testFindByDealAndDate = dataSetTest.getTable("testFindByDealAndDate");					
			int pricingProfile=Integer.valueOf((String)testFindByDealAndDate.getValue(0,"PRICINGPROFILEID"));
			int dealId=Integer.valueOf((String)testFindByDealAndDate.getValue(0,"DEALID"));
			int copyId=Integer.valueOf((String)testFindByDealAndDate.getValue(0,"COPYID"));
			String applicationDate=(String)testFindByDealAndDate.getValue(0,"APPLICATIONDATE");
			SimpleDateFormat formate=new SimpleDateFormat("dd-MMM-yy");
			Date date=formate.parse(applicationDate);			
		    deal=new Deal(srk,null,dealId,copyId);
			srk.getExpressState().setDealInstitutionId(1);
			pricingRateInventory= pricingRateInventory.findByDealAndDate(deal,date);
			assertEquals(pricingProfile, deal.getPricingProfileId());
		    srk.rollbackTransaction();		    
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
		
	public void testFindByPricingProfileAndDate() throws Exception{
		try {			
			srk.beginTransaction();
			ITable testFindByDealAndDate = dataSetTest.getTable("testFindByPricingProfileAndDate");					
			int pricingProfile=Integer.valueOf((String)testFindByDealAndDate.getValue(0,"PRICINGPROFILEID"));			
			String applicationDate=(String)testFindByDealAndDate.getValue(0,"INDEXEFFECTIVEDATE");
			SimpleDateFormat formate=new SimpleDateFormat("dd-MMM-yy");
			Date date=formate.parse(applicationDate);		   
			srk.getExpressState().setDealInstitutionId(1);
			pricingRateInventory= pricingRateInventory.findByPricingProfileAndDate(pricingProfile,date);
			assertEquals(pricingProfile, pricingRateInventory.getPricingProfileID());
		    srk.rollbackTransaction();		    
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}	
	public void testFindPriByMinRate() throws Exception{
		try {			
			srk.beginTransaction();
			ITable testFindPriByMinRate = dataSetTest.getTable("testFindPriByMinRate");			
			int componentId=Integer.valueOf((String)testFindPriByMinRate.getValue(0,"COMPONENTID"));
			int copyId=Integer.valueOf((String)testFindPriByMinRate.getValue(0,"COPYID"));
			String indexEffectiveDate=(String)testFindPriByMinRate.getValue(0,"INDEXEFFECTIVEDATE");
			SimpleDateFormat formate=new SimpleDateFormat("dd-MMM-yy");
			Date date=formate.parse(indexEffectiveDate);			
		    component=new Component(srk,null,componentId,copyId);
			srk.getExpressState().setDealInstitutionId(1);
			pricingRateInventory= pricingRateInventory.findPriByMinRate(component,date);
			assertEquals(componentId, component.getComponentId());
		    srk.rollbackTransaction();		    
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}	
	public void testFindPriByMinRateDeal() throws Exception{
		try {			
			srk.beginTransaction();
			ITable testFindPriByMinRate = dataSetTest.getTable("testFindPriByMinRateDeal");	
			
			int pricingProfile=Integer.valueOf((String)testFindPriByMinRate.getValue(0,"PRICINGPROFILEID"));
			int dealId=Integer.valueOf((String)testFindPriByMinRate.getValue(0,"DEALID"));
			int copyId=Integer.valueOf((String)testFindPriByMinRate.getValue(0,"COPYID"));
			String applicationDate=(String)testFindPriByMinRate.getValue(0,"APPLICATIONDATE");
			SimpleDateFormat formate=new SimpleDateFormat("dd-MMM-yy");
			Date date=formate.parse(applicationDate);			
		    deal=new Deal(srk,null,dealId,copyId);			
			srk.getExpressState().setDealInstitutionId(1);
			pricingRateInventory= pricingRateInventory.findPriByMinRate(deal,date);
			assertEquals(pricingProfile, deal.getPricingProfileId());			
		    srk.rollbackTransaction();		    
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
	public void testFindPriByMinRateInt() throws Exception{
		try {			
			srk.beginTransaction();
			ITable testFindPriByMinRate = dataSetTest.getTable("testFindPriByMinRateInt");			
			int pricingProfileId=Integer.valueOf((String)testFindPriByMinRate.getValue(0,"PRICINGPROFILEID"));			
			String indexEffectiveDate=(String)testFindPriByMinRate.getValue(0,"INDEXEFFECTIVEDATE");
			SimpleDateFormat formate=new SimpleDateFormat("dd-MMM-yy");
			Date date=formate.parse(indexEffectiveDate);		    			
			srk.getExpressState().setDealInstitutionId(1);
			pricingRateInventory= pricingRateInventory.findPriByMinRate(pricingProfileId,date);
			assertEquals(pricingProfileId,pricingRateInventory.getPricingProfileID());			
		    srk.rollbackTransaction();		    
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}		
	public void testFindPriByMinRateComponent() throws Exception{
		try {			
			srk.beginTransaction();
			ITable testFindPriByMinRate = dataSetTest.getTable("testFindPriByMinRateComponent");
			int pricingrateinventoryid=Integer.valueOf((String)testFindPriByMinRate.getValue(0,"PRICINGRATEINVENTORYID"));
			int componentId=Integer.valueOf((String)testFindPriByMinRate.getValue(0,"COMPONENTID"));
			int copyId=Integer.valueOf((String)testFindPriByMinRate.getValue(0,"COPYID"));
			String indexEffectiveDate=(String)testFindPriByMinRate.getValue(0,"INDEXEFFECTIVEDATE");
			SimpleDateFormat formate=new SimpleDateFormat("dd-MMM-yy");
			Date date=formate.parse(indexEffectiveDate);			
			String indexExpiryDate=(String)testFindPriByMinRate.getValue(0,"INDEXEXPIRYDATE");
			SimpleDateFormat effrctiveFormate=new SimpleDateFormat("dd-MMM-yy");
			Date expireDate=formate.parse(indexEffectiveDate);	
		    component=new Component(srk,null,componentId,copyId);				    			
			srk.getExpressState().setDealInstitutionId(1);
			pricingRateInventory= pricingRateInventory.findPriByMinRate(component,date,expireDate);				
			assertEquals(componentId,component.getComponentId());			
		    srk.rollbackTransaction();		    
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}	  
	public void testFindByPricingRateInventorySuccess() throws Exception{
		try {			
			srk.beginTransaction();
			ITable testFindByPricingProfile = dataSetTest.getTable("testFindByPricingRateInventorySuccess");					
			int pricingRateInventoryId=Integer.valueOf((String)testFindByPricingProfile.getValue(0,"PRICINGRATEINVENTORYID"));	
			srk.getExpressState().setDealInstitutionId(1);
			pricingRateInventory=pricingRateInventory.findByPricingRateInventory(pricingRateInventoryId);
			assertEquals(pricingRateInventoryId,pricingRateInventory.getPricingRateInventoryID());	
		    srk.rollbackTransaction();		    
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
	
	public void testFindByPricingRateInventoryFailure() throws Exception{
		try {			
			srk.beginTransaction();
			ITable testFindByPricingProfile = dataSetTest.getTable("testFindByPricingRateInventoryFailure");					
			int pricingRateInventoryId=Integer.valueOf((String)testFindByPricingProfile.getValue(0,"PRICINGRATEINVENTORYID"));	
			srk.getExpressState().setDealInstitutionId(1);
			pricingRateInventory=pricingRateInventory.findByPricingRateInventory(pricingRateInventoryId);
			assertNotNull(pricingRateInventory);
		    srk.rollbackTransaction();		    
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
   public void testCreatePrimaryKeySuccess() throws Exception{
		try {			
			srk.beginTransaction();
			ITable testCreatePrimaryKey = dataSetTest.getTable("testCreatePrimaryKeySuccess");					
			int pricingRateInventoryId=Integer.valueOf((String)testCreatePrimaryKey.getValue(0,"PRICINGRATEINVENTORYID"));	
			srk.getExpressState().setDealInstitutionId(1);
			PricingRateInventoryPK pk=new PricingRateInventoryPK(pricingRateInventoryId);
			pk=pricingRateInventory.createPrimaryKey();
			assertEquals(pricingRateInventoryId,pricingRateInventory.getPricingRateInventoryID());	
		    srk.rollbackTransaction();		    
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
	
	public void testCreatePrimaryKeyFailure() throws Exception{
		try {			
			srk.beginTransaction();
			ITable testCreatePrimaryKey = dataSetTest.getTable("testCreatePrimaryKeyFailure");					
			int pricingRateInventoryId=Integer.valueOf((String)testCreatePrimaryKey.getValue(0,"PRICINGRATEINVENTORYID"));	
			srk.getExpressState().setDealInstitutionId(1);
			PricingRateInventoryPK pk=new PricingRateInventoryPK(pricingRateInventoryId);
			pk=pricingRateInventory.createPrimaryKey();
			assertNotNull(pk);
		    srk.rollbackTransaction();		    
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
		} 
	
}
