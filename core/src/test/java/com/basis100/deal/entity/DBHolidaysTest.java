package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;

public class DBHolidaysTest extends FXDBTestCase{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(DBHolidaysTest.class);
    
    //Initial data to be loaded before each test case.
	private final static String INIT_XML_DATA = "DBHolidaysDataSet.xml";
	private IDataSet dataSetTest;
	
	/**
     * Constructor function
     */
    public DBHolidaysTest(String name)  throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource("DBHolidaysDataSetTest.xml"));
    }
    
  
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
        return DatabaseOperation.INSERT;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.DELETE;
    }
    
    /**
     * test find by Name.
     */
    @Test
    public void testFindByName() throws Exception {

    	_logger.info(" Start testFindByName");
    	 
    	 ITable testFindByName = dataSetTest.getTable("testFindByName");
    	String  holidayName = (String)testFindByName.getValue(0,"holidayName");
    	String  holidayName1 = (String)testFindByName.getValue(0,"holidayName1");
  		int  instProfId = Integer.parseInt((String)testFindByName.getValue(0,"instProfId"));
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
        
        Holidays holiday = new Holidays(srk);
        holiday = holiday.findByName(holidayName);
        
        // verify the result.
        assert holiday != null;
        assertEquals(holidayName, holiday.getHolidayName().toUpperCase());
        
        //failure case
        Holidays holiday1 = new Holidays(srk);
        holiday1 = holiday1.findByName(holidayName1);
        assertEquals(holidayName1, holiday1.getHolidayName().toUpperCase());
        
        // verify the result.
        assert holiday1 == null;
        
        _logger.info(" End testFindByName");
    	
    }
    
    /**
     * test find by Holiday Id.
     */
    @Test
    public void testFindByHolidayId() throws Exception {

    	_logger.info(" Start testFindByHolidayId");
    	 
    	ITable testFindByHolidayId = dataSetTest.getTable("testFindByHolidayId");
    	int  holidayId = Integer.parseInt((String)testFindByHolidayId.getValue(0,"holidayId"));
  		int  instProfId = Integer.parseInt((String)testFindByHolidayId.getValue(0,"instProfId"));
  		
  		// Set VPD context
        srk.getExpressState().setDealInstitutionId(instProfId);
        
        Holidays holiday = new Holidays(srk, holidayId);
        Vector<Holidays> holidays = holiday.findByHolidayId();
        
        // verify the result.
        assert holidays != null;
        assert holidays.size() > 0;
        
        
        _logger.info(" End testFindByHolidayId");
    	
    }
    
}
