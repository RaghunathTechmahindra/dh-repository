/*
 * @(#)ScotiabankStepMortgageTest.java    2007-8-15
 *
 * Copyright (C) 2007 Filogix Limited Partnership All rights reserved.
 */


package com.basis100.deal.entity;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.ScotiabankStepMortgagePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * <p>ScotiabankStepMortgageTest</p>
 * Express Entity class unit test: ScotiabankStepMortgage
 */
public class ScotiabankStepMortgageTest extends ExpressEntityTestCase 
    implements UnitTestLogging {
   
    private final static Logger _logger = 
        LoggerFactory.getLogger(RegionProfileTest.class);
    private final static String ENTITY_NAME = "SCOTIABANKSTEPMORTGAGE"; 
     
    // retreived data count that is stored in UnitTestDataRepository.xml
    private static final int DATA_COUNT = 6;
  
    private SessionResourceKit _srk;
  
    // use this value for random sequence number data
    private int testSeqForGen = 0;

    // properties
    protected int           dealId;
    protected int         copyId;
    protected int         stepMortgageId;
    protected int     stepMortgageTypeId;
    protected int     stepProductId;
    protected int     stepMortgageNumber;
    protected double    stepMortgageGlobalLimit;
    protected double    stepMortgageRegistrationAmount;
    protected int     stepMortgageAccountLoanNumber;
    protected double    stepMortgageCreditLimit;
    protected double    stepMortgageAmountAdvanced;
    protected double    stepMortgagePostedRate;
    protected double    stepMortgageCommittedRate;
    protected int     stepMortgageTerm;
    protected int     stepMortgageAmortization;
    protected double    stepMortgagePayment;
    protected double    stepMortgageFindersFee;
    protected String    stepMortgageLOLP;
    protected String    stepMortgageHCP;
    protected double    stepMortgageCashbackPercentage;
    protected int     userProfileId;
    protected Date      dateTimeStamp;
    private int instProfId;
  
    /**
     * <p>setUp</p>
     * Setting up test case.
     */
    @Before
    public void setUp() throws Exception {

        _logger.info(BORDER_START, ENTITY_NAME + " TestCase setting up ...");
  
        //  init session resource kit.
        ResourceManager.init();
        //  init session resource kit.
        _srk = new SessionResourceKit();

      
        // get random seqence number 
        Double indexD = new Double(Math.random() * DATA_COUNT);
        testSeqForGen = indexD.intValue();

        // retreive data from test data file
        dealId 
          = _dataRepository.getInt( ENTITY_NAME, "DEALID", testSeqForGen);   
        copyId 
            = _dataRepository.getInt( ENTITY_NAME, "COPYID", testSeqForGen);   
        stepMortgageId 
            = _dataRepository.getInt( ENTITY_NAME, 
                                      "STEPMORTGAGETYPEID", 
                                      testSeqForGen);
        stepMortgageTypeId 
            = _dataRepository.getInt( ENTITY_NAME, 
                                      "STEPMORTGAGETYPEID", 
                                      testSeqForGen);
        stepProductId 
            = _dataRepository.getInt( ENTITY_NAME, "STEPPRODUCTID", testSeqForGen);
        stepMortgageNumber 
            = _dataRepository.getInt( ENTITY_NAME, 
                                      "STEPMORTGAGENUMBER", 
                                      testSeqForGen);
        stepMortgageGlobalLimit 
            = _dataRepository.getDouble( ENTITY_NAME, 
                                         "STEPMORTGAGEGLOBALLIMIT", 
                                         testSeqForGen);
        stepMortgageRegistrationAmount 
            = _dataRepository.getDouble( ENTITY_NAME, 
                                     "STEPMORTGAGEREGISTRATIONAMOUNT", 
                                     testSeqForGen);   
        stepMortgageAccountLoanNumber 
            = _dataRepository.getInt( ENTITY_NAME, 
                                      "STEPMORTGAGEACCOUNTLOANNUMBER", 
                                      testSeqForGen);
        stepMortgageCreditLimit 
            = _dataRepository.getDouble( ENTITY_NAME, 
                                         "STEPMORTGAGECREDITLIMIT", 
                                         testSeqForGen);   
        stepMortgageAmountAdvanced 
            = _dataRepository.getDouble( ENTITY_NAME, 
                                         "STEPMORTGAGEAMOUNTADVANCED", 
                                         testSeqForGen);   
        stepMortgagePostedRate 
            = _dataRepository.getDouble( ENTITY_NAME, 
                                         "STEPMORTGAGEPOSTEDRATE", 
                                         testSeqForGen);   
        stepMortgageCommittedRate 
            = _dataRepository.getDouble( ENTITY_NAME, 
                                         "STEPMORTGAGECOMMITTEDRATE", 
                                         testSeqForGen);
        stepMortgageTerm 
            = _dataRepository.getInt( ENTITY_NAME, 
                                      "STEPMORTGAGETERM", 
                                      testSeqForGen);   
        stepMortgageAmortization 
            = _dataRepository.getInt( ENTITY_NAME, 
                                      "STEPMORTGAGEAMORTIZATION", 
                                      testSeqForGen);
        stepMortgagePayment 
            = _dataRepository.getDouble( ENTITY_NAME, 
                                         "STEPMORTGAGEPAYMENT", 
                                         testSeqForGen);   
        stepMortgageFindersFee 
            = _dataRepository.getDouble( ENTITY_NAME, 
                                         "STEPMORTGAGEFINDERSFEE", 
                                         testSeqForGen);   
        stepMortgageLOLP 
            = _dataRepository.getString( ENTITY_NAME, 
                                         "STEPMORTGAGELOLP", 
                                         testSeqForGen);   
        stepMortgageHCP 
          = _dataRepository.getString( ENTITY_NAME, 
                                       "STEPMORTGAGEHCP", 
                                       testSeqForGen);   
        stepMortgageCashbackPercentage 
          = _dataRepository.getDouble( ENTITY_NAME, 
                                       "STEPMORTGAGECASHBACKPERCENTAGE", 
                                       testSeqForGen);   
        userProfileId 
            = _dataRepository.getInt( ENTITY_NAME, 
                                      "USERPROFILEID", 
                                      testSeqForGen);   
        dateTimeStamp 
            = _dataRepository.getDate( ENTITY_NAME, 
                                       "DATETIMESTAMP", 
                                       testSeqForGen);   
        instProfId 
            = _dataRepository.getInt( ENTITY_NAME, 
                                      "INSTITUTIONPROFILEID", 
                                      testSeqForGen);          
     
        _logger.info(BORDER_END, "Setting up Done");
    }

    @After
    public void tearDown() {
      
      //      free resources
      _srk.freeResources( );
  
    }

    /**
     * test findByPrimaryKey.
     */
    @Ignore
    @Test
    public void testFindByPrimaryKey()  throws Exception {
      
        _logger.info(BORDER_START, "findByPrimaryKey");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
        
        // excute method
        ScotiabankStepMortgage entity = new ScotiabankStepMortgage(_srk);
        entity 
            = entity.findByPrimaryKey(new ScotiabankStepMortgagePK(stepMortgageId, 
                                                                   dealId, copyId));
        
        // assertEquals
        assertEquals(entity.getDealId(), dealId);
        assertEquals(entity.getCopyId(), copyId);
        assertEquals(entity.getStepMortgageId(), stepMortgageId);
        assertEquals(entity.getStepMortgageTypeId(), stepMortgageTypeId);
        assertEquals(entity.getStepProductId(), stepProductId);
        assertEquals(entity.getStepMortgageNumber(), stepMortgageNumber);
        assertEquals(entity.getStepMortgageGlobalLimit(), 
                     stepMortgageGlobalLimit);
        assertEquals(entity.getStepMortgageRegistrationAmount(), 
                     stepMortgageRegistrationAmount);
        assertEquals(entity.getStepMortgageAccountLoanNumber(), 
                     stepMortgageAccountLoanNumber);
        assertEquals(entity.getStepMortgageCreditLimit(), 
                     stepMortgageCreditLimit);
        assertEquals(entity.getStepMortgageAmountAdvanced(), 
                     stepMortgageAmountAdvanced);
        assertEquals(entity.getStepMortgagePostedRate(), stepMortgagePostedRate);
        assertEquals(entity.getStepMortgageCommittedRate(), 
                     stepMortgageCommittedRate);
        assertEquals(entity.getStepMortgageTerm(), stepMortgageTerm);
        assertEquals(entity.getStepMortgageAmortization(), 
                     stepMortgageAmortization);
        assertEquals(entity.getStepMortgagePayment(), stepMortgagePayment);
        assertEquals(entity.getStepMortgageFindersFee(), stepMortgageFindersFee);
        assertEquals(entity.getStepMortgageLOLP(), stepMortgageLOLP);
        assertEquals(entity.getStepMortgageHCP(), stepMortgageHCP);
        assertEquals(entity.getStepMortgageCashbackPercentage(), 
                     stepMortgageCashbackPercentage);
        assertEquals(entity.getUserProfileId(), userProfileId);
        assertEquals(entity.getDateTimeStamp(), dateTimeStamp);
        
        _logger.info(BORDER_END, "findByPrimaryKey");
    }
           
               
    /**
     * test findByDeal.
     */
    @Ignore
    @Test
    public void testFindByDeal()  throws Exception {
      
        _logger.info(BORDER_START, "findByDeal");
        
        //  set VPD
        _srk.getExpressState().setDealInstitutionId(instProfId);
        
        // excute method
        ScotiabankStepMortgage entity = new ScotiabankStepMortgage(_srk);
        Collection entityies = entity.findByDeal(new DealPK(dealId, copyId));
        
        // check assertEqual only dealId and copyId
        Iterator entityIter = entityies.iterator();
        for (; entityIter.hasNext(); ) {
          ScotiabankStepMortgage ssm = (ScotiabankStepMortgage) entityIter.next();
            assertEquals(ssm.getDealId(), dealId);
            assertEquals(ssm.getCopyId(), copyId);
        }
        
        _logger.info(BORDER_END, "testFindByDeal");
    }
                 
 }