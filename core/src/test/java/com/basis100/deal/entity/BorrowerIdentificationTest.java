/*
 * @(#)BorrowerIdentificationTest.java Sep 20, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import java.util.Collection;
import java.util.Iterator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.BorrowerIdentificationPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * @author amalhi
 * 
 * Junit for BorrowerIdentification
 * 
 */
public class BorrowerIdentificationTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(BorrowerIdentificationTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public BorrowerIdentificationTest() {

    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.BorrowerIdentification#findByPrimaryKey(com.basis100.deal.pk.BorrowerIdentificationPK)}.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int identificationId = _dataRepository.getInt("BorrowerIdentification",
                "identificationId", 0);
        int instProfId = _dataRepository.getInt("BorrowerIdentification",
                "INSTITUTIONPROFILEID", 0);
        int copyId = _dataRepository.getInt("BorrowerIdentification", "copyId",
                0);
        int identificationTypeId = _dataRepository.getInt(
                "BorrowerIdentification", "identificationTypeId", 0);
        int borrowerId = _dataRepository.getInt("BorrowerIdentification",
                "borrowerId", 0);

        _logger.info(BORDER_START,
                "BorrowerIdentification - testFindByPrimaryKey");

        _logger.info("Got Values from new BorrowerIdentification ::: [{}]",
                identificationId + "::" + instProfId + "::" + copyId + "::"
                        + identificationTypeId + "::" + borrowerId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        BorrowerIdentification _borrowerIdentification = new BorrowerIdentification(
                _srk);

        // Find by primary key
        _borrowerIdentification = _borrowerIdentification
                .findByPrimaryKey(new BorrowerIdentificationPK(
                        identificationId, copyId));

        _logger.info("Got Values from new BorrowerIdentification ::: [{}]",
                _borrowerIdentification.getIdentificationId() + "::"
                        + _borrowerIdentification.getIdentificationTypeId()
                        + "::" + _borrowerIdentification.getCopyId() + "::"
                        + _borrowerIdentification.getBorrowerId());

        // Check if the data retrieved matches the DB
        assertEquals(_borrowerIdentification.getIdentificationId(),
                identificationId);
        assertEquals(_borrowerIdentification.getIdentificationTypeId(),
                identificationTypeId);
        assertEquals(_borrowerIdentification.getCopyId(), copyId);
        assertEquals(_borrowerIdentification.getBorrowerId(), borrowerId);

        _logger.info(BORDER_END,
                "BorrowerIdentification - testFindByPrimaryKey");
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.BorrowerIdentification#findByBorrower(com.basis100.deal.pk.BorrowerPK)}.
     */
    @Test
    public void testFindByBorrower() throws Exception {

        // get input data from repository
        int identificationId = _dataRepository.getInt("BorrowerIdentification",
                "identificationId", 0);
        int instProfId = _dataRepository.getInt("BorrowerIdentification",
                "INSTITUTIONPROFILEID", 0);
        int copyId = _dataRepository.getInt("BorrowerIdentification", "copyId",
                0);
        int identificationTypeId = _dataRepository.getInt(
                "BorrowerIdentification", "identificationTypeId", 0);
        int borrowerId = _dataRepository.getInt("BorrowerIdentification",
                "borrowerId", 0);

        _logger.info(BORDER_START,
                "BorrowerIdentification - testFindByBorrower");

        _logger.info("Values to be found in BorrowerIdentification ::: [{}]",
                identificationId + "::" + instProfId + "::" + copyId + "::"
                        + identificationTypeId + "::" + borrowerId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        BorrowerIdentification _borrowerIdentification = new BorrowerIdentification(
                _srk);

        // Find by primary key
        Collection _borrowerIdentificationList = _borrowerIdentification
                .findByBorrower(new BorrowerPK(borrowerId, copyId));

        // assertEquals for each value found till we reach matching
        // BorrowerIdentification
        Iterator _borrowerIdentificationItr = _borrowerIdentificationList
                .iterator();
        for (; _borrowerIdentificationItr.hasNext();) {
            BorrowerIdentification _borrowerIdentificationTemp = (BorrowerIdentification) _borrowerIdentificationItr
                    .next();

            _logger.info(
                    "Got Values from BorrowerIdentification found::: [{}]",
                    _borrowerIdentificationTemp.getIdentificationId()
                            + "::"
                            + _borrowerIdentificationTemp
                                    .getIdentificationTypeId() + "::"
                            + _borrowerIdentificationTemp.getCopyId() + "::"
                            + _borrowerIdentificationTemp.getBorrowerId());

            if (_borrowerIdentificationTemp.getIdentificationId() == identificationId) {
                // Check if the data retrieved matches the DB
                assertEquals(_borrowerIdentificationTemp.getIdentificationId(),
                        identificationId);
                assertEquals(_borrowerIdentificationTemp
                        .getIdentificationTypeId(), identificationTypeId);
                assertEquals(_borrowerIdentificationTemp.getCopyId(), copyId);
                assertEquals(_borrowerIdentificationTemp.getBorrowerId(),
                        borrowerId);
            }
        }

        _logger.info(BORDER_END, "BorrowerIdentification - testFindByBorrower");
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.BorrowerIdentification#create(com.basis100.deal.pk.BorrowerPK)}.
     */
    @Test
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create - BorrowerIdentification");

        // get input data from repository
        int instProfId = _dataRepository.getInt("BorrowerIdentification",
                "INSTITUTIONPROFILEID", 0);
        int borrowerId = _dataRepository.getInt("Borrower", "borrowerId", 0);
        int copyId = _dataRepository.getInt("Borrower", "copyId", 0);

        _logger.info("Got Values from new BorrowerIdentification ::: [{}]",
                instProfId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // begin transaction
        _srk.beginTransaction();

        // Create the initial entity
        BorrowerIdentification newEntity = new BorrowerIdentification(_srk);

        // call create method of entity
        newEntity = newEntity.create(new BorrowerPK(borrowerId, copyId));

        _logger.info("CREATED BorrowerIdentification ::: [{}]", newEntity
                .getIdentificationId()
                + "::" + newEntity.getBorrowerId());

        // check create correctly by calling findByPrimaryKey
        BorrowerIdentification foundEntity = new BorrowerIdentification(_srk);
        foundEntity = foundEntity
                .findByPrimaryKey(new BorrowerIdentificationPK(newEntity
                        .getIdentificationId(), copyId));

        // verifying the created entity
        assertEquals(foundEntity.getIdentificationId(), newEntity
                .getIdentificationId());
        assertEquals(foundEntity.getIdentificationTypeId(), newEntity
                .getIdentificationTypeId());
        assertEquals(foundEntity.getCopyId(), newEntity.getCopyId());
        assertEquals(foundEntity.getBorrowerId(), newEntity.getBorrowerId());

        _logger.info(BORDER_END, "Create - BorrowerIdentification");

        // roll-back transaction.
        _srk.rollbackTransaction();
    }

}
