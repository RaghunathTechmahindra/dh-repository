package com.basis100.deal.entity;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.docprep.extract.FMLQueryHandler;
import com.basis100.resources.SessionResourceKit;

public class FMLQueryHandlerTest extends FXDBTestCase {
	private IDataSet dataSetTest;
    private FMLQueryHandler fmlQueryHandler = null;
    CalcMonitor dcm =null;
    SessionResourceKit srk= new SessionResourceKit();
    List l = null;
	
	public FMLQueryHandlerTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(FMLQueryHandler.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(FMLQueryHandler.class.getSimpleName()+"DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.fmlQueryHandler = new FMLQueryHandler(srk);
	}
	
	public void testStatementMetaInfo() throws Exception
	{
		srk.beginTransaction();
		ITable testCreate = dataSetTest.getTable("testCreate");
		int institutionProfileId = Integer.parseInt((String)testCreate.getValue(0, "INSTITUTIONPROFILEID"));
		int id = Integer.parseInt((String)testCreate.getValue(0, "PRICINGPROFILEID"));
		int psid = Integer.parseInt((String)testCreate.getValue(0, "PRICINGSTATUSID"));
		int profileStatusId = Integer.parseInt((String)testCreate.getValue(0, "PROFILESTATUSID"));
		
		srk.getExpressState().setDealInstitutionId(institutionProfileId);
		
		
		srk.rollbackTransaction();
	}
	
	
}
