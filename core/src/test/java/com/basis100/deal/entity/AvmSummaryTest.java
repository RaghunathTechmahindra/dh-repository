/*
 * @(#)AvmSummaryTest.java Sep 18, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.AvmSummaryPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * @author amalhi
 * 
 * Junit for entity AvmSummary
 * 
 */
public class AvmSummaryTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(AvmSummaryTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public AvmSummaryTest() {

    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }

    /**
     * Test method for
     * {@link com.basis100.deal.entity.AvmSummary#findByPrimaryKey(com.basis100.deal.pk.AvmSummaryPK)}.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int responseId = _dataRepository.getInt("AvmSummary", "responseId", 0);
        int instProfId = _dataRepository.getInt("AvmSummary",
                "INSTITUTIONPROFILEID", 0);
        double avmValue = _dataRepository
                .getDouble("AvmSummary", "avmValue", 0);

        _logger.info(BORDER_START, "findByPrimaryKey");
        _logger.info("Got Values from new AvmSummary ::: [{}]", responseId
                + "::" + instProfId + "::" + avmValue);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        AvmSummary _avmSummary = new AvmSummary(_srk);

        // Find by primary key
        _avmSummary = _avmSummary
                .findByPrimaryKey(new AvmSummaryPK(responseId));

        _logger.info("Got Values from new AvmSummary ::: [{}]", _avmSummary
                .getResponseId()
                + "::" + _avmSummary.getAvmValue());

        // Check if the data retrieved matches the DB
        assertTrue(_avmSummary.getResponseId() == responseId);
        assertTrue(_avmSummary.getAvmValue() == avmValue);

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * Test method for {@link com.basis100.deal.entity.AvmSummary#create(int)}.
     */
    @Test
   public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create");

        // get input data from repository
        int instProfId = _dataRepository.getInt("Response",
                "INSTITUTIONPROFILEID", 0);
        int responseId = _dataRepository.getInt("Response", "responseId", 0);

        _logger.info("Got Values from new AvmSummary ::: [{}]", instProfId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(1);

        // begin transaction
        _srk.beginTransaction();

        // Create the initial entity
        AvmSummary newEntity = new AvmSummary(_srk);

        // call create method of entity
        newEntity = newEntity.create(responseId);

        _logger.info("CREATED AVM SUMMARY ::: [{}]", newEntity.getResponseId()
                + "::" + newEntity.getAvmValue());

        // check create correctly by calling findByPrimaryKey
        AvmSummary foundEntity = new AvmSummary(_srk);
        foundEntity = foundEntity
                .findByPrimaryKey(new AvmSummaryPK(responseId));

        // verifying the created entity
        assertTrue(foundEntity.getResponseId() == newEntity.getResponseId());
        assertTrue(foundEntity.getAvmValue() == newEntity.getAvmValue());
        _logger.info(BORDER_END, "Create");

        // roll-back transaction.
        _srk.rollbackTransaction();
    }

    

}
