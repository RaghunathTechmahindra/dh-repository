/*
 * @(#) PropertyTest 2007-09-12
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.deal.entity;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.pk.DealPK;

import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * @author amalhi
 *
 * JUnit test for entity Property
 *
 */
public class PropertyTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(PropertyTest.class);

    // session resource kit
    private SessionResourceKit _srk;

    /**
     * Constructor function
     */
    public PropertyTest() {

    }

    /**
     * To be executed before test
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env and free resources
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }

    /**
     * test find by primary key.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception {

        // get input data from repository
        int dealId = _dataRepository.getInt("Property", "dealId", 0);
        int instProfId = _dataRepository.getInt("Property",
                "INSTITUTIONPROFILEID", 0);
        int propertyId = _dataRepository.getInt("Property", "propertyId", 0);
        int copyId = _dataRepository.getInt("Property", "copyId", 0);

        _logger.info(BORDER_START, "findByPrimaryKey");
        _logger
                .info(
                        "Got Values from repository for PrimeIndexRateProfile ::: [{}]",
                        dealId + "::" + propertyId + "::" + copyId);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // Create the initial entity
        Property _property = new Property(_srk);

        // Find by primary key
        _property = _property.findByPrimaryKey(new PropertyPK(propertyId,
                copyId));

        _logger.info("Got Values from new found Property ::: [{}]", _property
                .getPropertyId()
                + "::" + _property.getDealId() + "::" + _property.getCopyId());

        // Check if the data retrieved matches the DB
        assertTrue(_property.getPropertyId() == propertyId);
        assertTrue(_property.getDealId() == dealId);
        assertTrue(_property.getCopyId() == copyId);

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * test create method of entity
     */
    @Test
    public void testCreate() throws Exception {

        _logger.info(BORDER_START, "Create");

        // get input data from repository
        int instProfId = _dataRepository.getInt("Property",
                "INSTITUTIONPROFILEID", 0);
        int dealId = _dataRepository.getInt("Deal", "dealId", 0);
        int copyId = _dataRepository.getInt("Deal", "copyId", 0);

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

		//begin transaction
        _srk.beginTransaction();

		// Create the initial entity
        Property newEntity = new Property(_srk);

		//call create method of entity
        newEntity = newEntity.create(new DealPK(dealId, copyId));
        _logger.info("CREATED NEW PROPERTY ENTITY ::: [{}]", newEntity
                .getPropertyId()
                + "::" + newEntity.getDealId() + "::" + newEntity.getCopyId());

        // check create correctly by calling findByPrimaryKey
        Property foundEntity = new Property(_srk);
        foundEntity = foundEntity.findByPrimaryKey(new PropertyPK(newEntity
                .getPropertyId(), newEntity.getCopyId()));

        // verifying the created entity
        assertEquals(foundEntity.getPropertyId(), newEntity.getPropertyId());
        assertEquals(foundEntity.getDealId(), newEntity.getDealId());
        assertEquals(foundEntity.getCopyId(), newEntity.getCopyId());

        _logger.info(BORDER_END, "Create");

        // roll-back transaction.
        _srk.rollbackTransaction();

    }

}
