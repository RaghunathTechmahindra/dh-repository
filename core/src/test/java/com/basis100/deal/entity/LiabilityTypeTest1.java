package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.entity.CreateException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;

public class LiabilityTypeTest1 extends FXDBTestCase {
	private String INIT_XML_DATA = "LiabilityTypeDataSet.xml";
	private LiabilityType liabilityType;
	private IDataSet dataSetTest;

	public LiabilityTypeTest1(String name) throws DataSetException, IOException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"LiabilityTypeDataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		// TODO Auto-generated method stub
		return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();

		liabilityType = new LiabilityType(srk);

	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	public void testCreate() throws JdbcTransactionException, RemoteException,
			CreateException, NumberFormatException, DataSetException {
		srk.beginTransaction();
		int id = Integer.parseInt(dataSetTest.getTable("testCreate")
				.getValue(0, "id").toString());
		String desc = dataSetTest.getTable("testCreate").getValue(0, "desc")
				.toString();
		Double gdsInc = Double.parseDouble(dataSetTest.getTable("testCreate")
				.getValue(0, "gdsInc").toString());
		Double tdsInc = Double.parseDouble(dataSetTest.getTable("testCreate")
				.getValue(0, "tdsInc").toString());
		String lpq = dataSetTest.getTable("testCreate").getValue(0, "lpq")
				.toString();
		liabilityType.create(id, desc, gdsInc, tdsInc, lpq);
		srk.rollbackTransaction();
	}
}
