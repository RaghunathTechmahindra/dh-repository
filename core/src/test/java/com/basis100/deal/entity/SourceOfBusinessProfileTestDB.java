package com.basis100.deal.entity;

import java.io.IOException;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.AppraisalOrderPK;
import com.basis100.deal.pk.ArchivedLoanApplicationPK;
import com.basis100.deal.pk.ArchivedLoanDecisionPK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.deal.pk.SourceOfBusinessProfilePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.util.EntityTestUtil;

public class SourceOfBusinessProfileTestDB extends FXDBTestCase{

	private IDataSet dataSetTest;
	private SourceOfBusinessProfile sourceOfBusinessProfile;	
	private DealEntity entity;
	public SourceOfBusinessProfileTestDB(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(SourceOfBusinessProfile.class.getSimpleName() + "DataSetTest.xml"));
	}
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;	
	}

	
	@Override
	protected void setUp() throws Exception {
		super.setUp();		
	}	
	public void testCreatePrimaryKeySuccess() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, SQLException, CloneNotSupportedException, CreateException{
		try {
			srk.beginTransaction();
			ITable testCreatePrimaryKey = dataSetTest.getTable("testCreatePrimaryKeySuccess");					
			int sourceOfBusinessProfileId=Integer.valueOf((String)testCreatePrimaryKey.getValue(0,"SOURCEOFBUSINESSPROFILEID"));			
			SourceOfBusinessProfilePK pk=new SourceOfBusinessProfilePK(sourceOfBusinessProfileId);
			sourceOfBusinessProfile = new SourceOfBusinessProfile(srk,sourceOfBusinessProfileId);
			pk=sourceOfBusinessProfile.createPrimaryKey();
		    assertEquals(sourceOfBusinessProfileId,sourceOfBusinessProfile.getSourceOfBusinessProfileId());
		    srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
	
	
	
	public void testCreate() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, SQLException, CloneNotSupportedException, CreateException{
		try {
			srk.beginTransaction();
			ITable testCreate = dataSetTest.getTable("testCreate");					
			int sourceOfBusinessProfileId=Integer.valueOf((String)testCreate.getValue(0,"SOURCEOFBUSINESSPROFILEID"));			
			srk.getExpressState().setDealInstitutionId(1);
			sourceOfBusinessProfile = new SourceOfBusinessProfile(srk,sourceOfBusinessProfileId);		    		
			sourceOfBusinessProfile=sourceOfBusinessProfile.create();
		    assertNotNull(sourceOfBusinessProfile);
		    srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
	
	
	public void testCreateSourceUserAssoc() throws Exception{
		try {
			srk.beginTransaction();
			ITable testCreateSourceUserAssoc = dataSetTest.getTable("testCreateSourceUserAssoc");					
			int sourceOfBusinessProfileId=Integer.valueOf((String)testCreateSourceUserAssoc.getValue(0,"SOURCEOFBUSINESSPROFILEID"));			
			int sourceUserProfileId=Integer.valueOf((String)testCreateSourceUserAssoc.getValue(0,"USERPROFILEID"));			
			String effectiveDate=(String)testCreateSourceUserAssoc.getValue(0,"EFFECTIVEDATE");
			SimpleDateFormat formate=new SimpleDateFormat("dd-MMM-yy");
			Date effectiveUserAsso=formate.parse(effectiveDate);			
			srk.getExpressState().setDealInstitutionId(1);
			sourceOfBusinessProfile = new SourceOfBusinessProfile(srk,sourceOfBusinessProfileId);			
			sourceOfBusinessProfile.createSourceUserAssoc(sourceUserProfileId,effectiveUserAsso);
		    assertEquals(sourceOfBusinessProfileId, sourceOfBusinessProfile.getSourceOfBusinessProfileId());
		    srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
	
	
		
	public void testUpdateSourceUserAssoc() throws Exception{
		try {
			srk.beginTransaction();
			ITable testUpdateSourceUserAssoc = dataSetTest.getTable("testUpdateSourceUserAssoc");					
			int sourceOfBusinessProfileId=Integer.valueOf((String)testUpdateSourceUserAssoc.getValue(0,"SOURCEOFBUSINESSPROFILEID"));			
			int sourceUserProfileId=Integer.valueOf((String)testUpdateSourceUserAssoc.getValue(0,"USERPROFILEID"));			
			String effectiveDate=(String)testUpdateSourceUserAssoc.getValue(0,"EFFECTIVEDATE");
			SimpleDateFormat formate=new SimpleDateFormat("dd-MMM-yy");
			Date effectiveUserAsso=formate.parse(effectiveDate);			
			srk.getExpressState().setDealInstitutionId(1);
			sourceOfBusinessProfile = new SourceOfBusinessProfile(srk,sourceOfBusinessProfileId);			
			sourceOfBusinessProfile.updateSourceUserAssoc(sourceUserProfileId,effectiveUserAsso);
		    assertEquals(sourceOfBusinessProfileId, sourceOfBusinessProfile.getSourceOfBusinessProfileId());
		    srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
	
	public void testDeleteSourceUserAssoc() throws Exception{
		try {
			srk.beginTransaction();
			ITable testDeleteSourceUserAssoc = dataSetTest.getTable("testDeleteSourceUserAssoc");					
			int sourceOfBusinessProfileId=Integer.valueOf((String)testDeleteSourceUserAssoc.getValue(0,"SOURCEOFBUSINESSPROFILEID"));				
			srk.getExpressState().setDealInstitutionId(1);
			sourceOfBusinessProfile = new SourceOfBusinessProfile(srk,sourceOfBusinessProfileId);			
			sourceOfBusinessProfile.deleteSourceUserAssoc();
		    assertEquals(sourceOfBusinessProfileId, sourceOfBusinessProfile.getSourceOfBusinessProfileId());
		    srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {			
			e.printStackTrace();			
		}
	}
}
