package com.basis100.deal.entity;

import java.io.IOException;
import java.text.DateFormatSymbols;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.TimeZone;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.util.BusinessCalendar;
import com.basis100.resources.SessionResourceKit;

public class BusinessCalendarTest1 extends FXDBTestCase {
	
	private IDataSet dataSetTest;
	private BusinessCalendar businessCalendar = null;
    SessionResourceKit srk= new SessionResourceKit();
    private Hashtable timeZoneTable=null;
	
	public BusinessCalendarTest1(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(BusinessCalendar.class.getSimpleName()+"DataSetTest.xml"));
	}
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		srk.freeResources();
		return DatabaseOperation.DELETE;	
	}

	@Override
	protected DatabaseOperation getSetUpOperation() throws Exception
	    {
		businessCalendar = new BusinessCalendar(srk);
	    	return DatabaseOperation.INSERT;
	    }
	
	public void testCalcBusDate() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCalcBusDate");
				
		//int tzid = Integer.parseInt((String)testExtract.getValue(0, "TIMEZONEENTRYID"));
		int bpid = Integer.parseInt((String)testExtract.getValue(0, "BRANCHPROFILEID"));
				
		Date d = new Date();
		//d = businessCalendar.calcBusDate(new Date(), theBranch.getTimeZoneEntryId(), -1);
		//Hardcoding TimeZoneEntryId as there is an invalid column name exception from calling theBranch.getTimeZoneEntryId()
		assertEquals(d.getDate(),(businessCalendar.calcBusDate(new Date(), 7, -1)).getDate());
		
		srk.rollbackTransaction();
	}
	
	public void testCalcBusDateFailure() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCalcBusDateFailure");
		int bpid = Integer.parseInt((String)testExtract.getValue(0, "BRANCHPROFILEID"));
				
		Date d = new Date();
		d.setDate(1);
		assertFalse(d.getDate() ==(businessCalendar.calcBusDate(new Date(), 7, 1)).getDate());
		srk.rollbackTransaction();
	}
	
	public void testCalcBusDate1() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCalcBusDate1");
		int provinceid = Integer.parseInt((String)testExtract.getValue(0, "PROVINCEID"));
		
		Date d = new Date();
		assertEquals(d.getDate(),(businessCalendar.calcBusDate(new Date(), 7, -1, provinceid)).getDate());
		srk.rollbackTransaction();
	}
	
	public void testCalcBusDate1Failure() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCalcBusDate1Failure");
		int provinceid = Integer.parseInt((String)testExtract.getValue(0, "PROVINCEID"));
		
		Date d = new Date();
		d.setDate(6);
		assertFalse(d.getDate()==(businessCalendar.calcBusDate(new Date(), 7, -1, provinceid)).getDate());
		srk.rollbackTransaction();
	}
	
	
	public void testCalcBusDate2() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCalcBusDate2");
		int provinceid = Integer.parseInt((String)testExtract.getValue(0, "PROVINCEID"));
					
		Date d = new Date();
		assertEquals(d.getDate(),(businessCalendar.calcBusDate(new Date(), -1)).getDate());
		srk.rollbackTransaction();
	}
	
	public void testCalcBusDate2Failure() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCalcBusDate2Failure");
		int provinceid = Integer.parseInt((String)testExtract.getValue(0, "PROVINCEID"));
					
		Date d = new Date();
		d.setDate(6);
		assertFalse(d.getDate()==(businessCalendar.calcBusDate(new Date(), -1)).getDate());
		srk.rollbackTransaction();
	}
	
	public void testCalcBusDate3() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCalcBusDate3");
		int provinceid = Integer.parseInt((String)testExtract.getValue(0, "PROVINCEID"));
		int callerTimeZoneId = Integer.parseInt((String)testExtract.getValue(0, "TIMEZONEENTRYID"));
		
		//TimeZoneEntry tze = (TimeZoneEntry)timeZoneTable.get(new Integer(callerTimeZoneId));			
		Date d = new Date();
		assertEquals(d.getDate(),(businessCalendar.calcBusDate(new Date(), TimeZone.getTimeZone("GMT+02:00"), -1, provinceid)).getDate());
		srk.rollbackTransaction();
	} 
	
	public void testCalcBusDate3Failure() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCalcBusDate2Failure");
		int provinceid = Integer.parseInt((String)testExtract.getValue(0, "PROVINCEID"));
					
		Date d = new Date();
		d.setDate(6);
		assertFalse(d.getDate()==(businessCalendar.calcBusDate(new Date(), TimeZone.getTimeZone("GMT+02:00"), -1, provinceid)).getDate());
		srk.rollbackTransaction();
	} 
	
	
	/*@SuppressWarnings("deprecation")
	public void testCalcBusDate4() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCalcBusDate4");
		int provinceid = Integer.parseInt((String)testExtract.getValue(0, "PROVINCEID"));
		int callerTimeZoneId = Integer.parseInt((String)testExtract.getValue(0, "TIMEZONEENTRYID"));
		Date d = new Date();
		int year = d.getYear();
		int month = d.getMonth();
		int day = d.getDay();
		int hour = d.getHours();
		int min = d.getMinutes();
		int sec = d.getSeconds();
		
		assertEquals(d.getDate(),(businessCalendar.calcBusDate(year, month, day, hour, min, sec, callerTimeZoneId, 1).getDate()));
		srk.rollbackTransaction();
	} */
	
	@SuppressWarnings("deprecation")
	public void testCalcBusDate5() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCalcBusDate5");
		int provinceid = Integer.parseInt((String)testExtract.getValue(0, "PROVINCEID"));
		int callerTimeZoneId = Integer.parseInt((String)testExtract.getValue(0, "TIMEZONEENTRYID"));
		Date d = new Date();
		int year = d.getYear();
		int month = d.getMonth();
		int day = d.getDay();
		int hour = d.getHours();
		int min = d.getMinutes();
		int sec = d.getSeconds();
		
		assertNotNull(businessCalendar.calcBusDate(year, month, day, hour, min, sec, TimeZone.getTimeZone("GMT-02:00"), 1).getDate());
		srk.rollbackTransaction();
	} 
	
	@SuppressWarnings("deprecation")
	public void testCalcBusDate5Failure() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCalcBusDate5Failure");
		int provinceid = Integer.parseInt((String)testExtract.getValue(0, "PROVINCEID"));
		int callerTimeZoneId = Integer.parseInt((String)testExtract.getValue(0, "TIMEZONEENTRYID"));
		Date d = new Date();
		int year = d.getYear();
		int month = d.getMonth();
		int day = d.getDay();
		int hour = d.getHours();
		int min = d.getMinutes();
		int sec = d.getSeconds();
		
		assertFalse(d.getDate()==(businessCalendar.calcBusDate(year, month, day, hour, min, sec, TimeZone.getTimeZone("GMT-02:00"), 1).getDate()));
		srk.rollbackTransaction();
	}
	
	@SuppressWarnings("deprecation")
	public void testCalcBusDate6() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCalcBusDate6");
		int provinceid = Integer.parseInt((String)testExtract.getValue(0, "PROVINCEID"));
		int timeZoneId = Integer.parseInt((String)testExtract.getValue(0, "TIMEZONEENTRYID"));
		Date d = new Date();
		int year = d.getYear();
		int month = d.getMonth();
		int day = d.getDay();
		int hour = d.getHours();
		int min = d.getMinutes();
		int sec = d.getSeconds();
		
		assertNotNull(businessCalendar.calcBusDate(year, month, day, hour, min, sec, timeZoneId, 1).getDate());
		srk.rollbackTransaction();
	} 
	@SuppressWarnings("deprecation")
	public void testCalcBusDate6Failure() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCalcBusDate6");
		int provinceid = Integer.parseInt((String)testExtract.getValue(0, "PROVINCEID"));
		int timeZoneId = Integer.parseInt((String)testExtract.getValue(0, "TIMEZONEENTRYID"));
		Date d = new Date();
		int year = d.getYear();
		int month = d.getMonth();
		int day = d.getDay();
		int hour = d.getHours();
		int min = d.getMinutes();
		int sec = d.getSeconds();
		
		assertFalse(d.getDate()==businessCalendar.calcBusDate(year, month, day, hour, min, sec, timeZoneId, 1).getDate());
		srk.rollbackTransaction();
	} 
	
	@SuppressWarnings("deprecation")
	public void testCalcBusDate7() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCalcBusDate6");
		int provinceid = Integer.parseInt((String)testExtract.getValue(0, "PROVINCEID"));
		int timeZoneId = Integer.parseInt((String)testExtract.getValue(0, "TIMEZONEENTRYID"));
		Date d = new Date();
		int year = d.getYear();
		int month = d.getMonth();
		int day = d.getDay();
		int hour = d.getHours();
		int min = d.getMinutes();
		int sec = d.getSeconds();
		
		assertNotNull(businessCalendar.calcBusDate(year, month, day, hour, min, TimeZone.getTimeZone("GMT-02:00"), 1).getDate());
		srk.rollbackTransaction();
	} 
	@SuppressWarnings("deprecation")
	public void testCalcBusDate7Failure() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testCalcBusDate6");
		int provinceid = Integer.parseInt((String)testExtract.getValue(0, "PROVINCEID"));
		int timeZoneId = Integer.parseInt((String)testExtract.getValue(0, "TIMEZONEENTRYID"));
		Date d = new Date();
		int year = d.getYear();
		int month = d.getMonth();
		int day = d.getDay();
		int hour = d.getHours();
		int min = d.getMinutes();
		int sec = d.getSeconds();
		
		assertFalse(d.getDate() == businessCalendar.calcBusDate(year, month, day, hour, min, TimeZone.getTimeZone("GMT-02:00"), 1).getDate());
		srk.rollbackTransaction();
	}
	
	@SuppressWarnings("deprecation")
	public void testTzDisplay() throws Exception
	{
		srk.beginTransaction();
		ITable testExtract = dataSetTest.getTable("testTzDisplay");
		int provinceid = Integer.parseInt((String)testExtract.getValue(0, "PROVINCEID"));
		int callerTimeZoneId = Integer.parseInt((String)testExtract.getValue(0, "TIMEZONEENTRYID"));
		Date d = new Date();
	
		String dateStr = null;
		dateStr = String.valueOf(d.getDate());
		
		String resultDt = businessCalendar.tzDisplay(dateStr, callerTimeZoneId, null);
		Date d1= new Date(resultDt);
		d1.setDate(Integer.parseInt(resultDt.substring(0,resultDt.indexOf("/"))));
		
		assertTrue(d.getDate() == d1.getDate());
		srk.rollbackTransaction();
	}
	
	
	
	
	
}
