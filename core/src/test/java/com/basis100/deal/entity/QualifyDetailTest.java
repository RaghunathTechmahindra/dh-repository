/*
 * @(#)QualifyDetailTest.java May 15, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.entity;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.pk.ComponentSummaryPK;
import com.basis100.deal.pk.QualifyDetailPK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * 
 */
public class QualifyDetailTest extends ExpressEntityTestCase implements UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(QualifyDetailTest.class);

    // session resource kit
    private SessionResourceKit  _srk;

    /**
     * Constructor function
     */
    public QualifyDetailTest() {

    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }
    /**
     * Test method for {@link com.basis100.deal.entity.QualifyDetail#findByPrimaryKey(com.basis100.deal.pk.QualifyDetailPK)}.
     */
    @Test
    public void testFindByPrimaryKey() throws Exception{
       // int dealId = _dataRepository.getInt("QualifyDetail", "dealId", 0);
    	 int dealId =100001;
       // int instProfId = _dataRepository.getInt("QualifyDetail", "INSTITUTIONPROFILEID", 0);
         int instProfId=0;
        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        _logger.info(BORDER_START, "findByPrimaryKey");
        _logger.info("Got Values from new QualifyDetail ::: [{}]", dealId + "::"
                + instProfId);
        
        //create the entity
        QualifyDetail _qDetail = new QualifyDetail(_srk);
        
        //Find by primary Key
        _qDetail = _qDetail.findByPrimaryKey(new QualifyDetailPK(dealId));
        
        
        _logger.info("Got Values from QualifyDetail found::: [{}]", _qDetail
                .getDealId());

        // Check if the data retrieved matches the DB
        assertEquals(_qDetail.getDealId(), dealId);

        _logger.info(BORDER_END, "findByPrimaryKey");
    }

    /**
     * Test method for {@link com.basis100.deal.entity.QualifyDetail#create(int, int, int)}.
     */
    @Ignore
    public void testCreate() throws Exception{
        _logger.info(BORDER_START, "Create - QualifyDetail");

        // get input data from repository
        int dealId =100002;
        int instProfId=0;
        int interestCompoundingId=0;
        int repaymentTypeId=0;
       /* int instProfId = _dataRepository.getInt("Deal",
                "INSTITUTIONPROFILEID", 0);
        int dealId = _dataRepository.getInt("Deal", "DealId", 0);
        int interestCompoundingId = _dataRepository.getInt("QualifyDetail", "interestCompoundingId", 0);
        int repaymentTypeId = _dataRepository.getInt("QualifyDetail", "repaymentTypeId", 0);
        _logger.info("Got Values from new QualifyDetail ::: [{}]", instProfId);*/

        // Set VPD context
        _srk.getExpressState().setDealInstitutionId(instProfId);

        // begin transaction
        _srk.beginTransaction();
        
        //create entity
        QualifyDetail newEntity = new QualifyDetail(_srk);
        newEntity = newEntity.create(dealId, interestCompoundingId, repaymentTypeId);
        
        _logger.info("Created a new QualifyDetail ::" + newEntity.getDealId());
        
        //create the holder entity
        QualifyDetail foundEntity = new QualifyDetail(_srk);
        
        //Find by primary Key
        foundEntity = foundEntity.findByPrimaryKey(new QualifyDetailPK(newEntity.getDealId()));
        
        // verifying the created entity
        assertEquals(foundEntity.getDealId(), newEntity.getDealId());
        
        _logger.info(BORDER_END, "Create - QualifyDetail");

        // roll-back transaction.
        _srk.rollbackTransaction();
    }

}
