package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.BranchProfilePK;

public class DBBranchProfileTest  extends FXDBTestCase{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(DBBranchProfileTest.class);
    
    //Initial data to be loaded before each test case.
	private final static String INIT_XML_DATA = "DBBranchProfileDataSet.xml";
	private IDataSet dataSetTest;
	
	/**
     * Constructor function
     */
    public DBBranchProfileTest(String name)  throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource("DBBranchProfileDataSetTest.xml"));
    }
    
    @Override
	protected IDataSet getDataSet() throws Exception {
    	return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}
    
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
        return DatabaseOperation.INSERT;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.DELETE;
    }
    
    public void testCreatePrimaryKey()
    {
    	_logger.info(" Start testCreatePrimaryKey");
    	try
    	{
    		BranchProfile branchProfile = new BranchProfile(srk);
    		BranchProfilePK branchProfilePK = branchProfile.createPrimaryKey();
    		
    		assert branchProfilePK != null;
    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    	}
    	_logger.info(" End testCreatePrimaryKey");
    }
    
}
