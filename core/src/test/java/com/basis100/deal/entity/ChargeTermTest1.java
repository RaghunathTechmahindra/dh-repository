package com.basis100.deal.entity;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;

public class ChargeTermTest1 extends FXDBTestCase {
	private ChargeTerm chargeTerm;
	IDataSet dataSetTest;

	public ChargeTermTest1(String name) throws DataSetException, IOException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				"ChargeTermDataSetTest.xml"));
	}

	
	@Override
	public void setUp() throws Exception {
		super.setUp();
		// srk.getExpressState().setDealInstitutionId(0);
		chargeTerm = new ChargeTerm(srk);

	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	public void testCreate() throws JdbcTransactionException, RemoteException,
			CreateException, NumberFormatException, DataSetException {
		srk.beginTransaction();
		int provinceId = Integer.parseInt(dataSetTest.getTable("testCreate")
				.getValue(0, "provinceId").toString());
		int languagePrefId = Integer.parseInt(dataSetTest
				.getTable("testCreate").getValue(0, "languagePrefId")
				.toString());
		int lenderProfileId = Integer.parseInt(dataSetTest
				.getTable("testCreate").getValue(0, "lenderProfileId")
				.toString());
		int dealInstitutionId = Integer.parseInt(dataSetTest
				.getTable("testCreate").getValue(0, "dealInstitutionId")
				.toString());
		srk.getExpressState().setDealInstitutionId(dealInstitutionId);
		chargeTerm.create(provinceId, languagePrefId, lenderProfileId);
		assertEquals(true, srk.getModified());
		srk.rollbackTransaction();

	}

	public void testFindFirstByLenderProvinceAndQualifier() throws RemoteException, FinderException, NumberFormatException, DataSetException {
		int provinceId = Integer.parseInt(dataSetTest.getTable("testFindFirstByLenderProvinceAndQualifier")
				.getValue(0, "provinceId").toString());
		String qualifier = dataSetTest
				.getTable("testFindFirstByLenderProvinceAndQualifier").getValue(0, "qualifier")
				.toString();
		int lenderProfileId = Integer.parseInt(dataSetTest
				.getTable("testFindFirstByLenderProvinceAndQualifier").getValue(0, "lenderProfileId")
				.toString());
		ChargeTerm chageTemByQualifier=chargeTerm.findFirstByLenderProvinceAndQualifier(lenderProfileId,provinceId,qualifier);
		assertEquals(0, chageTemByQualifier.getLenderProfileId());
	}
}
