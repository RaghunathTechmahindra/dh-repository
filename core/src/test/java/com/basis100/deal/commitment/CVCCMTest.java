package com.basis100.deal.commitment;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.Deal;

public class CVCCMTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private CVCCM cvccm;

	public CVCCMTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(CVCCM.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.cvccm = new CVCCM(srk);
	}
	
	@Test
	public void testSendServicingUpload() throws Exception {
		boolean status = false;
		ITable testExtract = dataSetTest.getTable("testSendServicingUpload");	
		String id=(String)testExtract.getValue(0,"DEALID");
		int dealId=Integer.parseInt(id);
		String copy=(String)testExtract.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);		
		Deal deal=new Deal(srk,null,dealId, copyId);
		cvccm.sendServicingUpload(srk,deal,"TYPE",1);
		status=true;
		assertTrue(status);
	}

}