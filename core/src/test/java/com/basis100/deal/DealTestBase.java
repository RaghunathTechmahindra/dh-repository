/*
 * @(#)DealTestBase.java    2005-6-3
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.deal;

import java.util.List;
import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;

import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.entity.RemoteException;
import com.basis100.entity.FinderException;
import com.basis100.deal.entity.Deal;

import com.basis100.TestingEnvironmentInitializer;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * DealTestBase the base test case class for unit test associated with some
 * deals.
 *
 * @version   1.0 2005-6-3
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class DealTestBase extends ExpressEntityTestCase implements UnitTestLogging {

    // The logger
    private static Log _log = LogFactory.getLog(DealTestBase.class);

    protected SessionResourceKit _srk;
    // the deals for Testing.
    protected List _testingDeals = new ArrayList();

    /**
     * Constructor function
     */
    public DealTestBase() {
    }

    /**
     * constructs a test case with the given test case name.
     */
//    public DealTestBase(String name) {
//        super(name);
//    }

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() throws Exception{
        // initializing the test environment.
        //TestingEnvironmentInitializer.initTestingEnv();
        // initializing the session resource kit.
        ResourceManager.init();
        _srk = new SessionResourceKit("Underwriter Routing Test ...");
        try {
            // we don't need CalcMonitor for testing.
            // prepare the deals for testing.
            _log.debug("Preparing the deals for testing ...");

            // for connectionpool = MOSXDQA  -- Xceed QA Database.
            //_testingDeals.add(new Deal(_srk, null, 82978, 70));
            //_testingDeals.add(new Deal(_srk, null, 81607, 1));
            //_testingDeals.add(new Deal(_srk, null, 80097, 7));
            //_testingDeals.add(new Deal(_srk, null, 80099, 15));
            //_testingDeals.add(new Deal(_srk, null, 80162, 1));

            // for connection = MOSLOCAL -- Local Database.
            //_testingDeals.add(new Deal(_srk, null, 212, 1));
            //_testingDeals.add(new Deal(_srk, null, 419, 1));
            //_testingDeals.add(new Deal(_srk, null, 167, 1));
            //// the following deals have rental as primary property.
            //_testingDeals.add(new Deal(_srk, null, 930, 1));
            //_testingDeals.add(new Deal(_srk, null, 931, 1));
            //// the following deals have SOB status id = 4.
            //_testingDeals.add(new Deal(_srk, null, 147, 1));

            // for connectionpool = MOSQADJ  -- DJ QA Database.
			//_log.info("======== Deals from DJ E2E Database ========");
            //_testingDeals.add(new Deal(_srk, null, 2240, 1));
            //_testingDeals.add(new Deal(_srk, null, 2238, 21));
            //_testingDeals.add(new Deal(_srk, null, 2222, 1));
            //_testingDeals.add(new Deal(_srk, null, 2241, 46));
            //_testingDeals.add(new Deal(_srk, null, 2093, 1));
            //_testingDeals.add(new Deal(_srk, null, 2093, 79));
            //_testingDeals.add(new Deal(_srk, null, 832, 2));
            //_testingDeals.add(new Deal(_srk, null, 1993, 1));
            //_testingDeals.add(new Deal(_srk, null, 1994, 1));
            //_testingDeals.add(new Deal(_srk, null, 1995, 1));
            //_testingDeals.add(new Deal(_srk, null, 1996, 1));
            //_testingDeals.add(new Deal(_srk, null, 1997, 1));

            // for MOSTDE2E. TD E2E database.
            //_log.info("======== Deals from TD E2E Database ========");
            //_testingDeals.add(new Deal(_srk, null, 982, 10));
            //_testingDeals.add(new Deal(_srk, null, 2816, 30));

			// Local database MOSXD
            
			_log.info("======== Deal from local XD database ========");
//			_testingDeals.add(new Deal(_srk, null, 85062, 1));
//			_testingDeals.add(new Deal(_srk, null, 85060, 1));
//			_testingDeals.add(new Deal(_srk, null, 85059, 1));
//			_testingDeals.add(new Deal(_srk, null, 85058, 1));
//			_testingDeals.add(new Deal(_srk, null, 85056, 1));
//			_testingDeals.add(new Deal(_srk, null, 85055, 1));
            
            SessionResourceKit _srk = new SessionResourceKit();
           
            for(int i = 0; i < 1; i ++){
                int institutionId = _dataRepository.getInt("Deal",
                        "InstitutionProfileID", i);
                int dealID = _dataRepository.getInt("Deal", "DealID", i);
                int copyID = _dataRepository.getInt("Deal", "CopyID", i);
                
                _srk.getExpressState().setDealInstitutionId(institutionId);
                
                Deal newDeal = new Deal(_srk, null, dealID, copyID);
                newDeal.setInstitutionProfileId(institutionId);
                _testingDeals.add(newDeal);
            }
            
        } catch (RemoteException re) {
            _log.error("Remote Exception: ", re);
        } catch (FinderException fe) {
            _log.error("Finder Exception: ", fe);
        }

        _log.info("==========================================================");
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _log.info("====================================================================");
     }
}
