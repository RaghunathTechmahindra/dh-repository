package com.basis100.deal.file.apc2;

import java.io.IOException;
import java.util.ArrayList;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

import com.basis100.FXDBTestCase;

/**
 * <p>
 * APC2ComponentTestDB
 * </p>
 * Express Entity class unit test: ServiceProvider
 */
public class APC2ComponentTestDB extends FXDBTestCase {

	private IDataSet dataSetTest;
	private APC2Component toComponent;
	private APC2Field apc2Field;

	public APC2ComponentTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				APC2Component.class.getSimpleName() + "DataSetTest.xml"));
	}
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	@Test
	public void testAdd() throws Exception {
		// get input data from xml repository
		ITable testAdd = dataSetTest.getTable("testAdd");
		String type = (String) testAdd.getValue(0, "TYPE");
		String name = (String) testAdd.getValue(0, "NAME");
		toComponent = new APC2Component(name, type, new ArrayList());
		apc2Field = new APC2Field();
		toComponent.add(apc2Field);
		assertSame(name, toComponent.getName());
	}

}