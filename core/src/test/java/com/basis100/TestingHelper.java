/*
 * @(#)TestingHelper.java    2005-5-4
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;

import com.basis100.deal.entity.Deal;

/**
 * TestingHelper is a helper class for unit test.
 *
 * @version   1.0 2005-5-4
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
@Ignore
public class TestingHelper {

    // The logger
    private static Log _log = LogFactory.getLog(TestingHelper.class);

    /**
     * Constructor function
     */
    public TestingHelper() {
    }

    /**
     * helper method to get a string desc for the given deal.
     */
    public static String dealString(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("DealId=").
                append(deal.getDealId()).
                append(" CopyId=").
                append(deal.getCopyId()).
                append(" UnderwriterUserId=").
                append(deal.getUnderwriterUserId()).
                append(" AdministratorId=").
                append(deal.getAdministratorId()).
                append(" SOBProfileId=").
                append(deal.getSourceOfBusinessProfileId()).
                append(" SourceFirmProfileId=").
                append(deal.getSourceFirmProfileId()).
                append(" SepcialFeatureId=").
                append(deal.getSpecialFeatureId());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }

    /**
     * helper method to get a string desc for calculation for the given deal.
     */
    public static String dealStr4Calc(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("DealId=").
                append(deal.getDealId()).
                append(" CopyId=").
                append(deal.getCopyId()).
                append(" MIPremiumAmount=").
                append(deal.getMIPremiumAmount()).
                append(" MIStatusId=").
                append(deal.getMIStatusId()).
                append(" MIExistingPolicyNumber=").
                append(deal.getMIExistingPolicyNumber()).
                append(" CombinedLTV=").
                append(deal.getCombinedLTV()).
				append(" TotalLoanAmount=").
				append(deal.getTotalLoanAmount());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }
}
