package com.basis100.BREngine;

import java.io.IOException;
import java.sql.SQLException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.ltx.unittest.util.EntityTestUtil;

public class MosEntitiesTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	//private MosEntities mosEntities;
	Object obj =null; 
	private BRInterpretor interpretor;
	private DealEntity entity;
	
	
	
	public MosEntitiesTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(MosEntities.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(MosEntities.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.INSERT;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}
	
	public void testMapEntityName() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, SQLException, CloneNotSupportedException{
		String mapEntity=null;
		ITable testMapEntityName = dataSetTest.getTable("testMapEntityName");
		String bankNumber=(String)testMapEntityName.getValue(0,"BANKABANUMBER");
		mapEntity=MosEntities.mapEntityName(bankNumber);
	    assertNotNull(mapEntity);
} 
}
