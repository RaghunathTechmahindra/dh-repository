package com.basis100.BREngine;

import java.util.Date;
import java.util.Vector;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.ServiceProduct;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.pk.ServiceProductPk;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.base.BaseBREEntityTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>Title: OCR007BRTest</p>
 * <p>Description: TiledViewBean for ComponentInfo Section </p>
 * 
 * @author MCM Team
 * @version 1.0 (XS 10.2 Initial Version � Aug 4, 2008)
 */

public class OCR007BRTest extends BaseBREEntityTest {/*

    private com.basis100.deal.entity.Request requestData;  // The entity object that gets data from the Excel sheet
    private com.basis100.deal.entity.Request request; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ServiceRequest serviceRequest; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.Property property; // The live entity, which loads data from the Db.

    private com.basis100.deal.entity.Component componentData; // The entity object that gets data from the Excel sheet.
    private com.basis100.deal.entity.Component component; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentMortgage mtgData; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentMortgage componentMTG; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentLOC locData; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentLOC componentLOC; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentLoan loanData; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentLoan componentLoan; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentCreditCard ccData; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentCreditCard componentCC; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentOverdraft odData; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentOverdraft componentOD; // The live entity, which loads data from the Db.
    
    
    String whereClause = "";
    
    *//**
     * <p>Description: Constructor</p>
     *
     * @version 1.0 Initial Version. (XS_2.39) July 15, 2008
     *//*
    public OCR007BRTest() {
        super();
        componentData=(com.basis100.deal.entity.Component) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Component");
        mtgData=(com.basis100.deal.entity.ComponentMortgage) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.ComponentMortgage");
        locData=(com.basis100.deal.entity.ComponentLOC) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.ComponentLOC");
        loanData=(com.basis100.deal.entity.ComponentLoan) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.ComponentLoan");
        ccData=(com.basis100.deal.entity.ComponentCreditCard) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.ComponentCreditCard");
        odData=(com.basis100.deal.entity.ComponentOverdraft) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.ComponentOverdraft");
        requestData=(com.basis100.deal.entity.Request) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Request");
    }
    
    *//**
     * <p>Description: Set up test data to check BR OCR-007. <br>
     * Deal has components
     *
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * 
     *//*
    public void setupTestData() throws Exception {
        
        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.ejbStore();
        SessionResourceKit srk = deal.getSessionResourceKit();
        setCopyId(deal.getCopyId());
        
        if (componentData.getComponentTypeId() > 0) {
            component=(com.basis100.deal.entity.Component) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Component");
            component.create(deal.getDealId(), deal.getCopyId(), 
                    componentData.getComponentTypeId(),
                    componentData.getMtgProdId()); 
    
            // componentId is created by create method so that needs to be set 
            componentData.setComponentId(component.getComponentId());
            componentData.setCopyId(component.getCopyId());
    
            switch (component.getComponentTypeId()) {
                case Mc.COMPONENT_TYPE_MORTGAGE: 
                    componentMTG = (com.basis100.deal.entity.ComponentMortgage) EntityTestUtil.getInstance()
                      .loadEntity("com.basis100.deal.entity.ComponentMortgage");
                    componentMTG.create(componentData.getComponentId(), componentData.getCopyId());
                    componentMTG.ejbStore();
                    break;
                case Mc.COMPONENT_TYPE_LOC: 
                    componentLOC = (com.basis100.deal.entity.ComponentLOC) EntityTestUtil.getInstance()
                      .loadEntity("com.basis100.deal.entity.ComponentLOC");
                    componentLOC.create(componentData.getComponentId(), componentData.getCopyId());
                                  
                    componentLOC.ejbStore();
                    break;
                case Mc.COMPONENT_TYPE_LOAN: 
                    componentLoan = (com.basis100.deal.entity.ComponentLoan) EntityTestUtil.getInstance()
                          .loadEntity("com.basis100.deal.entity.ComponentLoan");
                    componentLoan.create(componentData.getComponentId(), componentData.getCopyId());
                    componentLoan.ejbStore();
                    break;
                case Mc.COMPONENT_TYPE_CREDITCARD: 
                    componentCC = (com.basis100.deal.entity.ComponentCreditCard) EntityTestUtil.getInstance()
                        .loadEntity("com.basis100.deal.entity.ComponentCreditCard");
                        componentCC.create(componentData.getComponentId(), componentData.getCopyId());
                        componentCC.setCreditCardAmount(ccData.getCreditCardAmount());
                        componentCC.ejbStore();
                    break;
                case Mc.COMPONENT_TYPE_OVERDRAFT: 
                    componentOD = (com.basis100.deal.entity.ComponentOverdraft) EntityTestUtil.getInstance()
                        .loadEntity("com.basis100.deal.entity.ComponentOverdraft");
                    componentOD.create(componentData.getComponentId(), componentData.getCopyId());
                    componentOD.setOverDraftAmount(odData.getOverDraftAmount());
                    componentOD.ejbStore();
                    break;
                default: break;
            }
        }
        request=(com.basis100.deal.entity.Request) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Request");
        request.create(dealPK, requestData.getRequestStatusId(), new Date(), requestData.getUserProfileId());
        requestData.setRequestId(request.getRequestId());
        requestData.setCopyId(request.getCopyId());
        
        request.setChannelId(requestData.getChannelId());
        request.setPayloadTypeId(requestData.getPayloadTypeId());
        request.setRequestDate(new Date());
//        request.setRequestTypeId(requestData.getRequestTypeId());
        request.setServiceProductId(requestData.getServiceProductId());
        request.setServiceTransactionTypeId(requestData.getServiceTransactionTypeId());
        request.ejbStore();
        
        property = new com.basis100.deal.entity.Property(srk);
        property.create(dealPK);
        
        serviceRequest = new com.basis100.deal.entity.ServiceRequest(srk);        
        serviceRequest.create(request.getRequestId(), request.getCopyId());
        serviceRequest.setPropertyId(property.getPropertyId());
        serviceRequest.ejbStore();
        
    }

    *//**
     * <p>Description: This method is for removing the data that get stored. </p>
     *
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public void tearDownTestData() throws Exception {

        if (deal != null) {
            deal.dcm = null;
        }

        try {
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        System.out.println("ejb remove done");
    }

    *//**
     * <p>Description: This method test when pass BR OCR-007. </p>
     *
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public void testBusinessRule() throws Exception {
        try {
            setupTestData();
    
            SessionResourceKit srk = deal.getSessionResourceKit();
    
            BusinessRuleExecutor brExec = new BusinessRuleExecutor();
            brExec.setCurrentRequest(request.getRequestId());
            brExec.setCurrentProperty(property.getPropertyId());
            PassiveMessage pmProp = new PassiveMessage();
    
            brExec.BREValidator((DealPK) deal.getPk(), srk, pmProp, "OCR-%");
            ServiceProduct sp = new ServiceProduct(srk);
            sp = sp.findByPrimaryKey(new ServiceProductPk(request.getServiceProductId()));
            assertNotNull(pmProp.getNumMessages());
            
            tearDownTestData();
        } catch (Exception e) {
            throw e;
        } finally  {
            tearDownTestData();
        }
    }
    
    *//**
     * <p>Description: This method invokes the createSuite() of DDStepsSuiteFactory class. </p>
     *
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public static Test suite() {
        return (Test) DDStepsSuiteFactory.createSuite(OCR007BRTest.class);
    }

    *//**
    * <p>getComponentData</p>
    * <p>Description: getter </p>
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
    * @auther MCM Team 
    *//*
    public com.basis100.deal.entity.Component getComponentData() {
        return componentData;
    }

    *//**
     * <p>setComponentData</p>
     * <p>Description: setter for Component</p>
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public void setComponentData(com.basis100.deal.entity.Component componentData) {
        this.componentData = componentData;
    }
    
    *//**
     * <p>getMtgData</p>
     * <p>Description: getter </p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     *//*
    public com.basis100.deal.entity.ComponentMortgage getMtgData() {
        return mtgData;
    }

    *//**
     * <p>setMtgData</p>
     * <p>Description: setter for ComponentMortgage</p>
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public void setMtgData(
            com.basis100.deal.entity.ComponentMortgage mtg) {
        this.mtgData = mtg;
    }

    *//**
     * <p>getLocData</p>
     * <p>Description: getter </p>
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public com.basis100.deal.entity.ComponentLOC getLocData() {
        return locData;
    }

    *//**
     * <p>setLOCData</p>
     * <p>Description: setter for ComponentLOC</p>
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public void setLOCData(com.basis100.deal.entity.ComponentLOC loc) {
        this.locData = loc;
    }

    *//**
     * <p>getLoanData</p>
     * <p>Description: getter </p>
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public com.basis100.deal.entity.ComponentLoan getLoanData() {
        return loanData;
    }

    *//**
     * <p>setLoanData</p>
     * <p>Description: setter for ComponentLoan</p>
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public void setLoanData(
            com.basis100.deal.entity.ComponentLoan loan) {
        this.loanData = loan;
    }

    *//**
     * <p>getCcData</p>
     * <p>Description: getter </p>
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public com.basis100.deal.entity.ComponentCreditCard getCcData() {
        return ccData;
    }

    *//**
     * <p>setCcData</p>
     * <p>Description: setter for ComponentCreditCard</p>
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public void setCcData(
            com.basis100.deal.entity.ComponentCreditCard cc) {
        this.ccData = cc;
    }

    *//**
     * <p>getOdData</p>
     * <p>Description: getter </p>
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public com.basis100.deal.entity.ComponentOverdraft getOdData() {
        return odData;
    }

    *//**
     * <p>setOdData</p>
     * <p>Description: setter for ComponentOverdraft</p>
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public void setOdData(
            com.basis100.deal.entity.ComponentOverdraft od) {
        this.odData = od;
    }
    
    *//**
     * <p>getRequestData</p>
     * <p>Description: getter </p>
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public com.basis100.deal.entity.Request getRequestData() {
        return requestData;
    }

    *//**
     * <p>setOdData</p>
     * <p>Description: setter for ComponentOverdraft</p>
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public void setRequestData(
            com.basis100.deal.entity.Request rd) {
        this.requestData = rd;
    }
    
*/
	public void testTBR(){
        System.out.println("test");
}
}
