package com.basis100.BREngine;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.entity.RemoteException;
import com.ltx.unittest.base.BaseBREEntityTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>Title: ComponentOverdraftTest</p>
 * <p>Description: TestCase Class for ComponentMortgage BR with JUnit/UTF </p>
 *
 * @author MCM Team
 * @version 1.0 (Initial Version � Jul 10, 2008)
 */
public class ComponentOverdraftTest extends BaseBREEntityTest {
    com.basis100.deal.entity.Component componentData = null;
    com.basis100.deal.entity.Component component = null;
    com.basis100.deal.entity.ComponentOverdraft entityData; // The entity object that gets data from the Excel sheet.
    com.basis100.deal.entity.ComponentOverdraft entity; // The live entity, which loads data from the Db.
    ComponentOverdraft brEntity;
    String whereClause = "";

    /**
     * <p>Description: Constructor</p>
     *
     * @version 1.0 Initial Version. (XS_2.39) July 15, 2008
     */
    public ComponentOverdraftTest () {
        super();
        componentData = (com.basis100.deal.entity.Component) EntityTestUtil.getInstance().loadEntity(
            "com.basis100.deal.entity.Component");
        entityData=(com.basis100.deal.entity.ComponentOverdraft) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.ComponentOverdraft");

    }

    /**
     * <p>setupTestData</p>
     * <p>Description: setting up Test Data</p>
     *
     * @version 1.0 Initial Version.(XS 2.39) July 15, 2008
     */
    public void setupTestData () throws Exception {

        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.ejbStore();

        setCopyId(deal.getCopyId());

        component = (com.basis100.deal.entity.Component) EntityTestUtil.getInstance().loadEntity(
            "com.basis100.deal.entity.Component");

        component.create(deal.getDealId(), deal.getCopyId(), 
                componentData.getComponentTypeId(),
                componentData.getMtgProdId());
        component.ejbStore();

        /** ****************componentLoan Data******************************** */
        entity = ( com.basis100.deal.entity.ComponentOverdraft) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.ComponentOverdraft");
        entity.create(component.getComponentId(), component.getCopyId()); 
        entity.setOverDraftAmount(entityData.getOverDraftAmount());
        entity.ejbStore();
    }

    /**
     * <p>testEntity</p>
     * <p>Description: test method of this entity</p>
     * 
     * @throws Exception
     * 
     * @version 1.0 MCM Team July 15, 2008 (XS 2.39)
     */
    public void testEntity() throws Exception {
        try {
            setupTestData();
            whereClause="ComponentId = " + entity.getComponentId() + " and copyid = " + entity.getCopyId();
            brEntity = new ComponentOverdraft(interpretor, whereClause);
            assertEquals(entity.getComponentId(), brEntity.componentId);            
            assertEquals(entity.getCopyId(), brEntity.copyId);
            assertEquals(entity.getOverDraftAmount(), brEntity.overdraftAmount);
            
        } catch (Exception e) {
            throw e;
        } finally  {
            tearDownTestData();
        }
    }

    /**
     * <p>Description: This method is for removing the data that get stored. </p>
     *
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     */
    public void tearDownTestData () throws Exception {

        if (deal != null) {

            deal.dcm = null;
            // this remove componentLoan as well
            component.ejbRemove(false);
        }

        try {
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();

        } catch (RemoteException e) {
            e.printStackTrace();
        }

        System.out.println("ejb remove done");
    }

    /**
     * <p>Description: This method invokes the createSuite() of DDStepsSuiteFactory class. </p>
     *
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     */
    public static Test suite () {
        return (Test) DDStepsSuiteFactory.createSuite(ComponentLoanTest.class);
    }

    /**
     * <p>getComponentData</p>
     * <p>Description: getter </p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     */
    public com.basis100.deal.entity.Component getComponentData () {
        return componentData;
    }

    /**
     * <p>setComponentData</p>
     * <p>Description: setter for Component</p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     */
    public void setComponentData (com.basis100.deal.entity.Component componentData) {
        this.componentData = componentData;
    }

    /**
     * <p>getEntityData</p>
     * <p>Description: getter for ComponentMortgage</p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     */
    public com.basis100.deal.entity.ComponentOverdraft getEntityData() {
        return entityData;
    }

    /**
     * <p>setEntityData</p>
     * <p>Description: setter for ComponentMortgage</p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     */
    public void setEntityData(com.basis100.deal.entity.ComponentOverdraft entityData) {
        this.entityData = entityData;
    }
}
