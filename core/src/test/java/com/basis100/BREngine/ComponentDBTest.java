package com.basis100.BREngine;

import java.io.IOException;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.dbcp.DelegatingConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import com.basis100.FXDBTestCase;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.filogix.express.datasource.ExpressConnection;
import com.filogix.express.datasource.ExpressConnectionFactory;
import com.filogix.express.datasource.ExpressDataSourceException;
import com.filogix.express.datasource.IExpressConnection;
import com.filogix.express.state.vpd.VPDStateAware;
import com.filogix.express.state.vpd.VPDStateDetectable;
import com.ltx.unittest.util.EntityTestUtil;

public class ComponentDBTest extends FXDBTestCase{
	
	private IDataSet dataSetTest;
	private Component component;
	Object obj =null; 
	private BRInterpretor interpretor;
	private DealEntity entity;
	
	
	
	public ComponentDBTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(Component.class.getSimpleName() + "DataSetTest.xml"));
	}

	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		   interpretor = new BRInterpretor();
           try
             {
                 interpretor.setConnection((EntityTestUtil.getSessionResourceKit().getJdbcExecutor()).getCon());
             }
             catch(Exception e)
             {
                 e.printStackTrace();
             }             
		this.component = new Component(interpretor,89989,21);
	}
	public void testFetchField() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, SQLException, CloneNotSupportedException{
		obj=new Object();
		ITable testFetchField = dataSetTest.getTable("testFetchField");		
		String id=(String)testFetchField.getValue(0,"DEALID");
	    int dealId=Integer.parseInt(id);
		String copy=(String)testFetchField.getValue(0,"COPYID");
		int copyId=Integer.parseInt(copy);	
	    Deal deal=new Deal(srk,CalcMonitor.getMonitor(srk),dealId,copyId);
	    DealEntity entity=(DealEntity)deal;
	    obj=component.fetchField("ComponentMortgage");
	    assert component.getDefaultSQL()!=null;
	}
}
