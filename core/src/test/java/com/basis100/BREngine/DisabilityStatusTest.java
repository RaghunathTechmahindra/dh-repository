package com.basis100.BREngine;

import java.io.IOException;
import java.sql.SQLException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.ltx.unittest.util.EntityTestUtil;

public class DisabilityStatusTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private DisabilityStatus disabilityStatus;
	private BRInterpretor interpretor=null;
	Object obj =null; 
	
	public DisabilityStatusTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(DisabilityStatus.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(DisabilityStatus.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.INSERT;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		interpretor = new BRInterpretor();
		try
        {
            interpretor.setConnection((EntityTestUtil.getSessionResourceKit().getJdbcExecutor()).getCon());
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
		this.disabilityStatus = new DisabilityStatus(interpretor);
		
	}
	
	public void testOpen() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, SQLException{
		obj=new Object();		
		ITable testOpen = dataSetTest.getTable("testOpen");
	    Deal deal=new Deal(srk,null);
	    DealEntity entity=(DealEntity)deal; 
	    obj=disabilityStatus.Open();	 
	    assertNotNull(obj);
} 
}
