package com.basis100.BREngine;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.entity.FinderException;


public class BRUtilTest  extends FXDBTestCase{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(BRUtilTest.class);
    
    //Initial data to be loaded before each test case.
	private final static String INIT_XML_DATA = "DBTestDataSet.xml";
	private IDataSet dataSetTest;
BRUtil brUtil=null;
	
	/**
     * Constructor function
     */
    public BRUtilTest(String name)  throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource(BRUtilTest.class.getSimpleName() + "DataSetTest.xml"));
    }
    
    @Override
	protected IDataSet getDataSet() throws Exception {
    	return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}
    
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
        return DatabaseOperation.INSERT;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.DELETE;
    }
    

    /**
     * test isBoolean.
     */
    @Test
    public static void testIsBoolean()
    {
    	_logger.info("start testIsBoolean");
    	
    	BRUtil util = new BRUtil();
    	
    	assert false == util.isBoolean("");
    	assert true == util.isBoolean("true");
    	assert true == util.isBoolean("false");
    	assert false == util.isBoolean("string");
    	
    	_logger.info("end testIsBoolean");
    	
    }
    
    
    /**
     * test isDate.
     */
    @Test
    public static void testIsDate()
    {
    	_logger.info("start testIsDate");
    	
    	BRUtil util = new BRUtil();
    	
    	assert true == util.isDate("1999-10-09");
    	assert false == util.isDate("date");
    	
    	_logger.info("end testIsDate");
    	
    }
    
    /**
     * test isNull.
     */
    @Test
    public static void testIsNull()
    {
    	_logger.info("start testIsNull");
    	
    	BRUtil util = new BRUtil();
    	
    	assert true == util.isNull("null");
    	assert false == util.isNull("string");
    	
    	_logger.info("end testIsNull");
    }
    
    /**
     * test isTime.
     */
    @Test
    public static void testIsTime()
    {
    	_logger.info("start testIsTime");
    	
    	BRUtil util = new BRUtil();
    	assert true == util.isTime("18:05:00");
    	assert false == util.isTime("string");
    	
    	_logger.info("end testIsTime");
    }
    
    /**
     * test isTimestamp.
     */
    @Test
    public static void testIsTimestamp()
    {
    	_logger.info("start testIsTimestamp");
    	
    	BRUtil util = new BRUtil();
    	assert true == util.isTimestamp("2005-04-06 09:01:10");
    	assert false == util.isTimestamp("string");
    	
    	_logger.info("end testIsTimestamp");
    }
    
    
    /**
     * test isInteger.
     */
    @Test
    public static void testIsInteger()
    {
    	_logger.info("start testIsInteger");
    	
    	BRUtil util = new BRUtil();
    	
    	assert false == util.isInteger("");
    	
    	assert true == util.isInteger("20k");
    	assert true == util.isInteger("10K");
    	assert true == util.isInteger("20m");
    	assert true == util.isInteger("10M");
    	
    	assert false == util.isInteger("10l");
    	
    	assert true == util.isInteger("-20");
    	
    	_logger.info("end testIsInteger");
    }
    
    /**
     * test isFloat.
     */
    @Test
    public static void testIsFloat()
    {
    	_logger.info("start testIsFloat");
    	
    	BRUtil util = new BRUtil();
    	
    	try
    	{
    		assert false == util.isFloat("");
        	
        	assert false == util.isFloat("20k");
        	assert true == util.isFloat("10.0K");
        	assert true == util.isFloat("20.0m");
        	assert false == util.isFloat("10M");
        	
        	assert false == util.isFloat("-10l");
        	
	    	util.isFloat("-20.0.0");
    	}
    	catch (ParseException pe)
    	{
    		assert true;
    	}
    	
    	_logger.info("end testIsFloat");
    }
    
    /**
     * test isDealEntity.
     */
    @Test
    public void testIsDealEntity() throws DataSetException
    {
    	_logger.info("start testIsDealEntity");
    	
    	ITable testDealEntity = dataSetTest.getTable("testDealEntity");
 	    int dealId=Integer.parseInt((String)testDealEntity.getValue(0,"dealId"));
 	    int copyId=Integer.parseInt((String)testDealEntity.getValue(0,"copyId"));
    	
 	    BRUtil util = new BRUtil();
    	
    	try
    	{
    		assert false == util.isDealEntity(null, "token");
    		assert false == util.isDealEntity(null, "");
    		
    		Deal de=new Deal(srk, null, dealId, copyId);	    	
	    	DealEntity entity=(DealEntity)de;
	    	
	    	assert false == util.isDealEntity(entity, "token");
        	
    	}
    	catch (FinderException pe)
    	{
    		assert true;
    	}
    	catch (Exception ex)
    	{
    		ex.printStackTrace();
    	}
    	
    	_logger.info("end testIsDealEntity");
    }
    public void testMapNull() throws Exception {
        brUtil=new BRUtil();
		ITable testMapNullFailure = dataSetTest.getTable("testMapNull");
		String value=testMapNullFailure.getValue(0, "value").toString();
		Boolean result=brUtil.mapNull(value);
		assertSame(null, result);
	}
	public void testMapNullFailure() throws Exception {
		Boolean result=false;
		try{
		ITable testMapNullFailure = dataSetTest.getTable("testMapNullFailure");
		String value=testMapNullFailure.getValue(0, "value").toString();
		result=brUtil.mapNull(value);
		}catch(Exception e){
			result=false;
		}
		assertSame(false, result);
	}
}
