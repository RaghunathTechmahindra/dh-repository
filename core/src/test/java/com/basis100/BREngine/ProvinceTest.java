package com.basis100.BREngine;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.ltx.unittest.util.EntityTestUtil;

public class ProvinceTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private Province province;
	Object obj =null; 
	private BRInterpretor interpretor;
	private DealEntity entity;
	
	public ProvinceTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(Province.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(Province.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.INSERT;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		interpretor = new BRInterpretor();
		try
        {
            interpretor.setConnection((EntityTestUtil.getSessionResourceKit().getJdbcExecutor()).getCon());
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
	}
	
	public void testUpdate() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, ParseException, SQLException, JdbcTransactionException{
		obj=new Object();		
		ITable testUpdate = dataSetTest.getTable("testUpdate");	
		srk.beginTransaction();
		 String provinces=(String)testUpdate.getValue(0,"PROVINCEID");
		 int provinceId=Integer.parseInt(provinces);
		 String provinceName=(String)testUpdate.getValue(0,"PROVINCENAME");
		 province = new Province(interpretor,provinceId);		 
		 province.setProvinceName(provinceName);
		 province.update();	    
	     assertNotNull(province);
	     srk.rollbackTransaction();
}
	
	/*	public void testInsert() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, ParseException, SQLException, JdbcTransactionException{
		obj=new Object();		
		ITable testInsert = dataSetTest.getTable("testInsert");	
		srk.beginTransaction();
		 String provinces=(String)testInsert.getValue(0,"PROVINCEID");
		 short provinceId=Short.parseShort(provinces);
		 String provinceName=(String)testInsert.getValue(0,"PROVINCENAME");
		 String provinceAbbreviation=(String)testInsert.getValue(0,"PROVINCEABBREVIATION");
		 String provinceExtract=(String)testInsert.getValue(0,"PROVINCETAXRATE");
		 short provinceEx=Short.parseShort(provinceExtract);
		 province = new Province(interpretor,provinceId);
		 province.setProvinceId(provinceId);
		 province.setProvinceName(provinceName);
		 province.setProvinceAbbreviation(provinceAbbreviation);		 
		 province.insert();	    
	     assertNotNull(province);
	     srk.rollbackTransaction();
}
	
	public void testDelete() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, ParseException, SQLException, JdbcTransactionException{
		obj=new Object();		
		ITable testDelete = dataSetTest.getTable("testDelete");	
		srk.beginTransaction();
		 String provinces=(String)testDelete.getValue(0,"PROVINCEID");
		 short provinceId=Short.parseShort(provinces);		
		 province = new Province(interpretor,provinceId);		 
		 province.setProvinceId(provinceId);
		 province.delete();	    
	     assertNotNull(province);
	     srk.rollbackTransaction();
}*/
}
