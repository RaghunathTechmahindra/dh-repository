package com.basis100.BREngine;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import junit.framework.Test;

import MosSystem.Mc;

import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.entity.RemoteException;
import com.ltx.unittest.base.BaseBREEntityTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>Title: ComponentTest</p>
 * <p>Description: TestCase Class for Component BR with JUnit/UTF </p>
 *
 * @author MCM Team
 * @version 1.0 (Initial Version � Jul 8, 2008)
 */
public class ComponentTest extends BaseBREEntityTest{

  /*private com.basis100.deal.entity.Component entityData; // The entity object that gets data from the Excel sheet.
    private com.basis100.deal.entity.Component entity; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentMortgage mtgData; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentMortgage componentMTG; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentLOC locData; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentLOC componentLOC; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentLoan loanData; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentLoan componentLoan; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentCreditCard ccData; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentCreditCard componentCC; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentOverdraft odData; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentOverdraft componentOD; // The live entity, which loads data from the Db.
    Component brEntity;
    String whereClause = "";

    *//**
     * <p>Description: Constructor</p>
     *
     * @version 1.0 Initial Version. (XS_2.39) July 15, 2008
     *//*
    public ComponentTest() {
        super();
        entityData=(com.basis100.deal.entity.Component) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Component");
        mtgData=(com.basis100.deal.entity.ComponentMortgage) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.ComponentMortgage");
        locData=(com.basis100.deal.entity.ComponentLOC) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.ComponentLOC");
        loanData=(com.basis100.deal.entity.ComponentLoan) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.ComponentLoan");
        ccData=(com.basis100.deal.entity.ComponentCreditCard) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.ComponentCreditCard");
        odData=(com.basis100.deal.entity.ComponentOverdraft) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.ComponentOverdraft");
    }

    *//**
     * <p>setupTestData</p>
     * <p>Description: setting up Test Data</p>
     * 
     * @version 1.0 Initial Version. (XS_2.39) July 15, 2008
     *//*
    public void setupTestData() throws Exception {
          
        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.ejbStore();
        setCopyId(deal.getCopyId());

        entity=(com.basis100.deal.entity.Component) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Component");
        entity.create(deal.getDealId(), deal.getCopyId(), 
                entityData.getComponentTypeId(),
                entityData.getMtgProdId()); 

        // componentId is created by create method so that needs to be set 
        entityData.setComponentId(entity.getComponentId());
        entityData.setCopyId(entity.getCopyId());

        switch (entity.getComponentTypeId()) {
            case Mc.COMPONENT_TYPE_MORTGAGE: 
                componentMTG = (com.basis100.deal.entity.ComponentMortgage) EntityTestUtil.getInstance()
                  .loadEntity("com.basis100.deal.entity.ComponentMortgage");
                componentMTG.create(entityData.getComponentId(), entityData.getCopyId());
                componentMTG.ejbStore();
                break;
            case Mc.COMPONENT_TYPE_LOC: 
                componentLOC = (com.basis100.deal.entity.ComponentLOC) EntityTestUtil.getInstance()
                  .loadEntity("com.basis100.deal.entity.ComponentLOC");
                componentLOC.create(entityData.getComponentId(), entityData.getCopyId());
                              
                componentLOC.ejbStore();
                break;
            case Mc.COMPONENT_TYPE_LOAN: 
                componentLoan = (com.basis100.deal.entity.ComponentLoan) EntityTestUtil.getInstance()
                      .loadEntity("com.basis100.deal.entity.ComponentLoan");
                componentLoan.create(entityData.getComponentId(), entityData.getCopyId());
                componentLoan.ejbStore();
                break;
            case Mc.COMPONENT_TYPE_CREDITCARD: 
                componentCC = (com.basis100.deal.entity.ComponentCreditCard) EntityTestUtil.getInstance()
                    .loadEntity("com.basis100.deal.entity.ComponentCreditCard");
                    componentCC.create(entityData.getComponentId(), entityData.getCopyId());
                    componentCC.setCreditCardAmount(ccData.getCreditCardAmount());
                    componentCC.ejbStore();
                break;
            case Mc.COMPONENT_TYPE_OVERDRAFT: 
                componentOD = (com.basis100.deal.entity.ComponentOverdraft) EntityTestUtil.getInstance()
                    .loadEntity("com.basis100.deal.entity.ComponentOverdraft");
                componentOD.create(entityData.getComponentId(), entityData.getCopyId());
                componentOD.setOverDraftAmount(odData.getOverDraftAmount());
                componentOD.ejbStore();
                break;
            default: break;
        }
    }

    *//**
     * <p>testEntity</p>
     * <p>Description: test method of this entity</p>
     *//*
    public void testEntity() throws Exception {
        try {
            setupTestData();
            whereClause="ComponentId = " + entity.getComponentId() + " and copyid = " + entity.getCopyId();
            brEntity = new Component(interpretor, whereClause);
            
            assertEquals(entity.getComponentId(), brEntity.componentId);
            assertEquals(entity.getMtgProdId(), brEntity.mtgProdId);
            
            switch (entity.getComponentTypeId()) {
                case Mc.COMPONENT_TYPE_MORTGAGE: 
                    assertNotNull(brEntity.fetchField("ComponentMortgage"));
                    assertNull(brEntity.fetchField("ComponentLOC"));
                    assertNull(brEntity.fetchField("ComponentLoan"));
                    assertNull(brEntity.fetchField("ComponentCreditcard"));
                    assertNull(brEntity.fetchField("ComponentOverdraft"));
                    break;
                case Mc.COMPONENT_TYPE_LOC: 
                    assertNull(brEntity.fetchField("ComponentMortgage"));
                    assertNotNull(brEntity.fetchField("ComponentLOC"));
                    assertNull(brEntity.fetchField("ComponentLoan"));
                    assertNull(brEntity.fetchField("ComponentCreditcard"));
                    assertNull(brEntity.fetchField("ComponentOverdraft"));
                    break;
                case Mc.COMPONENT_TYPE_LOAN: 
                    assertNull(brEntity.fetchField("ComponentMortgage"));
                    assertNull(brEntity.fetchField("ComponentLOC"));
                    assertNotNull(brEntity.fetchField("ComponentLoan"));
                    assertNull(brEntity.fetchField("ComponentCreditcard"));
                    assertNull(brEntity.fetchField("ComponentOverdraft"));
                    break;
            }
            
        } catch (Exception e) {
            throw e;
        } finally  {
            tearDownTestData();
        }

    }

    *//**
     * <p>Description: This method is for removing the data that get stored. </p>
     *
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     *//*
    public void tearDownTestData() throws Exception {

        if (deal != null) {
            deal.dcm = null;
            entity.ejbRemove(false);
        }

        try {
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        System.out.println("ejb remove done");
    }

    *//**
     * <p>Description: This method invokes the createSuite() of DDStepsSuiteFactory class. </p>
     *
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     *//*
    public static Test suite() {
        return (Test) DDStepsSuiteFactory.createSuite(ComponentTest.class);
    }

    *//**
    * <p>getEntityData</p>
    * <p>Description: getter </p>
    * @version 1.0  15 July, 2008 (initial version) XS 3.39 
    * @auther MCM Team 
    *//*
    public com.basis100.deal.entity.Component getEntityData() {
        return entityData;
    }

    *//**
     * <p>setEntityData</p>
     * <p>Description: setter for Component</p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     *//*
    public void setEntityData(com.basis100.deal.entity.Component entityData) {
        this.entityData = entityData;
    }
    
    *//**
     * <p>getMtgData</p>
     * <p>Description: getter </p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     *//*
    public com.basis100.deal.entity.ComponentMortgage getMtgData() {
        return mtgData;
    }

    *//**
     * <p>setMtgData</p>
     * <p>Description: setter for ComponentMortgage</p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     *//*
    public void setMtgData(
            com.basis100.deal.entity.ComponentMortgage mtg) {
        this.mtgData = mtg;
    }

    *//**
     * <p>getLocData</p>
     * <p>Description: getter </p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     *//*
    public com.basis100.deal.entity.ComponentLOC getLocData() {
        return locData;
    }

    *//**
     * <p>setLOCData</p>
     * <p>Description: setter for ComponentLOC</p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     *//*
    public void setLOCData(com.basis100.deal.entity.ComponentLOC loc) {
        this.locData = loc;
    }

    *//**
     * <p>getLoanData</p>
     * <p>Description: getter </p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     *//*
    public com.basis100.deal.entity.ComponentLoan getLoanData() {
        return loanData;
    }

    *//**
     * <p>setLoanData</p>
     * <p>Description: setter for ComponentLoan</p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     *//*
    public void setLoanData(
            com.basis100.deal.entity.ComponentLoan loan) {
        this.loanData = loan;
    }

    *//**
     * <p>getCcData</p>
     * <p>Description: getter </p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     *//*
    public com.basis100.deal.entity.ComponentCreditCard getCcData() {
        return ccData;
    }

    *//**
     * <p>setCcData</p>
     * <p>Description: setter for ComponentCreditCard</p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     *//*
    public void setCcData(
            com.basis100.deal.entity.ComponentCreditCard cc) {
        this.ccData = cc;
    }

    *//**
     * <p>getOdData</p>
     * <p>Description: getter </p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     *//*
    public com.basis100.deal.entity.ComponentOverdraft getOdData() {
        return odData;
    }

    *//**
     * <p>setOdData</p>
     * <p>Description: setter for ComponentOverdraft</p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     *//*
    public void setOdData(
            com.basis100.deal.entity.ComponentOverdraft od) {
        this.odData = od;
    }*/
    public void testTBR(){
        System.out.println("test");
}


}
