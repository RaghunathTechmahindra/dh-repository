package com.basis100.BREngine;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.ltx.unittest.util.EntityTestUtil;

public class PropertyTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private Property property;
	Object obj =null; 
	private BRInterpretor interpretor;
	private DealEntity entity;
	
	public PropertyTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(Property.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(Property.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.INSERT;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		interpretor = new BRInterpretor();
		try
        {
            interpretor.setConnection((EntityTestUtil.getSessionResourceKit().getJdbcExecutor()).getCon());
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }		
		this.property = new Property(interpretor);		
		
	}
	
	public void testUpdate() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, ParseException, SQLException{
		obj=new Object();		
		ITable testUpdate = dataSetTest.getTable("testUpdate");	
		try {
			srk.beginTransaction();
			 String propertyCity="PROPERTYCITY='Quebeck'";
		     property.update(propertyCity);	    
		     assertNotNull(property);
		     srk.rollbackTransaction();
		} catch (JdbcTransactionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	     
}
}
