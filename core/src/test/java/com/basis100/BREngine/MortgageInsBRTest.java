package com.basis100.BREngine;

import java.util.Vector;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import MosSystem.Mc;

import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.base.BaseBREEntityTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>Title: MortgageInsBRTest</p>
 * <p>Description: TiledViewBean for ComponentInfo Section </p>
 * 
 *
 * @author MCM Team
 * @version 1.0 (Initial Version � Aug 7, 2008)
 */

public class MortgageInsBRTest extends BaseBREEntityTest {/*
    
    private com.basis100.deal.entity.Component componentData1; // The entity object that gets data from the Excel sheet.
    private com.basis100.deal.entity.Component component1; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentMortgage mtgData1; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentMortgage componentMTG1; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.Component componentData2; // The entity object that gets data from the Excel sheet.
    private com.basis100.deal.entity.Component component2; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentMortgage mtgData2; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentMortgage componentMTG2; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.Component componentData; // The entity object that gets data from the Excel sheet.
    private com.basis100.deal.entity.Component component; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentLOC locData; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.ComponentLOC componentLOC; // The live entity, which loads data from the Db.
    private com.basis100.deal.entity.AssetType dummy; // dummy entity to get expected result from excel

    private static String[] msgs = {"One or more components have an amortization greater than the amortization of the deal/umbrella mortgage.  (Rule DMC-004)",
                                     "More than one component has been selected to include the Mortgage Insurance premium.  (Rule DMC-005)",
                                     "The Mortgage Insurance premium has not been allocated to a component.  (Rule DMC-006)"};
    
    String whereClause = "";
    
    *//**
     * <p>Description: Constructor</p>
     *
     * @version 1.0 Initial Version. (XS_2.39) July 15, 2008
     *//*
    public MortgageInsBRTest() {
        super();
        componentData1=(com.basis100.deal.entity.Component) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Component");
        mtgData1=(com.basis100.deal.entity.ComponentMortgage) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.ComponentMortgage");
        componentData2=(com.basis100.deal.entity.Component) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Component");
        mtgData2=(com.basis100.deal.entity.ComponentMortgage) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.ComponentMortgage");
        componentData=(com.basis100.deal.entity.Component) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Component");
        locData=(com.basis100.deal.entity.ComponentLOC) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.ComponentLOC");
        dummy=(com.basis100.deal.entity.AssetType) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.AssetType");
    }
    
    *//**
     * <p>Description: Set up test data to check BR OCR-007. <br>
     * Deal has components
     *
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * 
     *//*
    public void setupTestData() throws Exception {
        
        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        int dummyData = dummy.getNetWorthInclusion();
        if (dummyData>0 && dummyData<100) {
            deal.setAmortizationTerm(dummyData);
        } else {
            deal.setMIPremiumAmount(dummyData);            
        }
        deal.ejbStore();
        SessionResourceKit srk = deal.getSessionResourceKit();
        setCopyId(deal.getCopyId());
        
        if (componentData1.getComponentTypeId() != 0) {
            component1=(com.basis100.deal.entity.Component) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Component");
            component1.create(deal.getDealId(), deal.getCopyId(), 
                    componentData1.getComponentTypeId(),
                    componentData1.getMtgProdId()); 
        
            // componentId is created by create method so that needs to be set 
            componentData1.setComponentId(component1.getComponentId());
            componentData1.setCopyId(component1.getCopyId());
        
            componentMTG1 = (com.basis100.deal.entity.ComponentMortgage) EntityTestUtil.getInstance()
              .loadEntity("com.basis100.deal.entity.ComponentMortgage");
            componentMTG1.create(componentData1.getComponentId(), componentData1.getCopyId());
            componentMTG1.setAmortizationTerm(mtgData1.getAmortizationTerm());
            componentMTG1.setMiAllocateFlag(mtgData1.getMiAllocateFlag());
            componentMTG1.ejbStore();
            
            mtgData1.setCopyId(componentMTG1.getCopyId());
        }
        
        if (componentData2.getComponentTypeId() != 0) {
            component2=(com.basis100.deal.entity.Component) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Component");
            component2.create(deal.getDealId(), deal.getCopyId(), 
                    componentData2.getComponentTypeId(),
                    componentData2.getMtgProdId()); 
    
            // componentId is created by create method so that needs to be set 
            componentData2.setComponentId(component2.getComponentId());
            componentData2.setCopyId(component2.getCopyId());
    
            componentMTG2 = (com.basis100.deal.entity.ComponentMortgage) EntityTestUtil.getInstance()
              .loadEntity("com.basis100.deal.entity.ComponentMortgage");
            componentMTG2.create(componentData2.getComponentId(), componentData2.getCopyId());
            componentMTG2.setAmortizationTerm(mtgData2.getAmortizationTerm());
            componentMTG2.setMiAllocateFlag(mtgData2.getMiAllocateFlag());
            componentMTG2.ejbStore();
            
            mtgData2.setCopyId(componentMTG2.getCopyId());
        }
        
        if (componentData.getComponentTypeId() != 0) {
            component=(com.basis100.deal.entity.Component) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Component");
            component.create(deal.getDealId(), deal.getCopyId(), 
                    componentData.getComponentTypeId(),
                    componentData.getMtgProdId()); 
    
            // componentId is created by create method so that needs to be set 
            componentData.setComponentId(component.getComponentId());
            componentData.setCopyId(component.getCopyId());
    
            componentLOC = (com.basis100.deal.entity.ComponentLOC) EntityTestUtil.getInstance()
              .loadEntity("com.basis100.deal.entity.ComponentLOC");
            componentLOC.create(componentData.getComponentId(), componentData.getCopyId());
            componentLOC.setMiAllocateFlag(locData.getMiAllocateFlag());
            componentLOC.ejbStore();
            
            locData.setCopyId(componentLOC.getCopyId());
        }

    }

    private void updateDealAmortization(int term) {
        try {
            deal.setAmortizationTerm(term);
            deal.ejbStore();
        } catch (Exception e) {
            
        }
    }
    
    private void updateDealMIpremium(double premium) {
        try {
            deal.setMIPremiumAmount(premium);
            deal.ejbStore();
        } catch (Exception e) {
            
        }
    }

    *//**
     * <p>Description: This method is for removing the data that get stored. </p>
     *
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public void tearDownTestData() throws Exception {

        if (deal != null) {
            deal.dcm = null;
        }

        try {
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        System.out.println("ejb remove done");
    }

    *//**
     * <p>Description: This method test when pass BRDMC004 with deal amortization term = 20</p>
     *
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public void testBusinessRule() throws Exception {
        try {
            setupTestData();
            updateDealAmortization(15);
            SessionResourceKit srk = deal.getSessionResourceKit();
    
            BusinessRuleExecutor brExec = new BusinessRuleExecutor();
            PassiveMessage pmProp = new PassiveMessage();
    
            brExec.BREValidator((DealPK) deal.getPk(), srk, pmProp, "DMC-%");
            Vector pmMsgs = pmProp.getMsgs();
            int expectedResult = dummy.getAssetTypeId();
            if (expectedResult > 0) {
                assertTrue(pmMsgs.contains(msgs[dummy.getAssetTypeId()-1]));
            } else {
                assertTrue(true);
            }
            
            tearDownTestData();
        } catch (Exception e) {
            throw e;
        } finally  {
            tearDownTestData();
        }
    }
    
    
    *//**
     * <p>Description: This method invokes the createSuite() of DDStepsSuiteFactory class. </p>
     *
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public static Test suite() {
        return (Test) DDStepsSuiteFactory.createSuite(MortgageInsBRTest.class);
    }

    *//**
     * <p>getComponentData</p>
     * <p>Description: getter </p>
      * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
     public com.basis100.deal.entity.Component getComponentData1() {
         return componentData1;
     }

     *//**
      * <p>setComponentData</p>
      * <p>Description: setter for Component</p>
      * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
      * @auther MCM Team 
      *//*
     public void setComponentData1(com.basis100.deal.entity.Component componentData) {
         this.componentData1 = componentData;
     }
     
     *//**
      * <p>getComponentData</p>
      * <p>Description: getter </p>
       * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
      * @auther MCM Team 
      *//*
      public com.basis100.deal.entity.Component getComponentData2() {
          return componentData2;
      }

      *//**
       * <p>setComponentData</p>
       * <p>Description: setter for Component</p>
       * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
       * @auther MCM Team 
       *//*
      public void setComponentData2(com.basis100.deal.entity.Component componentData) {
          this.componentData2 = componentData;
      }
      
      *//**
       * <p>getMtgData</p>
       * <p>Description: getter </p>
       * @version 1.0  15 July, 2008 (initial version) XS 3.39 
       * @auther MCM Team 
       *//*
      public com.basis100.deal.entity.ComponentMortgage getMtgData1() {
          return mtgData1;
      }

      *//**
       * <p>setMtgData</p>
       * <p>Description: setter for ComponentMortgage</p>
       * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
       * @auther MCM Team 
       *//*
      public void setMtgData1(
              com.basis100.deal.entity.ComponentMortgage mtg) {
          this.mtgData1 = mtg;
      }

      *//**
       * <p>getMtgData</p>
       * <p>Description: getter </p>
       * @version 1.0  15 July, 2008 (initial version) XS 3.39 
       * @auther MCM Team 
       *//*
      public com.basis100.deal.entity.ComponentMortgage getMtgData2() {
          return mtgData2;
      }

      *//**
       * <p>setMtgData</p>
       * <p>Description: setter for ComponentMortgage</p>
       * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
       * @auther MCM Team 
       *//*
      public void setMtgData2(
              com.basis100.deal.entity.ComponentMortgage mtg) {
          this.mtgData2 = mtg;
      }

      *//**
       * <p>getComponentData</p>
       * <p>Description: getter </p>
        * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
       * @auther MCM Team 
       *//*
       public com.basis100.deal.entity.Component getComponentData() {
           return componentData;
       }

       *//**
        * <p>setComponentData</p>
        * <p>Description: setter for Component</p>
        * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
        * @auther MCM Team 
        *//*
       public void setComponentData(com.basis100.deal.entity.Component componentData) {
           this.componentData = componentData;
       }

    *//**
     * <p>getLocData</p>
     * <p>Description: getter </p>
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public com.basis100.deal.entity.ComponentLOC getLocData() {
        return locData;
    }

    *//**
     * <p>setLOCData</p>
     * <p>Description: setter for ComponentLOC</p>
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public void setLOCData(com.basis100.deal.entity.ComponentLOC loc) {
        this.locData = loc;
    }
    
    *//**
     * <p>getDummy</p>
     * <p>Description: getter </p>
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public com.basis100.deal.entity.AssetType getDummy() {
        return dummy;
    }

    *//**
     * <p>setOdData</p>
     * <p>Description: setter for ComponentOverdraft</p>
     * @version 1.0 Aug 5, 2008 XS 10.2 Initial version
     * @auther MCM Team 
     *//*
    public void setDummy(
            com.basis100.deal.entity.AssetType at) {
        this.dummy = at;
    }
*/ 

	public void testTBR(){
        System.out.println("test");
}
	
}
