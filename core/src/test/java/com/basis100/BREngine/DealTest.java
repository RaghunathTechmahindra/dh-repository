package com.basis100.BREngine;

import java.io.IOException;
import java.sql.SQLException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.DealEntity;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.ltx.unittest.util.EntityTestUtil;

public class DealTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	Object obj =null;	
	private BRInterpretor interpretor;
	private Deal deal;

	
	public DealTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(Component.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(Component.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.INSERT;	
	}

	@Override
    protected void setUp() throws Exception {
          super.setUp();
             interpretor = new BRInterpretor();
      try
        {
            interpretor.setConnection((EntityTestUtil.getSessionResourceKit().getJdbcExecutor()).getCon());
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
          this.deal = new Deal(interpretor);
    }
    
    public void testFetchField() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, SQLException, CloneNotSupportedException{
           obj=new Object();
          ITable testFetchField = dataSetTest.getTable("testFetchField");         
          String id=(String)testFetchField.getValue(0,"DEALID");
        int dealId=Integer.parseInt(id);
          String copy=(String)testFetchField.getValue(0,"COPYID");
          int copyId=Integer.parseInt(copy);  
        Deal deal=new Deal(interpretor);       
        obj=deal.fetchField("Property");
        assertNotNull(obj);
    
}

}
