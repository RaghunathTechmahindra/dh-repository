package com.basis100.BREngine;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.conditions.sysgen.PremiumSalesTax;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.docprep.extract.FMLQueryResult;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.ltx.unittest.util.EntityTestUtil;

public class ForNodeTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private ForNode forNode;
	Object obj =null; 
	private BRInterpretor interpretor;
	private DealEntity entity;
	
	public ForNodeTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ForNode.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(ForNode.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.INSERT;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		interpretor = new BRInterpretor();
		try
        {
            interpretor.setConnection((EntityTestUtil.getSessionResourceKit().getJdbcExecutor()).getCon());
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }		
		this.forNode = new ForNode(interpretor);		
		
	}
	
	/*public void testEvaluate() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, ParseException, SQLException{
		obj=new Object();		
		ITable testEvaluate = dataSetTest.getTable("testEvaluate");		
	    obj=forNode.evaluate();	    
	   assertNull(obj);
}
	
	public void testParseExpressionPhase2() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, ParseException, SQLException{
	Boolean parse=true;	
	ITable testParseExpressionPhase2 = dataSetTest.getTable("testParseExpressionPhase2");	
	parse=forNode.parseExpressionPhase2();	    
    assertTrue(parse);	
}*/
	

	public void testTBR(){
	System.out.println("test");
	 }

}
