package com.basis100.BREngine;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.entity.RemoteException;
import com.ltx.unittest.base.BaseBREEntityTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>Title: ComponentMortgageTest</p>
 * <p>Description: TestCase Class for ComponentMortgage BR with JUnit/UTF </p>
 *
 * @author MCM Team
 * @version 1.0 (Initial Version � Jul 10, 2008)
 */
public class ComponentMortgageTest extends BaseBREEntityTest {

    com.basis100.deal.entity.Component componentData = null;
    com.basis100.deal.entity.Component component = null;
    com.basis100.deal.entity.ComponentMortgage entityData; // The entity object that gets data from the Excel sheet.
    com.basis100.deal.entity.ComponentMortgage entity; // The live entity, which loads data from the Db.
    ComponentMortgage brEntity;
    String whereClause = "";

    /**
     * <p>Description: Constructor</p>
     *
     * @version 1.0 Initial Version. (XS_2.39) July 15, 2008
     */
    public ComponentMortgageTest () {
        super();
        componentData = (com.basis100.deal.entity.Component) EntityTestUtil.getInstance().loadEntity(
            "com.basis100.deal.entity.Component");
        entityData=(com.basis100.deal.entity.ComponentMortgage) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.ComponentMortgage");

    }

    /**
     * <p>setupTestData</p>
     * <p>Description: setting up Test Data</p>
     *
     * @version 1.0 Initial Version.(XS 2.39) July 15, 2008
     */
    public void setupTestData () throws Exception {

        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.ejbStore();

        setCopyId(deal.getCopyId());

        component = (com.basis100.deal.entity.Component) EntityTestUtil.getInstance().loadEntity(
            "com.basis100.deal.entity.Component");

        component.create(deal.getDealId(), deal.getCopyId(), 
                componentData.getComponentTypeId(),
                componentData.getMtgProdId());

        component.ejbStore();

        /** ****************componentMortgage Data******************************** */
        entity = ( com.basis100.deal.entity.ComponentMortgage) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.ComponentMortgage");
        entity.create(component.getComponentId(), component.getCopyId());
        entity.setPaymentFrequencyId(entityData.getPaymentFrequencyId());
        entity.setPrePaymentOptionsId(entityData.getPrePaymentOptionsId());
        entity.setPrivilegePaymentId(entityData.getPrivilegePaymentId());
        entity.setCashBackAmountOverride(entityData.getCashBackAmountOverride());
        entity.setRateLock(entityData.getRateLock());
        entity.setPropertyTaxAllocateFlag(entityData.getPropertyTaxAllocateFlag());
        entity.setExistingAccountIndicator(entityData.getExistingAccountIndicator());
        entity.setExistingAccountNumber(entityData.getExistingAccountNumber());
        entity.setRateGuaranteePeriod(entityData.getRateGuaranteePeriod());
//        entity.setFirstPaymentDate(entityData.getFirstPaymentDate());
        entity.setCashBackAmount(entityData.getCashBackAmount());
        entity.setCashBackPercent(entityData.getCashBackPercent());
        entity.setActualPaymentTerm(entityData.getActualPaymentTerm());
        entity.setAdditionalPrincipal(entityData.getAdditionalPrincipal());
        entity.setAdvanceHold(entityData.getAdvanceHold());
        entity.setAmortizationTerm(entityData.getAmortizationTerm());
        entity.setBalanceRemainingAtEndOfTerm(entityData.getBalanceRemainingAtEndOfTerm());
        entity.setBuyDownRate(entityData.getBuyDownRate());
        entity.setCommissionCode(entityData.getCommissionCode());
        entity.setDiscount(entityData.getDiscount());
        entity.setEffectiveAmortizationMonths(entityData.getEffectiveAmortizationMonths());
//        entity.setFirstPaymentDateMonthly(entityData.getFirstPaymentDateMonthly());
        entity.setIadNumberOfDays(entityData.getIadNumberOfDays());
        entity.setInterestAdjustmentAmount(entityData.getInterestAdjustmentAmount());
//        entity.setInterestAdjustmentDate(entityData.getInterestAdjustmentDate());
//        entity.setMaturityDate(entityData.getMaturityDate());
        entity.setMiAllocateFlag(entityData.getMiAllocateFlag());
        entity.setNetInterestRate(entityData.getNetInterestRate());
        entity.setPAndIPaymentAmount(entityData.getPAndIPaymentAmount());
        entity.setPAndIPaymentAmountMonthly(entityData.getPAndIPaymentAmountMonthly());
        entity.setPerDiemInterestAmount(entityData.getPerDiemInterestAmount());
        entity.setPremium(entityData.getPremium());
        entity.setPropertyTaxEscrowAmount(entityData.getPropertyTaxEscrowAmount());
        entity.setRateLock(entityData.getRateLock());
        entity.setTotalMortgageAmount(entityData.getTotalMortgageAmount());
        entity.setTotalPaymentAmount(entityData.getTotalPaymentAmount());
        entity.setMortgageAmount(entityData.getMortgageAmount());
        entity.ejbStore();

    }

    /**
     * <p>testEntity</p>
     * <p>Description: test method of this entity</p>
     * 
     * @throws Exception
     * 
     * @version 1.0 MCM Team July 15, 2008 (XS 2.39)
     */
    public void testEntity() throws Exception {
        try {
            setupTestData();
            whereClause="ComponentId = " + entity.getComponentId() + " and copyid = " + entity.getCopyId();
            brEntity = new ComponentMortgage(interpretor, whereClause);
            
            assertEquals(entity.getComponentId(), brEntity.componentId);
            assertEquals(entity.getCopyId(), brEntity.copyId);
            assertEquals(entity.getCashBackAmount(), brEntity.cashbackAmount);
            assertEquals(entity.getCashBackPercent(), brEntity.cashbackPercent);
            assertEquals(entity.getRateGuaranteePeriod(), brEntity.rateGaranteePeriod);
            assertEquals(entity.getCashBackAmountOverride(), brEntity.cashabckAmountOverride);
            assertEquals(entity.getExistingAccountIndicator(), brEntity.existingAccountIndicator);
            assertEquals(entity.getExistingAccountNumber(), brEntity.existingAccountNumber);
//            assertEquals(entity.getFirstPaymentDate(), brEntity.firstpaymentDate);
            assertEquals(entity.getPropertyTaxAllocateFlag(), brEntity.propertyTaxAllocateFlag);
            assertEquals(entity.getPaymentFrequencyId(), brEntity.paymentFrequencyId);
            assertEquals(entity.getPrivilegePaymentId(), brEntity.privilegePaymentId);
            assertEquals(entity.getPrePaymentOptionsId(), brEntity.prePaymentOptionsId);
            assertEquals(entity.getActualPaymentTerm(), brEntity.actualPaymentTerm);
            assertEquals(entity.getAdvanceHold(), brEntity.advanceHold);
            assertEquals(entity.getAdditionalPrincipal(), brEntity.additionalPrincipal);
            assertEquals(entity.getAmortizationTerm(), brEntity.amortizationTerm);
            assertEquals(entity.getBalanceRemainingAtEndOfTerm(), brEntity.balanceRemainingAtEndOfTerm);
            assertEquals(entity.getBuyDownRate(), brEntity.buyDownRate);
            assertEquals(entity.getCommissionCode(), brEntity.commissionCode);
            assertEquals(entity.getDiscount(), brEntity.discount);
            assertEquals(entity.getEffectiveAmortizationMonths(), brEntity.effectiveAmortizationMonths);
//            assertEquals(entity.getFirstPaymentDateMonthly(), brEntity.firstPaymentDateMonthly);
            assertEquals(entity.getIadNumberOfDays(), brEntity.iadNumberOfDays);
            assertEquals(entity.getInterestAdjustmentAmount(), brEntity.interestAdjustmentAmount);
//            assertEquals(entity.getInterestAdjustmentDate(), brEntity.interestAdjustmentDate);
//            assertEquals(entity.getMaturityDate(), brEntity.maturityDate);
            assertEquals(entity.getMiAllocateFlag(), brEntity.miAllocateFlag);
            assertEquals(entity.getNetInterestRate(), brEntity.netInterestRate);
            assertEquals(entity.getPAndIPaymentAmount(), brEntity.pAndIPaymentAmount);
            assertEquals(entity.getPAndIPaymentAmountMonthly(), brEntity.pAndIPaymentAmountMonthly);
            assertEquals(entity.getPerDiemInterestAmount(), brEntity.perDiemInterestAmount);
            assertEquals(entity.getPremium(), brEntity.premium);
            assertEquals(entity.getPropertyTaxEscrowAmount(), brEntity.propertyTaxEscrowAmount);
            assertEquals(entity.getRateLock(), brEntity.rateLock);
            assertEquals(entity.getTotalMortgageAmount(), brEntity.totalMortgageAmount);
            assertEquals(entity.getTotalPaymentAmount(), brEntity.totalPaymentAmount);
            assertEquals(entity.getMortgageAmount(), brEntity.mortgageAmount);

        } catch (Exception e) {
            throw e;
        } finally  {
            tearDownTestData();
        }
    }

    /**
     * <p>Description: This method is for removing the data that get stored. </p>
     *
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     */
    public void tearDownTestData () throws Exception {

        if (deal != null) {

            deal.dcm = null;
            // this remove componentLoan as well
            component.ejbRemove(false);
        }

        try {
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();

        } catch (RemoteException e) {
            e.printStackTrace();
        }

        System.out.println("ejb remove done");
    }

    /**
     * <p>Description: This method invokes the createSuite() of DDStepsSuiteFactory class. </p>
     *
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     */
    public static Test suite () {
        return (Test) DDStepsSuiteFactory.createSuite(ComponentLoanTest.class);
    }

    /**
     * <p>getComponentData</p>
     * <p>Description: getter </p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     */
    public com.basis100.deal.entity.Component getComponentData () {
        return componentData;
    }

    /**
     * <p>setComponentData</p>
     * <p>Description: setter for Component</p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     */
    public void setComponentData (com.basis100.deal.entity.Component componentData) {
        this.componentData = componentData;
    }

    /**
     * <p>getEntityData</p>
     * <p>Description: getter for ComponentMortgage</p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     */
    public com.basis100.deal.entity.ComponentMortgage getEntityData() {
        return entityData;
    }

    /**
     * <p>setEntityData</p>
     * <p>Description: setter for ComponentMortgage</p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     */
    public void setEntityData(com.basis100.deal.entity.ComponentMortgage entityData) {
        this.entityData = entityData;
    }
}
