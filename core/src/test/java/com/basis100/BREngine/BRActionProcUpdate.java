/**
 * <p>@(#)BRActionProcUpdate.java Oct 11, 2007</p>
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.basis100.BREngine;

import java.io.Writer;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

public class BRActionProcUpdate {
    
    public static final String selectSQL1 = "Select * from BusinessRule"; 
    public static final String updateSQL = "update BusinessRule set actionProc = empty_clob() where ";
    public static final String selectSQL2 = "Select actionProc from BusinessRule where ";

    SessionResourceKit srk;
    ArrayList rules;
    
    public BRActionProcUpdate() {
        ResourceManager.init();
        srk = new SessionResourceKit("BRActionProcUpdate");
    }
    
    public ArrayList getRules() throws Exception {
       
        ArrayList<BusinessRule> rules = new ArrayList<BusinessRule>(10);
        JdbcExecutor jExec = srk.getJdbcExecutor();

        int key = jExec.execute(selectSQL1);

        for (; jExec.next(key); ) {
            int institutionProfileId = jExec.getInt(key,"institutionProfileId");
            String ruleName = jExec.getString(key,"ruleName");
            oracle.sql.CLOB actionProcLob = (oracle.sql.CLOB)jExec.getClob(key,"ACTIONPROC");
            if (actionProcLob != null) {
                String actionProc = actionProcLob.getSubString(1, Integer.parseInt("" + actionProcLob.length()));
                if (actionProc.indexOf("execSQL") > 0) {
                    BusinessRule rule = new BRActionProcUpdate.BusinessRule();
                    rule.actionProcLob = actionProcLob;
                    rule.institutionProfileId = institutionProfileId;
                    rule.ruleName = ruleName;
                    rule.workflowId = jExec.getInt(key,"WORKFLOWID");
                    rule.taskId = jExec.getInt(key,"taskId");
                    rule.taskEvent = jExec.getString(key,"taskEvent");
                    rule.expression = jExec.getString(key,"expression");
                    rules.add(rule);
                }
            }
        }
        jExec.closeData(key);
        return rules;        
    }
    
    public void updateActionProc(BusinessRule rule, boolean secondUpdate) throws Exception {
    
        System.out.print("********** START UPDATE *******");
        System.out.println("InstituionPRofileId: " + rule.institutionProfileId);
        System.out.println("ruleName: " + rule.ruleName);
        oracle.sql.CLOB actionProcLob = rule.actionProcLob;
        String actionProc = actionProcLob.getSubString(1, Integer.parseInt("" + actionProcLob.length()));
        int startIndex = actionProc.indexOf("execSQL");
        while (startIndex > -1) {
            startIndex += 9;
            String head = actionProc.substring(0, startIndex);
            int endIndex = actionProc.indexOf("\"", startIndex);
            String insertSQL = actionProc.substring(startIndex, endIndex);
            String tail = actionProc.substring(endIndex);
            insertSQL = getAdjustedSql(rule.institutionProfileId, insertSQL, secondUpdate);
            actionProc = head + insertSQL + tail;
            startIndex = actionProc.indexOf("execSQL", endIndex);
        }
        
        String sql = updateSQL 
            + " INSTITUTIONPROFILEID = " + rule.institutionProfileId
            + " and ruleName = '" + rule.ruleName + "'"
            + " and workflowId = " + rule.workflowId
            + " and taskId = " + rule.taskId;
        
        if (rule.expression != null)
            sql = sql + " and expression = ?";
        srk.getExpressState().setDealInstitutionId(rule.institutionProfileId);
        srk.beginTransaction();
            
        JdbcExecutor jExec = srk.getJdbcExecutor();
        PreparedStatement pstmt = jExec.getCon().prepareStatement(sql); 
        if (rule.expression != null)
            pstmt.setString(1, rule.expression);
        pstmt.executeUpdate();

        Writer writer = null;
        
        sql = selectSQL2 
            + " INSTITUTIONPROFILEID = " + rule.institutionProfileId
            + " and ruleName like '%" + rule.ruleName.trim() + "%'"
            + " and workflowId = " + rule.workflowId
            + " and taskId = " + rule.taskId;
        if (rule.expression != null)
            sql = sql + " and expression = ?";

        pstmt = jExec.getCon().prepareStatement(sql);
        if (rule.expression != null)
            pstmt.setString(1, rule.expression);
        ResultSet rs = pstmt.executeQuery();

        if (rs.next()) {
            actionProcLob = (oracle.sql.CLOB)rs.getObject(1);
            writer = actionProcLob.getCharacterOutputStream();
        }

         rs.close();
         pstmt.close();
                  
         writer.write(actionProc);
         writer.flush();
         writer.close();
         
         pstmt = jExec.getCon().prepareStatement(sql);
         if (rule.expression != null)
             pstmt.setString(1, rule.expression);
         rs = pstmt.executeQuery();

         if (rs.next()) {
             actionProcLob = (oracle.sql.CLOB)rs.getObject(1);
             String newActionProc = actionProcLob.getSubString(1, Integer.parseInt("" + actionProcLob.length()));
         }
         rs.close();
         pstmt.close();
         srk.commitTransaction();

        System.out.print("********** END UPDATE *******");
    }

    /**
     * <p> getAdjustedSql </p>
     * BusinessRule ActionProc has insert statment for (currently found only) DocumentQueue
     * this method add InstitutionProfileId into insert statement.
     *
     * insert statmenet in ActionProc is currently only the next one.
     * should be one of the following 
     * 
     * insert into <TABLE_NAME> (COLNAME_a, COLNAME_b, ,,, ) values (aaa, bbb, ,,,,)
     * =>
     * insert into <TABLE_NAME> (COLNAME_a, COLNAME_b, ,,, , INSTITUTIONPROFILEID) 
     * values (aaa, bbb, ,,,,,{deal_institutionProfileId} )
     * 
     * but this method works for the following statment.
     * However, it is not really good statment since it rely on sequence number
     * 
     * insert into <TABLE_NAME> values (aaa, bbb, ccc, ddd,,,,)
     * => 
     * insert into <TABLE_NAME> values (aaa, bbb, ccc, ddd,,,,{deal_institutionProfileId} )
     * 
     * this method calls hasInstitutionProfileId passing table name
     * 
     * 
     * @param dealObj :com.basis100.BREngin.Deal
     * @param strStmt : String
     * @return String adjustedSql
     * @throws SQLException
     */
    public String getAdjustedSql(int institutionProfileId, String strStmt, boolean secondUpdate) 
        throws SQLException {
        
        String sql = strStmt.trim();
        int length = sql.length();
        StringTokenizer tokens = new StringTokenizer(sql, " (");
        if (tokens.countTokens() > 4 
            && tokens.nextToken().equalsIgnoreCase("insert")
            && tokens.nextToken().equalsIgnoreCase("into") ){
            
            if (secondUpdate && sql.indexOf("$InstitutionProfileId") > 0) {
                return sql;
            }
            String tableName = tokens.nextToken().toUpperCase();

            if (secondUpdate) {
                int lastIndexOfComma = sql.lastIndexOf(",");

                String head = sql.substring(0, lastIndexOfComma);
                sql = head + " , $InstitutionProfileId )";
            } else {
                String before0 = sql.substring(0, length -1);
                sql = before0 + " , $InstitutionProfileId )";
                
                boolean ifNonColumnList = tokens.nextToken().equalsIgnoreCase("values");
                
                if (!ifNonColumnList) {
                    int indexOfFirstCloseBracket = sql.indexOf(")");
                    String head = sql.substring(0, indexOfFirstCloseBracket);
                    String tail = sql.substring(indexOfFirstCloseBracket);
                    sql = head + " , INSTITUTIONPROFILEID" + tail;
                }
            }
        }
        
        return sql;
    }
    
    public static void main(String[] args) throws Exception {
                
        BRActionProcUpdate actionPUpdate = new BRActionProcUpdate();
        ArrayList<BusinessRule> rules = actionPUpdate.getRules();

        for (int i=0; i<rules.size(); i++) {
            BusinessRule rule = (BusinessRule)rules.get(i);
            if (args.length > 0)
                actionPUpdate
                    .updateActionProc(rule, 
                                      args[0].equalsIgnoreCase("true") ? true:false);
            else 
                actionPUpdate.updateActionProc(rule, false);
        }
        actionPUpdate.srk.freeResources();

    }

    private class BusinessRule {
        int institutionProfileId;
        String ruleName;
        int workflowId;
        int taskId;
        String taskEvent;
        String expression;
        oracle.sql.CLOB actionProcLob;
        
    }
}
