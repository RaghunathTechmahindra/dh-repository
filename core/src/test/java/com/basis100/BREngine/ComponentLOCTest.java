package com.basis100.BREngine;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.entity.RemoteException;
import com.ltx.unittest.base.BaseBREEntityTest;
import com.ltx.unittest.util.EntityTestUtil;
/**
 * <p>Title: </p>
 * <p>Description: TestCase Class for ComponentLOC BR with JUnit/UTF </p>
 *
 * @author MCM Team
 * @version 1.0 (Initial Version � Jul 10, 2008)
 */


public class ComponentLOCTest extends BaseBREEntityTest {
    
    com.basis100.deal.entity.Component componentData = null;
    com.basis100.deal.entity.Component component = null;
    com.basis100.deal.entity.ComponentLOC entityData; // The entity object that gets data from the Excel sheet.
    com.basis100.deal.entity.ComponentLOC entity; // The live entity, which loads data from the Db.
    ComponentLOC brEntity;
    String whereClause = "";

    /**
     * <p>Description: Constructor</p>
     *
     * @version 1.0 Initial Version. (XS_2.39) July 15, 2008
     */
    public ComponentLOCTest () {
        super();
        componentData = (com.basis100.deal.entity.Component) EntityTestUtil.getInstance().loadEntity(
            "com.basis100.deal.entity.Component");
        entityData=(com.basis100.deal.entity.ComponentLOC) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.ComponentLOC");

    }

    /**
     * <p>setupTestData</p>
     * <p>Description: setting up Test Data</p>
     *
     * @version 1.0 Initial Version.(XS 2.39) July 15, 2008
     */
    public void setupTestData () throws Exception {

        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.ejbStore();

        setCopyId(deal.getCopyId());

        component = (com.basis100.deal.entity.Component) EntityTestUtil.getInstance().loadEntity(
            "com.basis100.deal.entity.Component");

        component.create(deal.getDealId(), deal.getCopyId(), 
                componentData.getComponentTypeId(),
                componentData.getMtgProdId());
        component.ejbStore();
        componentData.setComponentId(component.getComponentId());
        componentData.setCopyId(component.getCopyId());

        /** ****************componentLoan Data******************************** */
        entity = ( com.basis100.deal.entity.ComponentLOC) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.ComponentLOC");
        entity.create(component.getComponentId(), component.getCopyId());
        
        entity.setPaymentFrequencyId(entityData.getPaymentFrequencyId());
        entity.setPrePaymentOptionsId(entityData.getPrePaymentOptionsId());
        entity.setPrivilegePaymentId(entityData.getPrivilegePaymentId());
        entity.setPropertyTaxAllocateFlag(entityData.getPropertyTaxAllocateFlag());
        entity.setExistingAccountIndicator(entityData.getExistingAccountIndicator());
        entity.setExistingAccountNumber(entityData.getExistingAccountNumber());
//        entity.setFirstPaymentDate(entityData.getFirstPaymentDate());
        entity.setPropertyTaxEscrowAmount(entityData.getPropertyTaxEscrowAmount());
        entity.setAdvanceHold(entityData.getAdvanceHold());
        entity.setCommissionCode(entityData.getCommissionCode());
        entity.setDiscount(entityData.getDiscount());
//        entity.setInterestAdjustmentDate(entityData.getInterestAdjustmentDate());
        entity.setLocAmount(entityData.getLocAmount());
        entity.setMiAllocateFlag(entityData.getMiAllocateFlag());
        entity.setNetInterestRate(entityData.getNetInterestRate());
        entity.setPAndIPaymentAmount(entityData.getPAndIPaymentAmount());
        entity.setPremium(entityData.getPremium());
        entity.setPAndIPaymentAmountMonthly(entityData.getPAndIPaymentAmountMonthly());
        entity.setTotalLocAmount(entityData.getTotalLocAmount());
        entity.setTotalPaymentAmount(entityData.getTotalPaymentAmount());
        
        entity.ejbStore();

    }

    /**
     * <p>testEntity</p>
     * <p>Description: test method of this entity</p>
     * 
     * @throws Exception
     * 
     * @version 1.0 MCM Team July 15, 2008 (XS 2.39)
     */
    public void testEntity() throws Exception {
        try {
            setupTestData();
            whereClause="ComponentId = " + entity.getComponentId() + " and copyid = " + entity.getCopyId();
            brEntity = new ComponentLOC(interpretor, whereClause);
            assertEquals(entity.getComponentId(), brEntity.componentId);            
            assertEquals(entity.getCopyId(), brEntity.copyId);            
            assertEquals(entity.getExistingAccountIndicator(), brEntity.existingAccountIndicator);
            assertEquals(entity.getExistingAccountNumber(), brEntity.existingAccountNumber);
//            assertEquals(entity.getFirstPaymentDate(), brEntity.firstpaymentDate);
            assertEquals(entity.getPropertyTaxAllocateFlag(), brEntity.propertyTaxAllocateFlag);
            assertEquals(entity.getAdvanceHold(), brEntity.advanceHold);
            assertEquals(entity.getCommissionCode(), brEntity.commissionCode);
            assertEquals(entity.getDiscount(), brEntity.discount);
//            assertEquals(entity.getInterestAdjustmentDate(), brEntity.interestAdjustmentDate);
            assertEquals(entity.getLocAmount(), brEntity.locAmount);
            assertEquals(entity.getMiAllocateFlag(), brEntity.miAllocateFlag);
            assertEquals(entity.getPAndIPaymentAmount(), brEntity.pAndIPaymentAmount);
            assertEquals(entity.getPAndIPaymentAmountMonthly(), brEntity.pAndIPaymentAmountMonthly);
            assertEquals(entity.getPaymentFrequencyId(), brEntity.paymentFrequencyId);
            assertEquals(entity.getPremium(), brEntity.premium);
            assertEquals(entity.getPrePaymentOptionsId(), brEntity.prepaymentOptionsId);
            assertEquals(entity.getNetInterestRate(), brEntity.netInterestRate);
            assertEquals(entity.getPrivilegePaymentId(), brEntity.privilegePaymentId);
            assertEquals(entity.getPropertyTaxEscrowAmount(), brEntity.propertyTaxEscrowAmount);
            assertEquals(entity.getTotalLocAmount(), brEntity.totalLocAmount);
            assertEquals(entity.getTotalPaymentAmount(), brEntity.totalPaymentAmount);
            
        } catch (Exception e) {
            throw e;
        } finally  {
            tearDownTestData();
        }
    }


    /**
     * <p>Description: This method is for removing the data that get stored. </p>
     *
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     */
    public void tearDownTestData () throws Exception {

        if (deal != null) {

            deal.dcm = null;
            // this remove componentLoan as well
            component.ejbRemove(false);
        }

        try {
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();

        } catch (RemoteException e) {
            e.printStackTrace();
        }

        System.out.println("ejb remove done");
    }

    /**
     * <p>Description: This method invokes the createSuite() of DDStepsSuiteFactory class. </p>
     *
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     */
    public static Test suite () {
        return (Test) DDStepsSuiteFactory.createSuite(ComponentLoanTest.class);
    }

    /**
     * <p>getComponentData</p>
     * <p>Description: getter </p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     */
    public com.basis100.deal.entity.Component getComponentData () {
        return componentData;
    }

    /**
     * <p>setComponentData</p>
     * <p>Description: setter for Component</p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     */
    public void setComponentData (com.basis100.deal.entity.Component componentData) {
        this.componentData = componentData;
    }

    /**
     * <p>getEntityData</p>
     * <p>Description: getter for ComponentMortgage</p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     */
    public com.basis100.deal.entity.ComponentLOC getEntityData() {
        return entityData;
    }

    /**
     * <p>setEntityData</p>
     * <p>Description: setter for ComponentMortgage</p>
     * @version 1.0  15 July, 2008 (initial version) XS 3.39 
     * @auther MCM Team 
     */
    public void setEntityData(com.basis100.deal.entity.ComponentLOC entityData) {
        this.entityData = entityData;
    }
}
