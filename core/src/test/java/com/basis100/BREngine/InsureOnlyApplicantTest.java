package com.basis100.BREngine;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.docprep.extract.ExtractException;
import com.basis100.deal.duplicate.DupCheckActionException;
import com.basis100.deal.entity.DealEntity;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.ltx.unittest.util.EntityTestUtil;

public class InsureOnlyApplicantTest extends FXDBTestCase{

	private IDataSet dataSetTest;
	private InsureOnlyApplicant insureOnlyApplicant;
	Object obj =null; 
	private BRInterpretor interpretor;
	private DealEntity entity;
	
	public InsureOnlyApplicantTest(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(InsureOnlyApplicant.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getResource(InsureOnlyApplicant.class.getSimpleName() + "DataSet.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.INSERT;	
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		interpretor = new BRInterpretor();
		try
        {
            interpretor.setConnection((EntityTestUtil.getSessionResourceKit().getJdbcExecutor()).getCon());
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }		
		 insureOnlyApplicant = new InsureOnlyApplicant(interpretor,true);
		
		
	}
	
	
	public void testFetchField() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, SQLException, CloneNotSupportedException{
		obj=new Object();
		ITable testFetchField = dataSetTest.getTable("testFetchField");
		String copy=(String)testFetchField.getValue(0,"COPYID");
	      int copyId=Integer.parseInt(copy);      
	      String insureOnlyApp=(String)testFetchField.getValue(0,"INSUREONLYAPPLICANTID");
	      int insureOnlyApplicantId=Integer.parseInt(insureOnlyApp);
		 insureOnlyApplicant.copyId=copyId;
	     insureOnlyApplicant.insureOnlyApplicantId =insureOnlyApplicantId;  
	     obj=insureOnlyApplicant.fetchField("LifeDisPremiumsIOnlyA");	
	     assertNotNull(obj);
} 
	
	public void testUpdate() throws DataSetException, DupCheckActionException, RemoteException, FinderException, ExtractException, ParseException, SQLException, JdbcTransactionException{
	
	ITable testUpdate = dataSetTest.getTable("testUpdate");	
	srk.beginTransaction();
     String deal=(String)testUpdate.getValue(0,"DEALID");
      int dealId=Integer.parseInt(deal);      
      String insureOnlyApp=(String)testUpdate.getValue(0,"INSUREONLYAPPLICANTID");
      int insureOnlyApplicantId=Integer.parseInt(insureOnlyApp);
     String insuApplicantFirstName="INSUREONLYAPPLICANTFIRSTNAME='Hari'";	
     insureOnlyApplicant.dealId =dealId; 
     insureOnlyApplicant.insureOnlyApplicantId =insureOnlyApplicantId;     
	 insureOnlyApplicant.update(insuApplicantFirstName);
	 assertNotNull(insureOnlyApplicant);	
	 srk.rollbackTransaction();
}
	
}
