package com.basis100;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.Date;

import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.CachedDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.filter.DefaultColumnFilter;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlProducer;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.xml.sax.InputSource;

import com.basis100.deal.calc.CalcEntityCache;
import com.basis100.deal.calc.DealCalc;
import com.basis100.resources.SessionResourceKit;
import com.basis100.resources.PropertiesCache;
import com.ltx.unittest.util.EntityTestUtil;

public abstract class FXDBTestCase extends DBTestCase {
	
	protected final static String PROFILE_ID = "EntityClassUnitTest";
	protected final static int institutionId = 0;
	
	protected SessionResourceKit srk = new SessionResourceKit("PROFILE_ID");

	public FXDBTestCase(String name){ 
		super(name);
		
		PropertiesCache cache = PropertiesCache.getInstance();
		System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, cache.getInstanceProperty("connection.pool.jdbcDriver"));
        System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, cache.getInstanceProperty("connection.pool.jdbcDriverURL"));
        System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, cache.getInstanceProperty("connection.pool.dbLogin"));
        System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, cache.getInstanceProperty("connection.pool.dbPassword"));
        System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_SCHEMA, cache.getInstanceProperty("connection.pool.dbLogin"));
	}

	/**
	 * Overriding the default PROPERTY_DATATYPE_FACTORY to use OracleDataTypeFactory.
	 * This overriding method was implemented to resolve the Oracle CLOB issue.
	 */

	@Override
	protected void setUpDatabaseConfig(DatabaseConfig config) {
		super.setUpDatabaseConfig(config);
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new OracleDataTypeFactory());
	}
	
	/**
	 * Gets the ITable representation of tableName from database according to the whereClause.
	 * @param tableName the target table name
	 * @param whereClause where clause of the SQL statement. The parameter should not contain 'where' at the beginning.
	 * @return the ITable representation.
	 * @throws SQLException
	 * @throws DataSetException
	 * @throws Exception
	 */
	protected ITable getITableFromDB(String tableName, String whereClause) throws SQLException, DataSetException, Exception{
		String sql = "select * from " + tableName  + " where " + whereClause;
		return getConnection().createQueryTable(tableName, sql);
	}
	
	/**
	 * Gets the ITable representation of tableName from database according to the whereClause.
	 * The result is filtered according to the filterTable. Columns in filterTable should be a subset of that of tableName.
	 * Only columns that exists in both tables will be returned in the resulting ITable.
	 * @param tableName the target table name
	 * @param whereClause where clause of the SQL statement. The parameter should not contain 'where' at the beginning.
	 * @param filterTable
	 * @return the ITable representation.
	 * @throws SQLException
	 * @throws DataSetException
	 * @throws Exception
	 */
	protected ITable getFilteredITableFromDB(String tableName, String whereClause, ITable filterTable) throws SQLException, DataSetException, Exception{
		String sql = "select * from " + tableName  + " where " + whereClause;
		ITable unfiltered = getConnection().createQueryTable(tableName, sql);
		return DefaultColumnFilter.includedColumnsTable(unfiltered, filterTable.getTableMetaData().getColumns());
		
	}
	
	/**
	 * Gets the ITable representation of tableName in xmlFileName.
	 * @param xmlFileName
	 * @param tableName
	 * @return
	 * @throws DataSetException
	 * @throws IOException
	 */
	protected ITable getITableFromXml(String xmlFileName, String tableName) throws DataSetException, IOException{
		IDataSet dataSet = new FlatXmlDataSet(this.getClass().getResourceAsStream("/com/basis100/deal/entity/" + xmlFileName));
        ITable table = dataSet.getTable(tableName);
        return table;
	}
	
	/**
	 * Gets String value from the row, column in ITable it.
	 * @param it the table to get value from.
	 * @param row the row where the targeted value is at.
	 * @param column the column where the targeted value is at.
	 * @return
	 * @throws DataSetException
	 */
	protected String getStrValue(ITable it, int row, String column) throws DataSetException{
		return (String) DataType.VARCHAR.typeCast(it.getValue(row, column));
	}
	
	/**
	 * Gets int value from the row, column in ITable it.
	 * @param it the table to get value from.
	 * @param row the row where the targeted value is at.
	 * @param column the column where the targeted value is at.
	 * @return
	 * @throws DataSetException
	 */
	protected int getIntValue(ITable it, int row, String column) throws DataSetException{
		return (Integer) DataType.INTEGER.typeCast(it.getValue(row, column));
	}
	
	/**
	 * Gets Date value from the row, column in ITable it.
	 * @param it
	 * @param row
	 * @param column
	 * @return
	 * @throws DataSetException
	 */
	protected Date getDateValue(ITable it, int row, String column) throws DataSetException {
		return (Date) DataType.TIMESTAMP.typeCast(it.getValue(row, column));
	}
	
	 // This method newly added for reducing the number of dataset.xml 's.
	protected IDataSet getDataSet() throws Exception {
		FlatXmlProducer xmlProducer = new FlatXmlProducer(new InputSource("CoreDataSet.xml"));
		IDataSet dataSet = new CachedDataSet(xmlProducer);
		return dataSet;
		}
	public void setEntityCacheAndSession(DealCalc dealCalc){
		Class classCalc = dealCalc.getClass();
		Class superClassofCalc = classCalc.getSuperclass();
		Field ff[] = superClassofCalc.getDeclaredFields();
		for ( Field field : ff )
		{
			field.setAccessible(true);
			if (field.getName().equalsIgnoreCase("entityCache"))
			{
				Class fieldClasstest = field.getType();
				System.out.println(fieldClasstest.getName());
				Constructor con[] = fieldClasstest.getDeclaredConstructors();
				for ( Constructor conss : con )
				{
					conss.setAccessible(true);
					System.out.println(conss.getModifiers() + " " + conss.getName());

					conss.setAccessible(true);
					CalcEntityCache cache;
					try
					{
						cache = (CalcEntityCache) conss.newInstance(EntityTestUtil
								.getCalcMonitor());
						field.set(dealCalc, cache);
					}
					catch (Exception e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
				break;
			}
		}

		dealCalc.setResourceKit(EntityTestUtil.getSessionResourceKit());
	}	
	public void setResourceKit(SessionResourceKit srkin)
	  {
	    srk = srkin;
	  }
	
	public void tearDown() throws Exception {
        super.tearDown();
        srk.freeResources();
         Thread.sleep(250);

         }
        

	 
}
