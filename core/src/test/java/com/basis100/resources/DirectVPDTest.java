/*
 * @(#)DirectVPDTest.java    2007-6-4
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.basis100.resources;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.ResultSet;
import java.util.Random;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.TestingEnvironmentInitializer;

/**
 * DirectVPDTest - 
 *
 * @version   1.0 2007-6-4
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class DirectVPDTest extends TestCase {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(DirectVPDTest.class);

    // The connection
    private Connection _connection;
    private String _vpdProcedure =
        "{CALL VPDADMIN.VPD_SECURITY_CONTEXT.set_app_context (?, ?, ?)}";

    /**
     * choosing the testcase you want to test.
     */
    public static Test suite() {

        // Run all test cases.
        TestSuite suite = new TestSuite(DirectVPDTest.class);
        // Run selected test cases.
        //TestSuite suite = new TestSuite();
        //suite.addTest(new DirectVPDTest("testMethodName"));

        return suite;
    }

    /**
     * Constructor function
     */
    public DirectVPDTest() {
    }

    /**
     * constructs a test case with the given test case name.
     */
    public DirectVPDTest(String name) {
        super(name);
    }

    /**
     * setting up the testing env.
     */
    protected void setUp() {

        try {
            TestingEnvironmentInitializer.initTestingEnv();
            _connection = new SessionResourceKit("VPDTest").getConnection();
        } catch (Exception e) {
            _log.error("...", e);
        }
    }

    /**
     * test the VPD on user institution id.
     */
    public void testVPD4User() {

        _log.info("Testing VPD based on user profile id and user institution id...");
        try {

            String sql = "SELECT dealid, copyid, " +
    			"institutionprofileid, amortizationterm " +
    			"FROM deal WHERE dealid = 570 ORDER BY dealid";
            _log.debug("Querying SQL: ");
            _log.debug("----" + sql);

            _log.info("Query Result without NO VPD");
            printResultSet(sql);

            setVPDContext(16, 315);
            printResultSet(sql);

            _log.debug("Clean VPD...");
            setVPDContext();
            printResultSet(sql);

        } catch (SQLException e) {
            _log.error(e);
        }
        _log.info("====================================");
    }

    /**
     * test the VPD on Deal Institution id.
     */
    public void testVPD4Deal() {

        _log.info("Testing VPD based on deal institution id...");
        try {

            String sql = "SELECT dealid, copyid, " +
    			"institutionprofileid, amortizationterm " +
    			"FROM deal WHERE dealid = 570 ORDER BY dealid";
            _log.debug("Querying SQL: ");
            _log.debug("----" + sql);

            _log.info("Query Result without NO VPD");
            printResultSet(sql);

            _log.debug("Setting VPD...");
            setVPDContext(100);
            printResultSet(sql);
        } catch (SQLException e) {
            _log.error(e);
        }
        _log.info("====================================");
    }


    /**
     * test the VPD on deal and user cases.
     */
    public void testVPD4UserDeal() {

        _log.info("Testing VPD based on Deal and User Id...");
        try {
            String sql = "SELECT dealid, copyid, " +
    			"institutionprofileid, amortizationterm " +
    			"FROM deal WHERE dealid = 570 ORDER BY dealid";
            _log.debug("Querying SQL: ");
            _log.debug("----" + sql);

            _log.info("Query Result without NO VPD");
            printResultSet(sql);

            _log.debug("Setting VPD with two param...");
            setVPDContext(16, 315);
            printResultSet(sql);

            _log.debug("Setting VPD with one param...");
            setVPDContext(315);
            printResultSet(sql);

            _log.debug("Clean VPD...");
            setVPDContext();
            printResultSet(sql);

        } catch (SQLException e) {
            _log.error(e);
        }
        _log.info("====================================");
    }


    /**
     * test the VPD on Global User Id.
     */
    public void testVPD4UserDealGolbal() {

        _log.info("Testing VPD based on Global User Id...");
        try {
            String sql = "SELECT dealid, copyid, " +
    			"institutionprofileid, amortizationterm " +
    			"FROM deal WHERE dealid = 570 ORDER BY dealid";
            _log.debug("Querying SQL: ");
            _log.debug("----" + sql);

            _log.info("Query Result without NO VPD");
            printResultSet(sql);

            _log.debug("Setting VPD with two param...");
            setVPDContext(130, 100);
            printResultSet(sql);

            _log.debug("Clean VPD...");
            setVPDContext();
            printResultSet(sql);

        } catch (SQLException e) {
            _log.error(e);
        }
        _log.info("====================================");
    }
    
    /**
     * test the VPD on userid with UPDATE.
     */
    public void testVPDUPDATE4User() {

        _log.info("Testing VPD UPDATE based on user profile id ...");
        try {
            Statement stmt = _connection.createStatement();
            Random rand = new Random();
            int i = rand.nextInt(999);
            String sqlSelect = "SELECT dealid, copyid, " +
            		"institutionprofileid, amortizationterm " +
                "FROM deal WHERE dealid = 570 ORDER BY dealid";
            _log.debug("Querying SQL: ");
            _log.debug("sqlselect: " + sqlSelect);

            _log.info("Query Result without NO VPD");
            printResultSet(sqlSelect);

            setVPDContext(16, 315);
            String sqlUpdate = "UPDATE deal set amortizationterm = " + i +
                " WHERE dealid = 570 ";
            _log.info("sqlupdate: " + sqlUpdate);

            stmt.executeUpdate(sqlUpdate);
            printResultSet(sqlSelect);
            
            _log.debug("Clean VPD Show all result...");
            setVPDContext();
            printResultSet(sqlSelect);
        }
        catch (SQLException e) {
            _log.error(e);
        }
        _log.info("====================================");
    }

    /**
     * test the VPD on dealid with UPDATE.
     */
    public void testVPDUPDATE4Deal() {

        _log.info("Testing VPD UPDATE based on deal profile id ...");
        try {
            Statement stmt = _connection.createStatement();
            Random rand = new Random();
            int i = rand.nextInt(999);
            String sqlSelect = "SELECT dealid, copyid, " +
            		"institutionprofileid, amortizationterm " +
                "FROM deal WHERE dealid = 570 ORDER BY dealid";
            _log.debug("Querying SQL: ");
            _log.debug("sqlselect: " + sqlSelect);

            _log.info("Query Result without NO VPD");
            printResultSet(sqlSelect);

            setVPDContext(100);
            String sqlUpdate = "UPDATE deal set amortizationterm = " + i +
                " WHERE dealid = 570 ";
            _log.info("sqlupdate: " + sqlUpdate);

            stmt.executeUpdate(sqlUpdate);
            printResultSet(sqlSelect);
            
            _log.debug("Clean VPD Show all result...");
            setVPDContext();
            printResultSet(sqlSelect);
        }
        catch (SQLException e) {
            _log.error(e);
        }
        _log.info("====================================");
    }
    
    /**
     * test the VPD on Global UserId with UPDATE.
     */
    public void testVPDUPDATE4GlobalUserId() {

        _log.info("Testing VPD UPDATE based on Global User ...");
        try {
            Statement stmt = _connection.createStatement();
            Random rand = new Random();
            int i = rand.nextInt(999);
            String sqlSelect = "SELECT dealid, copyid, " +
            		"institutionprofileid, amortizationterm " +
                "FROM deal WHERE dealid = 570 ORDER BY dealid";
            _log.debug("Querying SQL: ");
            _log.debug("sqlselect: " + sqlSelect);

            _log.info("Query Result without NO VPD");
            printResultSet(sqlSelect);

            setVPDContext(130, 100);
            String sqlUpdate = "UPDATE deal set amortizationterm = " + i +
                " WHERE dealid = 570 ";
            _log.info("sqlupdate: " + sqlUpdate);

            stmt.executeUpdate(sqlUpdate);
            printResultSet(sqlSelect);
            
            _log.debug("Clean VPD Show all result...");
            setVPDContext();
            printResultSet(sqlSelect);
        }
        catch (SQLException e) {
            _log.error(e);
        }
        _log.info("====================================");
    }
    
    
    /**
     * test the VPD on user and institution id with UPDATE.
     */
    public void testVPDUPDATE4UserDeal() {

        _log.info("Testing VPD UPDATE based on user profile id and user institution id...");
        try {
            Statement stmt = _connection.createStatement();
            Random rand = new Random();
            int i = rand.nextInt(999);
            String sqlSelect = "SELECT dealid, copyid, " +
            		"institutionprofileid, amortizationterm " +
                "FROM deal WHERE dealid = 570 ORDER BY dealid";
            _log.debug("Querying SQL: ");
            _log.debug("----" + sqlSelect);

            _log.info("Query Result without NO VPD");
            printResultSet(sqlSelect);

            setVPDContext(16, 315);
            String sqlUpdate = "UPDATE deal set amortizationterm = " + i +
                " WHERE dealid = 570 ";
            _log.info("sql: " + sqlUpdate);

            stmt.executeUpdate(sqlUpdate);
            printResultSet(sqlSelect);

            _log.debug("Clean VPD Show the result updated...");
            setVPDContext();
            printResultSet(sqlSelect);

            _log.info("Change context to deal:");
            setVPDContext(100);
            sqlUpdate = "UPDATE deal set amortizationterm = " + i +
                " WHERE dealid = 570 ";
            _log.info("sql: " + sqlUpdate);
            stmt.executeUpdate(sqlUpdate);
            printResultSet(sqlSelect);
            
            _log.debug("Clean VPD Show the result updated...");
            setVPDContext();
            printResultSet(sqlSelect);
            
        } catch (SQLException e) {
            _log.error(e);
        }
        _log.info("====================================");
    }

    /**
     * set VPD context
     */
    private void setVPDContext(int userProfileId, int userInstitutionId) {

        _vpdProcedure =  "{CALL VPDADMIN.VPD_SECURITY_CONTEXT.set_app_context (?, ?)}";
        _log.info("Preparing VPD Store Precedure: " + _vpdProcedure);
        _log.info("             User Profile Id : " + userProfileId);
        _log.info("         User Institution Id : " + userInstitutionId);
        try {
            CallableStatement cstmt = _connection.prepareCall(_vpdProcedure);
            cstmt.setInt(1, userProfileId);
            cstmt.setInt(2, userInstitutionId);
            _log.info("Executing the VPD store precedure ...");
            cstmt.execute();
            _log.info("Successfully execute VPD store precudure.");
            cstmt.close();
        } catch (SQLException e) {
            _log.error(e.getMessage(), e);
        }
    }

    /**
     * set VPD context based on deal institution id.
     */
    private void setVPDContext(int dealInstitutionId) {
        
        _vpdProcedure =  "{CALL VPDADMIN.VPD_SECURITY_CONTEXT.set_app_context (?)}";
        _log.info("Preparing VPD Store Precedure: " + _vpdProcedure);
        _log.info("         Deal Institution Id : " + dealInstitutionId);
        try {
            CallableStatement cstmt = _connection.prepareCall(_vpdProcedure);
            cstmt.setInt(1, dealInstitutionId);
            _log.info("Executing the VPD store precedure ...");
            cstmt.execute();
            _log.info("Successfully execute VPD store precudure.");
            cstmt.close();
        } catch (SQLException e) {
            _log.error(e.getMessage(), e);
        }
    }

    /**
     * clean VPD store procedure.
     */
    private void setVPDContext() {
        
        _vpdProcedure =  "{CALL VPDADMIN.VPD_SECURITY_CONTEXT.set_app_context ()}";
        _log.info("Preparing VPD Store Precedure: " + _vpdProcedure);
        try {
            CallableStatement cstmt = _connection.prepareCall(_vpdProcedure);
            _log.info("Executing the VPD store precedure ...");
            cstmt.execute();
            _log.info("Successfully execute VPD store precudure.");
            cstmt.close();
        } catch (SQLException e) {
            _log.error(e.getMessage(), e);
        }
    }

    /**
     * print out the query result set.
     */
    private void printResultSet(String sql) throws SQLException {

        Statement stmt;
        stmt = _connection.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        _log.info("The Query Result (dealid, copyid, " +
        		"institutionprofileid, amortizationterm): ");
        while (rs.next()) {
            StringBuffer oneRecord = new StringBuffer();
            oneRecord.append("        (").
                append(rs.getString("dealid")).
                append(", ").
                append(rs.getString("copyid")).
                append(", ").
                append(rs.getString("institutionprofileid")).
                append(", ").
                append(rs.getString("AMORTIZATIONTERM")).
                append(")");
            _log.info(oneRecord.toString());
        }
        rs.close();
        stmt.close();
    }

    /**
     * clean the testing env.
     */
    protected void tearDown() {

        try {
            _connection.close();
        } catch (SQLException e) {
            _log.error("...", e);
        }
    }
}
