package com.basis100.resources;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.rowset.CachedRowSet;

//import oracle.jdbc.rowset.OracleFilteredRowSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * @author BZhang 
 * Jun. 21, 2007
 *
 * @ATTN: OracleFilteredRowSet related code commented out to avoid compile
 *        error when working with Oracle9 JDBC driver.
 *        OracleFilteredRowSet is a new class from Oracle10 JDBC driver.
 *        
 *        VpdSpTest WON'T work without OracleFilteredRowSet.
 */
public class JdbcUtils {
	protected static Log log = LogFactory.getLog(JdbcUtils.class);
	
	/**
	 * Return a disconnected rowset instead of a ResultSet, so that it can be 
	 * passed to other component.
	 * Use Oracle's implementation of RowSet (OracleFilteredRowSet) because Sun's 
	 * impl will throw a SQLException: Invalid scale size when working with Oracle. 
	 * 
	 * @param conn
	 * @param sql
	 * @return
	 */
	public static CachedRowSet query(Connection conn, String sql){
    	log.debug(sql);
    	CachedRowSet crs = null;
		try {
//			crs = new OracleFilteredRowSet();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
           
            if(rs != null)
                crs.populate(rs);
           
            rs.close();
            stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	return crs;
    }
	
	/**
	 * Put the content of a ResultSet to a String.
	 * @param rs
	 * @return
	 */
    public static String resultSetToString(ResultSet rs){
    	String str = "\n";
    	try {
    		ResultSetMetaData rsmd = (ResultSetMetaData)rs.getMetaData();
			int count = rsmd.getColumnCount();
			
			for(int i=1; i<count+1; i++){
				str = str + rsmd.getColumnName(i) + "\t";
			}
			str += "\n";
			
			while(rs.next()){
				for(int i=1; i<count + 1; i++){
					str = str + rs.getString(i) + "\t";
				}
				str += "\n";
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		log.debug(str);
		
		return str;
    }
}
