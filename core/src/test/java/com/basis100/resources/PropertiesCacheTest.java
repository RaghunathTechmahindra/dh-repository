/*
 * @(#)PropertiesCacheTest.java Oct 2, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */

package com.basis100.resources;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * PropertiesCacheTest
 * 
 * @version 1.0 Oct 2, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class PropertiesCacheTest extends ExpressEntityTestCase implements
        UnitTestLogging {
    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(PropertiesCacheTest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * Test the get instance of the singleton
     */
    @Test
    public final void testGetInstance() throws Exception {
        assertNotNull(PropertiesCache.getInstance());
    }

    /**
     * test get property
     */
    @Test
    public final void testGetPropertyIntString() throws Exception {
        // get the data from the repository
        int institutionProfileId = _dataRepository.getInt("SysProperty",
                "institutionprofileid", 0);
        String propertyName = _dataRepository.getString("SysProperty", "Name",
                0);
        String propertyValue = _dataRepository.getString("SysProperty",
                "Value", 0);
        _logger.info(BORDER_START, "testGetPropertyIntString");

        String actual = PropertiesCache.getInstance().getProperty(
                institutionProfileId, propertyName);

       assertEquals(propertyValue, actual);
        _logger.info(BORDER_END, "testGetPropertyIntString");
    }

    /**
     * test get property
     */
    @Test
    public final void testGetPropertyIntStringString() throws Exception {
        _logger.info(BORDER_START, "testGetPropertyIntStringString");
        // get the data from the repository
        int institutionProfileId = _dataRepository.getInt("SysProperty",
                "institutionprofileid", 0);

        String actual = PropertiesCache.getInstance().getProperty(
                institutionProfileId, "no-such-property", "default-value!!");

        assertEquals("default-value!!", actual);

        _logger.info(BORDER_END, "testGetPropertyIntStringString");
    }

    /**
     * test get unchanged property
     */
    @Ignore
    public final void testGetUnchangedProperty() throws Exception {
        _logger.info(BORDER_START, "testGetUnchangedProperty");
        // get the data from the repository
        int institutionProfileId = _dataRepository.getInt("SysProperty",
                "institutionprofileid", 0);

        String expectUnchange = "C:/Projects/fxp-3.2-GEN/src/mosAppWar/WEB-INF/classes/com/basis100/deal/calc/impl";
        String expectChange = "C:\\Projects\\fxp-3.2-GEN\\src\\mosAppWar\\WEB-INF\\classes\\com\\basis100\\deal\\calc\\impl";

        String actual = PropertiesCache.getInstance()
                .getProperty(institutionProfileId, "com.basis100.calc.impl",
                        "default-value");
        assertEquals(expectChange, actual);

        actual = PropertiesCache.getInstance()
                .getUnchangedProperty(institutionProfileId,
                        "com.basis100.calc.impl", "default-value");
        assertEquals(expectUnchange, actual);
        _logger.info(BORDER_END, "testGetUnchangedProperty");
    }
    
    /**
     * test get property
     */
    @Test
    public final void testFlushCache() throws Exception {
        // get the data from the repository
        int institutionProfileId = _dataRepository.getInt("SysProperty",
                "institutionprofileid", 0);
        String propertyName = _dataRepository.getString("SysProperty", "Name",
                0);
        String propertyValue = _dataRepository.getString("SysProperty",
                "Value", 0);
        _logger.info(BORDER_START, "testFlushCache");

        //Remove all entries
        PropertiesCache.getInstance().flush();
        _logger.info("-------------------CACHE CLEAR----------------------");
        
        //try to fetch an entry to trigger a reload
        String actual = PropertiesCache.getInstance().getProperty(
                institutionProfileId, propertyName);

       assertEquals(propertyValue, actual);

        _logger.info(BORDER_END, "testFlushCache");
    }

}
