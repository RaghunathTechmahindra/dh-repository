/*
 * @(#)PicklistDataTest.java   Oct 23, 2007
 *
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */


package com.basis100.picklist;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.resources.ResourceManager;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * PicklistDataTest
 *
 * @version 1.0 Oct 23, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>	
 */
public class PicklistDataTest  extends ExpressEntityTestCase implements UnitTestLogging  {
    // The logger
    private static Logger _log = LoggerFactory.getLogger(PicklistDataTest.class);

    /**
     * Setup the testing environment
     */
    @Before
    public void setUp() {
        ResourceManager.init();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
    }
        
    /**
     * 
     * @throws Exception
     */
    @Test
    public void testInit() throws Exception{
        _log.info(BORDER_START, "testInit");
        
        PicklistData.init(true);
        
        _log.info(BORDER_END, "testInit");
    }

    /**
     * Test getPicklistIdValue
     * @throws Exception
     */
    @Test
    public void testGetPicklistIdValue() throws Exception{
        int accessTypeId = _dataRepository.getInt("CrossSellProfile", "CrossSellProfileID", 0);
        String accessTypeDesc = _dataRepository.getString("CrossSellProfile", "CSDESCRIPTION", 0);
        
        _log.info(BORDER_START, "testGetPicklistIdValue");
        
        int returnValue = PicklistData.getPicklistIdValue(0,"CrossSellProfile",accessTypeDesc);
        
        assertEquals(accessTypeId, returnValue);
        
        _log.info(BORDER_END, "testGetPicklistIdValue");
    }
    
    /**
     * Test getDescription
     * @throws Exception
     */
    @Test
    public void testGetDescription() throws Exception{
        int accessTypeId = _dataRepository.getInt("AccessType", "ACCESSTYPEID", 3);
        String accessTypeDesc = _dataRepository.getString("AccessType", "ATDESCRIPTION", 3);

        _log.info(BORDER_START, "testGetDescription");
        
        String returnValue = PicklistData.getDescription("AccessType",accessTypeId);
        
        assertEquals(accessTypeDesc, returnValue);
        
        _log.info(BORDER_END, "testGetDescription");
    }
    
    /**
     * Test getDescription
     * @throws Exception
     */
    @Test
    public void testGetDescription2() throws Exception{
        int accessTypeId = _dataRepository.getInt("AccessType", "ACCESSTYPEID", 3);
        String accessTypeDesc = _dataRepository.getString("AccessType", "ATDESCRIPTION", 3);

        _log.info(BORDER_START, "testGetDescription2");
        
        String returnValue = PicklistData.getDescription("AccessType", "99", true);
        
        assertEquals("", returnValue);
        
        _log.info(BORDER_END, "testGetDescription2");
    }
    
    /**
     * Test getColumnValuesCDV
     * @throws Exception
     */
    @Test
    public void testGetColumnValuesCDV() throws Exception{
        int accessTypeId = _dataRepository.getInt("AccessType", "ACCESSTYPEID", 3);
        String accessTypeDesc = _dataRepository.getString("AccessType", "ATDESCRIPTION", 3);

        _log.info(BORDER_START, "testGetColumnValuesCDV");
        
        String returnValue = PicklistData.getColumnValuesCDV("AccessType","ACCESSTYPEID");
        
        _log.info("Column data " + returnValue);
        assertEquals("0, 1, 2, 3, 4, 5", returnValue);
        
        _log.info(BORDER_END, "testGetColumnValuesCDV");
    }
    
    /**
     * testGetMatchingColumnValue
     * @throws Exception
     */
    @Test
    public void testGetMatchingColumnValue() throws Exception
    {
        int accessTypeId = _dataRepository.getInt("CrossSellProfile", "CrossSellProfileID", 0);
        String accessTypeDesc = _dataRepository.getString("CrossSellProfile", "CSDESCRIPTION", 0);
        _log.info(BORDER_START, "testGetMatchingColumnValue");
        
        String returnValue = PicklistData.getMatchingColumnValue(0, "CrossSellProfile", accessTypeId, "CSDESCRIPTION");
        
        assertEquals(accessTypeDesc, returnValue);
        
        _log.info(BORDER_END, "testGetMatchingColumnValue");
        
    }
    
}

