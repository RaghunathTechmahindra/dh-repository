/*
 * @(#)BXResourcesTest.java    2005-5-10
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.basis100.picklist;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.resources.ResourceManager;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

import MosSystem.Mc;

/**
 * BXResourcesTest is the unit test class for class {@link BXResources}.
 *
 * @version   1.0 2005-5-10
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class BXResourcesTest extends ExpressEntityTestCase implements UnitTestLogging  {

    // The logger
    private static Logger _log = LoggerFactory.getLogger(BXResourcesTest.class);
    private int institutionProfileId;


    /**
     * Constructor function
     */
    public BXResourcesTest() {
    }

    @Before
    public void setUp() throws Exception {
        ResourceManager.init();
        institutionProfileId = _dataRepository.getInt("INSTITUTIONPROFILE",
                "INSTITUTIONPROFILEID", 0);
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
    }
    /**
     * test to get the the order list.
     */
    @Test
    public void testGetSysConfigOrderList() {

        // test case to get the routing order.
        _log.info("Trying to get ROUTINGORDER ...");
        List order = BXResources.getSysConfigOrderList(0, "ROUTINGORDER");
        _log.info("Got [" + order.size() + "] routing type");
        int count = 1;
        for (Iterator i = order.iterator(); i.hasNext(); ) {
            if (_log.isDebugEnabled())
                _log.debug("        Routing Order: " + count + " = "
                           + i.next());
            count ++;
        }

        // Test case to
    }

    /**
     * testcase to get the sys config value for the given key.
     */
    @Test
    public void testGetSysConfigValue() {

        // only the 
        _log.info("Testing get sysconfig value for MORTGAGEPRODUCTDEFAULT ...");
        String[] keys = {"0.5_0", "0.5_8", "0.50_5", "1_0", "1_7",
                         "2_7", "2_4", "3_3", "4_7"};
        for (int i = 0; i < keys.length; i ++) {
            String value = BXResources.getPickListDescription(0, "MORTGAGEPRODUCTDEFAULT",
                                                         keys[i], Mc.LANGUAGE_PREFERENCE_ENGLISH);
            _log.info("    " + keys[i] + " = " + value);
        }

        // only class mosApp/MosSystem/RateAdminHandler.java
        _log.info("Testing get SysConfig value for PRICINGPROFILE ...");
        
    }

    /**
     * testcase for testing the getPickListDescription.
     */
    @Test
    public void testGetPickListDescription2() throws Exception {
        _log.info(BORDER_START, "testGetPickListDescription2");
        _log.info("Testing get get picklist description for ACCESSTYPE ...");
        String[] keys = {"0", "1", "2", "3", "4","5"};
        
        for (int i = 0; i < keys.length; i ++) {
            //Call the new signature with institutionProfileId
            String value =
                BXResources.getPickListDescription(institutionProfileId,"ACCESSTYPE",
                                                   keys[i],
                                                   Mc.LANGUAGE_PREFERENCE_ENGLISH);
            _log.info("    " + keys[i] + " = " + value);
        }
        _log.info(BORDER_END, "testGetPickListDescription2");
    }
    
    /**
     * testcase for testing the getPickListDescription.
     */
    @Test
    public void testGetPickListDescription3() throws Exception {
        _log.info(BORDER_START, "testGetPickListDescription3");
        _log.info("Testing get get picklist description for ACCESSTYPE ...");
        String[] keys = {"0", "1", "2", "3", "4","5"};
        
        for (int i = 0; i < keys.length; i ++) {
            //Call the new signature with institutionProfileId
            String value =
                BXResources.getPickListDescription(institutionProfileId,"ACCESSTYPE",
                                                   keys[i],
                                                   Mc.LANGUAGE_PREFERENCE_ENGLISH);
            _log.info("    " + keys[i] + " = " + value);
        }
        _log.info(BORDER_END, "testGetPickListDescription3");
    }
    
    /**
     * test getPickListDescriptionId
     */
    @Test
    public void testGetPickListDescriptionId() throws Exception
    {
        _log.info(BORDER_START, "testGetPickListDescriptionId");
        String description = "View Only";
        String expectedId = "2"; 
        int institutionProfileId=1;
        //Call the new signature with institutionProfileId
        String id =
            BXResources.getPickListDescriptionId(institutionProfileId,"ACCESSTYPE",
                    description,
                    Mc.LANGUAGE_PREFERENCE_ENGLISH);

        //Check if the correct data is returned
        assertEquals(expectedId, id);
        
        _log.info(BORDER_END, "testGetPickListDescriptionId");

    }
    
    /**
     * test getPickListValuesAndDesc
     */
    @Test
    public void testGetPickListValuesAndDesc() throws Exception{
        _log.info(BORDER_START, "testGetPickListValuesAndDesc");
        int expectedSize = 6;
        int institutionProfileId=1;
        //Call the new signature with institutionProfileId
        Collection<String[]> keyValue = BXResources.getPickListValuesAndDesc(institutionProfileId,"ACCESSTYPE",
                Mc.LANGUAGE_PREFERENCE_ENGLISH);
        
        //Check if the correct data is returned
        assertTrue(keyValue.size() == expectedSize);
        _log.info("Collection Size " + keyValue.size());
        _log.info(BORDER_END, "testGetPickListValuesAndDesc");
    }
    
    /**
     * test GetPickListValues
     */
    @Test
    public void testGetPickListValues() throws Exception{
        _log.info(BORDER_START, "testGetPickListValues");

        int expectedSize = 6;
         int institutionProfileId=1;
        //Call the new signature with institutionProfileId
        Collection<String> values = BXResources.getPickListValues(institutionProfileId,"ACCESSTYPE",
                Mc.LANGUAGE_PREFERENCE_ENGLISH);
        
        //Check if the correct data is returned
        assertTrue(values.size() == expectedSize);
        _log.info("Collection " + values);
        _log.info(BORDER_END, "testGetPickListValues");
    }
    
    /**
     * test getPickListDescriptions
     */
    @Test
    public void testGetPickListDescriptions() throws Exception{
        _log.info(BORDER_START, "testGetPickListDescriptions");

        int expectedSize = 6;
         int institutionProfileId=1;
        //Call the new signature with institutionProfileId
        Collection<String> descriptions = BXResources.getPickListDescriptions(institutionProfileId,"ACCESSTYPE",
                Mc.LANGUAGE_PREFERENCE_ENGLISH);
        
        //Check if the correct data is returned
        assertTrue(descriptions.size() == expectedSize);
        _log.info("Collection " + descriptions);
        _log.info(BORDER_END, "testGetPickListDescriptions");
    }
    
    /**
     * test getPickListTableSize
     */
    @Test
    public void testGetPickListTableSize() throws Exception{
        _log.info(BORDER_START, "testGetPickListTableSize");

        int expectedSize = 6;
         int institutionProfileId=1;
        //Call the new signature with institutionProfileId
        int size = BXResources.getPickListTableSize(institutionProfileId,"ACCESSTYPE",
                Mc.LANGUAGE_PREFERENCE_ENGLISH);
        
        //Check if the correct data is returned
        assertTrue(size == expectedSize);
        _log.info("Size " + size);
        _log.info(BORDER_END, "testGetPickListTableSize");
    }
    
    /**
     * test GetDefaultInstitutionId
     */
    @Test
    public void testGetDefaultInstitutionId() throws Exception{
        _log.info(BORDER_START, "testGetDefaultInstitutionId");
        
        //Call the new signature with institutionProfileId
        int id = BXResources.getDefaultInstitutionId();
        
        //Check if the correct data is returned
        _log.info("InstitutionProfileId " + id);
        assertNotNull(id);
        _log.info(BORDER_END, "testGetDefaultInstitutionId");
    }
    
    /**
     * 
     * @throws Exception
     */
    @Test
    public void testGetAllInstitutions() throws Exception{
        _log.info(BORDER_START, "testGetAllInstitutions");
        
        
        //Call the new signature with institutionProfileId
        HashMap<Integer, String> insts = BXResources.getAllInstitutions();
        _log.info(" Institutions: " + insts);
        //Check if the correct data is returned
        
        assertTrue(insts.size() > 0);
        _log.info(BORDER_END, "testGetAllInstitutions");
    }
}
