/*
 * @(#)UnitTestLogging.java    2007-8-29
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.test;

/**
 * UnitTestLogging define some useful logging templates for unit testing.
 *
 * @version   1.0 2007-8-29
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface UnitTestLogging {

    // the start border.
    public final static String BORDER_START =
        "========================= START TESTING [{}] =========================";

    // the end border.
    public final static String BORDER_END =
        "========================== END TESTING [{}] ==========================";

    // the short break.
    public final static String SHORT_BREAK =
        "====================================";

    // the long break.
    public final static String LONG_BREAK =
        "==================================================================";
}