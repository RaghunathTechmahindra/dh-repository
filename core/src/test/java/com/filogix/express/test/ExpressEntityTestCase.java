/*
 * @(#)ExpressEntityTestCase.java    2007-8-29
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.test;

import org.junit.BeforeClass;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filogix.express.core.ExpressCoreTestContext;

/**
 * ExpressEntityTestCase - 
 *
 * @version   1.0 2007-8-29
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
@Ignore
public class ExpressEntityTestCase extends Assert {

    // The logger
    private final static Logger _logger = 
        LoggerFactory.getLogger(ExpressEntityTestCase.class);

    // the unit test data repository.
    protected static UnitTestDataRepository _dataRepository;

    /**
     * Constructor function
     */
    public ExpressEntityTestCase() {
    }

    /**
     * setting up the testing env.
     */
    @BeforeClass
    public static void beforeClass() throws Exception {

        _dataRepository = ExpressCoreTestContext.getUnitTestDataRepository();
    }
}