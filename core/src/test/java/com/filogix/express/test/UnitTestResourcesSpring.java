/*
 * @(#)UnitTestResourcesSpring.java    2007-8-24
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.test;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.core.io.Resource;

/**
 * UnitTestResourcesSpring is the Spring implementation for {@link
 * UnitTestResources}.
 *
 * @version   1.0 2007-8-24
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public class UnitTestResourcesSpring implements UnitTestResources {

    // The logger
    private final static Logger _logger = 
        LoggerFactory.getLogger(UnitTestResourcesSpring.class);

    /**
     * Constructor function
     */
    public UnitTestResourcesSpring() {
    }

    /**
     * {@inhertDoc}
     */
    public File getTestDataXmlFile() throws IOException {

        return _testDataXmlFile;
    }

    // the test data resoruce.
    protected File _testDataXmlFile;

    /**
     * inject the resource for test data.
     */
    public void setTestDataXmlFile(File file) {

        _testDataXmlFile = file;
    }
}