package com.filogix.express.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.xml.sax.InputSource;

import com.filogix.express.core.ExpressCoreTestContext;
import com.filogix.express.test.UnitTestResources;

public class TestDataUtil {

    private static Log _log = LogFactory.getLog(TestDataUtil.class);

    protected static File _testDataFile;
    static {

        try {
            UnitTestResources resources =
                (UnitTestResources)
                ExpressCoreTestContext.getService("unitTestResources");
            _testDataFile =resources.getTestDataXmlFile();
        } catch (IOException e) {
            _log.error("Failed to init unit test resources!", e);
        }
    }

    /**
     * Parses XML file mentioned in xmlFileLocation and returns the value by the XPath.
     * @param XPath String XPath expression
     * @return
     */
    public static String getValue(String XPath) throws Exception
    {
        _log.info("=====================");
        _log.info("XPath " + XPath);
        String value = null;
        XPathFactory factory = XPathFactory.newInstance();
        XPath xPath = factory.newXPath();
        InputSource inputSource =
            new InputSource(new FileInputStream(_testDataFile));
        XPathExpression  xPathExpression= xPath.compile(XPath);
        value = xPathExpression.evaluate(inputSource);

        _log.info("Value " + value);
        _log.info("=====================");
        return value;
    }
    
    public static String getTestDataPath() throws IOException
    {
      return _testDataFile.getParent();
    }
}
