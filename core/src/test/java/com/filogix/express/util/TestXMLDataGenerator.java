/**
 * <p>Title: TestXMLDataGenerator.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2007</p>
 *
 * <p>Company: Filogix Limited Partnership
</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � Aug 27, 2007)
 *
 */
package com.filogix.express.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import com.basis100.TestingEnvironmentInitializer;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;
import com.basis100.xml.XmlToolKit;
import com.basis100.xml.XmlWriter;
import com.filogix.express.util.TestDataUtil;


/**
 * <p> TestXMLDataGenerator</p?
 * <p> this class generate entity Test data file as 
 * core/target/test-classes/TestData_<TABLENAME>.xml</p>
 * 
 * 
 * HOW TO USE IT?
 * 
 * 1. run TestXMLDataGenerator <tablename>
 * That create   core/target/test-classes/TestData_<TABLENAME>.xml
 * 
 * 2. add entity into 
 * 
 * core\target\test-classes\TestData.xml 
 * 
 * <!ENTITY <REFERECE_NAME> SYSTEM "target/test-classes/TestData_<TABLENAME>.xml">
 * 
 * And 
 *
 * <TestDat>
 *   <Entities>
 *   ,,,
 *     &<REFERECE_NAME>;
 *   </Entities>
 * <TestData>
 *
 */
@Ignore
public class TestXMLDataGenerator
{
  public static final String FIND_COLUMN_NAME = "SELECT COLUMN_NAME, DATA_TYPE FROM ALL_TAB_COLUMNS WHERE TABLE_NAME = '";
  
  public static final String SELECT_PK_COLUMN1 
    = "SELECT B.COLUMN_NAME FROM ALL_CONSTRAINTS A, ALL_CONS_COLUMNS B WHERE A.TABLE_NAME = '";
  public static final String SELECT_PK_COLUMN2
    = "' AND A.CONSTRAINT_TYPE = 'P' AND B.OWNER = A.OWNER AND B.CONSTRAINT_NAME = A.CONSTRAINT_NAME ORDER BY B.POSITION";
  
  private static Log _log = LogFactory.getLog(TestXMLDataGenerator.class);

  //private Document document;
  private Node schemaRoot;

  private SysLogger logger;

  private Element pkElement;
  private Element propertyElement;
  private Element documentElement;

  private JdbcExecutor _jExec;
  private String _tableName;
  private String[] _pkList;
  private String[] _columnInfo;
  private Map<String, String> _data;
  
  static int COLUMN_NAME_INDEX = 0;
  static int DATA_TYPE_INDEX = 1;
  static int DATA_VALUE_INDEX = 1;

  public TestXMLDataGenerator(JdbcExecutor jExec, String tableName)
  {
    this._jExec = jExec;
    this._tableName = tableName;
  }
  
  public void init() throws Exception
  {
    this._pkList = initPKColumns();
    this._columnInfo =  initColumnInfo();
  }
  
  public String[] initPKColumns() throws Exception
  {
    List<String> pkList = new ArrayList<String>();
    String sql = SELECT_PK_COLUMN1 + _tableName + SELECT_PK_COLUMN2;

    int key = _jExec.execute(sql);

    for (; _jExec.next(key); )
    {
        pkList.add(_jExec.getString(key, 1));
    }
    _jExec.closeData(key);
        
    return pkList.toArray(new String[pkList.size()]);
  }
  
  public String[] initColumnInfo() throws Exception
  {
    List<String> colNameTypeList = new ArrayList<String>();
    String sql = FIND_COLUMN_NAME + _tableName + "'";

    int key = _jExec.execute(sql);

    for (; _jExec.next(key); )
    {
      colNameTypeList.add(_jExec.getString(key, 1));
      colNameTypeList.add(_jExec.getString(key, 2));
    }
    _jExec.closeData(key);
    return colNameTypeList.toArray(new String[colNameTypeList.size()]);
  }

  public int countRecord() throws Exception
  {
    int count = 0;
    String sql ="SELECT COUNT(*) from " + _tableName;
    int key = _jExec.execute(sql);
    
    for (; _jExec.next(key); )
    {
      count = _jExec.getInt(key, 1);
    }
    _jExec.closeData(key);
    return count;

  }
  
  public Map<String, String>  getRandamRecord() throws Exception
  {
    int count = countRecord();
    Double indexD = new Double((Math.random() * count + 0.5));
    int index = indexD.intValue();
    int columnLength = _columnInfo.length;
    Map<String, String> columnData = new HashMap<String, String>();
    
    String sql = "SELECT * from ( SELECT ROWNUM R, " + _tableName + ".* FROM " +  _tableName + ") " 
      + "WHERE " + index + " < R AND R < " + (index + 2);
    
    int key = _jExec.execute(sql);
    
    for (; _jExec.next(key);)
    {
      for (int i=0; i<columnLength; i=i+2)
      {
        String data = "";
        if (_columnInfo[i+1].startsWith("NUMBER"))
        {
          data = data + _jExec.getInt(key, _columnInfo[i]);
        }
        else if (_columnInfo[i+1].startsWith("DATE")) 
        {
          data = data + _jExec.getDate(key, _columnInfo[i]);        
        }
        else if (_columnInfo[i+1].startsWith("BLOB")) 
        {
          data = data + _jExec.getBlob(key, _columnInfo[i]);        
        }
        else if (_columnInfo[i+1].startsWith("CLOB")) 
        {
          data = data + _jExec.getClob(key, _columnInfo[i]);        
        }
        else 
        {
          data = _jExec.getString(key, _columnInfo[i]);        
        }
        columnData.put(_columnInfo[i], data);
      }
    }
    _jExec.closeData(key);

    return columnData;  
  }
  

  private Document buildDoc(Document document, Map<String, String> dataMap)
  {
    documentElement = document.createElement(_tableName);
    document.appendChild(documentElement);
    
    pkElement = createPKTree(document, "PK");
    propertyElement = createPropertyTree(document, "DATA", dataMap);
    
    documentElement.appendChild(pkElement);
    documentElement.appendChild(propertyElement);
    
    return document;
  }
  
  private Element createPKTree(Document owner, String name)
  {
    Element pkTree = owner.createElement(name);
    int size = _pkList.length;
    for (int i=0; i<size; i++)
    {
      Element e = owner.createElement(_pkList[i]);
      Node nd = owner.createTextNode((String)_data.get(_pkList[i]));
      e.appendChild(nd);
      pkTree.appendChild(e);
    }
    return pkTree;
  }
  
  private Element createPropertyTree(Document owner, String name, Map dataMap)
  {
    Element propertyTree = owner.createElement(name);
    int size = _columnInfo.length;
    for (int i=0; i<size; i=i+2)
    {
      Element e = owner.createElement(_columnInfo[i]);
      String value = (String)_data.get(_columnInfo[i]);
      Node nd = owner.createTextNode(value);
      e.appendChild(nd);
      propertyTree.appendChild(e);
    }
    return propertyTree;
  }

  public String toXMLString(Document domc)
  {
    String out = null;

    //ALERT: ADDRESS ENCODING ISSUES.

    try
    {
      StringWriter wr = new StringWriter();
      XmlWriter writer = XmlToolKit.getInstance().getXmlWriter();
      writer.setEncoding("iso-8859-1");
//      writer.setEncoding("UTF-8");

      out = writer.printString(domc);
    }
    catch(Exception e)
    {
      return null;
    }

    return out;
  }

  public void save(String entityDoc, String tableName) throws IOException
  {
    String dir = TestDataUtil.getTestDataPath();
    File file = new File(dir + "/TestData_" + tableName + ".xml");
    if (file.exists())
    {
      file.delete();
    }
    file.createNewFile();
    FileWriter writer = new FileWriter(file);
    writer.write(entityDoc);
    writer.close();
  }
  
  public static void main(String[] args)
  {    
    try
    {
      TestingEnvironmentInitializer.initTestingEnv();
      SessionResourceKit srk = new SessionResourceKit("Testing");
      TestXMLDataGenerator xmlGenerator = new TestXMLDataGenerator(srk.getJdbcExecutor(), args[0]);
      xmlGenerator.init();
      xmlGenerator.setData(xmlGenerator.getRandamRecord());
      Document aDocument = XmlToolKit.getInstance().newDocument();
      aDocument = xmlGenerator.buildDoc(aDocument, xmlGenerator.getData());
      String out = xmlGenerator.toXMLString(aDocument);
      xmlGenerator.save(out, args[0]);
    }
    catch(Exception e)
    {
      _log.error(e);
      
    }    
  }

  public JdbcExecutor getJExec()
  {
    return _jExec;
  }

  public void setJExec(JdbcExecutor exec)
  {
    _jExec = exec;
  }

  public String getTableName()
  {
    return _tableName;
  }

  public void setTableName(String name)
  {
    _tableName = name;
  }

  public String[] getColumnInfo()
  {
    return _columnInfo;
  }

  public void setColumnInfo(String[] info)
  {
    _columnInfo = info;
  }

  public Map getData()
  {
    return _data;
  }

  public void setData(Map<String, String> data)
  {
    this._data = data;
  }

  public String[] getPkList()
  {
    return _pkList;
  }

  public void setPkList(String[] list)
  {
    _pkList = list;
  }
}
