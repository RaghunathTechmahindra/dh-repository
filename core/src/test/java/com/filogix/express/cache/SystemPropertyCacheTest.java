/*
 * @(#)SystemPropertyCacheTest.java Oct 1, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.cache;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.SysPropertyTest;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.core.ExpressCoreContext;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

/**
 * 
 * SystemPropertyCacheTest
 * 
 * @version 1.0 Oct 1, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class SystemPropertyCacheTest extends ExpressEntityTestCase implements
        UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory
                                                .getLogger(SysPropertyTest.class);

    // the session resource kit.
    private SessionResourceKit  _srk;

    /**
     * setting up the testing env.
     */
    @Before
    public void setUp() {
        ResourceManager.init();
        _srk = new SessionResourceKit();
    }

    /**
     * clean the testing env.
     */
    @After
    public void tearDown() {
        _srk.freeResources();
    }

    /**
     * test the get property method 
     * @throws Exception
     */
    //result depends on the value provided in properties file so its not known when the test case run to success
    /*@Test
    public void testGetProperty() throws Exception {
        // get input data from repository
        String propertyName = _dataRepository.getString("SysProperty", "Name",
                0);
        int institutionProfileId = _dataRepository.getInt("SysProperty",
                "institutionprofileid", 0);
        String propertyValue = _dataRepository.getString("SysProperty",
                "Value", 0);
        _logger.info(BORDER_START, "testGetProperty");

        IPropertiesCache sysProp = (IPropertiesCache) ExpressCoreContext
                .getService("systemPropertyCache");
        String result = sysProp.getProperty(institutionProfileId, propertyName);

        assertEquals(propertyValue, result);

        _logger.info(BORDER_END, "testGetProperty");

    }*/
    
    @Test
    public void testGetPropertyFromFile() throws Exception {
    	
    	String propertyName = "com.basis100.useradmin.passwordencrypted";
    	
    	String propertyValue = "N";
    	
    	IPropertiesCache sysProp = (IPropertiesCache) ExpressCoreContext
        						.getService("systemPropertyCache");
    	
    	String result = (String) sysProp.getProperty(propertyName);

    	assertEquals(propertyValue, result);

    	_logger.info(BORDER_END, "testGetProperty");
    	
    }

}
