/*
 * @(#)ExpressVPDStateTest.java    2007-8-13
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */

package com.filogix.express.state.vpd;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.datasource.DataSourceConnectionFactory;
import com.filogix.express.state.vpd.VPDStateDetectable;
import com.filogix.express.core.ExpressCoreTestContext;

/**
 * ExpressVPDStateTest -
 * 
 * @version 1.0 2007-8-13
 * @author <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ExpressVPDStateTest extends TestCase {

	// The logger
	private final static Log _log = LogFactory
			.getLog(ExpressVPDStateTest.class);

	// the state.
	private ExpressVPDState _state;

	/**
	 * choosing the testcase you want to test.
	 */
	public static Test suite() {

		// Run all test cases.
		TestSuite suite = new TestSuite(ExpressVPDStateTest.class);
		// Run selected test cases.
		// TestSuite suite = new TestSuite();
		// suite.addTest(new ExpressVPDStateTest("testMethodName"));

		return suite;
	}

	/**
	 * Constructor function
	 */
	public ExpressVPDStateTest() {
	}

	/**
	 * constructs a test case with the given test case name.
	 */
	public ExpressVPDStateTest(String name) {
		super(name);
	}

	/**
	 * setting up the testing env.
	 */
	protected void setUp() {

		_state = new ExpressVPDState();
	}

	/**
	 * test setVPDStatus.
	 */
	public void testSetVPDStatus() throws Exception {

		DataSourceConnectionFactory factory = (DataSourceConnectionFactory) ExpressCoreTestContext
				.getService(ExpressCoreTestContext.DATASOURCE_CONNECTION_FACTORY_KEY);
		Connection connection = factory
				.getConnection((VPDStateDetectable) _state);
		assertTrue(null != connection);
		_log.info("Goe connection: " + connection.toString());

		Statement statement = connection.createStatement();
		assertTrue(null != statement);
		String query = "SELECT count(*) from USERPROFILE";

		_log.info("Executing Query: " + query);
		ResultSet result = statement.executeQuery(query);
		assertTrue(null != result);
		assertTrue(result.next());
		int count = result.getInt(1);
		assertTrue(count > 0);
		_log.info("Amount of user: " + count);

		String institutionQuery = "SELECT INSTITUTIONPROFILEID from INSTITUTIONPROFILE";
		result = statement.executeQuery(institutionQuery);
		result.next();
		int intitutionId = result.getInt(1);
		_state.setVPDStatus(intitutionId);
		result = statement.executeQuery(query);

		assertTrue(null != result);
		assertTrue(result.next());
		int count1 = result.getInt(1);
		assertTrue(count1 > 0);
		assertTrue(count1 == count);
		_log.info("Amount of user: " + count1);

		_state.setVPDStatus(intitutionId);
		result = statement.executeQuery(query);
		assertTrue(null != result);
		assertTrue(result.next());
		count1 = result.getInt(1);
		assertTrue(count1 > 0);
		assertTrue(count1 == count);
		_log.info("Amount of user: " + count1);

		result.close();
		statement.close();
		connection.close();
	}

	/**
	 * clean the testing env.
	 */
	protected void tearDown() {
	}
}