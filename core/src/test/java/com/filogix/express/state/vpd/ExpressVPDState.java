/*
 * @(#)ExpressVPDState.java    2007-8-13
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.state.vpd;

import java.io.Serializable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.core.ExpressCoreContext;

/**
 * ExpressVPDState is a simple {@link VPDStateDetectable} implementation for
 * test.
 *
 * @version   1.0 2007-8-13
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public class ExpressVPDState implements VPDStateDetectable, Serializable {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(ExpressVPDState.class);

    // the VPD Status.
    private ExpressVPDStatus _theStatus = new ExpressVPDStatus();

    // VPD State chagne mediator.
    private VPDStateChangeMediator _mediator;

    /**
     * Constructor function
     */
    public ExpressVPDState() {

        _mediator = (VPDStateChangeMediator) ExpressCoreContext.
            getService(ExpressCoreContext.VPD_STATE_MEDIATOR_KEY);
    }

    /**
     * {@inheritDoc}
     */
    public void attach(VPDStateAware awareness) {

        _mediator.register(this, awareness);
    }

    /**
     * {@inheritDoc}
     */
    public void detach(VPDStateAware awareness) {

        _mediator.unregister(this, awareness);
    }

    /**
     * notify the VPD status change.
     */
    public void notifyChange() {

        _mediator.notifyChange(this);
    }

    /**
     * returns the current VPD Status.
     */
    public VPDStatus getVPDStatus() {

        return _theStatus.getCurrentStatus();
    }

    /**
     * set VPD Status.
     */
    public void setVPDStatus(int... ids) {

        _theStatus.setStatus(ids);
        notifyChange();
    }
}