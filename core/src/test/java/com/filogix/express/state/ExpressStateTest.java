/*
 * @(#)ExpressStateTest.java    2007-8-23
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.state;

import org.junit.Test;
import org.junit.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.resources.SessionResourceKit;
import com.basis100.resources.ResourceManager;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.filogix.express.test.ExpressEntityTestCase;

/**
 * ExpressStateTest a simple test for express state.  
 *
 * @version   1.0 2007-8-23
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public class ExpressStateTest extends ExpressEntityTestCase {

    // The logger
    private final static Logger _logger = 
        LoggerFactory.getLogger(ExpressStateTest.class);

    // the border.
    public final static String BORDER_START =
        "========================= Start Testing [{}] =========================";

    /**
     * Constructor function
     */
    public ExpressStateTest() {
    }

    /**
     * test query.
     */
    @Test
    public void testQuery() throws Exception {

        _logger.info(BORDER_START, "testQuery()");

        // init resources manager.
        ResourceManager.init();
        // using jdbc executor do the transaction.
        SessionResourceKit srk = new SessionResourceKit();
        assertTrue(null != srk);
        JdbcExecutor jExec = srk.getJdbcExecutor();
        assertTrue(null != jExec);

        String sql = "SELECT count(*) from USERPROFILE";
        _logger.info("Executing Query [{}] ...", sql);

        int key = jExec.execute(sql);
        assertTrue(jExec.next(key));
        int count = jExec.getInt(key, 1);
        assertTrue(count > 0);
        _logger.info("Got ({}) users without seting VPD.", count);

       // int dealInstitutionId = _dataRepository.getInt("INSTITUTIONPROFILE", "INSTITUTIONPROFILEID", 0);
        int dealInstitutionId = 1;//
        srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        key = jExec.execute(sql);
        assertTrue(jExec.next(key));
        int count1 = jExec.getInt(key, 1);
        assertTrue(count1 > 0);
        _logger.info("Got ({}) users with VPD set to ({}).",
                     count1, dealInstitutionId);

        srk.freeResources();
    }
}