/*
 * @(#)ExpressCoreTestContext.java    2007-7-30
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.core;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.context.ExpressContext;
import com.filogix.express.context.ExpressServiceException;
import com.filogix.express.context.ExpressContextDirector;

import com.filogix.express.test.UnitTestDataRepository;

/**
 * ExpressCoreTestContext is a Express context for testing core module.  The
 * corresponded context xml file is
 * <code>com/filogix/express/core/expressCoreTestContext.xml</code>, which is
 * located in test folder.
 *
 * @version   1.0 2007-7-30
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public abstract class ExpressCoreTestContext implements ExpressContext {

    // the key for the test context.
    private static final String EXPRESS_CORE_TEST_CONTEXT_KEY =
        "com.filogix.express.coretest";

    // The logger
    private final static Log _log = 
        LogFactory.getLog(ExpressCoreTestContext.class);

    /**
     * return a service instance from the test context.
     */
    synchronized public static Object getService(String name)
        throws ExpressServiceException {

        return ExpressContextDirector.getDirector().
            getServiceFactory(EXPRESS_CORE_TEST_CONTEXT_KEY).
            getService(name);
    }

    /**
     * utility method to get the singlton instance of {@link
     * UnitTestDataRepository}.
     */
    public static UnitTestDataRepository getUnitTestDataRepository()
        throws ExpressServiceException {

        return (UnitTestDataRepository) ExpressContextDirector.getDirector().
            getServiceFactory(EXPRESS_CORE_TEST_CONTEXT_KEY).
            getService("unitTestDataRepository");
    }
}