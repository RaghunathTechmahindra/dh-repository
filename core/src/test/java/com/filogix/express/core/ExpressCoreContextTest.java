/*
 * @(#)ExpressCoreContextTest.java    2007-7-30
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.core;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * ExpressCoreContextTest - 
 *
 * @version   1.0 2007-7-30
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ExpressCoreContextTest extends TestCase {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(ExpressCoreContextTest.class);

    /**
     * choosing the testcase you want to test.
     */
    public static Test suite() {

        // Run all test cases.
        TestSuite suite = new TestSuite(ExpressCoreContextTest.class);
        // Run selected test cases.
        //TestSuite suite = new TestSuite();
        //suite.addTest(new ExpressCoreContextTest("testMethodName"));

        return suite;
    }

    /**
     * test get datasource from the express context.
     */
    public void testGetDataSource() throws Exception {

        DataSource dataSource =
            (DataSource) ExpressCoreContext.
            getService(ExpressCoreContext.EXPRESS_DATASOURCE_KEY);
        assertTrue(null != dataSource);
        Connection connection = dataSource.getConnection();
        assertTrue(null != connection);
        Statement statement = connection.createStatement();
        assertTrue(null != statement);
        ResultSet result = statement.executeQuery("Select * from USERPROFILE");
        assertTrue(null != result);
        assertTrue(result.next());

        result.close();
        statement.close();
        connection.close();
    }

    /**
     * Constructor function
     */
    public ExpressCoreContextTest() {
    }

    /**
     * constructs a test case with the given test case name.
     */
    public ExpressCoreContextTest(String name) {
        super(name);
    }

    /**
     * setting up the testing env.
     */
    protected void setUp() {
    }

    /**
     * clean the testing env.
     */
    protected void tearDown() {
    }
}