/*
 * @(#)DataSourceConnectionFactoryTest.java    2007-8-9
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.datasource;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.core.ExpressCoreTestContext;

/**
 * DataSourceConnectionFactoryTest is the test case for {@link
 * DataSourceConnectionFactory}.
 *
 * @version   1.0 2007-8-9
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class DataSourceConnectionFactoryTest extends TestCase {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(DataSourceConnectionFactoryTest.class);

    /**
     * choosing the testcase you want to test.
     */
    public static Test suite() {

        // Run all test cases.
        TestSuite suite = new TestSuite(DataSourceConnectionFactoryTest.class);
        // Run selected test cases.
        //TestSuite suite = new TestSuite();
        //suite.addTest(new DataSourceConnectionFactoryTest("testMethodName"));

        return suite;
    }

    /**
     * Constructor function
     */
    public DataSourceConnectionFactoryTest() {
    }

    /**
     * constructs a test case with the given test case name.
     */
    public DataSourceConnectionFactoryTest(String name) {
        super(name);
    }

    /**
     * trying to get a connection from the datasource connection factory and
     * execute a easy query to verify the connectio.
     */
/*    public void testGetConnection() throws Exception {

        DataSourceConnectionFactory factory =
            (DataSourceConnectionFactory) ExpressCoreTestContext.
            getService(ExpressCoreTestContext.
                       DATASOURCE_CONNECTION_FACTORY_KEY);
        assertTrue(null != factory);
        _log.info("Got factory: " + factory.toString());
        Connection connection = factory.getConnection();
        assertTrue(null != connection);
        _log.info("Got datasource connection: " + connection.toString());
        Statement statement = connection.createStatement();
        assertTrue(null != statement);
        String query = "SELECT count(*) from USERPROFILE";
        _log.info("Executing Query: " + query);
        ResultSet result = statement.executeQuery(query);
        assertTrue(null != result);
        assertTrue(result.next());
        int count = result.getInt(1);
        assertTrue(count > 0);
        _log.info("Amount of user: " + count);

        result.close();
        statement.close();
        connection.close();
    }*/

    /**
     * setting up the testing env.
     */
    protected void setUp() {
    }

    public void testTBD(){
    }
    
    
    /**
     * clean the testing env.
     */
    protected void tearDown() {
    }
}