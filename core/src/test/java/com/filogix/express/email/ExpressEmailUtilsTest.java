package com.filogix.express.email;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import org.junit.Test;

public class ExpressEmailUtilsTest {

	@Test
	public void testSplitEmailAddresses() {
		String testStr = "ryu@filogix.com; ken@filogix.com,zangief@filogix.com edmond.honda@filogix.com";
		String[] actual = ExpressEmailUtils.splitEmailAddresses(testStr);

		assertEquals(4, actual.length);
		assertEquals("ryu@filogix.com", actual[0]);
		assertEquals("ken@filogix.com", actual[1]);
		assertEquals("zangief@filogix.com", actual[2]);
		assertEquals("edmond.honda@filogix.com", actual[3]);

	}

	@Test
	public void testCloneEmailInfo() {
		EmailInfo info = new EmailInfo();
		info.setMailAddressesTo("zangief@filogix.com");
		info.setMailAddressFrom("edmond.honda@filogix.com");
		info.setMailSubject("hello!");
		info.setMailMessage("I just wanted to say hello.");

		EmailInfo actual = ExpressEmailUtils.cloneEmailInfo(info);

		assertNotSame(info, actual);

		assertEquals(info.getMailAddressesTo(), actual.getMailAddressesTo());
		assertEquals(info.getMailAddressFrom(), actual.getMailAddressFrom());
		assertEquals(info.getMailMessage(), actual.getMailMessage());
		assertEquals(info.getMailSubject(), actual.getMailSubject());

	}

}
