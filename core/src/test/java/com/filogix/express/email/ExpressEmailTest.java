package com.filogix.express.email;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.velocity.app.VelocityEngine;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;

public class ExpressEmailTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	private ExpressEmail mail;

	@Before
	public void setUp() throws Exception {
		mail = new ExpressEmail();
		mail.setHtml(true);
		mail.setMessageTemplateFile("com/filogix/express/email/test-message.vm");
		mail.setSubjectTemplateFile("com/filogix/express/email/test-subject.vm");
		VelocityEngineFactoryBean factry = new VelocityEngineFactoryBean();
		
		Properties prop = new Properties();
		prop.put("resource.loader", "class");
		prop.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		factry.setVelocityProperties(prop);
		factry.afterPropertiesSet();
		mail.setVelocityEngine((VelocityEngine) factry.getObject());
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPrepareSubject() {
		Map<String, Object> messageMap = new HashMap<String, Object>();
		messageMap.put("param", "ok");
		
		String expected = "this is test subject. ok <- this one has to be replaced. ";
		String actual = mail.prepareSubject(null, messageMap);
		
		assertEquals(expected, actual);
		
	}

	@Test
	public void testPrepareMessage() {
		
		Map<String, Object> messageMap = new HashMap<String, Object>();
		messageMap.put("param", "ok");
		
		String expected = "this is test message. ok <- this one has to be replaced.";
		String actual = mail.prepareMessage(null, messageMap);
		
		assertEquals(expected, actual);
	}

}
