package com.filogix.externallinks.services;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.DcFolder;
import com.basis100.deal.entity.Deal;

public class ExchangeFolderCreatorTestDB extends FXDBTestCase {
	private IDataSet dataSetTest;
	private ExchangeFolderCreator folderCreator;

	public ExchangeFolderCreatorTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass()
				.getResource(
						ExchangeFolderCreator.class.getSimpleName()
								+ "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		folderCreator = new ExchangeFolderCreator(srk);

	}

	public void testFolderExists() throws Exception {
		boolean folderExist = true;
		ITable testFolderExists = dataSetTest.getTable("testFolderExists");
		int dealId = Integer.parseInt((String) testFolderExists.getValue(0,
				"dealId"));
		int copyId = Integer.parseInt((String) testFolderExists.getValue(0,
				"copyId"));
		int dcFolderId = Integer.parseInt((String) testFolderExists.getValue(0,
				"DCFOLDERID"));
		srk.getExpressState().setDealInstitutionId(1);
		Deal deal = new Deal(srk, null, dealId, copyId);
		DcFolder dcf = new DcFolder(srk, dcFolderId);
		folderExist = folderCreator.folderExists(deal);
		assertEquals(dcFolderId, dcf.getDcFolderId());
		assertTrue(folderExist);
	}

	
	
}
