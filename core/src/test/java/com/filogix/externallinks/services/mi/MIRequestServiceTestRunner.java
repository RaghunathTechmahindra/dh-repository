package com.filogix.externallinks.services.mi;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.core.ExpressCoreContext;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.channel.Axis2BaseChannel;

public class MIRequestServiceTestRunner {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		SessionResourceKit srk = new SessionResourceKit();

		try {

			ServiceInfo info = new ServiceInfo();

			srk.getExpressState().setDealInstitutionId(0);
			Deal deal = new Deal(srk, null);
			deal.findByRecommendedScenario(new DealPK(900147, -1), true);

			//TEST CMHC
			deal.setMortgageInsurerId(Mc.MI_INSURER_CMHC);
			Axis2BaseChannel channel = (Axis2BaseChannel)ExpressCoreContext.getService("miChannelCIBC");

			//TEST GE
			//deal.setMortgageInsurerId(Mc.MI_INSURER_GE);
			//Axis2BaseChannel channel = (Axis2BaseChannel)ExpressCoreContext.getService("miChannelDefault");

			deal.ejbStore();

			info.setSrk(srk);
			info.setDeal(deal);

			MIRequestServiceFactory factory = MIRequestServiceFactory.getInstance();
			MIRequestService mirequest = factory.getMIRequestService(deal.getMortgageInsurerId());

			channel.setInitDone(true); //so that Express doesn't change channel config in MIRequestServics class


			channel.setEndpoint("http://localhost:8088/marketPlaceMI");
			channel.setUsername("test");
			channel.setPassword("test");


			mirequest.setChannel(channel);
			mirequest.service(info);


		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			srk.freeResources();
		}
	}

}
