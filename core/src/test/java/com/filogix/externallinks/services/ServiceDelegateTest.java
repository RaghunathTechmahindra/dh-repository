package com.filogix.externallinks.services;

import junit.framework.TestCase;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.TestingEnvironmentInitializer;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.ClosingRequestTest;
import org.junit.Test;

public class ServiceDelegateTest extends TestCase {

	// The logger
    private static Log _log = LogFactory.getLog(ClosingRequestTest.class);

    // the session resource kit
    private SessionResourceKit _srk;
    
    private final int MI_INSURER_ID_AIGUG = 3;
	private final int MI_INSURER_ID_PMI = 4;

    /**
     * Constructor function
     */
    public ServiceDelegateTest() {
    }
    
    /**
     * constructs a test case with the given test case name.
     */
    public ServiceDelegateTest(String name) {
        super(name);
    }

    /**
     * setting up the testing env.
     */
    protected void setUp() {

        // initializing the test environment.
        TestingEnvironmentInitializer.initTestingEnv();

        _srk = new SessionResourceKit("WSTesting");
    }

    /**
     * clean the testing env.
     */
    protected void tearDown() {
    }
    
    /*
     * 
     */
    @Test
    public void testMIPremiumRequest() {
        try {
        	ServiceDelegate serviceDelegate = new ServiceDelegate();
        	_srk.getExpressState().setDealInstitutionId(1);
        	serviceDelegate.sendMIPremiumRequest(getServiceInfo(MI_INSURER_ID_PMI));
        	assert(true);
        } catch (Exception e) {
            _log.error("...", e);
        }
    }
    
    @Test
    public void testMIRequest() {
        try {
        	ServiceDelegate serviceDelegate = new ServiceDelegate();
        	_srk.getExpressState().setDealInstitutionId(1);
        	serviceDelegate.sendMIRequest(getServiceInfo(MI_INSURER_ID_PMI));
        	assert(true);
        } catch (Exception e) {
            _log.error("...", e);
        }
    }
    
    private ServiceInfo getServiceInfo(int productType) {
    	ServiceInfo si = new ServiceInfo();
    	si.setDealId(new Integer(900388));
		si.setCopyId(new Integer(32));
		si.setSrk(_srk);
		si.setLanguageId(_srk.getLanguageId());
		si.setProductType(productType);
		return si;
    }
}