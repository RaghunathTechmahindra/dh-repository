/*
 * @(#)FxLinkUtilsTest.java    2009-6-23
 *
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.externallinks.services.util;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

import com.basis100.deal.entity.Channel;
import com.basis100.entity.FinderException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.services.util.ChannelUtils;

public class ChannelUtilsTest {

	@Test
	public void testConcatEndpoint() throws Exception {

		//this srk doesn't connect to db
		SessionResourceKit srk_dummy = new SessionResourceKit();
		Channel ch = new Channel(srk_dummy);
		ch.setProtocol("http");
		ch.setIp("172.0.0.1");
		ch.setPort(9090);
		ch.setPath("/service/path");

		String expected = "http://172.0.0.1:9090/service/path";
		assertEquals(expected, ChannelUtils.concatEndpoint(ch));

		//when port is set to 0, don't need to add port
		ch.setPort(0);
		expected = "http://172.0.0.1/service/path";
		assertEquals(expected, ChannelUtils.concatEndpoint(ch));

		// no "/" in the top, but still works
		ch.setPath("service/path");
		assertEquals(expected, ChannelUtils.concatEndpoint(ch));

	}


	@Test
	public void testGetEndPointURL_normal() throws Exception {
		//this srk doesn't connect to db
		SessionResourceKit srk = new SessionResourceKit();
		try{
			srk.getExpressState().setDealInstitutionId(0);
			srk.beginTransaction();
			
			addTestData(srk);
			Channel ch = new Channel(srk);
			String expected = "http://172.0.0.1:9090/service/path";
			assertEquals(expected, ChannelUtils.getEndPointURL(srk, 99));
			
		}finally{
			srk.cleanTransaction();
			srk.freeResources();
		}
	}
	
	@Test(expected=FinderException.class)
	public void testGetEndPointURL_finderException() throws Exception {
		//this srk doesn't connect to db
		SessionResourceKit srk = new SessionResourceKit();
		try{
			srk.getExpressState().setDealInstitutionId(0);
			srk.beginTransaction();
			
			addTestData(srk);
			
			//expecting FinderException
			ChannelUtils.getEndPointURL(srk, 99999);
			
		}finally{
			srk.cleanTransaction();
			srk.freeResources();
		}
	}
	
	private void addTestData(SessionResourceKit srk) throws SQLException{
		
		Connection con = srk.getConnection();
		Statement stat = con.createStatement();
		
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE from CHANNEL where channelid = 99");
		stat.execute(sb.toString());
		
		sb = new StringBuilder();
		sb.append("INSERT INTO channel");
		sb.append(" (channelid, channeltypeid, protocol, ip, port, userid,");
		sb.append(" PASSWORD,PATH,wsserviceport, institutionprofileid)");
		sb.append(" VALUES (99, 1, 'http', '172.0.0.1', 9090, ");
		sb.append("'usid',");
		sb.append("'pswd',");
		sb.append("'/service/path',");
		sb.append("null, 0)");
		stat.execute(sb.toString());
	}

}
