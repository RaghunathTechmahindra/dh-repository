package com.filogix.externallinks.services.esb;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.Deal;

public class Exchange2ServiceTest extends FXDBTestCase{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(Exchange2ServiceTest.class);
    
    //Initial data to be loaded before each test case.
	private final static String INIT_XML_DATA = "Exchange2ServiceDataSet.xml";
	private IDataSet dataSetTest;
	private Exchange2Service exchange2Service;
	
	/**
     * Constructor function
     */
    public Exchange2ServiceTest(String name)  throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource("Exchange2ServiceDataSetTest.xml"));
    }
    
    @Override
	protected IDataSet getDataSet() throws Exception {
    	return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}
    
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
    	exchange2Service = new Exchange2Service();
    	return DatabaseOperation.INSERT;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.DELETE;
    }
    
    /**
     * test find by primary key.
     */
    @Test
    public void testDcFolderExists() throws Exception {

    	_logger.info(" Start testDcFolderExists");
    	 
    	 ITable testDcFolderExists = dataSetTest.getTable("testDcFolderExists");
    	int  dealId = Integer.parseInt((String)testDcFolderExists.getValue(0,"dealId"));
    	int  copyId = Integer.parseInt((String)testDcFolderExists.getValue(0,"copyId"));
    	int  dealId1 = Integer.parseInt((String)testDcFolderExists.getValue(0,"dealId1"));
    	int  copyId1 = Integer.parseInt((String)testDcFolderExists.getValue(0,"copyId1"));
  		
  		Deal deal=new Deal(srk,null,dealId, copyId);	    
  		
  		boolean folderExists = exchange2Service.dcFolderExists(deal);
        assert !folderExists;
        
        Deal deal1=new Deal(srk,null,dealId1, copyId1);	    
  		
  		boolean folderExists1 = exchange2Service.dcFolderExists(deal1);
        assert !folderExists1;
        
        _logger.info(" End testDcFolderExists");
    	
    }
    
}
