/*
 * @(#)AVMRequestTest.java    2006-4-3
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.externallinks;

import java.util.Collection;
import java.util.Iterator;
import java.util.Date;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.resources.SessionResourceKit;
import com.basis100.entity.RemoteException;
import com.basis100.entity.FinderException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.Channel;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.ServiceRequest;
import com.basis100.deal.entity.ServiceProduct;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.RequestPK;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.framework.ServiceExecutor;
import com.filogix.externallinks.framework.ServiceConst;

import com.basis100.TestingEnvironmentInitializer;

/**
 * AVMRequestTest - 
 *
 * @version   1.0 2006-4-3
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class AVMRequestTest extends TestCase {

    // The logger
    private static Log _log = LogFactory.getLog(AVMRequestTest.class);

    // the session resource kit
    private SessionResourceKit _srk;

    public static final int SERVICE_PROVIDER_CENTRAC = 1;
    public static final int SERVICE_PROVIDER_TERANET = 2;
    public static final int SERVICE_PROVIDER_REAVS = 3;
    public static final int SERVICE_PROVIDER_LANDCOR = 4;
    public static final int SERVICE_PROVIDER_MPAC = 5;

    /**
     * choosing the testcase you want to test.
     */
    public static Test suite() {

        // Run all test cases.
        TestSuite suite = new TestSuite(AVMRequestTest.class);
        // Run selected test cases.
        //TestSuite suite = new TestSuite();
        //suite.addTest(new AVMRequestTest("testMethodName"));

        return suite;
    }

    /**
     * Constructor function
     */
    public AVMRequestTest() {
    }

    /**
     * constructs a test case with the given test case name.
     */
    public AVMRequestTest(String name) {
        super(name);
    }

    /**
     * setting up the testing env.
     */
    protected void setUp() {

        // initializing the test environment.
        TestingEnvironmentInitializer.initTestingEnv();

        _srk = new SessionResourceKit("Testing");
    }

    /**
     * clean the testing env.
     */
    protected void tearDown() {
    }

    public void testExecute() {

        try {
        // DEV_31, AVM test CLIENT 
        Deal de = new Deal(_srk, null, 3081, 30);
        int reqId = createRequest(_srk, de, SERVICE_PROVIDER_REAVS);
        exectuteService(_srk, "AVM", reqId);
        } catch (Exception e) {
            _log.error("...", e);
        }

    }

   private static int createRequest(SessionResourceKit srk, Deal de, int providerId){
     // ----------- 
     // NOTE: these parameters will always have the same value for AVM
     int avmSrvcTypeId = ServiceConst.SERVICE_TYPE_VALUATION;
     int avmSrvcSubTypeId = ServiceConst.SERVICE_SUB_TYPE_AVM;
     int avmSrvcTransactionTypeId = ServiceConst.SERVICE_TRANSACTION_TYPE_REQUEST;
     int avmPayloadTypeId = ServiceConst.SERVICE_PAYLOAD_TYPE_SRVC_REQUEST;
     int avmInitialStatusId = ServiceConst.REQUEST_STATUS_BLANK;
     // ----------- 
     
     int result = 0;
     try {
 
       srk.beginTransaction();
       
      // 1) Request
      Request req = new Request(srk);
 
      req = req.create(new DealPK(de.getDealId(), de.getCopyId()),
                       avmInitialStatusId,
                       new Date(), srk.getUserProfileId());
 
      if (req == null) {
        throw new Exception("Cannot create Request!");
      }
      System.out.println("request id = " + req.getRequestId());
 
      // 1.1) find service productId for AVM by provider:
      ServiceProduct svProduct = new ServiceProduct(srk);
      svProduct = svProduct.findByProviderAndTypeAndSubType(providerId,
                                                            avmSrvcTypeId,
                                                            avmSrvcSubTypeId,
                                                            true);
 
      if (svProduct == null) {
        throw new Exception("Cannot find product!");
      }
 
      req.setServiceProductId(svProduct.getServiceProductId());
      req.setServiceTransactionTypeId(avmSrvcTransactionTypeId);
      req.setPayloadTypeId(avmPayloadTypeId);
      req.setRequestDate(new Date());
 
      // 1.2) set Channel
      Channel ch = new Channel(srk);
      ch = ch.findByServiceProductAndTransactionType(svProduct.getServiceProductId(),
                                                     avmSrvcTransactionTypeId);
 
      if (ch == null) {
        throw new Exception("Cannot find primary channel for productId = " + svProduct.getServiceProductId()
            + "; transaction type id = " + avmSrvcTransactionTypeId);
      }
      System.out.println("channel id = " + ch.getChannelId());
      
      req.setChannelId(ch.getChannelId());
 
      req.ejbStore();
 
      // 2) ServiceRequest
      ServiceRequest svReq = new ServiceRequest(srk);
      svReq = svReq.create(req.getRequestId(), 1);
      if (svReq == null) {
        throw new Exception("Cannot create Service Request!");
      }
 
      Collection propList = de.getProperties();
      Iterator it = propList.iterator();
 
      Property prop = null;
      boolean found = false;
 
      while (it.hasNext()) {
        prop = (Property) it.next();
        if (prop.getPrimaryPropertyFlag() == 'Y') {
          found = true;
          break;
        }
      }
      if (!found) {
        throw new Exception("Cannot find primary property on deal = " + de.getDealId());
      }
      
      svReq.setPropertyId(prop.getPropertyId());
      svReq.setCopyId(prop.getCopyId());
      svReq.setSpecialInstructions("Test for deal #" + de.getDealId() + "; requestId = " + req.getRequestId() + "; providerId = "
          + providerId);
      
      svReq.ejbStore();
      result = req.getRequestId();
      srk.commitTransaction();
      
    } catch (Exception e) {
      e.printStackTrace();
      srk.cleanTransaction();
    }
    return result;
    
  }

   private static void exectuteService(SessionResourceKit srk, String string, int requestId) {
     ServiceExecutor srve = new ServiceExecutor(srk);  
       
     int result = srve.processSyncRequest(requestId, 1); // "AVM", // invalid addressL 2, 3 // valid address: 4, 5 // Sean's request in 3.1 dev: 179 
 
     System.out.println("ResponseId = " + result);
  }

}
