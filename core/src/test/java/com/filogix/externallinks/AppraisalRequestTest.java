/*
 * @(#)AppraisalRequestTest.java    2006-4-3
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.externallinks;

import java.util.Collection;
import java.util.Iterator;
import java.util.Date;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.resources.SessionResourceKit;
import com.basis100.entity.RemoteException;
import com.basis100.entity.FinderException;
import com.basis100.entity.CreateException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.picklist.BXResources;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealHistory;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.ServiceRequest;
import com.basis100.deal.entity.ServiceProduct;
import com.basis100.deal.entity.Channel;
import com.basis100.deal.entity.ServiceRequestContact;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.pk.ServiceRequestContactPK;
import com.basis100.deal.pk.ServiceRequestPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.validation.BusinessRuleExecutor;

import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.framework.ServiceExecutor;
import com.filogix.externallinks.framework.ServiceConst;

import com.basis100.TestingEnvironmentInitializer;

/**
 * AppraisalRequestTest - 
 *
 * @version   1.0 2006-4-3
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class AppraisalRequestTest extends TestCase {

    // The logger
    private static Log _log = LogFactory.getLog(AppraisalRequestTest.class);

    // the session resource kit
    private SessionResourceKit _srk;

    public static final int SERVICE_PROVIDER_CENTRAC = 1;
    public static final int SERVICE_PROVIDER_TERANET = 2;
    public static final int SERVICE_PROVIDER_REAVS = 3;
    public static final int SERVICE_PROVIDER_LANDCOR = 4;
    public static final int SERVICE_PROVIDER_MPAC = 5;

    /**
     * choosing the testcase you want to test.
     */
    public static Test suite() {

        // Run all test cases.
        TestSuite suite = new TestSuite(AppraisalRequestTest.class);
        // Run selected test cases.
        //TestSuite suite = new TestSuite();
        //suite.addTest(new AppraisalRequestTest("testMethodName"));

        return suite;
    }

    /**
     * Constructor function
     */
    public AppraisalRequestTest() {
    }

    /**
     * constructs a test case with the given test case name.
     */
    public AppraisalRequestTest(String name) {
        super(name);
    }

    /**
     * setting up the testing env.
     */
    protected void setUp() {

        // initializing the test environment.
        TestingEnvironmentInitializer.initTestingEnv();

        _srk = new SessionResourceKit("Testing");
    }

    /**
     * clean the testing env.
     */
    protected void tearDown() {
    }

    public void testExecute() {

        try {
            // DEV_31, AVM test CLIENT
            //Deal de = new Deal(_srk, null, 3081, 30);
            createRequest(_srk);
        } catch (Exception e) {
            _log.error("...", e);
        }
    }



    public static final String USER = "DATX_APP";
    public static final int COPYID = 83;
    public static final int DEALID = 3081;
    
    //CHANNEL table, use channeltype 7[DatXClosingReq], 
    //then channelId=5
    public static final int CHANNELID_CLOSING_FP_REQ = 5;
    
    //PAYLOADTYPEID = 9[Closing Upload]
    public static final int PAYLOADTYPEID_CLOSING_REQ = 9;
    public static final int SERVICETRANSATIONTYPEID_REQUEST = 1;
    public static final int SERVICETRANSATIONTYPEID_UPDATE = 2;
    public static final int SERVICETRANSATIONTYPEID_CANCEL = 3;
    public static final int SERVICEPROVIDERID_CENTRACT = 1;
    
    public static final int SERVICEPRODUCTID_CLOSING_FIXED_PURCHASE = 1;

    private static void createRequest(SessionResourceKit srk) 
       throws Exception {

        Deal deal = new Deal(srk, null);
        DealPK dealPK = new DealPK(DEALID, COPYID); //3081, 83
        deal = deal.findByPrimaryKey(dealPK);

        UserProfile user = new UserProfile(srk);
        user = user.findByLoginId("DATX_APP");

        srk.beginTransaction();

        Request requestEntity = new Request(srk);
        requestEntity.create(dealPK,
                             ServiceConst.REQUEST_STATUS_BLANK,
                             new Date(), srk.getUserProfileId());
        requestEntity.setChannelId(3);  // appraisal channel id.
		// PAYLOADTYPEID_APP_REQ,
		// ServiceConst.SERVICE_PAYLOAD_TYPE_APP_REQUEST
        requestEntity.
			setPayloadTypeId(ServiceConst.SERVICE_PAYLOAD_TYPE_APP_REQUEST);
		// request type id.
        requestEntity.
			setRequestTypeId(ServiceConst.REQUEST_TYPE_APPRAISAL_REQUEST_INT);
		// SERVICEPRODUCTID_APP_FULL
        requestEntity.setServiceProductId(4);
		// SERVICETRANSATIONTYPEID_REQUEST
		// ServiceConst.SERVICE_TRANSACTION_TYPE_REQUEST
        requestEntity.setServiceTransactionTypeId(ServiceConst.SERVICE_TRANSACTION_TYPE_REQUEST); 
        requestEntity.setUserProfileId(user.getUserProfileId());
        requestEntity.ejbStore();
        
System.out.println("requestEntity.getRequestId() = " + requestEntity.getRequestId());        
        ServiceRequest serviceRequest = new ServiceRequest(srk);
        serviceRequest = serviceRequest.create(requestEntity.getRequestId(),
                                               requestEntity.getCopyId());
        Iterator properties = deal.getProperties().iterator();
        Property property = null;
        while(properties.hasNext())
        {
            property = (Property)properties.next();
            if (property.isPrimaryProperty()) break;
        }
        // update other info.
        serviceRequest.setPropertyId(property.getPropertyId());
        serviceRequest.setCopyId(property.getCopyId());
        serviceRequest.ejbStore();

        ServiceRequestContact contact = new ServiceRequestContact(srk);
        
        Iterator borrowers = deal.getBorrowers().iterator();
        Borrower borrower = null;
        while (borrowers.hasNext())
        {
            borrower = (Borrower)borrowers.next();
            if (borrower.isPrimaryBorrower()) break;
        }
        if (borrower != null)
        {
            contact = contact.createWithBorrowerAssoc(requestEntity.getRequestId(),
													  borrower.getBorrowerId(), COPYID);
        }
        contact.setFirstName(borrower.getBorrowerFirstName());
        contact.setLastName(borrower.getBorrowerLastName());
        contact.ejbStore();
        srk.commitTransaction();

        ServiceExecutor executor = new ServiceExecutor(srk);
        int result = executor.processSyncRequest(requestEntity.getRequestId(),
                                                 requestEntity.getCopyId());
        System.out.println("" + result);
    }

   private static void exectuteService(SessionResourceKit srk, String string, int requestId) {
     ServiceExecutor srve = new ServiceExecutor(srk);  

     int result = srve.processSyncRequest(requestId, 1); 
 
     System.out.println("ResponseId = " + result);
  }
}
