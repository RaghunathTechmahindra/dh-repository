package com.filogix.externallinks.framework;

import java.io.IOException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.Request;
import com.basis100.resources.SessionResourceKit;
import com.filogix.datx.messagebroker.valuation.AVMResponseBean;
import com.filogix.externallinks.services.datx.DatxAvmResponseProcessor;

public class ServiceResponseProcessorFactoryTestDB extends FXDBTestCase{
	private IDataSet dataSetTest;
	private ServiceResponseProcessorFactory factory;
	private AVMResponseBean _bean = new AVMResponseBean();
    SessionResourceKit srk=new SessionResourceKit();
	
	public ServiceResponseProcessorFactoryTestDB(String name) throws IOException, DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(ServiceResponseProcessorFactory.class.getSimpleName() + "DataSetTest.xml"));
	}
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;	
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();		
		factory=ServiceResponseProcessorFactory.getInstance();		
	}	
	
public void testServiceResponseProcessorFactory() throws Exception {	
	ServiceResponseProcessorBase processor = null;
	
		ITable processorFactory = dataSetTest.getTable("testServiceResponseProcessorFactory");		
		int requestId=Integer.parseInt((String)processorFactory.getValue(0,"requestId"));
		int copyId=Integer.parseInt((String)processorFactory.getValue(0,"copyId"));		
	    Request request=new Request(srk, requestId, copyId);
	    ServiceResponseBase responseBase=new ServiceResponseBase(srk,requestId,copyId);	  
	    _bean.setStatusCondition("test status condition");
	    _bean.setStatusCode("DatX123");
	    responseBase.setResponseBean(_bean);
	    processor=new DatxAvmResponseProcessor();
	    processor.init(srk, requestId);
	    processor.process(responseBase);
	    factory.createRequest(requestId);		
		assertEquals(requestId, request.getRequestId());		
    }
}
