package com.filogix.externallinks.condition.management;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.pk.ConditionPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.ESBOutboundQueuePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;
import com.filogix.externallinks.framework.ServiceConst;

public class ConditionUpdateRequesterTest extends ExpressEntityTestCase
        implements UnitTestLogging 
{
    private Logger _logger = LoggerFactory.getLogger(ConditionUpdateRequesterTest.class);
    
    private SessionResourceKit _srk;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        ResourceManager.init();
        _srk = new SessionResourceKit();        
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

        _srk.freeResources();
    }
    
    /**
     * Test method for {@link com.filogix.externallinks.condition.management.ConditionUpdateRequester#insertConditionUpdateRequest(int, boolean)
     */
    @Test
    public void testInsertConditionUpdateRequest() throws Exception {
        _logger.info("testInsertConditionUpdateRequest: START");
        
        int dealId = _dataRepository.getInt("Deal", "DealId", 0);
        int copyId = _dataRepository.getInt("Deal", "CopyId", 0);
        Deal deal = new Deal(_srk, null);
        deal = deal.findByRecommendedScenario(new DealPK(dealId, copyId), true);
        _logger.info(_srk.getActualVPDStateForDebug());
        _srk.getExpressState().setDealInstitutionId(deal.getInstitutionProfileId()) ;
        _logger.info(_srk.getActualVPDStateForDebug());
        
        Condition condition = new Condition (_srk);
        condition = condition.findByPrimaryKey(new ConditionPK(74));
        
        ConditionUpdateRequester requester = new ConditionUpdateRequester(_srk);
        int statusEventQueueId = requester.insertConditionUpdateRequest(dealId, true);
        
        if(statusEventQueueId > 0) {
            ESBOutboundQueue queue = new ESBOutboundQueue(_srk);
            queue = queue.findByPrimaryKey(new ESBOutboundQueuePK(statusEventQueueId));
            
            assertEquals(queue.getDealId(), dealId);
            assertEquals(queue.getInstitutionProfileId(), deal.getInstitutionProfileId());
            assertEquals(queue.getServiceTypeId(), ServiceConst.SERVICE_TYPE_CONDITION_STATUS_UPDATE);
            assertEquals(queue.getSystemTypeId(), deal.getSystemTypeId());
        }
        _logger.info("testInsertConditionUpdateRequest: insert StatusEventQueue successfully ID[" + statusEventQueueId + "]");
        
        _logger.info("testInsertConditionUpdateRequest: END");
        
    }
    
    
}
