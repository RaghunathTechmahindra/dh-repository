package com.filogix.express.outbound.loanDecision.payload;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.BranchProfile;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.pk.AddrPK;
import com.basis100.deal.pk.BranchProfilePK;
import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.UnitTestHelper;
import com.filogix.schema.fcx._1.Address2Type;
import com.filogix.schema.fcx._1.ContactType;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.PhoneType;
import com.filogix.schema.fcx._1.ResponseType;
import com.filogix.schema.fcx._1.ContactType.ContactName;

public class ResponseTypeGeneratorTest {



	@Test
	public void testPopulateResponseType() throws Exception  {

		ObjectFactory fac = new ObjectFactory();
		
		ResponseType rt = fac.createResponseType();

		SessionResourceKit srk = new SessionResourceKit();
		srk.beginTransaction();
		try{
			int institution = UnitTestHelper.getDefaultInstitution(srk);
			srk.getExpressState().setDealInstitutionId(institution);
			Deal deal = UnitTestHelper.createDefaultDealTree(srk);
			
			InstitutionProfile ip = new InstitutionProfile(srk);
			ip = ip.findByPrimaryKey(new InstitutionProfilePK(institution));
			
			ip.setECNILenderId("XXX");
			ip.ejbStore();			
			
			Date now = new Date();
			deal.setCommitmentIssueDate(now);
			deal.setServicingMortgageNumber("1 ");
			deal.ejbStore();
			
			ResponseTypeGenerator.populateResponseType(rt, deal, fac);

			assertEquals(Convert.toXMLGregorianCalendar(now), rt.getCommitmentIssueDate());
			assertEquals(String.valueOf(deal.getDealId()), rt.getLenderReferenceNumber());
			
			
			ip.setECNILenderId("FLT");
			ip.ejbStore();
			
			ResponseTypeGenerator.populateResponseType(rt, deal, fac);
			assertEquals("1", rt.getLenderReferenceNumber());
			
			
			LenderProfile lp = deal.getLenderProfile();
			lp.setLenderName("lender name 1");
			lp.setRegistrationName("reg name");
			lp.ejbStore();
			
			BranchProfile bp = deal.getBranchProfile();
			bp.setBranchName("branch name 1");
			bp.ejbStore();
			
			ResponseTypeGenerator.populateResponseType(rt, deal, fac);
			
			assertEquals("lender name 1", rt.getLenderProfile().getLenderName());
			assertEquals("reg name", rt.getLenderProfile().getRegistrationName());
			assertEquals("branch name 1", rt.getLenderProfile().getBranchProfile().getBranchName());
			
			
		}finally{
			srk.cleanTransaction();
			srk.freeResources();
		}
	}

	@Test
	public void testPopulateUnderwriterContact() throws Exception {

		ObjectFactory fac = new ObjectFactory();

		SessionResourceKit srk = new SessionResourceKit();

		Contact cont = new Contact(srk);
		cont.setLanguagePreferenceId(1);
		cont.setContactEmailAddress("underwriter1@filogix.com");

		ContactType fcx = fac.createContactType();
		ResponseTypeGenerator.populateContactType(fcx, cont, fac);

		assertEquals("underwriter1@filogix.com", fcx.getContactEmailAddress());
		assertEquals(1, fcx.getLanguagePreferenceDd().intValue());

	}


	@Test
	public void testPopulateContactName() throws Exception {
		ContactName fcx = new ObjectFactory().createContactTypeContactName();
		SessionResourceKit srk = new SessionResourceKit();
		Contact db = new Contact(srk);

		db.setSalutationId(1);
		db.setContactFirstName("ffff");
		db.setContactMiddleInitial("M");
		db.setContactLastName("lLlL");

		ResponseTypeGenerator.populateContactName(db, fcx);

		assertEquals(1, fcx.getSalutationDd().intValue());
		assertEquals("ffff", fcx.getContactFirstName());
		assertEquals("M", fcx.getContactMiddleInitial());
		assertEquals("lLlL", fcx.getContactLastName());
	}

	@Test
	public void testPopulatePhoneType() throws Exception  {
		SessionResourceKit srk = new SessionResourceKit();
		Contact db = new Contact(srk);
		PhoneType fcx = new ObjectFactory().createPhoneType();

		db.setContactPhoneNumber("1-234-567-8901");
		db.setContactPhoneNumberExtension("1234");

		ResponseTypeGenerator.populatePhoneType(db, fcx);

		assertEquals("1-234-567-8901", fcx.getPhoneNumber());
		assertEquals("1234", fcx.getPhoneExtension());
		//EXP26914, default phone type = 3
		assertEquals("3", fcx.getPhoneTypeDd().toString());
		

	}

	@Test
	public void testPopulateAddress2Type() throws Exception  {
		SessionResourceKit srk = new SessionResourceKit();

		Addr db = new Addr(srk);
		Address2Type fcx = new ObjectFactory().createAddress2Type();

		db.setAddressLine1("7 King st, E, Toronto");
		db.setAddressLine2("ON, Canada");
		db.setCity("London");
		db.setProvinceId(2);
		db.setPostalFSA("M5C");
		db.setPostalLDU("3C5");

		ResponseTypeGenerator.populateAddress2Type(db, fcx);

		assertEquals("7 King st, E, Toronto", fcx.getAddressLine1());
		assertEquals("ON, Canada", fcx.getAddressLine2());
		assertEquals("London", fcx.getCity());
		assertEquals(2, fcx.getProvinceDd().intValue());
		assertEquals("M5C", fcx.getPostalFsa());
		assertEquals("3C5", fcx.getPostalLdu());

		fcx = new ObjectFactory().createAddress2Type();
		db.setProvinceId(0);
		db.setPostalFSA("M5C");
		db.setPostalLDU("");
		
		ResponseTypeGenerator.populateAddress2Type(db, fcx);
		Assert.assertNull(fcx.getProvinceDd());
		Assert.assertNull(fcx.getPostalFsa());
		Assert.assertNull(fcx.getPostalLdu());
		
	}

}
