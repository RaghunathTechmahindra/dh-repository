package com.filogix.express.outbound.loanDecision.payload;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import MosSystem.Mc;

import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealNotes;
import com.basis100.deal.pk.DealPK;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.UnitTestHelper;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.ResponseType;

public class DeclinePayloadGeneratorTest {
	
	ObjectFactory factory;
	SessionResourceKit srk;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.factory = new ObjectFactory();
		this.srk = new SessionResourceKit();
		srk.getExpressState().cleanAllIds();
		srk.getExpressState().setDealInstitutionId(UnitTestHelper.getDefaultInstitution(srk));
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		this.srk.getExpressState().cleanAllIds();
		this.srk.freeResources();
	}

	@Test
	public void testPopulateResponseTypeSpecific() throws JdbcTransactionException {
		try{
			srk.beginTransaction();
			
			Deal deal = UnitTestHelper.createDefaultDealTree(srk);
			deal.setDenialReasonId(Mc.DENIAL_REASON_CREDIT_HISTORY);
			
			DealNotes dn0 = new DealNotes(srk);
			dn0.create((DealPK)deal.getPk(), deal.getDealId(), Mc.DEAL_NOTES_CATEGORY_GENERAL,"Note0");
			DealNotes dn1 = new DealNotes(srk);
			dn1.create((DealPK)deal.getPk(), deal.getDealId(), Mc.DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS,"Note1");
			DealNotes dn2 = new DealNotes(srk);
			dn2.create((DealPK)deal.getPk(), deal.getDealId(), Mc.DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS,"Note2"); // right one

			ResponseType fcx = factory.createResponseType();
			DeclinePayloadGenerator dpg = new DeclinePayloadGenerator();
			dpg.populateResponseTypeSpecific(fcx, deal, factory);
			
			String denial_reason_text = BXResources.getPickListDescription(deal.getInstitutionProfileId(), "DENIALREASON", deal.getDenialReasonId(), 0);
			
			assertEquals(denial_reason_text + " Note2", fcx.getDecline().getDenialReasons().get(0).getText());
		} catch (Exception e) {
			fail("Exception during unit test:" + e.getMessage());
		} finally {
			srk.rollbackTransaction();
		}
	}
	
	@Test
	public void testPopulateResponseTypeSpecific1() throws JdbcTransactionException {
		int[] denialReasonIds = {Mc.DENIAL_REASON_IC_REJECT};
		for(int reasonId: denialReasonIds) {
			ICNotesTest(reasonId);
		}
	}

	private void ICNotesTest(int denialReasonId) throws JdbcTransactionException {
		try{
			srk.beginTransaction();
			Deal deal = UnitTestHelper.createDefaultDealTree(srk);
			deal.setDenialReasonId(denialReasonId);
			Borrower borr = (Borrower)deal.getBorrowers().iterator().next();
			borr.setLanguagePreferenceId(0);
			borr.ejbStore();

			String txt = "IC-TestTest";
			DealNotes dn = new DealNotes(srk);
			dn.create((DealPK)deal.getPk(), deal.getDealId(), Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION,0,txt,0);
			
			//execute target
			borr.setLanguagePreferenceId(0);
			borr.ejbStore();
			
			ResponseType fcx = factory.createResponseType();
			DeclinePayloadGenerator dpg = new DeclinePayloadGenerator();
			dpg.populateResponseTypeSpecific(fcx, deal, factory);
			
			assertEquals(fcx.getDecline().getDenialReasons().get(0).getText(), txt + "\r\n");
		
		} catch (Exception e) {
			fail("Exception during unit test:" + e.getMessage());
		} finally {
			srk.rollbackTransaction();
		}
	}

}
