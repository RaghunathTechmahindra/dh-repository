package com.filogix.express.outbound.loanDecision.payload;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.deal.entity.Deal;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.UnitTestHelper;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.LoanDecisionType;

public class LoanDecisionTypeGeneratorTest {

	ObjectFactory factory;
	SessionResourceKit srk;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.factory = new ObjectFactory();
		this.srk = new SessionResourceKit();
		srk.getExpressState().cleanAllIds();
		srk.getExpressState().setDealInstitutionId(UnitTestHelper.getDefaultInstitution(srk));
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		this.srk.freeResources();
	}

	@Test
	public void testCreateLoanDecisionType() throws JdbcTransactionException {
		try{
			srk.beginTransaction();
			Deal deal = UnitTestHelper.createDefaultDealTree(srk);
			
			LoanDecisionType ldt = LoanDecisionTypeGenerator.
			createLoanDecisionType(deal, factory);
			
			assertEquals(ldt.getDeal().getApplicationId(), deal.getSourceApplicationId());
			
		} catch (Exception e) {
			fail("Exception during unit test:" + e.getMessage());
		} finally {
			srk.rollbackTransaction();
		}
	}

}
