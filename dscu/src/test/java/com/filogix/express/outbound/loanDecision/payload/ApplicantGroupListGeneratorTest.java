package com.filogix.express.outbound.loanDecision.payload;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;

import MosSystem.Mc;

import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.pk.DealPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.schema.fcx._1.ApplicantDecisionType;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.LoanDecisionType.ApplicantGroup;

/**
 * @author HKobayashi
 *
 */
public class ApplicantGroupListGeneratorTest {


	private int getDefaultInstitution(SessionResourceKit srk) throws RemoteException, FinderException{
		InstitutionProfile ip = new InstitutionProfile(srk);
		return ip.findByFirst().getInstitutionProfileId();
	}

	private Deal createDealTree(SessionResourceKit srk) throws Exception{

		MasterDeal md = MasterDeal.createDefaultTree(srk, null);
		Deal deal = new Deal(srk, null, md.getDealId(), 1);

		Borrower b0 = null;
		for(Borrower temp : (Collection<Borrower>)deal.getBorrowers()){
			b0 = temp;
		}

		b0.setPrimaryBorrowerFlag("Y");
		b0.setBorrowerTypeId(Mc.BT_BORROWER);
		b0.setMaritalStatusId(2);
		b0.ejbStore();

		Borrower b1 = new Borrower(srk, null);
		b1.create((DealPK) deal.getPk());
		b1.setPrimaryBorrowerFlag("N");
		b1.setBorrowerTypeId(Mc.BT_BORROWER);
		b1.setMaritalStatusId(2);
		b1.ejbStore();

		Borrower b2 = new Borrower(srk, null);
		b2.create((DealPK) deal.getPk());
		b2.setPrimaryBorrowerFlag("N");
		b2.setBorrowerTypeId(Mc.BT_GUARANTOR);
		b2.setMaritalStatusId(2);
		b2.ejbStore();

		Borrower b3 = new Borrower(srk, null);
		b3.create((DealPK) deal.getPk());
		b3.setPrimaryBorrowerFlag("N");
		b3.setBorrowerTypeId(Mc.BT_GUARANTOR);
		b3.setMaritalStatusId(5);
		b3.ejbStore();

		Borrower b4 = new Borrower(srk, null);
		b4.create((DealPK) deal.getPk());
		b4.setPrimaryBorrowerFlag("N");
		b4.setBorrowerTypeId(Mc.BT_BUILDER);
		b4.setMaritalStatusId(6);
		b4.ejbStore();

		Borrower b5 = new Borrower(srk, null);
		b5.create((DealPK) deal.getPk());
		b5.setPrimaryBorrowerFlag("N");
		b5.setBorrowerTypeId(Mc.BT_GUARANTOR);
		b5.setMaritalStatusId(2);
		b5.ejbStore();
		
		return deal;
	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.ApplicantGroupListGenerator#populateApplicantGroupList(java.util.List, com.basis100.deal.entity.Deal, com.filogix.schema.fcx._1.ObjectFactory)}.
	 */
	@Test
	public void testPopulateApplicantGroupList() {

		SessionResourceKit srk = new SessionResourceKit();
		try{
			srk.beginTransaction();

			srk.getExpressState().setDealInstitutionId(getDefaultInstitution(srk));
			
			Deal deal = createDealTree(srk);

			List<ApplicantGroup> appGroupList = new ArrayList<ApplicantGroup>();
			ObjectFactory factory = new ObjectFactory();

			ApplicantGroupListGenerator.populateApplicantGroupList(
					appGroupList, deal, factory);

			assertEquals(3, appGroupList.size());

			ApplicantGroup ag0 = appGroupList.get(0);
			assertEquals(ApplicantGroupListGenerator.BT_BORROWER_EXTERNAL, ag0.getApplicantGroupTypeDd().intValue());
			
			List<ApplicantDecisionType> applicantList0 = ag0.getApplicant();
			assertEquals(2, applicantList0.size());

			ApplicantGroup ag1 = appGroupList.get(1);
			assertEquals(ApplicantGroupListGenerator.BT_GUARANTOR_EXTERNAL, ag1.getApplicantGroupTypeDd().intValue());

			List<ApplicantDecisionType> applicantList1 = ag1.getApplicant();
			assertEquals(2, applicantList1.size());

			ApplicantGroup ag2 = appGroupList.get(2);
			assertEquals(ApplicantGroupListGenerator.BT_GUARANTOR_EXTERNAL, ag2.getApplicantGroupTypeDd().intValue());

			List<ApplicantDecisionType> applicantList2 = ag2.getApplicant();
			assertEquals(1, applicantList2.size());


		}catch(Exception e){
			e.printStackTrace();
			fail(e.getMessage());
		}finally{
			srk.cleanTransaction();
			srk.freeResources();
		}
	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.ApplicantGroupListGenerator#populateApplicantDecisionType(com.filogix.schema.fcx._1.ApplicantDecisionType, com.basis100.deal.entity.Borrower, com.basis100.deal.entity.Deal, com.filogix.schema.fcx._1.ObjectFactory)}.
	 * @throws FinderException 
	 * @throws RemoteException 
	 * @throws CreateException 
	 */
	@Test
	public void testPopulateApplicantDecisionType() throws RemoteException, FinderException, CreateException {
		ObjectFactory f = new ObjectFactory();
		SessionResourceKit srk = new SessionResourceKit();
		try{
			srk.beginTransaction();
			srk.getExpressState().setDealInstitutionId(getDefaultInstitution(srk));
			
			MasterDeal md = MasterDeal.createDefaultTree(srk, null);
			Deal deal = new Deal(srk, null, md.getDealId(), 1);
			Borrower b = new Borrower(srk, null);
			b = b.findByFirstBorrower(deal.getDealId(), deal.getCopyId());
			
			b.setLanguagePreferenceId(1);
			b.setSalutationId(2);
			b.setBorrowerFirstName("Denis");
			b.setBorrowerMiddleInitial("X");
			b.setBorrowerLastName("Aida");
			b.setPrimaryBorrowerFlag("Y");//no such flug, but it test logic
            b.setSuffixId(1);
			b.ejbStore();
			
			ApplicantDecisionType appDecType = f.createApplicantDecisionType();
			ApplicantGroupListGenerator.populateApplicantDecisionType(appDecType, b, deal, f);

			assertEquals(1, appDecType.getLanguagePreferenceDd().intValue());
			assertEquals(2, appDecType.getName().getSalutationDd().intValue());
			assertEquals("Denis", appDecType.getName().getFirstName());
			assertEquals("X", appDecType.getName().getMiddleInitial());
			assertEquals("Aida", appDecType.getName().getLastName());
			assertEquals("1", appDecType.getPrimaryApplicantFlag());
            assertEquals(new BigDecimal(1), appDecType.getName().getSuffixDd());
		}catch(Exception e){
			fail(e.getMessage());
		}finally{
			srk.cleanTransaction();
			srk.freeResources();
		}

		


	}

}
