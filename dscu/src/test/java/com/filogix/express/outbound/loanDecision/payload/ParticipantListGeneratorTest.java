/*
 * @(#)ParticipantListGeneratorTest.java     14-Aug-09
 * 
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.outbound.loanDecision.payload;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.PartyProfile;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.UnitTestHelper;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.ParticipantType;

/**
 * @author HKobayashi
 *
 */
public class ParticipantListGeneratorTest {

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.ParticipantListGenerator#populateParticipantList(java.util.List, com.basis100.deal.entity.Deal, com.filogix.schema.fcx._1.ObjectFactory)}.
	 * @throws Exception 
	 */
	@Test
	public void testPopulateParticipantList() throws Exception {

		ObjectFactory fac = new ObjectFactory();
		SessionResourceKit srk = new SessionResourceKit();
		try{
			srk.beginTransaction();
			srk.getExpressState().setDealInstitutionId(UnitTestHelper.getDefaultInstitution(srk));
			Deal deal = UnitTestHelper.createDefaultDealTree(srk);

			List<ParticipantType> list = new ArrayList();


			PartyProfile pp = new PartyProfile(srk);

			createDummyPartyProfile(srk, deal, 50, "ptb-01");
			createDummyPartyProfile(srk, deal, 51, "ptb-02");
			createDummyPartyProfile(srk, deal, 62, "ptb-03");
			createDummyPartyProfile(srk, deal, 62, null);
			createDummyPartyProfile(srk, deal, 62, "ptb-05");

			ParticipantListGenerator.populateParticipantList(list, deal, fac);

			assertEquals(2, list.size());
			assertEquals("ptb-03", list.get(0).getBranchTransitNo());
			assertEquals("ptb-05", list.get(1).getBranchTransitNo());
			
		}finally{
			srk.cleanTransaction();
			srk.freeResources();
		}
	}

	private PartyProfile createDummyPartyProfile(SessionResourceKit srk, Deal deal, int ptypeId, String ptBusinessid) throws Exception{
		final int statusActive = 0;
		
		PartyProfile pp = new PartyProfile(srk);
		
		pp.create(ptypeId, statusActive);
		pp.associate(deal.getDealId());
		pp.setPtPBusinessId(ptBusinessid);
		pp.ejbStore();
		return pp;
	}
}
