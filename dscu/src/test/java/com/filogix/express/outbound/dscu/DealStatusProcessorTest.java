/**
 * 
 */
package com.filogix.express.outbound.dscu;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEventStatusSnapshot;
import com.basis100.deal.entity.DealEventStatusUpdateAssoc;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.pk.DealEventStatusSnapshotPK;
import com.basis100.deal.pk.DealEventStatusUpdateAssocPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.ESBOutboundQueuePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.ExpressOutboundTestContext;
import com.filogix.express.test.ExpressEntityTestCase;

/**
 * 
 * DealStatusProcessorTest
 * 
 * @version 1.0 Jun 9, 2009
 */
public class DealStatusProcessorTest extends ExpressEntityTestCase {

    // The logger
    private final static Log _log = LogFactory
            .getLog(DealStatusProcessorTest.class);

    private SessionResourceKit srk;

    private int dealId;

    private Deal deal;

    private ESBOutboundQueue statusEvent;

    private int queueId;

    private int institutionId;

    private DealStatusProcessor dsProcessor;

    /**
     * @throws Exception 
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception  {
        _log.info("DealStatusProcessor initialize...");

        ResourceManager.init();
        srk = new SessionResourceKit("Test");
        deal = new Deal(srk, null);

        queueId = _dataRepository.getInt("ESBOutboundQueue",
                "ESBOutboundQueueId", 1);
        dealId = _dataRepository.getInt("ESBOutboundQueue", "DealId", 1);
        institutionId = _dataRepository.getInt("ESBOutboundQueue",
                "institutionProfileId", 1);

        srk.getExpressState().setDealInstitutionId(institutionId);

        DealPK dpk = new DealPK(dealId, -1);
        deal = deal.findByRecommendedScenario(dpk, true);

        statusEvent = new ESBOutboundQueue(srk);
        statusEvent = statusEvent.findByPrimaryKey(new ESBOutboundQueuePK(
                queueId));

        dsProcessor = (DealStatusProcessor) ExpressOutboundTestContext.
            getService("ESBOUTBOUND_SERVICE_9");
        
        _log.info("DealStatusProcessorTest initialize... finished ");
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        srk.freeResources();
        _log.info("DealStatusProcessorTest tearDown...");
    }

    /**
     * Test method for
     * {@link com.filogix.express.dscu.processor.DealStatusProcessor#handleProcess(java.lang.Object)}.
     * 
     * @throws Exception
     */
    @Test
    public void testHandleProcess() throws Exception {

        _log.info("dscu testing HandleProcess queueid: "
                + statusEvent.getESBOutboundQueueId());

        dsProcessor.setSessionResourceKit(srk);
        dsProcessor.handleProcess(statusEvent);
    }

    /**
     * testCheckStatusUpdateSendPayload
     * 
     * @throws RemoteException
     * @throws CreateException
     * @throws JdbcTransactionException
     * 
     * @throws Exception
     */
    @Test
    public void testCheckStatusUpdate() throws RemoteException,
            JdbcTransactionException, CreateException {

        _log.info("dscu testing CheckStatusUpdateSendPayload queueid: "
                + statusEvent.getESBOutboundQueueId());

        int typeId = DESUpdatePayloadGenerator.DESTATUS_TYPE_MI;
        DealEventStatusUpdateAssoc updateAssoc = new DealEventStatusUpdateAssoc(
                srk);
        try {
            updateAssoc.findByPrimaryKey(new DealEventStatusUpdateAssocPK(deal
                    .getSystemTypeId(), typeId, deal.getMIStatusId()));
        } catch (FinderException e1) {
            _log.error("Fail to find DealEventStatusUpdateAssoc in DealStatusProcessor");
            updateAssoc = null;
        }

        int replacementId = 0;
        if (updateAssoc != null)
            replacementId = updateAssoc.getStatusNameReplacementId();

        DealEventStatusSnapshot dealSnapShot = new DealEventStatusSnapshot(srk);
        try {
            dealSnapShot = dealSnapShot.findByEventStatusType(
                    new DealEventStatusSnapshotPK(statusEvent.getDealId(), typeId),
                    typeId);
        } catch (FinderException e) {
            _log.error("Fail to find DealEventStatusSnapshot in DealStatusProcessor");
            dealSnapShot = null;
        }

        boolean update = dsProcessor.checkStatusUpdate(deal,
                DESUpdatePayloadGenerator.DESTATUS_TYPE_MI, deal
                        .getMIStatusId());
        _log.info("testCheckStatusUpdate result: " + update);
    }

}
