/*
 * @(#)CommitmentPayloadGeneratorTest.java     12-Aug-09
 * 
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.outbound.loanDecision.payload;

import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import MosSystem.Mc;

import com.basis100.deal.docprep.Dc;
import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.Fee;
import com.basis100.deal.entity.DocumentTracking.DocTrackingVerbiage;
import com.basis100.deal.pk.DealPK;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.UnitTestHelper;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.ResponseType;
import com.filogix.schema.fcx._1.ResponseType.Commitment;

/**
 * @author HKobayashi
 * 
 */
public class CommitmentPayloadGeneratorTest {
	
	ObjectFactory factory;
	SessionResourceKit srk;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.factory = new ObjectFactory();
		this.srk = new SessionResourceKit();
		srk.getExpressState().cleanAllIds();
		srk.getExpressState().setDealInstitutionId(UnitTestHelper.getDefaultInstitution(srk));
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		this.srk.getExpressState().cleanAllIds();
		this.srk.freeResources();
	}

	/**
	 * Test method for
	 * {@link com.filogix.express.outbound.loanDecision.payload.CommitmentPayloadGenerator#populateResponseTypeSpecific(com.filogix.schema.fcx._1.ResponseType, com.basis100.deal.entity.Deal, com.filogix.schema.fcx._1.ObjectFactory)}.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testPopulateResponseTypeSpecific() throws Exception {

		CommitmentPayloadGenerator target = new CommitmentPayloadGenerator();
		try{
			srk.beginTransaction();

			Deal d = new Deal(srk, null);
			ResponseType rt = factory.createResponseType();
	
			Date now = new Date();
			d.setCommitmentExpirationDate(now);
			d.setReturnDate(now);
	
			// execute
			target.populateResponseTypeSpecific(rt, d, factory);
	
			Commitment c = rt.getCommitment();
	
			assertEquals(Convert.toXMLGregorianCalendar(now), c
					.getCommitmentExpirationDate());
			assertEquals(Convert.toXMLGregorianCalendar(now), c
					.getReturnDate());
	
			assertTrue(c.getDocumentTracking().isEmpty());
		}catch (Exception e) {
			fail("Exception during unit test:" + e.getMessage());
		} finally {
			srk.rollbackTransaction();
		}

	}

	@Test
	public void testPopulateListOfDocumentTracking() throws Exception {
		CommitmentPayloadGenerator target = new CommitmentPayloadGenerator();

		ArrayList<DocumentTracking> collectionOfDt = new ArrayList<DocumentTracking>();
		ArrayList<com.filogix.schema.fcx._1.ResponseType.Commitment.DocumentTracking> fcxDocTrackList = 
			new ArrayList<com.filogix.schema.fcx._1.ResponseType.Commitment.DocumentTracking>();
		
		try{
			srk.beginTransaction();
			Deal deal = UnitTestHelper.createDefaultDealTree(srk);
			
			DocumentTracking doc_track_with_text = new DocumentTracking(srk);
			doc_track_with_text.setDocumentStatusId(1);
			DocTrackingVerbiage verb0 = doc_track_with_text.new DocTrackingVerbiage(srk, 1, 1,
					Mc.LANGUAGE_PREFERENCE_ENGLISH, "en-label", "en-text");
			doc_track_with_text.getVerb().add(verb0);
			DocTrackingVerbiage verb1 = doc_track_with_text.new DocTrackingVerbiage(srk, 1, 1,
					Mc.LANGUAGE_PREFERENCE_FRENCH, "fr-label", "fr-text");
			doc_track_with_text.getVerb().add(verb1);
			doc_track_with_text.ejbStore();
	
			collectionOfDt.add(doc_track_with_text);
			collectionOfDt.add(new DocumentTracking(srk));
			collectionOfDt.add(new DocumentTracking(srk));
			
			// execute
			target.populateListOfDocumentTracking(deal, factory, fcxDocTrackList,
					collectionOfDt);
	
			assertEquals(1, fcxDocTrackList.size());
		
		} catch (Exception e) {
			fail("Exception during unit test:" + e.getMessage());
		} finally {
			srk.rollbackTransaction();
		}

	}

	@Test
	public void testPopulateDocumentTracking() throws Exception {

		CommitmentPayloadGenerator target = new CommitmentPayloadGenerator();

		com.filogix.schema.fcx._1.ResponseType.Commitment.DocumentTracking fcx = factory
		.createResponseTypeCommitmentDocumentTracking();
		
		try{
			srk.beginTransaction();
			DocumentTracking db = new DocumentTracking(srk);
			db.setDocumentStatusId(1);
			DocTrackingVerbiage verb0 = db.new DocTrackingVerbiage(srk, 1, 1,
					Mc.LANGUAGE_PREFERENCE_ENGLISH, "en-label", "en-text");
			db.getVerb().add(verb0);
			DocTrackingVerbiage verb1 = db.new DocTrackingVerbiage(srk, 1, 1,
					Mc.LANGUAGE_PREFERENCE_FRENCH, "fr-label", "fr-text");
			db.getVerb().add(verb1);
	
			// execute
			target.populateDocumentTracking(0, db, fcx);
	
			assertEquals(1, fcx.getDocumentStatusDd().intValue());
			assertEquals("en-text", fcx.getDocumentText());
	
			// execute
			target.populateDocumentTracking(1, db, fcx);
	
			assertEquals("fr-text", fcx.getDocumentText());
		} catch (Exception e) {
			fail("Exception during unit test:" + e.getMessage());
		} finally {
			srk.rollbackTransaction();
		}
	}
	
	@Test
	public void testPopulateDealFees() throws JdbcTransactionException {
		
		double FEE_AMOUNT = 500;
		Date FEE_PAYMENT_DATE = new Date();
		int FEE_PAYMENT_METHOD_ID = 1;
		int FEE_STATUS_ID = 0;
		String REFUNDABLE = "Y";
		int FEE_PAYOR_TYPE_ID = 0;
		int FEE_TYPE_ID = 5;

		srk.getExpressState().setDealInstitutionId(UnitTestHelper.getDefaultInstitution(srk));
		CommitmentPayloadGenerator target = new CommitmentPayloadGenerator();
		
		try{
			srk.beginTransaction();
			Deal deal = UnitTestHelper.createDefaultDealTree(srk);
			DealFee df1_creator = new DealFee(srk, null);
			DealFee df1 = df1_creator.create((DealPK)deal.getPk());
			df1.ejbStore();
			DealFee df2_creator = new DealFee(srk, null);
			DealFee df2 = df2_creator.create((DealPK)deal.getPk());
			df2.ejbStore();
			DealFee df3_creator = new DealFee(srk, null);
			DealFee df3 = df3_creator.create((DealPK)deal.getPk());
			df3.ejbStore();
			DealFee df4_creator = new DealFee(srk, null);
			DealFee df4 = df4_creator.create((DealPK)deal.getPk());
			df4.ejbStore();
			
			Fee fee1_creator = new Fee(srk, null);
			//fee1 should be included in the output.
			Fee fee1 = fee1_creator.create( "fee1",
                    27,
                    0,
                    0,
                    FEE_PAYOR_TYPE_ID,
                    "Courier Fee",
                    "N",
                    25,
                    0,
                    1,
                    "0",
                    "0");
			fee1.setMITypeFlag("N");
			fee1.setPayableIndicator("N");
			fee1.setRefundable(REFUNDABLE);
			fee1.setFeePayorTypeId(FEE_PAYOR_TYPE_ID);
			fee1.setFCXFeeTypeID(new Integer(FEE_TYPE_ID));
			fee1.ejbStore();
			df1.setFeeId(fee1.getFeeId());
			
			//fee2 fee3 and fee4 shouldn't be included.
			Fee fee2_creator = new Fee(srk, null);
			Fee fee2 = fee2_creator.create( "fee2",
                    27,
                    0,
                    0,
                    FEE_PAYOR_TYPE_ID,
                    "Courier Fee",
                    "N",
                    25,
                    0,
                    1,
                    "0",
                    "0");
			fee2.setMITypeFlag("Y");
			fee2.setPayableIndicator("N");
			fee2.setRefundable(REFUNDABLE);
			fee2.setFeePayorTypeId(FEE_PAYOR_TYPE_ID);
			fee2.ejbStore();
			df2.setFeeId(fee2.getFeeId());
			
			Fee fee3_creator = new Fee(srk, null);
			Fee fee3 = fee3_creator.create( "fee3",
                    27,
                    0,
                    0,
                    FEE_PAYOR_TYPE_ID,
                    "Courier Fee",
                    "Y",
                    25,
                    0,
                    1,
                    "0",
                    "0");
			fee3.setMITypeFlag("N");
			fee3.setPayableIndicator("Y");
			fee3.setRefundable(REFUNDABLE);
			fee3.setFeePayorTypeId(FEE_PAYOR_TYPE_ID);
			fee3.ejbStore();
			df3.setFeeId(fee3.getFeeId());
			
			Fee fee4_creator = new Fee(srk, null);
			Fee fee4 = fee4_creator.create( "fee4",
                    27,
                    0,
                    0,
                    FEE_PAYOR_TYPE_ID,
                    "Courier Fee",
                    "Y",
                    25,
                    0,
                    1,
                    "0",
                    "0");
			fee4.setMITypeFlag("Y");
			fee4.setPayableIndicator("Y");
			fee4.setRefundable(REFUNDABLE);
			fee4.setFeePayorTypeId(FEE_PAYOR_TYPE_ID);
			fee4.ejbStore();
			df4.setFeeId(fee4.getFeeId());
			
			df1.setFeeAmount(FEE_AMOUNT);
			df1.setFeePaymentDate(FEE_PAYMENT_DATE);
			df1.setFeePaymentMethodId(FEE_PAYMENT_METHOD_ID);
			df1.setFeeStatusId(FEE_STATUS_ID);
			df1.ejbStore();
			
			Commitment fcx = factory.createResponseTypeCommitment();
			target.populateDealFees(deal, factory, fcx);
			
			assertEquals(1, fcx.getDealFee().size());
			assertEquals(Convert.toBigDecimal(FEE_AMOUNT), fcx.getDealFee().get(0).getFeeAmount());
			assertEquals(Convert.toBigDecimal(FEE_TYPE_ID), fcx.getDealFee().get(0).getFee().getFeeTypeDd());
			
		} catch (Exception e) {
			fail("Exception during unit test:" + e.getMessage());
		} finally {
			srk.rollbackTransaction();
		}
		
	}
	
	@Test
	public void testPopulateDealFees2() throws JdbcTransactionException {
		
		double FEE_AMOUNT = 500;
		Date FEE_PAYMENT_DATE = new Date();
		int FEE_PAYMENT_METHOD_ID = 1;
		int FEE_STATUS_ID = 0;
		String REFUNDABLE = "Y";
		int FEE_PAYOR_TYPE_ID = 0;
		Integer FEE_TYPE_ID = null;
		
		srk.getExpressState().setDealInstitutionId(UnitTestHelper.getDefaultInstitution(srk));
		srk.beginTransaction();
		
		try{
			CommitmentPayloadGenerator target = new CommitmentPayloadGenerator();
			ObjectFactory factory = new ObjectFactory();
			
			Deal deal = UnitTestHelper.createDefaultDealTree(srk);
			DealFee df1_creator = new DealFee(srk, null);
			DealFee df1 = df1_creator.create((DealPK)deal.getPk());
			df1.ejbStore();
			
			Fee fee1_creator = new Fee(srk, null);
			//fee1 should be included in the output.
			Fee fee1 = fee1_creator.create( "fee1",
                    27,
                    0,
                    0,
                    FEE_PAYOR_TYPE_ID,
                    "Courier Fee",
                    "N",
                    25,
                    0,
                    1,
                    "0",
                    "0");
			fee1.setMITypeFlag("N");
			fee1.setPayableIndicator("N");
			fee1.setRefundable(REFUNDABLE);
			fee1.setFeePayorTypeId(FEE_PAYOR_TYPE_ID);
			fee1.setFCXFeeTypeID(FEE_TYPE_ID);
			fee1.ejbStore();
			df1.setFeeId(fee1.getFeeId());
			
			df1.setFeeAmount(FEE_AMOUNT);
			df1.setFeePaymentDate(FEE_PAYMENT_DATE);
			df1.setFeePaymentMethodId(FEE_PAYMENT_METHOD_ID);
			df1.setFeeStatusId(FEE_STATUS_ID);
			df1.ejbStore();
			
			Commitment fcx = factory.createResponseTypeCommitment();
			target.populateDealFees(deal, factory, fcx);
			
			assertEquals(1, fcx.getDealFee().size());
			assertEquals(Convert.toBigDecimal(FEE_AMOUNT), fcx.getDealFee().get(0).getFeeAmount());
			assertEquals(null, fcx.getDealFee().get(0).getFee().getFeeTypeDd());
			
		} catch (Exception e) {
			fail("Exception during unit test:" + e.getMessage());
		} finally {
			srk.rollbackTransaction();
		}
		
	}
	
	private DocumentTracking buildFreeFormCondition(DealPK pk, String text, String label) throws Exception
	  {
	    Condition source = new Condition(srk,0);

	    DocumentTracking dt = new DocumentTracking(srk);

	    dt = dt.create(source,pk.getId(),pk.getCopyId(),Mc.DOC_STATUS_OPEN, Dc.DT_SOURCE_CUSTOM_FREEFORM);

	    dt.setDocumentTrackingSourceId(Mc.DOCUMENT_TRACKING_SOURCE_CUSTOM_FREE_FORM);

	    dt.setDocumentLabelForLanguage(0,label);

	    dt.setDocumentLabelForLanguage(1,label);

	    dt.setDocumentTextForLanguage(0,text);

	    dt.setDocumentTextForLanguage(1,text);

	    dt.ejbStore();

	    return dt;

	  }
}
