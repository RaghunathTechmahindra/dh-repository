/*
 * @(#)MortgageTypeGeneratorTest.java     Aug 14, 2009
 * 
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.outbound.loanDecision.payload;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.UnitTestHelper;
import com.filogix.schema.fcx._1.MortgageType;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.MortgageType.LenderSubmission;
import com.filogix.schema.fcx._1.MortgageType.PricingRateInventory;
import com.filogix.schema.fcx._1.MortgageType.Rate;

/**
 * @author Phil.Yuan
 *
 */
public class MortgageTypeGeneratorTest {

	ObjectFactory factory;
	SessionResourceKit srk;

	public static final double ADVANCE_HOLD = 2;
	public static final String COMMISSION_CODE = "commission code";
	public static final double DISCOUNT = 1;
	public static final String EXISTING_ACCOUNT_INDICATOR = "No existing account";
	public static final String EXISTING_ACCOUNT_NUMBER = "No existing number";
	public static final Date FIRST_PAYMENT_DATE = new Date();
	public static final Date INTEREST_ADJUSTMENT_DATE = new Date();
	public static final double LOC_AMOUNT = 20000;
	public static final String MI_ALLOCATE_FLAG = "Y";
	public static final double NET_INTEREST_RATE = 7;
	public static final double P_AND_I_PAYMENT_AMOUNT = 1000;
	public static final double P_AND_I_PAYMENT_AMOUNT_MONTHLY = 50;
	public static final int PAYMENT_FREQUENCY_ID = 1;
	public static final double PREMIUM = 2;
	public static final int PREPAYMENT_OPTIONS_ID = 5;
	public static final int PRIVILEGE_PAYMENT_ID = 1;
	public static final String PROPERTY_TAX_ALLOCATE_FLAG = "Y";
	public static final double PROPERTY_TAX_ESCROW_AMOUNT = 500; 
	public static final double TOTAL_LOC_AMOUNT = 20000;

	public static final int ACTUAL_PAYMENT_TERM = 10;
	public static final int IAD_NUMBER_OF_DAYS = 20;
	public static final double INTEREST_ADJUSTMENT_AMOUNT = 2;
	public static final double LOAN_AMOUNT = 10000;
	public static final Date MATURITY_DATE = new Date();
	public static final double PER_DIEM_INTEREST_AMOUNT = 2;

	public static final double ADDITIONAL_PRINCIPAL = 50000;
	public static final int AMORTIZATION_TERM = 35;
	public static final double BALANCE_REMAINING_AT_END_OF_TERM = 300000;
	public static final double BUY_DOWN_RATE = 5;
	public static final double CASH_BACK_AMOUNT = 0;
	public static final String CASH_BACK_AMOUNT_OVERRIDE = "N";
	public static final double CASH_BACK_PERCENT = 0;
	public static final String ORIGINAL_MTG_NUMBER = "  123";
	public static final String CURRENT_MTG_NUMBER = "123";
	public static final int EFFECTIVE_AMORTIZATION_MONTHS = 60;
	public static final Date FIRST_PAYMENT_DATE_MONTHLY = new Date();

	public static final double MORTGAGE_AMOUNT = 400000;
	public static final int RATE_GUARANTEE_PERIOD = 60;
	public static final double TOTAL_MORTGAGE_AMOUNT = 20000;

	public static final String SOURCE_APPLICATION_VERSION = "1.1";
	public static final int AFFILIATION_PROGRAM_ID = 1;
	public static final int INTEREST_COMPOUND_ID = 2;
	public static final int PRODUCT_TYPE_ID = 3;
	public static final String MI_POLICY_NUMBER = "123";
	public static final int MORTGAGE_INSURER_ID = 2;
	public static final double MORTGAGE_INSURER_DD = 4.0;
	public static final int MTG_PRODUCT_ID = 0;
	public static final int LIEN_POSITION_ID = 23455;
	public static final double NET_LOAN_AMOUNT = 50000;
	public static final int PAYMENT_TERM_ID= 3;
	public static final int PROGRESS_ADVANCE_TYPE_ID = 11100;
	public static final double TOTAL_LOAN_AMOUNT = 500000;
	public static final String MCC_MARKET_TYPE = "5";

	public static final String FXLINK_POS_CHANNEL = "666";
	public static final String FXLINK_UW_CHANNEL = "777";

	public static final Date INDEX_EFFECTIVE_DATE = new Date();
	public static final int REPAYMENT_TYPE_ID = 1;


	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.factory = new ObjectFactory();
		this.srk = new SessionResourceKit();
		srk.getExpressState().setDealInstitutionId(UnitTestHelper.getDefaultInstitution(srk));
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		this.srk.freeResources();
	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.MortgageTypeGenerator#createMortgageType(com.basis100.deal.entity.Deal, com.filogix.schema.fcx._1.ObjectFactory)}.
	 * @throws JdbcTransactionException 
	 */
	@Test
	public void testCreateMortgageType() throws JdbcTransactionException {
		try{
			srk.beginTransaction();

			MasterDeal md = MasterDeal.createDefaultTree(srk, null);
			Deal db = new Deal(srk, null, md.getDealId(), 1);
			db.setSourceApplicationVersion(SOURCE_APPLICATION_VERSION);

			MortgageType fcx = MortgageTypeGenerator.createMortgageType(db, factory);

			assertEquals(fcx.getDealId(), Convert.toBigDecimal(SOURCE_APPLICATION_VERSION));
		} catch (Exception e) {
			fail("Exception during unit test:" + e.getMessage());
		} finally {
			srk.rollbackTransaction();
		}

	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.MortgageTypeGenerator#populateMortgageTypeRate(com.filogix.schema.fcx._1.MortgageType.Rate, com.basis100.deal.entity.Deal)}.
	 * @throws JdbcTransactionException 
	 */
	@Test
	public void testPopulateMortgageTypeRate() throws JdbcTransactionException {

		try{
			srk.beginTransaction();

			MasterDeal md = MasterDeal.createDefaultTree(srk, null);
			Deal db = new Deal(srk, null, md.getDealId(), 1);

			db.setBuydownRate(BUY_DOWN_RATE);
			db.setDiscount(DISCOUNT);
			db.setNetInterestRate(NET_INTEREST_RATE);
			db.setNetInterestRate(NET_INTEREST_RATE);
			db.setPremium(PREMIUM);
			db.ejbStore();

			Rate fcx = factory.createMortgageTypeRate();
			MortgageTypeGenerator.populateMortgageTypeRate(fcx, db);

			assertEquals(fcx.getBuyDownRate(), Convert.toBigDecimal(BUY_DOWN_RATE));
			assertEquals(fcx.getDiscount(), Convert.toBigDecimal(DISCOUNT));
			assertEquals(fcx.getInterestRate(),Convert.toBigDecimal(NET_INTEREST_RATE));
			assertEquals(fcx.getNetRate(), Convert.toBigDecimal(NET_INTEREST_RATE));
			assertEquals(fcx.getPremium(), Convert.toBigDecimal(PREMIUM));

		} catch (Exception e) {
			fail("Exception during unit test:" + e.getMessage());
		} finally {
			srk.rollbackTransaction();
		}
	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.MortgageTypeGenerator#populatePricingRateInventory(com.filogix.schema.fcx._1.MortgageType.PricingRateInventory, com.basis100.deal.entity.PricingRateInventory)}.
	 * @throws JdbcTransactionException 
	 */
	@Test
	public void testPopulatePricingRateInventory() throws JdbcTransactionException {
		try{
			srk.beginTransaction();

			MasterDeal md = MasterDeal.createDefaultTree(srk, null);
			Deal deal = new Deal(srk, null, md.getDealId(), 1);
			com.basis100.deal.entity.PricingRateInventory db = deal.getPricingRateInventory();

			db.setIndexEffectiveDate(INDEX_EFFECTIVE_DATE);

			PricingRateInventory fcx = factory.createMortgageTypePricingRateInventory();
			MortgageTypeGenerator.populatePricingRateInventory(fcx, db);

			assertEquals(fcx.getIndexEffectiveDate(), Convert.toXMLGregorianCalendar(INDEX_EFFECTIVE_DATE));

		} catch (Exception e) {
			fail("Exception during unit test:" + e.getMessage());
		} finally {
			srk.rollbackTransaction();
		}
	}

	@Test
	public void testPopulateMortgageType_cashBackPercentage() throws Exception {

		try{
			srk.beginTransaction();
			
			Deal db = UnitTestHelper.createDefaultDealTree(srk);

			db.setCashBackPercent(999.98d);
			MortgageType fcx = factory.createMortgageType();
			MortgageTypeGenerator.populateMortgageType(fcx, db);

			assertEquals(Convert.toBigDecimal("999.98"), fcx.getCashBackPercentage());

			db.setCashBackPercent(1000.00d);
			fcx = factory.createMortgageType();
			MortgageTypeGenerator.populateMortgageType(fcx, db);

			assertEquals(Convert.toBigDecimal("999.99"), fcx.getCashBackPercentage());

		} catch (Exception e) {
			fail("Exception during unit test:" + e.getMessage());
		} finally {
			srk.rollbackTransaction();
		}
	}

	@Test
	public void testPopulateMortgageType_mtgProviderId() throws JdbcTransactionException {

		try{
			srk.beginTransaction();

			Deal db = UnitTestHelper.createDefaultDealTree(srk);
			db.setMortgageInsurerId(1);
			MortgageType fcx = factory.createMortgageType();
			MortgageTypeGenerator.populateMortgageType(fcx, db);
			assertEquals(Convert.toBigDecimal(1), fcx.getMtgProviderId());

			db.setMortgageInsurerId(0);
			fcx = factory.createMortgageType();
			MortgageTypeGenerator.populateMortgageType(fcx, db);
			assertNull(fcx.getMtgProviderId());

		} catch (Exception e) {
			fail("Exception during unit test:" + e.getMessage());
		} finally {
			srk.rollbackTransaction();
		}
	}


	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.MortgageTypeGenerator#populateMortgageType(com.filogix.schema.fcx._1.MortgageType, com.basis100.deal.entity.Deal)}.
	 * @throws JdbcTransactionException 
	 * @throws JdbcTransactionException 
	 */
	@Test
	public void testPopulateMortgageType() throws Exception {
		try{
			srk.beginTransaction();

			Deal db = UnitTestHelper.createDefaultDealTree(srk);

			db.setSourceApplicationVersion(SOURCE_APPLICATION_VERSION);
			db.setActualPaymentTerm(ACTUAL_PAYMENT_TERM);
			db.setAffiliationProgramId(AFFILIATION_PROGRAM_ID);

			db.setAmortizationTerm(AMORTIZATION_TERM);
			db.setCashBackAmount(CASH_BACK_AMOUNT);
			db.setCashBackPercent(CASH_BACK_PERCENT);
			db.setOriginalMortgageNumber(" 1234567890123456789012345");
			db.setInterestCompoundId(INTEREST_COMPOUND_ID);
			db.setRepaymentTypeId(REPAYMENT_TYPE_ID);
			db.setInterestTypeId(1); //EXP26981

			db.setProductTypeId(PRODUCT_TYPE_ID);
			db.setMIPolicyNumber(MI_POLICY_NUMBER);
			db.setMITypeId(PRODUCT_TYPE_ID);
			db.setMortgageInsurerId(MORTGAGE_INSURER_ID);
			db.setLienPositionId(LIEN_POSITION_ID);

			db.setProductTypeId(PRODUCT_TYPE_ID);
			db.setMortgageInsurerId(MORTGAGE_INSURER_ID);
			db.setNetLoanAmount(NET_LOAN_AMOUNT);
			db.setPandiPaymentAmount(P_AND_I_PAYMENT_AMOUNT);
			db.setPaymentFrequencyId(PAYMENT_FREQUENCY_ID);
			//db.setPaymentTermId(PAYMENT_TERM_ID);//EXP26914
			db.setRateGuaranteePeriod(RATE_GUARANTEE_PERIOD);
			db.setPriviligePaymentId(PRIVILEGE_PAYMENT_ID);
			db.setProgressAdvanceTypeId(PROGRESS_ADVANCE_TYPE_ID);
			db.setTotalLoanAmount(TOTAL_LOAN_AMOUNT);
			db.setMccMarketType(MCC_MARKET_TYPE);

			//FXP26686 start
			db.setMIFeeAmount(123.45);
			db.setMIPremiumAmount(234.56);
			db.setMIPremiumPST(345.67);
			//FXP26686 end

			MortgageType fcx = factory.createMortgageType();
			MortgageTypeGenerator.populateMortgageType(fcx, db);

			assertEquals(fcx.getDealId(), Convert.toBigDecimal(SOURCE_APPLICATION_VERSION));

			assertEquals(fcx.getActualPaymentTerm(), Convert.toBigDecimal(ACTUAL_PAYMENT_TERM));
			assertEquals(fcx.getAffiliationProgramDd(), Convert.toBigDecimal(AFFILIATION_PROGRAM_ID));
			assertEquals(fcx.getAmortizationTerm(), Convert.toBigDecimal(AMORTIZATION_TERM));
			assertEquals(fcx.getCashBackAmt(), Convert.toBigDecimal(CASH_BACK_AMOUNT));
			assertEquals(fcx.getCashBackPercentage(), Convert.toBigDecimal(CASH_BACK_PERCENT, 2));
			assertEquals(fcx.getCurrentMortgageNumber(), "123456789012345");
			assertEquals(fcx.getInterestCompoundDd(), Convert.toBigDecimal(INTEREST_COMPOUND_ID));
			assertEquals(fcx.getRepaymentTypeDd(), Convert.toBigDecimal(REPAYMENT_TYPE_ID));

			assertEquals(fcx.getLoanTypeDd(), Convert.toBigDecimal(MortgageTypeGenerator.convertLoanTypeId(PRODUCT_TYPE_ID)));
			assertEquals(fcx.getMiReferenceNumber(), MI_POLICY_NUMBER);
			assertEquals(fcx.getMortgageInsurerId(), Convert.toString(MORTGAGE_INSURER_ID));
			assertEquals(fcx.getMortgageTypeDd(), Convert.toBigDecimal(MortgageTypeGenerator.convertLoanTypeId(LIEN_POSITION_ID)));

			assertEquals(fcx.getMtgProductId(), Convert.toBigDecimal(PRODUCT_TYPE_ID));
			assertEquals(fcx.getMtgProviderId().doubleValue(), MORTGAGE_INSURER_ID, 0.0);
			assertEquals(fcx.getNetLoanAmount(), Convert.toBigDecimal(NET_LOAN_AMOUNT));
			assertEquals(fcx.getPAndIPaymentAmount(), Convert.toBigDecimal(P_AND_I_PAYMENT_AMOUNT));
			assertEquals(fcx.getPaymentFrequencyDd(), Convert.toBigDecimal(PAYMENT_FREQUENCY_ID));
			//assertEquals(fcx.getPaymentTermDd(), Convert.toBigDecimal(PAYMENT_TERM_ID));
			assertEquals(fcx.getPaymentTermDd(), Convert.toBigDecimal(db.getPaymentTermTypeId()));//EXP26914
			assertEquals(fcx.getInterestTypeDd(), Convert.toBigDecimal(db.getInterestTypeId()));//EXP26981
			assertEquals(fcx.getRateGuaranteeLength(), Convert.toBigDecimal(RATE_GUARANTEE_PERIOD));
			assertEquals(fcx.getPrivilegePaymentDd(), Convert.toBigDecimal(PRIVILEGE_PAYMENT_ID));
			assertEquals(fcx.getSingleProgressiveTypeDd(), Convert.toBigDecimal(PROGRESS_ADVANCE_TYPE_ID));
			assertEquals(fcx.getTotalLoanAmount(), Convert.toBigDecimal(TOTAL_LOAN_AMOUNT));
			assertEquals(fcx.getMarketSubmission(), MCC_MARKET_TYPE);			

			//FXP26686 start
			assertEquals(fcx.getMiFeeAmount(), Convert.toBigDecimal(123.45));
			assertEquals(fcx.getMiPremiumAmount(), Convert.toBigDecimal(234.56));
			assertEquals(fcx.getMiPremiumPst(), Convert.toBigDecimal(345.67));
			//FXP26686 end

		} finally {
			srk.rollbackTransaction();
		}
	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.MortgageTypeGenerator#populatelenderSubmission(com.filogix.schema.fcx._1.MortgageType.LenderSubmission, com.basis100.deal.entity.MasterDeal)}.
	 * @throws JdbcTransactionException 
	 */
	@Test
	public void testPopulatelenderSubmission() throws JdbcTransactionException {
		try{
			srk.beginTransaction();

			MasterDeal md = MasterDeal.createDefaultTree(srk, null);

			md.setFXLinkPOSChannel(FXLINK_POS_CHANNEL);
			md.setFXLinkUWChannel(FXLINK_UW_CHANNEL);

			LenderSubmission fcx = factory.createMortgageTypeLenderSubmission();
			MortgageTypeGenerator.populatelenderSubmission(fcx, md);

			assertEquals(fcx.getRoutingReceiver(), FXLINK_POS_CHANNEL);
			assertEquals(fcx.getRoutingSender(), FXLINK_UW_CHANNEL);

		} catch (Exception e) {
			fail("Exception during unit test:" + e.getMessage());
		} finally {
			srk.rollbackTransaction();
		}
	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.MortgageTypeGenerator#convertLoanTypeId(int)}.
	 */
	@Test
	public void testConvertLoanTypeId() {
		assertEquals(MortgageTypeGenerator.PRODUCT_TYPE_MORTGAGE_EX, 
				MortgageTypeGenerator.convertLoanTypeId(Mc.PRODUCT_TYPE_MORTGAGE));
		assertEquals(MortgageTypeGenerator.PRODUCT_TYPE_SECURED_LOC_EX, 
				MortgageTypeGenerator.convertLoanTypeId(Mc.PRODUCT_TYPE_SECURED_LOC));
		assertEquals(MortgageTypeGenerator.PRODUCT_TYPE_MULTIPLE_COMPONENT_EX,
				MortgageTypeGenerator.convertLoanTypeId(Mc.PRODUCT_TYPE_MULTIPLE_COMPONENT));
		assertEquals(100, MortgageTypeGenerator.convertLoanTypeId(100));

	}
	
	
	/**
	 * FXP31358 mortgageTypeDd not mapped correctly for Loan Decision
	 */
	@Test
	public void testConvertMortgageTypeId() {
		assertEquals(MortgageTypeGenerator.MORTGAGE_TYPE_FIRST, 
				MortgageTypeGenerator.convertMortgageTypeId(Mc.LIEN_POSITION_FIRST));
		assertEquals(MortgageTypeGenerator.MORTGAGE_TYPE_SECOND, 
				MortgageTypeGenerator.convertMortgageTypeId(Mc.LIEN_POSITION_SECOND));
		assertEquals(MortgageTypeGenerator.MORTGAGE_TYPE_THIRD,
				MortgageTypeGenerator.convertMortgageTypeId(Mc.LIEN_POSITION_FORTH));
		assertEquals(7, MortgageTypeGenerator.convertMortgageTypeId(7));

	}

}
