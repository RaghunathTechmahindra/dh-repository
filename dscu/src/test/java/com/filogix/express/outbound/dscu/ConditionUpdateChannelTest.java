package com.filogix.express.outbound.dscu;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEventStatusUpdateAssoc;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.DealEventStatusUpdateAssocPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.dscu.ConditionUpdateChannel;
import com.filogix.express.outbound.dscu.ConditionUpdatePayloadGenerator;
import com.filogix.express.outbound.dscu.DESUpdatePayloadGenerator;
import com.filogix.express.outbound.dscu.DESUpdatePayloadGenerator.DEStatus;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

public class ConditionUpdateChannelTest extends ExpressEntityTestCase implements UnitTestLogging {
    
    // The logger
    private final static Logger _logger = LoggerFactory.getLogger(ConditionUpdateChannelTest.class);
    private SessionResourceKit  _srk;
    private static final int DATA_COUNT_RollType = 17;
    private static final int DATA_COUNT_DEAL = 50;
    
    ConditionUpdateChannel channel = null;
    int                         userTypeId;
    int                         instProfId;
    String                      descEN;
    String                      descFR;
    int                        dealId;
    int                        institutionProfileId;
    int                        upId;
    Deal                       deal;
    UserProfile                up;
    String                      payload;
    
    @Before
    public void setup() throws Exception {
        
        ResourceManager.init();
        _srk = new SessionResourceKit();
        channel = new ConditionUpdateChannel();
        DocumentTracking dt = new DocumentTracking(_srk);
        Collection<DocumentTracking> dts = null;
        
        dts = setupRandom(dts);
        
        if (dts == null) {
            throw new Exception( "ERROR!!!! NO DEAL DATA");
        }
        upId = deal.getUnderwriterUserId();
        up = new UserProfile(_srk);
        up = up.findByPrimaryKey(new UserProfileBeanPK(upId, institutionProfileId));

    }
    
    public Collection<DocumentTracking> setupRandom(Collection<DocumentTracking> dts) throws Exception {
        
        String propertyConfig = PropertiesCache.getInstance().getProperty(-1,
                "statusUpdate.conditions.include.condition.types", "");
        if (propertyConfig.length()==0) return null;
        
        DocumentTracking dt = new DocumentTracking(_srk);
        for(int count = 0; count < DATA_COUNT_DEAL; count++) {
        //while (dts == null || dts.size()==0){
            Double indexD = new Double(Math.random() * DATA_COUNT_DEAL);
            int i = indexD.intValue();
          dealId  = _dataRepository.getInt("DEAL", "DealId", i);
          institutionProfileId  = _dataRepository.getInt("DEAL", "InstitutionProfileId", i);
          int copyId  = _dataRepository.getInt("DEAL", "CopdId", i);
            _srk.getExpressState().setDealIds(dealId, institutionProfileId, copyId);
            try {
                deal = new Deal(_srk, null);
                deal.setSilentMode(true);
                deal = deal.findByRecommendedScenario(new DealPK(dealId, copyId), true);
                if (deal.getSystemTypeId() == 8){
                    ConditionStatusProcessor csp = new ConditionStatusProcessor();
                    csp.setSessionResourceKit(_srk);
                    dts = csp.findMatchConditons(deal);
                }
            } catch (Exception e) {
                System.out.println("couldn't find deal Entity dealid=" + dealId);
            }
        }
        return dts;
    }
    
    public Collection<DocumentTracking> setupSeq(Collection<DocumentTracking> dts) throws Exception {
        
        DocumentTracking dt = new DocumentTracking(_srk);        
        for (int i=0; i<DATA_COUNT_DEAL; i++ ){
            dealId  = _dataRepository.getInt("DEAL", "DealId", i);
            institutionProfileId  = _dataRepository.getInt("DEAL", "InstitutionProfileId", i);
            int copyId  = _dataRepository.getInt("DEAL", "CopdId", i);
            _srk.getExpressState().setDealIds(dealId, institutionProfileId, copyId);
            try {
                deal = new Deal(_srk, null);
                deal.setSilentMode(true);
                deal = deal.findByRecommendedScenario(new DealPK(dealId, copyId), true);
                if (deal.getSystemTypeId() == 8){
                    ConditionStatusProcessor csp = new ConditionStatusProcessor();
                    csp.setSessionResourceKit(_srk);
                    dts = csp.findMatchConditons(deal);
                    if (dts!=null && dts.size() > 0) 
                        i = DATA_COUNT_DEAL;
                }
            } catch (Exception e) {
                System.out.println("couldn't find deal Entity dealid=" + dealId);
            }
        }
        return dts;
    }

    
    @After
    public void tearDown() {

      //      free resources
      _srk.freeResources( );

    }

    //@Test
    public void sendTestCM()  {
        Object o = null;
        try {
            DocumentTracking dt1 = new DocumentTracking(_srk);
            
            Collection<DocumentTracking> dts1 = dt1.findByDeal(new DealPK(dealId, deal.getCopyId()));
            
            ConditionUpdatePayloadGenerator payloadGenerator = new ConditionUpdatePayloadGenerator();
            payloadGenerator.init(deal);
            payloadGenerator.setConditions(dts1);
            payloadGenerator.setUserProfileId(upId);
    
            payloadGenerator.init(deal);
            payloadGenerator.setConditions(dts1);
            payloadGenerator.setUserProfileId(upId);
            payload = payloadGenerator.getXmlPayload();
            
    
            channel = new ConditionUpdateChannel();
            channel.setChannelId(17);
            channel.init(_srk, deal);
            o = channel.send(payload);
            
        } catch (Exception e) {
            e.printStackTrace();
        } 

    }

//    @Test
    public void sendTestDSU() throws Exception {

        DESUpdatePayloadGenerator payloadGenerator = new DESUpdatePayloadGenerator();
        Collection<DEStatus> destatuses = new ArrayList();
        
        SessionResourceKit srk = deal.getSessionResourceKit();
        DealEventStatusUpdateAssoc updateAssoc = new DealEventStatusUpdateAssoc(srk);
        updateAssoc.setSilentMode(true);
        
        try {
            updateAssoc.findByPrimaryKey(new DealEventStatusUpdateAssocPK(deal
                    .getSystemTypeId(), DESUpdatePayloadGenerator.DESTATUS_TYPE_APPLICATION, deal.getStatusId()));
            DEStatus dealStatus = payloadGenerator.new DEStatus(deal, updateAssoc, new Date());
            destatuses.add(dealStatus);
        } catch (Exception e) {
            
        }
        
        try {
            updateAssoc.findByPrimaryKey(new DealEventStatusUpdateAssocPK(deal
                    .getSystemTypeId(), DESUpdatePayloadGenerator.DESTATUS_TYPE_MI, deal.getMIStatusId()));
            DEStatus miStatus = payloadGenerator.new DEStatus(deal, updateAssoc, new Date());
            destatuses.add(miStatus);
        } catch (Exception e) {
            
        }
        
        payloadGenerator.init(deal);
        payloadGenerator.setDEStatuses(destatuses);
        payloadGenerator.setUserProfileId(upId);

        payloadGenerator.setUserProfileId(upId);
        payload = payloadGenerator.getXmlPayload();
        

        channel = new ConditionUpdateChannel();
        channel.setChannelId(17);
        channel.init(_srk, deal);
        Object o = channel.send(payload);
        assertNull(o);
    }
}
