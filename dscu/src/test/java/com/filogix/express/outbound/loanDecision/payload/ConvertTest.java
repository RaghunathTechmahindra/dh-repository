package com.filogix.express.outbound.loanDecision.payload;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ConvertTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testToXMLGregorianCalendar() throws DatatypeConfigurationException {
		Calendar c = Calendar.getInstance();
		c.set(2009, 6, 21, 15, 30, 21);
		Date d = new Date(c.getTimeInMillis());
		
		XMLGregorianCalendar xgc = Convert.toXMLGregorianCalendar(d);
		Assert.assertNotNull(xgc);
		Assert.assertEquals(2009, xgc.getYear());
		//XMLGregorianCalendar returns 1 to 12
		Assert.assertEquals(7, xgc.getMonth());
		Assert.assertEquals(21, xgc.getDay());
		Assert.assertEquals(15, xgc.getHour());
		Assert.assertEquals(30, xgc.getMinute());
		Assert.assertEquals(21, xgc.getSecond());
		
		assertNull(Convert.toXMLGregorianCalendar(null));
		
	}

	@Test
	public void testToBigDecimalInt() {
		BigDecimal actual = Convert.toBigDecimal(-1);
		BigDecimal expected = new BigDecimal(-1);
		Assert.assertEquals(expected, actual);
		
		actual = Convert.toBigDecimal(200);
		expected = new BigDecimal(200);
		Assert.assertEquals(expected, actual);
		
		System.out.println(expected);
	}

//	@Test
//	public void testToBigDecimalDouble() {
//		BigDecimal actual = Convert.toBigDecimal(0.23d);
//		BigDecimal expected = new BigDecimal(String.valueOf(0.23d));
//		Assert.assertEquals(expected, actual);
//		
//		actual = Convert.toBigDecimal(49009.01d);
//		expected = new BigDecimal(String.valueOf(49009.01d));
//		Assert.assertEquals(expected, actual);
//		
//		System.out.println(expected);
//	}

	@Test
	public void testToBigDecimalDoubleInt() {
		
		BigDecimal actual = Convert.toBigDecimal(1234.56789d, 3);
		BigDecimal expected = new BigDecimal("1234.568");
		Assert.assertEquals(expected, actual);
		
		actual = Convert.toBigDecimal(1234.12345d, 3);
		expected = new BigDecimal("1234.123");
		Assert.assertEquals(expected, actual);
		
		actual = Convert.toBigDecimal(1234d, 3);
		expected = new BigDecimal("1234.000");
		Assert.assertEquals(expected, actual);

		actual = Convert.toBigDecimal(1234.1d, 3);
		expected = new BigDecimal("1234.100");
		Assert.assertEquals(expected, actual);
		
		actual = Convert.toBigDecimal(1234.1234298579284375d, 3);
		expected = new BigDecimal("1234.123");
		Assert.assertEquals(expected, actual);

		actual = Convert.toBigDecimal(1234.1234298579284375d, 0);
		expected = new BigDecimal("1234");
		Assert.assertEquals(expected, actual);
		
		actual = Convert.toBigDecimal(1234.5234298579284375d, 0);
		expected = new BigDecimal("1235");
		Assert.assertEquals(expected, actual);
		
		actual = Convert.toBigDecimal(1234, 0);
		expected = new BigDecimal("1234");
		Assert.assertEquals(expected, actual);
		
	}
	
	@Test
	public void testToBigDecimalString() {
		BigDecimal actual = Convert.toBigDecimal("123.456");
		BigDecimal expected = new BigDecimal("123.456");
		Assert.assertEquals(expected, actual);
		
		actual = Convert.toBigDecimal("-123.456");
		expected = new BigDecimal("-123.456");
		Assert.assertEquals(expected, actual);
		
		System.out.println(expected);
		
	}
	
}
