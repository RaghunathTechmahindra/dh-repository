/*
 * @(#)ExpressCoreTestContext.java    2007-7-30
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.outbound;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.context.ExpressContext;
import com.filogix.express.context.ExpressServiceException;
import com.filogix.express.context.ExpressContextDirector;

import com.filogix.express.test.UnitTestDataRepository;

/**
 * ExpressCoreTestContext is a Express context for testing core module.  The
 * corresponded context xml file is
 * <code>com/filogix/express/dscu/expressDscuTestContext.xml</code>, which is
 * located in test folder.
 *
 */
public abstract class ExpressOutboundTestContext implements ExpressContext {

    // the key for the test context.
    private static final String EXPRESS_OUTBOUND_TEST_CONTEXT_KEY =
        "com.filogix.express.outboundtest";

    public static final String CONDITION_PROCESSOR_FACTORY_KEY =
        "ESBOUTBOUND_SERVICE_8";

    public static final String DEAL_PROCESSOR_FACTORY_KEY =
        "ESBOUTBOUND_SERVICE_9";

    // The logger
    private final static Log _log = 
        LogFactory.getLog(ExpressOutboundTestContext.class);

    /**
     * return a service instance from the test context.
     */
    synchronized public static Object getService(String name)
        throws ExpressServiceException {

        return ExpressContextDirector.getDirector().
            getServiceFactory(EXPRESS_OUTBOUND_TEST_CONTEXT_KEY).
            getService(name);
    }

}