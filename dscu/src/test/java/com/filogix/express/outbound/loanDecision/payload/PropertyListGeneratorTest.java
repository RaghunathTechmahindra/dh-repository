/*
 * @(#)PropertyListGeneratorTest.java     14-Aug-09
 * 
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.outbound.loanDecision.payload;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.EscrowPayment;
import com.basis100.deal.entity.Property;
import com.basis100.deal.pk.DealPK;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.core.ExpressRuntimeException;
import com.filogix.express.outbound.UnitTestHelper;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.PropertyType;
import com.filogix.schema.fcx._1.PropertyType.PropertyExpense;

/**
 * @author HKobayashi
 *
 */
public class PropertyListGeneratorTest {
	
	public static final double PROPERTY_EXPENSE_AMOUNT = 9876543;
	public static final int PROPERTY_EXPENSE_PERIOD_ID = 1;
	public static final int PROPERTY_EXPENSE_TYPD_ID = 0;
	private static final double DWELLING_TYPE_ID = 0;
	private static final int DWELLING_STYLE_ID = 4;
	private static final double DWELLING_STYLE_DD = 5;
	private static final double PROPERTY_TYPE_ID = 2;
	
	ObjectFactory factory;
	SessionResourceKit srk;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.factory = new ObjectFactory();
		this.srk = new SessionResourceKit();
		srk.getExpressState().cleanAllIds();
		srk.getExpressState().setDealInstitutionId(UnitTestHelper.getDefaultInstitution(srk));
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		this.srk.getExpressState().cleanAllIds();
		this.srk.freeResources();
	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.PropertyListGenerator#populatePropertyList(java.util.List, com.basis100.deal.entity.Deal, com.filogix.schema.fcx._1.ObjectFactory)}.
	 */
	@Test
	public void testPopulatePropertyList() throws Exception {

		try{
			srk.beginTransaction();
			Deal deal = UnitTestHelper.createDefaultDealTree(srk);
			Collection<Property> props = deal.getProperties();
			for(Property prop:props) {
				prop.setDwellingTypeId((int)DWELLING_TYPE_ID);
				prop.setDwellingStyleId(DWELLING_STYLE_ID);
				prop.setPropertyTypeId((int)PROPERTY_TYPE_ID);
				prop.setPrimaryPropertyFlag("Y");
				prop.ejbStore();
			}

			List<PropertyType> list = new ArrayList<PropertyType>();

			PropertyListGenerator.populatePropertyList(list,deal,factory);

			assertEquals(1, list.size());
			assertEquals(DWELLING_TYPE_ID, list.get(0).getDwellingTypeDd().doubleValue(), 0.0);
			assertEquals(DWELLING_STYLE_DD, list.get(0).getDwellingStyleDd().doubleValue(), 0.0);
			assertEquals(PROPERTY_TYPE_ID, list.get(0).getPropertyTypeDd().doubleValue(), 0.0);
			assertEquals("Y", list.get(0).getPrimaryPropertyFlag());
			assertEquals("Y", list.get(0).getSubjectPropertyFlag());
			
		}finally{
			srk.cleanTransaction();
		}

	}

	@Test
	public void testPopulatePropertyExpenseList() throws Exception{
		
		try{
			srk.beginTransaction();
			Deal deal = UnitTestHelper.createDefaultDealTree(srk);
			Property property = new Property(srk);
			property.create((DealPK) deal.getPk());
			property.setDealId(deal.getDealId());
			property.ejbStore();
			com.basis100.deal.entity.PropertyExpense pe1 = new com.basis100.deal.entity.PropertyExpense(srk, null);
			pe1.setPropertyId(property.getPropertyId());
			pe1.setPropertyExpenseAmount(PROPERTY_EXPENSE_AMOUNT);
			pe1.setPropertyExpenseId(PROPERTY_EXPENSE_TYPD_ID);
			pe1.setPropertyExpensePeriodId(PROPERTY_EXPENSE_PERIOD_ID);
			pe1.ejbStore();
			Collection<com.basis100.deal.entity.PropertyExpense> propExpenses = deal.getPropertyExpenses();
			
			int expected = 0;
			for(com.basis100.deal.entity.PropertyExpense propExp: propExpenses) {
				expected +=(propExp.getPropertyExpenseTypeId() == 0)?1:0;
			}
			
			List<PropertyExpense> list  = new ArrayList<PropertyExpense>();
	
			PropertyListGenerator.populatePropertyExpenseList(list, propExpenses, factory);
			assertEquals(expected, list.size());
			
			for(PropertyExpense propExpFcx: list) {
				if(propExpFcx.getPropertyExpenseAmount().doubleValue() == PROPERTY_EXPENSE_AMOUNT) {
					assertEquals(PROPERTY_EXPENSE_PERIOD_ID, propExpFcx.getPropertyExpensePeriodDd().doubleValue());
					assertEquals(PROPERTY_EXPENSE_TYPD_ID, propExpFcx.getPropertyExpenseTypeDd().doubleValue());
				}
			}

		} catch (Exception e) {
			srk.cleanTransaction();
		} finally {
			srk.cleanTransaction();
		}
		
	}

	@Test
	public void testCalculateAnnualEscrowPaymentAmount() throws Exception{
		try{
			srk.beginTransaction();
			EscrowPayment ep = new EscrowPayment(srk);
			ep.setEscrowPaymentAmount(100d);
	
			assertEquals(1200, PropertyListGenerator.calculateAnnualEscrowPaymentAmount(ep, 0), 0.000);
			assertEquals(2400, PropertyListGenerator.calculateAnnualEscrowPaymentAmount(ep, 1), 0.000);
			assertEquals(2600, PropertyListGenerator.calculateAnnualEscrowPaymentAmount(ep, 2), 0.000);
			assertEquals(2600, PropertyListGenerator.calculateAnnualEscrowPaymentAmount(ep, 3), 0.000);
			assertEquals(5200, PropertyListGenerator.calculateAnnualEscrowPaymentAmount(ep, 4), 0.000);
			assertEquals(5200, PropertyListGenerator.calculateAnnualEscrowPaymentAmount(ep, 5), 0.000);

			try{
				PropertyListGenerator.calculateAnnualEscrowPaymentAmount(ep, 6);
				fail("Runtime exception is required");
			}catch(ExpressRuntimeException ignore){}
			
			try{
				PropertyListGenerator.calculateAnnualEscrowPaymentAmount(null, 0);
				fail("Runtime exception is required");
			}catch(ExpressRuntimeException ignore){}
			
		}catch (Exception e) {
			fail("Exception during unit test:" + e.getMessage());
		} finally {
			srk.rollbackTransaction();
		}

		
	}

}
