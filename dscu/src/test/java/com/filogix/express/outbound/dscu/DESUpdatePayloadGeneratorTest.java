/**
 * <p>Title: .java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Ltd. Partnership. (c) 2009</p>
 *
 * <p>Company: Filogix Ltd. Partnership</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � 24-May-09
 *
 */
package com.filogix.express.outbound.dscu;

import java.io.StringReader;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.ValidationEventLocator;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEventStatusUpdateAssoc;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.DealEventStatusUpdateAssocPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.dscu.DESUpdatePayloadGenerator;
import com.filogix.express.outbound.dscu.DESUpdatePayloadGenerator.DEStatus;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Update;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Routing.Receiver;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Routing.Sender.Institution;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Routing.Sender.Lender;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Routing.Sender;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Routing;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Update.StatusInfo.Status;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Update.StatusInfo.StatusReplacement;
import com.filogix.schema.postoffice.des.LanguageSpecificText;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Update.StatusInfo;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Update.StatusInfo.DealEventStatusType;

public class DESUpdatePayloadGeneratorTest extends ExpressEntityTestCase 
    implements UnitTestLogging {
    
    private static final int DATA_COUNT_ROLLTYPE = 17;
    private static final int DATA_COUNT_DEAL = 2;
    private static final int DATA_COUNT_REQUESTSTATUS = 24;
    private static final int DATA_COUNT_DEALEVENTUPDATETYPE = 4;
    
    private static final int DESTATUS_TYPE_APPLICATION = 1;
    private static final int DESTATUS_TYPE_MI = 2;
    private static final int DESTATUS_TYPE_APPRASAL = 3;
    private static final int DESTATUS_TYPE_OSC = 4;

    private static int SERCICEPRODUCT_APP_FULL = 4;
    private static int SERCICEPRODUCT_APP_DRIVEBY = 5;
    private static int SERCICEPRODUCT_OSC_REF_FCT_LAWYER = 13;
    private static int SERCICEPRODUCT_OSC_REF_CLIENT_LAYWER = 14;
    private static int SERCICEPRODUCT_OSC_PUR_FCT_LAYWER = 15;
    private static int SERCICEPRODUCT_OSC_PUR_CLIENT_LAYWER = 16;
    private static int SERCICEPRODUCT_OSC_TRA_FUNDING = 17;
    private static int SERCICEPRODUCT_OSC_TRA_NOFUNDING = 18;
    private static int SERCICEPRODUCT_OSC_INREF_FUNDING = 19;
    private static int SERCICEPRODUCT_OSC_INREF_NOFUNDING = 20;
    private static int SERCICEPRODUCT_OSC_REF = 21;
    private static int SERCICEPRODUCT_OSC_LOAN = 22;
    private static int SERCICEPRODUCT_CREDIT = 23;
    

    DESUpdatePayloadGenerator payloadGenerator = null;
    
    static Short enShort = new Short(""+Mc.LANGUAGE_PREFERENCE_ENGLISH);
    static Short frShort = new Short(""+Mc.LANGUAGE_PREFERENCE_FRENCH);
    
     // The logger
    private final static Logger _logger = LoggerFactory.getLogger(DESUpdatePayloadGeneratorTest.class);
    private final static int INSTITUTIONPROFILEID = 0;
    // the session resource kit.
    private SessionResourceKit  _srk;

    
    int                         userTypeId;
    int                         instProfId;
    String                      descEN;
    String                      descFR;
    int                        dealId;
    int                        institutionProfileId;
    int                        copyId;
    int                        upId;
    Deal                       deal;
    int     testSeqForDeal = 0;
    UserProfile             up;
    Request appRequest;
    Request oscRequest;
    Collection<DEStatus> dess;
    
    @Before
    public void setup() throws Exception {
        ResourceManager.init();
        _srk = new SessionResourceKit();
        payloadGenerator = new DESUpdatePayloadGenerator();
        
        Double indexD = new Double(Math.random() * DATA_COUNT_DEAL);
        testSeqForDeal = indexD.intValue();
        
        deal = new Deal(_srk, null);
        dealId = _dataRepository.getInt("Deal", "DealId", 1);
        DealPK dpk = new DealPK(dealId, -1);
        int institutionId = _dataRepository.getInt("Deal", "institutionProfileId", 1);
        _srk.getExpressState().setDealInstitutionId(institutionId);
        deal = deal.findByRecommendedScenario(dpk, true);

        upId = deal.getUnderwriterUserId();
        up = new UserProfile(_srk);
        up = up.findByPrimaryKey(new UserProfileBeanPK(upId,institutionProfileId));

        // determin appraisal serviceProductId 4 or 5
        Double indexAPP = new Double(Math.random() * 2);
        int biasApp = indexAPP.intValue();
        //insert appraisal request
        appRequest = insertRequest(deal, up, SERCICEPRODUCT_APP_FULL + biasApp, testSeqForDeal);
        
        // determin appraisal serviceProductId 4 or 5
        Double indexOSC = new Double(Math.random() * 11);
        int biasOSC = indexOSC.intValue();
        //insert osc request
        oscRequest = insertRequest(deal, up, SERCICEPRODUCT_OSC_REF_FCT_LAWYER + biasOSC, testSeqForDeal);
        
        dess = createDESS(deal, appRequest, oscRequest, testSeqForDeal);
        
        assertNotNull(dess);
            
    }
        
    private Collection<DEStatus> createDESS(Deal deal, 
            Request appRequest, Request oscRequest, int dealIndex) {
        Collection<DEStatus> statuses = new ArrayList<DEStatus>();
        DealEventStatusUpdateAssoc assocDeal = null;
        try {
            assocDeal= new DealEventStatusUpdateAssoc(_srk);
            assocDeal = assocDeal.findByPrimaryKey(
                    new DealEventStatusUpdateAssocPK(deal.getSystemTypeId(), 1, deal.getStatusId()));
        } catch (Exception e) {
            ;
        }
        if(assocDeal != null) {
            DEStatus statusDeal = payloadGenerator.new DEStatus(deal, assocDeal, new Date());
            statuses.add(statusDeal);
        }
        
        DealEventStatusUpdateAssoc assocMI = null; 
        try {
            assocMI = new DealEventStatusUpdateAssoc(_srk);
            assocMI = assocMI.findByPrimaryKey(
                new DealEventStatusUpdateAssocPK(deal.getSystemTypeId(), 2, deal.getMIStatusId()));
        } catch (Exception e) {
            ;
        }
        if(assocMI != null) {
            DEStatus statusMI = payloadGenerator.new DEStatus(deal, assocMI, new Date());
            statuses.add(statusMI);
        }
        
        DealEventStatusUpdateAssoc assocAPP = null;
        if( appRequest != null ){
            try {
                assocAPP = new DealEventStatusUpdateAssoc(_srk);
                assocAPP = assocAPP.findByPrimaryKey(
                        new DealEventStatusUpdateAssocPK(deal.getSystemTypeId(), 3, appRequest.getRequestStatusId()));
            } catch (Exception e) {
                ;
            }
            if(assocAPP != null) {
                DEStatus statusApp = payloadGenerator.new DEStatus(deal, assocAPP, appRequest.getStatusDate());
                statuses.add(statusApp);
            }
        }
        
        DealEventStatusUpdateAssoc assocOSC = null;
        if(oscRequest!=null) {
            try {
                assocOSC = new DealEventStatusUpdateAssoc(_srk);
                assocOSC = assocOSC.findByPrimaryKey(
                        new DealEventStatusUpdateAssocPK(deal.getSystemTypeId(), 4, oscRequest.getRequestStatusId()));
            } catch (Exception e) {
                ;
            }
            if (assocOSC != null) {
                DEStatus statusOsc = payloadGenerator.new DEStatus(deal, assocOSC, oscRequest.getStatusDate());
                statuses.add(statusOsc);
            }
        }
        return statuses;
            
    }
        
    private Request insertRequest(Deal deal, UserProfile up, int serviceProductId, int dealIndex) {


        Request request = null;
        try {
            request = new Request(_srk);
       
        RequestPK requestPK = request.createPrimaryKey(deal.getCopyId());
        
        String sql = "INSERT INTO REQUEST( REQUESTID" 
                + ", REQUESTDATE"
                + ", DEALID, COPYID, SERVICEPRODUCTID"
                + ", REQUESTSTATUSID, STATUSDATE, USERPROFILEID, INSTITUTIONPROFILEID "
                + " ) VALUES ( " + requestPK.getId() + ", ";
        
        
        Date now = new Date();
        Date requestDate = new Date();
        requestDate.setDate(now.getDate() - 3);
        Date statusDate = new Date();
        statusDate.setDate(requestDate.getDate() + 1);
                    
        String requestDateString = TypeConverter.stringTypeFrom(requestDate);
        sql = sql + (requestDateString==null ? "null" : "to_date(\'" + requestDateString + "\', \'yyyy-mm-dd hh24:mi:ss\')")
            +  ", " + deal.getDealId() + ", " + deal.getCopyId()
            + ", " + serviceProductId;
        
        Double indexRS = new Double(Math.random() * DATA_COUNT_REQUESTSTATUS);
        int requestStatusId = indexRS.intValue();
        sql = sql + ", " + requestStatusId;
        
        String statusDateString = TypeConverter.stringTypeFrom(statusDate);
        sql = sql + ", " + (statusDate==null ? "null" : "to_date(\'" + statusDateString + "\', \'yyyy-mm-dd hh24:mi:ss\')")
            +  ", " + up.getUserProfileId() + ", " + deal.getInstitutionProfileId()
            +")";
            
            _dataRepository.executeSQL(sql);
            
            request = request.findByPrimaryKey(requestPK);
        } catch (Exception e) {
            ; 
        } finally {
            return request;
        }
    }
    
    @Test
    public void getDEStatusTypeTest() throws Exception {
        
        payloadGenerator.init(deal);
        payloadGenerator.setDEStatuses(dess);
        payloadGenerator.setUserProfileId(upId);
        String descEn = "";
        String descFr = "";
        if(dess != null) {
            Iterator deStatuses = dess.iterator();
            while (deStatuses.hasNext()) {
                DEStatus deStatus = (DEStatus)deStatuses.next();
                DealEventStatusType statusType = payloadGenerator.getDEStatusType(deStatus);
                assertEquals(statusType.getDealEventStatusTypeId(), deStatus.getDesAssoc().getDealEventStatusTypeId());
    
                descEn = BXResources.getPickListDescription(this.deal.getInstitutionProfileId(), 
                        "DEALEVENTSTATUSTYPE", deStatus.getDesAssoc().getDealEventStatusTypeId(), Mc.LANGUAGE_PREFERENCE_ENGLISH);
                descFr = BXResources.getPickListDescription(this.deal.getInstitutionProfileId(), 
                        "DEALEVENTSTATUSTYPE", deStatus.getDesAssoc().getDealEventStatusTypeId(), Mc.LANGUAGE_PREFERENCE_FRENCH);
                
                List<LanguageSpecificText> desc= statusType.getDealEventStatusTypeDescription();
                if (enShort.equals((Byte)(desc.get(0)).getLanguageId())){
                    assertEquals(descEn, (String)desc.get(2).getText());
                } else if (frShort.equals((Byte)(desc.get(0)).getLanguageId())) {
                    assertEquals(descFr, (String)desc.get(2).getText());
                }
                if (enShort.equals((Byte)(desc.get(0)).getLanguageId())){
                    assertEquals(descEn, (String)desc.get(2).getText());
                } else if (frShort.equals((Byte)(desc.get(0)).getLanguageId())){
                    assertEquals(descEn, (String)desc.get(2).getText());
                }
            }
        } else {
            _logger.info("getDEStatusTypeTest couldn't find for deal : " + deal.getDealId());
        }
    }
    
    @Test
    public void getStatusTest() throws Exception {
        
        payloadGenerator.init(deal);
        payloadGenerator.setDEStatuses(dess);
        payloadGenerator.setUserProfileId(upId);
        String descEn = "";
        String descFr = "";
        if(dess!=null) {
            Iterator deStatuses = dess.iterator();
            while (deStatuses.hasNext()) {
                DEStatus deStatus = (DEStatus)deStatuses.next();
                int statusTypeId = deStatus.getDesAssoc().getDealEventStatusTypeId();
                int statusId = deStatus.getDesAssoc().getStatusId();
                Status status = payloadGenerator.getStatus(deStatus);
                assertEquals(status.getStatusId(), new Short("" + statusId));
                
                switch (statusTypeId) {
                case DESTATUS_TYPE_APPLICATION:
                    descEn = BXResources.getPickListDescription(this.deal.getInstitutionProfileId(), 
                            "DEALSTATUS", statusId, Mc.LANGUAGE_PREFERENCE_ENGLISH);
                    descFr = BXResources.getPickListDescription(this.deal.getInstitutionProfileId(), 
                            "DEALSTATUS", statusId, Mc.LANGUAGE_PREFERENCE_FRENCH);
                    break;
                case DESTATUS_TYPE_MI:
                    descEn = BXResources.getPickListDescription(this.deal.getInstitutionProfileId(), 
                            "MISTATUS", statusId, Mc.LANGUAGE_PREFERENCE_ENGLISH);
                    descFr = BXResources.getPickListDescription(this.deal.getInstitutionProfileId(), 
                            "MISTATUS", statusId, Mc.LANGUAGE_PREFERENCE_FRENCH);
                    break;
                case DESTATUS_TYPE_APPRASAL:
                case DESTATUS_TYPE_OSC:
                    descEn = BXResources.getPickListDescription(this.deal.getInstitutionProfileId(), 
                            "REQUESTSTATUS", statusId, Mc.LANGUAGE_PREFERENCE_ENGLISH);
                    descFr = BXResources.getPickListDescription(this.deal.getInstitutionProfileId(), 
                            "REQUESTSTATUS", statusId, Mc.LANGUAGE_PREFERENCE_FRENCH);
                    break;
                } 
                
                List<LanguageSpecificText> desc= status.getStatusDescription();
                
                if (enShort.equals((Byte)(desc.get(0)).getLanguageId())){
                    assertEquals(descEn, (String)desc.get(2).getText());
                } else if (frShort.equals((Byte)(desc.get(0)).getLanguageId())){
                    assertEquals(descFr, (String)desc.get(2).getText());
                }
                if (enShort.equals((Byte)(desc.get(0)).getLanguageId())){
                    assertEquals(descEn, (String)desc.get(2).getText());
                } else if (frShort.equals((Byte)(desc.get(0)).getLanguageId())){
                    assertEquals(descFr, (String)desc.get(2).getText());
                }
            }
        } else {
            _logger.info("getStatusTest couldn't find for deal : " + deal.getDealId());
        }
    }

    @Test
    public void getStatusReplacementTest() throws Exception {
        
        payloadGenerator.init(deal);
        payloadGenerator.setDEStatuses(dess);
        payloadGenerator.setUserProfileId(upId);
        String descEn = "";
        String descFr = "";
        if(dess != null) {
            Iterator deStatuses = dess.iterator();
            while (deStatuses.hasNext()) {
                DEStatus deStatus = (DEStatus)deStatuses.next();
                int statusTypeId = deStatus.getDesAssoc().getDealEventStatusTypeId();
                int statusReplacementTypeId = deStatus.getDesAssoc().getStatusNameReplacementId();
                StatusReplacement statusReplacement = payloadGenerator.getStatusReplacement(deStatus);
                assertEquals(statusReplacement.getStatusReplacementId(), new String("" + statusReplacementTypeId));
                
                descEn = BXResources.getPickListDescription(this.deal.getInstitutionProfileId(), 
                        "STATUSNAMEREPLACEMENT", statusReplacementTypeId, Mc.LANGUAGE_PREFERENCE_ENGLISH);
                descFr = BXResources.getPickListDescription(this.deal.getInstitutionProfileId(), 
                        "STATUSNAMEREPLACEMENT", statusReplacementTypeId, Mc.LANGUAGE_PREFERENCE_FRENCH);
    
                List<LanguageSpecificText> desc = statusReplacement.getStatusReplacementDescription();
                if (enShort.equals((Byte)(desc.get(0)).getLanguageId())){
                    assertEquals(descEn, (String)desc.get(2).getText());
                } else if (frShort.equals((Byte)(desc.get(0)).getLanguageId())){
                    assertEquals(descFr, (String)desc.get(2).getText());
                }
                if (enShort.equals((Byte)(desc.get(0)).getLanguageId())){
                    assertEquals(descEn, (String)desc.get(2).getText());
                } else if (frShort.equals((Byte)(desc.get(0)).getLanguageId())){
                    assertEquals(descFr, (String)desc.get(2).getText());
                }
            }
        }
        else {
            _logger.info("getStatusReplacementTest couldn't find for deal : " + deal.getDealId());
        }
    }

    @Test
    public void getMIProviderNameTest() throws Exception {
        
        payloadGenerator.init(deal);
        payloadGenerator.setDEStatuses(dess);
        payloadGenerator.setUserProfileId(upId);
        String descEn = "";
        String descFr = "";
        if(dess != null) {
            Iterator deStatuses = dess.iterator();
            while (deStatuses.hasNext()) {
                DEStatus deStatus = (DEStatus)deStatuses.next();
                int statusTypeId = deStatus.getDesAssoc().getDealEventStatusTypeId();
                if (DESTATUS_TYPE_MI == statusTypeId) {
                    int miInsurerId = deStatus.getDeal().getMortgageInsurerId();
                    LanguageSpecificText miProviderName = payloadGenerator
                            .getMIProviderName(deStatus.getDeal(),
                                    Mc.LANGUAGE_PREFERENCE_ENGLISH);
                    assertNotNull(miProviderName);
                    
                    descEn = BXResources.getPickListDescription(this.deal.getInstitutionProfileId(), 
                            "MORTGAGEINSURER", miInsurerId, Mc.LANGUAGE_PREFERENCE_ENGLISH);
                    descFr = BXResources.getPickListDescription(this.deal.getInstitutionProfileId(), 
                            "MORTGAGEINSURER", miInsurerId, Mc.LANGUAGE_PREFERENCE_FRENCH);
                    
                    if (enShort.equals((Byte)(miProviderName.getLanguageId()))) {
                        assertEquals(descEn, (String)miProviderName.getText());
                    } else if (frShort.equals((Byte)(miProviderName.getLanguageId()))) {
                        assertEquals(descFr, (String)miProviderName.getText());
                    }
                    if (enShort.equals((Byte)(miProviderName.getLanguageId()))) {
                        assertEquals(descEn, (String)miProviderName.getText());
                    } else if (frShort.equals((Byte)(miProviderName.getLanguageId()))) {
                        assertEquals(descFr, (String)miProviderName.getText());
                    }
                }
            }
        } else {
            _logger.info("MIProviderNameTest couldn't find for deal : " + deal.getDealId());
        }
    }

    @Test
    public void getStatusInfoTest() throws Exception {
        
        payloadGenerator.init(deal);
        payloadGenerator.setDEStatuses(dess);
        payloadGenerator.setUserProfileId(upId);
        Collection<Short> statusTypeIds = new ArrayList();
        if(dess != null) {
            Iterator typsIds = dess.iterator();
            
            while (typsIds.hasNext()) {
                statusTypeIds.add(new Short("" + ((DEStatus)typsIds.next()).getDesAssoc().getDealEventStatusTypeId()));
            }
            
            Collection<StatusInfo> statusInfos = payloadGenerator.getStatusInfo();
            int size = dess.size();
            assertEquals(statusInfos.size(), dess.size());
            Iterator deStatusInfos = statusInfos.iterator();
            while (deStatusInfos.hasNext()) {
                
                StatusInfo statusType = (StatusInfo)deStatusInfos.next();
                assertTrue(statusTypeIds.contains(new Short("" + statusType.getDealEventStatusType().getDealEventStatusTypeId())));
            }
        } else {
            _logger.info("StatusInfoTest couldn't find for deal : " + deal.getDealId());
        }
    }
    
    @Test
    public void getUpdateTest() throws Exception {
      
      payloadGenerator.init(deal);
      payloadGenerator.setDEStatuses(dess);
      payloadGenerator.setUserProfileId(upId);
      Update update = payloadGenerator.getUpdate();
      assertNotNull(update);
      
    }

    @Test
    public void getSystemTypeTest() throws Exception {
        
        payloadGenerator.init(deal);
        payloadGenerator.setDEStatuses(dess);
        payloadGenerator.setUserProfileId(upId);
        com.filogix.schema.postoffice.des.DealEventStatusUpdate.Routing.Receiver.SystemType systemType 
            = payloadGenerator.getSystemType();
        
        int systemTypeId = deal.getSystemTypeId();
        String systemTypeDesc = BXResources.getPickListDescription(
                this.deal.getInstitutionProfileId(), "SYSTEMTYPE", systemTypeId, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        assertEquals(systemTypeId, systemType.getSystemTypeId());
        
    }
    
    @Test
    public void getReceiverTest() throws Exception {
        
        payloadGenerator.init(deal);
        payloadGenerator.setDEStatuses(dess);
        payloadGenerator.setUserProfileId(upId);
        Receiver receiver = payloadGenerator.getReceiver();
        
        assertEquals(deal.getSourceApplicationId(), receiver.getSourceApplicationId());
        assertEquals(deal.getSourceApplicationVersion(), receiver.getSourceApplicationVersion());
        assertNotNull(receiver.getSystemType());
    }
    
    @Test
    public void getInstitutionTest() throws Exception {
        
        payloadGenerator.init(deal);
        payloadGenerator.setDEStatuses(dess);
        payloadGenerator.setUserProfileId(upId);
        Institution institution = payloadGenerator.getInstitution();
        
        InstitutionProfile instp = new InstitutionProfile(_srk);
        instp = instp.findByPrimaryKey(new InstitutionProfilePK(deal.getInstitutionProfileId()));
        
        assertEquals(instp.getECNILenderId(), institution.getEcniLenderId());
        assertEquals(instp.getInstitutionName(), institution.getInstitutionName());
    }
        
    @Test
    public void getLenderTest() throws JAXBException  {
        
        payloadGenerator.init(deal);
        payloadGenerator.setDEStatuses(dess);
        payloadGenerator.setUserProfileId(upId);
        Lender lender = payloadGenerator.getLender();
        
        LenderProfile lenderP = null;
        try {
            lenderP = deal.getLenderProfile();
            assertEquals(lenderP.getLPBusinessId(), lender.getLpBusinessId());
            assertEquals(lenderP.getLenderName(), lender.getLenderName());
        } catch (Exception e) {
            assertNull(lenderP);
        }
    }

    @Test
    public void getSenderTest() throws Exception {
        
        payloadGenerator.init(deal);
        payloadGenerator.setDEStatuses(dess);
        payloadGenerator.setUserProfileId(upId);
     
        Sender sender = payloadGenerator.getSender();
        
        assertNotNull(sender.getLender());
        assertNotNull(sender.getInstitution());
        assertEquals(new Long(deal.getDealId()), sender.getDealId());

    }
    
    @Test
    public void getRoutingTest() throws Exception {
        
        payloadGenerator.init(deal);
        payloadGenerator.setDEStatuses(dess);
        payloadGenerator.setUserProfileId(upId);
     
        Routing routing = payloadGenerator.getRouting();
        
        assertNotNull(routing.getReceiver());
        assertNotNull(routing.getSender());
    }
    
    @Test
    public void getXmlPayloadTest() throws Exception {
      
      payloadGenerator.init(deal);
      payloadGenerator.setDEStatuses(dess);
      payloadGenerator.setUserProfileId(upId);
      String payload = payloadGenerator.getXmlPayload();
      System.out.println(payload);
      assertNotNull(payload);
    }
    
    @Test
    public void schemaValidationTest() throws Exception {
        
        DocumentTracking dt = new DocumentTracking(_srk);
        int dealId = 100319;
        Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
        payloadGenerator.init(deal);
        payloadGenerator.setDEStatuses(dess);
        payloadGenerator.setUserProfileId(upId);
        String payload = payloadGenerator.getXmlPayload();
        System.out.println(payload);
        validate(payload);
        
    }
    
    public void validate(String payloadXML) throws Exception {
        
        JAXBContext jc = JAXBContext.newInstance("com.filogix.schema.postoffice.des");

        // Validate the object.
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        SchemaFactory sf = SchemaFactory
            .newInstance(javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(ClassLoader
                .getSystemResource("com/filogix/express/dscu/DealEventStatusUpdate_1_1.xsd"));
        unmarshaller.setSchema(schema);
        
        final boolean[] hasError = new boolean[] { false }; 
        final Map<Integer, String> errorMap = new HashMap<Integer, String>();

        unmarshaller.setEventHandler(new ValidationEventHandler() {
            public boolean handleEvent(ValidationEvent ve) {
              if ((ve.getSeverity() == ValidationEvent.FATAL_ERROR)
                  || (ve.getSeverity() == ValidationEvent.ERROR)) {
                ValidationEventLocator locator = ve.getLocator();
     
                hasError[0] = true;
                errorMap.put(locator.getLineNumber(), ve.getMessage());
              }
     
              return true;
            }
          });

        Object jaxbObject = unmarshaller.unmarshal(new StringReader(payloadXML));

        if (hasError[0]) {
          System.out.println("INPUT XML IS NOT VALID:");
      
          String[] lines = payloadXML.split("\n");
          for (int cnt = 0; cnt < lines.length; cnt++) {
            System.out.println("L" + (cnt + 1) + ":" + lines[cnt]);
      
            if (errorMap.containsKey(cnt + 1)) {
              System.out.println("***ERROR " + errorMap.get(cnt + 1));
            }
          }
        } 
    }
    
    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        _srk.freeResources();
    }
    
}