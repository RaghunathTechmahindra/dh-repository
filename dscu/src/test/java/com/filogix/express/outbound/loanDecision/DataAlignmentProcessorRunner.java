/*
 * @(#)DataAlignmentProcessorRunner.java     19-Aug-09
 * 
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.outbound.loanDecision;

import org.junit.Ignore;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.pk.ESBOutboundQueuePK;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.ExpressOutboundContext;
import com.filogix.express.outbound.IOutboundProcessor;
import com.filogix.externallinks.framework.ServiceConst;

@Ignore
public class DataAlignmentProcessorRunner {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try{
			DataAlignmentProcessorRunner me = new DataAlignmentProcessorRunner();
			me.run();
		}catch(Throwable t){
			t.printStackTrace();
		}
	}

	void run() throws Exception {
		
		IOutboundProcessor proc = (IOutboundProcessor) ExpressOutboundContext
				.getService("ESBOUTBOUND_SERVICE_11");

		// dummy data
		SessionResourceKit srk = new SessionResourceKit();
		try{
			srk.getExpressState().setDealInstitutionId(0);
			srk.beginTransaction();
			Deal deal = new Deal(srk,null,100252,13);
			ESBOutboundQueue q = new ESBOutboundQueue(srk);
			ESBOutboundQueuePK pk = q.createPrimaryKey();
			q.create(pk, ServiceConst.SERVICE_TYPE_LOANDECISION_UPDATE, deal.getDealId(), deal.getSystemTypeId(), 0);
			q.setServiceSubTypeid(1); //commitment
			q.ejbStore();
			srk.commitTransaction();
			
			proc.setSessionResourceKit(srk);
			proc.handleProcess(q);
			
		}finally{
			srk.freeResources();
		}

	}

}
