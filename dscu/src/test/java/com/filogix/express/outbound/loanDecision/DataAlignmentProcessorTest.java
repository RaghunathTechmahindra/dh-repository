/*
 * @(#)DataAlignmentProcessorTest.java     18-Aug-09
 * 
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.outbound.loanDecision;

import static org.junit.Assert.*;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.deal.entity.ArchivedLoanDecision;
import com.basis100.deal.entity.Deal;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.UnitTestHelper;

/**
 * @author hkobayashi
 *
 */
public class DataAlignmentProcessorTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

//	/**
//	 * Test method for {@link com.filogix.express.outbound.loanDecision.DataAlignmentProcessor#handleProcess(java.lang.Object)}.
//	 */
//	@Test
//	public void testHandleProcess() {
//		fail("Not yet implemented");
//	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.DataAlignmentProcessor#archiveTransaction(com.basis100.deal.entity.Deal, java.lang.String)}.
	 * @throws Exception 
	 */
	@Test
	public void testArchiveTransaction() throws Exception {
		
		
		SessionResourceKit srk = new SessionResourceKit();
		srk.beginTransaction();
		String dummyXML = "<foo>aaaa</foo>";
		try{
			srk.getExpressState().setDealInstitutionId(UnitTestHelper.getDefaultInstitution(srk));
			Deal deal = UnitTestHelper.createDefaultDealTree(srk);
			
			DataAlignmentProcessor dap = new DataAlignmentProcessor();
			dap.setSessionResourceKit(srk);
			int id = dap.archiveTransaction(deal,dummyXML);
			System.out.println("==================" + id);
			ArchivedLoanDecision archive = new ArchivedLoanDecision(srk, id);
			assertEquals(dummyXML, archive.getFCXLoanDecision());
		}catch(Exception e){
			fail(e.getMessage());
			ExceptionUtils.getCause(e).printStackTrace();
		}finally{
			srk.cleanTransaction();
			srk.freeResources();
		}
	}




}
