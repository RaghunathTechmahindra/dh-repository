/*
 * @(#)ComponentsListGeneratorTest.java     31-Jul-09
 * 
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.outbound.loanDecision.payload;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.UnitTestHelper;
import com.filogix.schema.fcx._1.MortgageType;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.MortgageType.Component;
import com.filogix.schema.fcx._1.MortgageType.Component.ComponentCreditCard;
import com.filogix.schema.fcx._1.MortgageType.Component.ComponentLOC;
import com.filogix.schema.fcx._1.MortgageType.Component.ComponentLoan;
import com.filogix.schema.fcx._1.MortgageType.Component.ComponentMortgage;
import com.filogix.schema.fcx._1.MortgageType.Component.ComponentOverdraft;
import com.filogix.schema.fcx._1.MortgageType.Component.MtgProd;
import com.filogix.schema.fcx._1.MortgageType.Component.PricingRateInventory;

/**
 * @author HKobayashi
 *
 */
public class ComponentsListGeneratorTest {
	
	ObjectFactory factory;
	SessionResourceKit srk;
	
	//Test data to be used.
	double ADVANCE_HOLD = 2;
	String COMMISSION_CODE = "commission code";
	double DISCOUNT = 1;
	String EXISTING_ACCOUNT_INDICATOR = "No existing account";
	String EXISTING_ACCOUNT_NUMBER = "No existing number";
	Date FIRST_PAYMENT_DATE = new Date();
	Date INTEREST_ADJUSTMENT_DATE = new Date();
	double LOC_AMOUNT = 20000;
	String MI_ALLOCATE_FLAG = "Y";
	double NET_INTEREST_RATE = 7;
	double P_AND_I_PAYMENT_AMOUNT = 1000;
	double P_AND_I_PAYMENT_AMOUNT_MONTHLY = 50;
	int PAYMENT_FREQUENCY_ID = 1;
	double PREMIUM = 2;
	int PREPAYMENT_OPTIONS_ID = 5;
	int PRIVILEGE_PAYMENT_ID = 1;
	String PROPERTY_TAX_ALLOCATE_FLAG = "Y";
	double PROPERTY_TAX_ESCROW_AMOUNT = 500; 
	double TOTAL_LOC_AMOUNT = 20000;
	
	int ACTUAL_PAYMENT_TERM = 10;
	int IAD_NUMBER_OF_DAYS = 20;
	double INTEREST_ADJUSTMENT_AMOUNT = 2;
	double LOAN_AMOUNT = 10000;
	Date MATURITY_DATE = new Date();
	double PER_DIEM_INTEREST_AMOUNT = 2;
	
	double ADDITIONAL_PRINCIPAL = 50000;
	int AMORTIZATION_TERM = 35;
	double BALANCE_REMAINING_AT_END_OF_TERM = 300000;
	double BUY_DOWN_RATE = 5;
	double CASH_BACK_AMOUNT = 0;
	String CASH_BACK_AMOUNT_OVERRIDE = "N";
	double CASH_BACK_PERCENT = 0;
	int EFFECTIVE_AMORTIZATION_MONTHS = 60;
	Date FIRST_PAYMENT_DATE_MONTHLY = new Date();

	double MORTGAGE_AMOUNT = 20000;
	int RATE_GUARANTEE_PERIOD = 60;
	double TOTAL_MORTGAGE_AMOUNT = 20000;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.factory = new ObjectFactory();
		this.srk = new SessionResourceKit();
		srk.getExpressState().setDealInstitutionId(UnitTestHelper.getDefaultInstitution(srk));
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		this.srk.freeResources();
	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.ComponentsListGenerator#createComponentsList(java.util.List, com.basis100.deal.entity.Deal, com.filogix.schema.fcx._1.ObjectFactory)}.
	 * @throws JdbcTransactionException 
	 */
	@Test
	public void testCreateComponentsList() throws JdbcTransactionException {
		try{
			srk.beginTransaction();
			
			MasterDeal md = MasterDeal.createDefaultTree(srk, null);
			Deal deal = new Deal(srk, null, md.getDealId(), 1);
						
			com.basis100.deal.entity.Component component_creator = new com.basis100.deal.entity.Component(srk);
			com.basis100.deal.entity.Component component_type_mortgage = component_creator.
			create(deal.getDealId(), 1, Mc.COMPONENT_TYPE_MORTGAGE, deal.getMtgProdId());
			component_type_mortgage.ejbStore();
			
			com.basis100.deal.entity.ComponentMortgage componentMortgage_creator = new com.basis100.deal.entity.ComponentMortgage(srk);
			componentMortgage_creator.create(component_type_mortgage.getComponentId(), component_type_mortgage.getCopyId());
			componentMortgage_creator.ejbStore();
			deal.getComponents().add(component_type_mortgage);
			
			com.basis100.deal.entity.Component component_type_loc = component_creator.
			create(deal.getDealId(), 1, Mc.COMPONENT_TYPE_LOC, deal.getMtgProdId());
			component_type_loc.ejbStore();
			
			com.basis100.deal.entity.ComponentLOC componentLOC_creator = new com.basis100.deal.entity.ComponentLOC(srk);
			componentLOC_creator.create(component_type_loc.getComponentId(), component_type_loc.getCopyId());
			componentLOC_creator.ejbStore();
			deal.getComponents().add(component_type_loc);
			
			com.basis100.deal.entity.Component component_type_loan = component_creator.
			create(deal.getDealId(), 1, Mc.COMPONENT_TYPE_LOAN, deal.getMtgProdId());
			component_type_loan.ejbStore();
			
			com.basis100.deal.entity.ComponentLoan componentLoan_creator = new com.basis100.deal.entity.ComponentLoan(srk);
			componentLoan_creator.create(component_type_loan.getComponentId(), component_type_loan.getCopyId());
			componentLoan_creator.ejbStore();
			deal.getComponents().add(component_type_loan);
			
			com.basis100.deal.entity.Component component_type_creditCard = component_creator.
			create(deal.getDealId(), 1, Mc.COMPONENT_TYPE_CREDITCARD, deal.getMtgProdId());
			component_type_creditCard.ejbStore();
			
			com.basis100.deal.entity.ComponentCreditCard componentCreditCard_creator = new com.basis100.deal.entity.ComponentCreditCard(srk);
			componentCreditCard_creator.create(component_type_creditCard.getComponentId(), component_type_creditCard.getCopyId());
			componentCreditCard_creator.ejbStore();
			deal.getComponents().add(component_type_creditCard);
			
			com.basis100.deal.entity.Component component_type_overdraft = component_creator.
			create(deal.getDealId(), 1, Mc.COMPONENT_TYPE_OVERDRAFT, deal.getMtgProdId());
			component_type_overdraft.ejbStore();
			com.basis100.deal.entity.ComponentOverdraft componentOverdraft_creator = new com.basis100.deal.entity.ComponentOverdraft(srk);
			componentOverdraft_creator.create(component_type_overdraft.getComponentId(), component_type_overdraft.getCopyId());
			componentOverdraft_creator.ejbStore();
			deal.getComponents().add(component_type_overdraft);

			deal.ejbStore();

			MortgageType fcx_mortgageType = factory.createMortgageType();
			ComponentsListGenerator.createComponentsList(fcx_mortgageType.getComponent(), deal, factory);

			assertEquals(5, fcx_mortgageType.getComponent().size());
			
			boolean[] componentTypeFlags = {false, false, false, false, false};
			for(Component fcx_component: fcx_mortgageType.getComponent()) {
				switch (fcx_component.getComponentTypeDd().intValue()) {
					case Mc.COMPONENT_TYPE_MORTGAGE:
						componentTypeFlags[0] = true;
						break;
					case Mc.COMPONENT_TYPE_LOC:
						componentTypeFlags[1] = true;
						break;
					case Mc.COMPONENT_TYPE_LOAN:
						componentTypeFlags[2] = true;
						break;
					case Mc.COMPONENT_TYPE_CREDITCARD:
						componentTypeFlags[3] = true;
						break;
					case Mc.COMPONENT_TYPE_OVERDRAFT:
						componentTypeFlags[4] = true;
						break;
				}
			}

			for(boolean val: componentTypeFlags)	assertEquals(true, val);
		
		} catch (Exception e) {
			fail("Exception during unit test:" + e.getMessage());
		} finally {
			srk.rollbackTransaction();
		}
	}
	
	@Test
	public void testPopulateComponentCreditCard() throws RemoteException, FinderException, JdbcTransactionException {
		double CREDIT_CARD_AMOUNT = 5000;
		srk.beginTransaction();
		ComponentCreditCard fcx = factory.createMortgageTypeComponentComponentCreditCard();
		com.basis100.deal.entity.ComponentCreditCard db = new com.basis100.deal.entity.ComponentCreditCard(srk);
		
		db.setCreditCardAmount(CREDIT_CARD_AMOUNT);
		db.ejbStore();
		
		ComponentsListGenerator.populateComponentCreditCard(fcx, db);
		srk.rollbackTransaction();
		
		assertEquals(fcx.getCreditCardAmount(), Convert.toBigDecimal(CREDIT_CARD_AMOUNT));
		
	}
	
	@Test
	public void testPopulateComponentOverdraft() throws RemoteException, FinderException, JdbcTransactionException {
		srk.beginTransaction();
		double OVERDRAFT_AMOUNT = 1000;
		ComponentOverdraft fcx = factory.createMortgageTypeComponentComponentOverdraft();
		com.basis100.deal.entity.ComponentOverdraft db = new com.basis100.deal.entity.ComponentOverdraft(srk);
		
		db.setOverDraftAmount(OVERDRAFT_AMOUNT);
		db.ejbStore();
		
		ComponentsListGenerator.populateComponentOverdraft(fcx, db);
		srk.rollbackTransaction();
		assertEquals(fcx.getOverDraftAmount(), Convert.toBigDecimal(OVERDRAFT_AMOUNT));
	}
	
	@Test
	public void testPopulateComponentLOC() throws JdbcTransactionException, RemoteException, FinderException {
		


		srk.beginTransaction();
		
		ComponentLOC fcx = factory.createMortgageTypeComponentComponentLOC();
		com.basis100.deal.entity.ComponentLOC db = new com.basis100.deal.entity.ComponentLOC(srk);
		
		db.setAdvanceHold(ADVANCE_HOLD);
		db.setCommissionCode(COMMISSION_CODE);
		db.setDiscount(DISCOUNT);
		db.setExistingAccountIndicator(EXISTING_ACCOUNT_INDICATOR);
		db.setExistingAccountNumber(EXISTING_ACCOUNT_NUMBER);
		db.setFirstPaymentDate(FIRST_PAYMENT_DATE);
		db.setInterestAdjustmentDate(INTEREST_ADJUSTMENT_DATE);
		db.setLocAmount(LOC_AMOUNT);
		db.setMiAllocateFlag(MI_ALLOCATE_FLAG);
		db.setNetInterestRate(NET_INTEREST_RATE);
		db.setPAndIPaymentAmount(P_AND_I_PAYMENT_AMOUNT);
		db.setPAndIPaymentAmountMonthly(P_AND_I_PAYMENT_AMOUNT_MONTHLY);
		db.setPaymentFrequencyId(PAYMENT_FREQUENCY_ID);
		db.setPremium(PREMIUM);
		db.setPrePaymentOptionsId(PREPAYMENT_OPTIONS_ID);
		db.setPrivilegePaymentId(PRIVILEGE_PAYMENT_ID);
		db.setPropertyTaxAllocateFlag(PROPERTY_TAX_ALLOCATE_FLAG);
		db.setPropertyTaxEscrowAmount(PROPERTY_TAX_ESCROW_AMOUNT);
		db.setTotalLocAmount(TOTAL_LOC_AMOUNT);
		db.ejbStore();
		
		ComponentsListGenerator.populateComponentLOC(fcx, db);
		
		assertEquals(fcx.getAdvanceHold(), Convert.toBigDecimal(ADVANCE_HOLD));
		assertEquals(fcx.getCommissionCode(), (COMMISSION_CODE));
		assertEquals(fcx.getDiscount(), Convert.toBigDecimal(DISCOUNT));
		assertEquals(fcx.getExistingAccountIndicator(), EXISTING_ACCOUNT_INDICATOR);
		assertEquals(fcx.getExistingAccountNumber(), EXISTING_ACCOUNT_NUMBER);
		assertEquals(fcx.getFirstPaymentDate(), Convert.toXMLGregorianCalendar(FIRST_PAYMENT_DATE));
		assertEquals(fcx.getInterestAdjustmentDate(), Convert.toXMLGregorianCalendar(INTEREST_ADJUSTMENT_DATE));
		assertEquals(fcx.getLocAmount(), Convert.toBigDecimal(db.getLocAmount()));
		assertEquals(fcx.getMiAllocateFlag(), MI_ALLOCATE_FLAG);
		assertEquals(fcx.getNetInterestRate(), Convert.toBigDecimal(NET_INTEREST_RATE));
		assertEquals(fcx.getPAndIPaymentAmount(), Convert.toBigDecimal(P_AND_I_PAYMENT_AMOUNT));
		assertEquals(fcx.getPAndIPaymentAmountMonthly(), Convert.toBigDecimal(P_AND_I_PAYMENT_AMOUNT_MONTHLY));
		assertEquals(fcx.getPaymentFrequencyDd(), Convert.toBigDecimal(PAYMENT_FREQUENCY_ID));
		assertEquals(fcx.getPremium(), Convert.toBigDecimal(PREMIUM));
		assertEquals(fcx.getPrepaymentOptionsDd(), Convert.toBigDecimal(PREPAYMENT_OPTIONS_ID));
		assertEquals(fcx.getPrivilegePaymentDd(), Convert.toBigDecimal(PRIVILEGE_PAYMENT_ID));
		assertEquals(fcx.getPropertyTaxAllocateFlag(), PROPERTY_TAX_ALLOCATE_FLAG);
		assertEquals(fcx.getPropertyTaxEscrowAmount(), Convert.toBigDecimal(PROPERTY_TAX_ESCROW_AMOUNT));
		assertEquals(fcx.getTotalLOCAmount(), Convert.toBigDecimal(TOTAL_LOC_AMOUNT));
		
		srk.rollbackTransaction();
		
	}
	
	@Test
	public void testPopulateComponentLoan() throws JdbcTransactionException, RemoteException, FinderException {

		
		srk.beginTransaction();
		ComponentLoan fcx = factory.createMortgageTypeComponentComponentLoan();
		com.basis100.deal.entity.ComponentLoan db = new com.basis100.deal.entity.ComponentLoan(srk);
		
		db.setActualPaymentTerm(ACTUAL_PAYMENT_TERM);
		db.setDiscount(DISCOUNT);
		db.setFirstPaymentDate(FIRST_PAYMENT_DATE);
		db.setIadNumberOfDays(IAD_NUMBER_OF_DAYS);
		db.setInterestAdjustmentAmount(INTEREST_ADJUSTMENT_AMOUNT);
		db.setInterestAdjustmentDate(INTEREST_ADJUSTMENT_DATE);
		db.setLoanAmount(LOAN_AMOUNT);
		db.setMaturityDate(MATURITY_DATE);
		db.setNetInterestRate(NET_INTEREST_RATE);
		db.setPAndIPaymentAmount(P_AND_I_PAYMENT_AMOUNT);
		db.setPAndIPaymentAmountMonthly(P_AND_I_PAYMENT_AMOUNT_MONTHLY);
		db.setPaymentFrequencyId(PAYMENT_FREQUENCY_ID);
		db.setPerDiemInterestAmount(PER_DIEM_INTEREST_AMOUNT);
		db.setPremium(PREMIUM);
		db.ejbStore();
		
		ComponentsListGenerator.populateComponentLoan(fcx, db);
		srk.rollbackTransaction();
		
		assertEquals(fcx.getActualPaymentTerm(), Convert.toBigDecimal(ACTUAL_PAYMENT_TERM));
		assertEquals(fcx.getDiscount(), Convert.toBigDecimal(DISCOUNT));
		assertEquals(fcx.getFirstPaymentDate(), Convert.toXMLGregorianCalendar(FIRST_PAYMENT_DATE));
		assertEquals(fcx.getIadNumberOfDays(), Convert.toBigDecimal(IAD_NUMBER_OF_DAYS));
		assertEquals(fcx.getInterestAdjustmentAmount(), Convert.toBigDecimal(INTEREST_ADJUSTMENT_AMOUNT));
		assertEquals(fcx.getInterestAdjustmentDate(), Convert.toXMLGregorianCalendar(INTEREST_ADJUSTMENT_DATE));
		assertEquals(fcx.getLoanAmount(), Convert.toBigDecimal(LOAN_AMOUNT));
		assertEquals(fcx.getMaturityDate(), Convert.toXMLGregorianCalendar(MATURITY_DATE));
		assertEquals(fcx.getNetInterestRate(), Convert.toBigDecimal(NET_INTEREST_RATE));
		assertEquals(fcx.getPAndIPaymentAmount(), Convert.toBigDecimal(P_AND_I_PAYMENT_AMOUNT));
		assertEquals(fcx.getPAndIPaymentAmountMonthly(), Convert.toBigDecimal(P_AND_I_PAYMENT_AMOUNT_MONTHLY));
		assertEquals(fcx.getPaymentFrequencyDd(), Convert.toBigDecimal(PAYMENT_FREQUENCY_ID));
		assertEquals(fcx.getPerDiemInterestAmount(), Convert.toBigDecimal(PER_DIEM_INTEREST_AMOUNT));
		assertEquals(fcx.getPremium(), Convert.toBigDecimal(PREMIUM));

	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.ComponentsListGenerator#populateComponentMortgage(com.filogix.schema.fcx._1.MortgageType.Component.ComponentMortgage, com.basis100.deal.entity.Component, com.filogix.schema.fcx._1.ObjectFactory)}.
	 * @throws FinderException 
	 * @throws RemoteException 
	 * @throws JdbcTransactionException 
	 * @throws LoanDecisionPayloadGeneratorException 
	 */
	@Test
	public void testPopulateComponentMortgage() throws RemoteException, FinderException, JdbcTransactionException {
		

		
		srk.beginTransaction();
		
		ComponentMortgage fcx = factory.createMortgageTypeComponentComponentMortgage();
		com.basis100.deal.entity.ComponentMortgage db = new com.basis100.deal.entity.ComponentMortgage();
		
		db.setAdditionalPrincipal(ADDITIONAL_PRINCIPAL);
		db.setAdvanceHold(ADVANCE_HOLD);
		db.setActualPaymentTerm(ACTUAL_PAYMENT_TERM);
		db.setAmortizationTerm(AMORTIZATION_TERM);
		db.setBalanceRemainingAtEndOfTerm(BALANCE_REMAINING_AT_END_OF_TERM);
		db.setBuyDownRate(BUY_DOWN_RATE);
		db.setCashBackAmount(CASH_BACK_AMOUNT);
		db.setCashBackAmountOverride(CASH_BACK_AMOUNT_OVERRIDE);
		db.setCashBackPercent(CASH_BACK_PERCENT);
		db.setCommissionCode(COMMISSION_CODE);
		db.setDiscount(DISCOUNT);
		db.setEffectiveAmortizationMonths(EFFECTIVE_AMORTIZATION_MONTHS);
		db.setExistingAccountIndicator(EXISTING_ACCOUNT_INDICATOR);
		db.setExistingAccountNumber(EXISTING_ACCOUNT_NUMBER);
		db.setFirstPaymentDate(FIRST_PAYMENT_DATE);
		db.setFirstPaymentDateMonthly(FIRST_PAYMENT_DATE_MONTHLY);
		db.setIadNumberOfDays(IAD_NUMBER_OF_DAYS);
		db.setInterestAdjustmentAmount(INTEREST_ADJUSTMENT_AMOUNT);
		db.setInterestAdjustmentDate(INTEREST_ADJUSTMENT_DATE);
		db.setMaturityDate(MATURITY_DATE);
		db.setMiAllocateFlag(MI_ALLOCATE_FLAG);
		db.setMortgageAmount(MORTGAGE_AMOUNT);
		db.setNetInterestRate(NET_INTEREST_RATE);
		db.setPAndIPaymentAmount(P_AND_I_PAYMENT_AMOUNT);
		db.setPAndIPaymentAmountMonthly(P_AND_I_PAYMENT_AMOUNT_MONTHLY);
		db.setPaymentFrequencyId(PAYMENT_FREQUENCY_ID);
		db.setPerDiemInterestAmount(PER_DIEM_INTEREST_AMOUNT);
		db.setPremium(PREMIUM);
		db.setPrePaymentOptionsId(PREPAYMENT_OPTIONS_ID);
		db.setPrivilegePaymentId(PRIVILEGE_PAYMENT_ID);
		db.setPropertyTaxAllocateFlag(PROPERTY_TAX_ALLOCATE_FLAG);
		db.setPropertyTaxEscrowAmount(PROPERTY_TAX_ESCROW_AMOUNT);
		db.setRateGuaranteePeriod(RATE_GUARANTEE_PERIOD);
		db.setTotalMortgageAmount(TOTAL_MORTGAGE_AMOUNT);
		db.ejbStore();
		
		ComponentsListGenerator.populateComponentMortgage(fcx, db);
		
		srk.rollbackTransaction();
		
		assertEquals(fcx.getAdditionalPrincipal(), Convert.toBigDecimal(ADDITIONAL_PRINCIPAL));
		assertEquals(fcx.getAdvanceHold(), Convert.toBigDecimal(ADVANCE_HOLD));
		assertEquals(fcx.getActualPaymentTerm(), Convert.toBigDecimal(ACTUAL_PAYMENT_TERM));
		assertEquals(fcx.getAmortizationTerm(), Convert.toBigDecimal(AMORTIZATION_TERM));
		assertEquals(fcx.getBalanceRemainingAtEndOfTerm(), Convert.toBigDecimal(BALANCE_REMAINING_AT_END_OF_TERM));
		assertEquals(fcx.getBuyDownRate(), Convert.toBigDecimal(BUY_DOWN_RATE));
		assertEquals(fcx.getCashBackAmount(), Convert.toBigDecimal(CASH_BACK_AMOUNT));
		assertEquals(fcx.getCashBackAmountOverride(), CASH_BACK_AMOUNT_OVERRIDE);
		assertEquals(fcx.getCashBackPercent(), Convert.toBigDecimal(CASH_BACK_PERCENT));
		assertEquals(fcx.getCommissionCode(), COMMISSION_CODE);
		assertEquals(fcx.getDiscount(), Convert.toBigDecimal(DISCOUNT));
		assertEquals(fcx.getEffectiveAmortizationMonths().intValue(), EFFECTIVE_AMORTIZATION_MONTHS);
		assertEquals(fcx.getExistingAccountIndicator(), EXISTING_ACCOUNT_INDICATOR);
		assertEquals(fcx.getExistingAccountNumber(), EXISTING_ACCOUNT_NUMBER);
		assertEquals(fcx.getFirstPaymentDate(), Convert.toXMLGregorianCalendar(FIRST_PAYMENT_DATE));
		assertEquals(fcx.getFirstPaymentDateMonthly(), Convert.toXMLGregorianCalendar(FIRST_PAYMENT_DATE_MONTHLY));
		assertEquals(fcx.getIadNumberOfDays(), Convert.toBigDecimal(IAD_NUMBER_OF_DAYS));
		assertEquals(fcx.getInterestAdjustmentAmount(), Convert.toBigDecimal(INTEREST_ADJUSTMENT_AMOUNT));
		assertEquals(fcx.getInterestAdjustmentDate(), Convert.toXMLGregorianCalendar(INTEREST_ADJUSTMENT_DATE));
		assertEquals(fcx.getMaturityDate(), Convert.toXMLGregorianCalendar(MATURITY_DATE));
		assertEquals(fcx.getMiAllocateFlag(), MI_ALLOCATE_FLAG);
		assertEquals(fcx.getMortgageAmount(), Convert.toBigDecimal(MORTGAGE_AMOUNT));
		assertEquals(fcx.getNetInterestRate(), Convert.toBigDecimal(NET_INTEREST_RATE));
		assertEquals(fcx.getPAndIPaymentAmount(), Convert.toBigDecimal(P_AND_I_PAYMENT_AMOUNT));
		assertEquals(fcx.getPAndIPaymentAmountMonthly(), Convert.toBigDecimal(P_AND_I_PAYMENT_AMOUNT_MONTHLY));
		assertEquals(fcx.getPaymentFrequencyDd(), Convert.toBigDecimal(PAYMENT_FREQUENCY_ID));
		assertEquals(fcx.getPerDiemInterestAmount(), Convert.toBigDecimal(PER_DIEM_INTEREST_AMOUNT));
		assertEquals(fcx.getPremium(), Convert.toBigDecimal(PREMIUM));
		assertEquals(fcx.getPrepaymentOptionsDd(), Convert.toBigDecimal(PREPAYMENT_OPTIONS_ID));
		assertEquals(fcx.getPrivilegePaymentDd(), Convert.toBigDecimal(PRIVILEGE_PAYMENT_ID));
		assertEquals(fcx.getPropertyTaxAllocateFlag(), PROPERTY_TAX_ALLOCATE_FLAG);
		assertEquals(fcx.getPropertyTaxEscrowAmount(), Convert.toBigDecimal(PROPERTY_TAX_ESCROW_AMOUNT));
		assertEquals(fcx.getRateGuaranteePeriod(), Convert.toBigDecimal(RATE_GUARANTEE_PERIOD));
		assertEquals(fcx.getTotalMortgageAmount(), Convert.toBigDecimal(TOTAL_MORTGAGE_AMOUNT));
		
	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.ComponentsListGenerator#populatePricingRateInventory(com.filogix.schema.fcx._1.MortgageType.Component.PricingRateInventory, com.basis100.deal.entity.Component, com.filogix.schema.fcx._1.ObjectFactory)}.
	 * @throws FinderException 
	 * @throws RemoteException 
	 * @throws JdbcTransactionException 
	 */
	@Test
	public void testPopulatePricingRateInventory() throws RemoteException, FinderException, JdbcTransactionException {
		Date INDEX_EFFECTIVE_DATE = new Date();
		
		PricingRateInventory fcx = factory.createMortgageTypeComponentPricingRateInventory();
		srk.beginTransaction();
		com.basis100.deal.entity.PricingRateInventory db = new com.basis100.deal.entity.PricingRateInventory(srk);
		db.setIndexEffectiveDate(INDEX_EFFECTIVE_DATE);
		db.ejbStore();
		
		ComponentsListGenerator.populatePricingRateInventory(fcx, db);
		
		srk.rollbackTransaction();
		
		assertEquals(fcx.getIndexEffectiveDate(), Convert.toXMLGregorianCalendar(INDEX_EFFECTIVE_DATE));
		
	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.ComponentsListGenerator#populateComponent(com.filogix.schema.fcx._1.MortgageType.Component, com.basis100.deal.entity.Deal, com.basis100.deal.entity.Component, com.filogix.schema.fcx._1.ObjectFactory)}.
	 * @throws JdbcTransactionException 
	 */
	@Test
	public void testPopulateComponent() throws RemoteException, FinderException, JdbcTransactionException {
		String ADDITIONAL_INFO = "Test Additional Info";
		int COMPONENT_TYPE_ID = Mc.COMPONENT_TYPE_CREDITCARD;
		Date FIRST_PAYMENT_DATE_LIMIT = new Date();
		double POSTED_RATE = 5;
		int REPAYMENT_TYPE_ID = Mc.REPAYMENT_AT_MATURITY;
		Date TEASER_MATURITY_DATE = new Date();
		double TEASER_NET_INTEREST_RATE = 3;
		double TEASER_PI_AMOUNT = 0;
		double TEASER_RATE_INTEREST_SAVING = 0;
		
		Component fcx = factory.createMortgageTypeComponent();
		srk.beginTransaction();
		com.basis100.deal.entity.Component db = new com.basis100.deal.entity.Component(srk);
		db.setAdditionalInformation(ADDITIONAL_INFO);
		db.setComponentTypeId(COMPONENT_TYPE_ID);
		db.setFirstPaymentDateLimit(FIRST_PAYMENT_DATE_LIMIT);
		db.setPostedRate(POSTED_RATE);
		db.setRepaymentTypeId(REPAYMENT_TYPE_ID);
		db.setTeaserMaturityDate(TEASER_MATURITY_DATE);
		db.setTeaserNetInterestRate(TEASER_NET_INTEREST_RATE);
		db.setTeaserPiAmount(TEASER_PI_AMOUNT);
		db.setTeaserRateInterestSaving(TEASER_RATE_INTEREST_SAVING);
		db.ejbStore();
		
		ComponentsListGenerator.populateComponent(fcx, db);
		srk.rollbackTransaction();
		
		assertEquals(fcx.getAdditionalInformation(), ADDITIONAL_INFO);
		assertEquals(fcx.getComponentTypeDd(), Convert.toBigDecimal(COMPONENT_TYPE_ID));
		assertEquals(fcx.getFirstPaymentDateLimit(), 
				Convert.toXMLGregorianCalendar(FIRST_PAYMENT_DATE_LIMIT));
		assertEquals(fcx.getInterestRate(), Convert.toBigDecimal(POSTED_RATE));
		assertEquals(fcx.getRepaymentTypeDd(), Convert.toBigDecimal(REPAYMENT_TYPE_ID));
		assertEquals(fcx.getTeaserMaturityDate(), 
				Convert.toXMLGregorianCalendar(TEASER_MATURITY_DATE));
		assertEquals(fcx.getTeaserNetInterestRate(), Convert.toBigDecimal(TEASER_NET_INTEREST_RATE));
		assertEquals(fcx.getTeaserPIAmount(), Convert.toBigDecimal(TEASER_PI_AMOUNT));
		assertEquals(fcx.getTeaserRateInterestSaving(), Convert.toBigDecimal(TEASER_RATE_INTEREST_SAVING));
		
	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.ComponentsListGenerator#populateMtgProd(com.filogix.schema.fcx._1.MortgageType.Component.MtgProd, com.basis100.deal.entity.Deal, com.filogix.schema.fcx._1.ObjectFactory)}.
	 * @throws FinderException 
	 * @throws RemoteException 
	 * @throws JdbcTransactionException 
	 */
	@Test
	public void testPopulateMtgProd() throws RemoteException, FinderException, JdbcTransactionException {
		String MP_BUSINESS_ID = "123";
		MtgProd fcx = factory.createMortgageTypeComponentMtgProd();
		srk.beginTransaction();
		com.basis100.deal.entity.MtgProd db = new com.basis100.deal.entity.MtgProd(srk, null);
		db.setMPBusinessId(MP_BUSINESS_ID);
		db.ejbStore();
		
		srk.rollbackTransaction();
		
		ComponentsListGenerator.populateMtgProd(fcx, db);
		
		assertEquals(fcx.getMpBusinessId(), MP_BUSINESS_ID);
	}

}
