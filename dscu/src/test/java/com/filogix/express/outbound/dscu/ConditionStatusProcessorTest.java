/**
 * 
 */
package com.filogix.express.outbound.dscu;

import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;

import com.basis100.deal.docprep.DocPrepException;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.ESBOutboundQueuePK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.ExpressOutboundTestContext;
import com.filogix.express.outbound.dscu.ConditionStatusProcessor;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.externallinks.framework.ServiceConst;

/**
 * 
 * ConditionStatusProcessorTest
 * 
 * @version 1.0 Jun 9, 2009
 */
public class ConditionStatusProcessorTest extends ExpressEntityTestCase {

    // The logger
    private final static Log _log = LogFactory
            .getLog(ConditionStatusProcessorTest.class);

    // the session resource kit.
    private SessionResourceKit srk;
    private int dealId;
    private int institutionId;
    private int queueId;
    
    private Deal deal;
    private ESBOutboundQueue statusEvent;

    private ConditionStatusProcessor conditionProcessor;

    /**
     * initialation
     * 
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        _log.info("ConditionStatusProcessor initialize...");
        ResourceManager.init();

        srk = new SessionResourceKit("Test");
        srk.beginTransaction();
        
        dealId = _dataRepository.getInt("Deal", "DealId", 1);
        institutionId = _dataRepository.getInt("Deal", "institutionProfileId", 1);
        
        deal = new Deal(srk, null);
        DealPK dpk = new DealPK(dealId, -1);
        deal = deal.findByRecommendedScenario(dpk, true);
        
        statusEvent = createESBOutboundQueue(deal);
        queueId = statusEvent.getESBOutboundQueueId();

        srk.getExpressState().setDealInstitutionId(institutionId);
        
        conditionProcessor =
            (ConditionStatusProcessor) ExpressOutboundTestContext.
            getService(ExpressOutboundTestContext.
                    CONDITION_PROCESSOR_FACTORY_KEY);

        _log.info("ConditionStatusProcessorTest initialize... finished ");
    }

    /**
     * tear down
     * 
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        removeESBOutboundQueue(queueId);
        srk.commitTransaction();
        srk.freeResources();
        _log.info("ConditionStatusProcessorTest tearDown...");
    }

    /**
     * test handle process on processor
     * 
     * @throws Exception
     */
    @Test
    public final void testHandleProcess() throws Exception {

        _log.info("ConditionStatusProcessor initialize...");
        _log.info("dscu testing HandleProcess queueid: "
                + statusEvent.getESBOutboundQueueId());

        conditionProcessor.setSessionResourceKit(srk);
        conditionProcessor.handleProcess(statusEvent);
        _log.info("dscu testing HandleProcess finished: ");
    }

    /**
     * test handle result from post office
     */
//    @Test
    public void handlePostOfficeResultTest() {

    }
    
    /**
     * test send out email when connection error
     */
    @Test
    public void testSendEmailNotification() throws DocPrepException,
            FinderException, RemoteException {
        Random r = new Random(); 
        int code = r.nextInt();
        conditionProcessor.sendEmailNotification(deal, statusEvent, code);
        _log.info("ConditionStatusProcessor email text: ");

    }
    
    /**
     * test add dealhistory
     * @throws Exception 
     */
    @Test
    public void testAddDealHisotry() throws Exception  {
        String histroyMessage = BXResources.getSysMsg("CONDITION_DEALHISTORY",
                Mc.LANGUAGE_PREFERENCE_ENGLISH);
        conditionProcessor.addDealHisotry(deal, histroyMessage);
        _log.info("ConditionStatusProcessor dealhistory text: ");
    }
    
    /*
     * 
     */
    public ESBOutboundQueue createESBOutboundQueue(Deal deal) throws Exception {
        ESBOutboundQueue queue = new ESBOutboundQueue(srk);
        ESBOutboundQueuePK pk = queue.createPrimaryKey();
        int userId  = deal.getUnderwriterUserId();
        Random generator = new Random();
        int servicetypeId = generator.nextInt(4) + 1;
        queue.create(pk, servicetypeId, deal.getDealId(), 
                  deal.getSystemTypeId(), userId);
        queue.ejbStore();
        return queue;
    }
    
    /*
     * 
     */
    public void removeESBOutboundQueue(int queueId) throws Exception {
        ESBOutboundQueue queue = new ESBOutboundQueue(srk);  
        ESBOutboundQueuePK pk = new ESBOutboundQueuePK(queueId);
        queue.findByPrimaryKey(pk);
        queue.ejbRemove();
    }
}
