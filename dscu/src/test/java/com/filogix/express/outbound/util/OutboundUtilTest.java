/**
 * 
 */
package com.filogix.express.outbound.util;

import static org.junit.Assert.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.deal.entity.ProcessStatus;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.pk.ESBOutboundQueuePK;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.test.ExpressEntityTestCase;

/**
 * 
 */
public class OutboundUtilTest extends ExpressEntityTestCase{

    private final static Log _log = LogFactory
            .getLog(OutboundUtilTest.class);

    // the session resource kit.
    private SessionResourceKit _srk;

    private ESBOutboundQueue statusItem;
    private int dealId;
    private int institutionId;
    private int queueId;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        _log.info("ConditionStatusProcessor initialize...");
        ResourceManager.init();
        _srk = new SessionResourceKit("Test");
        queueId = _dataRepository.getInt("ESBOutboundQueue",
                "ESBOutboundQueueId", 1);
        dealId = _dataRepository.getInt("ESBOutboundQueue",
                "DealId", 1);
        institutionId = _dataRepository.getInt("ESBOutboundQueue",
                "institutionProfileId", 1);
        _srk.getExpressState().setDealInstitutionId(institutionId);
        statusItem = new ESBOutboundQueue(_srk);
        ESBOutboundQueuePK spk = new ESBOutboundQueuePK(queueId);
        statusItem.findByPrimaryKey(spk);
        _log.info("ConditionStatusProcessorTest initialize... finished ");
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        _srk.freeResources();
        _log.info("ConditionStatusProcessorTest tearDown...");
    }

    /**
     * Test method for testIncrementCounter
     * 
     * @throws Exception
     */
    @Test
    public void testIncrementCounter() throws Exception {
        _log.info("ConditionStatusProcessorTest testIncrementCounter...");
        int attemptCounter = 5;
        int step = 1;
        int previousCounter = statusItem.getAttemptCounter();
        
        OutboundUtil.incrementCounter(statusItem, step, _srk,
                attemptCounter);
        if(statusItem.getAttemptCounter() >= attemptCounter)
            assertEquals(statusItem.getAttemptCounter(), attemptCounter);
        else 
            assertEquals(statusItem.getAttemptCounter(), previousCounter + step);
    }

    /**
     * Test method for testChangeProcessStatus
     * 
     * @throws Exception
     */
    @Test
    public void testChangeProcessStatus() throws Exception {
        _log.info("ConditionStatusProcessorTest testChangeProcessStatus...");
        int newStatus = ProcessStatus.AVAILABLE;
        OutboundUtil.changeProcessStatus(statusItem, newStatus, "AVAILABLE",  _srk);
        assertEquals(statusItem.getTransactionStatusId(), newStatus);
    }

}
