/*
 * @(#)UnitTestHelper.java     13-Aug-09
 * 
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.outbound;

import org.apache.commons.lang.exception.ExceptionUtils;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;

public abstract class UnitTestHelper {

	public static int getDefaultInstitution(SessionResourceKit srk) {
		int ret = 0;
		try {
			InstitutionProfile ip = new InstitutionProfile(srk);
			ret = ip.findByFirst().getInstitutionProfileId();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(ExceptionUtils.getFullStackTrace(e));
			throw new RuntimeException(e);
		}
		return ret;
	}

	public static Deal createDefaultDealTree(SessionResourceKit srk)
			throws JdbcTransactionException {

		Deal ret = null;

		if (srk.isInTransaction() == false)
			throw new RuntimeException("use bigin transaction");

		try {
			MasterDeal md = MasterDeal.createDefaultTree(srk, null);
			ret = new Deal(srk, null, md.getDealId(), 1);

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		return ret;

	}

}
