package com.filogix.express.outbound.dscu;

import java.io.StringReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.ValidationEventLocator;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.entity.LenderToBranchAssoc;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.BranchProfilePK;
import com.basis100.deal.pk.ContactPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.deal.pk.LenderProfilePK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.dscu.ConditionUpdatePayloadGenerator;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Update.Condition.ConditionType;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Update.Condition.ConditionResponsibilityRole;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Update;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Routing.Receiver.SystemType;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Routing.Receiver;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Routing.Sender.Institution;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Routing.Sender.Lender;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Routing.Sender;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Routing;
import com.filogix.schema.postoffice.LanguageSpecificText;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Update.Condition.DocumentStatus;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Update.LenderContact;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Update.LenderContact.ContactRole;

public class ConditionUpdatePayloadGeneratorTest extends ExpressEntityTestCase 
	implements UnitTestLogging {

    private static final int DATA_COUNT_ROLLTYPE = 17;
    private static final int DATA_COUNT_DEAL = 5;
	ConditionUpdatePayloadGenerator payloadGenerator = null;
	
    static Short enShort = new Short(""+Mc.LANGUAGE_PREFERENCE_ENGLISH);
    static Short frShort = new Short(""+Mc.LANGUAGE_PREFERENCE_FRENCH);
	
	 // The logger
    private final static Logger _logger = LoggerFactory.getLogger(ConditionUpdatePayloadGeneratorTest.class);
    private final static int INSTITUTIONPROFILEID = 0;
    // the session resource kit.
    private SessionResourceKit  _srk;

    int                         userTypeId;
    int                         instProfId;
    String                      descEN;
    String                      descFR;
	int                        dealId;
	int                        institutionProfileId;
	int                        upId;
	Deal                       deal;
	UserProfile                up;

	@Before
	public void setup() throws Exception {
	    
        ResourceManager.init();
        _srk = new SessionResourceKit();
        payloadGenerator = new ConditionUpdatePayloadGenerator();
        Collection<DocumentTracking> dts = null;
        
        dts = setupRandom(dts);
		
        upId = deal.getUnderwriterUserId();
        up = new UserProfile(_srk);
        up = up.findByPrimaryKey(new UserProfileBeanPK(upId,institutionProfileId));


	}
	
	public Collection<DocumentTracking> setupRandom(Collection<DocumentTracking> dts) throws Exception {
        String propertyConfig = PropertiesCache.getInstance().getProperty(-1,
                "statusUpdate.conditions.include.condition.types", "");
        if (propertyConfig.length()==0) return null;
        
        DocumentTracking dt = new DocumentTracking(_srk);
        for(int count = 0; count < DATA_COUNT_DEAL; count++) {
	        Double indexD = new Double(Math.random() * DATA_COUNT_DEAL);
	        int i = indexD.intValue();
            dealId  = _dataRepository.getInt("DEAL", "DealId", i);
            institutionProfileId  = _dataRepository.getInt("DEAL", "InstitutionProfileId", i);
            int copyId  = _dataRepository.getInt("DEAL", "CopdId", i);
            _srk.getExpressState().setDealIds(dealId, institutionProfileId, copyId);
            try {
                deal = new Deal(_srk, null);
                deal.setSilentMode(true);
                deal = deal.findByRecommendedScenario(new DealPK(dealId, copyId), true);
                if (deal.getSystemTypeId() == 8)
                    dts = dt.findForConditionUpdate((DealPK)deal.getPk());
            } catch (Exception e) {
                System.out.println("couldn't find deal Entity dealid=" + dealId);
            }
		}
        return dts;
	}
	
	public void setupSeq(Collection<DocumentTracking> dts) throws Exception {
	    
        DocumentTracking dt = new DocumentTracking(_srk);        
        for (int i=0; i<DATA_COUNT_DEAL; i++ ){
            dealId  = _dataRepository.getInt("DEAL", "DealId", i);
            institutionProfileId  = _dataRepository.getInt("DEAL", "InstitutionProfileId", i);
            int copyId  = _dataRepository.getInt("DEAL", "CopdId", i);
            _srk.getExpressState().setDealIds(dealId, institutionProfileId, copyId);
            try {
                deal = new Deal(_srk, null);
                deal.setSilentMode(true);
                deal = deal.findByRecommendedScenario(new DealPK(dealId, copyId), true);
                if (deal.getSystemTypeId() == 8)
                    dts = dt.findForConditionUpdate((DealPK)deal.getPk());
                if (dts!=null && dts.size() > 0) 
                    i = DATA_COUNT_DEAL;
            } catch (Exception e) {
                System.out.println("couldn't find deal Entity dealid=" + dealId);
            }
        }
	}

	@After
    public void tearDown() {

      //      free resources
      _srk.freeResources( );

    }
    
//    @Test
    public void getRollNameTest() throws Exception {
		
        DocumentTracking dt = new DocumentTracking(_srk);
        Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
        
        payloadGenerator.init(deal);
        payloadGenerator.setConditions(dts);
        payloadGenerator.setUserProfileId(upId);
		
        for (int i = 0; i<DATA_COUNT_ROLLTYPE; i++) {
            userTypeId = _dataRepository.getInt("USERTYPE", "USERTYPEID", i);
            if (userTypeId < 20) {
                descEN = BXResources.getPickListDescription(INSTITUTIONPROFILEID,
                		"USERTYPE", userTypeId, Mc.LANGUAGE_PREFERENCE_ENGLISH);
                
                descFR = BXResources.getPickListDescription(INSTITUTIONPROFILEID,
                		"USERTYPE", userTypeId, Mc.LANGUAGE_PREFERENCE_FRENCH);
                
                LanguageSpecificText rollName = payloadGenerator.getRoleName(
                        userTypeId, Mc.LANGUAGE_PREFERENCE_ENGLISH);
                if (rollName.getText() != null ) {
                    Byte languageId = (Byte)(rollName.getLanguageId());
                    if (enShort.equals(languageId)) {
                        assertEquals( descEN, (String)rollName.getText());
                    } else if (frShort.equals(languageId)){
                        assertEquals( descFR, (String)rollName.getText());                    
                    }
                }
                if (rollName.getText() != null ) {
                    Byte languageId = (Byte)(rollName.getLanguageId());
                    if (enShort.equals(languageId)) {
                        assertEquals( descEN, (String)rollName.getText());
                    } else if (frShort.equals(languageId)){
                        assertEquals( descFR, (String)rollName.getText());                    
                    }
                }
            }
        }
    }
	
//  @Test
    public void getContactRoleTest() throws Exception {
      
        DocumentTracking dt = new DocumentTracking(_srk);
        Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
        
        payloadGenerator.init(deal);
        payloadGenerator.setConditions(dts);
        payloadGenerator.setUserProfileId(upId);
        
        ContactRole contactRoll = payloadGenerator.getContactRole(up);  
        
        assertEquals(up.getUserTypeId(), contactRoll.getRoleId());
      }
    
//  @Test
    public void getLenderContactTest() throws Exception {
      
        DocumentTracking dt = new DocumentTracking(_srk);
        Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
        Contact contact = new Contact(_srk);
        contact = contact.findByPrimaryKey(new ContactPK(up.getContactId(), 1)); 
        payloadGenerator.init(deal);
        payloadGenerator.setConditions(dts);
        payloadGenerator.setUserProfileId(upId);
        
        LenderContact lenderContact = payloadGenerator.getLenderContact();  
        
        assertEquals(contact.getContactEmailAddress(), lenderContact.getEmail());
        assertEquals(contact.getContactFirstName(), lenderContact.getFirstName());
        assertEquals(contact.getContactLastName(), lenderContact.getLastName());
        assertEquals(contact.getContactPhoneNumber() + " v " + contact.getContactPhoneNumberExtension(), 
                lenderContact.getPhone());
        assertEquals(contact.getContactFaxNumber(), lenderContact.getFax());
      }

//  @Test
    public void getTollFreeFaxTest() throws Exception {
      
        DocumentTracking dt = new DocumentTracking(_srk);
        Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
        Contact contact = new Contact(_srk);
        contact = contact.findByPrimaryKey(new ContactPK(up.getContactId(), 1)); 
        payloadGenerator.init(deal);
        payloadGenerator.setConditions(dts);
        payloadGenerator.setUserProfileId(upId);
        LenderToBranchAssoc lba = new LenderToBranchAssoc(_srk);
        Collection<LenderToBranchAssoc> lbac 
            = lba.findByBranchAndLender(new BranchProfilePK(deal.getBranchProfileId()), 
                                        new LenderProfilePK(deal.getLenderProfileId()));
        lba = (LenderToBranchAssoc)lbac.iterator().next();
        LanguageSpecificText tffax = payloadGenerator.getTollFreeFaxEn(lba);
        
        if (lba.getETFaxNumber()!= null ) {
            if (enShort.equals((Byte)tffax.getLanguageId())) {
                assertEquals(lba.getETFaxNumber(), (String)tffax.getText());                
            } else if (enShort.equals((Byte)tffax.getLanguageId())) {
                assertEquals(lba.getETFaxNumber(), (String)tffax.getText());
            }
        }
        if (lba.getFTFaxNumber()!= null ) {
            if (frShort.equals((Byte)tffax.getLanguageId())) {
                assertEquals(lba.getFTFaxNumber(), (String)tffax.getText());
            } else if (frShort.equals((Byte)tffax.getLanguageId())) {
                assertEquals(lba.getFTFaxNumber(), (String)tffax.getText());
            }
        }            
    }

//  @Test
    public void getTollFreePhoneTest() throws Exception {
      
        DocumentTracking dt = new DocumentTracking(_srk);
        Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
        Contact contact = new Contact(_srk);
        contact = contact.findByPrimaryKey(new ContactPK(up.getContactId(), 1)); 
        payloadGenerator.init(deal);
        payloadGenerator.setConditions(dts);
        payloadGenerator.setUserProfileId(upId);
        LenderToBranchAssoc lba = new LenderToBranchAssoc(_srk);
        Collection<LenderToBranchAssoc> lbac 
            = lba.findByBranchAndLender(new BranchProfilePK(deal.getBranchProfileId()), 
                                        new LenderProfilePK(deal.getLenderProfileId()));
        lba = (LenderToBranchAssoc)lbac.iterator().next();
        LanguageSpecificText tfPhone = payloadGenerator.getTollFreePhoneEn(lba);
        
        if (lba.getETPhoneNumber()!= null ) {
            if (enShort.equals((Byte)tfPhone.getLanguageId())) {
                assertEquals(lba.getETPhoneNumber(), (String)tfPhone.getText());                
            } else if (enShort.equals((Byte)tfPhone.getLanguageId())) {
                assertEquals(lba.getETPhoneNumber(), (String)tfPhone.getText());       
            }
        }
        if (lba.getETPhoneNumber()!= null ) {
            if (frShort.equals((Byte)tfPhone.getLanguageId())) {
                assertEquals(lba.getFTPhoneNumber(), (String)tfPhone.getText());
            } else if (frShort.equals((Byte)tfPhone.getLanguageId())) {
                assertEquals(lba.getFTPhoneNumber(), (String)tfPhone.getText());
            }
        }
    }
    
//  @Test
    public void getLocalFaxTest() throws Exception {
      
        DocumentTracking dt = new DocumentTracking(_srk);
        Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
        Contact contact = new Contact(_srk);
        contact = contact.findByPrimaryKey(new ContactPK(up.getContactId(), 1)); 
        payloadGenerator.init(deal);
        payloadGenerator.setConditions(dts);
        payloadGenerator.setUserProfileId(upId);
        LenderToBranchAssoc lba = new LenderToBranchAssoc(_srk);
        Collection<LenderToBranchAssoc> lbac 
            = lba.findByBranchAndLender(new BranchProfilePK(deal.getBranchProfileId()), 
                                        new LenderProfilePK(deal.getLenderProfileId()));
        lba = (LenderToBranchAssoc)lbac.iterator().next();
        LanguageSpecificText lFax = payloadGenerator.getLocalFaxEn(lba);
        
        if (lba.getELFaxNumber() != null ) {
            if (enShort.equals((Byte)lFax.getLanguageId())) {
                assertEquals(lba.getELFaxNumber(), (String)lFax.getText());                
            } else if (enShort.equals((String)lFax.getText())) {
                assertEquals(lba.getELFaxNumber(), (String)lFax.getText());
            }            
        }
        if (lba.getFLFaxNumber() != null ) {
            if (frShort.equals((Byte)lFax.getLanguageId())) {
                assertEquals(lba.getFLFaxNumber(), (String)lFax.getText());                
            } else if (frShort.equals((Byte)lFax.getLanguageId())){
                assertEquals(lba.getFLFaxNumber(), (String)lFax.getText());
            }            
        }
    }
    
//    @Test
    public void getLocalPhoneTest() throws Exception {
      
        DocumentTracking dt = new DocumentTracking(_srk);
        Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
        Contact contact = new Contact(_srk);
        contact = contact.findByPrimaryKey(new ContactPK(up.getContactId(), 1)); 
        payloadGenerator.init(deal);
        payloadGenerator.setConditions(dts);
        payloadGenerator.setUserProfileId(upId);
        LenderToBranchAssoc lba = new LenderToBranchAssoc(_srk);
        Collection<LenderToBranchAssoc> lbac 
            = lba.findByBranchAndLender(new BranchProfilePK(deal.getBranchProfileId()), 
                                        new LenderProfilePK(deal.getLenderProfileId()));
        lba = (LenderToBranchAssoc)lbac.iterator().next();
        LanguageSpecificText lphone = payloadGenerator.getLocalPhoneEn(lba);
        if (lba.getELPhoneNumber() != null) {
            if (enShort.equals((Byte)lphone.getLanguageId())) {
                assertEquals(lba.getELPhoneNumber(), (String)lphone.getText());                
            } else if (enShort.equals((Byte)lphone.getLanguageId())){
                assertEquals(lba.getELPhoneNumber(), (String)lphone.getText());
            }            
        }
        if (lba.getELPhoneNumber() != null) {
            if (frShort.equals((Byte)lphone.getLanguageId())) {
                assertEquals(lba.getFLPhoneNumber(), (String)lphone.getText());
            } else if (frShort.equals((Byte)lphone.getLanguageId())){
                assertEquals(lba.getFLPhoneNumber(), (String)lphone.getText());             
            }                        
        }
    }

//    @Test
    public void getMetDaysNoteTest() throws Exception {
      
        DocumentTracking dt = new DocumentTracking(_srk);
        Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
        payloadGenerator.init(deal);
        payloadGenerator.setConditions(dts);
        payloadGenerator.setUserProfileId(upId);
        String daysStr = "5"; // this should be from sysproeprty
        LanguageSpecificText metDaysNotes = payloadGenerator.getMetDaysNote(daysStr, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        
        String noteEn = BXResources.getSysMsg("CONDITION_MET_DAYS_NOTE", Mc.LANGUAGE_PREFERENCE_ENGLISH);
        noteEn = noteEn.replaceAll("%s", daysStr);
        if (enShort.equals((Byte)(metDaysNotes.getLanguageId()))) {
            assertEquals(noteEn, (String)metDaysNotes.getText());                
        } else if (enShort.equals((Byte)(metDaysNotes.getLanguageId()))){
            assertEquals(noteEn, (String)metDaysNotes.getText());
        }            

        String noteFr = BXResources.getSysMsg("CONDITION_MET_DAYS_NOTE", Mc.LANGUAGE_PREFERENCE_FRENCH);
        noteFr = noteFr.replaceAll("%s", daysStr);
        if (frShort.equals((Byte)(metDaysNotes.getLanguageId()))) {
            assertEquals(noteFr, (String)metDaysNotes.getText());                
        } else if (frShort.equals((Byte)(metDaysNotes.getLanguageId()))) {
            assertEquals(noteFr, (String)metDaysNotes.getText());
        }
    }
    
//    @Test
    public void getDocumentStatusTest() throws Exception {
      
        DocumentTracking dt = new DocumentTracking(_srk);
        Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
        payloadGenerator.init(deal);
        payloadGenerator.setConditions(dts);
        payloadGenerator.setUserProfileId(upId);
        Iterator<DocumentTracking> iter = dts.iterator();
        DocumentStatus docStatus = null;
        String descEn = "";
        String descFr = "";
        while (iter.hasNext()) {
            dt = (DocumentTracking)iter.next();
            
            docStatus = payloadGenerator.getDocumentStatus(dt);
            descEn = BXResources.getPickListDescription(this.deal.getInstitutionProfileId(), 
                    "DOCUMENTSTATUS", dt.getDocumentStatusId(), Mc.LANGUAGE_PREFERENCE_ENGLISH);
            descFr = BXResources.getPickListDescription(this.deal.getInstitutionProfileId(), 
                    "DOCUMENTSTATUS", dt.getDocumentStatusId(), Mc.LANGUAGE_PREFERENCE_FRENCH);
            
            
            List<LanguageSpecificText> desc = docStatus.getDocumentStatusDescription();
            if (enShort.equals((Byte)(desc.get(0)).getLanguageId())){
                assertEquals(descEn, (String)desc.get(1).getText());
            } else if (frShort.equals((Byte)(desc.get(0)).getLanguageId())){
                assertEquals(descFr, (String)desc.get(1).getText());
            }
            if (enShort.equals((Byte)(desc.get(2)).getLanguageId())){
                assertEquals(descEn, (String)desc.get(3).getText());
            } else if (frShort.equals((Byte)(desc.get(2)).getLanguageId())){
                assertEquals(descFr, (String)desc.get(3).getText());
            }
        }
    }
    
//    @Test
    public void getConditionTypeTest() throws Exception {
        DocumentTracking dt = new DocumentTracking(_srk);
        Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
        payloadGenerator.init(deal);
        payloadGenerator.setConditions(dts);
        payloadGenerator.setUserProfileId(upId);
        Iterator<DocumentTracking> iter = dts.iterator();
        DocumentStatus docStatus = null;
        String descEn = "";
        String descFr = "";
        while (iter.hasNext()) {
            dt = (DocumentTracking)iter.next();
            Condition c = new Condition(_srk, dt.getConditionId());
            descEn = BXResources.getPickListDescription(this.deal.getInstitutionProfileId(), 
                    "CONDITIONTYPE", c.getConditionTypeId(), Mc.LANGUAGE_PREFERENCE_ENGLISH);
            descFr = BXResources.getPickListDescription(this.deal.getInstitutionProfileId(), 
                    "CONDITIONTYPE", c.getConditionTypeId(), Mc.LANGUAGE_PREFERENCE_FRENCH);
            ConditionType conditionType = payloadGenerator.getConditionType(c);
            List<LanguageSpecificText> desc = conditionType.getConditionTypeDescription();
            
            if (enShort.equals((Byte)desc.get(0).getLanguageId())){
                assertEquals(descEn, (String)desc.get(1).getText());
            } else if (frShort.equals((Byte)desc.get(0).getLanguageId())){
                assertEquals(descFr, (String)desc.get(1).getText());
            }
            if (enShort.equals((Byte)desc.get(2).getLanguageId())){
                assertEquals(descEn, (String)desc.get(3).getText());
            } else if (frShort.equals((Byte)desc.get(2).getLanguageId())){
                assertEquals(descFr, (String)desc.get(2).getText());
            }
        }
    }
    
//    @Test
    public void getConditionTextTest() throws Exception {
        
      DocumentTracking dt = new DocumentTracking(_srk);
      Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
      payloadGenerator.init(deal);
      payloadGenerator.setConditions(dts);
      payloadGenerator.setUserProfileId(upId);
      Iterator<DocumentTracking> iter = dts.iterator();
      while (iter.hasNext()) {
          dt = (DocumentTracking)iter.next();
          LanguageSpecificText text = payloadGenerator.getConditionText(dt, Mc.LANGUAGE_PREFERENCE_ENGLISH);
          if (enShort.equals((Byte)(text.getLanguageId()))) {
              assertEquals(dt.getDocumentTextByLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH), text.getLanguageName());
          } else if (frShort.equals((Byte)(text.getLanguageId()))) {
              assertEquals(dt.getDocumentTextByLanguage(Mc.LANGUAGE_PREFERENCE_FRENCH), text.getLanguageName());              
          }
          if (enShort.equals((Byte)(text.getLanguageId()))){
              assertEquals(dt.getDocumentTextByLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH), text.getLanguageName());
          } else if (frShort.equals((Byte)(text.getLanguageId()))) {
              assertEquals(dt.getDocumentTextByLanguage(Mc.LANGUAGE_PREFERENCE_FRENCH), text.getLanguageName());              
          }
      }      
    }
  
//    @Test
    public void getConditionResponsibilityRoleTest() throws Exception {
        
      DocumentTracking dt = new DocumentTracking(_srk);
      Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
      payloadGenerator.init(deal);
      payloadGenerator.setConditions(dts);
      payloadGenerator.setUserProfileId(upId);
      Iterator<DocumentTracking> iter = dts.iterator();
      while (iter.hasNext()) {
          dt = (DocumentTracking)iter.next();
          Condition c = new Condition(_srk, dt.getConditionId());
          ConditionResponsibilityRole role = payloadGenerator.getConditionResponsibilityRole(c);
          assertEquals(c.getConditionResponsibilityRoleId(), role.getConditionResponsibilityRoleId());
          List<LanguageSpecificText> desc = role.getConditionResponsibilityRoleDescription();
          if (enShort.equals((Byte)(desc.get(0)).getLanguageId())){
              assertEquals(dt.getDocumentTextByLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH), (String)desc.get(1).getLanguageName());
          } else if (frShort.equals((Byte)(desc.get(0)).getLanguageId())){
              assertEquals(dt.getDocumentTextByLanguage(Mc.LANGUAGE_PREFERENCE_FRENCH), (String)desc.get(1).getLanguageName());              
          }
          if (enShort.equals((Byte)(desc.get(2)).getLanguageId())){
              assertEquals(dt.getDocumentTextByLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH), (String)desc.get(3).getLanguageName());
          } else if (frShort.equals((Byte)(desc.get(2)).getLanguageId())){
              assertEquals(dt.getDocumentTextByLanguage(Mc.LANGUAGE_PREFERENCE_FRENCH), (String)desc.get(3).getLanguageName());              
          }
      }
   }
    
//    @Test
    public void getConditionLabelTest() throws Exception {
        
      DocumentTracking dt = new DocumentTracking(_srk);
      Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
      payloadGenerator.init(deal);
      payloadGenerator.setConditions(dts);
      payloadGenerator.setUserProfileId(upId);
      Iterator<DocumentTracking> iter = dts.iterator();
      while (iter.hasNext()) {
          dt = (DocumentTracking)iter.next();
          LanguageSpecificText label = payloadGenerator.getConditionLabel(dt, Mc.LANGUAGE_PREFERENCE_ENGLISH);
          if (enShort.equals((Byte)(label.getLanguageId()))) {
              assertEquals(dt.getDocumentLabelByLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH), (String)label.getLanguageName());
          } else if (frShort.equals((Byte)(label.getLanguageId()))) {
              assertEquals(dt.getDocumentLabelByLanguage(Mc.LANGUAGE_PREFERENCE_FRENCH), (String)label.getLanguageName());              
          }
          if (enShort.equals((Byte)(label.getLanguageId()))) {
              assertEquals(dt.getDocumentLabelByLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH), (String)label.getText());
          } else if (frShort.equals((Byte)(label.getLanguageId()))) {
              assertEquals(dt.getDocumentLabelByLanguage(Mc.LANGUAGE_PREFERENCE_FRENCH), (String)label.getText());              
          }          
      }
    }
    
//    @Test
    public void getConditionTest() throws Exception {
        
        DocumentTracking dt = new DocumentTracking(_srk);
        Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
        payloadGenerator.init(deal);
        payloadGenerator.setConditions(dts);
        payloadGenerator.setUserProfileId(upId);

        Collection<com.filogix.schema.postoffice.ConditionStatusUpdate.Update.Condition> payloadCondtion 
            = payloadGenerator.getConditions();
        assertEquals(dts.size(), payloadCondtion.size());
    }
    
//    @Test
    public void getUpdateTest() throws Exception {
        
        DocumentTracking dt = new DocumentTracking(_srk);
        Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
        payloadGenerator.init(deal);
        payloadGenerator.setConditions(dts);
        payloadGenerator.setUserProfileId(upId);

        Update update = payloadGenerator.getUpdate();
        
        assertNotNull(update.getCondition());
        assertNotNull(update.getLenderBranchAddress());
        assertNotNull(update.getLenderContact());
        assertNotNull(update.getMetDaysNote());        
    }
    
//    @Test
    public void getSystemTypeTest() throws Exception {
        DocumentTracking dt = new DocumentTracking(_srk);
        Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
        payloadGenerator.init(deal);
        payloadGenerator.setConditions(dts);
        payloadGenerator.setUserProfileId(upId);
        int stid = deal.getSystemTypeId();
        com.basis100.deal.entity.SystemType st = new com.basis100.deal.entity.SystemType(_srk, stid);
        SystemType type = payloadGenerator.getSystemType();
        assertEquals( stid,  type.getSystemTypeId());
        assertEquals(st.getSystemTypeDescription(),  type.getSystemTypeDescription());
    }
    
//    @Test
    public void getReceiverTest() throws Exception {
      
      DocumentTracking dt = new DocumentTracking(_srk);
      Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
      payloadGenerator.init(deal);
      payloadGenerator.setConditions(dts);
      payloadGenerator.setUserProfileId(upId);

      Receiver receiver = payloadGenerator.getReceiver();
      assertEquals(deal.getSourceApplicationId(), receiver.getSourceApplicationId());
      assertEquals(deal.getSourceApplicationVersion(), receiver.getSourceApplicationVersion());
      assertNotNull(receiver.getSystemType());
      
    }
  
//    @Test
    public void getInstitutionTest()throws Exception  {
      
        DocumentTracking dt = new DocumentTracking(_srk);
        Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
        payloadGenerator.init(deal);
        payloadGenerator.setConditions(dts);
        payloadGenerator.setUserProfileId(upId);

        InstitutionProfile iProfile = new InstitutionProfile(_srk);
        iProfile = iProfile.findByPrimaryKey(new InstitutionProfilePK(deal.getInstitutionProfileId()));
        
        Institution inst = payloadGenerator.getInstitution();
        assertEquals(iProfile.getECNILenderId(), inst.getEcniLenderId());
        assertEquals(iProfile.getInstitutionName(), inst.getInstitutionName());
    }
    
//    @Test
    public void getLenderTest() throws Exception {
      
        DocumentTracking dt = new DocumentTracking(_srk);
        Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
        payloadGenerator.init(deal);
        payloadGenerator.setConditions(dts);
        payloadGenerator.setUserProfileId(upId);
        int lpid = deal.getLenderProfileId();
        LenderProfile lp = new LenderProfile(_srk, lpid);
        Lender lender = payloadGenerator.getLender();
        
        assertEquals(lp.getLPBusinessId(), lender.getLpBusinessId());
        assertEquals(lp.getLenderName(), lender.getLenderName());
    }
    
//    @Test
    public void getSenderTest() throws Exception {
      
        DocumentTracking dt = new DocumentTracking(_srk);
        Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
        payloadGenerator.init(deal);
        payloadGenerator.setConditions(dts);
        payloadGenerator.setUserProfileId(upId);
        
        Sender sender = payloadGenerator.getSender();
        assertEquals(new Long(""+deal.getDealId()), sender.getDealId());
        assertNotNull(sender.getInstitution());
        assertNotNull(sender.getLender());
    }
    
//    @Test
    public void getRoutingTest() throws Exception {
      
        DocumentTracking dt = new DocumentTracking(_srk);
        Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
        payloadGenerator.init(deal);
        payloadGenerator.setConditions(dts);
        payloadGenerator.setUserProfileId(upId);
        
        Routing routing = payloadGenerator.getRouting();
        assertNotNull(routing.getReceiver());
        assertNotNull(routing.getSender());
    }
    
    //@Test
    public void getXmlPayloadTest() throws Exception {
      
      DocumentTracking dt = new DocumentTracking(_srk);
      Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
      payloadGenerator.init(deal);
      payloadGenerator.setConditions(dts);
      payloadGenerator.setUserProfileId(upId);
      String payload = payloadGenerator.getXmlPayload();
      System.out.println(payload);
      assertNotNull(payload);
  }
    
  //@Test
    public void getUserTimeStampTest() throws Exception {
      
      DocumentTracking dt = new DocumentTracking(_srk);
      Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
      payloadGenerator.init(deal);
      payloadGenerator.setConditions(dts);
      payloadGenerator.setUserProfileId(upId);
      XMLGregorianCalendar calender = payloadGenerator.getUserTimeStamp(up);
      String payload = payloadGenerator.getXmlPayload();
      System.out.println(payload);
      assertNotNull(payload);
  }
  
  @Test
  public void schemaValidationTest() throws Exception {
      
      DocumentTracking dt = new DocumentTracking(_srk);
      Collection<DocumentTracking> dts = dt.findByDeal((DealPK)deal.getPk());
      payloadGenerator.init(deal);
      payloadGenerator.setConditions(dts);
      payloadGenerator.setUserProfileId(upId);
      String payload = payloadGenerator.getXmlPayload();
      _logger.debug("payload*****payload: " + payload);

      validate(payload);
      
  }
  
  public void validate(String payloadXML) throws Exception {
      
      JAXBContext jc = JAXBContext.newInstance("com.filogix.schema.postoffice");

      // Validate the object.
      Unmarshaller unmarshaller = jc.createUnmarshaller();
      SchemaFactory sf = SchemaFactory
          .newInstance(javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);
      Schema schema = sf.newSchema(ClassLoader
              .getSystemResource("com/filogix/express/dscu/CondtionStatusUpdate_1_1.xsd"));
      unmarshaller.setSchema(schema);
      
      final boolean[] hasError = new boolean[] { false }; 
      final Map<Integer, String> errorMap = new HashMap<Integer, String>();

      unmarshaller.setEventHandler(new ValidationEventHandler() {
          public boolean handleEvent(ValidationEvent ve) {
            if ((ve.getSeverity() == ValidationEvent.FATAL_ERROR)
                || (ve.getSeverity() == ValidationEvent.ERROR)) {
              ValidationEventLocator locator = ve.getLocator();
   
              hasError[0] = true;
              errorMap.put(locator.getLineNumber(), ve.getMessage());
            }
   
            return true;
          }
        });

      Object jaxbObject = unmarshaller.unmarshal(new StringReader(payloadXML));

      if (hasError[0]) {
        System.out.println("INPUT XML IS NOT VALID:");
    
        String[] lines = payloadXML.split("\n");
        for (int cnt = 0; cnt < lines.length; cnt++) {
          System.out.println("L" + (cnt + 1) + ":" + lines[cnt]);
    
          if (errorMap.containsKey(cnt + 1)) {
            System.out.println("***ERROR " + errorMap.get(cnt + 1));
          }
        }
      } 
  }
    
    
}