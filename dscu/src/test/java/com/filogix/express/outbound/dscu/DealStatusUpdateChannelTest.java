/**
 * <p>Title: .java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Ltd. Partnership. (c) 2009</p>
 *
 * <p>Company: Filogix Ltd. Partnership</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � 18-Jun-09
 *
 */
package com.filogix.express.outbound.dscu;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEventStatusUpdateAssoc;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.DealEventStatusUpdateAssocPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.dscu.DESUpdatePayloadGenerator;
import com.filogix.express.outbound.dscu.DESUpdatePayloadGenerator.DEStatus;
import com.filogix.express.test.ExpressEntityTestCase;
import com.filogix.express.test.UnitTestLogging;

public class DealStatusUpdateChannelTest extends ExpressEntityTestCase
        implements UnitTestLogging {

    // The logger
    private final static Logger _logger = LoggerFactory.getLogger(ConditionUpdateChannelTest.class);
    private SessionResourceKit  _srk;
    private static final int DATA_COUNT_RollType = 17;
    private static final int DATA_COUNT_DEAL = 500;
    private static final int DATA_COUNT_REQUESTSTATUS = 24;

    private static int SERCICEPRODUCT_APP_FULL = 4;
    private static int SERCICEPRODUCT_APP_DRIVEBY = 5;
    private static int SERCICEPRODUCT_OSC_REF_FCT_LAWYER = 13;
    private static int SERCICEPRODUCT_OSC_REF_CLIENT_LAYWER = 14;
    private static int SERCICEPRODUCT_OSC_PUR_FCT_LAYWER = 15;
    private static int SERCICEPRODUCT_OSC_PUR_CLIENT_LAYWER = 16;
    private static int SERCICEPRODUCT_OSC_TRA_FUNDING = 17;
    private static int SERCICEPRODUCT_OSC_TRA_NOFUNDING = 18;
    private static int SERCICEPRODUCT_OSC_INREF_FUNDING = 19;
    private static int SERCICEPRODUCT_OSC_INREF_NOFUNDING = 20;
    private static int SERCICEPRODUCT_OSC_REF = 21;
    private static int SERCICEPRODUCT_OSC_LOAN = 22;
    private static int SERCICEPRODUCT_CREDIT = 23;

    DealEventUpdateChannel channel = null;
    int testSeqForDeal;
    int                         userTypeId;
    int                         instProfId;
    String                      descEN;
    String                      descFR;
    int                        dealId;
    int                        institutionProfileId;
    int                        upId;
    Deal                       deal;
    int                        copyId;
    UserProfile                up;
    String                      payload;
    Request appRequest;
    Request oscRequest;
    Collection<DEStatus> dess;
    
    public DESUpdatePayloadGenerator payloadGenerator = null;
    
    @Before
    public void setup() throws Exception {
        
        ResourceManager.init();
        _srk = new SessionResourceKit();
    
        payloadGenerator = new DESUpdatePayloadGenerator();
        channel = new DealEventUpdateChannel();
    
        ResourceManager.init();
        _srk = new SessionResourceKit();
        payloadGenerator = new DESUpdatePayloadGenerator();
        
        Double indexD = new Double(Math.random() * DATA_COUNT_DEAL);
        testSeqForDeal = indexD.intValue();

        
        deal = getDealbySystemTypeId(8);

        upId = deal.getUnderwriterUserId();
        up = new UserProfile(_srk);
        up = up.findByPrimaryKey(new UserProfileBeanPK(upId,institutionProfileId));

        // determin appraisal serviceProductId 4 or 5
        Double indexAPP = new Double(Math.random() * 2);
        int biasApp = indexAPP.intValue();
        //insert appraisal request
        appRequest = insertRequest(deal, up, SERCICEPRODUCT_APP_FULL + biasApp, testSeqForDeal);
        
        // determin appraisal serviceProductId 4 or 5
        Double indexOSC = new Double(Math.random() * 11);
        int biasOSC = indexOSC.intValue();
        //insert osc request
        oscRequest = insertRequest(deal, up, SERCICEPRODUCT_OSC_REF_FCT_LAWYER + biasOSC, testSeqForDeal);
        
        dess = createDESS(deal, appRequest, oscRequest, testSeqForDeal);
        
        assertNotNull(dess);

    }
    
    private Deal getDealbySystemTypeId(int systemTypeId) throws Exception{
                    
        String sql = "Select d.* from deal d where systemTypeId=" + systemTypeId 
            + " and rownum < " + testSeqForDeal + " order by rownum desc"; 
        
        ResultSet rs = _dataRepository.executeSQL(sql);
        rs.next();
        dealId = rs.getInt("DealId");
        copyId = rs.getInt("CopyId");
        institutionProfileId = rs.getInt("InstitutionProfileId");
        _srk.getExpressState().setDealIds(dealId, institutionProfileId, copyId);
        Deal deal = new Deal(_srk, null);
        deal.setSilentMode(true);
        deal = deal.findByPrimaryKey(new DealPK(dealId, copyId));
        return deal;

    }
    
    private Request insertRequest(Deal deal, UserProfile up, int serviceProductId, int dealIndex) {
    
        Request request = null;
        try {
            request = new Request(_srk);
            RequestPK requestPK = request.createPrimaryKey(deal.getCopyId());
            
            String sql = "INSERT INTO REQUEST( REQUESTID" 
                    + ", REQUESTDATE"
                    + ", DEALID, COPYID, SERVICEPRODUCTID"
                    + ", REQUESTSTATUSID, STATUSDATE, USERPROFILEID, INSTITUTIONPROFILEID "
                    + " ) VALUES ( " + requestPK.getId() + ", ";
            
            
            Date now = new Date();
            Date requestDate = new Date();
            requestDate.setDate(now.getDate() - 3);
            Date statusDate = new Date();
            statusDate.setDate(requestDate.getDate() + 1);
                        
            String requestDateString = TypeConverter.stringTypeFrom(requestDate);
            sql = sql + (requestDateString==null ? "null" : "to_date(\'" + requestDateString + "\', \'yyyy-mm-dd hh24:mi:ss\')")
                +  ", " + deal.getDealId() + ", " + deal.getCopyId()
                + ", " + serviceProductId;
            
            Double indexRS = new Double(Math.random() * DATA_COUNT_REQUESTSTATUS);
            int requestStatusId = indexRS.intValue();
            sql = sql + ", " + requestStatusId;
            
            String statusDateString = TypeConverter.stringTypeFrom(statusDate);
            sql = sql + ", " + (statusDate==null ? "null" : "to_date(\'" + statusDateString + "\', \'yyyy-mm-dd hh24:mi:ss\')")
                +  ", " + up.getUserProfileId() + ", " + deal.getInstitutionProfileId()
                +")";
                
            _dataRepository.executeSQL(sql);
            
            request = request.findByPrimaryKey(requestPK);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return request;

    }
    
    @After
    public void tearDown() {

      //      free resources
      _srk.freeResources( );

    }

    //@Test
    public void sendTestDSU1() throws Exception {
                
        payloadGenerator.init(deal);
        payloadGenerator.setDEStatuses(dess);
        payloadGenerator.setUserProfileId(upId);

        payload = payloadGenerator.getXmlPayload();
        

        channel = new DealEventUpdateChannel();
        channel.setChannelId(18);
        channel.init(_srk, deal);
        Object o = channel.send(payload);
        assertNull(o);
    }

//    @Test
    public void sendTestDSU2() throws Exception {

        DESUpdatePayloadGenerator payloadGenerator = new DESUpdatePayloadGenerator();
        Collection<DEStatus> destatuses = new ArrayList();
        
        SessionResourceKit srk = deal.getSessionResourceKit();
        DealEventStatusUpdateAssoc updateAssoc = new DealEventStatusUpdateAssoc(srk);
        updateAssoc.setSilentMode(true);
        
        try {
            updateAssoc.findByPrimaryKey(new DealEventStatusUpdateAssocPK(deal
                    .getSystemTypeId(), DESUpdatePayloadGenerator.DESTATUS_TYPE_APPLICATION, deal.getStatusId()));
            DEStatus dealStatus = payloadGenerator.new DEStatus(deal, updateAssoc, new Date());
            destatuses.add(dealStatus);
        } catch (Exception e) {
            
        }
        
        try {
            updateAssoc.findByPrimaryKey(new DealEventStatusUpdateAssocPK(deal
                    .getSystemTypeId(), DESUpdatePayloadGenerator.DESTATUS_TYPE_MI, deal.getMIStatusId()));
            DEStatus miStatus = payloadGenerator.new DEStatus(deal, updateAssoc, new Date());
            destatuses.add(miStatus);
        } catch (Exception e) {
            
        }
        
        payloadGenerator.init(deal);
        payloadGenerator.setDEStatuses(destatuses);
        payloadGenerator.setUserProfileId(upId);

        payloadGenerator.setUserProfileId(upId);
        payload = payloadGenerator.getXmlPayload();
        

        channel = new DealEventUpdateChannel();
        channel.setChannelId(18);
        channel.init(_srk, deal);
        Object o = channel.send(payload);
        assertNull(o);
    }

    private Collection<DEStatus> createDESS(Deal deal, 
            Request appRequest, Request oscRequest, int dealIndex) {
        
        Collection<DEStatus> statuses = new ArrayList<DEStatus>();
        DealEventStatusUpdateAssoc assocDeal = null;
        try {
            assocDeal= new DealEventStatusUpdateAssoc(_srk);
            assocDeal = assocDeal.findByPrimaryKey(
                    new DealEventStatusUpdateAssocPK(deal.getSystemTypeId(), 1, deal.getStatusId()));
        } catch (Exception e) {
            ;
        }
        if(assocDeal != null) {
            DEStatus statusDeal = payloadGenerator.new DEStatus(deal, assocDeal, new Date());
            statuses.add(statusDeal);
        }
        
        DealEventStatusUpdateAssoc assocMI = null; 
        try {
            assocMI = new DealEventStatusUpdateAssoc(_srk);
            assocMI = assocMI.findByPrimaryKey(
                new DealEventStatusUpdateAssocPK(deal.getSystemTypeId(), 2, deal.getMIStatusId()));
        } catch (Exception e) {
            ;
        }
        if(assocMI != null) {
            DEStatus statusMI = payloadGenerator.new DEStatus(deal, assocMI, new Date());
            statuses.add(statusMI);
        }
        
        DealEventStatusUpdateAssoc assocAPP = null;
        if( appRequest != null ){
            try {
                assocAPP = new DealEventStatusUpdateAssoc(_srk);
                assocAPP = assocAPP.findByPrimaryKey(
                        new DealEventStatusUpdateAssocPK(deal.getSystemTypeId(), 3, appRequest.getRequestStatusId()));
            } catch (Exception e) {
                ;
            }
            if(assocAPP != null) {
                DEStatus statusApp = payloadGenerator.new DEStatus(deal, assocAPP, appRequest.getStatusDate());
                statuses.add(statusApp);
            }
        }
        
        DealEventStatusUpdateAssoc assocOSC = null;
        if(oscRequest!=null) {
            try {
                assocOSC = new DealEventStatusUpdateAssoc(_srk);
                assocOSC = assocOSC.findByPrimaryKey(
                        new DealEventStatusUpdateAssocPK(deal.getSystemTypeId(), 4, oscRequest.getRequestStatusId()));
            } catch (Exception e) {
                ;
            }
            if (assocOSC != null) {
                DEStatus statusOsc = payloadGenerator.new DEStatus(deal, assocOSC, oscRequest.getStatusDate());
                statuses.add(statusOsc);
            }
        }
        return statuses;
    }

}
