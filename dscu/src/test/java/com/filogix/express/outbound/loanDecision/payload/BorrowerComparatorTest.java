/*
 * @(#)BorrowerComparatorTest.java     12-Aug-09
 * 
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.outbound.loanDecision.payload;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.basis100.deal.entity.Borrower;
import com.basis100.resources.SessionResourceKit;

/**
 * @author HKobayashi
 * 
 */
public class BorrowerComparatorTest {


	/**
	 * check sort order by By Borrower type 
	 * Sorting order: 
	 * 1. By Borrower type 
	 * 2. By Primary flag 
	 * 3. By Martial Status 
	 * 4. By Borrower Id
	 *
	 * Test method for
	 * {@link com.filogix.express.outbound.loanDecision.payload.BorrowerComparator#compare(com.basis100.deal.entity.Borrower, com.basis100.deal.entity.Borrower)}.
	 * @throws Exception 
	 */
	@Test
	public void testCompare_BorrowerType() throws Exception {
		
		List<Borrower> list = new ArrayList<Borrower>();
		SessionResourceKit srk = new SessionResourceKit();
		
		//test data
		list.add(createBorrower(srk,3,"N",0,10001));
		list.add(createBorrower(srk,2,"N",0,10001));
		list.add(createBorrower(srk,1,"N",0,10001));
		
		java.util.Collections.sort(list, new BorrowerComparator());

		assertEquals(1, list.get(0).getBorrowerTypeId());
		assertEquals(2, list.get(1).getBorrowerTypeId());
		assertEquals(3, list.get(2).getBorrowerTypeId());
		
		srk.freeResources();
	}

	/**
	 * check sort order by By Primary flag 
	 * Sorting order: 
	 * 1. By Borrower type 
	 * 2. By Primary flag 
	 * 3. By Martial Status 
	 * 4. By Borrower Id
	 *
	 * Test method for
	 * {@link com.filogix.express.outbound.loanDecision.payload.BorrowerComparator#compare(com.basis100.deal.entity.Borrower, com.basis100.deal.entity.Borrower)}.
	 * @throws Exception 
	 */
	@Test
	public void testCompare_PrimaryFlag() throws Exception {
		
		List<Borrower> list = new ArrayList<Borrower>();
		SessionResourceKit srk = new SessionResourceKit();
		
		//test data
		list.add(createBorrower(srk,1,"N",0,10001)); 
		list.add(createBorrower(srk,1,"Y",0,10001));
		list.add(createBorrower(srk,1,"N",0,10001));
		
		java.util.Collections.sort(list, new BorrowerComparator());

		assertEquals("Y", list.get(0).getPrimaryBorrowerFlag());
		assertEquals("N", list.get(1).getPrimaryBorrowerFlag());
		assertEquals("N", list.get(2).getPrimaryBorrowerFlag());
		
		srk.freeResources();
	}

	/**
	 * check sort order By Martial Status 
	 * Sorting order: 
	 * 1. By Borrower type 
	 * 2. By Primary flag 
	 * 3. By Martial Status 
	 * 4. By Borrower Id
	 *
	 * Test method for
	 * {@link com.filogix.express.outbound.loanDecision.payload.BorrowerComparator#compare(com.basis100.deal.entity.Borrower, com.basis100.deal.entity.Borrower)}.
	 * @throws Exception 
	 */
	@Test
	public void testCompare_MaritalStatus() throws Exception {
		
		List<Borrower> list = new ArrayList<Borrower>();
		SessionResourceKit srk = new SessionResourceKit();
		
		//test data
		list.add(createBorrower(srk,1,"N",3,10001)); 
		list.add(createBorrower(srk,1,"N",2,10001));
		list.add(createBorrower(srk,1,"N",1,10001));
		
		java.util.Collections.sort(list, new BorrowerComparator());

		assertEquals(1, list.get(0).getMaritalStatusId());
		assertEquals(2, list.get(1).getMaritalStatusId());
		assertEquals(3, list.get(2).getMaritalStatusId());
		
		srk.freeResources();
	}
	
	/**
	 * check sort order By Borrower Id
	 * Sorting order: 
	 * 1. By Borrower type 
	 * 2. By Primary flag 
	 * 3. By Martial Status 
	 * 4. By Borrower Id
	 *
	 * Test method for
	 * {@link com.filogix.express.outbound.loanDecision.payload.BorrowerComparator#compare(com.basis100.deal.entity.Borrower, com.basis100.deal.entity.Borrower)}.
	 * @throws Exception 
	 */
	@Test
	public void testCompare_BorrowerId() throws Exception {
		
		List<Borrower> list = new ArrayList<Borrower>();
		SessionResourceKit srk = new SessionResourceKit();
		
		//test data
		list.add(createBorrower(srk,1,"N",2,10003)); 
		list.add(createBorrower(srk,1,"N",2,10002));
		list.add(createBorrower(srk,1,"N",2,10001));
		
		java.util.Collections.sort(list, new BorrowerComparator());

		assertEquals(10001, list.get(0).getBorrowerId());
		assertEquals(10002, list.get(1).getBorrowerId());
		assertEquals(10003, list.get(2).getBorrowerId());
		
		srk.freeResources();
	}
	
	/**
	 * check sort order by combination of each field
	 * Sorting order: 
	 * 1. By Borrower type 
	 * 2. By Primary flag 
	 * 3. By Martial Status 
	 * 4. By Borrower Id
	 *
	 * Test method for
	 * {@link com.filogix.express.outbound.loanDecision.payload.BorrowerComparator#compare(com.basis100.deal.entity.Borrower, com.basis100.deal.entity.Borrower)}.
	 * @throws Exception 
	 */
	@Test
	public void testCompare() throws Exception {
		
		List<Borrower> list = new ArrayList<Borrower>();
		SessionResourceKit srk = new SessionResourceKit();
		
		//test data
		list.add(createBorrower(srk,3,"N",1,10010)); 
		list.add(createBorrower(srk,3,"N",3,10009));
		list.add(createBorrower(srk,3,"N",3,10008));
		
		list.add(createBorrower(srk,2,"N",2,10007)); 
		list.add(createBorrower(srk,2,"N",1,10006));
		list.add(createBorrower(srk,2,"N",2,10005));
		
		list.add(createBorrower(srk,1,"Y",2,10004)); 
		list.add(createBorrower(srk,1,"N",2,10003));
		list.add(createBorrower(srk,1,"N",2,10002));
		
		java.util.Collections.sort(list, new BorrowerComparator());

		int idx = 0;
		assertEquals(10004, list.get(idx++).getBorrowerId());
		assertEquals(10002, list.get(idx++).getBorrowerId());
		assertEquals(10003, list.get(idx++).getBorrowerId());
		
		assertEquals(10006, list.get(idx++).getBorrowerId());
		assertEquals(10005, list.get(idx++).getBorrowerId());
		assertEquals(10007, list.get(idx++).getBorrowerId());
		
		assertEquals(10010, list.get(idx++).getBorrowerId());
		assertEquals(10008, list.get(idx++).getBorrowerId());
		assertEquals(10009, list.get(idx++).getBorrowerId());
		
		srk.freeResources();
	}
	
	private Borrower createBorrower(SessionResourceKit srk, int borrowerTypeid,
			String primaryBorrowerFlag, int maritalStatusid, int borrowerId)
			throws Exception {
		Borrower b = new Borrower(srk, null);
		b.setBorrowerTypeId(borrowerTypeid);
		b.setPrimaryBorrowerFlag(primaryBorrowerFlag);
		b.setMaritalStatusId(maritalStatusid);
		b.setBorrowerId(borrowerId);
		return b;
	}

}
