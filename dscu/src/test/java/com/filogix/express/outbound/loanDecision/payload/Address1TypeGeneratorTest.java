/*
 * @(#)Address1TypeGeneratorTest.java     10-Aug-09
 * 
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.outbound.loanDecision.payload;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.Property;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.UnitTestHelper;
import com.filogix.schema.fcx._1.Address1Type;
import com.filogix.schema.fcx._1.ObjectFactory;

/**
 * @author HKobayashi
 *
 */
public class Address1TypeGeneratorTest {

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.Address1TypeGenerator#generateAddress1Type(com.basis100.deal.entity.Deal, com.basis100.deal.entity.Addr, com.filogix.schema.fcx._1.ObjectFactory)}.
	 * @throws FinderException 
	 * @throws RemoteException 
	 */
	@Test
	public void testGenerateAddress1TypeDealAddrObjectFactory() throws RemoteException, FinderException {
		SessionResourceKit srk = new SessionResourceKit();
		try{
		Addr addr = new Addr(srk); 
		ObjectFactory factory = new ObjectFactory();

		addr.setUnitNumber("123");
		addr.setStreetNumber("23456");
		addr.setStreetName("Yonge");
		addr.setStreetTypeId(2);
		addr.setStreetDirectionId(3);
		addr.setCity("Toronto");
		addr.setProvinceId(4);
		addr.setPostalFSA("M5C");
		addr.setPostalLDU("3C5");

		Address1Type address1Type = Address1TypeGenerator.generateAddress1Type(addr, factory);

		assertEquals("123", address1Type.getUnitNumber());
		assertEquals("23456", address1Type.getStreetNumber());
		assertEquals("Yonge", address1Type.getStreetName());
		assertEquals(2, address1Type.getStreetTypeDd().intValue());
		assertEquals(3, address1Type.getStreetDirectionDd().intValue());
		assertEquals("Toronto", address1Type.getCity());
		assertEquals(4, address1Type.getProvinceDd().intValue());
		assertEquals("M5C", address1Type.getPostalFsa());
		assertEquals("3C5", address1Type.getPostalLdu());


		//if streettypeid = 0, it fails finding primary borrower and doesn't create this element
		addr.setStreetTypeId(0);
		address1Type = Address1TypeGenerator.generateAddress1Type(addr, factory);
		assertNull(address1Type.getStreetTypeDd());

		//Street name
		addr.setStreetName("abc def ghi jkl mlo pqr stu vwx yz1 234");
		address1Type = Address1TypeGenerator.generateAddress1Type(addr, factory);
		assertEquals("abc def ghi jkl mlo pqr stu vw", address1Type.getStreetName());
		}finally{
			srk.freeResources();
		}
	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.Address1TypeGenerator#generateAddress1Type(com.basis100.deal.entity.Deal, com.basis100.deal.entity.Property, com.filogix.schema.fcx._1.ObjectFactory)}.
	 * @throws FinderException 
	 * @throws RemoteException 
	 */
	@Test
	public void testGenerateAddress1TypeDealPropertyObjectFactory() throws RemoteException, FinderException {

		SessionResourceKit srk = new SessionResourceKit();
		try{
			Property property = new Property(srk); 
			ObjectFactory factory = new ObjectFactory();

			property.setUnitNumber("345");
			property.setPropertyStreetNumber("4567");
			property.setPropertyStreetName("Bloor");
			property.setStreetTypeId(2);
			property.setStreetDirectionId(3);
			property.setPropertyCity("Niagara");
			property.setProvinceId(6);
			property.setPropertyPostalFSA("M5V");
			property.setPropertyPostalLDU("1AJ");

			Address1Type address1Type = Address1TypeGenerator.generateAddress1Type(property, factory);

			assertEquals("345", address1Type.getUnitNumber());
			assertEquals("4567", address1Type.getStreetNumber());
			assertEquals("Bloor", address1Type.getStreetName());
			assertEquals(2, address1Type.getStreetTypeDd().intValue());
			assertEquals(3, address1Type.getStreetDirectionDd().intValue());
			assertEquals("Niagara", address1Type.getCity());
			assertEquals(6, address1Type.getProvinceDd().intValue());
			assertEquals("M5V", address1Type.getPostalFsa());
			assertEquals("1AJ", address1Type.getPostalLdu());
		}finally{
			srk.freeResources();
		}
	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.Address1TypeGenerator#populatePostalCode(java.lang.String, java.lang.String, com.filogix.schema.fcx._1.Address1Type)}.
	 */
	@Test
	public void testPopulatePostalCode() {

		Address1Type address1Type = new ObjectFactory().createAddress1Type();
		Address1TypeGenerator.populatePostalCode("M5C", "3C5", address1Type);
		assertEquals("M5C", address1Type.getPostalFsa());
		assertEquals("3C5", address1Type.getPostalLdu());


		address1Type = new ObjectFactory().createAddress1Type();
		Address1TypeGenerator.populatePostalCode("M5C", "", address1Type);
		assertNull(address1Type.getPostalFsa());
		assertNull(address1Type.getPostalLdu());

		address1Type = new ObjectFactory().createAddress1Type();
		Address1TypeGenerator.populatePostalCode("M5C", null, address1Type);
		assertNull(address1Type.getPostalFsa());
		assertNull(address1Type.getPostalLdu());

		address1Type = new ObjectFactory().createAddress1Type();
		Address1TypeGenerator.populatePostalCode("", "3C5", address1Type);
		assertNull(address1Type.getPostalFsa());
		assertNull(address1Type.getPostalLdu());

		address1Type = new ObjectFactory().createAddress1Type();
		Address1TypeGenerator.populatePostalCode(null, "3C5", address1Type);
		assertNull(address1Type.getPostalFsa());
		assertNull(address1Type.getPostalLdu());

		address1Type = new ObjectFactory().createAddress1Type();
		Address1TypeGenerator.populatePostalCode(null, null, address1Type);
		assertNull(address1Type.getPostalFsa());
		assertNull(address1Type.getPostalLdu());

	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.Address1TypeGenerator#populateStreetTypeDd(com.basis100.deal.entity.Deal, com.filogix.schema.fcx._1.Address1Type, int)}.
	 * @throws FinderException 
	 * @throws RemoteException 
	 */
	@Test
	public void testPopulateStreetTypeDd() throws RemoteException, FinderException {

		Address1Type address1Type = null;

		SessionResourceKit srk = new SessionResourceKit();
		try{
			srk.beginTransaction();
			srk.getExpressState().setDealInstitutionId(UnitTestHelper.getDefaultInstitution(srk));


			address1Type = new ObjectFactory().createAddress1Type();
			Address1TypeGenerator.populateStreetTypeDd(address1Type,1);	
			assertEquals(1, address1Type.getStreetTypeDd().intValue());


			address1Type = new ObjectFactory().createAddress1Type();
			Address1TypeGenerator.populateStreetTypeDd(address1Type,0);	
			assertNull(address1Type.getStreetTypeDd());

		}catch(Exception e){
			fail(e.getMessage());
		}finally{
			srk.cleanTransaction();
			srk.freeResources();
		}
	}


}
