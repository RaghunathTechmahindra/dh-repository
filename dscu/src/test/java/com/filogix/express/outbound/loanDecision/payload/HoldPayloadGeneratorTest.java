/*
 * @(#)HoldPayloadGeneratorTest.java     14-Aug-09
 * 
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.outbound.loanDecision.payload;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.basis100.deal.entity.Deal;
import com.basis100.resources.SessionResourceKit;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.ResponseType;
import com.filogix.schema.fcx._1.ResponseType.Pending;

/**
 * @author HKobayashi
 *
 */
public class HoldPayloadGeneratorTest {


	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.HoldPayloadGenerator#populateResponseTypeSpecific(com.filogix.schema.fcx._1.ResponseType, com.basis100.deal.entity.Deal, com.filogix.schema.fcx._1.ObjectFactory)}.
	 */
	@Test
	public void testPopulateResponseTypeSpecific() throws Exception {
		HoldPayloadGenerator target = new HoldPayloadGenerator();
		ObjectFactory fac = new ObjectFactory();
		SessionResourceKit srk = new SessionResourceKit();
		Deal deal = new Deal(srk, null);
		ResponseType rt = fac.createResponseType();
		target.populateResponseTypeSpecific(rt,deal,fac);
		Pending pending = rt.getPending();
		assertNotNull(pending);
	}

}
