/*
 * @(#)DealNotesGeneratorTest.java     13-Aug-09
 * 
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.outbound.loanDecision.payload;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import MosSystem.Mc;

import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealNotes;
import com.basis100.deal.pk.DealPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.UnitTestHelper;
import com.filogix.schema.fcx._1.DealNotesType;
import com.filogix.schema.fcx._1.ObjectFactory;

/**
 * @author HKobayashi
 *
 */
public class DealNotesGeneratorTest {

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.DealNotesGenerator#populateDealNotesListWithDenialReasonAndFirstQualifiedNotes(java.util.List, com.basis100.deal.entity.Deal, com.filogix.schema.fcx._1.ObjectFactory)}.
	 * @throws FinderException 
	 * @throws RemoteException 
	 * @throws JdbcTransactionException 
	 * @throws CreateException 
	 */
	@Test
	public void testPopulateDealNotesListWithDenialReasonAndFirstQualifiedNotes() throws RemoteException, FinderException, JdbcTransactionException, CreateException {
		ObjectFactory fac = new ObjectFactory();
		SessionResourceKit srk = new SessionResourceKit();
		srk.beginTransaction();
		try{
			Deal deal = UnitTestHelper.createDefaultDealTree(srk);
			List<DealNotesType> list = new ArrayList<DealNotesType>();
 
			//execute target
			DealNotesGenerator.populateDealNotesListWithDenialReasonAndFirstQualifiedNotes(list,deal,fac);
			
			String denial_reason_text = BXResources.getPickListDescription(deal.getInstitutionProfileId(), "DENIALREASON", deal.getDenialReasonId(), 0);

			assertEquals(denial_reason_text, list.get(0).getText());

			DealNotes dn0 = new DealNotes(srk);
			dn0.create((DealPK)deal.getPk(), deal.getDealId(), Mc.DEAL_NOTES_CATEGORY_GENERAL,"Note0");
			DealNotes dn1 = new DealNotes(srk);
			dn1.create((DealPK)deal.getPk(), deal.getDealId(), Mc.DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS,"Note1");
			DealNotes dn2 = new DealNotes(srk);
			dn2.create((DealPK)deal.getPk(), deal.getDealId(), Mc.DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS,"Note2"); // right one
			DealNotes dn3 = new DealNotes(srk);
			dn3.create((DealPK)deal.getPk(), deal.getDealId(), Mc.DEAL_NOTES_CATEGORY_GENERAL,"Note3");
			DealNotes dn4 = new DealNotes(srk);
			dn4.create((DealPK)deal.getPk(), deal.getDealId(), Mc.DEAL_NOTES_CATEGORY_CONDITION,"Note4");

			//execute target
			DealNotesGenerator.populateDealNotesListWithDenialReasonAndFirstQualifiedNotes(list,deal,fac);

			assertEquals(2, list.size());
			DealNotesType fcxDN = list.get(1);
			assertEquals(Mc.DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS, fcxDN.getCategoryDd().intValue());
			assertEquals(Convert.toXMLGregorianCalendar(dn2.getDealNotesDate()), fcxDN.getEntryDate());
			assertEquals(denial_reason_text + " Note2", fcxDN.getText());

		}catch(Exception e){
			fail(e.getMessage());
		}finally{
			srk.cleanTransaction();
			srk.freeResources();
		}
	}
	
	
	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.DealNotesGenerator#populateDealNotesListWithFirstQualifiedNotes(java.util.List, com.basis100.deal.entity.Deal, com.filogix.schema.fcx._1.ObjectFactory)}.
	 * @throws FinderException 
	 * @throws RemoteException 
	 * @throws JdbcTransactionException 
	 * @throws CreateException 
	 */
	@Test
	public void testPopulateDealNotesListWithFirstQualifiedNotes() throws RemoteException, FinderException, JdbcTransactionException, CreateException {
		ObjectFactory fac = new ObjectFactory();
		SessionResourceKit srk = new SessionResourceKit();
		srk.beginTransaction();
		try{
			Deal deal = UnitTestHelper.createDefaultDealTree(srk);
			List<DealNotesType> list = new ArrayList<DealNotesType>();
 
			//execute target
			DealNotesGenerator.populateDealNotesListWithFirstQualifiedNotes(list,deal,fac);
			
			String denial_reason_text = BXResources.getPickListDescription(deal.getInstitutionProfileId(), "DENIALREASON", deal.getDenialReasonId(), 0);

			assertEquals(0, list.size());

			DealNotes dn0 = new DealNotes(srk);
			dn0.create((DealPK)deal.getPk(), deal.getDealId(), Mc.DEAL_NOTES_CATEGORY_GENERAL,"Note0");
			DealNotes dn1 = new DealNotes(srk);
			dn1.create((DealPK)deal.getPk(), deal.getDealId(), Mc.DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS,"Note1");
			DealNotes dn2 = new DealNotes(srk);
			dn2.create((DealPK)deal.getPk(), deal.getDealId(), Mc.DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS,"Note2"); // right one
			DealNotes dn3 = new DealNotes(srk);
			dn3.create((DealPK)deal.getPk(), deal.getDealId(), Mc.DEAL_NOTES_CATEGORY_GENERAL,"Note3");
			DealNotes dn4 = new DealNotes(srk);
			dn4.create((DealPK)deal.getPk(), deal.getDealId(), Mc.DEAL_NOTES_CATEGORY_CONDITION,"Note4");

			//execute target
			DealNotesGenerator.populateDealNotesListWithFirstQualifiedNotes(list,deal,fac);

			assertEquals(1, list.size());
			DealNotesType fcxDN = list.get(0);
			assertEquals(Mc.DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS, fcxDN.getCategoryDd().intValue());
			assertEquals(Convert.toXMLGregorianCalendar(dn2.getDealNotesDate()), fcxDN.getEntryDate());
			assertEquals("Note2", fcxDN.getText());

		}catch(Exception e){
			fail(e.getMessage());
		}finally{
			srk.cleanTransaction();
			srk.freeResources();
		}


	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.DealNotesGenerator#populateDealNotesListWithICNotes(java.util.List, com.basis100.deal.entity.Deal, com.filogix.schema.fcx._1.ObjectFactory)}.
	 * @throws Exception 
	 */
	@Test
	public void testPopulateDealNotesListWithICNotes1() throws Exception {
		ObjectFactory fac = new ObjectFactory();
		SessionResourceKit srk = new SessionResourceKit();
		srk.beginTransaction();
		try{
			srk.getExpressState().setDealInstitutionId(UnitTestHelper.getDefaultInstitution(srk));
			Deal deal = UnitTestHelper.createDefaultDealTree(srk);
			List<DealNotesType> list = new ArrayList<DealNotesType>();
			Borrower borr = (Borrower)deal.getBorrowers().iterator().next();
			borr.setLanguagePreferenceId(0);
			borr.ejbStore();
			
			//execute target
			DealNotesGenerator.populateDealNotesListWithICNotes(list,deal,fac);

			DealNotesType fcxDN = list.get(0);
			assertNull(fcxDN.getText());

			borr.setLanguagePreferenceId(1);
			borr.ejbStore();
			list = new ArrayList<DealNotesType>();
			//execute target
			DealNotesGenerator.populateDealNotesListWithICNotes(list,deal,fac);

			fcxDN = list.get(0);
			assertNull(fcxDN.getText());
			

		}catch(Exception e){
			fail(e.getMessage());
		}finally{
			srk.cleanTransaction();
			srk.freeResources();
		}
	}

	@Test
	public void testPopulateDealNotesListWithICNotes2() throws Exception {
		ObjectFactory fac = new ObjectFactory();
		SessionResourceKit srk = new SessionResourceKit();
		srk.beginTransaction();
		try{
			srk.getExpressState().setDealInstitutionId(UnitTestHelper.getDefaultInstitution(srk));
			Deal deal = UnitTestHelper.createDefaultDealTree(srk);
			List<DealNotesType> list = new ArrayList<DealNotesType>();
			Borrower borr = (Borrower)deal.getBorrowers().iterator().next();
			borr.setLanguagePreferenceId(0);
			borr.ejbStore();

			String txt = "----+IC--1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0";
			for(int i =0;i<3;i++){
				DealNotes dn = new DealNotes(srk);
				dn.create((DealPK)deal.getPk(), deal.getDealId(), Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION,0,txt,0);
			}

			//execute target
			borr.setLanguagePreferenceId(0);
			borr.ejbStore();
			list = new ArrayList<DealNotesType>();
			DealNotesGenerator.populateDealNotesListWithICNotes(list,deal,fac);
			DealNotesType fcxDN = list.get(0);
			String expected = "----+IC--1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0\r\n"
				+ "----+IC--1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0\r\n"
				+ "----+IC--1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0\r\n";
			assertEquals(expected, fcxDN.getText());
			
		}catch(Exception e){
			fail(e.getMessage());
		}finally{
			srk.cleanTransaction();
			srk.freeResources();
		}
	}
	
	@Test
	public void testPopulateDealNotesListWithICNotes3() throws Exception {
		ObjectFactory fac = new ObjectFactory();
		SessionResourceKit srk = new SessionResourceKit();
		srk.beginTransaction();
		try{
			srk.getExpressState().setDealInstitutionId(UnitTestHelper.getDefaultInstitution(srk));
			Deal deal = UnitTestHelper.createDefaultDealTree(srk);
			List<DealNotesType> list = new ArrayList<DealNotesType>();
			Borrower borr = (Borrower)deal.getBorrowers().iterator().next();
			borr.setLanguagePreferenceId(0);
			borr.ejbStore();

			String txt = "----+IC--1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0";
			StringBuilder bigTxt = new StringBuilder(1000);
			for(int i=0;i<10;i++){bigTxt.append(txt);}
			for(int i =0;i<5;i++){
				DealNotes dn = new DealNotes(srk);
				dn.create((DealPK)deal.getPk(), deal.getDealId(), Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION,0,bigTxt.toString(),0);
			}

			//execute target
			borr.setLanguagePreferenceId(0);
			borr.ejbStore();
			list = new ArrayList<DealNotesType>();
			DealNotesGenerator.populateDealNotesListWithICNotes(list,deal,fac);
			DealNotesType fcxDN = list.get(0);
			assertEquals(DealNotesGenerator.MAX_LENGTH_DEALNOTE, fcxDN.getText().length());
			
		}catch(Exception e){
			fail(e.getMessage());
		}finally{
			srk.cleanTransaction();
			srk.freeResources();
		}
	}
	

}
