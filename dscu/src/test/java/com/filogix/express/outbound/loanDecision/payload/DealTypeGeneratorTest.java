/*
 * @(#)DealTypeGeneratorTest.java     14-Aug-09
 * 
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.outbound.loanDecision.payload;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DownPaymentSource;
import com.basis100.deal.pk.DealPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.UnitTestHelper;
import com.filogix.schema.fcx._1.DealType;
import com.filogix.schema.fcx._1.ObjectFactory;

/**
 * @author HKobayashi
 *
 */
public class DealTypeGeneratorTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.DealTypeGenerator#generateDealType(com.basis100.deal.entity.Deal, com.filogix.schema.fcx._1.ObjectFactory)}.
	 * @throws FinderException 
	 * @throws RemoteException 
	 * @throws JdbcTransactionException 
	 */
	@Test
	public void testGenerateDealType() throws RemoteException, FinderException, JdbcTransactionException {
		ObjectFactory fac = new ObjectFactory();
		SessionResourceKit srk = new SessionResourceKit();
		srk.beginTransaction();
		try{
			
			srk.getExpressState().setDealInstitutionId(UnitTestHelper.getDefaultInstitution(srk));
			Deal deal = UnitTestHelper.createDefaultDealTree(srk);
			DealType dt = DealTypeGenerator.generateDealType(deal, fac);

			assertNotNull(dt);
			assertNotNull(dt.getDealPurposeDd());
			
		}finally{
			srk.cleanTransaction();
			srk.freeResources();
		}
	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.DealTypeGenerator#populateDealType(com.filogix.schema.fcx._1.DealType, com.basis100.deal.entity.Deal, com.filogix.schema.fcx._1.ObjectFactory)}.
	 */
	@Test
	public void testPopulateDealType() throws Exception{
		ObjectFactory fac = new ObjectFactory();
		SessionResourceKit srk = new SessionResourceKit();
		srk.beginTransaction();
		try{
			
			srk.getExpressState().setDealInstitutionId(UnitTestHelper.getDefaultInstitution(srk));
			Deal deal = UnitTestHelper.createDefaultDealTree(srk);
			DealType dt = fac.createDealType();
			
			deal.setSourceApplicationId("112233");
			deal.setDealPurposeId(18);
			deal.setSpecialFeatureId(1);
			Date now = new Date();
			deal.setEstimatedClosingDate(now);
			deal.setTaxPayorId(1);
			deal.ejbStore();
			
			//deal.setDownPaymentAmount(5600);
			DownPaymentSource dps = new DownPaymentSource(srk, null);
			dps.create((DealPK)deal.getPk());
			dps.setAmount(123.45d);
			dps.ejbStore();
			
			dps.create((DealPK)deal.getPk());
			dps.setAmount(223.46d);
			dps.ejbStore();
			
			//execute
			DealTypeGenerator.populateDealType(dt, deal,fac);

			assertEquals("112233", dt.getApplicationId());
			assertEquals(6, dt.getDealPurposeDd().intValue());
			assertEquals(1, dt.getDealTypeDd().intValue());
			assertEquals(Convert.toXMLGregorianCalendar(now), dt
					.getEstimatedClosingDate());
			assertEquals(1, dt.getTaxPayorDd().intValue());
			List<com.filogix.schema.fcx._1.DealType.DownPaymentSource> list = dt
					.getDownPaymentSource();
			assertEquals(123.45d, list.get(0).getAmount().doubleValue(), 0.000);
			assertEquals(223.46d, list.get(1).getAmount().doubleValue(), 0.000);
			
		}finally{
			srk.cleanTransaction();
			srk.freeResources();
		}
	}

	/**
	 * Test method for {@link com.filogix.express.outbound.loanDecision.payload.DealTypeGenerator#convertDealPurposeId(int)}.
	 */
	@Test
	public void testConvertDealPurposeId() {
		assertEquals(0, DealTypeGenerator.convertDealPurposeId(0));
		assertEquals(1, DealTypeGenerator.convertDealPurposeId(17));
		assertEquals(2, DealTypeGenerator.convertDealPurposeId(5));
		assertEquals(2, DealTypeGenerator.convertDealPurposeId(4));
		assertEquals(3, DealTypeGenerator.convertDealPurposeId(7));
		assertEquals(3, DealTypeGenerator.convertDealPurposeId(6));
		assertEquals(6, DealTypeGenerator.convertDealPurposeId(18));
		assertEquals(7, DealTypeGenerator.convertDealPurposeId(19));
		assertEquals(1, DealTypeGenerator.convertDealPurposeId(1));
		assertEquals(1, DealTypeGenerator.convertDealPurposeId(99));
	}

}
