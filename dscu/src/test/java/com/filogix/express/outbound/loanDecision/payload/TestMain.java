package com.filogix.express.outbound.loanDecision.payload;

import org.junit.Ignore;

import com.basis100.deal.entity.Deal;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.loanDecision.DAUnrecoverableException;

@Ignore
public class TestMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		testCommitmentPayloadGenerator();

		testDenialPayloadGenerator();
	}

	private static void testDenialPayloadGenerator() {
		ResourceManager.init();
		IDAPayloadGenerator payloadGen = new DeclinePayloadGenerator();

		// dummy data
		SessionResourceKit srk = new SessionResourceKit();
		srk.getExpressState().setDealInstitutionId(1);
		Deal deal = null;
		try {
			deal = new Deal(srk, null, 901943, 1);
			//deal = new Deal(srk, null, 901075, 4);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String xml = null;
		try {
			xml = payloadGen.getXmlPayload(deal);
		} catch (DAUnrecoverableException e) {
			e.printStackTrace();
		}
		System.out.println(xml);
		ResourceManager.shutdown();
	}

	private static void testCommitmentPayloadGenerator() {
		ResourceManager.init();
		IDAPayloadGenerator payloadGen = new CommitmentPayloadGenerator();

		// dummy data
		SessionResourceKit srk = new SessionResourceKit();
		srk.getExpressState().setDealInstitutionId(1);
		Deal deal = null;
		try {
			//deal = new Deal(srk, null, 901921, 9);
			deal = new Deal(srk, null, 901918, 11);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String xml = null;
		try {
			xml = payloadGen.getXmlPayload(deal);
		} catch (DAUnrecoverableException e) {
			e.printStackTrace();
		}
		System.out.println(xml);
		ResourceManager.shutdown();
	}

}
