/*
 * @(#)AbstractUnitTestDataRepository.java    2007-8-29
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.test;

import java.lang.annotation.Inherited;
import java.sql.ResultSet;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;

import oracle.sql.CLOB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * AbstractUnitTestDataRepository is a facility class to set up the data
 * structure for the unit test virtual repository.  For temporary use, we just
 * use HashMap to keep those data for unit test.
 *
 * @version   1.0 2007-8-29
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public abstract class AbstractUnitTestDataRepository
    implements UnitTestDataRepository {

    // The logger
    private final static Logger _logger = 
        LoggerFactory.getLogger(AbstractUnitTestDataRepository.class);

    // the tables map.
    protected Map<String, Map> _tables = new HashMap<String, Map>();
    // indicator initialization.
    protected boolean _loaded = false;

    /**
     * returns the column value as an object.
     */
    protected Object getValue(String tableName,
                              String columnName, int sequence)
        throws Exception {

        if (! _loaded) {
            loadData();
        }

        Map oneRow = _tables.get(tableName.toUpperCase() +
                                 DELIMITER + sequence);
        return oneRow.get(columnName.toUpperCase());
    }

    /**
     * load unit test data to the repository.
     */
    abstract protected void loadData() throws Exception;

    /**
     * execute sql for unit test data to the repository.
     * intended to use store test data into table
     */
    abstract public ResultSet executeSQL(String sql) throws Exception;

    /**
     * {@inheritedDoc}
     */
    public int getInt(String tableName, String columnName, int sequence)
        throws Exception {

      Object o = getValue(tableName, columnName, sequence);
      if (o==null) return 0;
      return ((Integer) o).intValue();
    }

    /**
     * {@inheritedDoc}
     */
    public String getString(String tableName, String columnName, int sequence)
        throws Exception {

        return (String) getValue(tableName, columnName, sequence);
    }

    /**
     * {@inheritedDoc}
     */
    public Date getDate(String tableName, String columnName, int sequence)
        throws Exception {

        return (Date) getValue(tableName, columnName, sequence);
    }
    
    /**
     * {@inheritedDoc}
     */
    public double getDouble(String tableName, String columnName, int sequence)
        throws Exception {
    	
        Object o = getValue(tableName, columnName, sequence);
        if (o==null) return 0.0d;
        
      	return ((Double) o).doubleValue();
    }
    
    /**
     * {@inheritedDoc}
     */
    public boolean getBoolean(String tableName, String columnName, int sequence)
            throws Exception {

        String s = getString(tableName, columnName, sequence);
        if ("Y".equals(s) || "y".equals(s)) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * {@inheritedDoc}
     */
    public CLOB getClob(String tableName, String columnName, int sequence)
            throws Exception {

        return (CLOB) getValue(tableName, columnName, sequence);
    }
}