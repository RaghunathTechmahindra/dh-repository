/*
 * @(#)UnitTestDataRepositorySpring.java    2007-8-29
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.test;

import java.util.Map;
import java.util.HashMap;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Types;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * UnitTestDataRepositorySpring implements {@link UnitTestDataRepository} as a
 * Spring bean.  So it can be configured through Spring context xml file.
 *
 * @version   1.0 2007-8-29
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public class UnitTestDataRepositorySpring
    extends AbstractUnitTestDataRepository {

    // The logger
    private final static Logger _logger = 
        LoggerFactory.getLogger(UnitTestDataRepositorySpring.class);

    // the table list that we are going to load data from.
    private Map<String, Integer> _tableList;

    // the JDBC connection.
    private DataSource _dataSource;

    /**
     * Constructor function
     */
    public UnitTestDataRepositorySpring() {
    }

    /**
     * inject the datasource.
     */
    public void setDataSource(DataSource dataSource) {

        _dataSource = dataSource;
    }

    /**
     * inject the table list from Spring context.
     */
    public void setTableList(Map<String, Integer> tableList) {

        _tableList = tableList;
    }

    /**
     * {@inheritedDoc}
     */
    @Override
    protected void loadData() throws Exception {

        _logger.info("Trying to load all tables value to the virtual Repository ...");
        Statement statement = _dataSource.getConnection().createStatement();

        for (String tableName:_tableList.keySet()) {

            int count = _tableList.get(tableName).intValue();
            _logger.info("Loading [{}] records for table [{}]...",
                         count, tableName);

            loadData4Table(statement, tableName.toUpperCase(), count);
        }

        // everything is loaded...
        _loaded = true;
    }

    /**
     * load date for the given table.
     */
    protected void loadData4Table(Statement statement, String tableName, int count)
        throws Exception {

        ResultSet rs = statement.executeQuery("select * from " + tableName);
        ResultSetMetaData metaData = rs.getMetaData();

        // preparing column name an column type.
        String[] columnNames = new String[metaData.getColumnCount()];
        int[] columnTypes = new int[metaData.getColumnCount()];
        int[] columnScales = new int[metaData.getColumnCount()];
        for(int i = 0; i < metaData.getColumnCount(); i ++) {

            columnNames[i] = metaData.getColumnName(i + 1).toUpperCase();
            columnTypes[i] = metaData.getColumnType(i + 1);
            columnScales[i] = metaData.getScale(i + 1);
        }

        for (int sequence = 0;
             (sequence < count) & rs.next(); sequence ++) {

            // create a new map for each row.
            Map<String, Object> oneRow = new HashMap<String, Object>();
    
            for(int i = 0; i < columnNames.length; i ++) {

                if (rs.getObject(columnNames[i]) == null) {
                    // TODO may be just skip.
                    oneRow.put(columnNames[i], null);
                } else {
                    // for different type.
                    switch(columnTypes[i]) {
                    case Types.VARCHAR:
                    case Types.CHAR:
                        // string in java language.
                        String value = rs.getString(columnNames[i]);
                        oneRow.put(columnNames[i], value);
                        break;
                    case Types.INTEGER:
                    case Types.SMALLINT:
                    case Types.TINYINT:
                    case Types.NUMERIC:
                    case Types.DECIMAL:
                    	if(columnScales[i] > 0){
                    		double dblValue = rs.getDouble(columnNames[i]);
                    		oneRow.put(columnNames[i], new Double(dblValue));
                    	} else{
                    		int intValue = rs.getInt(columnNames[i]);
                    		oneRow.put(columnNames[i], new Integer(intValue));
                    	}
                        break;
                    case Types.DATE:
                    case Types.TIMESTAMP:
                        // Date format.
                        oneRow.put(columnNames[i],
                                   rs.getTimestamp(columnNames[i]));
                        break;
                    case Types.CLOB:
                        // Clob format.
                        oneRow.put(columnNames[i],
                                   rs.getObject(columnNames[i]));
                        break;
                        
                    default:
                        // TODO: skip other cases for now.
                        break;
                    }
                }
            }
    
            // save this in repository.
            _tables.put(tableName + DELIMITER + sequence, oneRow);
        }
    }
    
    public ResultSet executeSQL(String sql) throws Exception {
        
        _logger.info("Trying to execute passing sql: " + sql);
        Statement statement = _dataSource.getConnection().createStatement();

        ResultSet rs = statement.executeQuery(sql);
        
        return rs;

    }

}