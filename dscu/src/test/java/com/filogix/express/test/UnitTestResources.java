/*
 * @(#)UnitTestResources.java    2007-8-24
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.test;

import java.io.File;
import java.io.IOException;

/**
 * UnitTestResources defines the interfaces to get the resources for unit
 * testing in Express.
 *
 * @version   1.0 2007-8-24
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public interface UnitTestResources {

    /**
     * returns the xml file for unit testing.
     */
    public File getTestDataXmlFile() throws IOException;
}