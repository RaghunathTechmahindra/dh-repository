/*
 * @(#)Helper.java    2007-10-10
 *
 * Copyright (C) 2005 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.Deal;

public class Helper {
    
    // The logger
    private static Log _log = LogFactory.getLog(Helper.class);

    /**
     * Constructor function
     */

    public Helper() {
        // TODO Auto-generated constructor stub
    }

    /**
     * helper method to get a string desc for the given deal.
     */
    public static String dealString(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("DealId=").
                append(deal.getDealId()).
                append(" CopyId=").
                append(deal.getCopyId()).
                append(" UnderwriterUserId=").
                append(deal.getUnderwriterUserId()).
                append(" AdministratorId=").
                append(deal.getAdministratorId()).
                append(" SOBProfileId=").
                append(deal.getSourceOfBusinessProfileId()).
                append(" SourceFirmProfileId=").
                append(deal.getSourceFirmProfileId()).
                append(" SepcialFeatureId=").
                append(deal.getSpecialFeatureId());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }

    /**
     * helper method to get a string desc for calculation for the given deal.
     */
    public static String dealStr4CalcInBalanceRemainingAtEndOfTerm(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("InstitutionProfileId=").
                append(deal.getInstitutionProfileId()).
                append(" RepaymentTypeId=").
                append(deal.getRepaymentTypeId()).
                append(" AmortizationTerm=").
                append(deal.getAmortizationTerm()).
                append(" MtgProdId=").
                append(deal.getMtgProdId()).
                append(" PaymentFrequencyId=").
                append(deal.getPaymentFrequencyId()).
                append(" TotalLoanAmount=").
                append(deal.getTotalLoanAmount()).
                append(" ActualPaymentTerm=").
                append(deal.getActualPaymentTerm()).
                append(" NetInterestRate=").
                append(deal.getNetInterestRate()).
                append(" PandiPaymentAmount=").
                append(deal.getPandiPaymentAmount());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }
    
    /**
     * helper method to get a string desc for calculation for the given deal.
     */
    public static String dealStr4CalcInCombinedTotalGDS3Years(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("InstitutionProfileId=").
                append(deal.getInstitutionProfileId()).
                append(" NetInterestRate=").
                append(deal.getNetInterestRate()).
                append(" DealTypeId=").
                append(deal.getDealTypeId()).
                append(" ProductTypeId=").
                append(deal.getProductTypeId()).
                append(" PandiPaymentAmount=").
                append(deal.getPandiPaymentAmount()).
                append(" NetInterestRate=").
                append(deal.getNetInterestRate()).
                append(" PaymentFrequencyId=").
                append(deal.getPaymentFrequencyId());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }
    
    /**
     * helper method to get a string desc for calculation for the given deal.
     */
    public static String dealStr4CalcInCombinedTotalTDS3Years(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("DealId=").
                append(deal.getDealId()).
                append(" InstitutionProfileId=").
                append(deal.getInstitutionProfileId()).
                append(" CopyId=").
                append(deal.getCopyId()).
                append(" PandiPaymentAmount=").
                append(deal.getPandiPaymentAmount()).
                append(" AdditionalPrincipal=").
                append(deal.getAdditionalPrincipal()).
                append(" NetInterestRate=").
                append(deal.getNetInterestRate()).
                append(" ProductTypeId=").
                append(deal.getProductTypeId());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }
    
    /**
     * helper method to get a string desc for calculation for the given deal.
     */
    public static String dealStr4CalcInCommitmentExpiryDate(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("InstitutionProfileId=").
                append(deal.getInstitutionProfileId()).
                append(" AutoCalcCommitExpiryDate=").
                append(deal.getAutoCalcCommitExpiryDate()).
                append(" CommitmentExpirationDate=").
                append(deal.getCommitmentExpirationDate()).
                append(" EstimatedClosingDate=").
                append(deal.getEstimatedClosingDate()).
                append(" CommitmentIssueDate=").
                append(deal.getCommitmentIssueDate()).
                append(" ApplicationDate=").
                append(deal.getApplicationDate()).
                append(" BranchProfileId=").
                append(deal.getBranchProfileId());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }
    
    /**
     * helper method to get a string desc for calculation for the given deal.
     */
    public static String dealStr4CalcInCommitmentReturnDate(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("InstitutionProfileId=").
                append(deal.getInstitutionProfileId()).
                append(" CommitmentIssueDate=").
                append(deal.getCommitmentIssueDate()).
                append(" BranchProfileId=").
                append(deal.getBranchProfileId()).
                append(" CommitmentIssueDate=").
                append(deal.getCommitmentIssueDate()).
                append(" ReturnDate=").
                append(deal.getReturnDate());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }
    
    /**
     * helper method to get a string desc for calculation for the given deal.
     */
    public static String dealStr4CalcInEquivalentInterestRate(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("InstitutionProfileId=").
                append(deal.getInstitutionProfileId()).
                append(" NetInterestRate=").
                append(deal.getNetInterestRate()).
                append(" PaymentFrequencyId=").
                append(deal.getPaymentFrequencyId()).
                append(" EquivalentInterestRate=").
                append(deal.getEquivalentInterestRate());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }
    
    /**
     * helper method to get a string desc for calculation for the given deal.
     */
    public static String dealStr4CalcInFirstPaymentDate(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("InstitutionProfileId=").
                append(deal.getInstitutionProfileId()).
                append(" InterimInterestAdjustmentDate=").
                append(deal.getInterimInterestAdjustmentDate()).
                append(" EstimatedClosingDate=").
                append(deal.getEstimatedClosingDate()).
                append(" FirstPaymentDate=").
                append(deal.getFirstPaymentDate()).
                append(" PaymentFrequencyId=").
                append(deal.getPaymentFrequencyId());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }
    
    /**
     * helper method to get a string desc for calculation for the given deal.
     */
    public static String dealStr4CalcInGDSTDS3YearRate(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("InstitutionProfileId=").
                append(deal.getInstitutionProfileId()).
                append(" Discount=").
                append(deal.getDiscount()).
                append(" PricingProfileId=").
                append(deal.getPricingProfileId()).
                append(" GDSTDS3YearRate=").
                append(deal.getGDSTDS3YearRate());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }
    
    /**
     * helper method to get a string desc for calculation for the given deal.
     */
    public static String dealStr4CalcInInterestAdjustmentDate(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("InstitutionProfileId=").
                append(deal.getInstitutionProfileId()).
                append(" EstimatedClosingDate=").
                append(deal.getEstimatedClosingDate()).
                append(" PaymentFrequencyId=").
                append(deal.getPaymentFrequencyId()).
                append(" InterimInterestAdjustmentDate=").
                append(deal.getInterimInterestAdjustmentDate()).
                append(" FirstPaymentDate=").
                append(deal.getFirstPaymentDate());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }
    
    /**
     * helper method to get a string desc for calculation for the given deal.
     */
    public static String dealStr4CalcInInterestOverTerm(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("InstitutionProfileId=").
                append(deal.getInstitutionProfileId()).
                append(" ActualPaymentTerm=").
                append(deal.getActualPaymentTerm()).
                append(" PaymentFrequencyId=").
                append(deal.getPaymentFrequencyId()).
                append(" PandiPaymentAmount=").
                append(deal.getPandiPaymentAmount()).
                append(" TotalLoanAmount=").
                append(deal.getTotalLoanAmount()).
                append(" BalanceRemainingAtEndOfTerm=").
                append(deal.getBalanceRemainingAtEndOfTerm()).
                append(" InterestOverTerm=").
                append(deal.getInterestOverTerm());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }
    
    /**
     * helper method to get a string desc for calculation for the given deal.
     */
    public static String dealStr4CalcInInterimInterestAmount(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("InstitutionProfileId=").
                append(deal.getInstitutionProfileId()).
                append(" PerdiemInterestAmount=").
                append(deal.getPerdiemInterestAmount()).
                append(" IADNumberOfDays=").
                append(deal.getIADNumberOfDays()).
                append(" NetInterestRate=").
                append(deal.getNetInterestRate()).
                append(" InterimInterestAmount=").
                append(deal.getInterimInterestAmount());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }
    
    /**
     * helper method to get a string desc for calculation for the given deal.
     */
    public static String dealStr4CalcInMaturityDate(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("InstitutionProfileId=").
                append(deal.getInstitutionProfileId()).
                append(" InterimInterestAdjustmentDate=").
                append(deal.getInterimInterestAdjustmentDate()).
                append(" PaymentFrequencyId=").
                append(deal.getPaymentFrequencyId()).
                append(" ActualPaymentTerm=").
                append(deal.getActualPaymentTerm()).
                append(" MaturityDate=").
                append(deal.getMaturityDate());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }
    
    /**
     * helper method to get a string desc for calculation for the given deal.
     */
    public static String dealStr4CalcInMaximumLoanLimit(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("InstitutionProfileId=").
                append(deal.getInstitutionProfileId()).
                append(" DealTypeId=").
                append(deal.getDealTypeId()).
                append(" CombinedLendingValue=").
                append(deal.getCombinedLendingValue()).
                append(" MaximumLoanAmount=").
                append(deal.getMaximumLoanAmount());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }
    
    /**
     * helper method to get a string desc for calculation for the given deal.
     */
    public static String dealStr4CalcInMIPremium(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("DealId=").
                append(deal.getDealId()).
                append(" CopyId=").
                append(deal.getCopyId()).
                append(" MIPremiumAmount=").
                append(deal.getMIPremiumAmount()).
                append(" MIStatusId=").
                append(deal.getMIStatusId()).
                append(" MIExistingPolicyNumber=").
                append(deal.getMIExistingPolicyNumber()).
                append(" CombinedLTV=").
                append(deal.getCombinedLTV()).
                append(" TotalLoanAmount=").
                append(deal.getTotalLoanAmount());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }
    
    /**
     * helper method to get a string desc for calculation for the given deal.
     */
    public static String dealStr4CalcInPandI(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("InstitutionProfileId=").
                append(deal.getInstitutionProfileId()).
                append(" NetInterestRate=").
                append(deal.getNetInterestRate()).
                append(" AmortizationTerm=").
                append(deal.getAmortizationTerm()).
                append(" PaymentFrequencyId=").
                append(deal.getPaymentFrequencyId()).
                append(" TotalLoanAmount=").
                append(deal.getTotalLoanAmount()).
                append(" InterestCompoundId=").
                append(deal.getInterestCompoundId()).
                append(" PandiPaymentAmount=").
                append(deal.getPandiPaymentAmount());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }
    
    /**
     * helper method to get a string desc for calculation for the given deal.
     */
    public static String dealStr4CalcInTeaserRateInterestSaving(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("InstitutionProfileId=").
                append(deal.getInstitutionProfileId()).
                append(" TotalLoanAmount=").
                append(deal.getTotalLoanAmount()).
                append(" PandiPaymentAmount=").
                append(deal.getPandiPaymentAmount()).
                append(" TeaserPIAmount=").
                append(deal.getTeaserPIAmount()).
                append(" AmortizationTerm=").
                append(deal.getAmortizationTerm()).
                append(" MtgProdId=").
                append(deal.getMtgProdId()).
                append(" TeaserRateInterestSaving=").
                append(deal.getTeaserRateInterestSaving()).
                append(" NetInterestRate=").
                append(deal.getNetInterestRate());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }
    
    /**
     * helper method to get a string desc for calculation for the given deal.
     */
    public static String dealStr4CalcInTotalCostOfBorrowing(Deal deal) {

        StringBuffer ret = new StringBuffer();
        ret.append("Deal [");
        if (deal != null) {
            ret.append("InstitutionProfileId=").
                append(deal.getInstitutionProfileId()).
                append(" ActualPaymentTerm=").
                append(deal.getActualPaymentTerm()).
                append(" NetInterestRate=").
                append(deal.getNetInterestRate()).
                append(" MIStatusId=").
                append(deal.getMIStatusId()).
                append(" MIExistingPolicyNumber=").
                append(deal.getMIExistingPolicyNumber()).
                append(" CombinedLTV=").
                append(deal.getCombinedLTV()).
                append(" BalanceRemainingAtEndOfTerm").
                append(deal.getBalanceRemainingAtEndOfTerm()).
                append(" MIPremiumPST").
                append(deal.getMIPremiumPST()).
                append(" PaymentFrequencyId").
                append(deal.getPaymentFrequencyId()).
                append(" PandIPaymentAmountMonthly").
                append(deal.getPandIPaymentAmountMonthly()).
                append(" TotalCostOfBorrowing").
                append(deal.getTotalCostOfBorrowing()).
                append(" TotalLoanAmount=").
                append(deal.getTotalLoanAmount());
        } else {
            ret.append(deal);
        }
        ret.append("]");

        return ret.toString();
    }
}
