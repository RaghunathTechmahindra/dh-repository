/*
 * @(#)UnitTestDataRepository.java    2007-8-29
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.test;

import java.sql.ResultSet;
import java.util.Date;

import oracle.sql.CLOB;




/**
 * UnitTestDataRepository defines the interface to access the unit test data
 * from a virtual repository.  Those data in the virtual repository will be
 * loaded by a implementation of this interface.
 *
 * @version   1.0 2007-8-29
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public interface UnitTestDataRepository {

    /**
     * the delimiter beteween tablename and sequence.
     */
    public static final String DELIMITER = "_";

    /**
     * returns an int value for the given table name and column name.
     *
     * @param tableName the table name.
     * @param columnName the column name.
     * @param sequence the number of the record, starts from 0.
     * @return the column value as int.
     */
    public int getInt(String tableName, String columnName, int sequence)
        throws Exception;

    /**
     * returns a string value for the given table name and column name.
     */
    public String getString(String tableName, String columnName, int sequence)
        throws Exception;

    /**
     * return a Date value for the given table name and column name.
     */
    public Date getDate(String tableName, String columnName, int sequence)
        throws Exception;
    
    /**
     * execute passed sql.
     */
    public ResultSet executeSQL(String sql)
        throws Exception;

    /**
     * returns a double value from the given table name and column name.
     */
    public double getDouble(String tableName, String columnName, int sequence)
        throws Exception;
    
    /**
     * returns a double value from the given table name and column name.
     */
    public boolean getBoolean(String tableName, String columnName, int sequence)
        throws Exception;
    
    /**
     * returns a Clob value from the given table name and column name.
     */
    public CLOB getClob(String tableName, String columnName, int sequence)
        throws Exception;
}