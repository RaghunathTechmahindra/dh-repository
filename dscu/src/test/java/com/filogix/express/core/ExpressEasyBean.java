/*
 * @(#)ExpressEasyBean.java    2007-7-30
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.core;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * ExpressEasyBean is a simple bean for testing Express context for core module.
 *
 * @version   1.0 2007-7-30
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public class ExpressEasyBean {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(ExpressEasyBean.class);

    // the greeting property.
    private String _greeting;

    /**
     * Constructor function
     */
    public ExpressEasyBean() {
    }

    /**
     * inject the greeting property.
     */
    public void setGreeting(String greeting) {

        _greeting = greeting;
    }

    /**
     * returns the greeting message.
     */
    public String getGreeting() {

        return _greeting;
    }

    // the datasource.
    private DataSource _dataSource;

    /**
     * inject the data source.
     */
    public void setDataSource(DataSource dataSource) {

        _dataSource = dataSource;
    }

    /**
     * returns the datasource.
     */
    public DataSource getDataSource() {

        return _dataSource;
    }
}