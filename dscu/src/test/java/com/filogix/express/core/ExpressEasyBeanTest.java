/*
 * @(#)ExpressEasyBeanTest.java    2007-7-30
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.core;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import javax.sql.DataSource;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * ExpressEasyBeanTest is the test case for testing ExpressEasyBean.
 *
 * @version   1.0 2007-7-30
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ExpressEasyBeanTest extends TestCase {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(ExpressEasyBeanTest.class);

    /**
     * choosing the testcase you want to test.
     */
    public static Test suite() {

        // Run all test cases.
        TestSuite suite = new TestSuite(ExpressEasyBeanTest.class);
        // Run selected test cases.
        //TestSuite suite = new TestSuite();
        //suite.addTest(new ExpressEasyBeanTest("testMethodName"));

        return suite;
    }

    /**
     * Constructor function
     */
    public ExpressEasyBeanTest() {
    }

    /**
     * constructs a test case with the given test case name.
     */
    public ExpressEasyBeanTest(String name) {
        super(name);
    }

    /**
     * test get the greeting message.
     */
    public void testGetGreeting() throws Exception {

        _log.info("Getting bean from Express Context ...");
        ExpressEasyBean theBean =
            (ExpressEasyBean) ExpressCoreTestContext.getService("easyBean");
        assertTrue(null != theBean);

        _log.info("Here is the greeting message: " + theBean.getGreeting());
        assertTrue("Hello World, from testing".equals(theBean.getGreeting()));
    }

    public void testDataSource() throws Exception {

        _log.info("Trying to use datasource to do a simple query ...");

        ExpressEasyBean theBean =
            (ExpressEasyBean) ExpressCoreTestContext.getService("easyBean");
        assertTrue(null != theBean);

        DataSource dataSource = theBean.getDataSource();
        assertTrue(null != dataSource);
        Connection connection = dataSource.getConnection();
        assertTrue(null != connection);
        Statement statement = connection.createStatement();
        assertTrue(null != statement);
        String query = "SELECT count(*) from USERPROFILE";
        _log.info("Executing Query: " + query);
        ResultSet result = statement.executeQuery(query);
        assertTrue(null != result);
        assertTrue(result.next());
        int count = result.getInt(1);
        assertTrue(count > 0);
        _log.info("Amount of user: " + count);

        result.close();
        statement.close();
        connection.close();
    }

    /**
     * setting up the testing env.
     */
    protected void setUp() {
    }

    /**
     * clean the testing env.
     */
    protected void tearDown() {
    }
}