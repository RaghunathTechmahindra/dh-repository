package com.filogix.express.outbound.dscu;


import javax.xml.soap.SOAPException;

import javax.xml.soap.SOAPHeaderElement;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.soap.SOAPFactory;
import org.apache.axiom.soap.SOAPHeaderBlock;
//import org.apache.axis2.saaj.SOAPHeaderElementImpl;




public class RequestHeaderBlock {

    private static SOAPFactory factory = OMAbstractFactory.getSOAP12Factory();
    public String namespace;
    public String localPart;
    public Object value;
    public String prefix;
    
    public SOAPHeaderBlock   headerElement;
    
    public RequestHeaderBlock(String namespace, String localPart, String prefix, 
            String value)  throws SOAPException {
        this.namespace = namespace;
        this.localPart = localPart;
        this.prefix = prefix;
        this.value = value;
        OMNamespace ns = factory.createOMNamespace(namespace, prefix);
        headerElement = factory.createSOAPHeaderBlock(localPart, ns);
        headerElement.setText(value);
    }

    public RequestHeaderBlock(String namespace, String localPart, String prefix) 
        throws SOAPException{
        this.namespace = namespace;
        this.localPart = localPart;
        this.prefix = prefix;
        
        OMNamespace ns = factory.createOMNamespace(namespace, prefix);
        headerElement = factory.createSOAPHeaderBlock(localPart, ns);
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
    
    public String getPrefix() {
        return this.prefix;
    }
    
    public String getNamespace() {
        return namespace;
    }
    
    public String getLocalPart() {
        return localPart;
    }

    public Object getValue() {
        return value;
    }
    
    public SOAPHeaderBlock getSOAPHeaderBlock() {
        return headerElement;
    }
    
    public static SOAPHeaderBlock getRequestHeaderBlock(String namespace, String localPart, String prefix, 
            String value)  throws SOAPException {
        OMNamespace ns = factory.createOMNamespace(namespace, prefix);
        SOAPHeaderBlock  headerElement = factory.createSOAPHeaderBlock(localPart, ns);
        headerElement.setText(value);
        return headerElement;
    }

    public  static SOAPHeaderBlock getRequestHeaderBlock(String namespace, String localPart, String prefix) 
        throws SOAPException{
        
        OMNamespace ns = factory.createOMNamespace(namespace, prefix);
        SOAPHeaderBlock  headerElement = factory.createSOAPHeaderBlock(localPart, ns);
        return headerElement;
    }
}
