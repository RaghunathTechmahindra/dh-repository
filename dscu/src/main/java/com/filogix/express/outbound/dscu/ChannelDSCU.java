package com.filogix.express.outbound.dscu;

import java.util.UUID;


import org.apache.axiom.soap.SOAPHeaderBlock;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.client.Stub;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.Channel;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.entity.SourceOfBusinessProfile;
import com.basis100.deal.entity.SystemType;
import com.basis100.deal.pk.ChannelPK;
import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.deal.pk.SystemTypePK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.ExpressOutboundContext;
import org.springframework.stereotype.Repository;

/*
 * this class is abstract class implements IChannel that is intialized by Spring
 * channelid is set from Spring application Context
 */
@Repository
public abstract class ChannelDSCU implements IChannel {

    protected SessionResourceKit _srk;

    protected Deal _deal;

    protected Channel channel;

    protected String _url;

    protected int channelId;

    protected InstitutionProfile _institution;
    
    protected String sender;

    protected String receiver;

    private String eSBPostOffice_SocketTimetout;
    private String eSBPostOffice_HTTPConnectionTimetout;

    protected static Log _logger = LogFactory.getLog(ChannelDSCU.class);

    /*
     * <p>init 
     * @param SessionResouceKit srk
     * @param Deal               deal
     * @param String               payload
     * @throw RemoteException
     * @throw FinderException
     */
    public void init(SessionResourceKit srk, Deal deal)
            throws PostOfficeServiceException {
        _logger.debug(this.getClass().getName() + " Initialized");
        _srk = srk;
        setDeal(deal);

        try {
            setChannel(channelId);
            buildUrl();
            InstitutionProfile ip = new InstitutionProfile(srk);
            ip = ip.findByFirst();
            if (ip != null){
                InstitutionProfilePK institutionPk = new InstitutionProfilePK(deal.getInstitutionProfileId());
                _institution = ip.findByPrimaryKey(institutionPk);
                // introduced new sysproperty for 4.2GR
                String instanceName = PropertiesCache.getInstance().getProperty(this._deal.getInstitutionProfileId(),
                        "com.filogix.instance.name", "FXPGR");
                sender = "FILOGIX." + instanceName + "."  + _institution.getECNILenderId();
            }
            SystemType systemType = new SystemType(srk);
            systemType = systemType.findByPrimaryKey(new SystemTypePK(deal.getSystemTypeId()));
            String systemTypeStr = systemType.getChannelMask();
            SourceFirmProfile sfp = deal.getSourceFirmProfile();
            String sfpCode = sfp.getSourceFirmCode();

            SourceOfBusinessProfile sbp = deal.getSourceOfBusinessProfile();
            String sbpCode = sbp.getSourceOfBusinessCode();
            if (deal.getSystemTypeId() == 1) {
                if (sbpCode.length()<4) {
                    sbpCode = "";
                } else {
                    sbpCode = sbpCode.substring(3, sbpCode.length() < 12 ? sbpCode.length():12);
                }
            }
            
            receiver = systemTypeStr + "." + sfpCode + "." + sbpCode;
            
            initRequestBean();
        } catch (Exception e) {
            _logger.error("failed in init@ChannelGen: ", e);
            throw new PostOfficeServiceException(e);
        }
    }

    /**
     * <p> buildUrl
     * build String url from channel entity
     **/
    private void buildUrl() {

        if (_srk == null)
            return;
        if (channel == null)
            return;

        StringBuffer url = new StringBuffer();
        String protocol = channel.getProtocol();
        String ip = channel.getIp();
        String path = channel.getPath();

        if (protocol != null && !protocol.trim().equals("")) {
            url.append(protocol + "://");
        }

        if (ip != null && !ip.trim().equals("")) {
            url.append(ip);
        }

        if (channel.getPort() != 0) {
            url.append(":" + channel.getPort());
        }

        if (path != null && !path.trim().equals("")) {
            if (path.indexOf('\\') != -1) {
                path = path.replace('\\', '/');
            }
            if (!path.startsWith("/")) {
                url.append("/" + path);
            } else
                url.append(path);
        }

        if (channel.getWsServicePort() != null
                && channel.getWsServicePort().trim().length() > 0)
            url = url.append("/" + channel.getWsServicePort());

        this._url = url.toString();
        _logger.debug("chanel url: " + _url);
    }

    // Getters and Setters STARTS
    public Channel getChannel() {
        return channel;
    }

    public String getUrl() {
        return _url;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public void setChannel(int id) throws FinderException, RemoteException {
        if (_srk == null) {
            return;
        }

        channel = new Channel(_srk);
        channel = channel.findByPrimaryKey(new ChannelPK(id));
    }

    public Log getLogger() {
        return _logger;
    }

    public void setLogger(Log logger) {
        this._logger = logger;
    }

    public SessionResourceKit getSrk() {
        return _srk;
    }

    public void setSrk(SessionResourceKit srk) {
        this._srk = srk;
    }

    public Deal getDeal() {
        return _deal;
    }

    public void setDeal(Deal deal) {
        _deal = deal;
    }

    // Getters and Setters END

    /**
     * <p> abstract send
     * @throw PostOfficeServiceException
     **/
    public abstract Object send(String payload)
            throws PostOfficeServiceException;

    /**
     * <p> abstract initRequestBean
     * @throw PostOfficeServiceException
     **/
    public abstract void initRequestBean() throws PostOfficeServiceException;

    /**
     * @return the channelId
     */
    public int getChannelId() {
        return channelId;
    }
    
    // this method set extra soap headder including uuid.
    // uuid is generated by UUIDGenerator.getUUID() in wws4j 1.5.4
    // changed to get Action from spring
    public void setHeaders(Stub stub) throws PostOfficeServiceException {           

        _logger.debug("*** Setting SOAP Header *** ");
        ServiceClient client = stub._getServiceClient();
        client.addHeadersToEnvelope(null);

        try {
            Options options = client.getOptions();
            options.setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
            options.setProperty(org.apache.axis2.transport.http.HTTPConstants.HEADER_DEFAULT_CHAR_ENCODING, "UTF-8");
            options.setProperty(org.apache.axis2.Constants.Configuration.CHARACTER_SET_ENCODING, "UTF-8");
//            options.setProperty(org.apache.axis2.Constants.Configuration.CHARACTER_SET_ENCODING, "KOI8-R");

            Object timeoutO = ExpressOutboundContext.getService(Mc.REQUEST_HEADER_ESBPOSTOFFICE + Mc.TIMEOUT);
          options.setProperty(HTTPConstants.SO_TIMEOUT,new Integer(((String)timeoutO).substring(1))); 
          options.setProperty(HTTPConstants.CONNECTION_TIMEOUT,new Integer(((String)timeoutO).substring(1)));
          _logger.debug("TIMEOUT: " + ((String)timeoutO).substring(1));
          
            Object messageIdO = ExpressOutboundContext.getService(Mc.REQUEST_HEADER_ESBPOSTOFFICE + Mc.REQUEST_HEADER_MESSAGEID);
            RequestHeaderBlock messageIdRequestHeader = (RequestHeaderBlock)messageIdO;
            SOAPHeaderBlock messageHD = messageIdRequestHeader.getSOAPHeaderBlock();
            messageHD.setMustUnderstand(true);
            //this value is supposed to be String
            // FXP26510: change to dealid
//            String uuid = UUID.randomUUID().toString();
            messageHD.setText("" + _deal.getDealId());
            client.addHeader(messageHD);
            _logger.debug("MessageId: " + _deal.getDealId());

            setAction();
  
            Object toO = ExpressOutboundContext.getService(Mc.REQUEST_HEADER_ESBPOSTOFFICE + Mc.REQUEST_HEADER_TO);
            RequestHeaderBlock toRequestHeader = (RequestHeaderBlock)toO;
            SOAPHeaderBlock toHD = toRequestHeader.getSOAPHeaderBlock();
            toHD.setMustUnderstand(true);
            //this value is supposed to be String
            toHD.setText(_url);
            client.addHeader(toHD);
            _logger.debug("TO: " + _url);
  
            Object fromO =   ExpressOutboundContext.getService(Mc.REQUEST_HEADER_ESBPOSTOFFICE + Mc.REQUEST_HEADER_FROM);
            RequestHeaderBlock fromRequestHeader = (RequestHeaderBlock)fromO;
            SOAPHeaderBlock fromHD = fromRequestHeader.getSOAPHeaderBlock();
            fromHD.setMustUnderstand(true);
            
            Object fromAddressO = ExpressOutboundContext.getService(Mc.REQUEST_HEADER_ESBPOSTOFFICE + Mc.REQUEST_HEADER_FROMADDRESS);
            RequestHeaderBlock fromAddressRequestHeader = (RequestHeaderBlock)fromAddressO;
            SOAPHeaderBlock fromAddressHD = fromAddressRequestHeader.getSOAPHeaderBlock();
            fromAddressHD.setMustUnderstand(true);
            //this value is supposed to be String
            String originationURI = (String)fromAddressRequestHeader.getValue();
            fromAddressHD.setText(originationURI + _institution);
            fromHD.addChild(fromAddressHD);
            client.addHeader(fromHD);
            _logger.debug("TO: " + originationURI + _institution);
    
            Object sercurityO = ExpressOutboundContext.getService(Mc.REQUEST_HEADER_ESBPOSTOFFICE + Mc.REQUEST_HEADER_SECURITY);
            RequestHeaderBlock securityRequestHeader = (RequestHeaderBlock)sercurityO;
            SOAPHeaderBlock securityElement = securityRequestHeader.getSOAPHeaderBlock();
            securityElement.setMustUnderstand(true);
            // added. without this, security is added everytime. changed spring application context scope to Request but it didn't work.
            securityElement.setFirstChild(null);
            String prefixSecurity = securityRequestHeader.getPrefix();
            String nameSpaceSecurity = securityRequestHeader.getNamespace();
            
            String userName = channel.getUserId().trim();
            String password = channel.getPassword().trim();
            SOAPHeaderBlock usernameTokenElement = RequestHeaderBlock.getRequestHeaderBlock(nameSpaceSecurity, "UsernameToken", prefixSecurity);
            usernameTokenElement.setMustUnderstand(true);
            SOAPHeaderBlock usernameElement = RequestHeaderBlock.getRequestHeaderBlock(nameSpaceSecurity, "Username", prefixSecurity, userName);
            usernameElement.setMustUnderstand(true);
            SOAPHeaderBlock passwordElement = RequestHeaderBlock.getRequestHeaderBlock(nameSpaceSecurity, "Password", prefixSecurity, password);
            passwordElement.setMustUnderstand(true);
            usernameTokenElement.addChild(usernameElement);
            usernameTokenElement.addChild(passwordElement);
            
            securityElement.addChild(usernameTokenElement);
            client.addHeader(securityElement);
            _logger.debug("username: " + usernameElement);
            _logger.debug("password: " + passwordElement);
    
            Object requestO = ExpressOutboundContext.getService(Mc.REQUEST_HEADER_ESBPOSTOFFICE + Mc.REQUEST_HEADER_REQUEST);
            RequestHeaderBlock requestRequestHeader = (RequestHeaderBlock)requestO;
            String prefixRequest = requestRequestHeader.getPrefix();
            String nameSpaceRequest = requestRequestHeader.getNamespace();
            SOAPHeaderBlock requestElement = requestRequestHeader.getSOAPHeaderBlock();
            
            SOAPHeaderBlock requestSender = RequestHeaderBlock.getRequestHeaderBlock(nameSpaceRequest, "request", prefixRequest);
            SOAPHeaderBlock senderElement = RequestHeaderBlock.getRequestHeaderBlock(nameSpaceRequest, "sender", prefixRequest,  sender);
            SOAPHeaderBlock receiverElement = RequestHeaderBlock.getRequestHeaderBlock(nameSpaceRequest, "receiver", prefixRequest, receiver);

            SOAPHeaderBlock dealIdElement = RequestHeaderBlock.getRequestHeaderBlock(
                    nameSpaceRequest, "dealID", prefixRequest, ""+_deal.getSourceApplicationId());
            _logger.debug("sender: " + sender);
            _logger.debug("receiver: " + receiver);
            _logger.debug("dealI: " + _deal.getDealId());

            senderElement.setNamespace(null);
            receiverElement.setNamespace(null);
            dealIdElement.setNamespace(null);
            requestSender.addChild(senderElement);
            requestSender.addChild(receiverElement);
            requestSender.addChild(dealIdElement);
            client.addHeader(requestSender);
            
//            OMElement senderElement = factory.createOMElement(new QName("sender"));
//            senderElement.addTextNode("FILOGIX.FXP." + sender);
//            SOAPElement receiverElement = factory.createElement("receiver");
//            receiverElement.addTextNode("FILOGIX." + receiver);
//            SOAPElement dealIdElement = factory.createElement("dealId");
//            dealIdElement.addTextNode( _deal.getSourceApplicationId());
            

//              Object referenceO = ExpressDscuContext.getService(Mc.REQUEST_HEADER_ESBPOSTOFFICE + Mc.REQUEST_HEADER_REFERENCEPARAMATERS);
//              RequestHeaderBlock referenceParametersRequestHeader = (RequestHeaderBlock)referenceO;
//              SOAPHeaderBlock referenceParametersElement = referenceParametersRequestHeader.getSOAPHeaderBlock();
//          
//              Object businessO = ExpressDscuContext.getService(Mc.REQUEST_HEADER_ESBPOSTOFFICE + Mc.REQUEST_HEADER_BUSINSSSTRANSACTIONID);
//              RequestHeaderBlock businsessTransactionRequestHeader = (RequestHeaderBlock)businessO;
//              SOAPHeaderBlock businsessTransactionElement = businsessTransactionRequestHeader.getSOAPHeaderBlock();
//              businsessTransactionElement.setText(_deal.getSourceApplicationId());
//              referenceParametersElement.addChild(businsessTransactionElement);
//              client.addHeader(referenceParametersElement);

              
        } catch (Exception soape) {
            _logger.error("failed to set header@ConditionUpdateChannel: ", soape);
            throw new PostOfficeServiceException(soape);
        }
    }

    public String getSender() {
        return sender;
    }

    public String getReceiver() {
        return receiver;
    }
    
    public abstract void setAction();

}
