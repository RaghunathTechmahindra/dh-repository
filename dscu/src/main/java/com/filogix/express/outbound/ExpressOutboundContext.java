package com.filogix.express.outbound;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.filogix.express.context.ExpressContext;
import com.filogix.express.context.ExpressServiceException;
import com.filogix.express.context.ExpressContextDirector;

/**
 * ExpressOutboundContext provides the unified interfaces to access services in
 * Express ESB Outbound context.
 * 
 */
public class ExpressOutboundContext implements ExpressContext {

    // The logger
    private final static Log _logger = LogFactory
            .getLog(ExpressOutboundContext.class);

    // the key for the dscu service factory.
    public final static String EXPRESS_OUTBOUND_CONTEXT_KEY =
        "com.filogix.express.outbound";
    /**
     * returns a service instance from Express DCSU context.
     * 
     * @param name
     *            the service name.
     * @return an instance of the service.
     * @throw ExpressServiceException if failed to get an instance for the
     *        service.
     */
    synchronized public static Object getService(String name)
            throws ExpressServiceException {

        return ExpressContextDirector.getDirector().getServiceFactory(
                EXPRESS_OUTBOUND_CONTEXT_KEY).getService(name);
    }
}