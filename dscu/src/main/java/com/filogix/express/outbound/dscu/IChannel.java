package com.filogix.express.outbound.dscu;

import com.basis100.deal.entity.Deal;
import com.basis100.resources.SessionResourceKit;

/**
 * <p>IChannel Interface</p>
 * <p>Spring proxy for Channel classes</p>
 * @author Maida
 * @version 1.0 - April 15, 2009
 */
public interface IChannel {
        
    public void init(SessionResourceKit srk, Deal deal)
        throws PostOfficeServiceException;

    public Object send(String payload) throws PostOfficeServiceException;
    
    public void setChannelId(int id);

}
