package com.filogix.express.outbound.loanDecision.payload;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.entity.FinderException;
import com.filogix.express.core.ExpressRuntimeException;
import com.filogix.schema.fcx._1.MortgageType;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.MortgageType.Component;
import com.filogix.schema.fcx._1.MortgageType.LenderSubmission;
import com.filogix.schema.fcx._1.MortgageType.MtgProd;
import com.filogix.schema.fcx._1.MortgageType.PricingRateInventory;
import com.filogix.schema.fcx._1.MortgageType.Rate;

public class MortgageTypeGenerator {

	private final static Logger logger =
		LoggerFactory.getLogger(MortgageTypeGenerator.class);
	
	private static final int MAX_LENGTH_MTG_NUMBER = 15;
	private static final double MAX_CASHBACKPERCENTAGE = 999.99d;
	
	public static final int MORTGAGE_TYPE_FIRST = 1;
	public static final int MORTGAGE_TYPE_SECOND = 2;
	public static final int MORTGAGE_TYPE_THIRD = 3;

	public static MortgageType createMortgageType(Deal deal,
			ObjectFactory factory) {

		//MortgageType
		MortgageType mortgageType = factory.createMortgageType();
		populateMortgageType(mortgageType, deal);

		//MortgageType/LenderSubmission
		LenderSubmission lenderSubmission = factory.createMortgageTypeLenderSubmission();
		mortgageType.setLenderSubmission(lenderSubmission);
		MasterDeal masterDeal = null;
		try {
			masterDeal = new MasterDeal(deal.srk, null, deal.getDealId());
		} catch (Exception e) {
			throw new ExpressRuntimeException(e);
		}
		populatelenderSubmission(lenderSubmission, masterDeal);


		//MortgageType/PricingRateInventory
		PricingRateInventory mortgageTypePricingRateInventory = factory.createMortgageTypePricingRateInventory();
		mortgageType.setPricingRateInventory(mortgageTypePricingRateInventory);
		com.basis100.deal.entity.PricingRateInventory pricingRateInventory;
		try {
			pricingRateInventory = deal.getPricingRateInventory();
		} catch (Exception e) {
			throw new ExpressRuntimeException(e);
		}
		populatePricingRateInventory(mortgageTypePricingRateInventory, pricingRateInventory);


		//MortgageType/Rate
		Rate mortgageTypeRate = factory.createMortgageTypeRate();
		mortgageType.setRate(mortgageTypeRate);
		populateMortgageTypeRate(mortgageTypeRate, deal);

		//MortgageType/Components
		List<Component> listOfComponents = mortgageType.getComponent();
		ComponentsListGenerator.createComponentsList(listOfComponents, deal, factory);

		//MortgageType/MtgProd
		MtgProd mtgProd = factory.createMortgageTypeMtgProd();
		mortgageType.setMtgProd(mtgProd);
		populateMtgProd(mtgProd, deal);
		
		return mortgageType;

	}

	private static void populateMtgProd(MtgProd fcx, Deal deal) {
		
		try {
			com.basis100.deal.entity.MtgProd db = deal.getMtgProd();
			if(db == null) return;
			fcx.setMpBusinessId(db.getMPBusinessId());
		} catch (Exception e) {
			throw new ExpressRuntimeException(e);
		}
	}

	protected static void populateMortgageTypeRate(Rate fcx, Deal db) {

		fcx.setBuyDownRate(Convert.toBigDecimal(db.getBuydownRate()));
		fcx.setDiscount(Convert.toBigDecimal(db.getDiscount()));
		fcx.setInterestRate(Convert.toBigDecimal(db.getNetInterestRate()));
		fcx.setNetRate(Convert.toBigDecimal(db.getNetInterestRate()));
		fcx.setPremium(Convert.toBigDecimal(db.getPremium()));
	}

	protected static void populatePricingRateInventory(
			PricingRateInventory pricingRateInventory, com.basis100.deal.entity.PricingRateInventory db){

		pricingRateInventory.setIndexEffectiveDate(Convert.toXMLGregorianCalendar(db.getIndexEffectiveDate()));
	}

	protected static void populateMortgageType(MortgageType fcx, Deal db){

		fcx.setDealId(Convert.toBigDecimal(db.getSourceApplicationVersion()));
		fcx.setActualPaymentTerm(Convert.toBigDecimal(db.getActualPaymentTerm()));
		fcx.setAffiliationProgramDd(Convert.toBigDecimal(db.getAffiliationProgramId()));
		fcx.setAmortizationTerm(Convert.toBigDecimal(db.getAmortizationTerm()));
		fcx.setCashBackAmt(Convert.toBigDecimal(db.getCashBackAmount()));
		//has to be rounded. FCX=5,2 Express=12,6 + PCR(limit max: 999.99)
		fcx.setCashBackPercentage(Convert.toBigDecimal(Math.min(db.getCashBackPercent(), MAX_CASHBACKPERCENTAGE), 2));
		fcx.setCurrentMortgageNumber(StringUtils.substring(StringUtils.trim(db.getOriginalMortgageNumber()), 0, MAX_LENGTH_MTG_NUMBER));
		fcx.setInterestCompoundDd(Convert.toBigDecimal(db.getInterestCompoundId()));
		fcx.setRepaymentTypeDd(Convert.toBigDecimal(db.getRepaymentTypeId()));
		fcx.setInterestTypeDd(Convert.toBigDecimal(db.getInterestTypeId()));//EXP26981
		fcx.setLoanTypeDd(Convert.toBigDecimal(convertLoanTypeId(db.getProductTypeId())));
		fcx.setMiReferenceNumber(db.getMIPolicyNumber());
		fcx.setMortgageInsurerId(Convert.toString(db.getMortgageInsurerId()));
		fcx.setMortgageTypeDd(Convert.toBigDecimal(convertMortgageTypeId(db.getLienPositionId())));
	
		fcx.setMtgProductId(Convert.toBigDecimal(db.getMITypeId()));

		if(db.getMortgageInsurerId() != Mc.MI_INSURER_BLANK){
			fcx.setMtgProviderId(Convert.toBigDecimal(db.getMortgageInsurerId()));
		}
	
		fcx.setNetLoanAmount(Convert.toBigDecimal(db.getNetLoanAmount()));
		fcx.setPAndIPaymentAmount(Convert.toBigDecimal(db.getPandiPaymentAmount()));
		fcx.setPaymentFrequencyDd(Convert.toBigDecimal(db.getPaymentFrequencyId()));
		try {
			fcx.setPaymentTermDd(Convert.toBigDecimal(db.getPaymentTermTypeId()));
		} catch (FinderException e) {
			logger.warn("PaymentTermTypeId wasn't found", e);
		}
		fcx.setRateGuaranteeLength(Convert.toBigDecimal(db.getRateGuaranteePeriod()));
		fcx.setPrivilegePaymentDd(Convert.toBigDecimal(db.getPrivilegePaymentId()));
		//SingleProgressiveTypeDd: expert has to this convert numeric value to string
		fcx.setSingleProgressiveTypeDd(Convert.toBigDecimal(db.getProgressAdvanceTypeId()));
		fcx.setTotalLoanAmount(Convert.toBigDecimal(db.getTotalLoanAmount()));
		fcx.setMarketSubmission(db.getMccMarketType());
		
		fcx.setMiFeeAmount(Convert.toBigDecimal(db.getMIFeeAmount()));
		fcx.setMiPremiumAmount(Convert.toBigDecimal(db.getMIPremiumAmount()));
		fcx.setMiPremiumPst(Convert.toBigDecimal(db.getMIPremiumPST()));
		

	}

	protected static void populatelenderSubmission(LenderSubmission fcx, MasterDeal md)	{
		fcx.setRoutingReceiver(md.getFXLinkPOSChannel());
		fcx.setRoutingSender(md.getFXLinkUWChannel());
	}

	public static final int PRODUCT_TYPE_MORTGAGE_EX = 0;
	public static final int PRODUCT_TYPE_SECURED_LOC_EX = 1;
	public static final int PRODUCT_TYPE_MULTIPLE_COMPONENT_EX = 2;

	protected static int convertLoanTypeId(int productTypeId){
		if(productTypeId == Mc.PRODUCT_TYPE_MORTGAGE){
			return PRODUCT_TYPE_MORTGAGE_EX;
		}else if(productTypeId == Mc.PRODUCT_TYPE_SECURED_LOC){
			return PRODUCT_TYPE_SECURED_LOC_EX;
		}else if(productTypeId == Mc.PRODUCT_TYPE_MULTIPLE_COMPONENT){
			return PRODUCT_TYPE_MULTIPLE_COMPONENT_EX;
		} 
		return productTypeId;
	}
	
	/**
	 * FXP31358 change mortgageTypeDd mapping 
	 * @param lienPositionTypeId
	 * @return
	 */
	protected static int convertMortgageTypeId(int lienPositionTypeId){
		if(lienPositionTypeId == Mc.LIEN_POSITION_FIRST){
			return MORTGAGE_TYPE_FIRST;
		}else if(lienPositionTypeId == Mc.LIEN_POSITION_SECOND){
			return MORTGAGE_TYPE_SECOND;
		}else if(lienPositionTypeId == Mc.LIEN_POSITION_FORTH){
			return MORTGAGE_TYPE_THIRD;
		} 
		return lienPositionTypeId;
	}
}
