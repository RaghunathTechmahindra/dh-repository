package com.filogix.express.outbound.loanDecision;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.ArchivedLoanDecision;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.pk.DealPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.email.EmailSender;
import com.filogix.express.outbound.AbstractOutboundProcessor;
import com.filogix.express.outbound.ExpressOutboundContext;
import com.filogix.express.outbound.OutboundProcessException;
import com.filogix.express.outbound.loanDecision.payload.IDAPayloadGenerator;
import com.filogix.express.outbound.util.OutboundConst;
import com.filogix.express.outbound.util.OutboundUtil;
import com.filogix.externallinks.framework.ServiceConst;

public class DataAlignmentProcessor extends AbstractOutboundProcessor
{

	private final static Logger logger =
		LoggerFactory.getLogger(DataAlignmentProcessor.class);

	private String support;
	public boolean debugmode; 

	private SessionResourceKit srk;
	private IDAChannel linkChannel; //send to link for transmission

	/**
	 * method handle processing send payload to ESB, handle result form ESB
	 *
	 * @param obj
	 * @return
	 * @throws JdbcTransactionException 
	 * @throws RemoteException 
	 * @throws OutboundProcessException 
	 * @throws Exception
	 */
	public void handleProcess(Object obj) throws JdbcTransactionException, RemoteException, OutboundProcessException
	{
		ESBOutboundQueue loanDecision = (ESBOutboundQueue) obj;
		int attemptCounter = OutboundUtil.getMaxAttemptValue(loanDecision);
		String payloadXml;

		int outboundQueueId = loanDecision.getESBOutboundQueueId();

		try
		{

			DealPK dpk = new DealPK(loanDecision.getDealId(), -1);
			Deal deal = new Deal(srk, null);
			deal = deal.findByRecommendedScenario(dpk, true);

			//in theory, this should be enough for the production support guys to begin a trace
			logger.info(">DataAlignmentProcessor[" + outboundQueueId + "] begin processing - "  
					+ "DealId: " + deal.getDealId() 
					+ ", InsitutionProfileId: " + deal.getInstitutionProfileId());

			IDAPayloadGenerator payloadGenerator = (IDAPayloadGenerator) ExpressOutboundContext
			.getService(OutboundConst.SERVICESUBTYPE_PREFIX + loanDecision.getServiceSubTypeId());

			payloadXml = payloadGenerator.getXmlPayload(deal);

			logger.info(">DataAlignmentProcessor[" + outboundQueueId 
					+ "] payload generated, sending to link"); 
			
			logger.debug(payloadXml); //just for test ...
			
			linkChannel.init(srk, deal, outboundQueueId);
			linkChannel.send(payloadXml);

			logger.info(">DataAlignmentProcessor[" + outboundQueueId 
					+ "] payload sent, archiving");

			int archiveid = archiveTransaction(deal, payloadXml);
			logger.info(">DataAlignmentProcessor[" + outboundQueueId 
					+ "] payload archived, archiveId: "	+ archiveid); 

			// set both status lines to success

			loanDecision.setTransactionStatusId(ServiceConst.ESBOUT_TRANSACTIONSTATUS_SUCCESS);
			loanDecision.setTransactionStatusDescription("Successfully submitted to FXLink");
			loanDecision.ejbStore();

			if(!debugmode) 
			{
				loanDecision.ejbRemove();
			}
		}
		catch (DARecoverableException re)
		{	
			logger.error("(Theoretically) recoverable error in DataAlignmentProcessor, QueueId: " + outboundQueueId, re);

			loanDecision.setTransactionStatusId(ServiceConst.ESBOUT_TRANSACTIONSTATUS_AVALIABLE);
			loanDecision.setTransactionStatusDescription(StringUtils.substring(
					re.getMessage(), 0, OutboundConst.ESBOUTBOUNDQUEUE_TRANSACTIONSTATUSDESC_MAX));
			loanDecision.setAttemptCounter(loanDecision.getAttemptCounter() + 1);
			loanDecision.ejbStore();

			if (loanDecision.getAttemptCounter() > attemptCounter)
			{
				//log counter lapsing
				String counterMsg = "Retry counter elapsed for deal " + loanDecision.getDealId();
				logger.info(counterMsg);
				EmailSender.sendAlertEmail("ESBOutbound.DataAlignmentProcessor", re, null, counterMsg);
			}
		}
		catch (Throwable t) //changed from unrecoverable, catches pretty much everything as unrecoverable
		{
			String msg = "Unrecoverable error in DataAlignmentProcessor, QueueId: " + outboundQueueId;
			logger.error(msg, t);

			loanDecision.setTransactionStatusId(ServiceConst.ESBOUT_TRANSACTIONSTATUS_SYSTEMERROR);
			loanDecision.setTransactionStatusDescription(StringUtils.substring(
					t.getMessage(), 0, OutboundConst.ESBOUTBOUNDQUEUE_TRANSACTIONSTATUSDESC_MAX));
			loanDecision.ejbStore();

		}

	}

	int archiveTransaction(Deal deal, String payload) throws DAUnrecoverableException 
	{
		try {
			ArchivedLoanDecision archive = new ArchivedLoanDecision(srk);

			archive.create(deal.getDealId(), deal.getCopyId(), deal.getSourceApplicationId());
			archive.setInstProfileId(deal.getInstitutionProfileId());
			archive.setFCXLoanDecision(payload);
			archive.setSubmissionDateTimeStamp(new Date());

			//store the archive
			archive.ejbStore();

			return archive.getArchivedLoanDecisionId();

		} catch (RemoteException e) {
			throw new DAUnrecoverableException(e);
		} catch (FinderException e) {
			throw new DAUnrecoverableException(e);
		} catch (CreateException e) {
			throw new DAUnrecoverableException(e);
		}
	}

	public void setSessionResourceKit(SessionResourceKit srk)
	{
		this.srk = srk;
	}

	public SessionResourceKit getSrk()
	{
		return srk;
	}

	public void setLinkChannel(IDAChannel channel)
	{
		this.linkChannel = channel;
	}

	public void setDebugmode(boolean debugmode) 
	{
		this.debugmode = debugmode;
	}

	public String getSupport()
	{
		return support; 
	}

	public void setSupport(String support)
	{
		this.support = support;
	}

}
