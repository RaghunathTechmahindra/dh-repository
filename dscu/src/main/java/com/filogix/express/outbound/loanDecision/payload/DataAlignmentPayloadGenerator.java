package com.filogix.express.outbound.loanDecision.payload;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.Deal;
import com.filogix.express.outbound.loanDecision.DAUnrecoverableException;
import com.filogix.schema.fcx._1.LoanDecisionType;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.ResponseType;

public abstract class DataAlignmentPayloadGenerator implements
		IDAPayloadGenerator {

	private final static Logger logger = LoggerFactory
			.getLogger(DataAlignmentPayloadGenerator.class);

	public static final String SCHEMA_PACKAGE = "com.filogix.schema.fcx._1";

	public String getXmlPayload(Deal deal) throws DAUnrecoverableException {
		try {
			JAXBContext jc = JAXBContext.newInstance(SCHEMA_PACKAGE);
			ObjectFactory factory = new ObjectFactory();

			LoanDecisionType loanDecisionType = 
					LoanDecisionTypeGenerator.createLoanDecisionType(deal, factory);
			
			ResponseType responseType = loanDecisionType.getResponse();
			
			populateResponseTypeSpecific(responseType, deal, factory);
			
			JAXBElement<LoanDecisionType> loanDecision = 
					factory.createLoanDecision(loanDecisionType);
			
			
			Marshaller marshaller = jc.createMarshaller();
			StringWriter xmlWriter = new StringWriter();
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
					Boolean.FALSE);
			marshaller.marshal(loanDecision, xmlWriter);

			return xmlWriter.toString();
		} catch (RuntimeException e){
			String msg = "Error occured during generating Payload : " + e.getMessage();
			logger.error(msg, e);
			throw new DAUnrecoverableException(msg, e);
		} catch (JAXBException e) {
			String msg = "Error occured during generating Payload : " + e.getMessage();
			logger.error(msg, e);
			throw new DAUnrecoverableException(msg, e);
		}
	}

	protected abstract void populateResponseTypeSpecific(
			ResponseType responseType, Deal deal, ObjectFactory factory);

}
