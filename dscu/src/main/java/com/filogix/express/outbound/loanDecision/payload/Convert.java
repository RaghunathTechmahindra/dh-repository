package com.filogix.express.outbound.loanDecision.payload;

import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.StringUtils;

import com.filogix.express.core.ExpressRuntimeException;

public abstract class Convert {
	
	/**
	 * Given a java.util.Date, generate the XMLGregorianCalendar.
	 * @param date
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	public static XMLGregorianCalendar toXMLGregorianCalendar(Date date){
		if(date == null)return null;
		DatatypeFactory df;
		try {
			df = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			throw new ExpressRuntimeException(e);
		}
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		return df.newXMLGregorianCalendar(gc);
	}
	
	public static BigDecimal toBigDecimal(int arg){
		return new BigDecimal(arg);
	}
	
	public static BigDecimal toBigDecimal(double arg){
		// we should not directory convert from double
		// because rounding error might happen
		return toBigDecimal(String.valueOf(arg));
	}
	
	public static BigDecimal toBigDecimal(double arg, int roundScale){
		// we should not directory convert from double
		// because rounding error might happen
		return toBigDecimal(String.valueOf(arg), roundScale);
	}
	
	public static BigDecimal toBigDecimal(String arg){
		arg = StringUtils.trimToNull(arg);
		if(arg == null) return null;
		
		return new BigDecimal(arg);
	}

	public static BigDecimal toBigDecimal(String arg, int roundScale){
		arg = StringUtils.trimToNull(arg);
		if(arg == null) return null;
		BigDecimal bd = toBigDecimal(arg);
		return bd.setScale(roundScale, BigDecimal.ROUND_HALF_UP);
	}
	
	public static String toTrimedString(String arg){
		return StringUtils.trimToNull(arg);
	}
	
	public static String toString(int arg){
		return String.valueOf(arg);
	}
	
	public static String toCleanDocumentText(String arg){
		
		arg = StringUtils.trimToNull(arg);
		if(arg == null) return arg;
		Character c = new Character('\u00B6');
		return arg.replace(c.toString(), "\r\n");
	}
	
}
