package com.filogix.express.outbound.dscu;

import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.basis100.xml.XercesDocumentBuilder;

/**
 * PostOfficeServiceException  
 *
 */
public class PostOfficeServiceException extends Exception {

    private int faultCode = 0;
    private String faultDescription = "";
    private static DocumentBuilder parser = new XercesDocumentBuilder();
    /**
     * ConditionProcessException Constructor
     */
    public PostOfficeServiceException() {

        super();
    }

    /**
     * @param message
     */
    public PostOfficeServiceException(String message) {

        super(message);
    }

    /**
     * @param message
     * @param tr
     */
    public PostOfficeServiceException(String message, Throwable tr) {

        super(message, tr);
    }

    /**
     * @param tr
     */
    public PostOfficeServiceException(Throwable tr) {

        super(tr);
    }

    public int getFaultCode() {
        return faultCode;
    }

    public void setFaultCode(int faultCode) {
        this.faultCode = faultCode;
    }

    public String getFaultDescription() {
        return faultDescription;
    }

    public void setFaultDescription(String faultDescription) {
        this.faultDescription = faultDescription;
    }
    
    public void setFaultDetails(String response) throws PostOfficeServiceException{
        
        Reader reader = new StringReader(response);
        try {
            Document faultResponse = parser.parse(new InputSource(reader));
            
            NodeList faultCodeList = faultResponse.getElementsByTagName("faultcode");
            Node faultCodeNode = faultCodeList.item(0);
            String faultCodeStrWhole = faultCodeNode.getTextContent();
            String fautlCodeStr  = faultCodeStrWhole.substring(faultCodeStrWhole.lastIndexOf('.')+1);
            faultCode = Integer.parseInt(fautlCodeStr);
            
            NodeList faultStringList = faultResponse.getElementsByTagName("faultstring");
            Node faultStringNode = faultStringList.item(0);
            faultDescription = faultStringNode.getTextContent();
            
        } catch (Exception e) {
            throw this;
        }
    }
    
    public static void main (String[] args) throws Exception{
            
        PostOfficeServiceException poe = new PostOfficeServiceException();
        poe.setFaultDetails(testString);
    }
    
    static String testString = 
    "<?xml version = \"1.0\" encoding = \"UTF-8\" ?>" +
    "<soap:Envelope xmlns:fo=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
      "<soap:Body>" +
        "<soap:Fault>" +
          "<faultcode>ConditionUpdate.452</faultcode>" +
          "<faultstring>payload did not pass XML schema validation</faultstring>" +
          "<detail>" +
            "<transaction>"+
              "<transactionID>189800</transactionID>" +
              "<transactionClient>127.0.0.1</transactionClient> " +
              "<clientURL>http://localhost:4051/ConditionUpdate</clientURL>" +
              "<processorName>Send_1_0</processorName>" +
              "<processorType>XML Firewall</processorType>" +
            "</transaction>" +
            "<error>" +
              "<errorCode>0x00230001</errorCode>" +
              "<errorHeaders/>" +
              "<errorMessage>http://dynamic:80/ConditionUpdate: cvc-simple-type 1: element {http://www.filogix.com/Schema/PostOffice/1}conditionTypeId value '5' is not a valid instance of the element type</errorMessage>" +
              "<errorProtocolReasonPhrase>Internal Server Error</errorProtocolReasonPhrase>" +
              "<errorProtocolResponse>452</errorProtocolResponse>" +
              "<errorSubcode>0x01d30003</errorSubcode>" +
            "</error>" +
          "</detail>" +
        "</soap:Fault>" +
      "</soap:Body>" +
    "</soap:Envelope>";
    
}
