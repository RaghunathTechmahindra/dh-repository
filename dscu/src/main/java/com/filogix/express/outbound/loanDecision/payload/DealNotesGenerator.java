package com.filogix.express.outbound.loanDecision.payload;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import MosSystem.Mc;

import com.basis100.deal.docprep.DocPrepLanguage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealNotes;
import com.basis100.picklist.BXResources;
import com.filogix.express.core.ExpressRuntimeException;
import com.filogix.schema.fcx._1.DealNotesType;
import com.filogix.schema.fcx._1.ObjectFactory;

public class DealNotesGenerator {
	
	public static final int MAX_LENGTH_DEALNOTE = 4000;
	public static final String DEALNOTE_CRLF = "\r\n";
	
	public static void populateDealNotesListWithDenialReasonAndFirstQualifiedNotes(List<DealNotesType> dealNotesList, 
			Deal deal, ObjectFactory factory) {
		
		DealNotesType dealNotesType = factory.createDealNotesType();
		int languageId = getPrimaryBorrowersLanguagePreference(deal);
		
		//Denial reason based on denial reason ID.
		String notesText = BXResources.getPickListDescription(deal.getInstitutionProfileId(), "DENIALREASON", deal.getDenialReasonId(), languageId);
		
		try {
			DealNotes dnFinder = new DealNotes(deal.getSessionResourceKit());
			DealNotes dealNotes = dnFinder.findLastDealNotesByDealCategory(deal.getDealId(), 
					languageId, Mc.DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS);
			//If there is no deal notes that satisfies our criteria then 
			//only denial reason text is sent back.
			if(dealNotes == null) {
				dealNotesType.setText(StringUtils.substring(notesText, 0, MAX_LENGTH_DEALNOTE));
			} else { //Deal notes exist.
				notesText += " " + dealNotes.getDealNotesText();
				dealNotesType.setCategoryDd(Convert.
						toBigDecimal(Mc.DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS));
				dealNotesType.setText(StringUtils.substring(notesText, 0, MAX_LENGTH_DEALNOTE));
				dealNotesType.setEntryDate(Convert.
						toXMLGregorianCalendar(dealNotes.getDealNotesDate()));
				dealNotesType.setLanguageDd(Convert.toBigDecimal(languageId));
			}
			dealNotesList.add(dealNotesType);
		} catch (Exception e) {
			throw new ExpressRuntimeException(e);
		}
	}
	

	
	public static void populateDealNotesListWithICNotes(List<DealNotesType> dealNotesList, 
			Deal deal, ObjectFactory factory) {
		
		DealNotesType dealNotesType = factory.createDealNotesType();
		dealNotesList.add(dealNotesType);
		
		try {
			Collection<DealNotes> dealNotes = deal.getDealNotes();
			//If there is no deal notes that satisfies our criteria then don't populate.
			if (dealNotes == null || dealNotes.size() == 0) {
				return;
			}
			
			int languageId = getPrimaryBorrowersLanguagePreference(deal);

			Iterator<DealNotes> dealNotesIt = dealNotes.iterator();
			StringBuffer result = new StringBuffer();
			while(dealNotesIt.hasNext() && result.length() < MAX_LENGTH_DEALNOTE) {
				DealNotes curDealNotes = dealNotesIt.next();
				if(curDealNotes.getLanguagePreferenceId() == languageId) {
					String dealNotesText = curDealNotes.getDealNotesText();
					if(dealNotesText.contains("IC-")) {
						result.append(dealNotesText, 0, 
								Math.min(MAX_LENGTH_DEALNOTE - (result.length() + DEALNOTE_CRLF.length()), dealNotesText.length()));
						result.append(DEALNOTE_CRLF);
					}
				}
			}
			if(result.length() > 0){
				dealNotesType.setText(result.toString());
			}
			
		} catch (Exception e) {
			throw new ExpressRuntimeException(e);
		}
	}

	/**
	 * @param deal
	 * @return
	 */
	protected static int getPrimaryBorrowersLanguagePreference(Deal deal) {
		DocPrepLanguage dpl = DocPrepLanguage.getInstance();
		int languageId = dpl.findPreferredLanguageId(deal, deal.getSessionResourceKit());
		return languageId;
	}



	public static void populateDealNotesListWithFirstQualifiedNotes(
			List<DealNotesType> dealNotesList, Deal deal, ObjectFactory factory) {
		DealNotesType dealNotesType = factory.createDealNotesType();
		int languageId = getPrimaryBorrowersLanguagePreference(deal);
		
		//Denial reason based on denial reason ID.
		String notesText;
		
		try {
			DealNotes dnFinder = new DealNotes(deal.getSessionResourceKit());
			DealNotes dealNotes = dnFinder.findLastDealNotesByDealCategory(deal.getDealId(), 
					languageId, Mc.DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS);
			//If there is no deal notes that satisfies our criteria then 
			//then we do not return anything.
			if(dealNotes == null) {
				return;
			} else { //Deal notes exist.
				notesText = dealNotes.getDealNotesText();
				dealNotesType.setCategoryDd(Convert.
						toBigDecimal(Mc.DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS));
				dealNotesType.setText(StringUtils.substring(notesText, 0, MAX_LENGTH_DEALNOTE));
				dealNotesType.setEntryDate(Convert.
						toXMLGregorianCalendar(dealNotes.getDealNotesDate()));
				dealNotesType.setLanguageDd(Convert.toBigDecimal(languageId));
			}
			dealNotesList.add(dealNotesType);
		} catch (Exception e) {
			throw new ExpressRuntimeException(e);
		}
		
	}
}
