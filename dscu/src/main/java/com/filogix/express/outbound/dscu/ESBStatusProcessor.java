package com.filogix.express.outbound.dscu;

import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;

import com.basis100.deal.docprep.DocPrepException;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.AbstractOutboundProcessor;
import com.filogix.express.outbound.OutboundProcessException;
import com.filogix.express.outbound.util.OutboundUtil;
import com.filogix.externallinks.framework.ServiceConst;

/**
 * ESBStatusProcessor 
 * 
 * @version 1.0 Jun 9, 2009
 */
public abstract class ESBStatusProcessor extends AbstractOutboundProcessor {

    // the logger
	private final static Log _log = LogFactory.getLog(ESBStatusProcessor.class);
	
    protected SessionResourceKit srk;

    public IPayloadGenerator payloadGenerator;
    
    public IChannel channel;
    
    public boolean debugmode;
    
    public String support;
    
    
    /*
     * set SessionResourceKit
     */
    public void setSessionResourceKit(SessionResourceKit srk) {
        this.srk = srk;
    }

    /*
     * get SessionResourceKit
     */
    public SessionResourceKit getSrk() {
        return srk;
    }


    /* 
     * method handle process between express and esb
     * 
     */
    public abstract void handleProcess(Object obj) throws Exception;

    /**
     * return PayloadGenerator
     */
    public IPayloadGenerator getPayloadGenerator() {
        return payloadGenerator;
    }

    /**
     * set payloadGenerator
     */
    public void setPayloadGenerator(IPayloadGenerator payloadGenerator) {
        this.payloadGenerator = payloadGenerator;
    }

	/**
	 * get channel
	 */
	public IChannel getChannel() {
		return channel;
	}

	/**
	 * set channel
	 */
	public void setChannel(IChannel channel) {
		this.channel = channel;
	}

    
    /**
     * handle failure message from data power
     * 
     * @param deal
     * @param queue
     * @throws RemoteException
     * @throws FinderException
     * @throws DocPrepException
     * @throws OutboundProcessException 
     */
    public void handleFailureMessage(int code, String desc, Deal deal, ESBOutboundQueue queue)
            throws DocPrepException, FinderException, RemoteException,
            OutboundProcessException {
        OutboundUtil.changeProcessStatus(queue, code, desc, deal
                .getSessionResourceKit());
        sendEmailNotification(deal, queue, code);
    }
    
    
    /**
     * send email notification
     * 
     * @param deal
     * @param queue
     * @throws DocPrepException
     * @throws FinderException
     * @throws RemoteException
     */
    public void sendEmailNotification(Deal deal, ESBOutboundQueue queue,
            int code) throws DocPrepException, FinderException, RemoteException {

        SessionResourceKit srk = deal.getSessionResourceKit();
        String newLine = System.getProperty("line.separator");
        StringBuffer emailText = new StringBuffer();
        String emailSubject = "";

        if (code == ServiceConst.ESBOUT_TRANSACTIONSTATUS_TIMEOUT) {
            emailSubject = BXResources.getSysMsg("CONDITION_EMAILSUBJECT_TIMEOUT",
                    Mc.LANGUAGE_PREFERENCE_ENGLISH);
            emailText.append(BXResources.getSysMsg("ESB_TIMEOUTTEXT",
                    Mc.LANGUAGE_PREFERENCE_ENGLISH)); 
            if (deal != null) {
                emailText.append(newLine)
                    .append(newLine)
                    .append("deal Id = ").append(deal.getDealId()).append(newLine);
            }
            if (queue !=null){
                    emailText.append("ESBOUTBOUNDQUEUE Id = ")
                    .append(queue.getESBOutboundQueueId()).append(newLine);
            }
        } else {
            
            emailSubject = BXResources.getSysMsg("CONDITION_EMAILSUBJECT",
                    Mc.LANGUAGE_PREFERENCE_ENGLISH);

            String emailMessage = BXResources.getSysMsg("CONDITION_EMAILTEXT",
                    Mc.LANGUAGE_PREFERENCE_ENGLISH);

            emailText.append(emailMessage + newLine + newLine);
            emailText.append("Error code: " + queue.getTransactionStatusId()
                    + ", " + queue.getTransactionStatusDescription());

            LenderProfile lenderProfile = deal.getLenderProfile();
            String lenderName = lenderProfile.getLenderName();
            emailText.append(newLine + newLine + "Lender\t\t\t : " + lenderName);

            String institutionName = BXResources.getPickListDescription(deal
                    .getInstitutionProfileId(), "INSTITUTIONPROFILE", deal
                    .getInstitutionProfileId(), Mc.LANGUAGE_PREFERENCE_ENGLISH);
            emailText.append(newLine + "Institution\t\t\t : " + institutionName);

            int dealId = deal.getDealId();
            emailText.append(newLine + "Deal Id\t\t\t : " + dealId);
            emailText.append(newLine + "Source application\t : "
                    + deal.getSourceApplicationId());
            emailText.append(newLine
                    + newLine
                    + BXResources.getSysMsg("CONDITION_VERIFY",
                            Mc.LANGUAGE_PREFERENCE_ENGLISH));
        }

        String emailTo = PropertiesCache.getInstance().getProperty(
                deal.getInstitutionProfileId(),
                "com.filogix.docprep.alert.email.recipient");
        
        _log.info("ConditionStatusProcessor email subject: \n"
                + emailSubject);
        _log.info("ConditionStatusProcessor email text: \n"
                + emailText.toString());
        DocumentRequest.requestAlertEmail(srk, deal, emailTo, emailSubject,
                emailText.toString());

    }

	public boolean isDebugmode() {
		return debugmode;
	}

	public void setDebugmode(boolean debugmode) {
		this.debugmode = debugmode;
	}

    /**
     * @return the support
     */
    public String getSupport() {
        return support;
    }

    /**
     * @param support the support to set
     */
    public void setSupport(String support) {
        this.support = support;
    }

    public void setRetryErrorCodes(String retryErrorCodes) {
        Set<String> errorList = new HashSet<String>();
        StringTokenizer errorCodeTokens = new StringTokenizer(retryErrorCodes, " ,");
        while (errorCodeTokens.hasMoreTokens()) {
            String token = errorCodeTokens.nextToken();
            errorList.add(token);
        }
        setRetryErrorCodeList(errorList);
    }

    public abstract Set<String> getRetryErrorCodeList();
    public abstract void setRetryErrorCodeList(Set<String> errorList);
}
