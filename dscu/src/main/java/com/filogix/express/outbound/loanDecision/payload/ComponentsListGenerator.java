package com.filogix.express.outbound.loanDecision.payload;

import java.util.List;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.filogix.express.core.ExpressRuntimeException;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.MortgageType.Component;
import com.filogix.schema.fcx._1.MortgageType.Component.ComponentCreditCard;
import com.filogix.schema.fcx._1.MortgageType.Component.ComponentLOC;
import com.filogix.schema.fcx._1.MortgageType.Component.ComponentLoan;
import com.filogix.schema.fcx._1.MortgageType.Component.ComponentMortgage;
import com.filogix.schema.fcx._1.MortgageType.Component.ComponentOverdraft;
import com.filogix.schema.fcx._1.MortgageType.Component.MtgProd;
import com.filogix.schema.fcx._1.MortgageType.Component.PricingRateInventory;

public class ComponentsListGenerator {

	protected static void createComponentsList(
			List<Component> listOfComponentType, Deal deal, ObjectFactory factory){

		try {

			for(com.basis100.deal.entity.Component component : deal.getComponents()){

				//MortgageType/Components/Component
				Component mortgageTypeComponent = factory.createMortgageTypeComponent();
				listOfComponentType.add(mortgageTypeComponent);
				populateComponent(mortgageTypeComponent, component);

				//MortgageType/Components/Component/MtgProd
				MtgProd mortgageTypeComponentMtgProd = factory.createMortgageTypeComponentMtgProd();
				mortgageTypeComponent.setMtgProd(mortgageTypeComponentMtgProd);
				com.basis100.deal.entity.MtgProd mtgProd = deal.getMtgProd();
				populateMtgProd(mortgageTypeComponentMtgProd, mtgProd);

				//MortgageType/Components/Component/PricingRateInventory
				PricingRateInventory mortgageTypeComponentPricingRateInventory 
				= factory.createMortgageTypeComponentPricingRateInventory();
				mortgageTypeComponent.setPricingRateInventory(mortgageTypeComponentPricingRateInventory);
				com.basis100.deal.entity.PricingRateInventory pricingRateInventory = component.getPricingRateInventory();
				populatePricingRateInventory(mortgageTypeComponentPricingRateInventory, pricingRateInventory);

				switch (component.getComponentTypeId()) {
				case Mc.COMPONENT_TYPE_MORTGAGE:
					//MortgageType/Components/ComponentMortgage
					ComponentMortgage mortgageTypeComponentComponentMortgage 
					= factory.createMortgageTypeComponentComponentMortgage();
					mortgageTypeComponent.setComponentMortgage(
							mortgageTypeComponentComponentMortgage);
					com.basis100.deal.entity.ComponentMortgage 
					componentMortgage = component.getComponentMortgage();
					if(componentMortgage == null){
						String msg = "ComponetMortgage data doesn't exist for componentid = "
							+ component.getComponentId();
						throw new ExpressRuntimeException(msg);
					}
					populateComponentMortgage(
							mortgageTypeComponentComponentMortgage, componentMortgage);
					break;
				case Mc.COMPONENT_TYPE_LOC:
					//MortgageType/Components/ComponentLOC
					ComponentLOC mortgageTypeComponentComponentLOC = 
						factory.createMortgageTypeComponentComponentLOC();
					mortgageTypeComponent.setComponentLOC(mortgageTypeComponentComponentLOC);
					com.basis100.deal.entity.ComponentLOC componentLOC = component.getComponentLOC();
					if(componentLOC == null){
						String msg = "ComponentLOC data doesn't exist for componentid = "
							+ component.getComponentId();
						throw new ExpressRuntimeException(msg);
					}
					populateComponentLOC(mortgageTypeComponentComponentLOC, componentLOC);
					break;
				case Mc.COMPONENT_TYPE_LOAN:
					//MortgageType/Components/ComponentLoan
					ComponentLoan mortgageTypeComponentComponentLoan = 
						factory.createMortgageTypeComponentComponentLoan();
					mortgageTypeComponent.setComponentLoan(mortgageTypeComponentComponentLoan);
					com.basis100.deal.entity.ComponentLoan componentLoan = component.getComponentLoan();
					if(componentLoan == null){
						String msg = "ComponentLoan data doesn't exist for componentid = "
							+ component.getComponentId();
						throw new ExpressRuntimeException(msg);
					}
					populateComponentLoan(mortgageTypeComponentComponentLoan, componentLoan);
					break;
				case Mc.COMPONENT_TYPE_CREDITCARD:
					//MortgageType/Components/ComponentCreditCard
					ComponentCreditCard mortgageTypeComponentComponentCreditCard = 
						factory.createMortgageTypeComponentComponentCreditCard();
					mortgageTypeComponent.setComponentCreditCard(
							mortgageTypeComponentComponentCreditCard);
					com.basis100.deal.entity.ComponentCreditCard componentCreditCard
					= component.getComponentCreditCard();
					if(componentCreditCard == null){
						String msg = "ComponentCreditCard data doesn't exist for componentid = "
							+ component.getComponentId();
						throw new ExpressRuntimeException(msg);
					}
					populateComponentCreditCard(mortgageTypeComponentComponentCreditCard, componentCreditCard);
					break;
				case Mc.COMPONENT_TYPE_OVERDRAFT:
					//MortgageType/Components/ComponentOverdraft
					ComponentOverdraft mortgageTypeComponentComponentOverdraft = 
						factory.createMortgageTypeComponentComponentOverdraft();
					mortgageTypeComponent.setComponentOverdraft(mortgageTypeComponentComponentOverdraft);
					com.basis100.deal.entity.ComponentOverdraft componentOverdraft = component.getComponentOverdraft();
					if(componentOverdraft == null){
						String msg = "ComponentOverdraft data doesn't exist for componentid = "
							+ component.getComponentId();
						throw new ExpressRuntimeException(msg);
					}
					populateComponentOverdraft(mortgageTypeComponentComponentOverdraft, componentOverdraft);
					break;
				default:
					throw new ExpressRuntimeException(
							"Invalid componentTypeId (=" + component.getComponentTypeId() 
							+ ") in componentid = " + component.getComponentId());
				}

			}

		} catch (FinderException e) {
			throw new ExpressRuntimeException(e);
		} catch (RemoteException e) {
			throw new ExpressRuntimeException(e);
		} catch (Exception e) {
			throw new ExpressRuntimeException(e);
		}
	}

	protected static void populateComponentMortgage(
			ComponentMortgage fcx,
			com.basis100.deal.entity.ComponentMortgage db) {

		fcx.setAdditionalPrincipal(Convert.toBigDecimal(db.getAdditionalPrincipal()));
		fcx.setAdvanceHold(Convert.toBigDecimal(db.getAdvanceHold()));
		fcx.setActualPaymentTerm(Convert.toBigDecimal(db.getActualPaymentTerm()));
		fcx.setAmortizationTerm(Convert.toBigDecimal(db.getAmortizationTerm()));
		fcx.setBalanceRemainingAtEndOfTerm(Convert.toBigDecimal(db.getBalanceRemainingAtEndOfTerm()));
		fcx.setBuyDownRate(Convert.toBigDecimal(db.getBuyDownRate()));
		fcx.setCashBackAmount(Convert.toBigDecimal(db.getCashBackAmount()));
		fcx.setCashBackAmountOverride(db.getCashBackAmountOverride());
		fcx.setCashBackPercent(Convert.toBigDecimal(db.getCashBackPercent()));
		fcx.setCommissionCode(db.getCommissionCode());
		fcx.setDiscount(Convert.toBigDecimal(db.getDiscount()));
		fcx.setEffectiveAmortizationMonths(db.getEffectiveAmortizationMonths());
		fcx.setExistingAccountIndicator(db.getExistingAccountIndicator());
		fcx.setExistingAccountNumber(db.getExistingAccountNumber());
		fcx.setFirstPaymentDate(Convert.toXMLGregorianCalendar(db.getFirstPaymentDate()));
		fcx.setFirstPaymentDateMonthly(Convert.toXMLGregorianCalendar(db.getFirstPaymentDateMonthly()));
		fcx.setIadNumberOfDays(Convert.toBigDecimal(db.getIadNumberOfDays()));
		fcx.setInterestAdjustmentAmount(Convert.toBigDecimal(db.getInterestAdjustmentAmount()));
		fcx.setInterestAdjustmentDate(Convert.toXMLGregorianCalendar(db.getInterestAdjustmentDate()));
		fcx.setMaturityDate(Convert.toXMLGregorianCalendar(db.getMaturityDate()));
		fcx.setMiAllocateFlag(db.getMiAllocateFlag());
		fcx.setMortgageAmount(Convert.toBigDecimal(db.getMortgageAmount()));
		fcx.setNetInterestRate(Convert.toBigDecimal(db.getNetInterestRate()));
		fcx.setPAndIPaymentAmount(Convert.toBigDecimal(db.getPAndIPaymentAmount()));
		fcx.setPAndIPaymentAmountMonthly(Convert.toBigDecimal(db.getPAndIPaymentAmountMonthly()));
		fcx.setPaymentFrequencyDd(Convert.toBigDecimal(db.getPaymentFrequencyId()));
		fcx.setPerDiemInterestAmount(Convert.toBigDecimal(db.getPerDiemInterestAmount()));
		fcx.setPremium(Convert.toBigDecimal(db.getPremium()));
		fcx.setPrepaymentOptionsDd(Convert.toBigDecimal(db.getPrePaymentOptionsId()));
		fcx.setPrivilegePaymentDd(Convert.toBigDecimal(db.getPrivilegePaymentId()));
		fcx.setPropertyTaxAllocateFlag(db.getPropertyTaxAllocateFlag());
		fcx.setPropertyTaxEscrowAmount(Convert.toBigDecimal(db.getPropertyTaxEscrowAmount()));
		fcx.setRateGuaranteePeriod(Convert.toBigDecimal(db.getRateGuaranteePeriod()));
		fcx.setTotalMortgageAmount(Convert.toBigDecimal(db.getTotalMortgageAmount()));

	}

	protected static void populatePricingRateInventory(
			PricingRateInventory fcx,
			com.basis100.deal.entity.PricingRateInventory db) {

		fcx.setIndexEffectiveDate(
				Convert.toXMLGregorianCalendar(db.getIndexEffectiveDate()));
	}

	protected static void populateComponent(Component fcx, 
			com.basis100.deal.entity.Component db) {

		fcx.setAdditionalInformation(db.getAdditionalInformation());
		fcx.setComponentTypeDd(Convert.toBigDecimal(db.getComponentTypeId()));
		fcx.setFirstPaymentDateLimit(Convert.toXMLGregorianCalendar(db.getFirstPaymentDateLimit()));
		fcx.setInterestRate(Convert.toBigDecimal(db.getPostedRate()));
		fcx.setRepaymentTypeDd(Convert.toBigDecimal(db.getRepaymentTypeId()));
		fcx.setTeaserMaturityDate(Convert.toXMLGregorianCalendar(db.getTeaserMaturityDate()));
		fcx.setTeaserNetInterestRate(Convert.toBigDecimal(db.getTeaserNetInterestRate()));
		fcx.setTeaserPIAmount(Convert.toBigDecimal(db.getTeaserPiAmount()));
		fcx.setTeaserRateInterestSaving(Convert.toBigDecimal(db.getTeaserRateInterestSaving()));

	}

	protected static void populateMtgProd(MtgProd fcx, com.basis100.deal.entity.MtgProd db) {

		fcx.setMpBusinessId(db.getMPBusinessId());
	}


	protected static void populateComponentLOC(
			ComponentLOC fcx,
			com.basis100.deal.entity.ComponentLOC db) {

		fcx.setAdvanceHold(Convert.toBigDecimal(db.getAdvanceHold()));
		fcx.setCommissionCode(db.getCommissionCode());
		fcx.setDiscount(Convert.toBigDecimal(db.getDiscount()));
		fcx.setExistingAccountIndicator(db.getExistingAccountIndicator());
		fcx.setExistingAccountNumber(db.getExistingAccountNumber());
		fcx.setFirstPaymentDate(Convert.toXMLGregorianCalendar(db.getFirstPaymentDate()));
		fcx.setInterestAdjustmentDate(Convert.toXMLGregorianCalendar(db.getInterestAdjustmentDate()));
		fcx.setLocAmount(Convert.toBigDecimal(db.getLocAmount()));
		fcx.setMiAllocateFlag(db.getMiAllocateFlag());
		fcx.setNetInterestRate(Convert.toBigDecimal(db.getNetInterestRate()));
		fcx.setPAndIPaymentAmount(Convert.toBigDecimal(db.getPAndIPaymentAmount()));
		fcx.setPAndIPaymentAmountMonthly(Convert.toBigDecimal(db.getPAndIPaymentAmountMonthly()));
		fcx.setPaymentFrequencyDd(Convert.toBigDecimal(db.getPaymentFrequencyId()));
		fcx.setPremium(Convert.toBigDecimal(db.getPremium()));
		fcx.setPrepaymentOptionsDd(Convert.toBigDecimal(db.getPrePaymentOptionsId()));
		fcx.setPrivilegePaymentDd(Convert.toBigDecimal(db.getPrivilegePaymentId()));
		fcx.setPropertyTaxAllocateFlag(db.getPropertyTaxAllocateFlag());
		fcx.setPropertyTaxEscrowAmount(Convert.toBigDecimal(db.getPropertyTaxEscrowAmount()));
		fcx.setTotalLOCAmount(Convert.toBigDecimal(db.getTotalLocAmount()));

	}

	protected static void populateComponentLoan(
			ComponentLoan fcx,
			com.basis100.deal.entity.ComponentLoan db) {

		fcx.setActualPaymentTerm(Convert.toBigDecimal(db.getActualPaymentTerm()));
		fcx.setDiscount(Convert.toBigDecimal(db.getDiscount()));
		fcx.setFirstPaymentDate(Convert.toXMLGregorianCalendar(db.getFirstPaymentDate()));
		fcx.setIadNumberOfDays(Convert.toBigDecimal(db.getIadNumberOfDays()));
		fcx.setInterestAdjustmentAmount(Convert.toBigDecimal(db.getInterestAdjustmentAmount()));
		fcx.setInterestAdjustmentDate(Convert.toXMLGregorianCalendar(db.getInterestAdjustmentDate()));
		fcx.setLoanAmount(Convert.toBigDecimal(db.getLoanAmount()));
		fcx.setMaturityDate(Convert.toXMLGregorianCalendar(db.getMaturityDate()));
		fcx.setNetInterestRate(Convert.toBigDecimal(db.getNetInterestRate()));
		fcx.setPAndIPaymentAmount(Convert.toBigDecimal(db.getPAndIPaymentAmount()));
		fcx.setPAndIPaymentAmountMonthly(Convert.toBigDecimal(db.getPAndIPaymentAmountMonthly()));
		fcx.setPaymentFrequencyDd(Convert.toBigDecimal(db.getPaymentFrequencyId()));
		fcx.setPerDiemInterestAmount(Convert.toBigDecimal(db.getPerDiemInterestAmount()));
		fcx.setPremium(Convert.toBigDecimal(db.getPremium()));

	}

	protected static void populateComponentCreditCard(
			ComponentCreditCard fcx,
			com.basis100.deal.entity.ComponentCreditCard db) {

		fcx.setCreditCardAmount(Convert.toBigDecimal(db.getCreditCardAmount()));
	}

	protected static void populateComponentOverdraft(
			ComponentOverdraft fcx,
			com.basis100.deal.entity.ComponentOverdraft db) {

		fcx.setOverDraftAmount(Convert.toBigDecimal(db.getOverDraftAmount()));
	}
}
