package com.filogix.express.outbound;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.security.DLM;
import com.basis100.deal.util.InstitutionProfileUtils;
import com.basis100.deal.util.TypeConverter;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.core.ExpressRuntimeException;
import com.filogix.express.outbound.util.OutboundConst;
import com.filogix.express.outbound.util.OutboundUtil;
import com.filogix.externallinks.framework.ServiceConst;

public class OutboundManager {

    private final static Logger logger = LoggerFactory.getLogger(OutboundManager.class);

    private static final int MAX_ATTEMPT;
    private static final int RETRY_TIME;
    private static final int SLEEP_TIME;

    private static final Map<Integer, Integer> INSTITUTION_USER_MAP;

    static {

        PropertiesCache prop = PropertiesCache.getInstance();
        String maxAttempt = prop.getProperty(-1, OutboundConst.MAX_ATTEMP, "10");
        MAX_ATTEMPT = TypeConverter.intTypeFrom(maxAttempt, 10);

        String retryTime = prop.getProperty(-1, OutboundConst.RETRY_TIME, "600");
        RETRY_TIME = TypeConverter.intTypeFrom(retryTime, 600);

        String sleepTime = prop.getProperty(-1, OutboundConst.SLEEP_TIME, "60000");
        SLEEP_TIME = TypeConverter.intTypeFrom(sleepTime, 60000);

        SessionResourceKit srk = new SessionResourceKit();
        try {
            // all userProfileId for each institutionProfile
            // if no configuration exists, userProfileId=0 (SYSTEM) is used.
            INSTITUTION_USER_MAP = InstitutionProfileUtils.createMapAllInstitutionAndSysPropertyValueInt(
                    srk, OutboundConst.SYSPROP_SYSTEM_USERPROFILEID, 0); 

            // DLM initialize
            DLM.init(srk, false);

        } catch (Exception e) {
            logger.error("failed to initialize OutboundManager", e);
            throw new ExpressRuntimeException(e);

        } finally {
            srk.freeResources();
        }

        // shutdown hook
        Thread sdhook = new Thread(new ShutdownHook(INSTITUTION_USER_MAP));
        Runtime.getRuntime().addShutdownHook(sdhook);
    }

	public void startService() {

		ExecutorService execService = Executors.newSingleThreadExecutor();
		try {
			while (true) {

				loopWhileDataExist(execService);
				
				try {
					// sleep when no data found or error happened
					Thread.sleep(SLEEP_TIME);
				} catch (InterruptedException e) {
					logger.error(e.getMessage(), e);
				}
			}
		} finally {
			execService.shutdown();
		}

	}

    private void loopWhileDataExist(ExecutorService execService) {

        SessionResourceKit srk = null;
        try {
            srk = new SessionResourceKit();
            srk.getExpressState().cleanAllIds();
            // Manager thread doesn't use transaction
            srk.getConnection().setAutoCommit(true);

            ESBOutboundQueue rec = null;

            while (true) {

                rec = new ESBOutboundQueue(srk);
                rec.findNextUnprocessedEvent(MAX_ATTEMPT, RETRY_TIME);

                // exit loop if no data is found in the queue
                if (rec.getESBOutboundQueueId() <= 0)
                    break;

                logger.debug("found record " + rec.getESBOutboundQueueId());

                OutboundUtil.changeProcessStatus(rec,
                    ServiceConst.ESBOUT_TRANSACTIONSTATUS_PROCESSING, "In progress", srk);

                OutboundWorker worker = new OutboundWorker(
                    rec.getESBOutboundQueueId(), rec.getInstitutionProfileId(),
                    INSTITUTION_USER_MAP.get(rec.getInstitutionProfileId()));

                execService.execute(worker);

            }

        } catch (Throwable t) {
            logger.error("caught error in loopWhileDataExist", t);
        } finally {
            if (srk != null)
                srk.freeResources();
        }
    }

}

class ShutdownHook implements Runnable {

    private final static Logger logger =
        LoggerFactory.getLogger(ShutdownHook.class);
    Map<Integer, Integer> institutionUserMap;

    public ShutdownHook(Map<Integer, Integer> institutionUserMap) {
        super();
        this.institutionUserMap = institutionUserMap;
    }

    public void run() {
        logger.info("---------- shutdownHook start ----------");
        SessionResourceKit srk = null;

        // delete all deallocks
        try {
            srk = new SessionResourceKit();
            logger.info("deleting all deallocks records DSCU created");
            for (int inst : institutionUserMap.keySet()) {
                srk.getExpressState().setDealInstitutionId(inst);
                DLM.getInstance().unlockAnyByUser(
                    institutionUserMap.get(inst), srk, false);
            }
            logger.info("deallocks deleted successfully");
        } catch (Exception e) {
            logger.error("failed to unlock deal", e);
        } finally {
            if (srk != null) {
                srk.freeResources();
            }
        }
        logger.info("---------- shutdownHook end ----------");
    }
}
