package com.filogix.express.outbound.mi;


import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.docprep.DocPrepException;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealPropagator;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.pk.DealPK;
import com.basis100.entity.FinderException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.resources.SessionResourceWrapper;
import com.basis100.workflow.WFETrigger;
//import com.filogix.express.email.EmailSender;
import com.filogix.express.outbound.AbstractOutboundProcessor;
import com.filogix.express.outbound.OutboundProcessException;
import com.filogix.express.outbound.util.OutboundConst;
import com.filogix.express.outbound.util.OutboundUtil;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.externallinks.services.ServiceDelegate;
import com.filogix.externallinks.services.ServiceInfo;
import com.basis100.entity.RemoteException;


public class CGMIOutboundProcessor extends AbstractOutboundProcessor {

    private final static Logger logger = LoggerFactory.getLogger(CGMIOutboundProcessor.class);

    /**
     * method handle processing send payload to ESB, handle result form ESB
     * 
     * @param obj
     * @return
     * @throws JdbcTransactionException
     * @throws OutboundProcessException
     * @throws Exception
     */
    public void handleProcess(Object obj) throws Exception {

        ESBOutboundQueue record = (ESBOutboundQueue) obj;

        logger.debug("===== CGMIOutboundProcessor start. Deal="
            + record.getDealId() + " QID=" + record.getESBOutboundQueueId());

        DealPK dpk = new DealPK(record.getDealId(), -1);
        Deal deal = new Deal(srk, null);
        deal = deal.findByRecommendedScenario(dpk, true);

        ServiceInfo info = new ServiceInfo();

        info.setSrk(srk);
        info.setDeal(deal);

        ServiceDelegate serviceDelegate = new ServiceDelegate();

        info.setDealId(deal.getDealId());
        info.setCopyId(deal.getCopyId());
        info.setSrk(new SessionResourceWrapper(srk));
        info.setProductType(deal.getMortgageInsurerId());
        info.setDealStatusId(deal.getStatusId());
        info.setMiStatusId(deal.getMIStatusId());

        try {

            serviceDelegate.sendMIRequest(info);

            record.setTransactionStatusId(ServiceConst.ESBOUT_TRANSACTIONSTATUS_SUCCESS);
            record.setTransactionStatusDescription("successfully submitted CG MI cancel request");
            record.ejbStore();
            
            DealPropagator dealPropagator = new DealPropagator(deal, null);
            dealPropagator.setMIStatusId(Mc.MI_STATUS_CANCELLED);
            dealPropagator.ejbStore();

            WFETrigger.workflowTrigger(2, srk, deal.getUnderwriterUserId(),
                deal.getDealId(), deal.getCopyId(), -1, null);

            if (isDebugmode() == false) {
                record.ejbRemove();
            }

        } catch (ExternalServicesException e) {

            logger.error("failed to submit CG MI Cancel request" + record.getESBOutboundQueueId(), e);

            record.setTransactionStatusId(ServiceConst.ESBOUT_TRANSACTIONSTATUS_AVALIABLE);
            record.setTransactionStatusDescription(
                StringUtils.substring(e.getMessage(), 0,
                    OutboundConst.ESBOUTBOUNDQUEUE_TRANSACTIONSTATUSDESC_MAX));
            record.setAttemptCounter(record.getAttemptCounter() + 1);
            record.ejbStore();

            if (record.getAttemptCounter() > OutboundUtil.getMaxAttemptValue(record)) {
                // log counter lapsing
                String counterMsg = "Retry counter elapsed for deal " + record.getDealId();
                logger.info(counterMsg);
/* QC710 - stop using EmailSender, start using a copy of DSCU's email setup. 
                EmailSender.sendAlertEmail(
                    "ESBOutbound CGMICancelOutboundProcessor", e, null, counterMsg);
*/
                sendEmailNotification(deal, record);
            }
        }
        logger.debug("===== CGMIOutboundProcessor end. Deal="
            + record.getDealId() + " QID=" + record.getESBOutboundQueueId());
    }

    
    /**
     * send email notification
     * 
     * @param deal
     * @param queue
     * @throws DocPrepException
     * @throws FinderException
     * @throws RemoteException
     */
    public void sendEmailNotification(Deal deal, ESBOutboundQueue queue) 
    		throws DocPrepException, FinderException, RemoteException 
	{

        SessionResourceKit srk = deal.getSessionResourceKit();
        String newLine = System.getProperty("line.separator");
        StringBuffer emailText = new StringBuffer();
        String emailSubject = "";

		try
		{
            emailSubject = BXResources.getSysMsg("CGAUTOCANCEL_EMAILSUBJECT",
                    Mc.LANGUAGE_PREFERENCE_ENGLISH);

            String emailMessage = BXResources.getSysMsg("CGAUTOCANCEL_EMAILTEXT",
                    Mc.LANGUAGE_PREFERENCE_ENGLISH);

            emailText.append(emailMessage + newLine + newLine);
            emailText.append("Error code: " + queue.getTransactionStatusId()
                    + ", " + queue.getTransactionStatusDescription());

            LenderProfile lenderProfile = deal.getLenderProfile();
            String lenderName = lenderProfile.getLenderName();
            emailText.append(newLine + newLine + "Lender\t\t\t : " + lenderName);

            String institutionName = BXResources.getPickListDescription(deal
                    .getInstitutionProfileId(), "INSTITUTIONPROFILE", deal
                    .getInstitutionProfileId(), Mc.LANGUAGE_PREFERENCE_ENGLISH);
            emailText.append(newLine + "Institution\t\t\t : " + institutionName);

            int dealId = deal.getDealId();
            emailText.append(newLine + "Deal Id\t\t\t : " + dealId);
            emailText.append(newLine + "Source application\t : "
                    + deal.getSourceApplicationId());
            emailText.append(newLine
                    + newLine
                    + BXResources.getSysMsg("CGAUTOCANCEL_VERIFY",
                            Mc.LANGUAGE_PREFERENCE_ENGLISH));
		}
		catch (RemoteException e)
		{
			logger.error("Failed to get deal while sending email to support in CGMIOutboundProcessor", e);
		}

		String emailTo = PropertiesCache.getInstance().getProperty(
                deal.getInstitutionProfileId(),
                "com.filogix.docprep.alert.email.recipient");

		logger.info("CGMIOutboundProcessor email subject: \n"
                + emailSubject);
        logger.info("CGMIOutboundProcessor email text: \n"
                + emailText.toString());
        DocumentRequest.requestAlertEmail(srk, deal, emailTo, emailSubject,
                emailText.toString());

    }
    
}
