package com.filogix.express.outbound.loanDecision.payload;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DownPaymentSource;
import com.basis100.entity.FinderException;
import com.filogix.express.core.ExpressRuntimeException;
import com.filogix.schema.fcx._1.DealType;
import com.filogix.schema.fcx._1.ObjectFactory;

public class DealTypeGenerator {

	protected static DealType generateDealType(Deal deal, ObjectFactory factory) {

		DealType dealType = factory.createDealType();
		populateDealType(dealType, deal, factory);
		return dealType;
	}

	protected static void populateDealType(DealType dealType, Deal deal,
			ObjectFactory factory) {

		dealType.setEstimatedClosingDate(Convert.toXMLGregorianCalendar(
				deal.getEstimatedClosingDate()));
		//xsd:deal/applicationid, length =12, db:deal.sourceApplicationId, length=35, 
		//since express doesn't change sourceApplicationId, theoretically we should be able to truncate sourceApplicationId
		dealType.setApplicationId(StringUtils.substring(deal.getSourceApplicationId(),0,12));
		
		dealType.setDealPurposeDd(Convert.toBigDecimal(convertDealPurposeId(
				deal.getDealPurposeId())));
		dealType.setDealTypeDd(Convert.toBigDecimal(
				deal.getSpecialFeatureId() == Mc.SPECIAL_FEATURE_PRE_APPROVAL ?1:0));
		dealType.setTaxPayorDd(Convert.toBigDecimal(deal.getTaxPayorId()));

		Collection<DownPaymentSource> downPaymentSources = null;
		try {
			downPaymentSources = deal.getDownPaymentSources();
		} catch (Exception e) {
			throw new ExpressRuntimeException(e);
		}
		for(DownPaymentSource downPaymentSource: downPaymentSources){
			double downPaymentAmount = downPaymentSource.getAmount();
			if (downPaymentAmount > 0d) {
				com.filogix.schema.fcx._1.DealType.DownPaymentSource dps
				= factory.createDealTypeDownPaymentSource();
				dps.setAmount(Convert.toBigDecimal(downPaymentAmount));
				dealType.getDownPaymentSource().add(dps);
			}
		}



	}

	protected static int convertDealPurposeId(int dealPurposeId) {
		switch (dealPurposeId) {
		case Mc.DEAL_PURPOSE_PURCHASE:
			return 0;
		case Mc.DEAL_PURPOSE_PURCHASE_WITH_IMPROVEMENTS:
			return 1;
		case Mc.DEAL_PURPOSE_REFI_EXISTING_CLIENT:
			return 2;
		case Mc.DEAL_PURPOSE_REFI_EXTERNAL:
			return 2;
		case Mc.DEAL_PURPOSE_ETO_EXISTING_CLIENT:
			return 3;
		case Mc.DEAL_PURPOSE_ETO_EXTERNAL:
			return 3;
		case Mc.DEAL_PURPOSE_DEFICIENCY_SALE:
			return 6;
		case Mc.DEAL_PURPOSE_WORKOUT:
			return 7;
		default:
			return 1;
		}
	}

}
