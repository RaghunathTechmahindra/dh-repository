package com.filogix.express.outbound;


/**
 * DscuMain main class for dscu deamon
 * 
 * @version 1.0 Jun 9, 2009
 */
public class OutboundMain {
    
    public static void main(String[] args) {

        OutboundManager manager = new OutboundManager();
        manager.startService();

    }
}