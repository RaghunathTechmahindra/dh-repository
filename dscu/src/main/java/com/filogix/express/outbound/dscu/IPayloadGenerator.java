package com.filogix.express.outbound.dscu;

import java.util.Collection;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentTracking;
import com.filogix.express.outbound.dscu.DESUpdatePayloadGenerator.DEStatus;

public interface IPayloadGenerator {

    public String getXmlPayload() throws Exception;

    public void init(Deal deal);

    public void setUserProfileId(int userProfileId);

    public void setConditions(Collection<DocumentTracking> conditions);
    
    public void setDEStatuses(Collection<DEStatus> deStatuses);

}
