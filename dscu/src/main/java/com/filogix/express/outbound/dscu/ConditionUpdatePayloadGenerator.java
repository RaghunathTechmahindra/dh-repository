/**
 * <p>Title: ConditionUpdatePayloadGenerator.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2009</p>
 *
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � Mar 23, 2009)
 *
 */
package com.filogix.express.outbound.dscu;

import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.util.ValidationEventCollector;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.stream.StreamSource;

import MosSystem.Mc;

import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.DealNotes;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.entity.LenderToBranchAssoc;
import com.basis100.deal.entity.SystemType;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.BranchProfilePK;
import com.basis100.deal.pk.ContactPK;
import com.basis100.deal.pk.LenderProfilePK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;

import com.filogix.schema.postoffice.ConditionStatusUpdate;
import com.filogix.schema.postoffice.LanguageSpecificText;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Update.LenderContact.ContactRole;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Update.LenderContact;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Update.LenderBranchAddress;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Update.Condition.DocumentStatus;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Update.Condition.ConditionType;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Update.Condition.ConditionResponsibilityRole;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Update;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Routing.Receiver;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Routing.Sender.Institution;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Routing.Sender.Lender;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Routing.Sender;
import com.filogix.schema.postoffice.ConditionStatusUpdate.Routing;

public class ConditionUpdatePayloadGenerator extends  ServicePayloadGenerator {

    private static String logString = "";
    private static com.filogix.schema.postoffice.ObjectFactory factory;
    
    protected ConditionStatusUpdate payload;

    private static JAXBContext jc;
    static {
        try {
            jc = JAXBContext.newInstance("com.filogix.schema.postoffice");
            factory = new com.filogix.schema.postoffice.ObjectFactory();
        } catch (JAXBException e) {
            _logger.error(logString
                    + "Error in getting a JAXBContext instance: "
                    + e.getMessage());
        }
    }
    
    public ConditionUpdatePayloadGenerator() {
        super();  
    }

    protected Collection<DocumentTracking> documentTrackings;
    protected int userProfileId;
    protected XMLGregorianCalendar timeStamp = null;
    public void setConditions(Collection<DocumentTracking> c) {
        
        this.documentTrackings = c;
    }
    
    public void setUserProfileId(int upId) {
        
        this.userProfileId = upId;
    }
    
    public String getXmlPayload() throws JAXBException{
        
        ConditionStatusUpdate csu 
            = factory.createConditionStatusUpdate();
        csu.setRouting(getRouting());
        csu.setUpdate(getUpdate());
        
        try {
            _userprofile = new UserProfile(_srk);
            _userprofile = _userprofile.findByPrimaryKey(new UserProfileBeanPK(userProfileId, _deal.getInstitutionProfileId()));
            timeStamp = getUserTimeStamp(_userprofile);
            csu.setTimeStamp(timeStamp);
        } catch (Exception e) {
            _logger.warning("getXmlPayload couldn't create = " + _deal.getDealId());            
        }
        
        String payload = getXmlPayload(csu);
        return payload;
    }
    
    //unit test done
    protected Routing getRouting() throws JAXBException{
        
        Routing routing 
            = factory.createConditionStatusUpdateRouting();
        routing.setSender(getSender());
        routing.setReceiver(getReceiver());
        return routing;
    }
    
    //unit test done
    protected Sender  getSender() throws JAXBException{
        
        Sender sender 
            = factory.createConditionStatusUpdateRoutingSender();

        sender.setLender(getLender());
        sender.setInstitution(getInstitution());
        sender.setDealId(new Long(this._deal.getDealId()));
        return sender;
    }
    
    //unit test done
    protected Lender getLender()  throws JAXBException  {

        Lender lender 
            = factory.createConditionStatusUpdateRoutingSenderLender();
        int lpid = this._deal.getLenderProfileId();
        if (lpid >= 0) {
            try {
                LenderProfile lp = new LenderProfile(_srk, lpid);
                if (lp != null) {
                    lender.setLpBusinessId(lp.getLPBusinessId());
                    lender.setLenderName(lp.getLenderName());
                }
            } catch (RemoteException re) {
                return null;
            } catch (FinderException fe) {
                return null;
            }               
        }
        return lender;
    }
    
    //unit test done
    protected Institution getInstitution() throws JAXBException {
        
        Institution institution 
            = factory.createConditionStatusUpdateRoutingSenderInstitution();
        
        int ipid = this._deal.getInstitutionProfileId();
        if (ipid >= 0) {
            try {
                InstitutionProfile ip = new InstitutionProfile(_srk, ipid);
                if (ip != null) {
                    institution.setEcniLenderId(ip.getECNILenderId());
                    institution.setInstitutionName(ip.getInstitutionName());
                }
            } catch (RemoteException re) {
                return null;
            } catch (FinderException fe) {
                return null;
            }
        }
        return institution;
    }
    
    //unit test done
    protected Receiver getReceiver() throws JAXBException {
        
        Receiver receiver 
            = factory.createConditionStatusUpdateRoutingReceiver();
        
        receiver.setSystemType(getSystemType());
        receiver.setSourceApplicationId(this._deal.getSourceApplicationId());
        String saVersion = this._deal.getSourceApplicationVersion();
        if (saVersion!=null && saVersion.length() > 0)
            receiver.setSourceApplicationVersion(this._deal.getSourceApplicationVersion());
        else
            receiver.setSourceApplicationVersion(this._deal.getSourceApplicationId());
        return receiver;
    }
    
    //unit test done
    protected com.filogix.schema.postoffice.ConditionStatusUpdate.Routing.Receiver.SystemType getSystemType() throws JAXBException{
        
        com.filogix.schema.postoffice.ConditionStatusUpdate.Routing.Receiver.SystemType systemType 
            = factory.createConditionStatusUpdateRoutingReceiverSystemType();
        int stid = this._deal.getSystemTypeId();
        if (stid >= 0) {
            try {
                SystemType st = new SystemType(_srk, stid);
                if (st != null) {
                    systemType.setSystemTypeId(new Short(""+st.getSystemTypeId()));
                    systemType.setSystemTypeDescription(st.getSystemTypeDescription());
                }
            } catch (RemoteException re) {
                return null;
            } catch (FinderException fe) {
                return null;                
            }
        }
        return systemType;
        
    }
    
    //unit test done
    protected Update getUpdate() throws JAXBException{
        
        Update update 
            = factory.createConditionStatusUpdateUpdate();

        update.getCondition().addAll(getConditions());
        
        String metDaysStr = PropertiesCache.getInstance().getProperty(this._deal.getInstitutionProfileId(),
          "com.basis100.docprep.bco.conditions.met.days", "5");
        update.setConditionsMetDays(new Short(metDaysStr));
        update.setLenderBranchAddress(getLenderBranchAddress());

        LanguageSpecificText conditionDealNoteEn = getDealNote(Mc.LANGUAGE_PREFERENCE_ENGLISH);
        if(conditionDealNoteEn!=null)
            update.getConditionDealNote().add(conditionDealNoteEn);
        LanguageSpecificText conditionDealNoteFr = getDealNote(Mc.LANGUAGE_PREFERENCE_FRENCH);
        if(conditionDealNoteFr!=null)
            update.getConditionDealNote().add(conditionDealNoteFr);
        
        if ("Y".equalsIgnoreCase(PropertiesCache.getInstance().getProperty(this._deal.getInstitutionProfileId(),
          "com.basis100.docprep.bco.current.user", "Y"))) {
            update.setLenderContact(getLenderContact());
        }
        update.getMetDaysNote().add(getMetDaysNote(metDaysStr, Mc.LANGUAGE_PREFERENCE_ENGLISH));
        update.getMetDaysNote().add(getMetDaysNote(metDaysStr, Mc.LANGUAGE_PREFERENCE_FRENCH));
        
        return update;
    }

    //unit test done
    protected Collection<com.filogix.schema.postoffice.ConditionStatusUpdate.Update.Condition> getConditions() throws JAXBException{
        
        int size = documentTrackings.size();
        Iterator<DocumentTracking> dts = documentTrackings.iterator();
        Collection<com.filogix.schema.postoffice.ConditionStatusUpdate.Update.Condition> conditions 
            = new ArrayList<com.filogix.schema.postoffice.ConditionStatusUpdate.Update.Condition>(size);
        for (int i=0; i<size; i++) {
            DocumentTracking dt = (DocumentTracking) (dts.next());
            int conditionId = dt.getConditionId();
            try {
                Condition c = new Condition(_srk, conditionId);
                com.filogix.schema.postoffice.ConditionStatusUpdate.Update.Condition condition 
                    = factory.createConditionStatusUpdateUpdateCondition();
                condition.setConditionId(new BigInteger(""+conditionId));
                condition.getConditionLabel().add(getConditionLabel(dt, Mc.LANGUAGE_PREFERENCE_ENGLISH));
                condition.getConditionLabel().add(getConditionLabel(dt, Mc.LANGUAGE_PREFERENCE_FRENCH));
                condition.setConditionResponsibilityRole(getConditionResponsibilityRole(c));
                condition.getConditionText().add(getConditionText(dt, Mc.LANGUAGE_PREFERENCE_ENGLISH));
                condition.getConditionText().add(getConditionText(dt, Mc.LANGUAGE_PREFERENCE_FRENCH));
                condition.setConditionType(getConditionType(c));
                condition.setDocumentStatus(getDocumentStatus(dt) );
                condition.setStatusChangeDate(timeStamp);
                condition.setSectionCode(condition.getSectionCode());
                conditions.add(condition);
            } catch (RemoteException re) {
                
            } catch (FinderException fe) {
                
            }
        }
        return conditions;          
    }
        
    //unit test done
    protected LanguageSpecificText getConditionLabel(DocumentTracking dt, int language) throws JAXBException{
        
        LanguageSpecificText conditionLabel = factory.createLanguageSpecificText();
        
        conditionLabel.setLanguageId(new Byte("" + language));
        String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "LANGUAGEPREFERENCE", language, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        conditionLabel.setLanguageName(langDesc);
        conditionLabel.setText(dt.getDocumentLabelByLanguage(language));

        return conditionLabel;
    }
    
    //unit test done
    protected ConditionResponsibilityRole getConditionResponsibilityRole(Condition c) throws JAXBException{
        
        ConditionResponsibilityRole conditionResponsibilityRole 
            = factory.createConditionStatusUpdateUpdateConditionConditionResponsibilityRole();
        int roleId = c.getConditionResponsibilityRoleId();
        String roleDescEn = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "CONDITIONRESPONSIBILITYROLE", roleId, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        String roleDescFr = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "CONDITIONRESPONSIBILITYROLE", roleId, Mc.LANGUAGE_PREFERENCE_FRENCH);

        conditionResponsibilityRole.setConditionResponsibilityRoleId(new Short("" + roleId));
        
        LanguageSpecificText role = factory.createLanguageSpecificText();
        role.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_ENGLISH));
        String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_ENGLISH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        role.setLanguageName(langDesc);
        role.setText(roleDescEn);
        conditionResponsibilityRole.getConditionResponsibilityRoleDescription().add(role);
        
        role = factory.createLanguageSpecificText();
        role.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_FRENCH));
        langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_FRENCH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        role.setLanguageName(langDesc);
        role.setText(roleDescFr);        
        conditionResponsibilityRole.getConditionResponsibilityRoleDescription().add(role);
        
        return conditionResponsibilityRole;
    }
    
    //unit test done
    protected LanguageSpecificText getConditionText(DocumentTracking dt, int language) throws JAXBException{
        
        LanguageSpecificText textType = factory.createLanguageSpecificText();
        
        textType.setLanguageId(new Byte("" + language));
        String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "LANGUAGEPREFERENCE", language, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        textType.setLanguageName(langDesc);
        textType.setText(dt.getDocumentTextByLanguage(language));
        
        return textType;
    }
    
    //unit test done
    protected ConditionType getConditionType(Condition c)throws JAXBException {
        
        ConditionType conditionType
            = factory.createConditionStatusUpdateUpdateConditionConditionType();
        conditionType.setConditionTypeId(new Short(""+c.getConditionTypeId()));
        int typeId = c.getConditionTypeId();

        String bxDescEn = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "CONDITIONTYPE", typeId, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        String bxDescFr = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "CONDITIONTYPE", typeId, Mc.LANGUAGE_PREFERENCE_FRENCH);
        LanguageSpecificText desc = factory.createLanguageSpecificText();
        
        desc.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_ENGLISH));
        String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_ENGLISH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        desc.setLanguageName(langDesc);
        desc.setText(bxDescEn);
        conditionType.getConditionTypeDescription().add(desc);
        
        desc = factory.createLanguageSpecificText();
        desc.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_FRENCH));
        langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_FRENCH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        desc.setLanguageName(langDesc);
        desc.setText(bxDescFr);

        conditionType.getConditionTypeDescription().add(desc);
        
        return conditionType;
    }
    
    //unit test done
    protected DocumentStatus getDocumentStatus(DocumentTracking dt) throws JAXBException{
        
        DocumentStatus docStatus
            = factory.createConditionStatusUpdateUpdateConditionDocumentStatus();
        int statusId = dt.getDocumentStatusId();
        String bxDescEn = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "DOCUMENTSTATUS", statusId, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        String bxDescFr = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "DOCUMENTSTATUS", statusId, Mc.LANGUAGE_PREFERENCE_FRENCH);
        
        docStatus.setDocumentStatusId(new Short("" + statusId));
        
        LanguageSpecificText statusDesc = factory.createLanguageSpecificText();
        statusDesc.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_ENGLISH));
        String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_ENGLISH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        statusDesc.setLanguageName(langDesc);
        statusDesc.setText(bxDescEn);
        docStatus.getDocumentStatusDescription().add(statusDesc);
        
        statusDesc = factory.createLanguageSpecificText();
        statusDesc.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_FRENCH));
        langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_FRENCH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        statusDesc.setLanguageName(langDesc);
        statusDesc.setText(bxDescFr);
        
        docStatus.getDocumentStatusDescription().add(statusDesc);
        return docStatus;
    }
    
    //unit test done 
    //!!!! new systemMessage is required !!!!
    protected LanguageSpecificText getMetDaysNote(String daysStr, int language) throws JAXBException{
        
        LanguageSpecificText metDateNote = factory.createLanguageSpecificText();
        
        String note = BXResources.getSysMsg("CONDITION_MET_DAYS_NOTE", language);
        
        if (note != null && note.indexOf("%s")>0) 
            note = note.replaceAll("%s", daysStr);

        metDateNote.setLanguageId(new Byte("" + language));
        String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "LANGUAGEPREFERENCE", language, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        metDateNote.setLanguageName(langDesc);
        metDateNote.setText(note);
        
        return metDateNote;
    }
    
    protected LenderBranchAddress getLenderBranchAddress() throws JAXBException{
        
        LenderBranchAddress lbAddress
            = factory.createConditionStatusUpdateUpdateLenderBranchAddress();
        
        int lpid = this._deal.getLenderProfileId();
        int bpid = this._deal.getBranchProfileId();
        int copyId = 1; // copyid=1
        if (lpid >= 0 && bpid >= 0){
            try {
                LenderToBranchAssoc lbAssoc = new LenderToBranchAssoc(_srk);            
                Collection<LenderToBranchAssoc> associations 
                    = lbAssoc.findByBranchAndLender(new BranchProfilePK(bpid), new LenderProfilePK(lpid));
                if (associations != null && associations.size() > 0) {
                    LenderToBranchAssoc lba = (LenderToBranchAssoc)associations.iterator().next();
                    int contactId = lba.getContactId();
                    Contact contact = new Contact(_srk);
                    contact = contact.findByPrimaryKey(new ContactPK(contactId, copyId)); 
                    if (contact != null) {
                        Addr addr = new Addr(_srk, contact.getAddrId(), copyId);
                        if (addr != null) {
                            lbAddress.setAddressLine(addr.getAddressLine1() + " " + addr.getAddressLine2());
                        }
                    }
                    LanguageSpecificText lpen = getLocalPhoneEn(lba);
                    if (lpen != null )
                        lbAddress.getLocalPhone().add(lpen);
                    
                    LanguageSpecificText lpfr = getLocalPhoneFr(lba);
                    if (lpfr != null )
                        lbAddress.getLocalPhone().add(lpfr);

                    LanguageSpecificText faxen = getLocalFaxEn(lba);
                    if (faxen != null )
                        lbAddress.getLocalFax().add(faxen);
                    
                    LanguageSpecificText faxfr = getLocalFaxFr(lba);
                    if (faxfr != null )
                        lbAddress.getLocalFax().add(faxfr);

                    LanguageSpecificText tfen = getTollFreePhoneEn(lba);
                    if (tfen != null )
                        lbAddress.getTollFreePhone().add(tfen);
                    
                    LanguageSpecificText tffr = getTollFreePhoneFr(lba);
                    if (tffr != null )
                        lbAddress.getTollFreePhone().add(tffr);

                    LanguageSpecificText tffaxen = getTollFreeFaxEn(lba);
                    if (tffaxen != null )
                        lbAddress.getTollFreeFax().add(tffaxen);
                    
                    LanguageSpecificText tffaxfr = getTollFreeFaxFr(lba);
                    if (tffaxfr != null )
                        lbAddress.getTollFreeFax().add(tffaxfr);

                }
            } catch (FinderException fe) {
                
            } catch (RemoteException re) {
                
            }
        }
        
        return lbAddress;
    }
    
    // unit test done
    protected LanguageSpecificText getLocalPhoneEn(LenderToBranchAssoc lba) throws JAXBException{
        
        boolean added = false;
        LanguageSpecificText phone = factory.createLanguageSpecificText();
        if (lba.getELPhoneNumber()!=null && lba.getELPhoneNumber().trim().length()>0) {
            phone.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_ENGLISH));
            String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                    "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_ENGLISH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
            phone.setLanguageName(langDesc);
            phone.setText(lba.getELPhoneNumber());
            added = true;
        }
        
        if (added) return phone;
        else return null;
    }
    
    protected LanguageSpecificText getLocalPhoneFr(LenderToBranchAssoc lba) throws JAXBException{
        
        boolean added = false;
        LanguageSpecificText phone = factory.createLanguageSpecificText();
        if (lba.getFLPhoneNumber()!=null && lba.getFLPhoneNumber().trim().length()>0){
            phone.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_FRENCH));
            String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                    "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_FRENCH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
            phone.setLanguageName(langDesc);
            phone.setText(lba.getFLPhoneNumber());
            added = true;
        }

        if (added) return phone;
        else return null;
    }
    
    // unit test done
    protected LanguageSpecificText getLocalFaxEn( LenderToBranchAssoc lba) throws JAXBException{
        
        boolean added = false;
        LanguageSpecificText fax = factory.createLanguageSpecificText();
        if (lba.getFLPhoneNumber()!=null && lba.getFLPhoneNumber().trim().length()>0){
            fax.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_FRENCH));
            String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                    "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_FRENCH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
            fax.setLanguageName(langDesc);
            fax.setText(lba.getFLPhoneNumber());            
            added = true;
        }
        if (added) return fax;
        else return null;
    }
    
    protected LanguageSpecificText getLocalFaxFr( LenderToBranchAssoc lba) throws JAXBException{
        
        boolean added = false;
        LanguageSpecificText fax = factory.createLanguageSpecificText();
        if (lba.getELPhoneNumber()!=null && lba.getELPhoneNumber().trim().length()>0){
            fax.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_ENGLISH));
            String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                    "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_ENGLISH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
            fax.setLanguageName(langDesc);
            fax.setText(lba.getELPhoneNumber());
            added = true;
        }
        if (added) return fax;
        else return null;
    }
    
    // unit test done
    protected LanguageSpecificText getTollFreePhoneEn(LenderToBranchAssoc lba) throws JAXBException{
        
        boolean added = false;
        LanguageSpecificText tfPhone = factory.createLanguageSpecificText(); 
        if (lba.getETPhoneNumber()!=null && lba.getETPhoneNumber().trim().length()>0) {
            tfPhone.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_ENGLISH));
            String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                    "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_ENGLISH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
            tfPhone.setLanguageName(langDesc);
            tfPhone.setText(lba.getETPhoneNumber()); 
            added = true;
        }
        
        if (added) return tfPhone;
        else return null;
    }
    
    protected LanguageSpecificText getTollFreePhoneFr(LenderToBranchAssoc lba) throws JAXBException{
        
        boolean added = false;
        LanguageSpecificText tfPhone = factory.createLanguageSpecificText(); 
        if (lba.getFTPhoneNumber()!=null && lba.getFTPhoneNumber().trim().length()>0){
            tfPhone.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_FRENCH));
            String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                    "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_FRENCH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
            tfPhone.setLanguageName(langDesc);
            tfPhone.setText(lba.getFTPhoneNumber());
            added = true;
        } 
        
        if (added) return tfPhone;
        else return null;
    }
    
    // unit test done
    protected LanguageSpecificText getTollFreeFaxFr(LenderToBranchAssoc lba) throws JAXBException{
        
        boolean added = false;
        LanguageSpecificText tfFax = factory.createLanguageSpecificText();
        if (lba.getFTFaxNumber()!=null && lba.getFTFaxNumber().trim().length()>0) {
            tfFax.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_FRENCH));
            String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                    "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_FRENCH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
            tfFax.setLanguageName(langDesc);
            tfFax.setText(lba.getFTFaxNumber());
            added = true;
        }
        
        if (added) return tfFax;
        else return null;
    }
    
    protected LanguageSpecificText getTollFreeFaxEn(LenderToBranchAssoc lba) throws JAXBException{
        
        boolean added = false;
        LanguageSpecificText tfFax = factory.createLanguageSpecificText();
        if (lba.getETFaxNumber()!=null && lba.getETFaxNumber().trim().length()>0) {
            tfFax.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_ENGLISH));
            String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                    "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_ENGLISH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
            tfFax.setLanguageName(langDesc);
            tfFax.setText(lba.getETFaxNumber());        
            added = true;
        }
        
        if (added) return tfFax;
        else return null;
    }
    
    // unit test done
    protected LenderContact getLenderContact() throws JAXBException{

        try {
            UserProfile up = new UserProfile(_srk);
            up = up.findByPrimaryKey( new UserProfileBeanPK(this.userProfileId, this._deal.getInstitutionProfileId()));
            if (up!=null) {
                LenderContact lContact 
                    = factory.createConditionStatusUpdateUpdateLenderContact();
    
                lContact.setContactRole(getContactRole(up));
                Contact contact = new Contact(_srk, up.getContactId(), 1);
                if (contact != null) {
                    lContact.setEmail(contact.getContactEmailAddress());
                    lContact.setFirstName(contact.getContactFirstName());
                    lContact.setLastName(contact.getContactLastName());
                    // need Catherin's confirmaion
                    lContact.setPhone(contact.getContactPhoneNumber() 
                            + " v " + contact.getContactPhoneNumberExtension());
                    lContact.setFax(contact.getContactFaxNumber());
                }
                return lContact;
            }
        } catch (RemoteException re) {
            
        } catch (FinderException fe) {
            
        }
        return null; 
    }

    // unit test done
    protected ContactRole getContactRole(UserProfile up) throws JAXBException{
        
        ContactRole contactRole 
            = factory.createConditionStatusUpdateUpdateLenderContactContactRole();
        int userTypeId = up.getUserTypeId();
        contactRole.setRoleId(new Short(""+userTypeId));            
        contactRole.getRoleName().add(getRoleName(userTypeId, Mc.LANGUAGE_PREFERENCE_ENGLISH));
        contactRole.getRoleName().add(getRoleName(userTypeId, Mc.LANGUAGE_PREFERENCE_FRENCH));
        
        return contactRole;
    }
    
    // unit test done
    protected LanguageSpecificText getRoleName(int userTypeId, int language) throws JAXBException{
        
        LanguageSpecificText roleName = factory.createLanguageSpecificText();
     
        String roleDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(),
                "USERTYPE", userTypeId, language);
        
        roleName.setLanguageId(new Byte("" + language));
        String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "LANGUAGEPREFERENCE", language, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        roleName.setLanguageName(langDesc);
        roleName.setText(roleDesc);
        
        return roleName;
    }
    
    
    protected String getXmlPayload(ConditionStatusUpdate csu) throws JAXBException {
        
        if (_payloadXml == null ) {
            if (jc == null ) return null;
            Marshaller marshaller = jc.createMarshaller();
            StringWriter xmlWriter = new StringWriter();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE );
            marshaller.marshal(csu, xmlWriter);
            _payloadXml = xmlWriter.toString();

        }
        return _payloadXml;
    }
    
    
    /*
     * get condition dealnotes 
     */
    protected LanguageSpecificText getDealNote(int language) throws JAXBException {

        LanguageSpecificText conditionDealnote = null;
        try {
            DealNotes dn = new DealNotes(_srk);
            String eText = dn.findLastConditionNoteText(_deal.getDealId(), language);
            if (eText !=null && eText.length()>1) {
                conditionDealnote = factory.createLanguageSpecificText();
                conditionDealnote.setLanguageId(new Byte("" + language));
                String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                        "LANGUAGEPREFERENCE", language, Mc.LANGUAGE_PREFERENCE_ENGLISH);
                conditionDealnote.setLanguageName(langDesc);
                conditionDealnote.setText(eText);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (FinderException e) {
            e.printStackTrace();
        }
        return conditionDealnote;
    }

}
