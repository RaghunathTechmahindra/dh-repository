package com.filogix.express.outbound.loanDecision.payload;

import java.util.Collection;
import java.util.List;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.PartyProfile;
import com.filogix.express.core.ExpressRuntimeException;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.ParticipantType;

public class ParticipantListGenerator {

	public static void populateParticipantList(
			List<ParticipantType> participantList, Deal deal,
			ObjectFactory factory) {

		try {
			for (PartyProfile partyProfile : 
				(Collection<PartyProfile>) deal.getPartyProfiles()) {

				String ptpBusinessId = partyProfile.getPtPBusinessId();

				if (null != ptpBusinessId
						&& ptpBusinessId.trim().length() > 0
						&& partyProfile.getPartyTypeId() == Mc.PARTY_TYPE_SERVICE_BRANCH) {

					// PARTY_TYPE_SERVICE_BRANCH=62

					ParticipantType participantType = factory.createParticipantType();
					participantType.setBranchTransitNo(ptpBusinessId);
					participantList.add(participantType);
				}
			}
		} catch (Exception e) {
			throw new ExpressRuntimeException(e);
		}
	}
}
