/**
 * <p>Title: .java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Ltd. Partnership. (c) 2009</p>
 *
 * <p>Company: Filogix Ltd. Partnership</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � 21-May-09
 *
 */
package com.filogix.express.outbound.dscu;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.XMLGregorianCalendar;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEventStatusUpdateAssoc;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.SystemType;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Update;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Update.StatusInfo.StatusReplacement;
import com.filogix.schema.postoffice.des.LanguageSpecificText;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Update.StatusInfo.Status;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Update.StatusInfo.DealEventStatusType;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Update.StatusInfo;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Routing.Sender.Lender;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Routing.Receiver;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Routing.Sender;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Routing.Sender.Institution;
import com.filogix.schema.postoffice.des.DealEventStatusUpdate.Routing;
import com.filogix.util.Xc;

public class DESUpdatePayloadGenerator extends ServicePayloadGenerator {

    public static final int DESTATUS_TYPE_APPLICATION = 1;
    public static final int DESTATUS_TYPE_MI = 2;
    public static final int DESTATUS_TYPE_APPRASAL = 3;
    public static final int DESTATUS_TYPE_OSC = 4;
    
    private static String logString = "";
    private static com.filogix.schema.postoffice.des.ObjectFactory factory;
    
    protected DealEventStatusUpdate payload;

    private static JAXBContext jc;
    static {
        try {
            jc = JAXBContext.newInstance("com.filogix.schema.postoffice.des");
            factory = new com.filogix.schema.postoffice.des.ObjectFactory();
        } catch (JAXBException e) {
            _logger.error(logString
                    + "Error in getting a JAXBContext instance: "
                    + e.getMessage());
        }
    }
    
    protected Collection<DEStatus> deStatuses;
    protected int userProfileId;
    protected XMLGregorianCalendar timeStamp = null;

    //unit test done
    @Override
    public String getXmlPayload() throws Exception {
        
        DealEventStatusUpdate desu
            = factory.createDealEventStatusUpdate();
        desu.setRouting(getRouting());
        desu.setUpdate(getUpdate());
        try {
            _userprofile = new UserProfile(_srk);
            _userprofile = _userprofile.findByPrimaryKey(new UserProfileBeanPK(userProfileId, _deal.getInstitutionProfileId()));
            timeStamp = getUserTimeStamp(_userprofile);
            desu.setTimeStamp(timeStamp);
        } catch (Exception e) {
            _logger.warning("getXmlPayload couldn't create = " + _deal.getDealId());            
        }
        
        String payload = getXmlPayload(desu);
        return payload;
    }

    public void setUserProfileId(int upId) {
        
        this.userProfileId = upId;
    }

    public void setDEStatuses(Collection<DEStatus> deStatus) {
        
        this.deStatuses = deStatus;
    }

    //unit test done
    protected Routing getRouting() throws JAXBException{
        
        Routing routing 
            = factory.createDealEventStatusUpdateRouting();
        routing.setSender(getSender());
        routing.setReceiver(getReceiver());
        return routing;
    }

    //unit test done
    protected Sender  getSender() throws JAXBException{
        
        Sender sender 
            = factory.createDealEventStatusUpdateRoutingSender();

        sender.setLender(getLender());
        sender.setInstitution(getInstitution());
        sender.setDealId(new Long(this._deal.getDealId()));
        return sender;
    }

    //unit test done
    protected Lender getLender()  throws JAXBException  {

        Lender lender 
            = factory.createDealEventStatusUpdateRoutingSenderLender();
        int lpid = this._deal.getLenderProfileId();
        if (lpid >= 0) {
            try {
                LenderProfile lp = new LenderProfile(_srk, lpid);
                if (lp != null) {
                    lender.setLpBusinessId(lp.getLPBusinessId());
                    lender.setLenderName(lp.getLenderName());
                }
            } catch (RemoteException re) {
                return null;
            } catch (FinderException fe) {
                return null;
            }               
        }
        return lender;
    }
    
    //unit test done
    protected Institution getInstitution() throws JAXBException {
        
        Institution institution 
            = factory.createDealEventStatusUpdateRoutingSenderInstitution();
        
        int ipid = this._deal.getInstitutionProfileId();
        if (ipid >= 0) {
            try {
                InstitutionProfile ip = new InstitutionProfile(_srk, ipid);
                if (ip != null) {
                    institution.setEcniLenderId(ip.getECNILenderId());
                    institution.setInstitutionName(ip.getInstitutionName());
                }
            } catch (RemoteException re) {
                return null;
            } catch (FinderException fe) {
                return null;
            }
        }
        return institution;
    }
    
    //unit test done
    protected Receiver getReceiver() throws JAXBException {
        
        Receiver receiver 
            = factory.createDealEventStatusUpdateRoutingReceiver();
        
        receiver.setSystemType(getSystemType());
        receiver.setSourceApplicationId(this._deal.getSourceApplicationId());
        receiver.setSourceApplicationVersion(this._deal.getSourceApplicationVersion());
        return receiver;
    }
    
    //unit test done
    protected com.filogix.schema.postoffice.des.DealEventStatusUpdate.Routing.Receiver.SystemType getSystemType() throws JAXBException{
        
        com.filogix.schema.postoffice.des.DealEventStatusUpdate.Routing.Receiver.SystemType systemType 
            = factory.createDealEventStatusUpdateRoutingReceiverSystemType();
        int stid = this._deal.getSystemTypeId();
        if (stid >= 0) {
            try {
                SystemType st = new SystemType(_srk, stid);
                if (st != null) {
                    systemType.setSystemTypeId(new Short(""+st.getSystemTypeId()));
                    systemType.setSystemTypeDescription(st.getSystemTypeDescription());
                }
            } catch (RemoteException re) {
                return null;
            } catch (FinderException fe) {
                return null;                
            }
        }
        return systemType;
        
    }

    //unit test done
    protected Update getUpdate() throws JAXBException{
        
        Update update 
            = factory.createDealEventStatusUpdateUpdate();

        update.getStatusInfo().addAll(getStatusInfo());
        return update;
    }

    //unit test done
    protected Collection<StatusInfo> getStatusInfo() throws JAXBException{
        
        int size = deStatuses.size();
        Iterator<DEStatus> des = deStatuses.iterator();
        
        Collection<StatusInfo> statusInfos = new ArrayList<StatusInfo>(size);
        for (int i=0; i<size; i++) {
            DEStatus de = (DEStatus) (des.next());
            int dealId = de.getDeal().getDealId();
            try {
                Deal deal = new Deal(_srk, null);
                deal = deal.findByRecommendedScenario(new DealPK(dealId, 1), true);
                StatusInfo info = factory.createDealEventStatusUpdateUpdateStatusInfo();
                info.setDealEventStatusType(getDEStatusType(de));
                info.setStatus(getStatus(de));
                info.setStatusReplacement(getStatusReplacement(de));
                
                if ("Y".equalsIgnoreCase(PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), 
                        "com.filogix.dealEventStatusUpdate.useStatusText", "N"))) {
                    info.setStatusText(getStatusText(de, Mc.LANGUAGE_PREFERENCE_ENGLISH));
                    info.setStatusText(getStatusText(de, Mc.LANGUAGE_PREFERENCE_FRENCH));
                }

                if (de.getDesAssoc().getDealEventStatusTypeId() == DESTATUS_TYPE_MI) {
                    info.setMiProviderName(getMIProviderName(deal, Mc.LANGUAGE_PREFERENCE_ENGLISH));
                    info.setMiProviderName(getMIProviderName(deal, Mc.LANGUAGE_PREFERENCE_FRENCH));
                }
                
                statusInfos.add(info);
            } catch (RemoteException re) {
                
            } catch (FinderException fe) {
                
            }
        }
        return statusInfos;          
    }

    //unit test done
    protected DealEventStatusType getDEStatusType(DEStatus de) {

        DealEventStatusType deStatusType = factory.createDealEventStatusUpdateUpdateStatusInfoDealEventStatusType();
        int deStatusTypeId = de.getDesAssoc().getDealEventStatusTypeId();
        String deStatusTypeDescEn = BXResources.getPickListDescription(de.getDeal().getInstitutionProfileId(), 
                "DEALEVENTSTATUSTYPE", deStatusTypeId, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        String deStatusTypeDescFr = BXResources.getPickListDescription(de.getDeal().getInstitutionProfileId(), 
                "DEALEVENTSTATUSTYPE", deStatusTypeId, Mc.LANGUAGE_PREFERENCE_FRENCH);
        
        deStatusType.setDealEventStatusTypeId(new Short(""+deStatusTypeId));
        LanguageSpecificText desc = factory.createLanguageSpecificText();
        
        desc.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_ENGLISH));
        String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_ENGLISH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        desc.setLanguageName(langDesc);
        desc.setText(deStatusTypeDescEn);
        deStatusType.getDealEventStatusTypeDescription().add(desc);
        
        desc = factory.createLanguageSpecificText();
        desc.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_FRENCH));
        langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_FRENCH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        desc.setLanguageName(langDesc);
        desc.setText(deStatusTypeDescFr);
        
        deStatusType.getDealEventStatusTypeDescription().add(desc);
                
        return deStatusType;
        
    }

    //unit test done
    protected Status getStatus(DEStatus deStatus) {
        
        Status status = factory.createDealEventStatusUpdateUpdateStatusInfoStatus();
        
        int deTypeId = deStatus.getDesAssoc().getDealEventStatusTypeId();
        Deal deal = deStatus.getDeal();
        int statusId = deStatus.getDesAssoc().getStatusId();
        String deStatusDescEn = "";
        String deStatusDescFr = "";
        switch (deTypeId) {
            case DESTATUS_TYPE_APPLICATION: 
                deStatusDescEn = BXResources.getPickListDescription(deal.getInstitutionProfileId(), 
                        "STATUS", statusId, Mc.LANGUAGE_PREFERENCE_ENGLISH);
                deStatusDescFr = BXResources.getPickListDescription(deal.getInstitutionProfileId(), 
                        "STATUS", statusId, Mc.LANGUAGE_PREFERENCE_FRENCH);
                break;
            case DESTATUS_TYPE_MI:
                deStatusDescEn = BXResources.getPickListDescription(deal.getInstitutionProfileId(), 
                        "MISTATUS", statusId, Mc.LANGUAGE_PREFERENCE_ENGLISH);
                deStatusDescFr = BXResources.getPickListDescription(deal.getInstitutionProfileId(), 
                        "MISTATUS", statusId, Mc.LANGUAGE_PREFERENCE_FRENCH);
                break;
            case DESTATUS_TYPE_APPRASAL:
            case DESTATUS_TYPE_OSC:
                deStatusDescEn = BXResources.getPickListDescription(deal.getInstitutionProfileId(), 
                        "REQUESTSTATUS", statusId, Mc.LANGUAGE_PREFERENCE_ENGLISH);
                deStatusDescFr = BXResources.getPickListDescription(deal.getInstitutionProfileId(), 
                        "REQUESTSTATUS", statusId, Mc.LANGUAGE_PREFERENCE_FRENCH);
                break;
        }
        status.setStatusId(new Short("" + statusId));
        
        String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_ENGLISH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        LanguageSpecificText desc = factory.createLanguageSpecificText();
        desc.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_ENGLISH));
        desc.setLanguageName(langDesc);
        desc.setText(deStatusDescEn);
        status.getStatusDescription().add(desc);
        
        desc = factory.createLanguageSpecificText();
        langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_FRENCH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        desc.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_FRENCH));
        desc.setLanguageName(langDesc);
        desc.setText(deStatusDescFr);
        
        status.getStatusDescription().add(desc);
        return status;
    
    }
    
    //unit test done
    protected StatusReplacement getStatusReplacement(DEStatus deStatus) {
        
        StatusReplacement statusRep = factory.createDealEventStatusUpdateUpdateStatusInfoStatusReplacement();
        
        Deal deal = deStatus.getDeal();
        int statusRepId = deStatus.getDesAssoc().getStatusNameReplacementId();                

        String deStatusDescEn = BXResources.getPickListDescription(deal.getInstitutionProfileId(), 
                "STATUSNAMEREPLACEMENT", statusRepId, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        String deStatusDescFr = BXResources.getPickListDescription(deal.getInstitutionProfileId(), 
                "STATUSNAMEREPLACEMENT", statusRepId, Mc.LANGUAGE_PREFERENCE_FRENCH);
        statusRep.setStatusReplacementId("" + statusRepId); // this doesn't have type in XSD
        
        String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_ENGLISH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        LanguageSpecificText desc = factory.createLanguageSpecificText();
        desc.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_ENGLISH));
        desc.setLanguageName(langDesc);
        desc.setText(deStatusDescEn);
        statusRep.getStatusReplacementDescription().add(desc);
        
        desc = factory.createLanguageSpecificText();
        langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "LANGUAGEPREFERENCE", Mc.LANGUAGE_PREFERENCE_FRENCH, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        desc.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_FRENCH));
        desc.setLanguageName(langDesc);
        desc.setText(deStatusDescFr);

        statusRep.getStatusReplacementDescription().add(desc);
        return statusRep;
    }
    
    //unit test done
    protected LanguageSpecificText getMIProviderName(Deal deal, int language) {
        
        LanguageSpecificText miProviderName = factory.createLanguageSpecificText();
        
        int miProviderId = deal.getMortgageInsurerId();
        String miInsurue = BXResources.getPickListDescription(deal.getInstitutionProfileId(), 
                Xc.PKL_MORTGAGEINSURER, miProviderId, language);

        String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "LANGUAGEPREFERENCE", language, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        miProviderName.setLanguageId(new Byte("" + language));
        miProviderName.setLanguageName(langDesc);
        miProviderName.setText(miInsurue);
        
        return miProviderName;
    }
    

//    private static String ASOFEN = BXResources.getSysMsg("AS_OF", Mc.LANGUAGE_PREFERENCE_ENGLISH);
//    private static String ASOFFR = BXResources.getSysMsg("AS_OF", Mc.LANGUAGE_PREFERENCE_ENGLISH);
    
    //unit test done
    protected LanguageSpecificText getStatusText(DEStatus de, int language) {
        
        LanguageSpecificText statusText = factory.createLanguageSpecificText();
        Deal deal = de.getDeal();
        
        String status = BXResources.getPickListDescription(deal.getInstitutionProfileId(), 
                "STATUSNAMEREPLACEMENT", de.getDesAssoc().getStatusNameReplacementId(), language);
         
        statusText.setLanguageId(new Byte("" + Mc.LANGUAGE_PREFERENCE_ENGLISH));
        String langDesc = BXResources.getPickListDescription(this._deal.getInstitutionProfileId(), 
                "LANGUAGEPREFERENCE", language, Mc.LANGUAGE_PREFERENCE_ENGLISH);
        statusText.setLanguageName(langDesc);
        statusText.setText(status);
        
        return statusText;
    }
    
    
    protected String getXmlPayload(DealEventStatusUpdate desu) throws JAXBException {
        if (_payloadXml == null ) {
            if (jc == null ) return null;
            Marshaller marshaller = jc.createMarshaller();
            StringWriter xmlWriter = new StringWriter();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE ); 
            marshaller.marshal(desu, xmlWriter);
            _payloadXml = xmlWriter.toString();
        }
        return _payloadXml;
    }

    /**
     * DEStatus
     * inner calss for DESUpdatePayloadGenerator
     * 
     * <p>
     * 
     *       DealEventStatusUpdateAssoc assoc = new DealEventStatusUpdateAssoc(_srk);
     *       if (deStatusTypeId == DESTATUS_TYPE_APPLICATION) {
     *           assoc = assoc.findByPrimaryKey(deal.getSystemTypeId(), deStatusTypeId, deal.getStatusId());
     *       } else if (deStatusTypeId == DESTATUS_TYPE_MI) {
     *           assoc = assoc.findByPrimaryKey(deal.getSystemTypeId(), deStatusTypeId, deal.getMIStatusId());
     *       } else if (deStatusTypeId == DESTATUS_TYPE_APPRASAL) {
     *           Request request = new Request(_srk);
     *           request = request.findLastRequestByDealIdandCopyIdandServiceSubTypeId(deal.getDealId(), deal.getCopyId(), 
     *              ServiceConst.SERVICE_SUB_TYPE_APPRAISAL);
     *           assoc = assoc.findByPrimaryKey(deal.getSystemTypeId(), deStatusTypeId, request.getRequestStatusId());                
     *       } else if (deStatusTypeId == DESTATUS_TYPE_OSC) {
     *           Request request = new Request(_srk);
     *           request = request.findLastRequestByDealIdandCopyIdandServiceSubTypeId(deal.getDealId(), deal.getCopyId(), 
     *              ServiceConst.SERVICE_SUB_TYPE_OUTSORCED_CLOSING);
     *           assoc = assoc.findByPrimaryKey(deal.getSystemTypeId(), deStatusTypeId, request.getRequestStatusId());                
     *      } 
     * 
     * @author maida
     *
     */
    public class DEStatus {
        
        private Deal deal;
        private DealEventStatusUpdateAssoc desAssoc;
        private Date timestamp;
        
        public DEStatus(Deal deal, DealEventStatusUpdateAssoc desAssoc, Date timestamp) {
            
            this.deal=deal;
            this.desAssoc = desAssoc;
            this.timestamp = timestamp;
        }

        public DEStatus(Deal deal, int deStatusTypeId, Date timestamp) {
            
            this.deal=deal;
            try {
                DealEventStatusUpdateAssoc assoc = new DealEventStatusUpdateAssoc(deal.getSessionResourceKit());
                assoc.setSilentMode(true);
                if (deStatusTypeId == DESTATUS_TYPE_APPLICATION) {
                    assoc = assoc.findByPrimaryKey(deal.getSystemTypeId(), deStatusTypeId, 
                            deal.getStatusId());
                } else if (deStatusTypeId == DESTATUS_TYPE_MI) {
                    assoc = assoc.findByPrimaryKey(deal.getSystemTypeId(), deStatusTypeId, 
                            deal.getMIStatusId());
                } else if (deStatusTypeId == DESTATUS_TYPE_APPRASAL) {
                    Request request = new Request(_srk);
                    request = request.findLastRequestByDealIdandCopyIdandServiceSubTypeId(
                            deal.getDealId(), deal.getCopyId(), ServiceConst.SERVICE_SUB_TYPE_APPRAISAL);
                    assoc = assoc.findByPrimaryKey(deal.getSystemTypeId(), deStatusTypeId, 
                            request.getRequestStatusId());                
                } else if (deStatusTypeId == DESTATUS_TYPE_OSC) {
                    Request request = new Request(_srk);
                    request = request.findLastRequestByDealIdandCopyIdandServiceSubTypeId(
                            deal.getDealId(), deal.getCopyId(), ServiceConst.SERVICE_SUB_TYPE_OUTSORCED_CLOSING);
                    assoc = assoc.findByPrimaryKey(deal.getSystemTypeId(), deStatusTypeId, 
                            request.getRequestStatusId());                
               } 
               this.desAssoc = assoc;
            } catch (Exception e) {
                _logger.error("DEStatus: couldn't find DealEventStatusUpdateAssoc for dealid =  " 
                        + deal.getDealId() + " deStatusTypeId = " + deStatusTypeId);
            }
            
            this.timestamp = timestamp;
        }

        public Date getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Date timestamp) {
            this.timestamp = timestamp;
        }

        public Deal getDeal() {
            return deal;
        }

        public void setDeal(Deal deal) {
            this.deal = deal;
        }

        public DealEventStatusUpdateAssoc getDesAssoc() {
            return desAssoc;
        }

        public void setDesAssoc(DealEventStatusUpdateAssoc desAssoc) {
            this.desAssoc = desAssoc;
        }
    }
}
