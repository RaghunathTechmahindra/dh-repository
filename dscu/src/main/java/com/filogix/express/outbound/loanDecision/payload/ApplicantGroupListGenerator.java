package com.filogix.express.outbound.loanDecision.payload;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import MosSystem.Mc;

import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.Deal;
import com.filogix.express.core.ExpressRuntimeException;
import com.filogix.schema.fcx._1.Address1Type;
import com.filogix.schema.fcx._1.ApplicantAddressType;
import com.filogix.schema.fcx._1.ApplicantDecisionType;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.BasePersonType.Name;
import com.filogix.schema.fcx._1.LoanDecisionType.ApplicantGroup;

public class ApplicantGroupListGenerator {

	
	public static final int BT_GUARANTOR_EXTERNAL = 2;
	public static final int BT_BORROWER_EXTERNAL = 0;
	public static final int MARITALSTATUS_MARRIED = 2;
	public static final int MARITALSTATUS_COMMONLAW = 3;
	
	public static void populateApplicantGroupList(List<ApplicantGroup> applicantGroupList, Deal deal, ObjectFactory factory){
		
		Collection<Borrower> borrowersEX = null;
		try {
			borrowersEX = deal.getBorrowers();
			if(null == borrowersEX || borrowersEX.isEmpty())
				return;
		} catch (Exception e) {
			throw new ExpressRuntimeException(e);
		}
		//Get sorted borrowers.
		ArrayList<Borrower> borrowers = new ArrayList<Borrower>(borrowersEX);
		java.util.Collections.sort(borrowers, new BorrowerComparator());

		for(int i = 0; i < borrowers.size(); i++) {
			Borrower curBorrower = borrowers.get(i);
			
			ApplicantGroup currentGroup = factory.createLoanDecisionTypeApplicantGroup();

			
			//Set borrower type.
			int fcxTypeId = 0;
			
			if(curBorrower.getBorrowerTypeId() == Mc.BT_BORROWER){
				currentGroup.setApplicantGroupTypeDd(Convert.toBigDecimal(BT_BORROWER_EXTERNAL));
			}else if(curBorrower.getBorrowerTypeId() == Mc.BT_GUARANTOR){
				currentGroup.setApplicantGroupTypeDd(Convert.toBigDecimal(BT_GUARANTOR_EXTERNAL));
			}else{
				//we only care borrower and guarantor
				continue;
			}
			applicantGroupList.add(currentGroup);
			
			//Put current borrower into the group.
			List<ApplicantDecisionType> applicants = currentGroup.getApplicant();
			ApplicantDecisionType applicantDecisionType = factory.createApplicantDecisionType();
			applicants.add(applicantDecisionType);
			
			populateApplicantDecisionType(applicantDecisionType, curBorrower, deal, factory);
			
			//Test marital status to determine if we need to include the next borrower into the group.
			if(curBorrower.getMaritalStatusId() == MARITALSTATUS_MARRIED
					|| curBorrower.getMaritalStatusId() == MARITALSTATUS_COMMONLAW) { 
				//Married or in a common law relationship.
				//Look forward to the next record and test if it has the same marital statusId.
				int j = i + 1;
				if(j < borrowers.size()) {
					Borrower nextBorrower = borrowers.get(j);
					if( curBorrower.getBorrowerTypeId() == nextBorrower.getBorrowerTypeId() 
						&& curBorrower.getMaritalStatusId() == nextBorrower.getMaritalStatusId()) {
							ApplicantDecisionType applicantDecisionTypeSpouse = factory.createApplicantDecisionType();
							applicants.add(applicantDecisionTypeSpouse);
							populateApplicantDecisionType(applicantDecisionTypeSpouse, nextBorrower, deal, factory);
							i++;
					}
				}
			} 
		}
	}
	
	/**
	 * Populates the detailed info for applicantDecionType.
	 * @param applicantDecisionType
	 * @param borrower
	 * @param factory
	 * @throws LoanDecisionPayloadGeneratorException
	 */
	protected static void populateApplicantDecisionType(
			ApplicantDecisionType applicantDecisionType, 
			Borrower borrower, Deal deal, ObjectFactory factory) {
		
		//Language preference.
		applicantDecisionType.setLanguagePreferenceDd(
				Convert.toBigDecimal(borrower.getLanguagePreferenceId()));
		
		//Set applicant's primary borrower flag.
		applicantDecisionType.setPrimaryApplicantFlag("Y".equalsIgnoreCase(borrower.getPrimaryBorrowerFlag()) ?"1":"2");

		//Set applicant's name.
		Name name = factory.createBasePersonTypeName();
		name.setSalutationDd(Convert.toBigDecimal(borrower.getSalutationId()));
		name.setFirstName(borrower.getBorrowerFirstName());
		name.setLastName(borrower.getBorrowerLastName());
		name.setMiddleInitial(borrower.getBorrowerMiddleInitial());
        name.setSuffixDd(Convert.toBigDecimal(borrower.getSuffixId()));
		applicantDecisionType.setName(name);
		
		//Set address(es) here.
		try {
			Collection<BorrowerAddress> addresses = borrower.getBorrowerAddresses();
			Iterator<BorrowerAddress> addressesIt = addresses.iterator();
			
			List<ApplicantAddressType> applicantAddressTypeList 
				= applicantDecisionType.getAddressDetail();
			
			while(addressesIt.hasNext()) {
				Addr address = addressesIt.next().getAddr();
				ApplicantAddressType applicantAddressType 
					= factory.createApplicantAddressType();
				applicantAddressTypeList.add(applicantAddressType);
				
				Address1Type address1Type = 
					Address1TypeGenerator.generateAddress1Type(address, factory);
				applicantAddressType.setAddress(address1Type);
			}
			
		} catch (Exception e) {
			throw new ExpressRuntimeException(e);
		}
	}
}
