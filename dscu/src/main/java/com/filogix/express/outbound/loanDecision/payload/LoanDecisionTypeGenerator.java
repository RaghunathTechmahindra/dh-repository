package com.filogix.express.outbound.loanDecision.payload;

import java.util.List;

import com.basis100.deal.entity.Deal;
import com.filogix.schema.fcx._1.LoanDecisionType;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.ParticipantType;
import com.filogix.schema.fcx._1.PropertyType;
import com.filogix.schema.fcx._1.LoanDecisionType.ApplicantGroup;

public class LoanDecisionTypeGenerator {
	
	public static LoanDecisionType createLoanDecisionType(Deal deal, ObjectFactory factory){
		
		LoanDecisionType loanDecisionType = factory.createLoanDecisionType();

		loanDecisionType.setDeal(DealTypeGenerator.generateDealType(deal, factory));
		loanDecisionType.setMortgage(MortgageTypeGenerator.createMortgageType(deal, factory));
		loanDecisionType.setResponse(ResponseTypeGenerator.generateResponseType(deal, factory));
	
		List<ApplicantGroup> applicantGroupList = loanDecisionType.getApplicantGroup();
		ApplicantGroupListGenerator.populateApplicantGroupList(applicantGroupList, deal, factory);
	
		List<PropertyType> subjectPropertyList = loanDecisionType.getSubjectProperty();
		PropertyListGenerator.populatePropertyList(subjectPropertyList, deal, factory);
	
		List<ParticipantType> participantList = loanDecisionType.getParticipant();
		ParticipantListGenerator.populateParticipantList(participantList, deal, factory);

		return loanDecisionType;
	}
}
