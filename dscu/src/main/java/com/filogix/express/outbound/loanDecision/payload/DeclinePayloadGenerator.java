package com.filogix.express.outbound.loanDecision.payload;

import java.util.List;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.filogix.schema.fcx._1.DealNotesType;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.ResponseType;
import com.filogix.schema.fcx._1.ResponseType.Decline;

public class DeclinePayloadGenerator extends DataAlignmentPayloadGenerator //implements IDAPayloadGenerator
{
	/**
	 * 
	 * @see com.filogix.express.outbound.loanDecision.payload.DataAlignmentPayloadGenerator#createResponseType(com.filogix.schema.fcx._1.LoanDecisionType,
	 *      com.basis100.deal.entity.Deal,
	 *      com.filogix.schema.fcx._1.ObjectFactory)
	 */
	@Override
	protected void populateResponseTypeSpecific(ResponseType responseType,
			Deal deal, ObjectFactory factory) {

		Decline decline = factory.createResponseTypeDecline();
		responseType.setDecline(decline);

		//decline.setDenialReasonDd(Convert.toBigDecimal(deal.getDenialReasonId()));
		List<DealNotesType> listOfDenialReasons = decline.getDenialReasons();
		
		if(deal.getDenialReasonId() != Mc.DENIAL_REASON_IC_REJECT) {
			DealNotesGenerator.populateDealNotesListWithDenialReasonAndFirstQualifiedNotes(listOfDenialReasons, deal, factory);
		} else {
			DealNotesGenerator.populateDealNotesListWithICNotes(listOfDenialReasons, deal, factory);
		}

	}
}
