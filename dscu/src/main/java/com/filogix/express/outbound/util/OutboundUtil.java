package com.filogix.express.outbound.util;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEventStatusSnapshot;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.security.DLM;
import com.basis100.entity.CreateException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.IOutboundProcessor;
import com.filogix.express.outbound.OutboundManager;
import com.filogix.express.outbound.OutboundProcessException;
import com.filogix.express.outbound.OutboundWorker;
import com.filogix.externallinks.framework.ServiceConst;

/**
 * ESBStatusUtil util class for dscu deamon
 *
 * @version 1.0 Jun 9, 2009
 */
public class OutboundUtil {

    private final static Logger logger = 
        LoggerFactory.getLogger(OutboundUtil.class);
    
    /**
     * increament the attempt counter
     *
     * @param srvRespItem
     * @param step
     * @param srk
     * @throws OutboundProcessException
     */
    public static void incrementCounter(ESBOutboundQueue statusItem, int step,
            SessionResourceKit srk, int attemptCounter)
            throws OutboundProcessException {

        if (statusItem.getAttemptCounter() < attemptCounter) {
            try {
                statusItem.setAttemptCounter(statusItem.getAttemptCounter()
                        + step);
                statusItem.ejbStore();
            } catch (Exception e) {
                logger.error("Failed to increment Counter: "
                        + statusItem.getESBOutboundQueueId(), e);
                throw new OutboundProcessException(
						"Failed to increment Counter:", e);
            }
        }
    }

    /**
     * Change ESBOutboundQueue processStatus to newStaus
     *
     * @param srvRespItem
     * @param newStatus
     * @param srk
     * @throws OutboundProcessException
     */
    public static void changeProcessStatus(ESBOutboundQueue statusItem,
            int newStatus, String newDesc, SessionResourceKit srk)
            throws OutboundProcessException {
        try {
            if(statusItem == null) return;
            statusItem.setTransactionStatusId(newStatus);
            statusItem.setTransactionStatusDescription(StringUtils.substring(newDesc,0,200));
            statusItem.ejbStore();
        } catch (Exception e) {
            logger.error("failed to change Process status: "
                    + statusItem.getESBOutboundQueueId(), e);
            throw new OutboundProcessException(
					"Failed change process status", e);
        }
    }

    /**
     * Change ESBOutboundQueue retry timestamp to current time
     *
     * @param srvRespItem
     * @param newStatus
     * @param srk
     * @throws OutboundProcessException
     */
    public static void setRetryTimeStamp(ESBOutboundQueue statusItem,
            SessionResourceKit srk)
            throws OutboundProcessException {
        try {
            //statusItem.setRetryTimeStamp(new Date());
            if(statusItem == null) return;
            statusItem.setRetryTimeStampDBSystemDate();
            statusItem.ejbStore();
        } catch (Exception e) {
            logger.error("failed to change Process status: "
                    + statusItem.getESBOutboundQueueId(), e);
            throw new OutboundProcessException(
					"Failed change process status", e);
        }
    }

    /**
     * get max attempt from system property
     * @param statusEvent
     * @return
     */
    public static int getMaxAttemptValue(ESBOutboundQueue statusEvent) {
        String maxAttempt = PropertiesCache.getInstance().getProperty(
                statusEvent.getInstitutionProfileId(), OutboundConst.MAX_ATTEMP, "10");
        if (maxAttempt == null) {
            return 0;
        } else {
            return new Integer(maxAttempt).intValue();
        }
    }


    /**
     * save new replaymentId in dealeventstatussnapshot table
     *
     * @param srk
     * @param snapShot
     * @param replacementId
     * @throws RemoteException 
     * @throws JdbcTransactionException
     * @throws RemoteException 
     */
    public static void saveDealEventStatusSnapShot(SessionResourceKit srk,
            DealEventStatusSnapshot snapShot, int replacementId) throws RemoteException {

        snapShot.setStatusNameReplacementId(replacementId);
        snapShot.ejbStore();

    }


    /**
     * remove current queue
     *
     * @param srk
     * @param queue
     * @throws JdbcTransactionException
     * @throws RemoteException
     */
    public static void removeCurrentQueue(SessionResourceKit srk,
            ESBOutboundQueue queue)
            throws JdbcTransactionException, RemoteException {

        queue.ejbRemove();
    }


    /**
     * create new replaymentId in dealeventstatussnapshot table
     *
     * @param srk
     * @param deal
     * @param typeId
     * @param replacementId
     * @throws CreateException
     * @throws JdbcTransactionException
     * @throws RemoteException
     */
    public static void createDealEventStatusSnapShot(SessionResourceKit srk,
            Deal deal, int typeId, int replacementId) throws CreateException,
            JdbcTransactionException, RemoteException {

        DealEventStatusSnapshot snapShot = new DealEventStatusSnapshot(srk);
        snapShot.create(deal, typeId, replacementId);
    }

}
