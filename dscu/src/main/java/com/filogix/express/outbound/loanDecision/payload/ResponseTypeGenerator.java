package com.filogix.express.outbound.loanDecision.payload;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.Addr;
//import com.basis100.deal.entity.BranchProfile;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.entity.UserProfile;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.filogix.express.core.ExpressRuntimeException;
import com.filogix.schema.fcx._1.Address2Type;
import com.filogix.schema.fcx._1.ContactType;
import com.filogix.schema.fcx._1.PhoneType;
import com.filogix.schema.fcx._1.ResponseType;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.ContactType.Address;
import com.filogix.schema.fcx._1.ContactType.ContactName;
import com.filogix.schema.fcx._1.ResponseType.LenderProfile;
import com.filogix.schema.fcx._1.ResponseType.LenderProfile.BranchProfile;

public class ResponseTypeGenerator {


	private final static Logger logger = LoggerFactory
	.getLogger(ResponseTypeGenerator.class);

	private static final int DEFAULT_PHONE_TYPE = 3;
	
	protected static ResponseType generateResponseType(Deal deal, ObjectFactory factory) {
		ResponseType responseType = factory.createResponseType();
		populateResponseType(responseType, deal, factory);
		return responseType;

	}

	protected static void populateResponseType(ResponseType responseType,
			Deal deal, ObjectFactory factory) {

		try{
			responseType.setCommitmentIssueDate(Convert.toXMLGregorianCalendar(deal.getCommitmentIssueDate()));
			//Set ecniLenderId
			int institutionProfileId = deal.getInstitutionProfileId();
			InstitutionProfile institutionProfileFinder = new InstitutionProfile(deal.getSessionResourceKit());
			InstitutionProfile institutionProfile = institutionProfileFinder.findByPrimaryKey(new InstitutionProfilePK(institutionProfileId));
			String ecniLenderId = institutionProfile.getECNILenderId();
			if(ecniLenderId.equalsIgnoreCase("FLT")) {
				responseType.setLenderReferenceNumber(Convert.toTrimedString(deal.getServicingMortgageNumber()));
			} else {
				responseType.setLenderReferenceNumber(Convert.toString(deal.getDealId()));
			}

			LenderProfile lenderProfileFcx = factory.createResponseTypeLenderProfile();
			responseType.setLenderProfile(lenderProfileFcx);

			try {
				com.basis100.deal.entity.LenderProfile lenderProfileDb = deal.getLenderProfile();
				lenderProfileFcx.setLenderName(lenderProfileDb.getLenderName());
				lenderProfileFcx.setRegistrationName(lenderProfileDb.getRegistrationName());

			} catch (FinderException e) {
				logger.warn("no LenderProfile exists", e);
			} catch (RemoteException e) {
				logger.warn("faild to get LenderProfile info", e);
			}

			//populate branch profile here.
			BranchProfile branchProfile_fcx = factory.createResponseTypeLenderProfileBranchProfile();
			lenderProfileFcx.setBranchProfile(branchProfile_fcx);
			try {
				com.basis100.deal.entity.BranchProfile branchProfile = deal.getBranchProfile();
				branchProfile_fcx.setBranchName(branchProfile.getBranchName());
				
				Contact branchContact = (null != branchProfile)? branchProfile.getContact():null;
				Addr branchAddr = (null != branchContact)? branchContact.getAddr(): null;
				if(null != branchAddr) {
					
					Address address = factory.createContactTypeAddress();
					Address2Type address2Type = factory.createAddress2Type();
					address.setAddress2Type(address2Type);
					populateAddress2Type(branchAddr, address2Type);
					ContactType branchContactType = factory.createContactType();
					branchProfile_fcx.setContact(branchContactType);
					branchContactType.setAddress(address);
					
				}
			} catch (FinderException e) {
				logger.warn("no BranchProfile/Contact/Addr exists", e);
			} catch (RemoteException e) {
				logger.warn("faild to get BranchProfile/Contact/Addr", e);
			}
			//Populate Underwriter Information			
			int underwriterUserId = deal.getUnderwriterUserId();
			UserProfile userProfileFinder = new UserProfile(deal.getSessionResourceKit());
			UserProfile userProfile = userProfileFinder.findByPrimaryKey(new UserProfileBeanPK(underwriterUserId, deal.getInstitutionProfileId()));
			Contact userContact = null;
			try {
				userContact = userProfile.getContact();
			} catch (FinderException e) {
				logger.warn("no contact info exists", e);
			}

			if(userContact != null ){
				ContactType contactType = factory.createContactType();
				responseType.setUnderwriter(contactType);
				populateContactType(contactType, userContact, factory);
			}

		} catch (Exception e) {
			throw new ExpressRuntimeException(e);
		}
	}

	protected static void populateContactType(ContactType contactType,
			Contact contactDb, ObjectFactory factory) throws RemoteException, FinderException {

		contactType.setContactEmailAddress(contactDb.getContactEmailAddress());
		contactType.setLanguagePreferenceDd(Convert.toBigDecimal(contactDb.getLanguagePreferenceId()));

		ContactName contactName = factory.createContactTypeContactName();
		populateContactName(contactDb, contactName);
		contactType.setContactName(contactName);

		PhoneType contactPhone = factory.createPhoneType();
		populatePhoneType(contactDb, contactPhone);
		contactType.getContactPhone().add(contactPhone);

		contactType.setLanguagePreferenceDd(Convert.toBigDecimal(contactDb.getLanguagePreferenceId()));

		try {
			Addr addrDb = contactDb.getAddr();

			Address address = factory.createContactTypeAddress();
			Address2Type address2Type = factory.createAddress2Type();

			contactType.setAddress(address);
			address.setAddress2Type(address2Type);

			populateAddress2Type(addrDb, address2Type);

		} catch (FinderException e) {
			logger.warn("no addr exitsts in addr ", e);
		}


	}

	protected static void populateContactName(Contact contact,
			ContactName contactName) {

		contactName.setSalutationDd(Convert.toBigDecimal(contact.getSalutationId()));
		contactName.setContactFirstName(contact.getContactFirstName());
		contactName.setContactLastName(contact.getContactLastName());
		contactName.setContactMiddleInitial(contact.getContactMiddleInitial());
	}

	protected static void populatePhoneType(Contact contact,
			PhoneType contactPhone) {

		contactPhone.setPhoneTypeDd(Convert.toBigDecimal(DEFAULT_PHONE_TYPE));
		contactPhone.setPhoneNumber(contact.getContactPhoneNumber());
		contactPhone.setPhoneExtension(contact.getContactPhoneNumberExtension());
	}

	protected static void populateAddress2Type(Addr addr,
			Address2Type address2Type) {

		address2Type.setAddressLine1(addr.getAddressLine1());
		address2Type.setAddressLine2(addr.getAddressLine2());
		
		if (StringUtils.isNotEmpty(addr.getCity()))
			address2Type.setCity(addr.getCity());
		
		if(addr.getProvinceId() != Mc.PROVINCE_OTHER )//if addr.provinceid = 0 then "don't return value"
			address2Type.setProvinceDd(Convert.toBigDecimal(addr.getProvinceId()));
		
		//if contact.addr.PostalFSA and contact.addr.PostalLDU exist
		// then contact.addr.PostalFSA + contact.addr.PostalLDU
		if (StringUtils.isNotEmpty(addr.getPostalFSA())
				&& StringUtils.isNotEmpty(addr.getPostalLDU())) {
			address2Type.setPostalFsa(addr.getPostalFSA());
			address2Type.setPostalLdu(addr.getPostalLDU());
		}
	}
}
