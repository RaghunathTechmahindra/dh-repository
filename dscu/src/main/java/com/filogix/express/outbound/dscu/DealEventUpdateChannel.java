/**
 * <p>Title: .java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Ltd. Partnership. (c) 2009</p>
 *
 * <p>Company: Filogix Ltd. Partnership</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � 17-Jun-09
 *
 */
package com.filogix.express.outbound.dscu;

import MosSystem.Mc;

import com.filogix.express.outbound.ExpressOutboundContext;
import com.filogix.externallinks.framework.ServiceConst;

import java.net.ConnectException;
import java.rmi.RemoteException;
import java.util.List;

import org.apache.axiom.soap.SOAPHeaderBlock;
import org.apache.axis2.AxisFault;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;

import com.filogix.postoffice.DealStatusServiceStub;
import com.filogix.postoffice.InfoDocument;
import com.filogix.postoffice.PayloadDocument;
import com.filogix.util.Xc;


public class DealEventUpdateChannel extends ChannelDSCU {

    protected static Log _logger = LogFactory.getLog(DealEventUpdateChannel.class);
    
    protected DealStatusServiceStub     serviceStub;
    
    public DealEventUpdateChannel() {
    }
           
    @Override
    public Object send(String payload) throws PostOfficeServiceException  {

        PayloadDocument reqDoc = PayloadDocument.Factory.newInstance();
        XmlObject xObj =null;
        try {
            xObj = XmlObject.Factory.parse(payload);
        } catch (XmlException e) {
            _logger.error("failed to parsing " +
                    "payload@DealEventUpdateChannel: ", e);
            throw new PostOfficeServiceException(e);
        }
        
        PayloadDocument.Payload req = reqDoc.addNewPayload();
        req.set(xObj);
        try {
            
            InfoDocument info = serviceStub.Send(reqDoc);
            _logger.debug(info.toString());
            return info;
        } 
        catch (RemoteException e) {
            _logger.error("failed to send to " +
                    "postoffice@DealEventUpdateChannel: ", e);
            PostOfficeServiceException se = new PostOfficeServiceException(e);
            if (e instanceof AxisFault){
                AxisFault fault = (AxisFault)e;
                if (e.detail instanceof ConnectException) {
                    _logger.error("Connect Exception on CLIENT ");
                    se.setFaultCode(ServiceConst.ESBOUT_TRANSACTIONSTATUS_TIMEOUT);
                    se.setFaultDescription(e.getCause().toString());
                } else if (e.detail instanceof java.net.SocketTimeoutException){
                        _logger.error("Timeout Exception on CLIENT ");
                        se.setFaultCode(ServiceConst.ESBOUT_TRANSACTIONSTATUS_TIMEOUT);
                        se.setFaultDescription(e.getCause().toString());
                } else if (fault.getFaultSubCodes() != null && fault.getFaultSubCodes().size() > 0){
                    
                    List resList= fault.getFaultSubCodes();
                    String faultResponse = (String)resList.get(0);
                    _logger.error(faultResponse);
                    se.setFaultDetails(faultResponse);
                } else { // added for new error handling
                    String errorCode = "";
                    String errorStrig = "";
                    if (fault.getFaultCode()!=null) {
                        String faultCode = fault.getFaultCode().getLocalPart();
                        if (faultCode != null && faultCode.indexOf(".") > 0) {
                            errorCode = faultCode.substring(faultCode.indexOf(".") + 1);
                            se.setFaultCode(new Integer(errorCode).intValue());
                        } else {
                            _logger.error("************ Unknown error Happend!!!! for DealId = " + _deal.getDealId() + " ************* ");
                            se.setFaultCode(ServiceConst.ESBOUT_TRANSACTIONSTATUS_SYSTEMERROR);
                        }
                        String faultDetails = fault.getMessage();
                        if (fault.getFaultDetailElement() != null )
                            faultDetails += " *** " + fault.getFaultDetailElement().toString();
                        _logger.error(faultDetails);
                        se.setFaultDescription(faultDetails);
                    } else {
                        _logger.error("************ Unknown error Happend!!!! for DealId = " + _deal.getDealId() + " ************* ");
                        _logger.error(e);
                        se.setFaultCode(ServiceConst.ESBOUT_TRANSACTIONSTATUS_SYSTEMERROR);
                        se.setFaultDescription("Inknown Error from PostOffice");
                    }
                }
                throw se;
            }
            throw se;
        }
    }

    /**
     * <p>initRequestBean
     * initialize requestBean i.e. FxRequest for this request
     * @see com.filogix.externallinks.datx.NoStoredRequestChannel#initRequestBean()
     */
     public void initRequestBean()
         throws PostOfficeServiceException {
         try {
             serviceStub = new DealStatusServiceStub(_url);
             setHeaders(serviceStub);
         } catch (AxisFault af) {
             _logger.error("failed to init request " +
                    "bean@DealEventUpdateChannel", af);
             throw new PostOfficeServiceException(af);
         }
     }

    public void setChannelId(int id) {
        channelId = id;
    }
    
    public void setAction() {
    
        Object actionO = ExpressOutboundContext.getService(Mc.REQUEST_HEADER_ESBPOSTOFFICE 
            + Mc.REQUEST_HEADER_ACTION + "_" + Mc.REQUEST_HEADER_ACTION_DSU);
        RequestHeaderBlock actionRequestHeader = (RequestHeaderBlock)actionO;
        SOAPHeaderBlock actionHD = actionRequestHeader.getSOAPHeaderBlock();
        actionHD.setMustUnderstand(true);
        //this value is supposed to be String
        actionHD.setText((String)actionRequestHeader.getValue());
        serviceStub._getServiceClient().addHeader(actionHD);
    }
}
