package com.filogix.express.outbound.loanDecision.payload;

import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.docprep.DocPrepLanguage;
import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.Deal;
import com.filogix.express.core.ExpressRuntimeException;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.ResponseType;
import com.filogix.schema.fcx._1.ResponseType.Commitment;
import com.filogix.schema.fcx._1.ResponseType.Commitment.DealFee;
import com.filogix.schema.fcx._1.ResponseType.Commitment.DealFee.Fee;
import com.filogix.schema.fcx._1.ResponseType.Commitment.DocumentTracking;

public class CommitmentPayloadGenerator extends DataAlignmentPayloadGenerator {
	
	public static final String[] SECTION_CODES = {"PREP", "FLEX","RADJ","ASUM","PORT","SVCH","COND", "INST", "MCM"};
	public static final String OTHER_SECTION_CODE = "OTHR";

	private final static Logger logger = LoggerFactory
	.getLogger(CommitmentPayloadGenerator.class);

	/**
	 * 
	 * @see com.filogix.express.outbound.loanDecision.payload.DataAlignmentPayloadGenerator#createResponseType(com.filogix.schema.fcx._1.LoanDecisionType,
	 *      com.basis100.deal.entity.Deal,
	 *      com.filogix.schema.fcx._1.ObjectFactory)
	 */
	@Override
	protected void populateResponseTypeSpecific(ResponseType responseType,
			Deal deal, ObjectFactory factory) {

		Commitment commitment = factory.createResponseTypeCommitment();
		responseType.setCommitment(commitment);

		commitment.setCommitmentExpirationDate(Convert
				.toXMLGregorianCalendar(deal.getCommitmentExpirationDate()));
		commitment.setReturnDate(Convert
				.toXMLGregorianCalendar(deal.getReturnDate()));
		
		populateDealFees(deal, factory, commitment);

		List<DocumentTracking> fcxDocTrackList = commitment
		.getDocumentTracking();

		Collection<com.basis100.deal.entity.DocumentTracking> collectionOfDocTrack = null;

		try {
			collectionOfDocTrack = deal.getDocumentTracks();
		} catch (Exception e) {
			logger.warn("faild to find document tracking data", e);
			// when no data founds, don't throw exception
			// throw new ExpressRuntimeException(e);
		}

		populateListOfDocumentTracking(deal, factory, fcxDocTrackList,
				collectionOfDocTrack);

		// Create lenderNotes.
		DealNotesGenerator.populateDealNotesListWithFirstQualifiedNotes(
				responseType.getLenderNotes(), deal, factory);

	}

	/**
	 * Populates the DealFee and Fee nodes in commitment.
	 * @param deal
	 * @param factory
	 * @param commitment
	 */
	protected void populateDealFees(Deal deal, ObjectFactory factory,
			Commitment commitment) {
		//Create DealFee node here.
		Collection<com.basis100.deal.entity.DealFee> dealFeesEX = null;
		
		try {
			dealFeesEX = deal.getDealFees();
		} catch (Exception e) {
			logger.warn("faild to find dealFee data", e);
		}
		if(null != dealFeesEX) {
			for(com.basis100.deal.entity.DealFee dealFeeEX: dealFeesEX) {
				com.basis100.deal.entity.Fee feeEX = null;
				try {
					feeEX = dealFeeEX.getFee();
				} catch (Exception e) {
					logger.error("Unable to locate fee for DealFeeID " + dealFeeEX.getDealFeeId());
					throw new ExpressRuntimeException(e);
				}
								
				
		        int feeStatusId = dealFeeEX.getFeeStatusId();				
				boolean isPayable = feeEX.getPayableIndicator();
		        int feeType = feeEX.getFeeTypeId();
		        int payor = deal.getMIPayorId();
		        
		        /*
		         * Modifying the fee status to include waived status, remove the not condition and
		         * adding new fee types as part of QC Ticket # 142
		         */
		        
		        if (isPayable ||  (feeStatusId == Mc.FEE_STATUS_REQUIRED || feeStatusId == Mc.FEE_STATUS_RECIEVED)
		            || (payor == Mc.MI_PAYOR_LENDER
		                 && (feeType == Mc.FEE_TYPE_CMHC_FEE //3
		                 || feeType == Mc.FEE_TYPE_CMHC_PREMIUM //11
		                 || feeType == Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM //13
		                 || feeType == Mc.FEE_TYPE_CMHC_PRE_QUALIFICATION_FEE //29
		                 || feeType == Mc.FEE_TYPE_CMHC_LOAN_ASSESSMENT_FEE //28
		                 || feeType == Mc.FEE_TYPE_GE_CAPITAL_FEE //15
		                 || feeType == Mc.FEE_TYPE_GE_CAPITAL_PREMIUM //17 
		                 || feeType == Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM //19
		                 || feeType == Mc.FEE_TYPE_AIG_UG_FEE //37
		                 || feeType == Mc.FEE_TYPE_AIG_UG_PREMIUM //39
		                 || feeType == Mc.FEE_TYPE_AIG_UG_PST_ON_PREMIUM)) //41
		            )
		          continue;
				
				
				/*if(null != feeEX 
						&& (!feeEX.getPayableIndicator() && feeEX.getMITypeFlag().equals("N"))) {*/
					DealFee dealFee = factory.createResponseTypeCommitmentDealFee();
					dealFee.setFeeAmount(Convert.toBigDecimal(dealFeeEX.getFeeAmount()));
					dealFee.setFeePaymentDate(Convert.toXMLGregorianCalendar(dealFeeEX.getFeePaymentDate()));
					dealFee.setFeePaymentMethodDd(Convert.toBigDecimal(dealFeeEX.getFeePaymentMethodId()));
					dealFee.setFeeStatusDd(Convert.toBigDecimal(dealFeeEX.getFeeStatusId()));
					Fee fee = factory.createResponseTypeCommitmentDealFeeFee();
					fee.setRefundable(feeEX.getRefundable());
					fee.setFeePayorTypeDd(Convert.toBigDecimal(feeEX.getFeePayorTypeId()));
					if(null!=feeEX.getFCXFeeTypeID()) {
						fee.setFeeTypeDd(Convert.toBigDecimal(feeEX.getFCXFeeTypeID().intValue()));
					}
					dealFee.setFee(fee);
					commitment.getDealFee().add(dealFee);
				/*}*/
			}
		}
	}

	/**
	 * @param deal
	 * @param factory
	 * @param fcxDocTrackList
	 * @param collectionOfDocTrack
	 */
	protected void populateListOfDocumentTracking(
			Deal deal,
			ObjectFactory factory,
			List<DocumentTracking> fcxDocTrackList,
			Collection<com.basis100.deal.entity.DocumentTracking> collectionOfDocTrack) {

		if (collectionOfDocTrack == null || collectionOfDocTrack.isEmpty())
			return;

		DocPrepLanguage dpl = DocPrepLanguage.getInstance();
		int languageId = dpl.findPreferredLanguageId(deal,
				deal.getSessionResourceKit());

		for (com.basis100.deal.entity.DocumentTracking dt : collectionOfDocTrack) {

			// FXP30610, Mar 9, 2012, adding filter -- start
			/*
			 * <pre> if doctracking status = waived and approved, then ignore.
			 * if condition.conditiontypeid = "commitment only" or
			 * "commitment and doc tracking" where doctracking.conditionid =
			 * condition.conditiontype.id </pre>
			 */
			if (dt.getDocumentStatusId() == Mc.DOC_STATUS_WAIVED
					|| dt.getDocumentStatusId() == Mc.DOC_STATUS_APPROVED) {
				// ignore when document status is either waived or approved
				logger.debug("skipped docuemntTracking record ("
						+ dt.getDocumentTrackingId()
						+ ") because of the docStatusId");
				continue;
			}

			try {
				Condition condition = new Condition(dt.getSessionResourceKit(),
						dt.getConditionId());
				if (condition.getConditionTypeId() != Mc.CONDITION_TYPE_COMMITMENT_ONLY
						&& condition.getConditionTypeId() != Mc.CONDITION_TYPE_COMMITMENT_AND_DOC_TRACKING) {
					logger.debug("skipped docuemntTracking record ("
							+ dt.getDocumentTrackingId()
							+ ") because of the ConditionTypeId");
					continue;
				}

			} catch (Exception e) {
				throw new ExpressRuntimeException(e);
			}
			// FXP30610, Mar 9, 2012, adding filter -- end

			// FXP27097: Only generate document tracking when text exists.
			if (null != StringUtils.trimToNull(dt.getDocumentTextByLanguage(languageId))) {

				DocumentTracking fcxDocTrack = factory
						.createResponseTypeCommitmentDocumentTracking();

				populateDocumentTracking(languageId, dt, fcxDocTrack);

				fcxDocTrackList.add(fcxDocTrack);
			}
		}
	}

	/**
	 * @param languageId
	 * @param dt
	 * @param fcx
	 */
	protected void populateDocumentTracking(int languageId,
			com.basis100.deal.entity.DocumentTracking dt,
			DocumentTracking fcx) {

		fcx.setDocumentStatusDd(Convert.toBigDecimal(dt
				.getDocumentStatusId()));
		fcx.setDocumentText(
				Convert.toCleanDocumentText(
						dt.getDocumentTextByLanguage(languageId)));
		
		//Get ConditionSection.
		try {
			fcx.setSequenceId(Convert.toBigDecimal(dt.getSequenceId()));
			
			for(String sectionCode:SECTION_CODES) {
				if(sectionCode.equals(dt.getSectionCode())) {
					fcx.setSectionCode(dt.getSectionCode());
					return;
				}
			}
			//Loop finished we didn't find a match. Set OTHR.
			fcx.setSectionCode(OTHER_SECTION_CODE);
		} catch (Exception e) {
			throw new ExpressRuntimeException(e);
		}
	}


}
