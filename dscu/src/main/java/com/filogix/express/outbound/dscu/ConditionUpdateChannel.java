package com.filogix.express.outbound.dscu;

import java.net.ConnectException;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.List;

import org.apache.axiom.soap.SOAPHeaderBlock;
import org.apache.axis2.AxisFault;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;


import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.pk.DealPK;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.ExpressOutboundContext;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.postoffice.ConditionUpdateServiceStub;
import com.filogix.postoffice.InfoDocument;
import com.filogix.postoffice.PayloadDocument;
import com.filogix.util.Xc;


public class ConditionUpdateChannel extends ChannelDSCU {

    protected static Log _logger = LogFactory.getLog(ConditionUpdateChannel.class);
    
    protected ConditionUpdateServiceStub     serviceStub;
    
    public ConditionUpdateChannel() {
    }
       
    @Override
    public Object send(String payload) throws PostOfficeServiceException  {

        PayloadDocument reqDoc = PayloadDocument.Factory.newInstance();
        XmlObject xObj =null;
        try {
            xObj = XmlObject.Factory.parse(payload);
        } catch (XmlException e) {
            _logger.error("failed to parsing " +
                    "payload@ConditionUpdateChannel: ", e);
            throw new PostOfficeServiceException(e);
        }
        
        PayloadDocument.Payload req = reqDoc.addNewPayload();
        req.set(xObj);
        try {
            
            InfoDocument info = serviceStub.send(reqDoc);
            _logger.debug("Send to DP sucuess: " + info.toString());
            return info;
        } catch (RemoteException e) {
            _logger.error("failed to send to " +
                    "postoffice@ConditionUpdateChannel: ", e);
            PostOfficeServiceException se = new PostOfficeServiceException(e);
            if (e instanceof AxisFault){
                AxisFault fault = (AxisFault)e;
                if (e.detail instanceof ConnectException) {
                    _logger.error("Connect Exception on CLIENT ");
                    se.setFaultCode(ServiceConst.ESBOUT_TRANSACTIONSTATUS_TIMEOUT);
                    se.setFaultDescription(e.getCause().toString());
                } else if (e.detail instanceof java.net.SocketTimeoutException){
                        _logger.error("Timeout Exception on CLIENT ");
                        se.setFaultCode(ServiceConst.ESBOUT_TRANSACTIONSTATUS_TIMEOUT);
                        se.setFaultDescription(e.getCause().toString());
                } else if (fault.getFaultSubCodes() != null && fault.getFaultSubCodes().size() > 0){
                    
                    List resList= fault.getFaultSubCodes();
                    String faultResponse = (String)resList.get(0);
                    se.setFaultCode(ServiceConst.ESBOUT_TRANSACTIONSTATUS_XMLERROR);
                    _logger.error(faultResponse);
                    se.setFaultDetails(faultResponse);

                } else { // added for new error handling
                    String errorCode = "";;
                    if (fault.getFaultCode()!=null) {
                        String faultCode = fault.getFaultCode().getLocalPart();
                        if (faultCode != null && faultCode.indexOf(".") > 0) {
                            errorCode = faultCode.substring(faultCode.indexOf(".") + 1);
                            se.setFaultCode(new Integer(errorCode).intValue());
                        } else {
                            _logger.error("************ Unknown error Happend!!!! for DealId = " + _deal.getDealId() + " ************* ");
                            se.setFaultCode(ServiceConst.ESBOUT_TRANSACTIONSTATUS_SYSTEMERROR);
                        }
                        String faultDetails = fault.getMessage();
                        if (fault.getFaultDetailElement() != null )
                            faultDetails += " *** " + fault.getFaultDetailElement().toString();
                        _logger.error(faultDetails);
                        se.setFaultDescription(faultDetails);
                    } else {
                        _logger.error("************ Unknown error Happend!!!! for DealId = " + _deal.getDealId() + " ************* ");
                        se.setFaultCode(ServiceConst.ESBOUT_TRANSACTIONSTATUS_SYSTEMERROR);
                        se.setFaultDescription("Inknown Error from PostOffice");
                    }
                }
                throw se;
            }
            throw se;
        }
    }

    /**
     * <p>initRequestBean
     * initialize requestBean i.e. FxRequest for this request
     * @see com.filogix.externallinks.datx.NoStoredRequestChannel#initRequestBean()
     */
     public void initRequestBean()
         throws PostOfficeServiceException {
         try {
             serviceStub = new ConditionUpdateServiceStub(_url);
             setHeaders(serviceStub);
             
         } catch (AxisFault af) {
             _logger.error("failed to init request " +
                    "bean@ConditionUpdateChannel", af);
             throw new PostOfficeServiceException(af);
         }
     }

     public void setChannelId(int id) {
         channelId = id;
     }
     
     public void setAction() {
         
         Object actionO = ExpressOutboundContext.getService(Mc.REQUEST_HEADER_ESBPOSTOFFICE 
             + Mc.REQUEST_HEADER_ACTION + "_" + Mc.REQUEST_HEADER_ACTION_CM);
         
         RequestHeaderBlock actionRequestHeader = (RequestHeaderBlock)actionO;
         SOAPHeaderBlock actionHD = actionRequestHeader.getSOAPHeaderBlock();
         actionHD.setMustUnderstand(true);
         //this value is supposed to be String
         actionHD.setText((String)actionRequestHeader.getValue());
         serviceStub._getServiceClient().addHeader(actionHD);
     }


    /*********** Below this point, all methods are for Test purpose *******/

    public static void main(String[] args) throws Exception {

        SessionResourceKit srk = new SessionResourceKit("System");

        DocumentTracking dt = new DocumentTracking(srk);
        ConditionUpdatePayloadGenerator payloadGenerator = new ConditionUpdatePayloadGenerator();
        int dealId = 100152;
        Deal deal = new Deal(srk, null);
        deal.setSilentMode(true);
        deal = deal.findByPrimaryKey(new DealPK(dealId, 1));
        Collection<DocumentTracking> dts = dt.findByDeal(new DealPK(dealId, 1));
        payloadGenerator.init(deal);
        payloadGenerator.setConditions(dts);
        int upId = deal.getUnderwriterUserId();
        payloadGenerator.setUserProfileId(upId);
        String payload = payloadGenerator.getXmlPayload();
        System.out.println("******* payload *********\n" + payload);

        ConditionUpdateChannel conditionChannel = new ConditionUpdateChannel();
        conditionChannel.setChannelId(17);
        conditionChannel.init(srk, deal);

        conditionChannel.send(payload);
    }

}
