package com.filogix.express.outbound;

import com.basis100.resources.SessionResourceKit;

public abstract class AbstractOutboundProcessor implements IOutboundProcessor {

    protected SessionResourceKit srk;
    protected String support;
    protected boolean debugmode = true;
    protected boolean dealLockRequired;

    public void setSessionResourceKit(SessionResourceKit srk) {
        this.srk = srk;
    }

    public SessionResourceKit getSrk() {
        return this.srk;
    }

    public String getSupport() {
        return this.support;
    }

    public void setSupport(String support) {
        this.support = support;
    }

    public boolean isDealLockRequired() {
        return dealLockRequired;
    }

    public void setDealLockRequired(boolean dealLockRequired) {
        this.dealLockRequired = dealLockRequired;
    }

    public boolean isDebugmode() {
        return debugmode;
    }

    public void setDebugmode(boolean debugmode) {
        this.debugmode = debugmode;
    }

}
