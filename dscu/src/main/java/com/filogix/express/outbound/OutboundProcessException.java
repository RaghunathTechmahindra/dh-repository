package com.filogix.express.outbound;

/**
 * ConditionProcessException  
 *
 */
public class OutboundProcessException extends Exception {

    /**
     * ConditionProcessException Constructor
     */
    public OutboundProcessException() {

        super();
    }

    /**
     * @param message
     */
    public OutboundProcessException(String message) {

        super(message);
    }

    /**
     * @param message
     * @param tr
     */
    public OutboundProcessException(String message, Throwable tr) {

        super(message, tr);
    }

    /**
     * @param tr
     */
    public OutboundProcessException(Throwable tr) {

        super(tr);
    }
}
