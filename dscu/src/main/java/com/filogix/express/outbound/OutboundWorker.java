package com.filogix.express.outbound;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.security.DLM;
import com.basis100.entity.RemoteException;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.email.EmailSender;
import com.filogix.express.outbound.util.OutboundConst;
import com.filogix.express.outbound.util.OutboundUtil;
import com.filogix.externallinks.framework.ServiceConst;

public class OutboundWorker implements Runnable {

    private final static Logger logger = LoggerFactory.getLogger(OutboundWorker.class);

    private int esbOutBoundQueueId;
    private int institutionProfileId;
    private boolean acquiredDealLock = false;
    private int systemUserId;

    public OutboundWorker(int esbOutBoundQueueId, int institutionProfileId, int systemUserId) {
        this.esbOutBoundQueueId = esbOutBoundQueueId;
        this.institutionProfileId = institutionProfileId;
        this.systemUserId = systemUserId;
    }

    /**
     * method to pick queue from esboutboundqueue and execute process
     * 
     */
    public void run() {

        // naming thread; you can see this with "%t" in log4j
        Thread.currentThread().setName("QID:" + esbOutBoundQueueId);
        
        logger.info("dscu starting ... " + esbOutBoundQueueId);

        SessionResourceKit srk = null;
        ESBOutboundQueue rec = null;

        try {
            srk = new SessionResourceKit("" + esbOutBoundQueueId);
            srk.getConnection().setAutoCommit(true);
            srk.getExpressState().setDealInstitutionId(institutionProfileId);

            rec = new ESBOutboundQueue(srk, esbOutBoundQueueId);

            IOutboundProcessor processor = (IOutboundProcessor)
                ExpressOutboundContext.getService(OutboundConst.SERVICEPROCESSOR_PREFIX + rec.getServiceTypeId());

            // check if this is supported service
            if ("Y".equals(PropertiesCache.getInstance().getProperty(
                institutionProfileId, processor.getSupport(), "Y")) == false) {
                logger.warn("queueid = " + esbOutBoundQueueId
                    + " is not supported. please check system property : "
                    + processor.getSupport());
                return; // do nothing
            }

            // deal lock
            if (processor.isDealLockRequired()) {
                if (placeDealLock(processor, rec, srk, systemUserId)) {
                    acquiredDealLock = true;
                }else{
                    logger.warn("failed to acquire deallcok for deal=" + rec.getDealId());
                    // failed to place deal lock
                    // set current time stamp to wait
                    OutboundUtil.setRetryTimeStamp(rec, srk);
                    // exit
                    return;
                }
            }
            
            processor.setSessionResourceKit(srk);
            processor.handleProcess(rec);

        } catch (Throwable e) {
            // critical error
            logger.error("failed in dscu worker: ", e);

            try {
                if (rec != null) {
                    rec.setTransactionStatusId(ServiceConst.ESBOUT_TRANSACTIONSTATUS_SYSTEMERROR);
                    rec.setTransactionStatusDescription(StringUtils.substring(e.getMessage(), 0, 200));
                    rec.ejbStore();
                }
            } catch (RemoteException e1) {
                logger.error(e1.getMessage(), e1);
            }

            String msg = "Unrecoverable exception encountered in ESBOutbound Daemon." + "  QueueId: " + esbOutBoundQueueId + "  DealId: "
                + rec.getDealId() + "  InstitutionProfileId: " + institutionProfileId;
            EmailSender.sendAlertEmail("ESBOutbound.OutboundWorker", e, null, msg);
        } finally {
            //release deal lock
            if (acquiredDealLock) {
                unLockDeal(rec, srk, systemUserId);
            }
            srk.freeResources();
            logger.info("dscu thread finishing... " + esbOutBoundQueueId);
        }
    }

    public static boolean placeDealLock(IOutboundProcessor processor, ESBOutboundQueue rec, SessionResourceKit srk, int systemUserId) {
        try {
            String name = processor.getClass().getSimpleName();
            DLM dlm = DLM.getInstance();
            dlm.placeLock(rec.getDealId(), systemUserId, "Lock by DSCU for " + name + "", srk, true);
    
            return true;
        } catch (Exception e) {
            logger.warn("Deal was locked by other user", e);
            return false;
        }
    }

    public static boolean unLockDeal(ESBOutboundQueue rec, SessionResourceKit srk, int systemUserId) {
        try {
            DLM dlm = DLM.getInstance();
            dlm.doUnlock(rec.getDealId(), systemUserId, srk, true, true);
            return true;
        } catch (Exception e) {
            logger.warn("Problem when try to unlock the deal", e);
            return false;
        }
    }
}
