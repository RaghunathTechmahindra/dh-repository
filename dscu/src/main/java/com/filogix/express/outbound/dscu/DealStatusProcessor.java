package com.filogix.express.outbound.dscu;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEventStatusSnapshot;
import com.basis100.deal.entity.DealEventStatusUpdateAssoc;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.entity.Request;
import com.basis100.deal.pk.DealEventStatusSnapshotPK;
import com.basis100.deal.pk.DealEventStatusUpdateAssocPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.IOutboundProcessor;
import com.filogix.express.outbound.OutboundProcessException;
import com.filogix.express.outbound.util.OutboundConst;
import com.filogix.express.outbound.util.OutboundUtil;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.express.outbound.dscu.DESUpdatePayloadGenerator.DEStatus;;

/**
 * DealStatusProcessor 
 * 
 * @version 1.0 Jun 9, 2009
 */
public class DealStatusProcessor extends ESBStatusProcessor {

    private final static Log _log = LogFactory
            .getLog(DealStatusProcessor.class);

    private ESBOutboundQueue statusEvent;
    private IPayloadGenerator payloadGenerator;
    private IChannel channel;
    
    // this string is from spring application context.
    // retriable error code, comma delimiter
    // set only once
    private static String retryErrorCodes = null;
    // create set for etry error codes set
    // set only once
    private static Set<String> retryErrorCodeList = null;

    public DealStatusProcessor() {
        super();
    }
    
    /* 
     * handle process statuseventqueue
     * (non-Javadoc)
     * @see com.filogix.express.dscu.processor.AbstractStatusProcessor#handleProcess(java.lang.Object)
     */
    public void handleProcess(Object obj) throws OutboundProcessException {

        channel = new DealEventUpdateChannel();
        channel.setChannelId(OutboundConst.DEALSTATUS_CHANNEL_ID);
        payloadGenerator = new DESUpdatePayloadGenerator();
        statusEvent = (ESBOutboundQueue) obj;
        _log.info("dscu processing queueid: "
                + statusEvent.getESBOutboundQueueId());
        try {
            SessionResourceKit srk = this.getSrk();
            DealPK dpk = new DealPK(statusEvent.getDealId(), -1);
            Deal deal = new Deal(srk, null);
//            deal.setSilentMode(true);
            deal = deal.findByRecommendedScenario(dpk, true);
            _log.info("found recommended scenrio deal for " + deal.getDealId() 
                    + " copyId = " + deal.getCopyId());
            // update esboutboundqueue retrytimestamp
            OutboundUtil.setRetryTimeStamp(statusEvent, srk);
            
            Collection<DEStatus> deStatus = new ArrayList<DEStatus>();
            int attemptCounter = OutboundUtil.getMaxAttemptValue(statusEvent);
            
            if (checkMIUpdate(deal) || checkAppraisalUpdate(deal)
                    || checkOSCUpdate(deal) || checkDealStatusUpdate(deal)) {

                //check mi status update
                int statusId = deal.getMIStatusId();
                DESUpdatePayloadGenerator.DEStatus statusItem = checkStatusUpdateTrigger(
                        deal, DESUpdatePayloadGenerator.DESTATUS_TYPE_MI,
                        statusId);
                if(statusItem!=null)
                    deStatus.add(statusItem);

                //check appraisal status update
                int serviceTypeId = ServiceConst.SERVICE_SUB_TYPE_APPRAISAL;
                Request request = getServiceRequest(deal, serviceTypeId);
                if (request != null) {
                    statusId = request.getRequestStatusId();
                    statusItem = checkStatusUpdateTrigger(deal,
                            DESUpdatePayloadGenerator.DESTATUS_TYPE_APPRASAL,
                            statusId);
                    deStatus.add(statusItem);
                }

                //check osc status update
                serviceTypeId = ServiceConst.SERVICE_SUB_TYPE_OUTSORCED_CLOSING;
                request = getServiceRequest(deal, serviceTypeId);
                if (request != null) {
                    statusId = request.getRequestStatusId();
                    statusItem = checkStatusUpdateTrigger(deal,
                            DESUpdatePayloadGenerator.DESTATUS_TYPE_OSC,
                            statusId);
                    if(statusItem!=null)
                        deStatus.add(statusItem);
                }

                //check deal status update
                statusId = deal.getStatusId();
                statusItem = checkStatusUpdateTrigger(deal,
                        DESUpdatePayloadGenerator.DESTATUS_TYPE_APPLICATION,
                        statusId);
                if(statusItem!=null)
                    deStatus.add(statusItem);
            }

            //generate payload
            if (deStatus.size() > 0 ) {
                payloadGenerator.init(deal);
                payloadGenerator.setUserProfileId(statusEvent.getUserProfileId());
                payloadGenerator.setDEStatuses(deStatus);
                String payloadXml = payloadGenerator.getXmlPayload();
                
                _log.info("dscu generate payload: " + payloadXml);
                // get Channel 
                channel.init(srk, deal);
                try {
                    channel.send(payloadXml);
                    handleDealStatusPostOfficeResult(deal, statusEvent, deStatus);
                }
                catch (PostOfficeServiceException pe){
                    int faultCode = pe.getFaultCode();
                    String faultDescription = "" + faultCode + ": " + pe.getFaultDescription();
                    switch (faultCode) {
                    case ServiceConst.ESBOUT_TRANSACTIONSTATUS_TIMEOUT:
                        OutboundUtil.incrementCounter(statusEvent, 1, srk,
                                attemptCounter);
                        sendEmailNotification(deal, statusEvent, faultCode);
                        break;
                    default:
                        // this is special case due to DataPower failer issue
                        // must retry 
                        if (retryErrorCodeList.contains(""+faultCode)) {
                            OutboundUtil.incrementCounter(statusEvent, 1, srk,
                                    attemptCounter);
                            sendEmailNotification(deal, statusEvent, faultCode);
                        } else {
                            handleFailureMessage(faultCode, faultDescription,  deal, statusEvent);
                        } break;
                    }
                }
            } else {
                _log.info("deal/event processor no deal update for this queue");
                OutboundUtil.changeProcessStatus(statusEvent,
                		ServiceConst.ESBOUT_TRANSACTIONSTATUS_SYSTEMERROR, "UPDATE IS NOT NESESSARY", deal
                                .getSessionResourceKit());
            }
        } catch (Exception e) {
            _log.error("exceptions in DealStatusProcessor!", e);
            throw new OutboundProcessException(e);
        }
    }

    /**
     * check with Snapshot table for current update
     * 
     * @param deal
     * @param srk
     * @return
     * @throws RemoteException 
     * @throws JdbcTransactionException 
     * @throws CreateException 
     */
    public boolean checkStatusUpdate(Deal deal, int typeId, int statusId)
            throws RemoteException, JdbcTransactionException, CreateException {

        boolean update = false;
        SessionResourceKit srk = deal.getSessionResourceKit();
        DealEventStatusUpdateAssoc updateAssoc = new DealEventStatusUpdateAssoc(
                srk);
        updateAssoc.setSilentMode(true);
        try {
            updateAssoc.findByPrimaryKey(new DealEventStatusUpdateAssocPK(deal
                    .getSystemTypeId(), typeId, statusId));
        } catch (FinderException e1) {
            _log.info("Fail to find DealEventStatusUpdateAssoc in DealStatusProcessor");
            updateAssoc = null;
        }

        if (updateAssoc != null) {
            int replacementId = updateAssoc.getStatusNameReplacementId();
            DealEventStatusSnapshot dealSnapShot = new DealEventStatusSnapshot(srk);
            dealSnapShot.setSilentMode(true);
            try {
                dealSnapShot = dealSnapShot.findByEventStatusType(
                        new DealEventStatusSnapshotPK(statusEvent.getDealId(), typeId),
                        typeId);
            } catch (Exception e) {
                _log.info("Fail to find DealEventStatusSnapshot in DealStatusProcessor");
                dealSnapShot = null;
            }

            if (dealSnapShot == null
                    || dealSnapShot.getStatusNameReplacementId() != replacementId) {
                update = true;
            }
        }
        return update;
    }

    /**
     * method to handle the result from datapower
     * 
     * @param result
     * @param deal
     * @throws JdbcTransactionException
     * @throws RemoteException
     * @throws CreateException 
     * @throws OutboundProcessException 
     */
    private void handleDealStatusPostOfficeResult(Deal deal, ESBOutboundQueue queue,
            Collection<DEStatus> deStatus) throws JdbcTransactionException,
            RemoteException, CreateException, OutboundProcessException {
       
        _log.info("transaction successful from post office with queueId:"
                + queue.getESBOutboundQueueId());
        Iterator deStatusIter = deStatus.iterator();

        while (deStatusIter.hasNext()) {
            
            DEStatus deStatusObj = (DEStatus)deStatusIter.next();
            DealEventStatusUpdateAssoc assoc = deStatusObj.getDesAssoc();
            int typeId = assoc.getDealEventStatusTypeId();
            int statusId = assoc.getStatusNameReplacementId();
            
            DealEventStatusSnapshot dealSnapShot = new DealEventStatusSnapshot(srk);
            dealSnapShot.setSilentMode(true);
            try {
                dealSnapShot = dealSnapShot.findByEventStatusType(
                        new DealEventStatusSnapshotPK(statusEvent.getDealId(), typeId),
                        typeId);
            } catch (FinderException e) {
                _log.info("Fail to find DealEventStatusSnapshot in DealStatusProcessor");
                dealSnapShot = null;
            }
            
            if(dealSnapShot==null) {
                OutboundUtil.createDealEventStatusSnapShot(srk,
                        deal, typeId, statusId);
            }
            else {
                OutboundUtil.saveDealEventStatusSnapShot(srk,
                        dealSnapShot, statusId);
            }
        }
        
        OutboundUtil.changeProcessStatus(queue,
        		ServiceConst.ESBOUT_TRANSACTIONSTATUS_SUCCESS, "SUCCEED", srk);
        
        if(!debugmode) {
        	OutboundUtil.removeCurrentQueue(srk, queue);
        }
    }

    /**
     * method find current request for service sub type
     * 
     * @param deal
     * @param request
     * @param serviceTypeId
     * @return
     */
    public Request getServiceRequest(Deal deal,
            int serviceTypeId) {
        
        Request request = null;
        try {
            request = new Request(srk);
            request.setSilentMode(true);
            request = request
                    .findLastRequestByDealIdandCopyIdandServiceSubTypeId(deal
                            .getDealId(), deal.getCopyId(), serviceTypeId);
            _log.info("Found Service Request in DealStatusProcessor: DealId = " + deal.getDealId() 
                    + " copyId = " + deal.getCopyId() + " ServiceSubTypeId = " + serviceTypeId);
        } catch (Exception e) {
            _log.info("Fail to get Service Request in DealStatusProcessor: DealId = " + deal.getDealId() 
                    + " copyId = " + deal.getCopyId() + " ServiceSubTypeId = " + serviceTypeId);
            return null;
        }
        return request;
    }

    
    /**
     * check with deal MI status update
     * @throws CreateException 
     * @throws JdbcTransactionException 
     * @throws RemoteException 
     */
    public boolean checkMIUpdate(Deal deal) throws RemoteException,
            JdbcTransactionException, CreateException {
        boolean update = false;
        if (deal.getMIStatusId() > 0) {
            update = checkStatusUpdate(deal,
                    DESUpdatePayloadGenerator.DESTATUS_TYPE_MI, deal
                            .getMIStatusId());
        }
        return update;
    }

    /**
     * check with appraisal updates
     * @throws CreateException 
     * @throws JdbcTransactionException 
     * @throws RemoteException 
     */
    public boolean checkAppraisalUpdate(Deal deal) throws RemoteException,
            JdbcTransactionException, CreateException {
        boolean update = false;
        int serviceTypeId = ServiceConst.SERVICE_SUB_TYPE_APPRAISAL;
        Request request = getServiceRequest(deal, serviceTypeId);
        if (request != null && request.getRequestId() > 0) {
            update = checkStatusUpdate(deal,
                    DESUpdatePayloadGenerator.DESTATUS_TYPE_APPRASAL, request
                            .getRequestStatusId());
        }
        return update;
    }

    /**
     * check with OSC updates
     * @throws CreateException 
     * @throws JdbcTransactionException 
     * @throws RemoteException 
     */
    public boolean checkOSCUpdate(Deal deal) throws RemoteException,
            JdbcTransactionException, CreateException {
        boolean update = false;
        int serviceTypeId = ServiceConst.SERVICE_SUB_TYPE_OUTSORCED_CLOSING;
        Request request = getServiceRequest(deal, serviceTypeId);
        if (request != null && request.getRequestId() > 0) {
            update = checkStatusUpdate(deal,
                    DESUpdatePayloadGenerator.DESTATUS_TYPE_OSC, request
                            .getRequestStatusId());
        }
        return update;
    }

    /**
     * check with deal status updates
     * @throws CreateException 
     * @throws JdbcTransactionException 
     * @throws RemoteException 
     */
    public boolean checkDealStatusUpdate(Deal deal) throws RemoteException,
            JdbcTransactionException, CreateException {
        boolean update = checkStatusUpdate(deal,
                DESUpdatePayloadGenerator.DESTATUS_TYPE_APPLICATION, deal
                        .getStatusId());
        return update;
    }
    
    /**
     *
     * @throws RemoteException 
     */
    public DESUpdatePayloadGenerator.DEStatus checkStatusUpdateTrigger(
            Deal deal, int typeId, int statusId) throws RemoteException {
        DESUpdatePayloadGenerator.DEStatus deStatusItem = null;
        
        DealEventStatusUpdateAssoc updateAssoc = new DealEventStatusUpdateAssoc(
                srk);
        updateAssoc.setSilentMode(true);
        try {
            updateAssoc.findByPrimaryKey(new DealEventStatusUpdateAssocPK(deal
                    .getSystemTypeId(), typeId, statusId));
        } catch (FinderException e1) {
            _log.info("Fail to find DealEventStatusUpdateAssoc");
            updateAssoc = null;
        }

        if (updateAssoc != null) {
            deStatusItem = createDEStatus(deal, typeId, statusEvent);

        } else {
            DealEventStatusSnapshot dealSnapShot = new DealEventStatusSnapshot(
                    srk);
            dealSnapShot.setSilentMode(true);
            try {
                dealSnapShot = dealSnapShot.findByEventStatusType(
                        new DealEventStatusSnapshotPK(statusEvent.getDealId(), typeId),
                        typeId);
            } catch (FinderException e) {
                _log.info("Fail to find DealEventStatusSnapshot");
                dealSnapShot = null;
            }
            if (dealSnapShot != null)
                deStatusItem = createDEStatus(deal, typeId, statusEvent);
        }
        return deStatusItem;
    }
    
    /**
     * 
     * create a DEStatus Item entry
     * @param deal
     * @param typeId
     * @param queue
     * @return
     */
    public DESUpdatePayloadGenerator.DEStatus createDEStatus(Deal deal,
            int typeId, ESBOutboundQueue queue) {
        
        DESUpdatePayloadGenerator.DEStatus statusItem = null;
        DESUpdatePayloadGenerator payload = new DESUpdatePayloadGenerator();
        payload.init(deal);
        try {
            statusItem = payload.new DEStatus(deal, typeId, statusEvent
                    .getTimeStamp());
        } catch (Exception e) {
            _log.info("Fail to create DEStatus Item and set to null");
        }
        return statusItem;
    }
    
    public Set<String> getRetryErrorCodeList(){
        return retryErrorCodeList;
    }
    
    public void setRetryErrorCodeList(Set<String> errorList){
        retryErrorCodeList = errorList;
    }
    
    public void setRetryErrorCodes(String errorCodes) {
        if (retryErrorCodes == null) {
            retryErrorCodes = errorCodes;
            super.setRetryErrorCodes(errorCodes);
        }
    }
}
