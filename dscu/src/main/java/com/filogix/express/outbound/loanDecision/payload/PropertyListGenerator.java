package com.filogix.express.outbound.loanDecision.payload;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.EscrowPayment;
import com.basis100.deal.entity.Property;
import com.filogix.express.core.ExpressRuntimeException;
import com.filogix.schema.fcx._1.Address1Type;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.PropertyType;
import com.filogix.schema.fcx._1.PropertyType.PropertyExpense;

public class PropertyListGenerator {

	protected static void populatePropertyList(List<PropertyType> propertyList, Deal deal, ObjectFactory factory) {
		try {

			Collection<Property> properties = deal.getProperties();
			Iterator<Property> propertiesIt = properties.iterator();
			while(propertiesIt.hasNext()) {

				Property property = propertiesIt.next();
				PropertyType propertyType = factory.createPropertyType();
				propertyType.setPropertyTypeDd(Convert.toBigDecimal(property.getPropertyTypeId()));
				propertyType.setPurchasePrice(Convert.toBigDecimal(property.getPurchasePrice()));
				propertyType.setDwellingTypeDd(Convert.toBigDecimal(property.getDwellingTypeId()));
				
				String primaryPropertyFlag = String.valueOf(property.getPrimaryPropertyFlag());
				propertyType.setPrimaryPropertyFlag(primaryPropertyFlag);
				if("Y".equalsIgnoreCase(primaryPropertyFlag))
					propertyType.setSubjectPropertyFlag(primaryPropertyFlag);
				
				int dwellingStyle = getDwellingStyleDd(property.getDwellingStyleId());
				if(dwellingStyle != -1)
					propertyType.setDwellingStyleDd(Convert.toBigDecimal(dwellingStyle));

				Address1Type address1Type = Address1TypeGenerator
				.generateAddress1Type(property, factory);
				propertyType.setAddress(address1Type);

				populatePropertyExpenseList(propertyType.getPropertyExpense(),
						deal.getPropertyExpenses(), factory);


				propertyList.add(propertyType);
			}
		} catch (Exception e) {
			throw new ExpressRuntimeException(e);
		}

	}

	protected static void populatePropertyExpenseList(
			List<PropertyExpense> propertyExpenseList, 
			Collection<com.basis100.deal.entity.PropertyExpense> propertyExpenses, 
			ObjectFactory factory) {
		if (null == propertyExpenses || propertyExpenses.isEmpty()) return;

		for (com.basis100.deal.entity.PropertyExpense propExp: propertyExpenses) {
			PropertyExpense propExp_fcx = factory.createPropertyTypePropertyExpense();
			if(propExp.getPropertyExpenseTypeId() == 0) {
				propExp_fcx.setPropertyExpenseAmount(Convert.toBigDecimal(propExp.getPropertyExpenseAmount()));
				propExp_fcx.setPropertyExpensePeriodDd(Convert.toBigDecimal(propExp.getPropertyExpensePeriodId()));
				propExp_fcx.setPropertyExpenseTypeDd(Convert.toBigDecimal(0));
				propertyExpenseList.add(propExp_fcx);
			}

		}
	}

	protected static double calculateAnnualEscrowPaymentAmount(
			EscrowPayment ep, int paymentFrequencyId) {

		if (ep == null)
			throw new ExpressRuntimeException("EscrowPayment is null");

		switch (paymentFrequencyId) {
		case Mc.PAY_FREQ_MONTHLY:
			return ep.getEscrowPaymentAmount() * 12;
		case Mc.PAY_FREQ_SEMIMONTHLY:
			return ep.getEscrowPaymentAmount() * 24;
		case Mc.PAY_FREQ_BIWEEKLY:
		case Mc.PAY_FREQ_ACCELERATED_BIWEEKLY:
			return ep.getEscrowPaymentAmount() * 26;
		case Mc.PAY_FREQ_WEEKLY:
		case Mc.PAY_FREQ_ACCELERATED_WEEKLY:
			return ep.getEscrowPaymentAmount() * 52;
		default:
			throw new ExpressRuntimeException("deal.paymentFrequencyId:"
					+ paymentFrequencyId + " is invalid.");
		}
	}

	protected static int getDwellingStyleDd(int id) {
		switch(id) {
		case 0: return 0;
		case 1: return 1;
		case 2: return 2;
		case 3: return 4;
		case 4: return 5;
		case 5: return 7;
		case 6: return 8;
		case 7: return 10;
		case 8: return 11;
		case 9: return 13;
		default: return -1;
		}
	}

}
