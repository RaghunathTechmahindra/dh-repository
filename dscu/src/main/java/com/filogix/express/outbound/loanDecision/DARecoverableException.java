package com.filogix.express.outbound.loanDecision;

public class DARecoverableException extends Exception
{

	private static final long serialVersionUID = 1L;
	private int faultCode = 0;
    private String faultDescription = "";


	//constructors.....
	public DARecoverableException()
	{
		super();
	}

    /**
     * @param message
     */
	public DARecoverableException(String message)
	{
		super(message);
    }

	/**
	 * @param message
	 * @param tr
	 */
	public DARecoverableException(String message, Throwable tr)
	{
		super(message, tr);
    }

	/**
	 * @param tr
	 */
	public DARecoverableException(Throwable tr)
	{
		super(tr);
    }

	//get and set methods....
	public int getFaultCode()
	{
		return faultCode;
	}

	public void setFaultCode(int faultCode)
	{
		this.faultCode = faultCode;
	}

	public String getFaultDescription()
	{
		return faultDescription;
	}

	public void setFaultDescription(String faultDescription)
	{
		this.faultDescription = faultDescription;
    }




}
