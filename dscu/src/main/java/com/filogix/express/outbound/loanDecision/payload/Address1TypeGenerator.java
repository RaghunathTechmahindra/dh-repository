package com.filogix.express.outbound.loanDecision.payload;

import org.apache.commons.lang.StringUtils;

import MosSystem.Mc;

import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Property;
import com.filogix.schema.fcx._1.Address1Type;
import com.filogix.schema.fcx._1.ObjectFactory;

public class Address1TypeGenerator {

	private static final int MAX_LENGTH_STREETNAME = 30;
	
	public static Address1Type generateAddress1Type(Addr addr, ObjectFactory factory) {

		Address1Type address1Type = factory.createAddress1Type();
		address1Type.setUnitNumber(addr.getUnitNumber());
		address1Type.setStreetNumber(addr.getStreetNumber());
		address1Type.setStreetName(StringUtils.substring(addr.getStreetName(), 0, MAX_LENGTH_STREETNAME));
		populateStreetTypeDd(address1Type, addr.getStreetTypeId());
		address1Type.setStreetDirectionDd(Convert.toBigDecimal(addr.getStreetDirectionId()));
		address1Type.setCity(addr.getCity());
		if(addr.getProvinceId() != Mc.PROVINCE_OTHER)
			address1Type.setProvinceDd(Convert.toBigDecimal(addr.getProvinceId()));
		populatePostalCode(addr.getPostalFSA(), addr.getPostalLDU(), address1Type);

		return address1Type;
	}

	public static Address1Type generateAddress1Type(Property property, ObjectFactory factory) {

		Address1Type address1Type = factory.createAddress1Type();
		address1Type.setUnitNumber(property.getUnitNumber());
		address1Type.setStreetNumber(property.getPropertyStreetNumber());
		address1Type.setStreetName(StringUtils.substring(property.getPropertyStreetName(), 0, MAX_LENGTH_STREETNAME));
		populateStreetTypeDd(address1Type, property.getStreetTypeId());
		address1Type.setStreetDirectionDd(Convert.toBigDecimal(property.getStreetDirectionId()));
		address1Type.setCity(property.getPropertyCity());
		if(property.getProvinceId() != Mc.PROVINCE_OTHER)
			address1Type.setProvinceDd(Convert.toBigDecimal(property.getProvinceId()));
		
		populatePostalCode(property.getPropertyPostalFSA(), property.getPropertyPostalLDU(), address1Type);

		return address1Type;
	}
	
	
	protected static void populatePostalCode(String fsa, String ldu, 
			Address1Type address1Type) {
		if(null != fsa && fsa.trim().length() > 0
				&& null != ldu && ldu.trim().length() > 0) {
			address1Type.setPostalFsa(fsa);
			address1Type.setPostalLdu(ldu);
		}
	}

	protected static void populateStreetTypeDd(Address1Type address1Type, int streetTypeId) {
		if(streetTypeId > 0) {
			address1Type.setStreetTypeDd(Convert.toBigDecimal(streetTypeId));
		}
	}


}
