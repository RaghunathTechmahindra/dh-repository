package com.filogix.express.outbound.dscu;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.basis100.deal.entity.BranchProfile;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.BranchProfilePK;
import com.basis100.log.SysLogger;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.outbound.dscu.DESUpdatePayloadGenerator.DEStatus;

public abstract class ServicePayloadGenerator implements IPayloadGenerator {
	
    protected static SysLogger   _logger;

    protected SessionResourceKit _srk;
    protected String             _payloadXml;
    protected Deal				 _deal;
    protected UserProfile        _userprofile;
    
    public ServicePayloadGenerator() {
    }
    
    public void init(Deal deal){
    	this._deal = deal;
    	this._srk = deal.getSessionResourceKit();
    	this._logger = ResourceManager.getSysLogger("PayldGnBse");
    	_logger.debug("Initialized");
    }

    public Deal getDeal() {
        return _deal;
    }
    public UserProfile getUserProfile() {
        return _userprofile;
    }
        
    public void setConditions(Collection<DocumentTracking> conditions){
        
        // do nothing as default
    }

    public void setDEStatuses(Collection<DEStatus> deStatus) {
        
        // do nothing as default        
    }
    
    public abstract String getXmlPayload() throws Exception;

    public XMLGregorianCalendar getUserTimeStamp(UserProfile userProfile, GregorianCalendar date) {
        
        BranchProfile bp = null;

        int timeZoneId = 3; //EST
        try {
            int bId = userProfile.getBranchId();
            bp = new BranchProfile(_srk);
            bp = bp.findByPrimaryKey(new BranchProfilePK(bId));
            timeZoneId = bp.getTimeZoneEntryId();
        } catch (Exception e) {
            if (userProfile.getUserProfileId() == 0) 
                _logger.warning("getXmlPayload couldn't found Userprfofile for deal id = " + _deal.getDealId());
            else if ( bp.getBranchProfileId() == 0) 
                _logger.warning("getXmlPayload couldn't found BranchProfile for user id = " + _userprofile.getUserProfileId());
        }
        
        try {
            XMLGregorianCalendar timeStamp = DatatypeFactory.newInstance().newXMLGregorianCalendar(date);
            // to eliminate milisecond, rset time
            timeStamp.setTime(date.get(Calendar.HOUR_OF_DAY), date.get(Calendar.MINUTE), date.get(Calendar.SECOND));
            return timeStamp;
        } catch (Exception e) {
            _logger.warning("getXmlPayload couldn't create = " + _deal.getDealId());    
            return null;
        }
    }

    public XMLGregorianCalendar getUserTimeStamp(UserProfile userProfile) {
        
        GregorianCalendar now = new GregorianCalendar();
        return getUserTimeStamp(userProfile, now);
    }
}
