package com.filogix.express.outbound.util;

/**
 * ESBStatusConst
 *
 * @version 1.0 Jun 9, 2009
 */
public interface OutboundConst {

    //ServiceTypePrefix
    public static final String SERVICEPROCESSOR_PREFIX = "ESBOUTBOUND_SERVICE_";

    //max attempt store in sys property
    public static final String MAX_ATTEMP = "dscuDaemon.maxAttempCounts";

    //retry time system property
    public static final String RETRY_TIME = "dscuDaemon.retry.time";

    //sleep time system property
    public static final String SLEEP_TIME = "dscuDaemon.sleep.time";

    //condition support system property
    public static final String CONDITION_SUPPORTED = "statusUpdate.conditions.supported";

    //deal status support system property
    public static final String DEALEVENT_SUPPORTED = "statusUpdate.dealAndEvent.supported";

    //userprofile id for dscu daemon
    public static final int ESBOUTBOUND_USER_ID = 96;

    public static final int DEALSTATUS_CHANNEL_ID = 18;

    public static final int CM_CHANNEL_ID = 17;

    //New for Data Alignment
	public static final int SERVICETYPE_LOANDECISION = 11;

	public static final int ESBQUEUESTATUSCATEGORY_RECEIVED = 0;

	public static final int ESBQUEUESTATUSCATEGORY_PROCESSING = 1;

	public static final int ESBQUEUESTATUSCATEGORY_SUCCESS = 2;

	public static final int ESBQUEUESTATUSCATEGORY_JAXBERROR= 3;

	public static final int ESBQUEUESTATUSCATEGORY_OTHERERROR = 4;

    public static final String SERVICESUBTYPE_PREFIX = "ESBOUTBOUND_SERVICESUBTYPE_";
    
    public static final String LOANDECISION_TRANSFORMER_KEY = "LOANDECISION_TRANSFORMER_CHANNEL";
    
    public static final String LOANDECISION_OUTBOUND_KEY = "LOANDECISION_OUTBOUND_CHANNEL";
    
    public static final int ESBOUTBOUNDQUEUE_TRANSACTIONSTATUSDESC_MAX = 200;
    
    //userProfileid for deallock
    public static final String SYSPROP_SYSTEM_USERPROFILEID = "dscu.userprofileid";
}
