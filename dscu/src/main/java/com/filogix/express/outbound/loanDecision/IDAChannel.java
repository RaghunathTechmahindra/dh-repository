package com.filogix.express.outbound.loanDecision;

import com.basis100.deal.entity.Deal;
import com.basis100.resources.SessionResourceKit;
//do I need another exception class?

/**
 * <p>IDAChannel Interface</p>
 * <p>Spring proxy for Data Alignment Channel classes</p>
 * @author DBellinger
 * @version 1.0 - 20 July, 2009
 */
public interface IDAChannel {

    public void init(SessionResourceKit srk, Deal deal, int outboundQueueId)
        throws Exception;

    public String send(String payload) throws DARecoverableException, DAUnrecoverableException;

    public void setChannelId(int id);

}
