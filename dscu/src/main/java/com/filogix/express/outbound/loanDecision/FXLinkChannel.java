package com.filogix.express.outbound.loanDecision;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.namespace.QName;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.Channel;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.services.util.ChannelUtils;


public class FXLinkChannel implements IDAChannel
{
	private final static Logger logger = LoggerFactory.getLogger(FXLinkChannel.class);

	//spring framework sets values --- start
	private int channelId;
	private int timeout;
	private String versionedMessage_format;
	private String versionedMessage_transaction;
	private String versionedMessage_version;
	private String versionedDocument_docname;
	private String versionedDocument_format;
	private String versionedDocument_mimetype; 
	private String versionedDocument_version;
	//spring framework sets values --- end


	private Deal deal;
	private MasterDeal masterDeal;
	private Channel channel;
	private String endPoint;
	private int messageId; //what am I using this for again?
	private int queueId; //mostly for logging and tracking purposes
	private ServiceClient sender;

	public static final String RESPONSE_OK = "Ok";

	public void init(SessionResourceKit srk, Deal deal, int id)	throws Exception
	{
		this.deal = deal;
		this.queueId = id;

		logger.info("Initialization of FXLinkChannel, ESBOutboundQueueId: " + queueId 
				+ ", DealId: " + deal.getDealId() 
				+ ", InsitutionProfileId: " + deal.getInstitutionProfileId());

		//try/catch this part
		masterDeal = new MasterDeal(srk, null);
		masterDeal = masterDeal.findByPrimaryKey(new MasterDealPK(deal.getDealId()));

		channel = new Channel(srk, channelId);
		endPoint = ChannelUtils.concatEndpoint(channel);

		logger.debug("channelId =" + channelId);
		logger.debug("endPoint =" + endPoint);
		logger.debug("receiver =" + masterDeal.getFXLinkPOSChannel());
		logger.debug("sender =" + masterDeal.getFXLinkUWChannel());
		logger.debug("transaction =" + versionedMessage_transaction);
		logger.debug("format =" + versionedMessage_format);
		logger.debug("version =" + versionedMessage_version);
		
		prepareService();

	}

	protected void prepareService() throws DARecoverableException
	{
		logger.info("Preparing service, prior to sending to FXLinkChannel, ESBOutboundQueueId: " + queueId);  //temporary tracing comments

		EndpointReference targetEPR = new EndpointReference(endPoint);

		Options options = new Options();
		options.setTo(targetEPR);
		options.setTransportInProtocol(Constants.TRANSPORT_HTTP);

		options.setTimeOutInMilliSeconds(this.timeout);
		options.setProperty(HTTPConstants.CHUNKED, Boolean.FALSE);

		ArrayList<String> authSchemes = new ArrayList<String>();
		authSchemes.add(HttpTransportProperties.Authenticator.BASIC);

		HttpTransportProperties.Authenticator
		auth = new HttpTransportProperties.Authenticator();

		auth.setUsername(channel.getUserId());
		auth.setPassword(channel.getPassword());

		auth.setAuthSchemes(authSchemes);
		auth.setPreemptiveAuthentication(true);
		options.setProperty(HTTPConstants.AUTHENTICATE, auth);

		try
		{
			sender = new ServiceClient();
			sender.setOptions(options);
		}
		catch (AxisFault e)
		{
			String msg = new String("Failed to prepare Fxlink webservice.");
			DARecoverableException re = new DARecoverableException(msg, e);
			throw re;
		}
	}

//	---------------------------------------------------
	public String send(String payloadXml) throws DARecoverableException, DAUnrecoverableException
	{
		logger.info("Sending to FXLinkChannel, ESBOutboundQueueId: " + queueId);  //temporary tracing comments

		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace omNs = fac.createOMNamespace(
				"http://services.axis.fxportal.hermes.filogix.com", "");

		OMElement methodSetMessage = fac.createOMElement("setMessage", omNs);

		OMElement versionedMessage = fac.createOMElement("message", omNs);
		methodSetMessage.addChild(versionedMessage);


		//-------------------------------------------
		// Level1: VersionedMessage
		//-------------------------------------------
		OMElement elmDealId = fac.createOMElement("dealId", omNs);
		elmDealId.addChild(fac.createOMText(elmDealId, "" + deal.getSourceApplicationId()));
		versionedMessage.addChild(elmDealId);

		OMElement elmFormat = fac.createOMElement("format", omNs);
		elmFormat.addChild(fac.createOMText(elmFormat, versionedMessage_format));
		versionedMessage.addChild(elmFormat);

		//sending ESBOutboundQueueId as "messageId" to easier production support tracing
		OMElement elmMessageId = fac.createOMElement("messageId", omNs);
		elmMessageId.addChild(fac.createOMText(elmMessageId, "" + deal.getDealId()));
		versionedMessage.addChild(elmMessageId);

		OMElement elmReceiver = fac.createOMElement("receiver", omNs);
		elmReceiver.addChild(fac.createOMText(elmReceiver, masterDeal.getFXLinkPOSChannel()));
		versionedMessage.addChild(elmReceiver);

		OMElement elmSender = fac.createOMElement("sender", omNs);
		elmSender.addChild(fac.createOMText(elmSender, masterDeal.getFXLinkUWChannel()));
		versionedMessage.addChild(elmSender);

		OMElement elmTimestamp = fac.createOMElement("timestamp", omNs);
		SimpleDateFormat adf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		elmTimestamp.addChild(fac.createOMText(elmTimestamp, adf.format(new Date())));
		versionedMessage.addChild(elmTimestamp);

		OMElement elmTransaction = fac.createOMElement("transaction", omNs);
		elmTransaction.addChild(fac.createOMText(elmTransaction, versionedMessage_transaction));
		versionedMessage.addChild(elmTransaction);

		OMElement elmVersion = fac.createOMElement("version", omNs);
		elmVersion.addChild(fac.createOMText(elmVersion, versionedMessage_version));
		versionedMessage.addChild(elmVersion);

		//-------------------------------------------
		// Level2: VersionedDocument
		//-------------------------------------------
		OMElement versionedDocument = fac.createOMElement("documents", omNs);
		versionedMessage.addChild(versionedDocument);

		OMElement elmItem = fac.createOMElement("item", omNs);
		versionedDocument.addChild(elmItem);

		OMElement elmDocDocname = fac.createOMElement("docname", omNs);
		elmDocDocname.addChild(fac.createOMText(elmDocDocname, versionedDocument_docname));
		elmItem.addChild(elmDocDocname);

		OMElement elmDocformat = fac.createOMElement("format", omNs);
		elmDocformat.addChild(fac.createOMText(elmDocformat, versionedDocument_format));
		elmItem.addChild(elmDocformat);

		OMElement elmDocMimetype = fac.createOMElement("mimetype", omNs);
		elmDocMimetype.addChild(fac.createOMText(elmDocMimetype, versionedDocument_mimetype));
		elmItem.addChild(elmDocMimetype);

		OMElement elmDocVersion = fac.createOMElement("version", omNs);
		elmDocVersion.addChild(fac.createOMText(elmDocVersion, versionedDocument_version));
		elmItem.addChild(elmDocVersion);

		OMElement elmDocContent = fac.createOMElement("content", omNs);
		
		
		byte[] contextArray = null;
		try {
			//EXP26788
			//since DataAlignmentPayloadGenerator uses UTF-8, 
			//UTF-8 has to be specified here too.
			contextArray = Base64.encodeBase64(payloadXml.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			logger.error("faild to execute getBytes method", e);
		}
		
		elmDocContent.addChild(fac.createOMText(elmDocContent, new String(contextArray)));
		elmItem.addChild(elmDocContent);


		OMElement setMessageResponse = null;
		try 
		{
			logger.debug("Sending (ESBOutboundQueueId: " + queueId + ")... " 
					+ StringUtils.substring(methodSetMessage.toString(), 0, 500));
			setMessageResponse = sender.sendReceive(methodSetMessage);
			logger.debug("received (ESBOutboundQueueId: " + queueId + ")... " 
					+ StringUtils.substring(setMessageResponse.toString(), 0, 500));
			logger.info("Successfully completed sending to FXLinkChannel, ESBOutboundQueueId: " + queueId);  //temporary tracing comments
		} 
		catch (AxisFault af) 
		{	
			//As FxLink doesn't response error code, this class handle all errors as recoverable.
			String errMsg = "error happened during ws call to FxLink.";
			logger.error(errMsg,af);
			logger.error("=== rejected request message ===");
			logger.error(StringUtils.substring(methodSetMessage.toString(), 0, 2000));
			logger.error("=== rejected payload xml ===");
			//DA-DG-UC-4, Exception 2
			logger.error(payloadXml);

			throw new DARecoverableException(errMsg, af);
		}

		if(setMessageResponse == null) 
			throw new DARecoverableException("FxLink Server didn't response valid message");

		OMElement response = setMessageResponse.getFirstElement();
		if(response == null || response.getText() == null) 
			throw new DARecoverableException("response is not valid");

		String messageId = getTextFromElement(response, "messageId");
		String status = getTextFromElement(response, "status");
		
		logger.debug("response.messageId = " + messageId);
		logger.debug("response.status = " + status);
		
		if(RESPONSE_OK.equalsIgnoreCase(status) == false){
			//other than OK, all other status is un-recoverble 
			throw new DAUnrecoverableException("FxLink Server returned error status: ["
					+ status + "], messageId: [" + messageId + "]");
		}
		

		return messageId;

	}
//	--------------------------------------------------

	String getTextFromElement(OMElement element, String localName)
	{
		OMElement child = getFirstChildElement(element, localName);
		return (child != null) ? child.getText() : null; 
	}

	OMElement getFirstChildElement(OMElement element, String localName)
	{
		QName qn = new QName(element.getDefaultNamespace().getNamespaceURI(), localName);
		return element.getFirstChildWithName(qn);
	}

	public void setChannelId(int channelId)
	{
		this.channelId = channelId;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public void setMessageId(int id)
	{
		this.messageId = id;
	}

	public int getMessageId()
	{
		return messageId;
	}

	public void setVersionedMessage_format(String versionedMessage_format) {
		this.versionedMessage_format = versionedMessage_format;
	}

	public void setVersionedMessage_transaction(String versionedMessage_transaction) {
		this.versionedMessage_transaction = versionedMessage_transaction;
	}

	public void setVersionedMessage_version(String versionedMessage_version) {
		this.versionedMessage_version = versionedMessage_version;
	}

	public void setVersionedDocument_docname(String versionedDocument_docname) {
		this.versionedDocument_docname = versionedDocument_docname;
	}

	public void setVersionedDocument_format(String versionedDocument_format) {
		this.versionedDocument_format = versionedDocument_format;
	}

	public void setVersionedDocument_mimetype(String versionedDocument_mimetype) {
		this.versionedDocument_mimetype = versionedDocument_mimetype;
	}

	public void setVersionedDocument_version(String versionedDocument_version) {
		this.versionedDocument_version = versionedDocument_version;
	}

}
