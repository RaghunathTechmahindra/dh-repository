package com.filogix.express.outbound;

import com.basis100.resources.SessionResourceKit;

/**
 * IStatusProcessor 
 * 
 * @version 1.0 Jun 9, 2009
 */
public interface IOutboundProcessor {

    /**
     * method handle processing send payload to ESB, handle result form ESB 
     * 
     * @param obj
     * @return
     * @throws Exception
     */
    public void handleProcess(Object obj) throws Exception;

    /**
     * set SessionResourceKit
     * @param srk
     */
    public void setSessionResourceKit(SessionResourceKit srk);

    /**
     * get SessionResourceKit
     * @return
     */
    public SessionResourceKit getSrk();
    
    public String getSupport();
        
    public void setSupport(String support);
    
    public boolean isDealLockRequired();

    public void setDealLockRequired(boolean dealLockRequired);

}
