package com.filogix.express.outbound.loanDecision.payload;

import com.basis100.deal.entity.Deal;
import com.filogix.express.outbound.loanDecision.DAUnrecoverableException;

public interface IDAPayloadGenerator
{
    public String getXmlPayload(Deal deal) throws DAUnrecoverableException;
    
}
