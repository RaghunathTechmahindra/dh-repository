package com.filogix.express.outbound.loanDecision.payload;

import java.util.List;

import com.basis100.deal.entity.Deal;
import com.filogix.schema.fcx._1.DealNotesType;
import com.filogix.schema.fcx._1.ObjectFactory;
import com.filogix.schema.fcx._1.ResponseType;
import com.filogix.schema.fcx._1.ResponseType.Pending;

public class HoldPayloadGenerator extends DataAlignmentPayloadGenerator //implements IDAPayloadGenerator
{
	/**
	 * 
	 * @see com.filogix.express.outbound.loanDecision.payload.DataAlignmentPayloadGenerator#createResponseType(com.filogix.schema.fcx._1.LoanDecisionType,
	 *      com.basis100.deal.entity.Deal,
	 *      com.filogix.schema.fcx._1.ObjectFactory)
	 */
	@Override
	protected void populateResponseTypeSpecific(ResponseType responseType,
			Deal deal, ObjectFactory factory) {

		Pending pending = factory.createResponseTypePending();
		responseType.setPending(pending);

		List<DealNotesType> listOfDealNotesType = pending.getMessage();
		DealNotesGenerator.populateDealNotesListWithFirstQualifiedNotes(listOfDealNotesType, deal, factory);
	}


}
