package com.filogix.express.outbound.dscu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;

import com.basis100.deal.docprep.DocPrepException;
import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.ConditionStatusUpdateAssoc;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealHistory;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.pk.ConditionPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.outbound.IOutboundProcessor;
import com.filogix.express.outbound.util.OutboundConst;
import com.filogix.express.outbound.util.OutboundUtil;
import com.filogix.externallinks.framework.ServiceConst;

/**
 * ConditionStatusProcessor 
 * 
 * @version 1.0 Jun 9, 2009
 */
public class ConditionStatusProcessor extends ESBStatusProcessor {

    private final static Log _log = LogFactory
            .getLog(ConditionStatusProcessor.class);
    
    private IPayloadGenerator payloadGenerator;
    private IChannel channel;
    private ESBOutboundQueue statusEvent;
    // this string is from spring application context.
    // retriable error code, comma delimiter
    // set only once
    private static String retryErrorCodes = null;
    // set only once
    // create set for etry error codes set
    private static Set<String> retryErrorCodeList = null;

    public ConditionStatusProcessor() {
        super();
    }

    /*
     * handle process statuseventqueue
     * 
     */
    public void handleProcess(Object obj) throws Exception  {
        
        channel = new ConditionUpdateChannel();
        channel.setChannelId(OutboundConst.CM_CHANNEL_ID);
        payloadGenerator = new ConditionUpdatePayloadGenerator();
        statusEvent = (ESBOutboundQueue) obj;
        _log.info("dscu processing queueid: " + statusEvent.getESBOutboundQueueId());

        SessionResourceKit srk = this.getSrk();
        DealPK dpk = new DealPK(statusEvent.getDealId(), -1);
        Deal deal = new Deal(srk, null);
        deal = deal.findByRecommendedScenario(dpk, true);

        Collection<DocumentTracking> conditions = findMatchConditons(deal);
        int attemptCounter = OutboundUtil.getMaxAttemptValue(statusEvent);
        
        if( conditions.size() > 0) {
            // generate payload
            payloadGenerator.init(deal);
            payloadGenerator.setUserProfileId(statusEvent.getUserProfileId());
            payloadGenerator.setConditions(conditions);
            String payload = payloadGenerator.getXmlPayload();
            
            _log.info("condition processor generate payload: " + payload);
            OutboundUtil.setRetryTimeStamp(statusEvent, srk);
            // get Channel 
    		channel.init(srk, deal);
            try {
                channel.send(payload);
                handleSuccessResult(deal, statusEvent);
                
            } catch (PostOfficeServiceException pe){
                int faultCode = pe.getFaultCode();
                String faultDescription = "" + faultCode + ": " + pe.getFaultDescription();
                switch (faultCode) {
                case ServiceConst.ESBOUT_TRANSACTIONSTATUS_TIMEOUT:
                    OutboundUtil.incrementCounter(statusEvent, 1, srk,
                            attemptCounter);
                    sendEmailNotification(deal, statusEvent, faultCode);
                    break;
                default:
                    // this is special case due to DataPower failer issue
                    // must retry 
                    if (retryErrorCodeList.contains(""+faultCode)) {
                        OutboundUtil.incrementCounter(statusEvent, 1, srk,
                                attemptCounter);
                        sendEmailNotification(deal, statusEvent, faultCode);
                    } else {
                        handleFailureMessage(faultCode, faultDescription, deal, statusEvent);
                    }
                    break;
                }
            }
        } else {
            _log.info("condition processor no condition update for this queue");
            OutboundUtil.changeProcessStatus(statusEvent,
            		ServiceConst.ESBOUT_TRANSACTIONSTATUS_SYSTEMERROR, "UPDATE IS NOT NESESSARY",
            		deal.getSessionResourceKit());
        }
    }

    /**
     * add record in dealhistory for success
     * 
     * @param deal
     * @param msg
     * @throws RemoteException
     */
    public void addDealHisotry(Deal deal, String msg) throws Exception {

        SessionResourceKit srk = deal.getSessionResourceKit();
        DealHistory dh = new DealHistory(srk);
        _log.debug("dscu processor add dealhistroy record");
        dh = dh.create(deal.getDealId(), deal.getUnderwriterUserId(),
                Mc.DEAL_TX_TYPE_EVENT, deal.getStatusId(), msg, new Date());
    }

    /**
     * find condition should be include in payload for this deal
     * 
     * @param deal
     * @return
     * @throws Exception
     * 
     * @version 1.0 - original
     * @version 1.1 (April 15, 2009) - fixed logic, fixed to Collection<DocumentTracking>
     * @throws Exception 
     */
    public Collection<DocumentTracking> findMatchConditons(Deal deal) throws Exception  {

        // this will be return collection for this method
        Collection<DocumentTracking> conditionWillBeIn = new ArrayList<DocumentTracking>();

        // get DocumentTracking-condition from Deal
        DealPK dpk = new DealPK(deal.getDealId(), deal.getCopyId());
        DocumentTracking document = new DocumentTracking(srk);
        Collection<DocumentTracking> documentFromDeal = document.findForConditionUpdate(dpk);
        Iterator<DocumentTracking> documentIter = documentFromDeal.iterator();

        // find conditionID that should be in payload from system property
        String propertyConfig = PropertiesCache.getInstance().getProperty(
                deal.getInstitutionProfileId(),
                "statusUpdate.conditions.include.condition.types", "");
        if (propertyConfig == null) propertyConfig = "";
        String[] condFromProp = propertyConfig.split(",| ");
        List condtionIdsToBeIn = null;
        if (condFromProp!=null && condFromProp.length>0 ){
            condtionIdsToBeIn = Arrays.asList(condFromProp);
        }

        // find statusId that should be in payload from ConditionStatusUpdateAssoc by systemTypeId
        // this collection i.e. return value from finddocumentStatusIdBySystemType never get null;
        ConditionStatusUpdateAssoc conditionAssoc = new ConditionStatusUpdateAssoc(srk);
        Collection<Integer> docStatusToBeIn = conditionAssoc
                .finddocumentStatusIdBySystemType(deal.getSystemTypeId());
        
        //TODO: need to be ordered by ,,,
        while (documentIter.hasNext()) {
            DocumentTracking docTrack = (DocumentTracking) documentIter.next();
            int conditionId = docTrack.getConditionId();
            Condition condition = new Condition(srk);
            ConditionPK conditionPk = new ConditionPK(conditionId);
            condition.findByPrimaryKey(conditionPk);
            int conditionTypeId = condition.getConditionTypeId();
            int docStatus = docTrack.getDocumentStatusId();
            if (condtionIdsToBeIn.contains(conditionTypeId + "") && docStatusToBeIn.contains(docStatus)) {
                conditionWillBeIn.add(docTrack);
            }
        }
        return conditionWillBeIn;
    }

    /**
     * @param result
     * @param deal
     * @throws RemoteException
     * @throws DocPrepException
     */
    private void handleSuccessResult(Deal deal, ESBOutboundQueue queue)
            throws Exception {
        _log.info("transaction successful from post office");
        String histroyMessage = BXResources.getSysMsg("CONDITION_DEALHISTORY",
                Mc.LANGUAGE_PREFERENCE_ENGLISH);
        addDealHisotry(deal, histroyMessage);
        OutboundUtil.changeProcessStatus(statusEvent,
        		ServiceConst.ESBOUT_TRANSACTIONSTATUS_SUCCESS, "SUCCEED", srk);
        if(!debugmode) {
        	OutboundUtil.removeCurrentQueue(srk, queue);
        }
    }



	/**
	 * @param channel the channel to set
	 */
	public void setChannel(IChannel channel) {
		this.channel = channel;
	}

	/**
	 * @param payloadGenerator the payloadGenerator to set
	 */
	public void setPayloadGenerator(IPayloadGenerator payloadGenerator) {
		this.payloadGenerator = payloadGenerator;
	}
	
    public Set<String> getRetryErrorCodeList(){
        return retryErrorCodeList;
    }
    
    public void setRetryErrorCodeList(Set<String> errorList){
        retryErrorCodeList = errorList;
    }
    
    public void setRetryErrorCodes(String errorCodes) {
        if (retryErrorCodes==null) {
            retryErrorCodes = errorCodes;
            super.setRetryErrorCodes(errorCodes);
        }
    }
}
