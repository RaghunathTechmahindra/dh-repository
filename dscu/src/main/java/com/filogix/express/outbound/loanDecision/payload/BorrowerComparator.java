package com.filogix.express.outbound.loanDecision.payload;

import java.util.Comparator;
import com.basis100.deal.entity.Borrower;

public class BorrowerComparator implements Comparator<Borrower> {
	
	/**
	 * Sorting order:
	 * 1. By Borrower type
	 * 2. By Primary flag
	 * 3. By Martial Status
	 * 4. By Borrower Id 
	 */
	public int compare(Borrower a, Borrower b) {
		if(a.getBorrowerTypeId() != b.getBorrowerTypeId()){
			return a.getBorrowerTypeId() - b.getBorrowerTypeId();
		} else if ("Y".equalsIgnoreCase(a.getPrimaryBorrowerFlag())) {
			return -1;
		} else if ("Y".equalsIgnoreCase(b.getPrimaryBorrowerFlag())) {
			return 1;
		} else if (a.getMaritalStatusId() != b.getMaritalStatusId()) {
			return a.getMaritalStatusId() - b.getMaritalStatusId();
		} else if(a.getBorrowerId() != b.getBorrowerId()) {
			return a.getBorrowerId() - b.getBorrowerId(); 
		}
		return 0;
	}

}
