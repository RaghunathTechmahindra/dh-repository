/*
 * @(#)AMETestCase.java     Feb 14, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.ame;

public interface AMETestCase {
    public int setupProcessSpecificData() throws Exception;

    public void testProcess();

    public boolean verifyProcessOutput() throws Exception;
}
