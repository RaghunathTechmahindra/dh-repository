/*
 * @(#)TestAME.java     Feb 14, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.ame;

import java.util.ArrayList;
import java.util.Iterator;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import MosSystem.Sc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.security.DLM;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.entity.AssignedTask;
import com.basis100.workflow.entity.WorkflowTaskProcessAssoc;
import com.basis100.workflow.pk.AssignedTaskBeanPK;
import com.basis100.workflow.process.WFProcessBase;

public class TestAME extends TestCase {
    private static final PropertiesCache propertiesCache = PropertiesCache
            .getInstance();
    private String strUserId;
    private String strPw;
    int intRetryInternal = 0;
    int intRetryExternal = 1;
    int intExpireRetry = 2;
    int intTaskComplete = 3;
    int intError = -1;
    int userInstitutionId;
    SessionResourceKit srk;
    int intUserProfileId;
    CalcMonitor calMonitor;
    
    private AMESignInService signInService;

    public TestAME() {
        super();
    }

    public TestAME(String name) {
        super(name);
    }

    protected void tearDown() {
        srk.freeResources();
    }

    protected void setUp() throws Exception {
        srk = new SessionResourceKit("TestAME");
        signInService = (AMESignInService) ExpressAMEContext.getService("SignInService");
        UserProfile userProfile = new UserProfile(srk);
        
        strUserId = propertiesCache.getInstanceProperty(
                AMEConstants.PROPERTY_USER_ID, "");
        strPw = propertiesCache.getInstanceProperty(
                AMEConstants.PROPERTY_PASSWORD, "");
        
        userProfile = userProfile.findByLoginId(strUserId);
        intUserProfileId = userProfile.getUserProfileId();
        userInstitutionId = userProfile.getInstitutionId();

        ResourceManager.init();

        PicklistData.init();
        DLM.init(srk, false);
        calMonitor = CalcMonitor.getMonitor(srk);
    }

    private Deal getDeal(int i_intDealId) throws Exception {
        CalcMonitor calc = CalcMonitor.getMonitor(srk);
        Deal aDeal = new Deal(srk, calc);
        DealPK dealPk = new DealPK(i_intDealId, -1);
        try {
            aDeal.findByRecommendedScenario(dealPk, true);
        } catch (Exception ex) {
            srk
                    .getSysLogger()
                    .warning(
                            AMEConstants.ERROR_MESSAGE_AMEWORKER_NO_RECOMMENDED_COPY_FOUND_DEALID
                                    + i_intDealId
                                    + AMEConstants.ERROR_MESSAGE_AMEWORKER_FALLBACK_TO_USE_GOLD_COPYID);
            aDeal = aDeal.findByGoldCopy(i_intDealId);
        }
        return aDeal;
    }

    public static Test suite() {
        // Run all test cases.
        // TestSuite suite = new TestSuite(TestAME.class);
        // Run selected test cases.
        TestSuite suite = new TestSuite();
        suite.addTest(new TestAME("testALogin"));
//        suite.addTest(new TestAME("testCActiveTask"));
//        suite.addTest(new TestAME("testAMEManager"));
        return suite;
    }

    public void testALogin() {
        try {            
            UserProfile userProfile = signInService.login(strUserId, strPw);
            assertTrue(userProfile != null);
            assertEquals(userProfile.getPasswordFailedAttempts(),
                        Sc.PASSWORD_FAILED_ATTEMPTS_TRUE);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            fail();
        }
    }

    public void testB1ResetOutStandingTasks() {
        try {
            int assignedTasksWorkQueueId = 122;
            int userProfileId;
            AssignedTask assignedTask = new AssignedTask(srk);
            AssignedTaskBeanPK assignedTaskPk = new AssignedTaskBeanPK(
                    assignedTasksWorkQueueId);
            assignedTask = assignedTask.findByPrimaryKey(assignedTaskPk);
            userProfileId = assignedTask.getUserProfileId();
            srk.beginTransaction();
            assignedTask.setVisibleFlag(-2);
            // FOREIGN KEY VIOLATION;
            assignedTask.ejbStore();
            srk.commitTransaction();
            assignedTask.resetOutstandingTasks(userProfileId);
            assignedTask = new AssignedTask(srk);
            assignedTaskPk = new AssignedTaskBeanPK(assignedTasksWorkQueueId);
            assignedTask = assignedTask.findByPrimaryKey(assignedTaskPk);
            assertTrue(assignedTask.getVisibleFlag() == 1);
        } catch (Exception ex) {
            ex.printStackTrace();
            fail();
        }
    }

    public void testB2ResetOutStandingTasks() {
        try {
            int assignedTasksWorkQueueId;
            AssignedTask assignedTask = new AMEManager().getActiveTask();
            srk.beginTransaction();
            assignedTask.setVisibleFlag(-2);
            assignedTasksWorkQueueId = assignedTask
                    .getAssignedTasksWorkQueueId();
            assignedTask.ejbStore();
            srk.commitTransaction();
            assignedTask.resetOutstandingTasks(intUserProfileId);
            AssignedTaskBeanPK assignedTaskPk = new AssignedTaskBeanPK(
                    assignedTasksWorkQueueId);
            assignedTask = assignedTask.findByPrimaryKey(assignedTaskPk);
            assertTrue(assignedTask.getVisibleFlag() == 1);
        } catch (Exception ex) {
            ex.printStackTrace();
            fail();
        }
    }

    public void testCActiveTask() {
        try {
            AssignedTask aTask = new AMEManager().getActiveTask();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            fail();
        }
    }

    public void testD1ProcessAssoco() {
        try {
            int assignedTasksWorkQueueId = 122;
            int userProfileId;
            int retryCounter = 2;
            AssignedTask assignedTask = new AssignedTask(srk);
            AssignedTaskBeanPK assignedTaskPk = new AssignedTaskBeanPK(
                    assignedTasksWorkQueueId);
            assignedTask = assignedTask.findByPrimaryKey(assignedTaskPk);
            userProfileId = assignedTask.getUserProfileId();
            srk.beginTransaction();
            assignedTask.setTaskStatusId(1);
            assignedTask.setVisibleFlag(1);
            assignedTask.ejbStore();
            srk.commitTransaction();
            assignedTask = assignedTask.findNextAutoTask(userProfileId,
                    retryCounter);
            WorkflowTaskProcessAssoc wftProcessAssoc = new WorkflowTaskProcessAssoc(
                    srk);
            int intTaskId = assignedTask.getTaskId();
            ArrayList alWftProcessAssoc = (ArrayList) wftProcessAssoc
                    .findByTaskId(intTaskId);
            assertTrue(alWftProcessAssoc != null
                    && alWftProcessAssoc.size() > 0);
        } catch (Exception ex) {
            ex.printStackTrace();
            fail();
        }
    }

    public void testD2ProcessAssoco() {
        try {
            int retryCounter = 2;
            int assignedTasksWorkQueueId;
            AssignedTask assignedTask = new AMEManager().getActiveTask();
            srk.beginTransaction();
            assignedTask.setTaskStatusId(1);
            assignedTask.setVisibleFlag(1);
            assignedTask.ejbStore();
            srk.commitTransaction();
            assignedTask = assignedTask.findNextAutoTask(intUserProfileId,
                    retryCounter);
            WorkflowTaskProcessAssoc wftProcessAssoc = new WorkflowTaskProcessAssoc(
                    srk);
            int intTaskId = assignedTask.getTaskId();
            ArrayList alWftProcessAssoc = (ArrayList) wftProcessAssoc
                    .findByTaskId(intTaskId);
            assertTrue(alWftProcessAssoc != null
                    && alWftProcessAssoc.size() > 0);
        } catch (Exception ex) {
            ex.printStackTrace();
            fail();
        }
    }

    public void testExecuteProcess() {
        try {
            int intResult = 0;
            AssignedTask aTask = new AssignedTask(srk);
            aTask = aTask.findNextAutoTask(5372, 10);
            WorkflowTaskProcessAssoc wftProcessAssoc = new WorkflowTaskProcessAssoc(
                    srk);
            int intTaskId = aTask.getTaskId();
            int intDealId = aTask.getDealId();
            Deal aDeal = getDeal(intDealId);
            ArrayList alWftProcessAssoc = (ArrayList) wftProcessAssoc
                    .findByTaskId(intTaskId);
            Iterator itWftProcessAssoc = alWftProcessAssoc.iterator();
            WorkflowTaskProcessAssoc entityWftProcessAssoc = null;
            String strProcessName = null;
            WFProcessBase wfProcess = null;
            while (itWftProcessAssoc.hasNext()) {
                entityWftProcessAssoc = (WorkflowTaskProcessAssoc) itWftProcessAssoc
                        .next();
                strProcessName = entityWftProcessAssoc.getProcessName();
                StringBuffer strBufferPCName = new StringBuffer(500);
                strBufferPCName
                        .append(AMEConstants.IDENTITY_PACKAGE_NAME_WORKFLOW_PROCESS);
                strBufferPCName.append(strProcessName);
                System.out.println(strBufferPCName.toString());
                wfProcess = (WFProcessBase) Class.forName(
                        strBufferPCName.toString()).newInstance();
                wfProcess.setDeal(aDeal);
                wfProcess.setSessionResourceKit(srk);
                intResult = wfProcess.execute(true);
            }
            assertEquals(intResult, 0);
        } catch (Exception ex) {
            ex.printStackTrace();
            fail();
        }
    }

    private AssignedTask getAssignedTask(int assignedTasksWorkQueueId)
            throws Exception {
        AssignedTask assignedTask = new AssignedTask(srk);
        AssignedTaskBeanPK pk = new AssignedTaskBeanPK(assignedTasksWorkQueueId);
        assignedTask.findByPrimaryKey(pk);
        return assignedTask;
    }

    private int setupTaskQueueTestData() {
        return 1;
    }

    public void testAMEManager() {
        try {
            int assignedTasksWorkQueueId;
            int taskStatusId;
            AssignedTask assignedTask = new AssignedTask(srk);

            assignedTasksWorkQueueId = setupTaskQueueTestData();

            assignedTask = getAssignedTask(assignedTasksWorkQueueId);

            taskStatusId = assignedTask.getTaskStatusId();

            AMEWorker ameWork = new AMEWorker(assignedTasksWorkQueueId, userInstitutionId, intUserProfileId);

            ameWork.run();

            assignedTask = ameWork.getAssignedTask(assignedTasksWorkQueueId);

            assertNotNull(assignedTask);
            assertFalse(taskStatusId == assignedTask.getTaskStatusId());
            assertEquals(assignedTask.getTaskStatusId(), AMEWorker.COMPLETE);

        } catch (Exception ex) {
            ex.printStackTrace();
            fail();
        }
    }

    public void testFAME() {
        try {
            int assignedTasksWorkQueueId;
            AssignedTask aTask = new AMEManager().getActiveTask();
            assignedTasksWorkQueueId = aTask.getAssignedTasksWorkQueueId();
            srk.beginTransaction();
            aTask.setRetryCounter(0);
            aTask.ejbStore();
            srk.commitTransaction();
            AMEWorker aWorker = new AMEWorker(assignedTasksWorkQueueId, userInstitutionId, intUserProfileId);
            new Thread(aWorker).start();

            Thread.sleep(80000);
            AssignedTask bTask = aWorker
                    .getAssignedTask(assignedTasksWorkQueueId);
            int intRetryConter = bTask.getRetryCounter();
            assertTrue(intRetryConter == 1);
        } catch (Exception ex) {
            fail();
        }
    }
}