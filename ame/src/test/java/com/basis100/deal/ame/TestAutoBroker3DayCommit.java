/*
 * @(#)TestAutoBroker3DayCommit.java     Feb 14, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.ame;

import MosSystem.Sc;

import com.basis100.workflow.entity.AssignedTask;

public class TestAutoBroker3DayCommit extends AMEProcessTestBase {
    public static final String AUTOBROKER3DAYCOMMIT = "AutoBroker3DayCommit";

    public TestAutoBroker3DayCommit() {
        super();
    }

    public TestAutoBroker3DayCommit(String name) {
        super(name);
    }

    public int setupProcessSpecificData() throws Exception {
        // approach 1
        int assignedTasksWorkQueueId = 0;
        AssignedTask assignedTask = getAssignedTask(userProfileId,
                retryExternal);
        if (assignedTask != null) {
            srk.beginTransaction();
            assignedTasksWorkQueueId = assignedTask
                    .getAssignedTasksWorkQueueId();
            // work on the local db
            // for the selected row make the task as AutoBroker3DayCommit task
            // id
            assignedTask.setTaskId(getTaskId(AUTOBROKER3DAYCOMMIT,
                    userInstitutionId));
            assignedTask.ejbStore();
            srk.commitTransaction();
        }
        return assignedTasksWorkQueueId;

        // approach 2
        // not sure whether the above approach will cause any
        // problems[dependency]
        // not sure of a valid solution also. Confirm with SEAN.
    }

    public boolean verifyProcessOutput() throws Exception {
        boolean returnValue = false;
        AssignedTask assignedTask = getAssignedTask(assignedTasksWorkQueueId);
        returnValue = assignedTask.getTaskStatusId() == Sc.TASK_COMPLETE;
        // retrieve record from documentQueue and make sure the record is
        // generated.
        // Is the link based just on the deal id is ok!!!
        // long dealId;
        // dealId = assignedTask.getDealId();
        // DocumentQueue documentQueue = new DocumentQueue(srk);
        // DocumentQueue
        return returnValue;
    }
}
