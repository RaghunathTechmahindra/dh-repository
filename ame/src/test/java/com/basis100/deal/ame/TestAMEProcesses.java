/*
 * @(#)TestAMEProcesses.java     Feb 14, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.ame;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestAMEProcesses extends TestCase {
    static Class[] testProcesses = { TestAutoBroker3DayCommit.class };

    public TestAMEProcesses() {
        super();
    }

    public static Test suite() throws Exception {
        TestSuite suite = new TestSuite();
        for (Class process : testProcesses) {
            suite.addTestSuite(process);
        }
        return suite;
    }
}
