/*
 * @(#)AMEProcessTestBase.java     Feb 14, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.ame;

import junit.framework.TestCase;

import org.junit.Test;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.security.DLM;
import com.basis100.log.SysLog;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.entity.AssignedTask;
import com.basis100.workflow.entity.WorkflowTaskProcessAssoc;
import com.basis100.workflow.pk.AssignedTaskBeanPK;

public abstract class AMEProcessTestBase extends TestCase implements
        AMETestCase {
    protected static final PropertiesCache propertiesCache = PropertiesCache
            .getInstance();
    protected static final String strUserId = propertiesCache
            .getInstanceProperty(AMEConstants.PROPERTY_USER_ID, "");
    protected static final String strPw = propertiesCache.getInstanceProperty(
            AMEConstants.PROPERTY_PASSWORD, "");
    int retryInternal = 0;
    int retryExternal = 5;
    int expireRetry = 2;
    int taskComplete = 3;
    int error = -1;
    int userInstitutionId;
    int userProfileId;
    SessionResourceKit srk;
    CalcMonitor calMonitor;
    int assignedTasksWorkQueueId;

    public AMEProcessTestBase() {
        super();
    }

    public AMEProcessTestBase(String name) {
        super(name);
    }

    protected void tearDown() {
        srk.freeResources();
    }

    protected void setUp() throws Exception {
        srk = new SessionResourceKit("TestAMEProcesses");

        UserProfile userProfile = new UserProfile(srk);
        userProfile = userProfile.findByLoginId(strUserId);
        userProfileId = userProfile.getUserProfileId();
        userInstitutionId = userProfile.getInstitutionId();

        ResourceManager.init();

        PicklistData.init();
        DLM.init(srk, false);
        calMonitor = CalcMonitor.getMonitor(srk);
        assignedTasksWorkQueueId = setupProcessSpecificData();
    }

    @Test
    public void testProcess() {
        try {
            AMEWorker aW = new AMEWorker(assignedTasksWorkQueueId, userInstitutionId, userProfileId);
            aW.run();
            assertTrue(verifyProcessOutput());
        } catch (Exception ex) {
            ex.printStackTrace();
            fail();
        }
    }

    protected AssignedTask getAssignedTask(int assignedTasksWorkQueueId)
            throws Exception {
        AssignedTask assignedTask = new AssignedTask(srk);
        AssignedTaskBeanPK pk = new AssignedTaskBeanPK(assignedTasksWorkQueueId);
        assignedTask.findByPrimaryKey(pk);
        return assignedTask;
    }

    protected AssignedTask getAssignedTask(int userProfileId, int retryCounter)
            throws Exception {
        AssignedTask assignedTsk = new AssignedTask(srk);
        assignedTsk = assignedTsk.findNextAutoTask(userProfileId, retryCounter);
        return assignedTsk;
    }

    protected int getTaskId(String taskName, int institutionProfileId)
            throws Exception {
        int taskId;
        WorkflowTaskProcessAssoc wftpa = new WorkflowTaskProcessAssoc(srk);
        wftpa = wftpa.findByTaskName(taskName, institutionProfileId);
        taskId = wftpa.getTaskId();
        return taskId;
    }
}
