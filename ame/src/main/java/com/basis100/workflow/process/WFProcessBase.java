package com.basis100.workflow.process;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

import org.apache.log4j.Logger;

import MosSystem.Mc;

import com.basis100.deal.docprep.Dc;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.security.DLM;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.WFEExternalTaskCompletion;
import com.basis100.workflow.WFETrigger;

/**
 * <p>
 * Title: WFProcessBase
 * </p>
 * 
 * <p>
 * Description: Base class for Work Flow Processes.
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2004
 * </p>
 * 
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author Billy Lam
 * @version 1.0 (Initial Version - 31 May 2004) 1.1 : some changes for the new
 *          implementation of AME (see AME spec. Section 6.2.1)
 *  
 */

abstract public class WFProcessBase
{
    
    
    // resources kit provided by caller in (sub-class) constructors
    protected SessionResourceKit srk;

    // resources from resource kit
    protected SysLogger logger;
    protected JdbcExecutor jExec;
    protected Deal deal;
    protected DealHistoryLogger hLog;

    //--> Cervus II : Modifications for AME Implementation (AME spec. section
    // 6.2.1)
    //--> By Billy 20Oct2004
    public final static int RETRY_INTERNAL = 0;
    public final static int RETRY_EXTERNAL = 1;
    public final static int EXPIRE_RETRY = 2;
    public final static int COMPLETE = 3;
    public final static int ERROR = -1;
    public final static int AUTO_USER_TYPE = 99;
    public final static String DOCUMENT_CREATE_BY_BUSINESS_RULES = "com.basis100.document.create.by.business.rules";
    public final static String NO = "N";
    public final static String YES = "Y";
    public final static String MESSAGE_GO_THROUGH_BUSINESS_RULE = "Request for this document must go through business rule. Modify mossys properties please.";
    public final static String MESSAGE_DEAL_ID = "dealid";
    public final static String MESSAGE_COPY_ID = "copyid";
    public PropertiesCache proCache = PropertiesCache.getInstance();
    public final static String MESSAGE_REQUEST_DO_BY_BUSINESS_RULE = "Exception thrown from DocumentRequest.requestDocByBusRule()";
    public static final String WF_TRIGGER_KEY_DEAL_HOLD = "Auto-Hold";
    public static final String WF_TRIGGER_KEY_FUNDING_UPLOAD = "Funding.Upload";
    
    public static final String AUTOBROKER15PTC_PROCESS = "15PTC";
    public static final String AUTOBROKER10PTC_PROCESS = "10PTC";
    public static final String AUTOBROKER7PTC_PROCESS = "7PTC";
    public static final String AUTOBROKER3DAYCOMMIT = " 3DayCommit";
    public static final String AUTOBROKER7DAYCOMMIT = "7DayCommit";

    public static final String BORROWER = "Borrower";
    public static final String PROPERTY = "Property";

    private static Logger log = Logger.getLogger(WFProcessBase.class);

    // default constructor
    public WFProcessBase()
    {

        
    }

    public WFProcessBase(Deal aDeal, SessionResourceKit srk)
    {
        log = Logger.getLogger(this.getClass());
        this.deal = aDeal;
        setSessionResourceKit(srk);
    }

    public SysLogger getLogger()
    {
        return logger;
    }

    public void setLogger(SysLogger logger)
    {
        this.logger = logger;
    }

    public SessionResourceKit getSessionResourceKit()
    {
        return srk;
    }

    public void setSessionResourceKit(SessionResourceKit srk)
    {
        this.srk = srk;
        logger = srk.getSysLogger();
        jExec = srk.getJdbcExecutor();
    }

    public Deal getDeal()
    {
        return deal;
    }

    public void setDeal(Deal deal)
    {
        this.deal = deal;
    }

    /**
     * workflowTrigger Method to trigger workflow.
     * 
     * @param type
     *            int 0 - task completion activity 1 - workflow selection and
     *            initial task generation 2 - as 1) but add unconditional asnc
     *            state (test) task generation (e.g. possible MI task(s))
     * @param extObj
     *            WFEExternalTaskCompletion
     * @param theDeal
     *            Deal
     * @throws Exception
     */
    protected void workflowTrigger(int type, WFEExternalTaskCompletion extObj,
            Deal theDeal) throws Exception
    {
        logger
                .debug("@WFProcessBase.workflowTrigger :: ================ Begin =================");
        WFETrigger.workflowTrigger(type, srk, theDeal.getUnderwriterUserId(),
                theDeal.getDealId(), theDeal.getCopyId(), -1, extObj);
        logger
                .debug("@WFProcessBase.workflowTrigger :: ================ End =================");
    }

    /**
     * workflowTrigger Method to trigger workflow.
     * 
     * @param type
     *            int 0 - task completion activity 1 - workflow selection and
     *            initial task generation 2 - as 1) but add unconditional asnc
     *            state (test) task generation (e.g. possible MI task(s))
     * @param extObj
     *            WFEExternalTaskCompletion
     * @throws Exception
     */
    protected void workflowTrigger(int type, WFEExternalTaskCompletion extObj)
            throws Exception
    {
        logger
                .debug("@WFProcessBase.workflowTrigger :: ================ Begin =================");
        WFETrigger.workflowTrigger(type, srk, deal.getUnderwriterUserId(), deal
                .getDealId(), deal.getCopyId(), -1, extObj);
        logger
                .debug("@WFProcessBase.workflowTrigger :: ================ End =================");
    }

    /**
     * workflowTrigger Simplied method to trigger workflow
     * 
     * @param type
     *            int
     * @throws Exception
     */
    protected void workflowTrigger(int type) throws Exception
    {
        workflowTrigger(type, null);
    }

    /**
     * workflowTrigger Simplied method to trigger workflow : by default will
     * trigger with type 2
     * 
     * @throws java.lang.Exception
     */
    protected void workflowTrigger() throws Exception
    {
        workflowTrigger(2, null);
    }

    /**
     * placeDealLock Method the place a deal lock.
     * 
     * @return boolean : true - deal lock placed successfully false - the deal
     *         was locked by other user and was not overriable
     */
    public boolean placeDealLock()
    {
        try
        {
            String className = this.getClass().getName();
            // Take out the Package name
            className = className.substring(className.lastIndexOf(".") + 1);

            DLM dlm = DLM.getInstance();
            dlm.placeLock(deal.getDealId(), srk.getExpressState().getUserProfileId(),
                    "Lock by AME for " + className + " !!", srk, true);

            return true;
        } catch (Exception ex)
        {
            logger.warning("Deal was locked by other user :: " + ex);
            return false;
        }
    }

    /**
     * placeSoftDealLock Method the place a soft deal lock, i.e. users can
     * override the lock.
     * 
     * @return boolean : true - deal lock placed successfully false - the deal
     *         was locked by other user and was not overriable
     */
    public boolean placeSoftDealLock()
    {
        try
        {
            String className = this.getClass().getName();
            // Take out the Package name
            className = className.substring(className.lastIndexOf(".") + 1);

            DLM dlm = DLM.getInstance();
            dlm.placeSoftLock(deal.getDealId(), srk.getExpressState().getUserProfileId(),
                    "Lock by AME for " + className + " !!", srk, true);

            return true;
        } catch (Exception ex)
        {
            logger.warning("Deal was locked by other user :: " + ex);
            return false;
        }
    }

    /**
     * unLockDeal Method to unlock a deal.
     * 
     * @return boolean : true - deal was unlocked successfully false - deal was
     *         failed to unlock
     */
    public boolean unLockDeal()
    {
        try
        {
            DLM dlm = DLM.getInstance();
            dlm.doUnlock(deal.getDealId(), srk.getExpressState().getUserProfileId(), srk, true,
                    true); // Assume always in transaction

            return true;
        } catch (Exception ex)
        {
            logger.warning("Problem when try to unlock the deal :: " + ex);
            return false;
        }
    }

    /**
     * Validate()
     * 
     * Method to validate if the WF Process should be executed or not.
     * 
     * @return boolean true -- OK to run process false -- Process should not run
     */
    public abstract boolean Validate();

    /**
     * Execute() Method to execute the WorkFlow Process.
     * 
     * @param skipValidate
     *            boolean true -- Skip to call Validate() method false -- call
     *            Validate() method and return false if Validate() return false
     * 
     * @return int : indicator to tell AME on how to handle Retry RETRY_INTERNAL
     *         (0) -- Retry handle internally, AME should call
     *         AssignedTask.setForRetry(true) to increment Retry Counter
     *         RETRY_EXTERNAL (1) -- Retry handle externally, AME should call
     *         AssignedTask.setForRetry(false) to not increment Retry Counter
     *         EXPIRE_RETRY (2) -- Retry handle externally, AME should call
     *         AssignedTask.setRetryExpired(false) to stop retry and escalate
     *         ERROR (-1) -- Process executed with Fatal Errors, AME should Log
     *         the error and set to retry as usual
     */
    public abstract int execute(boolean skipValidate);

    protected void followUpExecute(String processName, Deal deal)
    {
        DealPK dealPk = null;
        int intConditionId = 0;
        Collection aListDocTracking = new ArrayList();

        try
        {

            hLog = DealHistoryLogger.getInstance(srk);
            hLog
                    .log(
                            deal.getDealId(),
                            deal.getCopyId(),
                            "Broker outstanding conditions update produced, DocTracking",
                            Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId());

           intConditionId = ConditionMapper.getInstance().getCondtionId(processName);

            DocumentTracking docTracking = new DocumentTracking(srk);
            int intDealId = deal.getDealId();
            int intCopyId = deal.getCopyId();
            dealPk = new DealPK(intDealId, intCopyId);

            aListDocTracking = docTracking.findByDealAndConditionId(dealPk,
                    intConditionId);
            if (!aListDocTracking.isEmpty())
            {

                Iterator itDocTracking = aListDocTracking.iterator();
                while (itDocTracking.hasNext())
                {
                    srk.beginTransaction();
                    docTracking = (DocumentTracking) itDocTracking.next();
                    docTracking.setDocumentStatusId(Mc.DOC_STATUS_APPROVED);
                    docTracking.ejbStore();
                    srk.commitTransaction();
                }

                //delegate the request of insert document to documentrequest
                // with strProcessname and deal

                log.debug("messageId - "
                        + MessageMapper.getInstance().getMessage(processName));
                DocumentRequest.requestConditionsOutstanding(srk, deal,
                        Dc.DOCUMENT_GENERAL_TYPE_CONDITIONS_OUTSTANDING,
                        MessageMapper.getInstance().getMessage(processName));

            }
            srk.beginTransaction();
            workflowTrigger(2);
            srk.commitTransaction();

        } catch (Exception ex)
        {
            logger.error("Problem when trying to changeDocumentStatus() in "
                    + this.getClass().getName() + ex);
        }

    }

    public boolean checkDealLocked()
    {
        boolean lockResult = false;
        try
        {
            srk.beginTransaction();
            lockResult = placeDealLock();
            srk.commitTransaction();
        } catch (Exception ex)
        {

        }
        return lockResult;
    }

    public Locale getLocale()
    {

        Locale locale = null;
        int langId = 0;
        try
        {

            ArrayList borrowerList = (ArrayList) getDeal().getBorrowers();
            Borrower aBorrower = (Borrower) borrowerList.get(0);
            langId = aBorrower.getLanguagePreferenceId();
            if (langId == 0)
                locale = Locale.CANADA;
            else
                locale = Locale.CANADA_FRENCH;

        } catch (Exception e)
        {

            log.error(e);
        }
        return locale;
    }

    public boolean isRecommendedScenerioCreated()
    {

        boolean scenarioLocated = false;
        Deal dealScenario;

        // get recommended scenario (if one)
        try
        {
            dealScenario = deal.findByRecommendedScenario(new DealPK(deal
                    .getDealId(), -1), true);
                    //.getDealId(), -1), false);
            scenarioLocated = true;
        } catch (Exception e)
        {
            ;
        }

        // get first unlocked scenario (if necessary and one)
        if (scenarioLocated == false)
        {
            try
            {
                dealScenario = deal.findByUnlockedScenario(new DealPK(deal
                        .getDealId(), -1));
                scenarioLocated = true;
                logger
                        .debug("@navigateToNextPage.underwriterWorksheetSpecial: unlocked sceanrio located.");
            } catch (Exception e)
            {
                ;
            }
        }

        return scenarioLocated;
    }

    /**
     * 
     * This method runs all the rules that are dependent on a collection.
     * Currently, this method only supports Borrower and Property collections.
     * If more are needed a factory method of some sort will be created.
     * 
     * 
     * @param brExec
     * @param ruleName
     * @param col
     * @param elementType
     * @return
     * @throws Exception
     */
    public boolean runCollectionRelatedRules(BusinessRuleExecutor brExec,
            String ruleName, Collection col, String elementType)
            throws Exception
    {

        PassiveMessage pm = null;

        //This implimentation may not retain the order of borrowers. If this is
        // required it will be changed.
        Iterator iter = col.iterator();
        int i = 0;
        while (iter.hasNext())
        {

            brExec.setCurrentProperty(-1);
            brExec.setCurrentBorrower(-1);

            Object element = iter.next();

            Integer id = null;
            if (BORROWER.equals(elementType))
            {
                Method meth = element.getClass().getDeclaredMethod(
                        "getBorrowerId", null);
                id = (Integer) meth.invoke(element, null);
                brExec.setCurrentBorrower(id.intValue());
            } else if (PROPERTY.equals(elementType))
            {
                Method meth = element.getClass().getDeclaredMethod(
                        "getPropertyId", null);
                id = (Integer) meth.invoke(element, null);
                brExec.setCurrentProperty(id.intValue());
            }

            logger.debug("@AutoMIProcess.ValidateDeal: " + ruleName
                    + " rules, " + elementType + "  num=" + i);
            pm = brExec.BREValidator((DealPK) (deal.getPk()), srk, null,
                    ruleName, true);

            if (pm != null && pm.getCritical() == true)
            {
                brExec.close();
                logger.debug("@AutoMIProcess.ValidateDeal: " + elementType
                        + " critial error found !! Return false !!");
                logger.debug(pm.getMsgs().toString());
                return false;
            }
            i++;
        }

        //find out if brExec needs to be closed
        return true;
    }

    static class MessageMapper
    {

        private HashMap mapper = new HashMap();
        private static MessageMapper instance = new MessageMapper();

        public MessageMapper()
        {

            mapper.put(AUTOBROKER10PTC_PROCESS, new Integer(
                    Dc.MESSAGE_AUTOBROKER_10_PTC));
            mapper.put(AUTOBROKER15PTC_PROCESS, new Integer(
                    Dc.MESSAGE_AUTOBROKER_15_PTC));
            mapper.put(AUTOBROKER3DAYCOMMIT, new Integer(
                    Dc.MESSAGE_AUTOBROKER_3_COMMIT));
            mapper.put(AUTOBROKER7DAYCOMMIT, new Integer(
                    Dc.MESSAGE_AUTOBROKER_7_COMMIT));
            mapper.put(AUTOBROKER7PTC_PROCESS, new Integer(
                    Dc.MESSAGE_AUTOBROKER_7_PTC));

        }

        public static MessageMapper getInstance()
        {

            return instance;
        }

        public int getMessage(String message)
        {

            return ((Integer) mapper.get(message)).intValue();
        }

    }

    static class ConditionMapper
    {

        /**
         * 
         * This class defines the following mapping.
         * 
         * if (!"".equals(processName) &&
         * processName.equals(AUTOBROKER15PTC_PROCESS)) { intConditionId =
         * 535;
         *  } else if (!"".equals(processName) &&
         * processName.equals(AUTOBROKER10PTC_PROCESS)) { intConditionId =
         * 534;
         *  } else if (!"".equals(processName) &&
         * processName.equals(AUTOBROKER7PTC_PROCESS)) { intConditionId = 533;
         *  } else if (!"".equals(processName) &&
         * processName.equals(AUTOBROKER3DAYCOMMIT)) { intConditionId = 516;
         *  } else if (!"".equals(processName) &&
         * processName.equals(AUTOBROKER7DAYCOMMIT)) { intConditionId = 517;
         *  }
         */

        private HashMap mapper = new HashMap();
        private static ConditionMapper instance = new ConditionMapper();

        public ConditionMapper()
        {

            mapper.put(AUTOBROKER15PTC_PROCESS, new Integer(535));
            mapper.put(AUTOBROKER10PTC_PROCESS, new Integer(534));
            mapper.put(AUTOBROKER7PTC_PROCESS, new Integer(533));
            mapper.put(AUTOBROKER3DAYCOMMIT, new Integer(516));
            mapper.put(AUTOBROKER7DAYCOMMIT, new Integer(517));
        }

        public static ConditionMapper getInstance()
        {

            return instance;
        }

        public int getCondtionId(String message)
        {

            return ((Integer) mapper.get(message)).intValue();
        }

    }

}