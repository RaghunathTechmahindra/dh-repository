/*
 * Created on Jan 11, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.basis100.workflow.process;

import java.util.Collection;
import java.util.Iterator;

import MosSystem.Mc;

import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;

/**
 * @author MMorris
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class AutoBrokerUpdateProcess extends WFProcessBase
{

    public AutoBrokerUpdateProcess()
    {

    }

    public AutoBrokerUpdateProcess(Deal deal, SessionResourceKit srk)
    {

        super(deal, srk);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.basis100.workflow.process.WFProcessBase#Validate()
     */
    public boolean Validate()
    {
        // TODO Auto-generated method stub
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.basis100.workflow.process.WFProcessBase#Execute(boolean)
     */
    public int execute(boolean skipValidate)
    {

        handleBrokerUpdate();
        return RETRY_INTERNAL;
    }

    public void handleBrokerUpdate()
    {

        try
        {

            DocumentTracking aDocument, dt = new DocumentTracking(srk);
            Collection col = dt.findByDealAndConditionId((DealPK) deal.getPk(),
                    516);
            
            Iterator iter = col.iterator();

            aDocument = (DocumentTracking) iter.next();
            if (aDocument.getDocumentStatusId() != 3)
            {
                srk.beginTransaction();
                aDocument.setDocumentStatusId(3);
                aDocument.ejbStore();
                srk.commitTransaction();

            } else
            {
                col = dt.findByDealAndConditionId((DealPK) deal.getPk(), 517);
                iter = col.iterator();
                aDocument = (DocumentTracking) iter.next();
                srk.beginTransaction();
                aDocument.setDocumentStatusId(3);
                aDocument.ejbStore();
                srk.commitTransaction();
            }
            
            //Taken from DocTrakingHandler.setDealStatus
            DocumentRequest.requestConditionsOutstanding(srk, deal);           
            DealHistoryLogger hLogBroker = DealHistoryLogger.getInstance(srk);
            String msgbroker = BXResources.getSysMsg(
                    "BROKER_OUTSTANDING_CONDITIONS_PRODUCED", 0);
            hLogBroker.log(deal.getDealId(), deal.getCopyId(), msgbroker,
                    Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), 
                    deal.getStatusId());
        } catch (Exception e)
        {
            StringUtil.stack2string(e);
        }
    }

}