/*
 * Created on Nov 1, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

// 06/June/2007 SL bilingual deal notes project

package com.basis100.workflow.process;

import java.util.Collection;
import java.util.Date;
import java.util.Formattable;
import java.util.HashMap;

import MosSystem.Mc;

import com.basis100.deal.ame.AMEConstants;
import com.basis100.deal.commitment.CCM;
import com.basis100.deal.entity.AutoDecision;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealNotes;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.WorkflowTrigger;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.miprocess.MIProcessHandler;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.WorkflowAssistBean;
import com.filogix.util.Xc;

/**
 * @author MMorris
 * 
 * This class represents the base functionality for handling the processing of
 * MI requests for the AME. *
 * @version 1.1 <br>
 *          Date: 06/06/006 <br>
 *          Author: NBC/PP Implementation Team <br>
 *          Change: <br>
 *          Added a new method - handleGeneratePecEmail() to insert a record in workflowtrigger table for
 *          generate pec email functionality, when an email is sent<br>
 *          <br>
 *          <br>
 * 
 */
public class BaseAutoMIProcess extends WFProcessBase
{
    
//    public static final String FAILED_MI_BUSINESS_RULE_LABEL = "FAILED_MI_BUSINESS_RULE_LABEL";

    protected WorkflowAssistBean wfa;

    protected DealHistoryLogger hLog;

    public static final String STATUS_APPROVED = "APPROVED";

    public static final String STATUS_FAILED = "FAILED";

    public static final String STATUS_MANUAL = "MANUAL";

    public static final String STATUS_UNDEFINED = "UNDEFINED";

    public static final String STATUS_DECLINED = "DECLINED";

    public static final String INSURER_CMHC = "CMHC";
    public static final String INSURER_GE = "GE";

    /** just a temp value, not sure where to grab the resource info from yet */
    public static final int MAX_ATTEMPTS = 30;

    protected static HashMap insurerMapping = new HashMap();
    
    public BaseAutoMIProcess()
    {
    }

    /**
     * @param deal
     * @param srk
     */
    public BaseAutoMIProcess(Deal deal, SessionResourceKit srk)
    {
        super(deal, srk);
        insurerMapping.put(INSURER_CMHC, new Integer(Mc.MI_INSURER_CMHC));
        insurerMapping.put(INSURER_GE, new Integer(Mc.MI_INSURER_GE));
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.basis100.workflow.process.WFProcessBase#Validate()
     */
    public boolean Validate()
    {
        // TODO Auto-generated method stub
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.basis100.workflow.process.WFProcessBase#Execute(boolean)
     */
    public int execute(boolean skipValidate)
    {
        try
        {
            // Lock the deal first : Return RETRY if failed
            srk.beginTransaction();
            boolean lockResult = placeDealLock();
            srk.commitTransaction();
            if (lockResult == false)
            {
                return RETRY_INTERNAL;
            }

            //WorkflowAssistBean wfa = new WorkflowAssistBean();
            hLog = DealHistoryLogger.getInstance(srk);

            //Send a request to helix. For now I will fake the call
            //just so I can test the class.
            // get response

            String helixResponse = STATUS_APPROVED;
            if (helixResponse.equalsIgnoreCase(STATUS_APPROVED))
            {
                handleApproved();

            } else if (helixResponse.equalsIgnoreCase(STATUS_FAILED))
            {

                return RETRY_INTERNAL;
            } else if (helixResponse.equalsIgnoreCase(STATUS_MANUAL))
            {

                srk.beginTransaction();
                String msg = hLog.statusChange(deal.getStatusId(),
                        "AutoMIProcess");
                updateDealStatus(deal, Mc.DEAL_INCOMPLETE, msg,
                        Mc.DEAL_TX_TYPE_EVENT);
                this.workflowTrigger();
                srk.commitTransaction();
            } else if (helixResponse.equalsIgnoreCase(STATUS_UNDEFINED))
            {
                AutoDecision ad = new AutoDecision(srk);
                ad.findByDealId(deal.getPk().getId());
                ad.incrementAttempt();
                if (ad.getAttemptNum() >= MAX_ATTEMPTS)
                {
                    return EXPIRE_RETRY;
                } else
                {
                    return RETRY_EXTERNAL;
                }
            } else if (helixResponse.equalsIgnoreCase(STATUS_DECLINED))
            {
                //Not sure what handle declined is
            }

        } catch (Exception e)
        {
            logger.error("Exception @AutoMIProcess.Execute :: "
                    + StringUtil.stack2string(e));
        } finally
        {
            // Unlock the deal before return
            try
            {
                srk.beginTransaction();
                this.unLockDeal();
                srk.commitTransaction();
            } catch (Exception ex)
            {
                srk.cleanTransaction();
            }
        }

        return RETRY_INTERNAL;
    }

    /**
     * ValidateDeal To run DE, DEA and DEP rules and return true if not critical
     * error fired. Otherwise, return false.
     * 
     * @return boolean true -- No critical error false -- Critical Error fired
     */
    private boolean ValidateDeal()
    {
        PassiveMessage pm = null;
        BusinessRuleExecutor brExec = new BusinessRuleExecutor();

        try
        {

            // Validate DE (Deal) rules
            pm = brExec.BREValidator((DealPK) (deal.getPk()), srk, null,
                    "DE-%", true);

            // Return false if there is any Critial Rule(s) fired
            if (pm != null && pm.getCritical() == true)
            {
                brExec.close();
                logger
                        .debug("@AutoMIProcess.ValidateDeal: Deal critial error found !! Return false !!");
                logger.debug("The Deal Rule Errors :: "
                        + pm.getMsgs().toString());
                return false;
            }

            //
            // local scope rules ...
            int lim = 0;
            Object[] objs = null;

            // borrower rules
            try
            {

                // borrowers for DEA rules");
                Collection borrowers = deal.getBorrowers();
                lim = borrowers.size();
                objs = borrowers.toArray();
            } catch (Exception e)
            {
                lim = 0;
            }

            // assume no borrowers
            for (int i = 0; i < lim; ++i)
            {
                Borrower borr = (Borrower) objs[i];
                brExec.setCurrentProperty(-1);
                brExec.setCurrentBorrower(borr.getBorrowerId());

                logger
                        .debug("@AutoMIProcess.ValidateDeal: DEA rules, borrower num="
                                + i);
                pm = brExec.BREValidator((DealPK) (deal.getPk()), srk, null,
                        "DEA-%", true);

                if (pm != null && pm.getCritical() == true)
                {
                    brExec.close();
                    logger
                            .debug("@AutoMIProcess.ValidateDeal: Borrower critial error found !! Return false !!");
                    logger.debug(pm.getMsgs().toString());
                    return false;
                }
            }

            // property rules
            try
            {
                // logger.debug("@DealHandlerCommon.validateDealEntry: collect
                // properties for DEP rules");
                Collection properties = deal.getProperties();
                lim = properties.size();
                objs = properties.toArray();
            } catch (Exception e)
            {
                lim = 0;
            }

            // assume no properties
            for (int i = 0; i < lim; ++i)
            {
                Property prop = (Property) objs[i];
                brExec.setCurrentProperty(prop.getPropertyId());
                brExec.setCurrentBorrower(-1);

                logger
                        .debug("@DealHandlerCommon.validateDealEntry: DEP rules, propert num="
                                + i);
                pm = brExec.BREValidator((DealPK) (deal.getPk()), srk, null,
                        "DEP-%", true);

                if (pm != null && pm.getCritical() == true)
                {
                    brExec.close();
                    logger
                            .debug("@AutoMIProcess.ValidateDeal: Property critial error found !! Return false !!");
                    logger.debug(pm.getMsgs().toString());
                    return false;
                }
            }

            // Return true by default
            brExec.close();
            return true;
        } catch (Exception e)
        {
            // Process performed failed
            logger.error("Problem @AutoMIProcess.ValidateDeal() :: ");
            logger.error(e.toString());
            try
            {
                brExec.close();
            } catch (Exception ex)
            {
                logger
                        .error("Problem @AutoMIProcess.ValidateDeal() :: when closing BRexecutor !!");
                logger.error(ex.toString());
            }
            return false;
        }
    }

    /**
     * ValidateMI To run MI rules and return true if not critical error fired.
     * Otherwise, return false.
     * 
     * @return boolean true -- No critical error false -- Critical Error fired
     */
    protected boolean ValidateMI()
    {
        PassiveMessage pm = null;
        BusinessRuleExecutor brExec = new BusinessRuleExecutor();

        try
        {
            // Validate MI (Deal) rules
            pm = brExec.BREValidator((DealPK) (deal.getPk()), srk, null,
                    "MI-%", true);

            // Return false if there is any Critial Rule(s) fired
            if (pm != null && pm.getCritical() == true)
            {
                brExec.close();
                logger
                        .debug("@AutoMIProcess.ValidateMI: MI critial error found !! Return false !!");
                logger
                        .debug("The MI Rule errors :: "
                                + pm.getMsgs().toString());
                return false;
            }

            // Return true by default
            brExec.close();
            return true;
        } catch (Exception e)
        {
            // Process performed failed
            logger.error("Problem @AutoMIProcess.ValidateMI() :: ");
            logger.error(e.toString());
            try
            {
                brExec.close();
            } catch (Exception ex)
            {
                logger
                        .error("Problem @AutoMIProcess.ValidateDeal() :: when closing BRexecutor !!");
                logger.error(ex.toString());
            }
            return false;
        }
    }

    public void updateDealStatus(Deal aDeal, int status, String msg, int typeId)
            throws RemoteException, FinderException
    {

        deal.setStatusDate(new Date());
        deal.setStatusId(status);
        deal.ejbStore();
        hLog.log(deal.getDealId(), deal.getCopyId(), msg, typeId, 
                 srk.getExpressState().getUserProfileId());
    }

    public void handleApproved() throws Exception
    {
        String msg = "";
        // fix for #5440
        if (handleApprovedDeal(msg)) {
          logger.debug("BaseAutoMIProcess.handleApproved(): calling handleApprovedMI()....");
        	handleApprovedMI(msg);
        }
    }

    public boolean handleApprovedDeal(String msg) throws Exception
    {
        if (ValidateDeal() == false)
        {
            logger.debug("BaseAutoMIProcess.handleApprovedDeal(): ValidateDeal() == false");
            
            srk.beginTransaction();
            msg = hLog.statusChange(deal.getStatusId(), "BaseAutoMIProcess");
            deal.setStatusDate(new Date());
            deal.setStatusId(Mc.DEAL_INCOMPLETE);
            deal.ejbStore();
            hLog.log(deal.getDealId(), deal.getCopyId(), msg, 
                     Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId());

            // Init and assign workflow
            //--> No need to Init WF as it should already init in
            // MosSystemServlet when handling WF Trigger
            //--> By Billy 06Dec2004
            //if(wfa.generateInitialTasks(deal.getDealId(), deal.getCopyId(),
            //   srk) == false)
            //{
            //  logger
            //    .error("@BaseAutoMIProcess.Execute :: No initial tasks generated
            // !!");
            //}

            //Trigger Workflow
            this.workflowTrigger();
            srk.commitTransaction();

            return false;
        } else
        {
            logger.debug("BaseAutoMIProcess.handleApprovedDeal(): ValidateDeal() == true");
            srk.beginTransaction();
            msg = hLog.statusChange(deal.getStatusId(), "BaseAutoMIProcess");
            deal.setStatusDate(new Date());
            deal.setStatusId(Mc.DEAL_IN_UNDEREWRITING);
            deal.ejbStore();
            if (!isRecommendedScenerioCreated())
                createNewScenario();
            hLog.log(deal.getDealId(), deal.getCopyId(), msg,
                    Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId());
            srk.commitTransaction();

            //The purpose of this code is to skip the MI check if the special
            // feature preapproval is set.
            if (deal.getSpecialFeatureId() == Mc.SPECIAL_FEATURE_PRE_APPROVAL)
            {
                srk.beginTransaction();
                WorkflowTrigger trigger = new WorkflowTrigger(srk);
                try
                {
                    trigger.setWorkflowTriggerValue(deal.getDealId(),
                            "Auto-PreApproval", "Y");
                } catch (Exception e)
                {
                    logger.error("Exception @BaseAutoMIProcess.handleApproved: "
                            + StringUtil.stack2string(e));
                }
                trigger.ejbStore();
                srk.commitTransaction();
                workflowTrigger(2);
            }
    		return true;
        }
    }

    public void handleApprovedMI(String msg) throws Exception
    {

            logger.debug("BaseAutoMIProcess.handleApprovedMI() start....");
            if (ValidateMI() == false)
            {
                logger.debug("BaseAutoMIProcess.handleApprovedMI(): ValidateMI() == false");
                srk.beginTransaction();

            deal.setMIStatusId(Mc.MI_STATUS_CRITICAL_ERRORS_REC_FROM_INSURER);
                deal.ejbStore();
                hLog.log(deal.getDealId(), deal.getCopyId(), msg,
                        Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId());

                //bilingual deal notes, rewrite
                DealNotes newNote = new DealNotes(srk);
                Formattable newMsg = BXResources.newGenMsg(Xc.AME_FAILED_MI_BUSINESS_RULE);
                newNote.create((DealPK)deal.getPk(),0, Mc.DEAL_NOTES_CATEGORY_DECISIONING, srk.getExpressState().getUserProfileId(), newMsg);

                // Trigger Workflow
                this.workflowTrigger();

                srk.commitTransaction();

            } else
            {

            setMIInsurer();
                logger.debug("BaseAutoMIProcess.handleApprovedMI(): ValidateMI() == true");

                // MI rule check passed
                // Request MI
                srk.beginTransaction();
                if (deal.getSpecialFeatureId() != Mc.SPECIAL_FEATURE_PRE_APPROVAL)
                {
                    MIProcessHandler mi = MIProcessHandler.getInstance();
                    int rcm = mi.determineRcmCommStatus(deal, srk);

                    boolean notMIApprovalStatus = false == mi
                            .isMIApprovalStatus(deal.getMIStatusId());

                    if (notMIApprovalStatus
                            && (rcm == MIProcessHandler.C_I_INITIAL_REQUEST_PENDING
                                    || rcm == MIProcessHandler.C_I_ERRORS_CORRECTED_AND_PENDING
                                    || rcm == MIProcessHandler.C_I_ERRORS_CORRECTED_AND_PENDING_OMIRF_M || rcm == MIProcessHandler.C_I_CHANGES_PENDING))
                    {

                        logger.debug("Mikey =====>  InsurerId = "
                                + deal.getMortgageInsurerId());
                        logger.debug("Mikey =====>  InidicatorId  = "
                                + deal.getMIIndicatorId());
                        mi.placeMIRequest(deal, srk, rcm);
                        deal.ejbStore();
                    }
                }
                // Trigger Workflow
                this.workflowTrigger();
                //There is an issue with transactions. Somewhere along the line
                // the transaction
                //seems to be lost. This check is added to ensure that there is
                // a current transaction
                //in progress.
                if (srk.getJdbcExecutor().isInTransaction())
                    srk.commitTransaction();
            }
        }

    protected void setMIInsurer() throws Exception
    {

       
        logger.debug(this.getClass() + ".setMIInsurer - loanValue = " + deal.getTotalLoanAmount());
        double dblCombinedLTV = deal.getCombinedLTV();
        double combinedLTVThreshold = new Double(PropertiesCache.getInstance().getProperty
        		(deal.getInstitutionProfileId(), AMEConstants.PROPERTY_LTV_THRESHOLD, AMEConstants.DEFAULT_LTV_THRESHOLD));
      
        if (dblCombinedLTV <= combinedLTVThreshold)
        {
            //srk.beginTransaction();
            //deal.setMortgageInsurerId(Mc.MI_INSURER_CMHC);               
            deal.setMortgageInsurerId( ((Integer)insurerMapping
                    .get(PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
                                AMEConstants.PROPERTY_DEFAULT_INSURER, INSURER_CMHC).trim())).intValue());
            deal
                    .setMIIndicatorId(Mc.MII_APPLICATION_FOR_LOAN_ASSESSMENT);
            deal.ejbStore();
        }
    }

    public void handleDeclined() throws Exception
    {

        srk.beginTransaction();
        CCM ccm = new CCM(srk);
        ccm.denyDeal(deal, Mc.DENIAL_REASON_OTHER);
        deal.setHoldReasonId(Mc.HOLD_REASON_NONE);
        
        //Create Broker notes, not sure what to put here.
        //bilingual deal notes, rewrite
        DealNotes notes = new DealNotes(srk);
        Formattable newMsg = BXResources.newGenMsg(Xc.AME_DEAL_DECLINED);
        notes.create((DealPK)deal.getPk(),0, 0, 0, newMsg);
        
        ccm.sendServicingUpload(srk, deal, Mc.SERVICING_UPLOAD_TYPE_DENY, 0);
        //Send helix upload is not created at current

        srk.commitTransaction();
    }

    
    /**
     * This code was taken from UWorkSheetHandler.handleCreateNewScenario.
     * The code was altered to always set the scenerio as recommended, because
     * this method will not be called unless no scenerio is found.
     * 
     * @throws Exception
     */
    public void createNewScenario() throws Exception
    {

        MasterDeal md = new MasterDeal(srk, null);
        md = md.findByPrimaryKey(new MasterDealPK(deal.getDealId()));

        //make new scenario
        logger.setJdbcTraceOn(false);
        int scenarioCid = md.copy(deal.getCopyId());
        logger.setJdbcTraceRestore();
        Deal dealScenario = new Deal(srk, null);
        dealScenario = dealScenario.findByPrimaryKey(new DealPK(deal.getDealId(), scenarioCid));
        dealScenario.setCopyType("S");
        dealScenario.setScenarioLocked("N");
        dealScenario.setScenarioRecommended("Y");
        dealScenario.setScenarioDescription("Ame Created Recommended");
        dealScenario.setScenarioNumber(md.nextScenarioNumber());
        
        dealScenario.ejbStore();

        md.ejbStore();

        setDeal(dealScenario);

    }

	/**
	 * handleGeneratePecEmail()- Added a new method to insert a record in
	 * workflowtrigger table for generate pec email functionality, when an email
	 * is sent
	 * 
	 * @param None
	 *            <br>
	 * @return void <br>
	 */
	public void handleGeneratePecEmail() {

		try {
			srk.beginTransaction();
			WorkflowTrigger trigger = new WorkflowTrigger(srk);

			trigger.setWorkflowTriggerValue(deal.getDealId(),
					Mc.WORKFLOWTRIGGER_TRIGGERKEY_AUTOPREAPPROVAL, "Y");

			trigger.ejbStore();
			srk.commitTransaction();
		} catch (JdbcTransactionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e) {
			logger.error("Exception @BaseAutoMIProcess.handleApproved: "
					+ StringUtil.stack2string(e));
		}
	}
    
}
