package com.basis100.workflow.process;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.util.StringUtil;
import com.basis100.resources.SessionResourceKit;

public class AutoBroker7PTC
  extends WFProcessBase
{
  public AutoBroker7PTC()
  {
  }

  public AutoBroker7PTC(Deal deal, SessionResourceKit srk)
  {
    super(deal, srk);

  }

  /**
   * Validate
   *
   * @return boolean
   */
  public boolean Validate()
  {
    return false;
  }

  /**
   * Execute
   *
   * @param skipValidate boolean
   * @return int
   */
  public int execute(boolean skipValidate)
  {
    String strMsg = null;
    try
    {
      if(!skipValidate && Validate() == false)
      {
        return RETRY_INTERNAL;
      }
      boolean lockResult = checkDealLocked();
      if(lockResult == false)
      {
        return RETRY_INTERNAL;
      }

      followUpExecute(AUTOBROKER7PTC_PROCESS, deal);
    }
    catch(Exception ex)
    {
      strMsg = "Exception encountered in execute() of " + this.getClass().getName() + ", dealId=" +
        deal.getDealId() + ", copyId=" + deal.getCopyId();
      logger.error(strMsg);
      logger.error(StringUtil.stack2string(ex));
      srk.cleanTransaction();
      return ERROR;

    }
    finally
    {
      // Unlock the deal before return
      try
      {
        srk.beginTransaction();
        unLockDeal();
        srk.commitTransaction();
      }
      catch(Exception ex)
      {
        srk.cleanTransaction();
      }
    }

    return RETRY_INTERNAL;
  }

}
