package com.basis100.workflow.process;

import com.basis100.deal.entity.Deal;
import com.basis100.resources.SessionResourceKit;



public class MIDenialBalancer extends MIDenialBase{
	

	
	protected void  determineMIInsurer() throws Exception{
		
		int insurer = new MISelector().getMIInsurer(deal, srk);
		deal.setMortgageInsurerId(insurer);
	}
	}
	
