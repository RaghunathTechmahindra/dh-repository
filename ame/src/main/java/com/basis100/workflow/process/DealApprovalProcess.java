/*
 * Created on Nov 16, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */


// 06/June/2007 SL bilingual deal notes project
 /*
  * <p>Title: DealApprovalProcess</p>
  *
  * <p>Description: Deal Approval Work Flow Processes.</p>
  *
  * <p>Copyright: Filogix Inc. (c) 2004</p>
  *
  * <p>Company: Filogix Inc.</p>
  * @author Michael Morris
  * @version 1.0 (Initial Version - Nov 16, 2004)
  *          1.1 : Ticket 1818 - July 21 2005 -- Corrected a defect that wasn't passing Borrower or property information
  *                                              to the DEA and DEP rules respectively.
  *
  */


package com.basis100.workflow.process;

import java.util.Date;
import java.util.Formattable;
import java.util.Hashtable;
import java.util.Vector;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.aag.AAGuideline;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.commitment.CCM;
import com.basis100.deal.conditions.ConditionHandler;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealNotes;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.entity.WorkflowTrigger;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.SourceFirmProfilePK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.security.ActiveMessage;
import com.basis100.deal.security.DealStatusManager;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.util.DBA;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.WFEExternalTaskCompletion;
import com.basis100.workflow.WorkflowAssistBean;
import com.filogix.externallinks.condition.management.ConditionUpdateRequester;
import com.filogix.externallinks.services.ExchangeFolderCreator;
import com.filogix.externallinks.services.datx.DocCentralInterface;
import com.filogix.util.Xc;

/**
 * @author MMorris
 *
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class DealApprovalProcess
  extends WFProcessBase
{

  private WorkflowAssistBean wfa;

  private ActiveMessage activeMessage;

  private DealHistoryLogger hLog;

  private Hashtable tasks = new Hashtable();

  private final static String DEAL_FOLDER_DOC_CENTRAL_CREATION_FAILED = "Deal Folder creation failed in DocCentral";

  //approval options
  public static final int APPROVE = 0;

  public static final int APPROVE_WITH_CHANGE = 1;

  public static final int APPROVE_REQUEST_EXTERNAL_APPROVAL = 2;

  public static final int APPROVE_EXTERNAL_APPROVAL_GRANTED = 3;

  public DealApprovalProcess()
  {
  }

  public DealApprovalProcess(Deal deal, SessionResourceKit srk)
  {
    super(deal, srk);
  }

  /*
   * (non-Javadoc)
   *
   * @see com.basis100.workflow.process.WFProcessBase#Validate()
   */
  public boolean Validate()
  {

    return false;
  }

  /*
   * (non-Javadoc)
   *
   * @see com.basis100.workflow.process.WFProcessBase#Execute(boolean)
   */
  public int execute(boolean skipValidate)
  {

//tst
    System.out.println("begining at dealapproval " + "underwriter " + deal.getUnderwriterUserId());
    logger.debug("Begin - DealApprovalProcess.Execute");
    wfa = new WorkflowAssistBean();
    hLog = new DealHistoryLogger(srk);

    try
    {

      // Lock the deal first : Return RETRY if failed
      srk.beginTransaction();
      boolean lockResult = placeDealLock();
      srk.commitTransaction();
      if(lockResult == false)
      {
        return RETRY_INTERNAL;
      }

      if(ValidateDeal())
      {

           logger.debug("!!After the deal check!!");

           srk.beginTransaction();
           String msg;

           //==================Set MI Status to
           // Approved======================
           msg = hLog.statusChange(deal.getStatusId(),
             "DealApprovalProcess");
           //Not sure what deal type to use

           //for ticket 951, only deal approved by AutoMI, the MI status will be displayed.
           if(deal.getMIStatusId() == Mc.MI_STATUS_APPROVAL_RECEIVED)
           {
             deal.setMIStatusId(Mc.MI_STATUS_APPROVED);
             deal.ejbStore();
             hLog.log(deal.getDealId(), deal.getCopyId(), msg,
               Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId());
           }

           logger.debug("Status Cat Id = " + deal.getStatusCategoryId());
           //==================Create System
           // Conditions========================
           createSysCond();

           //				==================Set Deal to Recommended
           // Scenerio========================
           deal.setScenarioRecommended("Y");

           srk.commitTransaction();
           executeAAG();

         }else {
             
              WorkflowTrigger trigger = new WorkflowTrigger(srk);
              trigger.setWorkflowTriggerValue(deal.getDealId(), "Auto approve failure", "Y");
              workflowTrigger();        
       }
    }catch(RemoteException e)
    {

      logger.error(StringUtil.stack2string(e));
      return -1;
    }
    catch(FinderException e)
    {

      logger.error(StringUtil.stack2string(e));
      return -1;
    }
    catch(JdbcTransactionException e)
    {

      logger.error(StringUtil.stack2string(e));
      return -1;
    }
    catch(Exception e)
    {

      srk.cleanTransaction();
      logger.error(StringUtil.stack2string(e));
      return -1;
    }
    finally
    {
      // Unlock the deal before return
      try
      {
        srk.beginTransaction();
        unLockDeal();
        srk.commitTransaction();
      }
      catch(Exception ex)
      {
        srk.cleanTransaction();
      }
    }
    logger.debug("End - DealApprovalProcess.Execute");
    return RETRY_INTERNAL;
  }

  public boolean ValidateDeal()
  {

    logger.debug("Begin - DealApprovalProcess.ValidateDeal");
    boolean bolValidDeal = true;
    PassiveMessage pm = new PassiveMessage();
    BusinessRuleExecutor brEx = new BusinessRuleExecutor();
    Vector batchRuleSet = new Vector();

    //Ticket 1818 - Start

    batchRuleSet.add("DI-%");
    batchRuleSet.add("IC-%");
    batchRuleSet.add("UN-%");
    batchRuleSet.add("DE-%");
    batchRuleSet.add("MI-%");

    try
    {

      brEx.BREValidator((DealPK)deal.getPk(), srk, pm, batchRuleSet);
      if(pm.getCritical())
      {

        brEx.close();
        handleExternalApproval(deal.getStatusId());
        bolValidDeal = false;
      }


            //Run the Borrower related rules
            bolValidDeal = runCollectionRelatedRules(brEx, "DEA-%", deal.getBorrowers(), WFProcessBase.BORROWER);
            if (bolValidDeal == false)
                return bolValidDeal;

            //Run Property related rules
            bolValidDeal = runCollectionRelatedRules(brEx, "DEP-%", deal.getProperties(), WFProcessBase.PROPERTY);
            if (bolValidDeal == false)
                return bolValidDeal;

    }
    catch(Exception e)
    {

      e.printStackTrace();
    }
    logger.debug("End - DealApprovalProcess.ValidateDeal - "
      + bolValidDeal);
    //Ticket 1818 - End
    return bolValidDeal;
  }

  public void createSysCond()
  {

    logger.debug("Begin - DealApprovalProcess.createSysCond");
    try
    {
      // check for locked scenario - rebuild conditions only for unlocked
      // scenario
      //boolean editable = false;

      // if Check Rules
      if(deal.getScenarioLocked() == null
        || deal.getScenarioLocked().equals("N"))
      {
        //editable = true;
        //srk.beginTransaction();
        ConditionHandler handler = new ConditionHandler(srk);
        logger
          .debug("BILLY ===> Before Calling buildSystemGeneratedDocumentTracking");

        handler.buildSystemGeneratedDocumentTracking(new DealPK(deal.getDealId(), deal.getCopyId()));
        logger
          .debug("BILLY ===> After Calling buildSystemGeneratedDocumentTracking");
        //srk.commitTransaction();
      }
    }
    catch(Exception e)
    {
      srk.cleanTransaction();
      logger.error("Exception @handleConditionsReview().");
      logger.error(e);
    }
    logger.debug("End - DealApprovalProcess.createSysCond");

  }

  public void executeAAG() throws Exception
  {

    int approvalType = APPROVE;
    logger.trace("--T--> @DealApprovalProcess.executeAAG");

    //May not need this portion, seems to be set from the page view.
    /*
     * if (approvalType < APPROVE || approvalType >
     * APPROVE_EXTERNAL_APPROVAL_GRANTED) {
     * setActiveMessageToAlert(BXResources.getSysMsg(
     * "DEAL_RESOLUTION_APPROVAL_MISSING_APPROVAL_TYPE",
     * theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
     * return; }
     */


    int cs = deal.getStatusId();

    DealStatusManager dsm = DealStatusManager.getInstance(srk);
    int statusCat = dsm.getStatusCategory(cs, srk);

    if(statusCat != Mc.DEAL_STATUS_CATEGORY_DECISION
      || cs == Mc.DEAL_APPROVED)
    {
      logger
        .debug(BXResources
        .getSysMsg(
        "DEAL_RESOLUTION_APPROVAL_OPTION_NOT_ALLOWED_STATUS_CATEGORY",
        0));
    }

    if(approvalType == APPROVE
      && !(cs == Mc.DEAL_EXTERNAL_APPROVAL_REVIEW
      || cs == Mc.DEAL_EXTERNAL_APPROVAL_REQUESTED || cs == Mc.DEAL_APPROVE_WITH_CHANGES))
    {
      AAGuideline aag = new AAGuideline(srk);

      int newCs = aag.pagelessQualifyApproval(srk, deal);
      switch(newCs)
      {
        case -1:

          setActiveMessage(aag.getUserDialog());
          workflowTrigger(2);
          break;
        case Mc.DEAL_APPROVAL_RECOMMENDED:

          //must escalate
          ActiveMessage am = aag.getUserDialog();
          am.setResponseIdentifier("E");
          setActiveMessage(am);

          // save the external task completion object (used if user OKs
          // escalation)
          tasks
            .put("EXTTASKCOMPLETIONOBJ", aag
            .getExtTaskCompletionObj());
          handleApprovalRecommended();
          break;
        case Mc.DEAL_APPROVED:

          runAAGFinalApprovalActivities();
          break;
        case Mc.DEAL_EXTERNAL_APPROVAL_REVIEW:

          handleExternalApproval(deal.getStatusId());
          break;
        default:

          //	 no other valid status codes
          throw new Exception("Unexpected (next) deal status (" + newCs
            + ") returned from AAG, dealId=" + deal.getDealId()
            + ", copyId=" + deal.getCopyId());
      }
    }
    else if(approvalType == APPROVE
      && cs == Mc.DEAL_EXTERNAL_APPROVAL_REVIEW)
    {
      // proceed to final approval
      runAAGFinalApprovalActivities();
    }
    else if(approvalType == APPROVE
      && cs == Mc.DEAL_APPROVE_WITH_CHANGES)
    {
      //// logger.debug("DRH@handleApproval::Stamp3");

      srk.beginTransaction();

      // adopt transactional copy (if one)
      //standardAdoptTxCopy(pg, true);
      deal = DBA.getDeal(srk, deal.getDealId(), deal.getCopyId());
      int oldStatus = deal.getStatusId();
      /**
       * BEGIN FIX FOR FXP-21298
       * Mohan V Premkumar
       * 05/07/2008
       */
      SessionResourceKit aSrk = new SessionResourceKit(srk.getExpressState());
      /**
       * END FIX FOR FXP-21298
       */
      DBA.recordDealStatus(aSrk, deal, Mc.DEAL_REVIEW_APPROVED_CHANGES,
        -1, -1);
      aSrk.freeResources();

      // if status changed ==> write DealHistory logging
      if(oldStatus != deal.getStatusId())
      {
        String msgForStatus = hLog.statusChange(oldStatus,
          "Deal Resolution");
        hLog.log(deal.getDealId(), deal.getCopyId(), msgForStatus,
          Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), deal
          .getStatusId());
      }

      //// SYNCADD.
      // Update Servicing Number based on Parameter setting -- by Billy
      // 10June2002

      //This code portion seems to be related to the page view, so it
      // will be
      //commented out for now. - mIke 2004-11-23
      /*
       * if (isDisplayServicingNumField() == true) { ////String theSrvNum = //
       * getCurrNDPage().getDisplayFieldStringValue("tbServMortgNum");
       * TextField tbTheSrvNum = (TextField) getCurrNDPage()
       * .getDisplayField("tbServMortgNum"); String theSrvNum = (String)
       * tbTheSrvNum.getValue();
       * //logger.debug("DRH@handleApproval::SrvNum: " + theSrvNum);
       *
       * deal.setServicingMortgageNumber(theSrvNum); }
       */
      ////===============================================
      deal.ejbStore();
      workflowTrigger(2);

      // add Broker note -- New requirement -- By Billy 13July2001
      createBrokerNote(srk);
      /*
       * setupForDealNotes(pg, BXResources.getSysMsg(
       * "DEAL_RESOULTION_ENTER_CHANGES", theSessionState
       * .getLanguageId()));
       */
      srk.commitTransaction();
    }
    /*
     * else if (approvalType == APPROVE && cs ==
     * Mc.DEAL_EXTERNAL_APPROVAL_REQUESTED) {
     * setActiveMessageToAlert(BXResources.getSysMsg(
     * "DEAL_RESOLUTION_APPROVAL_EAR_OPTION_REQUIRED", 0),
     * ActiveMsgFactory.ISCUSTOMCONFIRM); return; }
     */

  }

  public void runAAGFinalApprovalActivities() throws Exception
  {

    //Code taken from DealResolutionHandler.finalApprovalActivities
    //srk.beginTransaction();

    CCM ccm = new CCM(srk);

    // -- // Ticket 951, the auto agent should not be displayed on the search screen of deal.
    int finalApproverId = srk.getExpressState().getUserProfileId();

    String msg = " ";

    DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);

    int oldStatus = deal.getStatusId();

    String msgForStatus = "";

    srk.beginTransaction();
    deal.setFinalApproverId(finalApproverId);
    //tst for now
    /**
     * BEGIN FIX FOR FXP-21298
     * Mohan V Premkumar
     * 05/07/2008
     */
    SessionResourceKit aSrk = new SessionResourceKit(srk.getExpressState());
    /**
     * END FIX FOR FXP-21298
     */
    deal = DBA.recordDealStatus(aSrk, deal, Mc.DEAL_APPROVED, -1, -1);
	aSrk.freeResources();
    // Create History Log if Status changed
    if(oldStatus != deal.getStatusId())
    {
      //// logger.debug("DRH@finalApprovalActivities::Stamp5");
      msgForStatus = hLog.statusChange(oldStatus, "Deal Resolution");
      hLog.log(deal.getDealId(), deal.getCopyId(), msgForStatus,
        Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), deal
        .getStatusId());
    }
    // -- // Ticket 951, the auto agent should not be displayed on the search screen of deal.

    CalcMonitor dcm = CalcMonitor.getMonitor(srk);
    deal.setCalcMonitor(dcm);

    msg = hLog.getFinalApproverLogMessage(srk.getExpressState().getUserProfileId(), srk);
    boolean blnAutoMIReq = false;
    int intLanguageId = getLanguageId();
    ccm.issueCommitment(deal, blnAutoMIReq, intLanguageId);
    deal.ejbStore();
    srk.commitTransaction();

    oldStatus = deal.getStatusId();
    srk.beginTransaction();
    if(deal.getSpecialFeatureId() == Mc.SPECIAL_FEATURE_PRE_APPROVAL)
    {

      DBA.recordDealStatus(srk, deal, Mc.DEAL_PRE_APPROVAL_OFFERED);
      // pre-approval ...
      // Create History Log if Status changed
      if(oldStatus != deal.getStatusId())
      {
        msgForStatus = hLog.statusChange(oldStatus, "Deal Resolution");
        hLog.log(deal.getDealId(), deal.getCopyId(), msgForStatus,
          Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), 
          deal.getStatusId());
      }
      hLog.log(deal.getDealId(), deal.getCopyId(), msg,
        Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), 
        deal.getStatusId());
    }
    else
    {

      DBA.recordDealStatus(srk, deal, Mc.DEAL_COMMIT_OFFERED);

      // Create History Log if Status changed
      if(oldStatus != deal.getStatusId())
      {
        msgForStatus = hLog.statusChange(oldStatus, "Deal Resolution");
        hLog.log(deal.getDealId(), deal.getCopyId(), msgForStatus,
          Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), 
          deal.getStatusId());
      }
      hLog.log(deal.getDealId(), deal.getCopyId(), msg,
        Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), 
        deal.getStatusId());
    }
    srk.commitTransaction();

    srk.beginTransaction();
    ////===============================================

    //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
    //--> By Billy 09Jan2004
    //--> Reset Hold Reason if decision made
    deal.setHoldReasonId(0);
    //=============================================================
    deal.ejbStore();
    srk.commitTransaction();
    doCalculation(dcm);

    workflowTrigger(2);

    // add Broker note -- New requirement -- By Billy 13July2001
    //This brker note needs to be determined - mIke.
    createBrokerNote(srk);
    
    //--InsertConditionUpdataRequest -- 03-25-2009--start--//
    logger
    .debug("DRH@finalApprovalActivities::Before insertConditionUpdateRequest");
    ConditionUpdateRequester requester = new ConditionUpdateRequester(srk);
    int result = requester.insertConditionUpdateRequest(deal.getDealId(), false);
    if (result < 0) {
        logger
        .debug("DRH@finalApprovalActivities::Fail insertConditionUpdateRequest");
    }
    logger
    .debug("DRH@finalApprovalActivities::After insertConditionUpdateRequest");
    
    
    //--DocCentral_FolderCreate--21Sep--2004--start--//
    // If lender picks up option create DocCentral deal folder on 'Approval'
    // (Settig in DCCONFIG table) this activity should be processed on
    // success
    // of any previous one.
//    DocCentralInterface dci = new DocCentralInterface(srk);
    ExchangeFolderCreator dci = new ExchangeFolderCreator(srk);
    int fldrIndicator = dci.getFolderCreationIndicator();
    if(fldrIndicator == Mc.CREATE_DOC_CENTRAL_DEAL_FOLDER_ON_APPROVAL)
    {

      String folderIdentificator = createDocCentralFolder(srk, deal, dci);
      logger
        .debug("DRH@finalApprovalActivities::Stamp16_BeforeHitServer");

      if(folderIdentificator
        .equals(DEAL_FOLDER_DOC_CENTRAL_CREATION_FAILED))
      {

        //If the folder creation failed log the creation failure.
        //Not sure what lvl to log this at - mIke.
        logger.error(BXResources.getSysMsg(
          "DEAL_FOLDER_DOC_CENTRAL_CREATION_FAILED", 0));

      }
      else
      {
        String folderMsg = BXResources.getSysMsg(
          "DEAL_FOLDER_DOC_CENTRAL_CREATED", 0);
        folderMsg += folderIdentificator;
        logger.debug(folderMsg);
      }
    }
    //srk.commitTransaction();
  }

  public void doCalculation(CalcMonitor dcm) throws Exception
  {
    try
    {
      if(dcm == null)
      {
        throw new Exception(
          "Exception encountered in doCalculation - monitor not provided");
      }
      dcm.calc();
    }
    catch(Throwable th)
    {
      String report = "Exception @doCalculation:";
      logger.error(report);
      logger.error(th);
      throw new Exception(report);
    }

  }

  protected String createDocCentralFolder(SessionResourceKit srk, Deal deal,
          ExchangeFolderCreator dci)
  {
    String ret = DEAL_FOLDER_DOC_CENTRAL_CREATION_FAILED;
    try
    {
      int folderIndicator = dci.getFolderCreationIndicator();
      String folderIdentifier = "";
      logger.debug("DRH@createDocCentralFolder::FolderIndicator: "
        + folderIndicator);

      if(folderIndicator == Mc.DO_NOT_CREATE_DOC_CENTRAL_DEAL_FOLDER
        || folderIndicator == Mc.CREATE_DOC_CENTRAL_DEAL_FOLDER_ON_SUBMISSION)
      {
        // do nothing, in the second case folder creation is postponed
        // until deal approval.
        logger
          .trace("DRH@createDocCentralFolder::Do not create Doc Central Deal folder");
      }
      else if(folderIndicator == Mc.CREATE_DOC_CENTRAL_DEAL_FOLDER_ON_APPROVAL)
      {
        // Check first whether the folder for this deal is created or
        // not.
        logger
          .trace("DRH@createDocCentralFolder::Create Doc Central Deal folder");

        // Just sanity check, it should not be here at the moment.
        folderIdentifier = "";

        logger.debug("DRH@createDocCentralFolder::DealFoundId: "
          + deal.getDealId());
        logger.debug("DRH@createDocCentralFolder::DealFoundCopyId: "
          + deal.getCopyId());
        logger.debug("DRH@createDocCentralFolder::DealFoundCopyType: "
          + deal.getCopyType());
        logger.debug("DRH@createDocCentralFolder::FolderExists?: "
          + dci.folderExists(deal));

        if(!dci.folderExists(deal))
        {
          folderIdentifier = dci.createFolder(deal);
          String sfpIdentifier = deal.getSourceFirmProfile()
            .getSourceFirmName();
          logger
            .debug("DRH@createDocCentralFolder::sobIdentifierToDocCentral: "
            + sfpIdentifier);

          int sfpId = deal.getSourceFirmProfileId();

          SourceFirmProfile SFPBean = new SourceFirmProfile(srk)
            .findByPrimaryKey(new SourceFirmProfilePK(sfpId));

          logger
            .debug("DRH@createDocCentralFolder::sobIdentifierToDocCentralFromBean: "
            + SFPBean.getSourceFirmName());

          // Create record in DealHistory table and form the return
          // Folder Identifier.
          if(folderIdentifier != null
            && !folderIdentifier.trim().equals(""))
          {
            DealHistoryLogger hLog = DealHistoryLogger
              .getInstance(srk);
            String msg = hLog.docCentralFolderCreationInfo(SFPBean,
              folderIdentifier);
            hLog
              .log(
              deal.getDealId(),
              deal.getCopyId(),
              msg,
              Mc.DEAL_TX_TYPE_DOCCENTRAL_FOLDER_CREATION,
              srk.getExpressState().getUserProfileId(), deal
              .getStatusId());

            ret = folderIdentifier;
          }
          else
          {
            ret = DEAL_FOLDER_DOC_CENTRAL_CREATION_FAILED;
          }
        }
      }
    }
    catch(Exception ex)
    {
      String msg = "DRH@createDocCentralFolder::NOTE: Failed to create folder "
        + " Deal: " + deal.getDealId();
      logger.error(msg);
      //// Add this function later
      ////handler.logDocCentralFolderFailure(srk, beanPK, SFPBean,
      // folderIdentifier);
    }
    logger.debug("DRH@createDocCentralFolder::FolderIdentifier: " + ret);
    return ret;
  }

  public void createBrokerNote(SessionResourceKit srk) throws Exception
  {
    /*
    //Deal note text still needs to be determined - mIke.
    String dealNoteText = "";

    DealPK dealPk = new DealPK(deal.getDealId(), 1);

    // only needed to pass deal id
    DealNotes dealNotes = new DealNotes(srk);
    String noteText = StringUtil.makeQuoteSafe(dealNoteText);
    dealNotes.create(dealPk, deal.getDealId(),
      Sc.DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS, noteText, null, 
      srk.getExpressState().getUserProfileId());
    */
	  
    //bilingual deal notes, rewrite 
    //  Deal note text still needs to be determined - mIke.
    DealPK dealPk = new DealPK(deal.getDealId(), 1);

    DealNotes dealNotes = new DealNotes(srk);
    Formattable newMsg = BXResources.newGenMsg(Xc.AME_DEAL_RESOLUTION_ENTER_APPROVAL);
    dealNotes.create(dealPk, deal.getDealId(), Sc.DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS, srk.getExpressState().getUserProfileId(), newMsg);
	  
  }

  public void handleExternalApproval(int dealStatus) throws Exception
  {

    srk.beginTransaction();

    // adopt transactional copy (if one)
    //standardAdoptTxCopy(pg, true);
    deal = DBA.getDeal(srk, deal.getDealId(), deal.getCopyId());
    int oldStatus = deal.getStatusId();
    /**
     * BEGIN FIX FOR FXP-21298
     * Mohan V Premkumar
     * 05/07/2008
     */
    SessionResourceKit aSrk = new SessionResourceKit(srk.getExpressState());
    /**
     * END FIX FOR FXP-21298
     */
    DBA.recordDealStatus(aSrk, deal, dealStatus, -1, -1);
    aSrk.freeResources();

    // if status changed ==> write DealHistory logging
    if(oldStatus != deal.getStatusId())
    {
      String msgForStatus = hLog.statusChange(oldStatus,
        "Deal Resolution");
      hLog.log(deal.getDealId(), deal.getCopyId(), msgForStatus,
        Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), deal
        .getStatusId());
    }

    CCM ccm = new CCM(srk);
    ccm.lockScenarios(deal);

    ////SYNCADD.
    // Update Servicing Number based on Parameter setting -- by Billy
    // 10June2002
    /*
     * if (isDisplayServicingNumField() == true) { ///String theSrvNum = //
     * getCurrNDPage().getDisplayFieldStringValue("tbServMortgNum");
     *
     * TextField tbTheSrvNum = (TextField) getCurrNDPage()
     * .getDisplayField("tbServMortgNum"); String theSrvNum = (String)
     * tbTheSrvNum.getValue(); //logger.debug("DRH@handleApproval::SrvNum: " +
     * theSrvNum);
     *
     * deal.setServicingMortgageNumber(theSrvNum); }
     */

    deal.ejbStore();
    workflowTrigger(2);

    // add Broker note -- New requirement -- By Billy 13July2001
    createBrokerNote(srk);
    /*
     * setupForDealNotes(pg, BXResources.getSysMsg(
     * "DEAL_RESOULTION_ENTER_APPROVAL_NOTE", theSessionState
     * .getLanguageId()));
     */
    srk.commitTransaction();

  }

  public void handleApprovalRecommended() throws Exception
  {

    srk.beginTransaction();
    deal.setStatusId(Mc.DEAL_APPROVAL_RECOMMENDED);
    deal.ejbStore();

    WFEExternalTaskCompletion wfExt = (WFEExternalTaskCompletion)tasks
      .get("EXTTASKCOMPLETIONOBJ");
    workflowTrigger(0, wfExt);

    //bilingual deal notes, rewrite
/*    newNote = newNote.create((DealPK)(deal.getPk()));
    newNote.setApplicationId(deal.getDealId());
    newNote.setDealNotesCategoryId(Mc.DEAL_NOTES_CATEGORY_DECISIONING);
    newNote.setDealNotesUserId(srk.getExpressState().getUserProfileId());

    // The deal notes text needs to be determined.
    newNote.setDealNotesText("AME approved.");
    newNote.ejbStore();
*/    
    DealNotes newNote = new DealNotes(srk);
    Formattable newMsg = BXResources.newGenMsg(Xc.AME_DEAL_APPROVED);
    newNote.create((DealPK)deal.getPk(),
    		        deal.getDealId(), 
    		        Mc.DEAL_NOTES_CATEGORY_DECISIONING, 
    		        srk.getExpressState().getUserProfileId(), newMsg);

    srk.commitTransaction();
  }

  /**
   * @return Returns the activeMessage.
   */
  public ActiveMessage getActiveMessage()
  {
    return activeMessage;
  }

  /**
   * @param activeMessage
   *            The activeMessage to set.
   */
  public void setActiveMessage(ActiveMessage activeMessage)
  {
    this.activeMessage = activeMessage;
  }

  public void updateDealStatus(Deal aDeal, int status, String msg, int typeId) throws RemoteException, FinderException
  {

    deal.setStatusDate(new Date());
    deal.setStatusId(status);
    deal.ejbStore();
    hLog.log(deal.getDealId(), deal.getCopyId(), msg, typeId, 
             srk.getExpressState().getUserProfileId());
  }

  private int getLanguageId()throws Exception{
    int intLanguageId = 0;
    try{
    int intUnderWriterUserId = deal.getUnderwriterUserId();
    UserProfileBeanPK userProfileKey = new UserProfileBeanPK(intUnderWriterUserId, deal.getInstitutionProfileId());
    UserProfile userProfile = new UserProfile(srk);
    userProfile = userProfile.findByPrimaryKey(userProfileKey);
    Contact contact = userProfile.getContact();
    intLanguageId = contact.getLanguagePreferenceId();
  }catch(Exception ex){
    throw ex;
  }

  return intLanguageId;
}
}
