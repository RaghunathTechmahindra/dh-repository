package com.basis100.workflow.process;

import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.WorkflowTrigger;
import com.basis100.deal.util.StringUtil;
import com.basis100.resources.SessionResourceKit;

public class FundingUploadProcess
  extends WFProcessBase
{

  public FundingUploadProcess()
  {
  }

  public FundingUploadProcess(Deal deal, SessionResourceKit srk)
  {
    super(deal, srk);

  }

  public int execute(boolean skipValidate)
  {

    String strMsg = null;
    try
    {

      if(!skipValidate && Validate() == false)
      {
        return RETRY_INTERNAL;
      }

      // Lock the deal first : Return RETRY if failed
      srk.beginTransaction();
      boolean lockResult = placeDealLock();
      srk.commitTransaction();
      if(lockResult == false)
      {
        return RETRY_INTERNAL;
      }

      srk.beginTransaction();

      DocumentRequest.requestFundingUpload(srk, deal);

      WorkflowTrigger trigger = new WorkflowTrigger(srk);

      //???to degine new key.
      trigger.setWorkflowTriggerValue(deal.getDealId(), WF_TRIGGER_KEY_FUNDING_UPLOAD, "Y");

      // Trigger workflow
      workflowTrigger(2);
      srk.commitTransaction();

    }
    catch(Exception ex)
    {
      strMsg = "Exception encountered in execute() of " + this.getClass().getName() + ", dealId=" +
        deal.getDealId() + ", copyId=" + deal.getCopyId();
      logger.error(strMsg);
      logger.error(StringUtil.stack2string(ex));
      srk.cleanTransaction();
      return ERROR;

    }
    finally
    {
      // Unlock the deal before return
      try
      {
        srk.beginTransaction();
        unLockDeal();
        srk.commitTransaction();
      }
      catch(Exception ex)
      {
        srk.cleanTransaction();
      }
    }

    return RETRY_INTERNAL;
  }

  /**
   * Validate
   *
   * @return boolean
   */
  public boolean Validate()
  {
    return false;
  }

}
