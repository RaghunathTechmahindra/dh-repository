package com.basis100.workflow.process;

import MosSystem.Mc;

public class MIDenialSwitch extends MIDenialBase{
	
	
	protected void determineMIInsurer()throws Exception{
		
		int intMI = deal.getMortgageInsurerId();
        if(intMI == Mc.MI_INSURER_CMHC)
        {
          deal.setMortgageInsurerId(Mc.MI_INSURER_GE);
        }
        else
        {
          deal.setMortgageInsurerId(Mc.MI_INSURER_CMHC);
        }
	}
	
}
