/*
 * Created on Aug 22, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.basis100.workflow.process;

import MosSystem.Mc;

import com.basis100.deal.ame.AMEConstants;
import com.basis100.deal.entity.Deal;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;

/**
 * @author MMorris
 * 
 * This class Extends from the BaseAutoMIProcess, and slightly
 * changes the way large loans are handled.
 */
public class AutoMIProcessLL extends BaseAutoMIProcess
{

    
    public AutoMIProcessLL()
    {
    }
    
    /**
     * @param deal
     * @param srk
     */
    public AutoMIProcessLL(Deal deal, SessionResourceKit srk)
    {
        super(deal, srk);       
    }
    
    protected void setMIInsurer() throws Exception
    {

        logger.debug(this.getClass() + ".setMIInsurer - loanValue = " + deal.getNetLoanAmount());
        //For this method the insurer should alwasy be CMHC, this should be set in the ame.properties file.
        //srk.beginTransaction();        
        deal.setMortgageInsurerId(((Integer) insurerMapping.get(PropertiesCache
                .getInstance().getProperty(deal.getInstitutionProfileId(),
                        AMEConstants.PROPERTY_DEFAULT_INSURER, INSURER_CMHC)
                .trim())).intValue());
        deal.setMIIndicatorId(Mc.MII_APPLICATION_FOR_LOAN_ASSESSMENT);
        deal.ejbStore();
    }
}
