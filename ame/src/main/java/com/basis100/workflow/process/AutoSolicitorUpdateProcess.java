/*
 * Created on Dec 24, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.basis100.workflow.process;

import java.util.Date;

import MosSystem.Mc;

import com.basis100.deal.commitment.CCM;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.PartyProfile;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;

/**
 * @author MMorris
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class AutoSolicitorUpdateProcess
  extends WFProcessBase
{

  public AutoSolicitorUpdateProcess()
  {
  }

  /**
   * @param deal
   * @param kit
   */
  public AutoSolicitorUpdateProcess(Deal deal, SessionResourceKit srk)
  {

    super(deal, srk);
  }

  /*
   * (non-Javadoc)
   *
   * @see com.basis100.workflow.process.WFProcessBase#Validate()
   */
  public boolean Validate()
  {
    // TODO Auto-generated method stub
    return false;
  }

  /*
   * (non-Javadoc)
   *
   * @see com.basis100.workflow.process.WFProcessBase#Execute(boolean)
   */
  public int execute(boolean skipValidate)
  {

    SessionResourceKit srk = getSessionResourceKit();

    int dealId = deal.getDealId();

    DocumentRequest SolDocReq = null;
    try
    {
      //--> Billy to Mike : 05Jan2005
      // Lock the deal first : Return RETRY if failed
      srk.beginTransaction();
      boolean lockResult = placeDealLock();
      srk.commitTransaction();
      if(lockResult == false)
      {
        return RETRY_INTERNAL;
      }

      // Check if Solicitor Assigned
      boolean isSolicitorAssigned = false;
      try
      {
        PartyProfile partyProfile = new PartyProfile(srk);
        // no logging if finder fails - not considered an error
        partyProfile = partyProfile.findByDealSolicitor(dealId);
        if(partyProfile != null)
        {
          isSolicitorAssigned = true;
        }
      }
      catch(Exception e)
      {
        // Solicitor not found !!
        isSolicitorAssigned = false;
      }

      if(isSolicitorAssigned == false)
      {
        //--> Billy to Mike :
        // TODO : do something to trigger the Manual Schedule Closing task
        // - May need to use the WorkFlowTriggers for this.  Please work together with Brant and Derek.
        // --> By Billy 04Jan2005
        return RETRY_INTERNAL;
      }
      //=====================================================================

      logger.trace("--T-- @ClosingActivity -- updateForClosing Line 1.1");
      String flag = deal.getInstructionProduced();
      boolean alreadyInstructed = (flag == null) ? false : ((flag + "N")
        .substring(0, 1).equals("Y"));

      /*if (regeneratePackageOk == false && alreadyInstructed == true)
                   {
          setActiveMessageToAlert(BXResources.getSysMsg(
                  "CLOSING_ACTIVITY_SOLICITOR_PACKAGE", theSessionState
                          .getLanguageId()),
                  ActiveMsgFactory.ISCUSTOMDIALOG,
                  INSTRUCTION_PRODUCED_YES_MSG);

          //--Ticket#607--09Sep2004--start--//
          return;
          //--Ticket#607--09Sep2004--end--//
                   }*/

      if(alreadyInstructed == false)
      {
        PassiveMessage pm = runAllClosingActivityBusinessRules(srk);
        if(pm != null)
        {
          pm.setGenerate(true);
          //getTheSessionState().setPasMessage(pm);

          logger.debug("@ClosingActivitySubmit: before allowsoftstop");
          logger.debug("@ClosingActivitySubmit:GetCritical? "
            + pm.getCritical());

          ////SYNCADD
          //Check if allowed th continue if softstop errors -- By
          // BILLY 13June2002
          if(pm.getCritical() == true
            || PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
            "com.basis100.deal.closing.allowsoftstops",
            "N").equals("N"))
          {
            logger.debug("@ClosingActivitySubmit:I am returning");
            return -1;
          }
        }
      }
      logger.debug("@ClosingActivitySubmit: before Solisitor package starts");

      // call corresponding Processing Module (Solicitor Package).
      srk.beginTransaction();
      //standardAdoptTxCopy(pg, true);
      SolDocReq = DocumentRequest.requestSolicitorsPackage(srk, deal);

      // New requirement from Product to issue Eznet doc as well -- BILLY
      // 05Sept2001
      // Modified to add parameter to control the generation of the Ezenet
      // upload -- BILLY 20Feb2002
      if((PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
        "com.basis100.system.generateezenetupload", "Y"))
        .equals("Y"))
      {
        DocumentRequest.requestEzenet(srk, deal);
      }
      int statusId = deal.getStatusId();
      if(statusId == Mc.DEAL_CONDITIONS_OUT)
      {
        statusId = Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_OUT;
      }
      else if(statusId == Mc.DEAL_CONDITIONS_SATISFIED)
      {
        statusId = Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_SATISFIED;
      }
      if(statusId != deal.getStatusId())
      {
        //Add log to deal history -- Billy 16May2001
        DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);
        String msg = hLog.statusChange(deal.getStatusId(),
          "Closing Activity");
        deal.setStatusId((short)statusId);
        deal.setStatusDate(new Date());
        hLog.log(deal.getDealId(), deal.getCopyId(), msg,
          Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), deal
          .getStatusId());
      }
      //deal.setSolicitorSpecialInstructions(instr);
      deal.setInstructionProduced("YES");

      logger.debug("@ClosingActivitySubmit: before servicing upload");
      // Check and send Servicing Upload -- Modified by BILLY 09Nov2001
      CCM ccm = new CCM(srk);
      ccm.sendServicingUpload(srk, deal,
        Mc.SERVICING_UPLOAD_TYPE_CLOSING, 0);
      deal.ejbStore();

      logger.debug("@ClosingActivitySubmit: before triggerWorkflow");

      // Change to call the workflow with parameter 2 (previously 0) to
      // check for new task
      //  -- Billy 29Nov2001
      workflowTrigger(2);

      logger.debug("@ClosingActivitySubmit: after triggerWorkflow");

      srk.commitTransaction();
      logger.debug("@ClosingActivitySubmit: before return");
      return RETRY_EXTERNAL;
    }
    catch(Exception e)
    {
      srk.cleanTransaction();
      logger.error("Exception @ClosingActivityHandler.handleSubmit");
      logger.error(e);
    }
    finally
    {
      // Unlock the deal before return
      try
      {
        srk.beginTransaction();
        this.unLockDeal();
        srk.commitTransaction();
      }
      catch(Exception ex)
      {
        srk.cleanTransaction();
      }
    }

    return RETRY_EXTERNAL;
  }

  public PassiveMessage runAllClosingActivityBusinessRules(SessionResourceKit srk)
  {
    PassiveMessage pm = new PassiveMessage();
    BusinessRuleExecutor brExec = new BusinessRuleExecutor();

    try
    {

      brExec.BREValidator((DealPK)deal.getPk(), srk, pm, "IS-%");
      logger.trace("--T--> @ClosingActivityHandler.runAllClosingActivityBusinessRules: IS rules");
      if(pm.getNumMessages() > 0)
      {
        return pm;
      }
    }
    catch(Exception e)
    {
      logger.error("Problem encountered during funding, @ClosingActivityHandler.runAllClosingActivityBusinessRules");
      logger.error(e);
      logger.error(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", 0));

    }
    return null;
  }
}
