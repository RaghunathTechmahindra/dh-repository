/*
 * Created on Sep 19, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.basis100.workflow.process;

import com.basis100.deal.ame.AMEConstants;
import com.basis100.deal.entity.Deal;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;

/**
 * @author MMorris
 * 
 * This class was introduced to accomodate the new MI functionality
 * of the large loan logic, or the original MI functionality.
 */
public class MIProcessFactory
{

    private static MIProcessFactory instance = new MIProcessFactory();
private double defaultLargeLoan = new Double(PropertiesCache.getInstance()
            .getProperty(-1, AMEConstants.PROPERTY_LARGE_LOAN, "450000"))
            .doubleValue();
    private String env = PropertiesCache.getInstance().getProperty(-1, 
            "com.filogix.environment", "N");
    
    
    private MIProcessFactory()
    {
       
    }

    public static MIProcessFactory getInstance()
    {

        return instance;
    }

    public WFProcessBase getProcess(Deal aDeal, SessionResourceKit anSrk)
    {

        double loanValue = aDeal.getNetLoanAmount();
      
        boolean bolValue = AMEConstants.EXPRESS_ENV_CV.equalsIgnoreCase(env)
                && loanValue > defaultLargeLoan;
 
        //If the defaultLargeLoan value > 0, and the loan value is greater than the default large loan 
        //then we return the process that can handle the Large Loan logic.
        return bolValue == true ? new AutoMIProcessLL(aDeal, anSrk)
                : new BaseAutoMIProcess(aDeal, anSrk);
    }
}
