package com.basis100.workflow.process;

import com.basis100.deal.entity.Deal;
import com.basis100.resources.SessionResourceKit;

public class MIDenialProcess
  extends WFProcessBase
{

  public MIDenialProcess()
  {
  }

  public MIDenialProcess(Deal deal, SessionResourceKit srk)
  {
    super(deal, srk);
    // Override the logger
  }

  public int execute(boolean skipValidate)
  {
  
	  return getProcess().execute(false);
  }

  private WFProcessBase getProcess(){
	  
	  return MIDenialFactory.getInstance().getProcess();
  }
  
  /**
   * Validate
   *
   * @return boolean
   */
  public boolean Validate()
  {
    return true;
  }

}
