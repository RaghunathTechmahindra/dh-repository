package com.basis100.workflow.process;

public class TestProcess extends WFProcessBase
{
  public TestProcess()
  {
  }

  /**
   * Execute
   *
   * @param skipValidate boolean
   * @return int
   */
  public int execute(boolean skipValidate)
  {
    return RETRY_INTERNAL;
  }

  /**
   * Validate
   *
   * @return boolean
   */
  public boolean Validate()
  {
    return true;
  }
}
