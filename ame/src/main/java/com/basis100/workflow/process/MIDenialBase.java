package com.basis100.workflow.process;

import MosSystem.Mc;

import com.basis100.deal.ame.AMEConstants;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.util.StringUtil;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;

public abstract class MIDenialBase extends WFProcessBase {

	public MIDenialBase() {
	}

	public MIDenialBase(Deal deal, SessionResourceKit srk) {
		super(deal, srk);		
	}

	public int execute(boolean skipValidate) {

		try {
			// --> Billy to Ruth : should lock the deal first
			// --> 05Jan2005
			// Lock the deal first : Return RETRY if failed
			srk.beginTransaction();
			boolean lockResult = placeDealLock();
			srk.commitTransaction();
			if (lockResult == false) {
				return RETRY_INTERNAL;
			}

			if (!skipValidate && Validate() == false) {
				return RETRY_INTERNAL;
			}

			double dblCombinedLTV = deal.getCombinedLTV();
			double combinedLTVThreshold = new Double(PropertiesCache.getInstance().
	        		getProperty(deal.getInstitutionProfileId(), AMEConstants.PROPERTY_LTV_THRESHOLD, AMEConstants.DEFAULT_LTV_THRESHOLD));
			if (dblCombinedLTV < combinedLTVThreshold) {
				workflowTrigger(2);
			} else {

				determineMIInsurer();

				srk.beginTransaction();

				// --> Billy to Ruth : set Deal Status to Recived (1) in order
				// to trigger the Auto Deal Decision Task again (task 86)
				// aDeal.setMIStatusId(Mc.MI_STATUS_DENIED);
				deal.setStatusId(Mc.DEAL_RECEIVED);
				deal.ejbStore();

				workflowTrigger(2);

				srk.commitTransaction();

			}

		} catch (Exception ex) {
			String msg = "Exception encountered @MIDenialProcess.execute(): DealId = "
					+ deal.getDealId() + ",copyId=" + deal.getCopyId();
			logger.error(msg);
			logger.error(StringUtil.stack2string(ex));
			srk.cleanTransaction();
			return ERROR;

		} finally {
			// Unlock the deal before return
			try {
				srk.beginTransaction();
				this.unLockDeal();
				srk.commitTransaction();
			} catch (Exception ex) {
				srk.cleanTransaction();
			}
		}

		return RETRY_INTERNAL;

	}

	abstract protected void determineMIInsurer() throws Exception;

	

	/**
	 * Validate
	 * 
	 * @return boolean
	 */
	public boolean Validate() {
		return true;
	}

}
