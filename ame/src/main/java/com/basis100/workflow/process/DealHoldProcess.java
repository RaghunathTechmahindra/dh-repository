
// 06/June/2007 SL bilingual deal notes project

package com.basis100.workflow.process;

import com.filogix.externallinks.framework.ServiceConst;

import java.util.Formattable;
import java.util.HashMap;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.docprep.Dc;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealNotes;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.WorkflowTrigger;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.validation.DocBusinessRuleExecutor;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.WFTaskExecServices;
import com.filogix.express.email.EmailSender;
import com.filogix.util.Xc;

public class DealHoldProcess extends WFProcessBase
{
    //bilingual deal notes, move this message to System Message
    //private final static String TEXT_NOTE = "Thank you for your submission, your application is currently being reviewed by the Mortgage Insurance provider";

    private MIStatusMapper mapper = new MIStatusMapper();
    private int intHoldHrs = 1;

    public DealHoldProcess()
    {

    }

    public DealHoldProcess(Deal deal, SessionResourceKit srk)
    {
        super(deal, srk);

    }

    /**
     * Execute For Cervus phase I this method will be executed when a new deal
     * is ingested in the WorkFlow nitification handler. In the future, PhaseII
     * this will be called in in the Auto Agent when it processing the AutoMI
     * task.
     *
     * @param skipValidate
     *            boolean
     * @return int : indicator to tell AME on how to handle Retry RETRY_INTERNAL
     *         (0) -- Retry handle internally, AME should call
     *         AssignedTask.setForRetry(true) to increment Retry Counter
     *         RETRY_EXTERNAL (1) -- Retry handle externally, AME should call
     *         AssignedTask.setForRetry(false) to not increment Retry Counter
     *         EXPIRE_RETRY (2) -- Retry handle externally, AME should call
     *         AssignedTask.setRetryExpired(false) to stop retry and escalate
     *         ERROR (-1) -- Process executed with Fatal Errors, AME should Log
     *         the error and set to retry as usual
     */
    public int execute(boolean skipValidate)
    {
        String msg = null;
        long timeToHoldInSecs = 0;

        try
        {

            if (!skipValidate && Validate() == false)
            {
                return RETRY_INTERNAL;
            }

            // Lock the deal first : Return RETRY if failed
            srk.beginTransaction();
            boolean lockResult = placeDealLock();
            srk.commitTransaction();
            if (lockResult == false)
            {
                return RETRY_INTERNAL;
            }

            if (!isRecommendedScenerioCreated())
            {

                deal.setMIStatusId(mapper.getStatus(deal.getMIStatusId()));
                return RETRY_INTERNAL;
            } else
            {

                int intDealId = deal.getDealId();
                int intCopyId = deal.getCopyId();
                int intUserProfileId = srk.getExpressState().getUserProfileId();
                SysLogger logger = srk.getSysLogger();
                DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);
                logger.trace("Inside handleHold()---HOLDFORDAYS: noOfHrs = "
                        + intHoldHrs);
                timeToHoldInSecs = intHoldHrs * 60 * 60;
                logger
                        .trace("Inside handleHold()---HOLDFORDAYS: timeToHoldInSecs = "
                                + timeToHoldInSecs);
                srk.beginTransaction();
                String strMsgForLog = "Deal was held for " + intHoldHrs
                        + " hours  ";
                hLog.log(intDealId, intCopyId, strMsgForLog,
                        Mc.DEAL_TX_TYPE_EVENT, intUserProfileId);
                srk.commitTransaction();
                // notify workflow engine to extend open tasks
                WFTaskExecServices tes = new WFTaskExecServices(srk,
                        intUserProfileId, intDealId, intCopyId, AUTO_USER_TYPE);
                srk.beginTransaction();
                tes.extendOpenTasks(false, srk, intDealId, -1, deal.getInstitutionProfileId(), null,
                        timeToHoldInSecs, -1, -1, -1);
                createBrokerNote(intDealId, srk);
                if (PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
                        "com.basis100.deal.hold.sendpendingmsg", "Y").equals(
                        "Y"))
                {
                	//DA-DEC-UC-5/7 starts
                	//DocumentRequest.requestPendingMessage(srk, deal);
                	try {
                		MasterDeal masterDeal = new MasterDeal(srk, null, deal.getDealId());
                		if (masterDeal.getFXLinkSchemaId() == MasterDeal.FXLINKSCHEMA_FCX_V_1_0) {

                			//FXP27305, Nov 23, 2009: check doc business rule for holding -- start
                			DocBusinessRuleExecutor dbre = new DocBusinessRuleExecutor(srk);
                			if(dbre.evaluateRequest(deal, 
                					Dc.DOCUMENT_GENERAL_TYPE_PENDING_MESSAGE).isCreate()){

                				ESBOutboundQueue queue = new ESBOutboundQueue(srk);
                				queue.create(queue.createPrimaryKey(), 
                						ServiceConst.SERVICE_TYPE_LOANDECISION_UPDATE, deal.getDealId(), 
                						deal.getSystemTypeId(), srk.getExpressState().getUserProfileId());
                				queue.setServiceSubTypeid(ServiceConst.SERVICE_SUBTYPE_LOANDECISION_HOLD);
                				queue.ejbStore();
                			}
                			//FXP27305, Nov 23, 2009: check doc business rule for holding -- end
                		} else {
                			DocumentRequest.requestPendingMessage(srk, deal);
                		}
                	} catch (Exception e) {
                		EmailSender.sendAlertEmail("ame", e);
                		throw e;
                	}
                	//DA-DEC-UC-5/7 ends
                }
                WorkflowTrigger trigger = new WorkflowTrigger(srk);

                trigger.findByCompositeKey(deal.getDealId(), "Auto-Hold", "Y");

                deal.setHoldReasonId(Mc.DEAL_HOLD_REASON_OTHERS);

                logger.trace("Current Status MI Status - "
                        + deal.getMIStatusId());
                deal.setMIStatusId(mapper.getStatus(deal.getMIStatusId()));

                deal.ejbStore();
                // Trigger workflow
                workflowTrigger(2);
                srk.commitTransaction();
            }
        } catch (Exception e)
        {
            msg = "Exception encountered @DealResolution.handleHold(): timeToHoldInSecs="
                    + timeToHoldInSecs
                    + ", dealId="
                    + deal.getDealId()
                    + ", copyId=" + deal.getCopyId();
            logger.error(msg);
            logger.error(StringUtil.stack2string(e));
            srk.cleanTransaction();
            return ERROR;

        } finally
        {
            // Unlock the deal before return
            try
            {
                srk.beginTransaction();
                this.unLockDeal();
                srk.commitTransaction();
            } catch (Exception ex)
            {
                srk.cleanTransaction();
            }
        }

        return RETRY_INTERNAL;
    }

    private boolean isDisplayServicingNumField()
    {
        return (PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
                "com.basis100.deal.approval.displayservnumfield", "N")
                .equals("Y"));
    }

    private void createBrokerNote(int i_intDealId, SessionResourceKit srk)
            throws Exception
    {
        /*
        int userId = srk.getExpressState().getUserProfileId();

        DealPK dealPk = new DealPK(i_intDealId, 1);
        // only needed to pass deal id
        DealNotes dealNotes = new DealNotes(srk);
        String txt = getLocale() == Locale.CANADA ? TEXT_NOTE : " ";
        dealNotes = dealNotes.create(dealPk, i_intDealId,
                Sc.DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS, txt, null, userId);
        */

        //bilingual deal notes, rewrite
        DealPK dealPk = new DealPK(i_intDealId, 1);
        DealNotes dealNotes = new DealNotes(srk);
        Formattable newMsg = BXResources.newGenMsg(Xc.AME_DEAL_HOLD);
        dealNotes.create(dealPk, deal.getDealId(), Sc.DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS, srk.getExpressState().getUserProfileId(), newMsg);

    }

    /**
     * Validate
     *
     * @return boolean
     */
    public boolean Validate()
    {
        return false;
    }

    class MIStatusMapper
    {

        private HashMap mapper;

        public MIStatusMapper()
        {

            mapper = new HashMap();
            init();
        }

        private void init()
        {

            mapper.put(new Integer(9), new Integer(10));
            mapper.put(new Integer(19), new Integer(20));
            mapper.put(new Integer(3), new Integer(4));
            mapper.put(new Integer(5), new Integer(6));
        }

        public int getStatus(int status)
        {

            Integer miStatus = new Integer(status);
            if (!mapper.containsKey(miStatus))
                return status;
            return ((Integer) mapper.get(miStatus)).intValue();
        }
    }

}
