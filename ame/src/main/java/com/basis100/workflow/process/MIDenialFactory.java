package com.basis100.workflow.process;

import java.util.HashMap;

import com.basis100.deal.ame.AMEConstants;
import com.basis100.resources.PropertiesCache;

public class MIDenialFactory {

	private static MIDenialFactory instance = new MIDenialFactory();

	// aatcha starts
	private String bolAutoAssign = "";
	private HashMap mapping = new HashMap(); 
	
	
	private MIDenialFactory(){
		
		bolAutoAssign = PropertiesCache.getInstance().getProperty(-1, 
				AMEConstants.PROPERTY_BALANCE_MODE, "N");
		
		mapping.put("Y", new MIDenialBalancer());
		mapping.put("N", new MIDenialSwitch());		
	}
	

	// aatcha ends

	public static synchronized MIDenialFactory getInstance() {

		return instance;
	}

	public WFProcessBase getProcess() {

		return (WFProcessBase) mapping.get(bolAutoAssign);		
	}
}
