package com.basis100.workflow.process;

import com.basis100.deal.entity.Deal;
import com.basis100.resources.SessionResourceKit;

/**
 * <p>
 * Title: AutoMIProcess
 * </p>
 * 
 * <p>
 * Description: Process to Validate and send MI request.
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * 
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author Billy Lam
 * @version 1.0 (Initial Version - 31 May 2004)
 */
public class AutoMIProcess extends WFProcessBase
{

    public AutoMIProcess()
    {
    }
    
    public AutoMIProcess(Deal deal, SessionResourceKit srk)
    {
        
        super(deal, srk);     
    }

    /**
     * Execute For Cervus phase I this method will be executed when a new deal
     * is ingested in the WorkFlow nitification handler. In the future, PhaseII
     * this will be called in in the Auto Agent when it processing the AutoMI
     * task.
     * 
     * @param skipValidate
     *            boolean
     * @return int : indicator to tell AME on how to handle Retry RETRY_INTERNAL
     *         (0) -- Retry handle internally, AME should call
     *         AssignedTask.setForRetry(true) to increment Retry Counter
     *         RETRY_EXTERNAL * (1) -- Retry handle externally, AME should call
     *         AssignedTask.setForRetry(false) to not increment Retry Counter
     *         EXPIRE_RETRY (2) -- Retry handle externally, AME should call
     *         AssignedTask.setRetryExpired(false) to stop retry and escalate
     *         ERROR (-1) -- Process executed with Fatal Errors, AME should Log
     *         the error and set to retry as usual
     */
    public int execute(boolean skipValidate)
    {

        return getProcess().execute(skipValidate);
    }

    public WFProcessBase getProcess()
    {

        return MIProcessFactory.getInstance().getProcess(deal, srk);
    }
    
        
    /*
       * (non-Javadoc)
       * 
       * @see com.basis100.workflow.process.WFProcessBase#Validate()
       */

    public boolean Validate()
    {
        // TODO Auto-generated method stub
        return false;
    }
}