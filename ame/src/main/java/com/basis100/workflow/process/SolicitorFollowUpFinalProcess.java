package com.basis100.workflow.process;

import MosSystem.Mc;

import com.basis100.deal.docprep.Dc;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.WorkflowTrigger;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.util.StringUtil;

public class SolicitorFollowUpFinalProcess
  extends WFProcessBase
{
  public SolicitorFollowUpFinalProcess()
  {
  }

  /**
   * Execute
   *
   * @param skipValidate boolean
   * @return int
   */
  public int execute(boolean skipValidate)
  {

    String useBusRules = proCache.getProperty(srk.getExpressState().getDealInstitutionId(),DOCUMENT_CREATE_BY_BUSINESS_RULES, NO);
    String strMSG = MESSAGE_GO_THROUGH_BUSINESS_RULE + "," + MESSAGE_DEAL_ID +
      deal.getDealId() + "," + MESSAGE_COPY_ID + deal.getCopyId();
    try
    {
      if(!skipValidate && Validate() == false)
      {
        return RETRY_INTERNAL;
      }
      srk.beginTransaction();
      boolean lockResult = placeDealLock();
      srk.commitTransaction();
      if(lockResult == false)
      {
        return RETRY_INTERNAL;
      }

      if(!useBusRules.equalsIgnoreCase(YES))
      {
        logger.error(strMSG);
        return ERROR;

      }

      DocumentRequest.requestDocByBusRules(srk, deal, Dc.DOCUMENT_GENERAL_TYPE_SOL_FOLLOW_UP_2, null);
      WorkflowTrigger finalTrig = new WorkflowTrigger(srk);
      finalTrig.setWorkflowTriggerValue(deal.getDealId(), "FinalReport2", "Y");
      
      hLog = DealHistoryLogger.getInstance(srk);
	    hLog.log(deal.getDealId(), deal.getCopyId(), 
	             "Solicitor Follow-Up Final Report 2 produced, DocTracking", 
	             Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId());
      
      workflowTrigger(2);
    }
    catch(Exception ex)
    {
      strMSG = MESSAGE_REQUEST_DO_BY_BUSINESS_RULE + "," + MESSAGE_DEAL_ID +
        deal.getDealId() + "," + MESSAGE_COPY_ID + deal.getCopyId();
      logger.error(strMSG);
      logger.error(StringUtil.stack2string(ex));
      return ERROR;

    }
    finally
    {
      // Unlock the deal before return
      try
      {
        srk.beginTransaction();
        this.unLockDeal();
        srk.commitTransaction();
      }
      catch(Exception ex)
      {
        srk.cleanTransaction();
      }
    }

    return RETRY_INTERNAL;

  }

  /**
   * Validate
   *
   * @return boolean
   */
  public boolean Validate()
  {
    return false;
  }
}
