/*
 * @(#)AMEManager.java     Feb 14, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.ame;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.security.DLM;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.util.thread.ThreadPool;
import com.basis100.workflow.entity.AssignedTask;

/**
 * For this version, AMEManager has only one instance running in jvm. AMEManager
 * does configuration new threadpool and start the thread of AMEWork.
 */
public class AMEManager extends Thread {

    private ThreadPool threadPool;
    private PropertiesCache propertiesCache;
    private SessionResourceKit srk;
    private String strUserId;
    private String strPassword;
    private int intUserProfileId;
    private int userInstitutionId;
    
    private AMESignInService signInService;
    
    private UserProfile uf;

    // the counter of retry for the assignedtask.
    private int intRetryCounter;

    // the wait time for the thread of AMEManager
    private String strWaitTime;
    private CalcMonitor calMonitor;

    public AMEManager() throws Exception {
        init();
    }

    /**
     * To config the system.
     * 
     * @param i_strPath
     *            String
     * @throws AMELoginException
     * @throws Exception
     */
    private void init() throws AMELoginException, Exception {

        try {
            // --> In order to simplify the config of the AME, we need to share
            // the Main Properties file (Specified by passing VM Parameter -D)
            // --> and then load the AME specific Properties (Passed as VM input
            // parameter) on top of the Main Properties to override.
            // --> By Billy 23Nov2004
            // Load the Main Peoperties file first

            srk = new SessionResourceKit(
                    AMEConstants.IDENTITY_CLASS_NAME_AMEMANAGER);
            propertiesCache = PropertiesCache.getInstance();
            signInService = (AMESignInService) ExpressAMEContext.getService("SignInService");            

            strUserId = propertiesCache.getProperty(-1, AMEConstants.PROPERTY_USER_ID, "");
            strPassword = propertiesCache.getProperty(-1, AMEConstants.PROPERTY_PASSWORD, "");
 
            uf = signInService.login(strUserId, strPassword);
            
            intUserProfileId = uf.getUserProfileId();
            userInstitutionId = uf.getInstitutionId();

            String strThreadPoolSize = propertiesCache.
                getInstanceProperty(AMEConstants.PROPERTY_THREADPOOL_SIZE,
                                    "2");
            String strRetryCounter = propertiesCache.
                getInstanceProperty(AMEConstants.PROPERTY_RETRY_COUNTRER,
                                    "5");
            strWaitTime = propertiesCache.
                getInstanceProperty(AMEConstants.PROPERTY_AMEMANAGER_INTERVAL,
                                    "5");

            intRetryCounter = TypeConverter.intTypeFrom(strRetryCounter);
            // ResourceManager init should be perform first !!
            ResourceManager.init();

            // should init the intUserProfileId as well!!

            int intThreadPoolSize = TypeConverter.intTypeFrom(strThreadPoolSize);
            PicklistData.init();
            DLM.init(srk, false);
            calMonitor = CalcMonitor.getMonitor(srk);
            threadPool = new ThreadPool(intThreadPoolSize);

            // Here should clean up all the tasks :
            // if(VisibleFlag == AssignedTask.INPROGRESS and UserId =
            // this.intUserProfileId)
            // set VisibleFlag = AssignedTask.VISIBLE
            resetOutstandingTasks(srk);

            // Add the Hook to the VM to perform Logout if VM terminate
            // Getting the runtime object associated with the current Java
            // application.
            Runtime runTime = Runtime.getRuntime();
            // Initiating the shutdown hook
            AMEShutdownHook hook = new AMEShutdownHook(strUserId);
            // Registering the Shutdown Hook
            runTime.addShutdownHook(new Thread(hook));
            // =======================================================================
        } catch (Exception ex) {
            srk.getSysLogger().error(ex.getMessage());
            srk.getSysLogger().error("VPD = " + srk.getActualVPDStateForDebug());
            srk.getSysLogger().error(StringUtil.stack2string(ex));
            new Exception(this.getClass().getName()
                    + AMEConstants.INEDNTITY_METHOD_NAME_INIT + ex.getMessage());
        }

    }

    /**
     * in the run method, to get the active tasks. if no active tasks exiting,
     * the current thread will wait for the waittime and realse the lock. if
     * active tasks exits, set to invisible for this task. Add the new thread of
     * AMEWorker into the thread pool.
     */
    public void run() {
        int intWaitTime = new Integer(strWaitTime).intValue();
        long lngWaitTime = intWaitTime * 1000;
        SysLogger logger = srk.getSysLogger();
        try {

            for (;;) {
                // synchronized(this)
                // {
                srk.getExpressState().cleanAllIds();
                AssignedTask assignedTask = getActiveTask();
                if (assignedTask == null) {
                    // To free the resouce to release the JDBC connection.
                    srk.getExpressState().cleanAllIds();
                    srk.freeResources();
                    sleep(lngWaitTime);

                } else {
                    // --> Billy ==> Ruth : Need to put more Info message on the
                    // log file, here is an example.
                    // --> 22Nov2004
                    logger.info(
                            "========== One AutoTask found ==> TaskLabel = "
                                    + assignedTask.getTaskLabel()
                                    + " AssignedTaskId = "
                                    + assignedTask.getAssignedTasksWorkQueueId()
                                    + " institutionProfileId = "
                                    + assignedTask.getInstitutionProfileId()
                                    + " ==========");
                    // =======================================================================================
                    try {
                    	/**
                    	 * BEGIN FIX FOR FXP-21298
                         * Mohan V Premkumar
                         * 05/07/2008
                         */
                        srk.getExpressState().setUserIds(intUserProfileId, userInstitutionId);
                        /**
                         * END FIX FOR FXP-21298
                         */                        
                        srk.getExpressState().setDealIds(
                                assignedTask.getDealId(),
                                assignedTask.getInstitutionProfileId(), 0);
                        srk.beginTransaction();
                        assignedTask.setVisibleFlag(AssignedTask.INPROGRESS);
                        assignedTask.ejbStore();
                        srk.commitTransaction();
                    } catch (Exception ex) {
                        logger.error( this.getClass().getName() 
                                + AMEConstants.ERROR_MESSAGE_AMEMANAGER_EXCEPTION_FROM_SETVISIBLEFLAG);
                        srk.cleanTransaction();
                    } finally {
                        srk.getExpressState().cleanAllIds();
                        srk.freeResources();
                    }
					/**
                     * BEGIN FIX FOR FXP-21298
                     * Mohan V Premkumar
                     * 05/07/2008
                     */
                    AMEWorker ameWork = new AMEWorker(
                            assignedTask.getAssignedTasksWorkQueueId(),
                            assignedTask.getInstitutionProfileId(), intUserProfileId );
                    /**
                     * END FIX FOR FXP-21298
                     */ 
                    threadPool.addRequest(ameWork);
                    // --> Billy ==> Ruth : Need to put more Info message on the
                    // log file, here is an example.
                    // --> 22Nov2004
                    logger.info(
                            "========== AutoTask was assigned to Worker ==> TaskLabel = "
                            + assignedTask.getTaskLabel()
                            + " AssignedTaskId = "
                            + assignedTask.getAssignedTasksWorkQueueId()
                            + " institutionProfileId = "
                            + assignedTask.getInstitutionProfileId()
                            + " ==========");
                    // =======================================================================================

                }

            } // end for
        }
        // }

        // --> **Billy ==> Ruth : Consider and try to catch InterruptedException
        // in order to perform Logout.
        // --> 22Nov2004
        catch (Exception ex) {

            logger.error(AMEConstants.ERROR_MESSAGE_AMEMANAGER_EXCEPTION_FROM_RUN_METHOD_OF_AMEMANAGER);
            logger.error("VPD = " + srk.getActualVPDStateForDebug());
            logger.error(StringUtil.stack2string(ex));

            try {
                // Perform Logout
                logout();
            } catch (Exception e) {
                logger.error(AMEConstants.ERROR_MESSAGE_AMEMANAGER_EXCEPTION_USER_LOGOU);
                logger.error(StringUtil.stack2string(e));
            }
        }
    }

    /**
     * To get the active task associated with the auto user.
     * 
     * @throws Exception
     * @return AssignedTask
     */
    public AssignedTask getActiveTask() throws Exception {

        AssignedTask assignedTsk = new AssignedTask(srk);
        assignedTsk =
            assignedTsk.findNextAutoTask(intUserProfileId, intRetryCounter);
        return assignedTsk;
    }

    /**
     * To get userProfile based on the userId.
     * 
     * @param i_strUserId
     *            String
     * @param i_srk
     *            SessionResourceKit
     * @throws Exception
     * @return UserProfile
     */

    private void logout() throws Exception {
        signInService.logout(strUserId);
    }

    private void resetOutstandingTasks(SessionResourceKit i_srk)
            throws Exception {
        try {
            AssignedTask assignedTsk = new AssignedTask(i_srk);

            i_srk.beginTransaction();
            int intAffected = assignedTsk
                    .resetOutstandingTasks(this.intUserProfileId);
            i_srk.getSysLogger().info(
                    "AMEMabager.resetOutstandingTasks : " + intAffected
                            + " tasks affected.");
            i_srk.commitTransaction();
        } catch (Exception ex) {
            i_srk.cleanTransaction();
            throw ex;
        } finally {
            i_srk.freeResources();
        }
    }

    /**
     * <p>
     * Title: AMEShutdownHook
     * <p>
     * Description: The Shutdown Hook implementation for AME. This class will be
     * executed when system shutdown. The main purpose is to perform Auto User
     * Logout and any resource release if necessary.
     * </p>
     * Copyright: Copyright (c) 2004
     * Company: Filogix Inc.
     * </p>
     * 
     * @author Billy Lam
     * @version 1.0
     */
    public class AMEShutdownHook implements Runnable {
        private String userLoginId;

        // this is the shutdown hook thread
        public AMEShutdownHook(String userLoginId) {
            this.userLoginId = userLoginId;
            System.out.println("*** Shutdown Hook init ***");
        }

        /*
         * run method of shutdown hook thread, to display message when started
         */
        public void run() {
            System.out.println("*** Shutdown Hook Started ***");
            try {
                signInService.logout(userLoginId);
            } catch (Exception e) {
                System.out
                        .println("*** Shutdown Hook Exception encounted !! ***");
                System.out.println(e);
            }
            System.out.println("*** Shutdown Hook Ended ***");
        }
    }
    
    /**
     * This main method will start the AME engine.
     * 
     * @param args
     *            String[]: path of moss properties file
     * @throws Exception:
     *             if the exception thrown, the program will stop.
     */
    public static void main(String[] args) throws Exception {
        try {
            // create a new AMEManager and pass the path of moss properties to
            // it.
            AMEManager aM = new AMEManager();
            // start a thread of AMEManager
            aM.start();
        } catch (AMELoginException ex) {
            System.out.println(AMEConstants.ERROR_MESSAGE_MAIN_USER_NOT_LOGIN);
            throw new Exception(ex.getMessage());
        } catch (Exception ek) {
            System.out.println(ek.getMessage());
            throw new Exception(ek.getMessage());
        }
    }
}
