/*
 * @(#)ExpressAMEContext.java     Feb 14, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */


package com.basis100.deal.ame;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.context.ExpressContextDirector;
import com.filogix.express.context.ExpressServiceException;
import com.filogix.express.core.ExpressCoreContext;

/**
 */
public class ExpressAMEContext extends ExpressCoreContext {

    // the key to get a service factory for web context.
    private static final String EXPRES_AME_CONTEXT_KEY = "com.filogix.express.ame";
    
    // The logger
    private final static Log _log = LogFactory.getLog(ExpressAMEContext.class);

    /**
     * returns a service instance from Express web context.
     * 
     * @param name
     *            the service name.
     * @return an instance of the service.
     * @throw ExpressServiceException if failed to create an instance for the
     *        service.
     */
    synchronized public static Object getService(String name)
            throws ExpressServiceException {

        return ExpressContextDirector.getDirector().getServiceFactory(
                EXPRES_AME_CONTEXT_KEY).getService(name);
    }
}
