package com.basis100.deal.ame;

/**
 *
 * <p>AMEConstants: </p>
 * <p>Description: this class contains all constants used in the AME engine</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>filogix: </p>
 * @author: Ruth Zhou
 * @version 1.0
 */
public class AMEConstants
{
public static final String ALERT_MESSAGE_FOR_SENDING_EMAIL="Alert email has been sent for this deal: ";
 public static final String ERROR_MESSAGE_NO_PATH_NAME = "No path is defined";
 public static final String ERROR_MESSAGE_MAIN_USER_NOT_LOGIN = "User can not login!";
 public static final String ERROR_MESSAGE_RETURN_ERROR_AFTER_PROCESS = "Return error after processing.";
 public static final String ERROR_MESSAGE_AMEWORKER_NO_RECOMMENDED_COPY_FOUND_DEALID="No Recommended Copy found: Dealid=";
 public static final String ERROR_MESSAGE_AMEWORKER_FALLBACK_TO_USE_GOLD_COPYID=": Fallback to use Gold copyid";
 public static final String ERROR_MESSAGE_AMEMANAGER_EXCEPTION_FROM_SETVISIBLEFLAG="exception from setVisibleFlag()";
 public static final String ERROR_MESSAGE_AMEMANAGER_EXCEPTION_FROM_RUN_METHOD_OF_AMEMANAGER="exception from run() of AMEManager";
 public static final String ERROR_MESSAGE_AMEMANAGER_EXCEPTION_USER_LOGOU="exception thrown from logout method";
 public static final String IDENTITY_CLASS_NAME_AMEMANAGER = "AMEManager";
 public static final String IDENTITY_CLASS_NAME_AMESIGNINSERVICE = "AMESigninService";
 public static final String IDENTITY_CLASS_NAME_AMEMAIN = "AMEMain";
 public static final String INEDNTITY_METHOD_NAME_INIT = "init()";
 public static final String IDENTITY_METHOD_NAME_RUN = "run()";
 public static final String IDENTITY_PACKAGE_NAME_WORKFLOW_PROCESS="com.basis100.workflow.process.";
 public static final String FALSE = "false";
 public static final String TRUE = "true";
 public static final String YES = "Y";
 public static final String NO = "N";

 public static final String PROPERTY_THREADPOOL_SIZE = "com.basis100.deal.ame.threadpool.size";
 public static final String PROPERTY_USER_ID = "com.basis100.deal.ame.user.id";
 public static final String PROPERTY_PASSWORD = "com.basis100.deal.ame.password";
 public static final String PROPERTY_PASSWORDENCRYTED= "com.basis100.useradmin.passwordencrypted";
 public static final String PROPERTY_RETRY_COUNTRER = "com.basis100.workflow.maxautotaskretry";
 public static final String PROPERTY_AMEMANAGER_INTERVAL = "com.basis100.deal.ame.AMEManager.interval";
 public static final String PROPERTY_BALANCE_MODE = "com.filogix.autoAssignMI";
 public static final int USER_PROFILE_STATUS_ACTIVE = 0;
 public static final String AME_SQL_FIND_ACTIVE_TASKS = "SELECT.ACTIVE.TASK.BY.USERPROFILEID";
 public static final String EMAIL_SENT_TO_FOR_NO_PROCESS_CLASS="com.basis100.deal.ame.email.to";
 public static final String EMAIL_SUBJECT_FOR_NO_PROCESS_CLASS="com.basis100.deal.ame.email.subject";
 public static final String EMAIL_TEXT_FOR_NO_PROCESS_CLASS="NO PROCESS CLASS SET UP IN THE DB.";
 public static final String MEASSAGE_TASK_ID = "This task: ";
 public static final String DEFAULT_THREAD_POOL_SIZE = "2";
 public static final String DEFAULT_RETRY_COUNTER = "5";
 public static final String DEFAULT_THEARD_WAIT_TIME = "5";
 public static final String	PROPERTY_DEFAULT_INSURER = "com.basis100.deal.ame.default.insurer";
 public static final String	PROPERTY_LARGE_LOAN = "com.basis100.deal.ame.large.loan";
 public static final String EXPRESS_ENV_CV = "CV";
 public static final String EXPRESS_ENV_NONE = "N";
 
 public static final String PROPERTY_LTV_THRESHOLD = "com.filogix.ingestion.LTV.threshold";
 public static final String DEFAULT_LTV_THRESHOLD = "80";

}
