/*
 * @(#)AMESignInService.java     Feb 14, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.ame;

import java.util.Calendar;
import java.util.Date;

import MosSystem.Sc;

import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.security.DLM;
import com.basis100.deal.security.PasswordEncoder;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;

public class AMESignInService {
    
    private static SessionResourceKit srk; 
    
    public AMESignInService() {
        srk = new SessionResourceKit(
                AMEConstants.IDENTITY_CLASS_NAME_AMESIGNINSERVICE);
    }      

    public void setSessionResourceKit(SessionResourceKit srk) {
        this.srk = srk;
    }
    
    /**
     * In order to resue the login serivce later, the method is made to static.
     * 
     * @param i_strUserId
     *            String : User id.
     * @param i_strPw
     *            String : password
     * @throws Exception
     * @return boolean
     */
    public void logout(String i_strUserId) throws Exception {
        try {
            UserProfile userProfile = getUserProfile(i_strUserId, srk);
            int intUserProfielId = userProfile.getUserProfileId();
            DLM dlm = DLM.getInstance();
            srk.beginTransaction();
            dlm.unlockAnyByUser(intUserProfielId, srk, true);
            // userProfile.setLoginFlag(AMEConstants.NO);
            // userProfile.setLastLogoutTime(new Date());
            userProfile.ejbStore();
            srk.commitTransaction();
        } catch (Exception ex) {
            srk.cleanTransaction();
            throw ex;
        } finally {
            srk.freeResources();
        }
    }

    public UserProfile getUserProfile(String i_strUserId,
            SessionResourceKit i_srk) throws Exception {
        UserProfile userProfile = new UserProfile(i_srk);
        userProfile.findByLoginId(i_strUserId);
        return userProfile;
    }

    public UserProfile login(String strUserId, String i_strPw)
            throws Exception {
        try {

            UserProfile userProfile = getUserProfile(strUserId, srk);
            int intProfileStatusId = userProfile.getProfileStatusId();

            // user status has to be active.
            if (intProfileStatusId == AMEConstants.USER_PROFILE_STATUS_ACTIVE) {
                // to check if the password is locked?
                if (!userProfile.isPasswordLocked()) {
                    String strPw = i_strPw;
                    if (PropertiesCache.getInstance().getInstanceProperty(
                            AMEConstants.PROPERTY_PASSWORDENCRYTED,
                            // if(PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(),
                            // AMEConstants.PROPERTY_PASSWORDENCRYTED,
                            AMEConstants.YES).equals(AMEConstants.YES)) {

                        strPw = PasswordEncoder.getEncodedPassword(strPw);

                    }
                    // to check if the password from the properties file is
                    // matched to the password retrived from db.
                    if (userProfile.getPassword() != null
                            && userProfile.getPassword().equals(strPw)) {
                        //Calendar calendar = Calendar.getInstance();
                        //Date dteCurrent = calendar.getTime();
                        srk.beginTransaction();
                        userProfile
                                .setPasswordFailedAttempts(Sc.PASSWORD_FAILED_ATTEMPTS_TRUE);
                        // userProfile.setLoginFlag(AMEConstants.YES);
                        // userProfile.setLastLoginTime(dteCurrent);
                        userProfile.ejbStore();
                        srk.commitTransaction();
                        return userProfile;
                    } 
                } 
            }
        } catch (Exception ex) {
            srk.cleanTransaction();
            throw ex;
        } finally {
            srk.freeResources();
        }
        throw new AMELoginException();
    }
}
