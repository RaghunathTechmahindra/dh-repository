/*
 * @(#)AMELoginException.java     Feb 14, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.ame;

public class AMELoginException extends Exception {
    public AMELoginException() {
        super();
    }

    public AMELoginException(String message) {
        super(message);
    }
}
