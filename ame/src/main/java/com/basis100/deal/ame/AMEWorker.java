/*
 * @(#)AMEWorker.java     Feb 14, 2008
 * 
 * Copyright (C) 2008 Filogix Limited Partnership. All rights reserved.
 */
package com.basis100.deal.ame;

import com.basis100.deal.entity.Deal;
import com.basis100.workflow.entity.AssignedTask;
import com.basis100.workflow.pk.AssignedTaskBeanPK;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.pk.DealPK;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collection;
import com.basis100.workflow.entity.WorkflowTaskProcessAssoc;
import com.basis100.workflow.process.WFProcessBase;
import com.basis100.resources.*;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.log.SysLogger;
import com.basis100.deal.util.StringUtil;

/**
 * <p>
 * AMEWorker will have mutiple threads running in the threadpool AMEWorker will
 * find the deal based on the taskId of the assignedtask object and pass the
 * property process class to process. After executing the execute method of the
 * process class, based on the different return value, the assigned task object
 * will be updated.
 * </p>
 */
public class AMEWorker implements Runnable {
    private static PropertiesCache propertiesCache;
    public final static int RETRY_INTERNAL = 0;
    public final static int RETRY_EXTERNAL = 1;
    public final static int EXPIRE_RETRY = 2;
    public final static int COMPLETE = 3;
    public final static int ERROR = -1;

    static {
        propertiesCache = PropertiesCache.getInstance();
    }

    private int assignedTasksWorkQueueId;
    private SessionResourceKit srk;
    private String strEmailTo;
    private String strEmailSubj;
    private StringBuffer strBufferEmailText = new StringBuffer(500);
    private int dealInstitutionId = -1;
    
    /**
     * AMEWorker has independent SessionResourceKit. 
     *   
     * @param i_intAssignedTasksWorkQueueId
     * @param i_intDealInstitutionId
     */
    public AMEWorker(int i_intAssignedTasksWorkQueueId, int i_intDealInstitutionId, int userProfileId) {
        srk = new SessionResourceKit(String.valueOf(i_intAssignedTasksWorkQueueId));
        
        strEmailTo = propertiesCache.getInstanceProperty(
                AMEConstants.EMAIL_SENT_TO_FOR_NO_PROCESS_CLASS);
        strEmailSubj = propertiesCache.getInstanceProperty(
                AMEConstants.EMAIL_SUBJECT_FOR_NO_PROCESS_CLASS);
        strBufferEmailText.append(AMEConstants.EMAIL_TEXT_FOR_NO_PROCESS_CLASS);

        assignedTasksWorkQueueId = i_intAssignedTasksWorkQueueId;
        dealInstitutionId = i_intDealInstitutionId;
//        this.userProfileId = userProfileId;
        /**
         * BEGIN FIX FOR FXP-21298
         * Mohan V Premkumar
         * 05/07/2008
         */
        srk.getExpressState().setUserIds(userProfileId, dealInstitutionId);
        srk.getExpressState().setDealInstitutionId(dealInstitutionId);
        /**
         * END FIX FOR FXP-21298
         */
    }

    // ================================================================

    /**
     * In the run method, AMEworker will find the process class and deal
     * associated with the task and pass the deal to the process class to
     * process. Base on the different return value to update the assigned task
     * object.
     */
    public void run() {
        
        //you can see this name with %t in log4j
        Thread.currentThread().setName("QID=" + assignedTasksWorkQueueId);
        
        SysLogger logger = srk.getSysLogger();
        try {
            //ML : Apr 14, 2008
            //Setting VPD first, in order not to select irrelevant tasks.
            //AMEManager creates AMEWorker�s instance for every single 
            //assignedTask, so AMEWorker�s thread is virtually thread safe.
            
            AssignedTask assignedTask = getAssignedTask(assignedTasksWorkQueueId);
            int intTaskId = assignedTask.getTaskId();
            ArrayList alWftProcessAssoc = (ArrayList) getProcessAssoco(intTaskId);
            int intDealId = assignedTask.getDealId();
            Deal aDeal = getDeal(intDealId);

            // --> Billy ==> Ruth : Replace the SysLogger with the DealID as
            // Identifier
            // --> 22Nov2004
            srk.setSysLogger(ResourceManager.getSysLogger("AME" + intDealId));
            // ========================================================================

            Iterator itWftProcessAssoc = alWftProcessAssoc.iterator();
            WorkflowTaskProcessAssoc entityWftProcessAssoc = null;
            String strProcessName = null;
            WFProcessBase wfProcess = null;

            // --> Billy ==> Ruth : Should handle to setup for retry after all
            // process(es) executed.
            // --> 16Nov2004
            int finalResult = WFProcessBase.ERROR;

            while (itWftProcessAssoc.hasNext()) {
                entityWftProcessAssoc = (WorkflowTaskProcessAssoc) itWftProcessAssoc
                        .next();
                strProcessName = entityWftProcessAssoc.getProcessName().trim();
                // tst
                // strProcessName = null;
                if (strProcessName == null || strProcessName.equals("")) {
                    strBufferEmailText.append(AMEConstants.MEASSAGE_TASK_ID);
                    strBufferEmailText.append(intTaskId);
                    strBufferEmailText.append(AMEConstants.EMAIL_TEXT_FOR_NO_PROCESS_CLASS);
                    DocumentRequest.requestAlertEmail(srk, aDeal, strEmailTo,
                            strEmailSubj, strBufferEmailText.toString());
                    logger.error( this.getClass().getName() + AMEConstants.ALERT_MESSAGE_FOR_SENDING_EMAIL + aDeal.getDealId());
                } else {
                    StringBuffer strBufferPCName = new StringBuffer(500);
                    strBufferPCName
                            .append(AMEConstants.IDENTITY_PACKAGE_NAME_WORKFLOW_PROCESS);
                    strBufferPCName.append(strProcessName);

                    // Added debug message -- By Billy 12Jan2005
                    logger.trace(
                            "<======= Begin execute : " + strBufferPCName
                            + " assignedTasksWorkQueueId : " + assignedTasksWorkQueueId
                            + " dealInstitutionId : " + dealInstitutionId
                            + " dealId : " + intDealId
                            + " ========>");

                    wfProcess = (WFProcessBase) Class.forName(
                            strBufferPCName.toString()).newInstance();
                    wfProcess.setDeal(aDeal);
                    wfProcess.setSessionResourceKit(srk); // same institution as it was in AssignedTask.
                    int intResult = wfProcess.execute(true);

                    // Added debug message -- By Billy 12Jan2005
                    logger.trace(
                            "<======= End execute : " + strBufferPCName
                            + " assignedTasksWorkQueueId : " + assignedTasksWorkQueueId
                            + " dealInstitutionId : " + dealInstitutionId
                            + " dealId : " + intDealId
                            + " ========>");

                    if (intResult == ERROR || intResult > finalResult) {
                        finalResult = intResult;
                    }
                }
            }

            // --> Billy to Ruth : should handle this after all processes
            // executed
            // --> 12Jan2005
            // Added debug message -- By Billy 12Jan2005
            logger.trace(
                    "<======= All Process(es) executed for "
                    + " assignedTasksWorkQueueId : " + assignedTasksWorkQueueId
                    + " dealInstitutionId : " + dealInstitutionId
                    + " dealId : " + intDealId
                    + " Final Result : " + finalResult
                    + " ========>");

            srk.beginTransaction();

            // --> Billy to Ruth : Bug fix : Should refresh the AssignedTask
            // Entity in case the
            // --> the task was set to completed with in the Process(es)
            // --> 12Jan2005
            assignedTask = getAssignedTask(assignedTasksWorkQueueId);

            // 0 means retry internal
            if (finalResult == RETRY_INTERNAL) {
                assignedTask.setForRetry(true);
                assignedTask.ejbStore();
            }
            // 1 means retry external
            else if (finalResult == RETRY_EXTERNAL) {
                assignedTask.setForRetry(false);
                assignedTask.ejbStore();
            } // 2 means expire retry
            else if (finalResult == EXPIRE_RETRY) {
                assignedTask.setRetryExpired();
                assignedTask.ejbStore();
            }
            // 3 mean task completed.
            else if (finalResult == COMPLETE) {
                assignedTask.setTaskStatusId(COMPLETE);
                assignedTask.ejbStore();
            } else if (finalResult == WFProcessBase.ERROR) { 
                // -1 means error. System logs the error message and set retry to true.
                srk.getSysLogger().error(this.getClass().getName()
                        + AMEConstants.ERROR_MESSAGE_RETURN_ERROR_AFTER_PROCESS);
                assignedTask.setForRetry(true);
                assignedTask.ejbStore();
            }
            srk.commitTransaction();
        } catch (Exception ex) {
            logger.error(this.getClass().getName()
                    + AMEConstants.IDENTITY_METHOD_NAME_RUN + ex.getMessage());
            logger.error("VPD = " + srk.getActualVPDStateForDebug());
            logger.error(StringUtil.stack2string(ex));
            srk.cleanTransaction();

            // Need to retry for this suitation
            // --> It is necessary otherwise the Visible flag will state -2 and
            // the Auto Task will never retry
            // --> By Billy 12Jan2005
            try {
                this.srk.beginTransaction();
                AssignedTask assignedTask = getAssignedTask(assignedTasksWorkQueueId);
                assignedTask.setForRetry(true);
                assignedTask.ejbStore();
                this.srk.commitTransaction();
            } catch (Exception ex2) {
                // Not much we can do then...
                logger.error(StringUtil.stack2string(ex2));
            }

        } finally {
            srk.getExpressState().cleanAllIds();
            srk.freeResources();
            srk = null;
        }

    }

    /**
     * To get the process class associated with the assignedtask object.
     * 
     * @param i_intTaskId
     *            int
     * @throws Exception
     * @return Collection
     */
    private Collection getProcessAssoco(int i_intTaskId) throws Exception {
        WorkflowTaskProcessAssoc wftProcessAssoc = new WorkflowTaskProcessAssoc(srk);
        return wftProcessAssoc.findByTaskId(i_intTaskId);
    }

    /**
     * to get the assignedtasks.
     * 
     * @param assignedTasksWorkQueueId
     *            int
     * @throws Exception
     * @return AssignedTask
     */
    public AssignedTask getAssignedTask(int assignedTasksWorkQueueId)
            throws Exception {
        AssignedTask assignedTask = new AssignedTask(srk);
        AssignedTaskBeanPK pk = new AssignedTaskBeanPK(assignedTasksWorkQueueId);
        assignedTask.findByPrimaryKey(pk);
        return assignedTask;
    }

    /**
     * get the deal associated with the assignedtask.
     * 
     * @param i_intDealId
     *            int
     * @throws Exception
     * @return Deal
     */
    private Deal getDeal(int i_intDealId) throws Exception {
        CalcMonitor calc = CalcMonitor.getMonitor(srk);
        Deal aDeal = new Deal(srk, calc);
        DealPK dealPk = new DealPK(i_intDealId, -1);
        try {
            aDeal.findByRecommendedScenario(dealPk, true);
        } catch (Exception ex) {
            srk.getSysLogger().warning( 
                    AMEConstants.ERROR_MESSAGE_AMEWORKER_NO_RECOMMENDED_COPY_FOUND_DEALID 
                    + i_intDealId 
                    + AMEConstants.ERROR_MESSAGE_AMEWORKER_FALLBACK_TO_USE_GOLD_COPYID);
            srk.getSysLogger().warning(StringUtil.stack2string(ex));
            aDeal = aDeal.findByGoldCopy(i_intDealId);

        }
        return aDeal;
    }
}
