#!/bin/sh
#
# final element to AMECOMMAND string needs to be "target name" element from ame.xml
#
#==================================================================

PIDFILE=./ame.pid
AMECOMMAND="java -classpath . LauncherBootstrap -launchfile ame.xml ame"

#=================================================================

case "$1" in
  start)
    if [ -f $PIDFILE ]; then
      PID=`cat $PIDFILE`
      echo "AME process seems already be running under PID $PID"
      echo "(PID file $PIDFILE already exists). Check for process..."
      echo `ps -f -p $PID`
      exit 3
    else
      echo "Starting AME... "
      $AMECOMMAND &
      PID=$!
      echo $PID > $PIDFILE
    fi
  ;;
  stop)
    if [ ! -f $PIDFILE ]; then
      echo "PID file $PIDFILE not found, unable to determine process to kill..."
      exit 3
    else
      echo "Stopping AME... "
      PID=`cat $PIDFILE`
      kill -9 $PID
      rm -f $PIDFILE
      echo "AME Stopped."
    fi
  ;;
  restart)
    $0 stop
    $0 start
  ;;
  *)
    echo ""
    echo "AME Control"
    echo "-------------"
    echo "Syntax:"
    echo "  $0 {start|stop|restart}"
    echo ""
    exit 3
  ;;
esac
exit 0
