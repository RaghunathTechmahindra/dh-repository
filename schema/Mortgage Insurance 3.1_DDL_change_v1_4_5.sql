--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
------- CREATE new tables
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- create tenure table
CREATE TABLE TENURETYPE (
  TENURETYPEID NUMBER(2) DEFAULT 0 NOT NULL PRIMARY KEY,
  TENURETYPEDESCRIPTION VARCHAR2(35) NULL
);

-- create producttype table
CREATE TABLE PRODUCTTYPE (
  PRODUCTTYPEID NUMBER(2) DEFAULT 0 NOT NULL PRIMARY KEY,
  PRODUCTTYPEDESCRIPTION VARCHAR2(35) NULL
);

-- add LOCrepaymenttype table
CREATE TABLE LOCREPAYMENTTYPE (
  LOCREPAYMENTTYPEID NUMBER(2) DEFAULT 0 NOT NULL PRIMARY KEY,
  LOCREPAYMENTTYPEDESCRIPTION VARCHAR2(35) NULL
);


--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
------- INSERT into lookup tables
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
DELETE FROM TENURETYPE;
DELETE FROM PRODUCTTYPE;
DELETE FROM LOCREPAYMENTTYPE;

INSERT INTO TENURETYPE VALUES (0, '');
INSERT INTO TENURETYPE VALUES (1, 'Freehold');
INSERT INTO TENURETYPE VALUES (2, 'Leasehold');
INSERT INTO TENURETYPE VALUES (3, 'Condo');

INSERT INTO PRODUCTTYPE VALUES(0, '');
INSERT INTO PRODUCTTYPE VALUES(1, 'Mortgage');
INSERT INTO PRODUCTTYPE VALUES(2, 'Secured LOC');

INSERT INTO LOCREPAYMENTTYPE VALUES(0, '');
INSERT INTO LOCREPAYMENTTYPE VALUES(1, '5/20');
INSERT INTO LOCREPAYMENTTYPE VALUES(2, '10/15');

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
------- POPULATE
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
DELETE FROM PROGRESSADVANCETYPE WHERE PROGRESSADVANCETYPEID = 4;
DELETE FROM PROPERTYEXPENSETYPE WHERE PROPERTYEXPENSETYPEID = 6;

-- use this update sql only if the deal table had 4 or 5 in Mitype
-- UPDATE DEAL SET MITYPEID = 0 WHERE MITYPEID = 4 OR MITYPEID = 5;

-- MORTGAGEINSURANCETYPEID = 4 and 5 will be just deleted
DELETE FROM MORTGAGEINSURANCETYPE WHERE MORTGAGEINSURANCETYPEID IN (4,5,9,10,11,12,13,14,15,16,17,18);

-------------------------------------------------------------------------
-- ADD DATA : PROGRESSADVANCETYPE
-------------------------------------------------------------------------
INSERT INTO PROGRESSADVANCETYPE (PROGRESSADVANCETYPEID,PROGRESSADVANCETYPEDESC) VALUES (4,'Residential Home Builder');

-------------------------------------------------------------------------
-- ADD DATA : PROPERTYEXPENSETYPE
-------------------------------------------------------------------------
INSERT INTO PROPERTYEXPENSETYPE (PROPERTYEXPENSETYPEID,PETDESCRIPTION,GDSINCLUSION,TDSINCLUSION) VALUES (6,'Site Rent/Lease',100,100);

-------------------------------------------------------------------------
-- ADD DATA : MORTGAGEINSURANCETYPE
-------------------------------------------------------------------------
UPDATE MORTGAGEINSURANCETYPE SET MTDESCRIPTION = '50/50 Partial Coverage' WHERE MORTGAGEINSURANCETYPEID = 6;

UPDATE MORTGAGEINSURANCETYPE SET MTDESCRIPTION ='Cash Back' WHERE MORTGAGEINSURANCETYPEID = 7;

UPDATE MORTGAGEINSURANCETYPE SET MTDESCRIPTION = 'CMHC Flex Down' WHERE MORTGAGEINSURANCETYPEID = 8; 
       
UPDATE MORTGAGEINSURANCETYPE SET MTDESCRIPTION = 'Alt A Upfront Premium' WHERE MORTGAGEINSURANCETYPEID = 9; 
       
INSERT INTO MORTGAGEINSURANCETYPE (MORTGAGEINSURANCETYPEID,MTDESCRIPTION)
       VALUES (10,'Borrowed Down');
INSERT INTO MORTGAGEINSURANCETYPE (MORTGAGEINSURANCETYPEID,MTDESCRIPTION)
       VALUES (11,'CreditAssist');
INSERT INTO MORTGAGEINSURANCETYPE (MORTGAGEINSURANCETYPEID,MTDESCRIPTION)
       VALUES (12,'CreditAssist for Self-Employed');
INSERT INTO MORTGAGEINSURANCETYPE (MORTGAGEINSURANCETYPEID,MTDESCRIPTION)
       VALUES (13,'Family Plan');
INSERT INTO MORTGAGEINSURANCETYPE (MORTGAGEINSURANCETYPEID,MTDESCRIPTION)
       VALUES (14,'Foreign Employee');
INSERT INTO MORTGAGEINSURANCETYPE (MORTGAGEINSURANCETYPEID,MTDESCRIPTION)
       VALUES (15,'Staff application');
INSERT INTO MORTGAGEINSURANCETYPE (MORTGAGEINSURANCETYPEID,MTDESCRIPTION)
       VALUES (16,'Spousal Buyout');
INSERT INTO MORTGAGEINSURANCETYPE (MORTGAGEINSURANCETYPEID,MTDESCRIPTION)
       VALUES (17,'Vacation');
INSERT INTO MORTGAGEINSURANCETYPE (MORTGAGEINSURANCETYPEID,MTDESCRIPTION)
       VALUES (18,'Alt A Monthly');

-------------------------------------------------------------------------
-- UPDATE DATA : MORTGAGEINSURER
-------------------------------------------------------------------------
UPDATE MORTGAGEINSURER
SET    MORTGAGEINSURERNAME = 'Genworth'
WHERE  MORTGAGEINSURERID = 2;

-------------------------------------------------------------------------
-- UPDATE DATA : FEE
-------------------------------------------------------------------------
UPDATE FEE
SET    FEEDESCRIPTION = 'Genworth Fee',
       FEEBUSINESSID  = 'Genworth Fee'
WHERE  FEEID          = 15;
UPDATE FEE
SET    FEEDESCRIPTION = 'Genworth Fee Payable',
       FEEBUSINESSID  = 'Genworth Fee'
WHERE  FEEID          = 16;
UPDATE FEE
SET    FEEDESCRIPTION = 'Genworth Premium',
       FEEBUSINESSID  = 'Genworth Premium'
WHERE  FEEID          = 17;
UPDATE FEE
SET    FEEDESCRIPTION = 'Genworth Premium Payable',
       FEEBUSINESSID  = 'Genworth Premium'
WHERE  FEEID          = 18;
UPDATE FEE
SET    FEEDESCRIPTION = 'Genworth PST On Premium',
       FEEBUSINESSID  = 'Genworth PST'
WHERE  FEEID          = 19;
UPDATE FEE
SET    FEEDESCRIPTION = 'Genworth PST On Premium Payable',
       FEEBUSINESSID  = 'Genworth PST'
WHERE  FEEID          = 20;

-------------------------------------------------------------------------
-- UPDATE DATA : FEETYPE
-------------------------------------------------------------------------
UPDATE FEETYPE
SET    FEETYPEDESCRIPTION = 'Genworth Fee'
WHERE  FEETYPEID          = 15;
UPDATE FEETYPE
SET    FEETYPEDESCRIPTION = 'Genworth Fee Payable'
WHERE  FEETYPEID          = 16;
UPDATE FEETYPE
SET    FEETYPEDESCRIPTION = 'Genworth Premium'
WHERE  FEETYPEID          = 17;
UPDATE FEETYPE
SET    FEETYPEDESCRIPTION = 'Genworth Premium Payable'
WHERE  FEETYPEID          = 18;
UPDATE FEETYPE
SET    FEETYPEDESCRIPTION = 'Genworth PST On Premium'
WHERE  FEETYPEID          = 19;
UPDATE FEETYPE
SET    FEETYPEDESCRIPTION = 'Genworth PST On Premium Payable'
WHERE  FEETYPEID          = 20;

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
------- MODIFY tables
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

-- add fields to deal table.
ALTER TABLE DEAL ADD (
  PRODUCTTYPEID NUMBER(2) DEFAULT 0 NOT NULL REFERENCES PRODUCTTYPE
);
ALTER TABLE DEAL ADD (PROGRESSADVANCEINSPECTIONBY CHAR NULL);
ALTER TABLE DEAL ADD (SELFDIRECTEDRRSP CHAR NULL);
ALTER TABLE DEAL ADD (CMHCPRODUCTTRACKERIDENTIFIER VARCHAR2(5) NULL);
ALTER TABLE DEAL ADD (
  LOCREPAYMENTTYPEID NUMBER(2) DEFAULT 0 NOT NULL 
  REFERENCES LOCREPAYMENTTYPE
);
ALTER TABLE DEAL ADD (REQUESTSTANDARDSERVICE CHAR NULL);
ALTER TABLE DEAL ADD (LOCAMORTIZATIONMONTHS NUMBER(3) NULL);
ALTER TABLE DEAL ADD (LOCINTERESTONLYMATURITYDATE DATE NULL);
ALTER TABLE DEAL ADD (
  REFIPRODUCTTYPEID NUMBER(2) DEFAULT 0 NOT NULL REFERENCES PRODUCTTYPE);
ALTER TABLE DEAL ADD (PANDIUSING3YEARRATE NUMBER(13,2) NULL);

-- add fields to property table
ALTER TABLE PROPERTY ADD (
  TENURETYPEID NUMBER(2) DEFAULT 0 NOT NULL REFERENCES TENURETYPE
);
ALTER TABLE PROPERTY ADD (MIENERGYEFFICIENCY CHAR DEFAULT 'N' NULL);
ALTER TABLE PROPERTY ADD (ONRESERVETRUSTAGREEMENTNUMBER VARCHAR2(4) NULL);
ALTER TABLE PROPERTY ADD (SUBDIVISIONDISCOUNT CHAR DEFAULT 'N' NOT NULL);

COMMIT;
/