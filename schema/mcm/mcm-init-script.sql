/*==============================================================*/
/* DBMS name:      ORACLE Version 9i2                           */
/* Created on:     4/11/2008 9:55:25 AM                         */
/*==============================================================*/

/*==============================================================*/
/* Table: COMPONENT                                             */
/*==============================================================*/
create table COMPONENT  (
   INSTITUTIONPROFILEID NUMBER(13,0)                    not null,
   COMPONENTID          NUMBER(13,0)                    not null,
   COPYID               NUMBER(13,0)                    not null,
   ADDITIONALINFORMATION VARCHAR2(256),
   DEALID               NUMBER(13,0)                    not null,
   COMPONENTTYPEID      NUMBER(13,0)                    not null,
   FIRSTPAYMENTDATELIMIT DATE,
   MTGPRODID            NUMBER(13,0)                    not null,
   POSTEDRATE           NUMBER(12,6),
   PRICINGRATEINVENTORYID NUMBER(13,0)                    not null,
   REPAYMENTTYPEID      NUMBER(2,0)                     not null,
   TEASERMATURITYDATE   DATE,
   TEASERNETINTERESTRATE NUMBER(12,6),
   TEASERPIAMOUNT       NUMBER(13,2),
   TEASERRATEINTERESTSAVING NUMBER(13,2),
   constraint PK_COMPONENT primary key (INSTITUTIONPROFILEID, COMPONENTID, COPYID)
   using index tablespace mos_index
) tablespace mos_data;

/*==============================================================*/
/* Sequence for COMPONENTID
/*==============================================================*/
create sequence COMPONENTSEQ start with 1 increment by 1;

/*==============================================================*/
/* Index: MTGPROD_COMPONENT_FK                                  */
/*==============================================================*/
create index MTGPROD_COMPONENT_FK on COMPONENT (
   INSTITUTIONPROFILEID, MTGPRODID
) tablespace mos_index;

/*==============================================================*/
/* Index: REPAYMENT_COMPONENT_FK                                */
/*==============================================================*/
create index REPAYMENT_COMPONENT_FK on COMPONENT (
   REPAYMENTTYPEID
) tablespace mos_index;

/*==============================================================*/
/* Index: PRICERATE_FK                                          */
/*==============================================================*/
create index PRICERATE_FK on COMPONENT (
   INSTITUTIONPROFILEID, PRICINGRATEINVENTORYID
) tablespace mos_index;

/*==============================================================*/
/* Index: DEAL_COMPONENT_FK                                     */
/*==============================================================*/
create index DEAL_COMPONENT_FK on COMPONENT (
   INSTITUTIONPROFILEID, DEALID, COPYID
) tablespace mos_index;

/*==============================================================*/
/* Index: COMPTYPE_COMP_FK                                      */
/*==============================================================*/
create index COMPTYPE_COMP_FK on COMPONENT (
   INSTITUTIONPROFILEID, COMPONENTTYPEID
) tablespace mos_index;

/*==============================================================*/
/* Table: COMPONENTCREDITCARD                                   */
/*==============================================================*/
create table COMPONENTCREDITCARD  (
   INSTITUTIONPROFILEID NUMBER(13,0)                    not null,
   COMPONENTID          NUMBER(13,0)                    not null,
   COPYID               NUMBER(13,0)                    not null,
   CREDITCARDAMOUNT     NUMBER(13,2),
   constraint PK_COMPONENTCREDITCARD primary key (INSTITUTIONPROFILEID, COMPONENTID, COPYID)
   using index tablespace mos_index
) tablespace mos_data;

/*==============================================================*/
/* Table: COMPONENTLOAN                                         */
/*==============================================================*/
create table COMPONENTLOAN  (
   INSTITUTIONPROFILEID NUMBER(13,0)                    not null,
   COMPONENTID          NUMBER(13,0)                    not null,
   COPYID               NUMBER(13,0)                    not null,
   ACTUALPAYMENTTERM    NUMBER(3,0),
   DISCOUNT             NUMBER(12,6),
   FIRSTPAYMENTDATE     DATE,
   IADNUMBEROFDAYS      NUMBER(4,0),
   INTERESTADJUSTMENTAMOUNT NUMBER(13,2),
   INTERESTADJUSTMENTDATE DATE,
   LOANAMOUNT           NUMBER(13,2),
   MATURITYDATE         DATE,
   NETINTERESTRATE      NUMBER(12,6),
   PANDIPAYMENTAMOUNT   NUMBER(13,2),
   PAYMENTFREQUENCYID   NUMBER(2,0)                     not null,
   PERDIEMINTERESTAMOUNT NUMBER(13,2),
   PREMIUM              NUMBER(12,6),
   constraint PK_COMPONENTLOAN primary key (INSTITUTIONPROFILEID, COMPONENTID, COPYID)
   using index tablespace mos_index
) tablespace mos_data;


/*==============================================================*/
/* Index: PAYMENTFREQ_LOAN_FK                                   */
/*==============================================================*/
create index PAYMENTFREQ_LOAN_FK on COMPONENTLOAN (
   PAYMENTFREQUENCYID
) tablespace mos_index;

/*==============================================================*/
/* Table: COMPONENTLOC                                          */
/*==============================================================*/
create table COMPONENTLOC  (
   INSTITUTIONPROFILEID NUMBER(13,0)                    not null,
   COMPONENTID          NUMBER(13,0)                    not null,
   COPYID               NUMBER(13,0)                    not null,
   ADVANCEHOLD          NUMBER(13,2),
   COMMISSIONCODE       VARCHAR2(35),
   DISCOUNT             NUMBER(12,6),
   EXISTINGACCOUNTINDICATOR CHAR(1),
   EXISTINGACCOUNTNUMBER VARCHAR2(35),
   FIRSTPAYMENTDATE     DATE,
   INTERESTADJUSTMENTDATE DATE,
   LOCAMOUNT            NUMBER(13,2),
   MIALLOCATEFLAG       CHAR(1),
   NETINTERESTRATE      NUMBER(12,6),
   PANDIPAYMENTAMOUNT   NUMBER(13,2),
   PANDIPAYMENTAMOUNTMONTHLY NUMBER(13,2),
   PAYMENTFREQUENCYID   NUMBER(2,0)                     not null,
   PREMIUM              NUMBER(12,6),
   PREPAYMENTOPTIONSID  NUMBER(2,0)                     not null,
   PRIVILEGEPAYMENTID   NUMBER(2,0)                     not null,
   PROPERTYTAXALLOCATEFLAG CHAR(1),
   PROPERTYTAXESCROWAMOUNT NUMBER(13,2),
   TOTALLOCAMOUNT       NUMBER(13,2),
   TOTALPAYMENTAMOUNT   NUMBER(13,2),
   constraint PK_COMPONENTLOC primary key (INSTITUTIONPROFILEID, COMPONENTID, COPYID)
   using index tablespace mos_index
) tablespace mos_data;

/*==============================================================*/
/* Index: PRIVILEGE_LOC_FK                                      */
/*==============================================================*/
create index PRIVILEGE_LOC_FK on COMPONENTLOC (
   INSTITUTIONPROFILEID, PRIVILEGEPAYMENTID
) tablespace mos_index;

/*==============================================================*/
/* Index: PREPAY_LOC_FK                                         */
/*==============================================================*/
create index PREPAY_LOC_FK on COMPONENTLOC (
   INSTITUTIONPROFILEID, PREPAYMENTOPTIONSID
) tablespace mos_index;

/*==============================================================*/
/* Index: PAYFREQ_LOC_FK                                        */
/*==============================================================*/
create index PAYFREQ_LOC_FK on COMPONENTLOC (
   PAYMENTFREQUENCYID
) tablespace mos_index;

/*==============================================================*/
/* Table: COMPONENTMORTGAGE                                     */
/*==============================================================*/
create table COMPONENTMORTGAGE  (
   INSTITUTIONPROFILEID NUMBER(13,0)                    not null,
   COMPONENTID          NUMBER(13,0)                    not null,
   COPYID               NUMBER(13,0)                    not null,
   PAYMENTFREQUENCYID   NUMBER(2,0)                     not null,
   PRIVILEGEPAYMENTID   NUMBER(2,0)                     not null,
   PREPAYMENTOPTIONSID  NUMBER(2,0)                     not null,
   ACTUALPAYMENTTERM    NUMBER(3,0),
   ADDITIONALPRINCIPAL  NUMBER(13,2),
   ADVANCEHOLD          NUMBER(13,2),
   AMORTIZATIONTERM     NUMBER(8,0),
   BALANCEREMAININGATENDOFTERM NUMBER(12,4),
   BUYDOWNRATE          NUMBER(12,6),
   CASHBACKAMOUNT       NUMBER(13,2),
   CASHBACKAMOUNTOVERRIDE CHAR(1)                         not null,
   CASHBACKPERCENT      NUMBER(12,6),
   COMMISSIONCODE       VARCHAR2(35),
   DISCOUNT             NUMBER(12,6),
   EFFECTIVEAMORTIZATIONMONTHS NUMBER(3,0),
   EXISTINGACCOUNTINDICATOR CHAR(1),
   EXISTINGACCOUNTNUMBER VARCHAR2(35),
   FIRSTPAYMENTDATE     DATE,
   FIRSTPAYMENTDATEMONTHLY DATE,
   IADNUMBEROFDAYS      NUMBER(4,0),
   INTERESTADJUSTMENTAMOUNT NUMBER(13,2),
   INTERESTADJUSTMENTDATE DATE,
   MATURITYDATE         DATE,
   MIALLOCATEFLAG       CHAR(1),
   NETINTERESTRATE      NUMBER(12,6),
   PANDIPAYMENTAMOUNT   NUMBER(13,2),
   PANDIPAYMENTAMOUNTMONTHLY NUMBER(13,2),
   PERDIEMINTERESTAMOUNT NUMBER(13,2),
   PREMIUM              NUMBER(12,6),
   PROPERTYTAXALLOCATEFLAG CHAR(1),
   PROPERTYTAXESCROWAMOUNT NUMBER(13,2),
   RATEGUARANTEEPERIOD  NUMBER(3,0),
   RATELOCK             CHAR(1)                         not null,
   TOTALMORTGAGEAMOUNT  NUMBER(13,2),
   TOTALPAYMENTAMOUNT   NUMBER(13,2),
   MORTGAGEAMOUNT       NUMBER(13,2),
   constraint PK_COMPONENTMORTGAGE primary key (INSTITUTIONPROFILEID, COMPONENTID, COPYID)
   using index tablespace mos_index
) tablespace mos_data;

/*==============================================================*/
/* Index: PREPAYMENT_MTG_FK                                     */
/*==============================================================*/
create index PREPAYMENT_MTG_FK on COMPONENTMORTGAGE (
   INSTITUTIONPROFILEID, PREPAYMENTOPTIONSID
) tablespace mos_index;

/*==============================================================*/
/* Index: PAYMENTFREQ_MTG_FK                                    */
/*==============================================================*/
create index PAYMENTFREQ_MTG_FK on COMPONENTMORTGAGE (
   PAYMENTFREQUENCYID
) tablespace mos_index;

/*==============================================================*/
/* Index: PRIVILEGEPAY_MTG_FK                                   */
/*==============================================================*/
create index PRIVILEGEPAY_MTG_FK on COMPONENTMORTGAGE (
   INSTITUTIONPROFILEID, PRIVILEGEPAYMENTID
) tablespace mos_index;

/*==============================================================*/
/* Table: COMPONENTOVERDRAFT                                    */
/*==============================================================*/
create table COMPONENTOVERDRAFT  (
   INSTITUTIONPROFILEID NUMBER(13,0)                    not null,
   COMPONENTID          NUMBER(13,0)                    not null,
   COPYID               NUMBER(13,0)                    not null,
   OVERDRAFTAMOUNT      NUMBER(13,2),
   constraint PK_COMPONENTOVERDRAFT primary key (INSTITUTIONPROFILEID, COMPONENTID, COPYID)
   using index tablespace mos_index
) tablespace mos_data;

/*==============================================================*/
/* Table: COMPONENTSUMMARY                                      */
/*==============================================================*/
create table COMPONENTSUMMARY  (
   INSTITUTIONPROFILEID NUMBER(13,0)                    not null,
   DEALID               NUMBER(13,0)                    not null,
   COPYID               NUMBER(13,0)                    not null,
   TOTALAMOUNT          NUMBER(13,2),
   TOTALCASHBACKAMOUNT  NUMBER(13,2),
   constraint PK_COMPONENTSUMMARY primary key (INSTITUTIONPROFILEID, DEALID, COPYID)
   using index tablespace mos_index
) tablespace mos_data;

/*==============================================================*/
/* Table: COMPONENTTYPE                                         */
/*==============================================================*/
create table COMPONENTTYPE  (
   INSTITUTIONPROFILEID NUMBER(13,0)                    not null,
   COMPONENTTYPEID      NUMBER(13,0)                    not null,
   COMPONENTTYPEDESCRIPTION VARCHAR2(35),
   constraint PK_COMPONENTTYPE primary key (INSTITUTIONPROFILEID, COMPONENTTYPEID)
   using index tablespace mos_index
) tablespace mos_data;

/*==============================================================*/
/* Table: DEAL                                                  */
/*==============================================================*/
alter table DEAL ADD (
   REFIADDITIONALINFORMATION VARCHAR2(512));

/*==============================================================*/
/* Table: MTGPROD                                               */
/*==============================================================*/
alter table MTGPROD ADD (
   COMPONENTTYPEID      NUMBER(13,0),
   COMPONENTELIGIBLEFLAG CHAR(1),
   PRODUCTTYPEID        NUMBER(13,0),
   REPAYMENTTYPEID      NUMBER(2,0),
   UNDERWRITEASTYPEID   NUMBER(13,0)
);

update MTGPROD set COMPONENTTYPEID=0,PRODUCTTYPEID=0,REPAYMENTTYPEID=0,UNDERWRITEASTYPEID=0;

alter table MTGPROD MODIFY (
   COMPONENTTYPEID      not null,
   PRODUCTTYPEID        not null,
   REPAYMENTTYPEID      not null,
   UNDERWRITEASTYPEID   not null
);

/*==============================================================*/
/* Index: MTGPROD_UNDERWRITEAS_FK                               */
/*==============================================================*/
create index MTGPROD_UNDERWRITEAS_FK on MTGPROD (
   UNDERWRITEASTYPEID
) tablespace mos_index;

/*==============================================================*/
/* Index: MTGPROD_PRODUCTTYPE_FK                                */
/*==============================================================*/
create index MTGPROD_PRODUCTTYPE_FK on MTGPROD (
   PRODUCTTYPEID
) tablespace mos_index;

/*==============================================================*/
/* Index: COMPENTTYPE_MTGPROD_FK                                */
/*==============================================================*/
create index COMPENTTYPE_MTGPROD_FK on MTGPROD (
   INSTITUTIONPROFILEID, COMPONENTTYPEID
) tablespace mos_index;

/*==============================================================*/
/* Index: REPAYMENT_MTGPROD_FK                                  */
/*==============================================================*/
create index REPAYMENT_MTGPROD_FK on MTGPROD (
   REPAYMENTTYPEID
) tablespace mos_index;

/*==============================================================*/
/* Table: QUALIFYDETAIL                                         */
/*==============================================================*/
create table QUALIFYDETAIL  (
   INSTITUTIONPROFILEID NUMBER(13,0)                    not null,
   DEALID               NUMBER(13)                      not null,
   AMORTIZATIONTERM     NUMBER(8,0),
   INTERESTCOMPOUNDINGID NUMBER(2,0)                     not null,
   PANDIPAYMENTAMOUNTQUALIFY NUMBER(13,2),
   QUALIFYGDS           NUMBER(5,2),
   QUALIFYRATE          NUMBER(13,2),
   QUALIFYTDS           NUMBER(5,2),
   REPAYMENTTYPEID      NUMBER(2,0)                     not null,
   constraint PK_QUALIFYDETAIL primary key (INSTITUTIONPROFILEID, DEALID)
   using index tablespace mos_index
) tablespace mos_data;

/*==============================================================*/
/* Index: INTERESTCOMP_QUALIFYDETAIL_FK                         */
/*==============================================================*/
create index INTERESTCOMP_QUALIFYDETAIL_FK on QUALIFYDETAIL (
   INTERESTCOMPOUNDINGID
) tablespace mos_index;

/*==============================================================*/
/* Index: REPAYMENT_QUALIFYDETAIL_FK                            */
/*==============================================================*/
create index REPAYMENT_QUALIFYDETAIL_FK on QUALIFYDETAIL (
   REPAYMENTTYPEID
) tablespace mos_index;


/*==============================================================*/
/* Table: UNDERWRITEAS                                          */
/*==============================================================*/
create table UNDERWRITEAS  (
   UNDERWRITEASTYPEID   NUMBER(13,0)                    not null,
   UNDERWRITEASTYPEDESCRIPTION VARCHAR2(35),
   constraint PK_UNDERWRITEAS primary key (UNDERWRITEASTYPEID)
   using index tablespace mos_index
) tablespace mos_data;

alter table COMPONENT
   add constraint FK_COMPONEN_COMPTYPE__COMPONEN foreign key (INSTITUTIONPROFILEID, COMPONENTTYPEID)
      references COMPONENTTYPE (INSTITUTIONPROFILEID, COMPONENTTYPEID);

alter table COMPONENT
   add constraint FK_COMPONEN_DEAL_COMP_DEAL foreign key (INSTITUTIONPROFILEID, DEALID, COPYID)
      references DEAL (INSTITUTIONPROFILEID, DEALID, COPYID);

alter table COMPONENT
   add constraint FK_COMPONEN_MTGPROD_C_MTGPROD foreign key (INSTITUTIONPROFILEID, MTGPRODID)
      references MTGPROD (INSTITUTIONPROFILEID, MTGPRODID);

alter table COMPONENT
   add constraint FK_COMPONEN_PRICERATE_PRICINGR foreign key (INSTITUTIONPROFILEID, PRICINGRATEINVENTORYID)
      references PRICINGRATEINVENTORY (INSTITUTIONPROFILEID, PRICINGRATEINVENTORYID);

alter table COMPONENT
   add constraint FK_COMPONEN_REPAYMENT_REPAYMEN foreign key (REPAYMENTTYPEID)
      references REPAYMENTTYPE (REPAYMENTTYPEID);

alter table COMPONENTCREDITCARD
   add constraint FK_COMPONEN_COMP_CC_COMPONEN foreign key (INSTITUTIONPROFILEID, COMPONENTID, COPYID)
      references COMPONENT (INSTITUTIONPROFILEID, COMPONENTID, COPYID);

alter table COMPONENTLOAN
   add constraint FK_COMPONEN_COMP_LOAN_COMPONEN foreign key (INSTITUTIONPROFILEID, COMPONENTID, COPYID)
      references COMPONENT (INSTITUTIONPROFILEID, COMPONENTID, COPYID);

alter table COMPONENTLOAN
   add constraint FK_COMPONEN_PAYMENTFR_PAYMENTF foreign key (PAYMENTFREQUENCYID)
      references PAYMENTFREQUENCY (PAYMENTFREQUENCYID);

alter table COMPONENTLOC
   add constraint FK_COMPONEN_COMP_LOC_COMPONEN foreign key (INSTITUTIONPROFILEID, COMPONENTID, COPYID)
      references COMPONENT (INSTITUTIONPROFILEID, COMPONENTID, COPYID);

alter table COMPONENTLOC
   add constraint FK_COMPONEN_PAYFREQ_L_PAYMENTF foreign key (PAYMENTFREQUENCYID)
      references PAYMENTFREQUENCY (PAYMENTFREQUENCYID);

alter table COMPONENTLOC
   add constraint FK_COMPONEN_PREPAY_LO_PREPAYME foreign key (INSTITUTIONPROFILEID, PREPAYMENTOPTIONSID)
      references PREPAYMENTOPTIONS (INSTITUTIONPROFILEID, PREPAYMENTOPTIONSID);

alter table COMPONENTLOC
   add constraint FK_COMPONEN_PRIVILEGE_PRIVILEG foreign key (INSTITUTIONPROFILEID, PRIVILEGEPAYMENTID)
      references PRIVILEGEPAYMENT (INSTITUTIONPROFILEID, PRIVILEGEPAYMENTID);

alter table COMPONENTMORTGAGE
   add constraint FK_COMPONEN_COMP_MTG_COMPONEN foreign key (INSTITUTIONPROFILEID, COMPONENTID, COPYID)
      references COMPONENT (INSTITUTIONPROFILEID, COMPONENTID, COPYID);

alter table COMPONENTMORTGAGE
   add constraint FK_COMPONEN_PAYMENTFR_PAYMENT2 foreign key (PAYMENTFREQUENCYID)
      references PAYMENTFREQUENCY (PAYMENTFREQUENCYID);

alter table COMPONENTMORTGAGE
   add constraint FK_COMPONEN_PREPAYMEN_PREPAYME foreign key (INSTITUTIONPROFILEID, PREPAYMENTOPTIONSID)
      references PREPAYMENTOPTIONS (INSTITUTIONPROFILEID, PREPAYMENTOPTIONSID);

alter table COMPONENTMORTGAGE
   add constraint FK_COMPONEN_PRIVILEGE_PRIVILE2 foreign key (INSTITUTIONPROFILEID, PRIVILEGEPAYMENTID)
      references PRIVILEGEPAYMENT (INSTITUTIONPROFILEID, PRIVILEGEPAYMENTID);

alter table COMPONENTOVERDRAFT
   add constraint FK_COMPONEN_COMP_OD_COMPONEN foreign key (INSTITUTIONPROFILEID, COMPONENTID, COPYID)
      references COMPONENT (INSTITUTIONPROFILEID, COMPONENTID, COPYID);

alter table COMPONENTSUMMARY
   add constraint FK_COMPONEN_COMPSUM_D_DEAL foreign key (INSTITUTIONPROFILEID, DEALID, COPYID)
      references DEAL (INSTITUTIONPROFILEID, DEALID, COPYID);

alter table MTGPROD
   add constraint FK_MTGPROD_COMPENTTY_COMPONEN foreign key (INSTITUTIONPROFILEID, COMPONENTTYPEID)
      references COMPONENTTYPE (INSTITUTIONPROFILEID, COMPONENTTYPEID);

alter table MTGPROD
   add constraint FK_MTGPROD_MTGPROD_P_PRODUCTT foreign key (PRODUCTTYPEID)
      references PRODUCTTYPE (PRODUCTTYPEID);

alter table MTGPROD
   add constraint FK_MTGPROD_MTGPROD_U_UNDERWRI foreign key (UNDERWRITEASTYPEID)
      references UNDERWRITEAS (UNDERWRITEASTYPEID);

alter table MTGPROD
   add constraint FK_MTGPROD_REPAYMENT_REPAYMEN foreign key (REPAYMENTTYPEID)
      references REPAYMENTTYPE (REPAYMENTTYPEID);

alter table QUALIFYDETAIL
   add constraint FK_QUALIFYD_INTERESTC_INTEREST foreign key (INTERESTCOMPOUNDINGID)
      references INTERESTCOMPOUND (INTERESTCOMPOUNDINGID);

alter table QUALIFYDETAIL
   add constraint FK_QUALIFYD_MASTERDEA_MASTERDE foreign key (INSTITUTIONPROFILEID, DEALID)
      references MASTERDEAL (INSTITUTIONPROFILEID, DEALID);

alter table QUALIFYDETAIL
   add constraint FK_QUALIFYD_REPAYMENT_REPAYMEN foreign key (REPAYMENTTYPEID)
      references REPAYMENTTYPE (REPAYMENTTYPEID);

alter table MTGPROD
   add constraint FK_MTGPROD_MTGPROD_P_PRODUCTT foreign key (PRODUCTTYPEID)
      references PRODUCTTYPE (PRODUCTTYPEID);

alter table MTGPROD
   add constraint FK_MTGPROD_REPAYMENT_REPAYMEN foreign key (REPAYMENTTYPEID)
      references REPAYMENTTYPE (REPAYMENTTYPEID);

----------------- following Foreign key constraints require proper setup -----------------
alter table MTGPROD
   add constraint FK_MTGPROD_MTGPROD_U_UNDERWRI foreign key (UNDERWRITEASTYPEID)
      references UNDERWRITEAS (UNDERWRITEASTYPEID);

alter table MTGPROD
   add constraint FK_MTGPROD_COMPENTTY_COMPONEN foreign key (INSTITUTIONPROFILEID, COMPONENTTYPEID)
      references COMPONENTTYPE (INSTITUTIONPROFILEID, COMPONENTTYPEID);

ALTER TABLE COMPONENTLOAN MODIFY (PAYMENTFREQUENCYID DEFAULT 0);

ALTER TABLE DENIALREASON MODIFY catageory NUMBER(3,0);
ALTER TABLE DENIALREASON MODIFY DENIALREASONID NUMBER(3,0);
ALTER TABLE DEAL MODIFY DENIALREASONID NUMBER(3,0);
Alter table COMPONENTMORTGAGE modify (EXISTINGACCOUNTINDICATOR default 'N');
Alter table COMPONENTLOC modify (EXISTINGACCOUNTINDICATOR default 'N');
Alter table COMPONENTLOAN add (PANDIPAYMENTAMOUNTMONTHLY NUMBER(13,2));
