CREATE OR REPLACE package VPDADMIN.VPD_Security_Context as

    procedure set_app_context(p_userprofileid number, p_institutionprofileid number, p_schema_owner varchar2 DEFAULT 'MOS');

    procedure set_app_context(p_deal_institution_id number, p_schema_owner varchar2 DEFAULT 'MOS');

    procedure set_app_context;

end VPD_Security_Context;

/

 

CREATE OR REPLACE package BODY VPDADMIN.VPD_Security_Context as

procedure set_app_context(p_userprofileid number, p_institutionprofileid number, p_schema_owner varchar2) IS

type t_rc is ref cursor;

c_institution_list t_rc;

v_institution             varchar2(30);

v_institution_profile_id_list    varchar2(1000):='';

sql_stmt                          varchar2(200);

 

begin

    DBMS_SESSION.SET_CONTEXT('vpd_app_context', 'institution_profile_id_list', NULL);

    sql_stmt := 'select institutionprofileid from '||p_schema_owner||'.userprofile where userlogin = (select userlogin from '||p_schema_owner||'.userprofile where institutionprofileid= :v1 and userprofileid= :v2)';

    open c_institution_list FOR sql_stmt USING p_institutionprofileid, p_userprofileid;

    FETCH c_institution_list INTO v_institution;

    if c_institution_list%NOTFOUND then

       close c_institution_list;

       DBMS_SESSION.SET_CONTEXT('vpd_app_context', 'institution_profile_id_list', '1=0');

       DBMS_SESSION.SET_CONTEXT('vpd_app_context', 'deal_institution_id', NULL);

       raise_application_error(-20001, 'Invalid UserID and InstitutionID : '||p_userprofileid||', '||p_institutionprofileid);

    end if;

    v_institution_profile_id_list := v_institution_profile_id_list||','||v_institution;

    LOOP

    FETCH c_institution_list INTO v_institution;

    EXIT WHEN c_institution_list%NOTFOUND;

    v_institution_profile_id_list := v_institution_profile_id_list||','||v_institution;

    END LOOP;

    close c_institution_list;

    v_institution_profile_id_list := substr(v_institution_profile_id_list, 2);

    v_institution_profile_id_list := 'institutionprofileid in ('||v_institution_profile_id_list||')';

    DBMS_SESSION.SET_CONTEXT('vpd_app_context', 'institution_profile_id_list', v_institution_profile_id_list);

end set_app_context;

 

procedure set_app_context(p_deal_institution_id number, p_schema_owner varchar2) IS

type t_rc is ref cursor;

c_institution_list t_rc;

v_institution             varchar2(30);

v_institution_profile_id_list    varchar2(1000):='';

sql_stmt                          varchar2(200);

 

begin

    sql_stmt := 'select INSTITUTIONPROFILEID from '||p_schema_owner||'.institutionprofile where INSTITUTIONPROFILEID = :v0';

    open c_institution_list FOR sql_stmt USING p_deal_institution_id;

    FETCH c_institution_list INTO v_institution_profile_id_list;

    if c_institution_list%NOTFOUND then

       DBMS_SESSION.SET_CONTEXT('vpd_app_context', 'institution_profile_id_list', '1=0');

       DBMS_SESSION.SET_CONTEXT('vpd_app_context', 'deal_institution_id', NULL);

       close c_institution_list;

       raise_application_error(-20000, 'Invalid deal institution profile id : '||p_deal_institution_id );

    end if;

    v_institution_profile_id_list := 'institutionprofileid = '||v_institution_profile_id_list;

    close c_institution_list;

    DBMS_SESSION.SET_CONTEXT('vpd_app_context', 'institution_profile_id_list', v_institution_profile_id_list);

    DBMS_SESSION.SET_CONTEXT('vpd_app_context', 'deal_institution_id', p_deal_institution_id);

end set_app_context;

 

procedure set_app_context IS

begin

    DBMS_SESSION.SET_CONTEXT('vpd_app_context', 'institution_profile_id_list', NULL);

    DBMS_SESSION.SET_CONTEXT('vpd_app_context', 'deal_institution_id', NULL);

end set_app_context;

 

end VPD_Security_Context;

/
