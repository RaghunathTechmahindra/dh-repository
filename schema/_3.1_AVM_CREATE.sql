/*==============================================================*/
/* DBMS name:      ORACLE Version 9i                            */
/* Created on:     13/2/2006 8:13:03 PM                         */
/* Altered on:     2-Apr-006                                    */
/*==============================================================*/

--------------- create sequences --------------- 

CREATE SEQUENCE	REQUESTSEQ INCREMENT BY 1 START WITH 1 MINVALUE 1 NOCYCLE NOCACHE NOORDER;

CREATE SEQUENCE	RESPONSESEQ INCREMENT BY 1 START WITH 1 MINVALUE 1 NOCYCLE NOCACHE NOORDER;

CREATE SEQUENCE	SERVICEPRODUCTSEQ INCREMENT BY 1 START WITH 1 MINVALUE 1 NOCYCLE NOCACHE NOORDER;

CREATE SEQUENCE	SERVICEREQUESTCONTACTSEQ INCREMENT BY 1 START WITH 1 MINVALUE 1 NOCYCLE NOCACHE NOORDER;


---------------  drop constraints ---------------  
/*==============================================================*/
/* Table: ADJUDICATIONAPPLICANT                                */
/*==============================================================*/
create table ADJUDICATIONAPPLICANT  (
   REQUESTID            NUMBER(13,0)                    not null,
   APPLICANTNUMBER      NUMBER(2,0)                     not null,
   COPYID               NUMBER(3,0)                     not null,
   BORROWERID           NUMBER(13,0)                    not null,
   APPLICANTCURRENTIND  CHAR(1)                         not null,
   constraint PK_ADJUDICATIONAPPLICANT primary key (REQUESTID, APPLICANTNUMBER, COPYID)
);

/*==============================================================*/
/* Table: ADJUDICATIONREQUEST                                  */
/*==============================================================*/
create table ADJUDICATIONREQUEST  (
   REQUESTID            NUMBER(13,0)                    not null,
   COPYID               NUMBER(13,0)                    not null,
   SCENARIONUMBER       NUMBER(3,0),
   NOOFAPPLICANTS       NUMBER(1,0),
   constraint PK_ADJUDICATIONREQUEST primary key (REQUESTID, COPYID)
);

/*==============================================================*/
/* Table: ADJUDICATIONRESPONSE                                 */
/*==============================================================*/
create table ADJUDICATIONRESPONSE  (
   RESPONSEID           NUMBER(13,0)                    not null,
   ADJUDICATIONSTATUSID NUMBER(2,0)                     not null,
   ADJUDICATIONCONFIRMNO VARCHAR2(14),
   constraint PK_ADJUDICATIONRESPONSE primary key (RESPONSEID)
);


/*==============================================================*/
/* Table: ADJUDICATIONSTATUS                                   */
/*==============================================================*/
create table ADJUDICATIONSTATUS  (
   ADJUDICATIONSTATUSID NUMBER(2,0)                     not null,
   ADJUDICATIONDESC     VARCHAR2(35)                    not null,
   constraint PK_ADJUDICATIONSTATUS primary key (ADJUDICATIONSTATUSID)
);

/*==============================================================*/
/* Table: APPRAISALSUMMARY                                     */
/*==============================================================*/
create table APPRAISALSUMMARY  (
   RESPONSEID           NUMBER(13,0)                    not null,
   APPRAISALREPORT      BLOB,
   APPRAISALDATE        DATE,
   ACTUALAPPRAISALVALUE NUMBER(13,2),
   constraint PK_APPRAISALSUMMARY primary key (RESPONSEID)
);

/*==============================================================*/
/* Table: AVMSUMMARY                                           */
/*==============================================================*/
create table AVMSUMMARY  (
   RESPONSEID           NUMBER(13,0)                    not null,
   AVMREPORT            CLOB,                                            -- HTML file`
   AVMVALUE             NUMBER(13,2),
   CONFIDENCELEVEL      NUMBER(4,0),
   HIGHAVMRANGE         NUMBER(13,2),
   LOWAVMRANGE          NUMBER(13,2),
   constraint PK_AVMSUMMARY primary key (RESPONSEID)
);

/*==============================================================*/
/* Table: BORROWERREQUESTASSOC                                */
/*==============================================================*/
create table BORROWERREQUESTASSOC  (
   REQUESTID            NUMBER(13,0)                    not null,
   COPYID               NUMBER(3,0)                     not null,
   BORROWERID           NUMBER(13,0)                    not null,
   constraint PK_BORROWERREQUESTASSOC primary key (COPYID, REQUESTID, BORROWERID)
);

/*==============================================================*/
/* Table: CHANNEL                                               */
/*==============================================================*/
create table CHANNEL  (
   CHANNELID            NUMBER(4,0)                     not null,
   CHANNELTYPEID        NUMBER(2,0)                     not null,
   PROTOCOL             VARCHAR2(6),
   IP                   VARCHAR2(100),
   PORT                 NUMBER(4,0),
   USERID               VARCHAR2(35),
   PASSWORD             VARCHAR2(20),
   PATH                 VARCHAR2(100),
   constraint PK_CHANNEL primary key (CHANNELID)
);


/*==============================================================*/
/* Table: CHANNELTYPE                                          */
/*==============================================================*/
create table CHANNELTYPE  (
   CHANNELTYPEID        NUMBER(2,0)                     not null,
   CHANNELTYPEDESC      VARCHAR2(35)                    not null,
   constraint PK_CHANNELTYPE primary key (CHANNELTYPEID)
);

/*==============================================================*/
/* Table: PAYLOADTYPE                                          */
/*==============================================================*/
create table PAYLOADTYPE  (
   PAYLOADTYPEID        NUMBER(13,0)                    not null,
   DOCUMENTTYPEID       NUMBER(13,0)                    not null,
   PAYLOADTYPEDESC      VARCHAR2(50)                    not null,
   PAYLOADVERSION       VARCHAR2(3),
   constraint PK_PAYLOADTYPE primary key (PAYLOADTYPEID)
);


/*==============================================================*/
/* Table: REQUEST                                               */
/*==============================================================*/
create table REQUEST  (
   REQUESTID            NUMBER(13,0)                    not null,
   REQUESTDATE          DATE,
   DEALID               NUMBER(13,0)                    not null,
   COPYID               NUMBER(3,0)                     not null,
   SERVICEPRODUCTID     NUMBER(3,0),
   CHANNELID            NUMBER(4,0),
   PAYLOADTYPEID        NUMBER(13,0),
   SERVICETRANSACTIONTYPEID    NUMBER(13,0),
   REQUESTSTATUSID      NUMBER(2,0)                     not null,
   STATUSMESSAGE        VARCHAR2(255),
   STATUSDATE           DATE                       not null,
   USERPROFILEID        NUMBER(13,0),
   constraint PK_REQUEST primary key (REQUESTID, COPYID)
);

comment on table REQUEST is 'Transaction level Dynamic table';


/*==============================================================*/
/* Table: REQUESTSTATUS                                        */
/*==============================================================*/
create table REQUESTSTATUS  (
   REQUESTSTATUSID      NUMBER(2,0)                     not null,
   REQUESTSTATUSNAME    VARCHAR2(35)                    not null,
   REQUESTSTATUSDESC    VARCHAR2(35)                    not null,
   constraint PK_REQUESTSTATUS primary key (REQUESTSTATUSID)
);

comment on table REQUESTSTATUS is
'A status indicates current state of the message.
Ex: Submitted, Received, Complete, Error
';

/*==============================================================*/
/* Table: RESPONSE                                              */
/*==============================================================*/
create table RESPONSE  (
   RESPONSEID           NUMBER(13,0)                    not null,
   RESPONSEDATE         DATE                            not null,
   REQUESTID            NUMBER(13,0)                    not null,
   DEALID               NUMBER(13,0)                    not null,
   RESPONSESTATUSID     NUMBER(2,0)                     not null,
   STATUSDATE           DATE                            not null,
   STATUSMESSAGE        VARCHAR2(255),
   CHANNELTRANSACTIONKEY VARCHAR2(30),
   constraint PK_RESPONSE primary key (RESPONSEID)
);

/*==============================================================*/
/* Table: RESPONSESTATUS                                       */
/*==============================================================*/
create table RESPONSESTATUS  (
   RESPONSESTATUSID     NUMBER(2,0)                     not null,
   RESPONSESTATUSNAME   VARCHAR2(35)                    not null,
   RESPONSESTATUSDESC   VARCHAR2(35)                    not null,
   constraint PK_RESPONSESTATUS primary key (RESPONSESTATUSID)
);

/*==============================================================*/
/* Table: SAMADJUDICATIONAPPLICANT                            */
/*==============================================================*/
create table SAMADJUDICATIONAPPLICANT  (
   REQUESTID            NUMBER(13,0)                    not null,
   COPYID				NUMBER(13,0)					not null,
   APPLICANTNUMBER      NUMBER(2,0)                     not null,
   SAMTOTALMONTHLYPAYMENT NUMBER(13,2),
   SAMCREDITFILEREPORT  CLOB,
   constraint PK_SAMADJUDICATIONAPPLICANT primary key (APPLICANTNUMBER, REQUESTID, COPYID)
);

/*==============================================================*/
/* Table: SAMAPPLICATIONTYPE                                  */
/*==============================================================*/
create table SAMAPPLICATIONTYPE  (
   SAMAPPLICATIONTYPEID NUMBER(1,0)                     not null,
   SAMAPPLICATIONTYPEDESC VARCHAR2(35)                    not null,
   SAMAPPLICATIONTYPECODE CHAR(2)                         not null,
   constraint PK_SAMAPPLICATIONTYPE primary key (SAMAPPLICATIONTYPEID)
);

/*==============================================================*/
/* Table: SAMDISPOSITION                                       */
/*==============================================================*/
create table SAMDISPOSITION  (
   RESPONSEID           NUMBER(13,0)                    not null,
   SAMDISPOSITIONSTATUSID NUMBER(2,0),
   SAMDISPOSITIONCODEID NUMBER(2,0),
   USERPROFILEID        NUMBER(13,0),
   SUBMITDATETIMESTAMP  DATE,
   RESPONSEDATETIMESTAMP DATE,
   constraint PK_SAMDISPOSITION primary key (RESPONSEID)
);

/*==============================================================*/
/* Table: SAMDISPOSITIONCODE                                  */
/*==============================================================*/
create table SAMDISPOSITIONCODE  (
   SAMDISPOSITIONCODEID   NUMBER(2,0)                     not null,
   SAMDISPOSITIONCODEDESC VARCHAR2(35)                    not null,
   SAMDISPOSITIONCODE     CHAR(1)                    	  not null,
   constraint PK_SAMDISPOSITIONCODE primary key (SAMDISPOSITIONCODEID)
);

/*==============================================================*/
/* Table: SAMDISPOSITIONSTATUS                                */
/*==============================================================*/
create table SAMDISPOSITIONSTATUS  (
   SAMDISPOSITIONSTATUSID NUMBER(2,0)                     not null,
   SAMDISPOSITIONSTATUSDESC VARCHAR2(35)                    not null,
   constraint PK_SAMDISPOSITIONSTATUS primary key (SAMDISPOSITIONSTATUSID)
);

/*==============================================================*/
/* Table: SAMPRODUCTTYPE                                      */
/*==============================================================*/
create table SAMPRODUCTTYPE  (
   SAMPRODUCTTYPEID     NUMBER(1,0)                     not null,
   SAMPRODUCTTYPEDESC   VARCHAR2(35)                    not null,
   SAMPRODUCTTYPECODE   CHAR(2)                         not null,
   constraint PK_SAMPRODUCTTYPE primary key (SAMPRODUCTTYPEID)
);

/*==============================================================*/
/* Table: SAMPROPERTYTYPE                                     */
/*==============================================================*/
create table SAMPROPERTYTYPE  (
   SAMPROPERTYTYPEID    NUMBER(1,0)                     not null,
   SAMPROPERTYTYPEDESC  VARCHAR2(35)                    not null,
   constraint PK_SAMPROPERTYTYPE primary key (SAMPROPERTYTYPEID)
);

/*==============================================================*/
/* Table: SAMREQUEST                                           */
/*==============================================================*/
create table SAMREQUEST  (
   REQUESTID            NUMBER(13,0)                    not null,
   COPYID				NUMBER(13,0)					not null,
   SAMSTEPID            NUMBER(2,0),
   SAMAPPLICATIONTYPEID NUMBER(1,0),
   SAMPROPERTYTYPEID    NUMBER(1,0),
   SAMPRODUCTTYPEID     NUMBER(1,0),
   SAMREQUESTTYPECODE   CHAR(2),
   SAMCREDITFILEREQUIRED CHAR(1),
   SAMGUARANTOROCCUPANCY CHAR(1),
   constraint PK_SAMREQUEST primary key (REQUESTID, COPYID)
);

/*==============================================================*/
/* Table: SAMRESPONSE                                          */
/*==============================================================*/
create table SAMRESPONSE  (
   RESPONSEID           NUMBER(13,0)                    not null,
   SAMDECISIONID        NUMBER(10,0),
   SAMAMOUNTAPPROVED    NUMBER(13,2),
   SAMRESULT            CLOB,
   SAMXML               CLOB,
   constraint PK_SAMRESPONSE primary key (RESPONSEID)
);

/*==============================================================*/
/* Table: SAMSTEP                                              */
/*==============================================================*/
create table SAMSTEP  (
   SAMSTEPID            NUMBER(2,0)                     not null,
   SAMSTEPDESC          VARCHAR2(35)                    not null,
   constraint PK_SAMSTEP primary key (SAMSTEPID)
);

/*==============================================================*/
/* Table: SCOTIABANKADDITIONALDETAILS                         */
/*==============================================================*/
create table SCOTIABANKADDITIONALDETAILS  (
   DEALID               NUMBER(13,0)                    not null,
   COPYID               NUMBER(3,0)                     not null,
   APPROVALDATE         DATE,
   SCOTIABANKAPPLICATIONNUMBER NUMBER(10,0),
   BRANCHNAME           VARCHAR2(35),
   SERVICINGBRANCHNUMBER NUMBER(10,0),
   STANDALONEMORTGAGENUMBER NUMBER(15,0),
   STANDALONESCLNUMBER  NUMBER(20,0),
   HEALTHCRISISINSURANCESTATUS CHAR(1),
   SCOTIALIFEINSURANCESTATUS CHAR(1),
   BANKCODE             NUMBER(3,0),
   POSTEDRATE           NUMBER(12,6),
   TRANSITNUMBER        NUMBER(5,0),
   COMMITTEDRATE        NUMBER(12,6),
   BANKACCOUNT          NUMBER(10,0),
   AMORTIZATION         NUMBER(2,0),
   FINDERSFEETOTAL      NUMBER(8,2),
   TERMSTARTDATE        DATE,
   RATEBUYDOWN          NUMBER(6,3),
   REFINANCENEWMONEY    NUMBER(13,2),
   RATEBUYDOWNAMOUNT    NUMBER(13,2),
   SWITCHCODE           VARCHAR2(80),
   CASHBACKPERCENTAGE   NUMBER(6,3),
   EXISTINGACCOUNTNUMBER NUMBER(15,0),
   CASHBACKAMOUNT       NUMBER(13,2),
   RATEBUYDOWNPAYORID   NUMBER(2,0)                     not null,
   MDM                  VARCHAR2(50),
   HOLDBACKAMOUNT       NUMBER(13,2),
   OTHERMORTGAGEDEDUCTIONS VARCHAR2(15),
   PPDESCRIPTION        VARCHAR2(35),
   OTHERDEDUCTIONSDESCRIPTION VARCHAR2(150),
   SFDESCRIPTION        VARCHAR2(35),
   OTHERDESCRIPTION     VARCHAR2(150),
   constraint PK_SCOTIABANKADDITIONALDETAILS primary key (DEALID, COPYID)
);

comment on table SCOTIABANKADDITIONALDETAILS is
'This SAD table is not normalized
It is a reporting table with SAM specific information.';


/*==============================================================*/
/* Table: SCOTIABANKRATEBUYDOWNPAYOR                           */
/*==============================================================*/
create table SCOTIABANKRATEBUYDOWNPAYOR  (
   RATEBUYDOWNPAYORID   NUMBER(2,0)                     not null,
   RATEBUYDOWNPAYORDESC VARCHAR2(15)                    not null,
   RATEBUYDOWNPAYOR     CHAR(1)                         not null,
   constraint PK_SCOTIABANKRATEBUYDOWNPAYOR primary key (RATEBUYDOWNPAYORID)
);

/*==============================================================*/
/* Table: SCOTIABANKSTEPMORTGAGE                               */
/*==============================================================*/
create table SCOTIABANKSTEPMORTGAGE  (
   DEALID               NUMBER(13,0)                    not null,
   COPYID               NUMBER(3,0)                     not null,
   STEPMORTGAGEID       NUMBER(13,0)                    not null,
   STEPMORTGAGETYPEID   NUMBER(2,0)                     not null,
   STEPPRODUCTID        NUMBER(2,0)                     not null,
   STEPMORTGAGENUMBER   NUMBER(15,0)                    not null,
   STEPMORTGAGEGLOBALLIMIT NUMBER(13,2),
   STEPMORTGAGEREGISTRATIONAMOUNT NUMBER(13,2),
   STEPMORTGAGEACCOUNTLOANNUMBER NUMBER(20,0),
   STEPMORTGAGECREDITLIMIT NUMBER(13,2),
   STEPMORTGAGEAMOUNTADVANCED NUMBER(13,2)                    not null,
   STEPMORTGAGEPOSTEDRATE NUMBER(12,6),
   STEPMORTGAGECOMMITTEDRATE NUMBER(12,6),
   STEPMORTGAGETERM     NUMBER(3,0)                     not null,
   STEPMORTGAGEAMORTIZATION NUMBER(3,0)                     not null,
   STEPMORTGAGEPAYMENT  NUMBER(13,2),
   STEPMORTGAGEFINDERSFEE NUMBER(13,2),
   STEPMORTGAGELOLP     CHAR(1),
   STEPMORTGAGEHCP      CHAR(1),
   STEPMORTGAGECASHBACKPERCENTAGE NUMBER(6,3),
   USERPROFILEID        NUMBER(13,0)                    not null,
   DATETIMESTAMP        DATE,
   constraint PK_SCOTIABANKSTEPMORTGAGE primary key (STEPMORTGAGEID, DEALID, COPYID)
);

/*==============================================================*/
/* Table: SCOTIABANKSTEPMORTGAGETYPE                           */
/*==============================================================*/
create table SCOTIABANKSTEPMORTGAGETYPE  (
   STEPMORTGAGETYPEID   NUMBER(2,0)                     not null,
   STEPMORTGAGETYPEDESC VARCHAR2(35)                    not null,
   STEPMORTGAGETYPE     CHAR(2)                         not null,
   constraint PK_SCOTIABANKSTEPMORTGAGETYPE primary key (STEPMORTGAGETYPEID)
);

/*==============================================================*/
/* Table: SCOTIABANKSTEPPRODUCT                                */
/*==============================================================*/
create table SCOTIABANKSTEPPRODUCT  (
   STEPPRODUCTID        NUMBER(2,0)                     not null,
   STEPPRODUCTDESC      VARCHAR2(35)                    not null,
   constraint PK_SCOTIABANKSTEPPRODUCT primary key (STEPPRODUCTID)
);

/*==============================================================*/
/* Table: SERVICEPRODUCT                                       */
/*==============================================================*/
create table SERVICEPRODUCT  (
   SERVICEPRODUCTID     NUMBER(3,0)                     not null,
   SERVICETYPEID        NUMBER(3,0)                     not null,
   SERVICESUBTYPEID     NUMBER(3,0),
   SERVICESUBSUBTYPEID  NUMBER(3,0),
   SERVICEPROVIDERID    NUMBER(3,0)                     not null,
   SERVICEPRODUCTNAME   VARCHAR2(35),
   SERVICEPRODUCTDESC   VARCHAR2(255),
   SERVICEPRODUCTVERSION VARCHAR2(3),
   STARTDATE            DATE                            not null,
   ENDDATE              DATE,
   DISABLEDATE          DATE,
   constraint PK_SERVICEPRODUCT primary key (SERVICEPRODUCTID)
);

comment on table SERVICEPRODUCT is
'(product)';


/*==============================================================*/
/* Table: SERVICEPRODUCTREQUESTASSOC                            */
/*==============================================================*/
create table SERVICEPRODUCTREQUESTASSOC  (
   SERVICEPRODUCTID     NUMBER(3,0)                     not null,
   CHANNELID            NUMBER(4,0)                     not null,
   PAYLOADTYPEID        NUMBER(13,0)                    not null,
   SERVICETRANSACTIONTYPEID    NUMBER(13,0)                    not null,
   REQUESTTYPEDESC      VARCHAR2(35),
   REQUESTTYPEID		NUMBER(4),
   REQUESTTYPESHORTNAME VARCHAR(10),
   constraint PK_SERVICEPRODUCTREQUESTASSOC primary key (CHANNELID, PAYLOADTYPEID, SERVICEPRODUCTID, SERVICETRANSACTIONTYPEID)
);

/*==============================================================*/
/* Table: SERVICEPROVIDER                                      */
/*==============================================================*/
create table SERVICEPROVIDER  (
   SERVICEPROVIDERID    NUMBER(3,0)                     not null,
   CONTACTID            NUMBER(13,0),
   COPYID               NUMBER(3,0),
   SERVICEPROVIDERNAME  VARCHAR2(35)                    not null,
   STARTDATE            DATE                            not null,
   ENDDATE              DATE,
   RECEIVINGPARTYCODE   VARCHAR(25),
   constraint PK_SERVICEPROVIDER primary key (SERVICEPROVIDERID)
);

comment on table SERVICEPROVIDER is
'A provider is a company that offers services. 
Aka vendor
ex. Centract, NAS, FCT, FNF';

/*==============================================================*/
/* Table: SERVICEREQUEST                                       */
/*==============================================================*/
create table SERVICEREQUEST  (
   REQUESTID            NUMBER(13,0)                    not null,
   PROPERTYID           NUMBER(13,0),
   COPYID               NUMBER(3,0),
   SPECIALINSTRUCTIONS  VARCHAR2(255),
   SERVICEPROVIDERREFNO VARCHAR2(20),
   constraint PK_SERVICEREQUEST primary key (REQUESTID, COPYID)
);

/*==============================================================*/
/* Table: SERVICEREQUESTCONTACT                               */
/*==============================================================*/
create table SERVICEREQUESTCONTACT  (
   REQUESTID            NUMBER(13,0)                    not null,
   SERVICEREQUESTCONTACTID NUMBER(13,0)                    not null,
   COPYID               NUMBER(3,0),
   BORROWERID           NUMBER(13,0),
   NAMEFIRST            VARCHAR2(20),
   NAMELAST             VARCHAR2(20),
   EMAILADDRESS         VARCHAR2(50),
   WORKAREACODE         CHAR(3),
   WORKPHONENUMBER      CHAR(7),
   WORKEXTENSION        VARCHAR2(6),
   CELLAREACODE         CHAR(3),
   CELLPHONENUMBER      CHAR(7),
   HOMEAREACODE         CHAR(3),
   HOMEPHONENUMBER      CHAR(7),
   PROPERTYOWNERNAME    VARCHAR2(40),
   COMMENTS             VARCHAR2(255),
   constraint PK_SERVICEREQUESTCONTACT primary key (SERVICEREQUESTCONTACTID, REQUESTID, COPYID)
);

comment on table SERVICEREQUESTCONTACT is
'A contact is a person who shall be contacted by the service provider regarding the product ordered.';

/*==============================================================*/
/* Table: SERVICESUBSUBTYPE                                   */
/*==============================================================*/
create table SERVICESUBSUBTYPE  (
   SERVICETYPEID      NUMBER(3,0)                     not null,
   SERVICESUBTYPEID     NUMBER(3,0)                     not null,
   SERVICESUBSUBTYPEID    NUMBER(3,0)                   not null,
   SERVICESUBSUBTYPENAME  VARCHAR2(35)                  not null,
   SERVICESUBSUBTYPEDESC  VARCHAR2(255)                 not null,
   constraint PK_SERVICESUBSUBTYPE primary key (SERVICETYPEID, SERVICESUBTYPEID, SERVICESUBSUBTYPEID)
);

/*==============================================================*/
/* Table: SERVICESUBTYPE                                   */
/*==============================================================*/
create table SERVICESUBTYPE  (
   SERVICETYPEID        NUMBER(3,0)                     not null,
   SERVICESUBTYPEID     NUMBER(3,0)                     not null,
   SERVICESUBTYPENAME   VARCHAR2(35)                    not null,
   SERVICESUBTYPEDESC   VARCHAR2(255)                   not null,
   constraint PK_SERVICESUBTYPE primary key (SERVICETYPEID, SERVICESUBTYPEID)
);

/*==============================================================*/
/* Table: SERVICETYPE                                   */
/*==============================================================*/
create table SERVICETYPE  (
   SERVICETYPEID        NUMBER(3,0)                     not null,
   SERVICETYPENAME      VARCHAR2(35)                    not null,
   SERVICETYPEDESC      VARCHAR2(255)                   not null,
   constraint PK_SERVICETYPE primary key (SERVICETYPEID)
);

/*==============================================================*/
/* Table: SERVPRODLENDERPROFILEASSOC                          */
/*==============================================================*/
create table SERVPRODLENDERPROFILEASSOC  (
   LENDERPROFILEID      NUMBER (13)                     not null,
   SERVICEPRODUCTID     NUMBER(3,0)                     not null,
   constraint PK_SERVPRODLENDERPROFILEASSOC primary key (LENDERPROFILEID, SERVICEPRODUCTID)
);


/*==============================================================*/
/* Table: TIMEOUTDEFINITIONASSOC                              */
/*==============================================================*/
create table TIMEOUTDEFINITIONASSOC  (
   CHANNELID            NUMBER(4,0)                     not null,
   SERVICEPRODUCTID     NUMBER(3,0)                     not null,
   SERVICETIMEOUT       NUMBER(3,0),
   DATXTIMEOUT          NUMBER(3,0),
   constraint PK_TIMEOUTDEFINITIONASSOC primary key (CHANNELID, SERVICEPRODUCTID)
);

/*==============================================================*/
/* Table: SERVICETRANSACTIONTYPE                                      */
/*==============================================================*/
create table SERVICETRANSACTIONTYPE  (
   SERVICETRANSACTIONTYPEID    NUMBER(13,0)                    not null,
   SERVICETRANSACTIONTYPENAME  VARCHAR2(10)                    not null,
   SERVICETRANSACTIONTYPEDESC  VARCHAR2(35)                    not null,
   constraint PK_TRANSACTIONTYPE primary key (SERVICETRANSACTIONTYPEID)
);

alter table ADJUDICATIONAPPLICANT
   add constraint FK_ADJUD_APPL_BORROWER foreign key (COPYID, REQUESTID, BORROWERID)
      references BORROWERREQUESTASSOC (COPYID, REQUESTID, BORROWERID);

alter table ADJUDICATIONREQUEST
   add constraint FK_ADJUDREQUEST_REQUEST foreign key (REQUESTID, COPYID)
      references REQUEST (REQUESTID, COPYID);

alter table ADJUDICATIONRESPONSE
   add constraint FK_ADJUDRESPONSE_ADJUDSTATUS foreign key (ADJUDICATIONSTATUSID)
      references ADJUDICATIONSTATUS (ADJUDICATIONSTATUSID);

alter table ADJUDICATIONRESPONSE
   add constraint FK_ADJUD_RESPONSE_RESPONSE foreign key (RESPONSEID)
      references RESPONSE (RESPONSEID);

alter table APPRAISALSUMMARY
   add constraint FK_APPRAISALSUMM_RESPONSE foreign key (RESPONSEID)
      references RESPONSE (RESPONSEID);

alter table AVMSUMMARY
   add constraint FK_AVMSUMM_RESPONSE foreign key (RESPONSEID)
      references RESPONSE (RESPONSEID);

alter table BORROWERREQUESTASSOC
   add constraint FK_REQUEST_BORROWERREQUEST foreign key (REQUESTID, COPYID)
      references REQUEST (REQUESTID, COPYID);

alter table BORROWERREQUESTASSOC
   add constraint FK_BORROWER_BORROWERREQUEST foreign key (BORROWERID, COPYID)
      references BORROWER (BORROWERID, COPYID);

alter table CHANNEL
   add constraint FK_CHANNEL_CHANNELTYPE foreign key (CHANNELTYPEID)
      references CHANNELTYPE (CHANNELTYPEID);

alter table PAYLOADTYPE
   add constraint FK_PAYLOADT_PAYLOADTY_DOCUMENT foreign key (DOCUMENTTYPEID)
      references DOCUMENTTYPE (DOCUMENTTYPEID);

alter table REQUEST
   add constraint FK_REQUEST_DEAL foreign key (DEALID, COPYID)
      references DEAL (DEALID, COPYID);

alter table REQUEST
   add constraint FK_REQUEST_REQUESTSTATUS foreign key (REQUESTSTATUSID)
      references REQUESTSTATUS (REQUESTSTATUSID);

alter table REQUEST
   add constraint FK_REQUEST_SERVPRODREQASSOC foreign key (CHANNELID, PAYLOADTYPEID, SERVICEPRODUCTID, SERVICETRANSACTIONTYPEID)
      references SERVICEPRODUCTREQUESTASSOC (CHANNELID, PAYLOADTYPEID, SERVICEPRODUCTID, SERVICETRANSACTIONTYPEID);
/*
alter table RESPONSE
   add constraint FK_RESPONSE_REQUEST foreign key (REQUESTID, COPYID)
      references REQUEST (REQUESTID, COPYID);
*/

alter table RESPONSE
   add constraint FK_RESPONSE_RESPONSESTATUS foreign key (RESPONSESTATUSID)
      references RESPONSESTATUS (RESPONSESTATUSID);

alter table SAMADJUDICATIONAPPLICANT
   add constraint FK_SAM_ADJUD_APPLICANT foreign key (REQUESTID, APPLICANTNUMBER, COPYID)
      references ADJUDICATIONAPPLICANT (REQUESTID, APPLICANTNUMBER, COPYID);

alter table SAMDISPOSITION
   add constraint FK_SAMDISP_CODE foreign key (SAMDISPOSITIONCODEID)
      references SAMDISPOSITIONCODE (SAMDISPOSITIONCODEID);

alter table SAMDISPOSITION
   add constraint FK_SAMDISP_STATUS foreign key (SAMDISPOSITIONSTATUSID)
      references SAMDISPOSITIONSTATUS (SAMDISPOSITIONSTATUSID);

alter table SAMDISPOSITION
   add constraint FK_SAMDISP_RESPONSE foreign key (RESPONSEID)
      references SAMRESPONSE (RESPONSEID);

alter table SAMREQUEST
   add constraint FK_SAMREQUEST_ADJUDREQUEST foreign key (REQUESTID, COPYID)
      references ADJUDICATIONREQUEST (REQUESTID, COPYID);

alter table SAMREQUEST
   add constraint FK_SAMREQUEST_SAMAPPLTYPE foreign key (SAMAPPLICATIONTYPEID)
      references SAMAPPLICATIONTYPE (SAMAPPLICATIONTYPEID);

alter table SAMREQUEST
   add constraint FK_SAMREQUEST_SAMPRODTYPE foreign key (SAMPRODUCTTYPEID)
      references SAMPRODUCTTYPE (SAMPRODUCTTYPEID);

alter table SAMREQUEST
   add constraint FK_SAMREQUEST_SAMPROPERTYTYPE foreign key (SAMPROPERTYTYPEID)
      references SAMPROPERTYTYPE (SAMPROPERTYTYPEID);

alter table SAMREQUEST
   add constraint FK_SAMREQUEST_SAMSTEP foreign key (SAMSTEPID)
      references SAMSTEP (SAMSTEPID);

alter table SAMRESPONSE
   add constraint FK_SAMRESP_ADJUDRESPONSE foreign key (RESPONSEID)
      references ADJUDICATIONRESPONSE (RESPONSEID);

alter table SCOTIABANKADDITIONALDETAILS
   add constraint FK_SCOTIABANK_SAD_DEAL foreign key (DEALID, COPYID)
      references DEAL (DEALID, COPYID);

alter table SCOTIABANKADDITIONALDETAILS
   add constraint FK_SCOTIA_RATEPAYOR foreign key (RATEBUYDOWNPAYORID)
      references SCOTIABANKRATEBUYDOWNPAYOR (RATEBUYDOWNPAYORID);

alter table SCOTIABANKSTEPMORTGAGE
   add constraint FK_SCOTIABA_DEAL_SSM_DEAL foreign key (DEALID, COPYID)
      references DEAL (DEALID, COPYID);

alter table SCOTIABANKSTEPMORTGAGE
   add constraint FK_SCOTIA_STEPPROD foreign key (STEPPRODUCTID)
      references SCOTIABANKSTEPPRODUCT (STEPPRODUCTID);

alter table SCOTIABANKSTEPMORTGAGE
   add constraint FK_SCOTIA_STEPMORTTYPE foreign key (STEPMORTGAGETYPEID)
      references SCOTIABANKSTEPMORTGAGETYPE (STEPMORTGAGETYPEID);

alter table SERVICEPRODUCT
   add constraint FK_SERVPROD_SERVPROV foreign key (SERVICEPROVIDERID)
      references SERVICEPROVIDER (SERVICEPROVIDERID);

alter table SERVICEPRODUCT
   add constraint FK_SERVPROD_SERVTYPE3 foreign key (SERVICETYPEID, SERVICESUBTYPEID, SERVICESUBSUBTYPEID)
      references SERVICESUBSUBTYPE (SERVICETYPEID, SERVICESUBTYPEID, SERVICESUBSUBTYPEID);

alter table SERVICEPRODUCT
   add constraint FK_SERVPROD_SERVTYPE2 foreign key (SERVICETYPEID, SERVICESUBTYPEID)
      references SERVICESUBTYPE (SERVICETYPEID, SERVICESUBTYPEID);

alter table SERVICEPRODUCT
   add constraint FK_SERVPROD_SERVTYPE1 foreign key (SERVICETYPEID)
      references SERVICETYPE (SERVICETYPEID);

alter table SERVICEPRODUCTREQUESTASSOC
   add constraint FK_SERVICEPROD_CHANNEL foreign key (CHANNELID)
      references CHANNEL (CHANNELID);

alter table SERVICEPRODUCTREQUESTASSOC
   add constraint FK_SERVICEPROD_PAYLOAD_ foreign key (PAYLOADTYPEID)
      references PAYLOADTYPE (PAYLOADTYPEID);

alter table SERVICEPRODUCTREQUESTASSOC
   add constraint FK_SERVPRODREQASSOC_SERVPROD foreign key (SERVICEPRODUCTID)
      references SERVICEPRODUCT (SERVICEPRODUCTID);

alter table SERVICEPRODUCTREQUESTASSOC
   add constraint FK_SERVPRODREQASSOC_TRANTYPE foreign key (SERVICETRANSACTIONTYPEID)
      references SERVICETRANSACTIONTYPE (SERVICETRANSACTIONTYPEID);
	  
alter table SERVICEPRODUCTREQUESTASSOC ADD CONSTRAINT UK_REQUESTTYPEID UNIQUE (REQUESTTYPEID);
	  
alter table SERVICEPROVIDER
   add constraint FK_SERPROV_CONTACT foreign key (CONTACTID, COPYID)
      references CONTACT (CONTACTID, COPYID);

alter table SERVICEREQUEST
   add constraint FK_SERVICEREQUEST_PROPERTY foreign key (PROPERTYID, COPYID)
      references PROPERTY (PROPERTYID, COPYID); 

alter table SERVICEREQUEST
   add constraint FK_SERVICEREQUEST_REQUEST foreign key (REQUESTID, COPYID)
      references REQUEST (REQUESTID, COPYID);

alter table SERVICEREQUESTCONTACT
   add constraint FK_SERVICE_REQUEST_BORROWER foreign key (COPYID, REQUESTID, BORROWERID)
      references BORROWERREQUESTASSOC (COPYID, REQUESTID, BORROWERID);

alter table SERVICESUBSUBTYPE
   add constraint FK_SERVTYPE2_SERVTYPE3 foreign key (SERVICETYPEID, SERVICESUBTYPEID)
      references SERVICESUBTYPE (SERVICETYPEID, SERVICESUBTYPEID);

alter table SERVICESUBTYPE
   add constraint FK_SERVTYPE1_SERVTYPE2 foreign key (SERVICETYPEID)
      references SERVICETYPE (SERVICETYPEID);

alter table SERVPRODLENDERPROFILEASSOC
   add constraint FK_SERVPROD_LENDORPROF foreign key (LENDERPROFILEID)
      references LENDERPROFILE (LENDERPROFILEID);

alter table SERVPRODLENDERPROFILEASSOC
   add constraint FK_SERVPRODLPA_SERVICEPROD foreign key (SERVICEPRODUCTID)
      references SERVICEPRODUCT (SERVICEPRODUCTID);

alter table TIMEOUTDEFINITIONASSOC
   add constraint FK_TIMEOUT__CHANNEL foreign key (CHANNELID)
      references CHANNEL (CHANNELID);

alter table TIMEOUTDEFINITIONASSOC
   add constraint FK_TIMEOUT__SERVPROD foreign key (SERVICEPRODUCTID)
      references SERVICEPRODUCT (SERVICEPRODUCTID);

