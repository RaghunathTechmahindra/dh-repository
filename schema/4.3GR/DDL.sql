----------------------------------------------------------------------------------------------------
-- DEALLOCKS table for MI Alert
----------------------------------------------------------------------------------------------------
alter table DEALLOCKS add MIRESPONSEEXPECTED CHAR (1) default 'N';


----------------------------------------------------------------------------------------------------
-- DEAL table for Total Loan Amount Update 
----------------------------------------------------------------------------------------------------
alter table DEAL add MIPremiumAmountChangeNortified CHAR (1) default 'Y';

alter table DEAL add MIPremiumAmountPrevious NUMBER (13,2) default 0;

----------------------------------------------------------------------------------------------------
-- DOCUMENTTRACKING table for use default checkbox and display default
----------------------------------------------------------------------------------------------------
ALTER TABLE DOCUMENTTRACKING
ADD (USEDEFAULTCHECKED VARCHAR(1) DEFAULT 'N',
     DISPLAYDEFAULT VARCHAR(1) DEFAULT 'N');