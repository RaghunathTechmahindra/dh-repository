/*==============================================================*/
/* Table: SecurityPolicy                                        */
/*==============================================================*/
INSERT INTO securitypolicy VALUES (10, 'Password Consecutive Characters', 3, SYSDATE, 0, 0);
INSERT INTO securitypolicy VALUES (11, 'Password Character Sequencing', 4, SYSDATE, 0, 0);

INSERT INTO sysproperty VALUES (4, 'com.basis100.deal.ame.user.id', 0, 'AUTO_1','v4.3', SYSDATE, 'AME system user id', NULL);
INSERT INTO sysproperty VALUES (4, 'com.basis100.deal.ame.password', 0, 'Password_1','v4.3', SYSDATE, 'AME system user password', NULL);

INSERT INTO sysproperty VALUES (6, 'com.filogix.ecm.user.id', 0, 'ECM_1','v4.3', SYSDATE, 'ECM system user id', NULL);
INSERT INTO sysproperty VALUES (6, 'com.filogix.ecm.password', 0, 'Password_1','v4.3', SYSDATE, 'ECM system user password', NULL);


/*==============================================================*/
/* Table: SysProperty  for Preapproved firmup deal condition reset
/*==============================================================*/
INSERT INTO SysProperty VALUES(3, 'com.filogix.ingestion.preappfirm.conditions.reset', 0, 'N', 'V4.3', SYSDATE, 'Option to define whether the institution supports Conditions Maintained on Firm Up functionality. Each institution in Multi-Institution environment will have its own value', null);

/*==============================================================*/
/* Table: SysProperty  for Routing UW for Decliened deal resubmission
/*==============================================================*/
INSERT INTO SysProperty VALUES(3, 'ingestion.route.declined.resubmission.to.original.uw', 0, 'N', 'V4.3', SYSDATE, 'Option to route non-IC-Reject declined resubmissions to original underwriter', null);
INSERT INTO SysProperty VALUES(3, 'ingestion.route.declined.resubmission.to.original.uw.lob', 0, 'N', 'V4.3', SYSDATE, 'Option to route declined resubmissions to original underwriter only if LOB matches', null);

/*==============================================================*/
/* Table: SysProperty for Exchange instance URLs for Exchange Link
/*        Thses are QA/UAT instances, replace with produdction.
/*==============================================================*/
Insert into SYSPROPERTY Values (0, 'com.filogix.mosapp.ExchangeURLLink.English', 0, 'https://uat2.exchange.filogix.com/les971utf8qa/livelink', '4.3', sysdate, 'URL to exchange instance, english', NULL);
Insert into SYSPROPERTY Values (0, 'com.filogix.mosapp.ExchangeURLLink.French', 0, 'https://uat2.exchange.filogix.com/les971utf8qafr/livelink', '4.3', sysdate, 'URL to exchange instance, french', NULL);

/*==============================================================*/
/* Table: SysProperty for Duplicate Default Conditions
/*==============================================================*/
Insert into SYSPROPERTY Values (0, 'duplicate.default.conditions', 0, 'Y', '4.3', sysdate, 'enable default duplicate conditions check', NULL);
Insert into SYSPROPERTY Values (0, 'duplicate.default.conditions.expanded', 0, 'Y', '4.3', sysdate, 'enable default duplicate conditions full display instead of compact display', NULL);

----------------------------------------------------------------------------------------------------
-- SYSPROPERTY table for specifies the frequency of polling in seconds
----------------------------------------------------------------------------------------------------
Insert into SYSPROPERTY
   (SYSPROPERTYTYPEID, NAME, INSTITUTIONPROFILEID, VALUE, BUILD_VERSION,
    CREATED_DATE, DESCRIPTION, NOTES)
 Values
   (1, 'com.express.mosapp.freqPollingMIResponse', 2, '10', '4.3',
    sysdate, 'Number value that specifies the frequency of polling in seconds', NULL);

Insert into SYSPROPERTY
   (SYSPROPERTYTYPEID, NAME, INSTITUTIONPROFILEID, VALUE, BUILD_VERSION,
    CREATED_DATE, DESCRIPTION, NOTES)
 Values
   (1, 'com.express.mosapp.freqPollingMIResponse', 0, '10', '4.3',
    sysdate, 'Number value that specifies the frequency of polling in seconds', NULL);

Insert into SYSPROPERTY
   (SYSPROPERTYTYPEID, NAME, INSTITUTIONPROFILEID, VALUE, BUILD_VERSION,
    CREATED_DATE, DESCRIPTION, NOTES)
 Values
   (1, 'com.express.mosapp.freqPollingMIResponse', 1, '10', '4.3',
    sysdate, 'Number value that specifies the frequency of polling in seconds', NULL);

--------------------------------------------------------------------------------------------------------------
-- SYSPROPERTY table for specifies if Express provide the MI response notification facility for Genworth deals
--------------------------------------------------------------------------------------------------------------

Insert into SYSPROPERTY
   (SYSPROPERTYTYPEID, NAME, INSTITUTIONPROFILEID, VALUE, BUILD_VERSION,
    CREATED_DATE, DESCRIPTION, NOTES)
 Values
   (1, 'com.filogix.mosapp.MIResponseAlert.Genworth', 0, 'No', '4.3',
    sysdate, 'Express provide the MI response notification facility for Genworth deals. Values: Yes/No. Default: No', NULL);

Insert into SYSPROPERTY
   (SYSPROPERTYTYPEID, NAME, INSTITUTIONPROFILEID, VALUE, BUILD_VERSION,
    CREATED_DATE, DESCRIPTION, NOTES)
 Values
   (1, 'com.filogix.mosapp.MIResponseAlert.Genworth', 1, 'No', '4.3',
    sysdate, 'Express provide the MI response notification facility for Genworth deals. Values: Yes/No. Default: No', NULL);

Insert into SYSPROPERTY
   (SYSPROPERTYTYPEID, NAME, INSTITUTIONPROFILEID, VALUE, BUILD_VERSION,
    CREATED_DATE, DESCRIPTION, NOTES)
 Values
   (1, 'com.filogix.mosapp.MIResponseAlert.Genworth', 2, 'No', '4.3',
    sysdate, 'Express provide the MI response notification facility for Genworth deals. Values: Yes/No. Default: No', NULL);

--------------------------------------------------------------------------------------------------------------
-- SYSPROPERTY table for specifies if Express provide the MI response notification facility for CMHC deals
--------------------------------------------------------------------------------------------------------------

Insert into SYSPROPERTY
   (SYSPROPERTYTYPEID, NAME, INSTITUTIONPROFILEID, VALUE, BUILD_VERSION,
    CREATED_DATE, DESCRIPTION, NOTES)
 Values
   (1, 'com.filogix.mosapp.MIResponseAlert.CMHC', 0, 'No', '4.3',
   sysdate, 'Express provide the MI response notification facility for CMHC deals. Values: Yes/No. Default: No', NULL);

Insert into SYSPROPERTY
   (SYSPROPERTYTYPEID, NAME, INSTITUTIONPROFILEID, VALUE, BUILD_VERSION,
    CREATED_DATE, DESCRIPTION, NOTES)
 Values
   (1, 'com.filogix.mosapp.MIResponseAlert.CMHC', 1, 'No', '4.3',
    sysdate, 'Express provide the MI response notification facility for CMHC deals. Values: Yes/No. Default: No', NULL);

Insert into SYSPROPERTY
   (SYSPROPERTYTYPEID, NAME, INSTITUTIONPROFILEID, VALUE, BUILD_VERSION,
    CREATED_DATE, DESCRIPTION, NOTES)
 Values
   (1, 'com.filogix.mosapp.MIResponseAlert.CMHC', 2, 'No', '4.3',
    sysdate, 'Express provide the MI response notification facility for CMHC deals. Values: Yes/No. Default: No', NULL);

--------------------------------------------------------------------------------------------------------------
-- FXP26730 - enable ingestion to consider product types when choosing products with the non-ratesync method
--------------------------------------------------------------------------------------------------------------
Insert into SYSPROPERTY Values (0, 'com.filogix.ingestion.useproducttype', 0, 'N', '4.3', sysdate, 'enable product choice using ingested product type', NULL);

--------------------------------------------------------------------------------------------------------------
-- FXP28199 - Adding list of pages where we'd like to see instant MI response alert.
--------------------------------------------------------------------------------------------------------------
Insert into SYSPROPERTY
   (SYSPROPERTYTYPEID, NAME, INSTITUTIONPROFILEID, VALUE, BUILD_VERSION,
    CREATED_DATE, DESCRIPTION, NOTES)

Insert into SYSPROPERTY Values (1, 'com.filogix.express.mialert.pageid', 0, '17,500,14,535,519,21,70,19,15,38,43,22', '4.3',
     sysdate, 'List of pages that should show MI response alert.', NULL );

Insert into SYSPROPERTY Values (1, 'com.filogix.express.mialert.pageid', 1, '17,500,14,535,519,21,70,19,15,38,43,22', '4.3',
     sysdate, 'List of pages that should show MI response alert.', NULL );

Insert into SYSPROPERTY Values (1, 'com.filogix.express.mialert.pageid', 2, '17,500,14,535,519,21,70,19,15,38,43,22', '4.3',
     sysdate, 'List of pages that should show MI response alert.', NULL );

--------------------------------------------------------------------------------------------------------------
-- CIBC Performance Enhancement properties
--------------------------------------------------------------------------------------------------------------
Insert into SYSPROPERTY Values (0, 'com.filogix.conditions.useRegExParser', 0, 'N', '4.3', sysdate, 'use Condition Reg Expression Parser', 'is used for all inititutions');


