----------------------------------------------------------------------------------------------------
-- DEALLOCKS table for MI Alert
----------------------------------------------------------------------------------------------------
alter table DEALLOCKS drop column MIRESPONSEEXPECTED;


----------------------------------------------------------------------------------------------------
-- DEALLOCKS table for MI Alert
----------------------------------------------------------------------------------------------------
alter table DEAL drop column MIPREMIUMAMOUNTCHANGENORTIFIED;

alter table DEAL drop column MIPREMIUMAMOUNTPREVIOUS;

----------------------------------------------------------------------------------------------------
-- DOCUMENTTRACKING table for use default checkbox and display default
----------------------------------------------------------------------------------------------------
ALTER TABLE DOCUMENTTRACKING
DROP (USEDEFAULTCHECKED,
     DISPLAYDEFAULT);
 


