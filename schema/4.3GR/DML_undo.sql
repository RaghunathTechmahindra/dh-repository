/*==============================================================*/
/* Table: SecurityPolicy                                        */
/*==============================================================*/
DELETE FROM securitypolicy WHERE institutionprofileid = 0 AND securitypolicyid = 10;
DELETE FROM securitypolicy WHERE institutionprofileid = 0 AND securitypolicyid = 11;

DELETE FROM sysproperty WHERE syspropertytypeid=4 AND name='com.basis100.deal.ame.user.id';
DELETE FROM sysproperty WHERE syspropertytypeid=4 AND name='com.basis100.deal.ame.password';               

DELETE FROM sysproperty WHERE syspropertytypeid=6 AND name='com.filogix.ecm.user.id';
DELETE FROM sysproperty WHERE syspropertytypeid=6 AND name='com.filogix.ecm.password';               


/*==============================================================*/
/* Table: SysProperty  for Preapproved firmup deal condition reset                                                                                                        
/*==============================================================*/
DELETE FROM SysProperty where name = 'com.filogix.ingestion.preappfirm.conditions.reset';


/*==============================================================*/
/* Table: SysProperty  for Routing UW for Decliened deal resubmission
               
/*==============================================================*/
DELETE FROM SysProperty where name = 'ingestion.route.declined.resubmission.to.original.uw';
DELETE FROM SysProperty where name = 'ingestion.route.declined.resubmission.to.original.uw.lob';


/*==============================================================*/
/* Table: SysProperty  for MI Response Alert
               
/*==============================================================*/
DELETE FROM SysProperty where name = 'com.express.mosapp.freqPollingMIResponse';
DELETE FROM SysProperty where name = 'com.filogix.mosapp.MIResponseAlert.Genworth';
DELETE FROM SysProperty where name = 'com.filogix.mosapp.MIResponseAlert.CMHC';


--------------------------------------------------------------------------------------------------------------
-- CIBC Performance Enhancement properties
--------------------------------------------------------------------------------------------------------------
DELETE FROM SysProperty where name = 'com.filogix.conditions.useRegExParser';
