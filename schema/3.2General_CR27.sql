---------------------- 1) turn on address scrubbing updates on Ingestion

UPDATE ADDSCRUBCONFIG 
   SET STREETADDRESSLINE = 'Y', 
       CITY = 'Y', 
       PROVINCE = 'Y', 
       POSTALCODE = 'Y';
COMMIT;	

---------------------- 2) configure connection to DatX

INSERT INTO CHANNEL ( CHANNELID, CHANNELTYPEID, PROTOCOL, IP, PORT, USERID, PASSWORD, PATH, WSSERVICEPORT) 
VALUES (10, 2, 'http', '10.1.1.42', '8080', 'express_scrubber', 'pwd_scrubber_express', '/AddressScrubber/services/AddressScrubber', 'AddressScrubber');

INSERT INTO DOCUMENTTYPE
VALUES (97, 'Address Scrub REQUEST');
 
INSERT INTO PAYLOADTYPE (  PAYLOADTYPEID, DOCUMENTTYPEID, PAYLOADTYPEDESC, PAYLOADVERSION, XMLSCHEMANAME, NONAMESCHEMALOCATION,   NONAMEXMLSCHEMALOCATION) 
VALUES (24, 97, 'Address Scrub REQUEST', 1, 'AddressScrub', null, null);

COMMIT;
