------------------------------------------------------ Delete existing users for WS  ------------------------------------------------------ 

-- DELETE FROM usersecuritypolicy WHERE userid = (select userprofileid from userprofile where userlogin = 'DATX_APP');
-- DELETE FROM usersecuritypolicy WHERE userid = (select userprofileid from userprofile where userlogin = 'DATX_CLOSING');
-- DELETE FROM usersecuritypolicy WHERE userid = (select userprofileid from userprofile where userlogin = 'DATX_MI');

-- DELETE FROM userprofile WHERE userlogin = 'DATX_APP';
-- DELETE FROM userprofile WHERE userlogin = 'DATX_CLOSING';
-- DELETE FROM userprofile WHERE userlogin = 'DATX_MI';



------------------------------------------------------ ADD users for WS  ------------------------------------------------------ 
insert into userprofile 
(userprofileid, userlogin, usertypeid, groupprofileid, standardaccess, bilingual) 
values (((select max(userprofileid) from userprofile) + 1), 'DATX_APP', 97, 1, 'Y','N');

insert into userprofile 
(userprofileid, userlogin, usertypeid, groupprofileid, standardaccess, bilingual) 
values (((select max(userprofileid) from userprofile) + 1), 'DATX_CLOSING', 97, 1, 'Y','N');

insert into userprofile 
(userprofileid, userlogin, usertypeid, groupprofileid, standardaccess, bilingual) 
values (((select max(userprofileid) from userprofile) + 1), 'DATX_MI', 97, 1, 'Y','N');


insert into usersecuritypolicy
(userid) values ((select userprofileid from userprofile where userlogin = 'DATX_APP') );

insert into usersecuritypolicy
(userid) values ((select userprofileid from userprofile where userlogin = 'DATX_CLOSING') );

insert into usersecuritypolicy
(userid) values ((select userprofileid from userprofile where userlogin = 'DATX_MI') );

commit;
