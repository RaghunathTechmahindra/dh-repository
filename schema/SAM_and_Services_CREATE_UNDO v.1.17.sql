DROP SEQUENCE REQUESTSEQ; 

DROP SEQUENCE RESPONSESEQ; 

DROP SEQUENCE SERVICEPRODUCTSEQ; 

DROP SEQUENCE SERVICEREQUESTCONTACTSEQ; 

alter table ADJUDICATIONAPPLICANT
   drop constraint FK_ADJUD_APPL_BORROWER;

alter table ADJUDICATIONREQUEST
   drop constraint FK_ADJUDREQUEST_REQUEST;

alter table ADJUDICATIONRESPONSE
   drop constraint FK_ADJUDRESPONSE_ADJUDSTATUS;

alter table ADJUDICATIONRESPONSE
   drop constraint FK_ADJUD_RESPONSE_RESPONSE;

alter table APPRAISALSUMMARY
   drop constraint FK_APPRAISALSUMM_RESPONSE;

alter table AVMSUMMARY
   drop constraint FK_AVMSUMM_RESPONSE;

alter table BORROWERREQUESTASSOC
   drop constraint FK_REQUEST_BORROWERREQUEST;

alter table BORROWERREQUESTASSOC
   drop constraint FK_BORROWER_BORROWERREQUEST;

alter table CHANNEL
   drop constraint FK_CHANNEL_CHANNELTYPE;

alter table PAYLOADTYPE
   drop constraint FK_PAYLOADT_PAYLOADTY_DOCUMENT;

alter table REQUEST
   drop constraint FK_REQUEST_DEAL;

alter table REQUEST
   drop constraint FK_REQUEST_REQUESTSTATUS;

alter table REQUEST
   drop constraint FK_REQUEST_SERVPRODREQASSOC;
/*
alter table RESPONSE
   drop constraint FK_RESPONSE_REQUEST;
*/

alter table RESPONSE
   drop constraint FK_RESPONSE_RESPONSESTATUS;

alter table SAMADJUDICATIONAPPLICANT
   drop constraint FK_SAM_ADJUD_APPLICANT;

alter table SAMDISPOSITION
   drop constraint FK_SAMDISP_CODE;

alter table SAMDISPOSITION
   drop constraint FK_SAMDISP_STATUS;

alter table SAMDISPOSITION
   drop constraint FK_SAMDISP_RESPONSE;

alter table SAMREQUEST
   drop constraint FK_SAMREQUEST_ADJUDREQUEST;

alter table SAMREQUEST
   drop constraint FK_SAMREQUEST_SAMAPPLTYPE;

alter table SAMREQUEST
   drop constraint FK_SAMREQUEST_SAMPRODTYPE;

alter table SAMREQUEST
   drop constraint FK_SAMREQUEST_SAMPROPERTYTYPE;

alter table SAMREQUEST
   drop constraint FK_SAMREQUEST_SAMSTEP;

alter table SAMRESPONSE
   drop constraint FK_SAMRESP_ADJUDRESPONSE;

alter table SCOTIABANKADDITIONALDETAILS
   drop constraint FK_SCOTIABANK_SAD_DEAL;

alter table SCOTIABANKADDITIONALDETAILS
   drop constraint FK_SCOTIA_RATEPAYOR;

alter table SCOTIABANKSTEPMORTGAGE
   drop constraint FK_SCOTIABA_DEAL_SSM_DEAL;

alter table SCOTIABANKSTEPMORTGAGE
   drop constraint FK_SCOTIA_STEPPROD;

alter table SCOTIABANKSTEPMORTGAGE
   drop constraint FK_SCOTIA_STEPMORTTYPE;

alter table SERVICEPRODUCT
   drop constraint FK_SERVPROD_SERVPROV;

alter table SERVICEPRODUCT
   drop constraint FK_SERVPROD_SERVTYPE3;

alter table SERVICEPRODUCT
   drop constraint FK_SERVPROD_SERVTYPE2;

alter table SERVICEPRODUCT
   drop constraint FK_SERVPROD_SERVTYPE1;

alter table SERVICEPRODUCTREQUESTASSOC
   drop constraint FK_SERVICEPROD_CHANNEL;

alter table SERVICEPRODUCTREQUESTASSOC
   drop constraint FK_SERVICEPROD_PAYLOAD_;

alter table SERVICEPRODUCTREQUESTASSOC
   drop constraint FK_SERVPRODREQASSOC_SERVPROD;

alter table SERVICEPRODUCTREQUESTASSOC
   drop constraint FK_SERVPRODREQASSOC_TRANTYPE;

alter table SERVICEPROVIDER
   drop constraint FK_SERPROV_CONTACT;

alter table SERVICEREQUEST
   drop constraint FK_SERVICEREQUEST_PROPERTY;

alter table SERVICEREQUEST
   drop constraint FK_SERVICEREQUEST_REQUEST;

alter table SERVICEREQUESTCONTACT
   drop constraint FK_SERVICE_REQUEST_BORROWER;

alter table SERVICESUBSUBTYPE
   drop constraint FK_SERVTYPE2_SERVTYPE3;

alter table SERVICESUBTYPE
   drop constraint FK_SERVTYPE1_SERVTYPE2;

alter table SERVPRODLENDERPROFILEASSOC
   drop constraint FK_SERVPROD_LENDORPROF;

alter table SERVPRODLENDERPROFILEASSOC
   drop constraint FK_SERVPRODLPA_SERVICEPROD;

alter table TIMEOUTDEFINITIONASSOC
   drop constraint FK_TIMEOUT__CHANNEL;

alter table TIMEOUTDEFINITIONASSOC
   drop constraint FK_TIMEOUT__SERVPROD;

drop table ADJUDICATIONAPPLICANT cascade constraints;

drop table ADJUDICATIONREQUEST cascade constraints;

drop table ADJUDICATIONRESPONSE cascade constraints;

drop table ADJUDICATIONSTATUS cascade constraints;

drop table APPRAISALSUMMARY cascade constraints;

drop table AVMSUMMARY cascade constraints;

drop table BORROWERREQUESTASSOC cascade constraints;

drop table CHANNEL cascade constraints;

drop table CHANNELTYPE cascade constraints;

drop table PAYLOADTYPE cascade constraints;

drop table REQUEST cascade constraints;

drop table REQUESTSTATUS cascade constraints;

drop table RESPONSE cascade constraints;

drop table RESPONSESTATUS cascade constraints;

drop table SAMADJUDICATIONAPPLICANT cascade constraints;

drop table SAMAPPLICATIONTYPE cascade constraints;

drop table SAMDISPOSITION cascade constraints;

drop table SAMDISPOSITIONCODE cascade constraints;

drop table SAMDISPOSITIONSTATUS cascade constraints;

drop table SAMPRODUCTTYPE cascade constraints;

drop table SAMPROPERTYTYPE cascade constraints;

drop table SAMREQUEST cascade constraints;

drop table SAMRESPONSE cascade constraints;

drop table SAMSTEP cascade constraints;

drop table SCOTIABANKADDITIONALDETAILS cascade constraints;

drop table SCOTIABANKRATEBUYDOWNPAYOR cascade constraints;

drop table SCOTIABANKSTEPMORTGAGE cascade constraints;

drop table SCOTIABANKSTEPMORTGAGETYPE cascade constraints;

drop table SCOTIABANKSTEPPRODUCT cascade constraints;

drop table SERVICEPRODUCT cascade constraints;

drop table SERVICEPRODUCTREQUESTASSOC cascade constraints;

drop table SERVICEPROVIDER cascade constraints;

drop table SERVICEREQUEST cascade constraints;

drop table SERVICEREQUESTCONTACT cascade constraints;

drop table SERVICESUBSUBTYPE cascade constraints;

drop table SERVICESUBTYPE cascade constraints;

drop table SERVICETYPE cascade constraints;

drop table SERVPRODLENDERPROFILEASSOC cascade constraints;

drop table TIMEOUTDEFINITIONASSOC cascade constraints;

drop table SERVICETRANSACTIONTYPE cascade constraints;
