-- 4.4 Simple Entity Cache : on/off switch
INSERT INTO sysproperty
            (institutionprofileid, syspropertytypeid, NAME, VALUE,
             build_version, created_date, description, notes)
   (SELECT institutionprofileid, 1,
           'com.basis100.deal.entity.simpleentitycache', 'Y', '4.4',
           SYSDATE, NULL, 'this is only available in GUI, Default value is N'
      FROM institutionprofile);

-- 4.4 Copy Mgt : on/off switch
INSERT INTO sysproperty
            (institutionprofileid, syspropertytypeid, NAME, VALUE,
             build_version, created_date, description, notes)
   (SELECT institutionprofileid, 0, 'com.basis100.deal.entity.copyWithStoredProc',
           'Y', '4.4', SYSDATE, NULL, 'this is available in all Express sub programs as well as GUI , default value is Y'
      FROM institutionprofile);

--DocumentTrackingVerbiage data migration
update documenttrackingverbiage
set DocumentTextLite=documenttext
where documenttext is not null and 
      length(documenttext)<=4000;

update documenttrackingverbiage
set DocumentText=null
where DocumentTextLite is not null;

commit;

--Transaction History calc-tracking on/off.
INSERT INTO sysproperty
            (institutionprofileid, syspropertytypeid, NAME, VALUE,
             build_version, created_date, description, notes)
   (SELECT institutionprofileid, 1, 'com.filogix.transactionhistory.trackcalcs', 'N', '4.4', 
   	SYSDATE, 'Turn on tracking of calcs in transaction history.', 'Y = on, off otherwise.  Default off.'
      FROM institutionprofile);





--INDIVIDUAL WORK QUEUE SEARCH: Adding maximum number of individual work queue tasks shown
INSERT INTO sysproperty VALUES (1, 'com.filogix.individualworkqueue.numofrows', 0, '200', NULL, SYSDATE, 'Maximum number of tasks appear on a single page for IWQ page', NULL);
INSERT INTO sysproperty VALUES (1, 'com.filogix.individualworkqueue.numofrows', 1, '200', NULL, SYSDATE, 'Maximum number of tasks appear on a single page for IWQ page', NULL);
INSERT INTO sysproperty VALUES (1, 'com.filogix.individualworkqueue.numofrows', 2, '200', NULL, SYSDATE, 'Maximum number of tasks appear on a single page for IWQ page', NULL);
COMMIT;

--MASTER WORK QUEUE SEARCH: Changing maximum number of tasks shown
UPDATE sysproperty SET value='200' WHERE name='com.basis100.masterworkqueue.numofrows';
COMMIT;

--MASTER WORK QUEUE SEARCH: Adding new page
INSERT INTO page (pageid, pagename, pagelabel, editable, editviatxcopy, deallockonedit, alloweditwhenlocked, dealpage, includeingoto, institutionprofileid ) VALUES (537, 'pgMWorkQueueSearch', 'Master Work Queue Search', 'N', 'N', 'N', 'N', 'N', 'Y', 0);
INSERT INTO page (pageid, pagename, pagelabel, editable, editviatxcopy, deallockonedit, alloweditwhenlocked, dealpage, includeingoto, institutionprofileid ) VALUES (537, 'pgMWorkQueueSearch', 'Master Work Queue Search', 'N', 'N', 'N', 'N', 'N', 'Y', 1);
INSERT INTO page (pageid, pagename, pagelabel, editable, editviatxcopy, deallockonedit, alloweditwhenlocked, dealpage, includeingoto, institutionprofileid ) VALUES (537, 'pgMWorkQueueSearch', 'Master Work Queue Search', 'N', 'N', 'N', 'N', 'N', 'Y', 2);
COMMIT;

--MASTER WORK QUEUE SEARCH: Adding user access for new page
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 1, 537, 0);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 2, 537, 0);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 3, 537, 0);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 4, 537, 0);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 5, 537, 0);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 6, 537, 0);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 7, 537, 0);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (3, 8, 537, 0);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (3, 9, 537, 0);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (3, 10, 537, 0);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (3, 11, 537, 0);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (3, 12, 537, 0);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (3, 13, 537, 0);

INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 1, 537, 1);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 2, 537, 1);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 3, 537, 1);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 4, 537, 1);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 5, 537, 1);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 6, 537, 1);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 7, 537, 1);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (3, 8, 537, 1);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (3, 9, 537, 1);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (3, 10, 537, 1);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (3, 11, 537, 1);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (3, 12, 537, 1);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (3, 13, 537, 1);

INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 1, 537, 2);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 2, 537, 2);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 3, 537, 2);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 4, 537, 2);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 5, 537, 2);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 6, 537, 2);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (1, 7, 537, 2);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (3, 8, 537, 2);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (3, 9, 537, 2);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (3, 10, 537, 2);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (3, 11, 537, 2);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (3, 12, 537, 2);
INSERT INTO usertypepageaccess (accesstypeid, usertypeid, pageid, institutionprofileid) VALUES (3, 13, 537, 2);

COMMIT;


--DocumentTrackingVerbiage data migration

update documenttrackingverbiage
set DocumentTextLite=documenttext
where documenttext is not null and 
      length(documenttext)<=4000;

update documenttrackingverbiage
set DocumentText=null
where DocumentTextLite is not null;

commit;


