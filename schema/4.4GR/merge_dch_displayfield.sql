create table temp_dch 
(
  DEALCHANGEHISTORYID   NUMBER(13),
  APPLICATIONID         NUMBER(13),
  TRANSACTIONDATE       DATE,
  DCHUSERPROFILEID      NUMBER(13),
  HISTORYFILENAMEID     NUMBER(13),
  CONTEXTTEXTSOURCE     VARCHAR2(35 BYTE),
  CONTEXTTEXT           VARCHAR2(35 BYTE),
  CURRENTVALUE          NUMBER(24,6),
  PREVIOUSVALUE         NUMBER(24,6),
  CURRENTVALUETEXT      VARCHAR2(255 BYTE),
  PREVIOUSVALUETEXT     VARCHAR2(255 BYTE),
  SCENARIOCONTEXT       VARCHAR2(255 BYTE),
  AID                   NUMBER(13),
  INSTITUTIONPROFILEID  NUMBER(13),
  ENTITYNAME            VARCHAR2(35 BYTE),
  ATTRIBUTENAME         VARCHAR2(35 BYTE),
  DISPLAYFIELDNAME      VARCHAR2(255 BYTE)
)
TABLESPACE MOS_DATA
PCTUSED    40
PCTFREE    10
INITRANS   5
MAXTRANS   255
PARTITION BY LIST (INSTITUTIONPROFILEID) 
(  
  PARTITION P_INSTITUTION_1 VALUES (0)
    LOGGING
    NOCOMPRESS
    TABLESPACE MOS_DATA
    PCTFREE    10
    INITRANS   5
    MAXTRANS   255
    STORAGE    (
                INITIAL          100M
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_INSTITUTION_2 VALUES (1)
    LOGGING
    NOCOMPRESS
    TABLESPACE MOS_DATA
    PCTFREE    10
    INITRANS   5
    MAXTRANS   255
    STORAGE    (
                INITIAL          100M
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                BUFFER_POOL      DEFAULT
               )
)
NOCOMPRESS 
NOCACHE
NOPARALLEL;

insert into temp_dch
select d.*,f.ENTITYNAME,f.ATTRIBUTENAME,f.DISPLAYFIELDNAME
from dealchangehistory d, displayfield f
where d.historyfilenameid=f.displayfieldid(+) and
      d.institutionprofileid=f.institutionprofileid(+);

commit;

rename dealchangehistory to old_dch;
alter table old_dch drop constraint pk_0188;
alter table old_dch drop constraint fk_0317;
alter table old_dch drop constraint sys_c0024574;
alter table old_dch drop constraint sys_c0024575;
drop index I_fk_0317;

rename temp_DCH  to dealchangehistory;

CREATE INDEX I_FK_0317 ON DEALCHANGEHISTORY
(INSTITUTIONPROFILEID, DCHUSERPROFILEID)
LOGGING
TABLESPACE MOS_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          100M
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX PK_0188 ON DEALCHANGEHISTORY
(INSTITUTIONPROFILEID, DEALCHANGEHISTORYID)
  TABLESPACE MOS_INDEX
  INITRANS   2
  MAXTRANS   255
LOCAL (  
  PARTITION P_INSTITUTION_1
    LOGGING
    NOCOMPRESS
    TABLESPACE MOS_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          100M
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_INSTITUTION_2
    LOGGING
    NOCOMPRESS
    TABLESPACE MOS_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          50M
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                BUFFER_POOL      DEFAULT
               )
)
NOPARALLEL;


ALTER TABLE DEALCHANGEHISTORY ADD (
  CONSTRAINT PK_0188
 PRIMARY KEY
 (INSTITUTIONPROFILEID, DEALCHANGEHISTORYID)
 USING INDEX LOCAL);

/*
ALTER TABLE DEALCHANGEHISTORY ADD (
  CONSTRAINT FK_0317 
 FOREIGN KEY (INSTITUTIONPROFILEID, DCHUSERPROFILEID) 
 REFERENCES USERPROFILE (INSTITUTIONPROFILEID,USERPROFILEID));
*/
rename displayfield to old_displayfield;
