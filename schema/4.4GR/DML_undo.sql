-- 4.4 Simple Entity Cache : on/off
DELETE FROM sysproperty
      WHERE NAME LIKE 'com.basis100.deal.entity.simpleentitycache'

-- 4.4 Copy Mgt : on/off
DELETE FROM sysproperty
      WHERE NAME LIKE 'com.basis100.deal.entity.copyWithStoredProc';

--DocumentTrackingVerbiage (Use of CLOB)
update documenttrackingverbiage
set documenttext=DocumentTextLite
where documenttext is null


commit;


--INDIVIDUAL WORK QUEUE SEARCH: Adding maximum number of individual work queue tasks shown
DELETE FROM sysproperty WHERE name='com.filogix.individualworkqueue.numofrows';
COMMIT;

--MASTER WORK QUEUE SEARCH: Changing maximum number of tasks shown
UPDATE sysproperty SET value='100' WHERE name='com.basis100.masterworkqueue.numofrows';
COMMIT;

--MASTER WORK QUEUE SEARCH: Removing user access for master work queue search page
DELETE FROM usertypepageaccess WHERE pageid=537;

--MASTER WORK QUEUE SEARCH: Removing master work queue search page
DELETE FROM page WHERE institutionprofileid=0 AND pageid=537;
DELETE FROM page WHERE institutionprofileid=1 AND pageid=537;
DELETE FROM page WHERE institutionprofileid=2 AND pageid=537;

COMMIT;

--DocumentTrackingVerbiage (Use of CLOB)
update documenttrackingverbiage
set documenttext=DocumentTextLite
where documenttext is null

commit;
