----------------------------------------------------------------
-- DocumentTrackingVerbiage (Use of CLOB)
----------------------------------------------------------------
alter table DOCUMENTTRACKINGVERBIAGE modify DOCUMENTTEXT not null;
alter table DOCUMENTTRACKINGVERBIAGE drop column DocumentTextLite;


----------------------------------------------------------------------------------------------------
-- DEAL
----------------------------------------------------------------------------------------------------
ALTER TABLE DEAL DROP COLUMN FINANCINGWAIVERDATE;

----------------------------------------------------------------------------------------------------
-- SUFFIX
----------------------------------------------------------------------------------------------------
DROP TABLE SUFFIX;

----------------------------------------------------------------------------------------------------
-- BORROWER
----------------------------------------------------------------------------------------------------
ALTER TABLE BORROWER DROP COLUMN BORROWERCELLPHONENUMBER;

ALTER TABLE BORROWER DROP COLUMN SUFFIXID;

ALTER TABLE BORROWER DROP COLUMN CBAUTHORIZATIONDATE;

ALTER TABLE BORROWER DROP COLUMN CBAUTHORIZATIONMETHOD;

----------------------------------------------------------------------------------------------------
-- LIABILITY
----------------------------------------------------------------------------------------------------
ALTER TABLE LIABILITY
DROP (CREDITLIMIT, MATURITYDATE, CREDITBUREAURECORDINDICATOR);
 

----------------------------------------------------------------------------------------------------
-- SOURCEFIRMPROFILE
----------------------------------------------------------------------------------------------------
ALTER TABLE SOURCEFIRMPROFILE DROP COLUMN SFLICENSEREGISTRATIONNUMBER;


----------------------------------------------------------------------------------------------------
-- SOURCEOFBUSINESSPROFILE
----------------------------------------------------------------------------------------------------
ALTER TABLE SOURCEOFBUSINESSPROFILE DROP COLUMN SOBLICENSEREGISTRATIONNUMBER;

----------------------------------------------------------------
-- Submission Agent column in deal.
----------------------------------------------------------------
ALTER TABLE deal DROP COLUMN sourceOfBusinessProfile2ndary;

----------------------------------------------------------------
-- DocumentTrackingVerbiage (Use of CLOB)
----------------------------------------------------------------
alter table DOCUMENTTRACKINGVERBIAGE modify DOCUMENTTEXT not null;
alter table DOCUMENTTRACKINGVERBIAGE drop column DocumentTextLite;