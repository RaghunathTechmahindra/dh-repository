-- add channel for Exchange2.

insert into ChannelType values (9, 'ESB');
insert into channel values (16, 9, 'http', '10.1.201.39', '3045', 'roy', 'secret', 'CreateLenderFolder', null, 0);
insert into channel values (16, 9, 'http', '10.1.201.39', '3045', 'roy', 'secret', 'CreateLenderFolder', null, 1);
insert into channel values (16, 9, 'http', '10.1.201.39', '3045', 'roy', 'secret', 'CreateLenderFolder', null, 2);


-- add payloadType for Exchange2.
insert into documentType values (98, 'General WS Client payload', 0);
insert into documentType values (98, 'General WS Client payload', 1);
insert into documentType values (98, 'General WS Client payload', 2);
insert into payloadType values (25, 98, 'Exchange2 Folder Creation - null', 1, 'LenderFolderCreation', null, null, 0);
insert into payloadType values (25, 98, 'Exchange2 Folder Creation - null', 1, 'LenderFolderCreation', null, null, 1);
insert into payloadType values (25, 98, 'Exchange2 Folder Creation - null', 1, 'LenderFolderCreation', null, null, 2);

-- new sysproperty for configurable exchage version
 
insert into sysproperty values (1, 'com.filogix.exchange.version', 0, '1', 'V4.1', sysdate, 'exchange version',  null);
insert into sysproperty values (1, 'com.filogix.exchange.version', 1, '0', 'V4.1', sysdate, 'exchange version',  null);
insert into sysproperty values (1, 'com.filogix.exchange.version', 2, '0', 'V4.1', sysdate, 'exchange version',  null);

insert into sysproperty values (3, 'com.filogix.exchange.version', 0, '1', 'V4.1', sysdate, 'exchange version',  null);
insert into sysproperty values (3, 'com.filogix.exchange.version', 1, '0', 'V4.1', sysdate, 'exchange version',  null);
insert into sysproperty values (3, 'com.filogix.exchange.version', 2, '0', 'V4.1', sysdate, 'exchange version',  null);

-- delete from sysproperty where syspropertyTypeid=1 and name = 'com.filogix.exchange.version';

commit;

