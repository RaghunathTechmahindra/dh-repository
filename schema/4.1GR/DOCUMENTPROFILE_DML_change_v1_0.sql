/*==============================================================*/
/* Table: DOCUMENTPROFILE                                       */
/* CommitmentLetter Document                                    */
/*==============================================================*/
Update DOCUMENTPROFILE set DOCUMENTTEMPLATEPATH = 'CommitmentLetterGENX.xsl', DOCUMENTSCHEMAPATH ='com.basis100.deal.docprep.extract.data.GENXCommitmentExtractor' where DOCUMENTTYPEID = 1;

/*==============================================================*/
/* Table: DOCUMENTPROFILE                                       */
/* PreApprovalCommitmentLetter Document                         */
/*==============================================================*/
Update DOCUMENTPROFILE set DOCUMENTTEMPLATEPATH = 'CommitmentLetterPreApproval.xsl', DOCUMENTSCHEMAPATH ='com.basis100.deal.docprep.extract.data.GENXCommitmentExtractor' where DOCUMENTTYPEID = 5;

COMMIT;