delete from CONDITIONSTATUSUPDATEASSOC;
delete from DEALEVENTSTATUSUPDATEASSOC;

delete from CHANNEL where CHANNELID in (17, 19, 20);
delete from CHANNELTYPE where CHANNELTYPEID in (10);

delete from SYSPROPERTY where name like 'fcxingestion%';
delete from SYSPROPERTY where name like 'fcxprocessor%';
delete from SYSPROPERTY where name like 'fxLinkClient%';
delete from SYSPROPERTY where name like 'com.filogix.instance.name';
delete from SYSPROPERTY where name like 'dscuDaemon%';
delete from SYSPROPERTY where name like 'statusUpdate%';
delete from SYSPROPERTY where name like 'com.filogix.express.email.simplealert%';

delete from BORROWERADDRESSTYPE where BORROWERADDRESSTYPEID=3;
delete from SALUTATION where SALUTATIONID=7;
delete from LIABILITYTYPE where LIABILITYTYPEID = 22;
delete from PROPERTYEXPENSETYPE where PROPERTYEXPENSETYPEID=7;
delete from DOWNPAYMENTSOURCETYPE where DOWNPAYMENTSOURCETYPEID=14;
delete from LIENPOSITION where LIENPOSITIONID=3;
delete from DEALNOTESCATEGORY where DEALNOTESCATEGORYID=13;
delete from SERVICESUBTYPE where SERVICETYPEID = 11;
delete from SERVICETYPE where SERVICETYPEID in (8,9,10,11);

delete from STREETTYPE where STREETTYPEID >= 30;
delete from FCXFEETYPE;
delete from fxlinkschema;
delete from ingestionqueuestatus;
delete from FCXLOANAPPSTATUS;
delete from STATUSNAMEREPLACEMENT;
delete from DEALEVENTSTATUSTYPE;











