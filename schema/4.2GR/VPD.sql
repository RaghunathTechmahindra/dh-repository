begin
DBMS_RLS.ADD_POLICY(
 object_schema=> 'MOS',
 object_name=> 'CONDITIONSTATUSUPDATEASSOC',
 policy_name=> 'PLY_CONDITIONSTATUSUPDATEASSOC',
 function_schema=>'VPDADMIN',
 policy_function=>'VPD_SECURITY_POLICY.SET_INSTITUTION_ACCESS',
 statement_types=>'insert,update,delete,select',
 update_check=>true);

DBMS_RLS.ADD_POLICY(
 object_schema=> 'MOS',
 object_name=> 'ESBOUTBOUNDQUEUE',
 policy_name=> 'PLY_ESBOUTBOUNDQUEUE',
 function_schema=>'VPDADMIN',
 policy_function=>'VPD_SECURITY_POLICY.SET_INSTITUTION_ACCESS',
 statement_types=>'insert,update,delete,select',
 update_check=>true);
 
DBMS_RLS.ADD_POLICY(
 object_schema=> 'MOS',
 object_name=> 'DEALEVENTSTATUSUPDATEASSOC',
 policy_name=> 'PLY_DEALEVENTSTATUSUPDATEASSOC',
 function_schema=>'VPDADMIN',
 policy_function=>'VPD_SECURITY_POLICY.SET_INSTITUTION_ACCESS',
 statement_types=>'insert,update,delete,select',
 update_check=>true);

end;

      

                                          
    
