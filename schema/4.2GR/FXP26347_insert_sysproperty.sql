Insert into SYSPROPERTY
   (SYSPROPERTYTYPEID, NAME, INSTITUTIONPROFILEID, VALUE, BUILD_VERSION, 
    CREATED_DATE, DESCRIPTION, NOTES)
 Values
   (3, 'com.filogix.ingestion.sourceofbusinessprofile.brokeremail.update', 0, 'N', '4.2', 
    sysdate, 'Option to update existing SOB contact email address at ingestion', 'Default is N for off, set Y for on.');

Insert into SYSPROPERTY
   (SYSPROPERTYTYPEID, NAME, INSTITUTIONPROFILEID, VALUE, BUILD_VERSION, 
    CREATED_DATE, DESCRIPTION, NOTES)
 Values
   (3, 'com.filogix.ingestion.sourceofbusinessprofile.brokeremail.update', 1, 'N', '4.2', 
    sysdate, 'Option to update existing SOB contact email address at ingestion', 'Default is N for off, set Y for on.');

Insert into SYSPROPERTY
   (SYSPROPERTYTYPEID, NAME, INSTITUTIONPROFILEID, VALUE, BUILD_VERSION, 
    CREATED_DATE, DESCRIPTION, NOTES)
 Values
   (3, 'com.filogix.ingestion.sourceofbusinessprofile.brokeremail.update', 2, 'N', '4.2', 
    sysdate, 'Option to update existing SOB contact email address at ingestion', 'Default is N for off, set Y for on.');
