begin
DBMS_RLS.DROP_POLICY(
 object_schema=> 'MOS',
 object_name=> 'CONDITIONSTATUSUPDATEASSOC',
 policy_name=> 'PLY_CONDITIONSTATUSUPDATEASSOC');

DBMS_RLS.DROP_POLICY(
 object_schema=> 'MOS',
 object_name=> 'ESBOUTBOUNDQUEUE',
 policy_name=> 'PLY_ESBOUTBOUNDQUEUE');
 
DBMS_RLS.DROP_POLICY(
 object_schema=> 'MOS',
 object_name=> 'DEALEVENTSTATUSUPDATEASSOC',
 policy_name=> 'PLY_DEALEVENTSTATUSUPDATEASSOC');

end;
                                          
    
