CREATE OR REPLACE PACKAGE copy_management
AS
   FUNCTION delete_copy (
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      i_institutionprofileid   deal.institutionprofileid%TYPE
   )
      RETURN NUMBER;

   FUNCTION create_copy (
      src_dealid               deal.dealid%TYPE,
      src_copyid               deal.copyid%TYPE,
      dest_copyid              deal.copyid%TYPE,
      i_institutionprofileid   deal.institutionprofileid%TYPE
   )
      RETURN NUMBER;

   FUNCTION duplicate_dealtree (
      src_dealid               deal.dealid%TYPE,
      src_copyid               deal.copyid%TYPE,
      dest_dealid              deal.dealid%TYPE,
      dest_copyid              deal.copyid%TYPE,
      i_institutionprofileid   deal.institutionprofileid%TYPE
   )
      RETURN NUMBER;

   FUNCTION copy_manager (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
      RETURN NUMBER;

   PROCEDURE deal_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE bridge_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE borrower_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE borroweraddress_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_borrowerid             borrower.borrowerid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_borrowerid             borrower.borrowerid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE income_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_borrowerid             borrower.borrowerid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_borrowerid             borrower.borrowerid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE asset_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_borrowerid             borrower.borrowerid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_borrowerid             borrower.borrowerid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE liability_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_borrowerid             borrower.borrowerid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_borrowerid             borrower.borrowerid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE creditreference_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_borrowerid             borrower.borrowerid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_borrowerid             borrower.borrowerid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE employmenthistory_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_borrowerid             borrower.borrowerid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_borrowerid             borrower.borrowerid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE lifedisabilitypremiums_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_borrowerid             borrower.borrowerid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_borrowerid             borrower.borrowerid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE borroweridentification_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_borrowerid             borrower.borrowerid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_borrowerid             borrower.borrowerid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE property_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE propertyexpense_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_propertyid             property.propertyid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_propertyid             property.propertyid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE appraisalorder_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_propertyid             property.propertyid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_propertyid             property.propertyid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE documenttracking_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE documenttrackingverbiage_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_documenttrackingid     documenttrackingverbiage.documenttrackingid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_documenttrackingid     documenttrackingverbiage.documenttrackingid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR
   );

   PROCEDURE escrowpayment_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE downpaymentsource_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE dealfee_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE insureonlyapplicant_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE lifedispremiumsionlya_copy (
      i_institutionprofileid    deal.institutionprofileid%TYPE,
      i_insureonlyapplicantid   insureonlyapplicant.insureonlyapplicantid%TYPE,
      i_copyid                  deal.copyid%TYPE,
      n_insureonlyapplicantid   insureonlyapplicant.insureonlyapplicantid%TYPE,
      n_copyid                  deal.copyid%TYPE,
      action                    VARCHAR2
   );

   PROCEDURE request_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE borrowerrequestassoc_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_requestid              request.requestid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_requestid              request.requestid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE servicerequest_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_requestid              request.requestid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_requestid              request.requestid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE servicerequestcontact_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_requestid              request.requestid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_requestid              request.requestid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE component_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE componentmortgage_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_componentid            component.componentid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_componentid            component.componentid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE componentloc_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_componentid            component.componentid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_componentid            component.componentid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE componentcreditcard_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_componentid            component.componentid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_componentid            component.componentid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE componentloan_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_componentid            component.componentid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_componentid            component.componentid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE componentoverdraft_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_componentid            component.componentid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_componentid            component.componentid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   PROCEDURE componentsummary_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   );

   FUNCTION contact_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_contactid              contact.contactid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
      RETURN NUMBER;

   FUNCTION addr_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_addrid                 addr.addrid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
      RETURN NUMBER;

   TYPE rec_id IS RECORD (
      old_id   NUMBER,
      new_id   NUMBER
   );

   TYPE tab_id IS TABLE OF rec_id;

   m_borrowerid   tab_id         := tab_id ();
   m_incomeid     tab_id         := tab_id ();

   FUNCTION find_id (KEY NUMBER, src tab_id)
      RETURN NUMBER;

   TYPE typetablecount IS TABLE OF NUMBER
      INDEX BY VARCHAR2 (64);

   tab_count      typetablecount;

   PROCEDURE init_tabcount;

   FUNCTION numberofchangedtable
      RETURN NUMBER;

   FUNCTION printchanges
      RETURN VARCHAR2;
END copy_management;
/







CREATE OR REPLACE PACKAGE BODY copy_management
AS
   FUNCTION delete_copy (
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      i_institutionprofileid   deal.institutionprofileid%TYPE
   )
      RETURN NUMBER
   IS
      RESULT   NUMBER;
   BEGIN
      RESULT :=
         copy_manager (i_institutionprofileid,
                       i_dealid,
                       i_copyid,
                       i_dealid,
                       i_copyid,
                       'D'
                      );
      RETURN RESULT;
   END;

   FUNCTION create_copy (
      src_dealid               deal.dealid%TYPE,
      src_copyid               deal.copyid%TYPE,
      dest_copyid              deal.copyid%TYPE,
      i_institutionprofileid   deal.institutionprofileid%TYPE
   )
      RETURN NUMBER
   IS
      RESULT   NUMBER;
   BEGIN
      RESULT :=
         copy_manager (i_institutionprofileid,
                       src_dealid,
                       src_copyid,
                       src_dealid,
                       dest_copyid,
                       'C'
                      );
      RETURN RESULT;
   END;

   FUNCTION duplicate_dealtree (
      src_dealid               deal.dealid%TYPE,
      src_copyid               deal.copyid%TYPE,
      dest_dealid              deal.dealid%TYPE,
      dest_copyid              deal.copyid%TYPE,
      i_institutionprofileid   deal.institutionprofileid%TYPE
   )
      RETURN NUMBER
   IS
      RESULT   NUMBER;
   BEGIN
      RESULT :=
         copy_manager (i_institutionprofileid,
                       src_dealid,
                       src_copyid,
                       dest_dealid,
                       dest_copyid,
                       'N'
                      );
      RETURN RESULT;
   END;

   FUNCTION copy_manager (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
      RETURN NUMBER
   IS
      changed_table   NUMBER;
   BEGIN
      init_tabcount;
      deal_copy (i_institutionprofileid,
                 i_dealid,
                 i_copyid,
                 n_dealid,
                 n_copyid,
                 action
                );
      changed_table := numberofchangedtable;
      RETURN changed_table;
   --return tab_count.count;
   EXCEPTION
      WHEN OTHERS
      THEN
         --rollback;
         RAISE;
   END;

   PROCEDURE deal_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      one_row   deal%ROWTYPE;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT *
              INTO one_row
              FROM deal
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            one_row.copyid := n_copyid;

            INSERT INTO deal
                 VALUES one_row;

            tab_count ('deal') := 1;
            bridge_copy (i_institutionprofileid,
                         i_dealid,
                         i_copyid,
                         n_dealid,
                         n_copyid,
                         action
                        );
            borrower_copy (i_institutionprofileid,
                           i_dealid,
                           i_copyid,
                           n_dealid,
                           n_copyid,
                           action
                          );
            property_copy (i_institutionprofileid,
                           i_dealid,
                           i_copyid,
                           n_dealid,
                           n_copyid,
                           action
                          );
            documenttracking_copy (i_institutionprofileid,
                                   i_dealid,
                                   i_copyid,
                                   n_dealid,
                                   n_copyid,
                                   action
                                  );
            escrowpayment_copy (i_institutionprofileid,
                                i_dealid,
                                i_copyid,
                                n_dealid,
                                n_copyid,
                                action
                               );
            downpaymentsource_copy (i_institutionprofileid,
                                    i_dealid,
                                    i_copyid,
                                    n_dealid,
                                    n_copyid,
                                    action
                                   );
            dealfee_copy (i_institutionprofileid,
                          i_dealid,
                          i_copyid,
                          n_dealid,
                          n_copyid,
                          action
                         );
            insureonlyapplicant_copy (i_institutionprofileid,
                                      i_dealid,
                                      i_copyid,
                                      n_dealid,
                                      n_copyid,
                                      action
                                     );
            request_copy (i_institutionprofileid,
                          i_dealid,
                          i_copyid,
                          n_dealid,
                          n_copyid,
                          action
                         );
            component_copy (i_institutionprofileid,
                            i_dealid,
                            i_copyid,
                            n_dealid,
                            n_copyid,
                            action
                           );
            componentsummary_copy (i_institutionprofileid,
                                   i_dealid,
                                   i_copyid,
                                   n_dealid,
                                   n_copyid,
                                   action
                                  );
         WHEN 'N'
         THEN
            SELECT *
              INTO one_row
              FROM deal
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            one_row.dealid := n_dealid;
            one_row.applicationid := n_dealid;
            one_row.copyid := n_copyid;

            INSERT INTO deal
                 VALUES one_row;

            tab_count ('deal') := 1;
            bridge_copy (i_institutionprofileid,
                         i_dealid,
                         i_copyid,
                         n_dealid,
                         n_copyid,
                         action
                        );
            borrower_copy (i_institutionprofileid,
                           i_dealid,
                           i_copyid,
                           n_dealid,
                           n_copyid,
                           action
                          );
            property_copy (i_institutionprofileid,
                           i_dealid,
                           i_copyid,
                           n_dealid,
                           n_copyid,
                           action
                          );
            documenttracking_copy (i_institutionprofileid,
                                   i_dealid,
                                   i_copyid,
                                   n_dealid,
                                   n_copyid,
                                   action
                                  );
            escrowpayment_copy (i_institutionprofileid,
                                i_dealid,
                                i_copyid,
                                n_dealid,
                                n_copyid,
                                action
                               );
            downpaymentsource_copy (i_institutionprofileid,
                                    i_dealid,
                                    i_copyid,
                                    n_dealid,
                                    n_copyid,
                                    action
                                   );
            dealfee_copy (i_institutionprofileid,
                          i_dealid,
                          i_copyid,
                          n_dealid,
                          n_copyid,
                          action
                         );
            insureonlyapplicant_copy (i_institutionprofileid,
                                      i_dealid,
                                      i_copyid,
                                      n_dealid,
                                      n_copyid,
                                      action
                                     );
            request_copy (i_institutionprofileid,
                          i_dealid,
                          i_copyid,
                          n_dealid,
                          n_copyid,
                          action
                         );
            component_copy (i_institutionprofileid,
                            i_dealid,
                            i_copyid,
                            n_dealid,
                            n_copyid,
                            action
                           );
            componentsummary_copy (i_institutionprofileid,
                                   i_dealid,
                                   i_copyid,
                                   n_dealid,
                                   n_copyid,
                                   action
                                  );
         WHEN 'D'
         THEN
            bridge_copy (i_institutionprofileid,
                         i_dealid,
                         i_copyid,
                         n_dealid,
                         n_copyid,
                         action
                        );
            request_copy (i_institutionprofileid,
                          i_dealid,
                          i_copyid,
                          n_dealid,
                          n_copyid,
                          action
                         );
            borrower_copy (i_institutionprofileid,
                           i_dealid,
                           i_copyid,
                           n_dealid,
                           n_copyid,
                           action
                          );
            property_copy (i_institutionprofileid,
                           i_dealid,
                           i_copyid,
                           n_dealid,
                           n_copyid,
                           action
                          );
            documenttracking_copy (i_institutionprofileid,
                                   i_dealid,
                                   i_copyid,
                                   n_dealid,
                                   n_copyid,
                                   action
                                  );
            escrowpayment_copy (i_institutionprofileid,
                                i_dealid,
                                i_copyid,
                                n_dealid,
                                n_copyid,
                                action
                               );
            downpaymentsource_copy (i_institutionprofileid,
                                    i_dealid,
                                    i_copyid,
                                    n_dealid,
                                    n_copyid,
                                    action
                                   );
            dealfee_copy (i_institutionprofileid,
                          i_dealid,
                          i_copyid,
                          n_dealid,
                          n_copyid,
                          action
                         );
            insureonlyapplicant_copy (i_institutionprofileid,
                                      i_dealid,
                                      i_copyid,
                                      n_dealid,
                                      n_copyid,
                                      action
                                     );
            component_copy (i_institutionprofileid,
                            i_dealid,
                            i_copyid,
                            n_dealid,
                            n_copyid,
                            action
                           );
            componentsummary_copy (i_institutionprofileid,
                                   i_dealid,
                                   i_copyid,
                                   n_dealid,
                                   n_copyid,
                                   action
                                  );

            DELETE      deal
                  WHERE dealid = i_dealid
                    AND copyid = i_copyid
                    AND institutionprofileid = i_institutionprofileid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('deal') := 1;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in deal_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE bridge_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF bridge%ROWTYPE;

      rowset          typerowset;
      next_bridgeid   bridge.bridgeid%TYPE;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM bridge
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO bridge
                    VALUES rowset (i);
            END LOOP;

            tab_count ('bridge') := tab_count ('bridge') + 1;
         WHEN 'N'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM bridge
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            SELECT MAX (bridgeid) + 1
              INTO next_bridgeid
              FROM bridge;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).bridgeid := next_bridgeid;
               next_bridgeid := next_bridgeid + 1;
               rowset (i).dealid := n_dealid;
               rowset (i).copyid := n_copyid;

               INSERT INTO bridge
                    VALUES rowset (i);
            END LOOP;

            tab_count ('bridge') := tab_count ('bridge') + 1;
         WHEN 'D'
         THEN
            DELETE      bridge
                  WHERE dealid = i_dealid
                    AND copyid = i_copyid
                    AND institutionprofileid = i_institutionprofileid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('bridge') := tab_count ('bridge') + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in bridge_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE borrower_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF borrower%ROWTYPE;

      rowset    typerowset;
      next_id   borrower.borrowerid%TYPE;

      TYPE typeid IS TABLE OF borrower.borrowerid%TYPE;

      old_id    typeid;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM borrower
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO borrower
                    VALUES rowset (i);

               borroweraddress_copy (i_institutionprofileid,
                                     rowset (i).borrowerid,
                                     i_copyid,
                                     rowset (i).borrowerid,
                                     n_copyid,
                                     'C'
                                    );
               income_copy (i_institutionprofileid,
                            rowset (i).borrowerid,
                            i_copyid,
                            rowset (i).borrowerid,
                            n_copyid,
                            'C'
                           );
               asset_copy (i_institutionprofileid,
                           rowset (i).borrowerid,
                           i_copyid,
                           rowset (i).borrowerid,
                           n_copyid,
                           'C'
                          );
               liability_copy (i_institutionprofileid,
                               rowset (i).borrowerid,
                               i_copyid,
                               rowset (i).borrowerid,
                               n_copyid,
                               'C'
                              );
               creditreference_copy (i_institutionprofileid,
                                     rowset (i).borrowerid,
                                     i_copyid,
                                     rowset (i).borrowerid,
                                     n_copyid,
                                     'C'
                                    );
               employmenthistory_copy (i_institutionprofileid,
                                       rowset (i).borrowerid,
                                       i_copyid,
                                       rowset (i).borrowerid,
                                       n_copyid,
                                       'C'
                                      );
               lifedisabilitypremiums_copy (i_institutionprofileid,
                                            rowset (i).borrowerid,
                                            i_copyid,
                                            rowset (i).borrowerid,
                                            n_copyid,
                                            'C'
                                           );
               borroweridentification_copy (i_institutionprofileid,
                                            rowset (i).borrowerid,
                                            i_copyid,
                                            rowset (i).borrowerid,
                                            n_copyid,
                                            'C'
                                           );
            END LOOP;
         WHEN 'N'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM borrower
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            m_borrowerid := tab_id ();
            old_id := typeid ();

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT borrowerseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               old_id.EXTEND ();
               old_id (i) := rowset (i).borrowerid;
               m_borrowerid.EXTEND ();
               m_borrowerid (i).old_id := rowset (i).borrowerid;
               m_borrowerid (i).new_id := next_id;
               rowset (i).borrowerid := next_id;
               rowset (i).dealid := n_dealid;
               rowset (i).copyid := n_copyid;

               INSERT INTO borrower
                    VALUES rowset (i);

               borroweraddress_copy (i_institutionprofileid,
                                     old_id (i),
                                     i_copyid,
                                     rowset (i).borrowerid,
                                     n_copyid,
                                     'N'
                                    );
               income_copy (i_institutionprofileid,
                            old_id (i),
                            i_copyid,
                            rowset (i).borrowerid,
                            n_copyid,
                            'N'
                           );
               asset_copy (i_institutionprofileid,
                           old_id (i),
                           i_copyid,
                           rowset (i).borrowerid,
                           n_copyid,
                           'N'
                          );
               liability_copy (i_institutionprofileid,
                               old_id (i),
                               i_copyid,
                               rowset (i).borrowerid,
                               n_copyid,
                               'N'
                              );
               creditreference_copy (i_institutionprofileid,
                                     old_id (i),
                                     i_copyid,
                                     rowset (i).borrowerid,
                                     n_copyid,
                                     'N'
                                    );
               employmenthistory_copy (i_institutionprofileid,
                                       old_id (i),
                                       i_copyid,
                                       rowset (i).borrowerid,
                                       n_copyid,
                                       'N'
                                      );
               lifedisabilitypremiums_copy (i_institutionprofileid,
                                            old_id (i),
                                            i_copyid,
                                            rowset (i).borrowerid,
                                            n_copyid,
                                            'N'
                                           );
               borroweridentification_copy (i_institutionprofileid,
                                            old_id (i),
                                            i_copyid,
                                            rowset (i).borrowerid,
                                            n_copyid,
                                            'N'
                                           );
            END LOOP;
         WHEN 'D'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM borrower
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN 1 .. rowset.COUNT
            LOOP
               --dbms_output.put_line('deleting borrowerid:'||RowSet(i).borrowerid);
               borroweraddress_copy (i_institutionprofileid,
                                     rowset (i).borrowerid,
                                     i_copyid,
                                     rowset (i).borrowerid,
                                     n_copyid,
                                     'D'
                                    );
               income_copy (i_institutionprofileid,
                            rowset (i).borrowerid,
                            i_copyid,
                            rowset (i).borrowerid,
                            n_copyid,
                            'D'
                           );
               asset_copy (i_institutionprofileid,
                           rowset (i).borrowerid,
                           i_copyid,
                           rowset (i).borrowerid,
                           n_copyid,
                           'D'
                          );
               liability_copy (i_institutionprofileid,
                               rowset (i).borrowerid,
                               i_copyid,
                               rowset (i).borrowerid,
                               n_copyid,
                               'D'
                              );
               creditreference_copy (i_institutionprofileid,
                                     rowset (i).borrowerid,
                                     i_copyid,
                                     rowset (i).borrowerid,
                                     n_copyid,
                                     'D'
                                    );
               employmenthistory_copy (i_institutionprofileid,
                                       rowset (i).borrowerid,
                                       i_copyid,
                                       rowset (i).borrowerid,
                                       n_copyid,
                                       'D'
                                      );
               lifedisabilitypremiums_copy (i_institutionprofileid,
                                            rowset (i).borrowerid,
                                            i_copyid,
                                            rowset (i).borrowerid,
                                            n_copyid,
                                            'D'
                                           );
               borroweridentification_copy (i_institutionprofileid,
                                            rowset (i).borrowerid,
                                            i_copyid,
                                            rowset (i).borrowerid,
                                            n_copyid,
                                            'D'
                                           );
            END LOOP;

            DELETE      borrower
                  WHERE dealid = i_dealid
                    AND copyid = i_copyid
                    AND institutionprofileid = i_institutionprofileid;
         ELSE
            NULL;
      END CASE;

      tab_count ('borrower') := tab_count ('borrower') + rowset.COUNT;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in borrower_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE borroweraddress_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_borrowerid             borrower.borrowerid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_borrowerid             borrower.borrowerid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF borroweraddress%ROWTYPE;

      rowset       typerowset;
      next_id      borroweraddress.borroweraddressid%TYPE;
      new_addrid   addr.addrid%TYPE;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM borroweraddress i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.borrowerid = i_borrowerid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;
               new_addrid :=
                  addr_copy (i_institutionprofileid,
                             rowset (i).addrid,
                             i_copyid,
                             n_copyid,
                             'C'
                            );

               INSERT INTO borroweraddress
                    VALUES rowset (i);
            END LOOP;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM borroweraddress i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.borrowerid = i_borrowerid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT borroweraddressseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               rowset (i).borroweraddressid := next_id;
               rowset (i).borrowerid := n_borrowerid;
               new_addrid :=
                  addr_copy (i_institutionprofileid,
                             rowset (i).addrid,
                             i_copyid,
                             n_copyid,
                             'N'
                            );
               rowset (i).addrid := new_addrid;
               rowset (i).copyid := n_copyid;

               INSERT INTO borroweraddress
                    VALUES rowset (i);
            END LOOP;
         WHEN 'D'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM borroweraddress i
             WHERE i.borrowerid = i_borrowerid
               AND i.copyid = i_copyid
               AND i.institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            DELETE      borroweraddress i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.borrowerid = i_borrowerid
                    AND i.copyid = i_copyid;

            FOR i IN 1 .. rowset.COUNT
            LOOP
               --dbms_output.put_line('in borroweraddress_copy, deleting addrid:'||RowSet(i).addrid);
               new_addrid :=
                  addr_copy (i_institutionprofileid,
                             rowset (i).addrid,
                             i_copyid,
                             n_copyid,
                             'D'
                            );
            END LOOP;
         ELSE
            NULL;
      END CASE;

      tab_count ('borroweraddress') :=
                                   tab_count ('borroweraddress')
                                   + rowset.COUNT;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in borroweraddress_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE income_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_borrowerid             borrower.borrowerid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_borrowerid             borrower.borrowerid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF income%ROWTYPE;

      rowset    typerowset;
      next_id   income.incomeid%TYPE;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM income i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.borrowerid = i_borrowerid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO income
                    VALUES rowset (i);
            END LOOP;

            tab_count ('income') := tab_count ('income') + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM income i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.borrowerid = i_borrowerid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            m_incomeid := tab_id ();

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT incomeseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               m_incomeid.EXTEND ();
               m_incomeid (i).old_id := rowset (i).incomeid;
               m_incomeid (i).new_id := next_id;
               rowset (i).incomeid := next_id;
               rowset (i).borrowerid := n_borrowerid;
               rowset (i).copyid := n_copyid;

               INSERT INTO income
                    VALUES rowset (i);
            END LOOP;

            tab_count ('income') := tab_count ('income') + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      income i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.borrowerid = i_borrowerid
                    AND i.copyid = i_copyid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('income') := tab_count ('income') + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in income_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE asset_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_borrowerid             borrower.borrowerid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_borrowerid             borrower.borrowerid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF asset%ROWTYPE;

      rowset    typerowset;
      next_id   asset.assetid%TYPE;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM asset i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.borrowerid = i_borrowerid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO asset
                    VALUES rowset (i);
            END LOOP;

            tab_count ('asset') := tab_count ('asset') + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM asset i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.borrowerid = i_borrowerid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT assetseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               rowset (i).assetid := next_id;
               rowset (i).borrowerid := n_borrowerid;
               rowset (i).copyid := n_copyid;

               INSERT INTO asset
                    VALUES rowset (i);
            END LOOP;

            tab_count ('asset') := tab_count ('asset') + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      asset i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.borrowerid = i_borrowerid
                    AND i.copyid = i_copyid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('asset') := tab_count ('asset') + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in asset_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE liability_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_borrowerid             borrower.borrowerid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_borrowerid             borrower.borrowerid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF liability%ROWTYPE;

      rowset    typerowset;
      next_id   liability.liabilityid%TYPE;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM liability i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.borrowerid = i_borrowerid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO liability
                    VALUES rowset (i);
            END LOOP;

            tab_count ('liability') := tab_count ('liability') + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM liability i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.borrowerid = i_borrowerid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT liabilityseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               rowset (i).liabilityid := next_id;
               rowset (i).borrowerid := n_borrowerid;
               rowset (i).copyid := n_copyid;

               INSERT INTO liability
                    VALUES rowset (i);
            END LOOP;

            tab_count ('liability') := tab_count ('liability') + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      liability i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.borrowerid = i_borrowerid
                    AND i.copyid = i_copyid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('liability') :=
                                      tab_count ('liability')
                                      + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in liability_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE creditreference_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_borrowerid             borrower.borrowerid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_borrowerid             borrower.borrowerid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF creditreference%ROWTYPE;

      rowset    typerowset;
      next_id   creditreference.creditreferenceid%TYPE;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM creditreference i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.borrowerid = i_borrowerid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO creditreference
                    VALUES rowset (i);
            END LOOP;

            tab_count ('creditreference') :=
                                   tab_count ('creditreference')
                                   + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM creditreference i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.borrowerid = i_borrowerid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT creditreferenceseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               rowset (i).creditreferenceid := next_id;
               rowset (i).borrowerid := n_borrowerid;
               rowset (i).copyid := n_copyid;

               INSERT INTO creditreference
                    VALUES rowset (i);
            END LOOP;

            tab_count ('creditreference') :=
                                   tab_count ('creditreference')
                                   + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      creditreference i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.borrowerid = i_borrowerid
                    AND i.copyid = i_copyid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('creditreference') :=
                                tab_count ('creditreference')
                                + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in creditreference_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE employmenthistory_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_borrowerid             borrower.borrowerid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_borrowerid             borrower.borrowerid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF employmenthistory%ROWTYPE;

      rowset          typerowset;
      next_id         employmenthistory.employmenthistoryid%TYPE;
      new_contactid   contact.contactid%TYPE;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM employmenthistory i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.borrowerid = i_borrowerid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;
               new_contactid :=
                  contact_copy (i_institutionprofileid,
                                rowset (i).contactid,
                                i_copyid,
                                n_copyid,
                                'C'
                               );

               INSERT INTO employmenthistory
                    VALUES rowset (i);
            END LOOP;

            tab_count ('employmenthistory') :=
                                 tab_count ('employmenthistory')
                                 + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM employmenthistory i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.borrowerid = i_borrowerid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT employmenthistoryseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               rowset (i).employmenthistoryid := next_id;
               rowset (i).incomeid :=
                                     find_id (rowset (i).incomeid, m_incomeid);
               rowset (i).borrowerid := n_borrowerid;
               rowset (i).copyid := n_copyid;
               new_contactid :=
                  contact_copy (i_institutionprofileid,
                                rowset (i).contactid,
                                i_copyid,
                                n_copyid,
                                'N'
                               );
               rowset (i).contactid := new_contactid;

               INSERT INTO employmenthistory
                    VALUES rowset (i);
            END LOOP;

            tab_count ('employmenthistory') :=
                                 tab_count ('employmenthistory')
                                 + rowset.COUNT;
         WHEN 'D'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM employmenthistory i
             WHERE i.borrowerid = i_borrowerid
               AND i.copyid = i_copyid
               AND i.institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            DELETE      employmenthistory i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.borrowerid = i_borrowerid
                    AND i.copyid = i_copyid;

            FOR i IN 1 .. rowset.COUNT
            LOOP
               --DBMS_OUTPUT.PUT_LINE('in employmenthistory_copy deleting contactid:'||RowSet(i).contactid);
               new_contactid :=
                  contact_copy (i_institutionprofileid,
                                rowset (i).contactid,
                                i_copyid,
                                n_copyid,
                                'D'
                               );
            END LOOP;

            tab_count ('employmenthistory') :=
                                 tab_count ('employmenthistory')
                                 + rowset.COUNT;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in employmenthistory_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE lifedisabilitypremiums_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_borrowerid             borrower.borrowerid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_borrowerid             borrower.borrowerid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF lifedisabilitypremiums%ROWTYPE;

      rowset    typerowset;
      next_id   lifedisabilitypremiums.lifedisabilitypremiumsid%TYPE;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM lifedisabilitypremiums i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.borrowerid = i_borrowerid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO lifedisabilitypremiums
                    VALUES rowset (i);
            END LOOP;

            tab_count ('lifedisabilitypremiums') :=
                            tab_count ('lifedisabilitypremiums')
                            + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM lifedisabilitypremiums i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.borrowerid = i_borrowerid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT lifedisabilitypremiumsseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               rowset (i).lifedisabilitypremiumsid := next_id;
               rowset (i).borrowerid := n_borrowerid;
               rowset (i).copyid := n_copyid;

               INSERT INTO lifedisabilitypremiums
                    VALUES rowset (i);
            END LOOP;

            tab_count ('lifedisabilitypremiums') :=
                            tab_count ('lifedisabilitypremiums')
                            + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      lifedisabilitypremiums i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.borrowerid = i_borrowerid
                    AND i.copyid = i_copyid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('lifedisabilitypremiums') :=
                         tab_count ('lifedisabilitypremiums')
                         + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in lifedisabilitypremiums_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE borroweridentification_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_borrowerid             borrower.borrowerid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_borrowerid             borrower.borrowerid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF borroweridentification%ROWTYPE;

      rowset    typerowset;
      next_id   borroweridentification.identificationid%TYPE;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM borroweridentification i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.borrowerid = i_borrowerid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO borroweridentification
                    VALUES rowset (i);
            END LOOP;

            tab_count ('borroweridentification') :=
                            tab_count ('borroweridentification')
                            + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM borroweridentification i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.borrowerid = i_borrowerid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT borroweridentificationseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               rowset (i).identificationid := next_id;
               rowset (i).borrowerid := n_borrowerid;
               rowset (i).copyid := n_copyid;

               INSERT INTO borroweridentification
                    VALUES rowset (i);
            END LOOP;

            tab_count ('borroweridentification') :=
                            tab_count ('borroweridentification')
                            + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      borroweridentification i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.borrowerid = i_borrowerid
                    AND i.copyid = i_copyid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('borroweridentification') :=
                         tab_count ('borroweridentification')
                         + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in borroweridentification_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE property_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF property%ROWTYPE;

      rowset    typerowset;
      next_id   property.propertyid%TYPE;

      TYPE typeid IS TABLE OF property.propertyid%TYPE;

      old_id    typeid;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM property
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO property
                    VALUES rowset (i);

               propertyexpense_copy (i_institutionprofileid,
                                     rowset (i).propertyid,
                                     i_copyid,
                                     rowset (i).propertyid,
                                     n_copyid,
                                     'C'
                                    );
               appraisalorder_copy (i_institutionprofileid,
                                    rowset (i).propertyid,
                                    i_copyid,
                                    rowset (i).propertyid,
                                    n_copyid,
                                    'C'
                                   );
            END LOOP;
         WHEN 'N'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM property
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            old_id := typeid ();

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT propertyseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               old_id.EXTEND ();
               old_id (i) := rowset (i).propertyid;
               rowset (i).propertyid := next_id;
               rowset (i).dealid := n_dealid;
               rowset (i).copyid := n_copyid;

               INSERT INTO property
                    VALUES rowset (i);

               propertyexpense_copy (i_institutionprofileid,
                                     old_id (i),
                                     i_copyid,
                                     rowset (i).propertyid,
                                     n_copyid,
                                     'N'
                                    );
               appraisalorder_copy (i_institutionprofileid,
                                    old_id (i),
                                    i_copyid,
                                    rowset (i).propertyid,
                                    n_copyid,
                                    'N'
                                   );
            END LOOP;
         WHEN 'D'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM property
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN 1 .. rowset.COUNT
            LOOP
               propertyexpense_copy (i_institutionprofileid,
                                     rowset (i).propertyid,
                                     i_copyid,
                                     rowset (i).propertyid,
                                     n_copyid,
                                     'D'
                                    );
               appraisalorder_copy (i_institutionprofileid,
                                    rowset (i).propertyid,
                                    i_copyid,
                                    rowset (i).propertyid,
                                    n_copyid,
                                    'D'
                                   );
            END LOOP;

            DELETE      property
                  WHERE dealid = i_dealid
                    AND copyid = i_copyid
                    AND institutionprofileid = i_institutionprofileid;
         ELSE
            NULL;
      END CASE;

      tab_count ('property') := tab_count ('property') + rowset.COUNT;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in property_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE propertyexpense_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_propertyid             property.propertyid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_propertyid             property.propertyid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF propertyexpense%ROWTYPE;

      rowset    typerowset;
      next_id   propertyexpense.propertyexpenseid%TYPE;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM propertyexpense i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.propertyid = i_propertyid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO propertyexpense
                    VALUES rowset (i);
            END LOOP;

            tab_count ('propertyexpense') :=
                                   tab_count ('propertyexpense')
                                   + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM propertyexpense i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.propertyid = i_propertyid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT propertyexpenseseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               rowset (i).propertyexpenseid := next_id;
               rowset (i).propertyid := n_propertyid;
               rowset (i).copyid := n_copyid;

               INSERT INTO propertyexpense
                    VALUES rowset (i);
            END LOOP;

            tab_count ('propertyexpense') :=
                                   tab_count ('propertyexpense')
                                   + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      propertyexpense i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.propertyid = i_propertyid
                    AND i.copyid = i_copyid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('propertyexpense') :=
                                tab_count ('propertyexpense')
                                + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in propertyexpense_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE appraisalorder_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_propertyid             property.propertyid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_propertyid             property.propertyid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF appraisalorder%ROWTYPE;

      rowset    typerowset;
      next_id   appraisalorder.appraisalorderid%TYPE;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM appraisalorder i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.propertyid = i_propertyid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO appraisalorder
                    VALUES rowset (i);
            END LOOP;

            tab_count ('appraisalorder') :=
                                    tab_count ('appraisalorder')
                                    + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM appraisalorder i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.propertyid = i_propertyid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT appraisalorderseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               rowset (i).appraisalorderid := next_id;
               rowset (i).propertyid := n_propertyid;
               rowset (i).copyid := n_copyid;

               INSERT INTO appraisalorder
                    VALUES rowset (i);
            END LOOP;

            tab_count ('appraisalorder') :=
                                    tab_count ('appraisalorder')
                                    + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      appraisalorder i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.propertyid = i_propertyid
                    AND i.copyid = i_copyid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('appraisalorder') :=
                                 tab_count ('appraisalorder')
                                 + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in appraisalorder_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE documenttracking_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF documenttracking%ROWTYPE;

      rowset    typerowset;
      next_id   documenttracking.documenttrackingid%TYPE;

      TYPE typeid IS TABLE OF documenttracking.documenttrackingid%TYPE;

      old_id    typeid;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM documenttracking
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO documenttracking
                    VALUES rowset (i);

               documenttrackingverbiage_copy (i_institutionprofileid,
                                              rowset (i).documenttrackingid,
                                              i_copyid,
                                              rowset (i).documenttrackingid,
                                              n_copyid,
                                              'C'
                                             );
            END LOOP;
         WHEN 'N'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM documenttracking
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            old_id := typeid ();

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT documenttrackingseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               old_id.EXTEND ();
               old_id (i) := rowset (i).documenttrackingid;
               rowset (i).documenttrackingid := next_id;
               rowset (i).dealid := n_dealid;
               rowset (i).applicationid := n_dealid;
               rowset (i).copyid := n_copyid;

               INSERT INTO documenttracking
                    VALUES rowset (i);

               documenttrackingverbiage_copy (i_institutionprofileid,
                                              old_id (i),
                                              i_copyid,
                                              rowset (i).documenttrackingid,
                                              n_copyid,
                                              'N'
                                             );
            END LOOP;
         WHEN 'D'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM documenttracking
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN 1 .. rowset.COUNT
            LOOP
               documenttrackingverbiage_copy (i_institutionprofileid,
                                              rowset (i).documenttrackingid,
                                              i_copyid,
                                              rowset (i).documenttrackingid,
                                              n_copyid,
                                              'D'
                                             );
            END LOOP;

            DELETE      documenttracking
                  WHERE dealid = i_dealid
                    AND copyid = i_copyid
                    AND institutionprofileid = i_institutionprofileid;
         ELSE
            NULL;
      END CASE;

      tab_count ('documenttracking') :=
                                  tab_count ('documenttracking')
                                  + rowset.COUNT;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in documenttracking_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE documenttrackingverbiage_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_documenttrackingid     documenttrackingverbiage.documenttrackingid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_documenttrackingid     documenttrackingverbiage.documenttrackingid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR
   )
   IS
      TYPE typerowset IS TABLE OF documenttrackingverbiage%ROWTYPE;

      rowset     typerowset;
--      onthefly   NUMBER (1);

   BEGIN

/*
      SELECT COUNT (*)
        INTO onthefly
        FROM sysproperty
       WHERE NAME = 'com.basis100.conditions.dtverbiage.generateonthefly'
         AND institutionprofileid = i_institutionprofileid
         AND syspropertytypeid IN (0, 1)
         AND VALUE = 'Y';

      IF onthefly = 1
      THEN
         RETURN;
      END IF;
*/

      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM documenttrackingverbiage i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.documenttrackingid = i_documenttrackingid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO documenttrackingverbiage
                    VALUES rowset (i);
            END LOOP;

            tab_count ('documenttrackingverbiage') :=
                          tab_count ('documenttrackingverbiage')
                          + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM documenttrackingverbiage i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.documenttrackingid = i_documenttrackingid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).documenttrackingid := n_documenttrackingid;
               rowset (i).copyid := n_copyid;

               INSERT INTO documenttrackingverbiage
                    VALUES rowset (i);
            END LOOP;

            tab_count ('documenttrackingverbiage') :=
                          tab_count ('documenttrackingverbiage')
                          + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      documenttrackingverbiage i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.documenttrackingid = i_documenttrackingid
                    AND i.copyid = i_copyid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('documenttrackingverbiage') :=
                       tab_count ('documenttrackingverbiage')
                       + SQL%ROWCOUNT;
            END IF;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in documenttrackingverbiage_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE escrowpayment_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF escrowpayment%ROWTYPE;

      rowset    typerowset;
      next_id   escrowpayment.escrowpaymentid%TYPE;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM escrowpayment
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).dealid := n_dealid;
               rowset (i).copyid := n_copyid;

               INSERT INTO escrowpayment
                    VALUES rowset (i);
            END LOOP;

            tab_count ('escrowpayment') :=
                                     tab_count ('escrowpayment')
                                     + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM escrowpayment
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT escrowpaymentseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               rowset (i).escrowpaymentid := next_id;
               rowset (i).dealid := n_dealid;
               rowset (i).copyid := n_copyid;

               INSERT INTO escrowpayment
                    VALUES rowset (i);
            END LOOP;

            tab_count ('escrowpayment') :=
                                     tab_count ('escrowpayment')
                                     + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      escrowpayment
                  WHERE dealid = i_dealid
                    AND copyid = i_copyid
                    AND institutionprofileid = i_institutionprofileid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('escrowpayment') :=
                                  tab_count ('escrowpayment')
                                  + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in escrowpayment_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE downpaymentsource_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF downpaymentsource%ROWTYPE;

      rowset    typerowset;
      next_id   downpaymentsource.downpaymentsourceid%TYPE;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM downpaymentsource
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).dealid := n_dealid;
               rowset (i).copyid := n_copyid;

               INSERT INTO downpaymentsource
                    VALUES rowset (i);
            END LOOP;

            tab_count ('downpaymentsource') :=
                                 tab_count ('downpaymentsource')
                                 + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM downpaymentsource
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT downpaymentsourceseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               rowset (i).downpaymentsourceid := next_id;
               rowset (i).dealid := n_dealid;
               rowset (i).copyid := n_copyid;

               INSERT INTO downpaymentsource
                    VALUES rowset (i);
            END LOOP;

            tab_count ('downpaymentsource') :=
                                 tab_count ('downpaymentsource')
                                 + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      downpaymentsource
                  WHERE dealid = i_dealid
                    AND copyid = i_copyid
                    AND institutionprofileid = i_institutionprofileid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('downpaymentsource') :=
                              tab_count ('downpaymentsource')
                              + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in downpaymentsource_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE dealfee_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF dealfee%ROWTYPE;

      rowset    typerowset;
      next_id   dealfee.dealfeeid%TYPE;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM dealfee
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).dealid := n_dealid;
               rowset (i).copyid := n_copyid;

               INSERT INTO dealfee
                    VALUES rowset (i);
            END LOOP;

            tab_count ('dealfee') := tab_count ('dealfee') + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM dealfee
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT dealfeeseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               rowset (i).dealfeeid := next_id;
               rowset (i).dealid := n_dealid;
               rowset (i).copyid := n_copyid;

               INSERT INTO dealfee
                    VALUES rowset (i);
            END LOOP;

            tab_count ('dealfee') := tab_count ('dealfee') + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      dealfee
                  WHERE dealid = i_dealid
                    AND copyid = i_copyid
                    AND institutionprofileid = i_institutionprofileid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('dealfee') := tab_count ('dealfee')
                                        + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in dealfee_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE insureonlyapplicant_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF insureonlyapplicant%ROWTYPE;

      rowset    typerowset;
      next_id   insureonlyapplicant.insureonlyapplicantid%TYPE;

      TYPE typeid IS TABLE OF insureonlyapplicant.insureonlyapplicantid%TYPE;

      old_id    typeid;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM insureonlyapplicant
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO insureonlyapplicant
                    VALUES rowset (i);

               lifedispremiumsionlya_copy (i_institutionprofileid,
                                           rowset (i).insureonlyapplicantid,
                                           i_copyid,
                                           rowset (i).insureonlyapplicantid,
                                           n_copyid,
                                           'C'
                                          );
            END LOOP;
         WHEN 'N'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM insureonlyapplicant
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            old_id := typeid ();

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT insureonlyapplicantseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               old_id.EXTEND ();
               old_id (i) := rowset (i).insureonlyapplicantid;
               rowset (i).insureonlyapplicantid := next_id;
               rowset (i).dealid := n_dealid;
               rowset (i).copyid := n_copyid;

               INSERT INTO insureonlyapplicant
                    VALUES rowset (i);

               lifedispremiumsionlya_copy (i_institutionprofileid,
                                           old_id (i),
                                           i_copyid,
                                           rowset (i).insureonlyapplicantid,
                                           n_copyid,
                                           'N'
                                          );
            END LOOP;
         WHEN 'D'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM insureonlyapplicant
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN 1 .. rowset.COUNT
            LOOP
               NULL;
               lifedispremiumsionlya_copy (i_institutionprofileid,
                                           rowset (i).insureonlyapplicantid,
                                           i_copyid,
                                           rowset (i).insureonlyapplicantid,
                                           n_copyid,
                                           'D'
                                          );
            END LOOP;

            DELETE      insureonlyapplicant
                  WHERE dealid = i_dealid
                    AND copyid = i_copyid
                    AND institutionprofileid = i_institutionprofileid;
         ELSE
            NULL;
      END CASE;

      tab_count ('insureonlyapplicant') :=
                               tab_count ('insureonlyapplicant')
                               + rowset.COUNT;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in insureonlyapplicant_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE lifedispremiumsionlya_copy (
      i_institutionprofileid    deal.institutionprofileid%TYPE,
      i_insureonlyapplicantid   insureonlyapplicant.insureonlyapplicantid%TYPE,
      i_copyid                  deal.copyid%TYPE,
      n_insureonlyapplicantid   insureonlyapplicant.insureonlyapplicantid%TYPE,
      n_copyid                  deal.copyid%TYPE,
      action                    VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF lifedispremiumsionlya%ROWTYPE;

      rowset    typerowset;
      next_id   lifedispremiumsionlya.lifedispremiumsionlyaid%TYPE;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM lifedispremiumsionlya i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.insureonlyapplicantid = i_insureonlyapplicantid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO lifedispremiumsionlya
                    VALUES rowset (i);
            END LOOP;

            tab_count ('lifedispremiumsionlya') :=
                             tab_count ('lifedispremiumsionlya')
                             + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM lifedispremiumsionlya i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.insureonlyapplicantid = i_insureonlyapplicantid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT lifedispremiumsionlyaseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               rowset (i).lifedispremiumsionlyaid := next_id;
               rowset (i).insureonlyapplicantid := n_insureonlyapplicantid;
               rowset (i).copyid := n_copyid;

               INSERT INTO lifedispremiumsionlya
                    VALUES rowset (i);
            END LOOP;

            tab_count ('lifedispremiumsionlya') :=
                             tab_count ('lifedispremiumsionlya')
                             + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      lifedispremiumsionlya i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.insureonlyapplicantid = i_insureonlyapplicantid
                    AND i.copyid = i_copyid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('lifedispremiumsionlya') :=
                          tab_count ('lifedispremiumsionlya')
                          + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in LIFEDISPREMIUMSIONLYA_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE request_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF request%ROWTYPE;

      rowset    typerowset;
      next_id   request.requestid%TYPE;

      TYPE typeid IS TABLE OF request.requestid%TYPE;

      old_id    typeid;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM request
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO request
                    VALUES rowset (i);

               borrowerrequestassoc_copy (i_institutionprofileid,
                                          rowset (i).requestid,
                                          i_copyid,
                                          rowset (i).requestid,
                                          n_copyid,
                                          'C'
                                         );
               servicerequest_copy (i_institutionprofileid,
                                    rowset (i).requestid,
                                    i_copyid,
                                    rowset (i).requestid,
                                    n_copyid,
                                    'C'
                                   );
               servicerequestcontact_copy (i_institutionprofileid,
                                           rowset (i).requestid,
                                           i_copyid,
                                           rowset (i).requestid,
                                           n_copyid,
                                           'C'
                                          );
            END LOOP;
         WHEN 'N'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM request
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            old_id := typeid ();

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT requestseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               old_id.EXTEND ();
               old_id (i) := rowset (i).requestid;
               rowset (i).requestid := next_id;
               rowset (i).dealid := n_dealid;
               rowset (i).copyid := n_copyid;

               INSERT INTO request
                    VALUES rowset (i);

               borrowerrequestassoc_copy (i_institutionprofileid,
                                          old_id (i),
                                          i_copyid,
                                          rowset (i).requestid,
                                          n_copyid,
                                          'N'
                                         );
               servicerequest_copy (i_institutionprofileid,
                                    old_id (i),
                                    i_copyid,
                                    rowset (i).requestid,
                                    n_copyid,
                                    'N'
                                   );
               servicerequestcontact_copy (i_institutionprofileid,
                                           old_id (i),
                                           i_copyid,
                                           rowset (i).requestid,
                                           n_copyid,
                                           'N'
                                          );
            END LOOP;
         WHEN 'D'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM request
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN 1 .. rowset.COUNT
            LOOP
               borrowerrequestassoc_copy (i_institutionprofileid,
                                          rowset (i).requestid,
                                          i_copyid,
                                          rowset (i).requestid,
                                          n_copyid,
                                          'D'
                                         );
               servicerequest_copy (i_institutionprofileid,
                                    rowset (i).requestid,
                                    i_copyid,
                                    rowset (i).requestid,
                                    n_copyid,
                                    'D'
                                   );
               servicerequestcontact_copy (i_institutionprofileid,
                                           rowset (i).requestid,
                                           i_copyid,
                                           rowset (i).requestid,
                                           n_copyid,
                                           'D'
                                          );
            END LOOP;

            DELETE      request
                  WHERE dealid = i_dealid
                    AND copyid = i_copyid
                    AND institutionprofileid = i_institutionprofileid;
         ELSE
            NULL;
      END CASE;

      tab_count ('request') := tab_count ('request') + rowset.COUNT;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in request_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE borrowerrequestassoc_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_requestid              request.requestid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_requestid              request.requestid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF borrowerrequestassoc%ROWTYPE;

      rowset   typerowset;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM borrowerrequestassoc i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.requestid = i_requestid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO borrowerrequestassoc
                    VALUES rowset (i);
            END LOOP;

            tab_count ('borrowerrequestassoc') :=
                              tab_count ('borrowerrequestassoc')
                              + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM borrowerrequestassoc i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.requestid = i_requestid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).borrowerid :=
                                find_id (rowset (i).borrowerid, m_borrowerid);
               rowset (i).requestid := n_requestid;
               rowset (i).copyid := n_copyid;

               INSERT INTO borrowerrequestassoc
                    VALUES rowset (i);
            END LOOP;

            tab_count ('borrowerrequestassoc') :=
                              tab_count ('borrowerrequestassoc')
                              + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      borrowerrequestassoc i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.requestid = i_requestid
                    AND i.copyid = i_copyid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('borrowerrequestassoc') :=
                           tab_count ('borrowerrequestassoc')
                           + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in borrowerrequestassoc_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE servicerequest_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_requestid              request.requestid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_requestid              request.requestid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF servicerequest%ROWTYPE;

      rowset   typerowset;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM servicerequest i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.requestid = i_requestid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO servicerequest
                    VALUES rowset (i);
            END LOOP;

            tab_count ('servicerequest') :=
                                    tab_count ('servicerequest')
                                    + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM servicerequest i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.requestid = i_requestid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).requestid := n_requestid;
               rowset (i).copyid := n_copyid;

               INSERT INTO servicerequest
                    VALUES rowset (i);
            END LOOP;

            tab_count ('servicerequest') :=
                                    tab_count ('servicerequest')
                                    + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      servicerequest i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.requestid = i_requestid
                    AND i.copyid = i_copyid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('servicerequest') :=
                                 tab_count ('servicerequest')
                                 + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in servicerequest_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE servicerequestcontact_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_requestid              request.requestid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_requestid              request.requestid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF servicerequestcontact%ROWTYPE;

      rowset    typerowset;
      next_id   servicerequestcontact.servicerequestcontactid%TYPE;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM servicerequestcontact i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.requestid = i_requestid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO servicerequestcontact
                    VALUES rowset (i);
            END LOOP;

            tab_count ('servicerequestcontact') :=
                             tab_count ('servicerequestcontact')
                             + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM servicerequestcontact i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.requestid = i_requestid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT servicerequestcontactseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               rowset (i).servicerequestcontactid := next_id;
               rowset (i).requestid := n_requestid;
               rowset (i).copyid := n_copyid;

               INSERT INTO servicerequestcontact
                    VALUES rowset (i);
            END LOOP;

            tab_count ('servicerequestcontact') :=
                             tab_count ('servicerequestcontact')
                             + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      servicerequestcontact i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.requestid = i_requestid
                    AND i.copyid = i_copyid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('servicerequestcontact') :=
                          tab_count ('servicerequestcontact')
                          + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in servicerequestcontact_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE component_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF component%ROWTYPE;

      rowset    typerowset;
      next_id   component.componentid%TYPE;

      TYPE typeid IS TABLE OF component.componentid%TYPE;

      old_id    typeid;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM component
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO component
                    VALUES rowset (i);

               CASE rowset (i).componenttypeid
                  WHEN 1
                  THEN
                     componentmortgage_copy (i_institutionprofileid,
                                             rowset (i).componentid,
                                             i_copyid,
                                             rowset (i).componentid,
                                             n_copyid,
                                             'C'
                                            );
                  WHEN 2
                  THEN
                     componentloc_copy (i_institutionprofileid,
                                        rowset (i).componentid,
                                        i_copyid,
                                        rowset (i).componentid,
                                        n_copyid,
                                        'C'
                                       );
                  WHEN 3
                  THEN
                     componentloan_copy (i_institutionprofileid,
                                         rowset (i).componentid,
                                         i_copyid,
                                         rowset (i).componentid,
                                         n_copyid,
                                         'C'
                                        );
                  WHEN 4
                  THEN
                     componentcreditcard_copy (i_institutionprofileid,
                                               rowset (i).componentid,
                                               i_copyid,
                                               rowset (i).componentid,
                                               n_copyid,
                                               'C'
                                              );
                  WHEN 5
                  THEN
                     componentoverdraft_copy (i_institutionprofileid,
                                              rowset (i).componentid,
                                              i_copyid,
                                              rowset (i).componentid,
                                              n_copyid,
                                              'C'
                                             );
                  ELSE
                     NULL;
               END CASE;
            END LOOP;
         WHEN 'N'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM component
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            old_id := typeid ();

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT componentseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               old_id.EXTEND ();
               old_id (i) := rowset (i).componentid;
               rowset (i).componentid := next_id;
               rowset (i).dealid := n_dealid;
               rowset (i).copyid := n_copyid;

               INSERT INTO component
                    VALUES rowset (i);

               CASE rowset (i).componenttypeid
                  WHEN 1
                  THEN
                     componentmortgage_copy (i_institutionprofileid,
                                             old_id (i),
                                             i_copyid,
                                             rowset (i).componentid,
                                             n_copyid,
                                             'N'
                                            );
                  WHEN 2
                  THEN
                     componentloc_copy (i_institutionprofileid,
                                        old_id (i),
                                        i_copyid,
                                        rowset (i).componentid,
                                        n_copyid,
                                        'N'
                                       );
                  WHEN 3
                  THEN
                     componentloan_copy (i_institutionprofileid,
                                         old_id (i),
                                         i_copyid,
                                         rowset (i).componentid,
                                         n_copyid,
                                         'N'
                                        );
                  WHEN 4
                  THEN
                     componentcreditcard_copy (i_institutionprofileid,
                                               old_id (i),
                                               i_copyid,
                                               rowset (i).componentid,
                                               n_copyid,
                                               'N'
                                              );
                  WHEN 5
                  THEN
                     componentoverdraft_copy (i_institutionprofileid,
                                              old_id (i),
                                              i_copyid,
                                              rowset (i).componentid,
                                              n_copyid,
                                              'N'
                                             );
                  ELSE
                     NULL;
               END CASE;
            END LOOP;
         WHEN 'D'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM component
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN 1 .. rowset.COUNT
            LOOP
               CASE rowset (i).componenttypeid
                  WHEN 1
                  THEN
                     componentmortgage_copy (i_institutionprofileid,
                                             rowset (i).componentid,
                                             i_copyid,
                                             rowset (i).componentid,
                                             n_copyid,
                                             'D'
                                            );
                  WHEN 2
                  THEN
                     componentloc_copy (i_institutionprofileid,
                                        rowset (i).componentid,
                                        i_copyid,
                                        rowset (i).componentid,
                                        n_copyid,
                                        'D'
                                       );
                  WHEN 3
                  THEN
                     componentloan_copy (i_institutionprofileid,
                                         rowset (i).componentid,
                                         i_copyid,
                                         rowset (i).componentid,
                                         n_copyid,
                                         'D'
                                        );
                  WHEN 4
                  THEN
                     componentcreditcard_copy (i_institutionprofileid,
                                               rowset (i).componentid,
                                               i_copyid,
                                               rowset (i).componentid,
                                               n_copyid,
                                               'D'
                                              );
                  WHEN 5
                  THEN
                     componentoverdraft_copy (i_institutionprofileid,
                                              rowset (i).componentid,
                                              i_copyid,
                                              rowset (i).componentid,
                                              n_copyid,
                                              'D'
                                             );
                  ELSE
                     NULL;
               END CASE;
            END LOOP;

            DELETE      component
                  WHERE dealid = i_dealid
                    AND copyid = i_copyid
                    AND institutionprofileid = i_institutionprofileid;
         ELSE
            NULL;
      END CASE;

      tab_count ('component') := tab_count ('component') + rowset.COUNT;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in component_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE componentmortgage_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_componentid            component.componentid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_componentid            component.componentid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF componentmortgage%ROWTYPE;

      rowset   typerowset;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM componentmortgage i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.componentid = i_componentid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO componentmortgage
                    VALUES rowset (i);
            END LOOP;

            tab_count ('componentmortgage') :=
                                 tab_count ('componentmortgage')
                                 + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM componentmortgage i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.componentid = i_componentid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).componentid := n_componentid;
               rowset (i).copyid := n_copyid;

               INSERT INTO componentmortgage
                    VALUES rowset (i);
            END LOOP;

            tab_count ('componentmortgage') :=
                                 tab_count ('componentmortgage')
                                 + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      componentmortgage i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.componentid = i_componentid
                    AND i.copyid = i_copyid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('componentmortgage') :=
                              tab_count ('componentmortgage')
                              + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in componentmortgage_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE componentloc_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_componentid            component.componentid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_componentid            component.componentid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF componentloc%ROWTYPE;

      rowset   typerowset;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM componentloc i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.componentid = i_componentid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO componentloc
                    VALUES rowset (i);
            END LOOP;

            tab_count ('componentloc') :=
                                      tab_count ('componentloc')
                                      + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM componentloc i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.componentid = i_componentid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).componentid := n_componentid;
               rowset (i).copyid := n_copyid;

               INSERT INTO componentloc
                    VALUES rowset (i);
            END LOOP;

            tab_count ('componentloc') :=
                                      tab_count ('componentloc')
                                      + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      componentloc i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.componentid = i_componentid
                    AND i.copyid = i_copyid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('componentloc') :=
                                   tab_count ('componentloc')
                                   + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in componentloc_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE componentcreditcard_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_componentid            component.componentid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_componentid            component.componentid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF componentcreditcard%ROWTYPE;

      rowset   typerowset;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM componentcreditcard i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.componentid = i_componentid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO componentcreditcard
                    VALUES rowset (i);
            END LOOP;

            tab_count ('componentcreditcard') :=
                               tab_count ('componentcreditcard')
                               + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM componentcreditcard i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.componentid = i_componentid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).componentid := n_componentid;
               rowset (i).copyid := n_copyid;

               INSERT INTO componentcreditcard
                    VALUES rowset (i);
            END LOOP;

            tab_count ('componentcreditcard') :=
                               tab_count ('componentcreditcard')
                               + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      componentcreditcard i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.componentid = i_componentid
                    AND i.copyid = i_copyid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('componentcreditcard') :=
                            tab_count ('componentcreditcard')
                            + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in componencreditcard_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE componentloan_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_componentid            component.componentid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_componentid            component.componentid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF componentloan%ROWTYPE;

      rowset   typerowset;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM componentloan i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.componentid = i_componentid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO componentloan
                    VALUES rowset (i);
            END LOOP;

            tab_count ('componentloan') :=
                                     tab_count ('componentloan')
                                     + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM componentloan i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.componentid = i_componentid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).componentid := n_componentid;
               rowset (i).copyid := n_copyid;

               INSERT INTO componentloan
                    VALUES rowset (i);
            END LOOP;

            tab_count ('componentloan') :=
                                     tab_count ('componentloan')
                                     + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      componentloan i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.componentid = i_componentid
                    AND i.copyid = i_copyid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('componentloan') :=
                                  tab_count ('componentloan')
                                  + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in componentloan_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE componentoverdraft_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_componentid            component.componentid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_componentid            component.componentid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF componentoverdraft%ROWTYPE;

      rowset   typerowset;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM componentoverdraft i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.componentid = i_componentid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO componentoverdraft
                    VALUES rowset (i);
            END LOOP;

            tab_count ('componentoverdraft') :=
                                tab_count ('componentoverdraft')
                                + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM componentoverdraft i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.componentid = i_componentid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).componentid := n_componentid;
               rowset (i).copyid := n_copyid;

               INSERT INTO componentoverdraft
                    VALUES rowset (i);
            END LOOP;

            tab_count ('componentoverdraft') :=
                                tab_count ('componentoverdraft')
                                + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      componentoverdraft i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.componentid = i_componentid
                    AND i.copyid = i_copyid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('componentoverdraft') :=
                             tab_count ('componentoverdraft')
                             + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in componentoverdraft_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   PROCEDURE componentsummary_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_dealid                 deal.dealid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_dealid                 deal.dealid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
   IS
      TYPE typerowset IS TABLE OF componentsummary%ROWTYPE;

      rowset   typerowset;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM componentsummary
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO componentsummary
                    VALUES rowset (i);
            END LOOP;

            tab_count ('componentsummary') :=
                                  tab_count ('componentsummary')
                                  + rowset.COUNT;
         WHEN 'N'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM componentsummary
             WHERE dealid = i_dealid
               AND copyid = i_copyid
               AND institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).dealid := n_dealid;
               rowset (i).copyid := n_copyid;

               INSERT INTO componentsummary
                    VALUES rowset (i);
            END LOOP;

            tab_count ('componentsummary') :=
                                  tab_count ('componentsummary')
                                  + rowset.COUNT;
         WHEN 'D'
         THEN
            DELETE      componentsummary
                  WHERE dealid = i_dealid
                    AND copyid = i_copyid
                    AND institutionprofileid = i_institutionprofileid;

            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('componentsummary') :=
                               tab_count ('componentsummary')
                               + SQL%ROWCOUNT;
            END IF;
         ELSE
            NULL;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in componentsummary_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   FUNCTION contact_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_contactid              contact.contactid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
      RETURN NUMBER
   IS
      TYPE typerowset IS TABLE OF contact%ROWTYPE;

      rowset       typerowset;
      next_id      contact.contactid%TYPE;
      new_addrid   addr.addrid%TYPE;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM contact i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.contactid = i_contactid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN 0;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;
               new_addrid :=
                  addr_copy (i_institutionprofileid,
                             rowset (i).addrid,
                             i_copyid,
                             n_copyid,
                             'C'
                            );

               INSERT INTO contact
                    VALUES rowset (i);
            END LOOP;

            tab_count ('contact') := tab_count ('contact') + rowset.COUNT;
            RETURN i_contactid;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM contact i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.contactid = i_contactid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN 0;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT contactseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               rowset (i).contactid := next_id;
               rowset (i).copyid := n_copyid;
               new_addrid :=
                  addr_copy (i_institutionprofileid,
                             rowset (i).addrid,
                             i_copyid,
                             n_copyid,
                             'N'
                            );
               rowset (i).addrid := new_addrid;

               INSERT INTO contact
                    VALUES rowset (i);
            END LOOP;

            tab_count ('contact') := tab_count ('contact') + rowset.COUNT;
            RETURN next_id;
         WHEN 'D'
         THEN
            SELECT *
            BULK COLLECT INTO rowset
              FROM contact i
             WHERE i.contactid = i_contactid
               AND i.copyid = i_copyid
               AND i.institutionprofileid = i_institutionprofileid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN 0;
            END IF;

            DELETE      contact i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.contactid = i_contactid
                    AND i.copyid = i_copyid;

            FOR i IN 1 .. rowset.COUNT
            LOOP
               new_addrid :=
                  addr_copy (i_institutionprofileid,
                             rowset (i).addrid,
                             i_copyid,
                             n_copyid,
                             'D'
                            );
            END LOOP;

            tab_count ('contact') := tab_count ('contact') + rowset.COUNT;
            RETURN i_contactid;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in Contact_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   FUNCTION addr_copy (
      i_institutionprofileid   deal.institutionprofileid%TYPE,
      i_addrid                 addr.addrid%TYPE,
      i_copyid                 deal.copyid%TYPE,
      n_copyid                 deal.copyid%TYPE,
      action                   VARCHAR2
   )
      RETURN NUMBER
   IS
      TYPE typerowset IS TABLE OF addr%ROWTYPE;

      rowset    typerowset;
      next_id   addr.addrid%TYPE;
   BEGIN
      CASE action
         WHEN 'C'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM addr i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.addrid = i_addrid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN 0;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               rowset (i).copyid := n_copyid;

               INSERT INTO addr
                    VALUES rowset (i);
            END LOOP;

            tab_count ('addr') := tab_count ('addr') + rowset.COUNT;
            RETURN i_addrid;
         WHEN 'N'
         THEN
            SELECT i.*
            BULK COLLECT INTO rowset
              FROM addr i
             WHERE i.institutionprofileid = i_institutionprofileid
               AND i.addrid = i_addrid
               AND i.copyid = i_copyid;

            IF (rowset.COUNT = 0)
            THEN
               RETURN 0;
            END IF;

            FOR i IN rowset.FIRST .. rowset.LAST
            LOOP
               SELECT addrseq.NEXTVAL
                 INTO next_id
                 FROM DUAL;

               rowset (i).addrid := next_id;
               rowset (i).copyid := n_copyid;

               INSERT INTO addr
                    VALUES rowset (i);
            END LOOP;

            tab_count ('addr') := tab_count ('addr') + rowset.COUNT;
            RETURN next_id;
         WHEN 'D'
         THEN
            DELETE      addr i
                  WHERE i.institutionprofileid = i_institutionprofileid
                    AND i.addrid = i_addrid
                    AND i.copyid = i_copyid;

            --DBMS_OUTPUT.PUT_LINE('addrid:'||i_addrid||' deleted.');
            IF SQL%ROWCOUNT > 0
            THEN
               tab_count ('addr') := tab_count ('addr') + SQL%ROWCOUNT;
            END IF;

            RETURN i_addrid;
      END CASE;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'in Addr_copy'
                               || ' Error code '
                               || SQLCODE
                               || ': '
                               || SUBSTR (SQLERRM, 1, 64)
                              );
         RAISE;
   END;

   FUNCTION find_id (KEY NUMBER, src tab_id)
      RETURN NUMBER
   IS
      i   NUMBER;
   BEGIN
      IF src.COUNT > 0
      THEN
         i := 0;

         WHILE i < src.LAST
         LOOP
            i := i + 1;

            IF src (i).old_id = KEY
            THEN
               RETURN src (i).new_id;
            END IF;
         END LOOP;
      END IF;

      RETURN -1;
   END;

   PROCEDURE init_tabcount
   IS
   BEGIN
      tab_count ('lifedispremiumsionlya') := 0;
      tab_count ('addr') := 0;
      tab_count ('appraisalorder') := 0;
      tab_count ('asset') := 0;
      tab_count ('borroweridentification') := 0;
      tab_count ('borrowerrequestassoc') := 0;
      tab_count ('bridge') := 0;
      tab_count ('componentcreditcard') := 0;
      tab_count ('componentloan') := 0;
      tab_count ('componentloc') := 0;
      tab_count ('componentmortgage') := 0;
      tab_count ('componentoverdraft') := 0;
      tab_count ('componentsummary') := 0;
      tab_count ('contact') := 0;
      tab_count ('creditreference') := 0;
      tab_count ('deal') := 0;
      tab_count ('dealfee') := 0;
      tab_count ('documenttrackingverbiage') := 0;
      tab_count ('downpaymentsource') := 0;
      tab_count ('employmenthistory') := 0;
      tab_count ('escrowpayment') := 0;
      tab_count ('income') := 0;
      tab_count ('liability') := 0;
      tab_count ('lifedisabilitypremiums') := 0;
      tab_count ('propertyexpense') := 0;
      tab_count ('servicerequest') := 0;
      tab_count ('servicerequestcontact') := 0;
      tab_count ('borrower') := 0;
      tab_count ('borroweraddress') := 0;
      tab_count ('component') := 0;
      tab_count ('documenttracking') := 0;
      tab_count ('insureonlyapplicant') := 0;
      tab_count ('property') := 0;
      tab_count ('request') := 0;
   END;

   FUNCTION numberofchangedtable
      RETURN NUMBER
   IS
      changed_table   NUMBER        := 0;
      table_name      VARCHAR2 (64);
   BEGIN
      table_name := tab_count.FIRST;

      LOOP
         EXIT WHEN table_name IS NULL;

         --dbms_output.put_line('table: '||tab_name||'  value:'||copy_management_new2.tab_count(tab_name));
         IF tab_count (table_name) > 0
         THEN
            changed_table := changed_table + 1;
         END IF;

         table_name := tab_count.NEXT (table_name);
      END LOOP;

      RETURN changed_table;
   END;

   FUNCTION printchanges
      RETURN VARCHAR2
   IS
      table_name   VARCHAR2 (64);
      changes      VARCHAR2 (32767) := '';
   BEGIN
      table_name := tab_count.FIRST;

      LOOP
         EXIT WHEN table_name IS NULL;
         changes :=
                 changes || table_name || ',' || tab_count (table_name)
                 || ';';
         --dbms_output.put_line(table_name||','||tab_count(table_name)||';');
         table_name := tab_count.NEXT (table_name);
      END LOOP;

      RETURN changes;
   END;
END copy_management;
/


