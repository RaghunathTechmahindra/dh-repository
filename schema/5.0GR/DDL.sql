set serveroutput on;

----------------------------------------------------------------------------------------------------
-- DEALLOCKS
----------------------------------------------------------------------------------------------------
ALTER TABLE  DEALLOCKS
ADD ( SESSIONID varchar2(1024) );


----------------------------------------------------------------------------------------------------
-- Adding FK constraint between Deal and MasterDeal
----------------------------------------------------------------------------------------------------
 
ALTER TABLE deal ADD CONSTRAINT fk_deal_masterdeal
  FOREIGN KEY (dealid,institutionprofileid)
  REFERENCES masterdeal(dealid,institutionprofileid);
 
 
CREATE INDEX i_deal_masterdeal ON deal(dealid,institutionprofileid)
  TABLESPACE mos_index
  COMPUTE STATISTICS ONLINE;

----------------------------------------------------------------------------------------------------
-- Qualify Rate schema change
----------------------------------------------------------------------------------------------------

ALTER TABLE DEAL
ADD( QualifyingOverrideFlag char(1));

ALTER TABLE DEAL
ADD( qualifyingRateOverrideFlag char(1));

ALTER TABLE DEAL
ADD( OverrideQualProd NUMBER(13));

ALTER TABLE MTGPROD
ADD( QualifyingRateEligibleFlag char(1));

----------------------------------------------------------------------------------------------------
-- Condition Verbiage on-the-fly
----------------------------------------------------------------------------------------------------

CREATE TABLE DTVERBIAGEGENERATIONTYPE
(
  DTVERBIAGEGENERATIONTYPEID  NUMBER(2)   DEFAULT 0   NOT NULL,
  DESCRIPTION                 VARCHAR2(63)
);

ALTER TABLE DTVERBIAGEGENERATIONTYPE ADD (
  CONSTRAINT DTVERBIAGEGENERATIONTYPE_PK
 PRIMARY KEY
 (DTVERBIAGEGENERATIONTYPEID));

ALTER TABLE CONDITION
ADD (DTVERBIAGEGENERATIONTYPEID NUMBER(2));

ALTER TABLE CONDITION
ADD CONSTRAINT FK_COND_DTVGENTYPE
FOREIGN KEY
  (DTVERBIAGEGENERATIONTYPEID)
REFERENCES
  DTVERBIAGEGENERATIONTYPE
  (DTVERBIAGEGENERATIONTYPEID)
ENABLE
VALIDATE;


----------------------------------------------------------------------------------------------------
-- Quick Links 
----------------------------------------------------------------------------------------------------
-- create sequence QUICKLINKSSEQ;
CREATE SEQUENCE QUICKLINKSSEQ
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;


-- Create Quick Links Table script

CREATE TABLE QUICKLINKS
(
  QUICKLINKID           NUMBER(13),
  LINKNAMEENGLISH       VARCHAR2(100),
  LINKNAMEFRENCH        VARCHAR2(100),
  URLLINK               VARCHAR2(500),
  BRANCHPROFILEID       NUMBER(13),
  INSTITUTIONPROFILEID  NUMBER(13),    
  SORTORDER             NUMBER(2),
  FAVICON               VARCHAR2(500)
);


-- Unique Constraint
CREATE INDEX FK_QUICKLINKS_I ON QUICKLINKS
(BRANCHPROFILEID, INSTITUTIONPROFILEID);

-- PK contraint
ALTER TABLE QUICKLINKS ADD (
  CONSTRAINT PK_QUICKLINKS
 PRIMARY KEY
 (QUICKLINKID,INSTITUTIONPROFILEID));

-- FK contraint
ALTER TABLE QUICKLINKS ADD (
  CONSTRAINT FK_QUICKLINKS_BRANCHPROFILE 
 FOREIGN KEY (INSTITUTIONPROFILEID, BRANCHPROFILEID) 
 REFERENCES BranchProfile (INSTITUTIONPROFILEID,BRANCHPROFILEID));
 
 CREATE TABLE PAGELOCKS
 (
   PAGEID                NUMBER(13) NOT NULL,
   USERPROFILEID         NUMBER(13) NOT NULL,
   LOCKTYPE              CHAR(1)    DEFAULT 'E',
   LOCKTIME              DATE       DEFAULT sysdate NOT NULL,
   REASON                VARCHAR2(50),
   INSTITUTIONPROFILEID  NUMBER(13),
   SESSIONID   			VARCHAR2(500)
 );
 
 CREATE UNIQUE INDEX I_PK_PAGELOCKS ON PAGELOCKS
 (INSTITUTIONPROFILEID, PAGEID, USERPROFILEID);
 
 
 CREATE INDEX I_FK_PAGELOCKS ON PAGELOCKS
 (INSTITUTIONPROFILEID, USERPROFILEID);
 
 
 ALTER TABLE PAGELOCKS ADD (
   CONSTRAINT PK_PAGELOCKS
  PRIMARY KEY
  (INSTITUTIONPROFILEID, PAGEID, USERPROFILEID));
 
 -- PAGE Table - add column to hold PageLockOnEdit information 
 
ALTER TABLE PAGE ADD PAGELOCKONEDIT CHAR(1) DEFAULT 'N';

 ----------------------------------------------------------------------------------------
 
 
----------------------------------------------------------------------------------------------------
-- MI Start 
----------------------------------------------------------------------------------------------------

CREATE TABLE MORTGAGEINSURANCETYPEASSOC
(
  INSTITUTIONPROFILEID     NUMBER,
  MORTGAGEINSURERID        NUMBER,
  MORTGAGEINSURANCETYPEID  NUMBER
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;

ALTER TABLE MORTGAGEINSURANCETYPEASSOC ADD (
  CONSTRAINT MORTGAGEINSURANCETYPEASSOC_PK
 PRIMARY KEY
 (INSTITUTIONPROFILEID, MORTGAGEINSURANCETYPEID, MORTGAGEINSURERID));


ALTER TABLE DEAL ADD( MIDEALREQUESTNUMBER NUMBER(4) DEFAULT 0);

ALTER TABLE DEAL ADD (PREVIOUSMIINDICATORID NUMBER(2) DEFAULT 0);

ALTER TABLE SERVICERESPONSEQUEUE ADD( LASTPROCESSDATE DATE);

---------------------------------------------------------------------------------------------------
-- MI End 
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- SET DEFAULT 0 for Copy Management defect fix update START 
-- TBD
----------------------------------------------------------------------------------------------------

--FXP28812:  V5.0_Merge - Remove documentprofile.maildestinationtypeID 
ALTER TABLE DOCUMENTPROFILE DROP COLUMN MAILDESTINATIONTYPEID;

--FXP33272 - transaction history page timing out
CREATE INDEX i_fk_dch_masterdeal ON dealchangehistory(applicationid,institutionprofileid)
  TABLESPACE mos_index
  COMPUTE STATISTICS ONLINE;

--FXP33313- Existing Account Reference field has incorrect max length on Deal Entry and Deal Mod pages 
ALTER TABLE DEAL MODIFY (REFEXISTINGMTGNUMBER VARCHAR2(35));

--FXP33405 - Mapping_SOURCEFIRMPROFILE. SFLICENSEREGISTRATIONNUMBER - Increase size to 15
ALTER TABLE SOURCEFIRMPROFILE MODIFY (SFLICENSEREGISTRATIONNUMBER VARCHAR2(15));

-- FXP33106: 	Gold 4.4.1- Deal Search is showing question mark in the field value ... 
alter table borrower modify SUFFIXID default 0;


ALTER TABLE REQUEST MODIFY ( CHANNELTRANSACTIONKEY VARCHAR2(36));
ALTER TABLE RESPONSE MODIFY ( CHANNELTRANSACTIONKEY VARCHAR2(36));
ALTER TABLE SERVICERESPONSEQUEUE MODIFY ( CHANNELTRANSACTIONKEY VARCHAR2(36));

-- QC286
ALTER TABLE BRANCHPROFILE ADD (FAXCERTIFICATE CHAR(1 BYTE));
