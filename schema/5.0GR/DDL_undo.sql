
----------------------------------------------------------------------------------------------------
-- DEALLOCKS
----------------------------------------------------------------------------------------------------
ALTER TABLE  DEALLOCKS
drop ( SESSIONID );


----------------------------------------------------------------------------------------------------
-- Adding FK constraint between Deal and MasterDeal
----------------------------------------------------------------------------------------------------
 
ALTER TABLE deal DROP  CONSTRAINT fk_deal_masterdeal;
 
DROP INDEX i_deal_masterdeal;
 
----------------------------------------------------------------------------------------------------
-- Qualify Rate schema change
----------------------------------------------------------------------------------------------------

ALTER TABLE DEAL
drop( QualifyingOverrideFlag);

ALTER TABLE DEAL
drop( QualifyingRateOverrideFlag);

ALTER TABLE DEAL
drop( OverrideQualProd);

ALTER TABLE MTGPROD
drop (QualifyingRateEligibleFlag );

----------------------------------------------------------------------------------------------------
-- Condition Verbiage on-the-fly
----------------------------------------------------------------------------------------------------

drop table DTVERBIAGEGENERATIONTYPE cascade constraints;
alter table condition drop column DTVERBIAGEGENERATIONTYPEID;

----------------------------------------------------------------------------------------------------
-- Quick Links 
----------------------------------------------------------------------------------------------------
-- Drop table and its constraints 
DROP TABLE QUICKLINKS CASCADE CONSTRAINTS;

-- Drop sequence 
DROP SEQUENCE QUICKLINKSSEQ;

-- Drop pageLocks Table
DROP TABLE PAGELOCKS CASCADE CONSTRAINTS;

ALTER TABLE PAGE DROP COLUMN PAGELOCKONEDIT;
DROP SEQUENCE QUICKLINKSSEQ;


----------------------------------------------------------------------------------------------------
-- MI Start 
----------------------------------------------------------------------------------------------------
DROP TABLE MORTGAGEINSURANCETYPEASSOC;

alter table SERVICERESPONSEQUEUE drop column LASTPROCESSDATE;

ALTER TABLE DEAL drop (MIDEALREQUESTNUMBER );

ALTER TABLE DEAL drop (PREVIOUSMIINDICATORID );
---------------------------------------------------------------------------------------------------
-- MI End 
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- SET DEFAULT 0 for Copy Management defect fix update START 
-- TBD
----------------------------------------------------------------------------------------------------

--FXP28812:  V5.0_Merge - Remove documentprofile.maildestinationtypeID 
ALTER TABLE DOCUMENTPROFILE ADD MAILDESTINATIONTYPEID NUMBER(2) DEFAULT 0;

--FXP33405 - Mapping_SOURCEFIRMPROFILE. SFLICENSEREGISTRATIONNUMBER - Increase size to 15
ALTER TABLE SOURCEFIRMPROFILE MODIFY (SFLICENSEREGISTRATIONNUMBER VARCHAR2(5));

-- FXP33106: 	Gold 4.4.1- Deal Search is showing question mark in the field value ... 

alter borrower modify SUFFIXID default -1;
