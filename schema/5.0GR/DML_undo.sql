delete from businessrule where rulename = 'DE-622';
delete from sysproperty where name = 'com.filogix.qualrateoverride';
delete from sysproperty where name = 'com.filogix.qualoverridechk';
delete from sysproperty where name = 'com.basis100.calc.gdstds.cmhclowratioratecode';
delete from auditfield where auditfieldid in (36,37,38);

----------------------------------------------------------------------------------------------------
-- Condition Verbiage on-the-fly
----------------------------------------------------------------------------------------------------
delete from DTVERBIAGEGENERATIONTYPE;

----------------------------------------------------------------------------------------------------
-- Quick Links 
----------------------------------------------------------------------------------------------------

DELETE from USERTYPEPAGEACCESS where PAGEID = 538;

DELETE from page  where PAGEID = 538;


----------------------------------------------------------------------------------------------------
-- MI Start 
----------------------------------------------------------------------------------------------------
delete from request where serviceProductid >= 34;
delete from SERVICEPRODUCTREQUESTASSOC where SERVICEPRODUCTID in (34, 35);
delete from serviceResponseQueue where serviceProductid >= 34;
delete from SERVICEPRODUCT where SERVICEPRODUCTID in (34, 35);
DELETE FROM MORTGAGEINSURANCETYPEASSOC;
delete from PAYLOADTYPE where payloadtypeid = 28;
delete from serviceprovider where serviceproviderid in (12, 13);
update MORTGAGEINSURER set serviceproviderid = null where MORTGAGEINSURERID in (1, 2);
delete from channel where CHANNELID = 21;

delete from SERVICETYPE where SERVICETYPEID = 12;
delete from PROCESSSTATUS where PROCESSSTATUSID = 4;

delete from SYSPROPERTY where NAME like 'dscu.userprofileid';
delete from SYSPROPERTY where NAME like 'com.filogix.ecm.deallock.retryInterval.seconds';



---------------------------------------------------------------------------------------------------
-- MI End 
----------------------------------------------------------------------------------------------------
 
