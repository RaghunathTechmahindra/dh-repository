Alter table channel add WSSERVICEPORT varchar2(30);
Alter table payloadtype add XMLSCHEMANAME varchar2(30), NoNAMESCHEMALOCATION varchar(75);

----------- Update Channel Info

update channel 
	set ip='10.1.1.255', userid='user_appraisal', password='pws_appraisal', path='/appraisal/services/AppraisalService', wsserviceport='AppraisalService' 
	where channelid=3;
update channel 
	set ip='10.1.1.255', userid='user_closing', password='pws_closing', path='/closing/services/ClosingService', wsserviceport='ClosingService' 
	where channelid=5;


----------- Update Payload Info and modify serviceproductrequestassoc and related request

insert into documenttype values (92, 'Appraisal Upload REQUEST');
insert into documenttype values (93, 'Appraisal Upload CANCEL');
update documenttype set documenttypename='AVM upload REQUEST' where documenttypeid=90;
update documenttype set documenttypename='AVM upload CANCEL' where documenttypeid=91;


update payloadtype set payloadtypedesc='AVM upload REQUEST' where payloadtypeid=10;
update payloadtype set payloadtypedesc='AVM upload CANCEL' where payloadtypeid=11;
update payloadtype 
	set xmlschemaname='ClosingRequest', nonameschemalocation='http://10.1.1.225:8080/closing/schemas/closingOrderRequest.xsd' 
	where payloadtypeid=9;
insert into payloadtype 
	values (12, 92, 'Appraisal Upload REQUEST', 1, 'AppraisalRequest', 'http://10.1.1.225:8080/appraisal/schemas/appraisalOrderRequest.xsd');
insert into payloadtype 
	values (13, 93, 'Appraisal Upload CANCEL', 1, 'ServiceCancel', 'http://10.1.1.225:8080/appraisal/schemas/cancelOrderRequest.xsd');

insert into serviceproductrequestassoc values (4, 3, 12, 1, 'Full Appraisal', 17, 'APP_REQ' );
update request set payloadtypeid=12 where channelid=3 and serviceproductid=4 and servicetransactiontypeid=1;
delete from serviceproductrequestassoc where requesttypeid=8;
update serviceproductrequestassoc set requesttypeid=8 where requesttypeid=17;


insert into serviceproductrequestassoc values (5, 3, 12, 1, 'Drive-by Appraisal', 17, 'APP_REQ' );
update request set payloadtypeid=12 where channelid=3 and serviceproductid=5 and servicetransactiontypeid=1;
delete from serviceproductrequestassoc where requesttypeid=9;
update serviceproductrequestassoc set requesttypeid=9 where requesttypeid=17;

insert into serviceproductrequestassoc values (4, 3, 13, 3, 'Cancel Full Appraisal', 17, 'APP_CAN' );
update request set payloadtypeid=13 where channelid=3 and serviceproductid=4 and servicetransactiontypeid=3;
delete from serviceproductrequestassoc where requesttypeid=15;
update serviceproductrequestassoc set requesttypeid=15 where requesttypeid=17;


insert into serviceproductrequestassoc values (5, 3, 13, 3, 'Cancel Drive-by Appraisal', 17, 'APP_CAN' );
update request set payloadtypeid=12 where channelid=3 and serviceproductid=5 and servicetransactiontypeid=3;
delete from serviceproductrequestassoc where requesttypeid=16;
update serviceproductrequestassoc set requesttypeid=16 where requesttypeid=17;

commit;