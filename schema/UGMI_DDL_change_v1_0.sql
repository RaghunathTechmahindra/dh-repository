------------------------- AIG UG Database Modification -------------------------

--------------------------------------------------------------------------------
------- CREATE new tables and Sequences
--------------------------------------------------------------------------------
--Create EVENTCALLBACKRESPONSE table
CREATE TABLE EVENTCALLBACKRESPONSE(
  CALLBACKID NUMBER(3) PRIMARY KEY,
  SERVICETYPEID NUMBER(3) NOT NULL CONSTRAINT ECB_SVT_FK REFERENCES SERVICETYPE(SERVICETYPEID) ON DELETE CASCADE,
  XMLRESPONSE CLOB,                  ----change made from BLOB to CLOB under the request of Asif
  RECEIVEDTIMESTAMP DATE,
  STATUS CHAR(1) DEFAULT 0 NOT NULL
);

--- Create Sequence EVENTCALLBACKRESPONSESEQ
CREATE SEQUENCE EVENTCALLBACKRESPONSESEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;

--Create PROCESSSTATUS table ===> JULY 13, 2006
CREATE TABLE PROCESSSTATUS(
  PROCESSSTATUSID NUMBER(2) PRIMARY KEY,
  PROCESSSTATUSDESC VARCHAR2(35) NOT NULL
);

--Create SEQUENCE PROCESSSTATUSSEQ ===> JULY 13, 2006

CREATE SEQUENCE PROCESSSTATUSSEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;

--Create SERVICERESPONSEQUEUE table ===> JULY 13, 2006
CREATE TABLE SERVICERESPONSEQUEUE(
  SERVICERESPONSEQUEUEID NUMBER(13) PRIMARY KEY,
  DEALID NUMBER(13) NOT NULL,
  COPYID NUMBER(13) NOT NULL,
  SERVICEPRODUCTID NUMBER(3) NOT NULL REFERENCES SERVICEPRODUCT(SERVICEPRODUCTID) ON DELETE CASCADE,
  PROCESSSTATUSID NUMBER(2) NOT NULL REFERENCES PROCESSSTATUS(PROCESSSTATUSID)ON DELETE CASCADE,
  RETRYCOUNTER NUMBER(13) NOT NULL,
  RESPONSEXML CLOB NOT NULL,                 
  RESPONSEDATE DATE NOT NULL,
  CHANNELTRANSACTIONKEY VARCHAR2(30), --- CANNOT BE A FOREIGN KEY AS THE CORRESPONDING COLUMN IN THE REQUEST TABLE IS NOT UNIQUE AND NULLABLE
  CONSTRAINT SRVRESQ_DEAL_FK FOREIGN KEY(DEALID, COPYID) REFERENCES DEAL(DEALID, COPYID) ON DELETE CASCADE
);

--Create SEQUENCE SERVICERESPONSEQUEUESEQ ===> JULY 13, 2006
CREATE SEQUENCE SERVICERESPONSEQUEUESEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;



--------------------------------------------------------------------------------
------- MODIFY tables
--------------------------------------------------------------------------------

--- FEE 
ALTER TABLE FEE ADD (
  MITYPEFLAG CHAR(1) DEFAULT '0' NOT NULL,
  INCLUDEINTOTALFEEAMOUNT CHAR(1) DEFAULT '0' NOT NULL 
);

--- MORTGAGEINSURER (not Finalized)

ALTER TABLE MORTGAGEINSURER ADD(DATXMIPREMIUMSUPPORTED CHAR(1) DEFAULT '0' NOT NULL,
				DATXMISUPPORTED        CHAR(1) DEFAULT '0' NOT NULL,
				SERVICEPROVIDERID      NUMBER(3),
				DISTRIBUTIONPERCENTAGE NUMBER(5,2));

--- DEAL
ALTER TABLE DEAL ADD(MIPOLICYNUMAIGUG      VARCHAR2(50),
		     MIFEEAMOUNT           NUMBER(13,2),
		     MIPREMIUMERRORMESSAGE VARCHAR2(256));
										 
--- BRANCHPROFILE
ALTER TABLE BRANCHPROFILE ADD(AIGUGBRANCHTRANSITNUMBER VARCHAR2(20));	

-- INSTITUTIONPROFILE
ALTER TABLE INSTITUTIONPROFILE ADD(AIGUGID VARCHAR2(35));

-- REQUEST ==> JULY 13, 2006

ALTER TABLE REQUEST ADD(CHANNELTRANSACTIONKEY VARCHAR2(30);					 
								

--------------------------------------------------------------------------------
--- POPULATE Tables
--------------------------------------------------------------------------------

------ Add Records to Table FEETYPE ------

INSERT INTO FEETYPE  
  VALUES(37,'AIG UG Fee');
  
INSERT INTO FEETYPE  
  VALUES(38,'AIG UG Fee Payable');
  
INSERT INTO FEETYPE  
  VALUES(39,'AIG UG Premium');

INSERT INTO FEETYPE  
  VALUES(40,'AIG UG Premium Payable');
  
INSERT INTO FEETYPE  
  VALUES(41,'AIG UG PST On Premium');
  
INSERT INTO FEETYPE  
  VALUES(42,'AIG UG PST On Premium Payable');
  
------ Add Records to Table FEE ------

--1
INSERT INTO FEE 
  VALUES(37, 'AIG UG Fee',NULL, NULL, 37, 38, 0, 5, 'AIG UG Fee', 'N', 165, 0, 1, 'N', 'Y', 'Y');
  
--2
INSERT INTO FEE 
  VALUES(38, 'AIG UG Fee Payable',NULL, NULL, 38, NULL, 0, 0, 'AIG UG Fee Payable', 'Y', 0, 0, 0, 'N', 'Y', 'N');

--3
INSERT INTO FEE 
  VALUES(39, 'AIG UG Premium',NULL, NULL, 39, 40, 0, 5, 'AIG UG Premium', 'N', 0, 0, 1, 'N', 'Y', 'N');
  
--4
INSERT INTO FEE 
  VALUES(40, 'AIG UG Premium Payable',NULL, NULL, 40,NULL, 0, 0, 'AIG UG Premium Payable', 'Y', 0, 0, 0, 'N', 'Y', 'N');
  
--5
INSERT INTO FEE  
  VALUES(41, 'AIG UG PST On Premium', NULL, NULL, 41, 42, 0, 5, 'AIG UG PST On Premium', 'N', 0, 0, 1, 'N', 'Y', 'N');
  
--6
INSERT INTO FEE 
  VALUES(42, 'AIG UG PST On Premium Payable',NULL, NULL, 42, NULL, 0, 0, 'AIG UG PST On Premium Payable', 'Y', 0, 0, 0, 'N', 'Y', 'N');

------ Add record to table MORTGAGEINSURER (NOT FINALIZED) ------

INSERT INTO MORTGAGEINSURER VALUES (3, AIG UG, 'Y', 'Y', NULL, NULL);

------ Add record to table MORTGAGEINSURANCETYPE ------
INSERT INTO MORTGAGEINSURANCETYPE 
	VALUES(20, 'A Minus Advantage');

INSERT INTO MORTGAGEINSURANCETYPE 
	VALUES(21, 'Low Doc');
	
INSERT INTO MORTGAGEINSURANCETYPE 
	VALUES(22, 'Credit Guide');
	
------ Add record to table SERVICEPROVIDER (NOT FINALIZED) ------ 
INSERT INTO SERVICEPROVIDER 
	VALUES(TBD,N/A, N/A, 'AIG UG', TBD, NULL, TBD);

------ Add record to table SERVICEPRODUCT (NOT FINALIZED) ------ 
INSERT INTO SERVICEPRODUCT
	VALUES(TBD, 5, 7, NULL, 'AIG UG', 'MI Premuim', 'Mortgage Insurance Premium Calculation', '1.0', TBD, TBD, TBD);
	
INSERT INTO SERVICEPRODUCT
	VALUES(TBD, 5, 4, NULL, 'AIG UG', 'Mortgage Insurance', 'Mortgage Insurance', '1.0', TBD, TBD, TBD);
	
------ Add record to table TIMEOUTDEFINITIONASSOC (NOT FINALIZED) ------ 
INSERT INTO TIMEOUTDEFINITIONASSOC
	VALUES(1, TBD, TBD, TBD);

INSERT INTO TIMEOUTDEFINITIONASSOC
	VALUES(1, TBD, TBD, TBD);
	
------ Add record to table SERVICEPRODUCTREQUESTASSOC (NOT FINALIZED) ------ 
INSERT INTO SERVICEPRODUCTREQUESTASSOC
	VALUES(TBD, 1, 'Premium Request', 1, 'AIG UG Premium', TBD, TBD);

INSERT INTO SERVICEPRODUCTREQUESTASSOC
	VALUES(TBD, 1, 'Mortgage Insurance Request', 1, 'AIG UG MI', TBD, TBD);
	
------ Add record to table PAYLOADTYPE (NOT FINALIZED) ------ 	
INSERT INTO PAYLOADTYPE
	VALUES(TBD, NULL, 'Premium Request', TBD, TBD, '1.0');
	
INSERT INTO PAYLOADTYPE
	VALUES(TBD, NULL, 'Mortgage Insurance Request', TBD, TBD, '1.0');
	
------ Add record to table REQUESTSTATUS (NOT FINALIZED) ------ 
INSERT INTO REQUESTSTATUS
	VALUES(TBD, 'Submit', 'Request submitted');
	
INSERT INTO REQUESTSTATUS
	VALUES(TBD, 'Error Submit', 'Error during submission');
	
------ Add record to table RESPONSESTATUS (NOT FINALIZED) ------ 
INSERT INTO RESPONSESTATUS
	VALUES(TBD, 'Received', 'Response submitted');
	
INSERT INTO REQUESTSTATUS
	VALUES(TBD, 'Error Received', 'Error during response process');	

------ Add record to table USERTYPE ------ JULY 13, 2006

INSERT INTO USERTYPE(USERTYPEID, UTDESCRIPTION) VALUES(98, 'ECM');

------ Add record to table USERPROFILE ------ JULY 13, 2006

INSERT INTO USERPROFILE (USERPROFILEID, USERLOGIN, USERTYPEID, GROUPPROFILEID, CONTACTID, STANDARDACCESS, BILINGUAL) VALUES(USERPROFILESEQ.NEXTVAL, 'ECM_1', 98, 1, 99399, 'Y', 'N');
	
------ POPULATE LOOKUP TABLE PROCESSSTATUS ------ JULY 14, 2006
DELETE FROM PROCESSSTATUS; --- Make sure no other values exist
INSERT INTO PROCESSSTATUS VALUES(0, 'Available');
INSERT INTO PROCESSSTATUS VALUES(1, 'Processing');
INSERT INTO PROCESSSTATUS VALUES(2, 'Processed');
INSERT INTO PROCESSSTATUS VALUES(3, 'Error');

-------------------------------------------------------------------------
--- UPDATE DATA 
-------------------------------------------------------------------------

------ Update Table FEE ------

UPDATE FEE
SET    FEEDESCRIPTION = 'Appraisal Fee',
			 MITYPEFLAG  = 'N',
			 INCLUDEINTOTALFEEAMOUNT = 'Y'
WHERE  FEEID = 0;

UPDATE FEE
SET    FEEDESCRIPTION = 'BuyDown Fee',
			 MITYPEFLAG  = 'N',
			 INCLUDEINTOTALFEEAMOUNT = 'Y'
WHERE  FEEID = 2;

UPDATE FEE
SET    FEEDESCRIPTION = 'CMHC Fee',
			 MITYPEFLAG  = 'Y',
			 INCLUDEINTOTALFEEAMOUNT = 'Y'
WHERE  FEEID = 3;

UPDATE FEE
SET    FEEDESCRIPTION = 'Appraisal Fee Payable',
			 MITYPEFLAG  = 'N',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 5;

UPDATE FEE
SET    FEEDESCRIPTION = 'CMHC Fee Payable',
			 MITYPEFLAG  = 'N',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 6;

UPDATE FEE
SET    FEEDESCRIPTION = 'Agent Referral Fee',
			 MITYPEFLAG  = 'N',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 7;

UPDATE FEE
SET    FEEDESCRIPTION = 'Origination Fee',
			 MITYPEFLAG  = 'N',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 8;

UPDATE FEE
SET    FEEDESCRIPTION = 'Application Fee',
			 MITYPEFLAG  = 'N',
			 INCLUDEINTOTALFEEAMOUNT = 'Y'
WHERE  FEEID = 9;

UPDATE FEE
SET    FEEDESCRIPTION = 'CMHC Premium',
			 MITYPEFLAG  = 'Y',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 11;

UPDATE FEE
SET    FEEDESCRIPTION = 'CMHC Premium Payable',
			 MITYPEFLAG  = 'Y',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 12;

UPDATE FEE
SET    FEEDESCRIPTION = 'CMHC PST On Premium',
			 MITYPEFLAG  = 'Y',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 13;

UPDATE FEE
SET    FEEDESCRIPTION = 'CMHC PST On Premium Payable',
			 MITYPEFLAG  = 'Y',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 14;

UPDATE FEE
SET    FEEDESCRIPTION = 'GE Capital Fee',
			 MITYPEFLAG  = 'Y',
			 INCLUDEINTOTALFEEAMOUNT = 'Y'
WHERE  FEEID = 15;

UPDATE FEE
SET    FEEDESCRIPTION = 'GE Capital Fee Payable',
			 MITYPEFLAG  = 'Y',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 16;

UPDATE FEE
SET    FEEDESCRIPTION = 'GE Capital Premium',
			 MITYPEFLAG  = 'Y',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 17;

UPDATE FEE
SET    FEEDESCRIPTION = 'GE Capital Premium Payable',
			 MITYPEFLAG  = 'Y',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 18;

UPDATE FEE
SET    FEEDESCRIPTION = 'GE Capital PST On Premium',
			 MITYPEFLAG  = 'Y',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 19;

UPDATE FEE
SET    FEEDESCRIPTION = 'GE Capital PST On Premium Payable',
			 MITYPEFLAG  = 'Y',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 20;

UPDATE FEE
SET    FEEDESCRIPTION = 'Appraisal Fee (Additional)',
			 MITYPEFLAG  = 'N',
			 INCLUDEINTOTALFEEAMOUNT = 'Y'
WHERE  FEEID = 22;

UPDATE FEE
SET    FEEDESCRIPTION = 'Property Tax Holdback (interim)',
			 MITYPEFLAG  = 'N',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 25;

UPDATE FEE
SET    FEEDESCRIPTION = 'Property Tax Holdback (final)',
			 MITYPEFLAG  = 'N',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 26;

UPDATE FEE
SET    FEEDESCRIPTION = 'Courier Fee',
			 MITYPEFLAG  = 'N',
			 INCLUDEINTOTALFEEAMOUNT = 'Y'
WHERE  FEEID = 27;

UPDATE FEE
SET    FEEDESCRIPTION = 'Appraisal Valuation Fee Payable',
			 MITYPEFLAG  = 'N',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 32;

UPDATE FEE
SET    FEEDESCRIPTION = 'AIG UG Fee',
			 MITYPEFLAG  = 'Y',
			 INCLUDEINTOTALFEEAMOUNT = 'Y'
WHERE  FEEID = 37;

UPDATE FEE
SET    FEEDESCRIPTION = 'AIG UG Fee Payable',
			 MITYPEFLAG  = 'Y',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 38;

UPDATE FEE
SET    FEEDESCRIPTION = 'AIG UG Premium',
			 MITYPEFLAG  = 'Y',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 39;

UPDATE FEE
SET    FEEDESCRIPTION = 'AIG UG Premium Payable',
			 MITYPEFLAG  = 'Y',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 40;

UPDATE FEE
SET    FEEDESCRIPTION = 'AIG UG PST On Premium',
			 MITYPEFLAG  = 'Y',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 41;

UPDATE FEE
SET    FEEDESCRIPTION = 'AIG UG PST On Premium Payable',
			 MITYPEFLAG  = 'Y',
			 INCLUDEINTOTALFEEAMOUNT = 'N'
WHERE  FEEID = 42;

------ Update Table MORTGAGEINSURER ------
UPDATE MORTGAGEINSURER
SET    DATXMIPREMIUMSUPPORTED = 'N',
			 DATXMISUPPORTED = 'N',
			 SERVICEPROVIDERID = NULL,
			 DISTRIBUTIONPERCENTAGE = NULL
WHERE  MORTGAGEINSURERID = 0;

UPDATE MORTGAGEINSURER
SET    MORTGAGEINSURERNAME = 'CMHC',
       DATXMIPREMIUMSUPPORTED = 'N',
			 DATXMISUPPORTED = 'N',
			 SERVICEPROVIDERID = NULL,
			 DISTRIBUTIONPERCENTAGE = NULL
WHERE  MORTGAGEINSURERID = 1;

UPDATE MORTGAGEINSURER
SET    MORTGAGEINSURERNAME = 'Genworth',
       DATXMIPREMIUMSUPPORTED = 'N',
			 DATXMISUPPORTED = 'N',
			 SERVICEPROVIDERID = NULL,
			 DISTRIBUTIONPERCENTAGE = NULL
WHERE  MORTGAGEINSURERID = 2;

------ Update Table SERVICETYPE ------
UPDATE SERVICETYPE
SET    SERVICETYPENAME = 'Mortgage Insurance (MI)',
			 SERVICETYPEDESC = 'Mortgage Insurance'
WHERE  SERVICETYPEID = 5;	 

------ Update Table SERVICESUBTYPE ------ 
UPDATE SERVICESUBTYPE
SET    SERVICESUBTYPENAME = 'Mortgage Insurance',
			 SERVICESUBTYPEDESC = 'Mortgage Insurance'
WHERE  SERVICETYPEID = 5	
AND    SERVICESUBTYPEID = 4;

UPDATE SERVICESUBTYPE
SET    SERVICESUBTYPENAME = 'Low Ratio Assessment',
			 SERVICESUBTYPEDESC = 'Low Ratio Assessment'
WHERE  SERVICETYPEID = 5	
AND    SERVICESUBTYPEID = 5;

UPDATE SERVICESUBTYPE
SET    SERVICESUBTYPENAME = 'Mortgage Insurance Premium'
			 WHERE  SERVICETYPEID = 5	
AND    SERVICESUBTYPEID = 7;

UPDATE SERVICESUBTYPE
SET    SERVICESUBTYPENAME = 'Mortgage Ins Pre-Qualification'
			 WHERE  SERVICETYPEID = 5	
AND    SERVICESUBTYPEID = 8;


COMMIT;
/