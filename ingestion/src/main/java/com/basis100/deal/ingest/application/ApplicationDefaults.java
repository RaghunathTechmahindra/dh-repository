package com.basis100.deal.ingest.application;


import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.ingest.RequiredElementException;
import com.basis100.deal.ingest.constraints.DefaultConstraint;
import com.basis100.deal.ingest.constraints.IngestionConstraint;
import com.basis100.deal.ingest.constraints.RequiredConstraint;
import com.basis100.deal.xml.EntityNodeAssociation;
import com.basis100.log.SysLogger;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.xml.DomUtil;
import com.basis100.xml.XmlToolKit;


public class ApplicationDefaults
{
  private SessionResourceKit srk;

  // changed from Node to Map for ML
  // one record of this is origianl Ingestion, now each Institution
  Map<String, Node> roots;
  SysLogger logger;
  private File rules;

  private String codePackage;


  public ApplicationDefaults(SessionResourceKit srk, File desc)throws Exception
  {
      this.srk = srk;
      logger =  srk.getSysLogger();
      rules = desc;
      init();
  }

  public void init()throws Exception
  {

    DocumentBuilder parser = XmlToolKit.getInstance().newDocumentBuilder();

    if(rules == null || !rules.exists())
      throw new Exception("File not found: " + rules.getAbsolutePath());

    try
    {
      Document rulesNodes = parser.parse(rules);
      Node root = DomUtil.getFirstNamedChild(rulesNodes, "Ingestion");
      List institutions = DomUtil.getChildElements(root);
      roots = new HashMap<String, Node>(institutions.size());
      for(int i = 0; i<institutions.size(); i++ )
      {
          Node aNode = (Node)institutions.get(i);
          Node abberNode = aNode.getAttributes().getNamedItem("abber");
          roots.put(abberNode.getNodeValue(), aNode);          
      }

      logger.trace("Loaded Ingestion Rules...");
    }
    catch(Exception e)
    {
      throw new Exception("XML File Parse Error - File: " + rules.getName());
    }

  }

  public void checkDefaults(EntityNodeAssociation assoc, String abber)
  {
     DealEntity de = assoc.getEntity();

     List nodes = getConstraints("Default",String.valueOf(de.getClassId()), abber);

     Iterator it = nodes.iterator();

     String value = null;

     while(it.hasNext())
     {
       Node current = (Node)it.next();

       String field = DomUtil.getAttributeValue(current,"targetField");
       // zivko:marker
       if(field != null && field.equalsIgnoreCase("actualpaymentterm")){
         System.out.println("here is mi actualPaymentTerm");
       }
       if(!assoc.isSetField(field))
       {
         value = handleDefaultValueNode(assoc,field,current);

         if(value != null)
          logger.trace("INGESTION: DEFAULT: Setting field: " + field + " to: " + value);
       }

     }

     return;
  }

//add de.forceChange(field) to fix ticket #4097
  private String handleDefaultValueNode(EntityNodeAssociation ass, String field, Node parent)
  {
    DealEntity de = ass.getEntity();

    Node valNode = DomUtil.getFirstNamedChild(parent,"Value");
    String type = DomUtil.getAttributeValue(valNode,"type");
    type = type.toLowerCase();
    String value = DomUtil.getChildValue(parent,"Value");

    try
    {
      if(type.equals("string"))
      {
        de.setField(field,value);
        de.forceChange(field);
        return value;
      }
      /*else if(type.equals("rule"))
      {
       ;
      } */
      else if(type.equals("code"))
      {
        handleCode(ass,value);
        de.forceChange(field);
        return value;
      }
      else if(type.equals("picklist"))  //Note: same as string type. Original intent was to
      {                                 //have all picklist defaults = 0 - changed for BGW
        de.setField(field, value);
        de.forceChange(field);
        return value;
      }
      else if(type.equals("field"))   //transpose value from a recieved field to another field
      {
        String val = de.getStringValue(value);

        if(val != null)
        {
          de.setField(field,val);
          de.forceChange(field);
          return val;
        }
      }
    }
    catch(Exception e)
    {
      String msg = e.getMessage();
      if(msg == null) msg = "Unknown.";

      msg = "INGESTION: ERROR: failed to set default field: " + field + " for: "
             + de.getEntityTableName() + " Reason: " + msg;

      ResourceManager.getSysLogger("0").error(msg);
    }

    return null;
  }


  public List getConstraints(String action,String entityClassId, String abber)
  {
    Node root = (Node)roots.get(abber);
    List constraints = DomUtil.getNamedDescendants(root, "Constraint");
    List found = new ArrayList();

    Iterator it =  constraints.iterator();
    Node node = null;

    while(it.hasNext())
    {
      node = (Node)it.next();

      if(entityClassId.equalsIgnoreCase(DomUtil.getAttributeValue(node,"targetEntity"))
        && action.equals(DomUtil.getAttributeValue(node,"action")))
        {
          found.add(node);
        }
    }

    return found;
  }

   public void handleCode(EntityNodeAssociation ass, String code)throws RequiredElementException, Exception
  {
     Class cls = Class.forName(code);
     IngestionConstraint ic = (IngestionConstraint)cls.newInstance();

     if(ic instanceof DefaultConstraint)
     {
       DefaultConstraint df = (DefaultConstraint)ic;
       df.execute(srk,ass);

     }
     else if(ic instanceof RequiredConstraint)
     {
       RequiredConstraint df = (RequiredConstraint)ic;

       df.execute(srk,ass);
     }

  }




}
