package com.basis100.deal.ingest.constraints;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.EmploymentHistory;
import com.basis100.deal.entity.JobTitle;
import com.basis100.deal.pk.JobTitlePK;
import com.basis100.deal.xml.EntityNodeAssociation;
import com.basis100.resources.*;

public class JobTitleDefault implements DefaultConstraint
{

  public void execute(SessionResourceKit srk, EntityNodeAssociation ass) throws Exception
  {
    DealEntity de = ass.getEntity();
    try
    {
      String cls = de.getClass().getName();

      if(de.getClassId() == ClassId.EMPLOYMENTHISTORY)
      {
         EmploymentHistory eh = (EmploymentHistory)ass.getEntity();

         if(!ass.isSetField("jobTitleId"))
         {
            JobTitle title = new JobTitle(srk);

            title = title.findByName("Other");

            if(title == null) return;

            eh.setIndustrySectorId(title.getIndustrySectorId());
            eh.setJobTitleId(title.getJobTitleId());
            eh.setOccupationId(title.getOccupationId());

         }
         else
         {
            JobTitle title = new JobTitle(srk);
            title.findByPrimaryKey(new JobTitlePK(eh.getJobTitleId()));

            if(title.getJobTitleDescription() != null &&
                title.getJobTitleDescription().equals("Unknown"))
            {
                title = title.findByName("Other");

                if(title == null) return;

                eh.setIndustrySectorId(title.getIndustrySectorId());
                eh.setOccupationId(title.getOccupationId());
            }
            else
            {
                eh.setIndustrySectorId(title.getIndustrySectorId());
                eh.setOccupationId(title.getOccupationId());

            }
         }
      }
    }
    catch(Exception e)
    {
       throw new Exception("Default Value not found for " + this.getClass().getName());
    }

    // zivko removed this exception from code because it is not clear why we
    // are throwing exception every time we enter this class and
    // SUCCESSFULY finish processing
    // removed on 28-XI-2003
    //throw new Exception("Default Value not found for " + this.getClass().getName());
  }

}
