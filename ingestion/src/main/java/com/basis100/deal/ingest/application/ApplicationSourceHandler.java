package com.basis100.deal.ingest.application;

import com.basis100.log.SysLogger;
import com.basis100.picklist.*;
import com.basis100.resources.SessionResourceKit;
import com.basis100.xml.DomUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.entity.SourceOfBusinessProfile;
import com.basis100.deal.security.PassiveMessage;
  /**
 *  The ApplicationSourceHandler recieves all SourceRelatedInformation during ingestion
 *  and determines the source of the application or throws a SourceDeterminationException
 *  if the application source cannot be acertained. Source determination follows a few simple
 *  steps:<br>
 * - Gather source component tag info such as MailBoxNbr and SystemType
 * - Determine the SourceOfBusinessProfile via the Deal.sourceSystemMailboxNumber
 * - check the status of the above profile to see if it is valid
 * - Find the firm associatied with the above profile via the SourceToFirmAssoc entity
 * - Match the firm system type with the ingested system type
 *
 */

public class ApplicationSourceHandler
{
  protected String mailbox;
  protected int systemType;

  protected org.w3c.dom.Document dom;

  protected SessionResourceKit srk;
  protected SysLogger logger;

  public ApplicationSourceHandler(SessionResourceKit srk)
  {
    this.srk = srk;
    this.logger = srk.getSysLogger();
  }

  public PassiveMessage handleSourceDetails(Deal deal, Document document) throws SourceDeterminationException, Exception
  {
    PassiveMessage pm = new PassiveMessage();
    SourceProfileCreator sourceCreator = new SourceProfileCreator(srk);

    Node sfNode = DomUtil.getFirstNamedDescendant(document,"SourceFirmProfile");
    String sfCode = DomUtil.getChildValue(sfNode, "sourceFirmCode");
    String lSystemTypeFromNode = DomUtil.getChildValue(sfNode,"systemTypeId");
    
    if (lSystemTypeFromNode == null) {
    	throw new Exception("ERROR : " + this.getClass().getName() + " Could not find node named systemTypeId for source firm profile.  ");
    }

    Node sbNode = DomUtil.getFirstNamedDescendant(document,"SourceOfBusinessProfile");
    String sbCode = DomUtil.getChildValue(sbNode, "sourceOfBusinessCode");

    // ticket #458
    //systemType = deal.getSystemTypeId();
    try {
    	systemType = Integer.parseInt(lSystemTypeFromNode);
    }
    catch(Exception numExc){
    	throw new Exception("ERROR : " + this.getClass().getName() + " Could not create valid integer for system type id :  " + lSystemTypeFromNode );
    }

    //set source firm
    SourceFirmProfile sfp = new SourceFirmProfile(srk);
    sfp = sfp.findBySystemTypeAndSFCode(systemType, sfCode);
    if (sfp == null) {
    	sfp = sourceCreator.createSourceFirmProfile(deal,document);
    	adjustNameFields(sfp);
    }
    else {
    	sourceCreator.modifySFPFromNode(sfp,document);
    }

    //set source of business
    SourceOfBusinessProfile sob = new SourceOfBusinessProfile(srk);
    sob = sob.findBySystemTypeSFPAndSOBCode(systemType, sfp.getSourceFirmProfileId(), sbCode);
    if (sob == null) {
    	sob = sourceCreator.createSOBProfile(deal, document, systemType, "SourceOfBusinessProfile");
    	adjustNameFields(sob);
    }
    else {
    	sourceCreator.modifySOBFromNode(sob, document,"SourceOfBusinessProfile");
    }

    //set source of business agent
    SourceOfBusinessProfile soba = new SourceOfBusinessProfile(srk);
	Node sbaNode = DomUtil.getFirstNamedDescendant(document, "SourceOfBusinessProfileAgent");
    if (sbaNode == null)
    	soba = sob;  //set equal to primary sob 
    else
    {
    	String sbaCode = DomUtil.getChildValue(sbaNode, "sourceOfBusinessCode");

	    soba = soba.findBySystemTypeSFPAndSOBCode(systemType, sfp.getSourceFirmProfileId(), sbaCode);

	    //FXP30063 - when agent == source we get a constraints violation below.
	    // At this point it should be a safe assumption that source and agent come 
	    // from the same firm and system, so we're only checking codes.
	    if (sbaCode.equals(sbCode))
	    {
	    	soba = sob;
	    }
	    else
	    {
		    if(soba == null)
		    {
				soba = sourceCreator.createSOBProfile(deal, document, systemType, "SourceOfBusinessProfileAgent");
				adjustNameFields(soba);
			}
		    else 
		    {
		    	sourceCreator.modifySOBFromNode(soba, document, "SourceOfBusinessProfileAgent");
		    }
	    }

    }
    //end of sobAgent
    
    sob.setSourceFirmProfileId( sfp.getSourceFirmProfileId() );
    soba.setSourceFirmProfileId( sfp.getSourceFirmProfileId() );
    sfp.ejbStore();
    sob.ejbStore();
    soba.ejbStore();  //store soba record only when it's not the same as sob?  otherwise this just stores sob twice

    String channelMedia = PicklistData.getMatchingColumnValue(deal.getInvestorProfileId(),
                                                              "SystemType",systemType,"CHANNELMEDIA");
    if( channelMedia != null ){
      deal.setChannelMedia(channelMedia);
    }else{
      deal.setChannelMedia("E");
    }

    deal.setSourceOfBusinessProfileId( sob.getSourceOfBusinessProfileId() );
    deal.setSourceFirmProfileId(sfp.getSourceFirmProfileId());
    deal.setSourceOfBusinessProfile2ndaryId(soba.getSourceOfBusinessProfileId());
    deal.ejbStore();

    return pm;
  }

  private void adjustNameFields(Object obj){
	if (obj instanceof SourceFirmProfile) {
		SourceFirmProfile lSfp = (SourceFirmProfile)obj;
		if (lSfp.getSourceFirmName() == null ) {
			if (lSfp.getSourceFirmCode() != null) {
				lSfp.setSourceFirmName( lSfp.getSourceFirmCode() );
				if (lSfp.getSourceFirmCode().length() > 10 ) {
					lSfp.setSfShortName(lSfp.getSourceFirmCode().substring(0,10));
				}
				else {
					lSfp.setSfShortName( lSfp.getSourceFirmCode() );
				}
			}
		}
	}
	else if(obj instanceof SourceOfBusinessProfile) {
		SourceOfBusinessProfile lSob = (SourceOfBusinessProfile)obj;
		try{
			Contact con = lSob.getContact();
			if( con != null && con.getContactLastName() == null && lSob.getSourceOfBusinessCode() != null) {
				con.setContactLastName( lSob.getSourceOfBusinessCode() );
				con.ejbStore();
			}
		}
		catch(Exception exc){
			// do nothing here
		}
		if(lSob.getSOBPShortName() == null && lSob.getSourceOfBusinessCode() != null) {
			if(lSob.getSourceOfBusinessCode().length() > 10){
				lSob.setSOBPShortName( lSob.getSourceOfBusinessCode().substring(0,10) );
			}
			else {
				lSob.setSOBPShortName( lSob.getSourceOfBusinessCode() );
			}
		}
	}
  }

}
/*
  public PassiveMessage  handleSourceDetails(Deal deal, Document document)  throws SourceDeterminationException, Exception
  {
    PassiveMessage pm = new PassiveMessage();
    SourceProfileCreator sourceCreator = new SourceProfileCreator(srk);

    mailbox = deal.getSourceSystemMailBoxNBR();
    systemType = deal.getSystemTypeId();

    SourceFirmProfile sfp = null;
    SourceOfBusinessProfile sob = null;

    if(mailbox == null || mailbox.length() == 0)
    {
      logger.error("ApplicationSourceHandler: No source mailbox number recieved!");

      pm.addMsg("Application Rejected - Cannot determine source.", pm.CRITICAL);

      return pm;
    }

    // initialize flags
    boolean sobCreated = false;
    boolean sfpCreated = false;

    //Search through the Source of Business Mailbox table matching with the
    //deal.sourceSystemMailboxNumber.

    sob = new SourceOfBusinessProfile(srk);
    sob = sob.findByMailBox(mailbox);

    if(sob == null){
      logger.error("INGESTION: ApplicationSourceHandler: Unidentified SourceOfBusinessProfileId associated with mailbox:" + mailbox);
      sob = sourceCreator.createSOBProfile(deal, document);   //also creates the mailbox record
      sobCreated = true;   // raise the flag
    }else{
      logger.debug("INGESTION: ApplicationSourceHandler: Found SourceOfBusinessProfile for deal:" + deal.getDealId() + " with mailbox: " + mailbox);
    }

     // For matches found: Using the Source of business entry, get the SourceFirmProfile
     // using the SourceToFirmAssoc - match the SourceFirmProfile.sourceSystemTypeId
     // with the Deal.SourceSystemTypeId.

     sfp = new SourceFirmProfile(srk);
     String mBoxSearch = mailbox.trim();
     if( mBoxSearch.length() > 3 ){
        mBoxSearch = mBoxSearch.substring(0,3);
     }
     List firmProfilesList = (List)sfp.findBySFBusinessIdAndSystemType(mBoxSearch,systemType);
     if( firmProfilesList.isEmpty() ){
        sfp = sourceCreator.createSourceFirmProfile(deal,sob,document);
        sfpCreated = true;
     }else{
        sfp = (SourceFirmProfile) firmProfilesList.get(0);
     }

     if(firmProfilesList.size() > 1 ){
      logger.debug("INFO: found more than one source firm profile.");
     }

     //=========================================================================
     // if any of flags is set to true, means that we have to create new
     // association.
     //=========================================================================
     if( sobCreated || sfpCreated ){
      try{
        sourceCreator.createSOBtoSFPAssociation( sfp, sob );
      }catch(Exception exc){
        pm.addMsg("Application Rejected - Cannot cerate SOB-SFP association.", pm.CRITICAL);
      }
     }

     int stat =  sob.getProfileStatusId();

     if(stat == Sc.PROFILE_STATUS_ACTIVE || stat == Sc.PROFILE_STATUS_CAUTION)
     {
       deal.setSourceOfBusinessProfileId(sob.getSourceOfBusinessProfileId());
       deal.setSourceFirmProfileId(sfp.getSourceFirmProfileId());
       deal.setSourceSystemMailBoxNBR(mailbox);
       deal.setChannelMedia("E");
       deal.ejbStore();
     }
     else
     {
       String status = PicklistData.getDescription("ProfileStatus", stat);

       pm.addMsg("Critical Stop: Invalid SourceOfBusinessProfile Status: " + status, pm.CRITICAL);

       return pm;
     }

     return pm;
  }


*/
/*
  public PassiveMessage  handleSourceDetails(Deal deal, Document document)  throws SourceDeterminationException, Exception
  {
    PassiveMessage pm = new PassiveMessage();
    SourceProfileCreator sourceCreator = new SourceProfileCreator(srk);

    mailbox = deal.getSourceSystemMailBoxNBR();
    systemType = deal.getSystemTypeId();

    SourceFirmProfile sfp = null;
    SourceOfBusinessProfile sob = null;

    if(mailbox == null || mailbox.length() == 0)
    {
      logger.error("ApplicationSourceHandler: No source mailbox number recieved!");

      pm.addMsg("Application Rejected - Cannot determine source.", pm.CRITICAL);

      return pm;
    }

    //Search through the Source of Business Mailbox table matching with the
    //deal.sourceSystemMailboxNumber.

    sob = new SourceOfBusinessProfile(srk);
    sob = sob.findByMailBox(mailbox);

    if(sob == null)
    {
      logger.error("INGESTION: ApplicationSourceHandler: Unidentified SourceOfBusinessProfileId associated with mailbox:" + mailbox);

      sob = sourceCreator.createSOBProfile(deal, document);   //also creates the mailbox record
    }

     // For matches found: Using the Source of business entry, get the SourceFirmProfile
     // using the SourceToFirmAssoc - match the SourceFirmProfile.sourceSystemTypeId
     // with the Deal.SourceSystemTypeId.

     sfp = new SourceFirmProfile(srk);

     logger.debug("INGESTION: ApplicationSourceHandler: Found SourceOfBusinessProfile for deal:" + deal.getDealId() + " with mailbox: " + mailbox);

     List associatedFirms = (List)sfp.findBySourceAndDealSystemType(
              (SourceOfBusinessProfilePK)sob.getPk(),(DealPK)deal.getPk());

     if(associatedFirms.isEmpty())
     {
       String msg =  "INGESTION: ApplicationSourceHandler: No systemType match for: ";
       msg += systemType + " - Associated Source Firm could not identified. ";
       msg += "Creating SourceFirmProfile record. ";
       logger.info(msg);

       try
       {
         sfp = sourceCreator.createSourceFirmProfile(deal,sob);
       }
       catch(Exception e)
       {
         String err = "Unable to create SourceFirmProfile or association to SourceOfBusiness";
         logger.error(e);
         throw new SourceDeterminationException(err);
       }
     }
     else
     {
       sfp = (SourceFirmProfile)associatedFirms.get(0);
     }

     int stat =  sob.getProfileStatusId();

     if(stat == Sc.PROFILE_STATUS_ACTIVE || stat == Sc.PROFILE_STATUS_CAUTION)
     {
       deal.setSourceOfBusinessProfileId(sob.getSourceOfBusinessProfileId());
       deal.setSourceFirmProfileId(sfp.getSourceFirmProfileId());
       deal.setSourceSystemMailBoxNBR(mailbox);
       deal.setChannelMedia("E");
       deal.ejbStore();
     }
     else
     {
       String status = PicklistData.getDescription("ProfileStatus", stat);

       pm.addMsg("Critical Stop: Invalid SourceOfBusinessProfile Status: " + status, pm.CRITICAL);

       return pm;
     }

     return pm;
  }

*/
