package com.basis100.deal.ingest.handlers;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */
import java.io.File;
import java.util.Iterator;
import java.util.List;
import com.basis100.log.SysLogger;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.ingest.IngestionHandler;
import com.basis100.deal.ingest.IngestionResult;
import com.basis100.deal.ingest.SourceMonitor;
import com.basis100.deal.ingest.application.SourceProfileCreator;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.workflow.notification.WorkflowNotifier;
import com.basis100.xml.DomUtil;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class SFPAndSOBHandler implements IngestionHandler
{
  protected SessionResourceKit srk;
  public SysLogger logger;

  public SFPAndSOBHandler()
  {
    logger = ResourceManager.getSysLogger("SFPAndSOBHandler");
  }

  /**
   *  Perform any pre-XML parse and store duties in the implementation of this method
   */
  public File preProcess(File file)throws Exception
  {
    return file;
  }

  public IngestionResult storeDocument(org.w3c.dom.Document dom, SessionResourceKit srk, SourceMonitor source )
                                  throws Exception
  {
    logger.debug("INGESTION: " + this.getClass().getName() + " Method storeDocument entered.");
    int lCounter = 0;
    this.srk = srk;
    IngestionResult ingestionResult = new IngestionResult();
    System.out.println("Store document started ......");
    ingestionResult.isNotificationExcused(true);

    Node rootNode = DomUtil.getFirstNamedChild(dom,"expressSourceUpload");
    if (rootNode == null){
      return ingestionResult;
    }

    SourceProfileCreator sourceCreator = new SourceProfileCreator(srk);

    List sflList = DomUtil.getNamedChildren(rootNode,"SourceFirmProfile");
    Iterator lit = sflList.iterator();
    Node oneNode;

    while(lit.hasNext()){
      oneNode = (Node) lit.next();
      try{
        sourceCreator.createSourceFirmProfile(oneNode);
        lCounter ++;
        System.out.println( "Processing SFP --> " + lCounter );
      }catch(Exception exc){
        ingestionResult.setSfpBatchSuccess(false);
        return ingestionResult;
      }
    }
    System.out.println("Store document finished ......");
    ingestionResult.setSfpBatchSuccess(true);
    logger.debug("INGESTION: " + this.getClass().getName() + " Method storeDocument finished.");
    return ingestionResult;
  }

  public void notifyWorkflow(WorkflowNotifier notifier, IngestionResult result)throws Exception  {  }

  public void logSuccess(SessionResourceKit srk, IEntityBeanPK deal, String filename)  {  }

  public void logFailure(SessionResourceKit srk, IEntityBeanPK deal, String filename)  {  }

  //--DocCentral_FolderCreate--21Sep--2004--start--//
  public void logDocCentralFolderCreation(SessionResourceKit srk,
                                        IEntityBeanPK pk,
                                        SourceFirmProfile sourceFirmEntity,
                                        String folderIdentifier) {;}
 //--DocCentral_FolderCreate--21Sep--2004--end--//


  public int processResult(IngestionResult res, File input, Document dom, SourceMonitor source) throws Exception
  {
    if(res.isSfpBatchSuccess()){
      return STORE;
    }else{
      return ROLLBACK;
    }
  }

  //FXP28696, Feb 2012
  // changed from externalCode do to match with ApplicationHandler
  private static String FXLINK_KEY = "ecniLenderId";
  /**
   * <p>getInstitutiionId</p>
   * <p> find InstituionProfileId from Document.
   * 
   * 
   * @param dom
   * @return
   * @throws Exception
   */
  public int getInstitutionId(Document dom, SessionResourceKit srk) throws RemoteException, FinderException
  {
    InstitutionProfile ip = new InstitutionProfile(srk);
    List nodesByName = DomUtil.getNamedDescendants(dom, FXLINK_KEY);
    if (nodesByName == null || nodesByName.size()!=1)
      throw new FinderException();
      
    Node institionNode = (Node)nodesByName.get(0);
    
    //FXP28696, Sep 2010
    //String idStr = institionNode.getNodeValue();
    String idStr = institionNode.getFirstChild().getNodeValue();

    if (idStr== null || idStr.trim().length() == 0) 
      throw new FinderException();
    ip = ip.findByECNID(idStr);
    
    return ip.getInstitutionProfileId();
  }

}
