package com.basis100.deal.ingest.application;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Vector;

import org.w3c.dom.Node;

import MosSystem.Mc;

import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.PricingRateInventory;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.xml.EntityNodeAssociation;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.mtgrates.PricingLogic2;
import com.basis100.mtgrates.RateInfo;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.xml.DomUtil;
import com.filogix.util.Xc;

/**
 * @version 1.1
 * @author: NBC/PP Implementation Team <br>
 *          Date: 06/28/2006 <br>
 *          Change: <br>
 *          Modified handleProcess so that it uses the values from the ingested
 *          file to set the appropriate mortgage product and rate information on
 *          the deal<br> - //AS : changed method signature - added one more
 *          parameter public PassiveMessage handleProcess(Date current, double
 *          ingestRate, Node nodeElements) throws Exception
 * @version 1.2
 * @author: MCM Implementation Team <br>
 *          Story: XS_1.5 <br>
 *          Date: 08/13/2008 <br>
 *          Change: <br>
 *          Added two methods handleMtgProdPRICheckforComp(..) and
 *          handleMtgProdPRICheckforDeal(..) *
 * @version 1.3 *
 * @author: MCM Implementation Team <br>
 *          Date: 12/Aug/2006 <br>
 *          Change: <br>
 *          XS_1.6 13-Aug-2008 : Added sendMailAlert(..) and
 *          frameMailMessage(..) methods.
 * @version 1.4 *
 * @author: MCM Implementation Team <br>
 *          Date: 23/Aug/2008 <br>
 *          Change: <br>
 *          artf767017 23-Aug-2008 :
 * @version 1.5 *
 * @author: MCM Implementation Team <br>
 *          Date: 19/Sep/2008 <br>
 *          Change: <br>
 *          artf783817 19-Sep-2008 : Modified handleMtgProdPRICheckforComp(..)
 *          method.
 * @version 1.6
 * @author: MCM Implementation Team <br>
 *          Date: 24/Oct/2008 <br>
 *          Change: <br>
 *          FXP22615 24-Oct-2008 : Modified handleMtgProdPRICheckforComp(..) passing false in currentComponent.ejbstore(false).
 * @version 1.7
 * @author: MCM Implementation Team <br>
 *          Date: 29/Oct/2008 <br>
 *          Change: <br>
 *          FXP23187 : Modified handleMtgProdPRICheckforComp to call ejbStore on currentComponent.
 *
 */

public class ApplicationLenderHandler implements Xc
{

  private SessionResourceKit srk;
  private Deal deal;
  private SysLogger logger;
  private Date ingestionDate;
  private double inboundRate;

  public final static double DOUBLE_ZERO = 0.0;

  public ApplicationLenderHandler(SessionResourceKit kit, Deal deal)
  {
    this.srk = kit;
    this.logger = srk.getSysLogger();
    this.deal = deal;
  }

  /**
   *
   * @param current - current
   * @param ingestRate - ingestrate
   * @param node - deal node<br>
   * @return - PassiveMessage
   * @throws Exception
   * @version 1.1  <br>
   * Date: 06/28/2006 <br>
   * Author: NBC/PP Implementation Team <br>
   * Change:  <br>
   *    Updates:
   *    1. deal.mtgProdId,
   *    2. deal.paymentTermId
   *    3. deal.pricingProfileId
   *    4. deal.postedRate
   *    5. deal.premium
   *    6. deal.discount
   */
  public PassiveMessage handleProcess(Date current, double ingestRate, Node dealNode) throws Exception
  {
     ingestionDate = current;
     inboundRate = ingestRate;

     PassiveMessage pm = new PassiveMessage();

     String ssmb = deal.getSourceSystemMailBoxNBR();
     int srcSystem = deal.getSystemTypeId();

     LenderProfile lp = determineLender(srcSystem, ssmb);

     if(lp != null)
     {
       deal.setLenderProfileId(lp.getLenderProfileId());
     }
     else
     {
       pm.addMsg("Unable to determine LenderProfile for: " + deal.getDealId(),pm.NONCRITICAL);
       return pm;
     }

     //    ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
     MtgProd mtg = null;
     boolean mtgProdExistsInDom = false;
     boolean prodRateExistsInDom = false;

     Node mtgProdNode = DomUtil.getFirstNamedDescendant(dealNode, "MtgProd");
     String mpBusinessValue = DomUtil.getChildValue(mtgProdNode, "mpBusinessId");
     if (mpBusinessValue != null)
     {
         mtg = new MtgProd(srk,null);
         try {
             mtg.findByMPBusinessId(mpBusinessValue);
             mtgProdExistsInDom = true;
         }
         catch (Exception e)
         {
             logger.debug(e.getMessage());
             logger.debug("Deal/MtgProd/mpBusinessId: " + mpBusinessValue);
             mtg = null;
             mtgProdExistsInDom = false;
         }
     }

     System.out.println(mtg);

     // if ingested mortgage product does not exist, use default mtgprod
     if (mtg == null)
     {
         mtg = determineMtgProd(lp);
     }

     if(mtg != null)
     {
       deal.setMtgProdId(mtg.getMtgProdId());
       deal.setPaymentTermId(mtg.getPaymentTermId());
       deal.setProductTypeId(mtg.getProductTypeId()); //4.2GR FXP27307, FXP24419
     }
     else
     {
       pm.addMsg("Unable to determine Mortgage Product for: " + deal.getDealId(), pm.NONCRITICAL);
       return pm;
     }

     // associate product rate only if the mortgage product
     // in the ingested file exists in the database
     if (mtgProdExistsInDom)
     {
         // check if product rate exists
         Node priNode = DomUtil.getFirstNamedDescendant(dealNode, "PricingRateInventory");
         String effectiveDateValue = DomUtil.getChildValue(priNode, "indexEffectiveDate");

         if (effectiveDateValue != null)
         {
             try
             {
                 //effectiveDateValue = effectiveDateValue.replace('T',' '); // deleted: FXP26840, 4.2GR, Oct 26 09
                 PricingRateInventory pri = new PricingRateInventory(srk);
                 pri.findByPricingProfileAndEffectiveDate(mtg.getPricingProfileId(), effectiveDateValue);
                 deal.setPricingProfileId(pri.getPricingRateInventoryID());
                 deal.setPostedRate(pri.getInternalRatePercentage());
                 prodRateExistsInDom = true;
             }
             catch (Exception e)
             {
                 logger.debug(e.getMessage());
                 logger.debug("Deal/PricingRateInventory/indexEffectiveDate: " + effectiveDateValue);
                 prodRateExistsInDom = false;
             }
         }
     }

     // associate default rate if mortgage product
     // or product rate does not exist in database
     if (!mtgProdExistsInDom || !prodRateExistsInDom)
     {
     RateInfo ri = determineRate(mtg);
     //Note: the deal.pricingProfileId is actually the pricingRateInventoryId for this deal

     if(ri != null)
     {
       deal.setPricingProfileId(ri.getRateId());
       deal.setPostedRate(ri.getPostedRate());
     }
     else if(ri == null)
     {
       pm.addMsg("Unable to determine rates for: " + deal.getDealId(), pm.NONCRITICAL);
       return pm;
     }

	 // #DG722 just let the mossy property control the behaviour
     determineAndSetDiscount(ri);
     }
     //    ***** Change by NBC Impl. Team - Version 1.1 - End*****//
     return pm;
   }

   /**
  *   Gets the lender for this deal based on the source system type.	<br>
  *
  *   If no lender is determined based on system type the default lender is
  *   returned.
  *
  *	  @return the Lender or null if it cannot be determined
  **/

   private LenderProfile determineLender(int sourceSystem, String mailbox)
   {

      String shortName = "";

      switch(sourceSystem)
      {
        //
        //removed as per of David's (Coleman) request
        //
        //case Mc.SYSTEM_TYPE_CLARICA:
        //  shortName = "Clarica";
        //  break;

        case  Mc.SYSTEM_TYPE_MORTY:
          //if(mailbox.equalsIgnoreCase("ING"))  shortName = "ING";
          if(mailbox.length() > 3 ){
            String firstThree = mailbox.trim().substring(0,3);
            if(firstThree.equalsIgnoreCase("amt")){
              shortName = "AMA";
            }
          }
          break;

        default:
          shortName = "";
      }

      try
      {
         LenderProfile lender = new LenderProfile(srk);
         List nLenders = (List)lender.findByShortName(shortName);

         if(nLenders.isEmpty())
         {
           return lender.findByDefaultLender();
         }
         else
         {
           return (LenderProfile)nLenders.get(0); //just return the first one found
         }
       }
       catch(Exception e)
       {
         String msg = e.getMessage();

         if(msg == null) msg = "Unknown Error";

         srk.getSysLogger().error("INGESTION: " + msg + " while determining LenderProfile");

         return null;
       }

   }

  /**
  *	    After the lender is assigned to the deal,	<br>
  *	    retrieve the valid list of products for the assigned lender.	<br>
  *	    Compare the payment term on the deal with the valid payment	<br>
  *	    terms in the retrieved lender products.  If the ingested deal.interestType	<br>
  *	    is 'VIP' then the product (deal.mtgProd)will  be 'VIP'.	<br>
  *	    Otherwise if the payment term is found in the product list, assign the	<br>
  *	    appropriate product to the deal.  If the payment term is not found in the	<br>
  *	    list of valid products, assign the default lender product to the deal via	<br>
  *	    the Mortgage Product Investor Lender Association table.	<br>
  *
  *	    @return the MtgProd or null if it cannot be determined
  *
  *	    Note: setting correct mtg product could be handled as a calc
  **/
   public MtgProd determineMtgProd(LenderProfile lender)
   {
      //retrieve the valid list of products for the assigned lender. -
      //Note - all products available R1 are valid for the default (only lender)

      //List products = null;

      try
      {
        //	Obtain the paymentTermId for the deal by using the actualPaymentTerm in months/12
        //  and compare to PaymentTerm.ptValue.

        double mos = (double)deal.getActualPaymentTerm();

        logger.trace("INGESTION: LENDERHANDLER: determine MtgProd: actualPaymentTerm = "  + mos);

        double mosint = (double)(mos/12);

        String ptvalue = String.valueOf((int)mosint);

        if(mosint < 1 && mosint > 0)
         ptvalue = "0.50";

        logger.debug("INGESTION: LENDERHANDLER: determine MtgProd:  ptValue = "  + ptvalue);

        logger.debug("INGESTION: LENDERHANDLER: determine MtgProd:  interestType = " + deal.getInterestTypeId() + ".");
        String lSearchKey = "" + ptvalue + "_" + deal.getInterestTypeId();

        //FXP24290 - this could potentially be applied to others but for now it's customized
        Boolean useProductType = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), 
        		INGESTION_USE_PRODUCTTYPES, "N").equals("Y");

        if(useProductType)	
           lSearchKey = lSearchKey + '_' + deal.getProductTypeId();
        //end FXP24290

        //FXP24419, Feb 26, 2009, 4.1GR - institutionalising - start
        int institutionProfileId = srk.getExpressState().getDealInstitutionId();
        String lMtgProIdAsStr = BXResources.getSysConfigValue(institutionProfileId, "MORTGAGEPRODUCTDEFAULT", lSearchKey, null);
        //FXP24419, Feb 26, 2009, 4.1GR - institutionalising - end
        // SEAN Routing Order Changes END
        logger.debug("INGESTION: LENDERHANDLER: determine MtgProd:  mortgage product id = "  + lMtgProIdAsStr);
        if(lMtgProIdAsStr != null){
          MtgProd mtgProd = new MtgProd(srk,null,Integer.parseInt(lMtgProIdAsStr));
          if(mtgProd != null){
            logger.debug("INGESTION: LENDERHANDLER: determine MtgProd:  mortgage product found. " );
            //--DisableProductRateTicket#310--10Aug2004--start--//
            // Verify whether this product disabled on not first.
            // This is very temporarily solution to provide the rate data integrity.
            // The new MORTGAGEPRODUCTDEFAULT assoc table bridging the brokers' rates to the interanl express
            // (lender) rates should eliminate determination of a product from the BXResources
            // and return original logic via entities as it should be.

            int prodId = mtgProd.getMtgProdId();
            int prodStatusId = getIngestionMtgProdStatusId(prodId);

            if (prodStatusId == Mc.PRICING_STATUS_ACTIVE)
                return mtgProd;
            //--DisableDisableProductRateTicket#310--10Aug2004//
          }
        }
        logger.debug("INGESTION: LENDERHANDLER: determine MtgProd:  mortgage product NOT found - looking for default. " );
        /*
        if(ptid != -1)
        {
          MtgProd mtgProd = new MtgProd(srk,null);

          //Compare the payment term on the deal with the valid payment terms in valid lender products

          products = (List)mtgProd.findByLenderAndPaymentTerm(lender.getLenderProfileId(),ptid);

          if(products != null && !products.isEmpty())
          {
            int size = products.size();

            logger.trace("INGESTION: LENDERHANDLER: determine MtgProd:  found ("  + size + ") MtgProds useing Lender and PaymentTerm");

            if(size == 1)
            {
              return (MtgProd)products.get(0);
            }
            else
            {
              int it = deal.getInterestTypeId();

              Iterator prit = products.iterator();

              while(prit.hasNext())
              {
                mtgProd = (MtgProd)prit.next();

                if(mtgProd.getInterestTypeId() == it)
                 return mtgProd;
              }
            }//endelse
          }//endif
        }//endif
        */
      }
      catch(Exception e)
      {
        e.printStackTrace();
       logger.warning("INGESTION: LENDERHANDLER: determine MtgProd: No matching product found - determine default...");
       //fall through to default product
      }

      try
      {
        MtgProd mtgProd = new MtgProd(srk,null);
        mtgProd = mtgProd.findDefaultFor(lender);

        logger.trace("INGESTION: LENDERHANDLER: determined MtgProd with name: " + mtgProd.getMtgProdName());

        return mtgProd;
      }
      catch(Exception e)
      {
        logger.trace("INGESTION: LENDERHANDLER: failed to find MtgProd for deal: " + deal.getDealId());
        logger.trace("INGESTION: LENDERHANDLER: Benign exception:");
        logger.trace("INGESTION: LENDERHANDLER: e.getMessage(): " + e.getMessage());
        return null;
      }
   }

   private int getIngestionMtgProdStatusId(int mtgProdId)
    {
      int prodStatusId = 0; //by default push to the default mtgProd if something goes wrong.

      try
      {
        JdbcExecutor jExec = srk.getJdbcExecutor();

        String sql =
         "select p.PRICINGSTATUSID" +
         " FROM  mtgprod m, pricingprofile p" +
         " WHERE m.PRICINGPROFILEID = p.PRICINGPROFILEID AND" +
         " m.MTGPRODID = " + mtgProdId;

         int key = jExec.execute(sql);
         while(jExec.next(key))
         {
           prodStatusId  = jExec.getInt(key, 1);
           break;
         }
         jExec.closeData(key);

       }
       catch(Exception e)
       {
         logger.error("ALH@getIngestionMtgProdStatusId Exception at getting statusId ");
       }
logger.debug("ALH@getIngestionMtgProdStatusId: " + prodStatusId);

       return prodStatusId;
    }

   private RateInfo determineRate(MtgProd product)
   {
      //int institutioId = srk.getExpressState().getDealInstitutionId();

	  /***************MCM Impl team changes starts - XS_2.29*******************
       * MCM Impl Team. XS_2.29 -- Jun 27, 2008
       * Due to the refactoring for PricingLogic2,
       * the signature of the method was changed.
       */
      //PricingLogic2 pLogic = PricingLogic2.getInstance(institutioId);
      PricingLogic2 pLogic = new PricingLogic2();
      /***************MCM Impl team changes ends - XS_2.29*********************/
      //RateInfo theRate = null;

      try
      {
         //--DisableProductRateTicket#570--10Aug2004--start--//
         //// This is a part of future important rate admin fix (disable rate).
         //// By default a deal should be ingested with an active rate and,
         //// therefore, with RateDisabledDate is null.
          /***************MCM Impl team changes starts - XS_2.29*******************
           * MCM Impl Team. XS_2.29 -- Jun 27, 2008
           * Due to the refactoring for PricingLogic2,
           * the signature of the method was changed.
           */
          // Vector rates = pLogic.getLenderProductRates(srk, logger, product.getMtgProdId(), ingestionDate, Mc.PRICING_STATUS_ACTIVE, Mc.NULL_STRING);
         ////Vector rates = pLogic.getLenderProductRates(srk, logger, product.getMtgProdId(), ingestionDate);
         //--DisableProductRateTicket#570--10Aug2004--end--//
         Vector rates = pLogic.getLenderProductRates(
        		 srk, product.getMtgProdId(), ingestionDate, Mc.NULL_STRING);
		 /***************MCM Impl team changes ends - XS_2.29*******************/
         if(rates.isEmpty())
          throw new Exception("No valid rates found.");

         return (RateInfo)rates.get(0);

      }
      catch(Exception e)
      {
         String msg = e.getMessage();

         if(msg == null) msg = "Unknown Error";

         srk.getSysLogger().error("INGESTION: " + msg + " while running PricingLogic.");

         return null;
      }


   }

   /**
      brokerDiscount = deal.postedRate(calculated from PricingLogic) - Ingested.netInterestRate

      If brokerDiscount < 0
        Then Discount =  0
         return.
      else
      {
          if (brokerDiscount < RateInfo.maxDiscount )
           discount = brokerDiscount
          else
           discount = RateInfo.maxDiscount

      }
    */
   private void determineAndSetDiscount(RateInfo theRate)throws Exception
   {
      String lDetermineDiscount = PropertiesCache.getInstance().getProperty(
              srk.getExpressState().getDealInstitutionId(),
              "com.basis100.ingestion.determine.discount", "N");

      //--CR_Ticket#478_AutoDiscount_process--05Aug2004--start--//
      //1.  Default value is N, not to calculate
      if(lDetermineDiscount.equalsIgnoreCase("N"))
      {
        logger.trace("INGESTION: LENDERHANDLER: determine and set discount has been set to N");
        return;
      }

     if(theRate == null)
     {
        throw new Exception("Cannot determine discount - null rate recieved - no discount set");
     }

     double rateValue = theRate.getPostedRate();
     ////double discount = rateValue - inboundRate;
     double maxdisc = theRate.getMaxDiscount();
     double premium = DOUBLE_ZERO;
     double discount = DOUBLE_ZERO;
     double delta = inboundRate - rateValue;

     boolean isMaxDiscountNeg = getMaxDiscountSign(theRate);

     // 2. Some customers want to calculate discount (Auto Discount process)
     // during the ingestion of a deal. Logic of such calculation is based on:
     // dign of maxDiscount:
     // I. if maxDiscount < 0 premium need to be added to a prime type rate
     // II. if maxDiscount >= 0 logic is driven by property with the following values:
     //         M = give maximum discount all the time.
     //         B = give discount requested by Source of Business
     //             (netInterestRate as an inbound tag in xml file) up to
     //             the maximum discount amount found on the product.
     //         Z = discount set to zero.
	 //         U = discount, premium and buyDownRate set to zero       //#DG722
     // For detailed info see the "Auto Discount Process for ingested deals" document.

//Orig logic
/**
     if (discount < 0) {
       discount = 0;
     }
     else if (discount > maxdisc) {
       discount = maxdisc;
     }
**/

     if(isMaxDiscountNeg)
     {
       logger.trace("INGESTION: LENDERHANDLER: determine Auto Discount process (in negative mode)");

       premium = maxdisc * ( -1.0 );
       discount = DOUBLE_ZERO;

     }
     else if (!isMaxDiscountNeg)
     {
       if (lDetermineDiscount.equalsIgnoreCase("M"))
       {
         premium = DOUBLE_ZERO;
         discount = maxdisc;
       }
       else if (lDetermineDiscount.equalsIgnoreCase("B")) {
         logger.trace("INGESTION: LENDERHANDLER: determine Auto Discount process in B mode");

         // round them first.
         delta = Math.round (delta * 100.00) / 100.00;

         if (delta < maxdisc) {
           premium = DOUBLE_ZERO;
           discount = delta;
         }
         else if(delta > maxdisc) {
           premium = DOUBLE_ZERO;
           discount = maxdisc;
         }
         else if (delta <= 0.0) {
           discount = DOUBLE_ZERO;
           premium = DOUBLE_ZERO;
         }
       }
       else if (lDetermineDiscount.equalsIgnoreCase("Z")) {
         logger.trace("INGESTION: LENDERHANDLER: determine Auto Discount process in Z mode");

         // Virtually the B with delta <= 0 case, but should be explicitely determined.
         discount = DOUBLE_ZERO;
         premium = DOUBLE_ZERO;
       }
       else if (lDetermineDiscount.equals("U")) {       //#DG722

         // like 'Z' but also set buyDownRate to zero
         discount = 0.0;
         premium = 0.0;
         deal.setBuydownRate(0);
       }
     }

     logger.trace("InboundRate: " + inboundRate + ", RateVal: " + rateValue + ", Delta: " + delta +
                  ", Premium: " + premium + ", Discount: " + discount);

     deal.setPremium(premium);
     deal.setDiscount(discount);
   }

   private boolean getMaxDiscountSign(RateInfo ri)
   {
     boolean ret = false; //default is positive maxDiscount.

     double maxdisc = ri.getMaxDiscount();

     if (maxdisc < 0.0)
       ret = true;
     else if (maxdisc >= 0.0)
       ret = false;

     return ret;
   }
   //--CR_Ticket#478_AutoDiscount_process-end--//

   /**
    * <p>
    * Description: This method does Price rate inventory and mtgprod check for Deal
    * </p>
    * @version 1.0 XS_1.5  13-Aug-2008 Initial Version
    * @param current
    * 		Date
    * @param ingestRate
    * 		double
    * @param dealNode
    * 		Node
    * @return PassiveMessage
    * @throws Exception
    *             exception that may occurs if the entities do not exist.
    */
   public PassiveMessage handleMtgProdPRICheckforDeal(Date current, double ingestRate, Node dealNode) throws Exception {
     MtgProd mtg = null;
     ingestionDate = current;
     inboundRate = ingestRate;
     
     PassiveMessage pm = new PassiveMessage();
     boolean mtgProdExistsInDom = false;
     boolean prodRateExistsInDom  = false;
     
     String ssmb = deal.getSourceSystemMailBoxNBR();
     int srcSystem = deal.getSystemTypeId();

     LenderProfile lp = determineLender(srcSystem, ssmb);

     if(lp != null)
     {
       deal.setLenderProfileId(lp.getLenderProfileId());
     }
     else
     {
       pm.addMsg("Unable to determine LenderProfile for: " + deal.getDealId(),pm.NONCRITICAL);
       return pm;
     }
     
     Node mtgProdNode = DomUtil.getFirstNamedDescendant(dealNode, "MtgProd");
     String mpBusinessValue = DomUtil.getChildValue(mtgProdNode, "mpBusinessId");
     if (mpBusinessValue!=null){
       mtg = new MtgProd(srk,null);
       try {
           mtg.findByMPBusinessId(mpBusinessValue);
           mtgProdExistsInDom = true;
       }catch (Exception e){
    	   logger.debug(e.getMessage());
           logger.debug("Deal/MtgProd/mpBusinessId: " + mpBusinessValue);
           
           mtg = mtg.findDefaultFor(lp);
           deal.setDenialReasonId(Mc.DENIAL_REASON_FAILURE_PRODUCT_DEAL);
           sendMailAlert(dealNode, REASON_FOR_FAILURE_PRODUCT, PRODUCT_CODE_OR_RATE_FAILURE_DEAL_LEVEL);
           pm.addMsg("Unable to find mpBusinessId in ingested deal for: " + deal.getDealId(), pm.CRITICAL);
           mtgProdExistsInDom = false;
       }
     }else{
         mtg = determineMtgProd(lp);
         if(mtg==null){
             mtg = mtg.findDefaultFor(lp);
         }
     }

     //4.2GR Nov 20, 2009:  -- start 
     if(mtg != null){
    	 deal.setMtgProdId(mtg.getMtgProdId());
         deal.setPaymentTermId(mtg.getPaymentTermId()); //4.2GR (CM/DSU) - FXP25925
         deal.setProductTypeId(mtg.getProductTypeId()); //4.2GR FXP27307, FXP24419
     }
     //4.2GR Nov 20, 2009 -- end
     
     if(mtgProdExistsInDom){
       Node priNode = DomUtil.getFirstNamedDescendant(dealNode, "PricingRateInventory");
       String effectiveDateValue = DomUtil.getChildValue(priNode, "indexEffectiveDate");
       if(effectiveDateValue!= null){
    	 try {
             //effectiveDateValue = effectiveDateValue.replace('T',' ');//deleted : FXP26840, 4.2GR, Oct 26 09
             PricingRateInventory pri = new PricingRateInventory(srk);
             pri.findByPricingProfileAndEffectiveDate(mtg.getPricingProfileId(), effectiveDateValue);
             deal.setPricingProfileId(pri.getPricingRateInventoryID());
             deal.setPostedRate(pri.getInternalRatePercentage());
             prodRateExistsInDom = true;
         }catch (Exception e){
             logger.debug(e.getMessage());
             logger.debug("Deal/PricingRateInventory/indexEffectiveDate: " + effectiveDateValue);
             prodRateExistsInDom = false;
        	 deal.setDenialReasonId(Mc.DENIAL_REASON_FAILURE_RATE_DEAL);
             //mail logic
        	 sendMailAlert(dealNode, REASON_FOR_FAILURE_RATE, PRODUCT_CODE_OR_RATE_FAILURE_DEAL_LEVEL);
             pm.addMsg("Unable to match deal rate inventory in ingested deal for: " + deal.getDealId(), pm.CRITICAL);
         }
       }else{
    	 deal.setDenialReasonId(Mc.DENIAL_REASON_FAILURE_RATE_DEAL);
         //mail logic
    	 sendMailAlert(dealNode, REASON_FOR_FAILURE_RATE, PRODUCT_CODE_OR_RATE_FAILURE_DEAL_LEVEL);
         pm.addMsg("Unable to match deal rate inventory in ingested deal for: " + deal.getDealId(), pm.CRITICAL);
         return pm;
       }
     }
     
     // associate default rate if mortgage product
     // or product rate does not exist in database
     if (!mtgProdExistsInDom || !prodRateExistsInDom)
     {
     RateInfo ri = determineRate(mtg);
     //Note: the deal.pricingProfileId is actually the pricingRateInventoryId for this deal

     if(ri != null)
     {
       deal.setPricingProfileId(ri.getRateId());
       deal.setPostedRate(ri.getPostedRate());
     }
     else if(ri == null)
     {
       pm.addMsg("Unable to determine rates for: " + deal.getDealId(), pm.NONCRITICAL);
       return pm;
     }

     // #DG722 just let the mossy property control the behaviour
     determineAndSetDiscount(ri);
     }
     //    ***** Change by NBC Impl. Team - Version 1.1 - End*****//
     return pm;

   }


   /**
    * <p>
    * Description: This method does Price rate inventory and mtgprod check for components
    * </p>
    * @version 1.0 XS_1.5  13-Aug-2008 Initial Version
    * @param associationList
    *            List of EntityNodeAssociation;
    * @return PassiveMessage
    * @throws RemoteException, FinderException
    *             exception that may occurs if the entities do not exist.
    * Bug Fix FXP23902: Removing all the return statments and adding returns statement at the end,so that it will remove all the componet which are having wrong mpbusiness id            
    */
   public PassiveMessage handleMtgProdPRICheckforComp(List associationList)throws RemoteException, FinderException {
	 MtgProd mtg = null;
	 boolean mtgProdExistsInDom = false;
	 boolean mtgProdComponetTypeMatch = false;
	 PassiveMessage pm = new PassiveMessage();
	 ListIterator liAssociation = associationList.listIterator();
	 EntityNodeAssociation current = null;
	 DealEntity currentEntity = null;
	 while(liAssociation.hasNext()){
       current = (EntityNodeAssociation)liAssociation.next();
       if("Component".equals(current.getNode().getNodeName())){
    	 currentEntity = current.getEntity();
       
    	 Component currentComponent = (Component)currentEntity;
       Node mtgProdNode = DomUtil.getFirstNamedDescendant(current.getNode(), "MtgProd");
         String mpBusinessValue = DomUtil.getChildValue(mtgProdNode, "mpBusinessId");
         if (mpBusinessValue!=null){
           mtg = new MtgProd(srk,null);
           try {
               mtg.findByMPBusinessId(mpBusinessValue);
               mtgProdExistsInDom = true;
               mtgProdComponetTypeMatch = (mtg.getComponentTypeId()==currentComponent.getComponentTypeId());
           }catch (Exception e){
               logger.debug(e.getMessage());
               logger.debug("Deal/MtgProd/mpBusinessId: " + mpBusinessValue);
               mtg = null;
               mtgProdExistsInDom = false;
           }
         }else {
           deal.setDenialReasonId(Mc.DENIAL_REASON_FAILURE_PRODUCT_COMPONENT);
           currentComponent.ejbRemove(false);
           //mail logic
           sendMailAlert(current.getNode(), REASON_FOR_FAILURE_PRODUCT, " ");
           pm.addMsg("Unable to find mpBusinessId in ingested deal for the given component: " + currentComponent.getComponentId(), pm.CRITICAL);
           //return pm;
         }

         if(mtgProdExistsInDom && mtgProdComponetTypeMatch){
           //artf767017
           currentComponent.setMtgProdId(mtg.getMtgProdId());
           currentComponent.setRepaymentTypeId(mtg.getRepaymentTypeId());
           //Artifact artf783817 -MCM Impl Team Starts -19-Sep-2008 - Added ebjStore()
           currentComponent.ejbStore();
           //Artifact artf783817 -MCM Impl Team Ends -19-Sep-2008 - Added ebjStore()
           Node priNode = DomUtil.getFirstNamedDescendant(current.getNode(), "PricingRateInventory");
           String effectiveDateValue = DomUtil.getChildValue(priNode, "indexEffectiveDate");
           if(effectiveDateValue!= null){
        	 try
             {
                 //effectiveDateValue = effectiveDateValue.replace('T',' ');//deleted : FXP26840, 4.2GR, Oct 26 09
                 PricingRateInventory pri = new PricingRateInventory(srk);
                 pri.findByPricingProfileAndEffectiveDate(mtg.getPricingProfileId(), effectiveDateValue);
                 currentComponent.setPricingRateInventoryId(pri.getPricingRateInventoryID());
                 currentComponent.setPostedRate(pri.getInternalRatePercentage());
                 currentComponent.ejbStore();
             }catch (Exception e){
                 logger.debug(e.getMessage());
                 logger.debug("Deal/PricingRateInventory/indexEffectiveDate: " + effectiveDateValue);
            	 deal.setDenialReasonId(Mc.DENIAL_REASON_FAILURE_RATE_COMPONENT);
            	 currentComponent.ejbRemove(false);
                 //mail logic
            	 sendMailAlert(current.getNode(), REASON_FOR_FAILURE_RATE, " ");
                 pm.addMsg("Unable to match component rate inventory in ingested deal for: " + currentComponent.getComponentId(), pm.CRITICAL);
                 //return pm;
             }
           }else{
        	 deal.setDenialReasonId(Mc.DENIAL_REASON_FAILURE_RATE_COMPONENT);
        	 currentComponent.ejbRemove(false);
             //mail logic
        	 sendMailAlert(current.getNode(), REASON_FOR_FAILURE_RATE, " ");
             pm.addMsg("Unable to match component rate inventory in ingested deal for: " + currentComponent.getComponentId(), pm.CRITICAL);
             //return pm;
           }
         }else {
           deal.setDenialReasonId(Mc.DENIAL_REASON_FAILURE_PRODUCT_COMPONENT);
           currentComponent.ejbRemove(false);
           //mail logic
           sendMailAlert(current.getNode(), REASON_FOR_FAILURE_PRODUCT, " ");
           pm.addMsg("Unable to match mpBusinessId in ingested deal for the given component: " + currentComponent.getComponentId(), pm.CRITICAL);
           //return pm;
         }
       }
     }
	 return pm;
   }
   /***************************************************************************
    **************MCM Impl team changes -  12-Aug-2008 - Starts - XS_1.6 /
    **************************************************************************/
   /**
    * <p>
    * Description : This method sends an email to customer service with the
    * reasons of failure of product code/Rate match failure for deal/Component
    * </p>
    * @version 1.0 XS_1.6 13-Aug-2008 Initial version
    * @param node -
    *            Node of a deal/Component
    * @param reasonforFailure -
    *            Reason for Failure (i.e either
    * @param level -
    *            level can be either Deal / Component
    */
   private void sendMailAlert(Node node, String reasonforFailure, String level)

   {
       try
       {
           boolean prodCodeFailureFlag = false;
           boolean rateFailureFlag = false;
           String envirId = BXResources.getPickListDescription(deal
                   .getInstitutionProfileId(), PKL_LENDER_PROFILE, deal
                   .getLenderProfileId(), Locale.ENGLISH);

           if (REASON_FOR_FAILURE_PRODUCT.equalsIgnoreCase(reasonforFailure))
           {
               // if the product match falis then set product code faliure flag
               // to true
               prodCodeFailureFlag = true;

           }
           else
               if (REASON_FOR_FAILURE_RATE.equalsIgnoreCase(reasonforFailure))
               {
                   // if the rate match falis then set rate faliure flag to
                   // true
                   rateFailureFlag = true;
               }
               else
                   if (REASON_FOR_FAILURE_PRODUCT_RATE
                           .equalsIgnoreCase(reasonforFailure))
                   {
                       // if the both product code/Rate match falis then set
                       // product code, rate faliure flags to true
                       prodCodeFailureFlag = true;
                       rateFailureFlag = true;
                   }
           //Gets the Fromatted Mail Message
           StringBuffer mailFormat = frameMailMessage(rateFailureFlag,
                   prodCodeFailureFlag, node, level);
           //get the Support Mail id
           String defEmail = PropertiesCache.getInstance().getProperty(
                   srk.getExpressState().getDealInstitutionId(),
                   COM_BASIS100_SYSMAIL_TO, FXSUPPORT_EMAIL);
           //Send a mail Request
           DocumentRequest
           .requestAlertEmail(srk, deal, defEmail, "Product code/rate match failure",
                   mailFormat.toString());
       }
       catch (Exception exp)
       {
           logger.info("ALH@sendMailAlert Caught exception in catch Block..."
                   + exp);
       }
   }
    /**
     * <p>
     * Description : This method returns the formatted message with the reasons
     * of failure of product code/Rate match failure for deal/Component
     * </p>
     * @version 1.0 XS_1.6 13-Aug-2008 Initial version
     * @param rateFailureFlag -
     *            Rate Code Failure Flag
     * @param prodCodeFailureFlag -
     *            product Code Failure Flag
     * @param node -
     *            Node of a deal/Component
     * @param level -
     *            level can be either Deal / Component
     */
    private StringBuffer frameMailMessage(boolean rateFailureFlag,
            boolean prodCodeFailureFlag, Node node, String level){
        StringBuffer mailFormat = new StringBuffer();
        try
        {
            final String NL = System.getProperty("line.separator");

            String componentTypeDesc = BXResources.getPickListDescription(deal
                    .getInstitutionProfileId(), PKL_COMPONENTTYPE, DomUtil
                    .getChildValue(node, "componentTypeId"), Locale.ENGLISH);
            String envirId = BXResources.getPickListDescription(deal
                    .getInstitutionProfileId(), PKL_LENDER_PROFILE, deal
                    .getLenderProfileId(), Locale.ENGLISH);
            // prepare and send alert message
            Contact contact = new Contact(srk);
            contact = deal.getSourceOfBusinessProfile().getContact();
            String sourceOfBus = contact.getContactFirstName() + " "
                    + contact.getContactLastName() + ","
                    + contact.getContactPhoneNumber();
            
            mailFormat.append(PRODUCT_CODE_OR_RATE_MATCH_FAILURE_MAIL_FORMAT_MSG1 + NL + NL);
            mailFormat.append(PRODUCT_CODE_OR_RATE_MATCH_FAILURE_MAIL_FORMAT_MSG2 + NL + NL);
            mailFormat.append("Deal Id             = \t\t" + deal.getDealId() + NL);
            mailFormat.append("Lender              = \t\t" + envirId + NL);
            mailFormat.append("Source Application  = \t\t" + deal.getSourceApplicationId() + NL);
            mailFormat.append("Source of business  = \t\t" + sourceOfBus + NL);
            mailFormat.append(NL);
            
            Node mtgProdNode = DomUtil.getFirstNamedDescendant(node, "MtgProd");
            String mpBusinessId = DomUtil.getChildValue(mtgProdNode, "mpBusinessId");
            
            if (prodCodeFailureFlag){

                mailFormat.append("Product Code ");
                mailFormat.append(mpBusinessId);
                mailFormat.append(" specified at the");
                if (PRODUCT_CODE_OR_RATE_FAILURE_DEAL_LEVEL
                        .equalsIgnoreCase(level)){
                    mailFormat.append(" deal ");
                }else{
                    mailFormat.append(componentTypeDesc);
                }
                mailFormat.append("level does not exist in express." + NL + NL);
            }
            if (rateFailureFlag){
                //FXP23364, MCM Nov 10 2008, changed massage.
                Node pricingRateInv = DomUtil.getFirstNamedDescendant(node,
                        "PricingRateInventory");
                String indexEffectiveDate = DomUtil.getChildValue(pricingRateInv,
                "indexEffectiveDate");
//                indexEffectiveDate = indexEffectiveDate.replace('T', ' ');
                
                String dealOrComp = (PRODUCT_CODE_OR_RATE_FAILURE_DEAL_LEVEL
                        .equalsIgnoreCase(level)) ? "deal" : componentTypeDesc;

                String rateFaleMsg = String.format(
                        PRODUCT_CODE_OR_RATE_MATCH_FAILURE_MAIL_FORMAT_MSG5, 
                        mpBusinessId, indexEffectiveDate, dealOrComp);
                mailFormat.append(rateFaleMsg);
                mailFormat.append(NL);
                mailFormat.append(NL);
            }
            
            mailFormat
                    .append(PRODUCT_CODE_OR_RATE_MATCH_FAILURE_MAIL_FORMAT_MSG3
                            + NL + NL);
            mailFormat
                    .append(PRODUCT_CODE_OR_RATE_MATCH_FAILURE_MAIL_FORMAT_MSG4
                            + NL + NL);
            if (mailFormat.length() > 4000) // truncate to size of column below
                mailFormat = new StringBuffer(mailFormat.substring(0, 4000)
                        .toString().trim());

        }catch (Exception exp){
            logger
                    .info("ALH@frameMailMessage Caught exception in catch Block..."
                            + exp);
        }
        return mailFormat;
    }
   /***************************************************************************
     * *************MCM Impl team changes - 12-Aug-2008 - Ends - XS_1.6 /
     **************************************************************************/
}


