package com.basis100.deal.ingest.constraints;

import java.util.List;
import com.basis100.deal.xml.EntityNodeAssociation;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.ingest.RequiredElementException;
import com.basis100.entity.FinderException;
import com.basis100.resources.SessionResourceKit;
import com.basis100.xml.DomUtil;
import MosSystem.Mc;



public class BorrowerAddressRequired implements RequiredConstraint
{
  public void execute(SessionResourceKit srk, EntityNodeAssociation ass )
  throws RequiredElementException, Exception
  {
    DealEntity de = ass.getEntity();

    if(de == null)
      return ; // called on wrong type

    if(de.getClassId() == ClassId.BORROWER)
    {
      List l = DomUtil.getNamedChildren(ass.getNode(), "BorrowerAddress");

      if(l.isEmpty())
         throw new RequiredElementException("Borrower Address has no current address info");
    }
    else if(de.getClassId() == ClassId.BORROWERADDRESS)
    {

      BorrowerAddress ba = (BorrowerAddress)de;

      if(ba.getBorrowerAddressTypeId() == Mc.BT_ADDR_TYPE_CURRENT)
      {

        if(!ass.isSetField("monthsAtAddress"))
          throw new RequiredElementException("Borrower Address has missing address info for: monthsAtAddress" );

        Addr addr = null;

        try
        {
          addr = ba.getAddr();
        }
        catch(FinderException fe)
        {
           throw new RequiredElementException("Borrower Address has missing current address info");
        }
        catch(Exception e)
        {
          throw new Exception("Remote Exception while obtaining Addr from: "
                                        + this.getClass().getName());

        }

        List child = (List)ass.getChildren();

        if(!child.isEmpty())
        {
          EntityNodeAssociation ass2 = (EntityNodeAssociation)child.get(0);

          if(ass2 != null)
          {
            String[] fields = {"provinceId", "addressLine1", "city", "postalFSA", "postalLDU"};

            for(int i = 0; i < 5; i++)
            {
              if(!ass2.isSetField(fields[i]))
               throw new RequiredElementException("Borrower Address has missing current address info: " + fields[i]);
            }

          }

        }

      }

    }

 }
}
