package com.basis100.deal.ingest.application;

/**
* 15/Jun/2005 DVG #DG228 #1548 Xceed - ingestion constraint
* 10/Feb/2005 DVG #DG134 scr#941  Unable to ingest deals to BMOQA, minor bug
* 12/Jan/2005 DVG #DG126 BMO-CCAPS workflow notification
* 06/Jan/2005 DVG #DG124 cr#136 INSUREONLYAPPLICANT ingestion, minor correction
* 16/Sep/2004 DVG #DG100 SCR#622 allow control over resetting of PrePaymentOptionsId,
*      for AGF / Sheldon - must not happen for AGF
* 13/Sep/2004 DVG #DG80 address scrub validation
* 17/AUG/2004-DVG #DG40 SCR#522 change notification type
* 16/AUG/2004-DVG #DG30 show stopper if defaults file is missing
* 05/Nov/2008 FXP23298, Synchronizing db and entity after calc run.
 */


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Formattable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.StringTokenizer;
import java.text.SimpleDateFormat;
import java.text.ParseException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.duplicate.DealLockedException;
import com.basis100.deal.duplicate.DupeCheckProcessor;
import com.basis100.deal.duplicate.SearchResult;
import com.basis100.deal.duplicate.SourceMessageBuilder;
import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentSummary;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DealNotes;
import com.basis100.deal.entity.EmploymentHistory;
import com.basis100.deal.entity.EntityBuilder;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.MbmlRedirect;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.entity.SysProperty;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.ingest.DirectoryMonitor;
import com.basis100.deal.ingest.FileIngestionException;
import com.basis100.deal.ingest.IngestionHandler;
import com.basis100.deal.ingest.IngestionResult;
import com.basis100.deal.ingest.SourceMonitor;
import com.basis100.deal.ingest.application.ApplicationDefaults;
import com.basis100.deal.ingest.application.ApplicationLenderHandler;
import com.basis100.deal.ingest.application.ApplicationSourceHandler;
import com.basis100.deal.ingest.application.ApplicationValidator;
import com.basis100.deal.ingest.application.SourceDeterminationException;
import com.basis100.deal.ingest.application.SpecialProcessingHandler;
import com.basis100.deal.miprocess.MIProcessHandler;
import com.basis100.deal.pk.ComponentSummaryPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.deal.validation.addressScrub.AddScrubConstants;
import com.basis100.deal.validation.addressScrub.AddScrubExecutor;
import com.basis100.deal.xml.EntityNodeAssociation;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.util.file.IOUtil;
import com.basis100.util.message.ClientMessage;
import com.basis100.workflow.notification.WorkflowNotifier;
import com.basis100.xml.DomUtil;
import com.basis100.xml.XmlToolKit;
import com.basis100.xml.XmlWriter;
import com.filogix.util.Xc;

/**
* <p>Title: FXpressJ2EE</p>
* <p>Description: FXpress NetDynamics project converted to the J2EE realm with the JATO as an iPlanet Application Framework.</p>
* <p>Copyright: Copyright (c) 2002</p>
* <p>Company: Filogix</p>
* @author not attributable
* @version 1.0
 *  <pre>
 *  The Application class manages the mapping of Nodes to BoundElements. The ApplicationManager
 *  asks the BoundElementFactory whether the node a leaf node. If the node is not a
 *  leaf node the BindManager provides the factory with the node to obtain a bound element.
 *  The details of the bind are contained within the factory and the elements.
 *  In addition the BindManager provides a linear list nodes and methods for
 *  obtaining a bound element or it's fields given a document node.
 *  </pre>
 * @version 1.1  <br>
 * Date: 06/29/2006 <br>
 * Author: NBC/PP Implementation Team<br>
 * Change:  <br>
 *	Deal compare - Service Branch Update<br>
 *	- modified storeDocument( ) to call processServiceBranch in SpecialProcessing<br>  
 *
 *@version 1.6  <br>
 * Date: 08/10/2006 <br>
 * Author: NBC/PP Implementation Team<br>
 * Change:  <br>
 *	Added 2 constants BorrowerIdentification in ingestableTables array, ingestableTablesForDJClient array<br>
 *@version 1.7  <br>
 * Date: 08/Aug/2008 <br>
 * Author: MCM Implementation Team<br>
 * Story :XS_1.1 <br>
 * Change:  <br>
 * Added a constant QualifyDetail in ingestableTables array, ingestableTablesForDJClient array<br>
 *@version 1.8 <br>
 * Date: 11/Aug/2008 <br>
 * Author: MCM Implementation Team<br>
 * Story: XS_1.2 <br>
 * Change: <br>
 * Added 5 constants for component and related tables in ingestableTables array, ingestableTablesForDJClient array<br>
 * Added a method storeSpecial(..) to create ComponentSummary for component Enabled Deal<br>
 * Added call of storeSpecial(..) in storeDocument(..) <br>  
 *@version 1.9 <br>
 * Date: 13/Aug/2008 <br>
 * Author: MCM Implementation Team<br>
 * Story: XS_1.5 <br>
 * Change: <br>
 * Added call for ApplicationLenderHandler.handleMtgProdPRICheckforDeal(..) and <br>
 * ApplicationLenderHandler.handleMtgProdPRICheckforComp(..)  <br>
 * @version 1.10 <br>
 * Date: 23/Aug/2008 <br>
 * Author: MCM Implementation Team<br>
 * Story: artf767017 <br>
 * Change: <br>
 * Changed the setter for denial reason.
 */ 
 
public class ApplicationHandler implements IngestionHandler, Xc {
  public static boolean debug = true;
  public SysLogger logger;
  protected SessionResourceKit srk;

  protected List associationList;
  protected List ingestedNodeList;
  protected List passList;
  protected Document document;
  // institution Abberibation for ML
  protected String abbre;

  public static String SOURCE_FIRM = "SourceFirmProfile";
  public static String SOURCE_OF_BUSINESS = "SourceOfBusinessProfile";
  public static String MAIL_BOX = "SourceOfBusinessMailBox";
  public static String MAIL_BOX_NBR = "sourceSystemMailBoxNBR";
  public static String SYSTEM_TYPE = "systemType";
  public static String DEAL = "Deal";

 // 3.2patch LTV project rename "com.basis100.ingestion.Reset.PrePaymentOptionsId.LTVgt75" to "com.filogix.ingestion.Rst.PrePay.LTVgtLimit" 
  //DG#100 "Reset Pre payment option id when ltv > 75" flag

  // #DG126 optimize
  // changed non static for ML
  protected String islNotificationType = null;
  protected String dupeCheckClassName = null;
  protected boolean dupeCheckEnabled = true;
  //#DG126 end
  //changed to non static
  protected boolean ibRstPrePayLTVgtLimit = true;  
 
  private Date ingestionDate;
//**************MCM Impl team changes - 11-Aug-2008 - XS_1.2 -1.8 Starts *****//
  //**************MCM Impl team changes - 08-Aug-2008 - XS_1.1 -1.7 Starts *****// 
  public static String[] ingestableTables =
    { "Deal", "Borrower", "Property", "DownPaymentSource", "DealNotes",
      "DealFee", "EscrowPayment", "Income", "Liability",
      "Asset", "CreditReference", "EmploymentHistory",
      "BorrowerAddress" , "PropertyExpense",
      "CreditBureauReport" ,"BorrowerIdentification","QualifyDetail",
      "ComponentLOC", "Component", "ComponentMortgage", "ComponentCreditCard",
      "ComponentOverdraft", "ComponentLoan" };
  
	//--DJ_CR136--start--//
  public static String[] ingestableTablesDJ =
     { "Deal", "Borrower", "InsureOnlyApplicant", "Property", "DownPaymentSource", "DealNotes",
       "DealFee", "EscrowPayment", "Income", "Liability",
       "Asset", "CreditReference", "EmploymentHistory",
       "BorrowerAddress" , "PropertyExpense",
       "CreditBureauReport" ,"BorrowerIdentification" , "QualifyDetail",
       "ComponentLOC", "Component", "ComponentMortgage", "ComponentCreditCard",
       "ComponentOverdraft", "ComponentLoan" };
   //--DJ_CR136--end--//
//**************MCM Impl team changes - 08-Aug-2008 - XS_1.1 -1.7 Ends ******//
//**************MCM Impl team changes - 08-Aug-2008 - XS_1.2 -1.8 Ends *****//
  private Map<String, ApplicationDefaults> defaultsFileCache = new HashMap(); //#DG600
  /**
   *   Constructs an initialized application handler.
   *
   * @throws FileIngestionException
   */
  public ApplicationHandler() throws FileIngestionException
  {
	//#DG600 logger = ResourceManager.getSysLogger("0");
	logger = new SysLogger(this, "AplHandl");
  }
  public File preProcess(File file)throws Exception
  {
    return file;
  }

 /**
  *
  * @param dom Document -the document object model for the ingested application file
  * @param srk SessionResourceKit - session resource kit
  * @param monitor SourceMonitor
  * @throws Exception
  * @return IngestionResult the target Deal. That is, the Deal used to notify workflow.
  * 
  * @version 1.1  <br>
  * Date: 06/30/2006 <br>
  * Author: NBC/PP Implementation Team <br>
  * Change:  <br>
  *		- Call processServiceBranch in SpecialProcessing<br>
  * @version 1.2 <br>
  * Date: 08/11/2008 <br>
  * Author: MCM Implementation Team <br>
  * Change: <br>
  * 	- Call storeSpecial method to store componentSummary 
  * @version 1.3 <br>
  * Date: 08/13/2008 <br>
  * Author: MCM Implementation Team <br>
  * Change: <br>
  * 	- Call ApplicationLenderHandler.handleMtgProdPRICheckforDeal(..)
  * 	and ApplicationLenderHandler.handleMtgProdPRICheckforComp(..)
  * @version 1.4 
  * Author: MCM Impl Team
  * Bug Fix : FXP23902 commenting if(!pm.getCritical() 
  */
 public IngestionResult storeDocument(org.w3c.dom.Document dom, SessionResourceKit srk, SourceMonitor monitor) throws Exception
  {
    reset();

    this.srk = srk;
    CalcMonitor dcm = CalcMonitor.getMonitor(srk);
    //ingestedNodeList = new ArrayList();
    document = dom;
    String msg = null;
    IngestionResult result = new IngestionResult();
    PropertiesCache pc = PropertiesCache.getInstance();

    islNotificationType = pc.getProperty(srk.getExpressState().getDealInstitutionId(), COM_FILOGIX_INGESTION_WORKFLOW_NOTIFICATION_TYPE, "DEFAULT");

    // dupecheck processor has to be different for each institution.
    dupeCheckClassName = pc.getProperty(srk.getExpressState().getDealInstitutionId(),
                                        COM_BASIS100_DUPECHECK_PROCESSOR_NAME,
                                        DupeCheckProcessor.class.getName());
    dupeCheckEnabled = pc.getProperty(srk.getExpressState().getDealInstitutionId(),
                                      COM_BASIS100_INGESTION_DUPECHECK_ENABLED,
                                      "N").trim().equalsIgnoreCase("Y");

    //This method is called from storeDocument method.InstituionProfileId has been set in srk.
    ibRstPrePayLTVgtLimit = pc.getProperty(srk.getExpressState().getDealInstitutionId(),Xc.ING_RESET_PREPAY_LTVGTLIMIT,"Y").equalsIgnoreCase("Y");

    if( islNotificationType.equalsIgnoreCase("cervus") ){
      result.setCervusJob(true);
      result.setNotificationType( Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_WUP );
    }

    //#DG600 rewritten, code cleanup
    //start get defaults process
    ApplicationDefaults defaults = null;
    //get cached rules if any
    try {
	defaults = getDefaults(monitor);
    }
    catch (Exception exc) {
	msg = "INGESTION: ApplicationHandler: Error processing defaults file="
		+ monitor.getConstraints();
	logger.error(msg+':'+exc);
	System.err.println(msg);
	//result.addMessage("Error Handling Defaults. See Log", PassiveMessage.CRITICAL);
	//result.isApplicationError(true);
	System.exit(1); // do not proceed !
    }
    //#DG600 end

    passList = new ArrayList();
    List pass = DomUtil.getNamedDescendants(dom, "Ingestion");

    if(pass != null && !pass.isEmpty())
    {

      Iterator pi = pass.iterator();

      while(pi.hasNext())
      {
        Element current = (Element)pi.next();

        String type = current.getAttribute("type");

        if(type.equalsIgnoreCase("return"))
         passList.add(current);

      }

    }


    //- create Entities
    List asslist = null;
    try
    {
      asslist = makeAssociations(new EntityNodeAssociation(dom,null,dom,null),dcm);
    }
    catch(Exception exc)
    {
      logger.error("INGESTION: ApplicationHandler: Error handling Document Object Model.");
      logger.error(exc);
      result.isInvalidInput(true);
      return result;
    }

    Deal deal =(Deal)getRootAssociation().getEntity();
    Element dealNode = (Element)getRootAssociation().getNode();

    if(deal == null)
    {
      String em = ">>>>INGESTION: Unable to obtain Deal node from object model";
      logger.error(em);
      result.isInvalidInput(true);
      return result;
    }
    deal.setCopyType("G");
    result.setResultEntity(deal.getPk());
    // FXLink:phaseII
    if( isFXLinkType(deal.getSystemTypeId()) ){
      result.setFilogixLink(true);
    }

    ListIterator li = associationList.listIterator();
    EntityNodeAssociation current = null;

    //- Store the Deal check constraints and defaults
    try
    {
      while(li.hasNext())
      {
        current = (EntityNodeAssociation)li.next();

        DealEntity entity = current.getEntity();

         //reads the default parameters and loads the entity with default
         //if no value has been recieved
        loadDefaults(current,defaults, getAbbre(document));

        entity.ejbStore();
      }

    }
    catch(Exception ste) {
      //#DG600 code cleaning
      String name = current.getName();
      if(name == null)
        name = "Unknown Entity. ";
      msg = "INGESTION: Failed to store Entity: "+name+" - Reason: " + ste;
      System.err.println(getClass().getName() + ':' + msg);
      logger.error(msg);
      result.isApplicationError(true);
      result.addMessage(msg);
      msg = StringUtil.stack2string(ste);
      System.err.println(getClass().getName() + ':' + msg);
      logger.error(msg);
      return result;
    }
//  **************MCM Impl team changes - 11-Aug-2008 - XS_1.2 -1.2 Starts *****//
    try
    {
      storeSpecial(deal, dcm);
    }
    catch(Exception exc)
    {
        exc.printStackTrace();
        logger.error(exc);
        result.isApplicationError(true);
        return result;
    }
//  **************MCM Impl team changes - 11-Aug-2008 - XS_1.2 -1.2 Ends *****//
      msg = "INGESTION: Stored uncommitted deal...";
      System.out.println(msg);
      logger.debug(msg);
    //- Find the source
    try
    {
      ApplicationSourceHandler sourceHandler = new ApplicationSourceHandler(srk);
      PassiveMessage spm = sourceHandler.handleSourceDetails(deal,dom);

      result.addAllMessages(spm);
    }
    catch(SourceDeterminationException sde)
    {
      logger.warning("INGESTION: Error matching profiles or systemType - Invalid Input."+ sde);
      String srcmsg = "The Source Firm associated with this Application" +
                      " Source of Business could not be determined from the supplied data.";

      result.addMessage(srcmsg);
      result.isInvalidSource(true);
    }

      //set the lender and mtg product and rate
    try
    {
      double inboundRate = 0d;

      if(dealNode != null)
      {
        Node rateNode = DomUtil.getFirstNamedChild(dealNode,"netInterestRate");

        String rateString = DomUtil.getNodeText(rateNode);

        inboundRate = TypeConverter.doubleTypeFrom(rateString,0d);
      }

      /***** FXP24050 fix start ******/
      srk.getExpressState().setDealIds(deal.getDealId(), deal.getInstitutionProfileId(), deal.getCopyId());
      /***** FXP24050 fix end   ******/
      
      ApplicationLenderHandler lenderHandler = new ApplicationLenderHandler(srk,deal);
  
  //    ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
      //PassiveMessage pm = lenderHandler.handleProcess(ingestionDate, inboundRate, document.getFirstChild());
      //    ***** Change by NBC Impl. Team - Version 1.1 - End*****//
      //    **************MCM Impl team changes - 13-Aug-2008 - XS_1.5 -1.3 Starts *****//
      PassiveMessage pm = lenderHandler.handleMtgProdPRICheckforDeal(ingestionDate, inboundRate, document.getFirstChild());
      pm.addAllMessages(pm);
      
      //MCM Impl Bugfix FXP23902 : Calling handleMtgProdPRICheckforComp even the handleMtgProdPRICheckforDeal() check fails so commenting if statements
      //if(!pm.getCritical()){
    	PassiveMessage pmComponentPRICheck = lenderHandler.handleMtgProdPRICheckforComp(associationList);
    	pm.addAllMessages(pmComponentPRICheck);
     // }
      //    **************MCM Impl team changes - 13-Aug-2008 - XS_1.5 -1.3 Ends *****//
      if(pm.getNumMessages() > 0)
      {
        logger.info("INGESTION: Application Handler : " + pm.getMsgs().get(0));
      }

    }
    catch(Exception exc)
    {
      logger.info("INGESTION: LenderHandler: " + exc);
      result.isApplicationError(true);
      return result;
    }

    //==========================================================================
    //- perform calculations
    //==========================================================================
    long calcStart = System.currentTimeMillis();
    try
    {

      if(dcm != null)
      {
        System.out.println("INGESTION: Performing first round calcs....");
        dcm.calc();
        deal = deal.findByPrimaryKey((DealPK)deal.getPk()); //FXP23298, MCM Nov 5 08, Synchronizing db and entity
        System.out.println("INGESTION: End first round calcs - time (millisec): " + (System.currentTimeMillis()-calcStart));
      }

    }
    catch(Exception exc)
    {
      msg =  "INGESTION: Calculations failed on DealIngestion: " + exc;
      System.err.println(getClass().getName() + ':' + msg);
      logger.error(msg);
      result.isApplicationError(true);
      return result;
    }

// FXP21309
// CalcMonitor shouldn't be reset for 2nd calc.
// need to keep inputs to calc because of calc order
// call 2nd calc
// deleted,,,,,result of discussion
//    System.out.println(getClass().getName() + ':'
//      		+ "INGESTION: Performing Second round calcs....");
//
//      dcm.calc();
//
//      System.out.println(getClass().getName() + ':'
//      		+ "INGESTION: End Second Second calcs - time (millisec): "
//      		+ (System.currentTimeMillis() - calcStart));
//      logger.trace("INGESTION: finished 2nd store and calc... ");

// clean up calcMonitor    
    dcm = CalcMonitor.getMonitor(srk);
    deal.setCalcMonitor(dcm);

    SpecialProcessingHandler specialProcessingHandler = new SpecialProcessingHandler(srk);
    try
    {
      setStatusAndType(result, deal, dealNode);
      deal.ejbStore();

      /*#DG228 not needed anymore, will stay set by the defaults file instead, see setStatusAndType above
      String lDefaultMIIndicator = PropertiesCache.getInstance().getProperty("com.basis100.ingestion.default.miindicatorid");
      if(lDefaultMIIndicator != null){
        deal.setMIIndicatorId(Integer.parseInt(lDefaultMIIndicator));
        deal.ejbStore();
      }*/
      // redirection for special processing
      specialProcessingHandler.processMIInsurerRequest(deal,dealNode);

      calcStart = System.currentTimeMillis();
      System.out.println(getClass().getName() + ':'
      		+ "INGESTION: Performing second round calcs....");

      dcm.calc();

      System.out.println(getClass().getName() + ':'
      		+ "INGESTION: End Second round calcs - time (millisec): "
      		+ (System.currentTimeMillis() - calcStart));
      logger.trace("INGESTION: finished last store and calc... ");
      deal = deal.findByPrimaryKey((DealPK)deal.getPk()); //FXP23298, MCM Nov 5 08, Synchronizing db and entity
    }
    catch(Exception exc)
    {
      logger.info("INGESTION: Application Handler : " + exc);
    }
    //==========================================================================
    // Business rules IC RULES
    //==========================================================================
    ApplicationValidator validator = new ApplicationValidator(srk);
    //PassiveMessage icMsg = validator.validate(deal,srk);
    PassiveMessage icMsg = validator.validate(deal,srk, determineLanguage(deal) );
    
    // introduce mossys.properties to disable addressScrub for CIBC - Jan 16, 2008 - Midori
    boolean useAddressScrub = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), "com.filogix.ingestion.addressscrub", "Y").equals("Y");

    if (useAddressScrub) {
    //#DG80 address scrub validation
    AddScrubExecutor loAdrScrEx = new AddScrubExecutor(srk);
    loAdrScrEx.validateAddresses(deal, icMsg, AddScrubConstants.ADDRESS_SOURCE_INGESTION);
    //#DG80 end
    }
    result.appendICRulesMessages(icMsg);
    //==========================================================================
    // end of business rules section
    //==========================================================================

    // =======================================================================
    // now call the special processing handler
    // =======================================================================
    specialProcessingHandler.processOriginationBranch(deal,dealNode);
    //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
    specialProcessingHandler.processServiceBranch( deal , dealNode );
    //  ***** Change by NBC Impl. Team - Version 1.1 - End*****//
    specialProcessingHandler.processPartyProfile(deal, dealNode);
    // =======================================================================
    // end of special processing handler
    // =======================================================================

     // perform dupcheck routine
    //boolean dupArchive = false;
   

    try
    {
      doDupeCheck(dom, srk, result, deal);
    }
    catch(Exception exc)
    {   //#DG126 rewritten
      msg = "INGESTION: DupCheck Failure: " + exc;
      logger.info(msg);
      System.err.println(msg);

      if (exc instanceof DealLockedException)
        result.setDealLocked(true);
      else
        result.isApplicationError(true);
      return result;
    }

    // Ticket #458
    if(deal.getSystemTypeId() > 50){
      deal.setSystemTypeId(Mc.SYSTEM_TYPE_EXPERT);
      deal.ejbStore();  //dvg
    }
    // zivko:marker: decisioning
    try
    {
      if(result.hasMessages())
      {
        checkValidationExcused(result, deal);
        result.isValidationFailure(true);
        appendDealNote(dom,result);
        appendICMessagesAsDealNote(dom,result);
      }
      if( result.hasICRulesMessages() )
      {
        result.setICRulesFailure(true);
        appendICMessagesAsDealNote(dom,result);
        createDealNotes(result,deal);
        deal.setStatusId(Mc.DEAL_DENIED);
        //version 1.10 artf767017
        int denailReasonId = deal.getDenialReasonId();
        logger.error("DenialReasonID "+ denailReasonId);
        if(!(denailReasonId == 100 || denailReasonId == 101 || denailReasonId == 102 || denailReasonId == 103)){
          deal.setDenialReasonId(Mc.DENIAL_REASON_IC_REJECT);
        }
        deal.ejbStore();
      }
      if(result.isDuplicateInput()){
        appendDupeCheckMessageToSource(dom, result);
      }
    }
    catch(Exception exc)
    {
      logger.error("INGESTION: Failure while performing validation."+ exc);
    }

    msg = "INGESTION: " + result.printCurrentStatus();
    logger.debug(msg);
    System.out.println(getClass().getName() + ':' + msg);

    return result;
  }

	//#DG600 extracted for simplicity
	/**
	 * load and cache the defaults file
	 * @param monitor holds the path of the defaults file
	 * @return the ApplicationDefaults object 
	 * @throws Exception
	 * @throws FileNotFoundException
	 */
	private ApplicationDefaults getDefaults(SourceMonitor monitor)
			throws Exception, FileNotFoundException {
		String defaultsPath = monitor.getConstraints();
		String msg;

		msg = "INGESTION: ApplicationHandler: retrieve Defaults File=>"+ defaultsPath;
		logger.info(msg);

		ApplicationDefaults defaults = defaultsFileCache.get(defaultsPath);
		if (defaults != null)
			return defaults;

		defaults = new ApplicationDefaults(srk, new File(defaultsPath));
		if (defaults == null) {
			msg = "INGESTION: ApplicationHandler: Defaults File Indicated Not found or Locked. =>"
					+ defaultsPath;
			throw new FileNotFoundException(msg);
		}
		defaultsFileCache.put(defaultsPath, defaults);

		msg = "INGESTION: ApplicationHandler: Defaults File created and cached.";
		logger.info(msg);
		System.out.println(msg);

		return defaults;
	}

  private void doDupeCheck(Document dom, SessionResourceKit srk,
                           IngestionResult result, Deal deal) throws Exception {
    //========================================================================
    // duplicates check
    //========================================================================
		// #DG598 rewritten - deprecated default dupe chek engine
    if (!dupeCheckEnabled) {
	logger.info(debug("INGESTION: DupCheck action will not be taken at all."));
        return;
      }
    if(! result.hasICRulesMessages()) {
        Class processorClass = Class.forName(dupeCheckClassName);
        DupeCheckProcessor dupeCheckProcessor = (DupeCheckProcessor)processorClass.newInstance();
        if (dupeCheckProcessor == null) {
	  String msg = "Cannot instantiate class "+ dupeCheckClassName;
          logger.error(msg);
          throw new FileIngestionException(msg);
        }
	logger.info("INGESTION: DupeCheck Processor started: Class = "+ dupeCheckClassName);
        
        dupeCheckProcessor.process(deal, srk, dom);
        result.setNotificationType(dupeCheckProcessor.getNotificationType());
        if(dupeCheckProcessor.getStoredDealPK() != null ){
          result.setResultEntity(dupeCheckProcessor.getStoredDealPK() );
        }
        if( dupeCheckProcessor.isAnyDealLocked() ){
          result.setLockedDealId( dupeCheckProcessor.getLockedDealId() );
        }
        if( dupeCheckProcessor.getMessagesForBroker().size() > 0 ){
          result.setMessagesForBrokerAppended(true);
          result.setDupeCheckMessagesForBroker( dupeCheckProcessor.getMessagesForBroker() );
        }else{
          result.setMessagesForBrokerAppended(false);
        }
        result.isDuplicateInput( dupeCheckProcessor.isDuplicate() );
        result.setCapLinkRequired( dupeCheckProcessor.isCapLinkRequired() );
        result.setCapLinkSensitive( dupeCheckProcessor.isCapLinkSensitive() );
        logger.info("INGESTION: DupeCheck Processor finished: Class=" + dupeCheckClassName);
        return;
      }

  }

  /**
   *   Allows the handler to take action based on the result error statuses and messages.
   *   This is where the handler may override the criticality of a given error status.
   *   @return false if the Deal should not be stored (rollback) else return true;
   *
   * @param result IngestionResult
   * @param input File
   * @param dom Document
   * @param mon SourceMonitor
   * @return int
   */
  public int processResult(IngestionResult result, File input, Document dom, SourceMonitor mon)
  {

    if(dom == null) return ROLLBACK;

    //FileOutputStream str = null;
    //File dir = null;

    DirectoryMonitor monitor = (DirectoryMonitor)mon;

    if(result.isCriticalFailure() )//Application or Input failure
    {
      logger.info("INGESTION: processResult: decision point : critFail");
      //String msg =  "INGESTION: Critical failure during ingestion.";

      logFailure(srk,result.getResultEntity(),input.getAbsolutePath());

      writeFailed(dom, input, mon);
      logger.info("INGESTION: processResult: decision point : critFailA");
      return ROLLBACK;  //dont ingest
    }
    else if(result.isNonCritical())
    {
      logger.info("INGESTION: processResult: decision point : nonCritic");
      if(result.isValidationFailure())
      {
        logger.info("INGESTION: processResult: decision point : nonCritic : validFail");
        String msg =  "INGESTION: 'Validation Stop' during ingestion. ";
        result.addMessage(msg);
        //result.

        if(result.isValidationExcused() && !result.isInvalidSource())
        {
          logger.info("INGESTION: processResult: decision point : nonCritic : vfA");
          msg = "INGESTION: Validation excused due to Application Source";
          result.addMessage(msg);

          msg = "INGESTION: Setting Deal Status to 'Incomplete'";
          result.addMessage(msg);
          logger.info("INGESTION: processResult: decision point : nonCritic : vfB");
          return STORE;
        }
        else if(!result.isValidationExcused())
        {
           logger.info("INGESTION: processResult: decision point : nonCritic : vfAE");
           writeOutbound(dom, input, mon);
           return ROLLBACK;
        }

      }//validation failure ends
      else if(result.isDuplicateInput())
      {
        logger.info("INGESTION: processResult: decision point : dupInp");
        // =====================================================================
        // change made based on request from Dave Krick on Aug 30, 2002.
        // the request goes as follows: if there is no dupecheck message created
        // we should not write the file back to the broker.
        // =====================================================================
	//#DG598 if(ibdupeCheckProcessorUsed){  if dupe check enabled it's always true
	//logger.info("INGESTION: processResult: decision point : C : 11");
          if(result.isMessagesForBrokerAppended()){
            logger.info("INGESTION: processResult: decision point : dupInp : ba");
            if(result.isFilogixLink()){
              logger.info("INGESTION: processResult: decision point : dupInp : baF");
              this.writeOutBoundFXLink(dom, input, mon);
            }else{
              logger.info("INGESTION: processResult: decision point : dupInp : baO");
              this.writeOutbound(dom, input, mon);
            }
          }
        //   if( result.isMessagesForBrokerAppended() ){
        //     this.writeOutbound(dom, input, mon);
        //   }
        logger.info("INGESTION: processResult: decision point : dupInp : baC");
        return STORE;
      }
      else if( result.isICRulesFailure() ){
        logger.info("INGESTION: processResult: decision point : ICFail");
        if(result.isFilogixLink()){
          logger.info("INGESTION: processResult: decision point : ICFailF");
          this.writeOutBoundFXLink(dom, input, mon);
        }else{
          logger.info("INGESTION: processResult: decision point : ICFailO");
          this.writeOutbound(dom, input, mon);
        }
        return STORE;
      }

    } //non-critical ends
    logger.info("INGESTION: processResult: decision point : store");
    return STORE;
  }

  private void writeOutBoundFXLink(Document dom, File input, SourceMonitor mon){
    FileOutputStream str = null;
    File dir = null;
    DirectoryMonitor monitor = (DirectoryMonitor)mon;

    try
    {
      XmlWriter writer = XmlToolKit.getInstance().getXmlWriter();

      dir = monitor.getFxOutboundDirectory();

      String filename = input.getName();

      str = IOUtil.constructSafeFOS(dir,filename,monitor.getFxOutExt());

      Element addto = (Element)DomUtil.getFirstNamedChild(dom,"Document");

      if(passList != null && addto != null)
      {
        Iterator plit = passList.iterator();
        List toAppend = null;

        while(plit.hasNext())
        {
          Element curr = (Element)plit.next();

          toAppend = DomUtil.getChildElements(curr);

          if(toAppend != null && !toAppend.isEmpty())
          {
             Iterator alit = toAppend.iterator();
             Element currentChild = null;

             while(alit.hasNext())
             {
               currentChild = (Element)alit.next();

               addto.appendChild(currentChild);
             }

          }

          addto.removeChild(curr);
        }
      }
      //zivko:marker:FXLink make documentType INGESTION REJECT
      if(addto != null){
        Element documentTypeNode = dom.createElement("documentType");
        DomUtil.appendString(documentTypeNode, "INGESTION REJECT" );
        addto.appendChild(documentTypeNode);
      }

      writer.setPrintEmptyElements(false);

      writer.print(new OutputStreamWriter(str),dom,writer.UNFORMATTED);
      }
      catch(Exception e)
      {
        String err = "INGESTION: Failed to write to 'Outbound' directory:" + dir;
        err += " Exception Message =  " + e.getMessage();
        logger.error(err.toString());
      }
      finally
      {
       try{str.close();}catch(Exception e){ }
      }
  }

  private void writeOutbound(Document dom, File input, SourceMonitor mon)
  {

    FileOutputStream str = null;
    File dir = null;
    DirectoryMonitor monitor = (DirectoryMonitor)mon;

    try
    {
      dir = monitor.getOutboundDirectory();
      String filename = input.getName();

      str = IOUtil.constructSafeFOS(dir,filename,monitor.getOutboundExtension());

      Element addto = (Element)DomUtil.getFirstNamedChild(dom,"Document");

      if(passList != null && addto != null)
      {
        Iterator plit = passList.iterator();
        List toAppend = null;

        while(plit.hasNext())
        {
          Element curr = (Element)plit.next();

          toAppend = DomUtil.getChildElements(curr);

          if(toAppend != null && !toAppend.isEmpty())
          {
             Iterator alit = toAppend.iterator();
             Element currentChild = null;

             while(alit.hasNext())
             {
               currentChild = (Element)alit.next();

               addto.appendChild(currentChild);
             }

          }

          addto.removeChild(curr);
        }
      }

        XmlWriter writer = XmlToolKit.getInstance().getXmlWriter();
      writer.setPrintEmptyElements(false);

      writer.print(new OutputStreamWriter(str),dom,writer.UNFORMATTED);
      }
      catch(Exception e)
      {
        String err = "INGESTION: Failed to write to 'Outbound' directory:" + dir
                     + " Exception Message =  " + e;
        logger.error(err.toString());
      }
      finally
      {
       try{str.close();}catch(Exception e){ }
      }
  }

  private void writeFailed(Document dom, File input, SourceMonitor mon)
  {

    FileOutputStream str = null;
    File dir = null;
    DirectoryMonitor monitor = (DirectoryMonitor)mon;

    try
    {

      dir = monitor.getErrorDirectory();

      String filename = input.getName();

      str = IOUtil.constructSafeFOS(dir,filename,monitor.getErrorExtension());

      XmlWriter writer = XmlToolKit.getInstance().getXmlWriter();
      writer.setPrintEmptyElements(false);

      writer.print(new OutputStreamWriter(str),dom,writer.UNFORMATTED);
    }
    catch(Exception e)
    {
      String err = "INGESTION: Failed to write to 'Failed' directory:" + dir
                 + " Exception Message =  " + e;
      logger.error(err.toString());
    }
    finally
    {
      try{str.close();}catch(Exception e){ }
    }

  }

  private void notifyWorkflowForCervus(WorkflowNotifier wfn, IngestionResult parResult) throws Exception
  {
    IEntityBeanPK target = parResult.getResultEntity();
    logger.info("INGESTION: WFN-for-cervus - CP ENTERED");

    int id = target.getId();
    logger.info("INGESTION: WFN-for-cervus - CP 1");
    if (parResult.isICRulesFailure()) {
      wfn.send(id, Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_WUP); //#DG40
      return;
    }
    logger.info("INGESTION: WFN-for-cervus - CP 1a");	
    //FXP25534 - added to new_deal_wup.  This code was never being executed so tasks were never being assigned.
    if (parResult.getNotificationType() == Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL 
    		|| parResult.getNotificationType() == Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_WUP) 
    {
      logger.info("INGESTION: WFN-for-cervus - CP 2");
      wfn.send(id, Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_WUP);
      return;
    }

    // Notify WorkFlow : Modify Deal with CapLink
    logger.info("INGESTION: WFN-for-cervus - CP 7");
    wfn.send(id, Mc.WORKFLOW_NOTIFICATION_TYPE_TASK_TRIGGER_TYPE_2_WUP);

    logger.info("INGESTION: WFN-for-cervus - CP 8");
    logger.info("INGESTION: WFN-for-cervus - CP EXIT");
  }

  // #DG126 BMO-CCAPS workflow notification - rewritten
  public void notifyWorkflow(WorkflowNotifier wfn, IngestionResult parResult) throws Exception
  {
    IEntityBeanPK target = parResult.getResultEntity();
    logger.info("INGESTION: WFN - CP ENTERED");
    if(target == null)
      return;

    if (parResult.isCervusJob()){
      notifyWorkflowForCervus( wfn, parResult );
      return;
    }

    int id = target.getId();
    logger.info("INGESTION: WFN - CP 1");
    if(parResult.isICRulesFailure()){
      wfn.send( id, parResult.getNotificationType() );
      return;
    }
    // #DG126 BMO-CCAPS workflow notification
    if( islNotificationType.equalsIgnoreCase("bmo") ){
      logger.info("INGESTION: WFN - BMO");
      if (parResult.getNotificationType() == Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL) {
        wfn.send(id, Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_B);
        return;
      }
    }

    logger.info("INGESTION: WFN - CP 1a");
    if( parResult.isCapLinkSensitive() && parResult.getNotificationType() == Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL ){
      logger.info("INGESTION: WFN - CP 2");
      wfn.send( id, Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_WCP );
      return;
    }
    logger.info("INGESTION: WFN - CP 3");
    if( !parResult.isCapLinkRequired() || parResult.isICRulesFailure())
    {
      logger.info("INGESTION: WFN - CP 4");
      // Default to send Notification without CapLink
      wfn.send( id, parResult.getNotificationType() );
    }
    else
    {
      // We need CapLink here
      logger.info("INGESTION: WFN - CP 5");
      if(parResult.getNotificationType() == Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL){
        // Notify WorkFlow : New Deal with CapLink
        logger.info("INGESTION: WFN - CP 6");
        wfn.send( id, Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_WCP );
      } else {
        // Notify WorkFlow : Modify Deal with CapLink
        logger.info("INGESTION: WFN - CP 7");
        wfn.send( id, Mc.WORKFLOW_NOTIFICATION_TYPE_TASK_TRIGGER_TYPE_2_WCP );
      }
    }
    logger.info("INGESTION: WFN - CP 8");
    logger.info("INGESTION: WFN - CP EXIT");
  }

  public void logSuccess(SessionResourceKit srk, IEntityBeanPK pk, String filename)
  {

     SysLogger logger = srk.getSysLogger();

    try
    {
      DealHistoryLogger dhl = DealHistoryLogger.getInstance(srk);

      int dealId = pk.getId();
      int copyId = pk.getCopyId();
      int upid = srk.getUserProfileId();

      Deal deal = new Deal(srk,null,dealId, copyId);
      int sobid = deal.getSourceOfBusinessProfileId();

      dhl.log(dealId, copyId, dhl.dealIngestion(sobid),Mc.DEAL_TX_TYPE_INTERFACE,upid );
    }
    catch(Exception ex)
    {
      StringBuffer msg = new StringBuffer("Deal History Entry logging failed after Successful Deal Ingestion");
      msg.append("DealId: ").append(pk.getId());
      msg.append("CopyId: ").append(pk.getCopyId());
      logger.error(msg.toString());
      logger.error(ex);
    }
  }

  //--DocCentral_FolderCreate--21Sep--2004--start--//
  public void logDocCentralFolderCreation(SessionResourceKit srk,
                                          IEntityBeanPK pk,
                                          SourceFirmProfile sourceFirmEntity,
                                          String folderIdentifier)
  {
    try
    {
      DealHistoryLogger dhl = DealHistoryLogger.getInstance(srk);

      int dealId = pk.getId();
      int copyId = pk.getCopyId();
      int upid = srk.getUserProfileId();

      //Deal deal = new Deal(srk, null, dealId, copyId);

      dhl.log(dealId,
              copyId,
              dhl.docCentralFolderCreationInfo(sourceFirmEntity, folderIdentifier),
              Mc.DEAL_TX_TYPE_DOCCENTRAL_FOLDER_CREATION,
              upid );
    }
    catch(Exception ex)
    {
      StringBuffer msg = new StringBuffer("Deal History Entry logging failed after Successful DocCentral Folder creation");
      msg.append("DealId: ").append(pk.getId());
      msg.append("CopyId: ").append(pk.getCopyId());
      msg.append("FolderIdentifier: ").append(folderIdentifier);
      logger.error(msg.toString());
      logger.error(ex);
    }

  }
  //--DocCentral_FolderCreate--21Sep--2004--end--//


  public void logFailure(SessionResourceKit srk, IEntityBeanPK deal, String filename)
  {
     //int  MAX_MSG_LEN = 50;

     StringBuffer msg = new StringBuffer("Application Rejected - ");
     msg.append("File Name: ").append(filename);
     if(deal != null)
     {
      msg.append(" DealId: ").append(deal.getId());
      msg.append(", CopyId: ").append(deal.getCopyId());
     }
     else
     {
      msg.append(" DealId: ").append("Unstored Application");
      msg.append(", CopyId: ").append("Unstored Application");
     }
     SysLogger logger = srk.getSysLogger();
     logger.info(msg.toString());
  }

  private void appendICMessagesAsDealNote(org.w3c.dom.Document dom, IngestionResult res)
    throws FileIngestionException
  {
    List l = null;
    if( res.isFilogixLink()){
      l = DomUtil.getNamedDescendants(dom, "Document");
    }else{
      l = DomUtil.getNamedDescendants(dom, "Deal");
    }
    if( l == null || l.isEmpty() ) return;

    Node root = (Node)l.get(0);
    //Node root = getRootAssociation().getNode();
    //Deal rootEntity = (Deal)getRootAssociation().getEntity();

    Collection msgs = res.getICRulesMessages();
    Iterator it = msgs.iterator();
    String msg = null;

    while(it.hasNext())
    {
      msg = (String)it.next();
      Element noteElement = dom.createElement("DealNotes");
      Element noteText = dom.createElement("dealNotesText");
      Element dealNotesCategory = dom.createElement("dealNotesCategory");
      Element dealNotesUser = dom.createElement("dealNotesUser");

      noteElement.appendChild(noteText);
      noteText.appendChild(dom.createTextNode(msg));

      noteElement.appendChild(dealNotesCategory);
      dealNotesCategory.appendChild(dom.createTextNode("Deal Ingestion"));

      noteElement.appendChild(dealNotesUser);
      dealNotesUser.appendChild(dom.createTextNode("SYSTEM"));

      root.appendChild(noteElement);
    }
  }

  private void appendDealNote(org.w3c.dom.Document dom, IngestionResult res)
    throws FileIngestionException
  {
    Node root = getRootAssociation().getNode();
    Deal rootEntity = (Deal)getRootAssociation().getEntity();

    Collection msgs = res.getMessages();
    Iterator it = msgs.iterator();
    String msg = null;

    while(it.hasNext())
    {
      msg = (String)it.next();
      Element noteElement = dom.createElement("DealNotes");
      Element noteText = dom.createElement("dealNotesText");
      Element dealNotesCategory = dom.createElement("dealNotesCategory");
      Element dealNotesUser = dom.createElement("dealNotesUser");

      noteElement.appendChild(noteText);
      noteText.appendChild(dom.createTextNode(msg));

      noteElement.appendChild(dealNotesCategory);
      dealNotesCategory.appendChild(dom.createTextNode("Deal Ingestion"));

      noteElement.appendChild(dealNotesUser);
      dealNotesUser.appendChild(dom.createTextNode("SYSTEM"));

      root.appendChild(noteElement);
    }
 }

 /**
  * Deprecated method: do not use. This method has been used for old ay of
  * dealing with duplicates. Now nobody uses this way but we still have it in
  * code.
  *
  * @param dom Document
  * @param searchResult SearchResult
  * @param langId int
  * @throws FileIngestionException
  * @deprecated
  */
 private void appendDupeCheckMessageToSource(org.w3c.dom.Document dom, SearchResult searchResult, int langId)
    throws FileIngestionException
  {
	PassiveMessage res = SourceMessageBuilder.getMessage(srk.getExpressState().getDealInstitutionId(), searchResult, langId);

    Node root = getRootAssociation().getNode();
    Deal rootEntity = (Deal)getRootAssociation().getEntity();

    Collection msgs = res.getMsgs();
    Iterator it = msgs.iterator();
    String msg = null;

    while(it.hasNext())
    {
      msg = (String)it.next();
      Element noteElement = dom.createElement("DealNotes");
      Element noteText = dom.createElement("dealNotesText");
      Element dealNotesCategory = dom.createElement("dealNotesCategory");
      Element dealNotesUser = dom.createElement("dealNotesUser");

      noteElement.appendChild(noteText);
      noteText.appendChild(dom.createTextNode(msg));

      noteElement.appendChild(dealNotesCategory);
      dealNotesCategory.appendChild(dom.createTextNode("Deal Ingestion"));

      noteElement.appendChild(dealNotesUser);
      dealNotesUser.appendChild(dom.createTextNode("SYSTEM"));

      root.appendChild(noteElement);
    }
 }

  private void appendDupeCheckMessageToSource(org.w3c.dom.Document dom, IngestionResult pResult)
    throws FileIngestionException
  {
    List l = null;
    if( pResult.isFilogixLink()){
      l = DomUtil.getNamedDescendants(dom, "Document");
    }else{
      l = DomUtil.getNamedDescendants(dom, "Deal");
    }
    if( l == null || l.isEmpty() ) return;

    Node root = (Node)l.get(0);

    Collection msgs = pResult.getDupeCheckMessagesForBroker();
    Iterator it = msgs.iterator();

    String msg = null;

    while(it.hasNext())
    {
      msg = ((ClientMessage)it.next()).getMessageText();
      Element noteElement = dom.createElement("DealNotes");
      Element noteText = dom.createElement("dealNotesText");
      Element dealNotesCategory = dom.createElement("dealNotesCategory");
      Element dealNotesUser = dom.createElement("dealNotesUser");

      noteElement.appendChild(noteText);
      noteText.appendChild(dom.createTextNode(msg));

      noteElement.appendChild(dealNotesCategory);
      dealNotesCategory.appendChild(dom.createTextNode("Deal Ingestion"));

      noteElement.appendChild(dealNotesUser);
      dealNotesUser.appendChild(dom.createTextNode("SYSTEM"));

      root.appendChild(noteElement);
    }
 }

  /**
   * <pre>
   *  Recursively associates element nodes to Entities using a preorder traversal of the dom
   *  tree branches below node.
   *  </pre>
   *  @return a List of EntityNodeAssociation instances
   *
   * @param assoc EntityNodeAssociation
   * @param dcm CalcMonitor
   * @throws FileIngestionException
   * @return List
   */
  private List makeAssociations(EntityNodeAssociation assoc, CalcMonitor dcm)
    throws FileIngestionException
  {
    Node node = assoc.getNode();
    DealEntity container = assoc.getEntity();

    if(isElementNode(node))
    {
      ingestedNodeList.add(node);

      if(node.hasChildNodes())
      {
         NodeList nl = node.getChildNodes();
         int size = nl.getLength();

         for (int i = 0; i < size; i++)
         {
           Node child = nl.item(i);

           if(isIngestableTable(child))
           {

              try
              {                 
                String name = child.getNodeName();
                EntityNodeAssociation currentAssociation = null;
                DealEntity currentEntity = null;               
                currentEntity = EntityBuilder.createEntity(name, srk, dcm, container);
                currentAssociation = new EntityNodeAssociation(child,currentEntity,document,assoc);

                if(assoc != null) assoc.addChild(currentAssociation);

                associationList.add(currentAssociation);
                handleEntityFields(currentAssociation);

                handleSpecial(currentAssociation,child,currentEntity,dcm);
                makeAssociations(currentAssociation,dcm);
              }
              catch(Exception e)
              {
                String msg = "Failed to make EntityNodeAssociation for: " + child.getParentNode().getNodeName()+ "." + child.getNodeName() + "  ";
                logger.error(msg);
                logger.error(e) ;
                throw new FileIngestionException(msg);
              }
           }

         }//end for
      }
    }
    else
    {

      Node next = node.getNextSibling();

      if(next != null)
      {
        EntityNodeAssociation ena = new EntityNodeAssociation(next,null,document,assoc);
        if(assoc != null) assoc.addChild(ena);
        makeAssociations( ena, dcm);
      }
      else if( node.hasChildNodes() )
      {
        next = node.getFirstChild();
        EntityNodeAssociation ena = new EntityNodeAssociation(next,null,document,assoc);
        if(assoc != null) assoc.addChild(ena);
        makeAssociations(ena,dcm);
      }

    } // end if else

    return associationList;
  }

 /**
  *  Handles a leaf nodes for association - searches for picklist values in picklistValues.ini
  *
  * @param assoc EntityNodeAssociation
  */
 private void handleEntityFields(EntityNodeAssociation assoc)
  {
    Node node = assoc.getNode();

    NodeList nl = node.getChildNodes();

    int size = nl.getLength();

    for (int i = 0; i < size; i++)
    {
       Node child = nl.item(i);

       if(child.getNodeType() == Node.ELEMENT_NODE)
        handleFieldNode(assoc, child);
    }
  }

  private boolean handleFieldNode(EntityNodeAssociation ass, Node fieldNode)
  {

     DealEntity entity = ass.getEntity();

     if(entity == null) return false;

     String data = null;

     try
     {
       Node value = fieldNode.getFirstChild();
       data = value.getNodeValue();
       data = data.trim();
     }
     catch(NullPointerException npe)  //if the node has null value
     {
       return false;
     }

     if(data.length() == 0) return false;  //if the node has empty string value

     String field = fieldNode.getNodeName();
     //=====================================================================================
     //ZIVKO:ALERT:INGESTION: this is the place for making break point to see how the system handles
     // node from ingest file
     //=====================================================================================
     //String id = null;

     //if the field is handled as special - take a detour
     if(handleSpecialFields(ass, data, field))
      return true;

     String storable = handlePicklist(field, data);

     if(!storable.equals(data))
      field = field + "Id";

     try
     {
       entity.setField(field, storable);
       ass.updateSetFields(field);
       return true;
     }
     catch(Exception e)
     {
       String report =  "Failed to set FIELD: " + field + " in ENTITY: "
                     + fieldNode.getParentNode().getNodeName() + ':' + e;
       logger.trace(report);
       debug(getClass().getName() + ':' + report);
       return false;
     }

  }

  /**
   *  @param desc -  the description value
   *  @param table - the picklist table
   *  @return the picklistId value for the supplied description if found else <br>
   *  return the description  if the table is a picklist table and no Id is found log a message.
   */
  private String handlePicklist(String table, String desc)
  {
     String idvalue = null;
     if(PicklistData.containsTable(table))
     {
        idvalue = PicklistData.getIdAsString(table,desc);

        if(idvalue == null)
        {
          String msg = "INGESTION: INFO: Unknown description value recieved - TableName = ";
          msg += table + " : DescriptionValue = " + desc;
          logger.info(msg);
          idvalue = "0";
        }

        return idvalue;
     }
     else
     {
       return desc;
     }

  }

  /**
   * Performs any special field related logic.
   * @return true if the method handles the supplied field else return false
   *
   * @param ass EntityNodeAssociation
   * @param data String
   * @param field String
   * @return boolean
   */
  private boolean handleSpecialFields(EntityNodeAssociation ass,  String data, String field)
  {
     // handle jobtitle

     if(ass.getName().equals("EmploymentHistory") && field.equals("jobTitle"))
     {
        EmploymentHistory eh = (EmploymentHistory)ass.getEntity();
        eh.setJobTitle(data);    //set the jobtitle description supplied

        //if the description matches a value from the JobTitle picklist ...
        String value = handlePicklist(field, data);

        if(value.equals(data))
         return true;
        else
        {
           // ...value will be different than supplied description
           //If it is a number set the field - else set the field with 0
          eh.setJobTitleId( TypeConverter.intTypeFrom(value, 0));
          return true;
        }
     }
     // added for ML - by Midori July 3, 2007 == START
     else if(ass.getName().equals("Deal") && field.equals("ECNILenderId"))
     {
       try
       {
         InstitutionProfile profile = new InstitutionProfile(srk);
         profile = profile.findByECNID(data);
         Deal deal = (Deal)ass.getEntity();
         deal.setInstitutionProfileId(profile.getInstitutionProfileId());
       }
       catch(RemoteException re)
       {
         logger.error("RemoteException: InstitutionProfileId could not obtained by ECNILenderId = " + data);
         //TODO:what should I do?
         //Lender profile id is mandatory for ML
       }
       catch(FinderException fe)
       {
         logger.error("FinderException: InstitutionProfileId could not obtained by ECNILenderId = " + data);
         //TODO:what should I do?         
         //Lender profile id is mandatory for ML
       }
       return true;
     }
     // added for ML - by Midori July 3, 2007 == END

     return false;
  }

  /**
   * @param assc EntityNodeAssociation
   * @param node Node
   * @param ent DealEntity
   * @param dcm CalcMonitor
   * @throws Exception
   */
  private void handleSpecial(EntityNodeAssociation assc, Node node, DealEntity ent, CalcMonitor dcm)throws Exception
  {
      if(assc == null) return;

      if(assc.getName().equals("BorrowerAddress"))
      {
        if(DomUtil.hasChildNamed(node,"Addr"))
        {
           BorrowerAddress de = (BorrowerAddress)assc.getEntity();
           Addr add = de.getAddr();

           Node aNode = DomUtil.getFirstNamedChild(node,"Addr");

           EntityNodeAssociation current = new EntityNodeAssociation(aNode,add,document,assc);

           assc.addChild(current);

           handleEntityFields(current);

           associationList.add(current);

           return;
        }
      }
      else if(assc.getName().equals("EmploymentHistory"))
      {
        if(DomUtil.hasChildNamed(node,"Contact"))
        {
          EmploymentHistory hist = (EmploymentHistory)assc.getEntity();
          Contact ct = hist.getContact();

          Node aNode = DomUtil.getFirstNamedChild(node,"Contact");

          EntityNodeAssociation current = new EntityNodeAssociation(aNode,ct,document,assc);

          handleEntityFields(current);

          associationList.add(current);

          assc.addChild(current);

          handleSpecial(current,aNode,ct,dcm);

          return;
        }
      }
      else if(assc.getName().equals("Contact"))
      {
         Contact de = (Contact)assc.getEntity();
         Addr add = de.getAddr();

         Node aNode = DomUtil.getFirstNamedChild(node,"Addr");

         EntityNodeAssociation current = new EntityNodeAssociation(aNode,add,document,assc);

         handleEntityFields(current);

         assc.addChild(current);

         associationList.add(current);

         return;
      }

      return;
  }

  //--DJ_CR136--start--//
  private boolean isIngestableTable(Node n)
  {
     String name = n.getNodeName();
     //--DJ_CR136--start--//
     boolean isDJClient = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(),COM_BASIS100_CALC_ISDJCLIENT, "N").equals("Y");

     String[] aingestableTables =  isDJClient? ingestableTablesDJ: ingestableTables;
     for (String aTable : aingestableTables) 
    	 if (name.equals(aTable))
           return true;
     return false;
  }
  //--DJ_CR136--end--//

  private void setStatusAndType(IngestionResult result, Deal deal, Element dealNode) throws Exception
  {
      //#DG228 also used in specialProcessingHandler.processMIInsurerRequest
      String lSetMtgInsur = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(),COM_BASIS100_INGESTION_SET_MORTGAGE_INSURER,"N");

     logger.debug("INGESTION: Setting status and type....");

     if(result.isValidationExcused())
      deal.setStatusId(Mc.DEAL_INCOMPLETE);
     else
      deal.setStatusId(Mc.DEAL_RECEIVED);

     deal.setStatusDate(ingestionDate);
     deal.setApplicationDate(ingestionDate);

     double ltv = deal.getCombinedLTV();
     logger.debug("INGESTION: Current LTV: " + ltv);

     //3.2patch LTV project
 	 double lLtv_Threshold = Double.parseDouble(PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(),Xc.ING_LTV_THRESHOLD,"80"));

     if(ltv > lLtv_Threshold)
     {
       deal.setDealTypeId(Mc.DEAL_TYPE_HIGH_RATIO);

       //if( deal.getMortgageInsurerId() == Mc.MI_INSURER_BLANK)
       //{
       // ========================================================================
       // the following code has been removed based on tracker issue #267
       // and replaced with the code which will run unconditionaly
       // ========================================================================

       if( ! lSetMtgInsur.equalsIgnoreCase("X") )   //#DG228 let the defaults prevail for xcd
         MIProcessHandler.setForIngestionHighRatio(deal, srk);
       //}

       //3.2patch LTV project
       if(ibRstPrePayLTVgtLimit) {
         deal.setPrePaymentOptionsId(Mc.PREPAYMENT_OPTIONS_NHA);
       }

     }

    /*
     deal.setDealTypeId(Mc.DEAL_TYPE_HIGH_RATIO);
     if( deal.getMortgageInsurerId() == Mc.MI_INSURER_BLANK)
     {
       MIProcessHandler.setForIngestionHighRatio(deal, srk);
     }
     deal.setPrePaymentOptionsId(Mc.PREPAYMENT_OPTIONS_NHA);
*/
  }

  private boolean isElementNode(Node node)
  {
    return node.getNodeType() == node.ELEMENT_NODE ;
  }




  /**
   *  @return an Association object for the given node or null if none is found
   *
   * @param node Node
   * @return EntityNodeAssociation
   */
  private EntityNodeAssociation getAssociationFor(Node node)
  {

    if(node == null) return null;

    ListIterator li = associationList.listIterator();

    while(li.hasNext())
    {
      EntityNodeAssociation current = (EntityNodeAssociation)li.next();

      if((current.getNode()).equals(node))
      {
        return current;
      }
    }
    return null;
  }

  private List getAssociationsFor(String tagname)
  {
    List out = new ArrayList();
    ListIterator li = associationList.listIterator();

    while(li.hasNext())
    {
      EntityNodeAssociation current = (EntityNodeAssociation)li.next();

      if(current.getName().equals(tagname))
        out.add(current);

    }
    return null;
  }

  private EntityNodeAssociation getRootAssociation()
  {

    List l = DomUtil.getNamedDescendants(document, "Deal");

    if(l.isEmpty()) return null;

    return getAssociationFor((Node)l.get(0));
  }

  private String debug(String msg)
  {
     if(debug)
      System.out.println(msg);

    return msg;
  }


  private void loadDefaults(EntityNodeAssociation ass, ApplicationDefaults defaults, String abber)
    throws FileIngestionException
  {
     defaults.checkDefaults(ass, abber);
  }

  /**
   *  MCAP specific check that allows deals with the given system type and or lender
   *  to be ingested as incomplete on validation failure. This should be a database
   *  field!!!!!!!!
   *
   * @param current IngestionResult
   * @param deal Deal
   * @throws FileIngestionException
   * @return boolean
   */
  private boolean checkValidationExcused(IngestionResult current, Deal deal)
    throws FileIngestionException
  {
     try
     {
       String name = deal.getLenderProfile().getLPShortName();
       int st = deal.getSystemTypeId();

       if((st == Mc.SYSTEM_TYPE_MORTY && name.equals("ING")) || st == Mc.SYSTEM_TYPE_EXTRANET)
       {
          current.isValidationExcused(true);
       }

       return current.isValidationExcused();
     }
     catch(Exception ex)
     {
	String msg = "INGESTION: Error while checking validation excused - Reason: "+ ex;
        logger.error(msg);
        return false;
     }
  }


  private List getAssociationList()
  {
    if(associationList == null) return new ArrayList();

    return associationList;
  }


  private void reset()
  {
    this.associationList = null;
    this.ingestedNodeList = null;
    this.ingestedNodeList = new ArrayList();
    this.associationList = new ArrayList();
    this.ingestionDate = new Date();

  }

  //#DG600 rewritten to produce multilingual deal notes
  private void createDealNotes(IngestionResult res, Deal pDeal) throws Exception
  {
	final int catID = Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION;
	final int appId = TypeConverter.intTypeFrom(pDeal.getApplicationId());
	final ArrayList<Formattable[]> msgsObjs = res.getICRulesMsgObjects();
    DealNotes notes  = new DealNotes(srk);
	DealPK dpk = new DealPK(pDeal.getDealId(), pDeal.getCopyId());

	for (Formattable[] formatta : msgsObjs) {
		notes.create(dpk, appId, catID, 0, formatta);
	}
  }

  private int determineLanguage(Deal pDeal){
    int rv = Mc.LANGUAGE_PREFERENCE_ENGLISH;
    try{
      Collection col = pDeal.getBorrowers();
      Iterator it = col.iterator();
      while( it.hasNext() ){
        Borrower bor = (Borrower) it.next();
        if(bor.isPrimaryBorrower()){
          rv = bor.getLanguagePreferenceId();
          return rv;
        }
      }
    }catch(Exception exc){
      return rv;
    }
    return rv;
  }

  private boolean isFXLinkType(int pType) throws Exception
  {
    String fxlinkTypes = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(),COM_BASIS100_FXLINK_TYPES);
    // if the property does not exist return false
    if(fxlinkTypes == null){
      return false;
    }
    try
    {
      StringTokenizer st = new StringTokenizer(fxlinkTypes, ",");
      String token;

      while (st.hasMoreTokens())
      {
        token = st.nextToken();

        if( Integer.parseInt( token.trim() ) == pType){
          return true;
        }
      }
      return false;
    }
    catch (Exception e)
    {
      throw new Exception("ERROR - wrong property value(s) for property: com.basis100.fxlink.types ");
    }

  }

  private static String FXLINK_KEY = "ecniLenderId";

  /**
   * <p>getInstitutiionId</p>
   * <p> find InstituionProfileId from Document.
   * 
   * 
   * @param dom
   * @return
   * @throws Exception
   */
  public int getInstitutionId(Document dom, SessionResourceKit srk) throws RemoteException, FinderException
  {
    String idStr = getECNIID(dom);
    InstitutionProfile ip = new InstitutionProfile(srk);
    ip = ip.findByECNID(idStr);

   // return ip.getInstitutionProfileId();
    int institutionId = ip.getInstitutionProfileId();

    ///////////////////////Added for Multi Brand to Multi Lender July 2008///////////////////////////////////
    String strBrand  = PropertiesCache.getInstance().getInstanceProperty("multi.brand.legacy.redirect.max.number.of.days.to.redirect.deals.to.brand","0");
	logger.info("max number of days for redirect: " + strBrand);
    String strDate  = PropertiesCache.getInstance().getInstanceProperty("multi.brand.legacy.redirect.institution.activation.date",
																		"Active date not set");
	logger.info("redirect activation date: " + strDate);

    try{
        long maxOfDays = Integer.parseInt(strBrand);
        if (maxOfDays <= 0) return institutionId;

        SimpleDateFormat  sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date activationDate = sdf.parse(strDate);
		logger.info("redirect activation date object: " + activationDate);

        String sourceApplicationId = getSourceApplicationId(dom);
        MbmlRedirect  mbmlRedirect = new MbmlRedirect(srk);
        mbmlRedirect = mbmlRedirect.findBySourceApplicationId(sourceApplicationId);

        Date currentDate = java.util.Calendar.getInstance().getTime();

        if(mbmlRedirect != null && activationDate != null){
          int originalId = mbmlRedirect.getInstitutionProfileId();
		  logger.info("original institutionprofile id: " + originalId);
          long numberOfDays = currentDate.getTime() - activationDate.getTime();
          if (numberOfDays <= maxOfDays*(24 * 60 * 60 * 1000)){
              institutionId = originalId;
			  logger.info("redirecting " + sourceApplicationId + " to " + originalId);
          }
        }
    } catch(ParseException pe) {
        String msg =  pe.getMessage();
        if(msg == null) msg = "INGESTION: ApplicationHandler:getInstitutionId: " + "ERROR: Cannot parse \"" + strDate + "\"";
        msg =  "INGESTION: ApplicationHandler:getInstitutionId: " + msg;
        logger.error(msg);
        logger.error(pe);
    } catch(Exception e) {
        String msg =  e.getMessage();
        if(msg == null) msg = "Unknown Error";
        msg =  "INGESTION: ApplicationHandler:getInstitutionId: " + msg;
        logger.error(msg);
        logger.error(e);
     }

    return institutionId;
  }

  public String getAbbre(Document dom) throws RemoteException, FinderException
  {
      String idStr = getECNIID(dom);
      InstitutionProfile ip = new InstitutionProfile(srk);
      ip = ip.findByECNID(idStr);
      return ip.getInstitutionAbbr();
  }
  
  private String getECNIID(Document dom) throws RemoteException, FinderException
  {
      List nodesByName = DomUtil.getNamedDescendants(dom, FXLINK_KEY);
      if (nodesByName == null || nodesByName.size()!=1)
        throw new FinderException();
        
      Node institionNode = (Node)nodesByName.get(0);
      String idStr = institionNode.getFirstChild().getNodeValue();
      if (idStr== null || idStr.trim().length() == 0) 
        throw new FinderException();
      return idStr;
  }

  /**
   * <p>
   * Description: Creates the ComponentSummary entity object by taking
   * Deal object and Calcmonitor object.
   * </p>
   * @version 1.0 XS_1.2  11-Aug-2008 Initial Version
   * @param deal
   *            deal Object.
   * @param dcm
   *             CalcMonitor Object
   * @throws Exception
   *             exception that may occurs if the entities do not exist.
   */
  private void storeSpecial(Deal deal, CalcMonitor dcm) throws Exception
  {

	int dealId = deal.getDealId();
	int copyId = deal.getCopyId();
	Component comp = new Component(srk);
	Collection componentCollections = comp.findByDeal(new DealPK(dealId, copyId));
	if (componentCollections.size() == 0)
	{
	  return;
	}
	ComponentSummary compSummary = null;
	compSummary = new ComponentSummary(srk);
	ComponentSummaryPK compSummaryPk = new ComponentSummaryPK(dealId, copyId);
	try
	{
	  compSummary.setSilentMode(true); //FXP24463
	  compSummary = compSummary.findByPrimaryKey(compSummaryPk);
	}
	catch (RemoteException fe)
	{
	  logger.debug((new StringBuilder()).append(
		  "IGNORED:: Can not find the Component Summary: dealId =").append(
		  dealId).append(", copyId=").append(copyId).toString());
	  compSummary = compSummary.create(dealId, copyId);
	}
	catch (FinderException fe)
	{
	  logger.debug((new StringBuilder()).append(
		  "IGNORED:: Can not find the Component Summary: dealId =").append(
		  dealId).append(", copyId=").append(copyId).toString());
	  compSummary = compSummary.create(dealId, copyId);
	}
	
  }
  
  //Get SourceApplicationId---added for MB to ML  July, 2008
  public String getSourceApplicationId(Document dom) throws RemoteException, FinderException
  {
	  List nodesByName = DomUtil.getNamedDescendants(dom, "sourceApplicationId");
	  if (nodesByName == null || (nodesByName != null && nodesByName.size()!=1))
	        throw new FinderException();
	  
	  Node sourceApplicationIdNode = (Node)nodesByName.get(0);
	  String str = sourceApplicationIdNode.getFirstChild().getNodeValue();
	  if (str == null || (str != null && str.trim().length() == 0)) 
	        throw new FinderException();
	  return str;	  
  }
}
