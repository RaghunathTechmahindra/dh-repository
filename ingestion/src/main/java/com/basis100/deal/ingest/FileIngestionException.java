package com.basis100.deal.ingest;

public class FileIngestionException extends Exception
{

 /**
  * Constructs a <code>FileIngestionException</code> with no specified detail message.
  */

  public FileIngestionException()
  {
	  super();
  }

 /**
  * Constructs a <code>FileIngestionException</code> with the specified message.
  * @param   message  the related message.
  */
  public FileIngestionException(String message)
  {
    super(message);
  }
}

