package com.basis100.deal.ingest;

import java.util.ArrayList;
import java.util.Formattable;
import java.util.List;

import MosSystem.Mc;

import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.security.PassiveMessage;

/**
 *  IngestionResult encapsulates the results of the ingestion process -
 *  Including messages.<p>An IngestionResult is associated with a Deal and
 *  has one or more error flags indicating status.<br>
 *
 *
 */

public class IngestionResult
{

  private boolean duplicateInput;
  private boolean invalidSource;
  private boolean invalidInput;
  private boolean validationFailure;
  private boolean validationExcused;
  private boolean applicationError;
  private boolean notificationExcused;
  private boolean dealLocked;
  private int lockedDealId;
  private boolean icRulesFailure;
  private PassiveMessage message;
  private PassiveMessage icRulesPassiveMessage;
  private boolean messagesForBrokerAppended;
  protected ArrayList dupeCheckMessagesForBroker;
  private boolean capLinkRequired;
  private boolean capLinkSensitive;
  private int notificationType;
  private boolean filogixLink;

  // used for sfp/sob batch upload
  private boolean sfpBatchSuccess;
  private IEntityBeanPK resultEntity;
  private boolean cervusJob;

  public IngestionResult()
  {
    message = new PassiveMessage();
    icRulesPassiveMessage = new PassiveMessage();
    //notificationType = Mc.WORKFLOW_NOTIFICATION_TYPE_NULL;
    //=================================================================================
    // Billy and Zivko made this change this line because we do not want notification
    // to go with -1. Change has been made on Feb 28, 2002
    //==================================================================================
    notificationType = Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL;
    icRulesFailure = false;
    messagesForBrokerAppended = false;
    dupeCheckMessagesForBroker = new java.util.ArrayList();
    capLinkRequired = false; // by default
    capLinkSensitive = false; // by default
    filogixLink = false;   // default
    sfpBatchSuccess = false;  // default
    cervusJob = false;
  }

  public void setSfpBatchSuccess(boolean pVal){
    sfpBatchSuccess = pVal;
  }

  public boolean isSfpBatchSuccess(){
    return sfpBatchSuccess;
  }

  public void setDupeCheckMessagesForBroker(ArrayList pMessList){
    this.dupeCheckMessagesForBroker = pMessList;
  }

  public ArrayList getDupeCheckMessagesForBroker(){
    return dupeCheckMessagesForBroker;
  }

  public boolean isFilogixLink(){
    return filogixLink;
  }

  public void setFilogixLink(boolean val){
    filogixLink = val;
  }

  public boolean isMessagesForBrokerAppended(){
    return messagesForBrokerAppended;
  }

  public void setMessagesForBrokerAppended(boolean appendedOrNot){
    messagesForBrokerAppended = appendedOrNot;
  }

  public void setICRulesFailure(boolean icValue){
    icRulesFailure = icValue;
  }

  public boolean isICRulesFailure(){
    return icRulesFailure;
  }

  public boolean isDealLocked(){
    return dealLocked;
  }

  public void setDealLocked(boolean lockedOrNot){
    dealLocked = lockedOrNot;
  }

  public void setLockedDealId(int idToSet){
    lockedDealId = idToSet;
  }

  public int getLockedDealId(){
    return lockedDealId;
  }

  public void setNotificationType(int typeToSet){
    notificationType = typeToSet;
  }

  public int getNotificationType(){
    return notificationType;
  }

  public boolean isNonCritical()
  {
    return (duplicateInput || invalidSource || validationFailure || icRulesFailure);
  }

  public boolean isCriticalFailure()
  {
     return (applicationError || invalidInput);
  }

  /**
   * Gets a non - critical error status.<br>
   * InvalidSource Error indicates the file has come from an unrecognized or invalid<br>
   * source as defined within the handler logic.
   */
  public boolean isInvalidSource()
  {
    return invalidSource;
  }

  public void isInvalidSource(boolean yn)
  {
    invalidSource = yn;
  }

  /**
   * Gets a critical error status.<br>
   * InvalidInput Error reflects file level error i.e. in structure.<br>
   * Not an application functional error but critical none the less.
   */
  public boolean isInvalidInput()
  {
    return invalidInput;
  }
  /**
   * Sets critical error status.<br>
   * InvalidInput Error reflects file level error i.e. in structure
   * Not an application functional error but critical none the less.
   */
  public void isInvalidInput(boolean yn)
  {
    invalidInput = yn;
  }

   /**
   * sets error status.<br>
   * Validition Error is a an failure of content validation
   * Not an application functional error.
   */
  public boolean isValidationFailure()
  {
    return validationFailure;
  }
    /**
   * <br>
   * Validition Error can occur and handled but does not result in error state
   *
   */
  public boolean isValidationExcused()
  {
    return validationExcused;
  }

  public void isValidationFailure(boolean yn)
  {
    validationFailure = yn;
  }

  public void isValidationExcused(boolean yn)
  {
    validationExcused = yn;
  }

  public boolean isDuplicateInput()
  {
    return duplicateInput;
  }

  public void isDuplicateInput(boolean yn)
  {
    duplicateInput = yn;
  }

  /**
   * gets error status.<br>
   * Application Error is a non - content related error - Implies system or application
   * function error.
   */
  public boolean isApplicationError()
  {
    return applicationError;
  }

  /**
   * sets error status.<br>
   * Application Error is a non - content related error - Implies system or application
   * function error.
   */
  public void isApplicationError(boolean yn)
  {
    applicationError = yn;
  }


  /**
   * Turns off notfication for this result.<br>
   *
   */
  public void isNotificationExcused(boolean yn)
  {
    notificationExcused = yn;
  }

  /**
   * Turns off notfication for this result.<br>
   *
   */
  public boolean isNotificationExcused()
  {
    return notificationExcused;
  }



  /**
   * gets the PK object for the result entity if one exists
   */
  public IEntityBeanPK getResultEntity()
  {
    return resultEntity;
  }
  /**
   * sets the PK object for the result entity if one exists
   */
  public void setResultEntity(IEntityBeanPK ent)
  {
    this.resultEntity = ent;
  }

   /**
   * sets the passive message object for the result entity if one exists
   */
  public void setMessage(PassiveMessage msg)
  {
    this.message = msg;
  }

  public List getICRulesMessages(){
    if(icRulesPassiveMessage == null){
      return new ArrayList();
    }
    return new ArrayList( icRulesPassiveMessage.getMsgs() );
  }

  //#DG600 return list of formatable objects
  public final ArrayList<Formattable[]> getICRulesMsgObjects(){
  	if(icRulesPassiveMessage == null){
  		return new ArrayList<Formattable[]>();
  	}
  	return icRulesPassiveMessage.getMsgsObjs();
  }
  //#DG600 end 

  public List getMessages()
  {
    if(message == null) return new ArrayList();

    return new ArrayList(message.getMsgs());
  }

  public PassiveMessage getMessage()
  {
    if(this.message == null)
     return new PassiveMessage();

    return this.message;
  }

  public void appendICRulesMessages(PassiveMessage pm)
  {
    if(icRulesPassiveMessage == null){
      icRulesPassiveMessage = new PassiveMessage();
    }
    if( pm != null ){
      icRulesPassiveMessage.addAllMessages(pm);
    }
  }

  public PassiveMessage addAllMessages(PassiveMessage pm)
  {
    if(message == null)
       message = pm;

    message.addAllMessages(pm);
    return message;
  }

  public PassiveMessage addMessage(String msg)
  {
    if(message == null)
       message = new PassiveMessage();

    message.addMsg(msg,message.NONCRITICAL);

    return message;
  }

  public PassiveMessage addMessage(String msg, int type)
  {
    if(message == null)
       message = new PassiveMessage();

    message.addMsg(msg,type);

    return message;
  }

  public boolean hasICRulesMessages(){
    if(icRulesPassiveMessage == null){
      return false;
    }
    if(icRulesPassiveMessage.getNumMessages() <= 0){
      return false;
    }
    return true;
  }

  public boolean hasMessages()
  {
    if(message == null) return false;

    if(message.getNumMessages() <= 0) return false;

    return true;
  }

  public String entityString()
  {
    if(resultEntity == null)
     return "Id not set";

    String out = "Ingested File: " + resultEntity.getWhereClause();
    return out;
  }


  public String printCurrentStatus()
  {
      StringBuffer msg = new StringBuffer("Result ");

      if( duplicateInput) msg.append("is a duplicate Input and ");
      if( invalidSource) msg.append("is an invalid Source and ");
      if( invalidInput) msg.append("is a invalid Input and ");
      if( validationFailure) msg.append("is a validation Failure and ");
      if( validationExcused) msg.append("is a validation Excused and ");
      if( applicationError) msg.append("is an application Error and ");
      if( notificationExcused) msg.append("is a notification Excused and ");

      int nm  = message.getNumMessages() ;

      if(nm < 1)
       msg.append("has no messages.");
      else
       msg.append( "has " + nm + " messages.");

    return msg.toString();
  }

  public void setCapLinkRequired(boolean pReq){
    capLinkRequired = pReq;
  }

  public boolean isCapLinkRequired(){
    return capLinkRequired;
  }

  public void setCapLinkSensitive(boolean pReq){
    capLinkSensitive = pReq;
  }

  public boolean isCapLinkSensitive(){
    return capLinkSensitive;
  }

  public void setCervusJob(boolean pVal){
    cervusJob = pVal;
  }

  public boolean isCervusJob(){
    return cervusJob;
  }

}

