package com.basis100.deal.ingest;

import com.basis100.deal.ingest.event.FileReceptionListener;
import com.basis100.deal.ingest.event.FilesRecievedEvent;

/**
 * <pre>
 * this  contains methods for monitoring
 * a directory for file arrival firing a FileReceptionEvent when
 * files arrive.
 *  </pre>
 */
public interface SourceMonitor extends Runnable
{

  /**
   *  Monitors the directory named in the import_export.properties file for the arrival
   *  of deal files:  com.basis100.monitor.directory
   *  The arrival of files triggers a FilesRecievedEvent.
   */
  public void startMonitor() throws Exception;

  /**
   *  Notify listeners of the indicated FilesRecievedEvents
   */
  public void notifyListeners(FilesRecievedEvent event);

  public void addListener(FileReceptionListener l);

  public void removeListener(FileReceptionListener l) ;

  public void removeAllListeners();

  /**
   * The constraint may represent a file, other resource or data String
   * @return the constraint String
   */
  public String getConstraints();

  /**
   * Gets a String describing the location of the SourceMonitor file input.
   */
  public String getInputLocation();

  /**
   * Gets a String describing the location of the SourceMonitor file output.
   */
  public String getOutputLocation();

   /**
   * Gets a String describing the location of the SourceMonitor file archive.
   */
  public String getArchiveLocation();

   /**
   * Gets a String describing the location of the SourceMonitor errors archive.
   */
  public String getErrorLocation();

}




