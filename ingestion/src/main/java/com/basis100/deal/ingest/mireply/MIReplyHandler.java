/**
 * <p>Title: MIReplyHandler.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2007</p>
 *
 * <p>Company: Filogix Limited Partnership
</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � Aug 2, 2007)
 *
 */
package com.basis100.deal.ingest.mireply;

import java.util.List;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.traversal.TreeWalker;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.BranchProfile;
import com.basis100.deal.entity.PartyProfile;
import com.basis100.deal.ingest.IngestionHandler;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.xml.DomUtil;
import com.basis100.xml.XmlToolKit;

public abstract class MIReplyHandler implements IngestionHandler
{
  protected SessionResourceKit srk;
  protected CalcMonitor dcm;

  private static String BRANCH_KEY = "CMHCBranchTransitNumber";
  /**
   * <p>getInstitutiionId</p>
   * <p> find InstituionProfileId from Document.
   * 
   * 
   * @param dom
   * @return
   * @throws Exception
   */
  public int getInstitutionId(Document dom, SessionResourceKit srk) throws RemoteException, FinderException
  { 
    String idStr = null;
    Node n = dom.getFirstChild();
    TreeWalker tw = XmlToolKit.getInstance().getElementTreeWalker(n);
    Node current = null;
    String currentName = null;

    while (true) {
        current = tw.nextNode();
        if (current == null)
            break;

        if (current.getNodeType() == Node.ELEMENT_NODE) {
            currentName = current.getNodeName();
            if (currentName != null && currentName.equals(BRANCH_KEY)) {
                if (current.getNodeType() == Node.ELEMENT_NODE) {
                    idStr = current.getFirstChild().getNodeValue();
                }
            }
        }
    }

    // FXP23627 - Oct 9, 2009 Midori
    boolean isDJ = false;
    String isDJStr = PropertiesCache.getInstance().getInstanceProperty("com.basis100.calc.isdjclient","N");
    if ("Y".equalsIgnoreCase(isDJStr)){
        isDJ = true;
    }

    int institutionId = -1;
    if (isDJ)
    {
        PartyProfile partyProfile = new PartyProfile(srk);
        institutionId = partyProfile.findInstitionProfileIdByGEID(idStr);
    }
    else
    {
        BranchProfile branchProfile = new BranchProfile(srk);
        branchProfile = branchProfile.findByCMHCBranchTransitNumber(idStr);
        institutionId = branchProfile.getInstitutionProfileId();
    }
    return institutionId;   
  }
  
 
}
