package com.basis100.deal.ingest;

import java.io.File;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.notification.WorkflowNotifier;
import org.w3c.dom.Document;


/**
 *  The IngestionHandler interface ensures cooperation with the IngestionManager .
 *  and IngestionResult objects. The IngestionManager expects to call the required
 *  methods. Methods that do not augment the ingestion process implemented can implement
 *  empty methods. An IngestionHandler implementation must provide a no-args constructor.
 *
 */
public interface IngestionHandler
{
  public static final int ROLLBACK = 1;
  public static final int STORE = 0;

  /**
   *  Perform any pre-XML parse and store duties in the implementation of this method
   */
  public File preProcess(File file)throws Exception;

  /**
   *  Write the provided DOM tree to the target schema for this ingestion
   *  implementation and return a Result object.
   *  @return IngestionResult object
   *  @param dom -  the xml Document object handled by the storage method
   *  @param srk - A SessionResourceKit (supplied by the manager)
   *  @param source -  the Source of the ingested data
   */
  public IngestionResult storeDocument(org.w3c.dom.Document dom, SessionResourceKit srk, SourceMonitor source )
                                  throws Exception;

  /**
   *  Notification of workflow can be performed with this method if required
   */
  public void notifyWorkflow(WorkflowNotifier notifier, IngestionResult result)throws Exception;

  /**
   * Called by the Ingestion manager if processResult returns true
   * @param srk - A SessionResourceKit (supplied by the manager)
   * @param deal -  the Deal target of this ingestion process
   * @param filename -  the name of the ingested file.
   */
  public void logSuccess(SessionResourceKit srk, IEntityBeanPK deal, String filename);

   /**
   * Called by the <code>IngestionManager</code> if processResult() returns false
   * @param srk - A SessionResourceKit (supplied by the manager)
   * @param deal -  the Deal target of this ingestion process
   * @param filename -  the name of the ingested file.
   */
  public void logFailure(SessionResourceKit srk, IEntityBeanPK deal, String filename);

   /**
    *  The <code>IngestionManager</code> loads the <code>IngestionResult</code> produced
    *  during the storage method: storeDocument() into this method.
    *
    *  @return  boolean indicating success or failure of the ingestion process
    *  @param res - An Ingestion Result from the storage attempt
    *  @param input -  the ingested file.
    *  @param dom -  the xml Document object handled by the storage method
    *  @param source -  the Source of the ingested data
    */
  public int processResult(IngestionResult res, File input, Document dom, SourceMonitor source) throws Exception;

  //--DocCentral_FolderCreate--21Sep--2004--start--//
  /**
   * Called by the Ingestion manager if DocCentral API (via DatX) returns true
   * @param srk - A SessionResourceKit (supplied by the manager)
   * @param deal -  the Deal target of this ingestion process
   * @param sfp - SourceFirmProfile entity: brokerage source
   * @param folderIdentifier -  the string identifier of the created folder in DocCentral repository (Advectis)
   */
  public void logDocCentralFolderCreation(SessionResourceKit srk, IEntityBeanPK deal, SourceFirmProfile sourceFirmName, String folderIndentifier);
  //--DocCentral_FolderCreate--21Sep--2004--end--//

  //-- added for MultiLender project - Aug 2, 2007 by Midori --//
  public int getInstitutionId(Document dom, SessionResourceKit srk) throws RemoteException, FinderException;


}
