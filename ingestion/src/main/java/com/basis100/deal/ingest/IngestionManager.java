package com.basis100.deal.ingest;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.util.file.IOUtil;
import com.basis100.util.file.PropertiesReader;
import com.basis100.workflow.notification.WorkflowNotifier;
import com.basis100.xml.DomUtil;
import com.basis100.xml.XmlToolKit;
import com.basis100.xml.XmlWriter;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.SourceFirmProfilePK;
import com.basis100.deal.security.DLM;
import com.basis100.log.SysLogger;
import com.filogix.externallinks.services.ExchangeFolderCreator;



import MosSystem.Mc;

import org.w3c.dom.*;
import javax.xml.parsers.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * The IngestionManager controls the overall ingestion process from mailbox <br/>
 * monitoring to workflow notification.
 * <pre>
 * The Ingestion Manager may call one or more of the methods in the <code>IngestionHandler</code>
 * interface as follows:
 * -  preProcess
 * -  storeDocument
 * -  processResult
 * -  notifyWorkflow
 * -  logSuccess
 * -  logFailure
 *--DocCentral_FolderCreate--21Sep--2004--start--//
 * -  logDocCentralFolderCreation
 *--DocCentral_FolderCreate--21Sep--2004--end--//
 * </pre>
 */
public class IngestionManager implements Runnable
{
    // logger.
    private static final Log _log = LogFactory.getLog(IngestionManager.class);

  private FileReciever queueOwner;
  private Map monitormap;
  private Map namemap;
  private Map directoryInfoCache;
  private List monitors;
  private Thread engine;
  private SysLogger logger;
  private WorkflowNotifier workflowNotifier;
  private CalcMonitor calcMonitor;
  public  SessionResourceKit srk;
  private PropertiesReader propertiesReader;
  private PropertiesCache resources;
  private String notifierHost;
  private int notifierPort;

  public static String ingestionHome = "";

  private int count = 0;

  private static boolean debug = true;

  //public static String MONITOR_FILE = "/application.xml";
  //public static String MONITOR_FILE = "/" + PropertiesCache.getInstance().getProperty("com.basis100.application.xml.name","application.xml");

  public static String MONITOR_NODE = "Monitor";

    /**
     * the contractor is for unit test.
     */
    public IngestionManager() throws FileIngestionException{
        init();
    }

 /**
  * Constructs an IngestionManager indicating wether a heartbeat is to be issued to
  * the SysMon daemon
  * @param heartbeat true if the ingestor is to issue a heartbeat to the Sysmon daemon
  */
  public IngestionManager(boolean heartbeat) throws FileIngestionException
  {
    this(null,heartbeat);
  }

 /**
  * @param initpath  the path of the configuration file pointing to resources
  * @param heartbeat true if the ingestor is to issue a heartbeat to the Sysmon daemon
  */
  public IngestionManager(String initpath, boolean heartbeat) throws FileIngestionException
  {
      if(heartbeat)
      {
        HeartBeatProxy heartBeat = new HeartBeatProxy();
      }

      try
      {
        this.init();
        this.engine = new Thread(this, this.getClass().getName());
        this.engine.start();
      }
      catch(Exception starter)
      {
        starter.printStackTrace();
      }
  }

 /**
  *   initializes ResourceManager, Properties, and Picklist resources
  *   @param the location of the mosys.properties file
  */
  private void init() throws FileIngestionException
  {
      resources = PropertiesCache.getInstance();

      try {

          logger = ResourceManager.getSysLogger("IG");
          logger.info("--> IngestionManager@init ");
          logger.info("--> java.library.path = <"+ System.getProperty("java.library.path") + ">");

          try {

              logger.info("--> Initialize Resource Manager");
              ResourceManager.init();
          }
          catch (Exception exc) {
              System.err.println(exc);
              logger.error(exc);
          }

          PicklistData.init();
          new BXResources();

          srk = new SessionResourceKit("Ingestion");
          srk.setAuditOn(false);

          logger.debug("INGESTION: Recieving output from Ingestion Manager...");

          this.workflowNotifier = new WorkflowNotifier(srk);
          //create directory Monitors....
          logger.debug("INGESTION: Attempting to create monitors...");
    
          //ZIVKO:NOTE: initialize DLM
          logger.debug("INGESTION: Initializing DealLockMonitor.");
          DLM.init(this.srk,false);
          logger.debug("INGESTION: DealLockMonitor initialized.");
          //TODO admin folder lookup to be changed later
          File f = new File("admin");
          String initHome = f.getAbsolutePath();
          this.createMonitors(initHome);
          //set file reciever with the monitors
          this.queueOwner = new FileReciever(monitormap.values());
          this.calcMonitor = CalcMonitor.getMonitor(srk);
          logger.debug("INGESTION: Ingestion Manager Initialized.");

    }
    catch(Exception e)
    {
      String msg = "Error occured while initializing ingestion. ";
      if(e.getMessage() != null)
         msg += "Reason: " + e.getMessage();
      else
         e.printStackTrace();
      throw new FileIngestionException(msg);
    }

  }

 /**
  *   Creates the directory monitors.  Reads the properties file to determine the
  *  the location of directories to be monitored. Each monitor has an associated
  *  directory, archive and file extension. In other words directory x can be set up
  *  to be monitored for files ending in y. Archival of failed or unacceptable files
  *  can be done to z.
  */
  private void createMonitors(String inithome) throws FileIngestionException,IOException
  {

    monitormap = new HashMap(17);
    namemap = new HashMap(17);

    logger.debug("INGESTION: createMonitors: Reading properties...");

    Document dom;

    try
    {
      logger.trace("INGESTION: createMonitors Parse Begins... ");
      DocumentBuilder parser = XmlToolKit.getInstance().newDocumentBuilder();
      dom =  parser.parse(ClassLoader.getSystemResourceAsStream("application.xml"));
      logger.trace("INGESTION: createMonitors Parse Complete... ");
    }
    catch(Exception parse)
    {
      String msg =  "INGESTION: Error Parsing XML input: " + parse.getMessage();

      logger.error(msg);
      System.out.println(msg);
      logger.error(parse);
      throw new FileIngestionException(msg);
    }

    Element ingestionElement = DomUtil.getFirstNamedDescendant(dom,"ingestion");
    String inghome = ingestionElement.getAttribute("home");

    if(inghome == null || inghome.length() == 0)
     ingestionHome = inithome;
    else
     ingestionHome = inghome;

    if(!ingestionHome.endsWith("\\") || !ingestionHome.endsWith("/"))
     ingestionHome += "/";

    List nodes = DomUtil.getNamedDescendants(dom, MONITOR_NODE);

    Iterator it = nodes.iterator();
    Element current = null;
    String  name    = null;

    //search for monitor keys
    while(it.hasNext())
    {
       current = (Element)it.next();

     //no two monitors should have the same name.
     //this is for future use of in process monitor configuration
     //change (ingestion admin interface) using
     //manager.getMonitorByName  + mon.reinit() not implemented
       name = current.getAttribute("name");
       DirectoryMonitor mon = null;

       try
       {
          mon = new DirectoryMonitor(current,name);

          if(!namemap.containsKey(name))
          {
            namemap.put(name, mon);
            monitormap.put(mon.getInboundDirectory().getAbsolutePath(),mon);
          }
          else
            throw new Exception("Duplicate Monitor Name");

         //for use by getMonitorByName
       }
       catch(Exception e)
       {
          String msg = "INGESTION: ERROR: Unable to create monitor: " + name;
          logger.error(msg);
          System.out.println(msg);

          msg = e.getMessage();

          if(msg == null) msg = e.getClass().getName();

          msg = "INGESTION: ERROR: " + msg;
          logger.error(msg);
          System.out.println(msg);

       }

     }

  }


 /**
  *  recieves files from the ingestion queue
  */
  public void run()
  {

     try
     {
        while(true)
        {
          File currentFile = queueOwner.getFromQueue();

          try
          {
            long startTime = System.currentTimeMillis();

            this.ingest(currentFile);

            System.out.println("INGESTION: Total Time (millisec): " + (System.currentTimeMillis() - startTime) );
            logger.info("INGESTION: Total Time (millisec): " + (System.currentTimeMillis() - startTime));
          }
          catch(Exception e)
          {
            String reason = e.getMessage();

            if(reason == null) reason = "Unknown error.";

            String msg = "INGESTION: IngestionManager:Uncaught Ingestion Exception - msg: "
                          + reason;
            logger.error(msg);
            System.out.println(msg);
            logger.error(e);
          }
        }
     }
     catch(InterruptedException ie)
     {
         String msg = "INGESTION: IngestionManager: Ingestion thread failure";
         logger.error(msg);
         System.out.println(msg);
         ie.printStackTrace();
     }

  }

 /**
  *  Ingests a deal file
  *  @param file: the file to be ingested
  */
  public void ingest(File file)
  {
    Document dom = null;
    IngestionResult result = null;
   //get monitor details
    String ownerDirectory = null;
    DirectoryMonitor mon = null;
    IngestionHandler handler = null;

    String failed = null;
    String failext = null;

    //boolean rollback = false;

    _log.info("INGESTION: Ingest file: " + file.getAbsolutePath());

    System.out.println( "INGESTION: Count Since Init: " + ++count );
    if (_log.isDebugEnabled())
        _log.debug( "INGESTION: Count Since Init: " + ++count );

    try
    {
    //get monitor details
      ownerDirectory = (file.getParentFile()).getParent();

      if (_log.isDebugEnabled())
          _log.debug("INGESTION: get monitor details: " + ownerDirectory);

      mon = (DirectoryMonitor)monitormap.get(ownerDirectory);
      handler = mon.getHandler();
      if (_log.isDebugEnabled())
          _log.debug("Get the handler: " + handler.toString());
      failed = mon.getErrorDirectory().getAbsolutePath();
      failext = mon.getErrorExtension();

      //ask the monitor to archive the file.
      try
      {
        mon.performArchive(file);
      }
      catch(IOException io)
      {
        String msg = io.getMessage();

        String filename = null;

        if(file == null)
         filename = "Unknown file name.";
        else
         filename = file.getAbsolutePath();

        if( msg == null) msg = "Unknown Error";

        msg = "INGESTION: Failed to archive file: " + filename +
              " Reason: " + msg;

        System.out.println(msg);
        logger.error(msg);
        logger.error(io);
      }


    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if( msg == null) msg = "Unknown Error";

      msg = "INGESTION: Failure obtaining monitor parameters: " + msg;
      System.out.println(msg);
      logger.error(msg);
      logger.error(e);
      return;
    }

    String cbs = getCurrentBuildString(file.getName());
    logger.info(cbs);
    _log.info(cbs);
    System.out.println(cbs);

    try
    {
      file = handler.preProcess(file);

      if(file == null)
       logger.error("INGESTION: Inbound file is null after pre-processing");
    }
    catch(Exception e)
    {
      String msg =  "INGESTION: Error PreProcessing InboundFile: " + file.getAbsoluteFile();

      logger.error(msg);
      System.out.println(msg);

      writeFailed(file, null, null, failed,failext);
      return;

    }

    // SECTION 1 - PARSE (the input file)
    try
    {
      logger.trace("INGESTION: XML Parse Begins... ");
      DocumentBuilder parser = XmlToolKit.getInstance().newDocumentBuilder();
      _log.info("Got XML Parser: " + parser.toString());
      dom =  parser.parse(file);
      logger.trace("INGESTION: XML Parse Complete... ");
    }
    catch(Exception parse)
    {
      String msg =  "INGESTION: Error Parsing XML input: " + parse.getMessage();

      writeFailed(file, null, null, failed,failext);

      logger.error(msg);
      System.out.println(msg);
      logger.error(parse);

      _log.error(msg, parse);
      return;
    }

    // SECTION 2 - store the deal
    try
    {
      //clear vpd
      srk.getExpressState().cleanAllIds();
      
      //added for ML - START July24, 2007 by Midori
      int institutionId = handler.getInstitutionId(dom, srk);
      logger.trace("INGESTION: Begin Transaction. ");

      srk.getExpressState().setDealInstitutionId(institutionId);
      //added for ML - END July24, 2007 by Midori

      srk.beginTransaction();

      //srk.getJdbcExecutor().startDBPerformanceCheck();

      result = handler.storeDocument(dom, srk, mon);

      if(result == null)
      {
        throw new Exception("Ingestion attempted: No result obtained");
      }

      if( result.isDealLocked() ){
        //ZIVKO:NOTE here perform the action if the deal is locked
        logger.info("INGESTION: deal has been locked - sending to locked folder.");
        mon.sendToLockedFolder(file);
        logger.info("INGESTION: deal has been locked - sent to locked folder.");
        srk.rollbackTransaction();
        srk.freeResources();
        return;
      }

      logResult(result);
    }
    catch(Exception e)
    {
      String msg =  e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg +=  "INGESTION: Uncaught failure during ingestion: " + msg;

      logger.error(msg);
      logger.error(e);
      result = new IngestionResult();
      result.isApplicationError(true);
    }

    int action = 0;

    try
    {
      action = handler.processResult(result, file, dom, mon);
    }
    catch(Exception e)
    {
      String msg =  e.getMessage();

      if(msg == null) msg = "Unknown Error";

      msg =  "INGESTION: Uncaught failure during result processing: " + msg;

      logger.error(msg);
      logger.error(e);
    }

    try
    {
      if(action == IngestionHandler.ROLLBACK)
      {

        System.out.println("INGESTION: Rolling back database updates.");
        logger.debug("INGESTION: Rolling back database updates.");
        file.delete();

        srk.rollbackTransaction();
        this.unlockDeal(srk,result,false);
        srk.freeResources();
        return;
      }
      else if(action == IngestionHandler.STORE)
      {
          System.out.println("INGESTION: Commiting database updates. ");
          logger.trace("INGESTION: Commiting database updates. ");

        srk.commitTransaction();

        file.delete();
        this.unlockDeal(srk,result,false);
      }
    }
    catch(Exception ex)
    {
       String msg = ex.getMessage();
       if(msg == null) msg = "Unknown Error";

       msg =  "INGESTION: Commit or rollback failed!!!!!!!!!! Reason: " + msg;

       logger.error(ex);
       logger.error(msg);

       System.out.println(msg);

       return;
    }

    /*Commented the folder creation at ingestion and 
     * moved it to MosSystemServlet.java
     * Ref Ticket # FXP25270 - after the assignment of users.Begin
     */
    
    /*
    //------ 2008-12-23 Exchange Folder Creation Start ---- 
    // Section 3 - Create Deal Folder in Exchange 2.0 if not exist yet.
    logger.trace("INGESTION: Create Deal Folder in Exchange 2.0.");

    try

    {
    	ExchangeFolderCreator dci = new ExchangeFolderCreator(srk);
    	int folderIndicator = dci.getFolderCreationIndicator();

    	logger.debug("INGESTION: folderIndicator: " + folderIndicator);

      if (folderIndicator == Mc.DO_NOT_CREATE_DOC_CENTRAL_DEAL_FOLDER ||
          folderIndicator == Mc.CREATE_DOC_CENTRAL_DEAL_FOLDER_ON_APPROVAL)
      {
        // do nothing, in the second case folder creation is postponed until deal approval.
        logger.trace("INGESTION: Do not create Doc Central Deal folder");
      }
      else if (folderIndicator == Mc.CREATE_DOC_CENTRAL_DEAL_FOLDER_ON_SUBMISSION)
      {
        // Check first whether the folder for this deal is created or not.
        logger.trace("INGESTION: Create Doc Central Deal folder");

        Deal deal = null;
        IEntityBeanPK beanPK = result.getResultEntity();

        if (beanPK instanceof DealPK) {
          int dealid = ( (DealPK) beanPK).getId();
          int copyid = ( (DealPK) beanPK).getCopyId();
          ////int dealid = result.getLockedDealId();
          logger.debug("INGESTION: dealidToExchange2: " + dealid);
          logger.debug("INGESTION: copyidToExchange2: " + copyid);

          deal = new Deal(srk, null, dealid, copyid);

          // Just sanity check, it should not be here at the moment.
          String folderIdentifier = "";

          logger.debug("INGESTION: DealFoundId: " + deal.getDealId());
          logger.debug("INGESTION: DealFoundCopyId: " + deal.getCopyId());
          logger.debug("INGESTION: DealFoundCopyType: " + deal.getCopyType());
          logger.debug("INGESTION: FolderExists?: " + dci.folderExists(deal));


          if (!dci.folderExists(deal)) {
            folderIdentifier = dci.createFolder(deal);

            String sfpIdentifier = deal.getSourceFirmProfile().
              getSourceFirmName();
            logger.debug("INGESTION: sobIdentifierToDocCentral: " +
                         sfpIdentifier);

            int sfpId = deal.getSourceFirmProfileId();

            SourceFirmProfile SFPBean =
              new SourceFirmProfile(srk).findByPrimaryKey(
              new SourceFirmProfilePK(sfpId));

            logger.debug("INGESTION: sobIdentifierToDocCentralFromBean: " +
                         SFPBean.getSourceFirmName());

            if (folderIdentifier != null && !folderIdentifier.trim().equals("")) {
              handler.logDocCentralFolderCreation(srk,
                                                  beanPK,
                                                  SFPBean,
                                                  folderIdentifier);
            }
          }
        }
      }
    }
    catch(Exception ex)
    {
      String msg = "INGESTION:  NOTE: Failed to create folder "+
                   " Deal: " + result.getResultEntity().getId();
      logger.error(msg);
      //// Add this function
      ////handler.logDocCentralFolderFailure(srk, beanPK, SFPBean, folderIdentifier);
    }
    //--DocCentral_FolderCreate--21Sep--2004--end--//
    */
    // Ref Ticket # FXP25270 - after the assignment of users - END

    //SECTION 4 - NOTIFY WORKFLOW
    try
    {
      srk.beginTransaction();

      if(!result.isNotificationExcused())
      {
        logger.trace("INGESTION: Notify Workflow.");

        handler.notifyWorkflow(workflowNotifier,result);

        System.out.println( "INGESTION: Notified Workflow.");
        logger.debug("INGESTION: Notified Workflow.");
      }
      else
      {
        logger.trace("INGESTION: Workflow notification excused...");
        System.out.println( "INGESTION: Workflow notification excused...");
      }

      handler.logSuccess(srk,result.getResultEntity(),file.getAbsolutePath());

      srk.commitTransaction();
    }
    catch(Exception eeww)
    {
      try{srk.rollbackTransaction();}catch(Exception ex){;}
      String msg = "INGESTION:  NOTE: Failed to notify workflow of "+
                   " Deal: " + result.getResultEntity().getId() + " arrival.";
      logger.error(msg);
      handler.logFailure(srk,result.getResultEntity(),file.getAbsolutePath());
    }

    try
    {
      handler = null;
      //long totalTimeForDB = srk.getJdbcExecutor().stopDBPerformanceCheck();
      //long totalSQLCount = srk.getJdbcExecutor().getDBCount();
      //logger.info("INGESTION:DBPerformanceStatistics:TotalTimeForDBInMSec: " + totalTimeForDB);
      //logger.info("INGESTION:DBPerformanceStatistics:TotalSQLCount: " + totalSQLCount);
      srk.cleanTransaction();
      srk.freeResources();
      System.out.println("INGESTION: Clean Transaction and Free Resources.");
      logger.trace("INGESTION: Clean Transaction and Free Resources.");
    }
    catch(Exception e)
    {
      String msg = "INGESTION: Failed to clean transaction and free resources.";
      logger.error(msg);
    }

    logger.trace("INGESTION: Complete for: " + file.getName());

    String endmsg = "INGESTION: " + result.entityString();
    System.out.println(endmsg);
    logger.trace(endmsg);
 }

  private void unlockDeal(SessionResourceKit srk, IngestionResult result, boolean inTransaction) throws Exception
  {
    IEntityBeanPK beanPK = result.getResultEntity();

    if(beanPK instanceof DealPK){
      //int dealid = ((DealPK) beanPK).getId();
      int dealid = result.getLockedDealId();
      DLM.getInstance().unlock(dealid,0,srk,inTransaction);
    }
  }


 private void addIngestionInfo(Document dom, IngestionResult result)
 {
     System.out.println("INGESTION: Update file attributes.");
    logger.trace("INGESTION: Update file attributes.");
   Element docElement = (Element)DomUtil.getFirstNamedDescendant(dom,"Document");

   if(docElement == null)
   {
       System.out.println("INGESTION: Update file attributes failed.");
       logger.trace("INGESTION: Update file attributes failed.");
     return;
   }

   Date d = new Date();

   docElement.setAttribute("ingestedDate", d.toString());

   docElement.setAttribute("ingestedStatus", "rolledback");

   String count = docElement.getAttribute("count");
   int countval = 0;

   if(count == null || count.length() == 0)
    count = "1";
   else
   {
     try
     {
        countval = Integer.parseInt(count);
        countval++;
        docElement.setAttribute("count", String.valueOf(countval));
     }
     catch(Exception e)
     {
        ;//do nothing
     }
   }

 }

  public boolean writeFailed(File recieved, Document dom, IngestionResult result, String location, String ext)
  {

    if(dom == null)
    {

      try
      {
        File dest = new File(location);
        System.out.println("INGESTION: Archiving Deal File to: " + dest.getAbsolutePath());
        logger.trace("INGESTION: Archiving Deal File to: " + dest.getAbsolutePath());
        
        IOUtil.copyAndDelete(recieved , new File(location), ext) ;
        return true;
      }
      catch(Exception e)
      {
        e.printStackTrace();
        return false;
      }
    }
    else
    {
      try
      {
         addIngestionInfo(dom, result);

         String name = recieved.getName();

         name = IOUtil.changeExtension(name,ext);

         File dest = new File(location + "/" + name);

         XmlWriter writer = XmlToolKit.getInstance().getXmlWriter();
         String val = writer.printFormattedString(dom);

         FileWriter fwr = new FileWriter(dest);
         fwr.write(val);
         fwr.flush();
         fwr.close();

      }
      catch(Exception e)
      {
        e.printStackTrace();
        return false;
      }
    }

     return true;
  }

  private void logResult(IngestionResult result)
  {
    String msg = "INGESTION: ";

    List mlist = result.getMessages();

    Iterator it = mlist.iterator();

    while(it.hasNext())
    {
      Object m = it.next();

      logger.info(msg + m);
    }

    mlist = result.getICRulesMessages();
    it = mlist.iterator();
    while( it.hasNext() ){
      Object m = it.next();
      logger.info(msg + m);
    }
  }

  public SourceMonitor getMonitor(String name)
  {
    return (SourceMonitor)namemap.get(name);
  }

  public static String getIngestionHome()
  {
    return ingestionHome;
  }

  public static String getCurrentBuildString(String filename)
  {

    java.util.Date today = new java.util.Date();

    String str = "\n___________________________________________________________________\n\n";

    if(filename == null)
    str += "\tIngestion Engine Running. ";
    else
    str += "\tIngesting File: " + filename;

    str += "\n\tBuild 9.6.0 Patch 6:Generic  ::" + today;
    str += "\n_______________________________________________________________________\n";


    return str;

  }

}
