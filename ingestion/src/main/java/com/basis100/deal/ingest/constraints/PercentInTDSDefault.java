package com.basis100.deal.ingest.constraints;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.Income;
import com.basis100.deal.entity.IncomeType;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.LiabilityType;
import com.basis100.deal.entity.PropertyExpense;
import com.basis100.deal.entity.PropertyExpenseType;
import com.basis100.deal.xml.EntityNodeAssociation;
import com.basis100.resources.SessionResourceKit;


public class PercentInTDSDefault implements DefaultConstraint
{

  public void execute(SessionResourceKit srk, EntityNodeAssociation ass) throws Exception
  {
    DealEntity de = ass.getEntity();
    try
    {
      String cls = de.getClass().getName();

      if(de.getClassId() == ClassId.LIABILITY)
      {
         if(!ass.isSetField("percentInTDS"))
         {
           Liability li = (Liability)de;
           int type = li.getLiabilityTypeId();

           LiabilityType liType = new LiabilityType(srk,type);
           li.setPercentInTDS(liType.getTDSInclusion());
         }

      }
      else if(de.getClassId() == ClassId.INCOME)
      {
         if(!ass.isSetField("incPercentInTDS"))
         {
           Income in = (Income)de;
           int type = in.getIncomeTypeId();
           IncomeType inType = new IncomeType(srk,type);

           in.setIncPercentInTDS(inType.getTDSInclusion());
         }

      }
      else if(de.getClassId() == ClassId.PROPERTYEXPENSE)
      {
         if(!ass.isSetField("pePercentInTDS"))
         {
            PropertyExpense pe = (PropertyExpense)de;
            int type = pe.getPropertyExpenseTypeId();

            PropertyExpenseType peType = new PropertyExpenseType(srk,type);

            pe.setPePercentInTDS(peType.getTDSInclusion());
         }

      }
    }
    catch(Exception e)
    {
       throw new Exception("Default Value not found for " + this.getClass().getName());
    }
    // zivko removed this exception from code because it is not clear why we
    // are throwing exception every time we enter this class and
    // SUCCESSFULY finish processing
    // removed on 28-XI-2003
    //throw new Exception("Default Value not found for " + this.getClass().getName());
  }

}
