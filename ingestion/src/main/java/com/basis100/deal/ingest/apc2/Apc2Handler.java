package com.basis100.deal.ingest.apc2;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import com.basis100.deal.ingest.DirectoryMonitor;
import com.basis100.deal.ingest.IngestionHandler;
import com.basis100.deal.ingest.IngestionResult;
import com.basis100.deal.ingest.SourceMonitor;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.resources.SessionResourceKit;
import com.basis100.util.file.IOUtil;
import com.basis100.workflow.notification.*;
import com.basis100.xml.DomUtil;
import com.basis100.xml.IniFileConverter;
import com.basis100.xml.XmlToolKit;
import com.basis100.xml.XmlWriter;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.file.apc2.APC2Component;
import com.basis100.deal.file.apc2.APC2Field;
import com.basis100.deal.file.apc2.APC2File;
import com.basis100.deal.file.apc2.APC2Object;
import com.basis100.deal.file.apc2.APC2Util;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;


/**
 *  The Apc2Handler handles ini style files from mapping by converting them and
 *  placing them in an outbound mailbox.
 *  The IngestionManager expects to call the required
 *  methods. <br>Methods that do not augment the ingestion process implemented can implement
 *  empty methods. An IngestionHandler implementation must provide a no-args constructor.
 *
 */
public class Apc2Handler implements IngestionHandler
{
  public File preProcess(File file)throws Exception
  {
    IniFileConverter con = new IniFileConverter();
    Document doc = con.convert(file, "Document");

    XmlWriter wr = XmlToolKit.getInstance().getXmlWriter();
    FileWriter fwr = new FileWriter(file);
    fwr.write(wr.printString(doc));
    fwr.flush();
    fwr.close();

    return file;
  }


  /**
   *  Write the provided DOM tree to the target schema for this ingestion
   *  implementation and return a Result object.
   */
  public IngestionResult storeDocument(org.w3c.dom.Document dom, SessionResourceKit srk, SourceMonitor source )
                                  throws Exception
  {


    IngestionResult ir = new IngestionResult();


    return ir;
  }


  public void notifyWorkflow(WorkflowNotifier notifier, IngestionResult parResult)
  throws Exception
  {
    ;
  }

  public void logSuccess(SessionResourceKit srk, IEntityBeanPK deal, String filename)
  {
   ;
  }

  public void logFailure(SessionResourceKit srk, IEntityBeanPK deal, String filename)
  {
   ;
  }

  //--DocCentral_FolderCreate--21Sep--2004--start--//
  public void logDocCentralFolderCreation(SessionResourceKit srk,
                                          IEntityBeanPK pk,
                                          SourceFirmProfile sourceFirmEntity,
                                          String folderIdentifier)
  {
    ;
  }
  //--DocCentral_FolderCreate--21Sep--2004--end--//

  public int processResult(IngestionResult res, File input, Document dom, SourceMonitor mailbox) throws Exception
  {
    // convert dom to apc2 format

    List objects = new ArrayList(createObjects(dom));
    APC2File file = new APC2File();

    DirectoryMonitor inbox = (DirectoryMonitor)mailbox;

    ListIterator li = objects.listIterator();

    while(li.hasNext())
    {
      file.add((APC2Object)li.next());
    }

    String out = mailbox.getOutputLocation();
    File dir =  new File(out);

    String filename = input.getName();

    FileOutputStream str = IOUtil.constructSafeFOS(dir,filename,inbox.getOutboundExtension());

    OutputStreamWriter osw = new OutputStreamWriter(str);
    osw.write(file.toAPC2String());
    osw.flush();
    osw.close();


    return STORE;
  }

  private Collection createObjects(Document dom)throws Exception
  {

    Node n = DomUtil.getFirstNamedChild(dom, "Document");

    List sectionElements = DomUtil.getNamedChildren(n, "Section");
    Element currentSection = null;
    Set checkSet = new HashSet(17);
    Map objMap = new HashMap(17);
    String sectionName = null;


    Iterator sit = sectionElements.iterator();

    while(sit.hasNext())
    {
      currentSection = (Element)sit.next();
      sectionName = currentSection.getAttribute("name");

      String oName = APC2Util.parseObjectName(sectionName);
      String cName = APC2Util.parseComponentName(sectionName);

      if(checkSet.add(oName))
      {
        objMap.put(oName,new APC2Object(oName));
      }

      List fields = createFields(currentSection);
      APC2Object curObj = (APC2Object)objMap.get(oName);
      curObj.add(new APC2Component(cName,fields));

    }

     return objMap.values();
  }

  private List createFields(Element container)
  {
    List fields = new ArrayList();

    List fieldElements = DomUtil.getChildElements(container);
    Iterator it = fieldElements.iterator();

    String fieldname = null;
    String value = null;
    Element current = null;

    while(it.hasNext())
    {
      current = (Element)it.next();

      fieldname = current.getAttribute("name");
      value = current.getAttribute("value");

      fields.add(new APC2Field(fieldname,value));

      fieldname = null;
      value = null;
    }

    return fields;
  }

  public int getInstitutionId(Document dom, SessionResourceKit srk) throws RemoteException, FinderException
  {
    return -1;
  }
}
