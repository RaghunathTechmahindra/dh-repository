package com.basis100.deal.ingest.application;

/**
 * 08/Sep/2005 DVG #DG310 #2052  CV E2E Ingestion Failure
 *     list exception details
 */


import MosSystem.Mc;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.validation.PagelessBusinessRuleExecutor;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;


public class ApplicationValidator
{
  private SessionResourceKit srk;
  boolean off = false;
  SysLogger logger;

  public ApplicationValidator()
  {
     //for test only
  }

  public ApplicationValidator(SessionResourceKit srk)throws Exception
  {
      logger =  srk.getSysLogger();
      this.srk = srk;
      init();
  }


  public void init()throws Exception
  {
    String val = PropertiesCache.getInstance().getProperty(
            srk.getExpressState().getDealInstitutionId(),
            "temp.ingestion.runrules", "Y");

    if(val == null || !val.equalsIgnoreCase("Y"))
    {
     this.off  = true;
     this.logger.info("INGESTION: ApplicationValidator (IC Rules): OFF");
     System.out.println("INGESTION: ApplicationValidator (IC Rules): OFF");
    }
    else
    {
      this.logger.info("INGESTION: ApplicationValidator (IC Rules): ON");
      System.out.println("INGESTION: ApplicationValidator (IC Rules): ON");
    }

  }

  // simplified - Sep 25, 2007
  public PassiveMessage validate(Deal deal, SessionResourceKit srk) throws Exception
  {
    return validate(deal, srk, Mc.LANGUAGE_PREFERENCE_ENGLISH);
  }

  public PassiveMessage validate(Deal deal, SessionResourceKit srk, int pLang) throws Exception
  {

    //String rulePrefix = null;
    PassiveMessage pm = new PassiveMessage();
    if(off) return pm;

    try
    {
        // changed for ML
      PagelessBusinessRuleExecutor bre = new PagelessBusinessRuleExecutor();
      pm = bre.validate(deal,srk,pm,"IC-%", pLang);

    }
    catch (Exception e)
    {
      SysLogger logger = srk.getSysLogger();

      logger.error("INGESTION: ERROR: While validating ingested deal: "+ StringUtil.stack2string(e));  //#DG310
      //e.printStackTrace();

      throw new Exception("Business Rule Engine exception", e);  //#DG310
    }


    return pm;
  }


}
