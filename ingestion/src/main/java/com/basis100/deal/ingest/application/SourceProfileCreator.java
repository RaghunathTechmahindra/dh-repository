package com.basis100.deal.ingest.application;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import MosSystem.Sc;
import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.entity.SourceOfBusinessProfile;
import com.basis100.log.SysLogger;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.xml.DomUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class SourceProfileCreator
{

  private SessionResourceKit srk;
  private String[] sobFieldList = {"provinceId","addressLine1","addressLine2","city",
                                   "postalFSA", "postalLDU","contactFirstName","contactLastName",
                                   "contactPhoneNumber","contactFaxNumber","contactPhoneNumberExtension",
                                   "SOBPShortName", "contactEmailAddress", "SOBLicenseRegistrationNumber"}; // FXP24946  for 4.2GR

  private String[] sfpFieldList = { "sourceFirmName","sfShortName","provinceId","addressLine1","addressLine2","city","postalFSA", "postalLDU", 
          "SFLicenseRegistrationNumber"};

  private SysLogger logger;
  public SourceProfileCreator(SessionResourceKit kit)
  {
    srk = kit;
    logger = srk.getSysLogger();
  }

/* deleted commented out mthod - Midori Sep 11, 2007
 * public SourceOfBusinessProfile createSOBProfile(Deal deal, Document dom)throws Exception
*/
  /**
   *  This method will modify sob from node updating all fields found in the node.
   */
  private void updateSOBFromNode(SourceOfBusinessProfile sob, Node sobNode, String pListName, String target )throws Exception
  {

    if((sob == null) || !sobNode.getNodeName().equals(target))
     return;

    //clean existing SOB fields so that they can be overwritten with null if necessary... FXP30305
    sob.setSOBPShortName("");
    sob.setSobLicenseRegistrationNumber("");
    
    NodeList nl = sobNode.getChildNodes();

    for(int i = 0; i < nl.getLength(); i++)
    {
      Node current = nl.item(i);

      String field = current.getNodeName();

      if(field.equals("Contact"))
      {
        readContact(sob.getContact(), current, pListName);
      }
      else
      {
        if(isFieldInTheList(field, pListName)){
          setField(sob,field,current);
        }
      }
    }

  }


  private void readContact(Contact contact, Node node, String pListName) throws Exception
  {
    //String val = "";
    boolean shouldUpdateEmail = PropertiesCache.getInstance().getProperty(
            srk.getExpressState().getDealInstitutionId(),
            "com.filogix.ingestion.sourceofbusinessprofile.brokeremail.update", 
            "N").equalsIgnoreCase( "Y" );
    
    if(!node.getNodeName().equals("Contact")) return ;

    NodeList nl = node.getChildNodes();
    int size = nl.getLength();

	  /* Reset contact to blank because expert should be overwriting complete contact info.
	   * This is relevant due to FXP29304 where empty fields are being dropped prior to this,
	   * and so express is not emptying them.  Not blanking email due to special check below.
	   */
	  contact.setContactFirstName("");
	  contact.setContactMiddleInitial("");
	  contact.setContactLastName("");
	  contact.setContactJobTitle("");
	  contact.setContactPhoneNumber("");
	  contact.setContactPhoneNumberExtension("");
	  contact.setContactFaxNumber("");
	  
    for (int i = 0; i < size; i++)
    {
      Node current = nl.item(i);

      String field = current.getNodeName();

      if(field.equals("Addr"))
      {
        Addr add = contact.getAddr();
	  /* Reset contact to blank because expert should be overwriting complete contact info.
	   * This is relevant due to FXP29304 where empty fields are being dropped prior to this,
	   * and so express is not emptying them.
	   */        
        add.setAddressLine1("");
        add.setAddressLine2("");
        add.setCity("");
        add.setPostalFSA("");
        add.setPostalLDU("");
        add.setProvinceId(0);
        
        NodeList nol = current.getChildNodes();
        int nolsize = nol.getLength();

        for (int j = 0; j < nolsize; j++)
        {
          Node cnode = nol.item(j);
          String name = cnode.getNodeName();

          if( isFieldInTheList(name, pListName) == true){
            setField(add,name,cnode);
          }
        }
        add.ejbStore();
      }
      else
      {
        if(isFieldInTheList(field, pListName) == true)
        {
          // 13-Feb-2006 Susan Ticket 2666, add proper email address check
			if (field.equals("contactEmailAddress")) 
			{
				if (shouldUpdateEmail)
				{
					//regex email address check
				    String emailAddress = DomUtil.getNodeText(current);
				    Pattern pEmail = Pattern.compile("^[a-zA-Z][\\w\\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$");
				    Matcher mEmail = pEmail.matcher(emailAddress);
				    if (!mEmail.matches())
				    	logger.info("INGESTION: SourceProfileCreator: Invalid Email addresses : " + emailAddress );
				    else 
				    	setField(contact, field, current);
				}
			}
			else 
			{
            // 13-Feb-2006 Susan Ticket 2666, add proper email address check
				setField(contact, field, current);
			}
        }
      }

    }

    contact.ejbStore();
  }

//****** WARNING *****
// this method used SrouceTOFirmAssoc class 
// but it has not been completed.
// table name used in is wrong.
// deleted this method as well as classes
// deleted commented out mthod - Midori Sep 11, 2007
  
//  public void createSOBtoSFPAssociation(SourceFirmProfile pSFP,SourceOfBusinessProfile pSOB) throws Exception
//{
// ,,,,
//}
  
  /* deleted commented out mthod - Midori Sep 11, 2007
 * private SourceFirmProfile seekSFPOnMailBox(Deal deal) throws Exception
*/
  public SourceOfBusinessProfile createSOBProfile(Deal deal, Document pDoc,int pSystemTypeId, String target)throws Exception
  {

	  SourceOfBusinessProfile sob = new SourceOfBusinessProfile(srk);
    sob.create(sob.createPrimaryKey());

    sob.setSystemTypeId(pSystemTypeId);

    Node lSobNode = DomUtil.getFirstNamedDescendant(pDoc,target);
    if(lSobNode != null) updateSOBFromNode(sob, lSobNode, "NONE", target);

    String lStat = PropertiesCache.getInstance().getProperty(
            srk.getExpressState().getDealInstitutionId(),
            "com.basis100.ingestion.SOB.default.profilestatusid","0");
    if( isProfileStatusOk(lStat) ){
      int profStatus;
      try{
        profStatus = Integer.parseInt(lStat) ;
        sob.setProfileStatusId(profStatus);
      }catch(Exception exc){
        // blow up the ingestion process
        throw new Exception("Critical Stop: Invalid SourceOfBusinessProfile Status - a: " + lStat);
      }
    }else{
      // blow up the ingestion process
      throw new Exception("Critical Stop: Invalid SourceOfBusinessProfile Status - a: " + lStat);
    }


    sob.ejbStore();

    return sob;

  }

  public void createSourceFirmProfile(Node pNode) throws Exception
  {
    logger.debug("INGESTION: " + this.getClass().getName() + " Method createSourceFirmProfile entered.");
    SourceFirmProfile sfp = new SourceFirmProfile(srk);
    String lSystemTypeAsStr = DomUtil.getElementText(DomUtil.getFirstNamedDescendant(pNode,"systemTypeId"));
    String lSourceFirmCode = DomUtil.getElementText(DomUtil.getFirstNamedDescendant(pNode,"sourceFirmCode"));
    int lSystemType = Integer.parseInt(lSystemTypeAsStr);

    //srk.beginTransaction();
    sfp = sfp.findBySystemTypeAndSFCode(lSystemType, lSourceFirmCode);
    if(sfp == null){
      sfp = new SourceFirmProfile(srk);
      sfp.create(sfp.createPrimaryKey());
      generateProfileStatusProperty(sfp);
      modifySFPFromNode( sfp, pNode,"NONE");
      // only when we add new SFP we should do the following
      if(sfp.getSourceFirmName() == null && sfp.getSourceFirmCode() != null){
        sfp.setSourceFirmName(sfp.getSourceFirmCode());
      }
      if(sfp.getSfShortName() ==  null && sfp.getSourceFirmCode() !=  null){
        if(sfp.getSourceFirmCode().length() > 10){
          sfp.setSfShortName(sfp.getSourceFirmCode().substring(0,9));
        }else{
          sfp.setSfShortName(sfp.getSourceFirmCode());
        }
      }
    }else{
      modifySFPFromNode( sfp, pNode, "SFP_LIST" );
    }
    logger.debug("INGESTION: " + this.getClass().getName() + " Method createSourceFirmProfile CP - 1.");
    List sobList = DomUtil.getNamedChildren(pNode,"SourceOfBusinessProfile");
    Iterator lIt = sobList.iterator();
    SourceOfBusinessProfile lSobProfile ;
    String lSourceOfBusinessCode;
    Node lOneSobNode;
    while( lIt.hasNext() ){
      lOneSobNode = (Node)lIt.next();
      lSystemTypeAsStr = DomUtil.getElementText(DomUtil.getFirstNamedDescendant(lOneSobNode,"systemTypeId"));
      lSourceOfBusinessCode = DomUtil.getElementText(DomUtil.getFirstNamedDescendant(lOneSobNode,"sourceOfBusinessCode"));
      lSystemType = Integer.parseInt(lSystemTypeAsStr);
      lSobProfile = new SourceOfBusinessProfile(srk);
      // zivko:marker:sob
      //lSobProfile = lSobProfile.findBySystemTypeAndSOBCode(lSystemType,lSourceOfBusinessCode);
      lSobProfile = lSobProfile.findBySystemTypeSFPAndSOBCode(lSystemType, sfp.getSourceFirmProfileId(), lSourceOfBusinessCode);
      if( lSobProfile == null ){
         lSobProfile = new SourceOfBusinessProfile(srk);
         lSobProfile.create(lSobProfile.createPrimaryKey());
         generateProfileStatusProperty(lSobProfile);
         modifySOBFromNode(lSobProfile,lOneSobNode,"NONE", "SourceOfBusinessProfile");
         if(lSobProfile.getContact().getContactLastName() == null ||lSobProfile.getContact().getContactLastName().trim().equals("") ){
            lSobProfile.getContact().setContactLastName( lSobProfile.getSourceOfBusinessCode() );
         }
         if( ( lSobProfile.getSOBPShortName() == null || lSobProfile.getSOBPShortName().trim().equals("") )  && lSobProfile.getSourceOfBusinessCode() != null){
            if(lSobProfile.getSourceOfBusinessCode().length() > 10){
              lSobProfile.setSOBPShortName(lSobProfile.getSourceOfBusinessCode().substring(0,9));
            }else{
              lSobProfile.setSOBPShortName(lSobProfile.getSourceOfBusinessCode() );
            }
         }
         lSobProfile.setSourceFirmProfileId(sfp.getSourceFirmProfileId());
         lSobProfile.ejbStore();
      }else{
        modifySOBFromNode(lSobProfile,lOneSobNode,"SOB_LIST", "SourceOfBusinessProfile");
        lSobProfile.ejbStore();
      }
    }
    sfp.ejbStore();
    logger.debug("INGESTION: " + this.getClass().getName() + " Method createSourceFirmProfile finished.");
    //srk.commitTransaction();
  }

  private void generateProfileStatusProperty(SourceFirmProfile pSfp) throws Exception
  {
    logger.debug("INGESTION: " + this.getClass().getName() + " Method generateProfileStatusProperty(SFP) entered.");
    String lStat = PropertiesCache.getInstance().getProperty(
            srk.getExpressState().getDealInstitutionId(),
            "com.basis100.ingestion.SFP.default.profilestatusid","0");
    if( isProfileStatusOk(lStat) ){
      int profStatus;
      try{
        profStatus = Integer.parseInt(lStat) ;
        pSfp.setProfileStatusId(profStatus);
      }catch(Exception exc){
        // blow up the ingestion process
        throw new Exception("Critical Stop: Invalid SourceFirmProfile Status - a: " + lStat);
      }
    }else{
      // blow up the ingestion process
      throw new Exception("Critical Stop: Invalid SourceFirmProfile Status - a: " + lStat);
    }
    logger.debug("INGESTION: " + this.getClass().getName() + " Method generateProfileStatusProperty(SFP) finished.");
  }


  private void generateProfileStatusProperty(SourceOfBusinessProfile pSob) throws Exception
  {
    logger.debug("INGESTION: " + this.getClass().getName() + " Method generateProfileStatusProperty(SOB) entered.");
    String lStat = PropertiesCache.getInstance().getProperty(
            srk.getExpressState().getDealInstitutionId(),
            "com.basis100.ingestion.SOB.default.profilestatusid","0");
    if( isProfileStatusOk(lStat) ){
      int profStatus;
      try{
        profStatus = Integer.parseInt(lStat) ;
        pSob.setProfileStatusId(profStatus);
      }catch(Exception exc){
        // blow up the ingestion process
        throw new Exception("Critical Stop: Invalid SourceOfBusinessProfile Status - a: " + lStat);
      }
    }else{
      // blow up the ingestion process
      throw new Exception("Critical Stop: Invalid SourceOfBusinessProfile Status - a: " + lStat);
    }
    logger.debug("INGESTION: " + this.getClass().getName() + " Method generateProfileStatusProperty(SOB) finished.");
  }

  public SourceFirmProfile createSourceFirmProfile(Deal deal, Document pDoc)throws Exception
  {
    int systypeid = deal.getSystemTypeId();

    SourceFirmProfile sfp = new SourceFirmProfile(srk);
    sfp.create(sfp.createPrimaryKey());

    sfp.setSystemTypeId(systypeid);

    Node lSfpNode = DomUtil.getFirstNamedDescendant(pDoc,"SourceFirmProfile");
    if(lSfpNode != null) updateSFPFromNode(sfp, lSfpNode, "NONE");

    sfp.setProfileStatusId(Sc.PROFILE_STATUS_CAUTION);  // by default
    sfp.ejbStore();

    return sfp;

  }

  /**
   * Method for batch upload of source firm profiles /source of business profiles
   */
  private void modifySOBFromNode(SourceOfBusinessProfile pSob, Node pNode, String pListName, String target) throws Exception
  {
    logger.debug("INGESTION: " + this.getClass().getName() + " Method SourceOfBusinessProfile entered.");
    if( (pSob == null) ||  !pNode.getNodeName().equals(target)){
      return;
    }

    NodeList nl = pNode.getChildNodes();

    for(int i = 0; i < nl.getLength(); i++)
    {
      Node current = nl.item(i);

      String field = current.getNodeName();
      if(field.equals("Contact"))
      {
        readContact( pSob.getContact(), current, pListName );
      }
      else
      {
        if( isFieldInTheList(field,pListName) ){
          setField( pSob, field, current );
        }
      }
    }
    pSob.ejbStore();
    logger.debug("INGESTION: " + this.getClass().getName() + " Method SourceOfBusinessProfile finished.");
  }


  /**
   * Method for batch upload of source firm profiles /source of business profiles
   */
  private void modifySFPFromNode(SourceFirmProfile pSfp, Node pNode, String pListName) throws Exception
  {
    if((pSfp == null) || !pNode.getNodeName().equals("SourceFirmProfile"))
      return;

    NodeList nl = pNode.getChildNodes();

    for(int i = 0; i < nl.getLength(); i++)
    {
      Node current = nl.item(i);

      String field = current.getNodeName();
      if(field.equals("SourceOfBusinessProfile")){
        continue;
      }
      if(field.equals("Contact"))
      {
        readContact( pSfp.getContact(), current, pListName );
      }
      else
      {
        if( isFieldInTheList(field,pListName) ){
          setField( pSfp, field, current );
        }
      }
    }
    pSfp.ejbStore();
  }



  public void modifySFPFromNode(SourceFirmProfile pSfp, Document pDoc) throws Exception
  {
    Node lSfpNode = DomUtil.getFirstNamedDescendant(pDoc,"SourceFirmProfile");
    if(lSfpNode != null) updateSFPFromNode(pSfp, lSfpNode, "SFP_LIST");

    pSfp.ejbStore();
  }

  public void modifySOBFromNode(SourceOfBusinessProfile pSob, Document pDoc, String target) throws Exception
  {
    Node lSobNode = DomUtil.getFirstNamedDescendant(pDoc,target);
    if(lSobNode != null) updateSOBFromNode(pSob, lSobNode, "SOB_LIST", target);

    pSob.ejbStore();
  }

/* delted commented out method - midori Sep 11, 1007
  public SourceFirmProfile createSourceFirmProfile(Deal deal, SourceOfBusinessProfile sob, Document pDoc)throws Exception
*/

  private void updateSFPFromNode(SourceFirmProfile pSfp, Node pSfpNode,String pListName)throws Exception
  {

    if((pSfp == null) || !pSfpNode.getNodeName().equals("SourceFirmProfile"))
     return;
    
    //clean existing SF fields so that they can be overwritten with null if necessary... FXP30305
    pSfp.setSfLicenseRegistrationNumber("");
    pSfp.setSfShortName("");
    pSfp.setSourceFirmName("");
    
    NodeList nl = pSfpNode.getChildNodes();

    for(int i = 0; i < nl.getLength(); i++)
    {
      Node current = nl.item(i);

      String field = current.getNodeName();

      if(field.equals("Contact"))
      {
        readContact(pSfp.getContact(), current, pListName);
      }
      else
      {
          if(isFieldInTheList(field,pListName)){
            setField(pSfp,field,current);
          }
      }
    }

  }

  /* delted commented out method - midori Sep 11, 1007
  public SourceFirmProfile createSourceFirmProfile(Deal deal, SourceOfBusinessProfile sob)throws Exception
*/
  
  private void setField(DealEntity de, String field, Node value)throws Exception
  {
    if(value.getNodeType() != value.ELEMENT_NODE)
     return;

    String data = DomUtil.getNodeText(value);

    if(data != null && PicklistData.containsTable(field))
     {
        String id = PicklistData.getIdAsString(field,data);

        if(id != null) data = id;
        else data = "0";

        //boolean picklist = true;
        field = field + "Id";
     }

     if(data != null )
      de.setField(field, data);
  }

  private boolean isFieldInTheList(String pFName, String pListName){
    if(pListName.equals("NONE")){
      return true;
    }else if(pListName.equals("SOB_LIST")){
      for(int i=0; i<sobFieldList.length;i++){
        if(sobFieldList[i].equals(pFName)){
          return true;
        }
      }
      return false;
    }else if(pListName.equals("SFP_LIST")){
      for(int i=0; i<sfpFieldList.length;i++){
        if(sfpFieldList[i].equals(pFName)){
          return true;
        }
      }
      return false;
    }
    return true;

  }

  private boolean isProfileStatusOk(String pStat){

    String allowedProfileStatuses = PicklistData.getColumnValuesCDV("ProfileStatus","PROFILESTATUSID");

    if(pStat == null || allowedProfileStatuses == null || pStat.trim().length() == 0 || allowedProfileStatuses.trim().length() == 0 ){
      return false;
    }else{
      if(allowedProfileStatuses.indexOf(pStat) == -1){
        return false;
      } else {
        return true;
      }
    }
  }

}
