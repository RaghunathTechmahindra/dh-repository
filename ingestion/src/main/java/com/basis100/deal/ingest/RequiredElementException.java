package com.basis100.deal.ingest;

public class RequiredElementException extends FileIngestionException
{

 /**
  * Constructs a <code>FileIngestionException</code> with no specified detail message.
  */

  public RequiredElementException()
  {
	  super();
  }

 /**
  * Constructs a <code>FileIngestionException</code> with the specified message.
  * @param   message  the related message.
  */
  public RequiredElementException(String message)
  {
    super(message);
  }
}

