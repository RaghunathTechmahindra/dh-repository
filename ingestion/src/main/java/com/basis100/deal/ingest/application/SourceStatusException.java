package com.basis100.deal.ingest.application;

import com.basis100.deal.ingest.*;

public class SourceStatusException extends FileIngestionException
{

 /**
  * Constructs a <code>SourceStatusException</code> with no specified detail message.
  */
  public SourceStatusException()
  {
	  super();
  }

 /**
  * Constructs a <code>SourceStatusException</code> with the specified message.
  * @param   message  the related message.
  */
  public SourceStatusException(String message)
  {
    super(message);
  }
}

