package com.basis100.deal.ingest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Formattable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.duplicate.DealLockedException;
import com.basis100.deal.duplicate.DupeCheckProcessor;
import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.ArchivedLoanApplication;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentSummary;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DealNotes;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.entity.EmploymentHistory;
import com.basis100.deal.entity.EntityBuilder;
import com.basis100.deal.entity.IngestionQueue;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.MbmlRedirect;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.ingest.IngestionHandler;
import com.basis100.deal.ingest.IngestionResult;
import com.basis100.deal.ingest.application.ApplicationDefaults;
import com.basis100.deal.ingest.application.ApplicationLenderHandler;
import com.basis100.deal.ingest.application.ApplicationSourceHandler;
import com.basis100.deal.ingest.application.ApplicationValidator;
import com.basis100.deal.ingest.application.SourceDeterminationException;
import com.basis100.deal.ingest.application.SpecialProcessingHandler;
import com.basis100.deal.miprocess.MIProcessHandler;
import com.basis100.deal.pk.ComponentSummaryPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.ESBOutboundQueuePK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.SourceFirmProfilePK;
import com.basis100.deal.security.DLM;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.deal.validation.addressScrub.AddScrubConstants;
import com.basis100.deal.validation.addressScrub.AddScrubExecutor;
import com.basis100.deal.xml.EntityNodeAssociation;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLogger;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.util.message.ClientMessage;
import com.basis100.workflow.notification.WorkflowNotifier;
import com.basis100.xml.DomUtil;
import com.basis100.xml.XmlToolKit;
import com.basis100.xml.XmlWriter;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.externallinks.services.ExchangeFolderCreator;

import static com.filogix.util.Xc.*;

public final class FCXIngestionWorker {

	private FCXIngestionManager manager;
	private String sourceApplicationId;
	private Date receivedTimestamp;
	private String dealXml;
	private String senderChannel;
	private String receiverChannel;
	private boolean createDealNotesFlag;
	private String workflowNotificationType;

	private SessionResourceKit srk;
	private Logger logger;

	FCXIngestionWorker(FCXIngestionManager manager, String sourceApplicationId, Date receivedTimestamp, String dealXml, String senderChannel, String receiverChannel) {
		this.manager = manager;
		this.sourceApplicationId = sourceApplicationId;
		this.receivedTimestamp = receivedTimestamp;
		this.dealXml = dealXml;
		this.senderChannel = senderChannel;
		this.receiverChannel = receiverChannel;
		this.srk = new SessionResourceKit(String.valueOf(this.manager.getUserId()));
		this.logger = LoggerFactory.getLogger(this.getClass());

		try {
			this.createDealNotesFlag = "y".equalsIgnoreCase(PropertiesCache.getInstance().getProperty(-1, "com.filogix.ingestion.fcx.create.deal.notes"));
		} catch (Exception ignore) {
		}
		logger.info("com.filogix.ingestion.fcx.create.deal.notes = " + (createDealNotesFlag ? "Y" : "N"));

		workflowNotificationType = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), COM_FILOGIX_INGESTION_WORKFLOW_NOTIFICATION_TYPE, "DEFAULT");
	}

	/**
	 *
	 * This is the place to port all old implementation.
	 *
	 * srk.freeResource() at the end of the method
	 *
	 * @return status only for internal use; so manager knows what to do next!
	 * STATUS_SUCCESS = -1
	 * STATUS_READY_FOR_PROCESSING = 0
	 * STATUS_IN_PROGRESS = 1
	 * STATUS_DEAL_LOCKED = 2
	 * STATUS_ERROR = 3;
	 *
	 */
	int ingest() {

		// 1. Obtain DOM representation of the given xml
		Document document;
		try {
			DocumentBuilder builder = this.manager.documentBuilderFactory.newDocumentBuilder();
			document = builder.parse(new InputSource(new StringReader(dealXml)));
		} catch (Exception e) {
			logger.error("Failed to obtain the DOM document.", e);
			return IngestionQueue.STATUS_ERROR;
		}
		ArrayList passList = new ArrayList();
		List pass = DomUtil.getNamedDescendants(document, "Ingestion");
		if (pass != null && !pass.isEmpty()) {
			Iterator pi = pass.iterator();
			while (pi.hasNext()) {
				Element current = (Element) pi.next();
				String type = current.getAttribute("type");
				if (type.equalsIgnoreCase("return"))
					passList.add(current);
			}
		}

		// 2. Determine institution profile id;
		try {
			int institutionId = getInstitutionId(document);
			srk.getExpressState().cleanAllIds();
			srk.getExpressState().setDealInstitutionId(institutionId);
		} catch (Exception e) {
			logger.error("Failed to determine institution profile id.", e);
			return IngestionQueue.STATUS_ERROR;
		}

		// 3. Inserting to db, where the old/existing implementation begins
		IngestionResult result = null;
		try {
			srk.beginTransaction();
			result = storeDocument(document);
			if (result == null)
				throw new Exception("Ingestion attempted: No result obtained");
			if (result.isDealLocked()) {
				logger.info("The deal is locked.");
				srk.rollbackTransaction();
				srk.freeResources();
				return IngestionQueue.STATUS_DEAL_LOCKED;
			}
			logResult(result);
		} catch (Exception e) {
			logger.error("Failed to ingest " + sourceApplicationId, e);
			result = new IngestionResult();
			result.isApplicationError(true);
		}
		int action;
		try {
			action = processResult(result, document, passList);
			if (action == IngestionHandler.ROLLBACK) {
				System.out.println("INGESTION: Rolling back database updates.");
				logger.debug("INGESTION: Rolling back database updates.");
				srk.rollbackTransaction();
				unlockDeal(srk, result, false);
				srk.freeResources();
				return IngestionQueue.STATUS_ERROR;
			} else if (action == IngestionHandler.STORE) {

				try {
					ArchivedLoanApplication archive = new ArchivedLoanApplication(srk).create(result.getResultEntity().getId(), sourceApplicationId);
					archive.setDealXML(dealXml);
					archive.setArchivedTimeStamp(new Date());
					archive.setReceivedTimeStamp(receivedTimestamp);
					archive.ejbStore();
				} catch (Exception e) {
					//if failed to archive, log in the file, but still continue
					System.out.println("INGESTION: Failed to archive loan application xml.");
					logger.error("Failed to archive the loan application xml for SourceApplicationId=" + sourceApplicationId + ", DealId=" + result.getResultEntity().getId(), e);
					logger.error(dealXml);
				}

				System.out.println("INGESTION: Commiting database updates. ");
				logger.trace("INGESTION: Commiting database updates. ");
				srk.commitTransaction();
				this.unlockDeal(srk, result, false);
			}
		} catch (Exception e) {
			logger.error("Failed to process result for " + sourceApplicationId, e);
			return IngestionQueue.STATUS_ERROR;
		}

		// 4.
		logger.trace("INGESTION: Create Deal Folder in Exchange 2.0.");
		try

		{
			ExchangeFolderCreator dci = new ExchangeFolderCreator(srk);
			int folderIndicator = dci.getFolderCreationIndicator();

			logger.debug("INGESTION: folderIndicator: " + folderIndicator);

			if (folderIndicator == Mc.DO_NOT_CREATE_DOC_CENTRAL_DEAL_FOLDER || folderIndicator == Mc.CREATE_DOC_CENTRAL_DEAL_FOLDER_ON_APPROVAL) {
				// do nothing, in the second case folder creation is postponed until deal approval.
				logger.trace("INGESTION: Do not create Doc Central Deal folder");
			} else if (folderIndicator == Mc.CREATE_DOC_CENTRAL_DEAL_FOLDER_ON_SUBMISSION) {
				// Check first whether the folder for this deal is created or not.
				logger.trace("INGESTION: Create Doc Central Deal folder");

				Deal deal = null;
				IEntityBeanPK beanPK = result.getResultEntity();

				if (beanPK instanceof DealPK) {
					int dealid = ((DealPK) beanPK).getId();
					int copyid = ((DealPK) beanPK).getCopyId();
					////int dealid = result.getLockedDealId();
					logger.debug("INGESTION: dealidToExchange2: " + dealid);
					logger.debug("INGESTION: copyidToExchange2: " + copyid);

					deal = new Deal(srk, null, dealid, copyid);

					// Just sanity check, it should not be here at the moment.
					String folderIdentifier = "";

					logger.debug("INGESTION: DealFoundId: " + deal.getDealId());
					logger.debug("INGESTION: DealFoundCopyId: " + deal.getCopyId());
					logger.debug("INGESTION: DealFoundCopyType: " + deal.getCopyType());
					logger.debug("INGESTION: FolderExists?: " + dci.folderExists(deal));

					if (!dci.folderExists(deal)) {
						folderIdentifier = dci.createFolder(deal);

						String sfpIdentifier = deal.getSourceFirmProfile().getSourceFirmName();
						logger.debug("INGESTION: sobIdentifierToDocCentral: " + sfpIdentifier);

						int sfpId = deal.getSourceFirmProfileId();

						SourceFirmProfile SFPBean = new SourceFirmProfile(srk).findByPrimaryKey(new SourceFirmProfilePK(sfpId));

						logger.debug("INGESTION: sobIdentifierToDocCentralFromBean: " + SFPBean.getSourceFirmName());

						if (folderIdentifier != null && !folderIdentifier.trim().equals("")) {
							logDocCentralFolderCreation(srk, beanPK, SFPBean, folderIdentifier);
						}
					}
				}
			}
		} catch (Exception ex) {
			String msg = "INGESTION:  NOTE: Failed to create folder " + " Deal: " + result.getResultEntity().getId();
			logger.error(msg);
			//// Add this function
			////handler.logDocCentralFolderFailure(srk, beanPK, SFPBean, folderIdentifier);
		}

		// 5. workflow
		try {
			srk.beginTransaction();
			if (!result.isNotificationExcused()) {
				logger.trace("INGESTION: Notify Workflow.");
				notifyWorkflow(new WorkflowNotifier(srk), result);
				System.out.println("INGESTION: Notified Workflow.");
				logger.debug("INGESTION: Notified Workflow.");
			} else {
				logger.trace("INGESTION: Workflow notification excused...");
				System.out.println("INGESTION: Workflow notification excused...");
			}
			logSuccess(result.getResultEntity());
			srk.commitTransaction();
		} catch (Exception e) {
			try {
				srk.rollbackTransaction();
			} catch (Exception ex) {
				logger.error("Failed to roll back.", ex);
			}
			String msg = "INGESTION:  NOTE: Failed to notify workflow of " + " Deal: " + result.getResultEntity().getId() + " arrival.";
			logger.error(msg);
			logFailure(result.getResultEntity());
		}

		try {
			srk.cleanTransaction();
			srk.freeResources();
			System.out.println("INGESTION: Clean Transaction and Free Resources.");
			logger.trace("INGESTION: Clean Transaction and Free Resources.");
		} catch (Exception e) {
			String msg = "INGESTION: Failed to clean transaction and free resources.";
			logger.error(msg, e);
		}
		//logger.trace("INGESTION: Complete for: " + file.getName());
		String endmsg = "INGESTION: " + result.entityString();
		System.out.println(endmsg);
		logger.trace(endmsg);

		return IngestionQueue.STATUS_SUCCESS;
	}

	//
	//
	// All following private methods are copied from ApplicationHandler, then mod for new implementation.
	//
	//

	private static String[] ingestableTables = { "Deal", "Borrower", "Property", "DownPaymentSource", "DealNotes", "DealFee", "EscrowPayment", "Income", "Liability", "Asset", "CreditReference", "EmploymentHistory", "BorrowerAddress", "PropertyExpense", "CreditBureauReport", "BorrowerIdentification", "QualifyDetail", "ComponentLOC", "Component", "ComponentMortgage", "ComponentCreditCard", "ComponentOverdraft", "ComponentLoan" };
	private static String[] ingestableTablesDJ = { "Deal", "Borrower", "InsureOnlyApplicant", "Property", "DownPaymentSource", "DealNotes", "DealFee", "EscrowPayment", "Income", "Liability", "Asset", "CreditReference", "EmploymentHistory", "BorrowerAddress", "PropertyExpense", "CreditBureauReport", "BorrowerIdentification", "QualifyDetail", "ComponentLOC", "Component", "ComponentMortgage", "ComponentCreditCard", "ComponentOverdraft", "ComponentLoan" };

	private int getInstitutionId(Document document) throws RemoteException, FinderException {
		String idStr = getECNIID(document);
		InstitutionProfile ip = new InstitutionProfile(srk);
		ip = ip.findByECNID(idStr);

		// return ip.getInstitutionProfileId();
		int institutionId = ip.getInstitutionProfileId();

		///////////////////////Added for Multi Brand to Multi Lender July 2008///////////////////////////////////
		String strBrand = PropertiesCache.getInstance().getInstanceProperty("multi.brand.legacy.redirect.max.number.of.days.to.redirect.deals.to.brand", "0");
		logger.info("max number of days for redirect: " + strBrand);
		String strDate = PropertiesCache.getInstance().getInstanceProperty("multi.brand.legacy.redirect.institution.activation.date", "Active date not set");
		logger.info("redirect activation date: " + strDate);

		try {
			long maxOfDays = Integer.parseInt(strBrand);
			if (maxOfDays <= 0)
				return institutionId;

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date activationDate = sdf.parse(strDate);
			logger.info("redirect activation date object: " + activationDate);

			String sourceApplicationId = getSourceApplicationId(document);
			MbmlRedirect mbmlRedirect = new MbmlRedirect(srk);
			mbmlRedirect = mbmlRedirect.findBySourceApplicationId(sourceApplicationId);

			Date currentDate = java.util.Calendar.getInstance().getTime();

			if (mbmlRedirect != null && activationDate != null) {
				int originalId = mbmlRedirect.getInstitutionProfileId();
				logger.info("original institutionprofile id: " + originalId);
				long numberOfDays = currentDate.getTime() - activationDate.getTime();
				if (numberOfDays <= maxOfDays * (24 * 60 * 60 * 1000)) {
					institutionId = originalId;
					logger.info("redirecting " + sourceApplicationId + " to " + originalId);
				}
			}
		} catch (ParseException pe) {
			String msg = pe.getMessage();
			if (msg == null)
				msg = "INGESTION: ApplicationHandler:getInstitutionId: " + "ERROR: Cannot parse \"" + strDate + "\"";
			msg = "INGESTION: ApplicationHandler:getInstitutionId: " + msg;
			logger.error(msg, pe);
		} catch (Exception e) {
			String msg = e.getMessage();
			if (msg == null)
				msg = "Unknown Error";
			msg = "INGESTION: ApplicationHandler:getInstitutionId: " + msg;
			logger.error(msg, e);
		}

		return institutionId;
	}

	private String getECNIID(Document document) throws RemoteException, FinderException {
		List nodesByName = DomUtil.getNamedDescendants(document, "ecniLenderId");
		if (nodesByName == null || nodesByName.size() != 1)
			throw new FinderException();

		Node institionNode = (Node) nodesByName.get(0);
		String idStr = institionNode.getFirstChild().getNodeValue();
		if (idStr == null || idStr.trim().length() == 0)
			throw new FinderException();
		return idStr;
	}

	private String getSourceApplicationId(Document document) throws RemoteException, FinderException {
		List nodesByName = DomUtil.getNamedDescendants(document, "sourceApplicationId");
		if (nodesByName == null || (nodesByName != null && nodesByName.size() != 1))
			throw new FinderException();

		Node sourceApplicationIdNode = (Node) nodesByName.get(0);
		String str = sourceApplicationIdNode.getFirstChild().getNodeValue();
		if (str == null || (str != null && str.trim().length() == 0))
			throw new FinderException();
		return str;
	}

	private IngestionResult storeDocument(Document document) throws Exception {

		/*reset();
		    this.associationList = null;
		    this.ingestedNodeList = null;
		    this.ingestedNodeList = new ArrayList();
		    this.associationList = new ArrayList();
		    this.ingestionDate = new Date();*/

		List ingestedNodeList = new ArrayList();
		List associationList = new ArrayList();
		Date ingestionDate = new Date();
		Map<Node, ArrayList<Node>> extraFields = new HashMap<Node, ArrayList<Node>>();
		//List<String[]> extraFieldPairs = new ArrayList<String[]>();

		CalcMonitor dcm = CalcMonitor.getMonitor(srk);
		String msg = null;
		IngestionResult result = new IngestionResult();
		PropertiesCache pc = PropertiesCache.getInstance();

		// dupecheck processor has to be different for each institution.
		String dupeCheckClassName = pc.getProperty(srk.getExpressState().getDealInstitutionId(), COM_BASIS100_DUPECHECK_PROCESSOR_NAME, DupeCheckProcessor.class.getName());
		boolean dupeCheckEnabled = pc.getProperty(srk.getExpressState().getDealInstitutionId(), COM_BASIS100_INGESTION_DUPECHECK_ENABLED, "N").trim().equalsIgnoreCase("Y");

		//This method is called from storeDocument method.InstituionProfileId has been set in srk.
		boolean ibRstPrePayLTVgtLimit = pc.getProperty(srk.getExpressState().getDealInstitutionId(), ING_RESET_PREPAY_LTVGTLIMIT, "Y").equalsIgnoreCase("Y");

		if (workflowNotificationType.equalsIgnoreCase("cervus")) {
			result.setCervusJob(true);
			result.setNotificationType(Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_WUP);
		}

		//#DG600 rewritten, code cleanup
		//start get defaults process
		ApplicationDefaults defaults = null;
		//get cached rules if any
		try {
			//defaults = getDefaults(monitor);
			defaults = new ApplicationDefaults(srk, new File(ClassLoader.getSystemResource("ingestion.constraint").toURI()));
		} catch (Exception exc) {
			msg = "INGESTION: ApplicationHandler: Error processing defaults file ingestion.constraint.";
			logger.error(msg, exc);
			System.err.println(msg);
			//result.addMessage("Error Handling Defaults. See Log", PassiveMessage.CRITICAL);
			//result.isApplicationError(true);
			System.exit(1); // do not proceed !
		}
		//#DG600 end

		/*ArrayList passList = new ArrayList();
		List pass = DomUtil.getNamedDescendants(document, "Ingestion");
		if (pass != null && !pass.isEmpty()) {
			Iterator pi = pass.iterator();
			while (pi.hasNext()) {
				Element current = (Element) pi.next();
				String type = current.getAttribute("type");
				if (type.equalsIgnoreCase("return"))
					passList.add(current);
			}
		}*/

		//- create Entities
		List asslist = null;
		try {
			asslist = makeAssociations(new EntityNodeAssociation(document, null, document, null), dcm, ingestedNodeList, associationList, document, extraFields);
		} catch (Exception exc) {
			logger.error("INGESTION: ApplicationHandler: Error handling Document Object Model.", exc);
			result.isInvalidInput(true);
			return result;
		}

		Deal deal = (Deal) getRootAssociation(document, associationList).getEntity();
		Element dealNode = (Element) getRootAssociation(document, associationList).getNode();

		if (deal == null) {
			String em = ">>>>INGESTION: Unable to obtain Deal node from object model";
			logger.error(em);
			result.isInvalidInput(true);
			return result;
		}
		deal.setCopyType("G");
		result.setResultEntity(deal.getPk());
		// FXLink:phaseII
		if (isFXLinkType(deal.getSystemTypeId())) {
			result.setFilogixLink(true);
		}

		MasterDeal md = new MasterDeal(srk, null, deal.getDealId());
		md.setFXLinkPOSChannel(this.senderChannel);
		md.setFXLinkUWChannel(this.receiverChannel);
		int DEFAULT_FXLINKSCHEMAID = 1;
		md.setFXLinkSchemaId(DEFAULT_FXLINKSCHEMAID); //TODO locate DEFAULT_FXLINKSCHEMAID
		md.ejbStore();

		//TODO
		/*if (createDealNotesFlag && extraFields.size() > 0) {
			System.out.println(extraFields.size());
			for (Node key : extraFields.keySet()) {
				ArrayList<Node> value = extraFields.get(key);
				DealNotes dealNotes = new DealNotes(srk).create((DealPK) deal.getPk(), deal.getDealId(), DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, "Start " + key.getNodeName() + " for ", 0);
				for (Node node : value) {
					dealNotes = new DealNotes(srk).create((DealPK) deal.getPk(), deal.getDealId(), DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, "   " + node.getNodeName() + ": " + node.getTextContent(), 0);
				}
				dealNotes = new DealNotes(srk).create((DealPK) deal.getPk(), deal.getDealId(), DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, "End " + key.getNodeName() + " for ", 0);
			}

		}*/
		/*if (createDealNotesFlag && extraFieldPairs.size() > 0) {
			for (String[] pair : extraFieldPairs) {
				DealNotes dealNotes = new DealNotes(srk).create((DealPK) deal.getPk(), deal.getDealId(), DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0, pair[0] + ": " + pair[1], 0);
			}
		}*/

		ListIterator li = associationList.listIterator();
		EntityNodeAssociation current = null;

		//- Store the Deal check constraints and defaults
		try {
			while (li.hasNext()) {
				current = (EntityNodeAssociation) li.next();
				DealEntity entity = current.getEntity();
				//reads the default parameters and loads the entity with default
				//if no value has been recieved
				//loadDefaults(current, defaults, getAbbre(document));
				defaults.checkDefaults(current, getAbbre(document));
				entity.ejbStore();
			}
		} catch (Exception ste) {
			//#DG600 code cleaning
			String name = current.getName();
			if (name == null)
				name = "Unknown Entity. ";
			msg = "INGESTION: Failed to store Entity: " + name + " - Reason: " + ste;
			System.err.println(getClass().getName() + ':' + msg);
			logger.error(msg, ste);
			result.isApplicationError(true);
			result.addMessage(msg);
			msg = StringUtil.stack2string(ste);
			System.err.println(getClass().getName() + ':' + msg);
			logger.error(msg);
			return result;
		}
		//  **************MCM Impl team changes - 11-Aug-2008 - XS_1.2 -1.2 Starts *****//
		try {
			storeSpecial(deal, dcm);
		} catch (Exception exc) {
			exc.printStackTrace();
			logger.error(exc.getMessage(), exc);
			result.isApplicationError(true);
			return result;
		}
		//  **************MCM Impl team changes - 11-Aug-2008 - XS_1.2 -1.2 Ends *****//
		msg = "INGESTION: Stored uncommitted deal...";
		System.out.println(msg);
		logger.debug(msg);
		//- Find the source
		try {
			ApplicationSourceHandler sourceHandler = new ApplicationSourceHandler(srk);
			PassiveMessage spm = sourceHandler.handleSourceDetails(deal, document);

			result.addAllMessages(spm);
		} catch (SourceDeterminationException sde) {
			logger.warn("INGESTION: Error matching profiles or systemType - Invalid Input.", sde);
			String srcmsg = "The Source Firm associated with this Application" + " Source of Business could not be determined from the supplied data.";

			result.addMessage(srcmsg);
			result.isInvalidSource(true);
		}

		//set the lender and mtg product and rate
		try {
			double inboundRate = 0d;

			if (dealNode != null) {
				Node rateNode = DomUtil.getFirstNamedChild(dealNode, "netInterestRate");

				String rateString = DomUtil.getNodeText(rateNode);

				inboundRate = TypeConverter.doubleTypeFrom(rateString, 0d);
			}

			/***** FXP24050 fix start ******/
			srk.getExpressState().setDealIds(deal.getDealId(), deal.getInstitutionProfileId(), deal.getCopyId());
			/***** FXP24050 fix end   ******/

			ApplicationLenderHandler lenderHandler = new ApplicationLenderHandler(srk, deal);

			//    ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
			//PassiveMessage pm = lenderHandler.handleProcess(ingestionDate, inboundRate, document.getFirstChild());
			//    ***** Change by NBC Impl. Team - Version 1.1 - End*****//
			//    **************MCM Impl team changes - 13-Aug-2008 - XS_1.5 -1.3 Starts *****//
			PassiveMessage pm = lenderHandler.handleMtgProdPRICheckforDeal(ingestionDate, inboundRate, document.getFirstChild());
			pm.addAllMessages(pm);

			//MCM Impl Bugfix FXP23902 : Calling handleMtgProdPRICheckforComp even the handleMtgProdPRICheckforDeal() check fails so commenting if statements
			//if(!pm.getCritical()){
			PassiveMessage pmComponentPRICheck = lenderHandler.handleMtgProdPRICheckforComp(associationList);
			pm.addAllMessages(pmComponentPRICheck);
			// }
			//    **************MCM Impl team changes - 13-Aug-2008 - XS_1.5 -1.3 Ends *****//
			if (pm.getNumMessages() > 0) {
				logger.info("INGESTION: Application Handler : " + pm.getMsgs().get(0));
			}

		} catch (Exception exc) {
			logger.error("INGESTION: LenderHandler: ", exc);
			result.isApplicationError(true);
			return result;
		}

		//==========================================================================
		//- perform calculations
		//==========================================================================
		long calcStart = System.currentTimeMillis();
		try {

			if (dcm != null) {
				System.out.println("INGESTION: Performing first round calcs....");
				dcm.calc();
				deal = deal.findByPrimaryKey((DealPK) deal.getPk()); //FXP23298, MCM Nov 5 08, Synchronizing db and entity
				System.out.println("INGESTION: End first round calcs - time (millisec): " + (System.currentTimeMillis() - calcStart));
			}

		} catch (Exception exc) {
			msg = "INGESTION: Calculations failed on DealIngestion: " + exc;
			System.err.println(getClass().getName() + ':' + msg);
			logger.error(msg, exc);
			result.isApplicationError(true);
			return result;
		}

		// FXP21309
		// CalcMonitor shouldn't be reset for 2nd calc.
		// need to keep inputs to calc because of calc order
		// call 2nd calc
		// deleted,,,,,result of discussion
		//	    System.out.println(getClass().getName() + ':'
		//	      		+ "INGESTION: Performing Second round calcs....");
		//
		//	      dcm.calc();
		//
		//	      System.out.println(getClass().getName() + ':'
		//	      		+ "INGESTION: End Second Second calcs - time (millisec): "
		//	      		+ (System.currentTimeMillis() - calcStart));
		//	      logger.trace("INGESTION: finished 2nd store and calc... ");

		// clean up calcMonitor
		dcm = CalcMonitor.getMonitor(srk);
		deal.setCalcMonitor(dcm);

		SpecialProcessingHandler specialProcessingHandler = new SpecialProcessingHandler(srk);
		try {
			setStatusAndType(result, deal, dealNode, ingestionDate, ibRstPrePayLTVgtLimit);
			deal.ejbStore();

			/*#DG228 not needed anymore, will stay set by the defaults file instead, see setStatusAndType above
			String lDefaultMIIndicator = PropertiesCache.getInstance().getProperty("com.basis100.ingestion.default.miindicatorid");
			if(lDefaultMIIndicator != null){
			  deal.setMIIndicatorId(Integer.parseInt(lDefaultMIIndicator));
			  deal.ejbStore();
			}*/
			// redirection for special processing
			specialProcessingHandler.processMIInsurerRequest(deal, dealNode);

			calcStart = System.currentTimeMillis();
			System.out.println(getClass().getName() + ':' + "INGESTION: Performing second round calcs....");

			dcm.calc();

			System.out.println(getClass().getName() + ':' + "INGESTION: End Second round calcs - time (millisec): " + (System.currentTimeMillis() - calcStart));
			logger.trace("INGESTION: finished last store and calc... ");
			deal = deal.findByPrimaryKey((DealPK) deal.getPk()); //FXP23298, MCM Nov 5 08, Synchronizing db and entity
		} catch (Exception exc) {
			logger.error("INGESTION: Application Handler : ", exc);
		}
		//==========================================================================
		// Business rules IC RULES
		//==========================================================================
		ApplicationValidator validator = new ApplicationValidator(srk);
		//PassiveMessage icMsg = validator.validate(deal,srk);
		PassiveMessage icMsg = validator.validate(deal, srk, determineLanguage(deal));

		// introduce mossys.properties to disable addressScrub for CIBC - Jan 16, 2008 - Midori
		boolean useAddressScrub = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), "com.filogix.ingestion.addressscrub", "Y").equals("Y");

		if (useAddressScrub) {
			//#DG80 address scrub validation
			AddScrubExecutor loAdrScrEx = new AddScrubExecutor(srk);
			loAdrScrEx.validateAddresses(deal, icMsg, AddScrubConstants.ADDRESS_SOURCE_INGESTION);
			//#DG80 end
		}
		result.appendICRulesMessages(icMsg);
		//==========================================================================
		// end of business rules section
		//==========================================================================

		// =======================================================================
		// now call the special processing handler
		// =======================================================================
		specialProcessingHandler.processOriginationBranch(deal, dealNode);
		//  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
		specialProcessingHandler.processServiceBranch(deal, dealNode);
		//  ***** Change by NBC Impl. Team - Version 1.1 - End*****//
		specialProcessingHandler.processPartyProfile(deal, dealNode);
		// =======================================================================
		// end of special processing handler
		// =======================================================================

		// perform dupcheck routine
		//boolean dupArchive = false;

		try {
			doDupeCheck(document, srk, result, deal, dupeCheckEnabled, dupeCheckClassName);
		} catch (Exception exc) { //#DG126 rewritten
			msg = "INGESTION: DupCheck Failure: " + exc;
			logger.info(msg, exc);
			System.err.println(msg);

			if (exc instanceof DealLockedException)
				result.setDealLocked(true);
			else
				result.isApplicationError(true);
			return result;
		}

		// Ticket #458
		if (deal.getSystemTypeId() > 50) {
			deal.setSystemTypeId(Mc.SYSTEM_TYPE_EXPERT);
			deal.ejbStore(); //dvg
		}
		// zivko:marker: decisioning
		try {
			if (result.hasMessages()) {
				checkValidationExcused(result, deal);
				result.isValidationFailure(true);
				appendDealNote(document, result, associationList);
				appendICMessagesAsDealNote(document, result);
			}
			if (result.hasICRulesMessages()) {
				result.setICRulesFailure(true);
				appendICMessagesAsDealNote(document, result);
				createDealNotes(result, deal);
				deal.setStatusId(Mc.DEAL_DENIED);
				//version 1.10 artf767017
				int denailReasonId = deal.getDenialReasonId();
				logger.error("DenialReasonID " + denailReasonId);
				if (!(denailReasonId == 100 || denailReasonId == 101 || denailReasonId == 102 || denailReasonId == 103)) {
					deal.setDenialReasonId(Mc.DENIAL_REASON_IC_REJECT);
				}
				deal.ejbStore();
			}
			if (result.isDuplicateInput()) {
				appendDupeCheckMessageToSource(document, result);
			}
			//TODO: if ic business rule failure or resubmission of an approved deal
			//insert outbound queue
			if (result.isICRulesFailure()) {
				ESBOutboundQueue q = new ESBOutboundQueue(srk);
				ESBOutboundQueuePK pk = q.createPrimaryKey();
				q.create(pk, ServiceConst.SERVICE_TYPE_LOANDECISION_UPDATE, deal.getDealId(), deal.getSystemTypeId(), 0);
				q.setServiceSubTypeid(ServiceConst.SERVICE_SUBTYPE_LOANDECISION_DENIAL);
				q.ejbStore();
			}

		} catch (Exception exc) {
			logger.error("INGESTION: Failure while performing validation.", exc);
		}

		msg = "INGESTION: " + result.printCurrentStatus();
		logger.debug(msg);
		System.out.println(getClass().getName() + ':' + msg);

		return result;
	}

	private List makeAssociations(EntityNodeAssociation assoc, CalcMonitor dcm, List ingestedNodeList, List associationList, Document document, Map<Node, ArrayList<Node>> extraFields) throws IngestionException {
		Node node = assoc.getNode();
		DealEntity container = assoc.getEntity();

		if (node.getNodeType() == node.ELEMENT_NODE) {
			ingestedNodeList.add(node);

			if (node.hasChildNodes()) {
				NodeList nl = node.getChildNodes();
				int size = nl.getLength();

				for (int i = 0; i < size; i++) {
					Node child = nl.item(i);

					if (isIngestableTable(child)) {
						try {
							String name = child.getNodeName();
							EntityNodeAssociation currentAssociation = null;
							DealEntity currentEntity = null;
							currentEntity = EntityBuilder.createEntity(name, srk, dcm, container);
							currentAssociation = new EntityNodeAssociation(child, currentEntity, document, assoc);

							if (assoc != null)
								assoc.addChild(currentAssociation);

							associationList.add(currentAssociation);
							handleEntityFields(currentAssociation, extraFields);

							handleSpecial(currentAssociation, child, currentEntity, dcm, document, associationList, extraFields);
							//makeAssociations(currentAssociation, dcm);
							makeAssociations(currentAssociation, dcm, ingestedNodeList, associationList, document, extraFields);
						} catch (Exception e) {
							String msg = "Failed to make EntityNodeAssociation for: " + child.getParentNode().getNodeName() + "." + child.getNodeName() + "  ";
							logger.error(msg, e);
							throw new IngestionException(msg);
							//throw new FileIngestionException(msg);
						}
					}

				}//end for
			}
		} else {

			Node next = node.getNextSibling();

			if (next != null) {
				EntityNodeAssociation ena = new EntityNodeAssociation(next, null, document, assoc);
				if (assoc != null)
					assoc.addChild(ena);
				//makeAssociations(ena, dcm);
				makeAssociations(ena, dcm, ingestedNodeList, associationList, document, extraFields);
			} else if (node.hasChildNodes()) {
				next = node.getFirstChild();
				EntityNodeAssociation ena = new EntityNodeAssociation(next, null, document, assoc);
				if (assoc != null)
					assoc.addChild(ena);
				//makeAssociations(ena, dcm);
				makeAssociations(ena, dcm, ingestedNodeList, associationList, document, extraFields);
			}

		} // end if else

		return associationList;
	}

	private EntityNodeAssociation getRootAssociation(Document document, List associationList) {
		List l = DomUtil.getNamedDescendants(document, "Deal");
		if (l.isEmpty())
			return null;
		return getAssociationFor((Node) l.get(0), associationList);
	}

	private EntityNodeAssociation getAssociationFor(Node node, List associationList) {
		if (node == null)
			return null;
		ListIterator li = associationList.listIterator();
		while (li.hasNext()) {
			EntityNodeAssociation current = (EntityNodeAssociation) li.next();
			if ((current.getNode()).equals(node)) {
				return current;
			}
		}
		return null;
	}

	private boolean isIngestableTable(Node n) {
		String name = n.getNodeName();
		//--DJ_CR136--start--//
		boolean isDJClient = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), COM_BASIS100_CALC_ISDJCLIENT, "N").equals("Y");

		String[] aingestableTables = isDJClient ? ingestableTablesDJ : ingestableTables;
		for (String aTable : aingestableTables)
			if (name.equals(aTable))
				return true;
		return false;
	}

	private void handleEntityFields(EntityNodeAssociation assoc, Map<Node, ArrayList<Node>> extraFields) {
		Node node = assoc.getNode();

		NodeList nl = node.getChildNodes();

		int size = nl.getLength();

		for (int i = 0; i < size; i++) {
			Node child = nl.item(i);

			if (child.getNodeType() == Node.ELEMENT_NODE)
				handleFieldNode(assoc, child, extraFields);
		}
	}

	private boolean handleFieldNode(EntityNodeAssociation ass, Node fieldNode, Map<Node, ArrayList<Node>> extraFields) {
		DealEntity entity = ass.getEntity();

		if (entity == null)
			return false;

		String data = null;

		try {
			Node value = fieldNode.getFirstChild();
			data = value.getNodeValue();
			data = data.trim();
		} catch (NullPointerException npe) //if the node has null value
		{
			return false;
		}

		if (data.length() == 0)
			return false; //if the node has empty string value

		String field = fieldNode.getNodeName();
		//=====================================================================================
		//ZIVKO:ALERT:INGESTION: this is the place for making break point to see how the system handles
		// node from ingest file
		//=====================================================================================
		//String id = null;

		//if the field is handled as special - take a detour
		if (handleSpecialFields(ass, data, field))
			return true;

		String storable = handlePicklist(field, data);

		if (!storable.equals(data))
			field = field + "Id";

		try {

			Node node = ass.getNode();
			Node parent = (node == null ? null : node.getParentNode());
			Node granny = (parent == null ? null : parent.getParentNode());

			//TODO this is kept for future reference
			//if (!"DealNotes".equals(node.getNodeName()) && ("Deal".equals(node.getNodeName()) || "Deal".equals(parent.getNodeName()))) {
			//	System.out.println("parent:" + granny + "; parent:" + parent + "; node: " + node + "; " + field + ": " + storable);
			//	ArrayList<Node> value = extraFields.get(node);
			//	if (value == null) {
			//		value = new ArrayList<Node>();
			//		extraFields.put(node, value);
			//	}
			//	value.add(fieldNode);
			//}
			//System.out.println("parent:"+(node.getParentNode()==null?"null":node.getParentNode())+" node:"+node+" child: "+child);
			//if ("Deal".equals(child.getNodeName()) || "Deal".equals(node.getNodeName())) {
			//	ArrayList<Node> value = extraFields.get(child);
			//	if (value ==null) {
			//		value = new ArrayList<Node>();
			//		extraFields.put(node, value);
			//	}
			//	value.add
			//}

			entity.setField(field, storable);
			ass.updateSetFields(field);
			return true;
		} catch (Exception e) {
			//TODO here is where the extra fields are detected
			//DealNotes note = new DealNotes();
			//extraFieldPairs.add(new String[] { field, storable });
			String report = "Failed to set FIELD: " + field + " in ENTITY: " + fieldNode.getParentNode().getNodeName();
			logger.trace(report, e);
			logger.debug(getClass().getName() + ':' + report);
			return false;
		}

	}

	private boolean isFXLinkType(int pType) {
		String fxlinkTypes = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), COM_BASIS100_FXLINK_TYPES);
		// if the property does not exist return false
		if (fxlinkTypes == null) {
			return false;
		}
		//try {
		StringTokenizer st = new StringTokenizer(fxlinkTypes, ",");
		String token;

		while (st.hasMoreTokens()) {
			token = st.nextToken();

			if (Integer.parseInt(token.trim()) == pType) {
				return true;
			}
		}
		return false;
		//} catch (Exception e) {
		//	throw new Exception("ERROR - wrong property value(s) for property: com.basis100.fxlink.types ");
		//}

	}

	private String getAbbre(Document document) throws RemoteException, FinderException {
		String idStr = getECNIID(document);
		InstitutionProfile ip = new InstitutionProfile(srk);
		ip = ip.findByECNID(idStr);
		return ip.getInstitutionAbbr();
	}

	private void storeSpecial(Deal deal, CalcMonitor dcm) throws Exception {

		int dealId = deal.getDealId();
		int copyId = deal.getCopyId();
		Component comp = new Component(srk);
		Collection componentCollections = comp.findByDeal(new DealPK(dealId, copyId));
		if (componentCollections.size() == 0) {
			return;
		}
		ComponentSummary compSummary = null;
		compSummary = new ComponentSummary(srk);
		ComponentSummaryPK compSummaryPk = new ComponentSummaryPK(dealId, copyId);
		try {
			compSummary.setSilentMode(true); //FXP24463
			compSummary = compSummary.findByPrimaryKey(compSummaryPk);
		} catch (RemoteException fe) {
			logger.error(fe.getMessage(), fe);
			logger.debug((new StringBuilder()).append("IGNORED:: Can not find the Component Summary: dealId =").append(dealId).append(", copyId=").append(copyId).toString());
			compSummary = compSummary.create(dealId, copyId);
		} catch (FinderException fe) {
			logger.error(fe.getMessage(), fe);
			logger.debug((new StringBuilder()).append("IGNORED:: Can not find the Component Summary: dealId =").append(dealId).append(", copyId=").append(copyId).toString());
			compSummary = compSummary.create(dealId, copyId);
		}

	}

	private void setStatusAndType(IngestionResult result, Deal deal, Element dealNode, Date ingestionDate, boolean ibRstPrePayLTVgtLimit) throws Exception {
		//#DG228 also used in specialProcessingHandler.processMIInsurerRequest
		String lSetMtgInsur = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), COM_BASIS100_INGESTION_SET_MORTGAGE_INSURER, "N");

		logger.debug("INGESTION: Setting status and type....");

		if (result.isValidationExcused())
			deal.setStatusId(Mc.DEAL_INCOMPLETE);
		else
			deal.setStatusId(Mc.DEAL_RECEIVED);

		deal.setStatusDate(ingestionDate);
		deal.setApplicationDate(ingestionDate);

		double ltv = deal.getCombinedLTV();
		logger.debug("INGESTION: Current LTV: " + ltv);

		//3.2patch LTV project
		double lLtv_Threshold = Double.parseDouble(PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), ING_LTV_THRESHOLD, "80"));

		if (ltv > lLtv_Threshold) {
			deal.setDealTypeId(Mc.DEAL_TYPE_HIGH_RATIO);

			//if( deal.getMortgageInsurerId() == Mc.MI_INSURER_BLANK)
			//{
			// ========================================================================
			// the following code has been removed based on tracker issue #267
			// and replaced with the code which will run unconditionaly
			// ========================================================================

			if (!lSetMtgInsur.equalsIgnoreCase("X")) //#DG228 let the defaults prevail for xcd
				MIProcessHandler.setForIngestionHighRatio(deal, srk);
			//}

			//3.2patch LTV project
			if (ibRstPrePayLTVgtLimit) {
				deal.setPrePaymentOptionsId(Mc.PREPAYMENT_OPTIONS_NHA);
			}

		}
	}

	private int determineLanguage(Deal pDeal) {
		int rv = Mc.LANGUAGE_PREFERENCE_ENGLISH;
		try {
			Collection col = pDeal.getBorrowers();
			Iterator it = col.iterator();
			while (it.hasNext()) {
				Borrower bor = (Borrower) it.next();
				if (bor.isPrimaryBorrower()) {
					rv = bor.getLanguagePreferenceId();
					return rv;
				}
			}
		} catch (Exception exc) {
			return rv;
		}
		return rv;
	}

	private void doDupeCheck(Document document, SessionResourceKit srk, IngestionResult result, Deal deal, boolean dupeCheckEnabled, String dupeCheckClassName) throws Exception {
		//========================================================================
		// duplicates check
		//========================================================================
		// #DG598 rewritten - deprecated default dupe chek engine
		if (!dupeCheckEnabled) {
			logger.info("INGESTION: DupCheck action will not be taken at all.");
			return;
		}
		if (!result.hasICRulesMessages()) {
			Class processorClass = Class.forName(dupeCheckClassName);
			DupeCheckProcessor dupeCheckProcessor = (DupeCheckProcessor) processorClass.newInstance();
			if (dupeCheckProcessor == null) {
				String msg = "Cannot instantiate class " + dupeCheckClassName;
				logger.error(msg);
				//throw new FileIngestionException(msg);
				throw new IngestionException(msg);
			}
			logger.info("INGESTION: DupeCheck Processor started: Class = " + dupeCheckClassName);

			dupeCheckProcessor.process(deal, srk, document);
			result.setNotificationType(dupeCheckProcessor.getNotificationType());
			if (dupeCheckProcessor.getStoredDealPK() != null) {
				result.setResultEntity(dupeCheckProcessor.getStoredDealPK());
			}
			if (dupeCheckProcessor.isAnyDealLocked()) {
				result.setLockedDealId(dupeCheckProcessor.getLockedDealId());
			}
			if (dupeCheckProcessor.getMessagesForBroker().size() > 0) {
				result.setMessagesForBrokerAppended(true);
				result.setDupeCheckMessagesForBroker(dupeCheckProcessor.getMessagesForBroker());
			} else {
				result.setMessagesForBrokerAppended(false);
			}
			result.isDuplicateInput(dupeCheckProcessor.isDuplicate());
			result.setCapLinkRequired(dupeCheckProcessor.isCapLinkRequired());
			result.setCapLinkSensitive(dupeCheckProcessor.isCapLinkSensitive());
			logger.info("INGESTION: DupeCheck Processor finished: Class=" + dupeCheckClassName);
			return;
		}
	}

	private boolean checkValidationExcused(IngestionResult current, Deal deal) /*throws FileIngestionException*/{
		try {
			String name = deal.getLenderProfile().getLPShortName();
			int st = deal.getSystemTypeId();

			if ((st == Mc.SYSTEM_TYPE_MORTY && name.equals("ING")) || st == Mc.SYSTEM_TYPE_EXTRANET) {
				current.isValidationExcused(true);
			}

			return current.isValidationExcused();
		} catch (Exception ex) {
			String msg = "INGESTION: Error while checking validation excused - Reason: " + ex;
			logger.error(msg, ex);
			return false;
		}
	}

	private void appendDealNote(Document document, IngestionResult res, List associationList) /*throws FileIngestionException*/{
		Node root = getRootAssociation(document, associationList).getNode();
		Deal rootEntity = (Deal) getRootAssociation(document, associationList).getEntity();

		Collection msgs = res.getMessages();
		Iterator it = msgs.iterator();
		String msg = null;

		while (it.hasNext()) {
			msg = (String) it.next();
			Element noteElement = document.createElement("DealNotes");
			Element noteText = document.createElement("dealNotesText");
			Element dealNotesCategory = document.createElement("dealNotesCategory");
			Element dealNotesUser = document.createElement("dealNotesUser");

			noteElement.appendChild(noteText);
			noteText.appendChild(document.createTextNode(msg));

			noteElement.appendChild(dealNotesCategory);
			dealNotesCategory.appendChild(document.createTextNode("Deal Ingestion"));

			noteElement.appendChild(dealNotesUser);
			dealNotesUser.appendChild(document.createTextNode("SYSTEM"));

			root.appendChild(noteElement);
		}
	}

	private void appendICMessagesAsDealNote(Document document, IngestionResult res) /*throws FileIngestionException*/{
		List l = null;
		if (res.isFilogixLink()) {
			l = DomUtil.getNamedDescendants(document, "Document");
		} else {
			l = DomUtil.getNamedDescendants(document, "Deal");
		}
		if (l == null || l.isEmpty())
			return;

		Node root = (Node) l.get(0);
		//Node root = getRootAssociation().getNode();
		//Deal rootEntity = (Deal)getRootAssociation().getEntity();

		Collection msgs = res.getICRulesMessages();
		Iterator it = msgs.iterator();
		String msg = null;

		while (it.hasNext()) {
			msg = (String) it.next();
			Element noteElement = document.createElement("DealNotes");
			Element noteText = document.createElement("dealNotesText");
			Element dealNotesCategory = document.createElement("dealNotesCategory");
			Element dealNotesUser = document.createElement("dealNotesUser");

			noteElement.appendChild(noteText);
			noteText.appendChild(document.createTextNode(msg));

			noteElement.appendChild(dealNotesCategory);
			dealNotesCategory.appendChild(document.createTextNode("Deal Ingestion"));

			noteElement.appendChild(dealNotesUser);
			dealNotesUser.appendChild(document.createTextNode("SYSTEM"));

			root.appendChild(noteElement);
		}
	}

	private void createDealNotes(IngestionResult res, Deal pDeal) throws FinderException, RemoteException, CreateException {
		final int catID = Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION;
		final int appId = TypeConverter.intTypeFrom(pDeal.getApplicationId());
		final ArrayList<Formattable[]> msgsObjs = res.getICRulesMsgObjects();
		DealNotes notes = new DealNotes(srk);
		DealPK dpk = new DealPK(pDeal.getDealId(), pDeal.getCopyId());

		for (Formattable[] formatta : msgsObjs) {
			notes.create(dpk, appId, catID, 0, formatta);
		}
	}

	private void appendDupeCheckMessageToSource(Document document, IngestionResult pResult) /*throws FileIngestionException*/{
		List l = null;
		if (pResult.isFilogixLink()) {
			l = DomUtil.getNamedDescendants(document, "Document");
		} else {
			l = DomUtil.getNamedDescendants(document, "Deal");
		}
		if (l == null || l.isEmpty())
			return;

		Node root = (Node) l.get(0);

		Collection msgs = pResult.getDupeCheckMessagesForBroker();
		Iterator it = msgs.iterator();

		String msg = null;

		while (it.hasNext()) {
			msg = ((ClientMessage) it.next()).getMessageText();
			Element noteElement = document.createElement("DealNotes");
			Element noteText = document.createElement("dealNotesText");
			Element dealNotesCategory = document.createElement("dealNotesCategory");
			Element dealNotesUser = document.createElement("dealNotesUser");

			noteElement.appendChild(noteText);
			noteText.appendChild(document.createTextNode(msg));

			noteElement.appendChild(dealNotesCategory);
			dealNotesCategory.appendChild(document.createTextNode("Deal Ingestion"));

			noteElement.appendChild(dealNotesUser);
			dealNotesUser.appendChild(document.createTextNode("SYSTEM"));

			root.appendChild(noteElement);
		}
	}

	private void handleSpecial(EntityNodeAssociation assc, Node node, DealEntity ent, CalcMonitor dcm, Document document, List associationList, Map<Node, ArrayList<Node>> extraFields) throws Exception {
		if (assc == null)
			return;
		if (assc.getName().equals("BorrowerAddress")) {
			if (DomUtil.hasChildNamed(node, "Addr")) {
				BorrowerAddress de = (BorrowerAddress) assc.getEntity();
				Addr add = de.getAddr();
				Node aNode = DomUtil.getFirstNamedChild(node, "Addr");
				EntityNodeAssociation current = new EntityNodeAssociation(aNode, add, document, assc);
				assc.addChild(current);
				handleEntityFields(current, extraFields);
				associationList.add(current);
				return;
			}
		} else if (assc.getName().equals("EmploymentHistory")) {
			if (DomUtil.hasChildNamed(node, "Contact")) {
				EmploymentHistory hist = (EmploymentHistory) assc.getEntity();
				Contact ct = hist.getContact();
				Node aNode = DomUtil.getFirstNamedChild(node, "Contact");
				EntityNodeAssociation current = new EntityNodeAssociation(aNode, ct, document, assc);
				handleEntityFields(current, extraFields);
				associationList.add(current);
				assc.addChild(current);
				handleSpecial(current, aNode, ct, dcm, document, associationList, extraFields);
				return;
			}
		} else if (assc.getName().equals("Contact")) {
			Contact de = (Contact) assc.getEntity();
			Addr add = de.getAddr();
			Node aNode = DomUtil.getFirstNamedChild(node, "Addr");
			EntityNodeAssociation current = new EntityNodeAssociation(aNode, add, document, assc);
			handleEntityFields(current, extraFields);
			assc.addChild(current);
			associationList.add(current);
			return;
		}
		return;
	}

	private boolean handleSpecialFields(EntityNodeAssociation ass, String data, String field) {
		// handle jobtitle
		if (ass.getName().equals("EmploymentHistory") && field.equals("jobTitle")) {
			EmploymentHistory eh = (EmploymentHistory) ass.getEntity();
			eh.setJobTitle(data); //set the jobtitle description supplied

			//if the description matches a value from the JobTitle picklist ...
			String value = handlePicklist(field, data);

			if (value.equals(data))
				return true;
			else {
				// ...value will be different than supplied description
				//If it is a number set the field - else set the field with 0
				eh.setJobTitleId(TypeConverter.intTypeFrom(value, 0));
				return true;
			}
		}
		// added for ML - by Midori July 3, 2007 == START
		else if (ass.getName().equals("Deal") && field.equals("ECNILenderId")) {
			try {
				InstitutionProfile profile = new InstitutionProfile(srk);
				profile = profile.findByECNID(data);
				Deal deal = (Deal) ass.getEntity();
				deal.setInstitutionProfileId(profile.getInstitutionProfileId());
			} catch (RemoteException re) {
				logger.error("RemoteException: InstitutionProfileId could not obtained by ECNILenderId = " + data, re);
				//TODO:what should I do?
				//Lender profile id is mandatory for ML
			} catch (FinderException fe) {
				logger.error("FinderException: InstitutionProfileId could not obtained by ECNILenderId = " + data, fe);
				//TODO:what should I do?
				//Lender profile id is mandatory for ML
			}
			return true;
		}
		// added for ML - by Midori July 3, 2007 == END
		return false;
	}

	private String handlePicklist(String table, String desc) {
		String idvalue = null;
		if (PicklistData.containsTable(table)) {
			idvalue = PicklistData.getIdAsString(table, desc);

			if (idvalue == null) {
				String msg = "INGESTION: INFO: Unknown description value recieved - TableName = ";
				msg += table + " : DescriptionValue = " + desc;
				logger.info(msg);
				idvalue = "0";
			}

			return idvalue;
		} else {
			return desc;
		}
	}

	private static final int ROLLBACK = 1;
	private static final int STORE = 0;

	private int processResult(IngestionResult result, Document dom, ArrayList passList) {

		if (dom == null)
			return ROLLBACK;

		//FileOutputStream str = null;
		//File dir = null;

		//DirectoryMonitor monitor = (DirectoryMonitor) mon;

		if (result.isCriticalFailure())//Application or Input failure
		{
			logger.info("INGESTION: processResult: decision point : critFail");
			//String msg = "INGESTION: Critical failure during ingestion.";
			logFailure(srk, result.getResultEntity());
			//Removed call to 'writeFailed' function.
			logger.info("INGESTION: processResult: decision point : critFailA");
			return ROLLBACK; //dont ingest
		} else if (result.isNonCritical()) {
			logger.info("INGESTION: processResult: decision point : nonCritic");
			if (result.isValidationFailure()) {
				logger.info("INGESTION: processResult: decision point : nonCritic : validFail");
				String msg = "INGESTION: 'Validation Stop' during ingestion. ";
				result.addMessage(msg);
				//result.

				if (result.isValidationExcused() && !result.isInvalidSource()) {
					logger.info("INGESTION: processResult: decision point : nonCritic : vfA");
					msg = "INGESTION: Validation excused due to Application Source";
					result.addMessage(msg);

					msg = "INGESTION: Setting Deal Status to 'Incomplete'";
					result.addMessage(msg);
					logger.info("INGESTION: processResult: decision point : nonCritic : vfB");
					return STORE;
				} else if (!result.isValidationExcused()) {
					logger.info("INGESTION: processResult: decision point : nonCritic : vfAE");
					//Removed call to writeOutBound method.
					return ROLLBACK;
				}

			}//validation failure ends
			else if (result.isDuplicateInput()) {
				logger.info("INGESTION: processResult: decision point : dupInp");
				// =====================================================================
				// change made based on request from Dave Krick on Aug 30, 2002.
				// the request goes as follows: if there is no dupecheck message created
				// we should not write the file back to the broker.
				// =====================================================================
				//#DG598 if(ibdupeCheckProcessorUsed){  if dupe check enabled it's always true
				//logger.info("INGESTION: processResult: decision point : C : 11");
				if (result.isMessagesForBrokerAppended()) {
					logger.info("INGESTION: processResult: decision point : dupInp : ba");
					if (result.isFilogixLink()) {
						logger.info("INGESTION: processResult: decision point : dupInp : baF");
						//Removed call to writeOutBoundFXLink method.
					} else {
						logger.info("INGESTION: processResult: decision point : dupInp : baO");
						//Removed call to writeOutBound method.
					}
				}
				//   if( result.isMessagesForBrokerAppended() ){
				//     this.writeOutbound(dom, input, mon);
				//   }
				logger.info("INGESTION: processResult: decision point : dupInp : baC");
				return STORE;
			} else if (result.isICRulesFailure()) {
				logger.info("INGESTION: processResult: decision point : ICFail");
				if (result.isFilogixLink()) {
					logger.info("INGESTION: processResult: decision point : ICFailF");
					//Removed call to writeOutBoundFXLink method.
				} else {
					logger.info("INGESTION: processResult: decision point : ICFailO");
					//Removed call to writeOutbound function.
				}
				return STORE;
			}

		} //non-critical ends
		logger.info("INGESTION: processResult: decision point : store");
		return STORE;
	}

	private void logFailure(SessionResourceKit srk, IEntityBeanPK deal) {
		//int  MAX_MSG_LEN = 50;
		StringBuffer msg = new StringBuffer("Application Rejected - ");
		//msg.append("File Name: ").append(filename);
		if (deal != null) {
			msg.append(" DealId: ").append(deal.getId());
			msg.append(", CopyId: ").append(deal.getCopyId());
		} else {
			msg.append(" DealId: ").append("Unstored Application");
			msg.append(", CopyId: ").append("Unstored Application");
		}
		SysLogger logger = srk.getSysLogger();
		logger.info(msg.toString());
	}

	private void unlockDeal(SessionResourceKit srk, IngestionResult result, boolean inTransaction) throws Exception {
		IEntityBeanPK beanPK = result.getResultEntity();
		if (beanPK instanceof DealPK) {
			//int dealid = ((DealPK) beanPK).getId();
			int dealid = result.getLockedDealId();
			DLM.getInstance().unlock(dealid, 0, srk, inTransaction);
		}
	}

	private void notifyWorkflow(WorkflowNotifier wfn, IngestionResult parResult) throws Exception {
		IEntityBeanPK target = parResult.getResultEntity();
		logger.info("INGESTION: WFN - CP ENTERED");
		if (target == null)
			return;

		if (parResult.isCervusJob()) {
			notifyWorkflowForCervus(wfn, parResult);
			return;
		}

		int id = target.getId();
		logger.info("INGESTION: WFN - CP 1");
		if (parResult.isICRulesFailure()) {
			wfn.send(id, parResult.getNotificationType());
			return;
		}
		// #DG126 BMO-CCAPS workflow notification
		if (workflowNotificationType.equalsIgnoreCase("bmo")) {
			logger.info("INGESTION: WFN - BMO");
			if (parResult.getNotificationType() == Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL) {
				wfn.send(id, Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_B);
				return;
			}
		}

		logger.info("INGESTION: WFN - CP 1a");
		if (parResult.isCapLinkSensitive() && parResult.getNotificationType() == Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL) {
			logger.info("INGESTION: WFN - CP 2");
			wfn.send(id, Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_WCP);
			return;
		}
		logger.info("INGESTION: WFN - CP 3");
		if (!parResult.isCapLinkRequired() || parResult.isICRulesFailure()) {
			logger.info("INGESTION: WFN - CP 4");
			// Default to send Notification without CapLink
			wfn.send(id, parResult.getNotificationType());
		} else {
			// We need CapLink here
			logger.info("INGESTION: WFN - CP 5");
			if (parResult.getNotificationType() == Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL) {
				// Notify WorkFlow : New Deal with CapLink
				logger.info("INGESTION: WFN - CP 6");
				wfn.send(id, Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_WCP);
			} else {
				// Notify WorkFlow : Modify Deal with CapLink
				logger.info("INGESTION: WFN - CP 7");
				wfn.send(id, Mc.WORKFLOW_NOTIFICATION_TYPE_TASK_TRIGGER_TYPE_2_WCP);
			}
		}
		logger.info("INGESTION: WFN - CP 8");
		logger.info("INGESTION: WFN - CP EXIT");
	}

	private void notifyWorkflowForCervus(WorkflowNotifier wfn, IngestionResult parResult) throws Exception {
		IEntityBeanPK target = parResult.getResultEntity();
		logger.info("INGESTION: WFN-for-cervus - CP ENTERED");

		int id = target.getId();
		logger.info("INGESTION: WFN-for-cervus - CP 1");
		if (parResult.isICRulesFailure()) {
			wfn.send(id, Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_WUP); //#DG40
			return;
		}
		logger.info("INGESTION: WFN-for-cervus - CP 1a");
		//FXP25534 - added to new_deal_wup.  This code was never being executed so tasks were never being assigned.
		if (parResult.getNotificationType() == Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL
    	    || parResult.getNotificationType() == Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_WUP)
    	{
			logger.info("INGESTION: WFN-for-cervus - CP 2");
			wfn.send(id, Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_WUP);
			return;
		}

		// Notify WorkFlow : Modify Deal with CapLink
		logger.info("INGESTION: WFN-for-cervus - CP 7");
		wfn.send(id, Mc.WORKFLOW_NOTIFICATION_TYPE_TASK_TRIGGER_TYPE_2_WUP);

		logger.info("INGESTION: WFN-for-cervus - CP 8");
		logger.info("INGESTION: WFN-for-cervus - CP EXIT");
	}

	private void logResult(IngestionResult result) {
		String msg = "INGESTION: ";
		List mlist = result.getMessages();
		Iterator it = mlist.iterator();
		while (it.hasNext()) {
			Object m = it.next();
			logger.info(msg + m);
		}
		mlist = result.getICRulesMessages();
		it = mlist.iterator();
		while (it.hasNext()) {
			Object m = it.next();
			logger.info(msg + m);
		}
	}

	private void logSuccess(IEntityBeanPK pk) {
		SysLogger logger = srk.getSysLogger();
		try {
			DealHistoryLogger dhl = DealHistoryLogger.getInstance(srk);

			int dealId = pk.getId();
			int copyId = pk.getCopyId();
			int upid = srk.getUserProfileId();

			Deal deal = new Deal(srk, null, dealId, copyId);
			int sobid = deal.getSourceOfBusinessProfileId();

			dhl.log(dealId, copyId, dhl.dealIngestion(sobid), Mc.DEAL_TX_TYPE_INTERFACE, upid);
		} catch (Exception ex) {
			StringBuffer msg = new StringBuffer("Deal History Entry logging failed after Successful Deal Ingestion");
			msg.append("DealId: ").append(pk.getId());
			msg.append("CopyId: ").append(pk.getCopyId());
			logger.error(msg.toString());
			logger.error(ex);
		}
	}

	private void logFailure(IEntityBeanPK deal) {
		//int  MAX_MSG_LEN = 50;
		StringBuffer msg = new StringBuffer("Application Rejected - ");
		//msg.append("File Name: ").append(filename);
		if (deal != null) {
			msg.append(" DealId: ").append(deal.getId());
			msg.append(", CopyId: ").append(deal.getCopyId());
		} else {
			msg.append(" DealId: ").append("Unstored Application");
			msg.append(", CopyId: ").append("Unstored Application");
		}
		SysLogger logger = srk.getSysLogger();
		logger.info(msg.toString());
	}

	public void logDocCentralFolderCreation(SessionResourceKit srk, IEntityBeanPK pk, SourceFirmProfile sourceFirmEntity, String folderIdentifier) {
		try {
			DealHistoryLogger dhl = DealHistoryLogger.getInstance(srk);

			int dealId = pk.getId();
			int copyId = pk.getCopyId();
			int upid = srk.getExpressState().getUserProfileId();

			//Deal deal = new Deal(srk, null, dealId, copyId);

			dhl.log(dealId, copyId, dhl.docCentralFolderCreationInfo(sourceFirmEntity, folderIdentifier), Mc.DEAL_TX_TYPE_DOCCENTRAL_FOLDER_CREATION, upid);
		} catch (Exception ex) {
			StringBuffer msg = new StringBuffer("Deal History Entry logging failed after Successful DocCentral Folder creation");
			msg.append("DealId: ").append(pk.getId());
			msg.append("CopyId: ").append(pk.getCopyId());
			msg.append("FolderIdentifier: ").append(folderIdentifier);
			logger.error(msg.toString(), ex);
		}

	}

}
