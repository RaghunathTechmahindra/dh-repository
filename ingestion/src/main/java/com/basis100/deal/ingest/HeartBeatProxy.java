package com.basis100.deal.ingest;

import java.util.Date;

public class HeartBeatProxy implements Runnable
{
   private Thread beat;
   
   static
   {
      // load the library
      System.loadLibrary("JCppLibs");
   }

   public HeartBeatProxy()
   {
     beat = new Thread(this, "com.basis100.ingestion HeartBeatProxy Thread = beat");
     beat.start();
   }

   public native void MapHeartbeat(int daemonId);
   public native void UpdateHeartbeat();
   public native void UnmapHeartbeat();

   // load the JNI library once per class


   public void run()
   {
      this.MapHeartbeat(1);

      try
      {
        while(true)
        {
            Date d = new Date();
            this.UpdateHeartbeat();
            System.out.println("Beat..." + d.toLocaleString());
            beat.sleep(30000);
        }
      }
      catch(InterruptedException ie)
      {
         ie.printStackTrace();
         this.UnmapHeartbeat();
      }

      this.UnmapHeartbeat();



   }

} 



