package com.basis100.deal.ingest;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.basis100.deal.ingest.event.FileReceptionListener;
import com.basis100.deal.ingest.event.FilesRecievedEvent;
import com.basis100.util.collections.Queue;

 /**
 *  <pre>
 *   Owner of the ingestion queue. Listener for event signaling reception of files
 *   in the mailbox. Places the files on the ingestion queue. The files are removed by the
 *   IngestonManager
 *
 *  </pre>
 */

public class FileReciever implements FileReceptionListener
{

  private Queue queue;
  private static final Log _log = LogFactory.getLog(FileReciever.class);
  
  public FileReciever(Collection monitors)throws IOException, FileIngestionException
  {

       this.queue = new Queue();

       Iterator it = monitors.iterator();

       while(it.hasNext())
       {
          DirectoryMonitor monitor = (DirectoryMonitor)it.next();
          monitor.addListener(this);
          monitor.startMonitor();
       }
  }
  
 /**
  *  Recieve notification from the DirectoryMonitor that files have been recieved in the
  *  mailbox. The event contains an array of File references. Place the files on the
  *  ingestion queue.
  */
  public void filesRecieved(FilesRecievedEvent e)
  {
     System.out.println("Files recieved:");
     _log.trace("Files recieved:");
     File[] f = e.getFiles();

     for(int i = 0; i < f.length; i++)
     {
       System.out.println("\t" + f[i].getName());
       _log.trace("\t" + f[i].getName());
       addToQueue(f[i]);
     }
  }


  private synchronized void addToQueue(File toAdd)
  {
     queue.put(toAdd);
     notify();
  }
  /**
   *  get any available files from the ingestion queue
   *  @returns a Deal file
   */
  public synchronized File getFromQueue() throws InterruptedException
  {
    while(queue.isEmpty())
    {
       wait();
    }

    File ret = (File)queue.get();
    return ret;

  }


}



