package com.basis100.deal.ingest;

import java.util.Collection;
import java.util.Date;

import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.IngestionQueue;
import com.basis100.deal.security.DLM;
import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.email.EmailSender;

public class FCXIngestionManager implements Runnable {

	public static final int DEFAULT_USER_ID = 0;
	public static final int DEFAULT_INTERVAL = 4;
	public static final int DEFAULT_LOCKEDDEALRETRYWAITINSECONDS = 600;

	private Date dateStarted;
	private Logger logger;
	private int userId;
	private int interval;
	private int lockedDealRetryWaitInSeconds;
	private long intervalInMilliseconds;
	private SessionResourceKit srk;
	private Boolean terminated;
	protected DocumentBuilderFactory documentBuilderFactory;

	public FCXIngestionManager() throws IngestionException {
		this.dateStarted = new Date();
		this.logger = LoggerFactory.getLogger(this.getClass());

		try {
			userId = Integer.parseInt(PropertiesCache.getInstance().getProperty(-1, "fcxingestion.userid"));
			logger.info("com.filogix.ingestion.fcx.userid=" + userId + " is loaded.");
		} catch (Exception e) {
			userId = DEFAULT_USER_ID;
			logger.info("No valid com.filogix.ingestion.fcx.userid specified. Default userId=" + DEFAULT_USER_ID + " is used.");
		}

		try {
			interval = Integer.parseInt(PropertiesCache.getInstance().getProperty(-1, "fcxingestion.sleepinterval"));
			logger.info("com.filogix.ingestion.fcx.interval=" + interval + " seconds is loaded.");
		} catch (Exception e) {
			interval = DEFAULT_INTERVAL;
			logger.info("No valid com.filogix.ingestion.fcx.interval specified. Default interval=" + DEFAULT_INTERVAL + " seconds is used.");
		}
		intervalInMilliseconds = interval * 1000;

		try {
			lockedDealRetryWaitInSeconds = Integer.parseInt(PropertiesCache.getInstance().getProperty(-1, "fcxingestion.lockedDeal.retry.time"));
			logger.info("fcxingestion.lockedDeal.retry.time=" + lockedDealRetryWaitInSeconds + " seconds is loaded.");
		} catch (Exception e) {
			lockedDealRetryWaitInSeconds = DEFAULT_LOCKEDDEALRETRYWAITINSECONDS;
			logger.info("No valid fcxingestion.lockedDeal.retry.time specified. Default value=" + DEFAULT_LOCKEDDEALRETRYWAITINSECONDS + " seconds is used.");
		}

		this.srk = new SessionResourceKit(String.valueOf(userId));
		this.srk.setAuditOn(false);

		this.terminated = new Boolean(false);
		this.documentBuilderFactory = DocumentBuilderFactory.newInstance();

		//Copied from old implementation
		ResourceManager.init();
		PicklistData.init();
		new BXResources();
		try {
			DLM.init(this.srk, false);
		} catch (Exception e) {
			logger.error("Failed to instantiate IngestionManager." + e);
			throw new IngestionException("Errors while initializing Ingestion Manager", e);
		}

		try {
			Collection<IngestionQueue> c = new IngestionQueue(srk).findByStatus(IngestionQueue.STATUS_IN_PROGRESS);
			if (c.size() > 0) {
				String message = "There is already record(s) with 'In Progress' status in queue while starting up Ingestion Manager.";
				logger.error(message);
				EmailSender.sendAlertEmail("IG", null, null, message);
				//log error but continue
			}
		} catch (Exception e) {
			logger.error("Failed to instantiate IngestionManager." + e);
			throw new IngestionException("Errors while initializing Ingestion Manager", e);
		}

	}

	public void run() {
		System.out.println("IngestionManager starts");

		while (!terminated) {

			// Wait for a while
			try {
				Thread.sleep(intervalInMilliseconds);
			} catch (InterruptedException ie) {
				logger.info("IngestionManager sleep is interrupted.", ie);
			}

			// Pick up next one in queue
			IngestionQueue queue;
			try {
				queue = new IngestionQueue(srk).findNextAvailable(lockedDealRetryWaitInSeconds);
				if (queue == null)
					continue;
				queue.setStatusDateTimestamp(new Date());
				queue.setIngestionQueueStatusId(IngestionQueue.STATUS_IN_PROGRESS);
				queue.ejbStore();
				logger.info("IngestionQueueId=" + queue.getIngestionQueueId() + " is being processed.");
			} catch (Exception e) {
				logger.error("Failed to obtain next one in queue.", e);
				continue;
			}

			// Ingest
			int status;
			try {
				logger.info("===================================================");
				logger.info("start Ingestion");
				logger.info("IngestionQueueId = " + queue.getIngestionQueueId());
				logger.info("SourceApplicationId = " + queue.getSourceApplicationId());
				logger.info("===================================================");
				
				long startTime = System.nanoTime();
				FCXIngestionWorker ingestion = new FCXIngestionWorker(this, queue.getSourceApplicationId(), queue.getReceivedTimestamp(), queue.getDealXml(), queue.getSenderChannel(), queue.getReceiverChannel());
				status = ingestion.ingest();
				long estimatedDuration = System.nanoTime() - startTime;
				System.out.println("INGESTION: Total Time (millisec): " + estimatedDuration);
				logger.info("INGESTION: Total Time (millisec): " + estimatedDuration);
				logger.info("IngestionQueueId=" + queue.getIngestionQueueId() + " is processed with status=" + status);
			} catch (Exception e) {
				logger.error("Failed to ingest ingestionQueueId=" + queue.getIngestionQueueId(), e);
				status = IngestionQueue.STATUS_ERROR;
			}

			// Update/Delete queue with new status
			try {
				if (status == IngestionQueue.STATUS_SUCCESS) {
					queue.ejbRemove();
					logger.info("IngestionQueueId=" + queue.getIngestionQueueId() + " is done successfully and removed.");
					Collection<IngestionQueue> c = new IngestionQueue(srk).findBySourceApplicationId(queue.getSourceApplicationId(), IngestionQueue.STATUS_ERROR, queue.getReceivedTimestamp());
					for (IngestionQueue iq : c) {
						iq.ejbRemove();
						logger.info("IngestionQueueId=" + iq.getIngestionQueueId() + " is also removed.");
					}
				} else if (status == IngestionQueue.STATUS_ERROR) {
					queue.setStatusDateTimestamp(new Date());
					queue.setIngestionQueueStatusId(status);
					queue.ejbStore();
					logger.info("IngestionQueueId=" + queue.getIngestionQueueId() + " cannot be processed due to errors.");

					String message = "Errors occurred while processing ingestionid=" + queue.getIngestionQueueId();
					logger.error(message);
					EmailSender.sendAlertEmail("IG", null, null, message);
				} else if (status == IngestionQueue.STATUS_DEAL_LOCKED) {
					queue.setStatusDateTimestamp(new Date());
					queue.setIngestionQueueStatusId(status);
					queue.ejbStore();
					logger.info("IngestionQueueId=" + queue.getIngestionQueueId() + " cannot be processed due to deal locked.");
				} else {
					logger.error("Unexpected result from Ingestion.ingest(). IngestionQueue: " + queue.toString());
				}
			} catch (Exception e) {
				logger.error("Failed to update ingestionQueueId=" + queue.getIngestionQueueId(), e);
				continue;
			}

		}

		System.out.println("IngestionManager ends");
	}

	public Date getDateStarted() {
		return dateStarted;
	}

	public int getUserId() {
		return userId;
	}

	public static void main(String[] args) throws IngestionException {
		Thread thread = new Thread(new FCXIngestionManager());
		thread.start();
	}

}
