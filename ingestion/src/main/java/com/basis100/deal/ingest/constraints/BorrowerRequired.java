package com.basis100.deal.ingest.constraints;

import java.util.List;
import com.basis100.deal.xml.EntityNodeAssociation;
import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.ingest.RequiredElementException;
import com.basis100.resources.SessionResourceKit;
import com.basis100.xml.DomUtil;



public class BorrowerRequired implements RequiredConstraint
{
  public void execute(SessionResourceKit srk, EntityNodeAssociation ass )
  throws Exception, RequiredElementException
  {
    DealEntity de = ass.getEntity();

    if(de == null)
      return ; // called on wrong type

    if(de.getClassId() == ClassId.DEAL)
    {
      List l = DomUtil.getNamedChildren(ass.getNode(), "Borrower");

      if(l.isEmpty())
         throw new RequiredElementException("Deal has no borrowers");
    }
  }
}
