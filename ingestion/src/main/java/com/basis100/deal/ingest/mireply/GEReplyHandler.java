package com.basis100.deal.ingest.mireply;

/**
* 11/Feb/2005 DVG #DG136 #949 Cervus BRD2 - Low ratio GE responses
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.ingest.DirectoryMonitor;
import com.basis100.deal.ingest.FileIngestionException;
import com.basis100.deal.ingest.IngestionResult;
import com.basis100.deal.ingest.SourceMonitor;
import com.basis100.deal.miprocess.MIProcessHandler;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.security.DLM;
import com.basis100.deal.util.DBA;
import com.basis100.deal.util.TypeConverter;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.util.file.IOUtil;
import com.basis100.workflow.notification.WorkflowNotifier;
import com.basis100.xml.DomUtil;
import com.basis100.xml.XmlToolKit;
import com.basis100.xml.XmlWriter;

/**
 *    An implementation of Ingestion handler that absorbs a stores cmhc reply files.
 */
public class GEReplyHandler extends MIReplyHandler
{

  public SysLogger logger;


  public static boolean debug = true;

  public static String ERROR        = "E";
  public static String CANCELLED    = "C";
  public static String REFER        = "R";
  public static String APPROVED     = "A";
  public static String DECLINED     = "D";
  public final static String DECLINEDN    = "N";  //#DG136

  //------> Rel 3.1 - Sasa
  //  public static String AMOUNT_DUE = "GE Capital Fee";
  //  public static String PST        = "GE Capital PST on Premium";
  //  public static String PREMIUM    = "GE Capital Premium";

  public static int AMOUNT_DUE = Mc.FEE_TYPE_GE_CAPITAL_FEE;
  public static int PST        = Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM;
  public static int PREMIUM    = Mc.FEE_TYPE_GE_CAPITAL_PREMIUM;
  //------> End of Rel 3.1 - Sasa

  private String cervusCodes[] = {"0319","0320","0321","0322","0323","0406","0407",
                           "0408", "0409", "0410", "0411", "0412",
                           "0413", "0414", "0415", "0416", "0417", "0418",
                           "0419", "0420", "0420", "0421", "0422"};

  /**
   *   Constructs an application handler.
   */
  public GEReplyHandler() throws FileIngestionException
  {
    logger = ResourceManager.getSysLogger("GEReplyHandler");
  }

  public File preProcess(File file)throws Exception
  {
    return file;
  }

  public IngestionResult storeDocument(org.w3c.dom.Document dom, SessionResourceKit srk, SourceMonitor source)
        throws Exception
  {
    this.dcm = CalcMonitor.getMonitor(srk);
    this.srk = srk;

    IngestionResult result = new IngestionResult();

    String openMIResponse = null;
    //boolean preResolution = false;

    //get the dealId:copyId string
    String idString = DomUtil.getElementText(DomUtil.getFirstNamedDescendant(dom, "dealId"));

    if(idString == null)
    {
      logger.error("INGESTION: MIREPLY - GE: Incoming data error: DealId not present: " + idString);
      result.isInvalidInput(true);
      return result;
    }

    //recieved and calculated values
    int dealId = -99;
    //int gCopyId = -99;

    dealId = TypeConverter.intTypeFrom(idString.trim(),-99);

    if(dealId == -99)
    {
      logger.error("INGESTION: MIREPLY - GE: DealId string not a valid number: " +idString);
      result.isInvalidInput(true);
      return result;
    }

    //ZIVKO:NOTE: make sure that deal is not locked.
    DLM dlm = DLM.getInstance();
    if(dlm.isLocked(dealId,0,srk) ){
      logger.info("INGESTION: deal " + dealId + "  has been locked.");
      result.setDealLocked(true);
      //4.3GR Item MI Reponse Alert STARTS <--  CIBC CR63
      dlm.setMIResponseExpected(srk, dealId, "Y");
      //4.3GR Item MI Reponse Alert ENDS
      return result;
    }else{
      dlm.lock(dealId, 0, srk);
      result.setLockedDealId( dealId );
    }

    MasterDeal md = null;
    Deal deal = null;
    try
    {
      md = new MasterDeal(srk,dcm,dealId);
      
      //4.3GR, Dec 23, 2009, 
      //changed gold copy to Recommended Scenario
      deal = new Deal(srk, dcm);
      deal = deal.findByRecommendedScenario(new DealPK(dealId, -1), true);
      
      if(deal == null)
      {
       result.isInvalidInput(true);
       throw new FileIngestionException("Invalid dealId or copyId");
      }
      else
      {
       result.setResultEntity(deal.getPk());
      }
    }
    catch(Exception e)
    {
      logger.error("INGESTION: MIREPLY - GE: Invalid Deal.dealId recieved - dealId:" + dealId );

      result.isInvalidInput(true);
      return result;
    }

    String responseCode = readResponseString(dom);

    try
    {

      if(updateOK(responseCode, openMIResponse))
      {
        handleReply(result, deal, md, dom, responseCode, openMIResponse);
        //dcm.calc();
      }
      else
      {
        logger.info("INGESTION: MIREPLY - GE: Not proceeding with update. ResponseCode = " + responseCode + " openMIResponse: " + openMIResponse);
      }

    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown Error processing GE MI reply.";

      result.addMessage(msg);
      result.isApplicationError(true);

      logger.error(msg);
      logger.error(e);

    }


    return result;
  }

   /**
   *  performs updates and parsing duties on the MIReply: <br>
   *
   *
   */
  private void handleReply(IngestionResult result, Deal deal, MasterDeal md,
                            Document dom, String resp, String flag) throws Exception
  {

    String refStr = readReferenceNumber( dom );
    long refNum = TypeConverter.longTypeFrom(refStr, 0);   //safe method (filters nulls)

    //represent the commonly used null or M  deal.openMIResponseFlag
    boolean NULL_OR_M = ((flag == null) || flag.equals("M"));
    boolean NOT_M =  ((flag == null) || flag.equals("C"));

    String openMIResponseFlag = null;
    int status = -1;

    try {
      if(resp == null) {
        result.isInvalidInput(true);
        throw new Exception("Null responseCode in Post Mapping GE MIReply File.");
      }
      else if(refNum <= 0 && resp.equals(ERROR))
        status = Mc.MI_STATUS_CRITICAL_ERRORS_REC_FROM_INSURER;
      else if(refNum > 0 && resp.equals(ERROR))
        status = Mc.MI_STATUS_ERRORS_RECEIVED_FROM_INSURER;
      else if(resp.equals(CANCELLED) && NOT_M )
        status = Mc.MI_STATUS_CANCELLATION_CONFIRMATION_REC;
      else if(resp.equals(REFER) && NULL_OR_M )
        status = Mc.MI_STATUS_MANUAL_UNDERWRITING_REPLY;
      else if(resp.equals(APPROVED) && NULL_OR_M )
        status = Mc.MI_STATUS_APPROVAL_RECEIVED;
      else if( (resp.equals(DECLINED) || resp.equals(DECLINEDN)) && NULL_OR_M )  //#DG136
        status = Mc.MI_STATUS_DENIED_RECEIVED;
      else //the response code is not null but is not recognized
      {
        openMIResponseFlag = deal.getOpenMIResponseFlag();   //so dont clear the flag
        status = deal.getMIStatusId();                       // and dont set the status
      }
    }
    catch(Exception e) {
      String msg = e.getMessage();
      if(msg == null) msg = " Unknown error. ";
      logger.error("INGESTION: MIREPLY - GE: Error setting MIStatus due to: " + msg);
    }

    String responseStr = readRecordString(dom);
    if(responseStr == null) {
      logger.error("INGESTION: MIREPLY - GE: Mapping error - no recordString present.");
      logger.info("INGESTION: MIREPLY - GE:  The GE 'recordString' = the entire application as a String.");
    }
    else {
      responseStr = buildMIResponseField(responseStr, deal);
    }

    String pn = deal.getMIPolicyNumber();
    if(refNum > 0  && !refStr.equals(pn)) //don't need null check because > 0 implies - not null.
        pn = refStr;

    (MIProcessHandler.getMIAttributesPropogator()).propogateMIAttributes(
                srk, deal, status, MIProcessHandler.NO_MIIndicatorId,
                MIProcessHandler.NO_MIPremiumAmount, MIProcessHandler.NO_MortgageInsurerId, pn,
                MIProcessHandler.NO_MIPayorId, MIProcessHandler.NO_MITypeId, MIProcessHandler.NO_MIUpfront,
                MIProcessHandler.NO_MICommunicationFlag, openMIResponseFlag , MIProcessHandler.NO_MIExistingPolicyNumber,
                MIProcessHandler.NO_MIComments, responseStr, MIProcessHandler.NO_PreQualMICertNum,
                MIProcessHandler.NO_CMHCPolicyNumber, pn,
                MIProcessHandler.NO_LOCAmortizationMonths,		// Rel 3.1 - Sasa
                MIProcessHandler.NO_LOCInterestOnlyMaturityDate,	// Rel 3.1 - Sasa
                MIProcessHandler.NO_MIFeeAmount,					// Rel3.1.1 - Serghei
                MIProcessHandler.NO_MIPremiumPST,					// Rel3.1.1 - Serghei
                MIProcessHandler.NO_MIPolicyNumAIGUG,				// Rel3.1.1 - Serghei
                MIProcessHandler.NO_MIPolicyNumPMI
                );


    // Update all copies of the deal with required info
    if(status == Mc.MI_STATUS_APPROVAL_RECEIVED) {
        double premAmt = -1;		// feeTypeId = 17 - Genworth Premium
        double taxAmt  = -1;		// feeTypeId = 19 - Genworth PST on Premium
        double amtDue  = -1;		// feeTypeId = 15 - Genworth Fee

        // Premium Amount, feeAmount for feeTypeId = 17
        try {
          premAmt = readPremiumAmt(dom);
          if(premAmt != deal.getMIPremiumAmount()) 
        	  status = Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED;
          else
        	  status = MIProcessHandler.NO_MIStatusId;
        }
        catch(Exception ex) {
          String msg = ex.getMessage();
          if(msg == null) msg = "Unknown Error Obtaining premium from GE MI reply.";
          
          result.addMessage(msg);
          result.isInvalidInput(true);

          logger.error("INGESTION: MIREPLY - GE: premium: " + msg);
          logger.error(ex);
        }

        // ================================================================================
        // zivko:marker   here is the spot where we want to put cervus functionality
        // ================================================================================
        String validateCervusCodes = PropertiesCache.getInstance().getProperty(
                srk.getExpressState().getDealInstitutionId(),
                "com.filogix.ingestion.cervus.ge.reply.codes","N");
        if( validateCervusCodes.equalsIgnoreCase("y") && isCervusCodePresent(dom) ){
          status = Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED;
        }
        
        //================================================================================
        // Rel 3.1 - Sasa : New fields to be propagated
        //================================================================================
        int locAmortizationMonths = MIProcessHandler.NO_LOCAmortizationMonths;
        long locInterestOnlyMaturityDate = MIProcessHandler.NO_LOCInterestOnlyMaturityDate;
        try {
        	locAmortizationMonths = readLocAmortizationMonths(dom);
        	locInterestOnlyMaturityDate = readLocInterestOnlyMaturityDate(dom);
        }
        catch(Exception ex) {
            String msg = ex.getMessage();
            if(msg == null) msg = "Unknown Error obtaining LOC fields from GE MI reply.";

            result.addMessage(msg);
            result.isInvalidInput(true);

            logger.error("INGESTION: MIREPLY - GE: LOC fields: " + msg);
            logger.error(ex);        	
        }
        //================================================================================
        // End of Rel 3.1 - Sasa
        //================================================================================
        
        (MIProcessHandler.getMIAttributesPropogator()).propogateMIAttributes(
                srk, deal, status, MIProcessHandler.NO_MIIndicatorId,
                premAmt, MIProcessHandler.NO_MortgageInsurerId, MIProcessHandler.NO_MIPolicyNumber,
                MIProcessHandler.NO_MIPayorId, MIProcessHandler.NO_MITypeId, MIProcessHandler.NO_MIUpfront,
                MIProcessHandler.NO_MICommunicationFlag, MIProcessHandler.NO_OpenMIResponseFlag, 
                MIProcessHandler.NO_MIExistingPolicyNumber, MIProcessHandler.NO_MIComments, MIProcessHandler.NO_MIResponse, 
                MIProcessHandler.NO_PreQualMICertNum, MIProcessHandler.NO_CMHCPolicyNumber, MIProcessHandler.NO_GEPolicyNumber,
                locAmortizationMonths,			// Rel 3.1 - Sasa
                locInterestOnlyMaturityDate,		    // Rel 3.1 - Sasa
                amtDue,									// Rel. 3.1.1 - Serghei
                taxAmt,									// Rel. 3.1.1 - Serghei
                MIProcessHandler.NO_MIPolicyNumAIGUG,	// Rel. 3.1.1 - Serghei
                MIProcessHandler.NO_MIPolicyNumPMI
        );

        //================================================================================
        // Processing Fees
        //================================================================================
        // Tax Amount, feeAmount for feeTypeId = 19
        try {
          taxAmt = readTaxAmt(dom);
        }
        catch(Exception ex) {
          String msg = ex.getMessage();
          if(msg == null) msg = "Unknown Error Obtaining PST tax amount from GE MI reply.";

          result.addMessage(msg);
          result.isInvalidInput(true);

          logger.error("INGESTION: MIREPLY - GE: PST:" + msg);
          logger.error(ex);
        }

        // Amount Due, feeAmount for feeTypeId = 15
        try {
          amtDue  = readAmountDue(dom);
        }
        catch(Exception ex) {
          String msg = ex.getMessage();
          if(msg == null) msg = "Unknown Error Obtaining fee amount from GE MI reply..";

          result.addMessage(msg);
          result.isInvalidInput(true);

          logger.error("INGESTION: MIREPLY - GE: fee amount: " + msg);
          logger.error(ex);
        }
        
        // Processing each copy of the deal
        try {
          int dealId = deal.getDealId();

          Collection copyIds = md.getCopyIds();
          Integer curcpy = null;

          Iterator dit = copyIds.iterator();
          while(dit.hasNext()) {

            curcpy = (Integer)dit.next();
            Deal currentDeal = null;

            try {
               currentDeal = new Deal(srk, dcm, dealId, curcpy.intValue());
            }
            catch(com.basis100.entity.FinderException fe) {
               logger.info("INGESTION: MIREPLY - GE: Benign FinderException while building fees for copyId: " + curcpy);
               continue;  //the copy no longer exists
            }

            //================================================================================================
            // Tracker issue 259 says that before we build new fees during the
            // process of ingestion, we have to wipe out all fees of certain types.
            // The following piece of code does that.
            // note posted by Zivko: Feb 04, 2002
            //================================================================================================
/* Rel. 3.1.1 - Serghei            int[] typesToDelete = { Mc.FEE_TYPE_CMHC_PREMIUM,
                            Mc.FEE_TYPE_CMHC_PREMIUM_PAYABLE,
                            Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM,
                            Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM_PAYABLE,
                            Mc.FEE_TYPE_CMHC_FEE,
                            Mc.FEE_TYPE_CMHC_FEE_PAYABLE,
                            Mc.FEE_TYPE_GE_CAPITAL_FEE,
                            Mc.FEE_TYPE_GE_CAPITAL_FEE_PAYABLE,
                            Mc.FEE_TYPE_GE_CAPITAL_PREMIUM,
                            Mc.FEE_TYPE_GE_CAPITAL_PREMIUM_PAYABLE,
                            Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM,
                            Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM_PAYABLE };

            MIProcessHandler.getMIAttributesPropogator().propogateDealFeeRemoval(srk,currentDeal,typesToDelete);*/
            MIProcessHandler.getMIAttributesPropogator().removeFeeFromDeal(srk, currentDeal);
            //=================================================================================================
            // end of update
            //=================================================================================================
            MIFeeBuilder.buildFee(srk, dcm, currentDeal, Mc.FEE_TYPE_GE_CAPITAL_PREMIUM, premAmt);
            MIFeeBuilder.buildFee(srk, dcm, currentDeal, Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM, taxAmt);
            MIFeeBuilder.buildFee(srk, dcm, currentDeal, Mc.FEE_TYPE_GE_CAPITAL_FEE, amtDue);
          }
        }
        catch(Exception fe)
        {
          String msg = fe.getMessage();

          if(msg == null) msg = "Unknown error. ";

          logger.error("INGESTION: MIREPLY - GE: Error building fees due to: " + msg);
          logger.error(fe);
        }
    }
  }


  private void updateApproval(IngestionResult result, Deal deal, List deals, Document dom, String rf)
  {
      double premAmt = -1;
      double taxAmt  = -1;
      double amtDue  = -1;

      double currentPrem = -1;
      boolean changedPremium = false;

      try
      {
        premAmt = readPremiumAmt(dom);

        currentPrem = deal.getMIPremiumAmount();

        if(currentPrem != premAmt) changedPremium = true;

      }
      catch(Exception ex)
      {
        String msg = ex.getMessage();

        if(msg == null) msg = "Unknown Error Obtaining premium from GE MI reply.";

        result.addMessage(msg);
        result.isInvalidInput(true);

        logger.error("INGESTION: MIREPLY - GE: premium: " + msg);
        logger.error(ex);
      }

      try
      {
        taxAmt  = readTaxAmt(dom);
      }
      catch(Exception ex)
      {
        String msg = ex.getMessage();

        if(msg == null) msg = "Unknown Error Obtaining PST tax amount from GE MI reply.";

        result.addMessage(msg);
        result.isInvalidInput(true);

        logger.error("INGESTION: MIREPLY - GE: PST:" + msg);
        logger.error(ex);
      }

      try
      {
        amtDue  = readAmountDue(dom);
      }
      catch(Exception ex)
      {
        String msg = ex.getMessage();

        if(msg == null) msg = "Unknown Error Obtaining fee amount from GE MI reply..";

        result.addMessage(msg);
        result.isInvalidInput(true);

        logger.error("INGESTION: MIREPLY - GE: fee amount: " + msg);
        logger.error(ex);
      }

      try
      {
        ListIterator it = deals.listIterator();
        Deal current = null;

        while(it.hasNext())
        {
          current = (Deal)it.next();
          current.setMIPremiumAmount(premAmt);

          if(changedPremium)
           current.setMIStatusId(Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED);
          else
           current.setMIStatusId(Mc.MI_STATUS_APPROVAL_RECEIVED);


          MIFeeBuilder.buildFee(srk, dcm, current, Mc.FEE_TYPE_GE_CAPITAL_PREMIUM, premAmt);
          MIFeeBuilder.buildFee(srk, dcm, current, Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM, taxAmt);
          MIFeeBuilder.buildFee(srk, dcm, current, Mc.FEE_TYPE_GE_CAPITAL_FEE, amtDue);
        }

      }
      catch(Exception fe)
      {
        String msg = fe.getMessage();

        if(msg == null) msg = "Unknown error. ";

        logger.error("INGESTION: MIREPLY - GE: Error building fees due to: " + msg);
        logger.error(fe);
      }

  }


  public void notifyWorkflow(WorkflowNotifier wfn, IngestionResult parResult) throws Exception
  {
     IEntityBeanPK deal = parResult.getResultEntity();
     if(deal != null)
     {
        int id = deal.getId();
        logger.debug("INGESTION: MIREPLY - GE: Sending Notification record.");
        wfn.send(id,Mc.WORKFLOW_NOTIFICATION_TYPE_MI_REPLY);
     }
     //otherwise ingestion failed
  }

  public void logSuccess(SessionResourceKit srk, IEntityBeanPK dealpk, String filename)
  {
    if(dealpk != null)
    logger.info("INGESTION: MIREPLY - GE: Success." + dealpk.getId());

    try   //log to history:
    {
      MIProcessHandler.getInstance().updateOnReply((DealPK)dealpk,srk);
    }
    catch(Exception e)
    {
      logger.error("INGESTION: MIREPLY - GE: Error logging deal history for: " + dealpk.getId());
      logger.error(e);
    }
  }

  public void logFailure(SessionResourceKit srk, IEntityBeanPK deal, String filename)
  {
    if(deal != null)
     logger.info("INGESTION: MIREPLY - GE: Failed." + deal.getId());
    else
     logger.info("INGESTION: MIREPLY - GE: Failed. ( DealPK is null at log time.)");
  }

  //--DocCentral_FolderCreate--21Sep--2004--start--//
  public void logDocCentralFolderCreation(SessionResourceKit srk,
                                        IEntityBeanPK pk,
                                        SourceFirmProfile sourceFirmEntity,
                                        String folderIdentifier) {;}
  //--DocCentral_FolderCreate--21Sep--2004--end--//

  public int processResult(IngestionResult res, File input, Document dom, SourceMonitor mailbox) throws Exception
  {
     if(res.isApplicationError() || res.isDuplicateInput() || res.isCriticalFailure())
     {
        FileOutputStream str = null;
        DirectoryMonitor monitor = (DirectoryMonitor)mailbox;
        try
        {
          XmlWriter writer = XmlToolKit.getInstance().getXmlWriter();

          File dir = monitor.getErrorDirectory();

          String filename = input.getName();

          str = IOUtil.constructSafeFOS(dir,filename,monitor.getErrorExtension());

          writer.setPrintEmptyElements(false);

          writer.print(new OutputStreamWriter(str),dom,writer.UNFORMATTED);
        }
        catch(Exception e)
        {
          String err = "INGESTION: Failed to write to 'Error' directory - Exception Message =  " + e.getMessage();
          logger.error(err.toString());
        }
        finally
        {
          try{str.close();}catch(Exception e2){;}
        }

       return ROLLBACK;
     }

     return STORE;
  }


  private String readResponseString(Document doc)
  {
    Element rs = DomUtil.getFirstNamedDescendant(doc,"responseIndicator") ;

    if(rs == null) return null;

    return DomUtil.getElementText(rs);
  }

  //
  private String readReferenceNumber(Document doc)
  {
    Element rn = DomUtil.getFirstNamedDescendant(doc,"GEReferenceNumber") ;

    if(rn == null) return null;

    String elementText = DomUtil.getElementText(rn);

    return elementText;
  }

  /**
   *  Encapsulates logic for permitting update process.
   *  Once a reply has been recieved and parsed and the miStatus relating to the
   *  reply determined (though not saved to db) indicates whether the full database
   *  update can proceed.
   *
   *  @param flag - miOpenFlag
   *  if flag is M -> this means a Cancelled is pending return and a
   *  change has been sent out.
   *  if  flag = C -> this mean a cancellation is pending return.
   */
  private boolean updateOK(String resp, String flag) {
    if(resp != null && resp.equals(CANCELLED)) {
      if(flag != null && flag.equals("M")) {
        return false;
      }
      else if(flag != null && flag.equals("C")) {
        return true;
      }
    }
    return true;
  }

  //
  private double readPremiumAmt(Document dom) throws Exception
  {
	 // -------> Rel 3.1 - Sasa
     // Element el = DomUtil.findElementWithText(dom,PREMIUM,true);
	 Element el = DomUtil.findElementWithText(dom, Integer.toString(PREMIUM), true);
	 // -------> End of Rel 3.1 - Sasa

     if(el == null) 
    	 return 0d;
     else {
    	 double amt = findFromType(el);
    	 if(amt < 0d)
    		 throw new Exception("Unable to read amount for MI type: " + PREMIUM + " in GE MIReply file.");
    	 return amt;
     }
  }
  
  //
  private double readTaxAmt(Document dom)throws Exception {
	 // -------> Rel 3.1 - Sasa
     // Element el = DomUtil.findElementWithText(dom,PST,true);
     Element el = DomUtil.findElementWithText(dom, Integer.toString(PST), true);
     // -------> End of Rel 3.1 - Sasa

     if(el == null) 
    	 return 0d;
     else {
    	 double amt = findFromType(el);
    	 if(amt < 0d)
    		 throw new Exception("Unable to read amount for MI type: " + PST + " in GE MIReply file.");
    	 return amt;
     }
  }
  
  /**
   * Rel 3.1 - Sasa
   * 
   * @param doc
   * @return
   * @throws Exception
   */
  private int readLocAmortizationMonths(Document doc) throws Exception {
	 Element element = DomUtil.getFirstNamedDescendant(doc,"locAmortizationMonths") ;

	 if(element == null) 
		 return MIProcessHandler.NO_LOCAmortizationMonths;
	 else {
		 int value = Integer.parseInt( DomUtil.getElementText(element));
		 if( value < 0)
			 throw new Exception("Unable to read locAmortizationMonths in GE MIReply file.");
		 return value;
	 }
  }
  
  
/**
 * Rel 3.1 - Sasa
 * 
 * Returns long, number of milliseconds since January 1, 1970, 00:00:00 GMT representing 
 * the original Date object. This allows the same design patern to be used for non-primitive
 * Date type, minimizin a code change. Using this approach, variable passing value of
 * LOCInterestOnlyMaturityDate field may have a primitive value of static constant 
 * NO_LOCInterestOnlyMaturityDate, and still be able to pass actual date value represented
 * with this long.
 * 
 * @param doc
 * @return long locInterestOnlyMaturityDate
 * @throws Exception
 */
  private long readLocInterestOnlyMaturityDate(Document doc) throws Exception {
	  Element element = DomUtil.getFirstNamedDescendant(doc, "locInterestOnlyMaturityDate");
	  
	  long value = MIProcessHandler.NO_LOCInterestOnlyMaturityDate;
	  if( element != null ) {
		  try{ 
			  String sDate = DomUtil.getElementText(element);
			  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			  Date date = sdf.parse(sDate);
			  value = date.getTime();
		  }
		  catch( Exception ex ) {
			  throw new Exception("Unable to read locInterestOnlyMaturityDate in GE MIReply file.");
		  }
	  }	
	  return value;
  }

  /**
   * Looks for expected Elements in the Document Object Model and return the contained
   * data net of textual characters as a double
   */
  private double readAmountDue(Document dom)throws Exception
  {
	// Rel 3.1 - Sasa
    // Element el = DomUtil.findElementWithText(dom,AMOUNT_DUE,true);
    Element el = DomUtil.findElementWithText(dom, Integer.toString(AMOUNT_DUE), true);
    // End of Rel 3.1 - Sasa

    if(el == null) return 0.0;

    double amt = findFromType(el);

    if(amt < 0)
       throw new Exception("Unable to read amount for MI type: " + AMOUNT_DUE + " in GE MIReply file.");

    return amt;

  }
  /**
   * Look for expected Elements in the Document Object Model and return the contained
   * data net of textual characters as a double
   */
  private String readRecordString(Document dom)throws Exception
  {
    Element el = DomUtil.getFirstNamedDescendant(dom,"recordString");

    if(el == null)  return null;

    String text = DomUtil.getElementText(el);

    if(text == null || text.trim().length() == 0)
     return null;

    return text;
  }

  private String buildMIResponseField(String response, Deal deal)
  {

    String currentResponses = deal.getMortgageInsuranceResponse();
    StringBuffer outbuf = new StringBuffer();
    String crlf = System.getProperty("line.separator", "\n\r");

    if(response != null)
    {

      outbuf.append(response).append(crlf);
      /*
       //StringTokenizer st = new StringTokenizer(response, ",");
       StringTokenizer st = new StringTokenizer(response, "\t");
       String currentLine = null;

       while(st.hasMoreTokens())
       {
         currentLine = st.nextToken();
         currentLine = currentLine.trim();

         outbuf.append(currentLine).append(crlf);
       }
      */
       if(currentResponses != null)
        outbuf.append(currentResponses);

     }
     else
     {
        logger.info("INGESTION: MIREPLY - GE: No response String recieved for:" + deal.getDealId());
        response = currentResponses;
     }

    if(outbuf.length() == 0) return null;
    else return outbuf.toString();
  }


  /**
   * Look for expected Elements in the Document Object Model and return the contained
   * data net of textual characters as a double
   */
  private double findFromType(Element el)
  {
    Element fee = (Element)el.getParentNode();
    Element dealFee = (Element)fee.getParentNode();

    Element amt = (Element)DomUtil.getFirstNamedChild(dealFee, "feeAmount");

    String amtStr = DomUtil.getElementText(amt);

    if(amtStr == null || amtStr.trim().length() == 0) return 0.0;

    return TypeConverter.doubleTypeFrom(amtStr, -1.0);
  }

  private boolean verifyCVCodeExistance(String pCode){
    for(int i=0; i< cervusCodes.length; i++){
      if(cervusCodes[i].equals(pCode)){
        return true;
      }
    }
    return false;
  }

  private boolean isCervusCodePresent(Document pDoc){
    Node lNode = pDoc.getFirstChild();
    if(lNode == null){return false;}
    List l = DomUtil.getNamedChildren(lNode,"GEMessage");
    Iterator it = l.iterator();
    while(it.hasNext()){
      Node lOneNode = (Node) it.next();
      String lVal = DomUtil.getChildValue(lOneNode,"messageCode");
      if( lVal != null && verifyCVCodeExistance(lVal) ){
        return true;
      }
    }
    return false;
  }

}
