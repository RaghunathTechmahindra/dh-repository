package com.basis100.deal.ingest.handlers;

/**
* 02/Mar/2005 DVG #DG152 #1036  CCAPS number not displaying
* 25/Feb/2005 DVG #DG146 #1021 BMO-CCAPS ingest duplicate reply
* 12/Jan/2005 DVG #DG126 BMO-CCAPS ingest reply
 */

import com.basis100.deal.ingest.IngestionHandler;
import com.basis100.resources.SessionResourceKit;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.workflow.notification.WorkflowNotifier;
import com.basis100.deal.ingest.IngestionResult;
import MosSystem.Mc;
import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import org.w3c.dom.Document;
import com.basis100.deal.ingest.SourceMonitor;
import com.basis100.log.SysLogger;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.resources.ResourceManager;
import com.basis100.xml.DomUtil;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.security.DLM;
import com.basis100.deal.entity.DealNotes;
import org.w3c.dom.Node;
import com.basis100.picklist.BXResources;
import com.basis100.deal.entity.Borrower;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;

/**
 * <p>Title: FXpressJ2EE</p>
 * <p>Description: handle replies from lender BMO CCAPS system with request id.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Filogix</p>
 * @author Denis V. Gadelha
 * @version 1.0
 */

public class BMCCAPSHandler implements IngestionHandler {

  private static int BMO_IID = -1;
  private static String ECNI_BMO = "BM";
  public SysLogger logger;

  protected SessionResourceKit srk;
  protected CalcMonitor dcm;

  public BMCCAPSHandler() {
    logger = ResourceManager.getSysLogger("CCAPSReplyHandler");
  }

  /**
   * No pre processesing required
   *
   * @param file File
   * @throws Exception
   * @return File
   */
  public File preProcess(File file) throws Exception {
    return file;
  }

  public IngestionResult storeDocument(Document dom, SessionResourceKit srk,
                                       SourceMonitor source) throws Exception {
      
    this.dcm = CalcMonitor.getMonitor(srk);
    this.srk = srk;

    IngestionResult result = new IngestionResult();

    /*
     <CCAPSReply>
       <CCAPSReplyDate>	 	DateTime	Format:YYYY-MM-DDTHH:MM:SS
       <CCAPSApplicationId>	35	char	CCAPS Application ID or "System Failure"
       <dealId>	13	num	Express Deal Number
       <CCAPSApplicationSeqNum>	1	num	1 = Primary application
       2 = 1st Related Application
       3 = 2nd Related Application
       <CCAPSNumberOfApplicationsCreated>	1	num	Number of CCAPS IA's created
     </CCAPSReply>
     */

    //#DG152 assign all copies - rewritten
    try {
      return handleReply(dom, result);
    }
    catch (Exception loex) {
      logger.error("INGESTION: BMO - CCAPS: " + loex);
      result.isInvalidInput(true);
    }
    return result;
  }

  /**
   * handle the reply
   * @param dom Document input doc
   * @param result IngestionResult result of ingestion
   * @param psmsg String error msg if any
   * @throws Exception
   */
  private IngestionResult handleReply(Document dom, IngestionResult result) throws
      Exception {

    String ls;
    Node loBase = DomUtil.getFirstNamedChildIgnoreCase(dom, "CCAPSReply");
    if (loBase == null)
      throw new Exception("CCAPSReply not present");

    //psmsg = "dealId invalid ";
    int lidealId;
    ls = DomUtil.getChildValueIgnoreCase(loBase, "dealId");
    //psmsg += ls;
    lidealId = Integer.parseInt(ls);

    //ZIVKO:NOTE: make sure that deal is not locked.
    DLM dlm = DLM.getInstance();
    if (dlm.isLocked(lidealId, 0, srk)) {
      logger.info("INGESTION: deal " + lidealId + "  has been locked.");
      result.setDealLocked(true);
      return result;
    }
    else {
      dlm.lock(lidealId, 0, srk);
      result.setLockedDealId(lidealId);
    }
    srk.getExpressState().setDealInstitutionId(getInstitutionId(dom, srk));
    MasterDeal lomd = new MasterDeal(srk,dcm,lidealId);
    Deal loTheDeal = new Deal(srk,dcm,lidealId, lomd.getGoldCopyId());

    result.setResultEntity(loTheDeal.getPk());
    //psmsg = "handle CCAPSApplicationId ";
    String lsSeqNum = DomUtil.getChildValueIgnoreCase(loBase,"CCAPSApplicationSeqNum");
    String lsAppId = DomUtil.getChildValueIgnoreCase(loBase,"CCAPSApplicationId");
    ls = DomUtil.getChildValueIgnoreCase(loBase,"CCAPSNumberOfApplicationsCreated");
    ls = "CCAPS #: " + lsAppId + " " + lsSeqNum + " - " + ls;
    DealNotes lodnote = new DealNotes(srk);
    /*     \uF0A7 add to deal notes
     \uF0B7 Deal.DealNotes.dealNotesCategoryId  = 11
     \uF0B7 Deal.DealNotes.dealNotesText =  �CCAPS #: <CCAPSApplicationId> <CCAPSApplicationSeqNum> - <CCAPSNumberOfApplicationsCreated>�
     \uF0B7 example �CCAPS#: 98123454677 1 � 3�
     */
    lodnote.create( (DealPK) loTheDeal.getPk(), ((DealPK) loTheDeal.getPk()).getId(),
                   Mc.DEAL_NOTES_CATEGORY_INGESTION_LENDER, ls);

    if (lsSeqNum.equals("1")) {
      Borrower lobor = null;  // this is also used to flag that a deal note was already input
      Collection loCopyIds = lomd.getCopyIds();
      for (Iterator loiter = loCopyIds.iterator(); loiter.hasNext(); ) {
        int liCopyId = ((Integer)loiter.next()).intValue();
        loTheDeal = new Deal(srk,dcm,lidealId, liCopyId);
        lobor = update1Copy(loTheDeal, loBase, lodnote, lobor);
      }
      logger.info("INGESTION: BMO - CCAPS: updated with " + lsAppId);
      //dcm.calc();
    }
    else
      logger.info("INGESTION: BMO - CCAPS: NOT updated with " + lsAppId);
    return result;
  }

  /**
   * possibly update 1 deal copy with CCAPSApplicationId
   * @param deal Deal
   * @param poBase Node
   * @param podnote DealNotes
   * @param pobor Borrower
   * @throws Exception
   */
  private Borrower update1Copy(Deal deal, Node poBase, DealNotes podnote, Borrower pobor) throws Exception {
    //#DG146
    String lsAppId = DomUtil.getChildValueIgnoreCase(poBase,"CCAPSApplicationId");
    String ls = deal.getServicingMortgageNumber();
    //if (ls == null || ls.equals("")) {
    if (true) {  // 4/mar/2005 for now
      deal.setServicingMortgageNumber(lsAppId);
    }
    else {
      if (pobor == null) {
        pobor = new Borrower(srk,null).findByPrimaryBorrower(deal.getDealId(),deal.getCopyId());
        int liLangBor = pobor == null? Mc.LANGUAGE_PREFERENCE_ENGLISH: pobor.getLanguagePreferenceId();

        ls = BXResources.getDocPrepIngMsg("DUPLICATE_CCAPS_NUMBERa", liLangBor)
          + lsAppId + ' '+BXResources.getDocPrepIngMsg("DUPLICATE_CCAPS_NUMBERb", liLangBor);
        podnote.create( (DealPK) deal.getPk(), ((DealPK) deal.getPk()).getId(),
                     Mc.DEAL_NOTES_CATEGORY_INGESTION_LENDER, ls);
      }
      deal.setCheckNotesFlag("Y");
    }
    deal.ejbStore();
    return pobor;
    //#DG146 end
  }

  public void notifyWorkflow(WorkflowNotifier wfn, IngestionResult result) throws Exception {
    IEntityBeanPK deal = result.getResultEntity();
    if (deal != null) {
      int id = deal.getId();
      wfn.send(id, Mc.WORKFLOW_NOTIFICATION_TYPE_TASK_TRIGGER_TYPE_2);
      logger.debug("INGESTION: BMO - CCAPS: Sent workflow notification record.");
    }
    //otherwise ingestion failed
  }

  public void logSuccess(SessionResourceKit srk, IEntityBeanPK dpk, String filename) {
    if(dpk != null)
    {
      logger.info("IINGESTION: BMO - CCAPS: Success for: " + dpk.getId());

//      try   //log to history:
//      {
//        MIProcessHandler.getInstance().updateOnReply((DealPK)dpk, srk);
//      }
//      catch(Exception e)
//      {
//        logger.error("INGESTION: BMO - CCAPS: Error logging deal history for: " + dpk.getId());
//        logger.error(e);
//      }
//    }
    }
  }

  public void logFailure(SessionResourceKit srk, IEntityBeanPK deal, String filename) {
    if(deal != null)
     logger.info("INGESTION: BMO - CCAPS: Failed on: " + deal.getId());
  }

  public int processResult(IngestionResult res, File input, Document dom, SourceMonitor source) throws Exception {
    return STORE;
  }

  public void logDocCentralFolderCreation(SessionResourceKit srk, IEntityBeanPK deal, SourceFirmProfile sourceFirmName, String folderIndentifier) {
    /**@todo Implement this com.basis100.deal.ingest.IngestionHandler method*/
//    throw new java.lang.UnsupportedOperationException("Method logDocCentralFolderCreation() not yet implemented.");
  }
  
  /**
   * <p>getInstitutionId</p>
   * <p>find InstituionProfileId for BMO </p>
   * 
   * @param dom
   * @return int
   * @throws Exception
   */ 
  public int getInstitutionId(Document dom, SessionResourceKit srk) throws RemoteException, FinderException
  {
    if (BMO_IID > -1 ) return BMO_IID;
    
    InstitutionProfile ip = new InstitutionProfile(srk);
    ip = ip.findByECNID(ECNI_BMO);
    BMO_IID = ip.getInstitutionProfileId();
    return BMO_IID;
  }
}
