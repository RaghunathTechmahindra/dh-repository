package com.basis100.deal.ingest.mireply;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import MosSystem.Mc;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.ingest.DirectoryMonitor;
import com.basis100.deal.ingest.FileIngestionException;
import com.basis100.deal.ingest.IngestionResult;
import com.basis100.deal.ingest.SourceMonitor;
import com.basis100.deal.miprocess.MIProcessHandler;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.security.DLM;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.FinderException;
import com.basis100.log.SysLogger;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.util.file.IOUtil;
import com.basis100.workflow.notification.WorkflowNotifier;
import com.basis100.xml.DomUtil;
import com.basis100.xml.XmlToolKit;
import com.basis100.xml.XmlWriter;

/**
 *
* 26/Nov/2004 DVG #DG110 SCR#755 CMHC  MI Ingestion - Prequalification  changes
 */

/**
 *    An implementation of Ingestion handler that absorbs a stores cmhc reply files.
 */
public class CMHCReplyHandler extends MIReplyHandler
{

  public SysLogger logger;


  public static String[] nonChangeCodes = { "0210","0424","0102", "0108", "0110", "0115", "0123"};
  // 0210 -> this application is insurable
  // 0424 -> pre - qualified for insurance at above
  public static String[] approvalCodes = { "0108", "0102","0210","0424"};

  // 0123 -> unacceptable
  // 0211 -> this application is non insurable
  // 0157 -> insurance declined due to borrower(s) risk
  public static String[] unacceptableCodes = {"0123","0211","0157"};

  public static int CRITICAL_ERRORS  = 1;
  public static int CHANGE_EXPECTED  = 2;
  public static int CANCEL_CONFIRM  = 115;

  private static final String CMHC_REFERENCE_NUMBER_CONST = "CMHCReferenceNumber";

  public static String RESPONSE = "response";   //save for now
  public static String RECORD_STRING = "record_string";
  public static String REFERRED     = "0110";
  public static String CANCELLED    = "0115";
  public static String UNACCEPTABLE = "0123";

  //#DG110 private static final String APPLICATION_NUMBER_MESSAGE = "0433";

  public static String PREMIUM      = "0109";
  public static String PREMIUM_TAX  = "0135";
  public static String FEE_AMOUNT   = "0137";

  public static String TEXT         = "0000";

  /**
   *   Constructs an initialized application handler.
   *
   * @throws FileIngestionException
   */
  public CMHCReplyHandler() throws FileIngestionException
  {
    logger = ResourceManager.getSysLogger("CMHCReplyHandler");
  }

  /**
   * No pre processesing required
   *
   * @param file File
   * @throws Exception
   * @return File
   */
  public File preProcess(File file)throws Exception
  {
    return file;
  }

  /**
   *    Store the MI document attributes and handle business logic
   *
   * @param dom Document
   * @param srk SessionResourceKit
   * @param source SourceMonitor
   * @throws Exception
   * @return IngestionResult
   */
  public IngestionResult storeDocument(org.w3c.dom.Document dom, SessionResourceKit srk, SourceMonitor source)
        throws Exception
  {
    this.dcm = CalcMonitor.getMonitor(srk);
    this.srk = srk;
    
    IngestionResult result = new IngestionResult();

    String idString = DomUtil.getElementText(DomUtil.getFirstNamedDescendant(dom, "dealId"));

    if(idString == null)
    {
      logger.error("INGESTION: MIREPLY - CMHC: DealId not present: " + idString);
      result.isInvalidInput(true);
      return result;
    }

    //received and calculated values
    int dealId = -99;
    //int gCopyId = -99;

    dealId = TypeConverter.intTypeFrom(idString,-99);

    if(dealId == -99)
    {
      logger.error("INGESTION: MIREPLY - CMHC: DealId string not a valid number: " +idString);
      result.isInvalidInput(true);
      return result;
    }

    //ZIVKO:NOTE: make sure that deal is not locked.
    DLM dlm = DLM.getInstance();
    if(dlm.isLocked(dealId,0,srk) ){
      logger.info("INGESTION: deal " + dealId + "  has been locked.");
      result.setDealLocked(true);
      //4.3GR Item MI Reponse Alert STARTS <--  CIBC CR63
      dlm.setMIResponseExpected(srk, dealId, "Y");
      //4.3GR Item MI Reponse Alert ENDS 
      return result;
    }else{
      dlm.lock(dealId, 0, srk);
      result.setLockedDealId( dealId );
    }

    MasterDeal md = null;

    //all previous info relates to the gold copy.
    try
    {
      md = new MasterDeal(srk,dcm,dealId);

      Deal deal = new Deal(srk, dcm);
      deal = deal.findByRecommendedScenario(new DealPK(dealId, -1), true);

      result.setResultEntity( deal.getPk() );
      handleReply(result, deal, md, dom);
    }
    catch(Exception e)
    {
      logger.error("INGESTION: MIREPLY - CMHC: Invalid Deal.dealId received - dealId:" + dealId );
      logger.error(e);

      result.isInvalidInput(true);
      return result;
    }

   return result;
  }

  /**
   *  performs updates and parsing duties on the MIReply: <br>
   *
   * @param result IngestionResult
   * @param deal Deal
   * @param md MasterDeal
   * @param dom Document
   */
  private void handleReply(IngestionResult result, Deal deal, MasterDeal md, Document dom)
  {
    String refString = DomUtil.getElementText(DomUtil.getFirstNamedDescendant(dom, "CMHCReferenceNumber"));
    int cmhcRefNum = -9;

    if(!(refString == null || refString.trim().length() == 0))
    {

      cmhcRefNum = TypeConverter.intTypeFrom(refString,-9);

      if(cmhcRefNum == -9)
      {
        logger.error("INGESTION: MIREPLY - CMHC: Invalid CMHCReferenceNumber received: " + refString);
        result.isInvalidSource(true);
      }
    }

    //StringBuffer responseBuffer = new StringBuffer();
    HashMap codeMap = readMessages(dom);
    codeMap.put(CMHC_REFERENCE_NUMBER_CONST,new Integer(cmhcRefNum));

    String recordString = DomUtil.getElementText(DomUtil.getFirstNamedDescendant(dom,"recordString"));

    if(recordString != null)
     codeMap.put(RECORD_STRING, recordString);
    else
     logger.error("INGESTION: MIREPLY - CMHC: No recordString included with reply - dealId: " + deal.getDealId());

    //if an error reply ? what kind
    int errorState = errorState(cmhcRefNum, codeMap);

    boolean isCancel = false;

    int status = -1;

    if(errorState == CRITICAL_ERRORS)
    {
      status = Mc.MI_STATUS_CRITICAL_ERRORS_REC_FROM_INSURER;
    }
    else if(errorState == CHANGE_EXPECTED)
    {
      status = Mc.MI_STATUS_ERRORS_RECEIVED_FROM_INSURER;
    }
    else  //non error type:
    {

      if ( (hasCMHCReferenceNumber(codeMap)
           ) //#DG110 || hasMessageCode(APPLICATION_NUMBER_MESSAGE, codeMap))
           && codeMap.containsKey(CANCELLED))
      {
        status = Mc.MI_STATUS_CANCELLATION_CONFIRMATION_REC;
        isCancel = true;
      }
      else if(hasCMHCReferenceNumber(codeMap) && codeMap.containsKey(REFERRED))
        status = Mc.MI_STATUS_MANUAL_UNDERWRITING_REPLY;
      else if( (hasCMHCReferenceNumber(codeMap)
                ) //#DG110 || hasMessageCode(APPLICATION_NUMBER_MESSAGE,codeMap))
               && hasAnyCodeFromList( unacceptableCodes,codeMap))
        status = Mc.MI_STATUS_DENIED_RECEIVED;
      else if(isApproval(codeMap))
        status = Mc.MI_STATUS_APPROVAL_RECEIVED;
    }

    try
    {
      String miorFlag = deal.getOpenMIResponseFlag();

      //is it ok to perform the update or do we skip this reply?
      if(updateOK(deal, isCancel, miorFlag))
      {
        logger.info("INGESTION: MIREPLY - CMHC: OK to update. OpenMIResponseFlag = " + miorFlag + " MIStatus = " + status);
        updateMIValues(result, deal, md, status, codeMap, refString);
      }
      else
      {
        result.isNotificationExcused(true);
        result.addMessage("MI Ingestion: no update required.");
        logger.info("INGESTION: MIREPLY - CMHC: no update required. OpenMIResponseFlag = " + miorFlag + " MIStatus = " + status);
      }
    }
    catch(Exception e)
    {
      String msg = e.getMessage();
      if(msg == null) msg = "Unknown Error.";

      msg = " Error type: " + msg;

      logger.error("INGESTION: MIREPLY - CMHC: Error handling CMHC reply for deal: " + deal.getDealId() + msg);
      logger.error(e);
      result.isApplicationError(true);
      return ;
    }


    return ;
  }


  private void createDealFeeRecords(Deal deal, MasterDeal md, int feeId, double feeAmt){
      try{
          int dealId = deal.getDealId();

          Collection copyIds = md.getCopyIds();
          Integer curcpy = null;

          Iterator dit = copyIds.iterator();

          while(dit.hasNext())
          {
            curcpy = (Integer)dit.next();

            Deal currentDeal = null;

            try
            {
               currentDeal = new Deal(srk, dcm, dealId, curcpy.intValue());
            }
            catch(FinderException fe)
            {
               logger.info("INGESTION: MIREPLY - CMHC: Benign FinderException while building fees for copyId: " + curcpy);
               continue;
            }
             MIFeeBuilder.buildFee(srk, dcm, currentDeal, feeId, feeAmt);
          }

        }
        catch(Exception fe)
        {
          String msg = fe.getMessage();

          if(msg == null) msg = "Unknown error. ";

          logger.error("INGESTION: MIREPLY - CMHC: Error building fees due to: " + msg);
          logger.error(fe);
        }

  }

  private void updateMIValues(IngestionResult result, Deal deal, MasterDeal md, int miStatus,
                               Map codeMap, String cmhcRefNum) throws Exception
  {
    if(deal == null || codeMap == null )
    {
      result.isApplicationError(true);
      return ;
    }

    //get all the relevant info from the deal and the codeMap
    String preQualMICertNum = deal.getPreQualificationMICertNum();
    String preQualNum = MIProcessHandler.NO_PreQualMICertNum;    // default
    /* #DG110 String incomingPreQualNum = extractApplicationNumber(codeMap);

    if( hasMessageCode(APPLICATION_NUMBER_MESSAGE,codeMap) ){
      if(incomingPreQualNum != null ){
        if( preQualMICertNum != null && !(incomingPreQualNum.equals(preQualMICertNum)) ){
          preQualNum = incomingPreQualNum;
        }
      }else{
        preQualNum = incomingPreQualNum;
      }
    }  */
    if(deal.getMIIndicatorId() == 5) {
      if (cmhcRefNum != null  && ! cmhcRefNum.equals(preQualMICertNum ) )
        preQualNum = cmhcRefNum;
      cmhcRefNum = MIProcessHandler.NO_CMHCPolicyNumber;
    }

    //==========================================================================
    // Note: it seems that statusCategory variable has not been used at all.
    //==========================================================================
    //int statusCategory = deal.getStatusCategoryId();

    //==========================================================================
    // Policty num has not been used at all
    //==========================================================================
    //String policyNum = deal.getMIPolicyNumber();

    String response = buildMIResponseField((String)codeMap.get(RECORD_STRING), deal);

    (MIProcessHandler.getMIAttributesPropogator()).propogateMIAttributes(
                srk, deal, miStatus, MIProcessHandler.NO_MIIndicatorId,
                MIProcessHandler.NO_MIPremiumAmount, MIProcessHandler.NO_MortgageInsurerId, cmhcRefNum,
                MIProcessHandler.NO_MIPayorId, MIProcessHandler.NO_MITypeId, MIProcessHandler.NO_MIUpfront,
                MIProcessHandler.NO_MICommunicationFlag, null, MIProcessHandler.NO_MIExistingPolicyNumber,
                MIProcessHandler.NO_MIComments, response, preQualNum, cmhcRefNum, MIProcessHandler.NO_GEPolicyNumber,
                MIProcessHandler.NO_LOCAmortizationMonths,		// Rel 3.1 - Sasa
                MIProcessHandler.NO_LOCInterestOnlyMaturityDate,	// Rel 3.1 - Sasa 
                MIProcessHandler.NO_MIFeeAmount,					// Rel. 3.1.1 - Serghei
                MIProcessHandler.NO_MIPremiumPST,					// Rel. 3.1.1 - Serghei
                MIProcessHandler.NO_MIPolicyNumAIGUG,				// Rel. 3.1.1 - Serghei
                MIProcessHandler.NO_MIPolicyNumPMI
    );

    //==========================================================================
    if( hasCMHCReferenceNumber(codeMap) && hasMessageCode("0210",codeMap) && !dealHasFeeAttached(deal,Mc.FEE_TYPE_CMHC_LOAN_ASSESSMENT_FEE) ){
        String loanAssFeeText = null; //0230
        double loanAssFee = 0d;

        try
        {
          loanAssFeeText  = (String)codeMap.get("0230");    //0230
          loanAssFee = parseValueFrom(loanAssFeeText);
        }
        catch(Exception e)
        {
          String msg = e.getMessage();
          if(msg == null) msg = "Unknown Error.";
          msg += " Error parsing fee value MESSAGE NUMBER:0230  ORIGINAL TEXT: " ;
          msg += loanAssFeeText;
          logger.error("INGESTION: MIREPLY - CMHC: " + msg);
          logger.error(e);
        }
        createDealFeeRecords(deal,md,Mc.FEE_TYPE_CMHC_LOAN_ASSESSMENT_FEE , loanAssFee);
    }
    //==========================================================================
    if( deal.getPreQualificationMICertNum() != null && hasMessageCode("0424",codeMap) && !(dealHasFeeAttached(deal,Mc.FEE_TYPE_CMHC_PRE_QUALIFICATION_FEE )) ){
        String applicationFeeText = null; //0601
        double applicationFee = 0d;

        try
        {
          applicationFeeText  = (String)codeMap.get("0601");    //0601
          applicationFee = parseValueFrom(applicationFeeText);
        }
        catch(Exception e)
        {
          String msg = e.getMessage();
          if(msg == null) msg = "Unknown Error.";
          msg += " Error parsing fee value MESSAGE NUMBER:0601  ORIGINAL TEXT: " ;
          msg += applicationFeeText;
          logger.error("INGESTION: MIREPLY - CMHC: " + msg);
          logger.error(e);
        }
        createDealFeeRecords(deal,md,Mc.FEE_TYPE_CMHC_PRE_QUALIFICATION_FEE , applicationFee);
    }
    //==========================================================================
    //if approval and preDecision or postDecision deal
    if(miStatus == Mc.MI_STATUS_APPROVAL_RECEIVED)
    {
        logger.trace("INGESTION: MIREPLY - CMHC: Begin Update Approval.");

        String premText  = null; //0109
        double miPremDue = 0d;
        String taxText   = null; //0135
        double miTaxDue  = 0d;
        String feeText   = null; //0137
        double miFeeAmt  = 0d;

        try
        {
          //Deal.miPremium:
          premText  = (String)codeMap.get(PREMIUM);    //0109
          miPremDue = parseValueFrom(premText);

          //pst value
          taxText   = (String)codeMap.get(PREMIUM_TAX); //0135
          miTaxDue  = parseValueFrom(taxText);

          //CMHC fee
          feeText   = (String)codeMap.get(FEE_AMOUNT); //0137
          miFeeAmt  = parseValueFrom(feeText);
        }
        catch(Exception e)
        {
          String msg = e.getMessage();
          if(msg == null) msg = "Unknown Error.";

          logger.error("INGESTION: MIREPLY - CMHC: Error parsing fee values due to: " + msg);
          logger.error(e);
        }

        try
        {
           double currentPrem = deal.getMIPremiumAmount();  // get the premium on this deal.

           if(miPremDue != currentPrem)   //has the premium changed
            miStatus = Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED;

           (MIProcessHandler.getMIAttributesPropogator()).propogateMIAttributes(
                srk, deal, miStatus, MIProcessHandler.NO_MIIndicatorId,
                miPremDue, MIProcessHandler.NO_MortgageInsurerId, MIProcessHandler.NO_MIPolicyNumber,
                MIProcessHandler.NO_MIPayorId, MIProcessHandler.NO_MITypeId, MIProcessHandler.NO_MIUpfront,
                MIProcessHandler.NO_MICommunicationFlag, MIProcessHandler.NO_OpenMIResponseFlag, MIProcessHandler.NO_MIExistingPolicyNumber,
                MIProcessHandler.NO_MIComments, MIProcessHandler.NO_MIResponse, MIProcessHandler.NO_PreQualMICertNum,
                MIProcessHandler.NO_CMHCPolicyNumber, MIProcessHandler.NO_GEPolicyNumber,
                MIProcessHandler.NO_LOCAmortizationMonths,		// Rel 3.1 - Sasa
                MIProcessHandler.NO_LOCInterestOnlyMaturityDate,	// Rel 3.1 - Sasa 
                miFeeAmt,											// Rel. 3.1.1 - Serghei
                miTaxDue, 											// Rel. 3.1.1 - Serghei
                MIProcessHandler.NO_MIPolicyNumAIGUG,				// Rel. 3.1.1 - Serghei
                MIProcessHandler.NO_MIPolicyNumPMI
           );


          int dealId = deal.getDealId();

          Collection copyIds = md.getCopyIds();
          Integer curcpy = null;

          Iterator dit = copyIds.iterator();

          while(dit.hasNext())
          {

            curcpy = (Integer)dit.next();

            Deal currentDeal = null;

            try
            {
               currentDeal = new Deal(srk, dcm, dealId, curcpy.intValue());
            }
            catch(FinderException fe)
            {
               logger.info("INGESTION: MIREPLY - CMHC: Benign FinderException while building fees for copyId: " + curcpy);
               continue;
            }
            //================================================================================================
            // Tracker issue 259 says that before we build new fees during the
            // process of ingestion, we have to wipe out all fees of certain types.
            // The following piece of code does that.
            // note posted by Zivko: Feb 04, 2002
            //================================================================================================
 /* Rel. 3.1.1 - Serghei           int[] typesToDelete = { Mc.FEE_TYPE_CMHC_PREMIUM,
                            Mc.FEE_TYPE_CMHC_PREMIUM_PAYABLE,
                            Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM,
                            Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM_PAYABLE,
                            Mc.FEE_TYPE_CMHC_FEE,
                            Mc.FEE_TYPE_CMHC_FEE_PAYABLE,
                            Mc.FEE_TYPE_GE_CAPITAL_FEE,
                            Mc.FEE_TYPE_GE_CAPITAL_FEE_PAYABLE,
                            Mc.FEE_TYPE_GE_CAPITAL_PREMIUM,
                            Mc.FEE_TYPE_GE_CAPITAL_PREMIUM_PAYABLE,
                            Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM,
                            Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM_PAYABLE };

            MIProcessHandler.getMIAttributesPropogator().propogateDealFeeRemoval(srk,currentDeal,typesToDelete);*/
            MIProcessHandler.getMIAttributesPropogator().removeFeeFromDeal(srk, currentDeal);
            //=================================================================================================
            // end of update
            //=================================================================================================
             MIFeeBuilder.buildFee(srk, dcm, currentDeal, Mc.FEE_TYPE_CMHC_PREMIUM, miPremDue);
             MIFeeBuilder.buildFee(srk, dcm, currentDeal, Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM, miTaxDue);
             MIFeeBuilder.buildFee(srk, dcm, currentDeal, Mc.FEE_TYPE_CMHC_FEE, miFeeAmt);

          }

        }
        catch(Exception fe)
        {
          String msg = fe.getMessage();

          if(msg == null) msg = "Unknown error. ";

          logger.error("INGESTION: MIREPLY - CMHC: Error building fees due to: " + msg);
          logger.error(fe);
        }
    }
  }


  public void notifyWorkflow(WorkflowNotifier wfn, IngestionResult parResult) throws Exception
  {
     IEntityBeanPK deal = parResult.getResultEntity();
     if(deal != null)
     {
        int id = deal.getId();
        wfn.send(id,Mc.WORKFLOW_NOTIFICATION_TYPE_MI_REPLY);
        logger.debug("INGESTION: MIREPLY - CMHC: Sent workflow notification record.");
     }
     //otherwise ingestion failed
  }

  public void logSuccess(SessionResourceKit srk, IEntityBeanPK dpk, String filename)
  {
    if(dpk != null)
    {
      logger.info("IINGESTION: MIREPLY - CMHC: Success for: " + dpk.getId());

      try   //log to history:
      {
        MIProcessHandler.getInstance().updateOnReply((DealPK)dpk, srk);
      }
      catch(Exception e)
      {
        logger.error("INGESTION: MIREPLY - CMHC: Error logging deal history for: " + dpk.getId());
        logger.error(e);
      }
    }

  }

  public void logFailure(SessionResourceKit srk, IEntityBeanPK deal, String filename)
  {
    if(deal != null)
     logger.info("INGESTION: MIREPLY - CMHC: Failed on: " + deal.getId());;
  }

  public int processResult(IngestionResult res, File input, Document dom, SourceMonitor mailbox) throws Exception
  {
     if(res.isApplicationError() || res.isInvalidInput() || res.isCriticalFailure())
     {
        FileOutputStream str = null;
        DirectoryMonitor monitor = (DirectoryMonitor)mailbox;
        try
        {
          XmlWriter writer = XmlToolKit.getInstance().getXmlWriter();

          File dir = monitor.getErrorDirectory();

          String filename = input.getName();

          str = IOUtil.constructSafeFOS(dir,filename,monitor.getErrorExtension());

          writer.setPrintEmptyElements(false);

          writer.print(new OutputStreamWriter(str),dom,writer.UNFORMATTED);
        }
        catch(Exception e)
        {
          String err = "INGESTION: Failed to write to 'Error' directory - Exception Message =  " + e.getMessage();
          logger.error(err.toString());
        }
        finally
        {
          try{str.close();}catch(Exception e2){;}
        }

       return ROLLBACK;
     }

     return STORE;
  }

  // ===========================================================================
  // utility method to examine whether the reply has CMHC reference number or not
  // Method has been introduced because the spec has been dramatically changed
  // in relation to introduction of 2 new CMHC products.
  //============================================================================
  private boolean hasCMHCReferenceNumber(Map codeMap){
    try{
      Object obj = codeMap.get(CMHC_REFERENCE_NUMBER_CONST);
      if(obj instanceof Integer){
        if( ((Integer)obj).intValue() != -9 ){
          return true;
        }
      }else{
        return false;
      }
    }catch(Exception exc){
      return false;
    }
    return false;
  }

  private boolean hasAnyCodeFromList(String[] cList, Map codeMap){
    for(int i = 0; i < cList.length; i++ ){
      if( hasMessageCode(cList[i],codeMap) ){
        return true;
      }
    }
    return false;
  }

  private boolean dealHasFeeAttached(Deal pDeal, int pFee)throws Exception
  {
    DealFee df = new DealFee(srk,null);
    Collection dFees = df.findByDealAndType
      (new DealPK(pDeal.getDealId(), pDeal.getCopyId()), pFee );

    if(dFees.isEmpty()){
      return false;
    }
    return true;
  }

  private boolean hasMessageCode(String code, Map codeMap){
    if(codeMap.containsKey(code)){
      return true;
    }
    return false;
  }

  //============================================================================
  // This method has been changed significantly because of two new products.
  // Note posted by Zivko Mar, 13, 2002
  //============================================================================
  private int errorState(int refNum, Map codeMap)
  {
    //if(refNum < 0) return CRITICAL_ERRORS;
    if( refNum < 0 ) //#DG110 && !( hasMessageCode(APPLICATION_NUMBER_MESSAGE,codeMap) ) )
    {
       return CRITICAL_ERRORS;
    }

    if(refNum >= 0) //#DG110  || hasMessageCode(APPLICATION_NUMBER_MESSAGE,codeMap))
    {
      for(int i = 0; i < nonChangeCodes.length; i++ )
      {
         if(codeMap.containsKey(nonChangeCodes[i]))
          return 0;
      }

      return CHANGE_EXPECTED;
    }

    return 0;
  }

  private boolean isApproval(Map codeMap)
  {
    if(hasCMHCReferenceNumber(codeMap) && this.hasAnyCodeFromList(approvalCodes, codeMap) ){
      return true;
    }else{
      return false;
    }
    /*
    for(int i = 0; i < approvalCodes.length; i++ )
    {
       // ======================================================================
       // zivko commented out the following line because it looked odd that we
       // loop through one array and examine based on value from another array.
       // Changed posted on Mar 13, 2002
       //=======================================================================
       //if(codeMap.containsKey(nonChangeCodes[i]))
       if(codeMap.containsKey(approvalCodes[i]))
        return true;
    }

    return false;
    */
  }


  /**
   * Look for expected Elements in the Document Object Model and place the contained
   * data in a Map.
   *
   * @param dom Document
   * @return HashMap with codes as keys and element data as values
   */
  private HashMap readMessages(Document dom)
  {

    StringBuffer textMessage = new StringBuffer();
    StringBuffer responseVal = new StringBuffer();
    HashMap map = new HashMap(43);

    List nodes = DomUtil.getNamedDescendants(dom, "CMHCMessage");

    ListIterator li = nodes.listIterator();

    Node current = null;
    CMHCMessage message = null;
    String currentCode  = null;
    String currentValue = null;

    while(li.hasNext())
    {
      current = (Node)li.next();

      message = new CMHCMessage(current);

      currentCode = message.codeString;
      currentValue = message.messageText;

      if(currentValue == null)
       currentValue = "";

      if(currentCode != null && currentCode.equals(TEXT))
      {
        textMessage.append(currentValue);
      }
      else
      {
        responseVal.append( currentCode + " " + currentValue + "\n\r");
        map.put(currentCode,currentValue);
      }
    }

    responseVal.append(textMessage.toString());

    map.put(RESPONSE,responseVal.toString());

    return map;
  }

  /**
   *  Encapsulates logic for permitting update process.
   *  Once a reply has been received and parsed and the miStatus relating to the
   *  reply determined (though not saved to db) indicates whether the full database
   *  update can proceed.
   *
   *  if openMIResponsFlag is M -> this means a Cancelled is pending return and a
   *  change has been sent out.
   *  if  openMIResponsFlag = C -> this mean a cancellation is pending return.
   *
   * @param deal Deal
   * @param isCancel boolean
   * @param openMIResponsFlag String
   * @return boolean
   */
  private boolean updateOK(Deal deal, boolean isCancel, String openMIResponsFlag)
  {

     if(openMIResponsFlag == null) openMIResponsFlag = "NULL";

     if(openMIResponsFlag.equals("M") && isCancel)
      return false;

     if(openMIResponsFlag.equals("C") && !isCancel)
      return false;

     return true;
  }

/* #DG110
  private String extractApplicationNumber( Map codeMap ){
    String msgLine = (String)codeMap.get(APPLICATION_NUMBER_MESSAGE);
    if (msgLine == null ){return null;}
    msgLine = msgLine.trim();
    if( msgLine.equals("") ){return null;}
    int lIndex = msgLine.lastIndexOf(" ");

    if( lIndex == -1 ){return null;}
    String retVal = msgLine.substring(lIndex).trim();

    if(retVal.equals("")){
      return null;
    }else{
      return retVal;
    }
  }
*/
  private double parseValueFrom(String msg) throws NumberFormatException
  {
    StringBuffer msgbuf = new StringBuffer();
    char[] msgchars = msg.toCharArray();
    int len = msgchars.length;
    //parse out dollar signs

    char current = ' ';

    for(int i = 0; i < len; i++)
    {
       current = msgchars[i];

       if(Character.isDigit(current) || current == '.')
        msgbuf.append(msgchars[i]);
    }

    return Double.parseDouble(msgbuf.toString());
  }

   private String buildMIResponseField(String response, Deal deal)
   {

      String currentResponses = deal.getMortgageInsuranceResponse();
      StringBuffer outbuf = new StringBuffer();
      String crlf = System.getProperty("line.separator", "\n\r");

      if(response != null)
      {
        outbuf.append(response).append(crlf);
        /*
         //StringTokenizer st = new StringTokenizer(response, ",");
         StringTokenizer st = new StringTokenizer(response, "\t");
         String currentLine = null;

         while(st.hasMoreTokens())
         {
           currentLine = st.nextToken();
           currentLine = currentLine.trim();

           outbuf.append(currentLine).append(crlf);
         }
        */
         if(currentResponses != null)
          outbuf.append(currentResponses);

       }
       else
       {
          logger.info("INGESTION: MIREPLY - CMHC: No response String received for:" + deal.getDealId());
          response = currentResponses;
       }

      if(outbuf.length() == 0) return null;
      else return outbuf.toString();
  }


  class CMHCMessage
  {
     String codeString;
     String messageText;
     int messageCode;

     public CMHCMessage(Node messageNode)
     {
        if(messageNode == null || !messageNode.getNodeName().equals("CMHCMessage"))
         ;
        else
        {
           codeString = DomUtil.getChildValue(messageNode,"messageCode");
           messageText = DomUtil.getChildValue(messageNode,"messageText");
           messageCode = TypeConverter.intTypeFrom(this.codeString);
        }
     }
  }

  //--DocCentral_FolderCreate--21Sep--2004--start--//
  public void logDocCentralFolderCreation(SessionResourceKit srk,
                                        IEntityBeanPK pk,
                                        SourceFirmProfile sourceFirmEntity,
                                        String folderIdentifier) {;}
  //--DocCentral_FolderCreate--21Sep--2004--end--//

}

