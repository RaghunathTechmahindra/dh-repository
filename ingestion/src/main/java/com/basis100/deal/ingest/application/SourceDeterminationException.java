package com.basis100.deal.ingest.application;

import com.basis100.deal.ingest.FileIngestionException;

public class SourceDeterminationException extends FileIngestionException
{

 /**
  * Constructs a <code>SourceDeterminationException</code> with no specified detail message.
  */
  public SourceDeterminationException()
  {
	  super();
  }

 /**
  * Constructs a <code>IngestionSourceDeterminationException</code> with the specified message.
  * @param   message  the related message.
  */
  public SourceDeterminationException(String message)
  {
    super(message);
  }
}

