package com.basis100.deal.ingest.constraints;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.Asset;
import com.basis100.deal.entity.AssetType;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.xml.EntityNodeAssociation;
import com.basis100.resources.SessionResourceKit;


public class PercentInNetWorthDefault implements DefaultConstraint
{
  public void execute(SessionResourceKit srk, EntityNodeAssociation ass) throws Exception
  {
    DealEntity de = ass.getEntity();
    
    if(de.getClassId() == ClassId.ASSET)
    {
      try
      {
         Asset as = (Asset)de;
         int type = as.getAssetTypeId();

         AssetType at = new AssetType(srk,type);

         as.setPercentInNetWorth(at.getNetWorthInclusion());
      }
      catch(Exception e)
      {
         throw new Exception("Default Value not found for " + this.getClass().getName());
      }
    }
    else
     throw new Exception("Default Value not found for " + this.getClass().getName());

  }
}
