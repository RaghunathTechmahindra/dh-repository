package com.basis100.deal.ingest.constraints;

import com.basis100.deal.xml.EntityNodeAssociation;
import com.basis100.resources.SessionResourceKit;


public interface DefaultConstraint extends IngestionConstraint
{
  public void execute(SessionResourceKit srk, EntityNodeAssociation ass) throws Exception;
}
