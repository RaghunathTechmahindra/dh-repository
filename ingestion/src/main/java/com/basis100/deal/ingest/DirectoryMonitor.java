package com.basis100.deal.ingest;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;
import org.w3c.dom.Element;
import com.basis100.deal.ingest.event.FileReceptionListener;
import com.basis100.deal.ingest.event.FilesRecievedEvent;
import com.basis100.deal.util.TypeConverter;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.util.file.IOUtil;
import com.basis100.xml.DomUtil;

/**
 * <pre>
 * This class contains methods for monitoring a directory for file arrival
 * firing a FileReceptionEvent when files arrive.
 *  </pre>
 */
public class DirectoryMonitor implements SourceMonitor
{
  protected Thread monitor = null;

  protected String name;

  protected File directory;
  protected String dirext;

  protected File archive;
  protected String arcext;

  protected File error;
  protected String errext;

  protected File outdir;
  protected String outext;

  protected File fxOutboundDir;
  protected String fxOutExt;


  protected File lockedDir;
  protected String lockedExt;

  protected String handlerName;
  protected IngestionHandler handler;

  protected String constraints;


  protected Vector list;
  protected Vector listeners;

  protected SysLogger logger;

  protected int interval = 1000;

  protected File pickup;

  protected boolean archiveOn = true;

  /**
   *  Constructs and initializes an instance of a directory monitor with the <br/>
   *  given name configured with the attributes of the given <code>Element</code>.
   *  @param config - the configuration node
   *  @param name - a String identifier for this monitor
   */
  public DirectoryMonitor(Element config, String name) throws Exception
  {
    logger = ResourceManager.getSysLogger("DirectoryMonitor");
    logger.debug("INGESTION: DirectoryMonitor initializing ...");
    this.name = name;
    init(config);
  }

  private void init(Element config) throws Exception
  {

    String intervalstr = config.getAttribute("interval");

    if(intervalstr == null)
    {
     logger.info("INGESTION: SourceMonitor no monitor interval parameter found. ");
     logger.info("INGESTION: SourceMonitor using default interval. ");
    }

    interval = TypeConverter.intTypeFrom(intervalstr, 5);
    interval = interval * 1000;
    logger.info("INGESTION: SourceMonitor setting monitor interval to: " + interval);

    handlerName = config.getAttribute("handler");
    logger.debug("INGESTION: SourceMonitor getting monitor handler name: " + handlerName);

    constraints = config.getAttribute("constraints");

    constraints = IngestionManager.getIngestionHome() + constraints;

    Element dirElement = (Element)DomUtil.getFirstNamedChild(config, "inbound");
    directory = buildPath(dirElement);
    dirext = cleanExtension(dirElement.getAttribute("extension"));

    Element lockedElement = (Element) DomUtil.getFirstNamedChild(config, "ondeallocked");
    lockedDir = buildPath(lockedElement);
    lockedExt = cleanExtension(lockedElement.getAttribute("extension"));

    Element arcElement = (Element)DomUtil.getFirstNamedChild(config, "archive");
    if(arcElement != null)
    {
      archive = buildPath(arcElement);
      arcext = cleanExtension(arcElement.getAttribute("extension"));
    }
    else
    {
      archiveOn = false;

    }

    Element fXretElement = (Element)DomUtil.getFirstNamedChild(config, "fxlinkoutbound");
    fxOutboundDir = buildPath(fXretElement);
    fxOutExt = cleanExtension(fXretElement.getAttribute("extension"));

    Element retElement = (Element)DomUtil.getFirstNamedChild(config, "outbound");
    outdir = buildPath(retElement);
    outext = cleanExtension(retElement.getAttribute("extension"));

    Element errElement = (Element)DomUtil.getFirstNamedChild(config, "error");
    error = buildPath(errElement);
    errext = cleanExtension(errElement.getAttribute("extension"));

    if(dirext == null)
    {
       dirext = ".xml";
       logger.info("INGESTION: SourceMonitor - no extension for inbound files. Setting to *.xml");
    }

    if(fxOutExt == null)
    {
       fxOutExt = ".xml";
       logger.info("INGESTION: SourceMonitor - no extension for fx link files. Setting to *.xml");
    }

    if(arcext == null && archiveOn)
    {
      arcext = ".xml";
      logger.info("INGESTION: SourceMonitor - no extension for archive files. Setting to *.xml");
    }

    if(errext == null)
    {
      errext = ".xml";
      logger.info("INGESTION: SourceMonitor - no extension for error files. Setting to *.xml");
    }

    if(outext == null)
    {
      outext = ".xml";
      logger.info("INGESTION: SourceMonitor - no extension for outbound files. Setting to *.xml");
    }

    try
    {
      String dirpath = directory.getAbsolutePath();

      pickup = new File(dirpath + "/recieved/");

      if(!pickup.exists())
      {
        if(!pickup.mkdirs())
        {
          throw new IOException();
        }
      }
    }
    catch(IOException io)
    {
       logger.error("INGESTION: SourceMonitor - unable to create temp dir.");
       throw io;
    }
  }

  /**
   *  Monitors the directory named in the import_export.properties file for the arrival
   *  of deal files:  com.basis100.monitor.directory
   *  The arrival of files triggers a FilesRecievedEvent.
   */
  public void startMonitor() throws IOException, FileIngestionException
  {

     monitor = new Thread(this);
     monitor.setName(this.name);

     if(this.directory == null ||
        (archiveOn && this.archive == null) ||
        this.error == null ||
        this.outdir == null)
        throw new IOException("Error constructing directories for DirectoryMonitor");

     if(!this.directory.exists())
     {
        createDirectory(directory);
     }
     if(archiveOn && !this.archive.exists())
     {
        createDirectory(archive);
     }
     if(!this.error.exists())
     {
        createDirectory(error);
     }
     if(!this.outdir.exists())
     {
        createDirectory(outdir);
     }


     if(!this.directory.isDirectory() ||
        (archiveOn && !this.archive.isDirectory()) ||
        !this.error.isDirectory() ||
        !this.outdir.isDirectory() )
        throw new IOException("Directories for ingesting or archiving are invalid");

     if(this.handlerName == null || handlerName.length() == 0)
        throw new FileIngestionException("Cannot start a monitor without a IngestionHandler name!");

     try
     {
       logger.info("INGESTION: SourceMonitor getting class for handler: " + handlerName );
       Class hclass = Class.forName(handlerName);
       logger.info("INGESTION: SourceMonitor constructing class for handler: " + hclass );
       handler =(IngestionHandler)hclass.newInstance();
     }
     catch(Exception e)
     {
       logger.error("INGESTION: Failed while building handler: " + handlerName );
       logger.error(e);
       String msg = e.getMessage();
       if(msg != null) msg = "Cannot Create Monitor " + msg;
       else msg = "Construction of the Handler failed , cannot Create Monitor ";
        throw new FileIngestionException(msg);
     }

     System.out.println("INGESTION: Monitoring: " + directory.getAbsolutePath());

     logger.info("INGESTION: SourceMonitor starting thread: " + monitor.getName() );

     //check for undeleted files - evidence of a premature shut down
     recoverIncomplete();

     monitor.start();

  }

  public File buildPath(Element dir)
  {
    String path = dir.getAttribute("path");
    
    if(path == null) return null;

    return new File(path);

  }


  public void notifyListeners(FilesRecievedEvent event)
  {
    if(listeners == null) return;

    Enumeration e = listeners.elements();

    while(e.hasMoreElements())
    {
      ((FileReceptionListener)e.nextElement()).filesRecieved(event);
    }
  }

  /**
   *  Adds a new FileReceptionListener to this monitor instance.
   */
  public void addListener(FileReceptionListener l)
  {
       if(listeners == null)
         listeners = new Vector();

       if(l == null)
        logger.error("INGESTION: SourceMonitor cannot add null listener. ");
       else
        listeners.add(l);
  }

   /**
   *  Removes the indicated FileReceptionListener to this monitor instance.
   */
  public void removeListener(FileReceptionListener l)
  {
    if(listeners == null) return;

     if(l == null)
      logger.error("INGESTION: SourceMonitor cannot remove null listener. ");
     else
      listeners.remove(l);
  }

  /**
   *  Removes all FileReceptionListeners from this monitor instance.
   */
  public void removeAllListeners()
  {
    listeners = null;

    listeners = new Vector();
  }

  /**
   *   gets the handler associated with this SourceMonitor instance
   */
  public IngestionHandler getHandler()
  {
    return this.handler;
  }

  public String getConstraints()
  {
    return this.constraints;
  }

  /**
   *  @return true if this monitor archives incoming files
   */
  public boolean isArchiveOn(){ return archiveOn; }

  public File getErrorDirectory(){ return error; }
  public String getErrorExtension(){ return errext; }

  public File getOutboundDirectory(){ return outdir; }
  public String getOutboundExtension(){ return outext; }

  public File getFxOutboundDirectory(){ return fxOutboundDir; }
  public String getFxOutExt(){ return fxOutExt; }

  public File getInboundDirectory(){return directory;}
  public String getInboundExtension(){return dirext;}

  public String getInputLocation()
  {
    if(directory == null)return null;

    return directory.getAbsolutePath();
  }

  public String getOutputLocation()
  {
    if(this.outdir == null)return null;

    return outdir.getAbsolutePath();
  }

  public String getArchiveLocation()
  {
    if(archive == null)return null;

    return archive.getAbsolutePath();
  }

  public String getErrorLocation()
  {
    if(error == null)return null;

    return error.getAbsolutePath();
  }


  public String getName(){return name;}


  private void createDirectory(File dir) throws IOException
  {
    logger.info("Directories for ingesting or archiving do not exist. Attempting to create.");

    if(dir == null)
      throw new IOException("Null directory object recieved. Cannot create.");

    if(!dir.mkdirs())
    {
      throw new IOException("Unable to make directory: " + dir.getAbsolutePath());
    }
  }

  private String cleanExtension(String in)
  {
    if(in != null && !in.startsWith("."))
    in = "." + in;

    if(in == null) return ".xml";

    return in;
  }

  public void sendToLockedFolder(File file) throws Exception
  {
    if(file == null)
     throw new IOException("File is NULL, so it can not be sent to locked folder.");

    if(!file.exists() || !file.canRead())
      throw new IOException("File does not exist or cannot be read. Method name sendToLockedFolder");

    File out = null;

    if( lockedDir.isDirectory() )
    {
      String name = IOUtil.changeExtension(lockedDir.getAbsolutePath() + "/" + file.getName(), this.lockedExt );
      out = new File(name);
    }

    //IOUtil.moveFile(file, out, false);
    IOUtil.moveFile(file, out, true);
    logger.info("INGESTION: file " + file.getName() + " has been sent to locked folder: " + name);
  }

  public boolean performArchive(File file) throws IOException
  {
    if(file == null)
     throw new IOException("Archive File is NULL");

    if(!file.exists() || !file.canRead())
      throw new IOException("Archive File does not exist or cannot be read.");

    File out = null;

    if(archiveOn || archive.isDirectory())
    {
      String name = IOUtil.changeExtension(archive.getAbsolutePath() + "/" + file.getName(), this.arcext);
      out = new File(name);
    }

    IOUtil.moveFile(file, out, false);

    return true;

  }

  public void run()
  {
    try
    {
       while(true)
       {
          Vector files = getFiles(dirext);

          try
          {
            if(files != null)
            {
              //copy to new location for processing
              Vector movedFiles = IOUtil.copyAndDelete(files,this.pickup,".edf");
              // the only listener should be the queue owner
              notifyListeners(new FilesRecievedEvent(this,movedFiles));
            }
          }
          catch(Throwable ie)
          {
             String msg = "Directory Monitor: Exception handling recieved files.";
             logger.error(msg);
             System.out.println(msg);
             ie.printStackTrace();
          }

          files = null;
          monitor.sleep(interval);

        }

    }
    catch(Exception e)
    {
       String msg = "Directory Monitor: Exception while monitoring " + this.directory;
       logger.error(msg);
       System.out.println(msg);
       logger.error(e);
    }
  }

   public File[] getFilesArray()
  {
     File[] currentContents = this.directory.listFiles();

     return currentContents;
  }
   /**
   *  Gets the file contents of the monitored directory
   */
  public Vector getFiles(String ext)
  {
     File[] contents = this.directory.listFiles(new MonitorFilenameFilter(ext));

     //if no files return null
     if(contents == null || contents.length == 0) return null;

     Vector files = new Vector(contents.length);
     File current = null;

     for(int i = 0; i < contents.length; i++)
     {
       if(!contents[i].isDirectory())
       {
         current = contents[i];

         if(current != null  && current.exists() && current.canRead() && current.canWrite())
         {
           String name = current.getName();
           files.add(contents[i]);
         }
       }
     }

     if(files.isEmpty()) return null;

     try{
       Vector retV = sortFilesByDate(files);
       return retV;
     }catch(Exception exc){
       return files;
     }
     //return files;
  }

  private Vector sortFilesByDate(Vector inpVect)throws Exception
  {
    Vector retVect = new java.util.Vector( inpVect.size() );
    Iterator it = inpVect.iterator();
    boolean inserted;
    while( it.hasNext() ){
      File oneFile = (File)it.next();
      if( retVect.isEmpty() ){
        retVect.addElement(oneFile);
        continue;
      }
      inserted = false;
      for(int i=0; i<retVect.size(); i++){
        File f = (File)retVect.elementAt(i);
        if(oneFile.lastModified() <= f.lastModified() ){
          inserted = true;
          retVect.add(i,oneFile);   // insert the file
          break;
        }
      }
      if( inserted ){
        continue;
      }
      retVect.addElement(oneFile);   // append the file
    }
    return retVect;
  }

  private void recoverIncomplete() throws IOException
  {
    File[] contents = pickup.listFiles(new MonitorFilenameFilter("edf"));

    if(contents == null || contents.length == 0) return;

    for(int i = 0; i < contents.length; i++)
    {
      IOUtil.copyAndDelete(contents[i], getInboundDirectory(), getInboundExtension());
    }
  }


  class MonitorFilenameFilter implements FilenameFilter
  {
     String ext;

     public MonitorFilenameFilter(String extension)
     {
       ext = extension;
     }

     public boolean accept(File f, String name)
     {
       if(ext == null || ext.length() <= 1)
        return true;
       else if(name.endsWith(ext))
        return true;

       return false;
     }

  }

}




