package com.basis100.deal.ingest.application;

import MosSystem.Mc;
import com.basis100.deal.entity.*;
import org.w3c.dom.*;
import com.basis100.resources.*;
import com.basis100.deal.xml.*;
import com.basis100.deal.util.*;
import com.basis100.entity.*;
import com.basis100.picklist.PicklistData;
import com.basis100.xml.*;
import com.basis100.log.*;
import java.util.*;
import com.filogix.util.Xc;

/**
 * Title:  SEAN issue #957
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 * 
 * @version 1.1  <br>
 * Date: 06/30/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *	Provide a means to create an association of service branch association to deal<br>
 *	- added method processServiceBranch( Deal , Element )<br>
 *
 * 
 */

public class SpecialProcessingHandler
{
  private SysLogger logger;
  SessionResourceKit srk = null;
  public SpecialProcessingHandler(SessionResourceKit pSrk){
    this.srk = pSrk;
    logger = srk.getSysLogger();
  }


  /**
   * processPartyProfile
   * 
   * @version
   * Date: 11/03/2006<br>
   * Change: <br>
   * 	Added check for service branch type 
   * 
   */
  public void processPartyProfile(Deal pDeal, Element dealNode) throws Exception
  {
    // Catherine: fix for DJ-specific origination branch processing --- begin ---------
    boolean isSpecialOrigProcessing = (PropertiesCache.getInstance().getProperty(
            srk.getExpressState().getDealInstitutionId(),
            "com.basis100.ingestion.process.origination.branch","N")
            .equalsIgnoreCase("y"));
    // Catherine: fix for DJ-specific origination branch processing --- end ---------

    //  ***** Change by NBC/PP Implementation Team - Defect #614 - START *****//
    boolean isServiceBranchCheckActive = PropertiesCache.getInstance().getProperty(
            srk.getExpressState().getDealInstitutionId(),
            "com.basis100.party.includeServiceBranch" , "N" )
            .equalsIgnoreCase( "Y" );
    
    // extract sourceof business category id from node
    Node sbNode = DomUtil.getFirstNamedDescendant(dealNode,"SourceOfBusinessProfile");
    if( sbNode == null ){
		logger.warning("No Node name: SourceOfBusinessProfile");
		return;
	}
    String sbCategoryId = DomUtil.getChildValue(sbNode, "sourceOfBusinessCategoryId");
	logger.debug("Deal/SourceOfBusinessProfile/sourceOfBusinessCategoryId: " +
			   sbCategoryId);
    if( sbCategoryId == null ) {
		logger.warning("No sourceOfBusinessCategoryId!");
		return ;
	}
    int lSBCategoryId = Integer.parseInt(sbCategoryId.trim() ) ;

    String lPartyCompanyName;
    String lPartyName;
    int lPartyTypeId;

    PartyProfile pp = new PartyProfile(srk);
	// for check the property id for each DEAL and PartyProfile asso
	Property propertyDAO = new Property(srk);
    int shouldContinue ;

    List ppList = DomUtil.getNamedChildren(dealNode,"PartyProfile");
    Iterator it = ppList.iterator();
	logger.info("Checking each party profile ...");
    while( it.hasNext() ){
      Node ppNode = (Node)it.next();
      lPartyCompanyName = DomUtil.getChildValue(ppNode, "partyCompanyName");
      lPartyName = DomUtil.getChildValue(ppNode, "partyName");
      lPartyTypeId = Integer.parseInt( DomUtil.getChildValue(ppNode, "partyTypeId") );

	  logger.debug("======Processing SourceOfBusinessCategoryId=" + lSBCategoryId
				 + " PartyTypeId=" + lPartyTypeId + "...");
      shouldContinue = pp.extractIngestionProfileStatus(lSBCategoryId,lPartyTypeId);
      if(shouldContinue == -1){
		  logger.debug("Not satisfied! go to next PartyProfile.");
		  continue;
	  }

      //  Catherine: fix for DJ-specific origination branch processing --- begin ---------
      // skip Origination branch because it was processed before
      if (isSpecialOrigProcessing && lPartyTypeId == Mc.PARTY_TYPE_ORIGINATION_BRANCH) {
        logger.debug("isSpecialOrigProcessing = " + isSpecialOrigProcessing + ". Skipping origination branch record...");
        continue; 
      }
      //  Catherine: fix for DJ-specific origination branch processing --- end ---------
	  // ***** Change by NBC/PP Implementation Team - Defect #614 - START *****//
      // Skip over service branch as well, as it was already processed
  	  if ( isServiceBranchCheckActive && lPartyTypeId == Mc.PARTY_TYPE_SERVICE_BRANCH ) {
  		logger.debug("isServiceBranchCheckActive = " + isServiceBranchCheckActive + ". Skipping service branch record...");
        continue; 
  	  }
  	  // ***** Change by NBC/PP Implementation Team - Defect #614 - END *****//  
      
	  Collection profiles = null;
  	  if (lPartyTypeId == Mc.PARTY_TYPE_APPRAISER) {
		  profiles = pp.findByNameAndTypeId(lPartyName, lPartyTypeId);
	  } else {
		  profiles = pp.findByNameCompanyNameAndTypeId(lPartyName,
													   lPartyCompanyName,
													   lPartyTypeId);
	  }
	  // the party profile we are going to process.
	  PartyProfile onePartyProfile = null;
      if( (profiles) == null || (profiles.isEmpty()) ){
		  logger.debug("No PartyProfile exist! Create New One!");
        onePartyProfile = new PartyProfile(srk);
        onePartyProfile = onePartyProfile.create(lPartyTypeId,0);
        // handle contact
        Contact lContact = new Contact(srk);
        lContact = lContact.create();
        onePartyProfile.setContactId( lContact.getContactId() );
        updatePartyProfileFromNode(onePartyProfile, ppNode);
        //onePartyProfile.setCopyId(1);
        onePartyProfile.setProfileStatusId(shouldContinue);
        // zivko:marker set copyid to 1
      }else{
          Iterator profIt = profiles.iterator();
          while( profIt.hasNext() ){
			  logger.debug("Processing the first PartyProfile...");
            onePartyProfile = (PartyProfile)profIt.next();
            break;  // just process the first one
          }
      }

	  // try to set the property id.
	  int propertyId = -1;
  	  if (onePartyProfile.getPartyTypeId() == Mc.PARTY_TYPE_APPRAISER) {
  		  logger.debug("Checking the property ID for Appraiser");
		  Collection props =
			  propertyDAO.findByDealIdAndPrimaryFlag(pDeal.getDealId(), 'Y');
		  logger.debug("Found Amount: " + props.size());
		  for (Iterator i = props.iterator(); i.hasNext(); ) {
			  Property prop = (Property) i.next();
			  propertyId = prop.getPropertyId();
			  break; // find the first one.
		  }
	  }

	  logger.debug("Making association: DealId=" + pDeal.getDealId() +
				 " PartyProfileId=" + onePartyProfile.getPartyProfileId() +
				 " PropertyID=" + propertyId);
	  onePartyProfile.associate( pDeal.getDealId(), propertyId );
	  onePartyProfile.ejbStore();
    } // end while
    
    logger.info("[INGESTION] " + this.getClass().getName() + " processPartyProfile  - 4"   );
  }

  
  
	  /**
	   * Method : processServiceBranch( Deal , Element )
	   * Associates Service Brach to deal
	   *
       * @param deal - Deal entity to associate
       * @param dealNode - DOM element where service branch information
       * @throws Exception : Instantiation of PartyProfile entity or lookup fails 
	   */
  
  public void processServiceBranch( Deal deal , Element dealNode ) throws Exception 
  {
	  	// check flag, is service branch enabled
	  boolean isServiceBranchCheckActive = PropertiesCache.getInstance()
                .getProperty(srk.getExpressState().getDealInstitutionId(),
                "com.basis100.party.includeServiceBranch", "N")
                .equalsIgnoreCase("Y");
        if ( isServiceBranchCheckActive ) 
	  {
		  	// get list of party profiles from dom and iterate to find service branch type
		  List partyProfiles  = DomUtil.getNamedChildren( dealNode , "PartyProfile" );
		  Iterator itProfiles = partyProfiles.iterator();
		  
		  while( itProfiles.hasNext() )
		  {
			  Node ppNode = ( Node ) itProfiles.next();
			  String partyTypeId = DomUtil.getChildValue( ppNode , "partyTypeId" );
			  		// if party profile with service branch found, lookup party profile entity and associate it to the deal
			  if ( partyTypeId != null && Integer.parseInt( partyTypeId.trim() ) == Mc.PARTY_TYPE_SERVICE_BRANCH ) 
			  {
				  String partyBusinessId = DomUtil.getChildValue( ppNode , "ptPBusinessId" );
				  PartyProfile partyProfile = new PartyProfile( srk );
				  try{
					  partyProfile = partyProfile.findByBusinessIdAndType( partyBusinessId , Mc.PARTY_TYPE_SERVICE_BRANCH );
					  partyProfile.associate( deal.getDealId() );
				  }catch(Exception e){
					  logger.error("Can not find the party profile. ptpBusinessID = " + partyBusinessId);
				  }
				  break;
			  }
		  }
	  }
  }

  public void processOriginationBranch(Deal pDeal, Element dealNode) throws Exception
  {
    String lProcessOriginationBranch = PropertiesCache.getInstance().getProperty(
            srk.getExpressState().getDealInstitutionId(),
            "com.basis100.ingestion.process.origination.branch","N");
	logger.info("com.basis100.ingestion.process.origination.branch = " +
			  lProcessOriginationBranch);

    if(lProcessOriginationBranch.equalsIgnoreCase("n")){
      return;
    }
    Node sfNode = DomUtil.getFirstNamedDescendant(dealNode,"SourceFirmProfile");
    if( sfNode == null ){ return; }
    String sfCode = DomUtil.getChildValue(sfNode, "sourceFirmCode");
	logger.info("Deal/SourceFirmProfile/sourceFirmCode: " + sfCode);
    if( sfCode == null ){return;}
    if( !sfCode.equals("CPD") ){ return; }

    Node sbNode = DomUtil.getFirstNamedDescendant(dealNode,"SourceOfBusinessProfile");
    if( sbNode == null ){ return; }
    String sbCategoryId = DomUtil.getChildValue(sbNode, "sourceOfBusinessCategoryId");
	logger.info("Deal/SourceOfBusinessProfile/sourceOfBusinessCategoryId: " +
			  sbCategoryId);
    if( sbCategoryId == null ){ return ; }
    if( Integer.parseInt(sbCategoryId.trim() ) != 11 ){ return; }

    List nl = DomUtil.getNamedChildren(dealNode,"PartyProfile");
    if(nl.size() == 0){
		logger.warning("No PartyProfile in Deal Node.");
		return;
	}

    Iterator it = nl.iterator();
    String lPartyBusinessId = null;
	  logger.debug("Trying to find the PartyProfile with the PartyTypeId = Mc.PARTY_TYPE_ORIGINATION_BRANCH");
    while(it.hasNext()){
      Node lNode = (Node) it.next();
      String lPartyType = DomUtil.getChildValue(lNode,"partyTypeId");
      if(lPartyType != null && Integer.parseInt(lPartyType.trim()) == Mc.PARTY_TYPE_ORIGINATION_BRANCH ){
        lPartyBusinessId = DomUtil.getChildValue(lNode,"ptPBusinessId");
        break;
      }
    }
	logger.debug("Deal/PartyProfile/ptPBusinessId: " + lPartyBusinessId);
    PartyProfile pf = null;
    try{
      pf = new PartyProfile(srk);
      pf = pf.findByBusinessIdAndType(lPartyBusinessId, Mc.PARTY_TYPE_ORIGINATION_BRANCH);
	  logger.debug("Found Party Profile: " + pf.getPartyProfileId());
    } catch( FinderException fExc ){
		logger.error("Can not found the party profile." + fExc.toString());
      return;
    }
    if(pf == null){ return; }
	logger.info("Making association with deal: PartyProfile[" +
			  pf.getPartyProfileId() + "] -- Deal[" + pDeal.getDealId() + "]");
    pf.associate(pDeal.getDealId());
  }

  public void processMIInsurerRequest(Deal pDeal, Element dealNode) throws Exception
  {
    //==========================================================================
    // ###
    // ingestion process has changed because of desjardins
    // this functionality must come after calcs finish their processing
    //==========================================================================
    String lSetMtgInsur = PropertiesCache.getInstance().getProperty(
            srk.getExpressState().getDealInstitutionId(),
            "com.basis100.ingestion.set.mortgage.insurer","N");
    logger.info("From conf file: com.basis100.ingestion.set.mortgage.insurer = "
              + lSetMtgInsur);
    // desjardins
    if( lSetMtgInsur.equalsIgnoreCase("d") ){
        logger.info("This is desjardens special request ...");
      // desjardins
      if(dealNode != null)
      {
        Node lOneNode = DomUtil.getFirstNamedChild(dealNode,"MITypeId");
        String miTypeIdNodeValue = DomUtil.getNodeText(lOneNode);
        logger.info("Deal/MITypeID: " + miTypeIdNodeValue);

        lOneNode = DomUtil.getFirstNamedChild(dealNode,"mortgageInsurerId");
        String mortgageInsurerIdValue = DomUtil.getNodeText(lOneNode);
        logger.info("Deal/mortgageInsurerId: " + mortgageInsurerIdValue);

        if(miTypeIdNodeValue != null && mortgageInsurerIdValue != null &&
           ! miTypeIdNodeValue.trim().equals("") && ! mortgageInsurerIdValue.trim().equals("")
          )
        {
          pDeal.setMITypeId(Integer.parseInt(miTypeIdNodeValue));
          pDeal.setMortgageInsurerId(Integer.parseInt(mortgageInsurerIdValue));
          pDeal.ejbStore();
        }
      }
    }else if( lSetMtgInsur.equalsIgnoreCase("c") ){
      // cervus financial
      // 3.2 patch LTV project	
      double lLtv = Double.parseDouble(
              PropertiesCache.getInstance().getProperty(
                      srk.getExpressState().getDealInstitutionId(),
                      Xc.ING_LTV_THRESHOLD,"80"));
      if( pDeal.getCombinedLTV() > lLtv){
          logger.info("Cervus functionality for high ratio deal.");
          pDeal.setMortgageInsurerId( findRandomInsurer() );
          pDeal.setMITypeId(Mc.MI_TYPE_FULL_SERVICE);
          pDeal.setMIIndicatorId(Mc.MI_INDICATOR_REQUIRED_STD_GUIDELINES);
          pDeal.setMIPayorId(Mc.MI_PAYOR_BORROWER);
      }else{
          logger.info("Cervus functionality for low ratio deal.");
          pDeal.setMortgageInsurerId( Mc.MI_INSURER_CMHC );
          pDeal.setMITypeId(Mc.MI_TYPE_FULL_SERVICE);
          pDeal.setMIIndicatorId(Mc.MI_INDICATOR_APPLICATION_FOR_LOAN_ASSESSMENT);
          pDeal.setMIPayorId(Mc.MI_PAYOR_BORROWER);
          pDeal.setDealTypeId(Mc.DEAL_TYPE_CONVENTIONAL);
      }
    }
  }

  private int findRandomInsurer(){
    int lRandom = MathUtil.getRandomInt(1, 100);
    String lGePerc = PropertiesCache.getInstance().getProperty(
            srk.getExpressState().getDealInstitutionId(),
            "com.filogix.ingestion.cervus.ge.insurer.random.numbers.percent", "50");

    int lGePercInt = 50;
    try{
      lGePercInt = Integer.parseInt(lGePerc);
    }catch(Exception exc){
      lGePercInt = 50;
    }
    
    if( lGePercInt > lRandom){
      logger.info("Random number is : " + lRandom + " and mossys.properties value is: " + lGePercInt + " GE has been selected.");
      return Mc.MI_INSURER_GE;
    }
    logger.info("Random number is : " + lRandom + " and mossys.properties value is: " + lGePercInt + " CMHC has been selected.");
    return Mc.MI_INSURER_CMHC;
  }

  //============================================================================
  private void setField(DealEntity de, String field, Node value)throws Exception
  {
    if(value.getNodeType() != value.ELEMENT_NODE)
     return;

    String data = DomUtil.getNodeText(value);
	logger.debug("Got data[" + data + "] for field[" + field + "].");

    if(data != null && PicklistData.containsTable(field))
     {
        String id = PicklistData.getIdAsString(field,data);

        if(id != null) data = id;
        else data = "0";

        //boolean picklist = true;
        field = field + "Id";
     }

     if(data != null )
      de.setField(field, data.trim());
  }

  private void updatePartyProfileFromNode(PartyProfile  pProfile, Node pProfileNode )throws Exception
  {

    if((pProfile == null) || !pProfileNode.getNodeName().equals("PartyProfile"))
     return;

    NodeList nl = pProfileNode.getChildNodes();

    for(int i = 0; i < nl.getLength(); i++)
    {
      Node current = nl.item(i);

      String field = current.getNodeName();

      if(field.equals("Contact"))
      {
        readContact(pProfile.getContact(), current);
      }
      else
      {
          setField(pProfile,field,current);
      }
    }

  }
  private void readContact(Contact contact, Node node) throws Exception
  {
    //String val = "";

    if(!node.getNodeName().equals("Contact")) return ;

    NodeList nl = node.getChildNodes();
    int size = nl.getLength();

    for (int i = 0; i < size; i++)
    {
      Node current = nl.item(i);

      String field = current.getNodeName();

      if(field.equals("Addr"))
      {
        Addr add = contact.getAddr();
        NodeList nol = current.getChildNodes();
        int nolsize = nol.getLength();

        for (int j = 0; j < nolsize; j++)
        {
          Node cnode = nol.item(j);
          String name = cnode.getNodeName();

          setField(add,name,cnode);
        }
        add.ejbStore();
      }
      else
      {
        setField(contact,field,current);
      }

    }

    contact.ejbStore();
  }

}
