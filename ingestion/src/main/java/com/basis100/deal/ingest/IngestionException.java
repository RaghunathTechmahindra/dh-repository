package com.basis100.deal.ingest;

public class IngestionException extends Exception {

	private static final long serialVersionUID = -4816824242118039296L;

	public IngestionException(String message, Throwable cause) {
		super(message, cause);
	}

	public IngestionException(String message) {
		super(message);
	}

	public IngestionException(Throwable cause) {
		super(cause);
	}

	public IngestionException() {
	}

}
