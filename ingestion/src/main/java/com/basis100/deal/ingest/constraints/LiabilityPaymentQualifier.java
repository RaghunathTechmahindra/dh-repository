package com.basis100.deal.ingest.constraints;

import com.basis100.deal.calc.ClassId;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.LiabilityType;
import com.basis100.deal.xml.EntityNodeAssociation;
import com.basis100.resources.SessionResourceKit;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class LiabilityPaymentQualifier implements DefaultConstraint
{
  public void execute(SessionResourceKit srk, EntityNodeAssociation ass) throws Exception
  {
    DealEntity de = ass.getEntity();
    try
    {
      String cls = de.getClass().getName();

      if(de.getClassId() == ClassId.LIABILITY)
      {
         if(!ass.isSetField("liabilityPaymentQualifier"))
         {
           Liability li = (Liability)de;
           int type = li.getLiabilityTypeId();

           LiabilityType liType = new LiabilityType(srk,type);
           li.setLiabilityPaymentQualifier(liType.getLiabilityPaymentQualifier());
         }
      }
    }
    catch(Exception e)
    {
       throw new Exception("Default Value not found for " + this.getClass().getName());
    }
    // zivko removed this exception from code because it is not clear why we
    // are throwing exception every time we enter this class and
    // SUCCESSFULY finish processing
    // removed on 28-XI-2003
    //throw new Exception("Default Value not found for " + this.getClass().getName());
  }
}