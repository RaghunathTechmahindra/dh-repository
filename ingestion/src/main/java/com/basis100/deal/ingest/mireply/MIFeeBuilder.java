package com.basis100.deal.ingest.mireply;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.Fee;
import com.basis100.deal.entity.PartyProfile;
import com.basis100.deal.pk.DealPK;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;
import MosSystem.Mc;

/**
 *  Utility class for MIHandlers creates DealFee records
 *
 */

public class MIFeeBuilder
{

  public static DealFee buildFee(SessionResourceKit srk, CalcMonitor dcm, Deal deal, int type, double amt)
  {
     if(amt <= 0) return null;

     DealFee dealfee = null;
     DealFee offsetFee = null;
     Fee fee = null;
     SysLogger logger = srk.getSysLogger();

     try
     {
        dealfee = new DealFee(srk,dcm);
        fee = new Fee(srk,dcm);
        fee = fee.findFirstByType(type);
     }
     catch(Exception e)
     {
        String msg = e.getMessage();

        if(msg == null) msg = "Unknown.";

        msg = "INGESTION: MIREPLY: FeeType for id: " + type + " not found. Reason: " + msg;

        logger.error(msg);
        logger.error(e);
     }

     try
     {
       dealfee = dealfee.create((DealPK)deal.getPk());

       int feeId = fee.getFeeId();

       dealfee.setFeeId(feeId);

       setPayorProfileId(srk, dcm, deal, dealfee, fee);

       dealfee.setFeeAmount(amt);

       setPaymentDate(srk, dcm, deal, dealfee, fee);

       dealfee.setFeePaymentMethodId(fee.getDefaultPaymentMethodId());

       dealfee.setFeeStatusId(Mc.FEE_STATUS_REQUIRED);

       dealfee.ejbStore();

       int offset = checkOffset(dealfee);

       if(offset != 0)
       {
          offsetFee = buildOffset(srk, dcm, deal, offset, amt);
          String info = "INGESTION: MIREPLY: Offset DealFee ";

          if(offsetFee != null)
          {
            info += "built: Offset DealFeeId:" + offsetFee.getDealFeeId() + " OffsetId: " + offset;
            logger.info(info);
          }
          else
          {
            info += "not built: DealFeeId: " + dealfee.getDealFeeId() + " " + deal.getPk().getWhereClause();
            logger.error(info);
          }
       }

     }
     catch(Exception e)
     {
        String msg = e.getMessage();

        if(msg == null) msg = "Unknown.";

        msg = "INGESTION: MIREPLY: Unable to create DealFee record or Offset DealFee. Reason: " + msg;

        logger.error(msg);
        logger.error(e);
        return null;
     }

     return dealfee;
  }

  private static void setPayorProfileId(SessionResourceKit srk, CalcMonitor dcm, Deal deal, DealFee dfee, Fee fee)
  {
    int payorType = fee.getFeePayorTypeId();
    SysLogger logger = srk.getSysLogger();

    switch(payorType)
    {
      case Mc.FEE_PAYOR_TYPE_SOURCE_FIRM:
        dfee.setPayorProfileId(deal.getSourceFirmProfileId());
        break;
      case Mc.FEE_PAYOR_TYPE_SOURCE_OF_BUSINESS:
        dfee.setPayorProfileId(deal.getSourceOfBusinessProfileId());
        break;
      case Mc.FEE_PAYOR_TYPE_SOLICITOR:
        try
        {
          PartyProfile pp = new PartyProfile(srk);
          pp.setSilentMode(true);
          pp = pp.findByDealSolicitor(deal.getDealId());

          if(pp != null)
           dfee.setPayorProfileId(pp.getPartyProfileId());

        }
        catch(Exception e)
        {
          logger.error("INGESTION: CMHC REPLY: unable to determine payor for type: Solicitor");
        }

        break;
      case Mc.FEE_PAYOR_TYPE_APPRAISER:
        try
        {
            PartyProfile pp = new PartyProfile(srk);
            Collection c = pp.findByDealAndType((DealPK)deal.getPk(), Mc.PARTY_TYPE_APPRAISER);
            ArrayList ar = new ArrayList();

            if(c != null)
             ar.addAll(c);

            if((ar.size() > 0))
            {
              pp = (PartyProfile)ar.get(0);
              dfee.setPayorProfileId(pp.getPartyProfileId());
            }

        }
        catch(Exception e)
        {
          logger.error("INGESTION: CMHC REPLY: unable to determine payor for type: Appraiser");
        }

        break;
      case Mc.FEE_PAYOR_TYPE_MORTGAGE_INSURER:
         dfee.setPayorProfileId(Mc.MI_INSURER_CMHC);
        break;
      default:
        break;
    }

    return;
  }

  private static void setPaymentDate(SessionResourceKit srk, CalcMonitor dcm, Deal deal, DealFee dfee, Fee fee)
  {
    SysLogger logger = srk.getSysLogger();
    int dateType = fee.getDefaultFeePaymentDateTypeId();

    switch(dateType)
    {
      case Mc.FEE_PAYMENT_DATE_TYPE_CURRENT_DATE:
        dfee.setFeePaymentDate(new Date());
        break;
        
      case Mc.FEE_PAYMENT_DATE_TYPE_OFFER_RETURN_DATE:
        dfee.setFeePaymentDate(deal.getReturnDate());
        break;

      case Mc.FEE_PAYMENT_DATE_TYPE_CURRENT_DATE_PLUS_10_DAYS:
        try
        {
          Calendar c = Calendar.getInstance();
          c.add(c.DATE,10);

          dfee.setFeePaymentDate(c.getTime());

        }
        catch(Exception e)
        {
          logger.error("INGESTION: CMHC REPLY: unable to determine payor for type: Solicitor");
        }

        break;

      case Mc.FEE_PAYMENT_DATE_TYPE_FUNDS_ADVANCED_DATE:
          dfee.setFeePaymentDate(deal.getEstimatedClosingDate());
        break;

      default:
        break;
    }

    return;
  }


  public static DealFee buildOffset(SessionResourceKit srk, CalcMonitor dcm, Deal deal, int feeId, double amt) throws Exception
  {
    DealFee df = new DealFee(srk, dcm);

    try
    {
      df = new DealFee(srk, dcm);

      if(feeId == 0) return df;

      Fee fee = new Fee(srk, dcm, feeId);

      df.create((DealPK)deal.getPk());
      df.setFeeId(feeId);
      df.setFeePaymentMethodId(fee.getDefaultPaymentMethodId());
      df.setFeeAmount(amt);
      df.ejbStore();
    }
    catch(Exception e)
    {
      String msg = e.getMessage();

      if(msg == null) msg = "Unknown.";

      msg = "Unable to create offset DealFee. Reason: " + msg;

      throw new Exception(msg);
    }

    return df;
  }


  public static int checkOffset(DealFee dealfee)
  {
    try
    {
      Fee fee = dealfee.getFee();

      if(fee == null) return 0;

      int offsetId = fee.getOffSetingFeeTypeId();

      return offsetId;
    }
    catch(Exception e)
    {
     return 0;
    }
  }


} 