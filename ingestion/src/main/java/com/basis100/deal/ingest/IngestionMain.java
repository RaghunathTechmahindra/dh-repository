package com.basis100.deal.ingest;

import com.basis100.picklist.BXResources;

/**
 *  Program entry point for the Ingestion Module
 */
public class IngestionMain
{
  public static void main(String args[])
  {
    String path = null;

    if(args.length > 0)
    {
      path = args[0];
    }

    try
    {
      System.out.println(IngestionManager.getCurrentBuildString(null));
      IngestionManager ingest = new IngestionManager(path,false);
//      BXResources bxResources = new BXResources();
    }
    catch(Exception e)
    {
      System.out.println("Ingestion Manager Failed To Start");
      e.printStackTrace();
    }
  }

}
