/*
 * @(#)IngestionManagerTest.java    2005-3-28
 *
 * Copyright
 */


package com.basis100.deal.ingest;

import java.io.File;
import java.io.BufferedInputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.URL;

import junit.framework.TestCase;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * IngestionManagerTest - 
 *
 * @version   1.0 2005-3-28
 * @author    <A HREF="mailto:chyxiang@yahoo.com">Chen Xiang (Sean)</A>
 */
public class IngestionManagerTest extends TestCase {

    // The logger
    private static Log _log = LogFactory.getLog(IngestionManagerTest.class);

    public static final String CONF_FILE_NAME = "mos-ingestion.properties";

    // the configuration path.
    private String _mosConfFile =
        "c:/working/filogix/xpress/src/conf/mos-ingestion.properties";

    // the ingesting test file.
    private String _ingestFile =
        "c:/FROST_DJ/bin/Ingestion/Morty/deal/Inbound/received/test-1.xml";

    /**
     * Constructor function
     */
    public IngestionManagerTest() {
    }

    /**
     * setting up the test case.
     */
    protected void setUp() {

        // nothing for now.
        // try to find the ingestion configuration file for testing.
        URL resource = IngestionManagerTest.class.getClassLoader().
            getResource(CONF_FILE_NAME);
        _log.debug("Configuration File Path: " + resource.getPath());
        _mosConfFile = resource.getPath();
    }

    /**
     * testing the initializing process.
     */
    public void testInit() {

        _log.info("Test Initializing ...");
        try {
            IngestionManager manager = new IngestionManager();
            //manager.testInit(_mosConfFile);

            //new InputStreamReader(new BufferedInputStream(System.in)).read();
        } catch (FileIngestionException fe) {
            _log.error("Ingestion error: ", fe);
        } /*catch (IOException ie) {
            _log.error("IO Exception: ", ie);
            }*/
    }

    /**
     * testing the ingest process.
     */
    public void testIngest() {

        _log.info("Test Ingesting ...");
        try {
            IngestionManager manager = new IngestionManager();
            //manager.testInit(_mosConfFile);

            manager.ingest(new File(_ingestFile));
        } catch (FileIngestionException fe) {
            _log.error("Ingestion Error: ", fe);
        }
    }

    /**
     * clean up ...
     */
    protected void tearDown() {

        // nothing for now.
    }
}
