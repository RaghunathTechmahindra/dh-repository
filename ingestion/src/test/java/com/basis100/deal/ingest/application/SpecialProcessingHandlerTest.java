/*
 * @(#)SpecialProcessingHandlerTest.java    2005-4-7
 *
 */


package com.basis100.deal.ingest.application;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import junit.framework.TestCase;

/**
 * SpecialProcessingHandlerTest - for testing the special processing handler.
 *
 * @version   1.0 2005-4-7
 * @author    <A HREF="mailto:chyxiang@yahoo.com">Chen Xiang (Sean)</A>
 */
public class SpecialProcessingHandlerTest extends TestCase {

    // The logger
    private static Log _log = LogFactory.getLog(SpecialProcessingHandlerTest.class);

    /**
     * Constructor function
     */
    public SpecialProcessingHandlerTest() {
    }

	/**
	 * setting up the test case.
	 */
	protected void setUp() {
	}

	/**
	 * tear down the resource.
	 */
	protected void tearDown() {
	}

	/**
	 * test the party profile process.
	 */
	public void testProcessPartyProfile() {

		
	}
}
