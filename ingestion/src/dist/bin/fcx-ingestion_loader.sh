#!/bin/sh
#
#==================================================================
PIDFILE=./ingestion.pid
COMMAND="java -classpath . LauncherBootstrap -launchfile fcx-ingestion.xml  ingest"
#=================================================================

case "$1" in
  start)
    if [ -f $PIDFILE ]; then
      PID=`cat $PIDFILE`
      echo "fcx ingestion process seems already be running under PID $PID"
      echo "(PID file $PIDFILE already exists). Check for process..."
      echo `ps -f -p $PID`
      exit 3
    else
      echo "Starting fcx ingestion... "
      $COMMAND &
      PID=$!
      echo $PID > $PIDFILE
      exit 0
    fi
  ;;
  stop)
    if [ ! -f $PIDFILE ]; then
      echo "PID file $PIDFILE not found, unable to determine process to kill..."
      exit 3
    else
      echo "Stopping fcx ingestion... "
      PID=`cat $PIDFILE`
      kill -9 $PID
      rm -f $PIDFILE
      exit 0
    fi
  ;;
  restart)
    $0 stop
    $0 start
  ;;
  *)
    echo ""
    echo "fcx ingestion control"
    echo "-------------"
    echo "Syntax:"
    echo "  $0 {start|stop|restart}"
    echo ""
    exit 3
  ;;
esac
