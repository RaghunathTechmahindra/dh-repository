---------------------------------------------------------------------------------------------------------------------
set linesize 3100
-- Use this to suppress page headers, titles and all formatting
set pagesize 0
-- Don't list the SQL text before/after any variable substitution
set verify off
-- deletes any blank spaces at the end of each spooled line
set trimspool on
-- Don't display number of lines returned by the query
set feedback off
-- Don't display any SELECT output to your screen
set termout off

set serveroutput off
set heading off
set underline off
set echo off

clear col

-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

DOC

This report pulls weekly deal info for Xceed
author: D.Coleman Basis100 Feb.27,2002

--20030107 modify linkage of systemtypeid from deal to firm to systemtype table
--to make up for a shortfall application has in changing the source of business record

--20061010 D.Bellinger.  Massive rewrite to add fields and remove duplication.
--split into two data-sets returning the ordered pair (dealnumber, other-stuff) - one with employment data, one without - then UNION'd.
--ordered pair then concatenated in a SELECT to preserve ORDER BY sorting.  Messy, I know.


-- adding 1 to date = 1 day therefore 1/24 = .0416666666 1 hour and can further divide for minutes
-- use .013888666 to add 20 minutes
-- use .006944333 to add 10 minutes

-- daily XD deal data dump

#

--ttitle "XD - Deal data extract report"
ttitle OFF
btitle OFF

--*** Start of SQL ***

-- get parameter for spool file
col date_stp new_value date_stp noprint
select to_char(sysdate,'YYYYMMDD') date_stp from dual;

--*** Start of Spool Section

spool e:\XDWholeExtract_CSV_test_&date_stp..txt

-- Column headings
select 'Deal|Branch|TotalLoan|NetLoan|CMHCPremium|CurrentStatus|StatusDate|CommitmentIssued|ApplicationDate|FundingDate|GDS|TDS|LTV|GDS3Yr|TDS3Yr|TaxPayor|CombinedLendingValue|AdditionalPrincipal|DownPayment|PostedRate|Discount|Premium|BuyDown|Rate|RateCode|ProductDescription|Term|IAD|FirstPmnt|Maturity|AmortTerm|EffectiveAmort|PaymentOptions|PrivilegePayments|DealPurpose|DealType|PaymentFrequency|Escrow|PandI|TotalPayment|SecondaryFinancing|RateLock|SpecialFeature|NumBorrowers|NumGuarantors|UWUserID|UnderwriterLastName|UnderwriterFirstName|ApprovedByLastName|ApprovedByFirstName|MortgageOfficerLastName|MortgageOfficerFirstName|ClosingDate|FinanceProgram|LineOfBusiness|MortgageInsuranceIndicator|BrokerCompany|BrokerID|AgentShortName|AgentName|AgentPhone|AgentFax|SourceAppID|AgentAddress|AgentCity|AgentProvince|AgentPostal|AgentRegion|SystemType|BorrowerLastName|BorrowerFirstName|BirthDate|Age|Citizenship|BorrowerPhone|BorrowerWorkPhone|Ext|BorrowerFax|BorrowerEmail|BorrowerAddress|BorrowerCity|BorrowerProvince|BorrowerPostal|YearsAtAddress|ResidentialStatus|MaritalStatus|Staff|CreditScore|BorrowerGDS|BorrowerTDS|BorrowerGDS3yr|BorrowerTDS3yr|NetWorth|TotalIncome|Bankruptcy|TimesBankrupt|LanguagePreference|ExistingClient|ClientRefNumber|StreetNumber|StreetName|StreetType|StreetDirection|Unit|PropertyCity|PropertyProvince|PropertyPostal|DwellingType|DwellingStyle|PropertyType|OccupancyType|NumberOfUnits|Watertype|NewConstruction|PropertyUsage|PropertyZoning|PurchasePrice|LendingValue|ActualAppraisal|TotalAnnualTax|PrimaryProperty|PropertyLocation|ServiceNum|LegalLine1|DenialReason|SourceTier|RefCompany|RefName|RefAddress|CSDescription|MCCMarketType|IncomeType|Occupation|TotalIncomeAmount|IndustrySector|AppraisalDate|EmploymentPeriod' from dual;
select ' ' from dual;

--SQL
select tt1.dealid||'|'||
       tt1.other_stuff
  from (SELECT a.dealid,
    c.branchname||'|'||
    a.totalloanamount||'|'||
    a.netloanamount||'|'||
    a.mipremiumamount||'|'||
    b.statusdescription||'|'||
    ltrim(to_char(a.statusdate, 'MM/DD/YYYY HH24:MI:SS'))||'|'||
    ltrim(to_char(a.commitmentissuedate, 'MM/DD/YYYY HH24:MI:SS'))||'|'||
    ltrim(to_char(a.applicationdate, 'MM/DD/YYYY HH24:MI:SS'))||'|'||
    ltrim(to_char(a.actualclosingdate, 'MM/DD/YYYY'))||'|'||
    a.combinedGDS||'|'||
    a.combinedTDS||'|'||
    a.combinedLTV||'|'||
    a.combinedGDS3year||'|'||
    a.combinedTDS3year||'|'||
    am.tpdescription||'|'||
    a.combinedlendingvalue||'|'||
    a.additionalprincipal||'|'||
    a.downpaymentamount||'|'||
    a.postedrate||'|'||
    a.discount||'|'||
    a.premium||'|'||
    a.buydownrate||'|'||
    a.netinterestrate||'|'||
    m.ratecode||'|'||
    m.ratecodedescription||'|'||
    round(a.actualpaymentterm/12)||'|'||
    ltrim(to_char(a.interiminterestadjustmentdate, 'MM/DD/YYYY'))||'|'||
    ltrim(to_char(a.firstpaymentdate, 'MM/DD/YYYY'))||'|'||
    ltrim(to_char(a.maturitydate, 'MM/DD/YYYY'))||'|'||
    round(a.amortizationterm/12,1)||'|'||
    round(a.effectiveamortizationmonths/12,1)||'|'||
    x.podescription||'|'||
    aw.ppdescription||'|'||
    ag.dpdescription||'|'||
    au.dtdescription||'|'||
    ah.pfdescription||'|'||
    a.escrowpaymentamount||'|'||
    a.pandipaymentamount||'|'||
    a.totalpaymentamount||'|'||
    a.secondaryfinancing||'|'||
    a.ratelock||'|'||
    y.sfdescription||'|'||
    a.numberofborrowers||'|'||
    a.numberofguarantors||'|'||
    j.userlogin||'|'||
    k.contactlastname||'|'||
    k.contactfirstname||'|'||
    bc.contactlastname||'|'||
    bc.contactfirstname||'|'||
    trim((SELECT CASE WHEN a.administratorid IS NULL THEN '' ELSE be.contactlastname END FROM DEAL za, CONTACT zbe WHERE za.dealid = a.dealid AND zbe.contactid = be.contactid AND ROWNUM = 1))||'| '||
    trim((SELECT CASE WHEN a.administratorid IS NULL THEN '' ELSE be.contactfirstname END FROM DEAL za, CONTACT zbe WHERE za.dealid = a.dealid AND zbe.contactid = be.contactid AND ROWNUM = 1))||'|'||
    ltrim(to_char(a.estimatedClosingDate, 'MM/DD/YYYY'))||'|'||
    ax.fpdescription||'|'||
    ay.lobdescription||'|'||
    az.miidescription||'|'||
    f.sourcefirmname||'|'||
    a.sourcesystemmailboxnbr||'|'||
    g.sobpshortname||'|'||
    ai.contactfirstname||' '||
    ai.contactlastname||'|'||
    ai.contactphonenumber||'|'||
    ai.contactfaxnumber||'|'||
    a.sourceapplicationid||'|'||
    aj.addressline1||' '||
    aj.addressline2||'|'||
    aj.city||'|'||
    ak.provincename||'|'||
    aj.postalfsa||' '||
    aj.postalldu||'|'||
    al.SOBREGIONDESCRIPTION||'|'||
    an.systemtypedescription||'|'||
    d.borrowerlastname||'| '||
    d.borrowerfirstname||'|'||
    ltrim(to_char(d.borrowerbirthdate, 'MM/DD/YYYY'))||'|'||
    d.age||'|'||
    z.ctdescription||'|'||
    d.borrowerhomephonenumber||'|'||
    d.borrowerworkphonenumber||'|'||
    d.borrowerworkphoneextension||'|'||
    d.borrowerfaxnumber||'|'||
    d.borroweremailaddress||'|'||
    ad.addressline1||' '||
    ad.addressline2||'|'||
    ad.city||'|'||
    ae.provincename||'|'||
    ad.postalfsa||' '||
    ad.postalldu||'|'||
    ROUND(ac.monthsataddress/12,1)||'|'||
    af.residentialstatusname||'|'||
    w.msdescription||'|'||
    d.staffoflender||'|'||
    d.creditscore||'|'||
    d.gds||'|'||
    d.tds||'|'||
    d.gds3year||'|'||
    d.tds3year||'|'||
    d.networth||'|'||
    d.totalincomeamount||'|'||
    aa.bankruptcystatusdescription||'|'||
    d.numberoftimesbankrupt||'|'||
    ab.lpdescription||'|'||
    d.existingclient||'|'||
    d.clientreferencenumber||'|'||
    e.propertystreetnumber||'|'||
    e.propertystreetname||'|'||
    n.stdescription||'|'||
    r.sddescription||'|'||
    e.unitnumber||'|'||
    e.propertycity||'|'||
    h.provincename||'|'||
    e.propertypostalFSA||' '||
    e.propertypostalLDU||'|'||
    o.dtdescription||'|'||
    av.dsdescription||'|'||
    p.ptdescription||'|'||
    q.otdescription||'|'||
    e.numberofunits||'|'||
    s.wtdescription||'|'||
    t.newconstructiondescription||'|'||
    u.pudescription||'|'||
    v.propertyzoningdescription||'|'||
    e.purchaseprice||'|'||
    e.lendingvalue||'|'||
    e.actualappraisalvalue||'|'||
    e.totalannualtaxamount||'|'||
    e.primarypropertyflag||'|'||
    bg.propertylocationdescription||'|'||
    a.servicingmortgagenumber||'|'||
    e.legalline1||'|'||
    zz.drdescription||'|'||
    g.tierlevel||'|'||
    trim((select partycompanyname from partyprofile pp, partydealassoc pa where pa.dealid = a.dealid and pa.partyprofileid = pp.partyprofileid and pp.partytypeid = 61))||'|'||
    trim((select partyname from partyprofile pp, partydealassoc pa where pa.dealid = a.dealid and pa.partyprofileid = pp.partyprofileid and pp.partytypeid = 61))||'|'||
    trim((select addressline1 from addr ad, contact c, partyprofile pp, partydealassoc pa where pa.dealid = a.dealid and pa.partyprofileid = pp.partyprofileid and pp.contactid = c.contactid and c.addrid = ad.addrid and pp.partytypeid = 61))||'|'||
    bh.csdescription||'|'||
    a.MCCMARKETTYPE||'|'||
    bi.itdescription||'|'||
    bl.odescription||'|'||
    d.totalincomeamount||'|'||
    bm.isdescription||'|'||
    e.AppraisalDateAct||'|'||
    bk.monthsOfService other_stuff
FROM
    deal a,
    status b,
    branchprofile c,
    borrower d,
    property e,
    sourcefirmprofile f,
    sourceofbusinessprofile g,
    province h,
    lenderprofile i,
    userprofile j,
    contact k,
    pricingrateinventory l,
    pricingprofile m,
    streettype n,
    dwellingtype o,
    propertytype p,
    occupancytype q,
    streetdirection r,
    watertype s,
    newconstruction t,
    propertyusage u,
    propertyzoning v,
    maritalstatus w,
    prepaymentoptions x,
    specialfeature y,
    citizenshiptype z,
    bankruptcystatus aa,
    languagepreference ab,
    borroweraddress ac,
    addr ad,
    province ae,
    residentialstatus af,
    dealpurpose ag,
    paymentfrequency ah,
    contact ai,
    addr aj,
    province ak,
    sobregion al,
    taxpayor am,
    systemtype an,
    dealtype au,
    dwellingstyle av,
    privilegepayment aw,
    financeprogram ax,
    lineofbusiness ay,
    miindicator az,
    contact bc,
    userprofile bd,
    contact be,
    userprofile bf,
    denialreason zz,
    propertylocation bg,
    crosssellprofile bh,
    incometype bi,
    income bj,
    employmenthistory bk,
    occupation bl,
    industrysector bm,
    (select ddd.dealid dealnumber
      from borrower bbb, deal ddd
     where bbb.primaryborrowerflag = 'Y'
       and ddd.dealid = bbb.dealid and bbb.copyid = ddd.copyid and ddd.scenariorecommended = 'Y' and ddd.denialreasonid <> 18
       and bbb.borrowerid in (select eee.borrowerid from employmenthistory eee where eee.borrowerid = bbb.borrowerid and eee.copyid = bbb.copyid and eee.employmenthistorystatusid = 0)
     ) foo
WHERE
    a.scenariorecommended = 'Y'
    and a.copytype <> 'T'
    and a.denialreasonid <> 18
    and a.statusid = b.statusid
    and a.branchprofileid = c.branchprofileid (+)
    and a.prepaymentoptionsid = x.prepaymentoptionsid
    and a.specialfeatureid = y.specialfeatureid
    and a.dealpurposeid = ag.dealpurposeid
    and a.paymentfrequencyid = ah.paymentfrequencyid
    and a.sourcefirmprofileid = f.sourcefirmprofileid (+)
    and f.systemtypeid = an.systemtypeid (+)
    and a.sourceofbusinessprofileid = g.sourceofbusinessprofileid (+)
    and g.sobregionid = al.sobregionid (+)
    and g.contactid = ai.contactid (+)
    and ai.addrid = aj.addrid (+)
    and aj.provinceid = ak.provinceid (+)
    and a.lenderprofileid = i.lenderprofileid (+)
    and a.underwriteruserid = j.userprofileid
    and j.contactid = k.contactid
    and a.PRICINGPROFILEID = l.PRICINGRATEINVENTORYID (+)
    and l.pricingprofileid = m.pricingprofileid (+)
    and a.taxpayorid = am.taxpayorid (+)
    and a.dealtypeid = au.dealtypeid
    and a.privilegepaymentid = aw.privilegepaymentid
    and a.financeprogramid = ax.financeprogramid
    and a.lineofbusinessid = ay.lineofbusinessid
    and a.miindicatorid = az.miindicatorid
    and a.finalapproverid = bd.userprofileid (+)
    and bd.contactid = bc.contactid (+)
    and a.administratorid = bf.userprofileid
    and bf.contactid = be.contactid
    and a.denialreasonid = zz.denialreasonid (+)
    and a.crosssellprofileid = bh.crosssellprofileid
    and d.dealid = a.dealid
    and d.copyid = a.copyid
    and d.primaryBorrowerFlag = 'Y'
    and d.maritalstatusid = w.maritalstatusid
    and d.citizenshiptypeid = z.citizenshiptypeid
    and d.bankruptcystatusid = aa.bankruptcystatusid
    and d.languagepreferenceid = ab.languagepreferenceid
    and ac.borrowerid = d.borrowerid
    and ac.copyid = d.copyid
    and ac.borroweraddresstypeid = 0
    and ac.residentialstatusid = af.residentialstatusid
    and ac.borroweraddressid in
        (select min(borroweraddressid) from borroweraddress yy where yy.borrowerid = d.borrowerid and yy.copyid = d.copyid and yy.borroweraddresstypeid = 0)
    and ad.addrid = ac.addrid
    and ad.copyid = ac.copyid
    and ad.provinceid = ae.provinceid
    and e.dealid (+) = a.dealid
    and e.copyid (+) = a.copyid
    and e.primarypropertyflag (+) = 'Y'
    and e.provinceid = h.provinceid (+)
    and e.streettypeid = n.streettypeid (+)
    and e.dwellingtypeid = o.dwellingtypeid (+)
    and e.dwellingstyleid = av.dwellingstyleid (+)
    and e.propertytypeid = p.propertytypeid (+)
    and e.occupancytypeid = q.occupancytypeid (+)
    and e.streetdirectionid = r.streetdirectionid (+)
    and e.watertypeid = s.watertypeid (+)
    and e.newconstructionid = t.newconstructionid (+)
    and e.propertyusageid = u.propertyusageid (+)
    and e.zoning = v.propertyzoningid (+)
    and e.propertylocationid = bg.propertylocationid (+)
    and bk.borrowerid = d.borrowerid
    and bk.copyid = d.copyid
    and bk.EMPLOYMENTHISTORYSTATUSID = 0
    and bk.employmenthistoryid in
        (select min(xx.employmenthistoryid) from employmenthistory xx where xx.borrowerid = d.borrowerid and xx.copyid = d.copyid and xx.employmenthistorystatusid = 0)
    and bl.occupationid = bk.occupationid
    and bj.incomeid(+) = bk.incomeid
    and bj.copyid (+) = bk.copyid
    and bj.incometypeid = bi.incometypeid (+)
    and bk.industrysectorid = bm.industrysectorid
    and a.dealid = foo.dealnumber
UNION --set without data in employment or income.
SELECT a.dealid,
    c.branchname||'|'||
    a.totalloanamount||'|'||
    a.netloanamount||'|'||
    a.mipremiumamount||'|'||
    b.statusdescription||'|'||
    ltrim(to_char(a.statusdate, 'MM/DD/YYYY HH24:MI:SS'))||'|'||
    ltrim(to_char(a.commitmentissuedate, 'MM/DD/YYYY HH24:MI:SS'))||'|'||
    ltrim(to_char(a.applicationdate, 'MM/DD/YYYY HH24:MI:SS'))||'|'||
    ltrim(to_char(a.actualclosingdate, 'MM/DD/YYYY'))||'|'||
    a.combinedGDS||'|'||
    a.combinedTDS||'|'||
    a.combinedLTV||'|'||
    a.combinedGDS3year||'|'||
    a.combinedTDS3year||'|'||
    am.tpdescription||'|'||
    a.combinedlendingvalue||'|'||
    a.additionalprincipal||'|'||
    a.downpaymentamount||'|'||
    a.postedrate||'|'||
    a.discount||'|'||
    a.premium||'|'||
    a.buydownrate||'|'||
    a.netinterestrate||'|'||
    m.ratecode||'|'||
    m.ratecodedescription||'|'||
    round(a.actualpaymentterm/12)||'|'||
    ltrim(to_char(a.interiminterestadjustmentdate, 'MM/DD/YYYY'))||'|'||
    ltrim(to_char(a.firstpaymentdate, 'MM/DD/YYYY'))||'|'||
    ltrim(to_char(a.maturitydate, 'MM/DD/YYYY'))||'|'||
    round(a.amortizationterm/12,1)||'|'||
    round(a.effectiveamortizationmonths/12,1)||'|'||
    x.podescription||'|'||
    aw.ppdescription||'|'||
    ag.dpdescription||'|'||
    au.dtdescription||'|'||
    ah.pfdescription||'|'||
    a.escrowpaymentamount||'|'||
    a.pandipaymentamount||'|'||
    a.totalpaymentamount||'|'||
    a.secondaryfinancing||'|'||
    a.ratelock||'|'||
    y.sfdescription||'|'||
    a.numberofborrowers||'|'||
    a.numberofguarantors||'|'||
    j.userlogin||'|'||
    k.contactlastname||'|'||
    k.contactfirstname||'|'||
    bc.contactlastname||'|'||
    bc.contactfirstname||'|'||
    trim((SELECT CASE WHEN a.administratorid IS NULL THEN '' ELSE be.contactlastname END FROM DEAL za, CONTACT zbe WHERE za.dealid = a.dealid AND zbe.contactid = be.contactid AND ROWNUM = 1))||'| '||
    trim((SELECT CASE WHEN a.administratorid IS NULL THEN '' ELSE be.contactfirstname END FROM DEAL za, CONTACT zbe WHERE za.dealid = a.dealid AND zbe.contactid = be.contactid AND ROWNUM = 1))||'|'||
    ltrim(to_char(a.estimatedClosingDate, 'MM/DD/YYYY'))||'|'||
    ax.fpdescription||'|'||
    ay.lobdescription||'|'||
    az.miidescription||'|'||
    f.sourcefirmname||'|'||
    a.sourcesystemmailboxnbr||'|'||
    g.sobpshortname||'|'||
    ai.contactfirstname||' '||
    ai.contactlastname||'|'||
    ai.contactphonenumber||'|'||
    ai.contactfaxnumber||'|'||
    a.sourceapplicationid||'|'||
    aj.addressline1||' '||
    aj.addressline2||'|'||
    aj.city||'|'||
    ak.provincename||'|'||
    aj.postalfsa||' '||
    aj.postalldu||'|'||
    al.SOBREGIONDESCRIPTION||'|'||
    an.systemtypedescription||'|'||
    d.borrowerlastname||'| '||
    d.borrowerfirstname||'|'||
    ltrim(to_char(d.borrowerbirthdate, 'MM/DD/YYYY'))||'|'||
    d.age||'|'||
    z.ctdescription||'|'||
    d.borrowerhomephonenumber||'|'||
    d.borrowerworkphonenumber||'|'||
    d.borrowerworkphoneextension||'|'||
    d.borrowerfaxnumber||'|'||
    d.borroweremailaddress||'|'||
    ad.addressline1||' '||
    ad.addressline2||'|'||
    ad.city||'|'||
    ae.provincename||'|'||
    ad.postalfsa||' '||
    ad.postalldu||'|'||
    ROUND(ac.monthsataddress/12,1)||'|'||
    af.residentialstatusname||'|'||
    w.msdescription||'|'||
    d.staffoflender||'|'||
    d.creditscore||'|'||
    d.gds||'|'||
    d.tds||'|'||
    d.gds3year||'|'||
    d.tds3year||'|'||
    d.networth||'|'||
    d.totalincomeamount||'|'||
    aa.bankruptcystatusdescription||'|'||
    d.numberoftimesbankrupt||'|'||
    ab.lpdescription||'|'||
    d.existingclient||'|'||
    d.clientreferencenumber||'|'||
    e.propertystreetnumber||'|'||
    e.propertystreetname||'|'||
    n.stdescription||'|'||
    r.sddescription||'|'||
    e.unitnumber||'|'||
    e.propertycity||'|'||
    h.provincename||'|'||
    e.propertypostalFSA||' '||
    e.propertypostalLDU||'|'||
    o.dtdescription||'|'||
    av.dsdescription||'|'||
    p.ptdescription||'|'||
    q.otdescription||'|'||
    e.numberofunits||'|'||
    s.wtdescription||'|'||
    t.newconstructiondescription||'|'||
    u.pudescription||'|'||
    v.propertyzoningdescription||'|'||
    e.purchaseprice||'|'||
    e.lendingvalue||'|'||
    e.actualappraisalvalue||'|'||
    e.totalannualtaxamount||'|'||
    e.primarypropertyflag||'|'||
    bg.propertylocationdescription||'|'||
    a.servicingmortgagenumber||'|'||
    e.legalline1||'|'||
    zz.drdescription||'|'||
    g.tierlevel||'|'||
    trim((select partycompanyname from partyprofile pp, partydealassoc pa where pa.dealid = a.dealid and pa.partyprofileid = pp.partyprofileid and pp.partytypeid = 61))||'|'||
    trim((select partyname from partyprofile pp, partydealassoc pa where pa.dealid = a.dealid and pa.partyprofileid = pp.partyprofileid and pp.partytypeid = 61))||'|'||
    trim((select addressline1 from addr ad, contact c, partyprofile pp, partydealassoc pa where pa.dealid = a.dealid and pa.partyprofileid = pp.partyprofileid and pp.contactid = c.contactid and c.addrid = ad.addrid and pp.partytypeid = 61))||'|'||
    bh.csdescription||'|'||
    a.MCCMARKETTYPE||'|Other|Other|'||'|0|Other|'||
    e.AppraisalDateAct||'|0' other_stuff
FROM
    deal a,
    status b,
    branchprofile c,
    borrower d,
    property e,
    sourcefirmprofile f,
    sourceofbusinessprofile g,
    province h,
    lenderprofile i,
    userprofile j,
    contact k,
    pricingrateinventory l,
    pricingprofile m,
    streettype n,
    dwellingtype o,
    propertytype p,
    occupancytype q,
    streetdirection r,
    watertype s,
    newconstruction t,
    propertyusage u,
    propertyzoning v,
    maritalstatus w,
    prepaymentoptions x,
    specialfeature y,
    citizenshiptype z,
    bankruptcystatus aa,
    languagepreference ab,
    borroweraddress ac,
    addr ad,
    province ae,
    residentialstatus af,
    dealpurpose ag,
    paymentfrequency ah,
    contact ai,
    addr aj,
    province ak,
    sobregion al,
    taxpayor am,
    systemtype an,
    dealtype au,
    dwellingstyle av,
    privilegepayment aw,
    financeprogram ax,
    lineofbusiness ay,
    miindicator az,
    contact bc,
    userprofile bd,
    contact be,
    userprofile bf,
    denialreason zz,
    propertylocation bg,
    crosssellprofile bh,
    (select ddd.dealid dealnumber
      from borrower bbb, deal ddd
     where bbb.primaryborrowerflag = 'Y'
       and ddd.dealid = bbb.dealid and bbb.copyid = ddd.copyid and ddd.scenariorecommended = 'Y' and ddd.denialreasonid <> 18
       and bbb.borrowerid not in (select eee.borrowerid from employmenthistory eee where eee.borrowerid = bbb.borrowerid and eee.copyid = bbb.copyid and eee.employmenthistorystatusid = 0)
     ) foo
WHERE
    a.scenariorecommended = 'Y'
    and a.copytype <> 'T'
    and a.denialreasonid <> 18
    and a.statusid = b.statusid
    and a.branchprofileid = c.branchprofileid (+)
    and a.prepaymentoptionsid = x.prepaymentoptionsid
    and a.specialfeatureid = y.specialfeatureid
    and a.dealpurposeid = ag.dealpurposeid
    and a.paymentfrequencyid = ah.paymentfrequencyid
    and a.sourcefirmprofileid = f.sourcefirmprofileid (+)
    and f.systemtypeid = an.systemtypeid (+)
    and a.sourceofbusinessprofileid = g.sourceofbusinessprofileid (+)
    and g.sobregionid = al.sobregionid (+)
    and g.contactid = ai.contactid (+)
    and ai.addrid = aj.addrid (+)
    and aj.provinceid = ak.provinceid (+)
    and a.lenderprofileid = i.lenderprofileid (+)
    and a.underwriteruserid = j.userprofileid
    and j.contactid = k.contactid
    and a.PRICINGPROFILEID = l.PRICINGRATEINVENTORYID (+)
    and l.pricingprofileid = m.pricingprofileid (+)
    and a.taxpayorid = am.taxpayorid (+)
    and a.dealtypeid = au.dealtypeid
    and a.privilegepaymentid = aw.privilegepaymentid
    and a.financeprogramid = ax.financeprogramid
    and a.lineofbusinessid = ay.lineofbusinessid
    and a.miindicatorid = az.miindicatorid
    and a.finalapproverid = bd.userprofileid (+)
    and bd.contactid = bc.contactid (+)
    and a.administratorid = bf.userprofileid
    and bf.contactid = be.contactid
    and a.denialreasonid = zz.denialreasonid (+)
    and a.crosssellprofileid = bh.crosssellprofileid
    and d.dealid = a.dealid
    and d.copyid = a.copyid
    and d.primaryBorrowerFlag = 'Y'
    and d.maritalstatusid = w.maritalstatusid
    and d.citizenshiptypeid = z.citizenshiptypeid
    and d.bankruptcystatusid = aa.bankruptcystatusid
    and d.languagepreferenceid = ab.languagepreferenceid
    and ac.borrowerid = d.borrowerid
    and ac.copyid = d.copyid
    and ac.borroweraddresstypeid = 0
    and ac.residentialstatusid = af.residentialstatusid
    and ac.borroweraddressid in
        (select min(borroweraddressid) from borroweraddress yy where yy.borrowerid = d.borrowerid and yy.copyid = d.copyid and yy.borroweraddresstypeid = 0)
    and ad.addrid = ac.addrid
    and ad.copyid = ac.copyid
    and ad.provinceid = ae.provinceid
    and e.dealid (+) = a.dealid
    and e.copyid (+) = a.copyid
    and e.primarypropertyflag (+) = 'Y'
    and e.provinceid = h.provinceid (+)
    and e.streettypeid = n.streettypeid (+)
    and e.dwellingtypeid = o.dwellingtypeid (+)
    and e.dwellingstyleid = av.dwellingstyleid (+)
    and e.propertytypeid = p.propertytypeid (+)
    and e.occupancytypeid = q.occupancytypeid (+)
    and e.streetdirectionid = r.streetdirectionid (+)
    and e.watertypeid = s.watertypeid (+)
    and e.newconstructionid = t.newconstructionid (+)
    and e.propertyusageid = u.propertyusageid (+)
    and e.zoning = v.propertyzoningid (+)
    and e.propertylocationid = bg.propertylocationid (+)
    and a.dealid = foo.dealnumber
) tt1
order by tt1.dealid asc
--SQL

/
spool OFF
exit
