package com.filogix.externalservice;

import java.io.StringReader;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventLocator;
import javax.xml.bind.Validator;
import javax.xml.bind.util.ValidationEventCollector;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
*
* <p>Title: Filogix Express - AIG UG</p>
*
* <p>Description: Filogix AIGUG MI Project</p>
*
* <p>Copyright: Copyright (c) 2006</p>
*
* <p>Company: Filogix</p>
*
* @author Clement Cheng
* @version 1.0
*/

public class ResponseValidater {
	private String JaxbContext;
	static Log _log = LogFactory.getLog(ResponseValidater.class.getName());
	
	public void setJaxbContext(String JaxbContext)
	{
		this.JaxbContext = JaxbContext;
	}
	public boolean validate (String requestBody) throws Exception 
	{
		try {
    	    JAXBContext context = JAXBContext.newInstance(this.JaxbContext);
    		_log.info("JAXBContext: " + context);
    		Unmarshaller um = context.createUnmarshaller();
            _log.info("got UnMarshaller: " + um);
    		Validator va = context.createValidator();
            _log.info("got validator: " + va);
            
            ValidationEventCollector collector = new ValidationEventCollector();
            va.setEventHandler(collector);
            Object o = string2jaxb(um,requestBody);
            boolean valid = va.validate(o);
            
            if (valid) return valid;
            else {
                  System.out.println("There are " + collector.getEvents().length +
                  " events...");
              for (int i = 0; i < collector.getEvents().length; i++)
              {
                  // Validation failed.
                  ValidationEvent event = collector.getEvents()[i];
                  System.out.println("\nEvent #" + i);
                  System.out.println("Message: " + event.getMessage() +
                      " Savirty: " + event.getSeverity());
                  ValidationEventLocator locator = event.getLocator();
                  System.out.println("Column: " + locator.getColumnNumber());
                  System.out.println("Line: " + locator.getLineNumber());
                  System.out.println("Object: " + locator.getObject());
                  System.out.println("Node: " + locator.getNode());
                  System.out.println("URL: " + locator.getURL());
                  System.out.println("Offset: " + locator.getOffset());
              }
              String msg =
                  "Validation failed for object type '" + o.getClass().getName();
              return false;
            }
		} catch (JAXBException e) {
		    _log.info( "validation failed with JAXBException");
		    return false;
		}

	}
	private static Object string2jaxb(Unmarshaller unmarshaller, String element) throws Exception
	{
		return unmarshaller.unmarshal(new StreamSource(new StringReader(element)));
    }
}
