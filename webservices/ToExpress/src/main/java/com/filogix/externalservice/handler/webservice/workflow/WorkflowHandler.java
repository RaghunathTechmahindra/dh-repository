/**
 * <p>Title: WorkflowManager.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � Jul 1, 2006)
 *
 */

package com.filogix.externalservice.handler.webservice.workflow;

import MosSystem.Mc;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.WFEExternalTaskCompletion;
import com.basis100.workflow.WFETrigger;
import com.basis100.workflow.WFTaskExecServices;
import com.filogix.externalservice.ServiceConst;

public class WorkflowHandler implements ServiceConst
{
    private WFEExternalTaskCompletion _extObj = null;
    private int _statusId = -1;
    private int _requestSubTypeId = -1;
    private SessionResourceKit _srk;

    public WorkflowHandler(int requestSubTypeId, int statusId, SessionResourceKit srk)
    {
        this._requestSubTypeId = requestSubTypeId;
        this._statusId = statusId;
        _srk = srk;
        _extObj = getExtTaskComp(_requestSubTypeId, _statusId);
    }


    public void workflowTrigger( int userId, int dealId, int copyId)
        throws Exception
    {
          SysLogger logger = _srk.getSysLogger();
    
          logger.debug("@WS.workflowTrigger:" );
          /** workflowTrigger
          *
          * Workflow activation trigger.
          *
          *   Type - 0 - task completion activity
          *          1 - workflow selection and initial task generation
          *          2 - as 1) but add unconditional asnc state (test) task generation (e.g. possible MI task(s))
          *
          **/
//          public static void workflowTrigger(int type, SessionResourceKit srk, int userId, int dealId,
//                  int copyId, int userTypeId, WFEExternalTaskCompletion extObj) throws Exception
          WFETrigger.workflowTrigger(2, _srk, userId, dealId, copyId,  Mc.USER_TYPE_AUTO, _extObj);
    }
     
    private static WFEExternalTaskCompletion getExtTaskComp(int requestSubType, int statusId)
    {
        switch (requestSubType)
        {
            case SERVICE_SUB_TYPE_APPRAISAL:
                switch (statusId)
                {
                    case RESULT_CODE_COMPLETED_CONFIRMED:
                        return null;
                    case RESULT_CODE_QUOTATION_RECEIVED:
                        return null;
                    case RESULT_CODE_CANCELLATION_ACCEPTED:
                        return null;
                    default:
                        return null;
                }
            default: return null;
        }
    }
}