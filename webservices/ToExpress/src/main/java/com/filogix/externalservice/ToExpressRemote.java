/**
 * <p>Title: ResponseServiceSoap.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � Mar 14, 2006)
 *
 */

package com.filogix.externalservice;

import java.rmi.RemoteException;

public interface ToExpressRemote extends ExternallinkRemote
{   
    public ServiceResponse invoke(MessageDescriptor requestDescriptor, String requestBody) 
        throws RemoteException;
}