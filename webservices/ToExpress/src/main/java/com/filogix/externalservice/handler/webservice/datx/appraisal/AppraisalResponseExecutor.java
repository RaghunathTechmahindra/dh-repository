/**
 * <p>Title: AppraisalResultRemoteStub.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version ? Apr 3, 2006)
 *
 */

package com.filogix.externalservice.handler.webservice.datx.appraisal;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import com.basis100.deal.entity.AppraisalSummary;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.ServiceRequest;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.pk.ServiceRequestPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.picklist.BXResources;
import com.filogix.externallinks.services.datx.DatxResponseCodeAppraisalConverter;
import com.filogix.externalservice.ServiceConst;
import com.filogix.externalservice.ServiceException;
import com.filogix.externalservice.handler.webservice.workflow.WorkflowHandler;
import com.filogix.schema.datx.DatXUpdateStatus;
import com.filogix.schema.datx.appraisal.CentractReport;
import com.filogix.schema.datx.appraisal.CentractReportType;

public class AppraisalResponseExecutor extends AppraisalStatusUpdateExecutor implements ServiceConst
{
    private static final String MESSAGE_TYPE = MESSAGE_TYPE_STATUS_UPDATE;
    private static final String PAYLOAD_TYPE = PAYLOAD_TYPE_APPRAISAL_RESPONSE_REPORT;
    public DatXUpdateStatus _updateStatus = null;
    public CentractReportType _appReportResponse = null; 
    
    public AppraisalResponseExecutor()
    {
        super();
    }
    
    
    public void initMe() throws ServiceException, JAXBException
    {
        _response.getMessageDescriptor().setMessageType(MESSAGE_TYPE);
        if (_request instanceof DatXUpdateStatus)
        {
            _updateStatus = (DatXUpdateStatus)_request;
        }
        _appReportResponse = (CentractReport)getPayload(_requestBody);
        
    }
    
    public void process() throws ServiceException
    {
        _log.info("---->> AppraisalResponseExecutor.process()");
        _statusDate = new Date();
        DatxResponseCodeAppraisalConverter codeConverter = new DatxResponseCodeAppraisalConverter();
        _statusId = codeConverter.converetToFxpStatusCode(_updateStatus.getResultCode());
        if (_statusId == RESPONSE_STATUS_SYSTEM_ERROR)
        {
            _statusDesc = BXResources.getSysMsg(EXTERNAL_LINK_SYSTEM_ERROR, _langId);
        }
        else
        {
        _statusDesc = _updateStatus.getResultDescription();
        if (_statusDesc == null || _statusDesc.trim().equals(""))
        {
        try
        {
            if (getMessageTable() != null)
            {
                _statusDesc = BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(),
                                                                 getMessageTable(), _statusId, _langId );
            }
        }
        catch(Exception e)
        {
                    _log.error(this.getClass().getName()+ ": No Status Message found");
                }
            }
        }

        try
        {
            try
            {
                _srk.beginTransaction();
                updateRequest();
                _log.info("-------->> update Request done");
                insertResponse();
                _log.info("-------->> insert Response done");
                updateAppraisalSummary();
                _log.info("-------->> insert AppraisalSummary done");
                updatePropertyValue();
                _log.info("-------->> update property value done");
                setWorkflowTrigger();
                _log.info("-------->> set trigger for Workflow done");
                _srk.commitTransaction();
                _srk.freeResources();
                _response.setMessageDescriptor(_requestDesc);
                _response.setTransactionStatus(RESULT_CODE_SUCCEED);
                _response.setStatusMessage(RESULT_DESC_SUCCEED);
                _response.setResponseBody("");
                
            }
            catch(Exception e)
            {
                _log.error(StringUtil.stack2string(e));
                _statusId = RESPONSE_STATUS_PROCESSING_ERROR;
                _statusDesc = e.getClass().getName();
                
                _srk.cleanTransaction();
                _srk.beginTransaction();
                try 
                {
                  updateRequest();
                  _srk.commitTransaction();
    
                } 
                catch (Exception e1)
                {
                  _log.error(StringUtil.stack2string(e1));
                  _srk.cleanTransaction();
                }
                finally
                {
                    ServiceException se = new ServiceException(_statusId, _statusDesc);
                    throw se;
                }
            }
        }
        catch (JdbcTransactionException e1)
        {
            _log.error(StringUtil.stack2string(e1));
            ServiceException se = new ServiceException
                (RESULT_CODE_PROCESSING_ERROR,  "Error when processing DatX reponse for requestId = " + _requestDesc.getClientKey() + " :" + e1.getMessage());
            throw se;
        } finally {
            _srk.freeResources();
        }
    }
    
    /*
     * save Response record
     */
    private void updateAppraisalSummary() throws RemoteException, FinderException, CreateException
    {
        AppraisalSummary apps = new AppraisalSummary(_srk);
      
        apps = apps.create(_responseId);
        apps.setActualAppraisalValue(_appReportResponse.getAppraisalValue());
        if (_appReportResponse.getAppraisalDate() != null)
        {
        	apps.setAppraisalDate(_appReportResponse.getAppraisalDate().getTime());
        }
        apps.setAppraisalReport(_appReportResponse.getPdfReport());  
        apps.ejbStore();        
    }
  
    private void updatePropertyValue() throws RemoteException, FinderException, CreateException
    {
        Request requestEntity = new Request(_srk);
        Collection requests = requestEntity.findRequestsByRequestId(_requestId);
        Iterator iReq = requests.iterator();
        Set propPKs = new HashSet();
        while (iReq.hasNext())
        {
            Request r = (Request)iReq.next();
        
            try
            {
                ServiceRequest serviceRequest = new ServiceRequest(_srk);
                serviceRequest = serviceRequest.findByPrimaryKey(new ServiceRequestPK(
                    r.getRequestId(), r.getCopyId()));
                if (serviceRequest.getPropertyId() > 0)
                {
                    int propertyId = serviceRequest.getPropertyId();
                    int copyId = serviceRequest.getCopyId();
                    PropertyPK propPK = new PropertyPK(propertyId, copyId);
                    if (!propPKs.contains(propPK))
                    {
				        Property property = new Property(_srk);
				        property = property.findByPrimaryKey(propPK);
				        double eValue = property.getActualAppraisalValue();
				        if (eValue == 0)
				        {
				            property.setActualAppraisalValue(_appReportResponse.getAppraisalValue());
				            if (_appReportResponse.getAppraisalDate() != null)
				            	property.setAppraisalDateAct(_appReportResponse.getAppraisalDate().getTime());
				            property.ejbStore();
                        }
                        propPKs.add(propPK);
                    }
                }
            }
            catch(FinderException fe)
            {
                // ignore
            }
        }
    }
    
    
    private void setWorkflowTrigger() throws Exception
    {
        _log.debug("--> AppraisalResponseExecutor.setWorkflowTrigger() -- Calling Workflow");
        WorkflowHandler wfHander = new WorkflowHandler(SERVICE_SUB_TYPE_APPRAISAL, _statusId, _srk);
        int dealId = -1;
        Deal deal = null;
        
        try 
        {
            deal = getRequestedDeal();
            if (deal == null) return;
                        
            wfHander.workflowTrigger(deal.getUnderwriterUserId(), 
                                     deal.getDealId(), deal.getCopyId());
        }
        catch(Exception e)
        {
            _log.error("Exception @AppraisalResponseExecutor.setWorkflowTrigger():");
            _log.error(e);
            throw e;
        }
    }
    
    public String  addNamespace(String response)
    {
        String newString = new String(response);
        newString = super.addNamespace(newString);
        
        String payloadNamespace = getPayloadNamespace();
        String payloadName = getPayloadName();
        if(response.indexOf(getPayloadNamespace(), response.indexOf(payloadName)) < 0 )
        {
            String addingString =  " xmlns=\"" + payloadNamespace + "\"";
            newString = newString.replaceFirst(payloadName, payloadName + addingString);
        }
        return newString;
    }

    public static String replace(String source, String pattern, String replace)
    {
        if (source!=null)
        {
        final int len = pattern.length();
        StringBuffer sb = new StringBuffer();
        int found = -1;
        int start = 0;

        while( (found = source.indexOf(pattern, start) ) != -1) {
            sb.append(source.substring(start, found));
            sb.append(replace);
            start = found + len;
        }

        sb.append(source.substring(start));

        return sb.toString();
        }
        else return "";
    }

    public String getMessageName()
    {
        String messasgeName = DatXUpdateStatus.class.getName();
        return messasgeName.substring(messasgeName.lastIndexOf(".")+1, messasgeName.length());
    }
    
    public String getPayloadName()
    {
        String messasgeName = CentractReport.class.getName();
        return messasgeName.substring(messasgeName.lastIndexOf(".")+1, messasgeName.length());
    }
    
    public String getNamespace()
    {
        return DEFAULT_NAMESPACE_DATX;
    }

    public String getPayloadNamespace()
    {
        return DEFAULT_NAMESPACE_DATX + "/appraisal";
    }
    
    public JAXBContext getReqJaxbContext() throws JAXBException
    {
        ClassLoader loader = DatXUpdateStatus.class.getClassLoader();
        JAXBContext context = JAXBContext.newInstance(DatXUpdateStatus.class.getPackage().getName(),  
                                                      DatXUpdateStatus.class.getClassLoader());
        return context;
    }

    public JAXBContext getResJaxbContext() throws JAXBException
    {
        _log.debug("2: " + DatXUpdateStatus.class);
        JAXBContext context = JAXBContext.newInstance(DatXUpdateStatus.class.getPackage().getName());
        return context;
    }

    public JAXBContext getPayloadJaxbContext() throws JAXBException
    {
        _log.debug("3: " + CentractReportType.class);
        JAXBContext context = JAXBContext.newInstance(CentractReportType.class.getPackage().getName());
        return context;
    }

}
