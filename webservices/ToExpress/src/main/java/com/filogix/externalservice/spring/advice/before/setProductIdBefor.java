package com.filogix.externalservice.spring.advice.before;

import java.lang.reflect.Method;

import org.springframework.aop.MethodBeforeAdvice;

import com.filogix.externalservice.MessageDescriptor;

public class setProductIdBefor implements MethodBeforeAdvice{
	private String productType;
	public void setProductType(String type)
	{
		this.productType = type;
	}
	public void before(Method m, Object[] args, Object target) throws Throwable
	{
		MessageDescriptor _requestDesc = (MessageDescriptor) args[0];
		_requestDesc.setProductType(this.productType);
	}
	

}
