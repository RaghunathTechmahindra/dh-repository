package com.filogix.externalservice.handler;

import javax.xml.bind.JAXBException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.Request;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externalservice.MessageDescriptor;
import com.filogix.externalservice.ServiceConst;
import com.filogix.externalservice.ServiceException;
import com.filogix.externalservice.ServiceResponse;

/**
*
* <p>Title: Filogix Express - AIG UG</p>
*
* <p>Description: Filogix AIGUG MI Project</p>
*
* <p>Copyright: Copyright (c) 2007</p>
*
* <p>Company: Filogix</p>
*
* @author Clement Cheng
* @version 1.0
*/

public abstract class ResponseCallBackHandler implements IResponseCallBackHandler, ServiceConst {
    
    public abstract ServiceResponse handleResponse(MessageDescriptor messageDescriptor, String requestBody)
			throws ServiceException;
	
	public abstract void init(MessageDescriptor messagedescriptor, String s, Log log, SessionResourceKit sessionresourcekit)
		throws JAXBException, RemoteException, ServiceException;

	public ServiceResponse doProcess(MessageDescriptor messagedescriptor, String s, Log log, SessionResourceKit sessionresourcekit)
		throws JAXBException, RemoteException, ServiceException
	{
	    init(messagedescriptor,s,log,sessionresourcekit);
		return handleResponse(messagedescriptor,s);
	}
	
	  protected int setVPD(MessageDescriptor  requestDesc, SessionResourceKit srk) throws ServiceException 
	  {
	    MessageDescriptor _requestDesc = requestDesc;
	//  String request = (String) args[1]; //-- doesn�t use it 
	    Log log = LogFactory.getLog(ResponseCallBackHandler.class);
        int insitutionId = -1;
	    try
	    {
		//Dave was here - begin
	      String ctk;// = _requestDesc.getTransactionID();
			if (_requestDesc.getMessageType().equals("Genworth") || _requestDesc.getMessageType().equals("CMHC"))
				ctk = _requestDesc.getClientKey().substring(9);//substring to strip out the urn:uuid: at the start.
			else
				ctk = _requestDesc.getTransactionID();
		//Dave was here - end.
		  Request request = new Request(srk);
	      String msg = "";
	      
	      if(MESSAGE_TYPE_STATUS_UPDATE.equals(_requestDesc.getMessageType())
	                      || MESSAGE_TYPE_APPRAISAL_RESPONSE_REPORT.equals(_requestDesc.getMessageType())) {
	          msg = "Transactionkey: " + ctk + " ProductType: " + _requestDesc.getProductType();
	          // get first record by Datx TransactionKey and ProductType
	          request = request.findByChannelTranKeyAndServProduct(ctk, _requestDesc.getProductType());
	      } else {
	          msg = "Transactionkey: " + ctk + " RequestType: " + _requestDesc.getMessageType();
	          // get first record by Datx TransactionKey and RequestType
	          request = request.findByChannelTranKeyAndServProduct(ctk, _requestDesc.getMessageType());           
	      }
	      log.debug(msg);
	      if (request != null ) {
	          log.debug("Found Request: RequestId = " + request.getRequestId() + " InstitutionId = " + request.getInstitutionProfileId());
	          log.debug("Found Request: DealId = " + request.getDealId());
	          insitutionId = request.getInstitutionProfileId();
	          srk.getExpressState().setDealInstitutionId(insitutionId); 
	      } else {
	          throw new ServiceException(RESPONSE_STATUS_PROCESSING_ERROR,
	              "Request doesn't exist");
	      }
	    }
	    catch(Exception e)
	    {
	      log.debug(e.getMessage()); // think later
	      throw new ServiceException(12, e.getMessage());
	    }
	    log.debug("-------->> setVPD() done");
	    return insitutionId;
	  }

}
