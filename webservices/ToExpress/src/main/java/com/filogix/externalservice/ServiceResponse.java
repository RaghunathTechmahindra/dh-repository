/**
 * <p>Title: OutgoingResponse.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � Apr 26, 2006)
 *
 */

package com.filogix.externalservice;

public class ServiceResponse 
{
    /**
     * @param args
     */
    // OK, Error, Exception was Thrown
    private MessageDescriptor _desc;
    private int _transactionStatusID;  // mandatory
    private String _statusMessage;  // can be null
    private String _responseBody; // can be null
        
    public String getStatusMessage()
    {
        return _statusMessage;
    }
    public void setStatusMessage(String statusMessage)
    {
        _statusMessage = statusMessage;
    }
    public int getTransactionStatus()
    {
        return _transactionStatusID;
    }
    public void setTransactionStatus(int transactionStatus)
    {
        _transactionStatusID = transactionStatus;
    }
    
    public MessageDescriptor getMessageDescriptor()
    {
        return _desc;
    }
    public void setMessageDescriptor(MessageDescriptor desc)
    {
        _desc = desc;
    }
    public String getResponseBody()
    {
        return _responseBody;
    }
    public void setResponseBody(String body)
    {
        _responseBody = body;
    }
    
    public String toString()
    {
        return new StringBuffer()
            .append(super.toString())
            .append("TransactionStatusID: " + _transactionStatusID + MessageDescriptor.CR)
            .append("StatusMessage: " + _statusMessage + MessageDescriptor.CR)
            .append("ResponseBody: " + _responseBody + MessageDescriptor.CR).toString();
    }
    
}
