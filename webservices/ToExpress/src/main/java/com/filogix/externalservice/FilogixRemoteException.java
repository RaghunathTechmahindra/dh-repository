/**
 * <p>Title: FilogixRemoteException.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � Apr 3, 2006)
 *
 */

package com.filogix.externalservice;

import java.rmi.RemoteException;

public class FilogixRemoteException extends RemoteException
{
    public FilogixRemoteException()
    {
        super();
    }
    
    public FilogixRemoteException(String message)
    {
        super(message);
    }
    
}
