/**
 * <p>Title: Util.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � May 25, 2006)
 *
 */

package com.filogix.externalservice;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.logging.Log;

public class Util
{
    private Log _log;
    
    public Util(Log log)
    {
        this._log = log;
    }
    

    public Date getDateFromString(String strDate, DateFormat format) 
    {

        Date date = new Date();
        try 
        {
            date = format.parse(strDate);
        } 
        catch (ParseException e) 
        {
            _log.error("Error parsing date: " + e);
        }

        return date;
     }
    // the following 2 methods should move to com.basis100.util.date.DateUtilJavaFmt as public 
    private static Date getDateFromString(String strDate)
    {
        final String format1 = "MMM dd, yyyy";            // 
        final String format2 = "yyyy-MM-dd HH:mm:ssz";    //  2002-05-01 00:00:00EDT
        final String format3 = "yyyy-MM-dd HH:mm:ss.S";   //  1972-01-01 00:00:00.0
        final String format4 = "yyyy-MM-dd HH:mm:ss";     //  2006-05-31 14:46:25
        final String format5 = "dd-M-yyyy";
        final String format6 = "dd-MM-yyyy";
        final String format7 = "dd/MM/yyyy hh:mm:ss a";   // 21/08/2006 9:00:00 AM
        
        SimpleDateFormat df1 = new SimpleDateFormat(format1);     
        SimpleDateFormat df2 = new SimpleDateFormat(format2);     
        SimpleDateFormat df3 = new SimpleDateFormat(format3);     
        SimpleDateFormat df4 = new SimpleDateFormat(format4);     
        SimpleDateFormat df5 = new SimpleDateFormat(format5);
        SimpleDateFormat df6 = new SimpleDateFormat(format6); 
        SimpleDateFormat df7 = new SimpleDateFormat(format7);     

        System.out.println("Parsing date: " + strDate);
        Date date = new Date();
        
        try {
          date = df1.parse(strDate);
        } catch (ParseException e1) {
          System.out.println("df1: Error parsing date using " + format1  + ": " + e1 + ". Retrying using " + format2);
          try {
            date = df2.parse(strDate);
          } catch (ParseException e2) {
            System.out.println("df2: Error parsing date using " + format2  + ": " + e2 + ". Retrying using " + format3);
            try {
              date = df3.parse(strDate);
            } catch (ParseException e3) {
              System.out.println("df3: Error parsing date using " + format3  + ": " + e3 + ". Retrying using " + format4);
              try {
                date = df4.parse(strDate);
              } catch (ParseException e4) {
                System.out.println("df4: Error parsing date using " + format4  + ": " + e4 + ". Retrying using " + format5);
                try {
                  date = df5.parse(strDate);
                } catch (ParseException e5) {
                  System.out.println("df5: Error parsing date using " + format5  + ": " + e5 + ". Retrying using " + format6);
                  try {
                    date = df6.parse(strDate);
                  } catch (ParseException e6) {
                    System.out.println("df6: Error parsing date using " + format6  + ": " + e6 );
                    try {
                        date = df7.parse(strDate);
                      } catch (ParseException e7) {
                        System.out.println("df7: Error parsing date using " + format7  + ": " + e7 );
                      }
                  }
                }
              }
            }
        } 
        }

        return date;
     }

    public static String getJaxbDateFmt(String dateStr)
    {
        return  new SimpleDateFormat("yyyy-MM-dd-HH:mm").format(getDateFromString(dateStr));
    }
}
