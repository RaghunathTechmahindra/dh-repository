/**
 * <p>Title: AcceptedErrorException.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � Aug 11, 2006)
 *
 */

package com.filogix.externalservice;

public class AcceptedServiceException extends ServiceException
{
}
