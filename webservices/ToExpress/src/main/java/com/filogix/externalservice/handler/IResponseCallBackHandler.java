package com.filogix.externalservice.handler;

import javax.xml.bind.JAXBException;

import org.apache.commons.logging.Log;

import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externalservice.MessageDescriptor;
import com.filogix.externalservice.ServiceException;
import com.filogix.externalservice.ServiceResponse;

/**
*
* <p>Title: Filogix Express - AIG UG</p>
*
* <p>Description: Filogix AIGUG MI Project</p>
*
* <p>Copyright: Copyright (c) 2007</p>
*
* <p>Company: Filogix</p>
*
* @author Clement Cheng
* @version 1.0
*/

public interface IResponseCallBackHandler{
	public ServiceResponse doProcess(MessageDescriptor messagedescriptor, String s, Log log, SessionResourceKit sessionresourcekit)
	  throws JAXBException, RemoteException, ServiceException;

}
