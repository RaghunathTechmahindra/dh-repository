/**
 * <p>Title: WSSignInService.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � May 11, 2006)
 *
 */

package com.filogix.externalservice;
import com.basis100.resources.SessionResourceKit;
import com.basis100.deal.entity.UserProfile;
import com.basis100.resources.PropertiesCache;
import com.basis100.deal.security.PasswordEncoder;
import java.util.Calendar;
import java.util.Date;
import org.apache.commons.logging.Log;
import MosSystem.Sc;
import com.basis100.deal.security.DLM;

public class WSSignInService implements ServiceConst
{
    private SessionResourceKit _srk;
    private Log _log;
    
    public WSSignInService(SessionResourceKit srk, Log log)
    {
        this._srk = srk;
        this._log = log;
    }

    public void logout(String i_strUserId) throws Exception
    {
        try
        {
            UserProfile userProfile =getUserProfile(i_strUserId, _srk);
            int intUserProfielId = userProfile.getUserProfileId();
            _srk.beginTransaction();
            userProfile.setLoginFlag(ServiceConst.NO);
            userProfile.setLastLogoutTime(new Date());
            userProfile.ejbStore();
            _srk.commitTransaction();
        }
        catch(Exception ex)
        {
            _srk.cleanTransaction();
            throw ex;
        }
        finally
        {
            _srk.freeResources();
        }
    }

    public UserProfile getUserProfile(String i_strUserId, SessionResourceKit i_srk) throws ServiceException
    {
        try
        {
            UserProfile userProfile = new UserProfile(i_srk);
            userProfile.findByLoginId(i_strUserId);
            return userProfile;
        }
        catch(Exception e)
        {
            // log            
            throw new ServiceException(RESULT_CODE_PROCESSING_ERROR,
                                       "AUTHENTIFICATION_FAILED");
        }
    }

    public void login(String strUserId, String i_strPw) throws ServiceException
    {
        _log.info("***************Login Process*****************");
        _log.info("UserId: " + strUserId);
        _log.info("Password: " + i_strPw);
        try
        {
            UserProfile userProfile = getUserProfile(strUserId, _srk);
            int intProfileStatusId = userProfile.getProfileStatusId();

            // user status has to be active.
            if(intProfileStatusId != ServiceConst.USER_PROFILE_STATUS_ACTIVE)
               throw new ServiceException(RESULT_CODE_PROCESSING_ERROR, "AUTHENTIFICATION_FAILED");
            // if password is not required, then don't check father
            if (userProfile.getPassword() == null || userProfile.getPassword().trim().equals("")) return;
            
            // to check if the password is locked?
            if (userProfile.isPasswordLocked())
                throw new ServiceException(RESULT_CODE_PROCESSING_ERROR, "AUTHENTIFICATION_FAILED");

            String strPw = i_strPw;
            //to check if the password is matched to the password retrived from db.
            if(userProfile.getPassword().equals(strPw))
            {
                Calendar calendar = Calendar.getInstance();
                Date dteCurrent = calendar.getTime();
                _srk.beginTransaction();
                userProfile.setPasswordFailedAttempts(Sc.PASSWORD_FAILED_ATTEMPTS_TRUE);
                userProfile.setLoginFlag(ServiceConst.YES);
                userProfile.setLastLoginTime(dteCurrent);
                userProfile.ejbStore();
                _srk.commitTransaction();
            }
            else
            {
                throw new ServiceException(RESULT_CODE_PROCESSING_ERROR, "AUTHENTIFICATION_FAILED");
            }
        }
        catch(Exception ex)
        {
            _srk.cleanTransaction();
            _log.info(this.getClass().getName() + " Exception occured :" + ex.getMessage() );
            throw new ServiceException(RESULT_CODE_SYSTEM_ERROR, ex.getMessage());
        }
    }
}
