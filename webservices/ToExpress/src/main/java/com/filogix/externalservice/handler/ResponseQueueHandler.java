package com.filogix.externalservice.handler;

import java.util.Date;

import javax.xml.bind.JAXBException;
import javax.xml.bind.ValidationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.Response;
import com.basis100.deal.entity.ServiceResponseQueue;
import com.basis100.deal.pk.DealPK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externalservice.MessageDescriptor;
import com.filogix.externalservice.ResponseValidater;
import com.filogix.externalservice.ResponseValidaterFactory;
import com.filogix.externalservice.ServiceConst;
import com.filogix.externalservice.ServiceException;
import com.filogix.externalservice.ServiceResponse;
//import com.filogix.externalservice.handler.ResponseHandler;

/*
* <p>Title: Filogix Express - AIG UG</p>
*
* <p>Description: Filogix AIGUG MI Project</p>
*
* <p>Copyright: Copyright (c) 2006</p>
*
* <p>Company: Filogix</p>
*
* @author Clement Cheng
* @version 1.0
*/

public class ResponseQueueHandler extends ResponseCallBackHandler implements ServiceConst{

    protected MessageDescriptor _requestDesc;
    protected String _requestBody;
    protected Object _request;
    protected ServiceResponse _response;
    protected SessionResourceKit _srk;
    protected Response _responseEntity;
    protected Request _requestEntity;
    protected int _requestId;
    protected Log _log = LogFactory.getLog(ResponseQueueHandler.class.getName());


	public  void init(MessageDescriptor requestDesc, String requestBody, Log log, SessionResourceKit srk ) throws RemoteException, ServiceException
	{
        _srk = srk;
        setVPD(requestDesc, _srk);
        _requestBody = requestBody;
        _requestDesc = requestDesc;
        _response = new ServiceResponse();
        _response.setMessageDescriptor(requestDesc);
        _requestEntity = new Request(_srk);
        try
        {
			//Dave was here - begin
			String ctk;// = _requestDesc.getTransactionID();
			if (_requestDesc.getMessageType().equals("Genworth") || _requestDesc.getMessageType().equals("CMHC"))
				ctk = _requestDesc.getClientKey().substring(9);//substring to strip out the urn:uuid: at the start.
			else
				ctk = _requestDesc.getTransactionID();
			//Dave was here - end.
		
            //_requestEntity = _requestEntity.findByChannelTranKeyAndServProduct
            //    (_requestDesc.getTransactionID(), _requestDesc.getMessageType());
			_requestEntity = _requestEntity.findByChannelTranKeyAndServProduct
                (ctk, _requestDesc.getMessageType());
            _requestId = _requestEntity.getRequestId();
        }
        catch(NumberFormatException nfe)
        {
            ServiceException se = new ServiceException();
            se.setCode(RESULT_CODE_PROCESSING_ERROR);
            se.setMessage(this.getClass().getName() + ": " + nfe.getMessage() + " invalid TransactionID: " + _requestEntity.toString());
            throw se;

        }
        catch(FinderException fe)
        {
            ServiceException se = new ServiceException();
            se.setCode(RESULT_CODE_PROCESSING_ERROR);
            se.setMessage(this.getClass().getName() + ": " + fe.getMessage() + " invalid TransactionID: " + _requestEntity.toString());
            throw se;
        }
        catch(Exception e)
        {
        	ServiceException se = new ServiceException();
            se.setCode(RESULT_CODE_PROCESSING_ERROR);
            se.setMessage(this.getClass().getName() + ": " + e.getMessage() + " invalid TransactionID: " + _requestEntity.toString());
            throw se;
        }
	}
	public ServiceResponse handleResponse(MessageDescriptor requestDesc, String requestBody) throws ServiceException
	{
        _log.info("---->>"+this.getClass().getName()+"handleResponse()");
		validateResponse(); 
        _log.info("-------->> validateResponse() done");
		insertRequest();
        _log.info("-------->> insertRequest() done");
		ServiceResponse Ak = AKResponse();
        _log.info("-------->> insertRequest() done");
		return Ak;
	}


    public boolean validateResponse() throws ServiceException
    {
    	boolean result = false;
    	ResponseValidater validater = null;
    	try
    	{
    		validater = ResponseValidaterFactory.getValidater(_requestDesc.getMessageType());
    		result = validater.validate(_requestBody);
    	}
    	catch (ValidationException ue)
    	{
    	    if( validater == null ) 
    	        _log.info("cannot find response validator for message Type = " 
    	                  + _requestDesc.getMessageType());
    	    else 
    	        _log.info("validator " + validater.getClass().getName() 
                          +  "falied validation");
    		ServiceException se = new ServiceException();
            se.setMessage(this.getClass().getName() + ": " + ue.getMessage() + " Response Validation Failed " + " on request: " + _requestId);
            se.setCode(RESULT_CODE_PROCESSING_ERROR);
            throw se;
    	}
    	catch (JAXBException e)
    	{
            if( validater == null ) 
                _log.info("cannot find response validator for message Type = " 
                          + _requestDesc.getMessageType());
            else 
                _log.info("JAXBException with validator " + validater.getClass().getName() 
                          +  "falied validation " + e.getMessage());
    		ServiceException se = new ServiceException();
            se.setMessage(this.getClass().getName() + ": " + e.getMessage() + " Response Validation Failed " + " on request: " + _requestId);
            se.setCode(RESULT_CODE_PROCESSING_ERROR);
            throw se;
    	}
    	catch (Exception e)
    	{
            if( validater == null ) 
                _log.info("cannot find response validator for message Type = " 
                          + _requestDesc.getMessageType());
            else 
                _log.info("Exception with validator" + validater.getClass().getName() 
                          +  " " + e.getMessage());
    		ServiceException se = new ServiceException();
            se.setMessage(this.getClass().getName() + ": " + e.getMessage() + " Response Validation Failed " + " on request: " + _requestId);
            se.setCode(RESULT_CODE_PROCESSING_ERROR);
            throw se;
    	}
    	return result;
    }

	public void insertRequest() throws ServiceException
	{
		String productType = _requestDesc.getProductType();
		int serviceProductId = Integer.parseInt(productType);
		int processStatusId = 0;
		int retryCounter = 0;
//		CalcMonitor dcm = CalcMonitor.getMonitor(_srk);
		DealPK dpk = new DealPK(_requestEntity.getDealId(),_requestEntity.getCopyId());
		try
		{
			_srk.beginTransaction();
			ServiceResponseQueue responseQueue = new ServiceResponseQueue(_srk,null);
			try
			{
				responseQueue.create(dpk,serviceProductId,processStatusId,retryCounter,_requestBody, new Date());
				responseQueue.setRequestType(_requestDesc.getRequestType());
				//Dave was here - begin
				String ctk;// = _requestDesc.getTransactionID();
				if (_requestDesc.getMessageType().equals("Genworth") || _requestDesc.getMessageType().equals("CMHC"))
					ctk = _requestDesc.getClientKey().substring(9);//substring to strip out the urn:uuid: at the start.
				else
					ctk = _requestDesc.getTransactionID();
				//Dave was here - end.
				
				//responseQueue.setChannelTransactionKey(_requestDesc.getTransactionID());
				responseQueue.setChannelTransactionKey(ctk);
			}
			catch (CreateException ce)
			{
				ServiceException se = new ServiceException();
				se.setMessage(this.getClass().getName() + ": " + ce.getMessage() + " Could not create ServiceResponseQueue " + " on request: " + _requestId);
				se.setCode(RESULT_CODE_PROCESSING_ERROR);
				throw se;
			}
			responseQueue.ejbStore();
			_srk.commitTransaction();
		}
		catch (Exception e)
		{
			ServiceException se = new ServiceException();
			se.setMessage(this.getClass().getName() + ": " + e.getMessage() + " Failed to update ServiceResponseQueue " + " on request: " + _requestId);
			e.printStackTrace();
			se.setCode(RESULT_CODE_PROCESSING_ERROR);
			throw se;
		}
	}
	public ServiceResponse AKResponse()
	{
		_response.setMessageDescriptor(_requestDesc);
	    _response.setTransactionStatus(RESULT_CODE_SUCCEED);
	    _response.setStatusMessage(RESULT_DESC_SUCCEED);
	    _response.setResponseBody("Null"); 
	    return _response;
	}

}