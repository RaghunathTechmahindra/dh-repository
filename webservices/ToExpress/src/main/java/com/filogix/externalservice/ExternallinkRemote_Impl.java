/**
 * <p>Title: ExternallinkRemote_Impl.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version ? Mar 14, 2006)
 *
 */

package com.filogix.externalservice;

import javax.xml.rpc.server.ServiceLifecycle;

import MosSystem.Sc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

public abstract class ExternallinkRemote_Impl implements ExternallinkRemote, ServiceLifecycle, ServiceConst
{
    static private PropertiesCache propertiesCache;
    static protected Log _log;
        
    public void init(Object context) throws javax.xml.rpc.ServiceException
    {
        System.out.println("************* init in ExternallinkRemote_Impl");
        try {

            _log = LogFactory.getLog(this.getClass());

            try {

                _log.info("--> Initialize Resource Manager");
                ResourceManager.init();
            }
            catch (Exception exc) {
                System.err.println(exc);
                _log.error(exc);
            }

            SessionResourceKit srk = new SessionResourceKit("Sytem");
            _log.info("--> Initialize PickList Data");

            try {

                _log.info("--> Initialize Picklist Data");
                PicklistData.init();
            }
            catch (Exception exc) {
                System.err.println(exc);
                _log.error(exc);
            }
            try {

                _log.info("--> Initialize BXResources");
                new BXResources();
            }
            catch (Exception exc) {
                System.err.println(exc);
                _log.error(exc);
            }

            propertiesCache = PropertiesCache.getInstance();
            
            System.out.println("P1 :" + System.getProperty(SYS_PROPERTIES_LOCATION) + SYS_PROPERTIES_NAME);
              
//            String strWSHome = propertiesCache.getInstanceProperty(WS_RESOURCE_INIT);
//            String strMOSHome = propertiesCache.getInstanceProperty(MOS_RESOURCE_INIT);
//            String strPoolName = propertiesCache.getInstanceProperty(RESOURCE_CONNECTIONPOOL_NAME);
//          String strRetryCounter = propertiesCache.getProperty(RETRY_COUNTRER, "5");
          
//
//          System.out.println("WSHome :" + strWSHome);
//          System.out.println("MOSHome :" + strMOSHome );
//          System.out.println("poolName :" + strPoolName);

          _log.debug("************* init done successfully");

          //=======================================================================
        }
        catch(Exception ex)
        {
          _log.error(ex.getMessage());
          new Exception(this.getClass().getName() + "Init error: " + ex.getMessage());
        }
    }

    public void destroy()
    {
        propertiesCache = null;
        ResourceManager.shutdown();
    }

}
