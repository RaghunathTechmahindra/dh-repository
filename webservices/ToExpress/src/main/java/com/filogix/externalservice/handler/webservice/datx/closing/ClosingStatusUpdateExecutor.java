/**
 * <p>Title: ClosingStatusUpdateExecutor</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Hiroyuki Kobayashi
 * @version 1.0 (Initial Version ? Jun 22, 2006)
 *
 */

package com.filogix.externalservice.handler.webservice.datx.closing;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.Response;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.workflow.WFETrigger;
import com.filogix.externallinks.services.datx.DatxResponseCodeClosingConverter;
import com.filogix.externallinks.services.datx.DatxResponseCodeConverterBase;
import com.filogix.externalservice.ServiceConst;
import com.filogix.externalservice.ServiceException;
import com.filogix.externalservice.ServiceExecutor;
import com.filogix.schema.datx.DatXUpdateStatus;

public class ClosingStatusUpdateExecutor
    extends ServiceExecutor
    implements ServiceConst
{
  private int _statusId;
  private String _statusDesc;
  private Date _statusDate;
  private DatXUpdateStatus _updateStatus = null;

  protected void initMe() throws ServiceException
  {
    _response.getMessageDescriptor().setMessageType(MESSAGE_TYPE_STATUS_UPDATE);
    if (_request instanceof DatXUpdateStatus)
    {
      _updateStatus = (DatXUpdateStatus) _request;
    }
  }

  protected void process() throws ServiceException
  {
    _statusDate = new Date();
    DatxResponseCodeConverterBase codeConverter = new DatxResponseCodeClosingConverter();
    _statusId = codeConverter.converetToFxpStatusCode(_updateStatus.getResultCode());
    _statusDesc = _updateStatus.getResultDescription();
    
    if (_statusId == RESPONSE_STATUS_SYSTEM_ERROR)
    {
        _statusDesc = BXResources.getSysMsg(EXTERNAL_LINK_SYSTEM_ERROR, _langId);
    }
    else
    {
        _statusDesc = _updateStatus.getResultDescription();
    if (_statusDesc == null || _statusDesc.trim().equals(""))
    {
		    try
		    {
		        if (getMessageTable() != null)
		        {
		            _statusDesc = BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(),
		                                                             getMessageTable(), _statusId, _langId );
		        }
		    }
		    catch(Exception e)
		    {
                _log.error(this.getClass().getName()+ ": No Status Message found");
            }
        }
    }
    
    try
    {
      _srk.beginTransaction();
      updateRequest();
      insertResponse();
      _srk.commitTransaction();
      
      /***** Deal Status Update change start *****/
      Deal deal = new Deal(_srk, null);
      deal = deal.findByRecommendedScenario(new DealPK(
          _responseEntity.getDealId(), _responseEntity.getCopyId()), true);
      WFETrigger.workflowTrigger(2, _srk, deal.getUnderwriterUserId(), deal
                            .getDealId(), deal.getCopyId(), -1,
                            null);
      /***** Deal Status Update change end *****/
      
      _response.setMessageDescriptor(_requestDesc); 
      _response.setTransactionStatus(RESULT_CODE_SUCCEED);
      _response.setStatusMessage(RESULT_DESC_SUCCEED);
    }
    catch (Exception e)
    {
      _log.error(StringUtil.stack2string(e));
      _statusId = RESULT_CODE_PROCESSING_ERROR;
	    _statusDesc = e.getClass().getName();
      
      _srk.cleanTransaction();
      
      try
      {
        _srk.beginTransaction();
        updateRequest();
        _srk.commitTransaction();
      }
      catch (Exception e1)
      {
        _log.error(StringUtil.stack2string(e1));
        _srk.cleanTransaction();
      }
      ServiceException se = new ServiceException(_statusId, _statusDesc);
      throw se;
    }
  }

  public void updateRequest() throws RemoteException, ServiceException
  {
      Iterator iter = _requestEntities.iterator();
      while (iter.hasNext())
      {
          Request requestEntity = (Request)iter.next();
          requestEntity.setRequestStatusId(_statusId);
          requestEntity.setStatusDate(_statusDate);
          requestEntity.setStatusMessage(_statusDesc);
          requestEntity.ejbStore();
      }
  }
  
  public void insertResponse() throws RemoteException, CreateException, FinderException
  {
      _responseEntity = new com.basis100.deal.entity.Response(_srk);
      // #4147 copy acknowledgment transaction key for all response comming to ws - start 
      String transactionIdStr = null;
      try
      {
          Collection responses = _responseEntity.findByRequestId(_requestId);
          Iterator iter = responses.iterator();
          while (iter.hasNext())
          {
              Response response = (Response)iter.next();
              if (response.getChannelTransactionKey()!=null)
              {
                  transactionIdStr =  response.getChannelTransactionKey();
                  break;
              }
          }
      }
      catch(Exception e)
      {
          _log.error("Acknowledgement response doesn't exist for requestId = " + _requestId);
          FinderException fe = new FinderException("Acknowledgement response doesn't exist for Appraisal Request requestId = " + _requestId);
          throw fe;
      }
      // #4147 copy acknowledgment transaction key for all response comming to ws - end 

      	_responseEntity = _responseEntity.create(
            _responseEntity.createPrimaryKey(),
        _statusDate,
        _requestEntity.getRequestId(), 
        _requestEntity.getDealId(),
        _statusDate, 
        _statusId);

      	_responseEntity.setStatusMessage(_statusDesc);
      	_responseEntity.setChannelTransactionKey(_requestDesc.getTransactionID());
      	_responseEntity.ejbStore();
      	_responseId = _responseEntity.getResponseId();
  }
  
  public String getMessageName()
  {
      String messasgeName = DatXUpdateStatus.class.getName();
      return messasgeName.substring(messasgeName.lastIndexOf(".")+1, messasgeName.length());
  }

  public String getNamespace()
  {
      return DEFAULT_NAMESPACE_DATX;
  }

  public JAXBContext getReqJaxbContext() throws JAXBException
  {
    JAXBContext context = JAXBContext.newInstance(DatXUpdateStatus.class.getPackage().getName());
    return context;
  }
  
  public JAXBContext getResJaxbContext() throws JAXBException
  {
//    JAXBContext context = JAXBContext.newInstance(MESSAGE_SCHEMA_PACKAGE);
//    return context;
    return getReqJaxbContext();
  }

  
  protected String getMessageTableName()
  {
    return "CLOSING_ERROR";
  }

}
