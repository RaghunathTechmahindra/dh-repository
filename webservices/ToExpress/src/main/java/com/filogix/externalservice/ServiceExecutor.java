/**
 * <p>Title: ServiceExecuter.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version ? Apr 25, 2006)
 *
 */

package com.filogix.externalservice;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Iterator;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;

import com.basis100.deal.docprep.DocPrepException;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.Response;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.state.ExpressState;
import com.filogix.externalservice.handler.ResponseCallBackHandler;

public abstract class ServiceExecutor extends ResponseCallBackHandler implements ServiceConst  
{
    protected MessageDescriptor _requestDesc;
    protected String _requestBody;
    protected Object _request;
    protected ServiceResponse _response;
    protected Log _log;
    protected SessionResourceKit _srk;
    protected int _transactionStatusId =  ServiceConst.RESPONSE_STATUS_BLANK;
    protected Request _requestEntity;
    protected int _requestId;
    protected Collection _requestEntities; // all request with requestId
    protected Response _responseEntity;
    protected int _responseId;
    protected Deal _deal;
    protected int _copyId;
    protected UserProfile _userProfile;
    protected int _langId;
    protected static int CLIENTID_IS_INVALID = 1;
    
    public ServiceResponse handleResponse(MessageDescriptor requestDesc, String requestBody)
    	throws ServiceException
    {
    	return execute(requestDesc, requestBody);
    }
    
    public ServiceResponse execute(MessageDescriptor requestDesc, String requestBody) 
        throws ServiceException
    {
//      _log.debug(this.getClass().getName() + " in execute(" + requestDesc + "," + requestBody +")");
      _log.debug(this.getClass().getName() + " in execute(" + requestDesc + " )");
        try
        {
//            authentification(_srk, requestDesc);
            _log.debug("authentification sucseed");
            
//            if (lockDeal())
//            {
//                addMessageQue(_requestDesc, _requestBody, _log);
//                return _response;
//            }
//            else
//            {
                process();
                _log.trace(_response.toString());
                return _response;
//            }
        }
        catch(Exception e)
        {
            _log.error(this.getClass().getName() + ": " + e.getMessage() + " system error" + _request.toString());
            ServiceException se = new ServiceException();
            se.setCode(RESULT_CODE_PROCESSING_ERROR);
            se.setMessage(this.getClass().getName() + ": " + e.getMessage() + " system error" + _request.toString());
            throw se;
        }
    }
    
    public void init(MessageDescriptor requestDesc, String requestBody, Log log, SessionResourceKit srk ) 
        throws JAXBException, RemoteException, ServiceException, AcceptedServiceException
    {
        _log = LogFactory.getLog(this.getClass());

        _requestDesc = requestDesc;
        _srk = srk;
        setVPD(requestDesc, _srk);
        _log.info(this.getClass().getName() + ": init() ");
        _requestBody = addNamespace(requestBody); 
        _request = getInRequest(_requestBody);
        _log.info("Message Descriptor:" + _requestDesc.toString());
        _response = new ServiceResponse();
        _response.setMessageDescriptor(requestDesc);
        
        // validate requestId and set collection of request
        _requestId = validateRequestId(_requestDesc.getClientKey());
        _log.info("SRK Before call setGoldenEntities " + _srk.getExpressState().getVPDStatus());
        // set goldCopy of request, deal
        setGoldenEntities(_requestId); 
        _log.info("*** done setGoldenEntities " + _srk.getExpressState().getVPDStatus());

        setUserProfile();
        _log.info("*** done setUserProfile " + _srk.getExpressState().getVPDStatus());
        _langId = Mc.LANGUAGE_PREFERENCE_ENGLISH;
        
        initMe();
    }
    private Object getInRequest(String requestXML) throws JAXBException, RemoteException
    {
        _log.debug("ServiceExecutor: getInRequest()");
        try
        {
            StreamSource st = new StreamSource( new StringReader(_requestBody));
            JAXBContext jaxbContext = getReqJaxbContext();
            _log.debug("test3");
            // work request as String
            if (jaxbContext == null) return null;
    
            // Validate the object.
//            ValidationEventCollector collector = new ValidationEventCollector();
            _log.debug("create marshall");
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
//            unmarshaller.setEventHandler( collector );
//            unmarshaller.setValidating(true);
            // it should be ExpressResponse
            _log.debug("Start marshall");
            Object request = unmarshaller.unmarshal(st);
            _log.debug("Finish marshall");
            // check for events
//            if( collector.hasEvents() )
//            {
//               // iterate over events
//    
//                for (int i = 0; i < collector.getEvents().length; i++)
//                {
//                    // Validation failed.
//                    ValidationEvent event = collector.getEvents()[i];
//                    System.out.println("\nEvent #" + i);
//                    System.out.println("Message: " + event.getMessage() +
//                        " Savirty: " + event.getSeverity());
//                    ValidationEventLocator locator = event.getLocator();
//                    System.out.println("Column: " + locator.getColumnNumber());
//                    System.out.println("Line: " + locator.getLineNumber());
//                    System.out.println("Object: " + locator.getObject());
//                    System.out.println("Node: " + locator.getNode());
//                    System.out.println("URL: " + locator.getURL());
//                    System.out.println("Offset: " + locator.getOffset());
//                }
//                String msg =
//                    "Validation failed for object type '" + requestXML;
//                throw new RemoteException(msg);
//            }
            return request;
        }
        catch(JAXBException e)
        {
            _log.error("ServiceExecutor: JaxbException while getting requst ");
            _log.error(e);
//            _log.error(requestXML);
            throw e;
        }
        
    }

    public Object getPayload(String fixedRequestBody) throws JAXBException
    {
        String payloadTagName = getPayloadName();
        if (fixedRequestBody.indexOf(payloadTagName) < 0 ) return null;
        int startPayload = fixedRequestBody.indexOf(payloadTagName) - 1;
        int endPayload = fixedRequestBody.indexOf(payloadTagName, startPayload + 2) + payloadTagName.length() + 1;
        
        String payloadString = fixedRequestBody.substring(startPayload, endPayload);
        StreamSource st = new StreamSource( new StringReader(payloadString));
        try
        {
            JAXBContext jaxbContext = getPayloadJaxbContext();
            // work request as String
            if (jaxbContext == null) return null;
        
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            Object payload = unmarshaller.unmarshal(st);
            return payload;
        }
        catch(JAXBException e)
        {
            _log.info("ERROR @ getPayload " + e.getMessage());
            throw e;
        }
    }

//    private String getOutResponsXML(JAXBObject response) throws JAXBException
//    {
//        JAXBContext jaxbContext = getResJaxbContext();
//        if (jaxbContext == null ) return null;
//        Marshaller marshaller = jaxbContext.createMarshaller();
//        StringWriter xmlWriter = new StringWriter();
//        marshaller.marshal(response, xmlWriter);
//        return xmlWriter.toString();       
//    }
    
    private void authentification(SessionResourceKit srk, MessageDescriptor requestDesc) throws ServiceException
    {
        _log.debug("in Authentification");
        String userId = requestDesc.getSourceID();
        String password = requestDesc.getPassword();
        WSSignInService siService = new WSSignInService(srk, _log);
        siService.login(userId, password);
    }
    
      
    public int validateRequestId(String requestIdStr) throws RemoteException, AcceptedServiceException, ServiceException
    {
      try
      {
            int requestId = Integer.parseInt(requestIdStr);
            _requestEntity = new Request(_srk);
            _requestEntities = _requestEntity.findRequestsByRequestId(requestId);
            if (_requestEntities.size() == 0)
        {
                throw new FinderException();
            }
            return requestId;
        }
        catch(NumberFormatException nfe)
        {
            ServiceException se = new ServiceException();
            se.setCode(RESULT_CODE_PROCESSING_ERROR);
            se.setMessage(this.getClass().getName() + ": " + nfe.getMessage() + " ClientId is wrong: " + _request.toString());
            _log.trace(se.getMessage());
            throw se;
        }
        catch(FinderException fe)
        {
            _log.error("Request Doesn't exist");
            
//********************  DISABLED NOW FOR ML ********  OCT 2, 2007 *********
//         
//            //request doesn't exist 
//            // send alert emal to Express support via Docprep
//            String poolname = PropertiesCache.getInstance().getProperty("com.basis100.resource.connectionpoolname");
//        
//            try
//            {
//              String subject = "Express Web Services Error, %poolname environment".replaceFirst("%poolname", poolname);
//              String alertHeader = "**** PLEASE DO NOT REPLY FOR THIS MAIL **** \n\n";
//              String bodyHeader = "Express Web service: error in receiving response from %wsClient for request id = %requestId"
//                  .replaceFirst("%wsClient", _requestDesc.getSourceID())
//                  .replaceFirst("%requestId", requestIdStr);
//              String bodyMain = ("Request doesn't exist in %poolname DB ").replaceFirst("%poolname", poolname);
//              String attachemntText = "\n\n****************************\n" + _requestDesc.toString();
//                sendEmailAlert( _srk, subject, alertHeader + bodyHeader +"\n\n"+ bodyMain +"\n\n" + attachemntText, true);
//                _log.error("Email Alert was sent to :Express Support \n ");
//                _log.error("******" + subject );
//                _log.error(bodyHeader);
//                _log.error(bodyMain);
//          } 

          AcceptedServiceException se = new AcceptedServiceException();
          se.setCode(RESULT_CODE_SUCCEED);
          se.setMessage(this.getClass().getName() + ": ClientId doesn't exist: " + requestIdStr);

          throw se;
        }
  	}
    
    private void setGoldenEntities(int requestId) throws  RemoteException, ServiceException
    {
        int dealId = -1;
        try
        {
            
            Iterator iter = _requestEntities.iterator();
            while (iter.hasNext())
            {
                Request requestEntity = (Request)iter.next();
                dealId = requestEntity.getDealId();
                int copyId = requestEntity.getCopyId();
                _log.info("DealId = " + dealId);
                _log.info("CopyId = " + copyId);
                _srk.getExpressState().setDealInstitutionId(_srk.getExpressState().getDealInstitutionId());
            	_deal = new Deal(_srk, null);
                _deal =  _deal.findByPrimaryKey( new DealPK(dealId, copyId));
              if ("S".equals(_deal.getCopyType()) || "G".equals(_deal.getCopyType()))
//              if ("Y".equalsIgnoreCase(_deal.getScenarioRecommended()))
                {
            		_copyId = _deal.getCopyId();
            		_requestEntity = _requestEntity.findByPrimaryKey(new RequestPK(_requestId, _copyId));
                    break;
                }
            }
        }
        catch(FinderException fe)
        {
            ServiceException se = new ServiceException();
            se.setCode(RESULT_CODE_PROCESSING_ERROR);
            se.setMessage(this.getClass().getName() + ": " + fe.getMessage() + " DealId is wrong: " + dealId);
            _log.trace(se.getMessage());
            throw se;
        }
        }
        
    
    private void setUserProfile() throws RemoteException, ServiceException
    {
        try
        {   
            _log.info("@setUserProfile");
            _userProfile = new UserProfile(_srk);
            
            UserProfileBeanPK upPK = new UserProfileBeanPK(_requestEntity.getUserProfileId(), 
                                                           _deal.getInstitutionProfileId());
            _userProfile = _userProfile.findByPrimaryKey(upPK);
            _log.info("found userProfile@setUserProfile");
        }
        catch(FinderException fe)
        {
            _log.info("FinderException @setUserProfile");
            _userProfile = null;
        }        
    }
    
    protected String getStatusMessage(int id )
    {
        int langId = 0; // english
        if (_requestDesc.getLanguageID() >= 0)
        {
            langId = _requestDesc.getLanguageID();
        }
        try
        {
            if (getMessageTable() != null)
                return BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), getMessageTable(), id, langId );
            else
                return null;
        }
        catch(Exception e)
        {
            return  null;
        }
    }

    private Borrower getPrimaryBorrower(Deal deal)
    {
        try
        {
            Collection borrowers = deal.getBorrowers();
            Iterator iter = borrowers.iterator();
            while (iter.hasNext())
            {
                Borrower b = (Borrower)iter.next();
                if (b.isPrimaryBorrower())
                    return b;
            }
            return null;
        }
        catch(Exception e)
        {
            return null;
        }
    }

    public String getNamespace(String packageName)
    {
        String namespace = "http://schema.filogix.com/datx";
        String lastString = packageName.substring(packageName.lastIndexOf("."), packageName.length());
        if (!"datx".equals(lastString))
        {
            namespace = namespace + "/" + lastString;
        }
        return namespace;
    }
    
    public String getPayloadNamespace(String packageName)
    {
        String namespace = "";
        
        
        return namespace;
    }
    
    // for now this is only for datx
    public String  addNamespace(String response)
    {
        String newString = new String(response);
        String bodyNamespace = getNamespace();
        String messageName = getMessageName();
        if(response.indexOf(DEFAULT_NAMESPACE_DATX) < 0 )
        {
            String addingString =  " xmlns=\"" + bodyNamespace + "\"";
            newString = newString.replaceFirst(messageName, messageName + addingString);
        }
        return newString;
    }
        
    // if subclass need payload, then override this method
    public String getPayloadName()
    {
        return null;
    }

    // if subclass need payload, then override this method
    public String getPayloadNamespace()
    {
        return null;
    }

    // if subclass need payload, then override this method
    public JAXBContext getPayloadJaxbContext() throws JAXBException
    {
        return null;
    }
    
    protected String getMessageTable()
    {
        return "REQUESTSTATUS";
    }

    private void sendEmailAlert(SessionResourceKit srk, String pSubject, String pText, boolean ifSupport) 
        throws DocPrepException, JdbcTransactionException
    {        
        String emailTo = "";
        if (ifSupport) {
            emailTo = PropertiesCache.getInstance()
                .getInstanceProperty("com.filogix.docprep.alert.email.recipient");
        } else {
            emailTo = PropertiesCache.getInstance()
                .getProperty(srk.getExpressState().getDealInstitutionId(), 
                             "com.filogix.docprep.alert.email.recipient");            
        }
    
        _log.trace("Placing a request for an alert email into documentqueue...");
        DocumentRequest.requestAlertEmail(srk, 0, 0, emailTo, pSubject, pText);
        _log.trace("request for documentqueue is done");

    }
    
    // to observ compiler error by Clement modification on Feb 6, 2007
    public int handleResponse(Object obj)
    {
      return -1;
    }
    
    protected abstract void initMe() throws ServiceException, JAXBException;
    protected abstract void process() throws ServiceException;
    protected abstract String getMessageName();
    protected abstract String getNamespace();
    protected abstract JAXBContext getReqJaxbContext() throws JAXBException;
    protected abstract JAXBContext getResJaxbContext() throws JAXBException;
    
}
