/**
 * <p>Title: AppraisalStatusUpdateExecutor.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version ? Jun 1, 2006)
 *
 */

package com.filogix.externalservice.handler.webservice.datx.appraisal;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.Response;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.picklist.BXResources;
import com.filogix.externallinks.services.datx.DatxResponseCodeAppraisalConverter;
import com.filogix.externalservice.ServiceConst;
import com.filogix.externalservice.ServiceException;
import com.filogix.externalservice.ServiceExecutor;
import com.filogix.externalservice.handler.webservice.workflow.WorkflowHandler;
import com.filogix.schema.datx.DatXUpdateStatus;

public class AppraisalStatusUpdateExecutor extends ServiceExecutor
        implements ServiceConst
{
    protected int                 _statusId;
    protected String              _statusDesc;
    protected Date                _statusDate;

    private static final String MESSAGE_TYPE = MESSAGE_TYPE_STATUS_UPDATE;
    public DatXUpdateStatus _updateStatus = null;

    protected void initMe() throws ServiceException, JAXBException
    {
        _response.getMessageDescriptor().setMessageType(MESSAGE_TYPE);
        if (_request instanceof DatXUpdateStatus)
        {
            _updateStatus = (DatXUpdateStatus)_request;
        }
    }

    public void process() throws ServiceException
    {
        _log.info("***** ServicceExecugor " + this.getClass().getName() + "  process Start *****");
        _statusDate = new Date();
        DatxResponseCodeAppraisalConverter codeConverter = new DatxResponseCodeAppraisalConverter();
        _statusId = codeConverter.converetToFxpStatusCode(_updateStatus.getResultCode());
        if (_statusId == RESPONSE_STATUS_SYSTEM_ERROR)
        {
            _statusDesc = BXResources.getSysMsg(EXTERNAL_LINK_SYSTEM_ERROR, _langId);
        }
        else
        {
        _statusDesc = _updateStatus.getResultDescription();
        if (_statusDesc == null || _statusDesc.trim().equals(""))
        {
        try
        {
            if (getMessageTable() != null)
            {
                _statusDesc = BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(),
                                                                 getMessageTable(), _statusId, _langId );
            }
        }
        catch(Exception e)
        {
                    _log.error(this.getClass().getName()+ ": No Status Message found");
                }
            }
        }

        try
        {
            try
            {
                _srk.beginTransaction();
                updateRequest();
                _log.debug("-------->> update Request done");
                insertResponse();
                _log.debug("-------->> insert Response done");
                // changed to call workflowTrigger always for 4.2GR DESU
//                if (_statusId == RESPONSE_STATUS_CANCELLATION_ACCEPTED 
//                        || _statusId == RESPONSE_STATUS_QUOTATION_RECEIVED )
//                {
                    setWorkflowTrigger();
                    _log.debug("-------->> set trigger for Workflow done");
//                }
                _srk.commitTransaction();
                _response.setMessageDescriptor(_requestDesc);
                _response.setTransactionStatus(RESULT_CODE_SUCCEED);
                _response.setStatusMessage(RESULT_DESC_SUCCEED);
                _response.setResponseBody("");
            }
            catch(Exception e)
            {
                _log.error(StringUtil.stack2string(e));
                _statusId = RESULT_CODE_PROCESSING_ERROR;
                _statusDesc = e.getClass().getName();
                
                _srk.cleanTransaction();
                _srk.beginTransaction();
                try 
                {
                    updateRequest();
                  _srk.commitTransaction();
    
                } 
                catch (Exception e1)
                {
                  _log.error(StringUtil.stack2string(e1));
                  _srk.cleanTransaction();
                }
                finally
                {
                    ServiceException se = new ServiceException(_statusId, _statusDesc);
                    throw se;
                }
            }
        }
        catch (JdbcTransactionException e1)
        {
            _log.error(StringUtil.stack2string(e1));
            ServiceException se = new ServiceException
                (RESULT_CODE_PROCESSING_ERROR,  "Error when processing DatX reponse for requestId = " + _requestDesc.getClientKey() + " :" + e1.getMessage());
            throw se;
        }
      }

    public void updateRequest() throws RemoteException, ServiceException
    {
        Iterator iter = _requestEntities.iterator();
        while (iter.hasNext())
        {
            Request requestEntity = (Request)iter.next();
            requestEntity.setRequestStatusId(_statusId);
            requestEntity.setStatusDate(_statusDate);
            requestEntity.setStatusMessage(_statusDesc);
            requestEntity.ejbStore();
    	}
    }
    
    public void insertResponse() throws RemoteException, CreateException, FinderException
    {
        _responseEntity = new com.basis100.deal.entity.Response(_srk);
        // #4147 copy acknowledgment transaction key for all response comming to ws - start 
        String transactionIdStr = null;
        try
        {
            Collection responses = _responseEntity.findByRequestId(_requestId);
            Iterator iter = responses.iterator();
            while (iter.hasNext())
            {
                Response response = (Response)iter.next();
                if (response.getChannelTransactionKey()!=null)
                {
                    transactionIdStr =  response.getChannelTransactionKey();
                    break;
                }
            }
        }
        catch(Exception e)
    {
            _log.error("Acknowledgement response doesn't exist for requestId = " + _requestId);
            FinderException fe = new FinderException("Acknowledgement response doesn't exist for Appraisal Request requestId = " + _requestId);
            throw fe;
        }
        // #4147 copy acknowledgment transaction key for all response comming to ws - end
        
        _responseEntity = _responseEntity.create(_responseEntity.createPrimaryKey(),
                              new Date(),
                              _requestEntity.getRequestId(), 
                              _requestEntity.getDealId(),
                              new Date(),
                              _statusId);
        _responseEntity.setStatusMessage(_statusDesc);
        _responseEntity.setChannelTransactionKey(transactionIdStr);
        _responseEntity.ejbStore();
        _responseId = _responseEntity.getResponseId();
    }
    
    protected int getWFCopyId() throws Exception
    {
        Deal deal = getRequestedDeal();
        if (deal != null ) return deal.getCopyId();
        else return -1;
    }
    
    protected Deal getRequestedDeal() throws Exception
    {
        int dealId = -1;
        int copyid = -1;
        Deal deal = null;
        try 
        {
            deal = new Deal(_srk, null); 
            // check if the deal is locked or not by any user.

            // get dealid and copyid with S or G related this request,
//            deal = deal.findByGoldCopy(_responseEntity.getDealId());
//          deal = deal.findByRecommendedScenario( new DealPK(deal.getDealId(), deal.getCopyId()), true);
            deal.setSilentMode(true);
          deal = deal.findByRecommendedScenario( new DealPK(_responseEntity.getDealId(), _responseEntity.getCopyId()), true);
          if (deal!=null) deal.setSilentMode(false);
            
// regardless the deal is locked or not, return the deal
            return deal; 

//            int dealLockedUserId = -1;
//            try
//            {
//                DLM dml = DLM.getInstance();
//                dealLockedUserId = dml.getUserIdOfLockedDeal(dealId, _srk);
//            }
//            catch(Exception e)
//            {
//                String msg = "@DLM.isLocked() exception = " + e.getMessage();
//            }
//            
//            if (dealLockedUserId < 0)
//            {
//                return deal;
//            }
//            else
//            {
//                return null;
//            }
            
        }
        catch(Exception e)
        {
          _log.error("Exception @AppraisalResponseExecutor.getRequestedDeal():");
          _log.error(e);
          throw e;
        }
    }
    
    private void setWorkflowTrigger() throws RemoteException, CreateException
    {
        
        // the following code is from AppraisalHander 
        try 
        {
            _log.debug("--> AppraisalStatusUpdateExecutor.setWorkflowTrigger() -- Calling Workflow");
            int dealId = -1;
            Deal deal = getRequestedDeal();
            if (deal == null) return;
            
            WorkflowHandler wfHander = new WorkflowHandler(SERVICE_SUB_TYPE_APPRAISAL, _statusId, _srk);
            wfHander.workflowTrigger(deal.getUnderwriterUserId(), 
                                     deal.getDealId(), deal.getCopyId());
            

        }
        catch(Exception e)
        {
            _log.error("Exception @AppraisalStatusUpdateExecutor.setWorkflowTrigger():");
            _log.error(e);
          return;
        }
    }
    
    public String getMessageName()
    {
        String messasgeName = DatXUpdateStatus.class.getName();
        return messasgeName.substring(messasgeName.lastIndexOf(".")+1, messasgeName.length());
    }

    public String getNamespace()
    {
        return DEFAULT_NAMESPACE_DATX;
    }

    public JAXBContext getReqJaxbContext() throws JAXBException
    {
        JAXBContext context = JAXBContext.newInstance(DatXUpdateStatus.class.getPackage().getName());
          return context;
    }
    
    public JAXBContext getResJaxbContext() throws JAXBException
    {
        JAXBContext context = JAXBContext.newInstance(DatXUpdateStatus.class.getPackage().getName());
        return context;
    }
        
}
