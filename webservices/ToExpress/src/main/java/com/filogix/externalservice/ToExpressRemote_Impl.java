/**
 * <p>Title: ResponseServiceSoapStub.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version ? Mar 14, 2006)
 *
 */

package com.filogix.externalservice;

import javax.xml.bind.JAXBException;
import javax.xml.rpc.server.ServiceLifecycle;

import com.basis100.resources.SessionResourceKit;
import com.filogix.externalservice.handler.IResponseCallBackHandler;
import com.filogix.schema.datx.DatXUpdateStatus;
import com.filogix.schema.datx.ObjectFactory;

public class ToExpressRemote_Impl extends ExternallinkRemote_Impl
        implements ToExpressRemote, ServiceConst, ServiceLifecycle
{

    public ToExpressRemote_Impl()
    {
    }

    /*
     * <p>
     * WebServices clients invoke this method
     * grabing individual service executor
     * exetute service
     * generate response message as String
     * requestBody must xml fomarted and schema (xsd) should be provided and validate against the schema
     * @see com.filogix.express.services.ToExpressRemote#invoke(com.filogix.express.services.RequestDescriptor, java.lang.String)
     * @param RequestDesctiptor requestDescriptor
     * @String requestBody xml
     */
   public ServiceResponse invoke(MessageDescriptor messageDescriptor, String requestBody)
       throws java.rmi.RemoteException
   {
       _log.trace("************* in ToExpressRemote_Impl");

       ServiceResponse response = new ServiceResponse();
       response.setMessageDescriptor(messageDescriptor);
       _log.debug(this.getClass().getName() + ":invoke");
       _log.trace("ToExpressRemote_impl: Request Received ----");
       _log.debug("*** MessageDescriptor***");
       _log.debug(messageDescriptor.toString());
       _log.debug("*** requestBody: ***");
       if (requestBody.length() > 400) // avoid log report
           _log.debug(requestBody.substring(0, 400));
       else
           _log.debug(requestBody);

       try
       {
           validateMessageDescriptor(messageDescriptor);
       }
       catch(ServiceException se)
       {
           _log.error(this.getClass()+ ":" + se.getMessage());
           _log.error(messageDescriptor.toString());
           setErrorResponse(messageDescriptor, response,se);
           return response;
       }

       IResponseCallBackHandler handler = null;
       SessionResourceKit srk = null;
       try
       {
           try
           {
               srk = getSessionResourceKit();
        	   handler = getHandler(messageDescriptor);
        	   _log.debug("Handler : " +handler.getClass().getName());
               _log.debug("----> ServiceExecutor = " + handler.getClass().getName());
//               setVPD(messageDescriptor, srk);
               
               // Before does Authentication and setVPD
               response = handler.doProcess(messageDescriptor, requestBody,_log, srk);
               _log.debug("ready to return response!! ");
//               if (srk != null) srk.freeResources();
               srk = null;
               return response;
           }
           catch(JAXBException jaxbe)
           {
               _log.error(this.getClass()+ ":" + jaxbe.getMessage());
               setErrorResponse(messageDescriptor, response, RESULT_CODE_PROCESSING_ERROR, "Payload is invalid");
//               if (srk != null && srk.isInTransaction()) srk.freeResources();
               srk = null;
               return response;
           }
           catch(com.basis100.entity.RemoteException re)
           {
               _log.error(this.getClass()+ ":" + re.getMessage());
               setErrorResponse(messageDescriptor, response, RESULT_CODE_SYSTEM_ERROR, "DB is not available or no request exists");
//               if (srk != null && srk.isInTransaction()) srk.freeResources();
               srk = null;
               return response;
           }
           catch(AcceptedServiceException ase)
           {
               response.setMessageDescriptor(messageDescriptor);
               response.setTransactionStatus(RESULT_CODE_SUCCEED);
               response.setStatusMessage(RESULT_DESC_SUCCEED);
               response.setResponseBody("");
//               if (srk != null && srk.isInTransaction()) srk.freeResources();
               srk = null;
               return response;
           }
           catch(ServiceException se)
           {
               _log.error(this.getClass()+ ":" + se.getMessage());
               setErrorResponse(messageDescriptor, response, se);
//               if (srk != null && srk.isInTransaction()) srk.freeResources();
               srk = null;
               return response;
           }
       }
       catch(Throwable t)
       {
    	   t.printStackTrace();
           _log.error("!!!!!  Unexpected Error Occured  !!!!!");
           _log.error(this.getClass()+ ":" + t.getMessage());
           System.out.println(t);
           ServiceException se = new ServiceException();
           se.setCode(RESULT_CODE_SYSTEM_ERROR);
           se.setMessage("Unexpected Error");
           setErrorResponse(messageDescriptor, response, se);
//           if (srk != null) srk.freeResources();
           srk = null;
           return response;
       }
       finally
       {
//           if (srk != null) srk.freeResources();
       }
   }

 public SessionResourceKit getSessionResourceKit()
 {
     SessionResourceKit srk;
//     String sessionKey = PropertiesCache.getInstance().getInstanceProperty(SIGNIN_WEB_SERVICE);
//     if (sessionKey == null)
//     {
//         srk = new SessionResourceKit(SIGNIN_WEB_SERVICE);
//     }
//     else
//     {
         srk = new SessionResourceKit(SIGNIN_WEB_SERVICE);
//     }

     return srk;
 }

   private IResponseCallBackHandler getHandler(MessageDescriptor desc) throws ServiceException
   {
	   _log.debug("ToExpressRemote_Impl@getHanlder");
       try
       {
           return ServiceExecutorFactory.getHandler(desc.getSourceID(), desc.getMessageType());
       }
       catch(ServiceException se)
       {
           _log.info("getHandler()@" + this.getClass().getName() + ":" + se.getMessage() );
           throw se;
       }
   }

   private void validateMessageDescriptor(MessageDescriptor msgDesc) throws ServiceException
   {
       if (msgDesc == null)
       {
           ServiceException error = new ServiceException();
           _log.trace("MessageDescriptor is null");
           error.setCode(RESULT_CODE_PROCESSING_ERROR);
           error.setMessage("msgDesc is empty");
           throw error;
       }
       if (msgDesc.getSourceID() == null || "".equals(msgDesc.getSourceID().trim()))
       {
           ServiceException error = new ServiceException();
           _log.trace("MessageDescriptor.sourceId is null");
           error.setCode(RESULT_CODE_PROCESSING_ERROR);
           error.setMessage("sourceId is empty in MessageDescriptor");
           throw error;
       }
       if (msgDesc.getMessageType() == null || "".equals(msgDesc.getMessageType().trim()))
       {
           ServiceException error = new ServiceException();
           _log.trace("MessageDescriptor.messageType is null");
           error.setCode(RESULT_CODE_PROCESSING_ERROR);
           error.setMessage("messageType is empty in MessageDescriptor");
           throw error;
       } 
//       else if (msgDesc.getRequestType() == null || "".equals(msgDesc.getRequestType().trim())) {
//           ServiceException error = new ServiceException();
//           _log.trace("MessageDescriptor.requestType is null");
//           error.setCode(RESULT_CODE_PROCESSING_ERROR);
//           error.setMessage("requestType is empty in MessageDescriptor");
//           throw error;
//       }
       _log.trace("Validate MessagaDescriptor succeed: requestId = " + msgDesc.getClientKey());
   }

   protected void setErrorResponse(MessageDescriptor desc, 
                   ServiceResponse response, int code, String message)
   {
       response.setTransactionStatus(code);
       response.setStatusMessage(message);
       try {
           DatXUpdateStatus status = new ObjectFactory().createDatXUpdateStatus();
           status.setResultCode(code);
           status.setResultDescription(message);
       } catch (JAXBException jaxbe) {
           _log.trace("@setErrorResponse:Could not create DatXUpdateStatus class");
       }
       response.setMessageDescriptor(desc);
   }

   protected void setErrorResponse(MessageDescriptor desc, ServiceResponse response, ServiceException se)
   {
       setErrorResponse(desc, response, se.getCode(), se.getMessage());
   }

}
