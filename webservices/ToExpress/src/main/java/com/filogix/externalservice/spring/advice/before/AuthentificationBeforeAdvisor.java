package com.filogix.externalservice.spring.advice.before;

import java.lang.reflect.Method;
import org.springframework.aop.MethodBeforeAdvice;

import com.basis100.resources.SessionResourceKit;

import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.externalservice.MessageDescriptor;
import com.filogix.externalservice.ServiceException;
import com.filogix.externalservice.WSSignInService;
import org.apache.commons.logging.Log;

/**
*
* <p>Title: Filogix Express - AIG UG</p>
*
* <p>Description: Filogix AIGUG MI Project</p>
*
* <p>Copyright: Copyright (c) 2006</p>
*
* <p>Company: Filogix</p>
*
* @author Clement Cheng
* @version 1.0
*/

public class AuthentificationBeforeAdvisor implements MethodBeforeAdvice , ServiceConst{
	
    public void before(Method m, Object[] args, Object target) throws Throwable 
    {
    	MessageDescriptor _requestDesc = (MessageDescriptor) args[0];
//    	String _request = (String) args[1];
    	Log _log = (Log) args[2];
    	SessionResourceKit _srk = (SessionResourceKit) args[3];
    	
    	try
    	{
    		String userId = _requestDesc.getSourceID();
    		String password = _requestDesc.getPassword();
    		WSSignInService siService = new WSSignInService(_srk, _log);
    		siService.login(userId, password);
    	}
    	catch (ServiceException e)
    	{
    		ServiceException se = new ServiceException();
            se.setMessage(this.getClass().getName() + ": " + e.getMessage() + " system error for request: " + _requestDesc.getClientKey());
            se.setCode(RESULT_CODE_PROCESSING_ERROR);
            throw se;
    	}
    	_log.debug("-------->> authentification() done");
    }

}
