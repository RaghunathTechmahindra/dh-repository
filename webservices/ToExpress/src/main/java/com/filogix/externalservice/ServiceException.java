/**
 * <p>Title: ServiceError.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � Apr 27, 2006)
 *
 */

package com.filogix.externalservice;

import java.io.Serializable;

public class ServiceException extends Exception implements Serializable
{
    private int _code;
    private String _message;
    
    /**
     * @param args
     */
    public ServiceException()
    {
        
    }
    
    public ServiceException(int code, String message)
    {
        _code = code;
        _message = message;
    }
    public int getCode()
    {
        return _code;
    }

    public void setCode(int code)
    {
        this._code = code;
    }

    public String getMessage()
    {
        return _message;
    }

    public void setMessage(String message)
    {
        this._message = message;
    }
}
