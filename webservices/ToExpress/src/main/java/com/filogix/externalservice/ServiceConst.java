/**
 * <p>Title: ServiceConst.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � Apr 26, 2006)
 *
 */

package com.filogix.externalservice;

import com.basis100.resources.PropertiesCache;

public interface ServiceConst
        extends com.filogix.externallinks.framework.ServiceConst
{
    // this is used to create ws system SRK
    // it must be in DB in userprofile as userlogin 
    public static final String WS_MANAGER = "WS_MANAGER";
    
    /*****************************************************************
     * THESE MUST STAY HERE!!!!!!!!
     *****************************************************************/
    // ToExpress Default Request session keys
    public static final String SIGNIN_WEB_SERVICE = "WebServiceSignin";
    public static final String SYS_PROPERTIES_LOCATION = "SYS_PROPERTIES_LOCATION";
    public static final String SYS_PROPERTIES_NAME = "mossys.properties";
    public static final String MOS_RESOURCE_INIT = "com.basis100.resource.init";
    public static final String WS_PROPERTIES_LOCATION = "WS_PROPERTIES_LOCATION";
    public static final String WS_PROPERTIES_NAME = "ws.properties";
    public static final String WS_RESOURCE_INIT = "ws.resource.init";
    public static final String RESOURCE_CONNECTIONPOOL_NAME = "com.basis100.resource.connectionpoolname";
    public static final String RETRY_COUNTRER = "ws.autoretrycounter";

    
    /*****************************************************************
     * ERROR STATUS - Starts with 101 since super class has 1 - ,,,
     *****************************************************************/    

//    public static final int TO_EXPRESS_TRANSACTION_STATUS_ERROR = -1;
//    public static final int TO_EXPRESS_TRANSACTION_STATUS_SYSTEM_ERROR = 99;
//    public static final int TO_EXPRESS_TRANSACTION_STATUS_BUSY = 100;
//    
//    public static final int TO_EXPRESS_TRANSACTION_STATUS_MESSAGEDESC_IS_INVALID = 101;
//    public static final int TO_EXPRESS_TRANSACTION_STATUS_MESSAGEBODY_IS_INVALID = 102;
//    public static final int TO_EXPRESS_TRANSACTION_STATUS_AUTHENTIFICATION_IS_FAILED = 103;

    public static final String FALSE = "false";
    public static final String TRUE = "true";
    public static final String YES = "Y";
    public static final String NO = "N";
    public static final int USER_PROFILE_STATUS_ACTIVE = 0;
    
    public static String RESOURCE_ID_DATX_APP = "DATX_APP";
    public static String RESOURCE_ID_DATX_CLOSING = "DATX_CLOSING";
    
    public static String MESSAGE_TYPE_APPRAISAL_RESPONSE_REPORT = "AppraisalReportUpdate";
    public static String MESSAGE_TYPE_STATUS_UPDATE = "StatusUpdate";
    
    public static String MESSAGE_TYPE_MI_CALLBACK = "MICallBack";
    
    public static String PAYLOAD_TYPE_APPRAISAL_RESPONSE_REPORT = "CentractReport";
    
    public static String DEFAULT_NAMESPACE_DATX="http://schema.filogix.com/datx";

    public static String SPRING_CONTEXT_NAME ="ws.xml";
    public static String SPRING_CONTEXT_LOCATION = "ws.filogix.spring";
    
    
}
