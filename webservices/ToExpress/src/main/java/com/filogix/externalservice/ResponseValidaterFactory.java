package com.filogix.externalservice;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.resources.PropertiesCache;
import com.filogix.spring.SpringServiceFactory;


/**
*
* <p>Title: Filogix Express - AIG UG</p>
*
* <p>Description: Filogix AIGUG MI Project</p>
*
* <p>Copyright: Copyright (c) 2006</p>
*
* <p>Company: Filogix</p>
*
* @author Clement Cheng
* @version 1.0
*/

public class ResponseValidaterFactory implements ServiceConst{
	static private PropertiesCache propertiesCache;
	static Log log = LogFactory.getLog(ResponseValidaterFactory.class.getName());
    public ResponseValidaterFactory()
    {
    	
    }
    public static ResponseValidater getValidater(String messageType) throws ServiceException
    {
    	ResponseValidater validater = null;
    	try
    	{
//    		PropertiesCache.addPropertiesFile
//    		( 
//    		  System.getProperty(WS_PROPERTIES_LOCATION) +"//" + WS_PROPERTIES_NAME);
//    		propertiesCache = PropertiesCache.getInstance();
//    		String location = propertiesCache.getInstanceProperty(SPRING_CONTEXT_NAME);
// //   		SpringServiceFactory springFac = SpringServiceFactory.getInstance("C:\\Projects\\fxp-rel3.1\\ws\\ws\\admin"+ "\\" + SPRING_CONTEXT_NAME);
    		SpringServiceFactory springFac = SpringServiceFactory.getInstance(SPRING_CONTEXT_NAME);
    		validater = (ResponseValidater)springFac.getService(messageType + "_validater");
    		
    	}
    	catch(Exception ex)
        {
    	    log.info(ex.getMessage());
    	    throw new ServiceException(RESULT_CODE_PROCESSING_ERROR,ex.getMessage()+" " + "messageType: " + " No validater for " + messageType);
        }
    	return validater;
    }
}
