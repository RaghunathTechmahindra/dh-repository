/**
 * <p>Title: ServiceExcecuterFactory.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version ? Apr 25, 2006)
 *
 */

package com.filogix.externalservice;

import com.basis100.resources.PropertiesCache;
import com.filogix.externalservice.handler.IResponseCallBackHandler;
import com.filogix.spring.SpringServiceFactory;

public class ServiceExecutorFactory implements ServiceConst
{
	static private PropertiesCache propertiesCache;
    public ServiceExecutorFactory()
    {
        
    }
    
//    public static ResponseHandler getHandler(String sourceId, String messageType) throws ServiceException
//    {
//    	ResponseHandler handler = null;
//        if (RESOURCE_ID_DATX_APP.equals(sourceId))
//        {
//        
//            if (MESSAGE_TYPE_APPRAISAL_RESPONSE_REPORT.equals(messageType))
//            	handler =  new AppraisalResponseExecutor();
//            else if (MESSAGE_TYPE_STATUS_UPDATE.equals(messageType))
//            	handler = new AppraisalStatusUpdateExecutor();
//            else if (MESSAGE_TYPE_MI_CALLBACK.equals(messageType))
//            {
//            	//springFac = new SpringServiceFactory("!!!.xlm");
//            	//handler = springFac.getService("ResponseQueueHandler");
//            	handler = new ResponseQueueHandler();
//            }
//            else
//            {
//                throw new ServiceException(RESULT_CODE_PROCESSING_ERROR, "messageType: " + messageType + " is not registerd for " + sourceId);
//            }
//        }
//        else if (RESOURCE_ID_DATX_CLOSING.equals(sourceId))
//        {
//            if (MESSAGE_TYPE_STATUS_UPDATE.equals(messageType))
//            {
//            	handler = new ClosingStatusUpdateExecutor();
//            }
//            else
//                throw new ServiceException(RESULT_CODE_PROCESSING_ERROR, "messageType: " + messageType + " is not registerd for " + sourceId);
//        }
//        else
//        {
//            throw new ServiceException(RESULT_CODE_PROCESSING_ERROR, "sourceID: " + sourceId + " is not registerd");
//        }
//        return handler;
//    }
    
    public static IResponseCallBackHandler getHandler(String sourceId, String messageType) throws ServiceException
    {
    	IResponseCallBackHandler handler = null;
    	try
    	{
    		// this is for Appraisal and Closing
    	    // make sure if any new service doesn't use "StatusUpdate" as messageType
    	    if(MESSAGE_TYPE_STATUS_UPDATE.equals(messageType))
    		{
    				messageType = sourceId + "_" + messageType;
    		}
    		
    		SpringServiceFactory springFac = SpringServiceFactory.getInstance(SPRING_CONTEXT_NAME);
	    	handler = (IResponseCallBackHandler)springFac.getService(messageType + "_service");

	    }
	    
	    catch (Exception ex)
        {
            throw new ServiceException(RESULT_CODE_PROCESSING_ERROR,ex.getMessage()+" " + "messageType: " + messageType + " is not registerd for " + sourceId);
        }
	    return handler;
    }
    
}
