/**
 * <p>Title: RequestDescriptor.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � Apr 26, 2006)
 *
 */

package com.filogix.externalservice;

import java.util.Locale;

public class MessageDescriptor
{
    public static final String CR = System.getProperty("line.separator");
    /**
     * @param args
     */
    
//  add by Clement Aug 18 2006
    private String _productType;
    private String _requestType;
    private String _serviceProvider;
    
    private String _sourceID;
    private String _password;
    private String _clientKey;
    private String _transactionID = "";
    private String _messageType;
    private int _languageID = 0;
    
    public int getLanguageID()
    {
        return _languageID;
    }
    public void setLanguageID(int languageID)
    {
        _languageID = languageID;
    }
    public String getMessageType()
    {
        return _messageType;
    }
    public void setMessageType(String messageType)
    {
        _messageType = messageType;
    }
    public String getPassword()
    {
        return _password;
    }
    public void setPassword(String password)
    {
        _password = password;
    }
    public String getSourceID()
    {
        return _sourceID;
    }
    public void setSourceID(String sourceID)
    {
        _sourceID = sourceID;
    }
    public String getClientKey()
    {
        return _clientKey;
    }
    public void setClientKey(String clientKey)
    {
        this._clientKey = clientKey;
    }
    public String getTransactionID()
    {
        return _transactionID;
    }
    public void setTransactionID(String transactionID)
    {
        _transactionID = transactionID;
    }
    // add by Clement Aug 18 2006
    public String getProductType()
    {
    	return _productType;
    }
    public void setProductType(String productType)
    {
    	this._productType = productType;
    }
    public String getRequestType()
    {
    	return _requestType;
    }
    public void setRequestType(String requestType)
    {
    	this._requestType = requestType;
    }
    public String getServiceProvider()
    {
    	return _serviceProvider;
    }
    public void setServiceProvider(String serviceProvider)
    {
    	this._serviceProvider = serviceProvider;
    }
    public String toString()
    {
        return new StringBuffer()
            .append("Source ID: " + _sourceID + CR)
            .append("Password: " + _password + CR)
            .append("ClientKey: " + _clientKey + CR)
            .append("TransactionID: " + _transactionID + CR)
            .append("MessageType: " + _messageType + CR)
            .append("LanguageID: " + _languageID + CR)
            .append("ProductType: " + _productType + CR)
            .append("RequestType: " + _requestType + CR)
            .append("ServiceProvider: " + _serviceProvider).toString();
    }
    
}
