CREATE OR REPLACE package body vpd_security_policy  as

   FUNCTION SET_INSTITUTION_ACCESS(D1 VARCHAR2, D2 VARCHAR2) RETURN VARCHAR2 is

        pred_stat                    varchar2(2000):='';

   begin

      select sys_context('vpd_app_context', 'institution_profile_id_list')

        into pred_stat

        from dual;

 

        return pred_stat;

   end;

end vpd_security_policy;
