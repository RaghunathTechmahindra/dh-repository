/*
 * @(#)ExpressWebContext.java    2007-7-31
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */

package mosApp;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.ExpressWebContext;
import com.filogix.express.web.jato.ExpressSQLConnectionManager;

/**
 * This is the SQL Connection Manager for JATO framework, which support JNDI
 * datasource, direct JDBC connection, and IoC container managed datasource.
 * <p>
 * Since Express 3.3 we are going to use jdbc connection pool data source
 * managed by Express IoC Container.  The data source will be a service in
 * Express Web Context.
 *
 * @auther <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since 3.3
 */
public class SQLConnectionManagerImpl extends ExpressSQLConnectionManager {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(SQLConnectionManagerImpl.class);

    /**
     * Constructor.
     */
    public SQLConnectionManagerImpl() {
        super();
    }

    static {

        // make sure the jdbc driver is in the classpath.
        try {

            if (_log.isDebugEnabled())
                _log.debug("Make sure the Oracle jdbc driver is exist!");
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch(ClassNotFoundException e) {

            e.printStackTrace();
        }

        // turn on IoC container lookup.
        setUsingContext(true);
        // mapping the datasouce key to JATO SQL Connection datasource mapping.
        if (_log.isDebugEnabled())
            _log.debug("Add Express DataSource to JATO datasource mapping");
        addDataSourceMapping("jdbc/orcl",
                             ExpressWebContext.DATASOURCE_CONNECTION_FACTORY_KEY);
    }
}