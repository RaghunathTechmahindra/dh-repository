package mosApp;

/**
 * 29/Feb/2008 DVG #DG702 MLM x BDN merge adjustments
 * 11/Oct/2007 DVG #DG638 LEN217180: Malcolm Whitton Condition Text exceeds 4000 characters
 */

import com.iplanet.jato.ModelTypeMap;
import com.iplanet.jato.ModelTypeMapBase;

/**
 *
 *
 * @author	Todd Fast, todd.fast@sun.com
 * @author	Mike Frisino, michael.frisino@sun.com
 * @version	$Id: ModelTypeMapImpl.java,v 1.10 2002/01/23 07:14:43 todd Exp $
 * * @version 1.1  <br>
 * Date: 7/28/006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *	added mapping for borrower identification type tile <br>
 * @version 1.2 06-JUN-2008 XS_2.13 
 * Added mapping for QualifyingDetailsModelImpl and QualifyingDetailsModel
 * @version 1.3 16-JUN-2008 XS_16.7 
 * Added mapping for doComponentMortgageModelImpl and doComponentMortgageModel
 *                   doComponentLocModelImpl      and doComponentLocModel
 *                   doComponentLaonModelImpl     and doComponentLaonModel
 *                   doComponentCreditCardModelImpl and doComponentCreditCardModel
 *                   doComponentOverDraftModelImpl  and doComponentOverDraftModel
 */
public class ModelTypeMapImpl extends ModelTypeMapBase
implements ModelTypeMap
{
    /**
     *
     *
     */
    public ModelTypeMapImpl()
    {
        super();
    }
    static
    {

        addModelInterfaceMapping(
                mosApp.MosSystem.doDetailViewCBLiabilitiesModel.class,
                mosApp.MosSystem.doDetailViewCBLiabilitiesModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doApplicantOtherIncomesSelectModel.class,
                mosApp.MosSystem.doApplicantOtherIncomesSelectModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDetailViewTDSModel.class,
                mosApp.MosSystem.doDetailViewTDSModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doMainViewBorrowerModel.class,
                mosApp.MosSystem.doMainViewBorrowerModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doMIReviewModel.class,
                mosApp.MosSystem.doMIReviewModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doIALEmploymentHistoryModel.class,
                mosApp.MosSystem.doIALEmploymentHistoryModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealGetReferenceInfoModel.class,
                mosApp.MosSystem.doDealGetReferenceInfoModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doMainViewMortgageInsurerModel.class,
                mosApp.MosSystem.doMainViewMortgageInsurerModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doCountCreditBureauLiabModel.class,
                mosApp.MosSystem.doCountCreditBureauLiabModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doPropertyAddressModel.class,
                mosApp.MosSystem.doPropertyAddressModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDetailViewAssetsModel.class,
                mosApp.MosSystem.doDetailViewAssetsModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUWCountNetWorthModel.class,
                mosApp.MosSystem.doUWCountNetWorthModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doGetPaymentTermIdModel.class,
                mosApp.MosSystem.doGetPaymentTermIdModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUWDealDownPaymentModel.class,
                mosApp.MosSystem.doUWDealDownPaymentModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doApplicantEntryApplicantSelectModel.class,
                mosApp.MosSystem.doApplicantEntryApplicantSelectModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealOriginationModel.class,
                mosApp.MosSystem.doDealOriginationModelImpl.class);

//	CIBC DTV
//		    addModelInterfaceMapping(
//			    mosApp.MosSystem.doDefConditionModel.class,
//			    mosApp.MosSystem.doDefConditionModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUWPropertyInfosModel.class,
                mosApp.MosSystem.doUWPropertyInfosModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doMainViewDownPaymentModel.class,
                mosApp.MosSystem.doMainViewDownPaymentModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doApplicantCreditBureauLiabilitiesModel.class,
                mosApp.MosSystem.doApplicantCreditBureauLiabilitiesModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doGetBridgeIdModel.class,
                mosApp.MosSystem.doGetBridgeIdModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doGetLivingSpaceUnitOfMeasureModel.class,
                mosApp.MosSystem.doGetLivingSpaceUnitOfMeasureModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doSourceFirmModel.class,
                mosApp.MosSystem.doSourceFirmModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doGetBranchNameModel.class,
                mosApp.MosSystem.doGetBranchNameModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUserExistsModel.class,
                mosApp.MosSystem.doUserExistsModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doCreditReviewModel.class,
                mosApp.MosSystem.doCreditReviewModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doGetReferenceDealTypeModel.class,
                mosApp.MosSystem.doGetReferenceDealTypeModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doSetLiabPaymentQualifierModel.class,
                mosApp.MosSystem.doSetLiabPaymentQualifierModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUWMIStatusModel.class,
                mosApp.MosSystem.doUWMIStatusModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doSourceUserModel.class,
                mosApp.MosSystem.doSourceUserModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doNoteCategoryModel.class,
                mosApp.MosSystem.doNoteCategoryModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doGetRateCodeModel.class,
                mosApp.MosSystem.doGetRateCodeModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doSecurityPolicyModel.class,
                mosApp.MosSystem.doSecurityPolicyModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doMultyDealAssignUserModel.class,
                mosApp.MosSystem.doMultyDealAssignUserModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doCountTasksbyPriorityModel.class,
                mosApp.MosSystem.doCountTasksbyPriorityModelImpl.class);

		addModelInterfaceMapping(
				mosApp.MosSystem.doCountTasksbyPriorityModel2.class,
				mosApp.MosSystem.doCountTasksbyPriorityModel2Impl.class);
        
        addModelInterfaceMapping(
                mosApp.MosSystem.doUWDealRateLockModel.class,
                mosApp.MosSystem.doUWDealRateLockModelImpl.class);

// CIBC DTV
//		addModelInterfaceMapping(
//			mosApp.MosSystem.doStdConditionModel.class,
//			mosApp.MosSystem.doStdConditionModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUWMIUpfrontModel.class,
                mosApp.MosSystem.doUWMIUpfrontModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doTransactionHistoryModel.class,
                mosApp.MosSystem.doTransactionHistoryModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUserAdminGetGroupsModel.class,
                mosApp.MosSystem.doUserAdminGetGroupsModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doPageNameLabelModel.class,
                mosApp.MosSystem.doPageNameLabelModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doClosingDetailsModel.class,
                mosApp.MosSystem.doClosingDetailsModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealEntryGDSTDSAppDetailsModel.class,
                mosApp.MosSystem.doDealEntryGDSTDSAppDetailsModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doMWQPickListModel.class,
                mosApp.MosSystem.doMWQPickListModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doRowGeneratorModel.class,
                mosApp.MosSystem.doRowGeneratorModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doSessionInformationModel.class,
                mosApp.MosSystem.doSessionInformationModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doMainViewDealModel.class,
                mosApp.MosSystem.doMainViewDealModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doIWorkQueueModel.class,
                mosApp.MosSystem.doIWorkQueueModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doBorrowerEntryIncomeUpdateModel.class,
                mosApp.MosSystem.doBorrowerEntryIncomeUpdateModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDetailViewLiabilitiesModel.class,
                mosApp.MosSystem.doDetailViewLiabilitiesModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doBorrowerEntryEmploymentHistoryModel.class,
                mosApp.MosSystem.doBorrowerEntryEmploymentHistoryModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doFetchLenderProfileIdModel.class,
                mosApp.MosSystem.doFetchLenderProfileIdModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUWApplicantFinancialDetailsModel.class,
                mosApp.MosSystem.doUWApplicantFinancialDetailsModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doIALBorrowerModel.class,
                mosApp.MosSystem.doIALBorrowerModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doSaveNewPasswordModel.class,
                mosApp.MosSystem.doSaveNewPasswordModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doBridgeDescriptionModel.class,
                mosApp.MosSystem.doBridgeDescriptionModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doApplicantLiabilitiesSelectModel.class,
                mosApp.MosSystem.doApplicantLiabilitiesSelectModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doClosingDealFeesModel.class,
                mosApp.MosSystem.doClosingDealFeesModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doBorrowerEntryAssetUpdateModel.class,
                mosApp.MosSystem.doBorrowerEntryAssetUpdateModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealMITypeModel.class,
                mosApp.MosSystem.doDealMITypeModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUWDealSummarySnapShotModel.class,
                mosApp.MosSystem.doUWDealSummarySnapShotModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUWBridgeModel.class,
                mosApp.MosSystem.doUWBridgeModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUWDealOriginationModel.class,
                mosApp.MosSystem.doUWDealOriginationModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealUnderwriterModel.class,
                mosApp.MosSystem.doDealUnderwriterModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doApplicantCreditRefsSelectModel.class,
                mosApp.MosSystem.doApplicantCreditRefsSelectModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doGetActualClosingDateModel.class,
                mosApp.MosSystem.doGetActualClosingDateModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doBorrowerEntryLiabilityModel.class,
                mosApp.MosSystem.doBorrowerEntryLiabilityModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doAssignedTaskWorkqueueModel.class,
                mosApp.MosSystem.doAssignedTaskWorkqueueModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUWSourceDetailsModel.class,
                mosApp.MosSystem.doUWSourceDetailsModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doGetPricingProfileIdModel.class,
                mosApp.MosSystem.doGetPricingProfileIdModelImpl.class);

// CIBC DTV		
//		addModelInterfaceMapping(
//			mosApp.MosSystem.doDocConditionModel.class,
//			mosApp.MosSystem.doDocConditionModelImpl.class);
// CIBC DTV
//		addModelInterfaceMapping(
//			mosApp.MosSystem.doConditionDetailModel.class,
//			mosApp.MosSystem.doConditionDetailModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDetailViewPropertiesModel.class,
                mosApp.MosSystem.doDetailViewPropertiesModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealMIIndicatorModel.class,
                mosApp.MosSystem.doDealMIIndicatorModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDetailViewBorrowerAddressesModel.class,
                mosApp.MosSystem.doDetailViewBorrowerAddressesModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealRateLockModel.class,
                mosApp.MosSystem.doDealRateLockModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealEntryEscrowModel.class,
                mosApp.MosSystem.doDealEntryEscrowModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doBridgeDetailsModel.class,
                mosApp.MosSystem.doBridgeDetailsModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doRowGenerator2Model.class,
                mosApp.MosSystem.doRowGenerator2ModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doTaskStatusModel.class,
                mosApp.MosSystem.doTaskStatusModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doCusConditionModel.class,
                mosApp.MosSystem.doCusConditionModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDetailViewIncomesModel.class,
                mosApp.MosSystem.doDetailViewIncomesModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealEntryBridgeModel.class,
                mosApp.MosSystem.doDealEntryBridgeModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealFeeModel.class,
                mosApp.MosSystem.doDealFeeModelImpl.class);

// CIBC DTV
//		addModelInterfaceMapping(
//			mosApp.MosSystem.doDocTrackFunderCondModel.class,
//			mosApp.MosSystem.doDocTrackFunderCondModelImpl.class);


        addModelInterfaceMapping(
                mosApp.MosSystem.doUserAdminGetRegionsModel.class,
                mosApp.MosSystem.doUserAdminGetRegionsModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUWMIInsurerModel.class,
                mosApp.MosSystem.doUWMIInsurerModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doIALDealHeaderModel.class,
                mosApp.MosSystem.doIALDealHeaderModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doFunderModel.class,
                mosApp.MosSystem.doFunderModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doPirorityCountModel.class,
                mosApp.MosSystem.doPirorityCountModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDetailViewEmpOtherIncomeModel.class,
                mosApp.MosSystem.doDetailViewEmpOtherIncomeModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDetailViewGDSModel.class,
                mosApp.MosSystem.doDetailViewGDSModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doGetPricingRateInventoryModel.class,
                mosApp.MosSystem.doGetPricingRateInventoryModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUWGDSTDSApplicantDetailsModel.class,
                mosApp.MosSystem.doUWGDSTDSApplicantDetailsModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealEntryMainSelectModel.class,
                mosApp.MosSystem.doDealEntryMainSelectModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealProgressTasksModel.class,
                mosApp.MosSystem.doDealProgressTasksModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doTotalFeeAmountModel.class,
                mosApp.MosSystem.doTotalFeeAmountModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealHistoryModel.class,
                mosApp.MosSystem.doDealHistoryModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDetailViewPropertyExpensesModel.class,
                mosApp.MosSystem.doDetailViewPropertyExpensesModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doAppraiserSelectModel.class,
                mosApp.MosSystem.doAppraiserSelectModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUWSourceInformationModel.class,
                mosApp.MosSystem.doUWSourceInformationModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doApplicantEmploymentIncomeTypeModel.class,
                mosApp.MosSystem.doApplicantEmploymentIncomeTypeModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doPriorityModel.class,
                mosApp.MosSystem.doPriorityModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUserAdminGetBranchesModel.class,
                mosApp.MosSystem.doUserAdminGetBranchesModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDecisionModificationTypeModel.class,
                mosApp.MosSystem.doDecisionModificationTypeModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDocStatusModel.class,
                mosApp.MosSystem.doDocStatusModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealGetDefaultProductModel.class,
                mosApp.MosSystem.doDealGetDefaultProductModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doPartyTypeModel.class,
                mosApp.MosSystem.doPartyTypeModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDetailViewEmploymentModel.class,
                mosApp.MosSystem.doDetailViewEmploymentModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUserAdminPageAccessTypeModel.class,
                mosApp.MosSystem.doUserAdminPageAccessTypeModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doBorrowerEntryIncomeModel.class,
                mosApp.MosSystem.doBorrowerEntryIncomeModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDetailViewBorrowersModel.class,
                mosApp.MosSystem.doDetailViewBorrowersModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doGetUnderWriterNameModel.class,
                mosApp.MosSystem.doGetUnderWriterNameModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doPickConditionModel.class,
                mosApp.MosSystem.doPickConditionModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUWMIIndicatorModel.class,
                mosApp.MosSystem.doUWMIIndicatorModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealSummarySnapShotModel.class,
                mosApp.MosSystem.doDealSummarySnapShotModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doPasswordCorrectModel.class,
                mosApp.MosSystem.doPasswordCorrectModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doNullDataObjectModel.class,
                mosApp.MosSystem.doNullDataObjectModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doPropertyExpenseSelectModel.class,
                mosApp.MosSystem.doPropertyExpenseSelectModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealInformationModel.class,
                mosApp.MosSystem.doDealInformationModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealEntryDownPaymentModel.class,
                mosApp.MosSystem.doDealEntryDownPaymentModelImpl.class);

// CIBC DTV
//		addModelInterfaceMapping(
//			mosApp.MosSystem.doDocTrackOrderedModel.class,
//			mosApp.MosSystem.doDocTrackOrderedModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doApplicantOtherIncomeTypeModel.class,
                mosApp.MosSystem.doApplicantOtherIncomeTypeModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doBorrowerEntryCreditModel.class,
                mosApp.MosSystem.doBorrowerEntryCreditModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doBorrowerEntryCreditUpdateModel.class,
                mosApp.MosSystem.doBorrowerEntryCreditUpdateModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDetailViewEmpJobTitleModel.class,
                mosApp.MosSystem.doDetailViewEmpJobTitleModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doApplicantEntryAddressSelectModel.class,
                mosApp.MosSystem.doApplicantEntryAddressSelectModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDetailViewCreditRefsModel.class,
                mosApp.MosSystem.doDetailViewCreditRefsModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealMIInsurerModel.class,
                mosApp.MosSystem.doDealMIInsurerModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doBorrowerEntryLiabilityUpdateModel.class,
                mosApp.MosSystem.doBorrowerEntryLiabilityUpdateModelImpl.class);

		addModelInterfaceMapping(
				mosApp.MosSystem.doBorrowerEntryAssetModel.class,
				mosApp.MosSystem.doBorrowerEntryAssetModelImpl.class);
// CIBC DTV
//		addModelInterfaceMapping(
//			mosApp.MosSystem.doDocTrackSignedCommitmentModel.class,
//			mosApp.MosSystem.doDocTrackSignedCommitmentModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doAppraisalPropertyModel.class,
                mosApp.MosSystem.doAppraisalPropertyModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUWDealSelectMainModel.class,
                mosApp.MosSystem.doUWDealSelectMainModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doGetMortgageProductNameModel.class,
                mosApp.MosSystem.doGetMortgageProductNameModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doMasterWorkQueueModel.class,
                mosApp.MosSystem.doMasterWorkQueueModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doMainViewEscrowModel.class,
                mosApp.MosSystem.doMainViewEscrowModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doApplicantGSDTSDModel.class,
                mosApp.MosSystem.doApplicantGSDTSDModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doBridgeReviewNewModel.class,
                mosApp.MosSystem.doBridgeReviewNewModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUnderwriterModel.class,
                mosApp.MosSystem.doUnderwriterModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doMainViewBridgeModel.class,
                mosApp.MosSystem.doMainViewBridgeModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doAdministratorModel.class,
                mosApp.MosSystem.doAdministratorModelImpl.class);

        /*#DG638 not used
		addModelInterfaceMapping(
			mosApp.MosSystem.doFrenchTextModel.class,
			mosApp.MosSystem.doFrenchTextModelImpl.class);*/

        addModelInterfaceMapping(
                mosApp.MosSystem.doStreetTypeModel.class,
                mosApp.MosSystem.doStreetTypeModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doMainViewPrimaryPropertyModel.class,
                mosApp.MosSystem.doMainViewPrimaryPropertyModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUWEscrowDetailsModel.class,
                mosApp.MosSystem.doUWEscrowDetailsModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUserAdminGetJointAppModel.class,
                mosApp.MosSystem.doUserAdminGetJointAppModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doGetLotSizeUnitOfMeasureModel.class,
                mosApp.MosSystem.doGetLotSizeUnitOfMeasureModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUnderwriter1Model.class,
                mosApp.MosSystem.doUnderwriter1ModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doBridgeSalePropertyDetailsModel.class,
                mosApp.MosSystem.doBridgeSalePropertyDetailsModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doIsBridgeInDealModel.class,
                mosApp.MosSystem.doIsBridgeInDealModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealEntryPropertySelectModel.class,
                mosApp.MosSystem.doDealEntryPropertySelectModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doPAWorkQueueModel.class,
                mosApp.MosSystem.doPAWorkQueueModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealEntryApplicantSelectModel.class,
                mosApp.MosSystem.doDealEntryApplicantSelectModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doGetRateDateModel.class,
                mosApp.MosSystem.doGetRateDateModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUserAdminProfileInfoModel.class,
                mosApp.MosSystem.doUserAdminProfileInfoModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doApplicantEmploymentIncomeSelectModel.class,
                mosApp.MosSystem.doApplicantEmploymentIncomeSelectModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUWMITypeModel.class,
                mosApp.MosSystem.doUWMITypeModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUWMIPayorModel.class,
                mosApp.MosSystem.doUWMIPayorModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealNotesInfoModel.class,
                mosApp.MosSystem.doDealNotesInfoModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doSourceOfBusinessModel.class,
                mosApp.MosSystem.doSourceOfBusinessModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doViewAppraiserInfoModel.class,
                mosApp.MosSystem.doViewAppraiserInfoModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doTotalAppraisedModel.class,
                mosApp.MosSystem.doTotalAppraisedModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doApplicantAssetsSelectModel.class,
                mosApp.MosSystem.doApplicantAssetsSelectModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealEntrySourceInfoModel.class,
                mosApp.MosSystem.doDealEntrySourceInfoModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDealMIStatusModel.class,
                mosApp.MosSystem.doDealMIStatusModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doGetZonningDescModel.class,
                mosApp.MosSystem.doGetZonningDescModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doPartySearchModel.class,
                mosApp.MosSystem.doPartySearchModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doPartySummaryModel.class,
                mosApp.MosSystem.doPartySummaryModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doPropertyEntryPropertySelectModel.class,
                mosApp.MosSystem.doPropertyEntryPropertySelectModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doInterestRateHistoryModel.class,
                mosApp.MosSystem.doInterestRateHistoryModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doClosingActivityInfoModel.class,
                mosApp.MosSystem.doClosingActivityInfoModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.SessionStateModel.class,
                mosApp.MosSystem.SessionStateModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.SavedPagesModel.class,
                mosApp.MosSystem.SavedPagesModelImpl.class);

        //// SYNCADD. Merge with 9.3.1.8 version.
        addModelInterfaceMapping(
                mosApp.MosSystem.doNewDealInfoModel.class,
                mosApp.MosSystem.doNewDealInfoModelImpl.class);

		addModelInterfaceMapping(
				mosApp.MosSystem.doOrigDealInfoModel.class,
				mosApp.MosSystem.doOrigDealInfoModelImpl.class);
// CIBC DTV
//		addModelInterfaceMapping(
//			mosApp.MosSystem.doSignConditionModel.class,
//			mosApp.MosSystem.doSignConditionModelImpl.class);

    //--Release2.1--TD_DTS_CR--//
//	CIBC DTV	
//		addModelInterfaceMapping(
//			mosApp.MosSystem.doDocTrackingStdConditionModel.class,
//			mosApp.MosSystem.doDocTrackingStdConditionModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doDocTrackingCusConditionModel.class,
                mosApp.MosSystem.doDocTrackingCusConditionModelImpl.class);
        //--Release2.1--TD_DTS_CR--end//
        //--TD_MWQ_CR--start//
        addModelInterfaceMapping(
                mosApp.MosSystem.doCountTasksByTaskNameModel.class,
                mosApp.MosSystem.doCountTasksByTaskNameModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doTasksOutstandingsModel.class,
                mosApp.MosSystem.doTasksOutstandingsModelImpl.class);

//      ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
        addModelInterfaceMapping(
                mosApp.MosSystem.doIdentificationTypeModel.class,
                mosApp.MosSystem.doIdentificationTypeModelImpl.class);

//      ***** Change by NBC Impl. Team - Version 1.1 - End*****//
        //--TD_MWQ_CR--end//
        //--> Performance enhancement : elimination of the 2nd call of the DataObject
        //--> the DealId, ApplicationId, CopyId and WFTaskId will be stored and retrieved in
        //--> the hidden values on each row.
        //--> By Billy 16Jan2004
        addModelInterfaceMapping(
                mosApp.MosSystem.doMasterWorkQueueCountModel.class,
                mosApp.MosSystem.doMasterWorkQueueCountModelImpl.class);
        //=============================================================================
        //--DJ_LDI_CR--start//
        addModelInterfaceMapping(
                mosApp.MosSystem.doLifeDisabilityInsuranceInfoModel.class,
                mosApp.MosSystem.doLifeDisabilityInsuranceInfoModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doLifeDisabilityInsProportionsModel.class,
                mosApp.MosSystem.doLifeDisabilityInsProportionsModelImpl.class);
        //--DJ_LDI_CR--end//

        /* #DG702 not used or replaced by simple sql 
     //--DJ_CR134--start--19Oct2004--//
     addModelInterfaceMapping(
       mosApp.MosSystem.doDealNotesBrokerInfoModel.class,
       mosApp.MosSystem.doDealNotesBrokerInfoModelImpl.class);
     //--DJ_CR134--end--19Oct2004--//  */

        //--DJ_CR136--23Nov2004--start--//
        addModelInterfaceMapping(
                mosApp.MosSystem.doInsureOnlyApplicantModel.class,
                mosApp.MosSystem.doInsureOnlyApplicantModelImpl.class);
        //--DJ_CR136--23Nov2004--end--//

        //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
        addModelInterfaceMapping(
                mosApp.MosSystem.doPrimeIndexRateModel.class,
                mosApp.MosSystem.doPrimeIndexRateModelImpl.class);
        //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

        //  ***** Change by NBC Impl. Team - Version 1.2 - Start *****//
        addModelInterfaceMapping(
                mosApp.MosSystem.doApplicantIdentificationTypeModel.class,
                mosApp.MosSystem.doApplicantIdentificationTypeModelImpl.class);
        //  ***** Change by NBC Impl. Team - Version 1.2 - Start *****//

        // 3.1 -- Disclosure
        addModelInterfaceMapping(
                mosApp.MosSystem.doDisclosureSelectModel.class,
                mosApp.MosSystem.doDisclosureSelectModelImpl.class);

        // SEAN AVM Service Feb 15, 2006: add the model type map for the new models.
        addModelInterfaceMapping(
                mosApp.MosSystem.models.AVMOrderModel.class,
                mosApp.MosSystem.models.AVMOrderModelImpl.class);
        // SEAN AVM Service END.

        //--Release3.1--begins
        //--by Hiro Apr 25, 2006
        addModelInterfaceMapping(
                mosApp.MosSystem.doUWProgressAdvanceTypeModel.class,
                mosApp.MosSystem.doUWProgressAdvanceTypeModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doUWLOCRepaymentTypeModel.class,
                mosApp.MosSystem.doUWLOCRepaymentTypeModelImpl.class);

        //Outsource Closing
        addModelInterfaceMapping(
                mosApp.MosSystem.doOutsourcedClosingServiceModel.class,
                mosApp.MosSystem.doOutsourcedClosingServiceModelImpl.class);

        //FPC Closing
        addModelInterfaceMapping(
                mosApp.MosSystem.doFixedPriceClosingInfoModel.class,
                mosApp.MosSystem.doFixedPriceClosingInfoModelImpl.class);

        //Appraisal 
        addModelInterfaceMapping(
                mosApp.MosSystem.doAppraisalProviderModel.class,
                mosApp.MosSystem.doAppraisalProviderModelImpl.class);

        //--Release3.1--ends

        //***** Change by NBC/PP Implementation Team - GCD - START ******//

        // Credit Decision Screen
        addModelInterfaceMapping(
                mosApp.MosSystem.doAdjudicationMainBNCModel.class,
                mosApp.MosSystem.doAdjudicationMainBNCModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doAdjudicationResponseBNCModel.class,
                mosApp.MosSystem.doAdjudicationResponseBNCModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doAdjudicationResponseBNCDecisionMessagesModel.class,
                mosApp.MosSystem.doAdjudicationResponseBNCDecisionMessagesModelImpl.class);

        // Credit Decision Screen - Applicant Summary
        addModelInterfaceMapping(
                mosApp.MosSystem.doAdjudicationApplicantMainBNCModel.class,
                mosApp.MosSystem.doAdjudicationApplicantMainBNCModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doAdjudicationApplicantIncomeBNCModel.class,
                mosApp.MosSystem.doAdjudicationApplicantIncomeBNCModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doAdjudicationApplicantResponseBNCModel.class,
                mosApp.MosSystem.doAdjudicationApplicantResponseBNCModelImpl.class);

        // CD Applicant Details screen
        addModelInterfaceMapping(
                mosApp.MosSystem.doCDApplicantDetailsModel.class,
                mosApp.MosSystem.doCDApplicantDetailsModelImpl.class);

        //***** Change by NBC/PP Implementation Team - GCD - END ******//

        //MCM START

        addModelInterfaceMapping(
                mosApp.MosSystem.doComponentInfoModel.class,
                mosApp.MosSystem.doComponentInfoModelImpl.class);
        //MCM END

        /***************MCM Impl team changes starts - XS_2.13*******************/
        addModelInterfaceMapping(
                mosApp.MosSystem.doQualifyingDetailsModel.class,
                mosApp.MosSystem.doQualifyingDetailsModelImpl.class);
        /***************MCM Impl team changes ends - XS_2.13*******************/
        /*************************MCM IMPL TEAM STARTS XS_16.7 **********************/
        addModelInterfaceMapping(
                mosApp.MosSystem.doComponentMortgageModel.class,
                mosApp.MosSystem.doComponentMortgageModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doComponentLocModel.class,
                mosApp.MosSystem.doComponentLocModelImpl.class);

        addModelInterfaceMapping(
                mosApp.MosSystem.doComponentLoanModel.class,
                mosApp.MosSystem.doComponentLoanModelImpl.class);
        addModelInterfaceMapping(
                mosApp.MosSystem.doComponentCreditCardModel.class,
                mosApp.MosSystem.doComponentCreditCardModelImpl.class);
        addModelInterfaceMapping(
                mosApp.MosSystem.doComponentOverDraftModel.class,
                mosApp.MosSystem.doComponentOverDraftModelImpl.class);
        addModelInterfaceMapping(
                mosApp.MosSystem.doComponentSummaryModel.class,
                mosApp.MosSystem.doComponentSummaryModelImpl.class);
        /*************************MCM IMPL TEAM ENDS XS_16.7 **********************/
        addModelInterfaceMapping(
        		mosApp.MosSystem.models.MasterWorkQueueSearchModel.class,
        		mosApp.MosSystem.models.MasterWorkQueueSearchModelImpl.class);
        addModelInterfaceMapping(
                mosApp.MosSystem.models.MasterWorkQueueSearchCountModel.class,
                mosApp.MosSystem.models.MasterWorkQueueSearchCountModelImpl.class);
        
        /************************ Model for Quick Links*****************/
        addModelInterfaceMapping(
                mosApp.MosSystem.doQuickLinkModel.class,
                mosApp.MosSystem.doQuickLinkModelImpl.class);

    }
	
	/*
	 * CIBC DTV.
	 * Some models, should been mapped based on the newly introduced
	 * mossys.properties parameter: com.basis100.conditions.dtverbiage.generateonthefly.
	 * It could be done after initialization of the mossysy.properties file.
	 * See MosSystemServlet's init method.
	 */
	public static void mapLater(String isGenerateOnTheFly) {
		
		if(isGenerateOnTheFly == null || "N".equals(isGenerateOnTheFly)) {
		    addModelInterfaceMapping(
			    mosApp.MosSystem.doDefConditionModel.class,
			    mosApp.MosSystem.doDefConditionModelImpl.class);
		    
		    addModelInterfaceMapping(
					mosApp.MosSystem.doConditionDetailModel.class,
					mosApp.MosSystem.doConditionDetailModelImpl.class);
		    
		    addModelInterfaceMapping(
					mosApp.MosSystem.doStdConditionModel.class,
					mosApp.MosSystem.doStdConditionModelImpl.class);
		    
		    addModelInterfaceMapping(
					mosApp.MosSystem.doDocConditionModel.class,
					mosApp.MosSystem.doDocConditionModelImpl.class);
		    
		    addModelInterfaceMapping(
					mosApp.MosSystem.doSignConditionModel.class,
					mosApp.MosSystem.doSignConditionModelImpl.class);
		    
		    addModelInterfaceMapping(
					mosApp.MosSystem.doDocTrackSignedCommitmentModel.class,
					mosApp.MosSystem.doDocTrackSignedCommitmentModelImpl.class);
		    
		    addModelInterfaceMapping(
					mosApp.MosSystem.doDocTrackOrderedModel.class,
					mosApp.MosSystem.doDocTrackOrderedModelImpl.class);
		    
		    addModelInterfaceMapping(
					mosApp.MosSystem.doDocTrackFunderCondModel.class,
					mosApp.MosSystem.doDocTrackFunderCondModelImpl.class);
		    
		    addModelInterfaceMapping(
					mosApp.MosSystem.doDocTrackingStdConditionModel.class,
					mosApp.MosSystem.doDocTrackingStdConditionModelImpl.class);
		    
		}
		else {
			addModelInterfaceMapping(
				    mosApp.MosSystem.doDefConditionModel.class,
				    mosApp.MosSystem.doDefConditionModelImplNoVerbiage.class);
			
			addModelInterfaceMapping(
					mosApp.MosSystem.doConditionDetailModel.class,
					mosApp.MosSystem.doConditionDetailModelImplNoVerbiage.class);
			
			addModelInterfaceMapping(
					mosApp.MosSystem.doStdConditionModel.class,
					mosApp.MosSystem.doStdConditionModelImplNoVerbiage.class);
			
			addModelInterfaceMapping(
					mosApp.MosSystem.doDocConditionModel.class,
					mosApp.MosSystem.doDocConditionModelImplNoVerbiage.class);
			
			addModelInterfaceMapping(
					mosApp.MosSystem.doSignConditionModel.class,
					mosApp.MosSystem.doSignConditionModelImplNoVerbiage.class);
			
			addModelInterfaceMapping(
					mosApp.MosSystem.doDocTrackSignedCommitmentModel.class,
					mosApp.MosSystem.doDocTrackSignedCommitmentModelImplNoVerbiage.class);
			
			addModelInterfaceMapping(
					mosApp.MosSystem.doDocTrackOrderedModel.class,
					mosApp.MosSystem.doDocTrackOrderedModelImplNoVerbiage.class);
			
			addModelInterfaceMapping(
					mosApp.MosSystem.doDocTrackFunderCondModel.class,
					mosApp.MosSystem.doDocTrackFunderCondModelImplNoVerbiage.class);
			
			addModelInterfaceMapping(
					mosApp.MosSystem.doDocTrackingStdConditionModel.class,
					mosApp.MosSystem.doDocTrackingStdConditionModelImplNoVerbiage.class);
			
		}
		
	}
}

