package mosApp;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.util.*;

/**
 *
 *
 *
 */
public class mosAppServletBase extends ApplicationServletBase
{
	/**
	 *
	 *
	 */
	public mosAppServletBase()
	{
		super();
	}


	////////////////////////////////////////////////////////////////////////////////
	// Servlet initialization methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected void initializeRequestContext(RequestContext requestContext)
	{
		super.initializeRequestContext(requestContext);


		// Set a model manager in the request context.  This must be
		// done at the application level because the MODEL_TYPE_MAP
		// is application specific.
		ModelManager modelManager = new ModelManager(requestContext, MODEL_TYPE_MAP);

		((RequestContextImpl) requestContext).setModelManager(modelManager);


		// Set a connection manager in the request context.  This must be
		// done at the application level because the SQL_CONNECTION_MANAGER
		// is application specific.
		//
		// TODO: Currently, we are assuming that the SQL Connection manager can
		// be a single static instance. If in the future there is a need for the
		// SQL connection manager to be a per-request instance, this code should
		// be changed to reflect that.
		((RequestContextImpl) requestContext).setSQLConnectionManager(SQL_CONNECTION_MANAGER);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Servlet methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void init(ServletConfig config)
		throws ServletException
	{
		super.init(config);

		MODEL_TYPE_MAP = new ModelTypeMapImpl();

		SQL_CONNECTION_MANAGER = new SQLConnectionManagerImpl();

	}


	/**
	 *
	 *
	 */
	public void destroy()
	{
		super.destroy();

	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected void onNewSession(RequestContext requestContext)
		throws ServletException
	{
		super.onNewSession(requestContext);

	}


	/**
	 *
	 *
	 */
	protected void onSessionTimeout(RequestContext requestContext)
		throws ServletException
	{

		// TODO: Migrate specific onSessionTimeoutEvent code
		// This call to the super version of this method will
		// throw a servlet exception by default.  Eliminate this
		// method call to handle the event in this class.
		super.onSessionTimeout(requestContext);

	}


	/**
	 *
	 *
	 */
	protected void onBeforeRequest(RequestContext requestContext)
		throws ServletException
	{
		super.onBeforeRequest(requestContext);

	}


	/**
	 *
	 *
	 */
	protected void onInitializeHandler(RequestContext requestContext, RequestHandler requestHandler)
		throws ServletException
	{
		super.onInitializeHandler(requestContext, requestHandler);

	}


	/**
	 *
	 *
	 */
	protected void onAfterRequest(RequestContext requestContext)
	{
		super.onAfterRequest(requestContext);

	}
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	private static ModelTypeMap MODEL_TYPE_MAP;
	private static SQLConnectionManager SQL_CONNECTION_MANAGER;

}

