package mosApp.MosSystem;

import com.iplanet.jato.model.sql.*;

import java.math.BigDecimal;


/**
 *
 *
 *
 */
public interface doMasterWorkQueueModel extends QueryModel, SelectQueryModel
{
  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////
  public static final String FIELD_DFASSIGNTASKID = "dfAssignTaskId";
  public static final String FIELD_DFDEALID = "dfDEALID";
  public static final String FIELD_DFAPPLICATIONID = "dfAPPLICATIONID";
  public static final String FIELD_DFDUETIMESTAMP = "dfDUETIMESTAMP";
  public static final String FIELD_DFUSERPROFILEID = "dfUserProfileId";
  public static final String FIELD_DFASSIGNEDUSERNAME = "dfAssignedUserName";
  public static final String FIELD_DFBORROWER = "dfBorrower";
  public static final String FIELD_DFLOGIN = "dfLogin";
  public static final String FIELD_DFGROUPPROFILEID = "dfGroupProfileId";
  public static final String FIELD_DFBRANCHPROFILEID = "dfBranchProfileId";
  public static final String FIELD_DFREGIONPROFILEID = "dfRegionProfileId";
  public static final String FIELD_DFCOPYID = "dfCopyId";
  public static final String FIELD_DFTASKSTATUSID = "dfTaskStatusId";

  //--Release2.1--//
  //// 7. In addition to the regular 6 steps to adjust the computed column of fly
  //// another step should be done as well: the 'fake' hidden fields should be created
  //// to override the UserTypeDescripton in the afterExecute() method.
  public static final String FIELD_DFUTDESCRIPTION = "dfUtDescription";
  public static final String FIELD_DFUSERTYPEID = "dfUserTypeId";

  //--> Combined with the fileds from doMWQPickListModel for performance optimization
  //--> By Billy 17July2003
  public static final String FIELD_DFSTATUSDESC = "dfStatusDesc";
  public static final String FIELD_DFTMDESCRIPTION = "dfTMDescription";
  public static final String FIELD_DFTASKNAME = "dfTASKNAME";
  public static final String FIELD_DFPRIORITYDESCRIPTION = "dfPriorityDescription";
  public static final String FIELD_DFTASKID = "dfTASKID";

  //=================================================================================
  //--TD_MWQ_CR--start--//
  //// Added Source Firm and Source to the Borrower info.
  public static final String FIELD_DFSOURCE = "dfSource";
  public static final String FIELD_SFSHORTNAME = "dfSFShortName";

  //=================================================================================
  //-- ========== SCR#750 begins ======================================== --//
  //-- by Neil on Dec/20/2004
  public static final String FIELD_DFHOLDREASONID = "dfHoldReasonId";
  public static final String FIELD_DFHOLDREASONDESCRIPTION = "dfHoldReasonDescription";
  public static final String FIELD_DFDEALSTATUS = "dfDealStatus";

  public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
  public static final String FIELD_DFINSTITUTIONNAME = "dfInstitutionName";
  /**
   *
   *
   */
  public java.math.BigDecimal getDfAssignTaskId();

  /**
   *
   *
   */
  public void setDfAssignTaskId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfDEALID();

  /**
   *
   *
   */
  public void setDfDEALID(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfAPPLICATIONID();

  /**
   *
   *
   */
  public void setDfAPPLICATIONID(String value);

  /**
   *
   *
   */
  public java.sql.Timestamp getDfDUETIMESTAMP();

  /**
   *
   *
   */
  public void setDfDUETIMESTAMP(java.sql.Timestamp value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfUserProfileId();

  /**
   *
   *
   */
  public void setDfUserProfileId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfAssignedUserName();

  /**
   *
   *
   */
  public void setDfAssignedUserName(String value);

  /**
   *
   *
   */
  public String getDfBorrower();

  /**
   *
   *
   */
  public void setDfBorrower(String value);

  /**
   *
   *
   */
  public String getDfLogin();

  /**
   *
   *
   */
  public void setDfLogin(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfGroupProfileId();

  /**
   *
   *
   */
  public void setDfGroupProfileId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfBranchProfileId();

  /**
   *
   *
   */
  public void setDfBranchProfileId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfRegionProfileId();

  /**
   *
   *
   */
  public void setDfRegionProfileId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfCopyId();

  /**
   *
   *
   */
  public void setDfCopyId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfTaskStatusId();

  /**
   *
   *
   */
  public void setDfTaskStatusId(java.math.BigDecimal value);

  //--Release2.1--//
  //// 7. In addition to the regular 6 steps to adjust the computed column of fly
  //// another step should be done as well: the 'fake' hidden fields should be created
  //// to override the UserTypeDescripton in the afterExecute() method.

  /**
   *
   *
   */
  public String getDfUserDescription();

  /**
   *
   *
   */
  public void setDfUserDescription(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfUserTypeId();

  /**
   *
   *
   */
  public void setDfUserTypeId(java.math.BigDecimal value);

  //--> Combined with the fileds from doMWQPickListModel for performance optimization
  //--> By Billy 17July2003
  public String getDfStatusDesc();

  public void setDfStatusDesc(String value);

  public String getDfTMDescription();

  public void setDfTMDescription(String value);

  public String getDfTASKNAME();

  public void setDfTASKNAME(String value);

  public String getDfPriorityDescription();

  public void setDfPriorityDescription(String value);

  public java.math.BigDecimal getDfTASKID();

  public void setDfTASKID(java.math.BigDecimal value);

  //--TD_MWQ_CR--start--//
  //// Added Source Firm and Source to the Borrower info.
  public String getDfSFShortName();

  public void setDfSFShortName(String value);

  public String getDfSource();

  public void setDfSource(String value);

  public BigDecimal getDfHoldReasonId();

  public void setDfHoldReasonId(BigDecimal value);

  public void setDfHoldReasonDescription(String value);

  public String getDfHoldReasonDescription();

  public String getDfDealStatus();

  public void setDfDealStatus(String value);

  public BigDecimal getDfInstitutionId();
  
  public void setDfInstitutionId(BigDecimal value);
  
  public String getDfInstitutionName();
  
  public void setDfInstitutionName(String value);
  
  
}
