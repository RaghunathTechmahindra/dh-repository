package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgDealSummaryRepeatedDownPaymentsTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealSummaryRepeatedDownPaymentsTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(100);
		setPrimaryModelClass( doMainViewDownPaymentModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStDownPaymentSource().setValue(CHILD_STDOWNPAYMENTSOURCE_RESET_VALUE);
		getStDownPaymentDesc().setValue(CHILD_STDOWNPAYMENTDESC_RESET_VALUE);
		getStDownPaymentAmount().setValue(CHILD_STDOWNPAYMENTAMOUNT_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STDOWNPAYMENTSOURCE,StaticTextField.class);
		registerChild(CHILD_STDOWNPAYMENTDESC,StaticTextField.class);
		registerChild(CHILD_STDOWNPAYMENTAMOUNT,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoMainViewDownPaymentModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STDOWNPAYMENTSOURCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoMainViewDownPaymentModel(),
				CHILD_STDOWNPAYMENTSOURCE,
				doMainViewDownPaymentModel.FIELD_DFDOWNPAYMENTSOURCE,
				CHILD_STDOWNPAYMENTSOURCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDOWNPAYMENTDESC))
		{
			StaticTextField child = new StaticTextField(this,
				getdoMainViewDownPaymentModel(),
				CHILD_STDOWNPAYMENTDESC,
				doMainViewDownPaymentModel.FIELD_DFDOWNPAYMENTDESC,
				CHILD_STDOWNPAYMENTDESC_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDOWNPAYMENTAMOUNT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoMainViewDownPaymentModel(),
				CHILD_STDOWNPAYMENTAMOUNT,
				doMainViewDownPaymentModel.FIELD_DFDOWNPAYMENTAMOUNT,
				CHILD_STDOWNPAYMENTAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDownPaymentSource()
	{
		return (StaticTextField)getChild(CHILD_STDOWNPAYMENTSOURCE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDownPaymentDesc()
	{
		return (StaticTextField)getChild(CHILD_STDOWNPAYMENTDESC);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDownPaymentAmount()
	{
		return (StaticTextField)getChild(CHILD_STDOWNPAYMENTAMOUNT);
	}


	/**
	 *
	 *
	 */
	public doMainViewDownPaymentModel getdoMainViewDownPaymentModel()
	{
		if (doMainViewDownPayment == null)
			doMainViewDownPayment = (doMainViewDownPaymentModel) getModel(doMainViewDownPaymentModel.class);
		return doMainViewDownPayment;
	}


	/**
	 *
	 *
	 */
	public void setdoMainViewDownPaymentModel(doMainViewDownPaymentModel model)
	{
			doMainViewDownPayment = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STDOWNPAYMENTSOURCE="stDownPaymentSource";
	public static final String CHILD_STDOWNPAYMENTSOURCE_RESET_VALUE="";
	public static final String CHILD_STDOWNPAYMENTDESC="stDownPaymentDesc";
	public static final String CHILD_STDOWNPAYMENTDESC_RESET_VALUE="";
	public static final String CHILD_STDOWNPAYMENTAMOUNT="stDownPaymentAmount";
	public static final String CHILD_STDOWNPAYMENTAMOUNT_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doMainViewDownPaymentModel doMainViewDownPayment=null;
  private DealSummaryHandler handler=new DealSummaryHandler();
}

