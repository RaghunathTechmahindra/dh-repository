package mosApp.MosSystem;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.WFTaskExecServices;
import com.basis100.workflow.entity.AssignedTask;
import com.basis100.workflow.pk.AssignedTaskBeanPK;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.util.TypeConverter;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.Option;
import com.iplanet.jato.view.html.OptionList;
import static mosApp.MosSystem.pgIWorkQueueRepeated1TiledView.MAX_NUMBER_OF_INDIVIDUALWORKQUEUE_PER_PAGE;
import static mosApp.MosSystem.pgIWorkQueueRepeated1TiledView.MIN_NUMBER_OF_INDIVIDUALWORKQUEUE_PER_PAGE;

//// Original (iMT) inheritance.
////public class iWorkQueueHandler extends PageHandlerCommon
////	implements Cloneable, Sc, Mc
//// Highly discussable implementation with the XXXXHandler extension of the
//// ViewBeanBase JATO class. The sequence diagrams must be verified. On the other hand,
//// This implementation allows to use the basic JATO framework elements such as
//// RequestContext() class. Theoretically, it could allow to override the JATO/iMT
//// auto-migration. Temporarily these fragments are commented out now for the compability
//// with the stand alone MWHC<--PHC<--XXXHandlerName Utility hierarchy.
//// getModel(ClassName.class), etc., could be also rewritten in the Utility hierarchy
//// using this inheritance.
public class iWorkQueueHandler
  extends PageHandlerCommon implements Cloneable, Sc, Mc, View, ViewBean
{

  static boolean informPTask = "Y".equals(PropertiesCache.getInstance().getInstanceProperty
            ("com.filogix.express.informPriorityTask", "Y"));

  private static final Log _log = LogFactory.getLog(iWorkQueueHandler.class);
  
  public static final int FILTER_NUMBER_OPTIONS = 1;

  // count of chaoce boxes
  public static final int FILTER_INCLUDE_COMPLETED = 1;
  public static final String[] FILTER_INCLUDE_CB_FIELD_NAMES =
    {"chbFilterIncludeCompleted"};

  //-- ========== SCR#931 begins ========== --//
  //-- by Neil on Feb 09, 2005
  // only for BMO TASKSTATUS
  protected static final int BMO_TASKSTATUS_OPEN = 1;
  protected static final int BMO_TASKSTATUS_COMPLETE = 3;
  //-- ========== SCR#931   ends ========== --//

  ////////////////////////////////////////////////////////////////////////
  // OVERRIDE
  ////////////////////////////////////////////////////////////////////////
  // on this page the 10th (index 9) filter option on the cursor object
  // is used to hold boolean for priority tasks displayed
  private static final int DISP_PRIORITY_TASKS = 9;

  // following used since handles multiple pages (Individual Work Queue, Deal Progress)
  // appropriate valus set by call to setPageFlag()
  //--> Shouldn't have this here. It cause lot of mess.
  //--> Commented out by BILLY 22July2002
  boolean isIWorkQueue;
  boolean isDealProgress;

  public iWorkQueueHandler()
  {
    super();
    theSessionState = super.theSessionState;
    savedPages = super.savedPages;
  }

  public iWorkQueueHandler(String name)
  {
    this(null, name); // No parent
    theSessionState = super.theSessionState;
    savedPages = super.savedPages;
  }

  protected iWorkQueueHandler(View parent, String name)
  {
    super(parent, name);
    theSessionState = super.theSessionState;
    savedPages = super.savedPages;
  }

  /**
   *
   *
   */
  public iWorkQueueHandler cloneSS()
  {
    return(iWorkQueueHandler)super.cloneSafeShallow();
  }

  ////////////////////////////////////////////////////////////////////////
  //// //==>Vlad 18Jul2002. Almost each method in the IWHandler is reconsidered and
  //// cleaned up. The Oracle problem doesn't exist anymore for
  //// the current driver and the comment below temporarily left (should be eliminated
  //// with the final framework code clean up).
  ////
  ////
  //// This method should be logically separated into the BasisXpress Utility
  //// hierarchy part supporting the TiledView family. Along with this the
  //// PageCursorClass should be reorginized into the BeanAdapterModel for the
  //// TiledView. It will allow to implement the forward/back navigation through
  //// the selected rows.
  ////
  //// Another exraodinary problem related to this method is as follows.
  //// JDBC doesn't give us a way to determine the number of rows a query returned.
  //// Therefore, the user has to rely on the next() mechanism to iterate until all
  //// the rows have been fetched.
  ////
  //// JATO (in ResultSetModelBase and base/supporting classes) tries to hack this by
  //// iterating through the number of rows, counting them, and then resetting to
  //// the previous location.
  //// There are two drawbacks of this approach:
  //// 1) This would fetch all the data from the database and cache it locally
  //// (obviating the usefulness of cursors)
  //// 2) Some JDBC drivers may not support repositioning the cursor when
  //// we're done.
  ////
  //// Another and again very important problem is related to the point #2 of the
  //// previous paragraph. The class CSpDBResultTable has no analog in JATO.
  //// Basically the CSpDBResultTable is a ResultSet, but common (non-commercial) JDBC drivers
  //// has a lot of bugs taking the full access to the ResultSet. For example, the oficial
  //// BasisXpress jdbc driver (jdbc:oracle:thin, fixed only for Oracle 8.1.7 version) can't rely on
  //// the getString() and getObject() methods:
  ////
  //// - BUG-1247015 (published by Oracle).
  //// When using ResultSet::getObject() to access CHAR or VARCHAR columns
  //// in scrollable result sets, ResultSet::getObject() returned null.
  ////
  ////- BUG-1349713  (published by Oracle).
  //// getString() of scrollable result sets returns incorrect values if
  //// the column data contains multibyte characters.
  ////
  //// Even for the Oracle 8.1.7 and Oracle 9.0.1 (!!!!)
  //// The scrollable result set implementation has the following limitation:
  //// - setFetchDirection() on ScrollableResultSet is not supported.
  //// - refreshRow() on ScrollableResultSet does not support all
  //// combinations of sensitivity and concurrency. The following
  //// table depicts the supported combinations.
  ////
  //// Support Type Concurrency
  //// -------------------------------------------------------
  //// no TYPE_FORWARD_ONLY CONCUR_READ_ONLY
  //// no TYPE_FORWARD_ONLY CONCUR_UPDATABLE
  //// no TYPE_SCROLL_INSENSITIVE CONCUR_READ_ONLY
  //// yes TYPE_SCROLL_INSENSITIVE CONCUR_UPDATABLE
  //// yes TYPE_SCROLL_SENSITIVE CONCUR_READ_ONLY
  //// yes TYPE_SCROLL_SENSITIVE CONCUR_UPDATABLE
  ////
  //// The line three prevents to create the Statement (by JATO and by the ExecuteImmediate
  //// utility class) in the modern facion:
  ////
  ////      statement=connection.createStatement(
  ////      ResultSet.TYPE_SCROLL_INSENSITIVE,
  ////      ResultSet.CONCUR_READ_ONLY);
  //// The 'dirty' one statement=connection.createStatement() is used instead.
  ////
  //// Also, as a result of the #1 of JATO limitation the common BasisXpress call:
  //// int noofrows = theDO.getLastResults().getNumOfRows();
  //// could not be hundred persent reliable converted to the JATO.
  //// Unfortunately, we have no choice as follow by the JDBC API and JATO solution
  //// on it taking into account all described above problems (and may be unfortunately)
  //// more down the road.
  ////
  //// First we converted all related BasisXpess fragments/idioms to the JATO API
  //// and after the careful testing of the jdbc drivers must be done. Upon thid
  //// the recommendations of their usage should follow as a part of the conversion
  //// activity.
  public void setupBeforePageGeneration()
  {
    PageEntry pg = (PageEntry)theSessionState.getCurrentPage();
    _log.debug("IWQH@setupBeforePageGen::isWorkQueue: " + isIWorkQueue);
    _log.debug("IWQH@setupBeforePageGen::Page DealInstituionId: " + pg.getDealInstitutionId());
    _log.debug("IWQH@setupBeforePageGen::SessionState DealInstituionId: " + theSessionState.getDealInstitutionId());
    //theSessionState = getTheSessionState();
    //theSessionState.setSessionUserId(pg.g,
    //                                pg.getInstitutionId());

    if(pg.getSetupBeforeGenerationCalled() == true)
    {
      return;
    }

    pg.setSetupBeforeGenerationCalled(true);

    // -- //
    // determine page
    setPageFlag(pg);

    _log.debug("IWQH@setupBeforePageGen::isWorkQueue: " + isIWorkQueue);
    _log.debug("IWQH@setupBeforePageGen::isDealProgress: " + isDealProgress);
    // cursor object for work queue tasks list (named "DEFAULT" for this page)
    PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

    if(tds != null)
    {
      //_log.debug("IWQH@setupBeforePageGen::PstName: " + tds.getName());
      //_log.debug("IWQ@setupBeforePageGen::TotalNumberOfRows: " + tds.getTotalRows());
      //_log.debug("IWQ@setupBeforePageGen::RelIndex: " + tds.getRelSelectedRowNdx());
      //_log.debug("IWQ@setupBeforePageGen::CurrPageNdx: " + tds.getCurrPageNdx() + " RowsPerPage:" + tds.getRowsPerPage());
      //_log.debug("IWQ@setupBeforePageGen::AbsIndex: " + tds.getAbsSelectedRowNdx());
    }

    Hashtable pst = pg.getPageStateTable();

    //--> Check if need to refresh
    //--> by Billy 22July2002
    boolean refresh = false;
    Boolean theRefreshFlag = (Boolean)pst.get("REFRESH");

    if(theRefreshFlag != null)
    {
      refresh = theRefreshFlag.booleanValue();
    }

    //=================================================
    if((tds == null) || (refresh == true))
    {
      //--> Reset Refresh flag from PST
      //--> By BILLY 22July2002
      pst.put("REFRESH", new Boolean(false));

      //========================================
      //_log.debug("IWQH@setupBeforePageGen:: tds == null :: Create new tds !!");
      // determine number of task (shown) per display page
      String voName = "Repeated1";
      int institutionId = this.getTheSessionState().getCurrentPage().dealPage ? this.getTheSessionState().getDealInstitutionId():this.getTheSessionState().getUserInstitutionId();
      int tasksPerPage = TypeConverter.asInt(PropertiesCache.getInstance().getProperty(institutionId, "com.filogix.individualworkqueue.numofrows", String.valueOf(MAX_NUMBER_OF_INDIVIDUALWORKQUEUE_PER_PAGE)), MAX_NUMBER_OF_INDIVIDUALWORKQUEUE_PER_PAGE);
      if (tasksPerPage > MAX_NUMBER_OF_INDIVIDUALWORKQUEUE_PER_PAGE) {
         tasksPerPage = MAX_NUMBER_OF_INDIVIDUALWORKQUEUE_PER_PAGE;
      } else if (tasksPerPage < MIN_NUMBER_OF_INDIVIDUALWORKQUEUE_PER_PAGE) {
         tasksPerPage = MIN_NUMBER_OF_INDIVIDUALWORKQUEUE_PER_PAGE;
      }
      
      // set up and save task display state object
      tds = new PageCursorInfo("DEFAULT", voName, tasksPerPage, 0);

      // last (0) - don't know total number yet!
      tds.setRefresh(true);
      tds.setSortOption(Mc.IWQ_SORT_ORDER_PRIORITY);
      tds.setFilterOption(0, false);
      pst.put(tds.getName(), tds);

      //-- ========== SCR#931 begins ========== --//
      //-- by Neil on Feb 09, 2005
      // FOR BMO, default filter is by [OPEN]
      boolean isClientBMO = (PropertiesCache.getInstance().getInstanceProperty("com.basis100.fxp.clientid.bmo", "N")).equals("Y");
      _log.debug("IWQH@setupBeforePageGeneration::Client is BMO?" + isClientBMO);
      if(isClientBMO)
      {
        tds.setFilterOptionId(0);
      }
      //-- ========== SCR#931 ends ========== --//
    }

    // attribute state tracking (usage depends on page)
    Vector applIds = null;

    Vector dealIds = null;

    Vector copyIds = null;

    Vector wqIds = null;

    Vector borrowerNames = null;

    Vector assignedTo = null;
    
    Vector institutionIds = null;

    doIWorkQueueModelImpl wqDO =
      (doIWorkQueueModelImpl)(RequestManager
      .getRequestContext()
      .getModelManager()
      .getModel(doIWorkQueueModel.class));

    if(isIWorkQueue == true)
    {
      applIds = new Vector();
      pst.put("DEALAPPIDS", applIds);
      dealIds = new Vector();
      pst.put("DEALIDS", dealIds);
      copyIds = new Vector();
      pst.put("COPYIDS", copyIds);
      wqIds = new Vector();
      pst.put("WQ_IDS", wqIds);
      borrowerNames = new Vector();
      pst.put("B_NAMES", borrowerNames);
      institutionIds = new Vector();
      pst.put("INSTITUTIONIDS", institutionIds);
      setIWorkQueueCriteria(wqDO, tds, pg);
    }
    else
    {
      assignedTo = new Vector();
      pst.put("ASSIGNED_TO", assignedTo);

      setDealProgressCriteria(wqDO, tds, pg);
    }

    // execute work queue DO and store application ids (for snap shot population)
    // and work queue record id (for task selection support)
    String str = "";
    String profileId = "";
    ////spider.database.CSpDBResultTable resultTab = null;
    try
    {
      //_log.debug("IWQH@SQL before execute: " + wqDO.getSelectSQL());
      ResultSet rs = wqDO.executeSelect(null);

      if((rs != null) && (wqDO.getSize() > 0))
      {
        tds.setTotalRows(wqDO.getSize());
        _log.debug("Size of the tds: " + wqDO.getSize());

        while(wqDO.next())
        {
          Object applId =
            wqDO.getValue(doIWorkQueueModel.FIELD_DFAPPLICATIONID);
          Object workQueueId =
            wqDO.getValue(
            doIWorkQueueModel.FIELD_DFASSIGNEDTASKSWORKQUEUEID);
          Object dealId =
            wqDO.getValue(doIWorkQueueModel.FIELD_DFDEALID);
          Object copyId =
            wqDO.getValue(doIWorkQueueModel.FIELD_DFCOPYID);
          Object borrowerLastName =
            wqDO.getValue(
            doIWorkQueueModel.FIELD_DFCONTACTLASTNAME);
          Object profId =
            wqDO.getValue(doIWorkQueueModel.FIELD_DFUSERPROFILEID);
          
          Object institutionId = 
            wqDO.getValue(doIWorkQueueModel.FIELD_DFINSTITUTIONID);

          _log.debug("IWQHsetupBeforePageGeneration()@AppId: " + applId);
          _log.debug("IWQHsetupBeforePageGeneration()@WorkQueueId: " + workQueueId);
          _log.debug("IWQHsetupBeforePageGeneration()@DealId: " + dealId);
          _log.debug("IWQHsetupBeforePageGeneration()@CopyId: " + copyId);
          _log.debug("IWQHsetupBeforePageGeneration()@BorrowerLastName: " + borrowerLastName);
          _log.debug("IWQHsetupBeforePageGeneration()@ProfileId: " + profId);
          _log.debug("IWQHsetupBeforePageGeneration()@institutionId: " + institutionId );
          if(isIWorkQueue == true)
          {
            applIds.addElement(applId.toString());

            str = cleanNumber(workQueueId.toString());
            wqIds.addElement(new Integer(str));

            str = cleanNumber(dealId.toString());
            dealIds.addElement(new Integer(str));
            borrowerNames.addElement(new Integer(str));

            // changed to borrower names below
            str = cleanNumber(copyId.toString());
            copyIds.addElement(new Integer(str));
            
            str = cleanNumber(institutionId.toString());
            institutionIds.addElement(new Integer(str));
            
          }

          if(isDealProgress)
          {
            profileId = (profileId.toString()).trim();

            int ndx = profileId.indexOf(".");

            if(ndx > 0)
            {
              profileId = profileId.substring(0, ndx);
            }

            assignedTo.addElement(new Integer(profileId));

            // changes to user names below
          }
        }
      }
      else
      {
        tds.setTotalRows(0);
      }
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }

    if(tds.isTotalRowsChanged()
      || tds.isCriteriaChanged()
      || tds.getFilterOption(DISP_PRIORITY_TASKS))
    {
      // number of rows has changed - unconditional display of first page
      String voName = tds.getVoName();

      //_log.debug("IWQH@setupBeforePageGen::VoName: " + voName);
      pgIWorkQueueRepeated1TiledView vo =
        ((pgIWorkQueueViewBean)getCurrNDPage()).getRepeated1();

      //_log.debug("IWQH@setupBeforePageGen::Repeated1: " + vo);
      //_log.debug("IWQH@setupBeforePageGen::Repeated1Name: " + vo.getName());
      tds.setCurrPageNdx(0);

      ////vo.goToFirst();
      ////Reset the primary model to the "before first" state and resets the display index.
      try
      {
        vo.resetTileIndex();
      }
      catch(ModelControlException mce)
      {
        _log.error(
          "IWQH@setupBeforePageGeneration::Exception: " + mce);
      }

      tds.setTotalRowsChanged(false);
      tds.setCriteriaChanged(false);
    }

    if(isIWorkQueue)
    {
      setupPrimaryBorrowerNameList(borrowerNames, copyIds, institutionIds);
    }

    //_log.debug("IWQH@After setupPrimaryBorrowerNameList");
    if(isDealProgress)
    {
      setupAssignedUserList(assignedTo, institutionIds);
    }

    //_log.debug("IWQH@After setupAssignedUserList");
    _log.debug("IWQH@isIWorkQueue before setDealForSnap..." + isIWorkQueue);
    // set application Id for deal summary snapshot
    if(isIWorkQueue == true)
    {
      setDealForSnapshot(pg, tds);
    }

    _log.debug("IWQH@After setDealForSnapshot");
    setupDealSummarySnapShotDO(pg);

    _log.debug("IWQH@After setupDealSummarySnapShotDO");

  }

  ////////////////////////////////////////////////////////////////////////
  // in --> "nnn.dd" out --> "nnn"
  private String cleanNumber(String strNum)
  {
    strNum = strNum.trim();

    int ndx = strNum.indexOf(".");

    if(ndx > 0)
    {
      return strNum.substring(0, ndx);
    }

    return strNum;
  }

  ////////////////////////////////////////////////////////////////////////
  //This method is used to populate the fields in the header
  //with the appropriate information
  public void populatePageDisplayFields()
  {
    //_log.debug("IWQH@populatePageDisplayField:: start");
    ////PageEntry pg = getTheSession().getCurrentPage();
    PageEntry pg = (PageEntry)theSessionState.getCurrentPage();

    Hashtable pst = pg.getPageStateTable();

    //_log.debug("IWQH@populatePageDisplayField::pstSize: " + pst.size());
    PageCursorInfo tds = (PageCursorInfo)pst.get("DEFAULT");

    //_log.debug("IWQH@populatePageDisplayField:cursorVolume: " + tds.getTotalRows());
    // ensure resume checking of priority tasks after display of queue
    theSessionState.setDetectAlertTasks("Y");

    //_log.debug("IWQH@populatePageDisplayField::before populatePageShellDisplayField");
    populatePageShellDisplayFields();

    // Quick Link Menu display
    displayQuickLinkMenu();
 
    //_log.debug("IWQH@populatePageDisplayField::after populatePageShellDisplayField");
    populatePageDealSummarySnapShot();

    //_log.debug("IWQH@populatePageDisplayField::after populatePageDealSummarySnapShot");
    ////This method should be adjusted in the PageHandlerCommon base class.
    populatePreviousPagesLinks();

    ////_log.debug("IWQH@populatePageDisplayField::after populatePreviousPagesLinks");
    //_log.debug("IWQH@populatePageDisplayField::CurrentNDPage: " + getCurrNDPage());
    // populate the sort options.
    populateSortOptions(getCurrNDPage(), tds);

    // populate the filter options
    populateFilterOptions(getCurrNDPage(), tds);

    // generate tasks displayed display field
    populateRowsDisplayed(getCurrNDPage(), tds);

    // set display of priority alert tasks list (work queue only)
    setPageFlag(pg);

    if(isIWorkQueue)
    {
      if(tds.getFilterOption(DISP_PRIORITY_TASKS) == true)
      {
        getCurrNDPage().setDisplayFieldValue(
          "displayprioritytasks",
          new String("Y"));
      }
      else
      {
        getCurrNDPage().setDisplayFieldValue(
          "displayprioritytasks",
          new String("N"));
      }
    }
  }

  public void populatedRepeatedFields(int repeatedNumber, int ndx)
  {
    PageEntry pg = (PageEntry)theSessionState.getCurrentPage();

    // determine page
    setPageFlag(pg);

    if(isDealProgress)
    {
      populatedDPRepeatedFields(pg, repeatedNumber, ndx);

      return;
    }

    if(isIWorkQueue)
    {
      populatedIWQRepeatedFields(pg, repeatedNumber, ndx);

      return;
    }
  }

  ////////////////////////////////////////////////////////////////////////
  //
  // Handlers
  //
  // called to open the page associated with the task - <taskNdx> is relative to the
  // tasks being displayed on the the page, not absolute.
  public void handleSelectTask(int taskNdx)
  {
    ////SessionState theSession = getTheSession();
    ////PageEntry pg = getTheSession().getCurrentPage();

    PageEntry pg = (PageEntry)theSessionState.getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    PageCursorInfo tds = (PageCursorInfo)pst.get("DEFAULT");
    int numPages = 0;

    try
    {
      tds.setRelSelectedRowNdx(taskNdx);

      // ensure page entry reflects the deal (and copy id) of the task selected
      setDealForSnapshot(pg, tds);

      int absNdx = tds.getAbsSelectedRowNdx();
      
      Vector instIds = (Vector)pst.get("INSTITUTIONIDS");
      int instId = ((Integer)instIds.elementAt(absNdx)).intValue();
      // changed for userprofileid setting
      pg.setDealInstitutionId(instId);
      pg.setPageMosUserId(theSessionState.getUserProfileId(instId));
      pg.setPageMosUserTypeId(theSessionState.getUserTypeId(instId));

      getSessionResourceKit().getExpressState().setDealInstitutionId(instId);
      
      //_log.debug("IWQH@handleSelectTask::AbsIndx: " + absNdx);
      Vector wqIds = (Vector)pst.get("WQ_IDS");

      int wqId = ((Integer)wqIds.elementAt(absNdx)).intValue();
      
      //_log.debug("IWQH@handleSelectTask::wqId: " + wqId);
      AssignedTask aTask = new AssignedTask(getSessionResourceKit());
      aTask.findByPrimaryKey(new AssignedTaskBeanPK(wqId));

      //_log.debug("IWQH@handleSelectTask::aTask.getTaskId(): " + aTask.getTaskId());
      // check for a sentinel task
      if(aTask.getTaskId() >= 50000)
      {
        //_log.debug("IWQH@handleSelectTask::In sentinel Task Handling !!");
        // navigate to sentinel page (unconditionally)
        PageEntry sentinelPg =
          setupMainPagePageEntry(Mc.PGNM_SENTINEL, aTask.getDealId(), -1, aTask.getInstitutionProfileId());

        // store work queue record id in page criteria id (1)
        sentinelPg.setPageCriteriaId1(wqId);
        getSavedPages().setNextPage(sentinelPg);
        navigateToNextPage(true);

        return;
      }

      WFTaskExecServices tes =
        new WFTaskExecServices((PageHandlerCommon)this, pg);

      //_log.debug("IWQH@handleSelectTask::After new WFTaskExecServices : tes = "+tes);
      numPages =
        tes.setPageForAssignedTask(
        aTask,
        theSessionState.getUserProfileId(aTask.getInstitutionProfileId()),
        theSessionState.getUserTypeId(aTask.getInstitutionProfileId()));

      //_log.debug("IWQH@handleSelectTask::aTask.numPages: " + numPages);
      if(numPages > 0)
      {
        // pre-qualify task page(s) for access and skip disallowed pages
        //_log.debug("IWQH@handleSelectTask::Before preQualifyTaskPages");
        preQualifyTaskPages(numPages, tes);

        //_log.debug("IWQH@handleSelectTask::After preQualifyTaskPages");
        navigateToNextPage(true);

        return;
      }
    }
    catch(Exception e)
    {
      _log.error(
        "Error encountered opening page from Individual Work Queue : "
        + e.toString());
      _log.error(StringUtil.stack2string(e));
      e.printStackTrace();
    }

    if(numPages <= 0)
    {
      setActiveMessageToAlert(
        BXResources.getSysMsg(
        "TASK_RELATED_UNEXPECTED_FAILURE",
        theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
    }

    return;
  }

  ////////////////////////////////////////////////////////////////////////
  //
  // Check task page link(s) for access (page and deal status). Eliminate pages from links where access denied.
  // Ensures that at least one page is still navigatable aven if such a navigation would fail due to access
  //
  public void preQualifyTaskPages(int numPages, WFTaskExecServices tes) throws Exception
  {
    SessionResourceKit srk = getSessionResourceKit();
    SavedPagesModelImpl sp = getSavedPages();
    PageEntry pg = getSavedPages().getNextPage();

    //_log.debug("IWQH@preQualifyTaskPages::PageEntryName: " + pg.getPageName());
    //_log.debug("IWQH@preQualifyTaskPages::SavedPages: " + sp);
    PageEntry fallBackPg = pg;

    int cid = standardCopySelection(srk, pg);

    pg.setPageDealCID( -1);

    Deal deal = new Deal(srk, null);

    deal.findByPrimaryKey(new DealPK(pg.getPageDealId(), cid));

    Vector v = new Vector();

    for(int i = 0; i < numPages; ++i, pg = pg.getNextPE())
    {
      // qualify by page access
      int accessTypeUser =
        accessByPage(pg, theSessionState, sp, srk, false);

      if(accessTypeUser == Sc.PAGE_ACCESS_DISALLOWED)
      {
        continue;
      }

      if(pg.isDealPage() == true)
      {
        // qualify by deal status
        int accessTypeStatus =
          accessByDealStatus(
          deal,
          pg,
          theSessionState,
          sp,
          srk,
          accessTypeUser,
          false);

        if(!((accessTypeStatus == Sc.PAGE_ACCESS_VIEW_ONLY)
          || (accessTypeStatus == Sc.PAGE_ACCESS_EDIT)))
        {
          continue;
        }
      }

      v.add(pg);
    }

    if(v.size() == 0)
    {
      v.add(fallBackPg);
    }

    // link (re-link actually) the task pages and set first as the 'next page'
    tes.linkTaskPages(v);

    getSavedPages().setNextPage((PageEntry)v.elementAt(0));
  }

  ////////////////////////////////////////////////////////////////////////
  public void handleForwardButton()
  {
    PageEntry pg = (PageEntry)theSessionState.getCurrentPage();

    Hashtable pst = pg.getPageStateTable();

    PageCursorInfo tds = (PageCursorInfo)pst.get("DEFAULT");

    handleForwardBackwardCommon(pg, tds, -1, true);

    return;
  }

  ////////////////////////////////////////////////////////////////////////
  public void handleBackwardButton()
  {
    PageEntry pg = (PageEntry)theSessionState.getCurrentPage();

    Hashtable pst = pg.getPageStateTable();

    PageCursorInfo tds = (PageCursorInfo)pst.get("DEFAULT");

    handleForwardBackwardCommon(pg, tds, -1, false);

    return;
  }

  public void handleSortButton()
  {
    PageEntry pg = (PageEntry)theSessionState.getCurrentPage();

    Hashtable pst = pg.getPageStateTable();

    PageCursorInfo tds = (PageCursorInfo)pst.get("DEFAULT");

    ComboBox cbxSortOptions =
      (ComboBox)getCurrNDPage().getDisplayField("cbSortOptions");

    int sortNdx = tds.getSortOption();

    //_log.debug("IWQH@handleSortButton::sortIndex (from TDS): " + sortNdx);
    //int sortCurrent = sortNdx;

    try
    {
      String value = (String)cbxSortOptions.getValue();

      //_log.debug("IWQH@handleSortButton::ComboBoxValue: " + value);
      sortNdx = (new Integer(value)).intValue();
    }
    catch(Exception e)
    {
      //--> Any Problem default to 0 (default ioption)
      sortNdx = 0;
    }

    tds.setSortOption(sortNdx);

    return;
  }

  public void handleFilterButton()
  {
    ////SessionState theSession = getTheSession();
    ////PageEntry pg = getTheSession().getCurrentPage();
    PageEntry pg = (PageEntry)theSessionState.getCurrentPage();

    //_log.debug("IWQH@handleFilterButton::CurrentPage: " + pg.getPageName());
    Hashtable pst = pg.getPageStateTable();

    //_log.debug("IWQH@handleFilterButton::pstSize: " + pst.size());
    PageCursorInfo tds = (PageCursorInfo)pst.get("DEFAULT");

    ComboBox theCbFilterOptions =
      (ComboBox)getCurrNDPage().getDisplayField("cbFilterOptions");

    //_log.debug("IWQH@handleFilterButton::ComboFilterOptions: " + theCbFilterOptions);
    int filterNdx = tds.getFilterOptionId();

    //_log.debug("IWQH@handleFilterButton::FilterNdx: " + filterNdx);
    //int filterCurrent = filterNdx;

    try
    {
      String sFilterNdx = theCbFilterOptions.stringValue();

      _log.debug("IWQH@handleFilterButton::FilterIndx: " + sFilterNdx);
      tds.setFilterOptionId((new Integer(sFilterNdx)).intValue());
    }
    catch(Exception e)
    {
      ;
    }

    return ;
  }

  public void handleFilterSortResetButton()
  {
    PageEntry pg = (PageEntry)theSessionState.getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    PageCursorInfo tds = (PageCursorInfo)pst.get("DEFAULT");

    //Set Sort and Filter options to defaults
    tds.setFilterOptionId(0);
    tds.setSortOption(0);

    return;
  }

  ////////////////////////////////////////////////////////////////////////
  public void handleSnapShotButton(int relTaskNdx)
  {
    PageEntry pg = (PageEntry)theSessionState.getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    PageCursorInfo tds = (PageCursorInfo)pst.get("DEFAULT");

    //_log.debug("IWQH@handleSnapShotButton :: The Input Rel Selected Idx: " + relTaskNdx);
    tds.setRelSelectedRowNdx(relTaskNdx);
  }

  /**
   * Called after work queue display to ensure no tasks for the user are set as unacknowledged.
   * Rational - action of viewing unacknowledged tasks is sufficient to set then acknowledged!
   */
  public void handleClearPriorityAlertTasks()
  {
      PageEntry pg = (PageEntry)theSessionState.getCurrentPage();

      PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

      // check to see if we had priority alert tasks
      if(tds.getFilterOption(DISP_PRIORITY_TASKS) == false)
      {
          return;
      }

      // we did ... set tasks acknowledged
      SessionResourceKit srk = getSessionResourceKit();

      JdbcExecutor jExec = srk.getJdbcExecutor();

      String sql = null;

      try
      {
          srk.beginTransaction();

          List<Integer> insts = theSessionState.institutionList();
          Iterator<Integer> it = insts.iterator();
          StringBuffer userBuffer = new StringBuffer();

          for (int i = 0; i < insts.size(); i++) {
              Integer institutionId = it.next();
              if (i > 0)
                  userBuffer.append(" OR ");
              userBuffer.append("(ASSIGNEDTASKSWORKQUEUE.userProfileID = "
                      + theSessionState.getUserProfileId(institutionId)
                      + " AND ASSIGNEDTASKSWORKQUEUE.institutionProfileID = "
                      + institutionId
                      + ")");
          }
          if (insts.size() > 1) {
              userBuffer.insert(0, "(");
              userBuffer.append(")");
          }

          sql = "Update ASSIGNEDTASKSWORKQUEUE set ACKNOWLEDGED = 'Y' " +
          "where ACKNOWLEDGED <> 'Y' AND " + userBuffer.toString();
          jExec.executeUpdate(sql);
          srk.commitTransaction();
      }
      catch(Exception e)
      {
          _log.error(
          "Exception @iWorkQueueHandler.clearPriorityAlertTasksFlags: clearing ack flags - ignored");
          _log.error(sql);
          _log.error(e);
          _log.error(StringUtil.stack2string(e));
      }finally{
          srk.cleanTransaction();
      }
  }

  ////////////////////////////////////////////////////////////////////////
  //
  // Implementation
  //
  private void populatedIWQRepeatedFields(
    PageEntry pg,
    int repeatedNumber,
    int ndx)
  {
    Hashtable pst = pg.getPageStateTable();

    String dispStr = "";

    if(repeatedNumber == 2)
    {
      //_log.debug("IWQH@populatePageDisplayField::mode repeatedNumber=2");
      // must be display of priority tasks rows
      try
      {
        Vector borrowerNames = (Vector)pst.get("B_PA_NAMES");
        dispStr = (String)borrowerNames.elementAt(ndx);
      }
      catch(Exception e)
      {
        dispStr = "Cannot Determine!";
      }

      getCurrNDPage().setDisplayFieldValue(
        "Repeated2/stBPBorrower",
        new String());

      return;
    }

    PageCursorInfo tds = (PageCursorInfo)pst.get("DEFAULT");

    int absNdx = getCursorAbsoluteNdx(tds, ndx);

    //_log.debug("IWQH@populatePageDisplayField::absNdx: " + absNdx);
    //_log.debug("IWQH@populatePageDisplayField::indexIn: " + ndx);
    //_log.debug("IWQH@populatePageDisplayField::TotalRowsFromtds: " + tds.getTotalRows());
    try
    {
      if(tds.getTotalRows() > 0)
      {
        Vector borrowerNames = (Vector)pst.get("B_NAMES");
        dispStr = (String)borrowerNames.elementAt(absNdx);

        //_log.debug("IWQH@populatePageDisplayField::displayString: " + dispStr);
      }
    }
    catch(Exception e)
    {
      dispStr = "Cannot Determine!";
    }

    getCurrNDPage().setDisplayFieldValue(
      "Repeated1/stBorrower",
      new String(dispStr));

    // highligt line if current row - else standard background color
    String colorStr = "FFFFCC";

    //_log.debug("IWQH@populatePageDisplayField::RelSelectedIndex: " + tds.getRelSelectedRowNdx());
    if(tds.getRelSelectedRowNdx() == ndx)
    {
      colorStr = "FFFFFF";
    }

    // "99CCFF" - blue - ugly!
    getCurrNDPage().setDisplayFieldValue(
      "Repeated1/bgColor",
      new String(colorStr));
    
    //multi-institution hide
    UserProfile up = null;
    try {
        up = new UserProfile(srk);
        up = up.findByPrimaryKey(new UserProfileBeanPK(theSessionState
                            .getSessionUserId(), theSessionState
                            .getUserInstitutionId()));
    } catch (RemoteException e) {
        _log.error("Can not find UserProfile!", e);
    } catch (FinderException e) {
        _log.error("Can not find UserProfile!", e);
    }
    
    if(!theSessionState.isMultiAccess()) {
        getCurrNDPage().setDisplayFieldValue(
            "Repeated1/stInstitutionId",
            new String(""));
    }
    //multi-institution hide
  }

  ////////////////////////////////////////////////////////////////////////
  private void populatedDPRepeatedFields(
    PageEntry pg,
    int repeatedNumber,
    int ndx)
  {
    Hashtable pst = pg.getPageStateTable();

    PageCursorInfo tds = (PageCursorInfo)pst.get("DEFAULT");

    int absNdx = getCursorAbsoluteNdx(tds, ndx);

    String dispStr = "";

    try
    {
      if(tds.getTotalRows() > 0)
      {
        Vector assignedTo = (Vector)pst.get("ASSIGNED_TO");
        dispStr = (String)assignedTo.elementAt(absNdx);
      }
    }
    catch(Exception e)
    {
      dispStr = "Cannot Determine!";
    }

    getCurrNDPage().setDisplayFieldValue(
      "Repeated1.stAssignedTo",
      new String(dispStr));
  }

  ////////////////////////////////////////////////////////////////////////
  //// This is a very important idiom. See the method adjusted above.
  private void populateSortOptions(ViewBean NDPage, PageCursorInfo tds)
  {
    ComboBox cbxSortOptions =
      (ComboBox)NDPage.getDisplayField("cbSortOptions");

    Option[] optionsOrig = cbxSortOptions.getOptions().toOptionArray();

    //_log.debug("IWQH@populateSortOptions::Size: " + optionsOrig.length); //// not populated yet.
    //// Manual population of the Sort Combobox.
    //--Release2.1--//
    //Modified to get the SortOrderOption Labels from Resource Bundle
    //--> By Billy 07Nov2002
    //int lim = Mc.IWQ_SORT_ORDER_OPTIONS.length;
    Collection theSList =
      BXResources.getPickListValuesAndDesc(this.theSessionState.getUserInstitutionId(),
      "IWORKQUEUESORTORDERS",
      this.theSessionState.getLanguageId());
    int lim = theSList.size();
    Iterator theList = theSList.iterator();
    String[] theVal = new String[2];

    //_log.debug("IWQH@populateSortOptions::Limit: " + lim);
    String[] labels = new String[lim];
    String[] values = new String[lim];

    for(int i = 0; i < lim; i++)
    {
      theVal = (String[])(theList.next());

      //labels[i] = Mc.IWQ_SORT_ORDER_OPTIONS[i];
      //values[i] = (new Integer(i)).toString();
      labels[i] = theVal[1];
      values[i] = theVal[0];
    }

    //=========== End changes for Release 2.1 ===========================================
    //// This is already done on the page itself.
    ////OptionList cbSortOptionsOptions=new OptionList(new String[]{},new String[]{});
    OptionList sortOption = pgIWorkQueueViewBean.cbSortOptionsOptions;

    sortOption.setOptions(labels, values);

    int selectedNdx = tds.getSortOption();

    //_log.debug("IWQH@populateSortOptions::SelectedIndex: " + selectedNdx);
    if((selectedNdx < 0) || (selectedNdx >= lim))
    {
      selectedNdx = 0;
    }

    cbxSortOptions.setValue((new Integer(selectedNdx)).toString(), true);

    //_log.debug("IWQH@populateSortOptions::ComboBox value: " + (cbxSortOptions.getValue()).toString());
    if(cbxSortOptions.getValue() != null)
    {
      String selectedValue = cbxSortOptions.getValue().toString();
      String selectedLabel =
        cbxSortOptions.getOptions().getValueLabel(selectedValue);

      //_log.debug("IWQH@populateSortOptions::selectedLabel: " + selectedLabel);
      //_log.debug("IWQH@populateSortOptions::selectedValue: " + selectedValue);
      NDPage.setDisplayFieldValue(
        "stSortOptionDesc",
        selectedLabel);

      //_log.debug("IWQH@populateSortOptions::StaticSortOptionDesc: " + NDPage.getDisplayFieldValue("stSortOptionDesc"));
    }
  }

  /////////////////////////////////////////////////////////////////////////////////
  // setSelectSQLTemplate
  // public void setSelectSQLTemplate(java.lang.String value)
  // Sets the SELECT query SQL program template used for SELECT queries. The template consists of a standard
  // SELECT SQL program but without a WHERE clause, which this class will construct dynamically.
  // The location at which to insert the WHERE clause in the SQL program should be specified by the token
  // returned by the getSelectSQLTemplateWhereToken() method (the default value is __WHERE__). For example:
  //	SELECT * FROM Foo __WHERE__
  //	SELECT * FROM Foo __WHERE__ ORDER BY Foo.BAR
  //
  // In order to perform SELECT queries using this class, the SELECT SQL template must be specified
  // (the reason for this is that unlike the other query types, the SELECT query program can be arbitrarily
  // complex, including joins from tables that are outside of the knowledge of this model).
  // Parameters:
  // value - The SELECT SQL program template
  ////////////////////////////////////////////////////////////////////////
  private void setIWorkQueueCriteria(doIWorkQueueModel wqDO, PageCursorInfo tds, PageEntry pg)
  {
      List<Integer> insts = theSessionState.institutionList();
      Iterator<Integer> it = insts.iterator();
      StringBuffer userBuffer = new StringBuffer();
      int size = insts.size(); 
      for (int i = 0; i < size; i++) {
          Integer institutionId = it.next();
          if (i > 0)
              userBuffer.append(" OR ");
          userBuffer.append("(ASSIGNEDTASKSWORKQUEUE.userProfileID = "
                  + theSessionState.getUserProfileId(institutionId)
                  + " AND ASSIGNEDTASKSWORKQUEUE.institutionProfileID = "
                  + institutionId  + ")");
      }
      if( size > 1) {
          userBuffer.insert(0, "(");
          userBuffer.append(")");
      }


      String userCriteria = userBuffer.toString();

      //_log.debug("PWQH@UserCriteria SQL query: " + userCriteria);
      // visibility criteria (visible or tickle)
      String visibleCriteria = "(ASSIGNEDTASKSWORKQUEUE.visibleFlag = 1 or ASSIGNEDTASKSWORKQUEUE.visibleFlag = 2)";

      //_log.debug("PWQH@Visible criteria SQL query: " + visibleCriteria);
      String basicCriteria = userCriteria + " AND " + visibleCriteria;

      //_log.debug("PWQH@BasicCriteria SQL query: " + basicCriteria);
      // priority tasks query
      doPAWorkQueueModel dobPAWorkQueue = (doPAWorkQueueModel)RequestManager.getRequestContext().getModelManager().getModel(doPAWorkQueueModel.class);

      ((doPAWorkQueueModelImpl)dobPAWorkQueue).clearUserWhereCriteria();

      ////((SelectQueryModel) dobPAWorkQueue).setDynamicCriteria(basicCriteria);
      ((doPAWorkQueueModelImpl)dobPAWorkQueue).setStaticWhereCriteriaString(doPAWorkQueueModelImpl.STATIC_WHERE_CRITERIA
              + " AND "
              + basicCriteria);

      // priority count query
      ////String priCountCriteria = "(ASSIGNEDTASKSWORKQUEUE.userProfileID (+) = " + getTheSession().getSessionUserId() + ")" + "AND (ASSIGNEDTASKSWORKQUEUE.visibleFlag (+) >= 1) AND (ASSIGNEDTASKSWORKQUEUE.visibleFlag (+) <= 2))";
      //String priCountCriteria = "(ASSIGNEDTASKSWORKQUEUE.userProfileID (+) = " + theSessionState.getSessionUserId() + ")"
      //+ "AND (ASSIGNEDTASKSWORKQUEUE.visibleFlag (+) >= 1) AND (ASSIGNEDTASKSWORKQUEUE.visibleFlag (+) <= 2))";

      //_log.debug("PWQH@PriCountCriteria SQL query: " + priCountCriteria);
      doPirorityCountModel theDoPC =(doPirorityCountModel)RequestManager.getRequestContext().getModelManager().getModel(doPirorityCountModel.class);

      ((doPirorityCountModelImpl)theDoPC).clearUserWhereCriteria();

      //----SCR#1148--start--23Mar2005--//
      // Vlad: Counter model should be enhanced with the filter criteria as well
      boolean isClientBMO = (PropertiesCache.getInstance().getInstanceProperty("com.basis100.fxp.clientid.bmo", "N")).equals("Y");

      _log.debug("IWQH@setIWorkQueueCriteria::Client is BMO?" + isClientBMO);

      if(isClientBMO)
      {
          setupBMOFilterForPriorityCounter(tds.getFilterOptionId(), theDoPC, basicCriteria);
      }
      else
      {
          // Vlad: Counter model should be enhanced with the filter criteria as well
          // for other clients
          setupFilterForPriorityCounter(tds.getFilterOptionId(), theDoPC, basicCriteria);
      }
      //----SCR#1148--end--23Mar2005--//

      // The basicOne work queue
      ((doIWorkQueueModelImpl)wqDO).clearUserWhereCriteria();

      String orderBy = getSortOrder(tds.getSortOption());
      _log.debug("PWQH@setIWorkQueueCriteria::StringOrderBy: " + orderBy);

      //-- ========== SCR#931 begins ========== --//
      //-- by Neil on Feb 08, 2005
      _log.debug("IWQH@setIWorkQueueCriteria::Client is BMO?" + isClientBMO);
      if(isClientBMO)
      {
          // Vlad: for BMO the tds.getFilterOptionId is diffierent from the value in db.
          // so here, using the setup based on selected value.
          _log.debug("IWQH@setIWorkQueueCriteria::filterOptionId = " + tds.getFilterOptionId());
          //----SCR#1148--start--23Mar2005--//
          // Vlad: the whole method is revisited.
          setupBMOFilterForMainModel(tds.getFilterOptionId(), wqDO, basicCriteria, orderBy);
          //----SCR#1148--end--23Mar2005--//
      }
      else
      {
          //----SCR#1148--start--23Mar2005--//
          // Vlad: Filter criteria must be explicetely setup now for non-BMO client as well.
          setupFilterForMainModel(tds.getFilterOptionId(), wqDO, basicCriteria, orderBy);
          //----SCR#1148--end--23Mar2005--//
      }
      //-- ========== SCR#931 ends ========== --//

      _log.debug(
              "PWQH@setIWorkQueueCriteria:: the doIWorkQueueModelImpl final SQL :: "
              + ((doIWorkQueueModelImpl)wqDO).getSelectSQL());

      // save criteria and count of records
      tds.setCriteria(basicCriteria + orderBy);

      // check for presence of priority tasks
      tds.setFilterOption(DISP_PRIORITY_TASKS, false);

      if (informPTask) {
    	  if(isPriorityTasks(dobPAWorkQueue, pg))
    	  {
    		  tds.setFilterOption(DISP_PRIORITY_TASKS, true);
    	  }
      }
  }

  //----SCR#1148--start--23Mar2005--//
  // Vlad: Main model is polished with the filter criteria as well.
  private void setupBMOFilterForMainModel(int filterOption, doIWorkQueueModel wqDO, String basicCriteria, String orderBy)
  {
    switch(filterOption)
    {
      case 0: // open
        _log.debug("IWQH@setupBMOFilterForMainModel::Filter Option = by [OPEN]");
        ((doIWorkQueueModelImpl)wqDO).setStaticWhereCriteriaString(
          doIWorkQueueModelImpl.STATIC_WHERE_CRITERIA + " AND " + basicCriteria // The basicOne
          + " AND "
          + setSchemaName("(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID = "
          + iWorkQueueHandler.BMO_TASKSTATUS_OPEN
          + ") ")
          // per BMO, open should include both open and on hold: left unitl their sign-off
          //+ " AND "
          //+ setSchemaName("(DEAL.HOLDREASONID = "
          //+ Mc.HOLD_REASON_NONE
          //+ ") ")
          + " ORDER BY "
          + orderBy);
        break;
      case 1: // complete
        _log.debug("IWQH@setupBMOFilterForMainModel::Filter Option = by [COMPLETE]");
        ((doIWorkQueueModelImpl)wqDO).setStaticWhereCriteriaString(
          doIWorkQueueModelImpl.STATIC_WHERE_CRITERIA + " AND " + basicCriteria // The basicOne
          + " AND "
          + setSchemaName("(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID = "
          + iWorkQueueHandler.BMO_TASKSTATUS_COMPLETE
          + ") "
          + " ORDER BY "
          + orderBy));
        break;

      case 2: // on hold
        _log.debug("IWQH@setupBMOFilterForMainModel::Filter Option = by [ON HOLD]");
        ((doIWorkQueueModelImpl)wqDO).setStaticWhereCriteriaString(
          doIWorkQueueModelImpl.STATIC_WHERE_CRITERIA + " AND " + basicCriteria // The basicOne
          + " AND "
          + setSchemaName("(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID = "
          + iWorkQueueHandler.BMO_TASKSTATUS_OPEN
          + ") ")
          + " AND "
          + setSchemaName("(DEAL.HOLDREASONID <> "
          + Mc.HOLD_REASON_NONE
          + ") ")
          + " ORDER BY "
          + orderBy);
        break;

      case 3: // all
        _log.debug("IWQH@setupBMOFilterForMainModel::Filter Option = by [ALL]");
        ((doIWorkQueueModelImpl)wqDO).setStaticWhereCriteriaString(
          doIWorkQueueModelImpl.STATIC_WHERE_CRITERIA + " AND " + basicCriteria // The basicOne
          + " order by " + orderBy); // order by ...
        break;

      default: // all
        _log.debug("IWQH@setupBMOFilterForMainModel::Filter Option = by [NONE]");
        ((doIWorkQueueModelImpl)wqDO).setStaticWhereCriteriaString(
          doIWorkQueueModelImpl.STATIC_WHERE_CRITERIA + " AND " + basicCriteria // The basicOne
          + " order by " + orderBy); // order by ...
        break;
    }
  }

  private void setupFilterForMainModel(int filterOption, doIWorkQueueModel wqDO, String basicCriteria, String orderBy)
  {
    switch(filterOption)
    {
        case 0: //unused
        _log.debug("IWQH@setupFilterForMainModel::Filter Option = by [UNUSED]");
        ((doIWorkQueueModelImpl)wqDO).setStaticWhereCriteriaString(
        doIWorkQueueModelImpl.STATIC_WHERE_CRITERIA + " AND " + basicCriteria // The basicOne
        + " order by " + orderBy); // order by ...
        break;

        case 1: // open
        _log.debug("IWQH@setupFilterForMainModel::Filter Option = by [OPEN]");
        ((doIWorkQueueModelImpl)wqDO).setStaticWhereCriteriaString(
          doIWorkQueueModelImpl.STATIC_WHERE_CRITERIA + " AND " + basicCriteria // The basicOne
          + " AND "
          + setSchemaName("(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID = "
          + Sc.TASK_OPEN
          + ") ")
          + " ORDER BY "
          + orderBy);
        break;

      case 2: // re-route
        _log.debug("IWQH@setupFilterForMainModel::Filter Option = by [RE-ROUTE]");
        ((doIWorkQueueModelImpl)wqDO).setStaticWhereCriteriaString(
          doIWorkQueueModelImpl.STATIC_WHERE_CRITERIA + " AND " + basicCriteria // The basicOne
          + " AND "
          + setSchemaName("(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID = "
          + Sc.TASK_REROUTE
          + ") ")
          + " ORDER BY "
          + orderBy);
        break;

      case 3: // complete
        _log.debug("IWQH@setupFilterForMainModel::Filter Option = by [COMPLETE]");
        ((doIWorkQueueModelImpl)wqDO).setStaticWhereCriteriaString(
          doIWorkQueueModelImpl.STATIC_WHERE_CRITERIA + " AND " + basicCriteria // The basicOne
          + " AND "
          + setSchemaName("(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID = "
          + Sc.TASK_COMPLETE
          + ") "
          + " ORDER BY "
          + orderBy));
        break;

      case 4: // cancelled
        _log.debug("IWQH@setupFilterForMainModel::Filter Option = by [CANCELLED]");
        ((doIWorkQueueModelImpl)wqDO).setStaticWhereCriteriaString(
          doIWorkQueueModelImpl.STATIC_WHERE_CRITERIA + " AND " + basicCriteria // The basicOne
          + " AND "
          + setSchemaName("(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID = "
          + Sc.TASK_REROUTE
          + ") ")
          + " ORDER BY "
          + orderBy);
        break;

      default: // open
        _log.debug("IWQH@setupFilterForMainModel::Filter Option = by [DEFAULT]");
        ((doIWorkQueueModelImpl)wqDO).setStaticWhereCriteriaString(
          doIWorkQueueModelImpl.STATIC_WHERE_CRITERIA + " AND " + basicCriteria // The basicOne
          + " AND "
          + setSchemaName("(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID = "
          + Sc.TASK_OPEN
          + ") ")
          + " ORDER BY "
          + orderBy);
        break;
    }
  }
  //----SCR#1148--end--23Mar2005--//

  //----SCR#1148--start--23Mar2005--//
  // Vlad: Counter model should be enhanced with the filter criteria as well
  private void setupBMOFilterForPriorityCounter(int filterOption, doPirorityCountModel theDoPC, String basicCriteria)
  {
    switch (filterOption)
    {
      case 0: // open
      _log.debug("IWQH@setupBMOFilterForPriorityCounter::Filter Option = by [OPEN]");
      String sqlStr = "AND ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID(+) = ";
      ((doPirorityCountModelImpl)theDoPC).setStaticWhereCriteriaString(
        doPirorityCountModelImpl.STATIC_WHERE_CRITERIA + sqlStr +
        iWorkQueueHandler.BMO_TASKSTATUS_OPEN +
        " AND " + basicCriteria // The basicOne
        + " AND "
        + setSchemaName("(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID = "
        + iWorkQueueHandler.BMO_TASKSTATUS_OPEN
        + ") "));
      break;
      case 1: // complete
      _log.debug("IWQH@setupBMOFilterForPriorityCounter::Filter Option = by [COMPLETE]");
      ((doPirorityCountModelImpl)theDoPC).setStaticWhereCriteriaString(
        doPirorityCountModelImpl.STATIC_WHERE_CRITERIA + " AND " + basicCriteria // The basicOne
        + " AND "
        + setSchemaName("(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID = "
        + iWorkQueueHandler.BMO_TASKSTATUS_COMPLETE
        + ") "));
      break;
      case 2: // on hold
      _log.debug("IWQH@setupBMOFilterForPriorityCounter::Filter Option = by [ON HOLD]");
      ((doPirorityCountModelImpl)theDoPC).setStaticWhereCriteriaString(
        doPirorityCountModelImpl.STATIC_WHERE_CRITERIA + " AND " + basicCriteria // The basicOne
        + " AND "
        + setSchemaName("(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID = "
        + iWorkQueueHandler.BMO_TASKSTATUS_OPEN
        + ") ")
        + " AND "
        + setSchemaName("(DEAL.HOLDREASONID <> "
        + Mc.HOLD_REASON_NONE
        + ") "));
      break;
      case 3: // all
      //--
      _log.debug("IWQH@setupBMOFilterForPriorityCounter::Filter Option = by [ALL]");
      ((doPirorityCountModelImpl)theDoPC).setStaticWhereCriteriaString(
        doPirorityCountModelImpl.STATIC_WHERE_CRITERIA + " AND " + basicCriteria // The basicOne
        );
      break;
    default: // all
      _log.debug("IWQH@setupBMOFilterForPriorityCounter::Filter Option = by [NONE]");
      ((doPirorityCountModelImpl)theDoPC).setStaticWhereCriteriaString(
        doPirorityCountModelImpl.STATIC_WHERE_CRITERIA + " AND " + basicCriteria // The basicOne
        );
    }
  }

  private void setupFilterForPriorityCounter(int filterOption, doPirorityCountModel theDoPC, String basicCriteria)
  {
    switch (filterOption)
    {
      case 0: //unused
      _log.debug("IWQH@setupFilterForMainModel::Filter Option = by [UNUSED]");
      ((doPirorityCountModelImpl)theDoPC).setStaticWhereCriteriaString(
      doPirorityCountModelImpl.STATIC_WHERE_CRITERIA + " AND " + basicCriteria); // The basicOne

      break;

      case 1: // open
      _log.debug("IWQH@setupFilterForPriorityCounter::Filter Option = by [OPEN]" + filterOption);
      String sqlStr = "AND ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID(+) = ";
      ((doPirorityCountModelImpl)theDoPC).setStaticWhereCriteriaString(
        doPirorityCountModelImpl.STATIC_WHERE_CRITERIA + sqlStr +
        iWorkQueueHandler.BMO_TASKSTATUS_OPEN +
        " AND " + basicCriteria // The basicOne
        + " AND "
        + setSchemaName("(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID = "
        + Sc.TASK_OPEN
        + ") "));
      break;

      case 2: // re-route
      _log.debug("IWQH@setupFilterForPriorityCounter::Filter Option = by [RE-ROUTE]" + filterOption);
      ((doPirorityCountModelImpl)theDoPC).setStaticWhereCriteriaString(
        doPirorityCountModelImpl.STATIC_WHERE_CRITERIA + " AND " + basicCriteria // The basicOne
        + " AND "
        + setSchemaName("(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID = "
        + Sc.TASK_REROUTE
        + ") "));
      break;

      case 3: // complete
      _log.debug("IWQH@setupFilterForPriorityCounter::Filter Option = by [COMPLETE]" + filterOption);
      ((doPirorityCountModelImpl)theDoPC).setStaticWhereCriteriaString(
        doPirorityCountModelImpl.STATIC_WHERE_CRITERIA + " AND " + basicCriteria // The basicOne
        + " AND "
        + setSchemaName("(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID = "
        + Sc.TASK_COMPLETE
        + ") "));
      break;

      case 4: // cancelled
      //--
      _log.debug("IWQH@setupFilterForPriorityCounter::Filter Option = by [CANCELLED]" + filterOption);
      sqlStr = "AND ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID(+) = ";
      ((doPirorityCountModelImpl)theDoPC).setStaticWhereCriteriaString(
        doPirorityCountModelImpl.STATIC_WHERE_CRITERIA + sqlStr +
        iWorkQueueHandler.BMO_TASKSTATUS_OPEN +
        " AND " + basicCriteria // The basicOne
        + " AND "
        + setSchemaName("(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID = "
        + Sc.TASK_CANCELLED
        + ") "));
      break;

      default: // open
        _log.debug("IWQH@setupFilterForPriorityCounter::Filter Option = by [DEFAULT]" + filterOption);
        sqlStr = "AND ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID(+) = ";
        ((doPirorityCountModelImpl)theDoPC).setStaticWhereCriteriaString(
          doPirorityCountModelImpl.STATIC_WHERE_CRITERIA + sqlStr +
          iWorkQueueHandler.BMO_TASKSTATUS_OPEN +
          " AND " + basicCriteria // The basicOne
          + " AND "
          + setSchemaName("(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID = "
          + Sc.TASK_OPEN
          + ") "));
        break;
    }
  }
  //----SCR#1148--end--23Mar2005--//

  private void setDealProgressCriteria(
    SelectQueryModel wqDO,
    PageCursorInfo tds,
    PageEntry pg)
  {
    // deal criteria
    String dealCriteria =
      "(ASSIGNEDTASKSWORKQUEUE.dealID = " + pg.getPageDealId() + ")";

    // visibility criteria (visible or tickle)
    String visibleCriteria =
      "(ASSIGNEDTASKSWORKQUEUE.visibleFlag = 1 or ASSIGNEDTASKSWORKQUEUE.visibleFlag = 2)";

    String basicCriteria = dealCriteria + " AND " + visibleCriteria;

    ((doIWorkQueueModelImpl)wqDO).clearUserWhereCriteria();
    ((doIWorkQueueModelImpl)wqDO).setStaticWhereCriteriaString(
      doIWorkQueueModelImpl.STATIC_WHERE_CRITERIA
      + " AND "
      + basicCriteria);

    // The basicOne
    // order by ...
    String orderBy =
      "ASSIGNEDTASKSWORKQUEUE.taskStatusId DESC, ASSIGNEDTASKSWORKQUEUE.endTimeStamp DESC";

    orderBy = setSchemaName(orderBy);

    ((doIWorkQueueModelImpl)wqDO).setStaticWhereCriteriaString(
      doIWorkQueueModelImpl.STATIC_WHERE_CRITERIA
      + " AND "
      + setSchemaName(orderBy));

    // order by ...
    // save criteria and count of records
    tds.setCriteria(basicCriteria + orderBy);

    // check for presence of priority tasks (never in this mode)
    tds.setFilterOption(DISP_PRIORITY_TASKS, false);
  }


  private boolean isPriorityTasks(doPAWorkQueueModel dobPAWorkQueue, PageEntry pg)
  {
    boolean haveSome = false;

    try
    {
      // Should execute the DataObject instead for checking
      //
      //CSpDBResult result = CSpDataObject.executeImmediate("orcl",
      //		"SELECT count(*) from ASSIGNEDTASKSWORKQUEUE where " +
      //		"ASSIGNEDTASKSWORKQUEUE.userProfileID = " + getTheSession().getSessionUserId() +
      //		" and ASSIGNEDTASKSWORKQUEUE.ACKNOWLEDGED <> 'Y'");
      //
      //haveSome =TypeConverter.asInt(result.getResultTable().getValue(0,0)) > 0;
      ////dobPAWorkQueue.execute();
      ////haveSome = dobPAWorkQueue.getNumRows() > 0;
      //_log.debug("PWQH@isPriorityTasks dobPAWorkQueue SQL : " + ((doPAWorkQueueModelImpl)dobPAWorkQueue).getSelectSQL());
      ResultSet rs = dobPAWorkQueue.executeSelect(null);

      if((rs != null) && (dobPAWorkQueue.getSize() > 0))
      {
        haveSome = true;

        //_log.debug("IWQH@isPriorityTasks::Size of the result set: " + dobPAWorkQueue.getSize());
      }
    }
    catch(Exception ex)
    {
      _log.error("IWQH@isPriorityTasks::Exception: ", ex);
    }

    ////if (haveSome) setupPAPrimaryBorrowerNames(dobPAWorkQueue, pg);
    if(haveSome)
    {
      setupPAPrimaryBorrowerNames(dobPAWorkQueue, pg);
    }

    return haveSome;
  }

  ////////////////////////////////////////////////////////////////////////
  ////private boolean setupPAPrimaryBorrowerNames(CSpDataObject dobPAWorkQueue, PageEntry pg)
  // add InstitutionIds to fix Wrong Borrower Name issue from DEMO (FXP23322) - 2008 11 04
  private boolean setupPAPrimaryBorrowerNames(
    doPAWorkQueueModel dobPAWorkQueue,
    PageEntry pg)
  {
    Hashtable pst = pg.getPageStateTable();

    Vector borrowerNames = new Vector();

    Vector copyIds = new Vector();

    Vector institutionIds = new Vector();

    pst.put("B_PA_NAMES", borrowerNames);

    try
    {
      //--> Not neeed to execute the Model again here. -- Billy 23July2002
      //ResultSet rs = dobPAWorkQueue.executeSelect(null);

      if(dobPAWorkQueue.getSize() > 0)
      {
        //_log.debug("IWQH@isPriorityTasks::Size of the result set: " + dobPAWorkQueue.getSize());
        String dealId = null;
        String copyId = null;
        String institutionId = null;

        while(dobPAWorkQueue.next())
        {
          Object oDealId =
            dobPAWorkQueue.getValue(
            doPAWorkQueueModel.FIELD_DFDEALID);
          Object oCopyId =
            dobPAWorkQueue.getValue(
            doPAWorkQueueModel.FIELD_DFCOPYID);
          Object oBorrowerLastName =
            dobPAWorkQueue.getValue(
            doPAWorkQueueModel.FIELD_DFCONTACTLASTNAME);
          
          // added for FXP23322
          Object oInstitutionId = 
              dobPAWorkQueue.getValue(
              doPAWorkQueueModel.FIELD_DFINSTITUTIONPROFILEID);

          //_log.debug("IWQHsetupPAPrimaryBorrowerNames@DealId: " + oDealId);
          //_log.debug("IWQHsetupPAPrimaryBorrowerNames@CopyId: " + oCopyId);
          //_log.debug("IWQHsetupPAPrimaryBorrowerNames@BorrowerLastName: " + oBorrowerLastName);
          dealId = cleanNumber(oDealId.toString());
          copyId = cleanNumber(oCopyId.toString());
          borrowerNames.add(new Integer(dealId));
          institutionId = cleanNumber(oInstitutionId.toString());

          // changed to borrower names below
          copyIds.add(new Integer(copyId));
          institutionIds.add(new Integer(institutionId));
        }
      }
    }
    catch(Exception ex)
    {
      _log.error("IWQH@isPriorityTasks::Exception: ", ex);
    }

    setupPrimaryBorrowerNameList(borrowerNames, copyIds, institutionIds);

    return true;
  }

  ////////////////////////////////////////////////////////////////////////
  // on input list contains user deal ids - on output these are changed to primary
  // borrower name strings (e.g. Lastname, FirstName)
  // add InstitutionIds to fix Wrong Borrower Name issue from DEMO (FXP23322) - 2008 11 04
  private void setupPrimaryBorrowerNameList(
    Vector borrowerNames,
    Vector copyIds,
    Vector institutionIds)
  {
    //_log.debug("IWQH@I am in setupPrimaryBorrowerNameLis");
    Hashtable borrowerNamesHashList = new Hashtable();

    int len = borrowerNames.size();

    //_log.debug("IWQH@setupPrimaryBorrowerNameList::HTsize: " + len);
    for(int i = 0; i < len; ++i)
    {
      Integer dealId = (Integer)borrowerNames.elementAt(i);
      Integer copyId = (Integer)copyIds.elementAt(i);
      Integer institutionId = (Integer)institutionIds.elementAt(i);

      //_log.debug("IWQH@setupPrimaryBorrowerNameList::DealId: " + dealId);
      //_log.debug("IWQH@setupPrimaryBorrowerNameList::CopyId: " + copyId);
      // check for information already obtained
      String nameStr = (String)borrowerNamesHashList.get(dealId);

      if(nameStr == null)
      {
        // not encountered yet
        //// Should not be converted to escape extra primitive data massaging.
        nameStr =
          queryPrimaryBorrowerName(
          dealId.intValue(),
          copyId.intValue(),
          institutionId.intValue());
        borrowerNamesHashList.put(dealId, nameStr);
      }

      borrowerNames.setElementAt(nameStr, i);
    }
  }

  ////////////////////////////////////////////////////////////////////////
  // on input list contains user profile ids - on output these are changed to contact
  // strings (e.g. Lastname, FirstName)
  // add InstitutionIds to fix Wrong Borrower Name issue from DEMO (FXP23322) - 2008 11 04
  private void setupAssignedUserList(Vector assignedTo, Vector institutionIds)
  {
    Hashtable assignedUserContactInfoList = new Hashtable();

    int len = assignedTo.size();

    for(int i = 0; i < len; ++i)
    {
        Integer profileId = (Integer)assignedTo.elementAt(i);
        Integer institutionId = (Integer)institutionIds.elementAt(i);
        String keyStr = profileId.toString() + "_" + institutionId.toString();
      // check for information already obtained
      String nameStr =
        (String)assignedUserContactInfoList.get(keyStr);

      if(nameStr == null)
      {
        // not encountered yet - obtain via Contact entity
        nameStr = "Unknown";

        try
        {
          Contact contact = new Contact(getSessionResourceKit());
          contact.findByUserProfileIdWithInstitutionId(profileId.intValue(), institutionId.intValue());
          nameStr = contact.getContactFullName();
        }
        catch(Exception e)
        {
          ;
        }

        assignedUserContactInfoList.put(keyStr, nameStr);
      }

      assignedTo.setElementAt(nameStr, i);
    }
  }

  private void setPageFlag(PageEntry pg)
  {
    //_log.debug("IWQH@setPageFlag: I am from setup PageFlag");
    isIWorkQueue = false;
    isDealProgress = false;

    //_log.debug("IWQH@setPageFlag::PageId: " + pg.getPageId());
    if(pg.getPageId() == Mc.PGNM_INDIV_WORK_QUEUE_ID)
    {
      isIWorkQueue = true;
    }
    else
    {
      isDealProgress = true;
    }

    //_log.debug("IWQH@setPageFlag::After setPageFlag");
  }

  ////////////////////////////////////////////////////////////////////////

  /*
     private int countWorkQueueDisplayRows(String criteria)
     {
         try
         {
             CSpDBResult result = CSpDataObject.executeImmediate("orcl",
     "SELECT count(*) from ASSIGNEDTASKSWORKQUEUE where " + criteria);

     returnTypeConverter.asInt(result.getResultTable().getValue(0,0));
            }
            catch (Exception e)
            {
     _log.error("Error encountered querying number of work queue records - ignoring error");
                _log.error("Query criteria = " + criteria);
            }

            return 0;
        }

   */

  ////////////////////////////////////////////////////////////////////////
  private void setDealForSnapshot(PageEntry pg, PageCursorInfo tds)
  {
    if(tds.getTotalRows() <= 0)
    {
      pg.setPageDealApplicationId("0");
      pg.setPageDealId(0);

      //_log.debug("IWQ@setDealForSnapshot::TotalNumberOfRows is zero");
      return;
    }

    try
    {
      Hashtable pst = pg.getPageStateTable();
      Vector dealIds = (Vector)pst.get("DEALIDS");
      Vector appIds = (Vector)pst.get("DEALAPPIDS");
      Vector copyIds = (Vector)pst.get("COPYIDS");
      Vector institutionIds = (Vector)pst.get("INSTITUTIONIDS");
      int ndx = tds.getAbsSelectedRowNdx();

      //_log.debug("IWQ@setDealForSnapshot::TotalNumberOfRows: " + tds.getTotalRows());
      //_log.debug("IWQ@setDealForSnapshot::RelIndex: " + tds.getRelSelectedRowNdx());
      //_log.debug("IWQ@setDealForSnapshot::CurrPageNdx: " + tds.getCurrPageNdx() + " RowsPerPage:" + tds.getRowsPerPage());
      //_log.debug("IWQ@setDealForSnapshot::AbsIndex: " + ndx);
      pg.setPageDealId(((Integer)dealIds.elementAt(ndx)).intValue());
      pg.setPageDealCID(((Integer)copyIds.elementAt(ndx)).intValue());
      pg.setPageDealApplicationId((String)appIds.elementAt(ndx));
      int institutionId = ((Integer)institutionIds.elementAt(ndx)).intValue();
      pg.setDealInstitutionId(institutionId);
      pg.setPageMosUserId(theSessionState.getUserProfileId(institutionId));
      pg.setPageMosUserTypeId(theSessionState.getUserTypeId(institutionId));
      
      _log.debug("IWQ@setDealForSnapshot::PageDealId: " +
                    ((Integer)dealIds.elementAt(ndx)).intValue() +
                    ", CopyId: " + ((Integer)copyIds.elementAt(ndx)).intValue() +
                    ", institutionId: " + institutionId);
    }
    catch(Exception e)
    {
      pg.setPageDealApplicationId("0");
      _log.error("Error setting deal id for summary snap shot");
      _log.error(e);
    }
  }

  ////////////////////////////////////////////////////////////////////////
  private void populateFilterOptions(ViewBean NDPage, PageCursorInfo tds)
  {
    //-- ========== SCR#931 begins ========== --//
    //-- by Neil on Feb 08, 2005
    boolean isClientBMO = (PropertiesCache.getInstance().getProperty(theSessionState.getUserInstitutionId(),"com.basis100.fxp.clientid.bmo", "N")).equals("Y");
    _log.debug("IWQH@populateFilterOptions::Client is BMO?" + isClientBMO);
    if(isClientBMO)
    {
      // BMO: generate filter options manually instead of getting from db.
      ComboBox cbxFilterOptions = (ComboBox)getCurrNDPage().getDisplayField("cbFilterOptions");
      OptionList filterOption = ((pgIWorkQueueViewBean)NDPage).cbFilterOptionsOptions;
      Option[] options = cbxFilterOptions.getOptions().toOptionArray();
      int selectedIdx = tds.getFilterOptionId();
      _log.debug("IWQH@populateFilterOptions::selectedIdx = " + selectedIdx);

      String[] labels = new String[Mc.IWQ_TASK_STATUS_OPTIONS_LENGTH];
      String[] values = new String[Mc.IWQ_TASK_STATUS_OPTIONS_LENGTH];

      String openLabel = BXResources.getGenericMsg("IWQ_TASK_STATUS_CBOPTIONS_OPEN_LABEL", theSessionState.getLanguageId());
      String completeLabel = BXResources.getGenericMsg("IWQ_TASK_STATUS_CBOPTIONS_COMPLETE_LABEL",
        theSessionState.getLanguageId());
      String onholdLabel = BXResources.getGenericMsg("IWQ_TASK_STATUS_CBOPTIONS_ONHOLD_LABEL", theSessionState.getLanguageId());
      String allLabel = BXResources.getGenericMsg("IWQ_TASK_STATUS_CBOPTIONS_ALL_LABEL", theSessionState.getLanguageId());

      String[] IWQ_TASK_STATUS_OPTIONS =
        {openLabel, completeLabel, onholdLabel, allLabel};

      for(int i = 0; i < Mc.IWQ_TASK_STATUS_OPTIONS_LENGTH; i++)
      {
        labels[i] = IWQ_TASK_STATUS_OPTIONS[i];
        values[i] = new Integer(i).toString();
      }

      filterOption.setOptions(labels, values);

      if((selectedIdx < 0) || (selectedIdx >= Mc.IWQ_TASK_STATUS_OPTIONS_LENGTH))
      {
        selectedIdx = 0;
      }
      cbxFilterOptions.setValue(new Integer(selectedIdx).toString(), true);

      if(cbxFilterOptions.getValue() != null)
      {
        String selectedValue = cbxFilterOptions.getValue().toString();
        String selectedLabel = cbxFilterOptions.getOptions().getValueLabel(selectedValue);
        _log.debug("IWQH@populateFilterOptions::selectedLabel = " + selectedLabel);
        _log.debug("IWQH@populateFilterOptions::selectedValue = " + selectedValue);
        NDPage.setDisplayFieldValue("stFilterOptionDesc", selectedLabel);
      }
    }
    else
    {
      // for other clients, keep unchanged.

      int selectedNdx = tds.getFilterOptionId();

      ComboBox cbxFilterOptions =
        (ComboBox)getCurrNDPage().getDisplayField("cbFilterOptions");

      OptionList filterOption =
        ((pgIWorkQueueViewBean)NDPage).cbFilterOptionsOptions;

      PageEntry currNDPage = theSessionState.getCurrentPage();

      String filterOpt =
        NDPage.getDisplayFieldValue("cbFilterOptions").toString();

      String selectedValue = "";
      String selectedLabel = "";

      //--> Set the selected option of the ComboBox
      cbxFilterOptions.setValue((new Integer(selectedNdx)).toString(), true);

      if(cbxFilterOptions.getValue() == null)
      {
        // Wrong option value -- Set to default = 1 (OPEN)
        cbxFilterOptions.setValue("1", true);
        selectedValue = cbxFilterOptions.getValue().toString();
        selectedLabel =
          cbxFilterOptions.getOptions().getValueLabel(selectedValue);

        //_log.debug("IWQH@populateSortOptions:cbxFilterOptions.getValue()== null ==> default to Open :selectedValue=" + selectedValue
        //    + " :: selectedLabel=" + selectedLabel );
      }
      else
      {
        selectedValue = cbxFilterOptions.getValue().toString();
        selectedLabel =
          cbxFilterOptions.getOptions().getValueLabel(selectedValue);

        //_log.debug("IWQH@populateSortOptions:cbxFilterOptions.getValue()!= null ==> default to Open :selectedValue=" + selectedValue
        //    + " :: selectedLabel=" + selectedLabel );
      }

      NDPage.setDisplayFieldValue("stFilterOptionDesc", selectedLabel);
    }
    //-- ========== SCR#931   ends ========== --//

  }

  ////////////////////////////////////////////////////////////////////////
  private void populateRowsDisplayed(ViewBean NDPage, PageCursorInfo tds)
  {
    String rowsRpt = "";

    //--Release2.1--start//
    String dispTasksLbl =
      BXResources.getGenericMsg(
      "DISPLAYING_TASKS_LABEL",
      theSessionState.getLanguageId());
    String ofLbl =
      BXResources.getGenericMsg(
      "OF_LABEL",
      theSessionState.getLanguageId());

    if(tds.getTotalRows() > 0)
    {
      rowsRpt =
        dispTasksLbl
        + " "
        + tds.getFirstDisplayRowNumber()
        + "-"
        + tds.getLastDisplayRowNumber()
        + " "
        + ofLbl
        + " "
        + tds.getTotalRows();
    }

    //--Release2.1--end//
    NDPage.setDisplayFieldValue("rowsDisplayed", new String(rowsRpt));
  }

  // If priority tasks have been displayed set the acknowledged flag in work queu
  // records.
  ////////////////////////////////////////////////////////////////////////
  private String getSortOrder(int sortType)
  {
    //--> Ticket#139 : BMO - Ability to prioritize Broker (SOB)
    //--> By Billy 28Nov2003
    // Added sort by SOB Priority

    //-- ========== SCR#931 begins ========== --//
    //-- by Neil on Feb 10, 2005
    //----SCR#1147--start--23Mar2005--//
    // Vlad: SOB Priority business must be returned for BMO!!
    String priorityDueDate =
    	"ASSIGNEDTASKSWORKQUEUE.priorityId, SOURCEOFBUSINESSPROFILE.sourceOfBusinessPriorityId DESC, ASSIGNEDTASKSWORKQUEUE.dueTimeStamp";
    //String priorityDueDate =
    //  "ASSIGNEDTASKSWORKQUEUE.priorityId, ASSIGNEDTASKSWORKQUEUE.dueTimeStamp ,SOURCEOFBUSINESSPROFILE.sourceOfBusinessPriorityId DESC ";
    //-- ========== SCR#931   ends ========== --//
    //----SCR#1147--end-23Mar2005--//
    //========================================================
    String sql = null;

    switch(sortType)
    {
      case Mc.IWQ_SORT_ORDER_PRIORITY:
        sql = priorityDueDate;
        break;

      case Mc.IWQ_SORT_DUE_DATE:
        sql =
          "ASSIGNEDTASKSWORKQUEUE.dueTimeStamp, ASSIGNEDTASKSWORKQUEUE.priorityId, SOURCEOFBUSINESSPROFILE.sourceOfBusinessPriorityId DESC";
        break;

      case Mc.IWQ_SORT_SOURCE:
        sql = "CONTACT.contactLastName, " + priorityDueDate;
        break;

      case Mc.IWQ_SORT_SOURCE_FIRM:
        sql = "SOURCEFIRMPROFILE.SFSHORTNAME, " + priorityDueDate;
        break;

      case Mc.IWQ_SORT_SOURCE_BORROWER:
        sql = "BORROWER.borrowerLastName, " + priorityDueDate;
        break;

      case Mc.IWQ_SORT_DEAL_NUMBER_ASC:
        sql = "ASSIGNEDTASKSWORKQUEUE.dealId ASC, " + priorityDueDate;
        break;

      case Mc.IWQ_SORT_DEAL_NUMBER_DEC:
        sql =
          "ASSIGNEDTASKSWORKQUEUE.dealId DESC, " + priorityDueDate;
        break;

      case Mc.IWQ_SORT_TASK_DESC:
        sql = "ASSIGNEDTASKSWORKQUEUE.tasklabel, " + priorityDueDate;
        break;

      case Mc.IWQ_SORT_TASK_STATUS:
        sql =
          "ASSIGNEDTASKSWORKQUEUE.taskStatusID, " + priorityDueDate;
        break;

      case Mc.IWQ_SORT_TIME_WARNING:
        sql =
          "ASSIGNEDTASKSWORKQUEUE.warningtime, " + priorityDueDate;
        break;

      case Mc.IWQ_SORT_WARNING:

        // ALERT :: like Mc.IWQ_SORT_TIME_WARNING for now!
        //sql = "TIMEMILESTONE.TMDESCRIPTION, " + priorityDueDate;
          sql = "ASSIGNEDTASKSWORKQUEUE.timemilestoneid, " + priorityDueDate;
        break;

        //--> Ticket#139 : BMO - Ability to prioritize Broker (SOB)
        //--> By Billy 28Nov2003
        // new sort type by SOB Priority
      case Mc.IWQ_SORT_SOURCE_PRIORITY:
        sql =
          "SOURCEOFBUSINESSPROFILE.sourceOfBusinessPriorityId DESC, ASSIGNEDTASKSWORKQUEUE.priorityId, ASSIGNEDTASKSWORKQUEUE.dueTimeStamp";
        break;

        //-- ========== SCR#750 begins ========== --//
        //-- by Neil on Dec/20/2004
      case Mc.IWQ_SORT_DEAL_STATUS:

        //-- ========== SCR#885 begins ========= --//
        //-- by Neil on Jan/24/2005
        sql =
          "DEAL.STATUSID, "
          + "ASSIGNEDTASKSWORKQUEUE.dueTimeStamp, ASSIGNEDTASKSWORKQUEUE.priorityId, SOURCEOFBUSINESSPROFILE.sourceOfBusinessPriorityId DESC";

        //sql = "STATUS.STATUSDESCRIPTION, " + priorityDueDate;
        //-- ========== SCR#885   ends ========= --//
        break;
        //-- ========== SCR#750   ends ========== --//
      default:
        sql = priorityDueDate;
        break;
    }

    _log.debug("IWH@getSortOrder::sortTypeIn: " + sortType);
    _log.debug("IWH@getSortOrder::SQL : " + sql);
    _log.debug("IWH@getSortOrder::priorityDueDate: " + priorityDueDate);

    return setSchemaName(sql);
  }

  //// Actually, this method should (?) override the JATO framework synchronization
  //// method for the TiledView objects.
  //--> Moeified to sync it in any case.
  public void checkAndSyncDOWithCursorInfo(RequestHandlingTiledViewBase theRepeat)
  {
    PageEntry pg = (PageEntry)theSessionState.getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    PageCursorInfo tds = (PageCursorInfo)pst.get("DEFAULT");

    try
    {
      //--> So far we cannot find a way to manually set the Diaplay Offset of the TiledView here.
      //--> The reason is some necessary method is protected (e.g. setWebActionModelOffset()). The
      //--> work around is to create our own method (setCurrentDisplayOffset) in the TiledView class
      //--> (e.g. pgIWorkQueueRepeated1TiledView).  However, this method have to be implemented in all
      //--> TiledView which have Forward/Backward buttons.
      //--> Need to investigate more later  !!!!
      //--> By BILLY 22July2002
      (
        (
        pgIWorkQueueRepeated1TiledView)theRepeat)
        .setCurrentDisplayOffset(
        tds.getFirstDisplayRowNumber() - 1);
    }
    catch(Exception e)
    {
      _log.error(
        "IWQH@checkAndSyncDOWithCursorInfo::Exception: ", e);
    }
  }

  //-- ========== SCR#750 begins ============================================ --//
  //-- by Neil on Dec/17/2004

  /**
   * According to SCR#750, both <i>TaskStatus</i> and <i>HoldReason</i> should share the same
   * TextField component for displaying. Usually, the TextField is used to display TaskStatus.
   * When the DEAL.HOLDREASONID is not <i>0</i>,  however, it will be used to display
   * HoldReason description. This method changes the binding for that TextField to HoldReason
   * instead of TaskStatus.
   *
   * @throws Exception
   */
  protected void setTaskStatusWithHoldReason() throws Exception
  {
    doIWorkQueueModel doIWQ =
      (doIWorkQueueModel)(RequestManager
      .getRequestContext()
      .getModelManager()
      .getModel(doIWorkQueueModel.class));
    String holdReason = null;
    int holdReasonId = 0;
    String taskStatus = null;
    BigDecimal instId = doIWQ.getDfInstitutionId();
    try
    {
      // do not need to check if rs==null
      // because the result is the existing one not new.
      // if (rs != null && doIWQ.getSize() > 0) {
      if(doIWQ.getSize() > 0)
      {
        Object dfHoldReasonValue =
          doIWQ.getValue(
          doIWorkQueueModelImpl.FIELD_DFHOLDREASONDESCRIPTION);
        Object dfHoldReasonIdValue =
          doIWQ.getValue(doIWorkQueueModelImpl.FIELD_DFHOLDREASONID);
        Object dfTaskStatusValue =
          doIWQ.getValue(
          doIWorkQueueModelImpl.FIELD_DFTASKSTATUS_TSDESCRIPTION);

        Object dfDealIdValue =
          doIWQ.getValue(doIWorkQueueModelImpl.FIELD_DFDEALID);

        if(dfHoldReasonValue == null)
        {
          holdReason = null;
        }
        else
        {
          holdReason = dfHoldReasonValue.toString();
        }

        if(dfHoldReasonIdValue == null)
        {
          String strErr =
            "@iWorkQueueHandler.setTaskStatusWithHoldReason(): "
            + "Invalide HOLDREASONID found.";
          throw new ModelControlException(strErr);
        }
        else
        {
          holdReasonId =
            new Integer(dfHoldReasonIdValue.toString()).intValue();
        }

        if(dfTaskStatusValue == null)
        {
          String strErr =
            "@iWorkQueueHandler.setTaskStatusWithHoldReason(): "
            + "Invalide TASKSTATUS found.";
          throw new ModelControlException(strErr);
        }
        else
        {
          taskStatus = dfTaskStatusValue.toString();
        }
      }
      else
      {
        // SEAN Ticket #1979 Sept 16, 2005: change from error level to info level.
        _log.info(
          "@iWorkQueueHandler.setTaskStatusWithHoldReason(): There is no task for this user.");
		// SEAN Ticket #1979 END
      }

      //			_log.debug(
      //				"@iWorkQueueHandler.setTaskStatusWithHoldReason(): holdReason=" + holdReason);
      //			if (rs == null) {
      //				_log.debug(
      //					"@iWorkQueueHandler.setTaskStatusWithHoldReason(): resultSet is null");
      //			} else {
      //				_log.debug(
      //					"@iWorkQueueHandler.setTaskStatusWithHoldReason(): resultSet is NOT null");
      //			}
      //			int i = doIWQ.getSize();
      //			_log.debug("@iWorkQueueHandler.setTaskStatusWithHoldReason(): getSize()=" + i);
    }
    catch(ModelControlException mce)
    {
      String strError =
        "@iWorkQueueHandler.setTaskStatusWithHoldReason(): "
        + mce.getMessage();
      _log.error(mce);
      _log.error(strError);
      throw new Exception(strError);

      //		} catch (SQLException sqle) {
      //			String strError =
      //				"@iWorkQueueHandler.setTaskStatusWithHoldReason(): "
      //					+ sqle.getMessage();
      //			_log.error(sqle);
      //			_log.error(strError);
      //			throw new Exception(strError);
    }

    _log.debug(
      "@iWorkQueueHandler.setTaskStatusWithHoldReason(): holdReason="
      + holdReason);

    //-- just for debuging
    if((holdReasonId == 0) && (holdReason != null))
    {
      _log.debug(
        "NEIL@iWorkQueueHandler.setTaskStatusWithHoldReason: "
        + "holdReasonId = "
        + holdReasonId
        + " BUT holdReason = "
        + holdReason);
    }

    // set TaskStatus with HoldReason if holdReasonId is not 0.
    if((holdReasonId == 0) || (holdReason == null))
    {
      // display with TaskStatus
      getCurrNDPage().setDisplayFieldValue(
        "Repeated1/stTaskStatus",
        taskStatus);

      // keep the default style.
      getCurrNDPage().setDisplayFieldValue(
        "Repeated1/taskStatusOrHoldReasonStyle",
        "");
    }
    else
    {
      /***** GR 3.2.2 On Hold Functionality start *****/
      long currentTime = System.currentTimeMillis();
      Timestamp dfDueTime = (Timestamp)doIWQ
                      .getValue(doIWorkQueueModelImpl.FIELD_DFDUETIMESTAMP);
      long dueTime = dfDueTime.getTime();
      //add instId for MCM
      if (!onHoldNotDisplay(instId.intValue()) || dueTime > currentTime ) {
          // display with HoldReason
          String strOnHold =
            BXResources.getGenericMsg(
            "ON_HOLD_LABEL",
            theSessionState.getLanguageId());
          getCurrNDPage().setDisplayFieldValue(
                  "Repeated1/stTaskStatus",
                  strOnHold + " / " + holdReason);
          // change style.
          getCurrNDPage().setDisplayFieldValue(
            "Repeated1/taskStatusOrHoldReasonStyle",
            "color: #FF0000;");
      }
      /***** GR 3.2.2 On Hold Functionality end *****/
   }
  }

  //-- ========== SCR#750   ends ============================================ --//

  
    /**
     * Instituion's property setting for On Hold message display
     * GR 3.2.2 On Hold Functionality
     * @return boolean: true not display, false display
     */
  protected boolean onHoldNotDisplay(int instId) {
      boolean notDisplay = false;
      PageEntry pg = (PageEntry)theSessionState.getCurrentPage();
      String property = PropertiesCache.getInstance().getProperty(instId,
              "deal.onhold.stop.message.display.when.task.expires", "N");
      if (property.equalsIgnoreCase("Y"))
      	notDisplay = true;
      return notDisplay;
  }
}
