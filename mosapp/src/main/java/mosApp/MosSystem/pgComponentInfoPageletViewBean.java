package mosApp.MosSystem;

import java.util.ArrayList;
import java.util.List;

import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.ContainerView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.ViewBeanBase;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.StaticTextField;

/**
 * Description: pgComponentInfoPageletViewBean class is used to display data 
 * from the doComponentInfoModel Model.
 * 
 * @author: MCM Impl Team.
 * @version 1.0 2008-6-11
 *
 * @version 1.1 17-July-2008 XS_2.8 Added tiles for each components
 * @version 1.2 22-July-2008 XS_2.8 modified for multiple parent handler
 */

public class pgComponentInfoPageletViewBean extends ViewBeanBase implements
        ContainerView {

    // Class Variable to add tile for Party Details Information
    protected String DEFAULT_DISPLAY_URL = "/mosApp/MosSystem/pgComponentInfoPagelet.jsp";

    public static final String PAGE_NAME = "pgComponentInfoPagelet";

    public static final String CHILD_BTCOMPONENTDETAIL = "btComponentDetail";

    public static final String CHILD_BTCOMPONENTDETAIL_RESET_VALUE = "";

    public static final String CHILD_STTOTALAMOUNT = "stTotalAmount";

    public static final String CHILD_STTOTALAMOUNT_RESET_VALUE = "";

    public static final String CHILD_STUNALLOCATEDAMOUNT = "stUnallocatedAmount";

    public static final String CHILD_STUNALLOCATEDAMOUNT_RESET_VALUE = "";

    public static final String CHILD_STTOTALCASHBACKAMOUNT = "stCashbackAmount";

    public static final String CHILD_STTOTALCASHBACKAMOUNT_RESET_VALUE = "";

    // MCM Team added for XS 2.8 STARTS
    public static final String CHILD_TILEDMTGCOMP = "TiledMTGComp";
    public static final String CHILD_TILEDMTGCOMP_RESET_VALUE = "";
    public static final String CHILD_TILEDLOCCOMP = "TiledLOCComp";
    public static final String CHILD_TILEDLOCCOMP_RESET_VALUE = "";
    public static final String CHILD_TILEDLOANCOMP = "TiledLoanComp";
    public static final String CHILD_TILEDLOANCOMP_RESET_VALUE = "";
    public static final String CHILD_TILEDCCCOMP = "TiledCCComp";
    public static final String CHILD_TILEDCCCOMP_RESET_VALUE = "";
    public static final String CHILD_TILEDODCOMP = "TiledODComp";
    public static final String CHILD_TILEDODCOMP_RESET_VALUE = "";
    // MCM Team added for XS 2.8 ENDS

    private ComponentInfoHandler handler = new ComponentInfoHandler();
    
    // MCM Team added for 2.44 STARTS
    private DealHandlerCommon parentHandler = null;
    // MCM Team added for 2.44 ENDS

    private doComponentInfoModel doComponentInfoModelType = null;

    /**
     * Description: This Constructor registers the Childs and sets the default
     * URL.
     * 
     * @parem DealHandlerCommon parentHandler
     * 
     * @version 1.2 july 22, 2008 added DealHandlerCommon as the 3rd parameter
     */
    public pgComponentInfoPageletViewBean
        (View parent, String name, DealHandlerCommon parentHandler) {
        super(parent, name);
        this.parentHandler = parentHandler;
        registerChildren();
        initialize();
    }

    /**
     * Description: This method currently has no implementation but can be used
     * to do initializations
     * 
     */
    protected void initialize() {
    }

    /**
     * Description: This method registers the children to the framework.
     * 
     * @version 1.1 added component tiled view
     * 
     */
    protected void registerChildren() {
        registerChild(CHILD_BTCOMPONENTDETAIL, Button.class);
        registerChild(CHILD_STTOTALAMOUNT, StaticTextField.class);
        registerChild(CHILD_STUNALLOCATEDAMOUNT, StaticTextField.class);
        registerChild(CHILD_STTOTALCASHBACKAMOUNT, StaticTextField.class);
        
        // MCM Team added for XS 2.8 STARTS
        registerChild(CHILD_TILEDMTGCOMP, pgCompInfoMTGTiledView.class);
        registerChild(CHILD_TILEDLOCCOMP, pgCompInfoLOCTiledView.class);
        registerChild(CHILD_TILEDLOANCOMP, pgCompInfoLoanTiledView.class);
        registerChild(CHILD_TILEDCCCOMP, pgCompInfoCreditCardTiledView.class);
        registerChild(CHILD_TILEDODCOMP, pgCompInfoOverdraftTiledView.class);
        // MCM Team added for XS 2.8 EBDS
    }

    /**
     * Description: This method resets children.
     * 
     */
    public void resetChildren() {
        getBtComponentDetail().setValue(CHILD_BTCOMPONENTDETAIL_RESET_VALUE);
        getStTotalAmount().setValue(CHILD_STTOTALAMOUNT_RESET_VALUE);
        getStUnallocatedAmount()
                .setValue(CHILD_STUNALLOCATEDAMOUNT_RESET_VALUE);
        getStTotalCashBackAmount().setValue(
                CHILD_STTOTALCASHBACKAMOUNT_RESET_VALUE);
        // MCM Team added for XS 2.8 STARTS
        getTiledMTGComp().resetChildren();
        getTiledLOCComp().resetChildren();
        getTiledLoanComp().resetChildren();
        getTiledCCComp().resetChildren();
        getTiledODComp().resetChildren();
        // MCM Team added for XS 2.8 ENDS
    }

    /**
     * Description: This method adds the model to the Model List.
     * 
     * @param executionType
     */
    public Model[] getWebActionModels(int executionType) {
        List modelList = new ArrayList();
        switch (executionType) {
        case MODEL_TYPE_RETRIEVE:
            modelList.add(getdoComponentInfoModel());
            break;

        case MODEL_TYPE_UPDATE:
            ;
            break;

        case MODEL_TYPE_DELETE:
            ;
            break;

        case MODEL_TYPE_INSERT:
            ;
            break;

        case MODEL_TYPE_EXECUTE:
            ;
            break;
        }
        return (Model[]) modelList.toArray(new Model[0]);
    }

    /**
     * Description: This method creates the handler objects and saves the page
     * state and resets the Tile index
     * 
     * @param event
     */
    public void beginDisplay(DisplayEvent event) throws ModelControlException {        
        ComponentInfoHandler handler = (ComponentInfoHandler) this.handler
            .cloneSS();
        
        handler.pageGetState(this.getParentViewBean());
        handler.setupBeforePageGeneration();
        super.beginDisplay(event);
        handler.pageSaveState();
    }

    /**
     * Description: This method does necessary preparation to the page before
     * model executes.
     * 
     * @param model
     * @param executionContext
     */
    public boolean beforeModelExecutes(Model model, int executionContext) {
        return super.beforeModelExecutes(model, executionContext);
    }

    /**
     * Description: This method call the afterModelExecutes method of the super
     * class.
     * 
     * @param model 
     * @param executionContext 
     * 
     * @version 1.2 July 22, 2008 moved handler method to afterAllModelsExecute
     */
    public void afterModelExecutes(Model model, int executionContext) {
        super.afterModelExecutes(model, executionContext);
    }

    /**
     * Description: This method displays the fields on the page after executing
     * the model.
     * 
     * @param executionContext
     * 
     * @version 1.2 July 22, 2008 moved handler method from afterModelExecutes
     */
    public void afterAllModelsExecute(int executionContext) {
        super.afterAllModelsExecute(executionContext);
        
        ComponentInfoHandler handler =(ComponentInfoHandler) this.handler.cloneSS();
        handler.pageGetState(this.getParentViewBean());
        handler.populatePageDisplayFields();
        handler.pageSaveState();
    }

    /**
     * Description: This method calls the onModelError method of the super
     * class.
     * 
     * @param model
     * @param executionContext
     * @param exception
     */
    public void onModelError(Model model, int executionContext,
            ModelControlException exception) throws ModelControlException {
        super.onModelError(model, executionContext, exception);
    }

    /**
     * Description: This method creates the Children.
     * 
     * @param name
     */
    protected View createChild(String name) {
        if (name.equals(CHILD_BTCOMPONENTDETAIL)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTCOMPONENTDETAIL, CHILD_BTCOMPONENTDETAIL,
                    CHILD_BTCOMPONENTDETAIL_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_STTOTALAMOUNT)) {
            StaticTextField child = new StaticTextField(this,
                    getdoComponentInfoModel(), CHILD_STTOTALAMOUNT,
                    doComponentInfoModel.FIELD_DFTOTALAMOUNT,
                    CHILD_STTOTALAMOUNT_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_STUNALLOCATEDAMOUNT)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STUNALLOCATEDAMOUNT,
                    CHILD_STUNALLOCATEDAMOUNT,
                    CHILD_STUNALLOCATEDAMOUNT_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_STTOTALCASHBACKAMOUNT)) {
            StaticTextField child = new StaticTextField(this,
                    getdoComponentInfoModel(), CHILD_STTOTALCASHBACKAMOUNT,
                    doComponentInfoModel.FIELD_DFTOTALCASHBACKAMOUNT,
                    CHILD_STTOTALCASHBACKAMOUNT_RESET_VALUE, null);
            return child;
            // MCM Team added for XS 2.8 STARTS
        } else if (name.equals(CHILD_TILEDMTGCOMP)) {
            pgCompInfoMTGTiledView child = new pgCompInfoMTGTiledView(
                    this, CHILD_TILEDMTGCOMP);
            return child;
        } else if (name.equals(CHILD_TILEDLOCCOMP)) {
            pgCompInfoLOCTiledView child = new pgCompInfoLOCTiledView(
                    this, CHILD_TILEDLOCCOMP);
            return child;
        } else if (name.equals(CHILD_TILEDLOANCOMP)) {
            pgCompInfoLoanTiledView child = new pgCompInfoLoanTiledView(
                    this, CHILD_TILEDLOANCOMP);
            return child;
        } else if (name.equals(CHILD_TILEDCCCOMP)) {
            pgCompInfoCreditCardTiledView child = new pgCompInfoCreditCardTiledView(
                    this, CHILD_TILEDCCCOMP);
            return child;
        } else if (name.equals(CHILD_TILEDODCOMP)) {
            pgCompInfoOverdraftTiledView child = new pgCompInfoOverdraftTiledView(
                    this, CHILD_TILEDODCOMP);
            return child;
            // MCM Team added for XS 2.8 ENDS
        } else {
            throw new IllegalArgumentException("Invalid child name [" + name
                    + "]");
        }
    }

    /**
     * Description: Transfers the control to the button.
     * 
     * @param event
     * 
     * @version 1.2 july 22, 2008 Modified for multipile parents (XS 2.44)
     * 
     */
    public void handleBtComponentDetailRequest(RequestInvocationEvent event) {
        
        ComponentInfoHandler handler = (ComponentInfoHandler)this.handler.cloneSS();
        ViewBean parentView = (pgComponentInfoPageletViewBean) this.getParentViewBean();
        ViewBean superParent = (ViewBean) parentView.getParent();
        
        handler.preHandlerProtocol(superParent);

        PageEntry currPg = (PageEntry) handler.getTheSessionState().getCurrentPage();
        parentHandler.pageGetState(superParent);
        parentHandler.saveData(currPg, false);

        handler.handleComponentDetail();
        handler.postHandlerProtocol();
    }

    /**
     * Description: Returns the Button object
     * 
     * @return botton object.
     */
    public Button getBtComponentDetail() {
        return (Button) getChild(CHILD_BTCOMPONENTDETAIL);
    }

    /**
     * Description: Returns the StaticTextField object
     * @return StaticTextField
     */
    public StaticTextField getStTotalAmount() {
        return (StaticTextField) getChild(CHILD_STTOTALAMOUNT);
    }

    /**
     * Description: Returns the StaticTextField object
     * @return StaticTextField
     */
    public StaticTextField getStUnallocatedAmount() {
        return (StaticTextField) getChild(CHILD_STUNALLOCATEDAMOUNT);
    }

    /**
     * Description: Returns the StaticTextField object
     * @return StaticTextField
     */
    public StaticTextField getStTotalCashBackAmount() {
        return (StaticTextField) getChild(CHILD_STTOTALCASHBACKAMOUNT);
    }

    // MCM Team added for XS 2.8 STARTS
    /**
     * Description: Returns the pgCompInfoMTGTiledView object
     * @return pgCompInfoMTGTiledView
     */
    public pgCompInfoMTGTiledView getTiledMTGComp() {
        return (pgCompInfoMTGTiledView) getChild(CHILD_TILEDMTGCOMP);
    }

    /**
     * Description: Returns the pgCompInfoLOCTiledView object
     * @return pgCompInfoLOCTiledView
     */
    public pgCompInfoLOCTiledView getTiledLOCComp() {
        return (pgCompInfoLOCTiledView) getChild(CHILD_TILEDLOCCOMP);
    }

    /**
     * Description: Returns the pgCompInfoLoanTiledView object
     * @return pgCompInfoLoanTiledView
     */
    public pgCompInfoLoanTiledView getTiledLoanComp() {
        return (pgCompInfoLoanTiledView) getChild(CHILD_TILEDLOANCOMP);
    }

    /**
     * Description: Returns the pgCompInfoCreditCardTiledView object
     * @return pgCompInfoCreditCardTiledView
     */
    public pgCompInfoCreditCardTiledView getTiledCCComp() {
        return (pgCompInfoCreditCardTiledView) getChild(CHILD_TILEDCCCOMP);
    }

    /**
     * Description: Returns the pgCompInfoOverdraftTiledView object
     * @return pgCompInfoOverdraftTiledView
     */
    public pgCompInfoOverdraftTiledView getTiledODComp() {
        return (pgCompInfoOverdraftTiledView) getChild(CHILD_TILEDODCOMP);
    }
    // MCM Team added for XS 2.8 ENDS

    /**
     * Description: This method creates the model.
     *
     * @return
     */
    public doComponentInfoModel getdoComponentInfoModel() {

        if (doComponentInfoModelType == null)
            doComponentInfoModelType = (doComponentInfoModel) getModel(doComponentInfoModel.class);
        return doComponentInfoModelType;
    }

    /**
     * Description: This method sets the model.
     *
     * @param model
     */
    public void setdoComponentInfoModel(doComponentInfoModel model) {
        doComponentInfoModelType = model;
    }
    
}
