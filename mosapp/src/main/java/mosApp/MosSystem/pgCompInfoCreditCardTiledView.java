package mosApp.MosSystem;

import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.View;

/**
 * <p>Title: pgCompInfoCCTiledView</p>
 * <p>Description: TiledViewBean for ComponentInfo Section </p>
 * 
 *
 * @author MCM Team
 * @version 1.0 (Initial Version � Jul 18, 2008)
 */

public class pgCompInfoCreditCardTiledView extends
        pgDealSummaryComponentCreditCardTiledView {
    
    public pgCompInfoCreditCardTiledView (View parent, String name) {
        super(parent, name);
    }

    /**
     * <p>nextTile</p>
     * <p>Description: display next CreditCard section tiled view </p>
     */
    public boolean nextTile() throws ModelControlException {

        boolean movedToRow = super.nextTile(true);
        if (movedToRow) {
            ComponentInfoHandler handler = (ComponentInfoHandler) this.handler
            .cloneSS();
            handler.pageGetState(this.getParentViewBean());
            handler.pageSaveState();
        }
        return movedToRow;
    }
    
    /**
     * Handler class used by this class to display information.
     */
    private ComponentInfoHandler handler = new ComponentInfoHandler();


}
