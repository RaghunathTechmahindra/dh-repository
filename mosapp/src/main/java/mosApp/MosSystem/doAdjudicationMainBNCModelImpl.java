package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import MosSystem.*;

import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;
import com.basis100.picklist.BXResources;

/**
 * <p>Title: doAdjudicationMainBNCModelImpl</p>
 * 
 * <p>Description: Implementation class for doAdjudicationMainBNCModel</p>
 * 
 * <p> Copyright: Filogix Inc. (c) 2006 </p>
 *
 * <p> Company: Filogix Inc.</p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.0
 * @version 10/18/2006 <br>
 * Author: GCD Implementation Team<br>
 * Change: Updated PostedInterestRate field to PostedRate as per spec, defect #1031
 */
public class doAdjudicationMainBNCModelImpl extends QueryModelBase
	implements doAdjudicationMainBNCModel
{
	/**
	 *
	 *
	 */
	public doAdjudicationMainBNCModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

//   logger = SysLog.getSysLogger("AdjudicationMainBNCModel");
//   logger.debug("AdjudicationMainBNCModel@afterExecute::Size: " + sql);

		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
//	    logger = SysLog.getSysLogger("AdjudicationMainBNCModel");
//	    logger.debug("AdjudicationMainBNCModel@afterExecute::Size: " + this.getSize());
//	    logger.debug("AdjudicationMainBNCModel@afterExecute::SQLTemplate: " + this.getSelectSQL());
	
	    //--Release2.1--//
	    //To override all the Description fields by populating from BXResource
	    //Get the Language ID from the SessionStateModel
	    String defaultInstanceStateName =
				getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
	    SessionStateModelImpl theSessionState =
	        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
	                        SessionStateModel.class,
	                        defaultInstanceStateName,
	                        true);
	    int languageId = theSessionState.getLanguageId();
		int institutionId = theSessionState.getDealInstitutionId();

	    while(this.next())
	    {
	      //  Province
	      this.setDfProvince(BXResources.getPickListDescription(
	        institutionId, "PROVINCE", this.getDfProvince(), languageId));

	      // Occupancy type
	      this.setDfOccupancyType(BXResources.getPickListDescription(
	  	      institutionId, "OCCUPANCYTYPE", this.getDfOccupancyType(), languageId));
	      
	      // PropertyType
	      this.setDfPropertyType(BXResources.getPickListDescription(
		  	      institutionId, "PROPERTYTYPE", this.getDfPropertyType(), languageId));	
	      
	      // MortgageInsurer
	      this.setDfMortgageInsurer(BXResources.getPickListDescription(
		  	      institutionId, "MORTGAGEINSURER", this.getDfMortgageInsurer(), languageId));
	      
	      // MIStatus
	      this.setDfMIStatus(BXResources.getPickListDescription(
		  	      institutionId, "MISTATUS", this.getDfMIStatus(), languageId));
	      

	      // Translate Payment Term into ## Years, ## Months
	      java.math.BigDecimal numberYears = this.getDfActualPaymentTerm();
	      if (numberYears != null)
	      {
	      numberYears = numberYears.divide(new java.math.BigDecimal("12"), java.math.BigDecimal.ROUND_DOWN);
	      String fieldValue = numberYears.toString();
	      fieldValue = fieldValue.concat(" Yr(s) ");
	      fieldValue = fieldValue.concat(String.valueOf(this.getDfPaymentTermMonths()));
	      fieldValue = fieldValue.concat(" Mth(s)");
	      //logger.debug("CDH@afterExecute :: Payment fieldValue " + fieldValue);
	      this.setDfPaymentTerm(fieldValue);
	      }
	      
	      // Translate Amortization Term into ## Years, ## Months
	      numberYears = this.getDfAmortizationTerm();
	      if (numberYears != null)
	      {
	      numberYears = numberYears.divide(new java.math.BigDecimal("12"), java.math.BigDecimal.ROUND_DOWN);
		      String fieldValue = numberYears.toString();
	      fieldValue = fieldValue.concat(" Yr(s) ");
	      fieldValue = fieldValue.concat(String.valueOf(this.getDfAmortizationTermMonths()));
	      fieldValue = fieldValue.concat(" Mth(s)");
	      //logger.debug("CDH@afterExecute :: Amortization fieldValue " + fieldValue);
	      this.setDfAmortization(fieldValue);
	      }
	      
	      // Combine city and province into location
	      this.setDfLocation(this.getDfPropertyCity() + ", " + this.getDfProvince());
	    }

	    // reset Location
	    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealStatusCategoryId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALSTATUSCATEGORYID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealStatusCategoryId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALSTATUSCATEGORYID,value);
	}
	
	/**	 *
	 *
	 */
	public java.math.BigDecimal getDfPostedRate()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPOSTEDRATE);
	}


	/**
	 *
	 *
	 */
	public void setDfPostedRate(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPOSTEDRATE,value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfActualPaymentTerm()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFACTUALPAYMENTTERM);
	}


	/**
	 *
	 *
	 */
	public void setDfActualPaymentTerm(java.math.BigDecimal value)
	{
		setValue(FIELD_DFACTUALPAYMENTTERM,value);
	}

	/**
	 *
	 *
	 */
	public String getDfPaymentTerm()
	{
		return (String)getValue(FIELD_DFPAYMENTTERM);
	}


	/**
	 *
	 *
	 */
	public void setDfPaymentTerm(String value)
	{
		setValue(FIELD_DFPAYMENTTERM,value);
	}
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPaymentTermMonths()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPAYMENTTERMMONTHS);
	}


	/**
	 *
	 *
	 */
	public void setDfPaymentTermMonths(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPAYMENTTERMMONTHS,value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAmortizationTerm()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFAMORTIZATIONTERM);
	}


	/**
	 *
	 *
	 */
	public void setDfAmortizationTerm(java.math.BigDecimal value)
	{
		setValue(FIELD_DFAMORTIZATIONTERM,value);
	}

	/**
	 *
	 *
	 */
	public String getDfAmortization()
	{
		return (String)getValue(FIELD_DFAMORTIZATION);
	}


	/**
	 *
	 *
	 */
	public void setDfAmortization(String value)
	{
		setValue(FIELD_DFAMORTIZATION,value);
	}
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAmortizationTermMonths()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFAMORTIZATIONTERMMONTHS);
	}


	/**
	 *
	 *
	 */
	public void setDfAmortizationTermMonths(java.math.BigDecimal value)
	{
		setValue(FIELD_DFAMORTIZATIONTERMMONTHS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPAndIPaymentAmountMonthly()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPANDIPAYMENTAMOUNTMONTHLY);
	}


	/**
	 *
	 *
	 */
	public void setDfPAndIPaymentAmountMonthly(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPANDIPAYMENTAMOUNTMONTHLY,value);
	}
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDownPaymentAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDOWNPAYMENTAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfDownPaymentAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDOWNPAYMENTAMOUNT,value);
	}
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedLTV()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDLTV);
	}


	/**
	 *
	 *
	 */
	public void setDfCombinedLTV(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINEDLTV,value);
	}

	/**
	 *
	 *
	 */
	public String getDfPropertyCity()
	{
		return (String)getValue(FIELD_DFPROPERTYCITY);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyCity(String value)
	{
		setValue(FIELD_DFPROPERTYCITY,value);
	}
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfProvinceId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROVINCEID);
	}


	/**
	 *
	 *
	 */
	public void setDfProvinceId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROVINCEID,value);
	}
	
	/**
	 *
	 *
	 */
	public String getDfProvince()
	{
		return (String)getValue(FIELD_DFPROVINCE);
	}


	/**
	 *
	 *
	 */
	public void setDfProvince(String value)
	{
		setValue(FIELD_DFPROVINCE, value);
	}
	
	/**
	 *
	 *
	 */
	public String getDfLocation()
	{
		return (String)getValue(FIELD_DFLOCATION);
	}


	/**
	 *
	 *
	 */
	public void setDfLocation(String value)
	{
		setValue(FIELD_DFLOCATION, value);
	}
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfOccupancyTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFOCCUPANCYTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfOccupancyTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFOCCUPANCYTYPEID,value);
	}
	
	/**
	 *
	 *
	 */
	public String getDfOccupancyType()
	{
		return (String)getValue(FIELD_DFOCCUPANCYTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfOccupancyType(String value)
	{
		setValue(FIELD_DFOCCUPANCYTYPE, value);
	}
	
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYTYPEID,value);
	}
	
	/**
	 *
	 *
	 */
	public String getDfPropertyType()
	{
		return (String)getValue(FIELD_DFPROPERTYTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyType(String value)
	{
		setValue(FIELD_DFPROPERTYTYPE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMortgageInsurerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMORTGAGEINSURERID);
	}


	/**
	 *
	 *
	 */
	public void setDfMortgageInsurerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMORTGAGEINSURERID,value);
	}

	/**
	 *
	 *
	 */
	public String getDfMortgageInsurer()
	{
		return (String)getValue(FIELD_DFMORTGAGEINSURER);
	}


	/**
	 *
	 *
	 */
	public void setDfMortgageInsurer(String value)
	{
		setValue(FIELD_DFMORTGAGEINSURER,value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIStatusId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDGDSBORROWER);
	}


	/**
	 *
	 *
	 */
	public void setDfMIStatusId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINEDGDSBORROWER,value);
	}

	/**
	 *
	 *
	 */
	public String getDfMIStatus()
	{
		return (String)getValue(FIELD_DFMISTATUS);
	}


	/**
	 *
	 *
	 */
	public void setDfMIStatus(String value)
	{
		setValue(FIELD_DFMISTATUS,value);
	}
	

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedGDSBorrower()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDGDSBORROWER);
	}


	/**
	 *
	 *
	 */
	public void setDfCombinedGDSBorrower(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINEDGDSBORROWER,value);
	}
	

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedTDSBorrower()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDTDSBORROWER);
	}


	/**
	 *
	 *
	 */
	public void setDfCombinedTDSBorrower(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINEDTDSBORROWER,value);
	}
	

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalCombinedIncome()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALCOMBINEDINCOME);
	}
	
	/**
	 *
	 *
	 */
	public void setDfTotalCombinedIncome(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALCOMBINEDINCOME,value);
	}
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalAnnualIncome()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALANNUALINCOME);
	}
	
	/**
	 *
	 *
	 */
	public void setDfTotalAnnualIncome(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALANNUALINCOME,value);
	}
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalOtherIncome()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALOTHERINCOME);
	}
	
	/**
	 *
	 *
	 */
	public void setDfTotalOtherIncome(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALOTHERINCOME,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.
  public static final String DATA_SOURCE_NAME="jdbc/orcl";
  public static final String SELECT_SQL_TEMPLATE=
	  "SELECT ALL DEAL.DEALID, DEAL.COPYID, DEAL.POSTEDRATE, STATUS.STATUSCATEGORYID, " + 
	  "DEAL.ACTUALPAYMENTTERM, to_char(DEAL.ACTUALPAYMENTTERM) PAYMENTTERM, MOD(DEAL.ACTUALPAYMENTTERM, 12) PAYMENTTERMMONTHS, " + 
	  "DEAL.AMORTIZATIONTERM, to_char(DEAL.AMORTIZATIONTERM) AMORTIZATION, MOD(DEAL.AMORTIZATIONTERM, 12) AMORTIZATIONTERMMONTHS, " +
	  "DEAL.PANDIPAYMENTAMOUNTMONTHLY, DEAL.DOWNPAYMENTAMOUNT, DEAL.COMBINEDLTV, PROPERTY.PROPERTYCITY, " + 
	  "to_char(PROPERTY.PROVINCEID) PROVINCEID_STR, PROPERTY.PROVINCEID, to_char(PROPERTY.PROPERTYLOCATIONID) LOCATION, " +
	  "to_char(PROPERTY.OCCUPANCYTYPEID) OCCUPANCYTYPEID_STR, PROPERTY.OCCUPANCYTYPEID, " +
	  "to_char(PROPERTY.PROPERTYTYPEID) PROPERTYTYPEID_STR, PROPERTY.PROPERTYTYPEID, " + 
	  "to_char(DEAL.MORTGAGEINSURERID) MORTGAGEINSURERID_STR, DEAL.MORTGAGEINSURERID, " + 
	  "to_char(DEAL.MISTATUSID) MISTATUSID_STR, DEAL.MISTATUSID, DEAL.COMBINEDGDSBORROWER, DEAL.COMBINEDTDSBORROWER, " +
	  "SUM(I0.ANNUALINCOMEAMOUNT) TOTALCOMBINEDINCOME, SUM(I.ANNUALINCOMEAMOUNT) TOTALANNUALINCOME, SUM(I2.ANNUALINCOMEAMOUNT) TOTALOTHERINCOME " +
	  "FROM DEAL, STATUS, PROPERTY, BORROWER " +
	  "LEFT OUTER JOIN INCOME I0 ON BORROWER.BORROWERID = I0.BORROWERID AND BORROWER.COPYID = I0.COPYID " + 
	  "LEFT OUTER JOIN INCOME I2 ON I0.INCOMEID  = I2.INCOMEID AND I0.COPYID = I2.COPYID AND I2.INCOMETYPEID IN ( " +
	  Mc.INCOME_INTEREST_INCOME + "," + Mc.INCOME_CHILD_SUPPORT + "," + Mc.INCOME_PENSION + "," + Mc.INCOME_ALIMONY + "," + Mc.INCOME_OTHER + "," + Mc.INCOME_RENTAL_ADD + "," + Mc.INCOME_RENTAL_SUBTRACT + " ) " +
	  "LEFT OUTER JOIN INCOME I ON I0.INCOMEID = I.INCOMEID AND I0.COPYID = I.COPYID AND I.INCOMETYPEID IN ( " + 
	  Mc.INCOME_SALARY + "," + Mc.INCOME_HOURLY + "," + Mc.INCOME_HOURLY_PLUS_COMMISSION + "," + Mc.INCOME_COMMISSION + "," + Mc.INCOME_SELF_EMP + "," + Mc.INCOME_OTHER_EMPLOYMENT_INCOME + " ) " +
	  "__WHERE__ " +
	  "GROUP BY DEAL.DEALID, DEAL.COPYID, DEAL.POSTEDRATE, DEAL.ACTUALPAYMENTTERM, DEAL.AMORTIZATIONTERM, DEAL.PANDIPAYMENTAMOUNTMONTHLY, " +
	  "DEAL.DOWNPAYMENTAMOUNT, DEAL.COMBINEDLTV, PROPERTY.PROPERTYCITY, PROPERTY.PROVINCEID, PROPERTY.PROPERTYLOCATIONID, PROPERTY.OCCUPANCYTYPEID, " +
	  "PROPERTY.PROPERTYTYPEID, DEAL.MORTGAGEINSURERID,  DEAL.MISTATUSID, DEAL.COMBINEDGDSBORROWER, DEAL.COMBINEDTDSBORROWER, STATUS.STATUSCATEGORYID "; 
  
  public static final String MODIFYING_QUERY_TABLE_NAME="DEAL, PROPERTY  ";

  public static final String STATIC_WHERE_CRITERIA= " DEAL.DEALID = PROPERTY.DEALID AND DEAL.COPYID = PROPERTY.COPYID " +
  		"AND BORROWER.DEALID = DEAL.DEALID AND BORROWER.COPYID = DEAL.COPYID AND DEAL.STATUSID = STATUS.STATUSID";  
  
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();

	public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFDEALSTATUSCATEGORYID="STATUS.STATUSCATEGORYID";
	public static final String COLUMN_DFDEALSTATUSCATEGORYID="STATUSCATEGORYID";
	public static final String QUALIFIED_COLUMN_DFPOSTEDRATE="DEAL.POSTEDRATE";
	public static final String COLUMN_DFPOSTEDRATE="POSTEDRATE";
	public static final String QUALIFIED_COLUMN_DFACTUALPAYMENTTERM="DEAL.ACTUALPAYMENTTERM";
	public static final String COLUMN_DFACTUALPAYMENTTERM="ACTUALPAYMENTTERM";
	public static final String QUALIFIED_COLUMN_DFPAYMENTTERM="DEAL.PAYMENTTERM";
	public static final String COLUMN_DFPAYMENTTERM="PAYMENTTERM";
	public static final String QUALIFIED_COLUMN_DFPAYMENTTERMMONTHS="DEAL.PAYMENTTERMMONTHS";
	public static final String COLUMN_DFPAYMENTTERMMONTHS="PAYMENTTERMMONTHS";
	public static final String QUALIFIED_COLUMN_DFAMORTIZATIONTERM="DEAL.AMORTIZATIONTERM";
	public static final String COLUMN_DFAMORTIZATIONTERM="AMORTIZATIONTERM";
	public static final String QUALIFIED_COLUMN_DFAMORTIZATION="DEAL.AMORTIZATION";
	public static final String COLUMN_DFAMORTIZATION="AMORTIZATION";
	public static final String QUALIFIED_COLUMN_DFAMORTIZATIONTERMMONTHS="DEAL.AMORTIZATIONTERMMONTHS";
	public static final String COLUMN_DFAMORTIZATIONTERMMONTHS="AMORTIZATIONTERMMONTHS";
	public static final String QUALIFIED_COLUMN_DFPANDIPAYMENTAMOUNTMONTHLY="DEAL.PANDIPAYMENTAMOUNTMONTHLY";
	public static final String COLUMN_DFPANDIPAYMENTAMOUNTMONTHLY="PANDIPAYMENTAMOUNTMONTHLY";
	public static final String QUALIFIED_COLUMN_DFDOWNPAYMENTAMOUNT="DEAL.DOWNPAYMENTAMOUNT";
	public static final String COLUMN_DFDOWNPAYMENTAMOUNT="DOWNPAYMENTAMOUNT";
	public static final String QUALIFIED_COLUMN_DFCOMBINEDLTV="DEAL.COMBINEDLTV";
	public static final String COLUMN_DFCOMBINEDLTV="COMBINEDLTV";
	public static final String QUALIFIED_COLUMN_DFPROPERTYCITY="DEAL.PROPERTYCITY";
	public static final String COLUMN_DFPROPERTYCITY="PROPERTYCITY";
	public static final String QUALIFIED_COLUMN_DFPROVINCEID="DEAL.PROVINCEID";
	public static final String COLUMN_DFPROVINCEID="PROVINCEID";
	public static final String QUALIFIED_COLUMN_DFPROVINCE="DEAL.PROVINCEID_STR";
	public static final String COLUMN_DFPROVINCE="PROVINCEID_STR";
	public static final String QUALIFIED_COLUMN_DFLOCATION="DEAL.LOCATION";
	public static final String COLUMN_DFLOCATION="LOCATION";
	public static final String QUALIFIED_COLUMN_DFOCCUPANCYTYPEID="DEAL.OCCUPANCYTYPEID";
	public static final String COLUMN_DFOCCUPANCYTYPEID="OCCUPANCYTYPEID";
	public static final String QUALIFIED_COLUMN_DFOCCUPANCYTYPE="DEAL.OCCUPANCYTYPEID_STR";
	public static final String COLUMN_DFOCCUPANCYTYPE="OCCUPANCYTYPEID_STR";
	public static final String QUALIFIED_COLUMN_DFPROPERTYTYPEID="DEAL.PROPERTYTYPEID";
	public static final String COLUMN_DFPROPERTYTYPEID="PROPERTYTYPEID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYTYPE="DEAL.PROPERTYTYPEID_STR";
	public static final String COLUMN_DFPROPERTYTYPE="PROPERTYTYPEID_STR";
	public static final String QUALIFIED_COLUMN_DFMORTGAGEINSURERID="DEAL.MORTGAGEINSURERID";
	public static final String COLUMN_DFMORTGAGEINSURERID="MORTGAGEINSURERID";
	public static final String QUALIFIED_COLUMN_DFMORTGAGEINSURER="DEAL.MORTGAGEINSURERID_STR";
	public static final String COLUMN_DFMORTGAGEINSURER="MORTGAGEINSURERID_STR";
	public static final String QUALIFIED_COLUMN_DFMISTATUSID="DEAL.MISTATUSID";
	public static final String COLUMN_DFMISTATUSID="MISTATUSID";
	public static final String QUALIFIED_COLUMN_DFMISTATUS="DEAL.MISTATUSID_STR";
	public static final String COLUMN_DFMISTATUS="MISTATUSID_STR";
	public static final String QUALIFIED_COLUMN_DFCOMBINEDGDSBORROWER="DEAL.COMBINEDGDSBORROWER";
	public static final String COLUMN_DFCOMBINEDGDSBORROWER="COMBINEDGDSBORROWER";
	public static final String QUALIFIED_COLUMN_DFCOMBINEDTDSBORROWER="DEAL.COMBINEDTDSBORROWER";
	public static final String COLUMN_DFCOMBINEDTDSBORROWER="COMBINEDTDSBORROWER";
	
	public static final String QUALIFIED_COLUMN_DFTOTALCOMBINEDINCOME="I0.TOTALCOMBINEDINCOME";
	public static final String COLUMN_DFTOTALCOMBINEDINCOME="TOTALCOMBINEDINCOME";
	public static final String QUALIFIED_COLUMN_DFTOTALANNUALINCOME="I.TOTALANNUALINCOME";
	public static final String COLUMN_DFTOTALANNUALINCOME="TOTALANNUALINCOME";
	public static final String QUALIFIED_COLUMN_DFTOTALOTHERINCOME="I2.TOTALOTHERINCOME";
	public static final String COLUMN_DFTOTALOTHERINCOME="TOTALOTHERINCOME";
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFDEALID,
					COLUMN_DFDEALID,
					QUALIFIED_COLUMN_DFDEALID,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFCOPYID,
					COLUMN_DFCOPYID,
					QUALIFIED_COLUMN_DFCOPYID,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));

		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFDEALSTATUSCATEGORYID,
					COLUMN_DFDEALSTATUSCATEGORYID,
					QUALIFIED_COLUMN_DFDEALSTATUSCATEGORYID,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));

	
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPOSTEDRATE,
				COLUMN_DFPOSTEDRATE,
				QUALIFIED_COLUMN_DFPOSTEDRATE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFACTUALPAYMENTTERM,
				COLUMN_DFACTUALPAYMENTTERM,
				QUALIFIED_COLUMN_DFACTUALPAYMENTTERM,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFPAYMENTTERM,
					COLUMN_DFPAYMENTTERM,
					QUALIFIED_COLUMN_DFPAYMENTTERM,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));	
	
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFPAYMENTTERMMONTHS,
					COLUMN_DFPAYMENTTERMMONTHS,
					QUALIFIED_COLUMN_DFPAYMENTTERMMONTHS,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
			
			FIELD_SCHEMA.addFieldDescriptor(
					new QueryFieldDescriptor(
						FIELD_DFAMORTIZATIONTERM,
						COLUMN_DFAMORTIZATIONTERM,
						QUALIFIED_COLUMN_DFAMORTIZATIONTERM,
						java.math.BigDecimal.class,
						false,
						false,
						QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
						"",
						QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
						""));
				
				FIELD_SCHEMA.addFieldDescriptor(
						new QueryFieldDescriptor(
							FIELD_DFAMORTIZATION,
							COLUMN_DFAMORTIZATION,
							QUALIFIED_COLUMN_DFAMORTIZATION,
							String.class,
							false,
							false,
							QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
							"",
							QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
							""));	
				
				FIELD_SCHEMA.addFieldDescriptor(
						new QueryFieldDescriptor(
							FIELD_DFAMORTIZATIONTERMMONTHS,
							COLUMN_DFAMORTIZATIONTERMMONTHS,
							QUALIFIED_COLUMN_DFAMORTIZATIONTERMMONTHS,
							java.math.BigDecimal.class,
							false,
							false,
							QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
							"",
							QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
							""));
				
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFPANDIPAYMENTAMOUNTMONTHLY,
					COLUMN_DFPANDIPAYMENTAMOUNTMONTHLY,
					QUALIFIED_COLUMN_DFPANDIPAYMENTAMOUNTMONTHLY,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));		

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
						FIELD_DFDOWNPAYMENTAMOUNT,
					COLUMN_DFDOWNPAYMENTAMOUNT,
					QUALIFIED_COLUMN_DFDOWNPAYMENTAMOUNT,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));		

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
						FIELD_DFCOMBINEDLTV,
					COLUMN_DFCOMBINEDLTV,
					QUALIFIED_COLUMN_DFCOMBINEDLTV,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));		

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFPROPERTYCITY,
					COLUMN_DFPROPERTYCITY,
					QUALIFIED_COLUMN_DFPROPERTYCITY,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));	

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFPROVINCEID,
					COLUMN_DFPROVINCEID,
					QUALIFIED_COLUMN_DFPROVINCEID,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));	

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
						FIELD_DFPROVINCE,
					COLUMN_DFPROVINCE,
					QUALIFIED_COLUMN_DFPROVINCE,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));	


		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
						FIELD_DFLOCATION,
					COLUMN_DFLOCATION,
					QUALIFIED_COLUMN_DFLOCATION,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));	
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFOCCUPANCYTYPEID,
					COLUMN_DFOCCUPANCYTYPEID,
					QUALIFIED_COLUMN_DFOCCUPANCYTYPEID,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));	
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFOCCUPANCYTYPE,
					COLUMN_DFOCCUPANCYTYPE,
					QUALIFIED_COLUMN_DFOCCUPANCYTYPE,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));	
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFPROPERTYTYPEID,
					COLUMN_DFPROPERTYTYPEID,
					QUALIFIED_COLUMN_DFPROPERTYTYPEID,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));	
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFPROPERTYTYPE,
					COLUMN_DFPROPERTYTYPE,
					QUALIFIED_COLUMN_DFPROPERTYTYPE,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFMORTGAGEINSURERID,
					COLUMN_DFMORTGAGEINSURERID,
					QUALIFIED_COLUMN_DFMORTGAGEINSURERID,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));	
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFMORTGAGEINSURER,
					COLUMN_DFMORTGAGEINSURER,
					QUALIFIED_COLUMN_DFMORTGAGEINSURER,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFMISTATUSID,
					COLUMN_DFMISTATUSID,
					QUALIFIED_COLUMN_DFMISTATUS,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));	
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFMISTATUS,
					COLUMN_DFMISTATUS,
					QUALIFIED_COLUMN_DFMISTATUS,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));


		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFCOMBINEDGDSBORROWER,
					COLUMN_DFCOMBINEDGDSBORROWER,
					QUALIFIED_COLUMN_DFCOMBINEDGDSBORROWER,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFCOMBINEDTDSBORROWER,
					COLUMN_DFCOMBINEDTDSBORROWER,
					QUALIFIED_COLUMN_DFCOMBINEDGDSBORROWER,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
	
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFTOTALCOMBINEDINCOME,
					COLUMN_DFTOTALCOMBINEDINCOME,
					QUALIFIED_COLUMN_DFTOTALCOMBINEDINCOME,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFTOTALANNUALINCOME,
					COLUMN_DFTOTALANNUALINCOME,
					QUALIFIED_COLUMN_DFTOTALANNUALINCOME,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFTOTALOTHERINCOME,
					COLUMN_DFTOTALOTHERINCOME,
					QUALIFIED_COLUMN_DFTOTALOTHERINCOME,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));

  
	}

}

