package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 * Title: doComponentOverDraftModel
 * <p>
 * Description: This is the interface class for doComponentSummaryModel used to retrieve data from componentsummary table.
 *
 * @author MCM Impl Team.
 * @version 1.0 09-June-2008 XS_16.7 Initial version
 */
public interface doComponentSummaryModel extends QueryModel, SelectQueryModel {

	public static final String FIELD_DFCOPYID = "dfCopyId";
	public static final String FIELD_DFDEALID = "dfDealId";
	public static final String FIELD_DFISNTITUTIONID = "dfInstitutionId";
	public static final String FIELD_DFTOTALAMOUNT = "dfTotalAmount";
	public static final String FIELD_DFTOTALCASHBACKAMOUNT = "dfTotalCashBackAmount";
	public static final String FIELD_DFUNALLOCATEDAMOUNT = "dfUnallocatedAmount";

	/**
	 * This method declaration returns the value of field copyId.
	 *
	 * @return BigDecimal copyId from the model.
	 */
	public java.math.BigDecimal getDfCopyId();

	/**
	 * This method declaration sets the value of field copyId
	 *
	 * @param value - new value of the field
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	/**
	 * This method declaration returns the value of field InstitutionId.
	 *
	 * @return BigDecimal InstitutionId from the model.
	 */
	public java.math.BigDecimal getDfInstitutionId();

	/**
	 * This method declaration sets the value of field InstitutionId
	 *
	 * @param value -  new InstitutionId.
	 */
	public void setDfInstitutionId(java.math.BigDecimal value);

	/**
	 * This method declaration returns the value of field TotalAmount.
	 *
	 * @return BigDecimal TotalAmount from the model.
	 */
	public java.math.BigDecimal getDfTotalAmount();

	/**
	 * This method declaration sets the value of field TotalAmount
	 *
	 * @param value -  new TotalAmount.
	 */
	public void setDfTotalAmount(java.math.BigDecimal value);


	/**
	 * This method declaration returns the value of field the TotalCashBackAmount.
	 *
	 * @return the TotalCashBackAmount
	 */
	public java.math.BigDecimal getDfTotalCashBackAmount();

	/**
	 * This method declaration sets the the TotalCashBackAmount.
	 *
	 * @param value the new TotalCashBackAmount
	 */
	public void setDfTotalCashBackAmount(java.math.BigDecimal value);

	/**
	 * This method declaration returns the value of field the DealId.
	 *
	 * @return the DealId
	 */
	public java.math.BigDecimal getDfDealId();

	/**
	 * This method declaration sets the the DealId.
	 *
	 * @param value the new DealId
	 */
	public void setDfDealId(java.math.BigDecimal value);
	/**
	 * This method declaration returns the value of field the UnallocatedAmount.
	 *
	 * @return the UnallocatedAmount
	 */
	public java.math.BigDecimal getDfUnallocatedAmount();
	/**
	 * This method declaration sets the the UnallocatedAmount.
	 *
	 * @param value the new UnallocatedAmount
	 */
	public void setDfUnallocatedAmount(java.math.BigDecimal value);
}
