package mosApp.MosSystem;

import java.io.*;
import java.math.BigDecimal;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doMainViewMortgageInsurerModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfMIInsurer();

	
	/**
	 * 
	 * 
	 */
	public void setDfMIInsurer(String value);

	// SEAN DJ SPEC-Progress Advance Type July 25, 2005: the getter and setter
	// methods.
	public String getDfProgressAdvanceType();
	public void setDfProgressAdvanceType(String value);
	// SEAN DJ SPEC-PAT END
	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfMIPremiumAmount();

	
	/**
	 * 
	 * 
	 */
	public void setDfMIPremiumAmount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfMICertificateNo();

	
	/**
	 * 
	 * 
	 */
	public void setDfMICertificateNo(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfMIRequestStatus();

	
	/**
	 * 
	 * 
	 */
	public void setDfMIRequestStatus(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfMITypeDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfMITypeDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfMIUpfront();

	
	/**
	 * 
	 * 
	 */
	public void setDfMIUpfront(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfMIExistingPolicyNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfMIExistingPolicyNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfMIRUIntervention();

	
	/**
	 * 
	 * 
	 */
	public void setDfMIRUIntervention(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPreQualificationMICertNum();

	
	/**
	 * 
	 * 
	 */
	public void setDfPreQualificationMICertNum(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfMIIndicatorDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfMIIndicatorDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfMIPayorDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfMIPayorDesc(String value);
	
	
  //--Release3.1--begins
  //--by Hiro Apr 11, 2006

  public void setDfProgressAdvanceInspectionBy(String value);

  public String getDfProgressAdvanceInspectionBy();

  public void setDfSelfDirectedRRSP(String value);

  public String getDfSelfDirectedRRSP();

  public void setDfCMHCProductTrackerIdentifier(String value);

  public String getDfCMHCProductTrackerIdentifier();

  public void setDfLOCRepayment(String value);

  public String getDfLOCRepayment();

  public void setDfRequestStandardService(String value);

  public String getDfRequestStandardService();

  public void setDfLOCAmortization(BigDecimal value);

  public BigDecimal getDfLOCAmortization();

  public void setDfLOCInterestOnlyMaturityDate(Timestamp value);

  public Timestamp getDfLOCInterestOnlyMaturityDate();
  
  public java.math.BigDecimal getDfInstitutionId();
  public void setDfInstitutionId(java.math.BigDecimal id);
  
  //--Release3.1--ends

	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFMIINSURER="dfMIInsurer";
	public static final String FIELD_DFMIPREMIUMAMOUNT="dfMIPremiumAmount";
	public static final String FIELD_DFMICERTIFICATENO="dfMICertificateNo";
	public static final String FIELD_DFMIREQUESTSTATUS="dfMIRequestStatus";
	public static final String FIELD_DFMITYPEDESC="dfMITypeDesc";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFMIUPFRONT="dfMIUpfront";
	public static final String FIELD_DFMIEXISTINGPOLICYNUMBER="dfMIExistingPolicyNumber";
	public static final String FIELD_DFMIRUINTERVENTION="dfMIRUIntervention";
	public static final String FIELD_DFPREQUALIFICATIONMICERTNUM="dfPreQualificationMICertNum";
	public static final String FIELD_DFMIINDICATORDESC="dfMIIndicatorDesc";
	public static final String FIELD_DFMIPAYORDESC="dfMIPayorDesc";
	
	// SEAN DJ SPEC-Progress Advance Type July 25, 2005: Define the field.
	public static final String FIELD_DFPROGRESSADVANCETYPE="dfProgressAdvanceType";
	// SEAN DJ SPEC-PAT END
  
  //--Release3.1--begins
  //--by Hiro Apr 11, 2006
  
  public static final String FIELD_DFPROGRESSADVANCEINSPECTIONBY = "dfProgressAdvanceInspectionBy";
  public static final String FIELD_DFSELFDIRECTEDRRSP = "dfSelfDirectedRRSP";
  public static final String FIELD_DFCMHCPRODUCTTRACKERIDENTIFIER = "dfCMHCProductTrackerIdentifier";
  public static final String FIELD_DFLOCREPAYMENT = "dfLOCRepayment";
  public static final String FIELD_DFREQUESTSTANDARDSERVICE = "dfRequestStandardService";
  public static final String FIELD_DFLOCAMORTIZATION = "dfLOCAmortization";
  public static final String FIELD_DFLOCINTERESTONLYMATURITYDATE = "dfLOCInterestOnlyMaturityDate";
  
  public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";

  //--Release3.1--ends
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

