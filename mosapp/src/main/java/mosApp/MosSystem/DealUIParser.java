package mosApp.MosSystem;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.entity.FinderException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.mtgrates.RateInfo;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.core.ExpressRuntimeException;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;

public class DealUIParser
{
	private final static Logger logger = LoggerFactory.getLogger(DealUIParser.class);

  private SessionResourceKit srk = null;

	//4.4GR, keep result as instance field to avoid executing the same sql
	private RateInfo savedRateInfo;

  public DealUIParser(SessionResourceKit srk)
  {
      this.srk = srk;
  }

	public String getParsedPhone(String[] phoneparts)
	{

		// Modified to store ??? instead "" to fix the problem of Phone Num shifted when re-display
		//  e.g. if user not input area i.e. ( )123-4567, we will store 0001234567 in database.
		//       Which is agreed with Product
		//  - By Billy 20Apr2001
		String tmpFinal = "";

		String tmp = "";

		// Check if no input
		tmp = phoneparts [ 0 ] + phoneparts [ 1 ] + phoneparts [ 2 ];

		if (tmp.trim().equals("")) return "";


		// Get Area code
		tmp = getValidString(phoneparts [ 0 ]);

		if (tmp.equals("")) tmp = "000";

		tmpFinal += tmp;


		// Get NPA code
		tmp = getValidString(phoneparts [ 1 ]);

		if (tmp.equals("")) tmp = "000";

		tmpFinal += tmp;


		// Get NXX code
		tmp = getValidString(phoneparts [ 2 ]);

		if (tmp.equals("")) tmp = "0000";

		tmpFinal += tmp;

		return tmpFinal;

	}


	public String getParsedDate(String[] dateparts)
	{

		//0 - Month
		//1- Day
		//2 - Year

		try
		{
      String mn = getParsedMonth(dateparts [ 0 ]);
			if ((mn == null) ||(mn.trim().length() < 1)) return "";
			if (mn.trim().length() <= 1) mn = "0" + mn;
			String day = dateparts [ 1 ];
			if (day.trim().length() < 2) day = "0" + day;

      //logger.debug("DUIP@getParsedDate::Return: " + day + mn + dateparts [ 2 ]);
			return(day + mn + dateparts [ 2 ]);
		}
		catch(Exception e)
		{
			return "";
		}
	}


	public String getParsedMonth(String month)
	{
		try
		{
			int mnint = Integer.parseInt(month);

			//check the month range
			if (mnint > 0 && mnint < 13) return String.valueOf(mnint);
			else return "";
		}
		catch(Exception e)
		{
			return "";
		}

	}


	public String getTotalTerms(String[] duration)
	{

		//0 - years
		//1 - months
		int years = 0;

		int months = 0;

    //logger.debug("DUIP@getTotalTerms:: start");

		try
		{
			years = Integer.parseInt(duration [ 0 ]);
		}
		catch(Exception ex)
		{
			years = 0;
		}

		try
		{
			months = Integer.parseInt(duration [ 1 ]);
		}
		catch(Exception ex)
		{
			months = 0;
		}

		return String.valueOf(((years * 12) + months));

	}

	//4.4GR
	private static final String SQL_RATEINFO = ""
		+ "SELECT "
		+ "pi.pricingrateinventoryid, "
		+ "pp.ratecode, "
		+ "pp.ratecodedescription, "
		+ "pi.indexeffectivedate, "
		+ "pi.indexexpirydate, "
		+ "pi.internalratepercentage, "
		+ "pi.maximumdiscountallowed, "
		+ "pi.internalratepercentage - pi.maximumdiscountallowed, "
		+ "prp.primeindexrateprofileid, "
		+ "pi.primebaseadj, "
		+ "pri.primeindexrate, "
		+ "pi.teaserdiscount, "
		+ "pi.teaserterm, "
		+ "pi.institutionprofileid "
		+ "FROM "
		+ "pricingrateinventory pi "
		+ "JOIN pricingprofile pp "
		+ "ON     pi.institutionprofileid = pp.institutionprofileid "
		+ "AND    pi.pricingprofileid     = pp.pricingprofileid "
		+ "JOIN primeindexrateinventory pri "
		+ "ON     pi.institutionprofileid      = pri.institutionprofileid "
		+ "AND    pi.primeindexrateinventoryid = pri.primeindexrateinventoryid "
		+ "JOIN primeindexrateprofile prp "
		+ "ON     pri.institutionprofileid    = prp.institutionprofileid "
		+ "AND    pri.primeindexrateprofileid = prp.primeindexrateprofileid "
		+ "WHERE "
		+ "pi.pricingrateinventoryid = ?";

	private RateInfo getRateInfoToDatabase(int pricingProfileId)
  throws Exception
  {
		//4.4GR, keep result in instance field to avoid executing the same sql
		if (savedRateInfo != null && savedRateInfo.getRateId() == pricingProfileId) {
			return (RateInfo) BeanUtils.cloneBean(savedRateInfo);
		}

		savedRateInfo = new RateInfo();

     try
     {
       JdbcExecutor jExec = srk.getJdbcExecutor();
			PreparedStatement pstmt = jExec.getPreparedStatement(SQL_RATEINFO);
			pstmt.setInt(1, pricingProfileId);
			int key = jExec.executePreparedStatement(pstmt, SQL_RATEINFO);
      
			if (jExec.next(key)){
          int index = 0;
				savedRateInfo.setRateId(            jExec.getInt(key,++index));
				savedRateInfo.setRateCode(          jExec.getString(key,++index));
				savedRateInfo.setRateDescription(   jExec.getString(key,++index));
				savedRateInfo.setEffectiveDate(     jExec.getDate (key,++index));
				savedRateInfo.setExpiryDate(        jExec.getDate (key,++index));
				savedRateInfo.setPostedRate(        jExec.getDouble(key,++index));
				savedRateInfo.setMaxDiscount(       jExec.getDouble(key,++index));
				savedRateInfo.setBestRate (         jExec.getDouble(key,++index));
				savedRateInfo.setPrimeIndexId(      jExec.getInt(key,++index));
				savedRateInfo.setPrimeBaseAdj(      jExec.getDouble(key,++index));
				savedRateInfo.setPrimeIndexRate(    jExec.getDouble(key,++index));
				savedRateInfo.setTeaserDiscount(    jExec.getDouble(key,++index));
				savedRateInfo.setTeaserTerm(        jExec.getInt(key,++index));
				
			} else {
				String msg = "no data found with the SQL: "
						+ SQL_RATEINFO.replace("?", "" + pricingProfileId);
				throw new FinderException(msg);
        }

        jExec.closeData (key);
			showRate(savedRateInfo);
			return savedRateInfo;
      }
      catch(Exception e)
      {
			savedRateInfo = null;
          String msg = "Exception @DealUIParser.getRateInfoToDatabase";
			logger.error(msg, e);
			throw e;
      }
  }

	private void showRate(RateInfo curRate)
   {
		if(logger.isDebugEnabled() == false) return;
        logger.debug("-------------------");
        logger.debug( "RateId = " + curRate.getRateId() );
        logger.debug( "RateCode = " + curRate.getRateCode() );
        logger.debug( "RateDescription = " + curRate.getRateDescription() );
        logger.debug( "EffectiveDate = " + curRate.getEffectiveDate() );
        logger.debug( "ExpiryDate = " + curRate.getExpiryDate() );
        logger.debug( "PostedRate = " + curRate.getPostedRate() );
        logger.debug( "MaxDiscount = " + curRate.getMaxDiscount() );
        logger.debug( "CurRate.bestRate = " + curRate.getBestRate() );
        logger.debug( "PrimeIndexRateId = " + curRate.getPrimeIndexId() );
        logger.debug( "PrimeIndexAdjustemnt = " + curRate.getPrimeBaseAdj() );
        logger.debug( "PrimeIndexRate = " + curRate.getPrimeIndexRate() );
        logger.debug( "TeaserDiscount = " + curRate.getTeaserDiscount() );
        logger.debug( "TeaserTerm = " + curRate.getTeaserTerm() );
        logger.debug("-------------------");
   }

	public String getPricingRateInventoryId(String[] acombolinein)
	{
    String pricingProfileId = parseCompositeRateId(acombolinein);
    logger.debug("DUIP@getPricingRateInventoryId::CompositeIndex: " + pricingProfileId);

    return pricingProfileId;
	}

	/**
	 * This method is adjusted to the JATO architecture.
	 */
	public String getPricingProfileIdFromServlet(String rateCode)
	{
      rateCode = new String(rateCode.trim());
			doGetPricingProfileIdModel theModel = (doGetPricingProfileIdModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doGetPricingProfileIdModel.class);
			String priceProfileId = "";

			//int id = 0;
			theModel.clearUserWhereCriteria();
			theModel.addUserWhereCriterion("dfRateCode", "=", rateCode);

      try
      {
        ResultSet rs = theModel.executeSelect(null);

        if(rs == null || theModel.getSize() < 0)
        {
					//id = 0;
					priceProfileId = "";
        }

        else if(rs != null && theModel.getSize() > 0)
        {
           //logger.debug("DUIP@getPricingProfileIdFromServlet::Size of the result set: " + theModel.getSize());

            while (theModel.next()) {
					Object oPriceProfileId = theModel.getValue(doGetPricingProfileIdModel.FIELD_DFPRICINGPROFILEID);
                  ////logger.debug("DUIP@getPricingProfileIdFromServlet::PricingProfileIdFromServlet: " + oPriceProfileId.toString());
                  priceProfileId = oPriceProfileId.toString();
                }
            }
      }
      catch (ModelControlException mce)
      {
			logger.warn("DUIP@getPricingProfileIdFromServlet::MCEException: " + mce);
      }

      catch (SQLException sqle)
      {
			logger.warn("DUIP@getPricingProfileIdFromServlet::SQLException: " + sqle);
      }

			return priceProfileId;
	}

	// input string should come as a hidden text from the HTML page in the format:
	// #.##% [Max Discount] | #.##% | MMM DD YYYY | HH:MM EST | SC:MSC
	// it allows to take the constact the proper time.
	// getRateDateStringFromCombo(String[] acombolinein) function parses the composite
	// line and returns the posted rate date string. One of the supported string
	// format (in the TypeConverter.java class) for date is: yyyy-MM-dd HH:mm:ss
	// We must use only this format as we need to compare the exact time with the
	// the time incoming from the servlet to display the default value in case of
	// Deal Modification screen.
	// Input to this method is a string of the form incoming from the servlet
	//		| MMM DD YYYY | HH:MM CCC | SS:MSC
	// where '|' is a character literal
	// and
	// MMM 	-	literal (e.g. NOV or MAR ) value of the month
	// DD	-	2-digit value of the day (e.g 27 or 04)
	// YYYY -	4-digit value of the year
	// HH	-	2-digit hour value
	// MM	-	2-digit minute value
	// CCC 	-	Time Zone Code
	// SS   -   2-digit second value
	// MSC  -   2-digit millisecond value
	public String getRateDateFromPostedCombo(String[] acombolinein)
	{
    String pricingProfileId = parseCompositeRateId(acombolinein);
    logger.debug("DUIP@getRateDateFromPostedCombo::CompositeIndex: " + pricingProfileId);

    //--Ticket#575--22Aug2004--start--//
    String effectiveDate = "";
    DateFormat theFormat = new SimpleDateFormat("ddMMyyyy");

    try
    {
			RateInfo ri = getRateInfoToDatabase(new Integer(pricingProfileId).intValue());

      effectiveDate = theFormat.format(ri.getEffectiveDate());
      logger.debug("DUIP@getRateDateFromPostedCombo::EffectiveDate: " + ri.getEffectiveDate().toString());
      logger.debug("DUIP@getRateDateFromPostedCombo::EffectiveDateFormatted: " + effectiveDate);
      //--Ticket#575--22Aug2004--end--//
    }
    catch(Exception e)
    {
			logger.warn("DUIP@Exception getting the RateInfo to propagate to db: " + e);
    }

		return effectiveDate;
	}


	// Composite posted interest rate line is displaied on the screen in the
	// following format:
	// #.##% [Max Discount] | #.##% | MMM DD YYYY | HH:MM EST | SC:MSC
	// getPostedIntRateFromCombo(String[] acombolinein)) function parses this line and
	// returns the posted interest rate. This string is converted to
	// the double in Deal Entity.

  //--Release2.1--//
  //// The logic is changed now. ATNServlet passes Id for this composite comboline.
  //// Based on this lineId before the propagation to the database the
	public String getDealPostedRateFromPostedCombo(String[] acombolinein)
	{
    String pricingProfileId = parseCompositeRateId(acombolinein);
    logger.debug("DUIP@getDealPostedRateFromPostedCombo::CompositeIndex: " + pricingProfileId);
    String postedRate = "";

    try
    {
			RateInfo ri = getRateInfoToDatabase(new Integer(pricingProfileId).intValue());

      postedRate = new Double(ri.getPostedRate()).toString();
      logger.debug("DUIP@getDealPostedRateFromPostedCombo::DealPostedRate: " + ri.getPostedRate());
    }
    catch(Exception e)
    {
      logger.debug("DUIP@Exception getting the RateInfo to propagate to db: " + e);
    }

		return postedRate;
	}


	public String parseChoiceBoxId(String[] aval)
	{
		String retstr = "0";

		try
		{
			int intval =(int) Double.parseDouble(aval [ 0 ]);
			retstr = String.valueOf(intval);
		}
		catch(Exception e)
		{
			retstr = "0";
		}

		return retstr;
	}

	public String parseMtgProdBoxId(String[] aval)
	{
		String retstr = "0";

		try
		{
			int intval =(int) Double.parseDouble(aval [ 0 ]);
			retstr = String.valueOf(intval);
      logger.debug("DUIP@parseMtgProdBoxId: " + retstr);
		}
		catch(Exception e)
		{
			retstr = "0";
		}

		return retstr;
	}

	public String parseCompositeRateId(String[] aval)
	{
		String retstr = "0";

		try
		{
			int intval =(int) Double.parseDouble(aval [ 0 ]);
			retstr = String.valueOf(intval);
		}
		catch(Exception e)
		{
			retstr = "0";
		}

		return retstr;

	}


	/**
	 *
	 * @version 1.1 (July 4, 2008): added "Y" or trim() 
	 * @author MCM Team
	 */
	public String parseYesNo(String[] aval)
	{
		String retstr = "N";

		if (aval.length == 0) return retstr;
		if (aval [ 0 ] == null) return retstr;
		if ("yes".equalsIgnoreCase(aval [ 0 ].trim())) return "Y";
        if ("Y".equalsIgnoreCase(aval [ 0 ].trim())) return "Y";

		return retstr;

	}

	public static String getValidString(String avalue)
	{
		if (avalue == null) avalue = "";

		return avalue;

	}

  //--Ticket#575--16Sep2004--start--//
  public String getInterestTypeIdFromServlet(String sMtgProdId)
  {
    logger.debug("DUIP@getPaymentTermIdFromServlet::Start" + sMtgProdId);

    String sIntTypeId = "0"; // default it to zero.

    String intTypeId = getInterestTypeId(sMtgProdId);
    sIntTypeId = new Integer(intTypeId).toString();
    logger.debug("DUIP@getMtgProdId::IntTypeId: " + sIntTypeId);


    return sIntTypeId;
  }


  public String getInterestTypeId(String mtgProdId)
  {
    int interestTypeId = 0; //by default set to zero if something goes wrong.
    if (mtgProdId == null || mtgProdId.trim().length() ==0 ) {
        return "0";
    }
      try
      {
        JdbcExecutor jExec = srk.getJdbcExecutor();

        String sql =
         "select m.INTERESTTYPEID" +
         " FROM  mtgprod m " +
         " WHERE m.MTGPRODID = " + mtgProdId;

         int key = jExec.execute(sql);
         while(jExec.next(key))
         {
           interestTypeId  = jExec.getInt(key, 1);
           break;
         }
         jExec.closeData(key);

       }
       catch(Exception e)
       {
         logger.error("ALH@getIngestionMtgProdStatusId Exception at getting statusId ");
       }
		//logger.debug("DUIP@getInterestTypeIdFromServlet: " + interestTypeId);

       return ""+interestTypeId;
    }
 //--Ticket#575--16Sep2004--end--//

 //--Ticket#1773--18July2005--start--//
 public String getDealPrimeIndexRateFromPostedCombo(String[] acombolinein)
 {
   String pricingProfileId = parseCompositeRateId(acombolinein);
   logger.debug("DUIP@getDealPrimeRateFromPostedCombo::CompositeIndex: " + pricingProfileId);
   String primeRate = "";

   try
   {
			RateInfo ri = getRateInfoToDatabase(new Integer(pricingProfileId).intValue());

     primeRate = new Double(ri.getPrimeIndexRate()).toString();
     logger.debug("DUIP@getDealPrimeRateFromPostedCombo::DealPrimeRate: " + ri.getPrimeIndexRate());
   }
   catch(Exception e)
   {
     logger.debug("DUIP@Exception getting the PrimeRate to propagate to db: " + e);
   }

   return primeRate;
 }

 public String getDealPrimeBaseAdjFromPostedCombo(String[] acombolinein)
 {
   String pricingProfileId = parseCompositeRateId(acombolinein);
   logger.debug("DUIP@getDealPrimeRateFromPostedCombo::CompositeIndex: " + pricingProfileId);
   String primeBaseAdj = "";

   try
   {
			RateInfo ri = getRateInfoToDatabase(new Integer(pricingProfileId).intValue());

     primeBaseAdj = new Double(ri.getPrimeBaseAdj()).toString();
     logger.debug("DUIP@getDealPrimeBaseAdjFromPostedCombo::DealPrimeBaseAdj: " + ri.getPrimeBaseAdj());
   }
   catch(Exception e)
   {
     logger.debug("DUIP@Exception getting the PrimeBaseAdj to propagate to db: " + e);
   }

   return primeBaseAdj;
 }

 public String getDealPrimeIndexIdFromPostedCombo(String[] acombolinein)
 {
   String pricingProfileId = parseCompositeRateId(acombolinein);
   logger.debug("DUIP@getDealPrimeRateFromPostedCombo::CompositeIndex: " + pricingProfileId);
   String primeIndexId = "";

   try
   {
			RateInfo ri = getRateInfoToDatabase(new Integer(pricingProfileId).intValue());

     primeIndexId = new Integer(ri.getPrimeIndexId()).toString();
     logger.debug("DUIP@getDealPrimeIndexIdFromPostedCombo::DealPrimeIndexId: " + ri.getPrimeIndexId());
   }
   catch(Exception e)
   {
     logger.debug("DUIP@Exception getting the PrimeIndexid to propagate to db: " + e);
   }

   return primeIndexId;
 }
 //--Ticket#1773--18July2005--end--//

 //--Ticket#1736--18July2005--start--//
 public String getDealTeaserDiscountFromPostedCombo(String[] acombolinein)
 {
   //logger = SysLog.getSysLogger("DUIP");

   String pricingProfileId = parseCompositeRateId(acombolinein);
   //logger.debug("DUIP@getDealPrimeRateFromPostedCombo::CompositeIndex: " + pricingProfileId);
   String teaserDiscount = "";

   try
   {
			RateInfo ri = getRateInfoToDatabase(new Integer(pricingProfileId).intValue());

     teaserDiscount = new Double(ri.getTeaserDiscount()).toString();
     logger.debug("DUIP@getDealTeaserDiscountFromPostedCombo::DealTeaserDiscount: " + ri.getTeaserDiscount());
   }
   catch(Exception e)
   {
     logger.debug("DUIP@Exception getting the TeaserDiscount to propagate to db: " + e);
   }

   return teaserDiscount;
 }

 public String getDealTeaserTermFromPostedCombo(String[] acombolinein)
 {
   String pricingProfileId = parseCompositeRateId(acombolinein);
   //logger.debug("DUIP@getDealPrimeRateFromPostedCombo::CompositeIndex: " + pricingProfileId);
   String teaserTerm = "";

   try
   {
			RateInfo ri = getRateInfoToDatabase(new Integer(pricingProfileId).intValue());

     teaserTerm = new Integer(ri.getTeaserTerm()).toString();
     logger.debug("DUIP@getDealTeaserTermFromPostedCombo::DealTeaserTerm: " + ri.getTeaserTerm());
   }
   catch(Exception e)
   {
     logger.debug("DUIP@Exception getting the TeaserTerm to propagate to db: " + e);
   }

   return teaserTerm;
 }

 //--Ticket#1736--18July2005--end--//

}

