package mosApp.MosSystem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.WebActions;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.RadioButtonGroup;
import com.iplanet.jato.view.html.StaticTextField;


/**
 *
 * IMPORTANT!!! The commented out elements not deleted since DJ hasn't signed the
 * final specs/html layout yet. They reduced number of field displaying in the
 * repeatable section, but can come back with the original layout.
 *
 */
public class pgLifeDisabilityInsuranceRepeated1TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgLifeDisabilityInsuranceRepeated1TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doLifeDisabilityInsuranceInfoModel.class);
		registerChildren();
		initialize();
	}

	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStBorrowerFirstName().setValue(CHILD_STBORROWERFIRSTNAME_RESET_VALUE);
		getStBorrowerMiddleInitial().setValue(CHILD_STBORROWERMIDDLEINITIAL_RESET_VALUE);
		getStBorrowerLastName().setValue(CHILD_STBORROWERLASTNAME_RESET_VALUE);
		getStPrimaryBorrowerFlag().setValue(CHILD_STPRIMARYBORROWERFLAG_RESET_VALUE);
		////getStBorrowerDateOfBirth().setValue(CHILD_STBORROWERDATEOFBIRTH_RESET_VALUE);
		////getTxBorrowerGender().setValue(CHILD_TXBORROWERGENDER_RESET_VALUE);
		getCbBorrowerGender().setValue(CHILD_CBBORROWERGENDER_RESET_VALUE);
		getStBorrowerAge().setValue(CHILD_STBORROWERAGE_RESET_VALUE);
		////getTxLifePercCoverageReq().setValue(CHILD_TXLIFEPERCCOVERAGEREQ_RESET_VALUE);
		////getStLifePremium().setValue(CHILD_STLIFEPREMIUM_RESET_VALUE);
		////getStDisabilityPremium().setValue(CHILD_STDISABILITYPREMIUM_RESET_VALUE);
		getRbLifeInsurance().setValue(CHILD_RBLIFEINSURANCE_RESET_VALUE);
		getRbDisabilityInsurance().setValue(CHILD_RBDISABILITYINSURANCE_RESET_VALUE);
		////getStCombinedLDPremium().setValue(CHILD_STCOMBINEDLDPREMIUM_RESET_VALUE);
		getHdBorrowerId().setValue(CHILD_HDBORROWERID_RESET_VALUE);
		getHdBorrowerGenderId().setValue(CHILD_HDBORROWERGENDERID_RESET_VALUE);
		//getStBorrowerSmoker().setValue(CHILD_STSMOKER_RESET_VALUE);
		getCbBorrowerSmoker().setValue(CHILD_CBBORROWERSMOKER_RESET_VALUE);
	}

	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STBORROWERFIRSTNAME,StaticTextField.class);
		registerChild(CHILD_STBORROWERMIDDLEINITIAL,StaticTextField.class);
		registerChild(CHILD_STBORROWERLASTNAME,StaticTextField.class);
		registerChild(CHILD_STPRIMARYBORROWERFLAG,StaticTextField.class);
		////registerChild(CHILD_STBORROWERDATEOFBIRTH,StaticTextField.class);
		////registerChild(CHILD_TXBORROWERGENDER,TextField.class);
		registerChild(CHILD_CBBORROWERGENDER,ComboBox.class);
		registerChild(CHILD_STBORROWERAGE,StaticTextField.class);
		////registerChild(CHILD_TXLIFEPERCCOVERAGEREQ,TextField.class);
		registerChild(CHILD_STBORROWERAGE,StaticTextField.class);
		////registerChild(CHILD_STLIFEPREMIUM,StaticTextField.class);
		////registerChild(CHILD_STDISABILITYPREMIUM,StaticTextField.class);
		////registerChild(CHILD_STCOMBINEDLDPREMIUM,StaticTextField.class);
		registerChild(CHILD_RBLIFEINSURANCE,RadioButtonGroup.class);
		registerChild(CHILD_RBDISABILITYINSURANCE,RadioButtonGroup.class);
		registerChild(CHILD_HDBORROWERID,HiddenField.class);
		registerChild(CHILD_HDBORROWERGENDERID,HiddenField.class);
		//registerChild(CHILD_STSMOKER,StaticTextField.class);
		registerChild(CHILD_CBBORROWERSMOKER,StaticTextField.class);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoLifeDisabilityInsuranceInfoModel());

				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    logger = SysLog.getSysLogger("pgLDITV");
		handler.pageGetState(this.getParentViewBean());

    String acceptedStr = BXResources.getGenericMsg("ACCEPTED_LABEL", handler.getTheSessionState().getLanguageId());
    String refusedStr = BXResources.getGenericMsg("REFUSED_LABEL", handler.getTheSessionState().getLanguageId());
    String ineligibleStr = BXResources.getGenericMsg("INELIGIBLE_LABEL", handler.getTheSessionState().getLanguageId());

    //Setup Options for RadioButton groups
    rbLifeInsuranceOptions.setOptions(new String[]{acceptedStr, refusedStr, ineligibleStr},new String[]{"A", "R", "I"});
    rbDisabilityInsuranceOptions.setOptions(new String[]{acceptedStr, refusedStr, ineligibleStr},new String[]{"A", "R", "I"});

    //Set up all ComboBox options
    cbBorrowerGenderOptions.populate(getRequestContext());
    cbBorrowerSmokerOptions.populate(getRequestContext());

		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}

	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
    boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			int rowNum = this.getTileIndex();
      LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
			handler.pageGetState(this.getParentViewBean());
			boolean retval = handler.checkDisplayThisRow("Repeated1");
      logger.debug("pgLDITV_I@nextTile::Retval: " + retval);

			handler.pageSaveState();
			return retval;
		}

		return movedToRow;
	}

  public void setCurrentDisplayOffset(int offset) throws Exception
  {
    setWebActionModelOffset(offset);
    handleWebAction(WebActions.ACTION_FIRST);
  }

	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STBORROWERFIRSTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsuranceInfoModel(),
				CHILD_STBORROWERFIRSTNAME,
				doLifeDisabilityInsuranceInfoModel.FIELD_DFBORROWERFIRSTNAME,
				CHILD_STBORROWERFIRSTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBORROWERMIDDLEINITIAL))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsuranceInfoModel(),
				CHILD_STBORROWERMIDDLEINITIAL,
				doLifeDisabilityInsuranceInfoModel.FIELD_DFBORROWERMIDDLEINITIAL,
				CHILD_STBORROWERMIDDLEINITIAL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBORROWERLASTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsuranceInfoModel(),
				CHILD_STBORROWERLASTNAME,
				doLifeDisabilityInsuranceInfoModel.FIELD_DFBORROWERLASTNAME,
				CHILD_STBORROWERLASTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPRIMARYBORROWERFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsuranceInfoModel(),
				CHILD_STPRIMARYBORROWERFLAG,
				doLifeDisabilityInsuranceInfoModel.FIELD_DFPRIMARYBORROWERFLAG,
				CHILD_STPRIMARYBORROWERFLAG_RESET_VALUE,
				null);
			return child;
		}
    /**
		else
		if (name.equals(CHILD_STBORROWERDATEOFBIRTH))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsuranceInfoModel(),
				CHILD_STBORROWERDATEOFBIRTH,
				doLifeDisabilityInsuranceInfoModel.FIELD_DFBORROWERBIRTHDATE,
				CHILD_STBORROWERDATEOFBIRTH_RESET_VALUE,
				null);
			return child;
		}
    else
		if (name.equals(CHILD_TXBORROWERGENDER))
		{
			TextField child = new TextField(this,
				getdoLifeDisabilityInsuranceInfoModel(),
				CHILD_TXBORROWERGENDER,
				doLifeDisabilityInsuranceInfoModel.FIELD_DFBORROWERGENDERDESC,
				CHILD_TXBORROWERGENDER_RESET_VALUE,
				null);
			return child;
		}
    **/
		else
		if (name.equals(CHILD_STBORROWERAGE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsuranceInfoModel(),
				CHILD_STBORROWERAGE,
				doLifeDisabilityInsuranceInfoModel.FIELD_DFAGE,
				CHILD_STBORROWERAGE_RESET_VALUE,
				null);
			return child;
		}
    /**
		else
		if (name.equals(CHILD_STLIFEPREMIUM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsuranceInfoModel(),
				CHILD_STLIFEPREMIUM,
				doLifeDisabilityInsuranceInfoModel.FIELD_DFLIFEPREMIUM,
				CHILD_STLIFEPREMIUM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDISABILITYPREMIUM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsuranceInfoModel(),
				CHILD_STDISABILITYPREMIUM,
				doLifeDisabilityInsuranceInfoModel.FIELD_DFDISABILITYPREMIUM,
				CHILD_STDISABILITYPREMIUM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCOMBINEDLDPREMIUM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsuranceInfoModel(),
				CHILD_STCOMBINEDLDPREMIUM,
				doLifeDisabilityInsuranceInfoModel.FIELD_DFCOMBINEDLDPREMIUM,
				CHILD_STCOMBINEDLDPREMIUM_RESET_VALUE,
				null);
			return child;
		}
    else
		if (name.equals(CHILD_TXLIFEPERCCOVERAGEREQ))
		{
			TextField child = new TextField(this,
				getdoLifeDisabilityInsuranceInfoModel(),
				CHILD_TXLIFEPERCCOVERAGEREQ,
				doLifeDisabilityInsuranceInfoModel.FIELD_DFLIFEPERCENTCOVERAGEREQ,
				CHILD_TXLIFEPERCCOVERAGEREQ_RESET_VALUE,
				null);
			return child;
		}
    **/
		else
		if (name.equals(CHILD_RBLIFEINSURANCE))
		{
			RadioButtonGroup child = new RadioButtonGroup( this,
				getdoLifeDisabilityInsuranceInfoModel(),
				CHILD_RBLIFEINSURANCE,
				getdoLifeDisabilityInsuranceInfoModel().FIELD_DFLIFESTATUSVALUE,
				CHILD_RBLIFEINSURANCE_RESET_VALUE,
				null);
      child.setLabelForNoneSelected("");
			child.setOptions(rbLifeInsuranceOptions);
			return child;
		}
		else
		if (name.equals(CHILD_RBDISABILITYINSURANCE))
		{
			RadioButtonGroup child = new RadioButtonGroup( this,
				getdoLifeDisabilityInsuranceInfoModel(),
				CHILD_RBDISABILITYINSURANCE,
				getdoLifeDisabilityInsuranceInfoModel().FIELD_DFLDISABILITYSTATUSVALUE,
				CHILD_RBDISABILITYINSURANCE_RESET_VALUE,
				null);
      child.setLabelForNoneSelected("");
			child.setOptions(rbDisabilityInsuranceOptions);
			return child;
		}
    else
		if (name.equals(CHILD_HDBORROWERID))
		{
			HiddenField child = new HiddenField(this,
				getdoLifeDisabilityInsuranceInfoModel(),
				CHILD_HDBORROWERID,
				getdoLifeDisabilityInsuranceInfoModel().FIELD_DFBORROWERID,
				CHILD_HDBORROWERID_RESET_VALUE,
				null);
			return child;
		}
		else
    if (name.equals(CHILD_HDBORROWERGENDERID))
		{
			HiddenField child = new HiddenField(this,
				getdoLifeDisabilityInsuranceInfoModel(),
				CHILD_HDBORROWERGENDERID,
				getdoLifeDisabilityInsuranceInfoModel().FIELD_DFBORROWERGENDERID,
				CHILD_HDBORROWERGENDERID_RESET_VALUE,
				null);
			return child;
		}
    /**
		else
		if (name.equals(CHILD_STSMOKER))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsuranceInfoModel(),
				CHILD_STSMOKER,
				doLifeDisabilityInsuranceInfoModel.FIELD_DFSMOKER,
				CHILD_STSMOKER_RESET_VALUE,
				null);
			return child;
		}
    **/
		else
		if (name.equals(CHILD_CBBORROWERSMOKER))
		{
			ComboBox child = new ComboBox( this,
				getdoLifeDisabilityInsuranceInfoModel(),
				CHILD_CBBORROWERSMOKER,
				getdoLifeDisabilityInsuranceInfoModel().FIELD_DFBORROWERSMOKERID,
				CHILD_CBBORROWERSMOKER_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbBorrowerSmokerOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBBORROWERGENDER))
		{
			ComboBox child = new ComboBox( this,
				getdoLifeDisabilityInsuranceInfoModel(),
				CHILD_CBBORROWERGENDER,
				getdoLifeDisabilityInsuranceInfoModel().FIELD_DFBORROWERGENDERID,
				CHILD_CBBORROWERGENDER_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbBorrowerGenderOptions);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBorrowerFirstName()
	{
		return (StaticTextField)getChild(CHILD_STBORROWERFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBorrowerMiddleInitial()
	{
		return (StaticTextField)getChild(CHILD_STBORROWERMIDDLEINITIAL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBorrowerLastName()
	{
		return (StaticTextField)getChild(CHILD_STBORROWERLASTNAME);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStBorrowerSmoker()
	{
		return (StaticTextField)getChild(CHILD_STSMOKER);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStPrimaryBorrowerFlag()
	{
		return (StaticTextField)getChild(CHILD_STPRIMARYBORROWERFLAG);
	}

	/**
	 *
	 *
	 */
	//public StaticTextField getStBorrowerDateOfBirth()
	//{
	//	return (StaticTextField)getChild(CHILD_STBORROWERDATEOFBIRTH);
	//}

	/**
	 *
	 *
	 */
  //public TextField getTxBorrowerGender()
	//{
	//	return (TextField)getChild(CHILD_TXBORROWERGENDER);
	//}

	/**
	 *
	 *
	 */
	public StaticTextField getStBorrowerAge()
	{
		return (StaticTextField)getChild(CHILD_STBORROWERAGE);
	}

	/**
	 *
	 *
	 */
  /*
	public TextField getTxLifePercCoverageReq()
	{
		return (TextField)getChild(CHILD_TXLIFEPERCCOVERAGEREQ);
	}
  */

	/**
	 *
	 *
	 */
	////public StaticTextField getStLifePremium()
	////{
	////	return (StaticTextField)getChild(CHILD_STLIFEPREMIUM);
	////}

	/**
	 *
	 *
	 */
	////public StaticTextField getStDisabilityPremium()
	////{
	////	return (StaticTextField)getChild(CHILD_STDISABILITYPREMIUM);
	////}

	/**
	 *
	 *
	 */
	////public StaticTextField getStCombinedLDPremium()
	////{
	////	return (StaticTextField)getChild(CHILD_STCOMBINEDLDPREMIUM);
	////}

	/**
	 *
	 *
	 */
	public RadioButtonGroup getRbLifeInsurance()
	{
		return (RadioButtonGroup)getChild(CHILD_RBLIFEINSURANCE);
	}

	/**
	 *
	 *
	 */
	public RadioButtonGroup getRbDisabilityInsurance()
	{
		return (RadioButtonGroup)getChild(CHILD_RBDISABILITYINSURANCE);
	}

	/**
	 *
	 *
	 */
  /**
	public void handleBtRecalculateRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this.getParentViewBean());

		handler.handleRecalculate(event);

		handler.postHandlerProtocol();

	}
  **/

	/**
	 *
	 *
	 */
	public String endBtDetailsDisplay(ChildContentDisplayEvent event)
	{
		return event.getContent();
	}

	/**
	 *
	 *
	 */
	public HiddenField getHdBorrowerId()
	{
		return (HiddenField)getChild(CHILD_HDBORROWERID);
	}

	/**
	 *
	 *
	 */
	public HiddenField getHdBorrowerGenderId()
	{
		return (HiddenField)getChild(CHILD_HDBORROWERGENDERID);
	}

	/**
	 *
	 *
	 */
	public ComboBox getCbBorrowerGender()
	{
		return (ComboBox)getChild(CHILD_CBBORROWERGENDER);
	}

	/**
	 *
	 *
	 */
	static class CbBorrowerGenderOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbBorrowerGenderOptionList(){  }

		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                            "BORROWERGENDER", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}

	/**
	 *
	 *
	 */
	public ComboBox getCbBorrowerSmoker()
	{
		return (ComboBox)getChild(CHILD_CBBORROWERSMOKER);
	}

	/**
	 *
	 *
	 */
	static class CbBorrowerSmokerOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbBorrowerSmokerOptionList(){  }

		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                            "BORROWERSMOKER", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}

	/**
	 *
	 *
	 */
	public doLifeDisabilityInsuranceInfoModel getdoLifeDisabilityInsuranceInfoModel()
	{
		if (doLifeDisabilityInsuranceInfo == null)
			doLifeDisabilityInsuranceInfo = (doLifeDisabilityInsuranceInfoModel) getModel(doLifeDisabilityInsuranceInfoModel.class);
		return doLifeDisabilityInsuranceInfo;
	}

	/**
	 *
	 *
	 */
	public void setdoLifeDisabilityInsuranceInfoModel(doLifeDisabilityInsuranceInfoModel model)
	{
			doLifeDisabilityInsuranceInfo = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STBORROWERFIRSTNAME="stBorrowerFirstName";
	public static final String CHILD_STBORROWERFIRSTNAME_RESET_VALUE="";

	public static final String CHILD_STBORROWERMIDDLEINITIAL="stBorrowerMiddleInitial";
	public static final String CHILD_STBORROWERMIDDLEINITIAL_RESET_VALUE="";

	public static final String CHILD_STBORROWERLASTNAME="stBorrowerLastName";
	public static final String CHILD_STBORROWERLASTNAME_RESET_VALUE="";

	public static final String CHILD_STPRIMARYBORROWERFLAG="stPrimaryBorrowerFlag";
	public static final String CHILD_STPRIMARYBORROWERFLAG_RESET_VALUE="";

	////public static final String CHILD_STBORROWERDATEOFBIRTH="stBorrowerDateOfBirth";
	////public static final String CHILD_STBORROWERDATEOFBIRTH_RESET_VALUE="";

  public static final String CHILD_STSMOKER="stBorrowerSmoker";
	public static final String CHILD_STSMOKER_RESET_VALUE="";

	public static final String CHILD_STBORROWERAGE="stBorrowerAge";
	public static final String CHILD_STBORROWERAGE_RESET_VALUE="";

	////public static final String CHILD_TXBORROWERGENDER="txBorrowerGender";
	////public static final String CHILD_TXBORROWERGENDER_RESET_VALUE="";

	////public static final String CHILD_TXLIFEPERCCOVERAGEREQ="txLifePercCoverageReq";
	////public static final String CHILD_TXLIFEPERCCOVERAGEREQ_RESET_VALUE="";

	////public static final String CHILD_STLIFEPREMIUM="stLifePremium";
	////public static final String CHILD_STLIFEPREMIUM_RESET_VALUE="";

	////public static final String CHILD_STDISABILITYPREMIUM="stDisabilityPremium";
	////public static final String CHILD_STDISABILITYPREMIUM_RESET_VALUE="";

	////public static final String CHILD_STCOMBINEDLDPREMIUM="stCombinedLDPremium";
	////public static final String CHILD_STCOMBINEDLDPREMIUM_RESET_VALUE="";

	protected OptionList rbLifeInsuranceOptions=new OptionList(new String[]{},new String[]{});
	public static final String CHILD_RBLIFEINSURANCE="rbLifeInsurance";
	public static final String CHILD_RBLIFEINSURANCE_RESET_VALUE="";

	protected OptionList rbDisabilityInsuranceOptions=new OptionList(new String[]{},new String[]{});
	public static final String CHILD_RBDISABILITYINSURANCE="rbDisabilityInsurance";
	public static final String CHILD_RBDISABILITYINSURANCE_RESET_VALUE="";

	public static final String CHILD_HDBORROWERID="hdBorrowerId";
	public static final String CHILD_HDBORROWERID_RESET_VALUE="";

	public static final String CHILD_HDBORROWERGENDERID="hdBorrowerGenderId";
	public static final String CHILD_HDBORROWERGENDERID_RESET_VALUE="";

  public static final String CHILD_CBBORROWERGENDER="cbBorrowerGender";
	public static final String CHILD_CBBORROWERGENDER_RESET_VALUE="";

  private CbBorrowerGenderOptionList cbBorrowerGenderOptions=new CbBorrowerGenderOptionList();

  public static final String CHILD_CBBORROWERSMOKER="cbBorrowerSmoker";
	public static final String CHILD_CBBORROWERSMOKER_RESET_VALUE="";

  private CbBorrowerSmokerOptionList cbBorrowerSmokerOptions=new CbBorrowerSmokerOptionList();

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doLifeDisabilityInsuranceInfoModel doLifeDisabilityInsuranceInfo=null;
	private LifeDisabilityInsuranceHandler handler=new LifeDisabilityInsuranceHandler();
  public SysLogger logger;

}

