package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doDealEntryApplicantSelectModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerID();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerID(java.math.BigDecimal value);

	
    /**
     * 
     * 
     */
    public java.math.BigDecimal getDfCopyId();

    
    /**
     * 
     * 
     */
    public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPrimaryBorrowerFlag();

	
	/**
	 * 
	 * 
	 */
	public void setDfPrimaryBorrowerFlag(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfFirstName();

	
	/**
	 * 
	 * 
	 */
	public void setDfFirstName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfMiddleName();

	
	/**
	 * 
	 * 
	 */
	public void setDfMiddleName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLastName();

	
	/**
	 * 
	 * 
	 */
	public void setDfLastName(String value);

    /**
     * 
     * 
     */
    public String getDfSuffix();

    
    /**
     * 
     * 
     */
    public void setDfSuffix(String value);
    
	/**
	 * 
	 * 
	 */
	public String getDfHomePhoneNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfHomePhoneNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfWorkPhoneNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfWorkPhoneNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfWorkPhoneExt();

	
	/**
	 * 
	 * 
	 */
	public void setDfWorkPhoneExt(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfApplicantType();

	
	/**
	 * 
	 * 
	 */
	public void setDfApplicantType(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLanguagePreferenceDescr();

	
	/**
	 * 
	 * 
	 */
	public void setDfLanguagePreferenceDescr(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFBORROWERID="dfBorrowerID";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFPRIMARYBORROWERFLAG="dfPrimaryBorrowerFlag";
	public static final String FIELD_DFFIRSTNAME="dfFirstName";
	public static final String FIELD_DFMIDDLENAME="dfMiddleName";
	public static final String FIELD_DFLASTNAME="dfLastName";
    public static final String FIELD_DFSUFFIX="dfSuffix";
	public static final String FIELD_DFHOMEPHONENUMBER="dfHomePhoneNumber";
	public static final String FIELD_DFWORKPHONENUMBER="dfWorkPhoneNumber";
	public static final String FIELD_DFWORKPHONEEXT="dfWorkPhoneExt";
	public static final String FIELD_DFAPPLICANTTYPE="dfApplicantType";
	public static final String FIELD_DFLANGUAGEPREFERENCEDESCR="dfLanguagePreferenceDescr";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

