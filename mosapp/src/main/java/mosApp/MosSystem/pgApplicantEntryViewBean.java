package mosApp.MosSystem;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;
import javax.servlet.ServletException;
import mosApp.SQLConnectionManagerImpl;

import MosSystem.Mc;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.view.CommandFieldDescriptor;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.ViewBeanBase;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.ChildDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HREF;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.HtmlDisplayFieldBase;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

import com.filogix.express.web.jato.ExpressViewBeanBase;

/**
 * <p>Title: pgApplicantEntryViewBean</p>
 * <p>Description: View Bean for applicant entry page.</p>
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 * <p>Company: Filogix Inc.</p>
 * 
 * @version 1.1  <br>
 * Date: 08/02/2006 <br>
 * @author: NBC/PP Implementation Team<br>
 * Change:  <br>
 *	added new elements BorrowerGender, Smoker-NonSmoker, Solicitaion, Preferred 
 *	method of contact, employee number. New tile for Borrower Identification is 
 *	also added. <br>
 *	- Added following methods<br>
 *		getCbSmokeStatus()<br>
 *		getCbSolicitation()
 *		getTxEmployeeNo()
 *		getCbPreferredContactMethod()
 *		getStTargetIdentification()
 *		getBtAddAdditionalApplicantIdentification()
 *		getRepeatedIdentification()
 *		handleBtAddAdditionalApplicantIdentificationRequest
 *			(RequestInvocationEvent event)
 *		endBtAddAdditionalApplicantIdentificationDisplay(
 *				ChildContentDisplayEvent event)
 *
 *	- modified following methods <br>
 *
 *		registerChildren() <br>
 *		resetChildren() <br>
 *		createChild(String)<br>
 *		beginDisplay(DisplayEvent event)
 *
 */
public class pgApplicantEntryViewBean
  extends ExpressViewBeanBase
{
  /**
   *
   *
   */
  public pgApplicantEntryViewBean()
  {
    super(PAGE_NAME);
    setDefaultDisplayURL(DEFAULT_DISPLAY_URL);

    registerChildren();

    initialize();

  }

  /**
   *
   *
   */
  protected void initialize()
  {
  }

  ////////////////////////////////////////////////////////////////////////////////
  // Child manipulation methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  protected View createChild(String name)
  {
	  View superReturn = super.createChild(name);
	  if (superReturn != null) {
		  return superReturn;
	  } else if(name.equals(CHILD_TBDEALID))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBDEALID,
        CHILD_TBDEALID,
        CHILD_TBDEALID_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_CBPAGENAMES))
    {
      ComboBox child = new ComboBox(this,
        getDefaultModel(),
        CHILD_CBPAGENAMES,
        CHILD_CBPAGENAMES,
        CHILD_CBPAGENAMES_RESET_VALUE,
        null);
      //--Release2.1--//
      //Modified to set NonSelected Label from CHILD_CBPAGENAMES_NONSELECTED_LABEL
      //--> By Billy 05Nov2002
      //child.setLabelForNoneSelected("Choose a Page");
      child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
      //==========================================================================
      child.setOptions(cbPageNamesOptions);
      return child;
    }
    else
    if(name.equals(CHILD_BTPROCEED))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTPROCEED,
        CHILD_BTPROCEED,
        CHILD_BTPROCEED_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF1))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF1,
        CHILD_HREF1,
        CHILD_HREF1_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF2))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF2,
        CHILD_HREF2,
        CHILD_HREF2_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF3))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF3,
        CHILD_HREF3,
        CHILD_HREF3_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF4))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF4,
        CHILD_HREF4,
        CHILD_HREF4_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF5))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF5,
        CHILD_HREF5,
        CHILD_HREF5_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF6))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF6,
        CHILD_HREF6,
        CHILD_HREF6_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF7))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF7,
        CHILD_HREF7,
        CHILD_HREF7_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF8))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF8,
        CHILD_HREF8,
        CHILD_HREF8_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF9))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF9,
        CHILD_HREF9,
        CHILD_HREF9_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF10))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF10,
        CHILD_HREF10,
        CHILD_HREF10_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_STPAGELABEL))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPAGELABEL,
        CHILD_STPAGELABEL,
        CHILD_STPAGELABEL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STCOMPANYNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STCOMPANYNAME,
        CHILD_STCOMPANYNAME,
        CHILD_STCOMPANYNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STTODAYDATE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STTODAYDATE,
        CHILD_STTODAYDATE,
        CHILD_STTODAYDATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STUSERNAMETITLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STUSERNAMETITLE,
        CHILD_STUSERNAMETITLE,
        CHILD_STUSERNAMETITLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_BTWORKQUEUELINK))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTWORKQUEUELINK,
        CHILD_BTWORKQUEUELINK,
        CHILD_BTWORKQUEUELINK_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_CHANGEPASSWORDHREF))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_CHANGEPASSWORDHREF,
        CHILD_CHANGEPASSWORDHREF,
        CHILD_CHANGEPASSWORDHREF_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_BTTOOLHISTORY))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTTOOLHISTORY,
        CHILD_BTTOOLHISTORY,
        CHILD_BTTOOLHISTORY_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_BTTOONOTES))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTTOONOTES,
        CHILD_BTTOONOTES,
        CHILD_BTTOONOTES_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_BTTOOLSEARCH))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTTOOLSEARCH,
        CHILD_BTTOOLSEARCH,
        CHILD_BTTOOLSEARCH_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_BTTOOLLOG))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTTOOLLOG,
        CHILD_BTTOOLLOG,
        CHILD_BTTOOLLOG_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_STERRORFLAG))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STERRORFLAG,
        CHILD_STERRORFLAG,
        CHILD_STERRORFLAG_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_DETECTALERTTASKS))
    {
      HiddenField child = new HiddenField(this,
        getDefaultModel(),
        CHILD_DETECTALERTTASKS,
        CHILD_DETECTALERTTASKS,
        CHILD_DETECTALERTTASKS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_BTPREVTASKPAGE))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTPREVTASKPAGE,
        CHILD_BTPREVTASKPAGE,
        CHILD_BTPREVTASKPAGE_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_STPREVTASKPAGELABEL))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPREVTASKPAGELABEL,
        CHILD_STPREVTASKPAGELABEL,
        CHILD_STPREVTASKPAGELABEL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_BTNEXTTASKPAGE))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTNEXTTASKPAGE,
        CHILD_BTNEXTTASKPAGE,
        CHILD_BTNEXTTASKPAGE_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_STNEXTTASKPAGELABEL))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STNEXTTASKPAGELABEL,
        CHILD_STNEXTTASKPAGELABEL,
        CHILD_STNEXTTASKPAGELABEL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STTASKNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STTASKNAME,
        CHILD_STTASKNAME,
        CHILD_STTASKNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_SESSIONUSERID))
    {
      HiddenField child = new HiddenField(this,
        getDefaultModel(),
        CHILD_SESSIONUSERID,
        CHILD_SESSIONUSERID,
        CHILD_SESSIONUSERID_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_HDAPPLICANTID))
    {
      HiddenField child = new HiddenField(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_HDAPPLICANTID,
        doApplicantEntryApplicantSelectModel.FIELD_DFBORROWERID,
        CHILD_HDAPPLICANTID_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_HDAPPLICANTCOPYID))
    {
      HiddenField child = new HiddenField(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_HDAPPLICANTCOPYID,
        doApplicantEntryApplicantSelectModel.FIELD_DFCOPYID,
        CHILD_HDAPPLICANTCOPYID_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_CBSALUTATION))
    {
      ComboBox child = new ComboBox(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_CBSALUTATION,
        doApplicantEntryApplicantSelectModel.FIELD_DFSALUTATIONID,
        CHILD_CBSALUTATION_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbSalutationOptions);
      return child;
    }
    else
    if(name.equals(CHILD_TXFIRSTNAME))
    {
      TextField child = new TextField(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_TXFIRSTNAME,
        doApplicantEntryApplicantSelectModel.FIELD_DFFIRSTNAME,
        CHILD_TXFIRSTNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXMIDDLEINITIALS))
    {
      TextField child = new TextField(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_TXMIDDLEINITIALS,
        doApplicantEntryApplicantSelectModel.FIELD_DFMIDDLENAME,
        CHILD_TXMIDDLEINITIALS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXLASTNAME))
    {
      TextField child = new TextField(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_TXLASTNAME,
        doApplicantEntryApplicantSelectModel.FIELD_DFLASTNAME,
        CHILD_TXLASTNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_CBAPPLICANTTYPE))
    {
      ComboBox child = new ComboBox(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_CBAPPLICANTTYPE,
        doApplicantEntryApplicantSelectModel.FIELD_DFBORROWERTYPEID,
        CHILD_CBAPPLICANTTYPE_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbApplicantTypeOptions);
      return child;
    }
    else
    if(name.equals(CHILD_CBAPPLICANTDOBMONTH))
    {
      ComboBox child = new ComboBox(this,
        getDefaultModel(),
        CHILD_CBAPPLICANTDOBMONTH,
        CHILD_CBAPPLICANTDOBMONTH,
        CHILD_CBAPPLICANTDOBMONTH_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbApplicantDOBMonthOptions);
      return child;
    }
    else
    if(name.equals(CHILD_TXAPPLICANTDOBDAY))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TXAPPLICANTDOBDAY,
        CHILD_TXAPPLICANTDOBDAY,
        CHILD_TXAPPLICANTDOBDAY_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXAPPLICANTDOBYEAR))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TXAPPLICANTDOBYEAR,
        CHILD_TXAPPLICANTDOBYEAR,
        CHILD_TXAPPLICANTDOBYEAR_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_CBMARTIALSTATUS))
    {
      ComboBox child = new ComboBox(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_CBMARTIALSTATUS,
        doApplicantEntryApplicantSelectModel.FIELD_DFMARITALSTATUSID,
        CHILD_CBMARTIALSTATUS_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbMartialStatusOptions);
      return child;
    }
    else
    if(name.equals(CHILD_TXSINNO))
    {
      TextField child = new TextField(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_TXSINNO,
        doApplicantEntryApplicantSelectModel.FIELD_DFSIN,
        CHILD_TXSINNO_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_CBCITIZENSHIP))
    {
      ComboBox child = new ComboBox(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_CBCITIZENSHIP,
        doApplicantEntryApplicantSelectModel.FIELD_DFCITIZENSHIPID,
        CHILD_CBCITIZENSHIP_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbCitizenshipOptions);
      return child;
    }
    else
    if(name.equals(CHILD_TXNOOFDEPENDANTS))
    {
      TextField child = new TextField(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_TXNOOFDEPENDANTS,
        doApplicantEntryApplicantSelectModel.FIELD_DFNOOFDEPENDANTS,
        CHILD_TXNOOFDEPENDANTS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_CBEXISTINGCLIENT))
    {
      ComboBox child = new ComboBox(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_CBEXISTINGCLIENT,
        doApplicantEntryApplicantSelectModel.FIELD_DFEXISTINGCLIENT,
        CHILD_CBEXISTINGCLIENT_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbExistingClientOptions);
      return child;
    }
    else
    if(name.equals(CHILD_TXREFERENCECLIENTNO))
    {
      TextField child = new TextField(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_TXREFERENCECLIENTNO,
        doApplicantEntryApplicantSelectModel.FIELD_DFCLIENTREFERENCENUMBER,
        CHILD_TXREFERENCECLIENTNO_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXNOOFTIMESBANKRUPT))
    {
      TextField child = new TextField(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_TXNOOFTIMESBANKRUPT,
        doApplicantEntryApplicantSelectModel.FIELD_DFNOOFBANKCRUPTS,
        CHILD_TXNOOFTIMESBANKRUPT_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_CBBANKRUPTCYSTATUS))
    {
      ComboBox child = new ComboBox(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_CBBANKRUPTCYSTATUS,
        doApplicantEntryApplicantSelectModel.FIELD_DFBANKRUPTCYSTATUSID,
        CHILD_CBBANKRUPTCYSTATUS_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbBankruptcyStatusOptions);
      return child;
    }
    else
    if(name.equals(CHILD_CBSTAFFOFLENDER))
    {
      ComboBox child = new ComboBox(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_CBSTAFFOFLENDER,
        doApplicantEntryApplicantSelectModel.FIELD_DFSTAFFOFLENDER,
        CHILD_CBSTAFFOFLENDER_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbStaffOfLenderOptions);
      return child;
    }
    else
    if(name.equals(CHILD_TXAPPLICANTHOMEPHONEAREACODE))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TXAPPLICANTHOMEPHONEAREACODE,
        CHILD_TXAPPLICANTHOMEPHONEAREACODE,
        CHILD_TXAPPLICANTHOMEPHONEAREACODE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXAPPLICANTHOMEPHONEEXCHANGE))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TXAPPLICANTHOMEPHONEEXCHANGE,
        CHILD_TXAPPLICANTHOMEPHONEEXCHANGE,
        CHILD_TXAPPLICANTHOMEPHONEEXCHANGE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXAPPLICANTHOMEPHONEROUTE))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TXAPPLICANTHOMEPHONEROUTE,
        CHILD_TXAPPLICANTHOMEPHONEROUTE,
        CHILD_TXAPPLICANTHOMEPHONEROUTE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXAPPLICANTWORKPHONEAREACODE))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TXAPPLICANTWORKPHONEAREACODE,
        CHILD_TXAPPLICANTWORKPHONEAREACODE,
        CHILD_TXAPPLICANTWORKPHONEAREACODE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXAPPLICANTWORKPHONEEXCHANGE))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TXAPPLICANTWORKPHONEEXCHANGE,
        CHILD_TXAPPLICANTWORKPHONEEXCHANGE,
        CHILD_TXAPPLICANTWORKPHONEEXCHANGE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXAPPLICANTWORKPHONEROUTE))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TXAPPLICANTWORKPHONEROUTE,
        CHILD_TXAPPLICANTWORKPHONEROUTE,
        CHILD_TXAPPLICANTWORKPHONEROUTE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXAPPLICANTWORKPHONEEXT))
    {
      TextField child = new TextField(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_TXAPPLICANTWORKPHONEEXT,
        doApplicantEntryApplicantSelectModel.FIELD_DFWORKPHONEEXTENSION,
        CHILD_TXAPPLICANTWORKPHONEEXT_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXAPPLICANTFAXAREACODE))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TXAPPLICANTFAXAREACODE,
        CHILD_TXAPPLICANTFAXAREACODE,
        CHILD_TXAPPLICANTFAXAREACODE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXAPPLICANTFAXEXCHANGE))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TXAPPLICANTFAXEXCHANGE,
        CHILD_TXAPPLICANTFAXEXCHANGE,
        CHILD_TXAPPLICANTFAXEXCHANGE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXAPPLICANTFAXROUTE))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TXAPPLICANTFAXROUTE,
        CHILD_TXAPPLICANTFAXROUTE,
        CHILD_TXAPPLICANTFAXROUTE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_CBFIRSTTIMEBUYER))
    {
      ComboBox child = new ComboBox(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_CBFIRSTTIMEBUYER,
        doApplicantEntryApplicantSelectModel.FIELD_DFFIRSTTIMEBUYER,
        CHILD_CBFIRSTTIMEBUYER_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbFirstTimeBuyerOptions);
      return child;
    }
    else
    if(name.equals(CHILD_TXEMAILADDRESS))
    {
      TextField child = new TextField(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_TXEMAILADDRESS,
        doApplicantEntryApplicantSelectModel.FIELD_DFEMAILADDRESS,
        CHILD_TXEMAILADDRESS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_CBLANGUAGEPREFERENCE))
    {
      ComboBox child = new ComboBox(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_CBLANGUAGEPREFERENCE,
        doApplicantEntryApplicantSelectModel.FIELD_DFLANGUAGEPREFID,
        CHILD_CBLANGUAGEPREFERENCE_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbLanguagePreferenceOptions);
      return child;
    }
    else
    if(name.equals(CHILD_REPEATEDAPPLICANTADDRESS))
    {
      pgApplicantEntryRepeatedApplicantAddressTiledView child = new pgApplicantEntryRepeatedApplicantAddressTiledView(this,
        CHILD_REPEATEDAPPLICANTADDRESS);
      return child;
    }
    else
    if(name.equals(CHILD_BTADDADDTIONALAPPLICANTADDRESS))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTADDADDTIONALAPPLICANTADDRESS,
        CHILD_BTADDADDTIONALAPPLICANTADDRESS,
        CHILD_BTADDADDTIONALAPPLICANTADDRESS_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_REPEATEDEMPLOYMENT))
    {
      pgApplicantEntryRepeatedEmploymentTiledView child = new pgApplicantEntryRepeatedEmploymentTiledView(this,
        CHILD_REPEATEDEMPLOYMENT);
      return child;
    }
    else
    if(name.equals(CHILD_BTADDADTIONALEMPLOYMENT))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTADDADTIONALEMPLOYMENT,
        CHILD_BTADDADTIONALEMPLOYMENT,
        CHILD_BTADDADTIONALEMPLOYMENT_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_REPEATEDOTHERINCOME))
    {
      pgApplicantEntryRepeatedOtherIncomeTiledView child = new pgApplicantEntryRepeatedOtherIncomeTiledView(this,
        CHILD_REPEATEDOTHERINCOME);
      return child;
    }
    else
    if(name.equals(CHILD_BTADDADDITIONALOTHERINCOME))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTADDADDITIONALOTHERINCOME,
        CHILD_BTADDADDITIONALOTHERINCOME,
        CHILD_BTADDADDITIONALOTHERINCOME_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_REPEATEDCREDITREFERENCE))
    {
      pgApplicantEntryRepeatedCreditReferenceTiledView child = new pgApplicantEntryRepeatedCreditReferenceTiledView(this,
        CHILD_REPEATEDCREDITREFERENCE);
      return child;
    }
    else
    if(name.equals(CHILD_BTADDADITIONALCREDITREFS))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTADDADITIONALCREDITREFS,
        CHILD_BTADDADITIONALCREDITREFS,
        CHILD_BTADDADITIONALCREDITREFS_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_BTADDADDTIONALASSETS))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTADDADDTIONALASSETS,
        CHILD_BTADDADDTIONALASSETS,
        CHILD_BTADDADDTIONALASSETS_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_REPEATEDASSETS))
    {
      pgApplicantEntryRepeatedAssetsTiledView child = new pgApplicantEntryRepeatedAssetsTiledView(this,
        CHILD_REPEATEDASSETS);
      return child;
    }
    else
    if(name.equals(CHILD_REPEATEDCREDITBUREAULIABILITIES))
    {
      pgApplicantEntryRepeatedCreditBureauLiabilitiesTiledView child = new
        pgApplicantEntryRepeatedCreditBureauLiabilitiesTiledView(this,
        CHILD_REPEATEDCREDITBUREAULIABILITIES);
      return child;
    }
    else
    if(name.equals(CHILD_REPEATEDLIABILITIES))
    {
      pgApplicantEntryRepeatedLiabilitiesTiledView child = new pgApplicantEntryRepeatedLiabilitiesTiledView(this,
        CHILD_REPEATEDLIABILITIES);
      return child;
    }
    else
    if(name.equals(CHILD_BTDELETELIABILITY))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTDELETELIABILITY,
        CHILD_BTDELETELIABILITY,
        CHILD_BTDELETELIABILITY_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_BTADDADITIONALLIABILITIES))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTADDADITIONALLIABILITIES,
        CHILD_BTADDADITIONALLIABILITIES,
        CHILD_BTADDADITIONALLIABILITIES_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_BTAPPLICANTSUBMIT))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTAPPLICANTSUBMIT,
        CHILD_BTAPPLICANTSUBMIT,
        CHILD_BTAPPLICANTSUBMIT_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_BTAPPLICANTCANCEL))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTAPPLICANTCANCEL,
        CHILD_BTAPPLICANTCANCEL,
        CHILD_BTAPPLICANTCANCEL_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_BTAPPLICANTCANCELCURRENTCHANGES))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTAPPLICANTCANCELCURRENTCHANGES,
        CHILD_BTAPPLICANTCANCELCURRENTCHANGES,
        CHILD_BTAPPLICANTCANCELCURRENTCHANGES_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_STPMGENERATE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMGENERATE,
        CHILD_STPMGENERATE,
        CHILD_STPMGENERATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMHASTITLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMHASTITLE,
        CHILD_STPMHASTITLE,
        CHILD_STPMHASTITLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMHASINFO))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMHASINFO,
        CHILD_STPMHASINFO,
        CHILD_STPMHASINFO_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMHASTABLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMHASTABLE,
        CHILD_STPMHASTABLE,
        CHILD_STPMHASTABLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMHASOK))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMHASOK,
        CHILD_STPMHASOK,
        CHILD_STPMHASOK_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMTITLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMTITLE,
        CHILD_STPMTITLE,
        CHILD_STPMTITLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMINFOMSG))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMINFOMSG,
        CHILD_STPMINFOMSG,
        CHILD_STPMINFOMSG_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMONOK))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMONOK,
        CHILD_STPMONOK,
        CHILD_STPMONOK_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMMSGS))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMMSGS,
        CHILD_STPMMSGS,
        CHILD_STPMMSGS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMMSGTYPES))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMMSGTYPES,
        CHILD_STPMMSGTYPES,
        CHILD_STPMMSGTYPES_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMGENERATE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMGENERATE,
        CHILD_STAMGENERATE,
        CHILD_STAMGENERATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMHASTITLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMHASTITLE,
        CHILD_STAMHASTITLE,
        CHILD_STAMHASTITLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMHASINFO))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMHASINFO,
        CHILD_STAMHASINFO,
        CHILD_STAMHASINFO_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMHASTABLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMHASTABLE,
        CHILD_STAMHASTABLE,
        CHILD_STAMHASTABLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMTITLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMTITLE,
        CHILD_STAMTITLE,
        CHILD_STAMTITLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMINFOMSG))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMINFOMSG,
        CHILD_STAMINFOMSG,
        CHILD_STAMINFOMSG_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMMSGS))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMMSGS,
        CHILD_STAMMSGS,
        CHILD_STAMMSGS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMMSGTYPES))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMMSGTYPES,
        CHILD_STAMMSGTYPES,
        CHILD_STAMMSGTYPES_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMDIALOGMSG))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMDIALOGMSG,
        CHILD_STAMDIALOGMSG,
        CHILD_STAMDIALOGMSG_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMBUTTONSHTML))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMBUTTONSHTML,
        CHILD_STAMBUTTONSHTML,
        CHILD_STAMBUTTONSHTML_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STTARGETADDRESS))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STTARGETADDRESS,
        CHILD_STTARGETADDRESS,
        CHILD_STTARGETADDRESS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STTARGETEMPLOYMENT))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STTARGETEMPLOYMENT,
        CHILD_STTARGETEMPLOYMENT,
        CHILD_STTARGETEMPLOYMENT_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STTARGETINCOME))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STTARGETINCOME,
        CHILD_STTARGETINCOME,
        CHILD_STTARGETINCOME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STTARGETCREDIT))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STTARGETCREDIT,
        CHILD_STTARGETCREDIT,
        CHILD_STTARGETCREDIT_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STTARGETASSET))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STTARGETASSET,
        CHILD_STTARGETASSET,
        CHILD_STTARGETASSET_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STTARGETLIAB))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STTARGETLIAB,
        CHILD_STTARGETLIAB,
        CHILD_STTARGETLIAB_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXTOTALLIAB))
    {
      TextField child = new TextField(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_TXTOTALLIAB,
        doApplicantEntryApplicantSelectModel.FIELD_DFTOTALLIAB,
        CHILD_TXTOTALLIAB_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXTOTALMONTHLYLIAB))
    {
      TextField child = new TextField(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_TXTOTALMONTHLYLIAB,
        doApplicantEntryApplicantSelectModel.FIELD_DFTOTALLIABPAYMENT,
        CHILD_TXTOTALMONTHLYLIAB_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXTOTALASSET))
    {
      TextField child = new TextField(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_TXTOTALASSET,
        doApplicantEntryApplicantSelectModel.FIELD_DFTOTALASSET,
        CHILD_TXTOTALASSET_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXTOTALINCOME))
    {
      TextField child = new TextField(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_TXTOTALINCOME,
        doApplicantEntryApplicantSelectModel.FIELD_DFTOTALINCOME,
        CHILD_TXTOTALINCOME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STVALSDATA))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STVALSDATA,
        CHILD_STVALSDATA,
        CHILD_STVALSDATA_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_BTOK))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTOK,
        CHILD_BTOK,
        CHILD_BTOK_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_STVIEWONLYTAG))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STVIEWONLYTAG,
        CHILD_STVIEWONLYTAG,
        CHILD_STVIEWONLYTAG_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_BTEXPORTDELETELIABILITY))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTEXPORTDELETELIABILITY,
        CHILD_BTEXPORTDELETELIABILITY,
        CHILD_BTEXPORTDELETELIABILITY_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_STTARGETCREDITBUREAULIAB))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STTARGETCREDITBUREAULIAB,
        CHILD_STTARGETCREDITBUREAULIAB,
        CHILD_STTARGETCREDITBUREAULIAB_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STLIABILIESLABEL))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STLIABILIESLABEL,
        CHILD_STLIABILIESLABEL,
        CHILD_STLIABILIESLABEL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STTOTALLIABILIESLABEL))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STTOTALLIABILIESLABEL,
        CHILD_STTOTALLIABILIESLABEL,
        CHILD_STTOTALLIABILIESLABEL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXCREDITBUREAUTOTALLIAB))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TXCREDITBUREAUTOTALLIAB,
        CHILD_TXCREDITBUREAUTOTALLIAB,
        CHILD_TXCREDITBUREAUTOTALLIAB_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STMONTHPAYMENTLABEL))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STMONTHPAYMENTLABEL,
        CHILD_STMONTHPAYMENTLABEL,
        CHILD_STMONTHPAYMENTLABEL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXCREDITBUREAUTOTALMONTHLYLIAB))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TXCREDITBUREAUTOTALMONTHLYLIAB,
        CHILD_TXCREDITBUREAUTOTALMONTHLYLIAB,
        CHILD_TXCREDITBUREAUTOTALMONTHLYLIAB_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXCREDITBUREAUSUMMARY))
    {
      TextField child = new TextField(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_TXCREDITBUREAUSUMMARY,
        doApplicantEntryApplicantSelectModel.FIELD_DFCREDITBUREAUSUMMARY,
        CHILD_TXCREDITBUREAUSUMMARY_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STCREDITSCORELABEL))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STCREDITSCORELABEL,
        CHILD_STCREDITSCORELABEL,
        CHILD_STCREDITSCORELABEL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXCREDITSCORE))
    {
      TextField child = new TextField(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_TXCREDITSCORE,
        doApplicantEntryApplicantSelectModel.FIELD_DFCREDITSCORE,
        CHILD_TXCREDITSCORE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STBUREAUVIEWBUTTON))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STBUREAUVIEWBUTTON,
        CHILD_STBUREAUVIEWBUTTON,
        CHILD_STBUREAUVIEWBUTTON_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_RPTCREDITBUREAUREPORTS))
    {
      pgApplicantEntryrptCreditBureauReportsTiledView child = new pgApplicantEntryrptCreditBureauReportsTiledView(this,
        CHILD_RPTCREDITBUREAUREPORTS);
      return child;
    }
    else
    if(name.equals(CHILD_STCOMBINEDGDS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoApplicantGSDTSDModel(),
        CHILD_STCOMBINEDGDS,
        doApplicantGSDTSDModel.FIELD_DFCOMBINEDGDS,
        CHILD_STCOMBINEDGDS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STCOMBINED3YRGDS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoApplicantGSDTSDModel(),
        CHILD_STCOMBINED3YRGDS,
        doApplicantGSDTSDModel.FIELD_DFCOMBINEDGDS3YEAR,
        CHILD_STCOMBINED3YRGDS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STCOMBINEDBORROWERGDS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoApplicantGSDTSDModel(),
        CHILD_STCOMBINEDBORROWERGDS,
        doApplicantGSDTSDModel.FIELD_DFCOMBINEDGDSBORROWER,
        CHILD_STCOMBINEDBORROWERGDS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STCOMBINEDTDS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoApplicantGSDTSDModel(),
        CHILD_STCOMBINEDTDS,
        doApplicantGSDTSDModel.FIELD_DFCOMBINEDTDS,
        CHILD_STCOMBINEDTDS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STCOMBINED3YRTDS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoApplicantGSDTSDModel(),
        CHILD_STCOMBINED3YRTDS,
        doApplicantGSDTSDModel.FIELD_DFCOMBINEDTDS3YEAR,
        CHILD_STCOMBINED3YRTDS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STCOMBINEDBORROWERTDS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoApplicantGSDTSDModel(),
        CHILD_STCOMBINEDBORROWERTDS,
        doApplicantGSDTSDModel.FIELD_DFCOMBINEDTDSBORROWER,
        CHILD_STCOMBINEDBORROWERTDS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_BTRECALCULATE))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTRECALCULATE,
        CHILD_BTRECALCULATE,
        CHILD_BTRECALCULATE_RESET_VALUE,
        null);
      return child;

    }
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> by BILLY 15July2002
    else
    if(name.equals(CHILD_BTACTMSG))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTACTMSG,
        CHILD_BTACTMSG,
        CHILD_BTACTMSG_RESET_VALUE,
        new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));
      return child;
    }
    //--Release2.1--//
    //// New link to toggle language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new session
    //// language id throughout the all modulus.
    else
    if(name.equals(CHILD_TOGGLELANGUAGEHREF))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_TOGGLELANGUAGEHREF,
        CHILD_TOGGLELANGUAGEHREF,
        CHILD_TOGGLELANGUAGEHREF_RESET_VALUE,
        null);
      return child;
    }
    //--DJ_LDI_CR--start--//
    else
    if (name.equals(CHILD_HDBORROWERGENDERID))
    {
      HiddenField child = new HiddenField(this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_HDBORROWERGENDERID,
        getdoApplicantEntryApplicantSelectModel().FIELD_DFBORROWERGENDERID,
        CHILD_HDBORROWERGENDERID_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_CBBORROWERGENDER))
    {
      ComboBox child = new ComboBox( this,
        getdoApplicantEntryApplicantSelectModel(),
        CHILD_CBBORROWERGENDER,
        getdoApplicantEntryApplicantSelectModel().FIELD_DFBORROWERGENDERID,
        CHILD_CBBORROWERGENDER_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbBorrowerGenderOptions);
      return child;
    }
    else
    if (name.equals(CHILD_STINCLUDELIFEDISLABELSSTART))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STINCLUDELIFEDISLABELSSTART,
        CHILD_STINCLUDELIFEDISLABELSSTART,
        CHILD_STINCLUDELIFEDISLABELSSTART_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STINCLUDELIFEDISLABELSEND))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STINCLUDELIFEDISLABELSEND,
        CHILD_STINCLUDELIFEDISLABELSEND,
        CHILD_STINCLUDELIFEDISLABELSEND_RESET_VALUE,
        null);
      return child;
    }
    //--DJ_LDI_CR--end--//
    //--DJ_CR201.2--start//
		else
		if (name.equals(CHILD_CBGUARANTOROTHERLOANS))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantEntryApplicantSelectModel(),
				CHILD_CBGUARANTOROTHERLOANS,
				doApplicantEntryApplicantSelectModel.FIELD_DFGUARANTOROTHERLOANS,
				CHILD_CBGUARANTOROTHERLOANS,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbGuarantorOtherLoansOptions);
			return child;
		}
    //--DJ_CR201.2--end//
    
    //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
	  else
	        if(name.equals(CHILD_REPEATEDIDENTIFICATION))
	        {
	        	pgApplicantEntryRepeatedIdentificationTiledView child = new pgApplicantEntryRepeatedIdentificationTiledView(this,
	        		  CHILD_REPEATEDIDENTIFICATION);
	          return child;
	        }
	   else
        if(name.equals(CHILD_STTARGETIDENTIFICATION))
        {
          StaticTextField child = new StaticTextField(this,
            getDefaultModel(),
            CHILD_STTARGETIDENTIFICATION,
            CHILD_STTARGETIDENTIFICATION,
            CHILD_STTARGETIDENTIFICATION_RESET_VALUE,
            null);
          return child;
        }
        else
		    if(name.equals(CHILD_BTADDADDITIONALAPPLICANTIDENTIFICATION))
		    {
		      Button child = new Button(
		        this,
		        getDefaultModel(),
		        CHILD_BTADDADDITIONALAPPLICANTIDENTIFICATION,
		        CHILD_BTADDADDITIONALAPPLICANTIDENTIFICATION,
		        CHILD_BTADDADDITIONALAPPLICANTIDENTIFICATION_RESET_VALUE,
		        null);
		      return child;

		    }
     else
        if (name.equals(CHILD_CBSMOKESTATUS))
        {	        	       	
        	ComboBox child = new ComboBox(this,
        	        getdoApplicantEntryApplicantSelectModel(),
        	        CHILD_CBSMOKESTATUS,
        	        doApplicantEntryApplicantSelectModel.FIELD_DFSMOKESTATUSID,
        	        CHILD_CBSMOKESTATUS_RESET_VALUE,
        	        null);
		      child.setLabelForNoneSelected("");
		      child.setOptions(cbSmokeStatusOptions);
		      return child;

        }
        else
	    if (name.equals(CHILD_CBSOLICITATION))
        {

	    	ComboBox child = new ComboBox(this,
        	        getdoApplicantEntryApplicantSelectModel(),
        	        CHILD_CBSOLICITATION,
        	        doApplicantEntryApplicantSelectModel.FIELD_DFSOLICITATIONID,
        	        CHILD_CBSOLICITATION_RESET_VALUE,
        	        null);
		      child.setLabelForNoneSelected("");
		      child.setOptions(cbSolicitationOptions);
		      return child;
          
        }
	    else
        if (name.equals(CHILD_CBPREFERREDCONTACTMETHOD))
        {	        	       	
        	ComboBox child = new ComboBox(this,
        	        getdoApplicantEntryApplicantSelectModel(),
        	        CHILD_CBPREFERREDCONTACTMETHOD,
        	        doApplicantEntryApplicantSelectModel.FIELD_DFPREFCONTACTMETHODID,
        	        CHILD_CBPREFERREDCONTACTMETHOD_RESET_VALUE,
        	        null);
		      child.setLabelForNoneSelected("");
		      child.setOptions(cbPreferredContactMethodOptions);
		      return child;

        }
        else
        if (name.equals(CHILD_TXEMPLOYEENO))
        {       
      
          TextField child = new TextField(this,
        		  getdoApplicantEntryApplicantSelectModel(),
        	        CHILD_TXEMPLOYEENO,
        	        doApplicantEntryApplicantSelectModel.FIELD_DFEMPLOYEENUMBER,
        	        CHILD_TXEMPLOYEENO_RESET_VALUE,
        	        null);
        	     
          return child;
        }
    //  ***** Change by NBC Impl. Team - Version 1.1 - End *****//
        else
            if (name.equals(CHILD_CBSUFFIX))
            {

                ComboBox child = new ComboBox(this,
                        getdoApplicantEntryApplicantSelectModel(),
                        CHILD_CBSUFFIX,
                        doApplicantEntryApplicantSelectModel.FIELD_DFSUFFIXID,
                        CHILD_CBSUFFIX_RESET_VALUE,
                        null);
                  child.setLabelForNoneSelected("");
                  child.setOptions(cbSuffixOptions);
                  return child;
            }
            else
                if(name.equals(CHILD_TXAPPLICANTCELLAREACODE))
                {
                  TextField child = new TextField(this,
                    getDefaultModel(),
                    CHILD_TXAPPLICANTCELLAREACODE,
                    CHILD_TXAPPLICANTCELLAREACODE,
                    CHILD_TXAPPLICANTCELLAREACODE_RESET_VALUE,
                    null);
                  return child;
                }
                else
                if(name.equals(CHILD_TXAPPLICANTCELLEXCHANGE))
                {
                  TextField child = new TextField(this,
                    getDefaultModel(),
                    CHILD_TXAPPLICANTCELLEXCHANGE,
                    CHILD_TXAPPLICANTCELLEXCHANGE,
                    CHILD_TXAPPLICANTCELLEXCHANGE_RESET_VALUE,
                    null);
                  return child;
                }
                else
                if(name.equals(CHILD_TXAPPLICANTCELLROUTE))
                {
                  TextField child = new TextField(this,
                    getDefaultModel(),
                    CHILD_TXAPPLICANTCELLROUTE,
                    CHILD_TXAPPLICANTCELLROUTE,
                    CHILD_TXAPPLICANTCELLROUTE_RESET_VALUE,
                    null);
                  return child;
                }
                else
                if(name.equals(CHILD_STLIABILITYTILESIZE))
                {
                  StaticTextField child = new StaticTextField(this,
                    getDefaultModel(),
                    CHILD_STLIABILITYTILESIZE,
                    CHILD_STLIABILITYTILESIZE,
                    CHILD_STLIABILITYTILESIZE_RESET_VALUE,
                    null);
                  return child;
                }
                else
                if(name.equals(CHILD_STCREDITBUREAUTILESIZE))
                {
                  StaticTextField child = new StaticTextField(this,
                    getDefaultModel(),
                    CHILD_STCREDITBUREAUTILESIZE,
                    CHILD_STCREDITBUREAUTILESIZE,
                    CHILD_STCREDITBUREAUTILESIZE_RESET_VALUE,
                    null);
                  return child;
                }
    else
    {
      throw new IllegalArgumentException("Invalid child name [" + name + "]");
    }
  }

  /**
   *
   *
   */
  public void resetChildren()
  {
	  super.resetChildren();
    getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
    getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
    getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
    getHref1().setValue(CHILD_HREF1_RESET_VALUE);
    getHref2().setValue(CHILD_HREF2_RESET_VALUE);
    getHref3().setValue(CHILD_HREF3_RESET_VALUE);
    getHref4().setValue(CHILD_HREF4_RESET_VALUE);
    getHref5().setValue(CHILD_HREF5_RESET_VALUE);
    getHref6().setValue(CHILD_HREF6_RESET_VALUE);
    getHref7().setValue(CHILD_HREF7_RESET_VALUE);
    getHref8().setValue(CHILD_HREF8_RESET_VALUE);
    getHref9().setValue(CHILD_HREF9_RESET_VALUE);
    getHref10().setValue(CHILD_HREF10_RESET_VALUE);
    getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
    getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
    getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
    getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
    getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
    getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
    getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
    getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
    getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
    getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
    getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
    getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
    getBtPrevTaskPage().setValue(CHILD_BTPREVTASKPAGE_RESET_VALUE);
    getStPrevTaskPageLabel().setValue(CHILD_STPREVTASKPAGELABEL_RESET_VALUE);
    getBtNextTaskPage().setValue(CHILD_BTNEXTTASKPAGE_RESET_VALUE);
    getStNextTaskPageLabel().setValue(CHILD_STNEXTTASKPAGELABEL_RESET_VALUE);
    getStTaskName().setValue(CHILD_STTASKNAME_RESET_VALUE);
    getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
    getHdApplicantId().setValue(CHILD_HDAPPLICANTID_RESET_VALUE);
    getHdApplicantCopyId().setValue(CHILD_HDAPPLICANTCOPYID_RESET_VALUE);
    getCbSalutation().setValue(CHILD_CBSALUTATION_RESET_VALUE);
    getTxFirstName().setValue(CHILD_TXFIRSTNAME_RESET_VALUE);
    getTxMiddleInitials().setValue(CHILD_TXMIDDLEINITIALS_RESET_VALUE);
    getTxLastName().setValue(CHILD_TXLASTNAME_RESET_VALUE);
    getCbApplicantType().setValue(CHILD_CBAPPLICANTTYPE_RESET_VALUE);
    getCbApplicantDOBMonth().setValue(CHILD_CBAPPLICANTDOBMONTH_RESET_VALUE);
    getTxApplicantDOBDay().setValue(CHILD_TXAPPLICANTDOBDAY_RESET_VALUE);
    getTxApplicantDOBYear().setValue(CHILD_TXAPPLICANTDOBYEAR_RESET_VALUE);
    getCbMartialStatus().setValue(CHILD_CBMARTIALSTATUS_RESET_VALUE);
    getTxSINNo().setValue(CHILD_TXSINNO_RESET_VALUE);
    getCbCitizenship().setValue(CHILD_CBCITIZENSHIP_RESET_VALUE);
    getTxNoOfDependants().setValue(CHILD_TXNOOFDEPENDANTS_RESET_VALUE);
    getCbExistingClient().setValue(CHILD_CBEXISTINGCLIENT_RESET_VALUE);
    getTxReferenceClientNo().setValue(CHILD_TXREFERENCECLIENTNO_RESET_VALUE);
    getTxNoOfTimesBankrupt().setValue(CHILD_TXNOOFTIMESBANKRUPT_RESET_VALUE);
    getCbBankruptcyStatus().setValue(CHILD_CBBANKRUPTCYSTATUS_RESET_VALUE);
    getCbStaffOfLender().setValue(CHILD_CBSTAFFOFLENDER_RESET_VALUE);
    getTxApplicantHomePhoneAreaCode().setValue(CHILD_TXAPPLICANTHOMEPHONEAREACODE_RESET_VALUE);
    getTxApplicantHomePhoneExchange().setValue(CHILD_TXAPPLICANTHOMEPHONEEXCHANGE_RESET_VALUE);
    getTxApplicantHomePhoneRoute().setValue(CHILD_TXAPPLICANTHOMEPHONEROUTE_RESET_VALUE);
    getTxApplicantWorkPhoneAreaCode().setValue(CHILD_TXAPPLICANTWORKPHONEAREACODE_RESET_VALUE);
    getTxApplicantWorkPhoneExchange().setValue(CHILD_TXAPPLICANTWORKPHONEEXCHANGE_RESET_VALUE);
    getTxApplicantWorkPhoneRoute().setValue(CHILD_TXAPPLICANTWORKPHONEROUTE_RESET_VALUE);
    getTxApplicantWorkPhoneExt().setValue(CHILD_TXAPPLICANTWORKPHONEEXT_RESET_VALUE);
    getTxApplicantFaxAreaCode().setValue(CHILD_TXAPPLICANTFAXAREACODE_RESET_VALUE);
    getTxApplicantFaxExchange().setValue(CHILD_TXAPPLICANTFAXEXCHANGE_RESET_VALUE);
    getTxApplicantFaxRoute().setValue(CHILD_TXAPPLICANTFAXROUTE_RESET_VALUE);
    getCbFirstTimeBuyer().setValue(CHILD_CBFIRSTTIMEBUYER_RESET_VALUE);
    getTxEmailAddress().setValue(CHILD_TXEMAILADDRESS_RESET_VALUE);
    getCbLanguagePreference().setValue(CHILD_CBLANGUAGEPREFERENCE_RESET_VALUE);
    getRepeatedApplicantAddress().resetChildren();
    getBtAddAddtionalApplicantAddress().setValue(CHILD_BTADDADDTIONALAPPLICANTADDRESS_RESET_VALUE);
    getRepeatedEmployment().resetChildren();
    getBtAddAdtionalEmployment().setValue(CHILD_BTADDADTIONALEMPLOYMENT_RESET_VALUE);
    getRepeatedOtherIncome().resetChildren();
    getBtAddAdditionalOtherIncome().setValue(CHILD_BTADDADDITIONALOTHERINCOME_RESET_VALUE);
    getRepeatedCreditReference().resetChildren();
    getBtAddAditionalCreditRefs().setValue(CHILD_BTADDADITIONALCREDITREFS_RESET_VALUE);
    getBtAddAddtionalAssets().setValue(CHILD_BTADDADDTIONALASSETS_RESET_VALUE);
    getRepeatedAssets().resetChildren();
    getRepeatedCreditBureauLiabilities().resetChildren();
    getRepeatedLiabilities().resetChildren();
    getBtDeleteLiability().setValue(CHILD_BTDELETELIABILITY_RESET_VALUE);
    getBtAddAditionalLiabilities().setValue(CHILD_BTADDADITIONALLIABILITIES_RESET_VALUE);
    getBtApplicantSubmit().setValue(CHILD_BTAPPLICANTSUBMIT_RESET_VALUE);
    getBtApplicantCancel().setValue(CHILD_BTAPPLICANTCANCEL_RESET_VALUE);
    getBtApplicantCancelCurrentChanges().setValue(CHILD_BTAPPLICANTCANCELCURRENTCHANGES_RESET_VALUE);
    getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
    getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
    getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
    getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
    getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
    getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
    getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
    getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
    getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
    getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
    getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
    getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
    getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
    getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
    getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
    getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
    getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
    getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
    getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
    getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
    getStTargetAddress().setValue(CHILD_STTARGETADDRESS_RESET_VALUE);
    getStTargetEmployment().setValue(CHILD_STTARGETEMPLOYMENT_RESET_VALUE);
    getStTargetIncome().setValue(CHILD_STTARGETINCOME_RESET_VALUE);
    getStTargetCredit().setValue(CHILD_STTARGETCREDIT_RESET_VALUE);
    getStTargetAsset().setValue(CHILD_STTARGETASSET_RESET_VALUE);
    getStTargetLiab().setValue(CHILD_STTARGETLIAB_RESET_VALUE);
    getTxTotalLiab().setValue(CHILD_TXTOTALLIAB_RESET_VALUE);
    getTxTotalMonthlyLiab().setValue(CHILD_TXTOTALMONTHLYLIAB_RESET_VALUE);
    getTxTotalAsset().setValue(CHILD_TXTOTALASSET_RESET_VALUE);
    getTxTotalIncome().setValue(CHILD_TXTOTALINCOME_RESET_VALUE);
    getStVALSData().setValue(CHILD_STVALSDATA_RESET_VALUE);
    getBtOK().setValue(CHILD_BTOK_RESET_VALUE);
    getStViewOnlyTag().setValue(CHILD_STVIEWONLYTAG_RESET_VALUE);
    getBtExportDeleteLiability().setValue(CHILD_BTEXPORTDELETELIABILITY_RESET_VALUE);
    getStTargetCreditBureauLiab().setValue(CHILD_STTARGETCREDITBUREAULIAB_RESET_VALUE);
    getStLiabiliesLabel().setValue(CHILD_STLIABILIESLABEL_RESET_VALUE);
    getStTotalLiabiliesLabel().setValue(CHILD_STTOTALLIABILIESLABEL_RESET_VALUE);
    getTxCreditBureauTotalLiab().setValue(CHILD_TXCREDITBUREAUTOTALLIAB_RESET_VALUE);
    getStMonthPaymentLabel().setValue(CHILD_STMONTHPAYMENTLABEL_RESET_VALUE);
    getTxCreditBureauTotalMonthlyLiab().setValue(CHILD_TXCREDITBUREAUTOTALMONTHLYLIAB_RESET_VALUE);
    getTxCreditBureauSummary().setValue(CHILD_TXCREDITBUREAUSUMMARY_RESET_VALUE);
    getStCreditScoreLabel().setValue(CHILD_STCREDITSCORELABEL_RESET_VALUE);
    getTxCreditScore().setValue(CHILD_TXCREDITSCORE_RESET_VALUE);
    getStBureauViewButton().setValue(CHILD_STBUREAUVIEWBUTTON_RESET_VALUE);
    getRptCreditBureauReports().resetChildren();
    getStCombinedGDS().setValue(CHILD_STCOMBINEDGDS_RESET_VALUE);
    getStCombined3YrGDS().setValue(CHILD_STCOMBINED3YRGDS_RESET_VALUE);
    getStCombinedBorrowerGDS().setValue(CHILD_STCOMBINEDBORROWERGDS_RESET_VALUE);
    getStCombinedTDS().setValue(CHILD_STCOMBINEDTDS_RESET_VALUE);
    getStCombined3YrTDS().setValue(CHILD_STCOMBINED3YRTDS_RESET_VALUE);
    getStCombinedBorrowerTDS().setValue(CHILD_STCOMBINEDBORROWERTDS_RESET_VALUE);
    getBtRecalculate().setValue(CHILD_BTRECALCULATE_RESET_VALUE);
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> Test by BILLY 15July2002
    getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
    getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);
    //--DJ_LDI_CR--start--//
    getCbBorrowerGender().setValue(CHILD_CBBORROWERGENDER_RESET_VALUE);
    getHdBorrowerGenderId().setValue(CHILD_HDBORROWERGENDERID_RESET_VALUE);
    getStIncludeLifeDisLabelsStart().setValue(CHILD_STINCLUDELIFEDISLABELSSTART_RESET_VALUE);
    getStIncludeLifeDisLabelsEnd().setValue(CHILD_STINCLUDELIFEDISLABELSEND_RESET_VALUE);
    //--DJ_LDI_CR--end--//
    //--DJ_CR201.2--start//
    getCbGuarantorOtherLoans().setValue(CHILD_CBGUARANTOROTHERLOANS_RESET_VALUE);
    //--DJ_CR201.2--end//
    
    //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
    getBtAddAdditionalApplicantIdentification().setValue(
    		CHILD_BTADDADDITIONALAPPLICANTIDENTIFICATION_RESET_VALUE);
    getCbSmokeStatus().setValue(CHILD_CBSMOKESTATUS_RESET_VALUE);
    getCbSolicitation().setValue(CHILD_CBSOLICITATION_RESET_VALUE);
    getTxEmployeeNo().setValue(CHILD_TXEMPLOYEENO_RESET_VALUE);
    getCbPreferredContactMethod().setValue(
    		CHILD_CBPREFERREDCONTACTMETHOD_RESET_VALUE);
    //  ***** Change by NBC Impl. Team - Version 1.1 - End *****//
    
    getCbSuffix().setValue(CHILD_CBSUFFIX_RESET_VALUE);
    getTxApplicantCellAreaCode().setValue(CHILD_TXAPPLICANTCELLAREACODE_RESET_VALUE);
    getTxApplicantCellExchange().setValue(CHILD_TXAPPLICANTCELLEXCHANGE_RESET_VALUE);
    getTxApplicantCellRoute().setValue(CHILD_TXAPPLICANTCELLROUTE_RESET_VALUE);
    getStLiabilityTileSize().setValue(CHILD_STLIABILITYTILESIZE_RESET_VALUE);
    getStCreditBureanTileSize().setValue(CHILD_STCREDITBUREAUTILESIZE_RESET_VALUE);

  }

  /**
   *
   *
   */
  protected void registerChildren()
  {
	  super.registerChildren();
    registerChild(CHILD_TBDEALID, TextField.class);
    registerChild(CHILD_CBPAGENAMES, ComboBox.class);
    registerChild(CHILD_BTPROCEED, Button.class);
    registerChild(CHILD_HREF1, HREF.class);
    registerChild(CHILD_HREF2, HREF.class);
    registerChild(CHILD_HREF3, HREF.class);
    registerChild(CHILD_HREF4, HREF.class);
    registerChild(CHILD_HREF5, HREF.class);
    registerChild(CHILD_HREF6, HREF.class);
    registerChild(CHILD_HREF7, HREF.class);
    registerChild(CHILD_HREF8, HREF.class);
    registerChild(CHILD_HREF9, HREF.class);
    registerChild(CHILD_HREF10, HREF.class);
    registerChild(CHILD_STPAGELABEL, StaticTextField.class);
    registerChild(CHILD_STCOMPANYNAME, StaticTextField.class);
    registerChild(CHILD_STTODAYDATE, StaticTextField.class);
    registerChild(CHILD_STUSERNAMETITLE, StaticTextField.class);
    registerChild(CHILD_BTWORKQUEUELINK, Button.class);
    registerChild(CHILD_CHANGEPASSWORDHREF, HREF.class);
    registerChild(CHILD_BTTOOLHISTORY, Button.class);
    registerChild(CHILD_BTTOONOTES, Button.class);
    registerChild(CHILD_BTTOOLSEARCH, Button.class);
    registerChild(CHILD_BTTOOLLOG, Button.class);
    registerChild(CHILD_STERRORFLAG, StaticTextField.class);
    registerChild(CHILD_DETECTALERTTASKS, HiddenField.class);
    registerChild(CHILD_BTPREVTASKPAGE, Button.class);
    registerChild(CHILD_STPREVTASKPAGELABEL, StaticTextField.class);
    registerChild(CHILD_BTNEXTTASKPAGE, Button.class);
    registerChild(CHILD_STNEXTTASKPAGELABEL, StaticTextField.class);
    registerChild(CHILD_STTASKNAME, StaticTextField.class);
    registerChild(CHILD_SESSIONUSERID, HiddenField.class);
    registerChild(CHILD_HDAPPLICANTID, HiddenField.class);
    registerChild(CHILD_HDAPPLICANTCOPYID, HiddenField.class);
    registerChild(CHILD_CBSALUTATION, ComboBox.class);
    registerChild(CHILD_TXFIRSTNAME, TextField.class);
    registerChild(CHILD_TXMIDDLEINITIALS, TextField.class);
    registerChild(CHILD_TXLASTNAME, TextField.class);
    registerChild(CHILD_CBAPPLICANTTYPE, ComboBox.class);
    registerChild(CHILD_CBAPPLICANTDOBMONTH, ComboBox.class);
    registerChild(CHILD_TXAPPLICANTDOBDAY, TextField.class);
    registerChild(CHILD_TXAPPLICANTDOBYEAR, TextField.class);
    registerChild(CHILD_CBMARTIALSTATUS, ComboBox.class);
    registerChild(CHILD_TXSINNO, TextField.class);
    registerChild(CHILD_CBCITIZENSHIP, ComboBox.class);
    registerChild(CHILD_TXNOOFDEPENDANTS, TextField.class);
    registerChild(CHILD_CBEXISTINGCLIENT, ComboBox.class);
    registerChild(CHILD_TXREFERENCECLIENTNO, TextField.class);
    registerChild(CHILD_TXNOOFTIMESBANKRUPT, TextField.class);
    registerChild(CHILD_CBBANKRUPTCYSTATUS, ComboBox.class);
    registerChild(CHILD_CBSTAFFOFLENDER, ComboBox.class);
    registerChild(CHILD_TXAPPLICANTHOMEPHONEAREACODE, TextField.class);
    registerChild(CHILD_TXAPPLICANTHOMEPHONEEXCHANGE, TextField.class);
    registerChild(CHILD_TXAPPLICANTHOMEPHONEROUTE, TextField.class);
    registerChild(CHILD_TXAPPLICANTWORKPHONEAREACODE, TextField.class);
    registerChild(CHILD_TXAPPLICANTWORKPHONEEXCHANGE, TextField.class);
    registerChild(CHILD_TXAPPLICANTWORKPHONEROUTE, TextField.class);
    registerChild(CHILD_TXAPPLICANTWORKPHONEEXT, TextField.class);
    registerChild(CHILD_TXAPPLICANTFAXAREACODE, TextField.class);
    registerChild(CHILD_TXAPPLICANTFAXEXCHANGE, TextField.class);
    registerChild(CHILD_TXAPPLICANTFAXROUTE, TextField.class);
    registerChild(CHILD_CBFIRSTTIMEBUYER, ComboBox.class);
    registerChild(CHILD_TXEMAILADDRESS, TextField.class);
    registerChild(CHILD_CBLANGUAGEPREFERENCE, ComboBox.class);
    registerChild(CHILD_REPEATEDAPPLICANTADDRESS, pgApplicantEntryRepeatedApplicantAddressTiledView.class);
    registerChild(CHILD_BTADDADDTIONALAPPLICANTADDRESS, Button.class);
    registerChild(CHILD_REPEATEDEMPLOYMENT, pgApplicantEntryRepeatedEmploymentTiledView.class);
    registerChild(CHILD_BTADDADTIONALEMPLOYMENT, Button.class);
    registerChild(CHILD_REPEATEDOTHERINCOME, pgApplicantEntryRepeatedOtherIncomeTiledView.class);
    registerChild(CHILD_BTADDADDITIONALOTHERINCOME, Button.class);
    registerChild(CHILD_REPEATEDCREDITREFERENCE, pgApplicantEntryRepeatedCreditReferenceTiledView.class);
    registerChild(CHILD_BTADDADITIONALCREDITREFS, Button.class);
    registerChild(CHILD_BTADDADDTIONALASSETS, Button.class);
    registerChild(CHILD_REPEATEDASSETS, pgApplicantEntryRepeatedAssetsTiledView.class);
    registerChild(CHILD_REPEATEDCREDITBUREAULIABILITIES, pgApplicantEntryRepeatedCreditBureauLiabilitiesTiledView.class);
    registerChild(CHILD_REPEATEDLIABILITIES, pgApplicantEntryRepeatedLiabilitiesTiledView.class);
    registerChild(CHILD_BTDELETELIABILITY, Button.class);
    registerChild(CHILD_BTADDADITIONALLIABILITIES, Button.class);
    registerChild(CHILD_BTAPPLICANTSUBMIT, Button.class);
    registerChild(CHILD_BTAPPLICANTCANCEL, Button.class);
    registerChild(CHILD_BTAPPLICANTCANCELCURRENTCHANGES, Button.class);
    registerChild(CHILD_STPMGENERATE, StaticTextField.class);
    registerChild(CHILD_STPMHASTITLE, StaticTextField.class);
    registerChild(CHILD_STPMHASINFO, StaticTextField.class);
    registerChild(CHILD_STPMHASTABLE, StaticTextField.class);
    registerChild(CHILD_STPMHASOK, StaticTextField.class);
    registerChild(CHILD_STPMTITLE, StaticTextField.class);
    registerChild(CHILD_STPMINFOMSG, StaticTextField.class);
    registerChild(CHILD_STPMONOK, StaticTextField.class);
    registerChild(CHILD_STPMMSGS, StaticTextField.class);
    registerChild(CHILD_STPMMSGTYPES, StaticTextField.class);
    registerChild(CHILD_STAMGENERATE, StaticTextField.class);
    registerChild(CHILD_STAMHASTITLE, StaticTextField.class);
    registerChild(CHILD_STAMHASINFO, StaticTextField.class);
    registerChild(CHILD_STAMHASTABLE, StaticTextField.class);
    registerChild(CHILD_STAMTITLE, StaticTextField.class);
    registerChild(CHILD_STAMINFOMSG, StaticTextField.class);
    registerChild(CHILD_STAMMSGS, StaticTextField.class);
    registerChild(CHILD_STAMMSGTYPES, StaticTextField.class);
    registerChild(CHILD_STAMDIALOGMSG, StaticTextField.class);
    registerChild(CHILD_STAMBUTTONSHTML, StaticTextField.class);
    registerChild(CHILD_STTARGETADDRESS, StaticTextField.class);
    registerChild(CHILD_STTARGETEMPLOYMENT, StaticTextField.class);
    registerChild(CHILD_STTARGETINCOME, StaticTextField.class);
    registerChild(CHILD_STTARGETCREDIT, StaticTextField.class);
    registerChild(CHILD_STTARGETASSET, StaticTextField.class);
    registerChild(CHILD_STTARGETLIAB, StaticTextField.class);
    registerChild(CHILD_TXTOTALLIAB, TextField.class);
    registerChild(CHILD_TXTOTALMONTHLYLIAB, TextField.class);
    registerChild(CHILD_TXTOTALASSET, TextField.class);
    registerChild(CHILD_TXTOTALINCOME, TextField.class);
    registerChild(CHILD_STVALSDATA, StaticTextField.class);
    registerChild(CHILD_BTOK, Button.class);
    registerChild(CHILD_STVIEWONLYTAG, StaticTextField.class);
    registerChild(CHILD_BTEXPORTDELETELIABILITY, Button.class);
    registerChild(CHILD_STTARGETCREDITBUREAULIAB, StaticTextField.class);
    registerChild(CHILD_STLIABILIESLABEL, StaticTextField.class);
    registerChild(CHILD_STTOTALLIABILIESLABEL, StaticTextField.class);
    registerChild(CHILD_TXCREDITBUREAUTOTALLIAB, TextField.class);
    registerChild(CHILD_STMONTHPAYMENTLABEL, StaticTextField.class);
    registerChild(CHILD_TXCREDITBUREAUTOTALMONTHLYLIAB, TextField.class);
    registerChild(CHILD_TXCREDITBUREAUSUMMARY, TextField.class);
    registerChild(CHILD_STCREDITSCORELABEL, StaticTextField.class);
    registerChild(CHILD_TXCREDITSCORE, TextField.class);
    registerChild(CHILD_STBUREAUVIEWBUTTON, StaticTextField.class);
    registerChild(CHILD_RPTCREDITBUREAUREPORTS, pgApplicantEntryrptCreditBureauReportsTiledView.class);
    registerChild(CHILD_STCOMBINEDGDS, StaticTextField.class);
    registerChild(CHILD_STCOMBINED3YRGDS, StaticTextField.class);
    registerChild(CHILD_STCOMBINEDBORROWERGDS, StaticTextField.class);
    registerChild(CHILD_STCOMBINEDTDS, StaticTextField.class);
    registerChild(CHILD_STCOMBINED3YRTDS, StaticTextField.class);
    registerChild(CHILD_STCOMBINEDBORROWERTDS, StaticTextField.class);
    registerChild(CHILD_BTRECALCULATE, Button.class);
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> Test by BILLY 15July2002
    registerChild(CHILD_BTACTMSG, Button.class);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
    registerChild(CHILD_TOGGLELANGUAGEHREF, HREF.class);
    //--DJ_LDI_CR--start--//
    registerChild(CHILD_CBBORROWERGENDER,ComboBox.class);
    registerChild(CHILD_HDBORROWERGENDERID,HiddenField.class);
    registerChild(CHILD_STINCLUDELIFEDISLABELSSTART,StaticTextField.class);
    registerChild(CHILD_STINCLUDELIFEDISLABELSEND,StaticTextField.class);
    //--DJ_LDI_CR--end--//
    //--DJ_CR201.2--start//
		registerChild(CHILD_CBGUARANTOROTHERLOANS,ComboBox.class);
    //--DJ_CR201.2--end//
	
    //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//	
	registerChild(CHILD_REPEATEDIDENTIFICATION, 
			pgApplicantEntryRepeatedIdentificationTiledView.class);
	registerChild(CHILD_STTARGETIDENTIFICATION, StaticTextField.class);	
	registerChild(CHILD_BTADDADDITIONALAPPLICANTIDENTIFICATION, Button.class);
	registerChild(CHILD_CBSMOKESTATUS, ComboBox.class);
	registerChild(CHILD_CBSOLICITATION, ComboBox.class);
	registerChild(CHILD_TXEMPLOYEENO, TextField.class);
	registerChild(CHILD_CBPREFERREDCONTACTMETHOD, ComboBox.class);
    //  ***** Change by NBC Impl. Team - Version 1.1 - End *****//
    
    registerChild(CHILD_CBSUFFIX, ComboBox.class);
    registerChild(CHILD_TXAPPLICANTCELLAREACODE, TextField.class);
    registerChild(CHILD_TXAPPLICANTCELLEXCHANGE, TextField.class);
    registerChild(CHILD_TXAPPLICANTCELLROUTE, TextField.class);
    registerChild(CHILD_STLIABILITYTILESIZE, StaticTextField.class);
  }

  ////////////////////////////////////////////////////////////////////////////////
  // Model management methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  public Model[] getWebActionModels(int executionType)
  {
    List modelList = new ArrayList();
    switch(executionType)
    {
      case MODEL_TYPE_RETRIEVE:
        ;
        break;

      case MODEL_TYPE_UPDATE:
        ;
        break;

      case MODEL_TYPE_DELETE:
        ;
        break;

      case MODEL_TYPE_INSERT:
        ;
        break;

      case MODEL_TYPE_EXECUTE:
        ;
        break;
    }
    return(Model[])modelList.toArray(new Model[0]);
  }

  ////////////////////////////////////////////////////////////////////////////////
  // View flow control methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  public void beginDisplay(DisplayEvent event) throws ModelControlException
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    //--> Meger with Bernard's optimization changes
    handler.setupBeforePageGeneration();
    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By Billy 05Nov2002
    //Setup Options for cbPageNames
    //cbPageNamesOptions..populate(getRequestContext()); 
	int langId = handler.getTheSessionState().getLanguageId();
    cbPageNamesOptions.populate(getRequestContext(), langId);
    //Setup NoneSelected Label for cbPageNames
    CHILD_CBPAGENAMES_NONSELECTED_LABEL =
      BXResources.getGenericMsg("CBPAGENAMES_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    //Check if the cbPageNames is already created
    if(getCbPageNames() != null)
    {
      getCbPageNames().setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
      //=====================================

      //Set up other ComboBox options
    }
    cbSalutationOptions.populate(getRequestContext());
    cbApplicantTypeOptions.populate(getRequestContext());
    cbMartialStatusOptions.populate(getRequestContext());
    cbCitizenshipOptions.populate(getRequestContext());
    cbBankruptcyStatusOptions.populate(getRequestContext());
    cbLanguagePreferenceOptions.populate(getRequestContext());

    //--DJ_LDI_CR--start--//
    cbBorrowerGenderOptions.populate(getRequestContext());
    //--DJ_LDI_CR--end--//

    //Setup the Yes/No Options
    String yesStr = BXResources.getGenericMsg("YES_LABEL", handler.getTheSessionState().getLanguageId());
    String noStr = BXResources.getGenericMsg("NO_LABEL", handler.getTheSessionState().getLanguageId());
    cbStaffOfLenderOptions.setOptions(new String[]{noStr, yesStr}, new String[]{"N", "Y"});
    
    //FXP26782, 4.2GR, Oct 21, 2009 - start
    cbExistingClientOptions.setOptions(new String[]{"", yesStr, noStr}, new String[]{"", "Y", "N"});
    cbFirstTimeBuyerOptions.setOptions(new String[]{"", yesStr, noStr}, new String[]{"", "Y", "N"});
    //FXP26782, 4.2GR, Oct 21, 2009 - start
    
    //--DJ_CR201.2--start//
    cbGuarantorOtherLoansOptions.setOptions(new String[]{yesStr, noStr},new String[]{"Y", "N"});
    //--DJ_CR201.2--end//

    //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
    
    cbSmokeStatusOptions.populate(getRequestContext());
    cbSolicitationOptions.populate(getRequestContext());
    cbPreferredContactMethodOptions.populate(getRequestContext());
    
    //  ***** Change by NBC Impl. Team - Version 1.1 - End *****//
    
    cbSuffixOptions.populate(getRequestContext());
    
    HtmlDisplayFieldBase df = null;
	String theExtraHtml = null;
	
	df = (HtmlDisplayFieldBase) getDisplayField(CHILD_TXEMAILADDRESS);
	theExtraHtml = "onBlur=\"isEmail();\"";
	df.setExtraHtml(theExtraHtml);
	
    handler.pageSaveState();
    super.beginDisplay(event);
  }

  /**
   *
   *
   */
  public boolean beforeModelExecutes(Model model, int executionContext)
  {
    return super.beforeModelExecutes(model, executionContext);
  }

  /**
   *
   *
   */
  public void afterModelExecutes(Model model, int executionContext)
  {

    // This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
    super.afterModelExecutes(model, executionContext);

  }

  /**
   *
   *
   */
  public void afterAllModelsExecute(int executionContext)
  {

    // This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
    super.afterAllModelsExecute(executionContext);

    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    handler.populatePageDisplayFields();
    handler.pageSaveState();
  }

  /**
   *
   *
   */
  public void onModelError(Model model, int executionContext, ModelControlException exception) throws ModelControlException
  {

    // This is the analog of NetDynamics this_onDataObjectErrorEvent
    super.onModelError(model, executionContext, exception);

  }

  /**
   *
   *
   */
  public TextField getTbDealId()
  {
    return(TextField)getChild(CHILD_TBDEALID);
  }

  /**
   *
   *
   */
  public ComboBox getCbPageNames()
  {
    return(ComboBox)getChild(CHILD_CBPAGENAMES);
  }

  /**
   *
   *
   */
  //--DJ_Ticket661--start--//
  //Modified to sort by PageLabel based on the selected language
  static class CbPageNamesOptionList extends GotoOptionList
 {
    /**
     *
     *
     */
    CbPageNamesOptionList() {

    }


    public void populate(RequestContext rc) {
      Connection c = null;
      try {
        clear();
        SelectQueryModel m = null;

        SelectQueryExecutionContext eContext = new
          SelectQueryExecutionContext(
          (Connection)null,

          DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,

          DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null) {
          m = new doPageNameLabelModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else {
          m = (SelectQueryModel)rc.getModelManager().getModel(doPageNameLabelModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        // Sort the results from DataObject
        TreeMap sorted = new TreeMap();
        while (m.next()) {
          Object dfPageLabel =
            m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
          String label = (dfPageLabel == null ? "" : dfPageLabel.toString());
          Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
          String value = (dfPageId == null ? "" : dfPageId.toString());
          String[] theVal = new String[2];
          theVal[0] = label;
          theVal[1] = value;
          sorted.put(label, theVal);
        }

        // Set the sorted list to the optionlist
        Iterator theList = sorted.values().iterator();
        String[] theVal = new String[2];
        while (theList.hasNext()) {
          theVal = (String[]) (theList.next());
          add(theVal[0], theVal[1]);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      finally {
        try {
          if (c != null)
            c.close();
        }
        catch (SQLException ex) {
              // ignore
        }
      }
    }
 }
 //--DJ_Ticket661--end--//

  /**
   *
   *
   */
  public void handleBtProceedRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btProceed_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleGoPage();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtProceed()
  {
    return(Button)getChild(CHILD_BTPROCEED);
  }

  /**
   *
   *
   */
  public void handleHref1Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(0);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref1()
  {
    return(HREF)getChild(CHILD_HREF1);
  }

  /**
   *
   *
   */
  public void handleHref2Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(1);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref2()
  {
    return(HREF)getChild(CHILD_HREF2);
  }

  /**
   *
   *
   */
  public void handleHref3Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(2);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref3()
  {
    return(HREF)getChild(CHILD_HREF3);
  }

  /**
   *
   *
   */
  public void handleHref4Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(3);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref4()
  {
    return(HREF)getChild(CHILD_HREF4);
  }

  /**
   *
   *
   */
  public void handleHref5Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(4);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref5()
  {
    return(HREF)getChild(CHILD_HREF5);
  }

  /**
   *
   *
   */
  public void handleHref6Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(5);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref6()
  {
    return(HREF)getChild(CHILD_HREF6);
  }

  /**
   *
   *
   */
  public void handleHref7Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(6);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref7()
  {
    return(HREF)getChild(CHILD_HREF7);
  }

  /**
   *
   *
   */
  public void handleHref8Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(7);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref8()
  {
    return(HREF)getChild(CHILD_HREF8);
  }

  /**
   *
   *
   */
  public void handleHref9Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(8);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref9()
  {
    return(HREF)getChild(CHILD_HREF9);
  }

  /**
   *
   *
   */
  public void handleHref10Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(9);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref10()
  {
    return(HREF)getChild(CHILD_HREF10);
  }

  /**
   *
   *
   */
  public StaticTextField getStPageLabel()
  {
    return(StaticTextField)getChild(CHILD_STPAGELABEL);
  }

  /**
   *
   *
   */
  public StaticTextField getStCompanyName()
  {
    return(StaticTextField)getChild(CHILD_STCOMPANYNAME);
  }

  /**
   *
   *
   */
  public StaticTextField getStTodayDate()
  {
    return(StaticTextField)getChild(CHILD_STTODAYDATE);
  }

  /**
   *
   *
   */
  public StaticTextField getStUserNameTitle()
  {
    return(StaticTextField)getChild(CHILD_STUSERNAMETITLE);
  }

  /**
   *
   *
   */
  public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btWorkQueueLink_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleDisplayWorkQueue();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtWorkQueueLink()
  {
    return(Button)getChild(CHILD_BTWORKQUEUELINK);
  }

  /**
   *
   *
   */
  public void handleChangePasswordHrefRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the changePasswordHref_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleChangePassword();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getChangePasswordHref()
  {
    return(HREF)getChild(CHILD_CHANGEPASSWORDHREF);
  }

  //--Release2.1--start//
  //// Two new methods providing the business logic for the new link to toggle language.
  //// When touched this link should reload the page in opposite language (french versus english)
  //// and set this new session value.
  /**
   *
   *
   */
  public void handleToggleLanguageHrefRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleToggleLanguage();

    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getToggleLanguageHref()
  {
    return(HREF)getChild(CHILD_TOGGLELANGUAGEHREF);
  }

  //--Release2.1--end//
  /**
   *
   *
   */
  public void handleBtToolHistoryRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btToolHistory_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleDisplayDealHistory();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtToolHistory()
  {
    return(Button)getChild(CHILD_BTTOOLHISTORY);
  }

  /**
   *
   *
   */
  public void handleBtTooNotesRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btTooNotes_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleDisplayDealNotes(true);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtTooNotes()
  {
    return(Button)getChild(CHILD_BTTOONOTES);
  }

  /**
   *
   *
   */
  public void handleBtToolSearchRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btToolSearch_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleDealSearch();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtToolSearch()
  {
    return(Button)getChild(CHILD_BTTOOLSEARCH);
  }

  /**
   *
   *
   */
  public void handleBtToolLogRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btToolLog_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleSignOff();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtToolLog()
  {
    return(Button)getChild(CHILD_BTTOOLLOG);
  }

  /**
   *
   *
   */
  public StaticTextField getStErrorFlag()
  {
    return(StaticTextField)getChild(CHILD_STERRORFLAG);
  }

  /**
   *
   *
   */
  public HiddenField getDetectAlertTasks()
  {
    return(HiddenField)getChild(CHILD_DETECTALERTTASKS);
  }

  /**
   *
   *
   */
  public void handleBtPrevTaskPageRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btPrevTaskPage_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handlePrevTaskPage();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtPrevTaskPage()
  {
    return(Button)getChild(CHILD_BTPREVTASKPAGE);
  }

  /**
   *
   *
   */
  public String endBtPrevTaskPageDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btPrevTaskPage_onBeforeHtmlOutputEvent method

    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.generatePrevTaskPage();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public StaticTextField getStPrevTaskPageLabel()
  {
    return(StaticTextField)getChild(CHILD_STPREVTASKPAGELABEL);
  }

  /**
   *
   *
   */
  public String endStPrevTaskPageLabelDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the stPrevTaskPageLabel_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.generatePrevTaskPage();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public void handleBtNextTaskPageRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btNextTaskPage_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleNextTaskPage();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtNextTaskPage()
  {
    return(Button)getChild(CHILD_BTNEXTTASKPAGE);
  }

  /**
   *
   *
   */
  public String endBtNextTaskPageDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btNextTaskPage_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.generateNextTaskPage();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public StaticTextField getStNextTaskPageLabel()
  {
    return(StaticTextField)getChild(CHILD_STNEXTTASKPAGELABEL);
  }

  /**
   *
   *
   */
  public String endStNextTaskPageLabelDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the stNextTaskPageLabel_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.generateNextTaskPage();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public StaticTextField getStTaskName()
  {
    return(StaticTextField)getChild(CHILD_STTASKNAME);
  }

  /**
   *
   *
   */
  public String endStTaskNameDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the stTaskName_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.generateTaskName();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public HiddenField getSessionUserId()
  {
    return(HiddenField)getChild(CHILD_SESSIONUSERID);
  }

  /**
   *
   *
   */
  public HiddenField getHdApplicantId()
  {
    return(HiddenField)getChild(CHILD_HDAPPLICANTID);
  }

  /**
   *
   *
   */
  public HiddenField getHdApplicantCopyId()
  {
    return(HiddenField)getChild(CHILD_HDAPPLICANTCOPYID);
  }

  /**
   *
   *
   */
  public ComboBox getCbSalutation()
  {
    return(ComboBox)getChild(CHILD_CBSALUTATION);
  }

  /**
   *
   *
   */
  static class CbSalutationOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbSalutationOptionList()
    {

    }

    //--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 20Nov2002
    public void populate(RequestContext rc)
    {
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
          (SessionStateModelImpl)rc.getModelManager().getModel(
          SessionStateModel.class,
          defaultInstanceStateName,
          true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "SALUTATION", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
      }
      catch(Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

  /**
   *
   *
   */
  public TextField getTxFirstName()
  {
    return(TextField)getChild(CHILD_TXFIRSTNAME);
  }

  /**
   *
   *
   */
  public TextField getTxMiddleInitials()
  {
    return(TextField)getChild(CHILD_TXMIDDLEINITIALS);
  }

  /**
   *
   *
   */
  public TextField getTxLastName()
  {
    return(TextField)getChild(CHILD_TXLASTNAME);
  }

  /**
   *
   *
   */
  public ComboBox getCbApplicantType()
  {
    return(ComboBox)getChild(CHILD_CBAPPLICANTTYPE);
  }

  /**
   *
   *
   */
  static class CbApplicantTypeOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbApplicantTypeOptionList()
    {

    }

    //--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 20Nov2002
    public void populate(RequestContext rc)
    {
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
          (SessionStateModelImpl)rc.getModelManager().getModel(
          SessionStateModel.class,
          defaultInstanceStateName,
          true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "BORROWERTYPE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
      }
      catch(Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

  /**
   *
   *
   */
  public ComboBox getCbApplicantDOBMonth()
  {
    return(ComboBox)getChild(CHILD_CBAPPLICANTDOBMONTH);
  }

  /**
   *
   *
   */
  public TextField getTxApplicantDOBDay()
  {
    return(TextField)getChild(CHILD_TXAPPLICANTDOBDAY);
  }

  /**
   *
   *
   */
  public TextField getTxApplicantDOBYear()
  {
    return(TextField)getChild(CHILD_TXAPPLICANTDOBYEAR);
  }

  /**
   *
   *
   */
  public ComboBox getCbMartialStatus()
  {
    return(ComboBox)getChild(CHILD_CBMARTIALSTATUS);
  }

  /**
   *
   *
   */
  static class CbMartialStatusOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbMartialStatusOptionList()
    {

    }

    //--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 20Nov2002
    public void populate(RequestContext rc)
    {
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
          (SessionStateModelImpl)rc.getModelManager().getModel(
          SessionStateModel.class,
          defaultInstanceStateName,
          true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "MARITALSTATUS", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
      }
      catch(Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

  /**
   *
   *
   */
  public TextField getTxSINNo()
  {
    return(TextField)getChild(CHILD_TXSINNO);
  }

  /**
   *
   *
   */
  public ComboBox getCbCitizenship()
  {
    return(ComboBox)getChild(CHILD_CBCITIZENSHIP);
  }

  /**
   *
   *
   */
  static class CbCitizenshipOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbCitizenshipOptionList()
    {

    }

    //--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 20Nov2002
    public void populate(RequestContext rc)
    {
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
          (SessionStateModelImpl)rc.getModelManager().getModel(
          SessionStateModel.class,
          defaultInstanceStateName,
          true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                  "CITIZENSHIPTYPE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
      }
      catch(Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

  /**
   *
   *
   */
  public TextField getTxNoOfDependants()
  {
    return(TextField)getChild(CHILD_TXNOOFDEPENDANTS);
  }

  /**
   *
   *
   */
  public ComboBox getCbExistingClient()
  {
    return(ComboBox)getChild(CHILD_CBEXISTINGCLIENT);
  }

  /**
   *
   *
   */
  public TextField getTxReferenceClientNo()
  {
    return(TextField)getChild(CHILD_TXREFERENCECLIENTNO);
  }

  /**
   *
   *
   */
  public TextField getTxNoOfTimesBankrupt()
  {
    return(TextField)getChild(CHILD_TXNOOFTIMESBANKRUPT);
  }

  /**
   *
   *
   */
  public ComboBox getCbBankruptcyStatus()
  {
    return(ComboBox)getChild(CHILD_CBBANKRUPTCYSTATUS);
  }

  /**
   *
   *
   */
  static class CbBankruptcyStatusOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbBankruptcyStatusOptionList()
    {

    }

    //--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 20Nov2002
    public void populate(RequestContext rc)
    {
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
          (SessionStateModelImpl)rc.getModelManager().getModel(
          SessionStateModel.class,
          defaultInstanceStateName,
          true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "BANKRUPTCYSTATUS", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
      }
      catch(Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

  /**
   *
   *
   */
  public ComboBox getCbStaffOfLender()
  {
    return(ComboBox)getChild(CHILD_CBSTAFFOFLENDER);
  }

  //--DJ_LDI_CR--start--//
  /**
   *
   *
   */
  public HiddenField getHdBorrowerGenderId()
  {
    return (HiddenField)getChild(CHILD_HDBORROWERGENDERID);
  }

  /**
   *
   *
   */
  public StaticTextField getStIncludeLifeDisLabelsStart()
  {
    return (StaticTextField)getChild(CHILD_STINCLUDELIFEDISLABELSSTART);
  }

  /**
   *
   *
   */
  public StaticTextField getStIncludeLifeDisLabelsEnd()
  {
    return (StaticTextField)getChild(CHILD_STINCLUDELIFEDISLABELSEND);
  }

  /**
   *
   *
   */
  public ComboBox getCbBorrowerGender()
  {
    return (ComboBox)getChild(CHILD_CBBORROWERGENDER);
  }

  /**
   *
   *
   */
  static class CbBorrowerGenderOptionList extends OptionList
  {
    /**
     *
     *
     */
    CbBorrowerGenderOptionList(){  }

    /**
     *
     *
     */
    public void populate(RequestContext rc)
    {
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "BORROWERGENDER", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
    }
  }
  //--DJ_LDI_CR--end--//

  /**
   *
   *
   */
  public TextField getTxApplicantHomePhoneAreaCode()
  {
    return(TextField)getChild(CHILD_TXAPPLICANTHOMEPHONEAREACODE);
  }

  /**
   *
   *
   */
  public TextField getTxApplicantHomePhoneExchange()
  {
    return(TextField)getChild(CHILD_TXAPPLICANTHOMEPHONEEXCHANGE);
  }

  /**
   *
   *
   */
  public TextField getTxApplicantHomePhoneRoute()
  {
    return(TextField)getChild(CHILD_TXAPPLICANTHOMEPHONEROUTE);
  }

  /**
   *
   *
   */
  public TextField getTxApplicantWorkPhoneAreaCode()
  {
    return(TextField)getChild(CHILD_TXAPPLICANTWORKPHONEAREACODE);
  }

  /**
   *
   *
   */
  public TextField getTxApplicantWorkPhoneExchange()
  {
    return(TextField)getChild(CHILD_TXAPPLICANTWORKPHONEEXCHANGE);
  }

  /**
   *
   *
   */
  public TextField getTxApplicantWorkPhoneRoute()
  {
    return(TextField)getChild(CHILD_TXAPPLICANTWORKPHONEROUTE);
  }

  /**
   *
   *
   */
  public TextField getTxApplicantWorkPhoneExt()
  {
    return(TextField)getChild(CHILD_TXAPPLICANTWORKPHONEEXT);
  }

  /**
   *
   *
   */
  public TextField getTxApplicantFaxAreaCode()
  {
    return(TextField)getChild(CHILD_TXAPPLICANTFAXAREACODE);
  }

  /**
   *
   *
   */
  public TextField getTxApplicantFaxExchange()
  {
    return(TextField)getChild(CHILD_TXAPPLICANTFAXEXCHANGE);
  }

  /**
   *
   *
   */
  public TextField getTxApplicantFaxRoute()
  {
    return(TextField)getChild(CHILD_TXAPPLICANTFAXROUTE);
  }

  /**
   *
   *
   */
  public ComboBox getCbFirstTimeBuyer()
  {
    return(ComboBox)getChild(CHILD_CBFIRSTTIMEBUYER);
  }

  //--DJ_CR201.2--start//
	/**
	 *
	 *
	 */
	public ComboBox getCbGuarantorOtherLoans()
	{
		return (ComboBox)getChild(CHILD_CBGUARANTOROTHERLOANS);
	}
  //--DJ_CR201.2--end//

  /**
   *
   *
   */
  public TextField getTxEmailAddress()
  {
    return(TextField)getChild(CHILD_TXEMAILADDRESS);
  }

  /**
   *
   *
   */
  public ComboBox getCbLanguagePreference()
  {
    return(ComboBox)getChild(CHILD_CBLANGUAGEPREFERENCE);
  }

  /**
   *
   *
   */
  static class CbLanguagePreferenceOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbLanguagePreferenceOptionList()
    {

    }

    //--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 20Nov2002
    public void populate(RequestContext rc)
    {
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
          (SessionStateModelImpl)rc.getModelManager().getModel(
          SessionStateModel.class,
          defaultInstanceStateName,
          true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "LANGUAGEPREFERENCE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
      }
      catch(Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

//  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
  
  	public pgApplicantEntryRepeatedIdentificationTiledView 
  		getRepeatedIdentification()
  	{
  		return(pgApplicantEntryRepeatedIdentificationTiledView)
  			getChild(CHILD_REPEATEDIDENTIFICATION);
  	}
//  ***** Change by NBC Impl. Team - Version 1.1 - End *****//
  /**
   *
   *
   */
  public pgApplicantEntryRepeatedApplicantAddressTiledView getRepeatedApplicantAddress()
  {
    return(pgApplicantEntryRepeatedApplicantAddressTiledView)getChild(CHILD_REPEATEDAPPLICANTADDRESS);
  }

  /**
   *
   *
   */
  public void handleBtAddAddtionalApplicantAddressRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btAddAddtionalApplicantAddress_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleAddBorrowerAddress();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtAddAddtionalApplicantAddress()
  {
    return(Button)getChild(CHILD_BTADDADDTIONALAPPLICANTADDRESS);
  }

  /**
   *
   *
   */
  public String endBtAddAddtionalApplicantAddressDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btAddAddtionalApplicantAddress_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.displayEditButton();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public pgApplicantEntryRepeatedEmploymentTiledView getRepeatedEmployment()
  {
    return(pgApplicantEntryRepeatedEmploymentTiledView)getChild(CHILD_REPEATEDEMPLOYMENT);
  }

  /**
   *
   *
   */
  public void handleBtAddAdtionalEmploymentRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btAddAdtionalEmployment_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleAddEmploymentHistory();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtAddAdtionalEmployment()
  {
    return(Button)getChild(CHILD_BTADDADTIONALEMPLOYMENT);
  }

  /**
   *
   *
   */
  public String endBtAddAdtionalEmploymentDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btAddAdtionalEmployment_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.displayEditButton();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public pgApplicantEntryRepeatedOtherIncomeTiledView getRepeatedOtherIncome()
  {
    return(pgApplicantEntryRepeatedOtherIncomeTiledView)getChild(CHILD_REPEATEDOTHERINCOME);
  }


  //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
  	/**
	 * 
	 * <p>
	 * handles request to add additional identification type
	 * </p>
	 * 
	 * @param event
	 * @throws ServletException
	 * @throws IOException
	 */

	public void handleBtAddAdditionalApplicantIdentificationRequest(
			RequestInvocationEvent event) throws ServletException, IOException {
		ApplicantEntryHandler handler = (ApplicantEntryHandler) this.handler
				.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleAddIdentificationType();
		handler.postHandlerProtocol();
	}

	/**
	 * <p>
	 * Returns Button
	 * </p>
	 * 
	 * @return - Returns Button
	 */
	public Button getBtAddAdditionalApplicantIdentification() {
		return (Button) getChild(CHILD_BTADDADDITIONALAPPLICANTIDENTIFICATION);
	}
  
  

	/**
	 * @param event
	 * @return
	 */
	public String endBtAddAdditionalApplicantIdentificationDisplay(
			ChildContentDisplayEvent event) {

		ApplicantEntryHandler handler = (ApplicantEntryHandler) this.handler
				.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

		if (rc == true) {
			return event.getContent();
		} else {
			return "";
		}
	}
 // ***** Change by NBC Impl. Team - Version 1.1 - End *****//
  
  /**
   *
   *
   */
  public void handleBtAddAdditionalOtherIncomeRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btAddAdditionalOtherIncome_onWebEvent method

    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleAddIncome();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtAddAdditionalOtherIncome()
  {
    return(Button)getChild(CHILD_BTADDADDITIONALOTHERINCOME);
  }

  /**
   *
   *
   */
  public String endBtAddAdditionalOtherIncomeDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btAddAdditionalOtherIncome_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.displayEditButton();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public pgApplicantEntryRepeatedCreditReferenceTiledView getRepeatedCreditReference()
  {
    return(pgApplicantEntryRepeatedCreditReferenceTiledView)getChild(CHILD_REPEATEDCREDITREFERENCE);
  }

  /**
   *
   *
   */
  public void handleBtAddAditionalCreditRefsRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btAddAditionalCreditRefs_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleAddCreditRef();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtAddAditionalCreditRefs()
  {
    return(Button)getChild(CHILD_BTADDADITIONALCREDITREFS);
  }

  /**
   *
   *
   */
  public String endBtAddAditionalCreditRefsDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btAddAditionalCreditRefs_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.displayEditButton();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public void handleBtAddAddtionalAssetsRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btAddAddtionalAssets_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleAddAsset();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtAddAddtionalAssets()
  {
    return(Button)getChild(CHILD_BTADDADDTIONALASSETS);
  }

  /**
   *
   *
   */
  public String endBtAddAddtionalAssetsDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btAddAddtionalAssets_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.displayEditButton();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public pgApplicantEntryRepeatedAssetsTiledView getRepeatedAssets()
  {
    return(pgApplicantEntryRepeatedAssetsTiledView)getChild(CHILD_REPEATEDASSETS);
  }

  /**
   *
   *
   */
  public pgApplicantEntryRepeatedCreditBureauLiabilitiesTiledView getRepeatedCreditBureauLiabilities()
  {
    return(pgApplicantEntryRepeatedCreditBureauLiabilitiesTiledView)getChild(CHILD_REPEATEDCREDITBUREAULIABILITIES);
  }

  /**
   *
   *
   */
  public pgApplicantEntryRepeatedLiabilitiesTiledView getRepeatedLiabilities()
  {
    return(pgApplicantEntryRepeatedLiabilitiesTiledView)getChild(CHILD_REPEATEDLIABILITIES);
  }

  /**
   *
   *
   */
  public void handleBtDeleteLiabilityRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btDeleteLiability_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    Vector tmpIdxs = handler.getSelectedRowsFromCheckBoxes("RepeatedLiabilities/chbLiabilityDelete");

    //--> NOTE :: May need to take a look later (BILLYTODO)
    handler.handleCreditBureauExport(false, 1, tmpIdxs, "RepeatedLiabilities/hdLiabilityId", "liabilityId",
      "RepeatedLiabilities/hdLiabilityCopyId", "Liability", 0);
    //====================================================

    // flag 0 means either delete from Liability
    // or to move to the Credit Bureau Liability
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtDeleteLiability()
  {
    return(Button)getChild(CHILD_BTDELETELIABILITY);
  }

  /**
   *
   *
   */
  public String endBtDeleteLiabilityDisplay(ChildContentDisplayEvent event)
  {
    // The following code block was migrated from the btDeleteLiability_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.displayEditButton();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public void handleBtAddAditionalLiabilitiesRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btAddAditionalLiabilities_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleAddLiability();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtAddAditionalLiabilities()
  {
    return(Button)getChild(CHILD_BTADDADITIONALLIABILITIES);
  }

  /**
   *
   *
   */
  public String endBtAddAditionalLiabilitiesDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btAddAditionalLiabilities_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.displayEditButton();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public void handleBtApplicantSubmitRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btApplicantSubmit_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleDEASubmit();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtApplicantSubmit()
  {
    return(Button)getChild(CHILD_BTAPPLICANTSUBMIT);
  }

  /**
   *
   *
   */
  public String endBtApplicantSubmitDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btApplicantSubmit_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.displaySubmitButton();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public void handleBtApplicantCancelRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btApplicantCancel_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    // Changed to not saveing data (For better performance)-- By Billy 02Mat2002
    handler.preHandlerProtocol(this, true);
    // Changed to No confirm message (For better performance)-- By Billy 02Mat2002
    handler.handleCancelStandard(false);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtApplicantCancel()
  {
    return(Button)getChild(CHILD_BTAPPLICANTCANCEL);
  }

  /**
   *
   *
   */
  public String endBtApplicantCancelDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btApplicantCancel_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.displayCancelButton(1);
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public void handleBtApplicantCancelCurrentChangesRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btApplicantCancelCurrentChanges_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    // Changed to not saveing data (For better performance)-- By Billy 02Mat2002
    handler.preHandlerProtocol(this, true);
    // Changed to No confirm message (For better performance)-- By Billy 02Mat2002
    handler.handleCancelStandard(false);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtApplicantCancelCurrentChanges()
  {
    return(Button)getChild(CHILD_BTAPPLICANTCANCELCURRENTCHANGES);
  }

  /**
   *
   *
   */
  public String endBtApplicantCancelCurrentChangesDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btApplicantCancelCurrentChanges_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.displayCancelButton(2);
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public StaticTextField getStPmGenerate()
  {
    return(StaticTextField)getChild(CHILD_STPMGENERATE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmHasTitle()
  {
    return(StaticTextField)getChild(CHILD_STPMHASTITLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmHasInfo()
  {
    return(StaticTextField)getChild(CHILD_STPMHASINFO);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmHasTable()
  {
    return(StaticTextField)getChild(CHILD_STPMHASTABLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmHasOk()
  {
    return(StaticTextField)getChild(CHILD_STPMHASOK);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmTitle()
  {
    return(StaticTextField)getChild(CHILD_STPMTITLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmInfoMsg()
  {
    return(StaticTextField)getChild(CHILD_STPMINFOMSG);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmOnOk()
  {
    return(StaticTextField)getChild(CHILD_STPMONOK);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmMsgs()
  {
    return(StaticTextField)getChild(CHILD_STPMMSGS);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmMsgTypes()
  {
    return(StaticTextField)getChild(CHILD_STPMMSGTYPES);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmGenerate()
  {
    return(StaticTextField)getChild(CHILD_STAMGENERATE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmHasTitle()
  {
    return(StaticTextField)getChild(CHILD_STAMHASTITLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmHasInfo()
  {
    return(StaticTextField)getChild(CHILD_STAMHASINFO);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmHasTable()
  {
    return(StaticTextField)getChild(CHILD_STAMHASTABLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmTitle()
  {
    return(StaticTextField)getChild(CHILD_STAMTITLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmInfoMsg()
  {
    return(StaticTextField)getChild(CHILD_STAMINFOMSG);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmMsgs()
  {
    return(StaticTextField)getChild(CHILD_STAMMSGS);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmMsgTypes()
  {
    return(StaticTextField)getChild(CHILD_STAMMSGTYPES);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmDialogMsg()
  {
    return(StaticTextField)getChild(CHILD_STAMDIALOGMSG);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmButtonsHtml()
  {
    return(StaticTextField)getChild(CHILD_STAMBUTTONSHTML);
  }

  /**
   *
   *
   */
  public StaticTextField getStTargetAddress()
  {
    return(StaticTextField)getChild(CHILD_STTARGETADDRESS);
  }

  /**
   *
   *
   */
  public String endStTargetAddressDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the stTargetAddress_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.generateLoadTarget(Mc.TARGET_APP_ADDRESS);
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  
  /**
  *
  *
  */
 public String endStTargetIdentificationDisplay(ChildContentDisplayEvent event)
 {

   // The following code block was migrated from the stTargetAddress_onBeforeHtmlOutputEvent method
   ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
   handler.pageGetState(this);
   boolean rc = handler.generateLoadTarget(Mc.TARGET_APP_IDENTIFICATION);
   handler.pageSaveState();

   if(rc == true)
   {
     return event.getContent();
   }
   else
   {
     return "";
   }
 }
  
  	//  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
 
  	/**
	 * <p>
	 * Returns StaticTextField corresponding to target Identification
	 * </p>
	 * 
	 * @return - Returns StaticTextField corresponding to target Identification
	 */
	public StaticTextField getStTargetIdentification() {
		return (StaticTextField) getChild(CHILD_STTARGETIDENTIFICATION);
	}
	//  ***** Change by NBC Impl. Team - Version 1.1 - End *****//
  
  /**
   *
   *
   */
  public StaticTextField getStTargetEmployment()
  {
    return(StaticTextField)getChild(CHILD_STTARGETEMPLOYMENT);
  }

  /**
   *
   *
   */
  public String endStTargetEmploymentDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the stTargetEmployment_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.generateLoadTarget(Mc.TARGET_APP_EMPLOYMENT);
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public StaticTextField getStTargetIncome()
  {
    return(StaticTextField)getChild(CHILD_STTARGETINCOME);
  }

  /**
   *
   *
   */
  public String endStTargetIncomeDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the stTargetIncome_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.generateLoadTarget(Mc.TARGET_APP_INCOME);
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public StaticTextField getStTargetCredit()
  {
    return(StaticTextField)getChild(CHILD_STTARGETCREDIT);
  }

  /**
   *
   *
   */
  public String endStTargetCreditDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the stTargetCredit_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.generateLoadTarget(Mc.TARGET_APP_CREDIT);
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public StaticTextField getStTargetAsset()
  {
    return(StaticTextField)getChild(CHILD_STTARGETASSET);
  }

  /**
   *
   *
   */
  public String endStTargetAssetDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the stTargetAsset_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.generateLoadTarget(Mc.TARGET_APP_ASSET);
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public StaticTextField getStTargetLiab()
  {
    return(StaticTextField)getChild(CHILD_STTARGETLIAB);
  }

  /**
   *
   *
   */
  public String endStTargetLiabDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the stTargetLiab_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.generateLoadTarget(Mc.TARGET_APP_LIAB);
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public TextField getTxTotalLiab()
  {
    return(TextField)getChild(CHILD_TXTOTALLIAB);
  }

  /**
   *
   *
   */
  public TextField getTxTotalMonthlyLiab()
  {
    return(TextField)getChild(CHILD_TXTOTALMONTHLYLIAB);
  }

  /**
   *
   *
   */
  public TextField getTxTotalAsset()
  {
    return(TextField)getChild(CHILD_TXTOTALASSET);
  }

  /**
   *
   *
   */
  public TextField getTxTotalIncome()
  {
    return(TextField)getChild(CHILD_TXTOTALINCOME);
  }

  /**
   *
   *
   */
  public StaticTextField getStVALSData()
  {
    return(StaticTextField)getChild(CHILD_STVALSDATA);
  }

  /**
   *
   *
   */
  public void handleBtOKRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btOK_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleCancelStandard();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtOK()
  {
    return(Button)getChild(CHILD_BTOK);
  }

  /**
   *
   *
   */
  public String endBtOKDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btOK_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.displayOKButton();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public StaticTextField getStViewOnlyTag()
  {
    return(StaticTextField)getChild(CHILD_STVIEWONLYTAG);
  }

  /**
   *
   *
   */
  public String endStViewOnlyTagDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the stViewOnlyTag_onBeforeHtmlOutputEvent method

    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.displayViewOnlyTag();
    handler.pageSaveState();

    return rc;

  }

  /**
   *
   *
   */
  public void handleBtExportDeleteLiabilityRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btExportDeleteLiability_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);

    Vector exportIdxs = handler.getSelectedRowsFromCheckBoxes("RepeatedCreditBureauLiabilities/chCreditBureauLiabilityExport");
    Vector deleteIdxs = handler.getSelectedRowsFromCheckBoxes("RepeatedCreditBureauLiabilities/chCreditBureauLiabilityDelete");

    handler.handleCreditBureauExportDelete(false, 12, exportIdxs, deleteIdxs, "RepeatedCreditBureauLiabilities/hdCBLiabilityId",
      "liabilityId", "RepeatedCreditBureauLiabilities/hdCBLiabilityCopyId", "Liability", 1);

    handler.handleActMessageOK();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtExportDeleteLiability()
  {
    return(Button)getChild(CHILD_BTEXPORTDELETELIABILITY);
  }

  /**
   *
   *
   */
  public String endBtExportDeleteLiabilityDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btExportDeleteLiability_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.displayEditButton();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public StaticTextField getStTargetCreditBureauLiab()
  {
    return(StaticTextField)getChild(CHILD_STTARGETCREDITBUREAULIAB);
  }

  /**
   *
   *
   */
  public String endStTargetCreditBureauLiabDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the stTargetCreditBureauLiab_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.generateLoadTarget(Mc.TARGET_APP_CREDIT_BUREAU_LIAB);
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public StaticTextField getStLiabiliesLabel()
  {
    return(StaticTextField)getChild(CHILD_STLIABILIESLABEL);
  }

  /**
   *
   *
   */
  public StaticTextField getStTotalLiabiliesLabel()
  {
    return(StaticTextField)getChild(CHILD_STTOTALLIABILIESLABEL);
  }

  /**
   *
   *
   */
  public TextField getTxCreditBureauTotalLiab()
  {
    return(TextField)getChild(CHILD_TXCREDITBUREAUTOTALLIAB);
  }

  /**
   *
   *
   */
  public boolean beginTxCreditBureauTotalLiabDisplay(ChildDisplayEvent event)
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    handler.setCreditBureauLiabTotal(this, event);
    handler.pageSaveState();

    return true;

  }

  /**
   *
   *
   */
  public StaticTextField getStMonthPaymentLabel()
  {
    return(StaticTextField)getChild(CHILD_STMONTHPAYMENTLABEL);
  }

  /**
   *
   *
   */
  public TextField getTxCreditBureauTotalMonthlyLiab()
  {
    return(TextField)getChild(CHILD_TXCREDITBUREAUTOTALMONTHLYLIAB);
  }

  /**
   *
   *
   */
  public boolean beginTxCreditBureauTotalMonthlyLiabDisplay(ChildDisplayEvent event)
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    handler.setCreditBureauLiabMonthly(this, event);
    handler.pageSaveState();
    return true;

  }

  /**
   *
   *
   */
  public TextField getTxCreditBureauSummary()
  {
    return(TextField)getChild(CHILD_TXCREDITBUREAUSUMMARY);
  }

  /**
   *
   *
   */
  public StaticTextField getStCreditScoreLabel()
  {
    return(StaticTextField)getChild(CHILD_STCREDITSCORELABEL);
  }

  /**
   *
   *
   */
  public TextField getTxCreditScore()
  {
    return(TextField)getChild(CHILD_TXCREDITSCORE);
  }

  /**
   *
   *
   */
  public StaticTextField getStBureauViewButton()
  {
    return(StaticTextField)getChild(CHILD_STBUREAUVIEWBUTTON);
  }

  /**
   *
   *
   */
  public pgApplicantEntryrptCreditBureauReportsTiledView getRptCreditBureauReports()
  {
    return(pgApplicantEntryrptCreditBureauReportsTiledView)getChild(CHILD_RPTCREDITBUREAUREPORTS);
  }

  /**
   *
   *
   */
  public StaticTextField getStCombinedGDS()
  {
    return(StaticTextField)getChild(CHILD_STCOMBINEDGDS);
  }

  /**
   *
   *
   */
  public StaticTextField getStCombined3YrGDS()
  {
    return(StaticTextField)getChild(CHILD_STCOMBINED3YRGDS);
  }

  /**
   *
   *
   */
  public StaticTextField getStCombinedBorrowerGDS()
  {
    return(StaticTextField)getChild(CHILD_STCOMBINEDBORROWERGDS);
  }

  /**
   *
   *
   */
  public StaticTextField getStCombinedTDS()
  {
    return(StaticTextField)getChild(CHILD_STCOMBINEDTDS);
  }

  /**
   *
   *
   */
  public StaticTextField getStCombined3YrTDS()
  {
    return(StaticTextField)getChild(CHILD_STCOMBINED3YRTDS);
  }

  /**
   *
   *
   */
  public StaticTextField getStCombinedBorrowerTDS()
  {
    return(StaticTextField)getChild(CHILD_STCOMBINEDBORROWERTDS);
  }


	// ***** Change by NBC Impl. Team - Version 1.1 - Start *****//

	/**
	 * <p>Return ComboBox corresponding to smoke status</p>
	 * @return - ComboBox
	 */
	public ComboBox getCbSmokeStatus() {
		return (ComboBox) getChild(CHILD_CBSMOKESTATUS);
	}
	
	/**
	 * <p>Return ComboBox corresponding to solicitation</p>
	 * @return - ComboBox
	 */
	public ComboBox getCbSolicitation() {
		return (ComboBox) getChild(CHILD_CBSOLICITATION);
	}
	
	/**
	 * <p>Return TextField corresponding to Employee number</p>
	 * @return - TextField
	 */
	public TextField getTxEmployeeNo() {
		return (TextField) getChild(CHILD_TXEMPLOYEENO);
	}
	
	/**
	 * <p>Return ComboBox corresponding to smoke status</p>
	 * @return - ComboBox
	 */
	public ComboBox getCbPreferredContactMethod() {
		return (ComboBox) getChild(CHILD_CBPREFERREDCONTACTMETHOD);
	}

    /**
     * <p>Return ComboBox corresponding to suffix</p>
     * @return - ComboBox
     */
    public ComboBox getCbSuffix() {
        return (ComboBox) getChild(CHILD_CBSUFFIX);
    }
    
    /**
    *
    *
    */
   public TextField getTxApplicantCellAreaCode()
   {
     return(TextField)getChild(CHILD_TXAPPLICANTCELLAREACODE);
   }

   /**
    *
    *
    */
   public TextField getTxApplicantCellExchange()
   {
     return(TextField)getChild(CHILD_TXAPPLICANTCELLEXCHANGE);
   }

   /**
    *
    *
    */
   public TextField getTxApplicantCellRoute()
   {
     return(TextField)getChild(CHILD_TXAPPLICANTCELLROUTE);
   }
   
   /**
     * 
     */
    public StaticTextField getStLiabilityTileSize() {
        return (StaticTextField) getChild(CHILD_STLIABILITYTILESIZE);
    }
    
    /**
     * 
     */
    public StaticTextField getStCreditBureanTileSize() {
        return (StaticTextField) getChild(CHILD_STCREDITBUREAUTILESIZE);
    }
	// ***** Change by NBC Impl. Team - Version 1.1 - Start *****//

  /**
   *
   *
   */
  public void handleBtRecalculateRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btRecalculate_onWebEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleRecalculate();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtRecalculate()
  {
    return(Button)getChild(CHILD_BTRECALCULATE);
  }

  /**
   *
   *
   */
  public String endBtRecalculateDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btRecalculate_onBeforeHtmlOutputEvent method
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.displayEditButton();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public doApplicantEntryApplicantSelectModel getdoApplicantEntryApplicantSelectModel()
  {
    if(doApplicantEntryApplicantSelect == null)
    {
      doApplicantEntryApplicantSelect = (doApplicantEntryApplicantSelectModel)getModel(doApplicantEntryApplicantSelectModel.class);
    }
    return doApplicantEntryApplicantSelect;
  }

  /**
   *
   *
   */
  public void setdoApplicantEntryApplicantSelectModel(doApplicantEntryApplicantSelectModel model)
  {
    doApplicantEntryApplicantSelect = model;
  }

  /**
   *
   *
   */
  public doApplicantGSDTSDModel getdoApplicantGSDTSDModel()
  {
    if(doApplicantGSDTSD == null)
    {
      doApplicantGSDTSD = (doApplicantGSDTSDModel)getModel(doApplicantGSDTSDModel.class);
    }
    return doApplicantGSDTSD;
  }

  /**
   *
   *
   */
  public void setdoApplicantGSDTSDModel(doApplicantGSDTSDModel model)
  {
    doApplicantGSDTSD = model;
  }

  //--> Hidden ActMessageButton (FINDTHISLATER)
  //--> Test by BILLY 15July2002
  public Button getBtActMsg()
  {
    return(Button)getChild(CHILD_BTACTMSG);
  }

  //===========================================

  //--> Addition methods to propulate Href display String
  //--> Test by BILLY 07Aug2002
  public String endHref1Display(ChildContentDisplayEvent event)
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 0);
    handler.pageSaveState();

    return rc;
  }

  public String endHref2Display(ChildContentDisplayEvent event)
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 1);
    handler.pageSaveState();

    return rc;
  }

  public String endHref3Display(ChildContentDisplayEvent event)
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 2);
    handler.pageSaveState();

    return rc;
  }

  public String endHref4Display(ChildContentDisplayEvent event)
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 3);
    handler.pageSaveState();

    return rc;
  }

  public String endHref5Display(ChildContentDisplayEvent event)
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 4);
    handler.pageSaveState();

    return rc;
  }

  public String endHref6Display(ChildContentDisplayEvent event)
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 5);
    handler.pageSaveState();

    return rc;
  }

  public String endHref7Display(ChildContentDisplayEvent event)
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 6);
    handler.pageSaveState();

    return rc;
  }

  public String endHref8Display(ChildContentDisplayEvent event)
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 7);
    handler.pageSaveState();

    return rc;
  }

  public String endHref9Display(ChildContentDisplayEvent event)
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 8);
    handler.pageSaveState();

    return rc;
  }

  public String endHref10Display(ChildContentDisplayEvent event)
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 9);
    handler.pageSaveState();

    return rc;
  }

  //=====================================================
  //// This method overrides the getDefaultURL() framework JATO method and it is located
  //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
  //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
  //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
  //// The full method is still in PHC base class. It should care the
  //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
  public String getDisplayURL()
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this);

    String url = getDefaultDisplayURL();

    int languageId = handler.theSessionState.getLanguageId();

    //// Call the language specific URL (business delegation done in the BXResource).
    if(url != null && !url.trim().equals("") && !url.trim().equals("/"))
    {
      url = BXResources.getBXUrl(url, languageId);
    }
    else
    {
      url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
    }

    return url;
  }

  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Custom Methods - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////

  public void handleActMessageOK(String[] args)
  {
    ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this, true);

    handler.handleActMessageOK(args);

    handler.postHandlerProtocol();
  }

  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////

  public static final String PAGE_NAME = "pgApplicantEntry";

  //// It is a variable now (see explanation in the getDisplayURL() method above.
  ////public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgApplicantEntry.jsp";
  public static Map FIELD_DESCRIPTORS;
  public static final String CHILD_TBDEALID = "tbDealId";
  public static final String CHILD_TBDEALID_RESET_VALUE = "";
  public static final String CHILD_CBPAGENAMES = "cbPageNames";
  public static final String CHILD_CBPAGENAMES_RESET_VALUE = "";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By Billy 15Nov2002
  //private static CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  private CbPageNamesOptionList cbPageNamesOptions = new CbPageNamesOptionList();

  //Added the Variable for NonSelected Label
  private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";

  //==================================================================
  public static final String CHILD_BTPROCEED = "btProceed";
  public static final String CHILD_BTPROCEED_RESET_VALUE = "Proceed";
  public static final String CHILD_HREF1 = "Href1";
  public static final String CHILD_HREF1_RESET_VALUE = "";
  public static final String CHILD_HREF2 = "Href2";
  public static final String CHILD_HREF2_RESET_VALUE = "";
  public static final String CHILD_HREF3 = "Href3";
  public static final String CHILD_HREF3_RESET_VALUE = "";
  public static final String CHILD_HREF4 = "Href4";
  public static final String CHILD_HREF4_RESET_VALUE = "";
  public static final String CHILD_HREF5 = "Href5";
  public static final String CHILD_HREF5_RESET_VALUE = "";
  public static final String CHILD_HREF6 = "Href6";
  public static final String CHILD_HREF6_RESET_VALUE = "";
  public static final String CHILD_HREF7 = "Href7";
  public static final String CHILD_HREF7_RESET_VALUE = "";
  public static final String CHILD_HREF8 = "Href8";
  public static final String CHILD_HREF8_RESET_VALUE = "";
  public static final String CHILD_HREF9 = "Href9";
  public static final String CHILD_HREF9_RESET_VALUE = "";
  public static final String CHILD_HREF10 = "Href10";
  public static final String CHILD_HREF10_RESET_VALUE = "";
  public static final String CHILD_STPAGELABEL = "stPageLabel";
  public static final String CHILD_STPAGELABEL_RESET_VALUE = "";
  public static final String CHILD_STCOMPANYNAME = "stCompanyName";
  public static final String CHILD_STCOMPANYNAME_RESET_VALUE = "";
  public static final String CHILD_STTODAYDATE = "stTodayDate";
  public static final String CHILD_STTODAYDATE_RESET_VALUE = "";
  public static final String CHILD_STUSERNAMETITLE = "stUserNameTitle";
  public static final String CHILD_STUSERNAMETITLE_RESET_VALUE = "";
  public static final String CHILD_BTWORKQUEUELINK = "btWorkQueueLink";
  public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE = " ";
  public static final String CHILD_CHANGEPASSWORDHREF = "changePasswordHref";
  public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE = "";
  public static final String CHILD_BTTOOLHISTORY = "btToolHistory";
  public static final String CHILD_BTTOOLHISTORY_RESET_VALUE = " ";
  public static final String CHILD_BTTOONOTES = "btTooNotes";
  public static final String CHILD_BTTOONOTES_RESET_VALUE = " ";
  public static final String CHILD_BTTOOLSEARCH = "btToolSearch";
  public static final String CHILD_BTTOOLSEARCH_RESET_VALUE = " ";
  public static final String CHILD_BTTOOLLOG = "btToolLog";
  public static final String CHILD_BTTOOLLOG_RESET_VALUE = " ";
  public static final String CHILD_STERRORFLAG = "stErrorFlag";
  public static final String CHILD_STERRORFLAG_RESET_VALUE = "N";
  public static final String CHILD_DETECTALERTTASKS = "detectAlertTasks";
  public static final String CHILD_DETECTALERTTASKS_RESET_VALUE = "";
  public static final String CHILD_BTPREVTASKPAGE = "btPrevTaskPage";
  public static final String CHILD_BTPREVTASKPAGE_RESET_VALUE = " ";
  public static final String CHILD_STPREVTASKPAGELABEL = "stPrevTaskPageLabel";
  public static final String CHILD_STPREVTASKPAGELABEL_RESET_VALUE = "";
  public static final String CHILD_BTNEXTTASKPAGE = "btNextTaskPage";
  public static final String CHILD_BTNEXTTASKPAGE_RESET_VALUE = " ";
  public static final String CHILD_STNEXTTASKPAGELABEL = "stNextTaskPageLabel";
  public static final String CHILD_STNEXTTASKPAGELABEL_RESET_VALUE = "";
  public static final String CHILD_STTASKNAME = "stTaskName";
  public static final String CHILD_STTASKNAME_RESET_VALUE = "";
  public static final String CHILD_SESSIONUSERID = "sessionUserId";
  public static final String CHILD_SESSIONUSERID_RESET_VALUE = "";
  public static final String CHILD_HDAPPLICANTID = "hdApplicantId";
  public static final String CHILD_HDAPPLICANTID_RESET_VALUE = "";
  public static final String CHILD_HDAPPLICANTCOPYID = "hdApplicantCopyId";
  public static final String CHILD_HDAPPLICANTCOPYID_RESET_VALUE = "";
  public static final String CHILD_CBSALUTATION = "cbSalutation";
  public static final String CHILD_CBSALUTATION_RESET_VALUE = "";

  //--Release2.1--//
  //private static CbSalutationOptionList cbSalutationOptions=new CbSalutationOptionList();
  private CbSalutationOptionList cbSalutationOptions = new CbSalutationOptionList();

  //======================================================================================
  public static final String CHILD_TXFIRSTNAME = "txFirstName";
  public static final String CHILD_TXFIRSTNAME_RESET_VALUE = "";
  public static final String CHILD_TXMIDDLEINITIALS = "txMiddleInitials";
  public static final String CHILD_TXMIDDLEINITIALS_RESET_VALUE = "";
  public static final String CHILD_TXLASTNAME = "txLastName";
  public static final String CHILD_TXLASTNAME_RESET_VALUE = "";
  public static final String CHILD_CBAPPLICANTTYPE = "cbApplicantType";
  public static final String CHILD_CBAPPLICANTTYPE_RESET_VALUE = "";

  //--Release2.1--//
  //private static CbApplicantTypeOptionList cbApplicantTypeOptions=new CbApplicantTypeOptionList();
  private CbApplicantTypeOptionList cbApplicantTypeOptions = new CbApplicantTypeOptionList();

  //======================================================================================
  public static final String CHILD_CBAPPLICANTDOBMONTH = "cbApplicantDOBMonth";
  public static final String CHILD_CBAPPLICANTDOBMONTH_RESET_VALUE = "";
  private OptionList cbApplicantDOBMonthOptions = new OptionList(new String[]
    {}
    , new String[]
    {});
  public static final String CHILD_TXAPPLICANTDOBDAY = "txApplicantDOBDay";
  public static final String CHILD_TXAPPLICANTDOBDAY_RESET_VALUE = "";
  public static final String CHILD_TXAPPLICANTDOBYEAR = "txApplicantDOBYear";
  public static final String CHILD_TXAPPLICANTDOBYEAR_RESET_VALUE = "";
  public static final String CHILD_CBMARTIALSTATUS = "cbMartialStatus";
  public static final String CHILD_CBMARTIALSTATUS_RESET_VALUE = "";

  //--Release2.1--//
  //private static CbMartialStatusOptionList cbMartialStatusOptions=new CbMartialStatusOptionList();
  private CbMartialStatusOptionList cbMartialStatusOptions = new CbMartialStatusOptionList();

  //============================================================================================
  public static final String CHILD_TXSINNO = "txSINNo";
  public static final String CHILD_TXSINNO_RESET_VALUE = "";
  public static final String CHILD_CBCITIZENSHIP = "cbCitizenship";
  public static final String CHILD_CBCITIZENSHIP_RESET_VALUE = "";

  //--Release2.1--//
  //private static CbCitizenshipOptionList cbCitizenshipOptions=new CbCitizenshipOptionList();
  private CbCitizenshipOptionList cbCitizenshipOptions = new CbCitizenshipOptionList();

  //==========================================================================================
  public static final String CHILD_TXNOOFDEPENDANTS = "txNoOfDependants";
  public static final String CHILD_TXNOOFDEPENDANTS_RESET_VALUE = "";
  public static final String CHILD_CBEXISTINGCLIENT = "cbExistingClient";
  public static final String CHILD_CBEXISTINGCLIENT_RESET_VALUE = "N";

  //--Release2.1--//
  //--> this will be construct again on the BeginDisplay
  //private static OptionList cbExistingClientOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
  private OptionList cbExistingClientOptions = new OptionList();

  //=======================================================================
  public static final String CHILD_TXREFERENCECLIENTNO = "txReferenceClientNo";
  public static final String CHILD_TXREFERENCECLIENTNO_RESET_VALUE = "";
  public static final String CHILD_TXNOOFTIMESBANKRUPT = "txNoOfTimesBankrupt";
  public static final String CHILD_TXNOOFTIMESBANKRUPT_RESET_VALUE = "";
  public static final String CHILD_CBBANKRUPTCYSTATUS = "cbBankruptcyStatus";
  public static final String CHILD_CBBANKRUPTCYSTATUS_RESET_VALUE = "";

  //--Release2.1--//
  //private static CbBankruptcyStatusOptionList cbBankruptcyStatusOptions=new CbBankruptcyStatusOptionList();
  private CbBankruptcyStatusOptionList cbBankruptcyStatusOptions = new CbBankruptcyStatusOptionList();

  //=========================================================================================================
  public static final String CHILD_CBSTAFFOFLENDER = "cbStaffOfLender";
  public static final String CHILD_CBSTAFFOFLENDER_RESET_VALUE = "N";

  //--Release2.1--//
  //--> this will be construct again on the BeginDisplay
  //private static OptionList cbStaffOfLenderOptions=new OptionList(new String[]{"No", "Yes"},new String[]{"N", "Y"});
  private OptionList cbStaffOfLenderOptions = new OptionList();

  //=================================================================================
  public static final String CHILD_TXAPPLICANTHOMEPHONEAREACODE = "txApplicantHomePhoneAreaCode";
  public static final String CHILD_TXAPPLICANTHOMEPHONEAREACODE_RESET_VALUE = "";
  public static final String CHILD_TXAPPLICANTHOMEPHONEEXCHANGE = "txApplicantHomePhoneExchange";
  public static final String CHILD_TXAPPLICANTHOMEPHONEEXCHANGE_RESET_VALUE = "";
  public static final String CHILD_TXAPPLICANTHOMEPHONEROUTE = "txApplicantHomePhoneRoute";
  public static final String CHILD_TXAPPLICANTHOMEPHONEROUTE_RESET_VALUE = "";
  public static final String CHILD_TXAPPLICANTWORKPHONEAREACODE = "txApplicantWorkPhoneAreaCode";
  public static final String CHILD_TXAPPLICANTWORKPHONEAREACODE_RESET_VALUE = "";
  public static final String CHILD_TXAPPLICANTWORKPHONEEXCHANGE = "txApplicantWorkPhoneExchange";
  public static final String CHILD_TXAPPLICANTWORKPHONEEXCHANGE_RESET_VALUE = "";
  public static final String CHILD_TXAPPLICANTWORKPHONEROUTE = "txApplicantWorkPhoneRoute";
  public static final String CHILD_TXAPPLICANTWORKPHONEROUTE_RESET_VALUE = "";
  public static final String CHILD_TXAPPLICANTWORKPHONEEXT = "txApplicantWorkPhoneExt";
  public static final String CHILD_TXAPPLICANTWORKPHONEEXT_RESET_VALUE = "";
  public static final String CHILD_TXAPPLICANTFAXAREACODE = "txApplicantFaxAreaCode";
  public static final String CHILD_TXAPPLICANTFAXAREACODE_RESET_VALUE = "";
  public static final String CHILD_TXAPPLICANTFAXEXCHANGE = "txApplicantFaxExchange";
  public static final String CHILD_TXAPPLICANTFAXEXCHANGE_RESET_VALUE = "";
  public static final String CHILD_TXAPPLICANTFAXROUTE = "txApplicantFaxRoute";
  public static final String CHILD_TXAPPLICANTFAXROUTE_RESET_VALUE = "";
  public static final String CHILD_CBFIRSTTIMEBUYER = "cbFirstTimeBuyer";
  public static final String CHILD_CBFIRSTTIMEBUYER_RESET_VALUE = "N";

  //--Release2.1--//
  //--> this will be construct again on the BeginDisplay
  //private static OptionList cbFirstTimeBuyerOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
  private OptionList cbFirstTimeBuyerOptions = new OptionList();

  //=====================================================================================
  public static final String CHILD_TXEMAILADDRESS = "txEmailAddress";
  public static final String CHILD_TXEMAILADDRESS_RESET_VALUE = "";
  public static final String CHILD_CBLANGUAGEPREFERENCE = "cbLanguagePreference";
  public static final String CHILD_CBLANGUAGEPREFERENCE_RESET_VALUE = "";

  //--Release2.1--//
  //private static CbLanguagePreferenceOptionList cbLanguagePreferenceOptions=new CbLanguagePreferenceOptionList();
  private CbLanguagePreferenceOptionList cbLanguagePreferenceOptions = new CbLanguagePreferenceOptionList();

  //=========================================================================================================
  public static final String CHILD_REPEATEDAPPLICANTADDRESS = "RepeatedApplicantAddress";
  public static final String CHILD_BTADDADDTIONALAPPLICANTADDRESS = "btAddAddtionalApplicantAddress";
  public static final String CHILD_BTADDADDTIONALAPPLICANTADDRESS_RESET_VALUE = " ";
  public static final String CHILD_REPEATEDEMPLOYMENT = "RepeatedEmployment";
  public static final String CHILD_BTADDADTIONALEMPLOYMENT = "btAddAdtionalEmployment";
  public static final String CHILD_BTADDADTIONALEMPLOYMENT_RESET_VALUE = " ";
  public static final String CHILD_REPEATEDOTHERINCOME = "RepeatedOtherIncome";
  public static final String CHILD_BTADDADDITIONALOTHERINCOME = "btAddAdditionalOtherIncome";
  public static final String CHILD_BTADDADDITIONALOTHERINCOME_RESET_VALUE = " ";
  public static final String CHILD_REPEATEDCREDITREFERENCE = "RepeatedCreditReference";
  public static final String CHILD_BTADDADITIONALCREDITREFS = "btAddAditionalCreditRefs";
  public static final String CHILD_BTADDADITIONALCREDITREFS_RESET_VALUE = " ";
  public static final String CHILD_BTADDADDTIONALASSETS = "btAddAddtionalAssets";
  public static final String CHILD_BTADDADDTIONALASSETS_RESET_VALUE = " ";
  public static final String CHILD_REPEATEDASSETS = "RepeatedAssets";
  public static final String CHILD_REPEATEDCREDITBUREAULIABILITIES = "RepeatedCreditBureauLiabilities";
  public static final String CHILD_REPEATEDLIABILITIES = "RepeatedLiabilities";
  public static final String CHILD_BTDELETELIABILITY = "btDeleteLiability";
  public static final String CHILD_BTDELETELIABILITY_RESET_VALUE = " ";
  public static final String CHILD_BTADDADITIONALLIABILITIES = "btAddAditionalLiabilities";
  public static final String CHILD_BTADDADITIONALLIABILITIES_RESET_VALUE = " ";
  public static final String CHILD_BTAPPLICANTSUBMIT = "btApplicantSubmit";
  public static final String CHILD_BTAPPLICANTSUBMIT_RESET_VALUE = " ";
  public static final String CHILD_BTAPPLICANTCANCEL = "btApplicantCancel";
  public static final String CHILD_BTAPPLICANTCANCEL_RESET_VALUE = " ";
  public static final String CHILD_BTAPPLICANTCANCELCURRENTCHANGES = "btApplicantCancelCurrentChanges";
  public static final String CHILD_BTAPPLICANTCANCELCURRENTCHANGES_RESET_VALUE = " ";
  public static final String CHILD_STPMGENERATE = "stPmGenerate";
  public static final String CHILD_STPMGENERATE_RESET_VALUE = "";
  public static final String CHILD_STPMHASTITLE = "stPmHasTitle";
  public static final String CHILD_STPMHASTITLE_RESET_VALUE = "";
  public static final String CHILD_STPMHASINFO = "stPmHasInfo";
  public static final String CHILD_STPMHASINFO_RESET_VALUE = "";
  public static final String CHILD_STPMHASTABLE = "stPmHasTable";
  public static final String CHILD_STPMHASTABLE_RESET_VALUE = "";
  public static final String CHILD_STPMHASOK = "stPmHasOk";
  public static final String CHILD_STPMHASOK_RESET_VALUE = "";
  public static final String CHILD_STPMTITLE = "stPmTitle";
  public static final String CHILD_STPMTITLE_RESET_VALUE = "";
  public static final String CHILD_STPMINFOMSG = "stPmInfoMsg";
  public static final String CHILD_STPMINFOMSG_RESET_VALUE = "";
  public static final String CHILD_STPMONOK = "stPmOnOk";
  public static final String CHILD_STPMONOK_RESET_VALUE = "";
  public static final String CHILD_STPMMSGS = "stPmMsgs";
  public static final String CHILD_STPMMSGS_RESET_VALUE = "";
  public static final String CHILD_STPMMSGTYPES = "stPmMsgTypes";
  public static final String CHILD_STPMMSGTYPES_RESET_VALUE = "";
  public static final String CHILD_STAMGENERATE = "stAmGenerate";
  public static final String CHILD_STAMGENERATE_RESET_VALUE = "";
  public static final String CHILD_STAMHASTITLE = "stAmHasTitle";
  public static final String CHILD_STAMHASTITLE_RESET_VALUE = "";
  public static final String CHILD_STAMHASINFO = "stAmHasInfo";
  public static final String CHILD_STAMHASINFO_RESET_VALUE = "";
  public static final String CHILD_STAMHASTABLE = "stAmHasTable";
  public static final String CHILD_STAMHASTABLE_RESET_VALUE = "";
  public static final String CHILD_STAMTITLE = "stAmTitle";
  public static final String CHILD_STAMTITLE_RESET_VALUE = "";
  public static final String CHILD_STAMINFOMSG = "stAmInfoMsg";
  public static final String CHILD_STAMINFOMSG_RESET_VALUE = "";
  public static final String CHILD_STAMMSGS = "stAmMsgs";
  public static final String CHILD_STAMMSGS_RESET_VALUE = "";
  public static final String CHILD_STAMMSGTYPES = "stAmMsgTypes";
  public static final String CHILD_STAMMSGTYPES_RESET_VALUE = "";
  public static final String CHILD_STAMDIALOGMSG = "stAmDialogMsg";
  public static final String CHILD_STAMDIALOGMSG_RESET_VALUE = "";
  public static final String CHILD_STAMBUTTONSHTML = "stAmButtonsHtml";
  public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE = "";
  public static final String CHILD_STTARGETADDRESS = "stTargetAddress";
  public static final String CHILD_STTARGETADDRESS_RESET_VALUE = "";
  public static final String CHILD_STTARGETEMPLOYMENT = "stTargetEmployment";
  public static final String CHILD_STTARGETEMPLOYMENT_RESET_VALUE = "";
  public static final String CHILD_STTARGETINCOME = "stTargetIncome";
  public static final String CHILD_STTARGETINCOME_RESET_VALUE = "";
  public static final String CHILD_STTARGETCREDIT = "stTargetCredit";
  public static final String CHILD_STTARGETCREDIT_RESET_VALUE = "";
  public static final String CHILD_STTARGETASSET = "stTargetAsset";
  public static final String CHILD_STTARGETASSET_RESET_VALUE = "";
  public static final String CHILD_STTARGETLIAB = "stTargetLiab";
  public static final String CHILD_STTARGETLIAB_RESET_VALUE = "";
  public static final String CHILD_TXTOTALLIAB = "txTotalLiab";
  public static final String CHILD_TXTOTALLIAB_RESET_VALUE = "";
  public static final String CHILD_TXTOTALMONTHLYLIAB = "txTotalMonthlyLiab";
  public static final String CHILD_TXTOTALMONTHLYLIAB_RESET_VALUE = "";
  public static final String CHILD_TXTOTALASSET = "txTotalAsset";
  public static final String CHILD_TXTOTALASSET_RESET_VALUE = "";
  public static final String CHILD_TXTOTALINCOME = "txTotalIncome";
  public static final String CHILD_TXTOTALINCOME_RESET_VALUE = "";
  public static final String CHILD_STVALSDATA = "stVALSData";
  public static final String CHILD_STVALSDATA_RESET_VALUE = "";
  public static final String CHILD_BTOK = "btOK";
  public static final String CHILD_BTOK_RESET_VALUE = "OK";
  public static final String CHILD_STVIEWONLYTAG = "stViewOnlyTag";
  public static final String CHILD_STVIEWONLYTAG_RESET_VALUE = "";
  public static final String CHILD_BTEXPORTDELETELIABILITY = "btExportDeleteLiability";
  public static final String CHILD_BTEXPORTDELETELIABILITY_RESET_VALUE = " ";
  public static final String CHILD_STTARGETCREDITBUREAULIAB = "stTargetCreditBureauLiab";
  public static final String CHILD_STTARGETCREDITBUREAULIAB_RESET_VALUE = "";
  public static final String CHILD_STLIABILIESLABEL = "stLiabiliesLabel";
  public static final String CHILD_STLIABILIESLABEL_RESET_VALUE = "";
  public static final String CHILD_STTOTALLIABILIESLABEL = "stTotalLiabiliesLabel";
  public static final String CHILD_STTOTALLIABILIESLABEL_RESET_VALUE = "";
  public static final String CHILD_TXCREDITBUREAUTOTALLIAB = "txCreditBureauTotalLiab";
  public static final String CHILD_TXCREDITBUREAUTOTALLIAB_RESET_VALUE = "";
  public static final String CHILD_STMONTHPAYMENTLABEL = "stMonthPaymentLabel";
  public static final String CHILD_STMONTHPAYMENTLABEL_RESET_VALUE = "";
  public static final String CHILD_TXCREDITBUREAUTOTALMONTHLYLIAB = "txCreditBureauTotalMonthlyLiab";
  public static final String CHILD_TXCREDITBUREAUTOTALMONTHLYLIAB_RESET_VALUE = "";
  public static final String CHILD_TXCREDITBUREAUSUMMARY = "txCreditBureauSummary";
  public static final String CHILD_TXCREDITBUREAUSUMMARY_RESET_VALUE = "";
  public static final String CHILD_STCREDITSCORELABEL = "stCreditScoreLabel";
  public static final String CHILD_STCREDITSCORELABEL_RESET_VALUE = "";
  public static final String CHILD_TXCREDITSCORE = "txCreditScore";
  public static final String CHILD_TXCREDITSCORE_RESET_VALUE = "";
  public static final String CHILD_STBUREAUVIEWBUTTON = "stBureauViewButton";
  public static final String CHILD_STBUREAUVIEWBUTTON_RESET_VALUE = "";
  public static final String CHILD_RPTCREDITBUREAUREPORTS = "rptCreditBureauReports";
  public static final String CHILD_STCOMBINEDGDS = "stCombinedGDS";
  public static final String CHILD_STCOMBINEDGDS_RESET_VALUE = "";
  public static final String CHILD_STCOMBINED3YRGDS = "stCombined3YrGDS";
  public static final String CHILD_STCOMBINED3YRGDS_RESET_VALUE = "";
  public static final String CHILD_STCOMBINEDBORROWERGDS = "stCombinedBorrowerGDS";
  public static final String CHILD_STCOMBINEDBORROWERGDS_RESET_VALUE = "";
  public static final String CHILD_STCOMBINEDTDS = "stCombinedTDS";
  public static final String CHILD_STCOMBINEDTDS_RESET_VALUE = "";
  public static final String CHILD_STCOMBINED3YRTDS = "stCombined3YrTDS";
  public static final String CHILD_STCOMBINED3YRTDS_RESET_VALUE = "";
  public static final String CHILD_STCOMBINEDBORROWERTDS = "stCombinedBorrowerTDS";
  public static final String CHILD_STCOMBINEDBORROWERTDS_RESET_VALUE = "";
  public static final String CHILD_BTRECALCULATE = "btRecalculate";
  public static final String CHILD_BTRECALCULATE_RESET_VALUE = " ";

  //--> Hidden ActMessageButton (FINDTHISLATER)
  //--> Test by BILLY 15July2002
  public static final String CHILD_BTACTMSG = "btActMsg";
  public static final String CHILD_BTACTMSG_RESET_VALUE = "ActMsg";

  //--Release2.1--//
  //// New link to toggle the language. When touched this link should reload the
  //// page in opposite language (french versus english) and set this new language
  //// session id throughout all modulus.
  public static final String CHILD_TOGGLELANGUAGEHREF = "toggleLanguageHref";
  public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE = "";

  //--DJ_LDI_CR--start--//
  public static final String CHILD_HDBORROWERGENDERID="hdBorrowerGenderId";
  public static final String CHILD_HDBORROWERGENDERID_RESET_VALUE="";

  public static final String CHILD_CBBORROWERGENDER="cbBorrowerGender";
  public static final String CHILD_CBBORROWERGENDER_RESET_VALUE="";

  private CbBorrowerGenderOptionList cbBorrowerGenderOptions=new CbBorrowerGenderOptionList();

  public static final String CHILD_STINCLUDELIFEDISLABELSSTART="stIncludeLifeDisLabelsStart";
  public static final String CHILD_STINCLUDELIFEDISLABELSSTART_RESET_VALUE="";

  public static final String CHILD_STINCLUDELIFEDISLABELSEND="stIncludeLifeDisLabelsEnd";
  public static final String CHILD_STINCLUDELIFEDISLABELSEND_RESET_VALUE="";
  //--DJ_LDI_CR--end--//

  //--DJ_CR201.2--start//
	public static final String CHILD_CBGUARANTOROTHERLOANS="cbGuarantorOtherLoans";
	public static final String CHILD_CBGUARANTOROTHERLOANS_RESET_VALUE="N";

	private OptionList cbGuarantorOtherLoansOptions=new OptionList();
  //--DJ_CR201.2--end//

	//  ***** Change by NBC Impl. Team - Version 1.1 - End *****//
	public static final String CHILD_BTADDADDITIONALAPPLICANTIDENTIFICATION = 
			"btAddAdditionalApplicantIdentification";

	public static final String 
			CHILD_BTADDADDITIONALAPPLICANTIDENTIFICATION_RESET_VALUE = " ";

	public static final String CHILD_STTARGETIDENTIFICATION = 
			"stTargetIdentification";

	public static final String CHILD_STTARGETIDENTIFICATION_RESET_VALUE = "";

	public static final String CHILD_REPEATEDIDENTIFICATION = 
			"RepeatedIdentification";

	public static final String CHILD_CBSMOKESTATUS = "cbSmokeStatus";

	public static final String CHILD_CBSMOKESTATUS_RESET_VALUE = "";

	public static final String CHILD_CBSOLICITATION = "cbSolicitation";

	public static final String CHILD_CBSOLICITATION_RESET_VALUE = "";

	public static final String CHILD_TXEMPLOYEENO = "txEmployeeNo";

	public static final String CHILD_TXEMPLOYEENO_RESET_VALUE = "";

	public static final String CHILD_CBPREFERREDCONTACTMETHOD = 
			"cbPreferredContactMethod";

	public static final String CHILD_CBPREFERREDCONTACTMETHOD_RESET_VALUE = "";
    
    
    public static final String CHILD_CBSUFFIX = "cbSuffix";
    public static final String CHILD_CBSUFFIX_RESET_VALUE = "";
    
    public static final String CHILD_TXAPPLICANTCELLAREACODE = "txApplicantCellAreaCode";
    public static final String CHILD_TXAPPLICANTCELLAREACODE_RESET_VALUE = "";
    public static final String CHILD_TXAPPLICANTCELLEXCHANGE = "txApplicantCellExchange";
    public static final String CHILD_TXAPPLICANTCELLEXCHANGE_RESET_VALUE = "";
    public static final String CHILD_TXAPPLICANTCELLROUTE = "txApplicantCellRoute";
    public static final String CHILD_TXAPPLICANTCELLROUTE_RESET_VALUE = "";

    public static final String CHILD_STLIABILITYTILESIZE = "stLiabilityTileSize";
    public static final String CHILD_STLIABILITYTILESIZE_RESET_VALUE = "";
    
    public static final String CHILD_STCREDITBUREAUTILESIZE = "stCreditBureauTileSize";
    public static final String CHILD_STCREDITBUREAUTILESIZE_RESET_VALUE = "";
    
	private CbSmokeStatusOptionList cbSmokeStatusOptions = 
			new CbSmokeStatusOptionList();

	static class CbSmokeStatusOptionList extends OptionList {
		CbSmokeStatusOptionList() {
		}

		public void populate(RequestContext requestContext) {
			try {
				//Get Language from SessionState
				//Get the Language ID from the SessionStateModel
				String defaultInstanceStateName = 
						requestContext.getModelManager()
						.getDefaultModelInstanceName(SessionStateModel.class);
				
				SessionStateModelImpl theSessionState = 
						(SessionStateModelImpl) requestContext
						.getModelManager().getModel(SessionStateModel.class,
						defaultInstanceStateName, true);
				
				int languageId = theSessionState.getLanguageId();

				//Get IDs and Labels from BXResources
				Collection collection 
                    = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
						"BORROWERSMOKER", languageId);
				Iterator iterator = collection.iterator();
				String[] theVal = new String[2];
				while (iterator.hasNext()) {
					theVal = (String[]) (iterator.next());
					add(theVal[1], theVal[0]);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	private cbSolicitationOptionList cbSolicitationOptions = 
			new cbSolicitationOptionList();

	static class cbSolicitationOptionList extends OptionList {
		cbSolicitationOptionList() {
		}

		public void populate(RequestContext requestContext) {
			try {
				//Get Language from SessionState
				//Get the Language ID from the SessionStateModel
				String defaultInstanceStateName = 
						requestContext.getModelManager()
						.getDefaultModelInstanceName(SessionStateModel.class);
				
				SessionStateModelImpl theSessionState = 
						(SessionStateModelImpl) 
						requestContext.getModelManager().getModel(
						SessionStateModel.class,defaultInstanceStateName, true);
				
				int languageId = theSessionState.getLanguageId();

				//Get IDs and Labels from BXResources
				Collection collection 
                    = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
						"SOLICITATION", languageId);
				
				Iterator iterator = collection.iterator();
				String[] theVal = new String[2];
				while (iterator.hasNext()) {
					theVal = (String[]) (iterator.next());
					add(theVal[1], theVal[0]);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	private CbPreferredContactMethodOptionList cbPreferredContactMethodOptions 
			= new CbPreferredContactMethodOptionList();

	static class CbPreferredContactMethodOptionList extends OptionList {
		CbPreferredContactMethodOptionList() {
		}

		public void populate(RequestContext requestContext) {
			try {
				// Get Language from SessionState
				// Get the Language ID from the SessionStateModel

				String defaultInstanceStateName = requestContext
						.getModelManager().getDefaultModelInstanceName(
								SessionStateModel.class);

				SessionStateModelImpl theSessionState = 
						(SessionStateModelImpl) requestContext
						.getModelManager().getModel(SessionStateModel.class,
						defaultInstanceStateName, true);

				int languageId = theSessionState.getLanguageId();

				// Get IDs and Labels from BXResources
				Collection collection 
                    = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
						"PREFMETHODOFCONTACT", languageId);

				Iterator iterator = collection.iterator();
				String[] theVal = new String[2];
				while (iterator.hasNext()) {
					theVal = (String[]) (iterator.next());
					add(theVal[1], theVal[0]);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	// ***** Change by NBC Impl. Team - Version 1.1 - End *****//
	
    private cbSuffixOptionList cbSuffixOptions 
        = new cbSuffixOptionList();
    
    static class cbSuffixOptionList extends OptionList {
        cbSuffixOptionList() {
        }

        public void populate(RequestContext requestContext) {
            try {
                //Get Language from SessionState
                //Get the Language ID from the SessionStateModel
                String defaultInstanceStateName = 
                        requestContext.getModelManager()
                        .getDefaultModelInstanceName(SessionStateModel.class);
                
                SessionStateModelImpl theSessionState = 
                        (SessionStateModelImpl) 
                        requestContext.getModelManager().getModel(
                        SessionStateModel.class,defaultInstanceStateName, true);
                
                int languageId = theSessionState.getLanguageId();

                //Get IDs and Labels from BXResources
                Collection collection 
                    = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                        "SUFFIX", languageId);
                
                Iterator iterator = collection.iterator();
                String[] theVal = new String[2];
                while (iterator.hasNext()) {
                    theVal = (String[]) (iterator.next());
                    add(theVal[1], theVal[0]);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////

  private doApplicantEntryApplicantSelectModel doApplicantEntryApplicantSelect = null;
  private doApplicantGSDTSDModel doApplicantGSDTSD = null;
  protected String DEFAULT_DISPLAY_URL = "/mosApp/MosSystem/pgApplicantEntry.jsp";

  ////////////////////////////////////////////////////////////////////////////
  // Custom Members - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////

  private ApplicantEntryHandler handler = new ApplicantEntryHandler();
  public SysLogger logger;

}
