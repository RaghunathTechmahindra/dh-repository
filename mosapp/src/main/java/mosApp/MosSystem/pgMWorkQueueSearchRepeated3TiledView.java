package mosApp.MosSystem;

import java.util.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

import com.basis100.log.*;

/**
 * --TD_MWQ_CR--start--
 * Outstanding Task Section should include counting by some TaskName as well.
 *
 */
public class pgMWorkQueueSearchRepeated3TiledView extends RequestHandlingTiledViewBase implements TiledView, RequestHandler {
	/**
	 *
	 *
	 */
	public pgMWorkQueueSearchRepeated3TiledView(View parent, String name) {
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass(doTasksOutstandingsModel.class);
		registerChildren();
		initialize();
	}

	/**
	 *
	 *
	 */
	protected void initialize() {
	}

	/**
	 *
	 *
	 */
	public void resetChildren() {
		getStTaskNameLabel().setValue(CHILD_STTASKNAMELABEL_RESET_VALUE);
		getStTaskNameCount().setValue(CHILD_STTASKNAMECOUNT_RESET_VALUE);
	}

	/**
	 *
	 *
	 */
	protected void registerChildren() {
		registerChild(CHILD_STTASKNAMELABEL, StaticTextField.class);
		registerChild(CHILD_STTASKNAMECOUNT, StaticTextField.class);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType) {
		List modelList = new ArrayList();
		switch (executionType) {
		case MODEL_TYPE_RETRIEVE:
			modelList.add(getdoTasksOutstandingsModel());
			break;

		case MODEL_TYPE_UPDATE:
			;
			break;

		case MODEL_TYPE_DELETE:
			;
			break;

		case MODEL_TYPE_INSERT:
			;
			break;

		case MODEL_TYPE_EXECUTE:
			;
			break;
		}
		return (Model[]) modelList.toArray(new Model[0]);
	}

	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event) throws ModelControlException {
		//--> BILLYDEBUG 15Jan2004
		logger.debug("pgMWQRepeated3@beginDisplay ::start !!");
		//==================================

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null)
			throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}

	public boolean nextTile() throws ModelControlException {
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow) {
			int rowNum = this.getTileIndex();
			boolean ret = true;
			// Put migrated repeated_onBeforeRowDisplayEvent code here
			MasterWorkQueueSearchHandler handler = (MasterWorkQueueSearchHandler) this.handler.cloneSS();
			handler.pageGetState(this.getParentViewBean());
			ret = handler.checkDisplayThisRow("Repeated3");
			doTasksOutstandingsModelImpl theObj = (doTasksOutstandingsModelImpl) getdoTasksOutstandingsModel();
			if (theObj.getNumRows() > 0 && ret == true) {
				String taskId = theObj.getValue(doTasksOutstandingsModelImpl.FIELD_DFTASKID).toString();
				handler.setTaskCountByTaskName(rowNum, new Integer(taskId));
			}
			handler.pageSaveState();
			return ret;
		}

		if (movedToRow == false) {
			logger.debug("pgMWRepeated3@nextTile::Finished !!");
		}

		return movedToRow;
	}

	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext) {

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}

	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext) {

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}

	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext) {

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}

	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception) throws ModelControlException {

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}

	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name) {
		if (name.equals(CHILD_STTASKNAMELABEL)) {
			StaticTextField child = new StaticTextField(this, getdoTasksOutstandingsModel(), CHILD_STTASKNAMELABEL, doTasksOutstandingsModel.FIELD_DFTASKNAMELABEL, CHILD_STTASKNAMELABEL_RESET_VALUE, null);
			return child;
		} else if (name.equals(CHILD_STTASKNAMECOUNT)) {
			StaticTextField child = new StaticTextField(this, getDefaultModel(), CHILD_STTASKNAMECOUNT, CHILD_STTASKNAMECOUNT, CHILD_STTASKNAMECOUNT_RESET_VALUE, null);
			return child;
		} else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStTaskNameLabel() {
		return (StaticTextField) getChild(CHILD_STTASKNAMELABEL);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStTaskNameCount() {
		return (StaticTextField) getChild(CHILD_STTASKNAMECOUNT);
	}

	/**
	 *
	 *
	 */
	public doTasksOutstandingsModel getdoTasksOutstandingsModel() {
		if (doTasksOutstandings == null)
			doTasksOutstandings = (doTasksOutstandingsModel) getModel(doTasksOutstandingsModel.class);
		return doTasksOutstandings;
	}

	/**
	 *
	 *
	 */
	public void setdoTasksOutstandingsModel(doTasksOutstandingsModel model) {
		doTasksOutstandings = model;
	}

	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STTASKNAMELABEL = "stTaskNameLabel";
	public static final String CHILD_STTASKNAMELABEL_RESET_VALUE = "";
	public static final String CHILD_STTASKNAMECOUNT = "stTaskNameCount";
	public static final String CHILD_STTASKNAMECOUNT_RESET_VALUE = "";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doTasksOutstandingsModel doTasksOutstandings = null;
	public SysLogger logger = SysLog.getSysLogger("MWQSR3TV");;
	private MasterWorkQueueSearchHandler handler = new MasterWorkQueueSearchHandler();

}
