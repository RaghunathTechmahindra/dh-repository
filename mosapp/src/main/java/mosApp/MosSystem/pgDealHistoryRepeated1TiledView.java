package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgDealHistoryRepeated1TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealHistoryRepeated1TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doDealHistoryModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStTransactionDate().setValue(CHILD_STTRANSACTIONDATE_RESET_VALUE);
		getStTransactionType().setValue(CHILD_STTRANSACTIONTYPE_RESET_VALUE);
		getStUserId().setValue(CHILD_STUSERID_RESET_VALUE);
		getStStatus().setValue(CHILD_STSTATUS_RESET_VALUE);
		getStTransactionText().setValue(CHILD_STTRANSACTIONTEXT_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STTRANSACTIONDATE,StaticTextField.class);
		registerChild(CHILD_STTRANSACTIONTYPE,StaticTextField.class);
		registerChild(CHILD_STUSERID,StaticTextField.class);
		registerChild(CHILD_STSTATUS,StaticTextField.class);
		registerChild(CHILD_STTRANSACTIONTEXT,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDealHistoryModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
    {
      //--> Not necessary to cal the following
      //--> Commented out by Billy 09Sept2002
      /*
			DealHistoryHandler handler =(DealHistoryHandler) this.handler.cloneSS();
			handler.pageGetState(this);
			int rc = handler.viewRepeated();
			handler.pageSaveState();
			return rc;
      */
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STTRANSACTIONDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealHistoryModel(),
				CHILD_STTRANSACTIONDATE,
				doDealHistoryModel.FIELD_DFTRANSACTIONDATE,
				CHILD_STTRANSACTIONDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTRANSACTIONTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealHistoryModel(),
				CHILD_STTRANSACTIONTYPE,
				doDealHistoryModel.FIELD_DFTRANSACTIONTYPE,
				CHILD_STTRANSACTIONTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUSERID))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealHistoryModel(),
				CHILD_STUSERID,
				doDealHistoryModel.FIELD_DFUSER,
				CHILD_STUSERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealHistoryModel(),
				CHILD_STSTATUS,
				doDealHistoryModel.FIELD_DFSTATUS,
				CHILD_STSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTRANSACTIONTEXT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealHistoryModel(),
				CHILD_STTRANSACTIONTEXT,
				doDealHistoryModel.FIELD_DFTRANSACTIONTEXT,
				CHILD_STTRANSACTIONTEXT_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTransactionDate()
	{
		return (StaticTextField)getChild(CHILD_STTRANSACTIONDATE);
	}


	/**
	 *
	 *
	 */
	public boolean beginStTransactionDateDisplay(ChildDisplayEvent event)
	{
		// The following code block was migrated from the stTransactionDate_onBeforeDisplayEvent method
		DealHistoryHandler handler =(DealHistoryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.convertToUserTimeZone(this.getParentViewBean(), "Repeated1/stTransactionDate");
		handler.pageSaveState();

		return true;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTransactionType()
	{
		return (StaticTextField)getChild(CHILD_STTRANSACTIONTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUserId()
	{
		return (StaticTextField)getChild(CHILD_STUSERID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStStatus()
	{
		return (StaticTextField)getChild(CHILD_STSTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTransactionText()
	{
		return (StaticTextField)getChild(CHILD_STTRANSACTIONTEXT);
	}


	/**
	 *
	 *
	 */
	public doDealHistoryModel getdoDealHistoryModel()
	{
		if (doDealHistory == null)
			doDealHistory = (doDealHistoryModel) getModel(doDealHistoryModel.class);
		return doDealHistory;
	}


	/**
	 *
	 *
	 */
	public void setdoDealHistoryModel(doDealHistoryModel model)
	{
			doDealHistory = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STTRANSACTIONDATE="stTransactionDate";
	public static final String CHILD_STTRANSACTIONDATE_RESET_VALUE="";
	public static final String CHILD_STTRANSACTIONTYPE="stTransactionType";
	public static final String CHILD_STTRANSACTIONTYPE_RESET_VALUE="";
	public static final String CHILD_STUSERID="stUserId";
	public static final String CHILD_STUSERID_RESET_VALUE="";
	public static final String CHILD_STSTATUS="stStatus";
	public static final String CHILD_STSTATUS_RESET_VALUE="";
	public static final String CHILD_STTRANSACTIONTEXT="stTransactionText";
	public static final String CHILD_STTRANSACTIONTEXT_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDealHistoryModel doDealHistory=null;
  private DealHistoryHandler handler=new DealHistoryHandler();

}

