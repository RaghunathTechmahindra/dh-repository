package mosApp.MosSystem;

import java.sql.SQLException;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doDealEntryGDSTDSAppDetailsModelImpl
  extends QueryModelBase
  implements doDealEntryGDSTDSAppDetailsModel
{
  /**
   *
   *
   */
  public doDealEntryGDSTDSAppDetailsModelImpl()
  {
    super();
    setDataSourceName(DATA_SOURCE_NAME);

    setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

    setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

    setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

    setFieldSchema(FIELD_SCHEMA);

    initialize();

  }

  /**
   *
   *
   */
  public void initialize()
  {
  }

  ////////////////////////////////////////////////////////////////////////////////
  // NetDynamics-migrated events
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  protected String beforeExecute(ModelExecutionContext context, int queryType, String sql) throws ModelControlException
  {

    // TODO: Migrate specific onBeforeExecute code
    return sql;

  }

  /**
   *
   *
   */
  protected void afterExecute(ModelExecutionContext context, int queryType) throws ModelControlException
  {
    //--Release2.1--//
    ////To override all the Description fields by populating from BXResource.
    ////Get the Language ID from the SessionStateModel.
    String defaultInstanceStateName =
      getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);

    SessionStateModelImpl theSessionState =
      (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
      SessionStateModel.class,
      defaultInstanceStateName,
      true);

    int languageId = theSessionState.getLanguageId();
	int institutionId = theSessionState.getDealInstitutionId();

    while(this.next())
    {
      //// Convert BorrowerStatus Desc.
      this.setDfBorrowerType(
        BXResources.getPickListDescription(institutionId, "BORROWERTYPE", this.getDfBorrowerType(), languageId));
      //--> Bug fix to support Bilingual : By Billy 19May2004
      this.setDfInsuranceProportionsId(
        BXResources.getPickListDescription(institutionId, "INSURANCEPROPORTIONS", this.getDfInsuranceProportionsId(), languageId));
      this.setDfDisabilityStatusId(
        BXResources.getPickListDescription(institutionId, "DISABILITYSTATUS", this.getDfDisabilityStatusId(), languageId));
      this.setDfLifeStatusId(
        BXResources.getPickListDescription(institutionId, "LIFESTATUS", this.getDfLifeStatusId(), languageId));
      //======================================================
      this.setDfSuffix(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(), "SUFFIX", this.getDfSuffix(), languageId)); 

    }

    //Reset Location
    this.beforeFirst();
  }

  /**
   *
   *
   */
  protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
  {
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfDealId()
  {
    return(java.math.BigDecimal)getValue(FIELD_DFDEALID);
  }

  /**
   *
   *
   */
  public void setDfDealId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFDEALID, value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfBorrowerId()
  {
    return(java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
  }

  /**
   *
   *
   */
  public void setDfBorrowerId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFBORROWERID, value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfCopyId()
  {
    return(java.math.BigDecimal)getValue(FIELD_DFCOPYID);
  }

  /**
   *
   *
   */
  public void setDfCopyId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFCOPYID, value);
  }

  /**
   *
   *
   */
  public String getDfFirstName()
  {
    return(String)getValue(FIELD_DFFIRSTNAME);
  }

  /**
   *
   *
   */
  public void setDfFirstName(String value)
  {
    setValue(FIELD_DFFIRSTNAME, value);
  }

  /**
   *
   *
   */
  public String getDfMiddleInitial()
  {
    return(String)getValue(FIELD_DFMIDDLEINITIAL);
  }

  /**
   *
   *
   */
  public void setDfMiddleInitial(String value)
  {
    setValue(FIELD_DFMIDDLEINITIAL, value);
  }

  /**
   *
   *
   */
  public String getDfLastName()
  {
    return(String)getValue(FIELD_DFLASTNAME);
  }

  /**
   *
   *
   */
  public void setDfLastName(String value)
  {
    setValue(FIELD_DFLASTNAME, value);
  }

  
  /**
   *
   *
   */
  public String getDfSuffix()
  {
    return(String)getValue(FIELD_DFSUFFIX);
  }

  /**
   *
   *
   */
  public void setDfSuffix(String value)
  {
    setValue(FIELD_DFSUFFIX, value);
  }
 
  /**
   *
   *
   */
  public String getDfBorrowerType()
  {
    return(String)getValue(FIELD_DFBORROWERTYPE);
  }

  /**
   *
   *
   */
  public void setDfBorrowerType(String value)
  {
    setValue(FIELD_DFBORROWERTYPE, value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfBorrowerGDS()
  {
    return(java.math.BigDecimal)getValue(FIELD_DFBORROWERGDS);
  }

  /**
   *
   *
   */
  public void setDfBorrowerGDS(java.math.BigDecimal value)
  {
    setValue(FIELD_DFBORROWERGDS, value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfBorrowerGDS3Yr()
  {
    return(java.math.BigDecimal)getValue(FIELD_DFBORROWERGDS3YR);
  }

  /**
   *
   *
   */
  public void setDfBorrowerGDS3Yr(java.math.BigDecimal value)
  {
    setValue(FIELD_DFBORROWERGDS3YR, value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfBorrowerTDS()
  {
    return(java.math.BigDecimal)getValue(FIELD_DFBORROWERTDS);
  }

  /**
   *
   *
   */
  public void setDfBorrowerTDS(java.math.BigDecimal value)
  {
    setValue(FIELD_DFBORROWERTDS, value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfBorrowerTDS3Yr()
  {
    return(java.math.BigDecimal)getValue(FIELD_DFBORROWERTDS3YR);
  }

  /**
   *
   *
   */
  public void setDfBorrowerTDS3Yr(java.math.BigDecimal value)
  {
    setValue(FIELD_DFBORROWERTDS3YR, value);
  }

  //--DJ_LDI_CR--start--//
  /**
   *
   *
   */
  public String getDfInsuranceProportionsId()
  {
    return(String)getValue(FIELD_DFINSURANCEPROPORTIONSID);
  }

  /**
   *
   *
   */
  public void setDfInsuranceProportionsId(String value)
  {
    setValue(FIELD_DFINSURANCEPROPORTIONSID, value);
  }

  /**
   *
   *
   */
  public String getDfLifeStatusId()
  {
    return(String)getValue(FIELD_DFLIFESTATUSID);
  }

  /**
   *
   *
   */
  public void setDfLifeStatusId(String value)
  {
    setValue(FIELD_DFLIFESTATUSID, value);
  }

  /**
   *
   *
   */
  public String getDfDisabilityStatusId()
  {
    return(String)getValue(FIELD_DFDISABILITYSTATUSID);
  }

  /**
   *
   *
   */
  public void setDfDisabilityStatusId(String value)
  {
    setValue(FIELD_DFDISABILITYSTATUSID, value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getLifePremium()
  {
    return(java.math.BigDecimal)getValue(FIELD_DFLIFEPREMIUM);
  }

  /**
   *
   *
   */
  public void setLifePremium(java.math.BigDecimal value)
  {
    setValue(FIELD_DFLIFEPREMIUM, value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDisabilityPremium()
  {
    return(java.math.BigDecimal)getValue(FIELD_DFDISABILITYPREMIUM);
  }

  /**
   *
   *
   */
  public void setDisabilityPremium(java.math.BigDecimal value)
  {
    setValue(FIELD_DFDISABILITYPREMIUM, value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getCombinedLDPremium()
  {
    return(java.math.BigDecimal)getValue(FIELD_DFCOMBINEDLDPREMIUM);
  }

  /**
   *
   *
   */
  public void setCombinedLDPremium(java.math.BigDecimal value)
  {
    setValue(FIELD_DFCOMBINEDLDPREMIUM, value);
  }

  //--DJ_LDI_CR--end--//

  //--DJ_CR201.2--start//
  /**
   *
   *
   */
  public String getStGuarantorOtherLoans()
  {
    return(String)getValue(FIELD_DFGUARANTOROTHERLOANS);
  }

  /**
   *
   *
   */
  public void setStGuarantorOtherLoans(String value)
  {
    setValue(FIELD_DFGUARANTOROTHERLOANS, value);
  }

  //--DJ_CR201.2--end//


  /**
   * MetaData object test method to verify the SQL_TEMPLATE query.
   *
   */
  /**
      public void setResultSet(ResultSet value)
      {
          logger = SysLog.getSysLogger("METADATA");

          try
          {
              super.setResultSet(value);
              if( value != null)
              {
                  ResultSetMetaData metaData = value.getMetaData();
                  int columnCount = metaData.getColumnCount();
                  logger.debug("===============================================");
                  logger.debug("Testing MetaData Object for new synthetic fields for" +
                             " doDealEntryGDSTDSAppDetailsModel");
                  logger.debug("NumberColumns: " + columnCount);
                  for(int i = 0; i < columnCount ; i++)
                  {
                      logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
                  }
                  logger.debug("================================================");
                }
            }
            catch (SQLException e)
            {
                logger.debug("Class: " + getClass().getName());
                logger.debug("SQLException: " + e);
            }
      }
   **/

  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Custom Methods - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //// 1. Replace all Descriptions from the tables included into the PickList with
  //// their joins' counterparts in the SQL_TEMPLATE.
  //// 2. Convert all these new fragments (IDs) into the string format.
  //// 3. Adjust Field names definitions to string format.

  public static final String DATA_SOURCE_NAME = "jdbc/orcl";

  //--Release2.1--//
  //// 1. Replace all Descriptions from the tables included into the PickList with
  //// their joins' counterparts in the SQL_TEMPLATE.
  //// 2. Convert all these new fragments (IDs) into the string format.
  ////public static final String SELECT_SQL_TEMPLATE="SELECT ALL BORROWER.DEALID, BORROWER.BORROWERID, BORROWER.COPYID, BORROWER.BORROWERFIRSTNAME, BORROWER.BORROWERMIDDLEINITIAL, BORROWER.BORROWERLASTNAME, BORROWERTYPE.BTDESCRIPTION, BORROWER.GDS, BORROWER.GDS3YEAR, BORROWER.TDS, BORROWER.TDS3YEAR FROM BORROWER, BORROWERTYPE  __WHERE__  ORDER BY BORROWER.BORROWERID  ASC";

  public static final String SELECT_SQL_TEMPLATE = "SELECT distinct BORROWER.DEALID, BORROWER.BORROWERID, " +
    "BORROWER.COPYID, BORROWER.BORROWERFIRSTNAME, BORROWER.BORROWERMIDDLEINITIAL, " +
    "BORROWER.BORROWERLASTNAME, to_char(BORROWER.BORROWERTYPEID) BORROWERTYPEID_STR, BORROWER.GDS, " +
    "BORROWER.GDS3YEAR, BORROWER.TDS, BORROWER.TDS3YEAR, to_char(BORROWER.SUFFIXID) BORROWERSUFFIXID_STR, " +
    //--DJ_LDI_CR--start--//
    //--> Bug fix to support Bilingual : By Billy 19May2004
    //"BORROWER.INSURANCEPROPORTIONSID, INSURANCEPROPORTIONS.IPDESC, " +
    "to_char(BORROWER.INSURANCEPROPORTIONSID) INSURANCEPROPORTIONSID_STR, " +
    "DEAL.LIFEPREMIUM, DEAL.DISABILITYPREMIUM, DEAL.COMBINEDPREMIUM, " +
    //"BORROWER.LIFESTATUSID, BORROWER.DISABILITYSTATUSID, LIFESTATUS.LIFESTATUSDESC, " +
    //"DISABILITYSTATUS.DISABILITYSTATUSDESC, " +
    "to_char(BORROWER.LIFESTATUSID) LIFESTATUSID_STR, to_char(BORROWER.DISABILITYSTATUSID) DISABILITYSTATUSID_STR, " +
    //--DJ_CR201.2--start//
    "BORROWER.GUARANTOROTHERLOANS " +
    //--DJ_CR201.2--end//
    //"FROM BORROWER, BORROWERTYPE, INSURANCEPROPORTIONS, LIFESTATUS, DISABILITYSTATUS, DEAL " +
    "FROM BORROWER, DEAL " +
    "__WHERE__  ORDER BY BORROWER.BORROWERID  ASC";

  //--DJ_LDI_CR--end--//

  //--DJ_LDI_CR--start--//
  public static final String MODIFYING_QUERY_TABLE_NAME = "BORROWER, DEAL";

  public static final String STATIC_WHERE_CRITERIA =
    //" (INSURANCEPROPORTIONS.INSURANCEPROPORTIONSID = BORROWER.INSURANCEPROPORTIONSID) AND " +
    //"(BORROWER.LIFESTATUSID = LIFESTATUS.LIFESTATUSID) AND " +
    //"(BORROWER.DISABILITYSTATUSID = DISABILITYSTATUS.DISABILITYSTATUSID) AND " +
      " BORROWER.institutionprofileid = DEAL.institutionprofileid AND " +
    "BORROWER.DEALID = DEAL.DEALID AND " +
    "BORROWER.COPYID = DEAL.COPYID ";

  //--DJ_LDI_CR--end--//
  //"(BORROWER.BORROWERTYPEID  =  BORROWERTYPE.BORROWERTYPEID) ";
  //============================== End Fix ==================================

  public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();
  public static final String QUALIFIED_COLUMN_DFDEALID = "BORROWER.DEALID";
  public static final String COLUMN_DFDEALID = "DEALID";
  public static final String QUALIFIED_COLUMN_DFBORROWERID = "BORROWER.BORROWERID";
  public static final String COLUMN_DFBORROWERID = "BORROWERID";
  public static final String QUALIFIED_COLUMN_DFCOPYID = "BORROWER.COPYID";
  public static final String COLUMN_DFCOPYID = "COPYID";
  public static final String QUALIFIED_COLUMN_DFFIRSTNAME = "BORROWER.BORROWERFIRSTNAME";
  public static final String COLUMN_DFFIRSTNAME = "BORROWERFIRSTNAME";
  public static final String QUALIFIED_COLUMN_DFMIDDLEINITIAL = "BORROWER.BORROWERMIDDLEINITIAL";
  public static final String COLUMN_DFMIDDLEINITIAL = "BORROWERMIDDLEINITIAL";
  public static final String QUALIFIED_COLUMN_DFLASTNAME = "BORROWER.BORROWERLASTNAME";
  public static final String COLUMN_DFLASTNAME = "BORROWERLASTNAME";
  public static final String QUALIFIED_COLUMN_DFSUFFIX="BORROWER.BORROWERSUFFIXID_STR";
  public static final String COLUMN_DFSUFFIX="BORROWERSUFFIXID_STR";
  
  //--Release2.1--//
  //// 3. Adjust Field names definitions to string format.
  ////public static final String QUALIFIED_COLUMN_DFBORROWERTYPE="BORROWERTYPE.BTDESCRIPTION";
  ////public static final String COLUMN_DFBORROWERTYPE="BTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFBORROWERTYPE = "BORROWER.BORROWERTYPEID_STR";
  public static final String COLUMN_DFBORROWERTYPE = "BORROWERTYPEID_STR";

  public static final String QUALIFIED_COLUMN_DFBORROWERGDS = "BORROWER.GDS";
  public static final String COLUMN_DFBORROWERGDS = "GDS";
  public static final String QUALIFIED_COLUMN_DFBORROWERGDS3YR = "BORROWER.GDS3YEAR";
  public static final String COLUMN_DFBORROWERGDS3YR = "GDS3YEAR";
  public static final String QUALIFIED_COLUMN_DFBORROWERTDS = "BORROWER.TDS";
  public static final String COLUMN_DFBORROWERTDS = "TDS";
  public static final String QUALIFIED_COLUMN_DFBORROWERTDS3YR = "BORROWER.TDS3YEAR";
  public static final String COLUMN_DFBORROWERTDS3YR = "TDS3YEAR";

  //--DJ_LDI_CR--start--//
  //--> Bug fix to support Bilingual : By Billy 19May2004
  public static final String QUALIFIED_COLUMN_DFINSURANCEPROPORTIONSID = "BORROWER.INSURANCEPROPORTIONSID_STR";
  public static final String COLUMN_DFINSURANCEPROPORTIONSID = "INSURANCEPROPORTIONSID_STR";

  //public static final String QUALIFIED_COLUMN_DFIPDESC = "INSURANCEPROPORTIONS.IPDESC";
  //public static final String COLUMN_DFIPDESC = "IPDESC";

  //public static final String QUALIFIED_COLUMN_DFLIFESTATUSDESC = "LIFESTATUS.LIFESTATUSDESC";
  //public static final String COLUMN_DFLIFESTATUSDESC = "LIFESTATUSDESC";

  //public static final String QUALIFIED_COLUMN_DFDISABILITYSTATUSDESC = "DISABILITYSTATUS.DISABILITYSTATUSDESC";
  //public static final String COLUMN_DFDISABILITYSTATUSDESC = "DISABILITYSTATUSDESC";

  public static final String QUALIFIED_COLUMN_DFLIFESTATUSID = "BORROWER.LIFESTATUSID_STR";
  public static final String COLUMN_DFLIFESTATUSID = "LIFESTATUSID_STR";

  public static final String QUALIFIED_COLUMN_DFDISABILITYSTATUSID = "BORROWER.DISABILITYSTATUSID_STR";
  public static final String COLUMN_DFDISABILITYSTATUSID = "DISABILITYSTATUSID_STR";
  //========================================================

  public static final String QUALIFIED_COLUMN_DFLIFEPREMIUM = "DEAL.LIFEPREMIUM";
  public static final String COLUMN_DFLIFEPREMIUM = "LIFEPREMIUM";

  public static final String QUALIFIED_COLUMN_DFDISABILITYPREMIUM = "DEAL.DISABILITYPREMIUM";
  public static final String COLUMN_DFDISABILITYPREMIUM = "DISABILITYPREMIUM";

  public static final String QUALIFIED_COLUMN_DFCOMBINEDLDPREMIUM = "DEAL.COMBINEDPREMIUM";
  public static final String COLUMN_DFCOMBINEDLDPREMIUM = "COMBINEDPREMIUM";

  //--DJ_LDI_CR--end--//

  //--DJ_CR201.2--start//
  public static final String QUALIFIED_COLUMN_DFGUARANTOROTHERLOANS = "BORROWER.GUARANTOROTHERLOANS";
  public static final String COLUMN_DFGUARANTOROTHERLOANS = "GUARANTOROTHERLOANS";

  //--DJ_CR201.2--end//

  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Custom Members - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////

  static
  {

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFDEALID,
      COLUMN_DFDEALID,
      QUALIFIED_COLUMN_DFDEALID,
      java.math.BigDecimal.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFBORROWERID,
      COLUMN_DFBORROWERID,
      QUALIFIED_COLUMN_DFBORROWERID,
      java.math.BigDecimal.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFCOPYID,
      COLUMN_DFCOPYID,
      QUALIFIED_COLUMN_DFCOPYID,
      java.math.BigDecimal.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFFIRSTNAME,
      COLUMN_DFFIRSTNAME,
      QUALIFIED_COLUMN_DFFIRSTNAME,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFMIDDLEINITIAL,
      COLUMN_DFMIDDLEINITIAL,
      QUALIFIED_COLUMN_DFMIDDLEINITIAL,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFLASTNAME,
      COLUMN_DFLASTNAME,
      QUALIFIED_COLUMN_DFLASTNAME,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));
    
    FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
            FIELD_DFSUFFIX,
            COLUMN_DFSUFFIX,
            QUALIFIED_COLUMN_DFSUFFIX,
            String.class,
            false,
            false,
            QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
            "",
            QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
            ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFBORROWERTYPE,
      COLUMN_DFBORROWERTYPE,
      QUALIFIED_COLUMN_DFBORROWERTYPE,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFBORROWERGDS,
      COLUMN_DFBORROWERGDS,
      QUALIFIED_COLUMN_DFBORROWERGDS,
      java.math.BigDecimal.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFBORROWERGDS3YR,
      COLUMN_DFBORROWERGDS3YR,
      QUALIFIED_COLUMN_DFBORROWERGDS3YR,
      java.math.BigDecimal.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFBORROWERTDS,
      COLUMN_DFBORROWERTDS,
      QUALIFIED_COLUMN_DFBORROWERTDS,
      java.math.BigDecimal.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFBORROWERTDS3YR,
      COLUMN_DFBORROWERTDS3YR,
      QUALIFIED_COLUMN_DFBORROWERTDS3YR,
      java.math.BigDecimal.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    //--DJ_LDI_CR--start--//
    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFINSURANCEPROPORTIONSID,
      COLUMN_DFINSURANCEPROPORTIONSID,
      QUALIFIED_COLUMN_DFINSURANCEPROPORTIONSID,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFLIFESTATUSID,
      COLUMN_DFLIFESTATUSID,
      QUALIFIED_COLUMN_DFLIFESTATUSID,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFDISABILITYSTATUSID,
      COLUMN_DFDISABILITYSTATUSID,
      QUALIFIED_COLUMN_DFDISABILITYSTATUSID,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFLIFEPREMIUM,
      COLUMN_DFLIFEPREMIUM,
      QUALIFIED_COLUMN_DFLIFEPREMIUM,
      java.math.BigDecimal.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFDISABILITYPREMIUM,
      COLUMN_DFDISABILITYPREMIUM,
      QUALIFIED_COLUMN_DFDISABILITYPREMIUM,
      java.math.BigDecimal.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFCOMBINEDLDPREMIUM,
      COLUMN_DFCOMBINEDLDPREMIUM,
      QUALIFIED_COLUMN_DFCOMBINEDLDPREMIUM,
      java.math.BigDecimal.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));
    //--DJ_LDI_CR--end--//

    //--DJ_CR201.2--start//
    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFGUARANTOROTHERLOANS,
      COLUMN_DFGUARANTOROTHERLOANS,
      QUALIFIED_COLUMN_DFGUARANTOROTHERLOANS,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));
    //--DJ_CR201.2--end//
  }

}
