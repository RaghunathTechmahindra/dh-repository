package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.event.TiledViewRequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.ImageField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;
/**
 * <p>Title: </p>
 * <p>Description: Handler Class for Component Details screen with Jato Framework </p>
 * 
 *
 * @author MCM Team
 * @version 1.0 (Initial Version � Jul 2, 2008)
 * @version 1.1 delete Component section - XS_2.32 Modified - handleBtDeleteCCCompRequest()
 * @version 1.3 July 25, 2008 set MAXCOMPTILE_SIZE = 10 for artf750856 
 */



public class pgComponentCreditCardTiledView extends
RequestHandlingTiledViewBase implements TiledView, RequestHandler {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(pgComponentCreditCardTiledView.class);

    /**
     * <p>constructor</p>
     */
    public pgComponentCreditCardTiledView(View parent, String name) {
        super(parent, name);
        // MCM Team for artf750856
        setMaxDisplayTiles(pgComponentDetailsViewBean.MAXCOMPTILE_SIZE);
        setPrimaryModelClass(doComponentCreditCardModel.class);
        registerChildren();
        initialize();
    }

    /**
     * <p>initializer</p>
     */
    protected void initialize() {
    }

    /**
     * <p>createChild</p>
     * <p>Description: createChild method for Jato Framework</p>
     * 
     * @param name:String - child name
     * @return View 
     */
    protected View createChild(String name) {

        if (name.equals(CHILD_HDCOMPONENTID)) {
            HiddenField child 
            = new HiddenField(this,
                    getdoComponentCreditCardModel(),
                    CHILD_HDCOMPONENTID,
                    doComponentCreditCardModel.FIELD_DFCOMPONENTID,
                    CHILD_HDCOMPONENTID_RESET_VALUE,
                    null);
            return child;
        } else if (name.equals(CHILD_HDCOPYID)) {
            HiddenField child 
            = new HiddenField(this,
                    getdoComponentCreditCardModel(),
                    CHILD_HDCOPYID,
                    doComponentCreditCardModel.FIELD_DFCOPYID,
                    CHILD_HDCOPYID_RESET_VALUE,
                    null);
            return child;
            // MCM Team: fixed cosmetic issue, STARTS
        } else if (name.equals(CHILD_STTITLE)) {
            StaticTextField child 
            = new StaticTextField(this,
                    getdoComponentCreditCardModel(),
                    CHILD_STTITLE,
                    CHILD_STTITLE,
                    CHILD_STTITLE,
                    null);
            return child;
        } else if (name.equals(CHILD_IMSECTIONDEVIDER)) {
            ImageField child 
            = new ImageField(this,
                    getdoComponentCreditCardModel(),
                    CHILD_IMSECTIONDEVIDER,
                    CHILD_IMSECTIONDEVIDER,
                    CHILD_IMSECTIONDEVIDER,
                    null);
            return child;
        } else if (name.equals(CHILD_CBCOMPONENTTYPE)) {
            ComboBox child 
            = new ComboBox(this,
                    getdoComponentCreditCardModel(),
                    CHILD_CBCOMPONENTTYPE,
                    doComponentCreditCardModel.FIELD_DFCOMPONENTTYPE,
                    CHILD_CBCOMPONENTTYPE_RESET_VALUE,
                    null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbComponentTypeOptions);
            return child;
        } else if (name.equals(CHILD_CBCCPRODUCT)) {
            ComboBox child 
            = new ComboBox(this,
                    getdoComponentCreditCardModel(),
                    CHILD_CBCCPRODUCT,
                    doComponentCreditCardModel.FIELD_DFMTGPRODID,
                    CHILD_CBCCPRODUCT_RESET_VALUE,
                    null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbCCProductOptions);
            return child;
        } else if (name.equals(CHILD_CBCCINTERESTRATE)) {
            ComboBox child 
            = new ComboBox(this,
                    getdoComponentCreditCardModel(),
                    CHILD_CBCCINTERESTRATE,
                    doComponentCreditCardModel.FIELD_DFPRICINGRATEINVENTORYID,
                    CHILD_CBCCINTERESTRATE_RESET_VALUE,
                    null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbCCPostedInterestRateOptions);
            return child;
        } else if (name.equals(CHILD_TXCREDITCARDAMOUNT)) {
            TextField child 
            = new TextField(this,
                    getdoComponentCreditCardModel(),
                    CHILD_TXCREDITCARDAMOUNT,
                    doComponentCreditCardModel.FIELD_DFCREDITCARDAMOUNT,
                    CHILD_TXCREDITCARDAMOUNT_RESET_VALUE,
                    null);
            return child;
        } else if (name.equals(CHILD_TXADDITIONALINFO)) {
            TextField child 
            = new TextField(this,
                    getdoComponentCreditCardModel(),
                    CHILD_TXADDITIONALINFO,
                    doComponentCreditCardModel.FIELD_DFADDITIONALINFORMATION,
                    CHILD_TXADDITIONALINFO_RESET_VALUE,
                    null);
            return child;
        } else if (name.equals(CHILD_BTRECALCCCCOMP)) {
            Button child = new Button(
                    this, getDefaultModel(), CHILD_BTRECALCCCCOMP,
                    CHILD_BTRECALCCCCOMP, CHILD_BTRECALCCCCOMP_RESET_VALUE,
                    null);
            return child;
        } else if (name.equals(CHILD_BTDELETECCCOMP)) {
            Button child = new Button(
                    this, getDefaultModel(), CHILD_BTDELETECCCOMP,
                    CHILD_BTDELETECCCOMP, CHILD_BTDELETECCCOMP_RESET_VALUE,
                    null);
            return child;
        }
        /***************MCM Impl team changes starts - XS_2.36(dynamic drop down list)*********************/
        else if (name.equals(CHILD_STMTGPRODUCT)) {
            TextField child = new TextField(this, 
                    getDefaultModel(),
                    CHILD_STMTGPRODUCT, 
                    CHILD_STMTGPRODUCT,
                    CHILD_STMTGPRODUCT_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_STINTERESTRATE)) {
            TextField child = new TextField(this, 
                    getDefaultModel(),
                    CHILD_STINTERESTRATE, 
                    CHILD_STINTERESTRATE,
                    CHILD_STINTERESTRATE_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), 
                    CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT,
                    CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT,
                    CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_TXPOSTEDRATE)) {
            TextField child =
                new TextField(this, getdoComponentCreditCardModel(),
                        CHILD_TXPOSTEDRATE,
                        doComponentCreditCardModel.FIELD_DFPOSTEDRATE,
                        CHILD_TXPOSTEDRATE_RESET_VALUE, null);
            return child;
        }
        /***************MCM Impl team changes ends - XS_2.36(dynamic drop down list)*********************/

        else throw new IllegalArgumentException("Invalid child name [" + name + "]");
    }

    /**
     * <p>
     * registerChildren
     * </p>
     * <p>
     * Description: registerChildren method for Jato Framework
     * </p>
     * 
     * @param
     * @return
     */
    protected void registerChildren() {

        registerChild(CHILD_HDCOMPONENTID, HiddenField.class);
        registerChild(CHILD_HDCOPYID, HiddenField.class);
        registerChild(CHILD_STTITLE, StaticTextField.class);
        registerChild(CHILD_IMSECTIONDEVIDER, ImageField.class);
        // MCM Team: fixed cosmetic issue, ENDS
        registerChild(CHILD_CBCOMPONENTTYPE, ComboBox.class);
        registerChild(CHILD_TXCREDITCARDAMOUNT, TextField.class);
        registerChild(CHILD_TXADDITIONALINFO, TextField.class);
        registerChild(CHILD_CBCCPRODUCT, ComboBox.class);
        registerChild(CHILD_CBCCINTERESTRATE, ComboBox.class);
        registerChild(CHILD_BTRECALCCCCOMP, Button.class);
        registerChild(CHILD_BTDELETECCCOMP, Button.class);
        /***************MCM Impl team changes starts - XS_2.36(dynamic drop down list)*********************/
        registerChild(CHILD_STINTERESTRATE, TextField.class);
        registerChild(CHILD_STMTGPRODUCT, TextField.class);
        registerChild(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT, StaticTextField.class);
        registerChild(CHILD_TXPOSTEDRATE, TextField.class);
        /***************MCM Impl team changes ends - XS_2.36(dynamic drop down list)*********************/

    }

    /**
     * <p>resetChildren</p>
     * <p>Description: resetChildren method for Jato Framework</p>
     * 
     * @param 
     * @return
     */
    public void resetChildren() {

        super.resetChildren();

        getHdComponentId().setValue(CHILD_HDCOMPONENTID_RESET_VALUE);
        getHdCopyId().setValue(CHILD_HDCOMPONENTID_RESET_VALUE);
        // MCM Team: fixed cosmetic issue, STARTS
        getStTitle().setValue(CHILD_STTITLE_RESET_VALUE);
        getImSectionDevider().setValue(CHILD_IMSECTIONDEVIDER_RESET_VALUE);
        // MCM Team: fixed cosmetic issue, ENDS
        getCbComponentType().setValue(CHILD_CBCOMPONENTTYPE_RESET_VALUE);
        getCbProduct().setValue(CHILD_CBCCPRODUCT_RESET_VALUE);
        getTxCreditCardAmount().setValue(CHILD_TXCREDITCARDAMOUNT_RESET_VALUE);
        getTxAdditionalInfo().setValue(CHILD_TXADDITIONALINFO_RESET_VALUE);
        getBtRecalcCCComp().setValue(CHILD_BTRECALCCCCOMP_RESET_VALUE);
        getBtDeleteCCComp().setValue(CHILD_BTDELETECCCOMP_RESET_VALUE);
        /***************MCM Impl team changes starts - XS_2.36(dynamic drop down list)*********************/
        getStPostedInterestRate().setValue(CHILD_STINTERESTRATE_RESET_VALUE);
        getStMtgProduct().setValue(CHILD_STMTGPRODUCT_RESET_VALUE);
        getStInitDyanmicListJavaScript().setValue(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT_RESET_VALUE);
        getTxPostedRate().setValue(CHILD_TXPOSTEDRATE_RESET_VALUE);
        /***************MCM Impl team changes ends - XS_2.36(dynamic drop down list)*********************/
    }


    /**
     * <p>nextTile</p>
     * <p>Description: display next creditCard section tiled view </p>
     */
    public boolean nextTile() throws ModelControlException {

        boolean movedToRow = super.nextTile();
        if (movedToRow) {
            ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler
            .cloneSS();
            handler.pageGetState(this.getParentViewBean());
            handler.setCreditCardCompDisplayFields(getTileIndex());
            handler.pageSaveState();
        }
        return movedToRow;
    }

    // geters starts    
    public HiddenField getHdComponentId() {
        return (HiddenField) getChild(CHILD_HDCOMPONENTID);
    }

    public HiddenField getHdCopyId() {
        return (HiddenField) getChild(CHILD_HDCOPYID);
    }

    // MCM Team: fixed cosmetic issue, STARTS
    public StaticTextField getStTitle() {
        return (StaticTextField) getChild(CHILD_STTITLE);
    }

    public ImageField getImSectionDevider() {
        return (ImageField) getChild(CHILD_IMSECTIONDEVIDER);
    }
    // MCM Team: fixed cosmetic issue, ENDS

    public ComboBox getCbComponentType() {
        return (ComboBox) getChild(CHILD_CBCOMPONENTTYPE);
    }

    public TextField getTxCreditCardAmount() {
        return (TextField) getChild(CHILD_TXCREDITCARDAMOUNT);
    }

    public TextField getTxAdditionalInfo() {
        return (TextField) getChild(CHILD_TXADDITIONALINFO);
    }

    public ComboBox getCbProduct() {
        return (ComboBox) getChild(CHILD_CBCCPRODUCT);
    }

    public ComboBox getCbInterestRate() {
        return (ComboBox) getChild(CHILD_CBCCINTERESTRATE);
    }

    public Button getBtRecalcCCComp() {
        return (Button) getChild(CHILD_BTRECALCCCCOMP);
    }

    public Button getBtDeleteCCComp() {
        return (Button) getChild(CHILD_BTDELETECCCOMP);
    }

    /**
     * <p>handleBtRecalcCCCompRequest</p>
     * <p>Description: Handle clicking Recalclate</p>
     * @param event: RequestInvocationEvent
     * @return 
     * @author MCM Team
     * -- XS 2.36
     */
    public void handleBtRecalcCCCompRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _log.trace("Web event invoked: "+getClass().getName()+".BtRecalcCCComp");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this.getParentViewBean());
        handler.handleRecalc();
        handler.postHandlerProtocol();    
        return;
    }

    /**
     * <p>handleBtDeleteCCCompRequest</p>
     * <p>Description: Handle clicking Recalclate</p>
     * @param event: RequestInvocationEvent
     * @return 
     * @version 1.0 -- XS 2.36
     * @version 1.1 XS 2.32 delete component
     * @author MCM Team
     */
    public void handleBtDeleteCCCompRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _log.trace("Web event invoked: "+getClass().getName()+".BtDeleteCCComp");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this.getParentViewBean());
        int tileIndx = ((TiledViewRequestInvocationEvent)(event)).getTileNumber();
        handler.handleDeleteCCComp(tileIndx);
        handler.postHandlerProtocol();    
        return;
    }

    /***************MCM Impl team changes starts - XS_2.36(dynamic drop down list)*********************/
    public TextField getStPostedInterestRate() {
        return (TextField) getChild(CHILD_STINTERESTRATE);
    }

    public TextField getStMtgProduct() {
        return (TextField) getChild(CHILD_STMTGPRODUCT);
    }

    public StaticTextField getStInitDyanmicListJavaScript() {
        return (StaticTextField) getChild(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT);
    }

    public TextField getTxPostedRate() {
        return (TextField) getChild(CHILD_TXPOSTEDRATE);
    }
    /***************MCM Impl team changes ends - XS_2.36(dynamic drop down list)*********************/

    /**
     * <p>getWebActionModels</p>
     * <p>Description: get Auto executed models</p>
     *
     * @param int: executionType
     * @return Model[]
     */
    public Model[] getWebActionModels(int executionType) {
        List modelList = new ArrayList();
        switch (executionType) {
        case MODEL_TYPE_RETRIEVE:
            ;
            break;

        case MODEL_TYPE_UPDATE:
            ;
            break;

        case MODEL_TYPE_DELETE:
            ;
            break;

        case MODEL_TYPE_INSERT:
            ;
            break;

        case MODEL_TYPE_EXECUTE:
            ;
            break;
        }
        return (Model[]) modelList.toArray(new Model[0]);
    }

    /**
     * <p>beginDisplay</p>
     * <p>Description: beginDisplay method for Jato Framework</p>
     * 
     * @param event: DisplayEvent
     * @return
     */
    public void beginDisplay(DisplayEvent event) throws ModelControlException {

        ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler.cloneSS();
        handler.pageGetState(this.getParentViewBean());

        if (getPrimaryModel() == null) {
            throw new ModelControlException("Primary model is null");
        }

        cbComponentTypeOptions.populate(getRequestContext());
        super.beginDisplay(event);
        resetTileIndex();
    }

    /**
     * <p>beforeModelExecutes</p>
     * <p>Description: beforeModelExecutes method for Jato Framework</p>
     * 
     * @param model: Model
     * @param executionContext: int
     * @return boolean
     */
    public boolean beforeModelExecutes(Model model, int executionContext) {

        return super.beforeModelExecutes(model, executionContext);
    }

    /**
     * <p>afterModelExecutes</p>
     * <p>Description: beforeModelExecutes method for Jato Framework</p>
     * 
     * @param model: Model
     * @param executionContext: int
     * @return
     */
    public void afterModelExecutes(Model model, int executionContext) {

        // This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
        super.afterModelExecutes(model, executionContext);

    }

    /**
     * <p>afterModelExecutes</p>
     * <p>Description: afterModelExecutes method for Jato Framework</p>
     * 
     * @param executionContext: int
     * @return
     */
    public void afterAllModelsExecute(int executionContext) {
        super.afterAllModelsExecute(executionContext);

    }

    /**
     * <p>onModelError</p>     * 
     * <p>Description: onModelError method for Jato Framework</p>
     * 
     * @param model: Model
     * @param executionContext: int
     * @param exception: ModelControlException
     * @return
     */
    public void onModelError(Model model, int executionContext,
            ModelControlException exception) throws
            ModelControlException {

        super.onModelError(model, executionContext, exception);
    }

    public void setdoComponentCreditCardModel( doComponentCreditCardModel model) {
        doComponentCreditCardModel = model;
    }

    public doComponentCreditCardModel getdoComponentCreditCardModel() {
        if (doComponentCreditCardModel == null) {
            doComponentCreditCardModel = (doComponentCreditCardModel) getModel(
                    doComponentCreditCardModel.class);
        }
        return doComponentCreditCardModel;
    }


    private OptionList cbComponentTypeOptions = new CbComponentTypeOptionList();    

    /**
     * <p>CbComponentTypeOptionList</p>
     * <p>inner Class for ComponentType comboBox</p>
     * 
     * @author MCM Implimentation Team
     *
     */
    class CbComponentTypeOptionList 
    extends pgUWorksheetViewBean.BaseComboBoxOptionList {

        CbComponentTypeOptionList() {
        }

        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "COMPONENTTYPE",
                    cbComponentTypeOptions);
        }
    }

    private OptionList cbCCProductOptions = new OptionList(
            new String[] {}, new String[] {});                              

    private OptionList cbCCPostedInterestRateOptions = new OptionList(
            new String[] {}, new String[] {});

    // //////////////////////////////////////////////////////////////////////////////
    // Class variables
    // //////////////////////////////////////////////////////////////////////////////

    public static Map FIELD_DESCRIPTORS;
    public static final String CHILD_TBDEALID = "tbDealId";
    public static final String CHILD_HDCOMPONENTID = "hdComponentIdCC";
    public static final String CHILD_HDCOMPONENTID_RESET_VALUE = "";
    public static final String CHILD_HDCOPYID = "hdCopyIdCC";
    public static final String CHILD_HDCOPYID_RESET_VALUE = "";
    public static final String CHILD_STTITLE = "stTitle";
    public static final String CHILD_STTITLE_RESET_VALUE = "";    
    public static final String CHILD_IMSECTIONDEVIDER = "imSectionDevider";
    public static final String CHILD_IMSECTIONDEVIDER_RESET_VALUE = "";
    public static final String CHILD_CBCOMPONENTTYPE = "cbComponentType";
    public static final String CHILD_CBCOMPONENTTYPE_RESET_VALUE = "";
    public static final String CHILD_TXCREDITCARDAMOUNT = "txCreditCardAmount";
    public static final String CHILD_TXCREDITCARDAMOUNT_RESET_VALUE = "";
    public static final String CHILD_TXADDITIONALINFO = "txAdditionalInfo";
    public static final String CHILD_TXADDITIONALINFO_RESET_VALUE = "";
    public static final String CHILD_CBCCPRODUCT = "cbCreditCardProduct";
    public static final String CHILD_CBCCPRODUCT_RESET_VALUE = "";
    public static final String CHILD_CBCCINTERESTRATE = "cbCreditCardInterestRate";
    public static final String CHILD_CBCCINTERESTRATE_RESET_VALUE = "";
    public static final String CHILD_BTRECALCCCCOMP = "btRecalcCCComp";
    public static final String CHILD_BTRECALCCCCOMP_RESET_VALUE = "";
    public static final String CHILD_BTDELETECCCOMP = "btDeleteCCComp";
    public static final String CHILD_BTDELETECCCOMP_RESET_VALUE = "";

    /***************MCM Impl team changes starts - XS_2.36(dynamic drop down list)*******************/

    public static final String CHILD_STMTGPRODUCT = "stMtgProduct";
    public static final String CHILD_STMTGPRODUCT_RESET_VALUE = "";

    public static final String CHILD_STINTERESTRATE = "stInterestRate";
    public static final String CHILD_STINTERESTRATE_RESET_VALUE = "";

    public static final String CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT = "stInitJavaScript"; 
    public static final String CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT_RESET_VALUE = "";

    public static final String CHILD_TXPOSTEDRATE = "txPostedRate";
    public static final String CHILD_TXPOSTEDRATE_RESET_VALUE = "";

    /***************MCM Impl team changes ends - XS_2.36(dynamic drop down list)*********************/

    // //////////////////////////////////////////////////////////////////////////
    // Instance variables
    // //////////////////////////////////////////////////////////////////////////

    private doComponentCreditCardModel doComponentCreditCardModel = null;
    private ComponentDetailsHandler handler = new ComponentDetailsHandler();


}
