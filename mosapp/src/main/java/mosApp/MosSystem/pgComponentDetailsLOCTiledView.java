package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.event.TiledViewRequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.CheckBox;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.ImageField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

/**
 * Description: pgComponentDetailsLOCTiledView class is used to display data 
 * from the doComponentLocModel Model.
 * 
 * @author: MCM Impl Team.
 * @version 1.0 2008-6-17
 * @version 1.1 Jun 30, 208 : XS_2.34, adding Dynamic combobox
 * @version 1.2 July 11, 2009: artf741023: Added line separator and section title.
 * @version 1.3 delete Component section - XS_2.32 Modified - handleBtDeleteLOCCompRequest()
 * @version 1.4 July 25, 2008 set MAXCOMPTILE_SIZE = 10 for artf750856 
 */
public class pgComponentDetailsLOCTiledView extends
RequestHandlingTiledViewBase implements TiledView, RequestHandler {

    static final Log _log = LogFactory
    .getLog(pgComponentDetailsLOCTiledView.class);


    /**
     * Description: This Constructor registers the Childs and sets the default
     * URL.
     * 
     */
    public pgComponentDetailsLOCTiledView(View parent, String name) {
        super(parent, name);
        // MCM team for artf750856
        setMaxDisplayTiles(pgComponentDetailsViewBean.MAXCOMPTILE_SIZE);
        setPrimaryModelClass(doComponentLocModel.class);
        registerChildren();
        initialize();
    }

    /**
     * Description: This method currently has no implementation but can be used
     * to do initializations
     * 
     */
    protected void initialize() {
    }

    /**
     * Description: This method registers the children to the framework.
     * 
     */
    public void resetChildren() {
        getCbComponentType().setValue(CHILD_CBCOMPONENTTYPE_RESET_VALUE);
        getCbProductType().setValue(CHILD_CBPRODUCTTYPE_RESET_VALUE);
        getTbLOCAmount().setValue(CHILD_TBLOCAMOUNT_RESET_VALUE);
        getStMIPremium().setValue(CHILD_STMIPREMIUM_RESET_VALUE);
        getTbDiscount().setValue(CHILD_TBDISCOUNT_RESET_VALUE);
        getStTotalLOCAmount().setValue(CHILD_STTOTALLOCAMOUNT_RESET_VALUE);
        getTbPremium().setValue(CHILD_TBPREMIUM_RESET_VALUE);
        getCbPaymentFrequency().setValue(CHILD_CBPAYMENTFREQUENCY_RESET_VALUE);
        getStNetRate().setValue(CHILD_STNETRATE_RESET_VALUE);
        getTbCommissionCode().setValue(CHILD_TBCOMMISSIONCODE_RESET_VALUE);
        getStInterestOnlyPayment().setValue(
                CHILD_STINTERESTONLYPAYMENT_RESET_VALUE);
        getCbRepaymentType().setValue(CHILD_CBREPAYMENTTYPE_RESET_VALUE);
        getCbRepaymentType().setValue(CHILD_CBREPAYMENTTYPE_RESET_VALUE);
        getStTaxEscrow().setValue(CHILD_STTAXESCROW_RESET_VALUE);
        getCbExistingAccount().setValue(CHILD_CBEXISTINGACCOUNT_RESET_VALUE);
        getStTotalPayment().setValue(CHILD_STTOTALPAYMENT_RESET_VALUE);
        getTbExistingAccountRef().setValue(
                CHILD_TBEXISTINGACCOUNTREF_RESET_VALUE);
        getTbHoldBackAmount().setValue(CHILD_TBHOLDBACKAMOUNT_RESET_VALUE);
        getCbFirstPaymentMonth()
        .setValue(CHILD_CBFIRSTPAYMENTMONTH_RESET_VALUE);
        getTxFirstPaymentDay().setValue(CHILD_TXFIRSTPAYMENTDAY_RESET_VALUE);
        getTxFirstPaymentYear().setValue(CHILD_TXFIRSTPAYMENTYEAR_RESET_VALUE);
        getTbRateGuaranteePeriod().setValue(
                CHILD_TBRATEGUARANTEEPERIOD_RESET_VALUE);
        getTbAdditonalInfo().setValue(CHILD_TBADDITIONALINFO_RESET_VALUE);
        getChAllocateMIPremium().setValue(CHILD_CHALLOCATEMIPREMIUM_RESET_VALUE);
        getChAllocateTaxEscrow().setValue(CHILD_CHALLOCATETAXESCROW_RESET_VALUE);
        getBtRecalcLOCComp().setValue(CHILD_BTRECALCLOCCOMP_RESET_VALUE);
        getBtDeleteLOCComp().setValue(CHILD_BTDELETELOCCOMP_RESET_VALUE);
        getComponentId().setValue(CHILD_HDCOMPONENTID_RESET_VALUE);
        getCopyId().setValue(CHILD_HDCOPYID_RESET_VALUE);
        getCbPostedInterestRate().setValue(CHILD_CBPOSTEDINTERESTRATE_RESET_VALUE);
        /***************MCM Impl team changes starts - XS_2.34*******************/
        getStPostedInterestRate().setValue(CHILD_STPOSTEDINTERESTRATE_RESET_VALUE);
        getStMtgProduct().setValue(CHILD_STPRODUCT_RESET_VALUE);
        getStRepaymentType().setValue(CHILD_STREPAYMENTTYPE_RESET_VALUE);
        getStInitDyanmicListJavaScript().setValue(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT_RESET_VALUE);
        getTxPostedRate().setValue(CHILD_TXPOSTEDRATE_RESET_VALUE);
        /***************MCM Impl team changes ends - XS_2.34*********************/
        getStTitle().setValue(CHILD_STTITLE_RESET_VALUE);
        getImSectionDevider().setValue(CHILD_IMSECTIONDEVIDER_RESET_VALUE);

    }

    /**
     * Description: This method registers the children to the framework.
     * 
     */
    protected void registerChildren() {

        registerChild(CHILD_CBCOMPONENTTYPE, ComboBox.class);
        registerChild(CHILD_CBPRODUCTTYPE, ComboBox.class);
        registerChild(CHILD_TBLOCAMOUNT, TextField.class);
        registerChild(CHILD_STMIPREMIUM, StaticTextField.class);
        registerChild(CHILD_TBDISCOUNT, TextField.class);
        registerChild(CHILD_STTOTALLOCAMOUNT, StaticTextField.class);
        registerChild(CHILD_TBPREMIUM, TextField.class);
        registerChild(CHILD_CBPAYMENTFREQUENCY, ComboBox.class);
        registerChild(CHILD_STNETRATE, StaticTextField.class);
        registerChild(CHILD_TBCOMMISSIONCODE, TextField.class);
        registerChild(CHILD_STINTERESTONLYPAYMENT, StaticTextField.class);
        registerChild(CHILD_CBREPAYMENTTYPE, ComboBox.class);
        registerChild(CHILD_STTAXESCROW, StaticTextField.class);
        registerChild(CHILD_CBEXISTINGACCOUNT, ComboBox.class);
        registerChild(CHILD_STTOTALPAYMENT, StaticTextField.class);
        registerChild(CHILD_TBEXISTINGACCOUNTREF, TextField.class);
        registerChild(CHILD_TBHOLDBACKAMOUNT, TextField.class);
        registerChild(CHILD_CBFIRSTPAYMENTMONTH, ComboBox.class);
        registerChild(CHILD_TXFIRSTPAYMENTDAY, TextField.class);
        registerChild(CHILD_TXFIRSTPAYMENTYEAR, TextField.class);
        registerChild(CHILD_TBRATEGUARANTEEPERIOD, TextField.class);
        registerChild(CHILD_TBADDITIONALINFO, TextField.class);
        registerChild(CHILD_CHALLOCATEMIPREMIUM, CheckBox.class);
        registerChild(CHILD_CHALLOCATETAXESCROW, CheckBox.class);
        registerChild(CHILD_BTRECALCLOCCOMP, Button.class);
        registerChild(CHILD_BTDELETELOCCOMP, Button.class);
        registerChild(CHILD_HDCOMPONENTID,HiddenField.class);
        registerChild(CHILD_HDCOPYID,HiddenField.class);
        registerChild(CHILD_CBPOSTEDINTERESTRATE, ComboBox.class);
        /***************MCM Impl team changes starts - XS_2.34*******************/
        registerChild(CHILD_STPOSTEDINTERESTRATE, TextField.class);
        registerChild(CHILD_STPRODUCT, TextField.class);
        registerChild(CHILD_STREPAYMENTTYPE, TextField.class);
        registerChild(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT, StaticTextField.class);
        registerChild(CHILD_TXPOSTEDRATE, TextField.class);
        /***************MCM Impl team changes ends - XS_2.34*********************/
        registerChild(CHILD_STTITLE, StaticTextField.class);
        registerChild(CHILD_IMSECTIONDEVIDER, ImageField.class);
    }


    /**
     * Description: This method adds the model to the Model List.
     * 
     * @param executionType
     */
    public Model[] getWebActionModels(int executionType) {
        List modelList = new ArrayList();
        switch (executionType) {
        case MODEL_TYPE_RETRIEVE:
            modelList.add(getdoComponentLocModel());
            ;
            break;

        case MODEL_TYPE_UPDATE:
            ;
            break;

        case MODEL_TYPE_DELETE:
            ;
            break;

        case MODEL_TYPE_INSERT:
            ;
            break;

        case MODEL_TYPE_EXECUTE:
            ;
            break;
        }
        return (Model[]) modelList.toArray(new Model[0]);
    }


    /**
     * Description: This method creates the handler objects and saves the page
     * state and resets the Tile index
     * 
     * @param event
     */
    public void beginDisplay(DisplayEvent event) throws ModelControlException {

        // This is the analog of NetDynamics repeated_onBeforeDisplayEvent
        // Ensure the primary model is non-null; if null, it would cause havoc
        // with the various methods dependent on the primary model

        ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler
        .cloneSS();

        handler.pageGetState(this.getParentViewBean());

        if (getPrimaryModel() == null)
            throw new ModelControlException("Primary model is null");
        // Set up all ComboBox options
        String yesStr = BXResources.getGenericMsg("YES_LABEL", handler.getTheSessionState().getLanguageId());
        String noStr = BXResources.getGenericMsg("NO_LABEL", handler.getTheSessionState().getLanguageId());
        cbComponentTypeOptions.populate(getRequestContext());
        cbProductTypeOptions.populate(getRequestContext());
        cbPaymentFrequencyOptions.populate(getRequestContext());
        cbRePaymentTypeOptions.populate(getRequestContext());
        cbExistingAccountOptions.setOptions(new String[]{noStr, yesStr}, new String[]{"N", "Y"});
        super.beginDisplay(event);
        resetTileIndex();
    }

    /**
     * 
     * 
     */

    public boolean nextTile() throws ModelControlException {

        boolean movedToRow = super.nextTile();
        if (movedToRow) {
            ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler
            .cloneSS();
            handler.pageGetState(this.getParentViewBean());
            handler.setComponentLOCDisplayFields(getTileIndex());
            handler.pageSaveState();
        }
        return movedToRow;
    }

    /**
     * Description: This method does necessary preparation to the page before
     * model executes.
     * 
     * @param model
     * @param executionContext
     */
    public boolean beforeModelExecutes(Model model, int executionContext) {

        // This is the analog of NetDynamics
        // repeated_onBeforeDataObjectExecuteEvent
        return super.beforeModelExecutes(model, executionContext);

    }

    /**
     * Description: This method call the afterModelExecutes method of the super
     * class.
     * 
     * @param model 
     * @param executionContext 
     */
    public void afterModelExecutes(Model model, int executionContext) {

        // This is the analog of NetDynamics
        // repeated_onAfterDataObjectExecuteEvent
        super.afterModelExecutes(model, executionContext);

    }

    /**
     * Description: This method displays the fields on the page after executing
     * the model.
     * 
     * @param executionContext
     */
    public void afterAllModelsExecute(int executionContext) {

        // This is the analog of NetDynamics
        // repeated_onAfterAllDataObjectsExecuteEvent
        super.afterAllModelsExecute(executionContext);

    }

    /**
     * Description: This method calls the onModelError method of the super
     * class.
     * 
     * @param model
     * @param executionContext
     * @param exception
     */
    public void onModelError(Model model, int executionContext,
            ModelControlException exception) throws ModelControlException {

        // This is the analog of NetDynamics repeated_onDataObjectErrorEvent
        super.onModelError(model, executionContext, exception);

    }

    /**
     * Description: This method creates the Children.
     * 
     * @param name
     */
    protected View createChild(String name) {

        if (name.equals(CHILD_CBCOMPONENTTYPE)) {
            ComboBox child = new ComboBox(this, getdoComponentLocModel(),
                    CHILD_CBCOMPONENTTYPE,
                    doComponentLocModel.FIELD_DFCOMPONENTTYPEID,
                    CHILD_CBCOMPONENTTYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbComponentTypeOptions);
            return child;
        } else if (name.equals(CHILD_CBPRODUCTTYPE)) {
            ComboBox child = new ComboBox(this, getdoComponentLocModel(),
                    CHILD_CBPRODUCTTYPE,
                    doComponentLocModel.FIELD_DFPRODUCTID,
                    CHILD_CBPRODUCTTYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbProductTypeOptions);
            return child;
        } else if (name.equals(CHILD_TBLOCAMOUNT)) {
            TextField child = new TextField(this, getdoComponentLocModel(),
                    CHILD_TBLOCAMOUNT,
                    doComponentLocModel.FIELD_DFLOCAMOUNT,
                    CHILD_TBLOCAMOUNT_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_STMIPREMIUM)) {
            StaticTextField child = new StaticTextField(this,
                    getdoComponentLocModel(), CHILD_STMIPREMIUM,
                    doComponentLocModel.FIELD_DFMIPREMIUMAMOUNT,
                    CHILD_STMIPREMIUM_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_TBDISCOUNT)) {
            TextField child = new TextField(this, getdoComponentLocModel(),
                    CHILD_TBDISCOUNT, doComponentLocModel.FIELD_DFDISCOUNT,
                    CHILD_TBDISCOUNT_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_STTOTALLOCAMOUNT)) {
            StaticTextField child = new StaticTextField(this,
                    getdoComponentLocModel(), CHILD_STTOTALLOCAMOUNT,
                    doComponentLocModel.FIELD_DFTOTALLOCAMOUNT,
                    CHILD_STTOTALLOCAMOUNT_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_TBPREMIUM)) {
            TextField child = new TextField(this, getdoComponentLocModel(),
                    CHILD_TBPREMIUM, doComponentLocModel.FIELD_DFPREMIUM,
                    CHILD_TBPREMIUM_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_CBPAYMENTFREQUENCY)) {
            ComboBox child = new ComboBox(this, getdoComponentLocModel(),
                    CHILD_CBPAYMENTFREQUENCY,
                    doComponentLocModel.FIELD_DFPAYMENTFREQUENCYID,
                    CHILD_CBPAYMENTFREQUENCY_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbPaymentFrequencyOptions);
            return child;
        } else if (name.equals(CHILD_STNETRATE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoComponentLocModel(), CHILD_STNETRATE,
                    doComponentLocModel.FIELD_DFNETINTRESTRATE,
                    CHILD_STNETRATE_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_TBCOMMISSIONCODE)) {
            TextField child = new TextField(this, getdoComponentLocModel(),
                    CHILD_TBCOMMISSIONCODE,
                    doComponentLocModel.FIELD_DFCOMMISSIONCODE,
                    CHILD_TBCOMMISSIONCODE_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_STINTERESTONLYPAYMENT)) {
            StaticTextField child = new StaticTextField(this,
                    getdoComponentLocModel(), CHILD_STINTERESTONLYPAYMENT,
                    doComponentLocModel.FIELD_DFPANDIPAYMENTAMOUNT,
                    CHILD_STINTERESTONLYPAYMENT_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_CBREPAYMENTTYPE)) {
            ComboBox child = new ComboBox(this, getdoComponentLocModel(),
                    CHILD_CBREPAYMENTTYPE,
                    doComponentLocModel.FIELD_DFREPAYMENTTYPEID,
                    CHILD_CBREPAYMENTTYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbRePaymentTypeOptions);
            return child;
        } else if (name.equals(CHILD_STTAXESCROW)) {
            StaticTextField child = new StaticTextField(this,
                    getdoComponentLocModel(), CHILD_STTAXESCROW,
                    doComponentLocModel.FIELD_DFPROPERTYTAXESCROWAMOUNT,
                    CHILD_STTAXESCROW_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_CBEXISTINGACCOUNT)) {
            ComboBox child = new ComboBox(this, getdoComponentLocModel(),
                    CHILD_CBEXISTINGACCOUNT, doComponentLocModel.FIELD_DFEXISTINGACCOUNTINDICATOR,
                    CHILD_CBEXISTINGACCOUNT_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbExistingAccountOptions);
            return child;
        } else if (name.equals(CHILD_STTOTALPAYMENT)) {
            StaticTextField child = new StaticTextField(this,
                    getdoComponentLocModel(), CHILD_STTOTALPAYMENT,
                    doComponentLocModel.FIELD_DFTOTALPAYMENTAMOUNT,
                    CHILD_STTOTALPAYMENT_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_TBEXISTINGACCOUNTREF)) {
            TextField child = new TextField(this, getdoComponentLocModel(),
                    CHILD_TBEXISTINGACCOUNTREF,
                    doComponentLocModel.FIELD_DFEXISTINGACCOUNTNUMBER,
                    CHILD_TBEXISTINGACCOUNTREF_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_TBHOLDBACKAMOUNT)) {
            TextField child = new TextField(this, getdoComponentLocModel(),
                    CHILD_TBHOLDBACKAMOUNT,
                    doComponentLocModel.FIELD_DFADVANCEHOLD,
                    CHILD_TBHOLDBACKAMOUNT_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_CBFIRSTPAYMENTMONTH)) {
            ComboBox child = new ComboBox(this, getdoComponentLocModel(),
                    CHILD_CBFIRSTPAYMENTMONTH, CHILD_CBFIRSTPAYMENTMONTH,
                    CHILD_CBFIRSTPAYMENTMONTH_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbFirstPaymentMonthOptions);
            return child;
        } else if (name.equals(CHILD_TXFIRSTPAYMENTDAY)) {
            TextField child = new TextField(this, getdoComponentLocModel(),
                    CHILD_TXFIRSTPAYMENTDAY, CHILD_TXFIRSTPAYMENTDAY,
                    CHILD_TXFIRSTPAYMENTDAY_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_TXFIRSTPAYMENTYEAR)) {
            TextField child = new TextField(this, getdoComponentLocModel(),
                    CHILD_TXFIRSTPAYMENTYEAR, CHILD_TXFIRSTPAYMENTYEAR,
                    CHILD_TXFIRSTPAYMENTYEAR_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_TBRATEGUARANTEEPERIOD)) {
            TextField child = new TextField(this, getdoComponentLocModel(),
                    CHILD_TBRATEGUARANTEEPERIOD, CHILD_TBRATEGUARANTEEPERIOD,
                    CHILD_TBRATEGUARANTEEPERIOD_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_TBADDITIONALINFO)) {
            TextField child = new TextField(this, getdoComponentLocModel(),
                    CHILD_TBADDITIONALINFO, doComponentLocModel.FIELD_DFADDITIONALINFORMATION,
                    CHILD_TBADDITIONALINFO_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_CHALLOCATEMIPREMIUM)) {
            CheckBox child = new CheckBox(this, getdoComponentLocModel(),
                    CHILD_CHALLOCATEMIPREMIUM, doComponentLocModel.FIELD_DFMIFLAG, "Y",
                    "N", false, null);
            return child;
        } else if (name.equals(CHILD_CHALLOCATETAXESCROW)) {
            CheckBox child = new CheckBox(this, getdoComponentLocModel(),
                    CHILD_CHALLOCATETAXESCROW, doComponentLocModel.FIELD_DFPROPERTYTAXFLAG, "Y",
                    "N", false, null);
            return child;
        } else if (name.equals(CHILD_BTRECALCLOCCOMP)) {
            Button child = new Button(
                    this, getDefaultModel(), CHILD_BTRECALCLOCCOMP,
                    CHILD_BTRECALCLOCCOMP, CHILD_BTRECALCLOCCOMP_RESET_VALUE,
                    null);
            return child;
        } else if (name.equals(CHILD_BTDELETELOCCOMP)) {
            Button child = new Button(
                    this, getDefaultModel(), CHILD_BTDELETELOCCOMP,
                    CHILD_BTDELETELOCCOMP, CHILD_BTDELETELOCCOMP_RESET_VALUE,
                    null);
            return child;
        }
        if (name.equals(CHILD_HDCOMPONENTID))
        {
            HiddenField child = new HiddenField(this,
                    getdoComponentLocModel(),
                    CHILD_HDCOMPONENTID,
                    doComponentLocModel.FIELD_DFCOMPONENTID,
                    CHILD_HDCOMPONENTID_RESET_VALUE,
                    null);
            return child;
        }
        else
            if (name.equals(CHILD_HDCOPYID))
            {
                HiddenField child = new HiddenField(this,
                        getdoComponentLocModel(),
                        CHILD_HDCOPYID,
                        doComponentLocModel.FIELD_DFCOPYID,
                        CHILD_HDCOPYID_RESET_VALUE,
                        null);
                return child;

            } else if (name.equals(CHILD_CBPOSTEDINTERESTRATE)) {
                ComboBox child = new ComboBox(this, 
                        getdoComponentLocModel(),
                        CHILD_CBPOSTEDINTERESTRATE, 
                        doComponentLocModel.FIELD_DFPRICINGRATEINVENTORYID,
                        CHILD_CBPOSTEDINTERESTRATE_RESET_VALUE, null);
                return child;
            }
        /***************MCM Impl team changes starts - XS_2.34*******************/

            else if (name.equals(CHILD_STPRODUCT)) {
                TextField child = new TextField(this, getDefaultModel(),
                        CHILD_STPRODUCT, CHILD_STPRODUCT,
                        CHILD_STPRODUCT_RESET_VALUE, null);
                return child;
            } else if (name.equals(CHILD_STREPAYMENTTYPE)) {
                TextField child = new TextField(this, getDefaultModel(),
                        CHILD_STREPAYMENTTYPE, CHILD_STREPAYMENTTYPE,
                        CHILD_STREPAYMENTTYPE_RESET_VALUE, null);
                return child;
            } else if (name.equals(CHILD_STPOSTEDINTERESTRATE)) {
                TextField child = new TextField(this, getDefaultModel(),
                        CHILD_STPOSTEDINTERESTRATE, CHILD_STPOSTEDINTERESTRATE,
                        CHILD_STPOSTEDINTERESTRATE_RESET_VALUE, null);
                return child;
            } else if (name.equals(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT)) {
                StaticTextField child = new StaticTextField(this,
                        getDefaultModel(), CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT,
                        CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT,
                        CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT_RESET_VALUE, null);
                return child;
            } else if (name.equals(CHILD_TXPOSTEDRATE)) {
                TextField child =
                    new TextField(this, 
                            getdoComponentLocModel(),
                            CHILD_TXPOSTEDRATE,
                            doComponentLocModel.FIELD_DFPOSTEDRATE,
                            CHILD_TXPOSTEDRATE_RESET_VALUE, null);
                return child;
            }
        /** *************MCM Impl team changes ends - XS_2.34******************** */
            else if (name.equals(CHILD_STTITLE)) {
                StaticTextField child = new StaticTextField(this,
                        getDefaultModel(), CHILD_STTITLE, CHILD_STTITLE,
                        CHILD_STTITLE, null);
                return child;
            } else if (name.equals(CHILD_IMSECTIONDEVIDER)) {
                ImageField child = new ImageField(this, getDefaultModel(),
                        CHILD_IMSECTIONDEVIDER, CHILD_IMSECTIONDEVIDER,
                        CHILD_IMSECTIONDEVIDER, null);
                return child;
            }
            else
            throw new IllegalArgumentException("Invalid child name [" + name
                    + "]");
    }

    public ComboBox getCbProductType() {
        return (ComboBox) getChild(CHILD_CBCOMPONENTTYPE);
    }

    public ComboBox getCbComponentType() {
        return (ComboBox) getChild(CHILD_CBPRODUCTTYPE);
    }

    public TextField getTbLOCAmount() {
        return (TextField) getChild(CHILD_TBLOCAMOUNT);
    }

    public StaticTextField getStMIPremium() {
        return (StaticTextField) getChild(CHILD_STMIPREMIUM);
    }

    public TextField getTbDiscount() {
        return (TextField) getChild(CHILD_TBDISCOUNT);
    }

    public StaticTextField getStTotalLOCAmount() {
        return (StaticTextField) getChild(CHILD_STTOTALLOCAMOUNT);
    }


    public TextField getTbPremium() {
        return (TextField) getChild(CHILD_TBPREMIUM);
    }

    public ComboBox getCbPaymentFrequency() {
        return (ComboBox) getChild(CHILD_CBPAYMENTFREQUENCY);
    }

    public StaticTextField getStNetRate() {
        return (StaticTextField) getChild(CHILD_STNETRATE);
    }

    public TextField getTbCommissionCode() {
        return (TextField) getChild(CHILD_TBCOMMISSIONCODE);
    }

    public StaticTextField getStInterestOnlyPayment() {
        return (StaticTextField) getChild(CHILD_STINTERESTONLYPAYMENT);
    }

    public ComboBox getCbRepaymentType() {
        return (ComboBox) getChild(CHILD_CBREPAYMENTTYPE);
    }

    public StaticTextField getStTaxEscrow() {
        return (StaticTextField) getChild(CHILD_STTAXESCROW);
    }

    public ComboBox getCbExistingAccount() {
        return (ComboBox) getChild(CHILD_CBEXISTINGACCOUNT);
    }

    public StaticTextField getStTotalPayment() {
        return (StaticTextField) getChild(CHILD_STTOTALPAYMENT);
    }

    public TextField getTbExistingAccountRef() {
        return (TextField) getChild(CHILD_TBEXISTINGACCOUNTREF);
    }

    public TextField getTbHoldBackAmount() {
        return (TextField) getChild(CHILD_TBHOLDBACKAMOUNT);
    }

    public ComboBox getCbFirstPaymentMonth() {
        return (ComboBox) getChild(CHILD_CBFIRSTPAYMENTMONTH);
    }

    public TextField getTxFirstPaymentDay() {
        return (TextField) getChild(CHILD_TXFIRSTPAYMENTDAY);
    }

    public TextField getTxFirstPaymentYear() {
        return (TextField) getChild(CHILD_TXFIRSTPAYMENTYEAR);
    }

    public TextField getTbRateGuaranteePeriod() {
        return (TextField) getChild(CHILD_TBRATEGUARANTEEPERIOD);
    }

    public TextField getTbAdditonalInfo() {
        return (TextField) getChild(CHILD_TBADDITIONALINFO);
    }

    public CheckBox getChAllocateMIPremium()
    {   return (CheckBox)getChild(CHILD_CHALLOCATEMIPREMIUM);
    }

    public CheckBox getChAllocateTaxEscrow()
    {   return (CheckBox)getChild(CHILD_CHALLOCATETAXESCROW);
    }

    public Button getBtRecalcLOCComp() {
        return (Button) getChild(CHILD_BTRECALCLOCCOMP);
    }


    public Button getBtDeleteLOCComp() {
        return (Button) getChild(CHILD_BTDELETELOCCOMP);
    }

    public HiddenField getComponentId()
    {
        return (HiddenField)getChild(CHILD_HDCOMPONENTID);
    }

    public HiddenField getCopyId()
    {
        return (HiddenField)getChild(CHILD_HDCOPYID);
    }

    public ComboBox getCbPostedInterestRate() {
        return (ComboBox) getChild(CHILD_CBPOSTEDINTERESTRATE);
    }

    /***************MCM Impl team changes starts - XS_2.34*******************/

    public TextField getStPostedInterestRate() {
        return (TextField) getChild(CHILD_STPOSTEDINTERESTRATE);
    }
    public TextField getStMtgProduct() {
        return (TextField) getChild(CHILD_STPRODUCT);
    }
    public TextField getStRepaymentType() {
        return (TextField) getChild(CHILD_STREPAYMENTTYPE);
    }
    public StaticTextField getStInitDyanmicListJavaScript() {
        return (StaticTextField) getChild(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT);
    }
    public TextField getTxPostedRate() {
        return (TextField) getChild(CHILD_TXPOSTEDRATE);
    }
    
    public StaticTextField getStTitle() {
        return (StaticTextField) getChild(CHILD_STTITLE);
    }
    
    public ImageField getImSectionDevider() {
        return (ImageField) getChild(CHILD_IMSECTIONDEVIDER);
    }
    
    /***************MCM Impl team changes ends - XS_2.34*********************/
    /**
     * <p>handleBtRecalcLOCRequest</p>
     * <p>Description: Handle clicking Recalclate</p>
     * @param event: RequestInvocationEvent
     * @return 
     * @author MCM Team
     * -- XS 2.27
     */
    public void handleBtRecalcLOCCompRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _log.trace("Web event invoked: "+getClass().getName()+".BtRecalcLOC");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();    
        handler.preHandlerProtocol(this.getParentViewBean());
        handler.handleRecalc();
        handler.postHandlerProtocol();    
        return;
    }

    /**
     * <p>handleBtRecalcLOCRequest</p>
     * <p>Description: Handle clicking Recalclate</p>
     * @param event: RequestInvocationEvent
     * @return 
     * @version 1.0 -- XS 2.27
     * @version 1.1 XS 2.32 delete component
     * @author MCM Team
     */
    public void handleBtDeleteLOCCompRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _log.trace("Web event invoked: "+getClass().getName()+".BtDeleteLOC");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this.getParentViewBean());
        int tileIndx = ((TiledViewRequestInvocationEvent)(event)).getTileNumber();
        handler.handleDeleteLOCComp(tileIndx);
        handler.postHandlerProtocol();    
        return;
    }


    /**
     * Description: This method creates the model.
     *
     * @return
     */
    public doComponentLocModel getdoComponentLocModel() {
        if (doComponentDetails == null)
            doComponentDetails = (doComponentLocModel) getModel(doComponentLocModel.class);
        return doComponentDetails;
    }

    /**
     * Description: This method sets the model.
     *
     * @param model
     */
    public void setdoComponentLocModel(doComponentLocModel model) {
        doComponentDetails = model;
    }

    // //////////////////////////////////////////////////////////////////////////
    // Obsolete Netdynamics Events - Require Manual Migration
    // //////////////////////////////////////////////////////////////////////////

    public static Map FIELD_DESCRIPTORS;

    public static final String CHILD_CBCOMPONENTTYPE = "cbComponentTypeLOC";
    public static final String CHILD_CBCOMPONENTTYPE_RESET_VALUE = "";

    public static final String CHILD_CBPRODUCTTYPE = "cbProductTypeLOC";
    public static final String CHILD_CBPRODUCTTYPE_RESET_VALUE = "";

    public static final String CHILD_TBLOCAMOUNT = "tbLOCAmount";
    public static final String CHILD_TBLOCAMOUNT_RESET_VALUE = "";

    public static final String CHILD_STMIPREMIUM = "stMIPremiumLOC";
    public static final String CHILD_STMIPREMIUM_RESET_VALUE = "";

    public static final String CHILD_TBDISCOUNT = "tbDiscountLOC";
    public static final String CHILD_TBDISCOUNT_RESET_VALUE = "";

    public static final String CHILD_STTOTALLOCAMOUNT = "stTotalLOCAmount";
    public static final String CHILD_STTOTALLOCAMOUNT_RESET_VALUE = "";

    public static final String CHILD_TBPREMIUM = "tbPremiumLOC";
    public static final String CHILD_TBPREMIUM_RESET_VALUE = "";

    public static final String CHILD_CBPAYMENTFREQUENCY = "cbPaymentFrequencyLOC";
    public static final String CHILD_CBPAYMENTFREQUENCY_RESET_VALUE = "";

    public static final String CHILD_STNETRATE = "stNetRateLOC";
    public static final String CHILD_STNETRATE_RESET_VALUE = "";

    public static final String CHILD_TBCOMMISSIONCODE = "tbCommissionCodeLOC";
    public static final String CHILD_TBCOMMISSIONCODE_RESET_VALUE = "";

    public static final String CHILD_STINTERESTONLYPAYMENT = "stInterestOnlyPaymentLOC";
    public static final String CHILD_STINTERESTONLYPAYMENT_RESET_VALUE = "";

    public static final String CHILD_CBREPAYMENTTYPE = "cbRepaymentTypeLOC";
    public static final String CHILD_CBREPAYMENTTYPE_RESET_VALUE = "";

    public static final String CHILD_STTAXESCROW = "stTaxEscrowLOC";
    public static final String CHILD_STTAXESCROW_RESET_VALUE = "";

    public static final String CHILD_CBEXISTINGACCOUNT = "cbExistingAccountLOC";
    public static final String CHILD_CBEXISTINGACCOUNT_RESET_VALUE = "";

    public static final String CHILD_STTOTALPAYMENT = "stTotalPaymentLOC";
    public static final String CHILD_STTOTALPAYMENT_RESET_VALUE = "";

    public static final String CHILD_TBEXISTINGACCOUNTREF = "tbExistingAccountRefLOC";
    public static final String CHILD_TBEXISTINGACCOUNTREF_RESET_VALUE = "";

    public static final String CHILD_TBHOLDBACKAMOUNT = "tbHoldBackAmountLOC";
    public static final String CHILD_TBHOLDBACKAMOUNT_RESET_VALUE = "";

    public static final String CHILD_CBFIRSTPAYMENTMONTH = "cbFirstPaymentMonthLOC";
    public static final String CHILD_CBFIRSTPAYMENTMONTH_RESET_VALUE = "";

    public static final String CHILD_TXFIRSTPAYMENTDAY = "txFirstPaymentDayLOC";
    public static final String CHILD_TXFIRSTPAYMENTDAY_RESET_VALUE = "";

    public static final String CHILD_TXFIRSTPAYMENTYEAR = "txFirstPaymentYearLOC";
    public static final String CHILD_TXFIRSTPAYMENTYEAR_RESET_VALUE = "";

    public static final String CHILD_TBRATEGUARANTEEPERIOD = "tbRateGuaranteePeriodLOC";
    public static final String CHILD_TBRATEGUARANTEEPERIOD_RESET_VALUE = "";

    public static final String CHILD_TBADDITIONALINFO = "tbAdditionalInfoLOC";
    public static final String CHILD_TBADDITIONALINFO_RESET_VALUE = "";

    public static final String CHILD_CHALLOCATEMIPREMIUM = "chAllocateMIPremiumLOC";
    public static final String CHILD_CHALLOCATEMIPREMIUM_RESET_VALUE = "N";

    public static final String CHILD_CHALLOCATETAXESCROW = "chAllocateTaxEscrowLOC";
    public static final String CHILD_CHALLOCATETAXESCROW_RESET_VALUE = "N";

    public static final String CHILD_BTRECALCLOCCOMP = "btRecalcLOCComp";
    public static final String CHILD_BTRECALCLOCCOMP_RESET_VALUE = "N";

    public static final String CHILD_BTDELETELOCCOMP = "btDeleteLOCComp";
    public static final String CHILD_BTDELETELOCCOMP_RESET_VALUE = "N";

    public static final String CHILD_HDCOMPONENTID="hdComponentIDLOC";
    public static final String CHILD_HDCOMPONENTID_RESET_VALUE="";

    public static final String CHILD_HDCOPYID="hdCopyIDLOC";
    public static final String CHILD_HDCOPYID_RESET_VALUE="";

    public static final String CHILD_CBPOSTEDINTERESTRATE = "cbPostedInterestRateLOC";
    public static final String CHILD_CBPOSTEDINTERESTRATE_RESET_VALUE = "";

    /***************MCM Impl team changes starts - XS_2.34*******************/

    public static final String CHILD_STPRODUCT = "stProductLOC";
    public static final String CHILD_STPRODUCT_RESET_VALUE = "";

    public static final String CHILD_STPOSTEDINTERESTRATE = "stPostedInterestRate";
    public static final String CHILD_STPOSTEDINTERESTRATE_RESET_VALUE = "";

    public static final String CHILD_STREPAYMENTTYPE = "stRepaymentType";
    public static final String CHILD_STREPAYMENTTYPE_RESET_VALUE = "";

    public static final String CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT = "stInitJavaScript"; 
    public static final String CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT_RESET_VALUE = "";

    public static final String CHILD_TXPOSTEDRATE = "txPostedRate";
    public static final String CHILD_TXPOSTEDRATE_RESET_VALUE = "";

    public static final String CHILD_STTITLE = "stTitle";
    public static final String CHILD_STTITLE_RESET_VALUE = "";    

    public static final String CHILD_IMSECTIONDEVIDER = "imSectionDevider";
    public static final String CHILD_IMSECTIONDEVIDER_RESET_VALUE = "";

    /***************MCM Impl team changes ends - XS_2.34*********************/

    // //////////////////////////////////////////////////////////////////////////
    // Instance variables
    // //////////////////////////////////////////////////////////////////////////

    private doComponentLocModel doComponentDetails = null;

    private ComponentDetailsHandler handler = new ComponentDetailsHandler();

    private CbComponentTypeOptionList cbComponentTypeOptions = new CbComponentTypeOptionList();

    private CbProductTypeOptionList cbProductTypeOptions = new CbProductTypeOptionList();

    private CbPaymentFrequencyOptionList cbPaymentFrequencyOptions = new CbPaymentFrequencyOptionList();

    private CbRePaymentTypeOptionList cbRePaymentTypeOptions = new CbRePaymentTypeOptionList();

    private OptionList cbExistingAccountOptions = new OptionList();

    private OptionList cbFirstPaymentMonthOptions = new OptionList(
            new String[] {}, new String[] {});

    /**
     * CbComponentTypeOptionList
     * inner Class for COMPONENTTYPE comboBox
     * 
     */
    static class CbComponentTypeOptionList extends OptionList {

        CbComponentTypeOptionList() {
        }

        public void populate(RequestContext rc) {
            try {
                String defaultInstanceStateName = rc.getModelManager()
                .getDefaultModelInstanceName(SessionStateModel.class);
                SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
                .getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName, true);
                int languageId = theSessionState.getLanguageId();

                // Get IDs and Labels from BXResources
                Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                        "COMPONENTTYPE", languageId);
                Iterator l = c.iterator();
                String[] theVal = new String[2];
                while (l.hasNext()) {
                    theVal = (String[]) (l.next());
                    add(theVal[1], theVal[0]);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * CbProductTypeOptionList
     * inner Class for MTGPROD comboBox
     * 
     */
    static class CbProductTypeOptionList extends OptionList {

        CbProductTypeOptionList() {
        }

        public void populate(RequestContext rc) {
            try {
                String defaultInstanceStateName = rc.getModelManager()
                .getDefaultModelInstanceName(SessionStateModel.class);
                SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
                .getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName, true);
                int languageId = theSessionState.getLanguageId();

                // Get IDs and Labels from BXResources
                Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                        "MTGPROD", languageId);
                Iterator l = c.iterator();
                String[] theVal = new String[2];
                while (l.hasNext()) {
                    theVal = (String[]) (l.next());
                    add(theVal[1], theVal[0]);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * CbPaymentFrequencyOptionList
     * inner Class for PAYMENTFREQUENCY comboBox
     * 
     */
    static class CbPaymentFrequencyOptionList extends OptionList {

        CbPaymentFrequencyOptionList() {
        }

        public void populate(RequestContext rc) {
            try {
                String defaultInstanceStateName = rc.getModelManager()
                .getDefaultModelInstanceName(SessionStateModel.class);
                SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
                .getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName, true);
                int languageId = theSessionState.getLanguageId();

                // Get IDs and Labels from BXResources
                Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                        "PAYMENTFREQUENCY", languageId);
                Iterator l = c.iterator();
                String[] theVal = new String[2];
                while (l.hasNext()) {
                    theVal = (String[]) (l.next());
                    add(theVal[1], theVal[0]);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * CbRePaymentTypeOptionList
     * inner Class for REPAYMENTTYPE comboBox
     * 
     */
    static class CbRePaymentTypeOptionList extends OptionList {

        CbRePaymentTypeOptionList() {
        }

        public void populate(RequestContext rc) {
            try {
                String defaultInstanceStateName = rc.getModelManager()
                .getDefaultModelInstanceName(SessionStateModel.class);
                SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
                .getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName, true);
                int languageId = theSessionState.getLanguageId();

                // Get IDs and Labels from BXResources
                Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                        "REPAYMENTTYPE", languageId);
                Iterator l = c.iterator();
                String[] theVal = new String[2];
                while (l.hasNext()) {
                    theVal = (String[]) (l.next());
                    add(theVal[1], theVal[0]);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
