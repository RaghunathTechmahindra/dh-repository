package mosApp.MosSystem;

/**
 * 22/Jan/2007 DVG #DG570 #5572  E2E & Prod Abode - Interest Rate Admin screen corruption.  
 */

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doRowGeneratorModelImpl extends QueryModelBase
	implements doRowGeneratorModel
{
	/**
	 *
	 *
	 */
	public doRowGeneratorModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRowNdx()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFROWNDX);
	}


	/**
	 *
	 *
	 */
	public void setDfRowNdx(java.math.BigDecimal value)
	{
		setValue(FIELD_DFROWNDX,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	//#DG570 added order by 
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL ROWGENERATOR.ROWNDX FROM ROWGENERATOR  __WHERE__  ";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL ROWNDX FROM ROWGENERATOR  __WHERE__  ORDER BY ROWNDX";   
	
	public static final String MODIFYING_QUERY_TABLE_NAME="ROWGENERATOR";
	
	public static final String STATIC_WHERE_CRITERIA="";
	
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFROWNDX="ROWGENERATOR.ROWNDX";
	public static final String COLUMN_DFROWNDX="ROWNDX";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFROWNDX,
				COLUMN_DFROWNDX,
				QUALIFIED_COLUMN_DFROWNDX,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

