package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.basis100.picklist.BXResources;

/**
 *
 *
 *
 */
public class doUserAdminGetRegionsModelImpl extends QueryModelBase
	implements doUserAdminGetRegionsModel
{
	/**
	 *
	 *
	 */
	public doUserAdminGetRegionsModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 06Dec2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert Region name.
      this.setDfRegionName(
        BXResources.getPickListDescription(((this.getDfInstitutionProfileId()).intValue()),
                "REGIONPROFILE", this.getDfRegionProfileId().intValue(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRegionProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFREGIONPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfRegionProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFREGIONPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfRegionName()
	{
		return (String)getValue(FIELD_DFREGIONNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfRegionName(String value)
	{
		setValue(FIELD_DFREGIONNAME,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfInstitutionProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINSTITUTIONPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfInstitutionProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINSTITUTIONPROFILEID,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL REGIONPROFILE.REGIONPROFILEID, REGIONPROFILE.REGIONNAME, "+
    "REGIONPROFILE.INSTITUTIONPROFILEID "+
    "FROM REGIONPROFILE  "+
    "__WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="REGIONPROFILE";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFREGIONPROFILEID="REGIONPROFILE.REGIONPROFILEID";
	public static final String COLUMN_DFREGIONPROFILEID="REGIONPROFILEID";
	public static final String QUALIFIED_COLUMN_DFREGIONNAME="REGIONPROFILE.REGIONNAME";
	public static final String COLUMN_DFREGIONNAME="REGIONNAME";
	public static final String QUALIFIED_COLUMN_DFINSTITUTIONPROFILEID="REGIONPROFILE.INSTITUTIONPROFILEID";
	public static final String COLUMN_DFINSTITUTIONPROFILEID="INSTITUTIONPROFILEID";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREGIONPROFILEID,
				COLUMN_DFREGIONPROFILEID,
				QUALIFIED_COLUMN_DFREGIONPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREGIONNAME,
				COLUMN_DFREGIONNAME,
				QUALIFIED_COLUMN_DFREGIONNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINSTITUTIONPROFILEID,
				COLUMN_DFINSTITUTIONPROFILEID,
				QUALIFIED_COLUMN_DFINSTITUTIONPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

