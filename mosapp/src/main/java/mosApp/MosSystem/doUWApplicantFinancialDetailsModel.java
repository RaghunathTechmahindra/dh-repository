package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doUWApplicantFinancialDetailsModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPrimaryFlag();

	
	/**
	 * 
	 * 
	 */
	public void setDfPrimaryFlag(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfFirstName();

	
	/**
	 * 
	 * 
	 */
	public void setDfFirstName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBorrowerMiddleName();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerMiddleName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLastName();

	
	/**
	 * 
	 * 
	 */
	public void setDfLastName(String value);

    
    /**
     * 
     * 
     */
    public String getDfSuffix();

    
    /**
     * 
     * 
     */
    public void setDfSuffix(String value);
	
	/**
	 * 
	 * 
	 */
	public String getDfBorrowerTypeDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerTypeDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalIncomeAmount();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalIncomeAmount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalLiabilityAmount();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalLiabilityAmount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalAssetAmount();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalAssetAmount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerNetWorth();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerNetWorth(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFPRIMARYFLAG="dfPrimaryFlag";
	public static final String FIELD_DFFIRSTNAME="dfFirstName";
	public static final String FIELD_DFBORROWERMIDDLENAME="dfBorrowerMiddleName";
	public static final String FIELD_DFLASTNAME="dfLastName";
    public static final String FIELD_DFSUFFIX="dfSuffix";
	public static final String FIELD_DFBORROWERTYPEDESC="dfBorrowerTypeDesc";
	public static final String FIELD_DFTOTALINCOMEAMOUNT="dfTotalIncomeAmount";
	public static final String FIELD_DFTOTALLIABILITYAMOUNT="dfTotalLiabilityAmount";
	public static final String FIELD_DFTOTALASSETAMOUNT="dfTotalAssetAmount";
	public static final String FIELD_DFBORROWERNETWORTH="dfBorrowerNetWorth";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

