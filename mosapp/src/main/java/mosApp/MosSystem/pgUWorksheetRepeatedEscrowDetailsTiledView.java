package mosApp.MosSystem;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;

import MosSystem.Sc;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.event.TiledViewRequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.TextField;


/**
 *
 *
 *
 */
public class pgUWorksheetRepeatedEscrowDetailsTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgUWorksheetRepeatedEscrowDetailsTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doUWEscrowDetailsModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getHdEscrowPaymentId().setValue(CHILD_HDESCROWPAYMENTID_RESET_VALUE);
		getHdEscrowPaymentCopyId().setValue(CHILD_HDESCROWPAYMENTCOPYID_RESET_VALUE);
		getCbUWEscrowType().setValue(CHILD_CBUWESCROWTYPE_RESET_VALUE);
		getTxUWEscrowPaymentDesc().setValue(CHILD_TXUWESCROWPAYMENTDESC_RESET_VALUE);
		getTxUWEscrowPayment().setValue(CHILD_TXUWESCROWPAYMENT_RESET_VALUE);
		getBtDeleteEscrow().setValue(CHILD_BTDELETEESCROW_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_HDESCROWPAYMENTID,HiddenField.class);
		registerChild(CHILD_HDESCROWPAYMENTCOPYID,HiddenField.class);
		registerChild(CHILD_CBUWESCROWTYPE,ComboBox.class);
		registerChild(CHILD_TXUWESCROWPAYMENTDESC,TextField.class);
		registerChild(CHILD_TXUWESCROWPAYMENT,TextField.class);
		registerChild(CHILD_BTDELETEESCROW,Button.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--start//
    ////Populate EscrowType ComboBoxes manually here
    UWorksheetHandler handler =(UWorksheetHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

    //Setup EscrowType ComboBox options
    cbUWEscrowTypeOptions.populate(getRequestContext());

    handler.pageSaveState();
    //--Release2.1--end//

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
      validJSFromTiledView();
		}

		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_HDESCROWPAYMENTID))
		{
			HiddenField child = new HiddenField(this,
				getdoUWEscrowDetailsModel(),
				CHILD_HDESCROWPAYMENTID,
				doUWEscrowDetailsModel.FIELD_DFESCROWPAYMENTID,
				CHILD_HDESCROWPAYMENTID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDESCROWPAYMENTCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoUWEscrowDetailsModel(),
				CHILD_HDESCROWPAYMENTCOPYID,
				doUWEscrowDetailsModel.FIELD_DFCOPYID,
				CHILD_HDESCROWPAYMENTCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBUWESCROWTYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoUWEscrowDetailsModel(),
				CHILD_CBUWESCROWTYPE,
				doUWEscrowDetailsModel.FIELD_DFESCROWTYPEID,
				CHILD_CBUWESCROWTYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbUWEscrowTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXUWESCROWPAYMENTDESC))
		{
			TextField child = new TextField(this,
				getdoUWEscrowDetailsModel(),
				CHILD_TXUWESCROWPAYMENTDESC,
				doUWEscrowDetailsModel.FIELD_DFESCROWPAYMENTDESC,
				CHILD_TXUWESCROWPAYMENTDESC_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXUWESCROWPAYMENT))
		{
			TextField child = new TextField(this,
				getdoUWEscrowDetailsModel(),
				CHILD_TXUWESCROWPAYMENT,
				doUWEscrowDetailsModel.FIELD_DFESCROWPAYMENT,
				CHILD_TXUWESCROWPAYMENT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETEESCROW))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETEESCROW,
				CHILD_BTDELETEESCROW,
				CHILD_BTDELETEESCROW_RESET_VALUE,
				null);
				return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdEscrowPaymentId()
	{
		return (HiddenField)getChild(CHILD_HDESCROWPAYMENTID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdEscrowPaymentCopyId()
	{
		return (HiddenField)getChild(CHILD_HDESCROWPAYMENTCOPYID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbUWEscrowType()
	{
		return (ComboBox)getChild(CHILD_CBUWESCROWTYPE);
	}

	/**
	 *
	 *
	 */
	static class CbUWEscrowTypeOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbUWEscrowTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// Populate the comboBox from the ResourceBundle.
      public void populate(RequestContext rc)
      {
        try
        {
          ////Get the Language ID from the SessionStateModel
          String defaultInstanceStateName =
                      rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState =
                                                (SessionStateModelImpl)rc.getModelManager().getModel(
                                                SessionStateModel.class,
                                                defaultInstanceStateName,
                                                true);
          int languageId = theSessionState.getLanguageId();
          
          //Get IDs and Labels from BXResources
          Collection c = BXResources.getPickListValuesAndDesc
              (theSessionState.getDealInstitutionId(), "ESCROWTYPE", languageId);
          Iterator l = c.iterator();
          String[] theVal = new String[2];
          while(l.hasNext())
          {
            theVal = (String[])(l.next());
            add(theVal[1], theVal[0]);
          }
        }
        catch (Exception ex) {
          ex.printStackTrace();
        }
      }
	}



	/**
	 *
	 *
	 */
	public TextField getTxUWEscrowPaymentDesc()
	{
		return (TextField)getChild(CHILD_TXUWESCROWPAYMENTDESC);
	}


	/**
	 *
	 *
	 */
	public TextField getTxUWEscrowPayment()
	{
		return (TextField)getChild(CHILD_TXUWESCROWPAYMENT);
	}


	/**
	 *
	 *
	 */
	public void handleBtDeleteEscrowRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
    //logger = SysLog.getSysLogger("PGUWRETV");

    UWorksheetHandler handler =(UWorksheetHandler) this.handler.cloneSS();

    //logger.debug("pgUWRETV@handleBtDeleteEscrowRequest::Parent: " + (pgUWorksheetViewBean) this.getParent());

    handler.preHandlerProtocol((ViewBean) this.getParent());

    int tileIndx = ((TiledViewRequestInvocationEvent)(event)).getTileNumber();
    //logger.debug("pgUWRETV@handleBtDeleteEscrowRequest::TileIndex: " + tileIndx);

		handler.handleDelete(false,
                         4,
                          tileIndx,
                         "RepeatedEscrowDetails/hdEscrowPaymentId",
                         "escrowPaymentId",
                         "RepeatedEscrowDetails/hdEscrowPaymentCopyId",
                         "EscrowPayment");

		//logger.debug("pgUWRETV@handleBtDeleteEscrowRequest::After handleDelete)");

    handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtDeleteEscrow()
	{
		return (Button)getChild(CHILD_BTDELETEESCROW);
	}

	/**
	 *
	 *
	 */
	public String endBtDeleteEscrowDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btDeleteEscrow_onBeforeHtmlOutputEvent method
    UWorksheetHandler handler =(UWorksheetHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public doUWEscrowDetailsModel getdoUWEscrowDetailsModel()
	{
		if (doUWEscrowDetails == null)
			doUWEscrowDetails = (doUWEscrowDetailsModel) getModel(doUWEscrowDetailsModel.class);
		return doUWEscrowDetails;
	}


	/**
	 *
	 *
	 */
	public void setdoUWEscrowDetailsModel(doUWEscrowDetailsModel model)
	{
			doUWEscrowDetails = model;
	}

  public void validJSFromTiledView()
  {
          logger = SysLog.getSysLogger("PGUWEDTV");

          //// Currently it is called from the page. Should be moved to the
          //// TiledView utility classes to generalize the parent (ViewBean name/casting).
          //// ValidationPath is temporarily hardcoded as well. Should be replaced with
          //// the picklist mechanism along with the move to the utility class.

          pgUWorksheetViewBean viewBean = (pgUWorksheetViewBean) getParentViewBean();

          try
          {
            String escrowValidPath =  Sc.DVALID_UW_TILE_ESCROW_PROP_FILE_NAME;

            JSValidationRules escrowValidObj = new JSValidationRules(escrowValidPath, logger, viewBean);
            escrowValidObj.attachJSValidationRules();
          }
          catch( Exception e )
          {
              logger.debug("DataValidJSException: " + e);
              e.printStackTrace();
          }
  }



	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_HDESCROWPAYMENTID="hdEscrowPaymentId";
	public static final String CHILD_HDESCROWPAYMENTID_RESET_VALUE="";
	public static final String CHILD_HDESCROWPAYMENTCOPYID="hdEscrowPaymentCopyId";
	public static final String CHILD_HDESCROWPAYMENTCOPYID_RESET_VALUE="";
	public static final String CHILD_CBUWESCROWTYPE="cbUWEscrowType";
	public static final String CHILD_CBUWESCROWTYPE_RESET_VALUE="";

  //--Release2.1--//
	////private static CbUWEscrowTypeOptionList cbUWEscrowTypeOptions=new CbUWEscrowTypeOptionList();
	private CbUWEscrowTypeOptionList cbUWEscrowTypeOptions=new CbUWEscrowTypeOptionList();
	public static final String CHILD_TXUWESCROWPAYMENTDESC="txUWEscrowPaymentDesc";
	public static final String CHILD_TXUWESCROWPAYMENTDESC_RESET_VALUE="";
	public static final String CHILD_TXUWESCROWPAYMENT="txUWEscrowPayment";
	public static final String CHILD_TXUWESCROWPAYMENT_RESET_VALUE="";
	public static final String CHILD_BTDELETEESCROW="btDeleteEscrow";
	public static final String CHILD_BTDELETEESCROW_RESET_VALUE=" ";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

  public SysLogger logger;
	private doUWEscrowDetailsModel doUWEscrowDetails=null;
	private UWorksheetHandler handler=new UWorksheetHandler();

}

