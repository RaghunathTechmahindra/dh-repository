package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doDetailViewEmpOtherIncomeModelImpl extends QueryModelBase
	implements doDetailViewEmpOtherIncomeModel
{
	/**
	 *
	 *
	 */
	public doDetailViewEmpOtherIncomeModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 18Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert INCOMETYPE Desc.
      this.setDfIncomeTypeDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "INCOMETYPE", this.getDfIncomeTypeDesc(), languageId));
      //Convert INCOMEPERIOD Desc.
      this.setDfIncomePeriodDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "INCOMEPERIOD", this.getDfIncomePeriodDesc(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public String getDfIncomeTypeDesc()
	{
		return (String)getValue(FIELD_DFINCOMETYPEDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeTypeDesc(String value)
	{
		setValue(FIELD_DFINCOMETYPEDESC,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIncomeEmpRelated()
	{
		return (String)getValue(FIELD_DFINCOMEEMPRELATED);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeEmpRelated(String value)
	{
		setValue(FIELD_DFINCOMEEMPRELATED,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIncomePeriodDesc()
	{
		return (String)getValue(FIELD_DFINCOMEPERIODDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomePeriodDesc(String value)
	{
		setValue(FIELD_DFINCOMEPERIODDESC,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIncomeAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINCOMEAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINCOMEAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIncomePercentageInGDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINCOMEPERCENTAGEINGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomePercentageInGDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINCOMEPERCENTAGEINGDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIncomePercentageInTDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINCOMEPERCENTAGEINTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomePercentageInTDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINCOMEPERCENTAGEINTDS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIncomeDesc()
	{
		return (String)getValue(FIELD_DFINCOMEDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeDesc(String value)
	{
		setValue(FIELD_DFINCOMEDESC,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}

    public java.math.BigDecimal getDfInstitutionId() {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
      }

      public void setDfInstitutionId(java.math.BigDecimal value) {
        setValue(FIELD_DFINSTITUTIONID, value);
      }


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 18Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
	//public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT INCOMETYPE.ITDESCRIPTION, INCOMETYPE.EMPLOYMENTRELATED, INCOMEPERIOD.IPDESCRIPTION, INCOME.INCOMEAMOUNT, INCOME.BORROWERID, INCOME.INCPERCENTINGDS, INCOME.INCPERCENTINTDS, INCOME.INCOMEDESCRIPTION, INCOME.COPYID FROM INCOME, INCOMEPERIOD, INCOMETYPE  __WHERE__  ";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT DISTINCT to_char(INCOME.INCOMETYPEID) INCOMETYPEID_STR, INCOMETYPE.EMPLOYMENTRELATED, "+
    "to_char(INCOME.INCOMEPERIODID) INCOMEPERIODID_STR, INCOME.INCOMEAMOUNT, INCOME.BORROWERID, "+
    "INCOME.INCPERCENTINGDS, INCOME.INCPERCENTINTDS, INCOME.INCOMEDESCRIPTION, "+
    "INCOME.COPYID, "+
    "INCOME.INSTITUTIONPROFILEID "+
    " FROM INCOME, INCOMETYPE  "+
    "__WHERE__  ";
	//--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //public static final String MODIFYING_QUERY_TABLE_NAME="INCOME, INCOMEPERIOD, INCOMETYPE";
	public static final String MODIFYING_QUERY_TABLE_NAME=
    "INCOME, INCOMETYPE";
	//--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
  //public static final String STATIC_WHERE_CRITERIA=" (((INCOME.INCOMEPERIODID  =  INCOMEPERIOD.INCOMEPERIODID) AND (INCOME.INCOMETYPEID  =  INCOMETYPE.INCOMETYPEID)) AND INCOMETYPE.EMPLOYMENTRELATED = 'N')";
	public static final String STATIC_WHERE_CRITERIA=
    " (((INCOME.INCOMETYPEID  =  INCOMETYPE.INCOMETYPEID)) AND "+
    "INCOMETYPE.EMPLOYMENTRELATED = 'N')";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFINCOMEEMPRELATED="INCOMETYPE.EMPLOYMENTRELATED";
	public static final String COLUMN_DFINCOMEEMPRELATED="EMPLOYMENTRELATED";
	public static final String QUALIFIED_COLUMN_DFINCOMEAMOUNT="INCOME.INCOMEAMOUNT";
	public static final String COLUMN_DFINCOMEAMOUNT="INCOMEAMOUNT";
	public static final String QUALIFIED_COLUMN_DFBORROWERID="INCOME.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFINCOMEPERCENTAGEINGDS="INCOME.INCPERCENTINGDS";
	public static final String COLUMN_DFINCOMEPERCENTAGEINGDS="INCPERCENTINGDS";
	public static final String QUALIFIED_COLUMN_DFINCOMEPERCENTAGEINTDS="INCOME.INCPERCENTINTDS";
	public static final String COLUMN_DFINCOMEPERCENTAGEINTDS="INCPERCENTINTDS";
	public static final String QUALIFIED_COLUMN_DFINCOMEDESC="INCOME.INCOMEDESCRIPTION";
	public static final String COLUMN_DFINCOMEDESC="INCOMEDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFCOPYID="INCOME.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";

	//--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	public static final String QUALIFIED_COLUMN_DFINCOMETYPEDESC="INCOME.INCOMETYPEID_STR";
	public static final String COLUMN_DFINCOMETYPEDESC="INCOMETYPEID_STR";
  public static final String QUALIFIED_COLUMN_DFINCOMEPERIODDESC="INCOME.INCOMEPERIODID_STR";
	public static final String COLUMN_DFINCOMEPERIODDESC="INCOMEPERIODID_STR";
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "INCOME.INSTITUTIONPROFILEID";
      public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	//[[SPIDER_EVENTS BEGIN

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMETYPEDESC,
				COLUMN_DFINCOMETYPEDESC,
				QUALIFIED_COLUMN_DFINCOMETYPEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEEMPRELATED,
				COLUMN_DFINCOMEEMPRELATED,
				QUALIFIED_COLUMN_DFINCOMEEMPRELATED,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEPERIODDESC,
				COLUMN_DFINCOMEPERIODDESC,
				QUALIFIED_COLUMN_DFINCOMEPERIODDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEAMOUNT,
				COLUMN_DFINCOMEAMOUNT,
				QUALIFIED_COLUMN_DFINCOMEAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEPERCENTAGEINGDS,
				COLUMN_DFINCOMEPERCENTAGEINGDS,
				QUALIFIED_COLUMN_DFINCOMEPERCENTAGEINGDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEPERCENTAGEINTDS,
				COLUMN_DFINCOMEPERCENTAGEINTDS,
				QUALIFIED_COLUMN_DFINCOMEPERCENTAGEINTDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEDESC,
				COLUMN_DFINCOMEDESC,
				QUALIFIED_COLUMN_DFINCOMEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                  FIELD_DFINSTITUTIONID,
                  COLUMN_DFINSTITUTIONID,
                  QUALIFIED_COLUMN_DFINSTITUTIONID,
                  java.math.BigDecimal.class,
                  false,
                  false,
                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                  "",
                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                  ""));
	}

}

