package mosApp.MosSystem;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.commitment.CCM;
import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.entity.SourceOfBusinessProfile;
import com.basis100.deal.pk.SourceFirmProfilePK;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.HtmlDisplayFieldBase;


/**
 *
 *
 */
public class SourceFirmEditHandler extends PageHandlerCommon
	implements Cloneable, Sc
{
	// Key name to pass the SOB Id

	public static final String SOB_ID_PASSED_PARM="sourceOBIdPassed";
	public static final String SFIRM_ID_PASSED_PARM="sourceFirmIdPassed";
  //-- FXLink Phase II --//
  //--> System Type Must be passed for Adding new SOB
  //--> By Billy 04Nov2003
  public static final String SYSTYPE_ID_PASSED_PARM="systemTypeIdPassed";
  //--> Source Firm Returned from Adding new Firm
  //--> By Billy 17Nov2003
  public static final String SFIRM_ID_RETURN_PARM="sourceFirmIdReturned";

  //=================================================
	// following used as key in page state table to indicate if user has edit permission

	public static final String USER_CAN_EDIT="USER_CAN_EDIT";
	// key for edit buffer (in page state table)

	public static final String SF_EDIT_BUFFER="SF_EDIT_BUFFER";
	// Key to indicate the action : Add or Modify

	public static final String SF_EDIT_ACTION="SF_EDIT_ACTION";
	// AlertMessage OK type ==> User OK to unassociate user

	public static final String SF_OK_TO_UNASSOCIATEUSER="SF_OK_TO_UNASSOCIATEUSER";
  public static final String SF_OK_TO_UNASSOCIATEGROUP="SF_OK_TO_UNASSOCIATEGROUP";


	/**
	 *
	 *
	 */
	public SourceFirmEditHandler cloneSS()
	{
		return(SourceFirmEditHandler) super.cloneSafeShallow();

	}


	/////////////////////////////////////////////////////////////////////////

	public void preHandlerProtocol(ViewBean currNDPage)
	{
		super.preHandlerProtocol(currNDPage);


		// sefault page load method to display()
		PageEntry pg = theSessionState.getCurrentPage();

		pg.setPageDisplayMethod(Mc.DISPLAY_METHOD_DISP);

	}


	/////////////////////////////////////////////////////////////////////////
	// This method is used to populate the fields in the header
	// with the appropriate information

	public void populatePageDisplayFields()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		Hashtable pst = pg.getPageStateTable();

		ViewBean theNDPage = getCurrNDPage();

		populatePageShellDisplayFields();

		populatePreviousPagesLinks();

		populatePageDealSummarySnapShot();

		EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(SF_EDIT_BUFFER);

    //---------------- FXLink Phase II ------------------//
    //--> Set some fields to disable based on the ChannelMedia and System Properties
    //--> By Billy 14Nov2003
    // Note : this must be called before populateFields in order to preserve the
    //    fileds which disabled.
    setupDisabledFields(theNDPage, efb);
    //=====================================================
		efb.populateFields(getCurrNDPage());

		displayDSSConditional(pg, getCurrNDPage());

		//--Release2.1--//
    //// Since this static html label should be translated on fly toggling the
    //// language and pst content is not refreshed on toggle action the label
    //// except text should be set to pst.
    //// Lookup to translate the label.
    String sfAction = BXResources.getGenericMsg(new String((String) pst.get(SF_EDIT_ACTION)),
                                                 theSessionState.getLanguageId());

    getCurrNDPage().setDisplayFieldValue("stAction", sfAction);

    //// Data validation API implementation.
    try
    {
 
      String sourceFirmEditValidPath = Sc.DVALID_SOURCE_FIRM_EDIT_PROPS_FILE_NAME;

      //logger.debug("SFEH@sourceFirmEditValidPath: " + sourceFirmEditValidPath);

      JSValidationRules sourceFirmEditValidObj = new JSValidationRules(sourceFirmEditValidPath, logger, theNDPage);
      sourceFirmEditValidObj.attachJSValidationRules();
    }
    catch( Exception e )
    {
        logger.error("Exception SFEH@PopulatePageDisplayFields: Problem encountered in JS data validation API." + e.getMessage());
    }

	}

	/////////////////////////////////////////////////////////////////////////

	private void displayDSSConditional(PageEntry pg, ViewBean thePage)
	{
		String suppressStart = "<!--";

		String suppressEnd = "//-->";

		if (pg == null || pg.getPageDealId() <= 0)
		{
			// suppress
			thePage.setDisplayFieldValue("stIncludeDSSstart", new String(suppressStart));
			thePage.setDisplayFieldValue("stIncludeDSSend", new String(suppressEnd));
			return;
		}


		// display
		thePage.setDisplayFieldValue("stIncludeDSSstart", new String(""));

		thePage.setDisplayFieldValue("stIncludeDSSend", new String(""));

		return;

	}


	/////////////////////////////////////////////////////////////////////////

	public void setupBeforePageGeneration()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		ViewBean theNDPage = getCurrNDPage();

		Hashtable pst = pg.getPageStateTable();


		// determine if user can modify existing party information
		if (getUserAccessType(Mc.PGNM_SFIRM_ADD) == Sc.PAGE_ACCESS_EDIT) pst.put(USER_CAN_EDIT, USER_CAN_EDIT);

		setupEditBuffer();

		setupDealSummarySnapShotDO(pg);

	}


	/**
	 *
	 *
	 */
	public void setupEditBuffer()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		try
		{
			Hashtable pst = pg.getPageStateTable();
      //--> Bug fix : Need to fill the month even if state on the same page i.e. for ToggleLanguage
      //--> By Billy 21Aug2003
      // Set up Date display fields
      fillupMonths("cbAUserEffectiveMonth");
      //=================================================
      //--> CRF#37 :: Source/Firm to Group routing for TD
      //=================================================
      //--> Added new fields to handle Source to Group association
      //--> By Billy 20Aug2003
      fillupMonths("cbAGroupEffectiveMonth");
      //=================================================

			if (pst == null || pst.get(SF_EDIT_BUFFER) == null)
			{
				// first invocation!
				if (pst == null)
				{
					pst = new Hashtable();
					pg.setPageStateTable(pst);
				}
				EditFieldsBuffer efb = new EditSFBuffer("EditSF", logger);
				pst.put(SF_EDIT_BUFFER, efb);

				// determine if this is a modify existing vs. add new
				String SFProfileId =(String) pst.get(SFIRM_ID_PASSED_PARM);
				if (SFProfileId != null && SFProfileId.length() > 0)
				{
					logger.trace("BILLY ==>> @SourceFirmEditHandler.setupEditBuffer: is modify, SFirm profile id = " + SFProfileId);

					// set modify action string!
          //--Release2.1--//
          //// Since this static html label should be translated on fly toggling the
          //// language and pst content is not refreshed on toggle action the label
          //// except string should be set to pst.

					////pst.put(SF_EDIT_ACTION, "Modify Existing Source Firm:");
          pst.put(SF_EDIT_ACTION, "MODIFY_EXISTING_SOURCE_LABEL");

					// user data object to populate existing data
					////CSpCriteriaSQLObject theDO =(CSpCriteriaSQLObject) getModel(doSourceFirmModel.class);
          doSourceFirmModel theDO = (doSourceFirmModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doSourceFirmModel.class);


					theDO.clearUserWhereCriteria();
					theDO.addUserWhereCriterion("dfSBPFirmProfileId", "=", new String(SFProfileId));
					efb.readFields((QueryModelBase) theDO);

					//Manually populate some fileds
          //--> Simplified by Billy : 20Aug2003
          //--> Set Phone #
          setPhoneNoDisplayField(theDO, "tbPhoneNumber1", "tbPhoneNumber2", "tbPhoneNumber3", "dfSBPPhone", 0);
          //--> Also need to populate to efb
          efb.setFieldValue("tbPhoneNumber1", getCurrNDPage().getDisplayFieldValue("tbPhoneNumber1").toString());
          efb.setFieldValue("tbPhoneNumber2", getCurrNDPage().getDisplayFieldValue("tbPhoneNumber2").toString());
          efb.setFieldValue("tbPhoneNumber3", getCurrNDPage().getDisplayFieldValue("tbPhoneNumber3").toString());
          //--> Set FAX #
					setPhoneNoDisplayField(theDO, "tbFaxNumber1", "tbFaxNumber2", "tbFaxNumber3", "dfSBPFax", 0);
          //--> Also need to populate to efb
          efb.setFieldValue("tbFaxNumber1", getCurrNDPage().getDisplayFieldValue("tbFaxNumber1").toString());
          efb.setFieldValue("tbFaxNumber2", getCurrNDPage().getDisplayFieldValue("tbFaxNumber2").toString());
          efb.setFieldValue("tbFaxNumber3", getCurrNDPage().getDisplayFieldValue("tbFaxNumber3").toString());

          // Set up Date display fields
          // Set User Effective Date
          setDateDisplayField(theDO, "cbAUserEffectiveMonth", "txAUserEffectiveDay",
              "txAUserEffectiveYear", theDO.FIELD_DFSBPUSEREFFECTIVEDATE, 0);
          //--> Also need to populate to efb
          efb.setFieldValue("cbAUserEffectiveMonth", getCurrNDPage().getDisplayFieldValue("cbAUserEffectiveMonth").toString());
          efb.setFieldValue("txAUserEffectiveDay", getCurrNDPage().getDisplayFieldValue("txAUserEffectiveDay").toString());
          efb.setFieldValue("txAUserEffectiveYear", getCurrNDPage().getDisplayFieldValue("txAUserEffectiveYear").toString());
          //=================================================
          //--> CRF#37 :: Source/Firm to Group routing for TD
          //=================================================
          //--> Added new fields to handle Source to Group association
          //--> By Billy 20Aug2003
				  // Set Group Effective Date
          setDateDisplayField(theDO, "cbAGroupEffectiveMonth", "txAGroupEffectiveDay",
              "txAGroupEffectiveYear", theDO.FIELD_DFSBPGROUPEFFECTIVEDATE, 0);
          //--> Also need to populate to efb
          efb.setFieldValue("cbAGroupEffectiveMonth", getCurrNDPage().getDisplayFieldValue("cbAGroupEffectiveMonth").toString());
          efb.setFieldValue("txAGroupEffectiveDay", getCurrNDPage().getDisplayFieldValue("txAGroupEffectiveDay").toString());
          efb.setFieldValue("txAGroupEffectiveYear", getCurrNDPage().getDisplayFieldValue("txAGroupEffectiveYear").toString());
          //=================================================

					efb.setOriginalAsCurrent();
          //---------------- FXLink Phase II ------------------//
          // set the SystemType value to PST as well
          //--> By Billy 11Nov2003
          pst.put(SYSTYPE_ID_PASSED_PARM, theDO.getDfSBPSystemTypeId().toString());
          //====================================================
				}
				else
				{
          //--Release2.1--//
          //// Since this static html label should be translated on fly toggling the
          //// language and pst content is not refreshed on toggle action the label
          //// except string should be set to pst.
          ////pst.put(SF_EDIT_ACTION, "Add New Source Firm:");
          pst.put(SF_EDIT_ACTION, "ADD_NEW_SOURCE_FIRM_LABEL");

          //---------------- FXLink Phase II ------------------//
          //--> Setup System Type display from PST input
          //--> By Billy 11Nov2003
          efb.setFieldValue("stSystemType",
            BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),"SYSTEMTYPE", getPageSystemTypeId(), theSessionState.getLanguageId()));
          //====================================================
				}
			}
		}
		catch(Exception ex)
		{
			setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
			logger.error("Exception @SourceFirmEditHandler.setupEditBuffer");
			logger.error(ex);
			return;
		}

	}


	/**
	 *
	 *
	 */
	////public int handleViewModifyButton()
	public boolean handleViewModifyButton()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		try
		{
			Hashtable pst = pg.getPageStateTable();
			if (pst.get(USER_CAN_EDIT) != null)
          return true;
		}
		catch(Exception e)
		{
			;
		}
		return false;
	}


	/**
	 *
	 *
	 */
	public boolean handleViewUnassociateButtons()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		try
		{
			Hashtable pst = pg.getPageStateTable();
			if (pst.get(USER_CAN_EDIT) != null)
			{
				String SFProfileId =(String) pst.get(SFIRM_ID_PASSED_PARM);
				if (SFProfileId != null && SFProfileId.length() > 0)
            return true;
			}
		}
		catch(Exception e)
		{
			;
		}

		return false;

	}


	/**
	 *
	 *
	 */
	public void saveData(PageEntry currPage, boolean calledInTransaction)
	{
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(SF_EDIT_BUFFER);

		efb.readFields(getCurrNDPage());

		if (efb.isModified()) pg.setModified(true);

	}


	/**
	 *
	 *
	 */
  //// This is obsolete ND related method.
  //// BXTODO: Finally test and eliminate it during the final clean up.
  /**
	public void refreshComboBox(CSpDisplayEvent event)
	{

		//Get the combo box
		ComboBox theCbObj =(ComboBox) event.getSource();


		// get the DataObject Auto Fill control
		CSpDataObjectAutoFillControl theDoAutoCtl =(CSpDataObjectAutoFillControl) theCbObj.getAutoFillControl();


		// Get Data Object
		CSpCriteriaSQLObject theCbDoObj =(CSpCriteriaSQLObject) theDoAutoCtl.getDataObjectRef().getDataObject();


		// Clear all Criteria
		theCbDoObj.clearUserWhereCriteria();

		theCbObj.doAutoFill();

		theCbObj.select(theCbObj.getValue());

	}
  **/

	/**
	 *
	 *
	 */
	public void handleSubmit()
	{
		////SessionState theSession = getTheSession();

		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();


		// Validation check ... i.e. check for minimal data inpur, DupeCheck, ...
		String errorMsg = validateSF(getCurrNDPage());

		if (! errorMsg.equals(""))
		{
			// Validation Errors found -- set to display Alert Messages
			setActiveMessageToAlert(BXResources.getSysMsg("SOURCE_FIRM_EDIT_VALIDATION_ERROR", theSessionState.getLanguageId()) + errorMsg,
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			return;
		}


		// check for modify vs. add new
		SourceFirmProfile theSFP;

		String SFProfileId =(String) pst.get(SFIRM_ID_PASSED_PARM);

		if (SFProfileId == null || SFProfileId.length() == 0)
		{
			theSFP = addNewSF(theSessionState, pg, pst);

			// Check if Parent Page is Firm Review, then set to display the new SourceFirm -- Billy 19Nov2001
			if (theSFP != null)
			{
				PageEntry parent = pg.getParentPage();
				if (parent.getPageId() == Mc.PGNM_SFIRM_REVIEW)
				{
					Hashtable pPst = parent.getPageStateTable();
					pPst.put(SFIRM_ID_PASSED_PARM, "" + theSFP.getSourceFirmProfileId());
				}
        //-- FXLink Phase II --//
        //--> Return the Firm ID if from SOB Edit
        //--> By Billy 17Nov2003
        else if(parent.getPageId() == Mc.PGNM_SOB_ADD)
				{
          Hashtable pPst = parent.getPageStateTable();
					pPst.put(SFIRM_ID_RETURN_PARM, "" + theSFP.getSourceFirmProfileId());
        }
        //======================================
			}
		}
		else
		{
			theSFP = modifySF(getIntValue(SFProfileId), theSessionState, pg, pst);
		}


		// if no message set (no eror encountered) then away we go ...
		if (theSessionState.isActMessageSet() == false)
		{
			navigateToNextPage(true);
		}

	}


	/**
	 *
	 *
	 */
	public void handleUserUnassociate(boolean confirmed)
	{
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		SessionResourceKit srk = getSessionResourceKit();

		if (! confirmed)
		{
			// Validation Errors found -- set to display Alert Messages
			setActiveMessageToAlert(BXResources.getSysMsg("SOURCE_FIRM_UNASSOC_USER_CONFIRM", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMDIALOG, SF_OK_TO_UNASSOCIATEUSER);
			return;
		}

		try
		{
			srk.beginTransaction();
			SourceFirmProfile theSFP = new SourceFirmProfile(srk);
			String SFProfileId =(String) pst.get(SFIRM_ID_PASSED_PARM);
			theSFP = theSFP.findByPrimaryKey(new SourceFirmProfilePK(getIntValue(SFProfileId)));
			theSFP.deleteFirmUserAssoc();
			srk.commitTransaction();

			// reset Associated User DisplayFields
			EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(SF_EDIT_BUFFER);
			efb.setFieldValue("cbAssociatedUser", new String(""));
			efb.setFieldValue("cbAUserEffectiveMonth", new String("0"));
			efb.setFieldValue("txAUserEffectiveDay", new String(""));
			efb.setFieldValue("txAUserEffectiveYear", new String(""));
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Exception @SourceFirmEditHandler.handleUserUnassociate:");
			logger.error(e);
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
		}

	}


	//--> Miss convert to multi-lingual
  //--> Fixed by Billy 19Nov2003
	private String validateSF(ViewBean thePage)
	{
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		SessionResourceKit srk = getSessionResourceKit();
    int languageId = theSessionState.getLanguageId();

		String retStr = "";
		int SFProfileId = getIntValue((String) pst.get(SFIRM_ID_PASSED_PARM));

		// Check if ShortName input
		////if (thePage.getDisplayFieldStringValue("tbShortName").equals(""))
		if (thePage.getDisplayFieldValue("tbShortName").equals(""))
		{
			if (! retStr.equals("")) retStr += ", ";
      retStr += "<br> - " + BXResources.getSysMsg("SOB_SF_EDIT_MISS_SHORTNAME", languageId);
		}

		// Check if LastName input
		////if (thePage.getDisplayFieldStringValue("tbContactLastName").equals(""))
		if (thePage.getDisplayFieldValue("tbContactLastName").equals(""))
		{
			if (! retStr.equals("")) retStr += ", ";
			retStr += "<br> - " + BXResources.getSysMsg("SOB_SF_EDIT_MISS_LASTNAME", languageId);
		}

		// Check if FirmName input
		////if (thePage.getDisplayFieldStringValue("tbFirmName").equals(""))
		if (thePage.getDisplayFieldValue("tbFirmName").equals(""))
		{
			if (! retStr.equals("")) retStr += ", ";
			retStr += "<br> - " + BXResources.getSysMsg("SOB_SF_EDIT_MISS_FIRMNAME", languageId);
		}

    // Check if BusinessId input OK
		String tmpFieldValue = (String)thePage.getDisplayFieldValue("tbClientNum");
    //logger.debug("SFEH@validateSOB::cbxClientNum: " + tmpFieldValue);
    //---------------- FXLink Phase II ------------------//
    //--> Modified to check Uniqueness only if generatesourcefirmrefnum = N
    //--> By Billy 14Nov2003
    if (isBusIdEditable())
		{
      if (tmpFieldValue.equals(""))
      {
        if (! retStr.equals("")) retStr += ", ";
        retStr += "<br> - " + BXResources.getSysMsg("SOB_SF_EDIT_MISS_CLIENTNUM", languageId);
      }
      else
      {
        //Check if dupicated Business Id
        try
        {
          SourceFirmProfile tmpSF = new SourceFirmProfile(srk);
          if (tmpSF.isDupeBusId(tmpFieldValue, SFProfileId))
            retStr += "<br> - " + BXResources.getSysMsg("SOB_SF_EDIT_DUP_FIRMCLIENTNUM", languageId);
        }
        catch(Exception e)
        {
          logger.warning("Exception @SourceFirmEditHandler.validateSF : " + e.getMessage());
        }
      }
    }
    //=============================================

    // Check if Source System Code input OK
		//---------------- FXLink Phase II ------------------//
    //--> Logic changed for System Code Checking
    //--> by Billy 10Nov2003
    tmpFieldValue = thePage.getDisplayFieldValue("tbSystemCode").toString();
    int theSystemTypeId = getIntValue((String) pst.get(SYSTYPE_ID_PASSED_PARM));

		try
		{
      SourceFirmProfile tmpSF = new SourceFirmProfile(srk);
      // Check if dup if SystemType.ChannelMedia <> 'E'
			if (isSystemCodeEditable())
			{
				if (tmpFieldValue.equals(""))
				{
					if (! retStr.equals("")) retStr += ", ";
					retStr += "<br> - " + BXResources.getSysMsg("SOB_SF_EDIT_MISS_FIRMSYSTEMCODE", languageId);
				}
				else
				{
					if (tmpSF.isDupeSystemCode(tmpFieldValue, theSystemTypeId, SFProfileId))
            retStr += "<br> - " + BXResources.getSysMsg("SOB_SF_EDIT_DUP_FIRMSYSTEMCODE", languageId);
				}
			}
		}
		catch(Exception e)
		{
			logger.warning("Exception @SourceFirmEditHandler.validateSF (SystemCode DupeCheck) :" + e.getMessage());
		}

		return(retStr);

	}


	/**
	 *
	 *
	 */
	private SourceFirmProfile addNewSF(SessionStateModelImpl theSessionState, PageEntry pg, Hashtable pst)
	{
		SessionResourceKit srk = getSessionResourceKit();

		try
		{
			srk.beginTransaction();
			EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(SF_EDIT_BUFFER);

			// Create new SFP entity
			SourceFirmProfile theSFP = new SourceFirmProfile(srk);
			theSFP.create();

			// Perform save data from screen
			saveSFData(theSFP, efb, true);
			srk.commitTransaction();

			// Set modified flag to false to prevent display meaningless message to user
			pg.setModified(false);
			return theSFP;
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Exception @SourceFirmEditHandler.addNewSF:");
			logger.error(e);
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			return null;
		}

	}


	/**
	 *
	 *
	 */
	private SourceFirmProfile modifySF(int SFPId, SessionStateModelImpl theSessionState, PageEntry pg, Hashtable pst)
	{
		SessionResourceKit srk = getSessionResourceKit();

		try
		{
			srk.beginTransaction();
			EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(SF_EDIT_BUFFER);

			// Create new SOBP entity
			SourceFirmProfile theSFP = new SourceFirmProfile(srk);
			theSFP = theSFP.findByPrimaryKey(new SourceFirmProfilePK(SFPId));

			// Perform save data from screen
			saveSFData(theSFP, efb, false);
			srk.commitTransaction();

			// Set modified flag to false to prevent display meaningless message to user
			pg.setModified(false);
			return theSFP;
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Exception @SourceFirmEditHandler.modifySF:");
			logger.error(e);
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			return null;
		}

	}


	/**
	 *
	 *
	 */
  //---------------- FXLink Phase II ------------------//
  //--> Modified to pass the indicator to specify if it is a New or Modify
  //--> By Billy 11Nov2003
	private void saveSFData(SourceFirmProfile theSFP, EditFieldsBuffer efb, boolean isNew)
		throws Exception
	{
		// Update Contact and Address Info.
		Contact contact = theSFP.getContact();
		Addr addr = contact.getAddr();
		String [ ] val = new String [ 5 ];

		val [ 0 ] = efb.getFieldValue("cbProvince");
		if (val [ 0 ] != null) addr.setProvinceId(getIntValue(val [ 0 ]));

		val [ 0 ] = efb.getFieldValue("tbAddressLine1");
		if (val [ 0 ] != null) addr.setAddressLine1(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("tbAddressLine2");
		if (val [ 0 ] != null) addr.setAddressLine2(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("tbCity");
		if (val [ 0 ] != null) addr.setCity(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("tbPostalCodeFSA");
		if (val [ 0 ] != null) addr.setPostalFSA(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("tbPostalCodeLDU");
		if (val [ 0 ] != null) addr.setPostalLDU(val [ 0 ]);

		addr.ejbStore();

		val [ 0 ] = efb.getFieldValue("tbContactFirstName");
		if (val [ 0 ] != null) contact.setContactFirstName(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("tbContactMiddleInitial");
		if (val [ 0 ] != null) contact.setContactMiddleInitial(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("tbContactLastName");
		if (val [ 0 ] != null) contact.setContactLastName(val [ 0 ]);

		val [ 0 ] = phoneJoin(efb.getFieldValue("tbPhoneNumber1"), efb.getFieldValue("tbPhoneNumber2"), efb.getFieldValue("tbPhoneNumber3"));
		if (val [ 0 ] != null && val [ 0 ].trim().length() != 0) contact.setContactPhoneNumber(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("tbPhoneExtension");
		if (val [ 0 ] != null) contact.setContactPhoneNumberExtension(val [ 0 ]);

		val [ 0 ] = phoneJoin(efb.getFieldValue("tbFaxNumber1"), efb.getFieldValue("tbFaxNumber2"), efb.getFieldValue("tbFaxNumber3"));
		if (val [ 0 ] != null && val [ 0 ].trim().length() != 0) contact.setContactFaxNumber(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("tbEmail");
		if (val [ 0 ] != null) contact.setContactEmailAddress(val [ 0 ]);

    //--DJ_PREFCOMMETHOD_CR--start--//
    val [ 0 ] = efb.getFieldValue("cbPrefDeliveryMethod");

    if (val [ 0 ] != null) contact.setPreferredDeliveryMethodId((getIntValue(val [ 0 ])));
    //--DJ_PREFCOMMETHOD_CR--end--//

		contact.ejbStore();

		// Update SOBP info.
		val [ 0 ] = efb.getFieldValue("tbShortName");
		if (val [ 0 ] != null) theSFP.setSfShortName(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("cbStatus");
		if (val [ 0 ] != null) theSFP.setProfileStatusId(getIntValue(val [ 0 ]));

    //---------------- FXLink Phase II ------------------//
    //--> SystemType is display only ==> only save once if New
		//val [ 0 ] = efb.getFieldValue("cbSystemType");
		//if (val [ 0 ] != null) theSFP.setSystemTypeId(getIntValue(val [ 0 ]));
    // Update SystemType if new SF
    if(isNew)
      theSFP.setSystemTypeId(getPageSystemTypeId());
    //=====================================================

    //---------------- FXLink Phase II ------------------//
    //--> check if ClientNum editable
    //--> By Billy 14Nov2003
    if(isBusIdEditable())
    {
      val [ 0 ] = efb.getFieldValue("tbClientNum");
      if (val [ 0 ] != null) theSFP.setSfBusinessId(val [ 0 ]);
    }
    //--> Check if New ==> create Client # automatically
    else if(isNew)
    {
      // Create Client # automatically
      CCM theCCM = new CCM(srk);
      theSFP.setSfBusinessId(theCCM.getIPENumber(1, theSessionState.getLanguageId(), 
                                                 theSessionState.getDealInstitutionId()));
    }
    //=====================================================

    //---------------- FXLink Phase II ------------------//
    //--> Rename MailBox to SystemCode
    //--> Check if it is editable
    //--> By Billy 14Nov2003
    if(isSystemCodeEditable())
    {
      val [ 0 ] = efb.getFieldValue("tbSystemCode");
		  if (val [ 0 ] != null) theSFP.setSourceFirmCode(val [ 0 ]);
    }
    //======================================================

		val [ 0 ] = efb.getFieldValue("tbAlternativeId");
		if (val [ 0 ] != null) theSFP.setAlternativeId(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("tbFirmName");
		if (val [ 0 ] != null) theSFP.setSourceFirmName(val [ 0 ]);
		
		val [ 0 ] = efb.getFieldValue("tbLicenseNumber");
		if (val [ 0 ] != null) theSFP.setSfLicenseRegistrationNumber(val [ 0 ]);

		// Check for Update/Create/Delect SourceUser Association
		val [ 0 ] = efb.getFieldValue("cbAssociatedUser");

		// Check if SourceUser selected
		if (val [ 0 ] != null && ! val [ 0 ].trim().equals(""))
		{
			int theFirmUserId = getIntValue(val [ 0 ]);

			//get effective date
			val [ 0 ] = efb.getFieldValue("txAUserEffectiveYear");
			val [ 1 ] = efb.getFieldValue("cbAUserEffectiveMonth");
			val [ 2 ] = efb.getFieldValue("txAUserEffectiveDay");
			Date theEffDate = getDateFromFields(val);

			// Update/Create SourceFirm Association
			theSFP.updateFirmUserAssoc(theFirmUserId, theEffDate);
		}
		else
		{
			// Remove all Associations
			theSFP.deleteFirmUserAssoc();
		}

    //=================================================
    //--> CRF#37 :: Source/Firm to Group routing for TD
    //=================================================
    //--> Added new fields to handle Source to Group association
    //--> By Billy 19Aug2003
    // Check for Update/Create/Delect SourceGroup Association
		val [ 0 ] = efb.getFieldValue("cbAssociatedGroup");
		// Check if SourceGroup selected
		if (val [ 0 ] != null && ! val [ 0 ].trim().equals(""))
		{
			int theFirmGroupId = getIntValue(val [ 0 ]);
			//get effective date
			val [ 0 ] = efb.getFieldValue("txAGroupEffectiveYear");
			val [ 1 ] = efb.getFieldValue("cbAGroupEffectiveMonth");
			val [ 2 ] = efb.getFieldValue("txAGroupEffectiveDay");
			Date theEffDate = getDateFromFields(val);
			// Update/Create FirmGroup Association
			theSFP.updateFirmGroupAssoc(theFirmGroupId, theEffDate);
		}
		else
		{
			// Remove all Associations
			theSFP.deleteFirmGroupAssoc();
		}
    //================================================

		theSFP.ejbStore();

	}


	/**
	 *
	 *
	 */
	public void handleCustomActMessageOk(String[] args)
	{
		if (args [ 0 ].equals(SF_OK_TO_UNASSOCIATEUSER))
		{
			handleUserUnassociate(true);
			return;
		}
    //=================================================
    //--> CRF#37 :: Source/Firm to Group routing for TD
    //=================================================
    //--> Added new fields to handle Firm to Group association
    //--> By Billy 19Aug2003
    else if (args [ 0 ].equals(SF_OK_TO_UNASSOCIATEGROUP))
		{
			handleGroupUnassociate(true);
			return;
		}
    //===============================================

	}


	//////////////// Helper functions /////////////////////

	public int getTermsInMonths(String[] terms)
	{
		int years = getIntValue(terms [ 0 ]);

		int months = getIntValue(terms [ 1 ]);

		return((years * 12) + months);

	}


	/**
	 *
	 *
	 */
	public Date getDateFromFields(String[] inDate)
	{

		// Check if date not set then return current Date
		if (inDate [ 0 ] == null || inDate [ 0 ].trim().equals("") || inDate [ 1 ] == null || inDate [ 1 ].trim().equals("") || inDate [ 1 ].trim().equals("0") || inDate [ 2 ] == null || inDate [ 2 ].trim().equals(""))
		{
			return(new Date());
		}
		else
		{
			int year = getIntValue(inDate [ 0 ]);
			int month = getIntValue(inDate [ 1 ]);
			int date = getIntValue(inDate [ 2 ]);
			Calendar cDate = new GregorianCalendar(year, month - 1, date);
			return(cDate.getTime());
		}
	}

  //=================================================
  //--> CRF#37 :: Source/Firm to Group routing for TD
  //=================================================
  //--> New Methods to handle SourceFrim to Group association
  //--> By Billy 20Aug2003
  public void handleGroupUnassociate(boolean confirmed)
	{
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		SessionResourceKit srk = getSessionResourceKit();

		if (!confirmed)
		{
			// Validation Errors found -- set to display Alert Messages
			/**
         	 * BEGIN FIX FOR FXP20974
         	 * Mohan V Premkumar
         	 * 03/31/2008
         	*/
			setActiveMessageToAlert(BXResources.getSysMsg("SOURCE_FIRM_UNASSOC_GROUP_CONFIRM", theSessionState.getLanguageId()),
			/**
         	 * END FIX FOR FXP20974
          	 */
          ActiveMsgFactory.ISCUSTOMDIALOG, SF_OK_TO_UNASSOCIATEGROUP);
			return;
		}

		try
		{
			srk.beginTransaction();
			SourceFirmProfile theSFP = new SourceFirmProfile(srk);
			String SFProfileId =(String) pst.get(SFIRM_ID_PASSED_PARM);
			theSFP = theSFP.findByPrimaryKey(new SourceFirmProfilePK(getIntValue(SFProfileId)));
			theSFP.deleteFirmGroupAssoc();
			srk.commitTransaction();

			// reset Associated User DisplayFields
			EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(SF_EDIT_BUFFER);
			efb.setFieldValue("cbAssociatedGroup", new String(""));
			efb.setFieldValue("cbAGroupEffectiveMonth", new String("0"));
			efb.setFieldValue("txAGroupEffectiveDay", new String(""));
			efb.setFieldValue("txAGroupEffectiveYear", new String(""));
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Exception @SourceFirmEditHandler.handleGroupUnassociate:");
			logger.error(e);
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
		}
	}
  //=====================================================

  //---------------- FXLink Phase II ------------------//
  //--> New method to check if SystemCode editable
  //--> By Billy 14Nov2003
  public boolean isSystemCodeEditable()
  {
    return(!(SourceOfBusinessProfile.getChannelMedia(srk,getPageSystemTypeId()).equals("E")));
  }

  //---------------- FXLink Phase II ------------------//
  //--> New method to check if SystemCode editable
  //--> By Billy 14Nov2003
  public boolean isBusIdEditable()
  {
    // Skip if generatesourcefirmrefnum = Y
    if((PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),"com.basis100.system.generatesourcefirmrefnum", "Y")).equals("Y"))
      return false;
    else
      return true;
  }

  //---------------- FXLink Phase II ------------------//
  //--> New method to get the current System Type Id
  //--> By Billy 14Nov2003
  public int getPageSystemTypeId()
  {
    PageEntry pg = theSessionState.getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    return getIntValue((String) pst.get(SYSTYPE_ID_PASSED_PARM));
  }

  //---------------- FXLink Phase II ------------------//
  //--> Method to set fields disable
  //--> By Billy 11Nov2003
  private void setupDisabledFields(ViewBean thePage, EditFieldsBuffer efb)
	{
   	HtmlDisplayFieldBase df;

    //Set SourceSystemCode and Firm disable if not editable
    if(!isSystemCodeEditable())
    {
      df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbSystemCode");
      df.setExtraHtml(df.getExtraHtml() + " disabled");
      // Preserve the field from it's original value
      efb.setFieldValue("tbSystemCode", efb.getOriginalValue("tbSystemCode"));
    }

    // Set BusinessId filed disable if not editable
    if(!isBusIdEditable())
    {
      df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbClientNum");
      df.setExtraHtml(df.getExtraHtml() + " disabled");
      // Preserve the field from it's original value
      efb.setFieldValue("tbClientNum", efb.getOriginalValue("tbClientNum"));
    }
  }

}


// EditBuffer for SourceFirm Edit

class EditSFBuffer extends EditFieldsBuffer
	implements Serializable
{
	private static String[][] buf={{"TextBox", "tbShortName", "dfSBPShortName"},
                                {"ComboBox", "cbStatus", "dfSBPStatusId"},
                                //---------------- FXLink Phase II ------------------//
                                //--> Change SystemType from ComboBox to StaticText
                                //--> By Billy 14Nov2003
                                {"StaticText", "stSystemType", "dfSFPSystemTypeDesc"},
                                //=====================================================
                                {"TextBox", "tbClientNum", "dfSBPClientId"},
                                {"TextBox", "tbAlternativeId", "dfSBPAlternativeId"},
                                {"TextBox", "tbFirmName", "dfSBPFirmName"},
                                {"TextBox", "tbLicenseNumber", "dfLicenseNumber"},
                                {"TextBox", "tbContactFirstName", "dfSBPFirstName"},
                                {"TextBox", "tbContactMiddleInitial", "dfSBPMidInit"},
                                {"TextBox", "tbContactLastName", "dfSBPLastName"},
                                {"TextBox", "tbPhoneNumber1", "dfSBPPhone"},
                                {"TextBox", "tbPhoneNumber2", ""},
                                {"TextBox", "tbPhoneNumber3", ""},
                                {"TextBox", "tbPhoneExtension", "dfSBPPhoneExt"},
                                {"TextBox", "tbFaxNumber1", "dfSBPFax"},
                                {"TextBox", "tbFaxNumber2", ""},
                                {"TextBox", "tbFaxNumber3", ""},
                                {"TextBox", "tbEmail", "dfSBPEmail"},
                                {"TextBox", "tbAddressLine1", "dfSBPAddrLine1"},
                                {"TextBox", "tbAddressLine2", "dfSBPAddrLine2"},
                                {"TextBox", "tbCity", "dfSBPCity"},
                                {"ComboBox", "cbProvince", "dfSBPProvinceId"},
                                {"TextBox", "tbPostalCodeFSA", "dfSBP_FSA"},
                                {"TextBox", "tbPostalCodeLDU", "dfSBP_LDU"},
                                {"ComboBox", "cbAssociatedUser", "dfSBPUserProfileId"},
                                {"ComboBox", "cbAUserEffectiveMonth", ""},
                                {"TextBox", "txAUserEffectiveDay", ""},
                                {"TextBox", "txAUserEffectiveYear", ""},
                                //=================================================
                                //--> CRF#37 :: Source/Firm to Group routing for TD
                                //=================================================
                                //--> Added new fields to handle SourceFirm to Group association
                                //--> By Billy 20Aug2003
                                {"ComboBox", "cbAssociatedGroup", "dfSBPGroupProfileId"},
                                {"ComboBox", "cbAGroupEffectiveMonth", ""},
                                {"TextBox", "txAGroupEffectiveDay", ""},
                                {"TextBox", "txAGroupEffectiveYear", ""},
                                //=================================================
                                //---------------- FXLink Phase II ------------------//
                                //--> Add SYSTEMCODE field
                                //--> By Billy 14Nov2003
                                {"TextBox", "tbSystemCode", "dfSFPSystemCode"},
                                //====================================================
                                //--DJ_PREFCOMMETHOD_CR--start--//
                                {"ComboBox", "cbPrefDeliveryMethod", "dfPrefDeliveryMethodId"},
                                //--DJ_PREFCOMMETHOD_CR--end--//
                                };
	private String[][] dataBuffer;


	/**
	 *
	 *
	 */
	public EditSFBuffer(String name, SysLogger logger)
	{
		super(name, logger);
		int len = buf.length;

		setNumberOfFields(len);

		dataBuffer = new String [ len ] [ 2 ];

		for(int i = 0;
		i < len;
		++ i)
		{
			for(int j = 0;
			j < 2;
			j ++) dataBuffer [ i ] [ j ] = "";
		}

	}


	/**
	 *
	 *
	 */
	public String getFieldName(int fldNdx)
	{
		return buf [ fldNdx ] [ 1 ];

	}


	/**
	 *
	 *
	 */
	public String getDOFieldName(int fldNdx)
	{
		return buf [ fldNdx ] [ 2 ];

	}


	/**
	 *
	 *
	 */
	public String getFieldType(int fldNdx)
	{
		return buf [ fldNdx ] [ 0 ];

	}


	/**
	 *
	 *
	 */
	public String getFieldValue(int fldNdx)
	{
		return dataBuffer [ fldNdx ] [ 0 ];

	}


	/**
	 *
	 *
	 */
	public String getOriginalValue(int fldNdx)
	{
		return dataBuffer [ fldNdx ] [ 1 ];

	}


	// --- //

	public void setFieldName(int fldNdx, String value)
	{
		buf [ fldNdx ] [ 1 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setDOFieldName(int fldNdx, String value)
	{
		buf [ fldNdx ] [ 2 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setFieldType(int fldNdx, String value)
	{
		buf [ fldNdx ] [ 0 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setFieldValue(int fldNdx, String value)
	{
		dataBuffer [ fldNdx ] [ 0 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setOriginalValue(int fldNdx, String value)
	{
		dataBuffer [ fldNdx ] [ 1 ] = value;

	}

}

