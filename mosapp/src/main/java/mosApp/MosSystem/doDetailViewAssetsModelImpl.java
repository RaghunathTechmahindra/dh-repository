package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doDetailViewAssetsModelImpl extends QueryModelBase
	implements doDetailViewAssetsModel
{
	/**
	 *
	 *
	 */
	public doDetailViewAssetsModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
		//--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 05Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert Task Status Descriptions
      this.setDfAssetType(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "ASSETTYPE", this.getDfAssetType(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAssetType()
	{
		return (String)getValue(FIELD_DFASSETTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfAssetType(String value)
	{
		setValue(FIELD_DFASSETTYPE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAssetDesc()
	{
		return (String)getValue(FIELD_DFASSETDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfAssetDesc(String value)
	{
		setValue(FIELD_DFASSETDESC,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAssetValue()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFASSETVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfAssetValue(java.math.BigDecimal value)
	{
		setValue(FIELD_DFASSETVALUE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPercentIncludedInNetworth()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPERCENTINCLUDEDINNETWORTH);
	}


	/**
	 *
	 *
	 */
	public void setDfPercentIncludedInNetworth(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPERCENTINCLUDEDINNETWORTH,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerNetWorth()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERNETWORTH);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerNetWorth(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERNETWORTH,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}

    public java.math.BigDecimal getDfInstitutionId() {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
      }

      public void setDfInstitutionId(java.math.BigDecimal value) {
        setValue(FIELD_DFINSTITUTIONID, value);
      }


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 15Nov2002
  public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL ASSET.BORROWERID, ASSETTYPE.ASSETTYPEDESCRIPTION, ASSET.ASSETDESCRIPTION, ASSET.ASSETVALUE, ASSET.PERCENTINNETWORTH, BORROWER.NETWORTH, ASSET.COPYID FROM ASSET, ASSETTYPE, BORROWER  __WHERE__  ";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL ASSET.BORROWERID, to_char(ASSET.ASSETTYPEID) ASSETTYPEID_STR, ASSET.ASSETDESCRIPTION, "+
    "ASSET.ASSETVALUE, ASSET.PERCENTINNETWORTH, BORROWER.NETWORTH, ASSET.COPYID,ASSET.INSTITUTIONPROFILEID, BORROWER.DEALID "+
    " FROM ASSET, BORROWER  "+
    "__WHERE__  ";
	//--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //public static final String MODIFYING_QUERY_TABLE_NAME="ASSET, ASSETTYPE, BORROWER";
  public static final String MODIFYING_QUERY_TABLE_NAME="ASSET, BORROWER";
  //--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
	//public static final String STATIC_WHERE_CRITERIA=" (ASSET.ASSETTYPEID  =  ASSETTYPE.ASSETTYPEID) AND (ASSET.BORROWERID  =  BORROWER.BORROWERID) AND (ASSET.COPYID  =  BORROWER.COPYID)";
	public static final String STATIC_WHERE_CRITERIA=
    " (ASSET.BORROWERID  =  BORROWER.BORROWERID) AND "+
    "(ASSET.COPYID  =  BORROWER.COPYID) AND (ASSET.INSTITUTIONPROFILEID  =  BORROWER.INSTITUTIONPROFILEID)";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="ASSET.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFASSETDESC="ASSET.ASSETDESCRIPTION";
	public static final String COLUMN_DFASSETDESC="ASSETDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFASSETVALUE="ASSET.ASSETVALUE";
	public static final String COLUMN_DFASSETVALUE="ASSETVALUE";
	public static final String QUALIFIED_COLUMN_DFPERCENTINCLUDEDINNETWORTH="ASSET.PERCENTINNETWORTH";
	public static final String COLUMN_DFPERCENTINCLUDEDINNETWORTH="PERCENTINNETWORTH";
	public static final String QUALIFIED_COLUMN_DFBORROWERNETWORTH="BORROWER.NETWORTH";
	public static final String COLUMN_DFBORROWERNETWORTH="NETWORTH";
	public static final String QUALIFIED_COLUMN_DFDEALID="BORROWER.DEALID";
	public static final String COLUMN_DFDEALID="DEALID"; 
	public static final String QUALIFIED_COLUMN_DFCOPYID="ASSET.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";

  //--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFASSETTYPE="ASSETTYPE.ASSETTYPEDESCRIPTION";
	//public static final String COLUMN_DFASSETTYPE="ASSETTYPEDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFASSETTYPE="ASSET.ASSETTYPEID_STR";
	public static final String COLUMN_DFASSETTYPE="ASSETTYPEID_STR";

    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "ASSET.INSTITUTIONPROFILEID";
      public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	//[[SPIDER_EVENTS BEGIN

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFASSETTYPE,
				COLUMN_DFASSETTYPE,
				QUALIFIED_COLUMN_DFASSETTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFASSETDESC,
				COLUMN_DFASSETDESC,
				QUALIFIED_COLUMN_DFASSETDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFASSETVALUE,
				COLUMN_DFASSETVALUE,
				QUALIFIED_COLUMN_DFASSETVALUE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPERCENTINCLUDEDINNETWORTH,
				COLUMN_DFPERCENTINCLUDEDINNETWORTH,
				QUALIFIED_COLUMN_DFPERCENTINCLUDEDINNETWORTH,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERNETWORTH,
				COLUMN_DFBORROWERNETWORTH,
				QUALIFIED_COLUMN_DFBORROWERNETWORTH,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
	
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                  FIELD_DFINSTITUTIONID,
                  COLUMN_DFINSTITUTIONID,
                  QUALIFIED_COLUMN_DFINSTITUTIONID,
                  java.math.BigDecimal.class,
                  false,
                  false,
                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                  "",
                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                  ""));
        
        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                  FIELD_DFINSTITUTIONID,
                  COLUMN_DFINSTITUTIONID,
                  QUALIFIED_COLUMN_DFINSTITUTIONID,
                  java.math.BigDecimal.class,
                  false,
                  false,
                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                  "",
                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                  ""));        
	}

}

