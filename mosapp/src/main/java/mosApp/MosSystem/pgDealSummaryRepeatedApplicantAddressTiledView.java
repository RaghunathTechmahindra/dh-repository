package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgDealSummaryRepeatedApplicantAddressTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealSummaryRepeatedApplicantAddressTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(1);
		setPrimaryModelClass( doDetailViewBorrowerAddressesModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStAPDAddressLine1().setValue(CHILD_STAPDADDRESSLINE1_RESET_VALUE);
		getStAPDAddressLine2().setValue(CHILD_STAPDADDRESSLINE2_RESET_VALUE);
		getStAPDCity().setValue(CHILD_STAPDCITY_RESET_VALUE);
		getStAPDProvince().setValue(CHILD_STAPDPROVINCE_RESET_VALUE);
		getStAPDPostalCode1().setValue(CHILD_STAPDPOSTALCODE1_RESET_VALUE);
		getStAPDPostalCode2().setValue(CHILD_STAPDPOSTALCODE2_RESET_VALUE);
		getStAPDTimeAtResidence().setValue(CHILD_STAPDTIMEATRESIDENCE_RESET_VALUE);
		getStAPDAddressStatus().setValue(CHILD_STAPDADDRESSSTATUS_RESET_VALUE);
		getStAPDResidentialStatus().setValue(CHILD_STAPDRESIDENTIALSTATUS_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STAPDADDRESSLINE1,StaticTextField.class);
		registerChild(CHILD_STAPDADDRESSLINE2,StaticTextField.class);
		registerChild(CHILD_STAPDCITY,StaticTextField.class);
		registerChild(CHILD_STAPDPROVINCE,StaticTextField.class);
		registerChild(CHILD_STAPDPOSTALCODE1,StaticTextField.class);
		registerChild(CHILD_STAPDPOSTALCODE2,StaticTextField.class);
		registerChild(CHILD_STAPDTIMEATRESIDENCE,StaticTextField.class);
		registerChild(CHILD_STAPDADDRESSSTATUS,StaticTextField.class);
		registerChild(CHILD_STAPDRESIDENTIALSTATUS,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDetailViewBorrowerAddressesModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		// The following code block was migrated from the RepeatedApplicantAddress_onBeforeDisplayEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.setupNumOfRepeatedApplicantAddress(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}

		return movedToRow;

	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STAPDADDRESSLINE1))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowerAddressesModel(),
				CHILD_STAPDADDRESSLINE1,
				doDetailViewBorrowerAddressesModel.FIELD_DFADDRESSLINE1,
				CHILD_STAPDADDRESSLINE1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDADDRESSLINE2))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowerAddressesModel(),
				CHILD_STAPDADDRESSLINE2,
				doDetailViewBorrowerAddressesModel.FIELD_DFADDRESSLINE2,
				CHILD_STAPDADDRESSLINE2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDCITY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowerAddressesModel(),
				CHILD_STAPDCITY,
				doDetailViewBorrowerAddressesModel.FIELD_DFCITY,
				CHILD_STAPDCITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDPROVINCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowerAddressesModel(),
				CHILD_STAPDPROVINCE,
				doDetailViewBorrowerAddressesModel.FIELD_DFPROVINCENAME,
				CHILD_STAPDPROVINCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDPOSTALCODE1))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowerAddressesModel(),
				CHILD_STAPDPOSTALCODE1,
				doDetailViewBorrowerAddressesModel.FIELD_DFPOSTALFSA,
				CHILD_STAPDPOSTALCODE1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDPOSTALCODE2))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowerAddressesModel(),
				CHILD_STAPDPOSTALCODE2,
				doDetailViewBorrowerAddressesModel.FIELD_DFPOSTALLDU,
				CHILD_STAPDPOSTALCODE2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTIMEATRESIDENCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowerAddressesModel(),
				CHILD_STAPDTIMEATRESIDENCE,
				doDetailViewBorrowerAddressesModel.FIELD_DFMONTHSATADDRESS,
				CHILD_STAPDTIMEATRESIDENCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDADDRESSSTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowerAddressesModel(),
				CHILD_STAPDADDRESSSTATUS,
				doDetailViewBorrowerAddressesModel.FIELD_DFADDRESSSTATUS,
				CHILD_STAPDADDRESSSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDRESIDENTIALSTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowerAddressesModel(),
				CHILD_STAPDRESIDENTIALSTATUS,
				doDetailViewBorrowerAddressesModel.FIELD_DFRESIDENTIALSTATUS,
				CHILD_STAPDRESIDENTIALSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDAddressLine1()
	{
		return (StaticTextField)getChild(CHILD_STAPDADDRESSLINE1);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDAddressLine2()
	{
		return (StaticTextField)getChild(CHILD_STAPDADDRESSLINE2);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDCity()
	{
		return (StaticTextField)getChild(CHILD_STAPDCITY);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDProvince()
	{
		return (StaticTextField)getChild(CHILD_STAPDPROVINCE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDPostalCode1()
	{
		return (StaticTextField)getChild(CHILD_STAPDPOSTALCODE1);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDPostalCode2()
	{
		return (StaticTextField)getChild(CHILD_STAPDPOSTALCODE2);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDTimeAtResidence()
	{
		return (StaticTextField)getChild(CHILD_STAPDTIMEATRESIDENCE);
	}


	/**
	 *
	 *
	 */
	public String endStAPDTimeAtResidenceDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stAPDTimeAtResidence_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setMonthsToYearsMonths(event.getContent());
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDAddressStatus()
	{
		return (StaticTextField)getChild(CHILD_STAPDADDRESSSTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDResidentialStatus()
	{
		return (StaticTextField)getChild(CHILD_STAPDRESIDENTIALSTATUS);
	}


	/**
	 *
	 *
	 */
	public doDetailViewBorrowerAddressesModel getdoDetailViewBorrowerAddressesModel()
	{
		if (doDetailViewBorrowerAddresses == null)
			doDetailViewBorrowerAddresses = (doDetailViewBorrowerAddressesModel) getModel(doDetailViewBorrowerAddressesModel.class);
		return doDetailViewBorrowerAddresses;
	}


	/**
	 *
	 *
	 */
	public void setdoDetailViewBorrowerAddressesModel(doDetailViewBorrowerAddressesModel model)
	{
			doDetailViewBorrowerAddresses = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STAPDADDRESSLINE1="stAPDAddressLine1";
	public static final String CHILD_STAPDADDRESSLINE1_RESET_VALUE="";
	public static final String CHILD_STAPDADDRESSLINE2="stAPDAddressLine2";
	public static final String CHILD_STAPDADDRESSLINE2_RESET_VALUE="";
	public static final String CHILD_STAPDCITY="stAPDCity";
	public static final String CHILD_STAPDCITY_RESET_VALUE="";
	public static final String CHILD_STAPDPROVINCE="stAPDProvince";
	public static final String CHILD_STAPDPROVINCE_RESET_VALUE="";
	public static final String CHILD_STAPDPOSTALCODE1="stAPDPostalCode1";
	public static final String CHILD_STAPDPOSTALCODE1_RESET_VALUE="";
	public static final String CHILD_STAPDPOSTALCODE2="stAPDPostalCode2";
	public static final String CHILD_STAPDPOSTALCODE2_RESET_VALUE="";
	public static final String CHILD_STAPDTIMEATRESIDENCE="stAPDTimeAtResidence";
	public static final String CHILD_STAPDTIMEATRESIDENCE_RESET_VALUE="";
	public static final String CHILD_STAPDADDRESSSTATUS="stAPDAddressStatus";
	public static final String CHILD_STAPDADDRESSSTATUS_RESET_VALUE="";
	public static final String CHILD_STAPDRESIDENTIALSTATUS="stAPDResidentialStatus";
	public static final String CHILD_STAPDRESIDENTIALSTATUS_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDetailViewBorrowerAddressesModel doDetailViewBorrowerAddresses=null;
  private DealSummaryHandler handler=new DealSummaryHandler();
}

