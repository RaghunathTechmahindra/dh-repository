package mosApp.MosSystem;

import java.sql.ResultSet;
import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 * @version 1.3  <br>
 * Date: 07/28/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *	Modified to add field for affiliation program Id<br>
 *	- added declaration for getter/setter for affiliation program id<br>
 *
 *	@Version 1.4 <br>
 *	Date: 06/09/2008
 *	Author: MCM Impl Team <br>
 *	Change Log:  <br>
 *	XS_2.7 -- Modified to add field for RefiAdditionalInformation<br>
 *		- added declaration for getter/setter for RefiAdditionalInformation<br>
 *	
 */
public interface doDealEntryMainSelectModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value);

    /**
    *
    *
    */
   public java.math.BigDecimal getDfInstitutionId();

    /**
     *
     *
     */
    public void setDfInstitutionId(java.math.BigDecimal value);

    /**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealPurposeId();


	/**
	 *
	 *
	 */
	public void setDfDealPurposeId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealTypeId();


	/**
	 *
	 *
	 */
	public void setDfDealTypeId(java.math.BigDecimal value);

	// SEAN GECF IV Document Type drop down: getter and seeter for dfDocumentTypeId.
	// IV stands for Income Verification
	public java.math.BigDecimal getDfIVDocumentTypeId();
	public void setDfIVDocumentTypeId(java.math.BigDecimal value);
	// END GECF IV Document Type drop down

	// SEAN DJ SPEC-Progress Advance Type July 22, 2005: the getter and setter
	// methods.
	public java.math.BigDecimal getDfProgressAdvanceTypeId();
	public void setDfProgressAdvanceTypeId(java.math.BigDecimal value);
	// SEAN DJ SPEC-PAT END

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLOBId();


	/**
	 *
	 *
	 */
	public void setDfLOBId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLenderProfile();


	/**
	 *
	 *
	 */
	public void setDfLenderProfile(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfProductId();


	/**
	 *
	 *
	 */
	public void setDfProductId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfPaymentTermDesc();


	/**
	 *
	 *
	 */
	public void setDfPaymentTermDesc(String value);


	/**
	 *
	 *
	 */
	public String getDfRateLockedIn();


	/**
	 *
	 *
	 */
	public void setDfRateLockedIn(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfChargeId();


	/**
	 *
	 *
	 */
	public void setDfChargeId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfExistingLoanAmount();


	/**
	 *
	 *
	 */
	public void setDfExistingLoanAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPostedInterestRate();


	/**
	 *
	 *
	 */
	public void setDfPostedInterestRate(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDiscount();


	/**
	 *
	 *
	 */
	public void setDfDiscount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPremium();


	/**
	 *
	 *
	 */
	public void setDfPremium(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBuyDownRate();


	/**
	 *
	 *
	 */
	public void setDfBuyDownRate(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNetRate();


	/**
	 *
	 *
	 */
	public void setDfNetRate(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRequestedLoanAmount();


	/**
	 *
	 *
	 */
	public void setDfRequestedLoanAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPaymentFrequencyId();


	/**
	 *
	 *
	 */
	public void setDfPaymentFrequencyId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAmortization();


	/**
	 *
	 *
	 */
	public void setDfAmortization(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEffectiveAmortization();


	/**
	 *
	 *
	 */
	public void setDfEffectiveAmortization(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAdditionalPrincipalPayment();


	/**
	 *
	 *
	 */
	public void setDfAdditionalPrincipalPayment(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfActualPaymentTerm();


	/**
	 *
	 *
	 */
	public void setDfActualPaymentTerm(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPAndIPayment();


	/**
	 *
	 *
	 */
	public void setDfPAndIPayment(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalEscrowPayment();


	/**
	 *
	 *
	 */
	public void setDfTotalEscrowPayment(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalPaymentAmount();


	/**
	 *
	 *
	 */
	public void setDfTotalPaymentAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRepaymentId();


	/**
	 *
	 *
	 */
	public void setDfRepaymentId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfEstimatedClosingDate();


	/**
	 *
	 *
	 */
	public void setDfEstimatedClosingDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfFirstPaymentDate();


	/**
	 *
	 *
	 */
	public void setDfFirstPaymentDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfMaturityDate();


	/**
	 *
	 *
	 */
	public void setDfMaturityDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public String getDfSecondaryFinancing();


	/**
	 *
	 *
	 */
	public void setDfSecondaryFinancing(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPrePaymentOptionsId();


	/**
	 *
	 *
	 */
	public void setDfPrePaymentOptionsId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPrevilagePaymentId();


	/**
	 *
	 *
	 */
	public void setDfPrevilagePaymentId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfFinancingProgramId();


	/**
	 *
	 *
	 */
	public void setDfFinancingProgramId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSpedcialFeatureId();


	/**
	 *
	 *
	 */
	public void setDfSpedcialFeatureId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBranchId();


	/**
	 *
	 *
	 */
	public void setDfBranchId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfReferenceExisitingMTG();


	/**
	 *
	 *
	 */
	public void setDfReferenceExisitingMTG(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCrossSellId();


	/**
	 *
	 *
	 */
	public void setDfCrossSellId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIIndicator();


	/**
	 *
	 *
	 */
	public void setDfMIIndicator(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMITypeId();


	/**
	 *
	 *
	 */
	public void setDfMITypeId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIInsurerId();


	/**
	 *
	 *
	 */
	public void setDfMIInsurerId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIPayorId();


	/**
	 *
	 *
	 */
	public void setDfMIPayorId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIPremium();


	/**
	 *
	 *
	 */
	public void setDfMIPremium(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfMICertificateNo();

  //===============================================================
  // New fields to handle MIPolicyNum problem :
  //  the number lost if user cancel MI and re-send again later.
  // -- Modified by Billy 16July2003
	public String getDfMIPolicyNumberCMHC();
  public String getDfMIPolicyNumberGE();
  //===============================================================

	/**
	 *
	 *
	 */
	public void setDfMICertificateNo(String value);

  //===============================================================
  // New fields to handle MIPolicyNum problem :
  //  the number lost if user cancel MI and re-send again later.
  // -- Modified by Billy 16July2003
  public void setDfMIPolicyNumberCMHC(String value);
  public void setDfMIPolicyNumberGE(String value);
  //===============================================================

	/**
	 *
	 *
	 */
	public String getDfMIUpFrontId();


	/**
	 *
	 *
	 */
	public void setDfMIUpFrontId(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDownPaymentAmount();


	/**
	 *
	 *
	 */
	public void setDfDownPaymentAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTaxPayorId();


	/**
	 *
	 *
	 */
	public void setDfTaxPayorId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAdvanceHold();


	/**
	 *
	 *
	 */
	public void setDfAdvanceHold(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfReturnDate();


	/**
	 *
	 *
	 */
	public void setDfReturnDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public String getDfRefSourceAppId();


	/**
	 *
	 *
	 */
	public void setDfRefSourceAppId(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPaPurchase();


	/**
	 *
	 *
	 */
	public void setDfPaPurchase(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfRateDate();


	/**
	 *
	 *
	 */
	public void setDfRateDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRequiredDownpayment();


	/**
	 *
	 *
	 */
	public void setDfRequiredDownpayment(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfExistingPolicyNumber();


	/**
	 *
	 *
	 */
	public void setDfExistingPolicyNumber(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCommitmentPeriod();


	/**
	 *
	 *
	 */
	public void setDfCommitmentPeriod(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfCommitmentExpirationDate();
    

	/**
	 *
	 *
	 */
	public void setDfCommitmentExpirationDate(java.sql.Timestamp value);

    /**
    *
    *
    */
    public java.sql.Timestamp getDfFinancingWaiverDate();
    
    /**
     *
     *
     */
    public void setDfFinancingWaiverDate(java.sql.Timestamp value);

	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfCommitmentAcceptDate();


	/**
	 *
	 *
	 */
	public void setDfCommitmentAcceptDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedGDS();


	/**
	 *
	 *
	 */
	public void setDfCombinedGDS(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedTDS();


	/**
	 *
	 *
	 */
	public void setDfCombinedTDS(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedGDS3Year();


	/**
	 *
	 *
	 */
	public void setDfCombinedGDS3Year(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedTDS3Year();


	/**
	 *
	 *
	 */
	public void setDfCombinedTDS3Year(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedGDSBorrower();


	/**
	 *
	 *
	 */
	public void setDfCombinedGDSBorrower(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedTDSBorower();


	/**
	 *
	 *
	 */
	public void setDfCombinedTDSBorower(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfProgressAdvance();


	/**
	 *
	 *
	 */
	public void setDfProgressAdvance(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfADvanceToDateAmount();


	/**
	 *
	 *
	 */
	public void setDfADvanceToDateAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAdvanceNumber();


	/**
	 *
	 *
	 */
	public void setDfAdvanceNumber(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfMIRUIntervention();


	/**
	 *
	 *
	 */
	public void setDfMIRUIntervention(String value);


	/**
	 *
	 *
	 */
	public String getDfPreQualificationMICertNum();


	/**
	 *
	 *
	 */
	public void setDfPreQualificationMICertNum(String value);


	/**
	 *
	 *
	 */
	public String getDfRefiPurpose();


	/**
	 *
	 *
	 */
	public void setDfRefiPurpose(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRefiOrigPurchasePrice();


	/**
	 *
	 *
	 */
	public void setDfRefiOrigPurchasePrice(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfRefiBlendedAmortization();


	/**
	 *
	 *
	 */
	public void setDfRefiBlendedAmortization(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRefiOrigMtgAmount();


	/**
	 *
	 *
	 */
	public void setDfRefiOrigMtgAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfRefiImprovementsDesc();


	/**
	 *
	 *
	 */
	public void setDfRefiImprovementsDesc(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRefiImporvementAmount();


	/**
	 *
	 *
	 */
	public void setDfRefiImporvementAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNextAdvanceAmount();


	/**
	 *
	 *
	 */
	public void setDfNextAdvanceAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfRefiOrigPurchaseDate();


	/**
	 *
	 *
	 */
	public void setDfRefiOrigPurchaseDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public String getDfRefiCurMortgageHolder();


	/**
	 *
	 *
	 */
	public void setDfRefiCurMortgageHolder(String value);

  //-- FXLink Phase II --//
  //--> New Field for Market Type Indicator
  //--> By Billy 18Nov2003
  public String getDfMccMarketType();
	public void setDfMccMarketType(String value);
  //=======================================

  //--DJ_LDI_CR--start--//
	/**
	 *
	 *
	 */
	public void setDfPmntPlusLifeDisability(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfPmntPlusLifeDisability();
  //--DJ_LDI_CR--end--//

  //--DJ_CR010--start//
	/**
	 *
	 *
	 */
	public String getDfCommisionCode();

	/**
	 *
	 *
	 */
	public void setDfCommisionCode(String value);
  //--DJ_CR010--end//

  //--DJ_CR203.1--start//
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfProprietairePlusLOC();

	/**
	 *
	 *
	 */
	public void setDfProprietairePlusLOC(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public String getDfMultiProject();

	/**
	 *
	 *
	 */
	public void setDfMultiProject(String value);

	/**
	 *
	 *
	 */
	public String getDfProprietairePlus();

	/**
	 *
	 *
	 */
	public void setDfProprietairePlus(String value);
  //--DJ_CR203.1--end//

  //***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//
  // Add new RateGuaranteePeriod field
	/**
	 * 
	 */	
	public String getDfRateGuaranteePeriod();
	
	/**
	 * 
	 */	
	public void setDfRateGuaranteePeriod(String value);
  //***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//
	
  //***** Change by NBC/PP Implementation Team - Version 1.2 - Start *****//
	/**
	 * 
	 */	
	public String getDfFloatDownCompletedFlag();
	
	/**
	 * 
	 */	
	public void setDfFloatDownCompletedFlag(String value);
	
	/**
	 * 
	 */	
	public String getDfFloatDownTaskCreatedFlag();
	
	/**
	 * 
	 */	
	public void setDfFloatDownTaskCreatedFlag(String value);	
   //***** Change by NBC/PP Implementation Team - Version 1.2 - End *****//

	/**
	 *
	 *
	 */
	public void setResultSet(ResultSet rs);

	//    ***** Change by NBC Impl. Team - Version 1.3 - Start *****//
	
	public void setDfAffiliationProgramId(java.math.BigDecimal value);
	
	public java.math.BigDecimal getDfAffiliationProgramId();
	
   //    ***** Change by NBC Impl. Team - Version 1.3 - End *****//

//--Release3.1--begins
  //--by Hiro Apr 13, 2006
  public java.math.BigDecimal getDfProductTypeId();
  public void setDfProductTypeId(java.math.BigDecimal value);

  public java.math.BigDecimal getDfRefiProductTypeId();
  public void setDfRefiProductTypeId(java.math.BigDecimal value);

  public String getDfProgressAdvanceInspectionBy();
  public void setDfProgressAdvanceInspectionBy(String value);
  
  public String getDfSelfDirectedRRSP();
  public void setDfSelfDirectedRRSP(String value);
  
  public String getDfCmhcProductTrackerIdentifier();
  public void setDfCmhcProductTrackerIdentifier(String value);
  
  public java.math.BigDecimal getDfLOCRepaymentTypeId();
  public void setDfLOCRepaymentTypeId(java.math.BigDecimal value);
  
  public String getDfRequestStandardService();
  public void setDfRequestStandardService(String value);

  public java.math.BigDecimal getDfLOCAmortizationMonths();
  public void setDfLOCAmortizationMonths(java.math.BigDecimal value);

  public java.sql.Timestamp getDfLOCInterestOnlyMaturityDate();
  public void setDfLOCInterestOnlyMaturityDate(java.sql.Timestamp value);


//CR03
  public String getDfAutoCalCommitDate();

  public void setDfAutoCalCommitDate(String value);
//CR03
  // --Release3.1--ends
  
  /***************MCM Impl team changes starts - XS_2.7 *******************/  
	/**
	 * This method returns DfRefiAdditionalInformation value
	 *
	 * @param None <br>
	 *  
	 * @return String : the result of getDfRefiAdditionalInformation <br>
	 */
	public String getDfRefiAdditionalInformation();

	/**
	 * This method sets value for DfRefiAdditionalInformation
	 *
	 * @param String <br>
	 *  
	 */
	public void setDfRefiAdditionalInformation(String value);

	/***************MCM Impl team changes ends - XS_2.7 *********************/
	
    //***** Qualify Rate *****//
    public String getDfQualifyingOverrideFlag();
    
    public void setDfQualifyingOverrideFlag(String value);
    
    
    public java.math.BigDecimal getDfQualifyRate();
    
    public void setDfQualifyRate(java.math.BigDecimal value);
    
    
    public String getDfOverrideQualProd();
    
    public void setDfOverrideQualProd(String value);
    
    public String getDfQualifyingOverrideRateFlag();
    
    public void setDfQualifyingOverrideRateFlag(String value);
    //  ***** Qualify Rate *****//
    
    // Ticket 267
    public void setDfDealStatusId(String value);
    public String getDfDealStatusId();
    
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDEALID="dfDealId";
    public static final String FIELD_DFCOPYID="dfCopyId";
    public static final String FIELD_DFINSTITUTIONID="dfInstitutionId";
	public static final String FIELD_DFDEALPURPOSEID="dfDealPurposeId";
	public static final String FIELD_DFDEALTYPEID="dfDealTypeId";
	public static final String FIELD_DFLOBID="dfLOBId";
	public static final String FIELD_DFLENDERPROFILE="dfLenderProfile";
	public static final String FIELD_DFPRODUCTID="dfProductId";
	public static final String FIELD_DFPAYMENTTERMDESC="dfPaymentTermDesc";
	public static final String FIELD_DFRATELOCKEDIN="dfRateLockedIn";
	public static final String FIELD_DFCHARGEID="dfChargeId";
	public static final String FIELD_DFEXISTINGLOANAMOUNT="dfExistingLoanAmount";
	public static final String FIELD_DFPOSTEDINTERESTRATE="dfPostedInterestRate";
	public static final String FIELD_DFDISCOUNT="dfDiscount";
	public static final String FIELD_DFPREMIUM="dfPremium";
	public static final String FIELD_DFBUYDOWNRATE="dfBuyDownRate";
	public static final String FIELD_DFNETRATE="dfNetRate";
	public static final String FIELD_DFREQUESTEDLOANAMOUNT="dfRequestedLoanAmount";
	public static final String FIELD_DFPAYMENTFREQUENCYID="dfPaymentFrequencyId";
	public static final String FIELD_DFAMORTIZATION="dfAmortization";
	public static final String FIELD_DFEFFECTIVEAMORTIZATION="dfEffectiveAmortization";
	public static final String FIELD_DFADDITIONALPRINCIPALPAYMENT="dfAdditionalPrincipalPayment";
	public static final String FIELD_DFACTUALPAYMENTTERM="dfActualPaymentTerm";
	public static final String FIELD_DFPANDIPAYMENT="dfPAndIPayment";
	public static final String FIELD_DFTOTALESCROWPAYMENT="dfTotalEscrowPayment";
	public static final String FIELD_DFTOTALPAYMENTAMOUNT="dfTotalPaymentAmount";
	public static final String FIELD_DFREPAYMENTID="dfRepaymentId";
	public static final String FIELD_DFESTIMATEDCLOSINGDATE="dfEstimatedClosingDate";
	public static final String FIELD_DFFIRSTPAYMENTDATE="dfFirstPaymentDate";
	public static final String FIELD_DFMATURITYDATE="dfMaturityDate";
	public static final String FIELD_DFSECONDARYFINANCING="dfSecondaryFinancing";
	public static final String FIELD_DFPREPAYMENTOPTIONSID="dfPrePaymentOptionsId";
	public static final String FIELD_DFPREVILAGEPAYMENTID="dfPrevilagePaymentId";
	public static final String FIELD_DFFINANCINGPROGRAMID="dfFinancingProgramId";
	public static final String FIELD_DFSPEDCIALFEATUREID="dfSpedcialFeatureId";
	public static final String FIELD_DFBRANCHID="dfBranchId";
	public static final String FIELD_DFREFERENCEEXISITINGMTG="dfReferenceExisitingMTG";
	public static final String FIELD_DFCROSSSELLID="dfCrossSellId";
	public static final String FIELD_DFMIINDICATOR="dfMIIndicator";
	public static final String FIELD_DFMITYPEID="dfMITypeId";
	public static final String FIELD_DFMIINSURERID="dfMIInsurerId";
	public static final String FIELD_DFMIPAYORID="dfMIPayorId";
	public static final String FIELD_DFMIPREMIUM="dfMIPremium";
	public static final String FIELD_DFMICERTIFICATENO="dfMICertificateNo";
	public static final String FIELD_DFMIUPFRONTID="dfMIUpFrontId";
	public static final String FIELD_DFDOWNPAYMENTAMOUNT="dfDownPaymentAmount";
	public static final String FIELD_DFTAXPAYORID="dfTaxPayorId";
	public static final String FIELD_DFADVANCEHOLD="dfAdvanceHold";
	public static final String FIELD_DFRETURNDATE="dfReturnDate";
	public static final String FIELD_DFREFSOURCEAPPID="dfRefSourceAppId";
	public static final String FIELD_DFPAPURCHASE="dfPaPurchase";
	public static final String FIELD_DFRATEDATE="dfRateDate";
	public static final String FIELD_DFREQUIREDDOWNPAYMENT="dfRequiredDownpayment";
	public static final String FIELD_DFEXISTINGPOLICYNUMBER="dfExistingPolicyNumber";
	public static final String FIELD_DFCOMMITMENTPERIOD="dfCommitmentPeriod";
	public static final String FIELD_DFCOMMITMENTEXPIRATIONDATE="dfCommitmentExpirationDate";
    public static final String FIELD_DFFINANCINGWAIVERDATE="dfFinancingWaiverDate";
	public static final String FIELD_DFCOMMITMENTACCEPTDATE="dfCommitmentAcceptDate";
	public static final String FIELD_DFCOMBINEDGDS="dfCombinedGDS";
	public static final String FIELD_DFCOMBINEDTDS="dfCombinedTDS";
	public static final String FIELD_DFCOMBINEDGDS3YEAR="dfCombinedGDS3Year";
	public static final String FIELD_DFCOMBINEDTDS3YEAR="dfCombinedTDS3Year";
	public static final String FIELD_DFCOMBINEDGDSBORROWER="dfCombinedGDSBorrower";
	public static final String FIELD_DFCOMBINEDTDSBOROWER="dfCombinedTDSBorower";
	public static final String FIELD_DFPROGRESSADVANCE="dfProgressAdvance";
	public static final String FIELD_DFADVANCETODATEAMOUNT="dfADvanceToDateAmount";
	public static final String FIELD_DFADVANCENUMBER="dfAdvanceNumber";
	public static final String FIELD_DFMIRUINTERVENTION="dfMIRUIntervention";
	public static final String FIELD_DFPREQUALIFICATIONMICERTNUM="dfPreQualificationMICertNum";
	public static final String FIELD_DFREFIPURPOSE="dfRefiPurpose";
	public static final String FIELD_DFREFIORIGPURCHASEPRICE="dfRefiOrigPurchasePrice";
	public static final String FIELD_DFREFIBLENDEDAMORTIZATION="dfRefiBlendedAmortization";
	public static final String FIELD_DFREFIORIGMTGAMOUNT="dfRefiOrigMtgAmount";
	public static final String FIELD_DFREFIIMPROVEMENTSDESC="dfRefiImprovementsDesc";
	public static final String FIELD_DFREFIIMPORVEMENTAMOUNT="dfRefiImporvementAmount";
	public static final String FIELD_DFNEXTADVANCEAMOUNT="dfNextAdvanceAmount";
	public static final String FIELD_DFREFIORIGPURCHASEDATE="dfRefiOrigPurchaseDate";
	public static final String FIELD_DFREFICURMORTGAGEHOLDER="dfRefiCurMortgageHolder";
  //--Release2.1--start//
  public static final String FIELD_DFDEALPURPOSELABEL="dfDealPurposeLabel";
  //===============================================================
  // New fields to handle MIPolicyNum problem :
  //  the number lost if user cancel MI and re-send again later.
  // -- Modified by Billy 16July2003
  public static final String FIELD_DFMIPOLICYNUMBERCMHC="dfMIPolicyNumberCMHC";
  public static final String FIELD_DFMIPOLICYNUMBERGE="dfMIPolicyNumberGE";
  //===============================================================
  //=============================================================================
  //-- FXLink Phase II --//
  //--> New Field for Market Type Indicator
  //--> By Billy 18Nov2003
  public static final String FIELD_DFMCCMARKETTYPE="dfMccMarketType";
  //==============================================================
  //--DJ_LDI_CR--start--//
  public static final String FIELD_DFPMNTPLUSLIFEDISABILITY="dfPmntPlusLifeDisability";
  //--DJ_LDI_CR--end--//

  //--DJ_CR010--start//
  public static final String FIELD_DFCOMMISIONCODE="dfCommisionCode";
  //--DJ_CR010--end//

  //--DJ_CR203.1--start//
  public static final String FIELD_DFPROPRIETAIREPLUSLOC="dfProprietairePlusLOC";
  public static final String FIELD_DFPROPRIETAIREPLUS="dfProprietairePlus";
  public static final String FIELD_DFMULTIPROJECT="dfMultiProject";
  //--DJ_CR203.1--end//

	// SEAN GECF IV Document Type drop down: define the field.
	public static final String FIELD_DFIVDOCUMENTTYPEID="dfIVDocumentTypeId";
	// END GECF Document Type drop down

	// SEAN DJ SPEC-Progress Advance Type July 22, 2005: Define the field.
	public static final String FIELD_DFPROGRESSADVANCETYPEID="dfProgressAdvanceTypeId";
	// SEAN DJ SPEC-PAT END

  //--Release3.1--begins
  //--by Hiro Apr 13, 2006
  public static final String FIELD_DFPRODUCTTYPEID = "dfProductTypeId";
  public static final String FIELD_DFREFIPRODUCTTYPEID = "dfRefiProductTypeid";
  public static final String FIELD_DFPROGRESSADVANCEINSPECTIONBY = "dfProgressAdvanceInspectionBy";
  public static final String FIELD_DFSELFDIRECTEDRRSP = "dfSelfDirectedRRSP";
  public static final String FIELD_DFCMHCPRODUCTTRACKERIDENTIFIER = "dfCmhcProductTrackerIdentifier";
  public static final String FIELD_DFLOCREPAYMENTTYPEID = "dfLOCRepaymentTypeId";
  public static final String FIELD_DFREQUESTSTANDARDSERVICE = "dfRequestStandardService";
  public static final String FIELD_DFLOCAMORTIZATIONMONTHS = "dfLOCAmortizationMonths";
  public static final String FIELD_DFLOCINTERESTONLYMATURITYDATE = "dfLOCInterestOnlyMaturityDate";
  //--Release3.1--ends
  
	//***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//
	public static final String FIELD_DFRATEGUARANTEEPERIOD="dfRateGuaranteePeriod";
	//***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//	
	//***** Change by NBC/PP Implementation Team - Version 1.2 - Start *****//
	public static final String FIELD_DFFLOATDOWNCOMPLETEDFLAG ="dfFloatDownCompletedFlag";
	public static final String FIELD_DFFLOATDOWNTASKCREATEDFLAG  ="dfFloatDownTaskCreatedFlag";
	//***** Change by NBC/PP Implementation Team - Version 1.2 - End *****//		
	
//	***** Change by NBC Impl. Team - Version 1.3 - Start*****//
	
	public static final String FIELD_DFAFFILIATIONPROGRAMID = "dfAffiliationProgramId";

//	***** Change by NBC Impl. Team - Version 1.3 - End*****//


    //CR03
	public static final String FIELD_DFAUTOCALCOMMITDATE="dfAutoCalCommitDate";
	
	/***************MCM Impl team changes starts - XS_2.7 *******************/
	
	public static final String FIELD_DFREFIADDITIONALINFORMATION="dfRefiAdditionalInformation";
	
	/***************MCM Impl team changes ends - XS_2.7 *********************/
    //CR03
    
    // ***** Qualifying Rate *****//
    public static final String FIELD_DFQUALIFYINGOVERRIDEFLAG = "dfQualifyingOverrideFlag";
    public static final String FIELD_DFQUALIFYRATE = "dfQualifyRate";
    public static final String FIELD_DFOVERRIDEQUALPROD = "dfOverrideQualProd";
    public static final String FIELD_DFQUALIFYINGOVERRIDERATEFLAG = "dfQualifyingOverrideRateFlag";
    
    // ***** Qualifying Rate *****//
    
    // Ticket 267
	public static final String FIELD_DFDEALSTATUSID = "dfDealStatusId";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

