package mosApp.MosSystem;
/**
 * <p>Title: RateFloatDownHandler</p>
 *
 * <p>Description: handler for rate float down page
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.0
 * @version 1.3 - Added java docs to the following methods
 * cloneSS(),  populatePageDisplayFields(), setupBeforePageGeneration(), handleSubmit()
 *
 * @version 1.4 August 14, 2008 MCM Team (XS 9.3 Added performRateFloatDownForComponents(..) to perform RFD for components)
 * @version 1.5 August 19, 2008 MCM Team (artf764813 modified performRateFloatDownForComponents(..),
 * performRateFloatDownAndAddDealHistory(..) to check if RGP is greater than 0 )
 *
 * FXP23471, 18 November 2008 - Overloaded rfdMsgToLog() to log the product name.
 * Changed performRateFloatDownAndAddDealHistory() to call the new version
 *
 */
import java.util.Collection;
import java.util.Iterator;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.PricingRateInventory;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.mtgrates.RateFloatDown;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;


public class RateFloatDownHandler extends PageHandlerCommon implements
		Cloneable, Sc {
	protected static final String goldCopy = "1";

	/**
	 * cloneSS
	 * This method Creates and returns a copy of this RateFloatDownHandler.
	 *
	 * @return RateFloatDownHandler : the result of cloneSS <br>
	 */
	public RateFloatDownHandler cloneSS() {
		return (RateFloatDownHandler) super.cloneSafeShallow();

	}
	/**
	 * populatePageDisplayFields
	 * This method is used to populate the fields in the header
	 * with the appropriate information
	 *
	 * @param None <br>
	 *
	 * @return void : the result of populatePageDisplayFields <br>
	 */


	public void populatePageDisplayFields() {
		PageEntry pg = theSessionState.getCurrentPage();

		populatePageShellDisplayFields();

		populateTaskNavigator(pg);

		populatePageDealSummarySnapShot();

		populatePreviousPagesLinks();

	}

	/**
	 * setupBeforePageGeneration
	 * This method does page set up to generate the page.
	 * sets the deal summary snap shot on the top of the page
	 *
	 * @param None <br>
	 *
	 * @return void : the result of setupBeforePageGeneration <br>
	 */
	public void setupBeforePageGeneration() {
		PageEntry pg = theSessionState.getCurrentPage();

		try {
			if (pg.getSetupBeforeGenerationCalled() == true)
				return;
			pg.setSetupBeforeGenerationCalled(true);
			setupDealSummarySnapShotDO(pg);


			// Add any custom model binding here
			// or and any field customization logic here
			// Setup cursor parameters here, if necessary
			// ........................................
		} catch (Exception e) {
			logger
					.error("Exception @RateFloatDownHandler.setupBeforePageGeneration()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}

	}

	/**
	 * handleSubmit
	 * This method handles the page events when the page is submitted
	 *
	 * @param None <br>
	 *
	 * @return void : the result of handleSubmit <br>
	 */
    public void handleSubmit()
     {
      PageEntry pg = theSessionState.getCurrentPage();
      SessionResourceKit srk = getSessionResourceKit();
      try {
          srk.beginTransaction();
          standardAdoptTxCopy(pg, true);

         performRateFloatDownAndAddDealHistory(srk, pg, theSessionState.getLanguageId());
         // call workflow to close the existing RFD task
         workflowTrigger(2, srk, pg, null);
         srk.commitTransaction();
         navigateToNextPage(true);
      }
      catch(Exception e)
      {
          logger.error("Exception @RateFloatDownHandler.handleSubmit()");
          logger.error(e);
          setStandardFailMessage();
          return;
      }
     }
    /**
     * <p>
     * Description: Associates rate to components by performing a rate float
     * down and inserts a note to DealHistory.
     * </p>
     * @version 1.0 XS_9.3 Initial Version
     * @version 1.1 FXP23331, Jan 14, 2009, to add Calc for Component level, changed method signature. 
     * @author MCM Impl Team<br>
     * @param srk
     * @param dcm
     * @param deal
     * @param langid
     * @return boolean - if RFD is applied for component level
     */
    private boolean performRateFloatDownForComponents(SessionResourceKit srk,
            CalcMonitor dcm, Deal deal, int langid)
    {
    	boolean isRfdAppliedForComponent = false; //FXP23331, Jan 14, 2009
        try
        {
            // Get all the components for the deal
            Collection components = deal.getComponents();
            Iterator itComponent = components.iterator();

            // For each component
            while (itComponent.hasNext())
            {
                // Extract the component
                Component component = (Component) itComponent.next();
                MtgProd mtgProd = new MtgProd(srk, dcm, component
                        .getMtgProdId());
                int isNoFloatDown = mtgProd.getRateFloatDownTypeId();
                // Perform rate float down for the Mortgage component
                // Checks if the Component is Mortgage Component and product
                // supports RFD
                if (component.getComponentTypeId() == Mc.COMPONENT_TYPE_MORTGAGE
                        && isNoFloatDown != Mc.RATE_FLOAT_DOWN_TYPE_DEFAULT
                        && mtgProd.getRateGuaranteePeriod() > 0)
                {
                    // if component RGP is greater than 0
                    if (component.getComponentMortgage()
                            .getRateGuaranteePeriod() > 0)
                    {
                        // Checking whether the pricingRateInventoryId is null
                        // in component if yes do not perform Rfd.

                        RateFloatDown rfdProcess = new RateFloatDown(srk, dcm,
                                component, deal.getCommitmentIssueDate(),
                                langid);

                        // Perform the Rate floatdown and get the best rate
                        PricingRateInventory bestRate = rfdProcess
                                .performRateFloatDown(component, deal
                                        .getCommitmentIssueDate());
                        boolean isValidRfd = rfdProcess.isValidRate(bestRate
                                .getInternalRatePercentage(), component);

                        // If the best rate is valid
                        if (isValidRfd)
                        {
                            // Check if the best rate is lesser than the posted
                            // rate
                            boolean isRfdIsBest = rfdProcess.isRfdRateBest(
                                    bestRate.getInternalRatePercentage(),
                                    component);

                            if (isRfdIsBest)
                            {
                                // get the prevoius rate for inserting into the
                                // deal history
                                double componentPrevRate = component
                                        .getPostedRate();
                                // Update the component with the Rfd best rate
                                component.setPricingRateInventoryId(bestRate
                                        .getPricingRateInventoryID());
                                component.setPostedRate(bestRate
                                        .getInternalRatePercentage());
                                component.ejbStore();
                                isRfdAppliedForComponent = true;
                                // insert record in deal history

                                String msg = rfdMsgToLogForComponents(
                                        componentPrevRate, bestRate
                                                .getInternalRatePercentage(),
                                        mtgProd.getMtgProdName(), langid);
                                DealHistoryLogger hLog = DealHistoryLogger
                                        .getInstance(srk);
                                hLog.log(deal.getDealId(), deal.getCopyId(),
                                        msg, Mc.DEAL_TX_TYPE_EVENT, srk
                                                .getExpressState()
                                                .getUserProfileId());

                            }
                        }
                        else
                        {
                            setActiveMessageToAlert(BXResources.getSysMsg(
                                    "RATE_FLOAT_DOWN_INVALID_VALUE",
                                    theSessionState.getLanguageId()),
                                    ActiveMsgFactory.ISCUSTOMCONFIRM);
                            return false; // FXP23331, Jan 14, 2009
                        }//end of if(isValidRfd)-else

                    }//end of if(component.getComponentMortgage().getRateGuaranteePeriod() >0)

                }//end of component.getComponentTypeId()==Mc.COMPONENT_TYPE_MORTGAGE)

            }//end of while (itComponent.hasNext())

        }
        catch (Exception e)
        {
            logger
                    .error("Exception @RateFloatDownHandler.performRateFloatDownForComponents()");
            logger.error(e);
            setStandardFailMessage();
            return false; //FXP23331, Jan 14, 2009
        }
        return isRfdAppliedForComponent; //FXP23331, Jan 14, 2009
    }

   /**
     * Associates rate to deal by performing a rate float down and inserts a
     * note to DealHistory
     * @param srk
     * @param pg
     * @param langId
     * @version Date: 10/25/2006 <br>
     *          Author: NBC/PP Implementation Team <br>
     *          Change: Set value of FloatDownCompletedFlag, regardless of if
     *          the RFD rate <br>
     *          was applied, defect #1126
     * @version 1.1 August 14, 2008 MCM Team (XS 9.3 Added method call to
     *          perform RFD for components)
     * @version 1.2 FXP23331, Jan 14, 2009, added Calc for Component level too         
     */
    private void performRateFloatDownAndAddDealHistory(SessionResourceKit srk,
            PageEntry pg, int langid)
    {
        try
        {
            // Deal deal = DBA.getDeal(srk, pg.getPageDealId(),
            // pg.getPageDealCID());
            CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());
            Deal deal = new Deal(srk, dcm, pg.getPageDealId(), pg
                    .getPageDealCID());

            double prevRate = deal.getPostedRate();
            RateFloatDown rfdProcess = new RateFloatDown(deal, langid);
            // get PricingRateInventory by calling performRateFloatDown()
            PricingRateInventory pri = rfdProcess.performRateFloatDown();
            // check if the RFD rate is valid
            boolean isValidRfd = rfdProcess.isValidRate(pri
                    .getInternalRatePercentage());
            if (isValidRfd)
            {
            	boolean isRfdRateApplied = false; //FXP23331, Jan 14, 2009 moved up
                if (deal.getRateFloatDownTypeId() != Mc.RATE_FLOAT_DOWN_TYPE_DEFAULT
                        && deal.getRateGuaranteePeriod() > 0
                        && deal.getMtgProd().getRateGuaranteePeriod() > 0)
                {
                    
                    // only use RFD rate if it is lesser than the posted rate
                    isRfdRateApplied = rfdProcess.isRfdRateBest(pri
                            .getInternalRatePercentage());
                    if (isRfdRateApplied)
                    {
                        // set pricing rate information on the deal table
                        deal.setPricingProfileId(pri
                                .getPricingRateInventoryID());
                        deal.setPostedRate(pri.getInternalRatePercentage());
                        deal.ejbStore();
                        //doCalculation(dcm); //FXP23331, Jan 14, 2009
                    }
                    // set float down completed flag on deal, regardless of if
                    // the RFD rate was applied
                    // deal.setFloatDownCompletedFlag("Y");
                    deal.ejbStore();

                    // insert record in deal history
                    DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);
                    String msg = rfdMsgToLog(prevRate, deal.getPostedRate(),
                    		deal.getMtgProd().getMtgProdName(), langid);
                    hLog.log(deal.getDealId(), deal.getCopyId(), msg,
                            Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState()
                                    .getUserProfileId());

                    deal.setFloatDownCompletedFlag("Y");
                    deal.ejbStore();
                }

                /**
                 * **************MCM Impl Team XS_9.3 changes
                 * starts*******************
                 */
                // This executes RFD for the Mortgage Components
                boolean isRfdRateAppliedForComponent = performRateFloatDownForComponents(srk, dcm, deal, langid);
                //FXP23331, Jan 14, 2009 - start
                if(isRfdRateApplied || isRfdRateAppliedForComponent){
                	doCalculation(dcm);
                }
                //FXP23331, Jan 14, 2009 - end
                /**
                 * **************MCM Impl Team XS_9.3 changes
                 * ends*******************
                 */
            }
            else
            {
                setActiveMessageToAlert(BXResources.getSysMsg(
                        "RATE_FLOAT_DOWN_INVALID_VALUE", theSessionState
                                .getLanguageId()),
                        ActiveMsgFactory.ISCUSTOMCONFIRM);
                return;
            }
        }
        catch (Exception e)
        {
            logger.error("Exception @RateFloatDownHandler.handleSubmit()");
            logger.error(e);
            setStandardFailMessage();
            return;
        }
    }

   /**
    * Returns String to use for the note for the DealHistory entry
    *
    * @param prevRate
    * @param newRate
    * @param langId
    * @return String
    */
   private String rfdMsgToLog(double prevRate, double newRate, int langid)
   {
       String str = null;

       str = BXResources.getSysMsg("DEAL_HISTORY_RATE_FLOAT_DOWN_MSG", langid);
       str = str.replaceAll("x.xx", new Double(prevRate).toString());
       str = str.replaceAll("y.yy", new Double(newRate).toString());

       return str;
   }

   /**
    * Returns String to use for the note for the DealHistory entry
    * Overloaded to include the product name for FXP23471
    *
    * @param prevRate
    * @param newRate
    * @param prodName
    * @param langId
    * @return String
    */
   private String rfdMsgToLog(double prevRate, double newRate, String prodName, int langid)
   {
       String str = null;

       str = BXResources.getSysMsg("DEAL_HISTORY_RATE_FLOAT_DOWN_MSG", langid);
       str = str.replaceAll("xxProduct_Namexx", prodName);
       str = str.replaceAll("x.xx", new Double(prevRate).toString());
       str = str.replaceAll("y.yy", new Double(newRate).toString());

       return str;
   }

   /**
     * <p>
     * Description: Returns String to use for the note for the DealHistory entry .
     * </p>
     * @version 1.0 XS_9.3 Initial Version
     * @author MCM Impl Team<br>
     * @param prevRate
     * @param newRate
     * @param prodName
     * @param langid
     * @return
     */
    private String rfdMsgToLogForComponents(double prevRate, double newRate,
            String prodName, int langid)
    {
        String str = null;

        str = BXResources.getSysMsg("COMPONENT_HISTORY_RATE_FLOAT_DOWN_MSG",
                langid);
        str = str.replaceAll("xxProduct_Namexx", prodName);
        str = str.replaceAll("x.xx", new Double(prevRate).toString());
        str = str.replaceAll("y.yy", new Double(newRate).toString());

        return str;
    }
}
