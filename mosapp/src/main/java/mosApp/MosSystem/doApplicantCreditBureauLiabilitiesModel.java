package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doApplicantCreditBureauLiabilitiesModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLiabilityId();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabilityId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLiabilityTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabilityTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLiabilityTypeDescription();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabilityTypeDescription(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLiabilityAmount();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabilityAmount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLiabilityMonthlyPayment();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabilityMonthlyPayment(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLiabilityDescription();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabilityDescription(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPercentOutGDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfPercentOutGDS(java.math.BigDecimal value);
	
	
    /**
     * 
     */
    public java.math.BigDecimal getDfCreditBureauLiabilityLimit();

    
    /**
     * 
     */
    public void setDfCreditBureauLiabilityLimit(java.math.BigDecimal value);
    
    /**
     * 
     */
    public String getDfCreditBureauIndicator();
    
    /**
     * 
     */
    public void setDfCreditBureauIndicator(String value);
    
    /**
     * 
     */
    public  java.sql.Timestamp getDfMaturityDate();

    /**
     * 
     */
    public void setDfMaturityDate(java.sql.Timestamp value);
    
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFLIABILITYID="dfLiabilityId";
	public static final String FIELD_DFLIABILITYTYPEID="dfLiabilityTypeId";
	public static final String FIELD_DFLIABILITYTYPEDESCRIPTION="dfLiabilityTypeDescription";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFLIABILITYAMOUNT="dfLiabilityAmount";
	public static final String FIELD_DFLIABILITYMONTHLYPAYMENT="dfLiabilityMonthlyPayment";
	public static final String FIELD_DFLIABILITYDESCRIPTION="dfLiabilityDescription";
	public static final String FIELD_DFPERCENTOUTGDS="dfPercentOutGDS";
    public static final String FIELD_DFCREDITBUREAULIABILITYLIMIT="dfCreditBureauLiabilityLimit";
    public static final String FIELD_DFCREDITBUREAUINDICATOR="dfCreditBureauIndicator";
    public static final String FIELD_DFMATURITYDATE = "dfMaturityDate";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

