package mosApp.MosSystem;

import com.basis100.picklist.BXResources;
import com.filogix.express.web.jato.ExpressViewBeanBase;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.DisplayField;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.StaticTextField;

/**
 * <p>
 * Title: pgDisclaimerViewBean
 * </p>
 * <p>
 * Description: ViewBean Class for Disclaimer screen with Jato Framework
 * </p>
 * </p>
 * 
 * @author:
 * @version 1.0 May 25 2011
 * 
 */
public class pgDisclaimerViewBean extends ExpressViewBeanBase {

	public static final String PAGE_NAME = "pgDisclaimer";
	public static final String DEFAULT_DISPLAY_URL = "/mosApp/MosSystem/pgDisclaimer.jsp";
	public static final String CHILD_STNEXTURL = "stNextURL";
	public static final String CHILD_STNEXTURL_RESET_VALUE = "";
	public static final String CHILD_DISCLAIMERTITLE = "disclaimerTitle";

	public static final String CHILD_HDDISCLAIMERDURATION = "hdDisclaimerDuration";
	public static final String CHILD_HDDDISCLAIMERDURATION_RESET_VALUE = "";

	int nextURL;

	int languageId;
	DisclaimerHandler handler = new DisclaimerHandler();

	/**
	 * constructor
	 * 
	 */
	public pgDisclaimerViewBean() {
		super(PAGE_NAME);
		setDefaultDisplayURL(DEFAULT_DISPLAY_URL);

		DisclaimerHandler handler = new DisclaimerHandler();
		handler.pageGetState(this);

		languageId = handler.getTheSessionState().getLanguageId();
	}

	/**
	 * <p>
	 * createChild
	 * </p>
	 * <p>
	 * Description: createChild method for Jato Framework
	 * </p>
	 * 
	 * @param name
	 *            :String - child name
	 * @return View
	 * @version :
	 * 
	 */
	protected View createChild(String name) {
		if (CHILD_STNEXTURL.equals(name)) {
			StaticTextField child = new StaticTextField(this,
					getDefaultModel(), CHILD_STNEXTURL, CHILD_STNEXTURL,
					CHILD_STNEXTURL_RESET_VALUE, null);
			return child;
		} else if (CHILD_HDDISCLAIMERDURATION.equals(name)) {
			HiddenField child = new HiddenField(this, getDefaultModel(),
					CHILD_HDDISCLAIMERDURATION, CHILD_HDDISCLAIMERDURATION,
					CHILD_HDDDISCLAIMERDURATION_RESET_VALUE, null);
			return child;
		}
		return super.createChild(name);
	}

	/**
	 * <p>
	 * resetChildren
	 * </p>
	 * <p>
	 * Description: resetChildren method for Jato Framework
	 * </p>
	 * 
	 * @param
	 * @return
	 * @version
	 * 
	 */
	public void resetChildren() {
		((DisplayField) this.getChild(CHILD_STNEXTURL))
				.setValue(CHILD_STNEXTURL_RESET_VALUE);
		((DisplayField) this.getChild(CHILD_HDDISCLAIMERDURATION))
				.setValue(CHILD_HDDDISCLAIMERDURATION_RESET_VALUE);
		super.resetChildren();
	}

	/**
	 * <p>
	 * beginDisplay
	 * </p>
	 * <p>
	 * Description: beginDisplay method for Jato Framework
	 * </p>
	 * 
	 * @param event
	 *            : DisplayEvent
	 * @return
	 * @version
	 */
	public void beginDisplay(DisplayEvent event) throws ModelControlException {
		DisclaimerHandler handler = new DisclaimerHandler();
		super.beginDisplay(event);
	}

	/**
	 * <p>
	 * getDisplayURL
	 * </p>
	 * <p>
	 * Description: getDisplayURL method for Jato Framework
	 * </p>
	 * 
	 * @param event
	 *            : DisplayEvent
	 * @return
	 * @version
	 */
	public String getDisplayURL() {
		return BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
	}
}
