package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doDecisionModificationTypeModelImpl extends QueryModelBase
	implements doDecisionModificationTypeModel
{
	/**
	 *
	 *
	 */
	public doDecisionModificationTypeModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDecisionModificationType()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDECISIONMODIFICATIONTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfDecisionModificationType(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDECISIONMODIFICATIONTYPE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDecisionModificationDescription()
	{
		return (String)getValue(FIELD_DFDECISIONMODIFICATIONDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfDecisionModificationDescription(String value)
	{
		setValue(FIELD_DFDECISIONMODIFICATIONDESCRIPTION,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL DECISIONMODIFICATIONTYPE.DECISIONMODIFICATIONTYPEID, DECISIONMODIFICATIONTYPE.DMTDESCRIPTION FROM DECISIONMODIFICATIONTYPE  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="DECISIONMODIFICATIONTYPE";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDECISIONMODIFICATIONTYPE="DECISIONMODIFICATIONTYPE.DECISIONMODIFICATIONTYPEID";
	public static final String COLUMN_DFDECISIONMODIFICATIONTYPE="DECISIONMODIFICATIONTYPEID";
	public static final String QUALIFIED_COLUMN_DFDECISIONMODIFICATIONDESCRIPTION="DECISIONMODIFICATIONTYPE.DMTDESCRIPTION";
	public static final String COLUMN_DFDECISIONMODIFICATIONDESCRIPTION="DMTDESCRIPTION";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDECISIONMODIFICATIONTYPE,
				COLUMN_DFDECISIONMODIFICATIONTYPE,
				QUALIFIED_COLUMN_DFDECISIONMODIFICATIONTYPE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDECISIONMODIFICATIONDESCRIPTION,
				COLUMN_DFDECISIONMODIFICATIONDESCRIPTION,
				QUALIFIED_COLUMN_DFDECISIONMODIFICATIONDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

