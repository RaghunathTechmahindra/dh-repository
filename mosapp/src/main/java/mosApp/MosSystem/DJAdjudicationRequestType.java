package mosApp.MosSystem;

import java.text.*;
import java.util.*;


/**
 * <p>Title: DJAdjudicationRequestType.java </p>
 *
 * <p>Description: DJ adjudication request type prototype</p>
 *
 * <p>Copyright: </p>
 *
 * <p>Company: filogix</p>
 *
 * @author Vlad Ivanov
 * @version 1.0
 */

public class DJAdjudicationRequestType extends AdjudicationRequestType{
                public DJAdjudicationRequestType() {
                    //TODO: implement NBC specific gui logic here.
                  }

                  public int getDocumentTypeId() {
                    // this is the id in the DOCUMENTTYPE table if document exists.
                    return 0; //should be defined
                }
              }

