package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doGetLotSizeUnitOfMeasureModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLotSizeMeasureId();

	
	/**
	 * 
	 * 
	 */
	public void setDfLotSizeMeasureId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLotSizeUnitMeasureDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfLotSizeUnitMeasureDesc(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFLOTSIZEMEASUREID="dfLotSizeMeasureId";
	public static final String FIELD_DFLOTSIZEUNITMEASUREDESC="dfLotSizeUnitMeasureDesc";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

