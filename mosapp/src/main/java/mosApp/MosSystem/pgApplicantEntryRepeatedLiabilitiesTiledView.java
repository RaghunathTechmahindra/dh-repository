package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;
import com.basis100.picklist.BXResources;

import com.basis100.log.*;

/**
 *
 *
 *
 */
public class pgApplicantEntryRepeatedLiabilitiesTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgApplicantEntryRepeatedLiabilitiesTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(100);
		setPrimaryModelClass( doApplicantLiabilitiesSelectModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getHdLiabilityId().setValue(CHILD_HDLIABILITYID_RESET_VALUE);
		getHdLiabilityCopyId().setValue(CHILD_HDLIABILITYCOPYID_RESET_VALUE);
		getCbLiabilityType().setValue(CHILD_CBLIABILITYTYPE_RESET_VALUE);
		getTxLiabilityDesc().setValue(CHILD_TXLIABILITYDESC_RESET_VALUE);
		getTxLiabilityAmount().setValue(CHILD_TXLIABILITYAMOUNT_RESET_VALUE);
		getTxLiabilityMonthlyPayment().setValue(CHILD_TXLIABILITYMONTHLYPAYMENT_RESET_VALUE);
		getCbLiabilityPayOffType().setValue(CHILD_CBLIABILITYPAYOFFTYPE_RESET_VALUE);
		getCblLiabilityIncludeInGDS().setValue(CHILD_CBLLIABILITYINCLUDEINGDS_RESET_VALUE);
		getTxLiabilityPercentageIncludeInGDS().setValue(CHILD_TXLIABILITYPERCENTAGEINCLUDEINGDS_RESET_VALUE);
		getHdLiabilityPercentageIncludeInGDS().setValue(CHILD_HDLIABILITYPERCENTAGEINCLUDEINGDS_RESET_VALUE);
		getCblLiabilityIncludeInTDS().setValue(CHILD_CBLLIABILITYINCLUDEINTDS_RESET_VALUE);
		getTxLiabilityPercentageIncludeInTDS().setValue(CHILD_TXLIABILITYPERCENTAGEINCLUDEINTDS_RESET_VALUE);
		getHdLiabilityPercentageIncludeInTDS().setValue(CHILD_HDLIABILITYPERCENTAGEINCLUDEINTDS_RESET_VALUE);
		getChbLiabilityDelete().setValue(CHILD_CHBLIABILITYDELETE_RESET_VALUE);
        getChCreditBureauIndicator().setValue(CHILD_CHCREDITBUREAUINDICATOR_RESET_VALUE);
        getTxCreditBureauLiabilityLimit().setValue(CHILD_TXCREDITBUREAULIABILITYLIMIT_RESET_VALUE);
        getCbMonths().setValue(CHILD_CBMONTHS_RESET_VALUE);
        getTbDay().setValue(CHILD_TBDAY_RESET_VALUE);
        getTbYear().setValue(CHILD_TBYEAR_RESET_VALUE);
        getStTileIndex().setValue(CHILD_STTILEINDEX_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_HDLIABILITYID,HiddenField.class);
		registerChild(CHILD_HDLIABILITYCOPYID,HiddenField.class);
		registerChild(CHILD_CBLIABILITYTYPE,ComboBox.class);
		registerChild(CHILD_TXLIABILITYDESC,TextField.class);
		registerChild(CHILD_TXLIABILITYAMOUNT,TextField.class);
		registerChild(CHILD_TXLIABILITYMONTHLYPAYMENT,TextField.class);
		registerChild(CHILD_CBLIABILITYPAYOFFTYPE,ComboBox.class);
		registerChild(CHILD_CBLLIABILITYINCLUDEINGDS,ComboBox.class);
		registerChild(CHILD_TXLIABILITYPERCENTAGEINCLUDEINGDS,TextField.class);
		registerChild(CHILD_HDLIABILITYPERCENTAGEINCLUDEINGDS,HiddenField.class);
		registerChild(CHILD_CBLLIABILITYINCLUDEINTDS,ComboBox.class);
		registerChild(CHILD_TXLIABILITYPERCENTAGEINCLUDEINTDS,TextField.class);
		registerChild(CHILD_HDLIABILITYPERCENTAGEINCLUDEINTDS,HiddenField.class);
		registerChild(CHILD_CHBLIABILITYDELETE,CheckBox.class);
        registerChild(CHILD_CHCREDITBUREAUINDICATOR,CheckBox.class);
        registerChild(CHILD_TXCREDITBUREAULIABILITYLIMIT,TextField.class);
        registerChild(CHILD_CBMONTHS, ComboBox.class);
        registerChild(CHILD_TBDAY, TextField.class);
        registerChild(CHILD_TBYEAR, TextField.class);
        registerChild(CHILD_STTILEINDEX, StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By Billy 05Nov2002
    ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

    //Set up all ComboBox options
    cbLiabilityTypeOptions.populate(getRequestContext());
    cbLiabilityPayOffTypeOptions.populate(getRequestContext());


    //Setup the Yes/No Options
    String yesStr = BXResources.getGenericMsg("YES_LABEL_SHORT", handler.getTheSessionState().getLanguageId());
    String noStr = BXResources.getGenericMsg("NO_LABEL_SHORT", handler.getTheSessionState().getLanguageId());
    cblLiabilityIncludeInGDSOptions.setOptions(new String[]{yesStr, noStr},new String[]{"Y", "N"});
    cblLiabilityIncludeInTDSOptions.setOptions(new String[]{yesStr, noStr},new String[]{"Y", "N"});

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated RepeatedLiabilities_onBeforeRowDisplayEvent code here
      doApplicantLiabilitiesSelectModelImpl theObj =(doApplicantLiabilitiesSelectModelImpl)this.getdoApplicantLiabilitiesSelectModel();
      ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
      handler.pageGetState(this.getParentViewBean());
		  if (theObj.getSize() > 0)
      {
        //--> Important !! If the field (in TiledView) is not binded to any DataField, we must
        //--> set the defaslt value here in order to populate the value in the Model (PrimaryModel).
        //--> Otherwise, when the page submitted (via button or Herf) the user input will not populate
        //--> Comment by BILLY 02Aug2002
//    + " Curr Value = " + this.getDisplayFieldStringValue(CHILD_CHBLIABILITYDELETE));
        
        handler.setLiabilitySection(theObj, getTileIndex());
        
        setDisplayFieldValue(CHILD_CHBLIABILITYDELETE, CHILD_CHBLIABILITYDELETE_RESET_VALUE);
        setDisplayFieldValue(CHILD_HDLIABILITYPERCENTAGEINCLUDEINGDS, CHILD_HDLIABILITYPERCENTAGEINCLUDEINGDS_RESET_VALUE);
        setDisplayFieldValue(CHILD_HDLIABILITYPERCENTAGEINCLUDEINTDS, CHILD_HDLIABILITYPERCENTAGEINCLUDEINTDS_RESET_VALUE);
        //============================================================================================
        return true;
      }
		  else return false;
		}

		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
    return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_HDLIABILITYID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantLiabilitiesSelectModel(),
				CHILD_HDLIABILITYID,
				doApplicantLiabilitiesSelectModel.FIELD_DFLIABILITYID,
				CHILD_HDLIABILITYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDLIABILITYCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantLiabilitiesSelectModel(),
				CHILD_HDLIABILITYCOPYID,
				doApplicantLiabilitiesSelectModel.FIELD_DFCOPYID,
				CHILD_HDLIABILITYCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBLIABILITYTYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantLiabilitiesSelectModel(),
				CHILD_CBLIABILITYTYPE,
				doApplicantLiabilitiesSelectModel.FIELD_DFLIABILITYTYPEID,
				CHILD_CBLIABILITYTYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbLiabilityTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXLIABILITYDESC))
		{
			TextField child = new TextField(this,
				getdoApplicantLiabilitiesSelectModel(),
				CHILD_TXLIABILITYDESC,
				doApplicantLiabilitiesSelectModel.FIELD_DFLIABILITYDESC,
				CHILD_TXLIABILITYDESC_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXLIABILITYAMOUNT))
		{
			TextField child = new TextField(this,
				getdoApplicantLiabilitiesSelectModel(),
				CHILD_TXLIABILITYAMOUNT,
				doApplicantLiabilitiesSelectModel.FIELD_DFLIABILITYAMOUNT,
				CHILD_TXLIABILITYAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXLIABILITYMONTHLYPAYMENT))
		{
			TextField child = new TextField(this,
				getdoApplicantLiabilitiesSelectModel(),
				CHILD_TXLIABILITYMONTHLYPAYMENT,
				doApplicantLiabilitiesSelectModel.FIELD_DFLIABILITYMONTHLYPAYMENT,
				CHILD_TXLIABILITYMONTHLYPAYMENT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBLIABILITYPAYOFFTYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantLiabilitiesSelectModel(),
				CHILD_CBLIABILITYPAYOFFTYPE,
				doApplicantLiabilitiesSelectModel.FIELD_DFLIABILITYPAYOFTYPEID,
				CHILD_CBLIABILITYPAYOFFTYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbLiabilityPayOffTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBLLIABILITYINCLUDEINGDS))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantLiabilitiesSelectModel(),
				CHILD_CBLLIABILITYINCLUDEINGDS,
				doApplicantLiabilitiesSelectModel.FIELD_DFINCLUDEINGDS,
				CHILD_CBLLIABILITYINCLUDEINGDS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cblLiabilityIncludeInGDSOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXLIABILITYPERCENTAGEINCLUDEINGDS))
		{
			TextField child = new TextField(this,
				getdoApplicantLiabilitiesSelectModel(),
				CHILD_TXLIABILITYPERCENTAGEINCLUDEINGDS,
				doApplicantLiabilitiesSelectModel.FIELD_DFPERCENTINCLUDEDINGDS,
				CHILD_TXLIABILITYPERCENTAGEINCLUDEINGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDLIABILITYPERCENTAGEINCLUDEINGDS))
		{
			HiddenField child = new HiddenField(this,
				//getDefaultModel(),
        getdoApplicantLiabilitiesSelectModel(),
				CHILD_HDLIABILITYPERCENTAGEINCLUDEINGDS,
				CHILD_HDLIABILITYPERCENTAGEINCLUDEINGDS,
				CHILD_HDLIABILITYPERCENTAGEINCLUDEINGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBLLIABILITYINCLUDEINTDS))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantLiabilitiesSelectModel(),
				CHILD_CBLLIABILITYINCLUDEINTDS,
				doApplicantLiabilitiesSelectModel.FIELD_DFINCLUDEINTDS,
				CHILD_CBLLIABILITYINCLUDEINTDS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cblLiabilityIncludeInTDSOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXLIABILITYPERCENTAGEINCLUDEINTDS))
		{
			TextField child = new TextField(this,
				getdoApplicantLiabilitiesSelectModel(),
				CHILD_TXLIABILITYPERCENTAGEINCLUDEINTDS,
				doApplicantLiabilitiesSelectModel.FIELD_DFPERCENTINCLUDEDINTDS,
				CHILD_TXLIABILITYPERCENTAGEINCLUDEINTDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDLIABILITYPERCENTAGEINCLUDEINTDS))
		{
			HiddenField child = new HiddenField(this,
				//getDefaultModel(),
        getdoApplicantLiabilitiesSelectModel(),
				CHILD_HDLIABILITYPERCENTAGEINCLUDEINTDS,
				CHILD_HDLIABILITYPERCENTAGEINCLUDEINTDS,
				CHILD_HDLIABILITYPERCENTAGEINCLUDEINTDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CHBLIABILITYDELETE))
		{
			CheckBox child = new CheckBox(this,
				//getDefaultModel(),
        getdoApplicantLiabilitiesSelectModel(),
				CHILD_CHBLIABILITYDELETE,
				CHILD_CHBLIABILITYDELETE,
				"T","F",
				false,
				null);

			return child;
		}
        else if (name.equals(CHILD_CHCREDITBUREAUINDICATOR)) {
            CheckBox child = new CheckBox(this,
                getdoApplicantLiabilitiesSelectModel(),
                CHILD_CHCREDITBUREAUINDICATOR,
                doApplicantLiabilitiesSelectModel.FIELD_DFCREDITBUREAUINDICATOR,
                "Y", "N", false, null);
            return child;
        }
        else if (name.equals(CHILD_TXCREDITBUREAULIABILITYLIMIT)) {
            TextField child = new TextField(this,
                getdoApplicantLiabilitiesSelectModel(),
                CHILD_TXCREDITBUREAULIABILITYLIMIT,
                doApplicantLiabilitiesSelectModel.FIELD_DFCREDITBUREAULIABILITYLIMIT,
                CHILD_TXCREDITBUREAULIABILITYLIMIT_RESET_VALUE,
                null);
            return child;
        }
        else if (name.equals(CHILD_CBMONTHS)) {
            ComboBox child = new ComboBox(this,
                    getdoApplicantLiabilitiesSelectModel(), CHILD_CBMONTHS,
                    CHILD_CBMONTHS, CHILD_CBMONTHS_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbMonthsOptions);
            return child;
        } else if (name.equals(CHILD_TBDAY)) {
            TextField child = new TextField(this,
                    getdoApplicantLiabilitiesSelectModel(), CHILD_TBDAY, CHILD_TBDAY,
                    CHILD_TBDAY_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_TBYEAR)) {
            TextField child = new TextField(this,
                    getdoApplicantLiabilitiesSelectModel(), CHILD_TBYEAR, CHILD_TBYEAR,
                    CHILD_TBYEAR_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_STTILEINDEX)) {
            StaticTextField child =
                new StaticTextField(this,
                                    getDefaultModel(),
                                    CHILD_STTILEINDEX,
                                    CHILD_STTILEINDEX,
                                    CHILD_STTILEINDEX_RESET_VALUE,
                                    null);
            return child;
        }
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdLiabilityId()
	{
		return (HiddenField)getChild(CHILD_HDLIABILITYID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdLiabilityCopyId()
	{
		return (HiddenField)getChild(CHILD_HDLIABILITYCOPYID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbLiabilityType()
	{
		return (ComboBox)getChild(CHILD_CBLIABILITYTYPE);
	}

	/**
	 *
	 *
	 */
	static class CbLiabilityTypeOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbLiabilityTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 21Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "LIABILITYTYPE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public TextField getTxLiabilityDesc()
	{
		return (TextField)getChild(CHILD_TXLIABILITYDESC);
	}


	/**
	 *
	 *
	 */
	public TextField getTxLiabilityAmount()
	{
		return (TextField)getChild(CHILD_TXLIABILITYAMOUNT);
	}


	/**
	 *
	 *
	 */
	public TextField getTxLiabilityMonthlyPayment()
	{
		return (TextField)getChild(CHILD_TXLIABILITYMONTHLYPAYMENT);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbLiabilityPayOffType()
	{
		return (ComboBox)getChild(CHILD_CBLIABILITYPAYOFFTYPE);
	}

	/**
	 *
	 *
	 */
	static class CbLiabilityPayOffTypeOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbLiabilityPayOffTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 21Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(), "LIABILITYPAYOFFTYPE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public ComboBox getCblLiabilityIncludeInGDS()
	{
		return (ComboBox)getChild(CHILD_CBLLIABILITYINCLUDEINGDS);
	}


	/**
	 *
	 *
	 */
	public TextField getTxLiabilityPercentageIncludeInGDS()
	{
		return (TextField)getChild(CHILD_TXLIABILITYPERCENTAGEINCLUDEINGDS);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdLiabilityPercentageIncludeInGDS()
	{
		return (HiddenField)getChild(CHILD_HDLIABILITYPERCENTAGEINCLUDEINGDS);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCblLiabilityIncludeInTDS()
	{
		return (ComboBox)getChild(CHILD_CBLLIABILITYINCLUDEINTDS);
	}


	/**
	 *
	 *
	 */
	public TextField getTxLiabilityPercentageIncludeInTDS()
	{
		return (TextField)getChild(CHILD_TXLIABILITYPERCENTAGEINCLUDEINTDS);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdLiabilityPercentageIncludeInTDS()
	{
		return (HiddenField)getChild(CHILD_HDLIABILITYPERCENTAGEINCLUDEINTDS);
	}


	/**
	 *
	 *
	 */
	public CheckBox getChbLiabilityDelete()
	{
		return (CheckBox)getChild(CHILD_CHBLIABILITYDELETE);
	}

    /**
     * 
     * @return
     */
    public CheckBox getChCreditBureauIndicator() {

        return (CheckBox) getChild(CHILD_CHCREDITBUREAUINDICATOR);
    }
    
    /**
     * 
     * 
     */
    public TextField getTxCreditBureauLiabilityLimit() {
        return (TextField) getChild(CHILD_TXCREDITBUREAULIABILITYLIMIT);
    }
    
    /**
     * 
     * 
     */
    public ComboBox getCbMonths() {
        return (ComboBox) getChild(CHILD_CBMONTHS);
    }

    /**
     * 
     * 
     */
    public TextField getTbDay() {
        return (TextField) getChild(CHILD_TBDAY);
    }

    /**
     * 
     * 
     */
    public TextField getTbYear() {
        return (TextField) getChild(CHILD_TBYEAR);
    }
    
    public StaticTextField getStTileIndex() {
        return (StaticTextField) getChild(CHILD_STTILEINDEX);
    }

	/**
	 *
	 *
	 */
	public doApplicantLiabilitiesSelectModel getdoApplicantLiabilitiesSelectModel()
	{
		if (doApplicantLiabilitiesSelect == null)
			doApplicantLiabilitiesSelect = (doApplicantLiabilitiesSelectModel) getModel(doApplicantLiabilitiesSelectModel.class);
		return doApplicantLiabilitiesSelect;
	}


	/**
	 *
	 *
	 */
	public void setdoApplicantLiabilitiesSelectModel(doApplicantLiabilitiesSelectModel model)
	{
			doApplicantLiabilitiesSelect = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_HDLIABILITYID="hdLiabilityId";
	public static final String CHILD_HDLIABILITYID_RESET_VALUE="";
	public static final String CHILD_HDLIABILITYCOPYID="hdLiabilityCopyId";
	public static final String CHILD_HDLIABILITYCOPYID_RESET_VALUE="";
	public static final String CHILD_CBLIABILITYTYPE="cbLiabilityType";
	public static final String CHILD_CBLIABILITYTYPE_RESET_VALUE="";
  //--Release2.1--//
	//private static CbLiabilityTypeOptionList cbLiabilityTypeOptions=new CbLiabilityTypeOptionList();
  private CbLiabilityTypeOptionList cbLiabilityTypeOptions=new CbLiabilityTypeOptionList();
	public static final String CHILD_TXLIABILITYDESC="txLiabilityDesc";
	public static final String CHILD_TXLIABILITYDESC_RESET_VALUE="";
	public static final String CHILD_TXLIABILITYAMOUNT="txLiabilityAmount";
	public static final String CHILD_TXLIABILITYAMOUNT_RESET_VALUE="";
	public static final String CHILD_TXLIABILITYMONTHLYPAYMENT="txLiabilityMonthlyPayment";
	public static final String CHILD_TXLIABILITYMONTHLYPAYMENT_RESET_VALUE="";
	public static final String CHILD_CBLIABILITYPAYOFFTYPE="cbLiabilityPayOffType";
	public static final String CHILD_CBLIABILITYPAYOFFTYPE_RESET_VALUE="";
  //--Release2.1--//
	//private static CbLiabilityPayOffTypeOptionList cbLiabilityPayOffTypeOptions=new CbLiabilityPayOffTypeOptionList();
	private CbLiabilityPayOffTypeOptionList cbLiabilityPayOffTypeOptions=new CbLiabilityPayOffTypeOptionList();
	public static final String CHILD_CBLLIABILITYINCLUDEINGDS="cblLiabilityIncludeInGDS";
	public static final String CHILD_CBLLIABILITYINCLUDEINGDS_RESET_VALUE="Y";
  //--Release2.1--//
	//private static OptionList cblLiabilityIncludeInGDSOptions=new OptionList(new String[]{"Y", "N"},new String[]{"Y", "N"});
	private OptionList cblLiabilityIncludeInGDSOptions=new OptionList();
	public static final String CHILD_TXLIABILITYPERCENTAGEINCLUDEINGDS="txLiabilityPercentageIncludeInGDS";
	public static final String CHILD_TXLIABILITYPERCENTAGEINCLUDEINGDS_RESET_VALUE="";
	public static final String CHILD_HDLIABILITYPERCENTAGEINCLUDEINGDS="hdLiabilityPercentageIncludeInGDS";
	public static final String CHILD_HDLIABILITYPERCENTAGEINCLUDEINGDS_RESET_VALUE="";
	public static final String CHILD_CBLLIABILITYINCLUDEINTDS="cblLiabilityIncludeInTDS";
	public static final String CHILD_CBLLIABILITYINCLUDEINTDS_RESET_VALUE="Y";
  //--Release2.1--//
	//private static OptionList cblLiabilityIncludeInTDSOptions=new OptionList(new String[]{"Y", "N"},new String[]{"Y", "N"});
	private OptionList cblLiabilityIncludeInTDSOptions=new OptionList();
	public static final String CHILD_TXLIABILITYPERCENTAGEINCLUDEINTDS="txLiabilityPercentageIncludeInTDS";
	public static final String CHILD_TXLIABILITYPERCENTAGEINCLUDEINTDS_RESET_VALUE="";
	public static final String CHILD_HDLIABILITYPERCENTAGEINCLUDEINTDS="hdLiabilityPercentageIncludeInTDS";
	public static final String CHILD_HDLIABILITYPERCENTAGEINCLUDEINTDS_RESET_VALUE="";
	public static final String CHILD_CHBLIABILITYDELETE="chbLiabilityDelete";
	public static final String CHILD_CHBLIABILITYDELETE_RESET_VALUE="F";
    public static final String CHILD_CHCREDITBUREAUINDICATOR="chCreditBureauIndicator";
    public static final String CHILD_CHCREDITBUREAUINDICATOR_RESET_VALUE="";
    public static final String CHILD_TXCREDITBUREAULIABILITYLIMIT="txCreditBureauLiabilityLimit";
    public static final String CHILD_TXCREDITBUREAULIABILITYLIMIT_RESET_VALUE="";
    public static final String CHILD_CBMONTHS = "cbMonths";
    public static final String CHILD_CBMONTHS_RESET_VALUE = "";
    private OptionList cbMonthsOptions = new OptionList(new String[] {}
        , new String[] {});
    public static final String CHILD_TBDAY = "tbDay";
    public static final String CHILD_TBDAY_RESET_VALUE = "";
    public static final String CHILD_TBYEAR = "tbYear";
    public static final String CHILD_TBYEAR_RESET_VALUE = "";

    // the tile index.
    public static final String CHILD_STTILEINDEX = "stTileIndex";
    public static final String CHILD_STTILEINDEX_RESET_VALUE = "";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doApplicantLiabilitiesSelectModel doApplicantLiabilitiesSelect=null;

  private ApplicantEntryHandler handler=new ApplicantEntryHandler();

  public SysLogger logger;

}

