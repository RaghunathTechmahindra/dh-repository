package mosApp.MosSystem;

import java.math.BigDecimal;
import java.sql.SQLException;

import com.basis100.log.SysLogger;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doPartySummaryModelImpl extends QueryModelBase
	implements doPartySummaryModel
{
	/**
	 *
	 *
	 */
	public doPartySummaryModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By John 25Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();

    //  Party type
    //// The new partyTypeId field should be created in order to pass proper id
    //// to the BXResources. Translation must be done only if a party already
    //// assigned to the deal. Otherwise just display page blank.
    //--Ticket#1700--14Jun2005--start--//
    // Translation is done now in the handler since other fields set there as per
    // row basis.
    //if( this.getSize() > 0)
    //{
    //  this.setDfPartyType(BXResources.getPickListDescription(
    //    "PARTYTYPE", this.getDfPartyTypeId().intValue(), languageId));
    //}
    //--Ticket#1700--14Jun2005--start--//

	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public String getDfPartyType()
	{
		return (String)getValue(FIELD_DFPARTYTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfPartyType(String value)
	{
		setValue(FIELD_DFPARTYTYPE,value);
	}

  //--Release2.1--/
  //// The new partyTypeId field should be created in order to pass proper id
  //// to the BXResources.

  /**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPartyTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPARTYTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPartyTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPARTYTYPEID,value);
	}

	/**
	 *
	 *
	 */
	public String getDfPartyName()
	{
		return (String)getValue(FIELD_DFPARTYNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfPartyName(String value)
	{
		setValue(FIELD_DFPARTYNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPartyCompany()
	{
		return (String)getValue(FIELD_DFPARTYCOMPANY);
	}


	/**
	 *
	 *
	 */
	public void setDfPartyCompany(String value)
	{
		setValue(FIELD_DFPARTYCOMPANY,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPartyPhone()
	{
		return (String)getValue(FIELD_DFPARTYPHONE);
	}


	/**
	 *
	 *
	 */
	public void setDfPartyPhone(String value)
	{
		setValue(FIELD_DFPARTYPHONE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPartyFax()
	{
		return (String)getValue(FIELD_DFPARTYFAX);
	}


	/**
	 *
	 *
	 */
	public void setDfPartyFax(String value)
	{
		setValue(FIELD_DFPARTYFAX,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPartyProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPARTYPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPartyProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPARTYPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPartyShortName()
	{
		return (String)getValue(FIELD_DFPARTYSHORTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfPartyShortName(String value)
	{
		setValue(FIELD_DFPARTYSHORTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPartyPhoneExt()
	{
		return (String)getValue(FIELD_DFPARTYPHONEEXT);
	}


	/**
	 *
	 *
	 */
	public void setDfPartyPhoneExt(String value)
	{
		setValue(FIELD_DFPARTYPHONEEXT,value);
	}


  //--DJ_PT_CR--start//
	/**
	 *
	 *
	 */
	public String getDfTransitNumber()
	{
		return (String)getValue(FIELD_DFTRANSITNUMBER);
	}

	/**
	 *
	 *
	 */
	public void setDfTransitNumber(String value)
	{
		setValue(FIELD_DFTRANSITNUMBER,value);
	}

	/**
	 *
	 *
	 */
	public String getDfAddressLine1()
	{
		return (String)getValue(FIELD_DFADDRESSLINE1);
	}

	/**
	 *
	 *
	 */
	public void setDfAddressLine1(String value)
	{
		setValue(FIELD_DFADDRESSLINE1,value);
	}

	/**
	 *
	 *
	 */
	public String getDfCity()
	{
		return (String)getValue(FIELD_DFCITY);
	}


	/**
	 *
	 *
	 */
	public void setDfCity(String value)
	{
		setValue(FIELD_DFCITY,value);
	}
  //--DJ_PT_CR--end//

	  public BigDecimal getDfInstitutionID() {

	      return (java.math.BigDecimal)getValue(FIELD_DFINSTITUTIONID);
	    }


	    public void setDfInstitutionID(BigDecimal value) {

	      setValue(FIELD_DFINSTITUTIONID,value);
	    }
	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  public SysLogger logger;
	public static final String DATA_SOURCE_NAME="jdbc/orcl";

  //--Release2.1--//
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL PARTYTYPE.PTDESCRIPTION, PARTYPROFILE.PARTYNAME, PARTYPROFILE.PARTYCOMPANYNAME, CONTACT.CONTACTPHONENUMBER, CONTACT.CONTACTFAXNUMBER, PARTYDEALASSOC.PARTYPROFILEID, PARTYDEALASSOC.DEALID, PARTYPROFILE.PTPSHORTNAME, CONTACT.CONTACTPHONENUMBEREXTENSION FROM PARTYPROFILE, PARTYTYPE, CONTACT, PARTYDEALASSOC  __WHERE__  ";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL " +
    "to_char(PARTYTYPE.PTDESCRIPTION) PTDESCRIPTION_STR, PARTYTYPE.PARTYTYPEID, PARTYPROFILE.PARTYNAME, " +
    "PARTYPROFILE.PARTYCOMPANYNAME, CONTACT.CONTACTPHONENUMBER, " +
    "CONTACT.CONTACTFAXNUMBER, PARTYDEALASSOC.PARTYPROFILEID, " +
    "PARTYDEALASSOC.DEALID, PARTYPROFILE.PTPSHORTNAME, " +
    //--DJ_PT_CR--//
    "PARTYPROFILE.PTPBUSINESSID, ADDR.ADDRESSLINE1, ADDR.CITY, " +
    "CONTACT.CONTACTPHONENUMBEREXTENSION,PARTYPROFILE.INSTITUTIONPROFILEID FROM PARTYPROFILE, " +
    "CONTACT, ADDR, PARTYDEALASSOC, PARTYTYPE  __WHERE__  ";

	//public static final String MODIFYING_QUERY_TABLE_NAME="PARTYPROFILE, PARTYTYPE, CONTACT, PARTYDEALASSOC";
  public static final String MODIFYING_QUERY_TABLE_NAME=
    "PARTYPROFILE, CONTACT, PARTYDEALASSOC";

  //public static final String STATIC_WHERE_CRITERIA=" (PARTYPROFILE.PARTYTYPEID  =  PARTYTYPE.PARTYTYPEID) AND (PARTYPROFILE.CONTACTID  =  CONTACT.CONTACTID) AND (PARTYPROFILE.PARTYPROFILEID  =  PARTYDEALASSOC.PARTYPROFILEID)";
  public static final String STATIC_WHERE_CRITERIA=
    " (PARTYPROFILE.CONTACTID  =  CONTACT.CONTACTID) AND (PARTYPROFILE.INSTITUTIONPROFILEID  =  CONTACT.INSTITUTIONPROFILEID) AND " +
    "(PARTYPROFILE.PARTYPROFILEID  =  PARTYDEALASSOC.PARTYPROFILEID) AND " +
    "(PARTYPROFILE.INSTITUTIONPROFILEID  =  PARTYDEALASSOC.INSTITUTIONPROFILEID) AND " +
    "(PARTYPROFILE.PARTYTYPEID  =  PARTYTYPE.PARTYTYPEID) AND " +
    
    //--DJ_PT_CR--//
    "(ADDR.ADDRID  =  CONTACT.ADDRID) AND "+
    "(ADDR.INSTITUTIONPROFILEID  =  CONTACT.INSTITUTIONPROFILEID) ";

	public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();
	//public static final String QUALIFIED_COLUMN_DFPARTYTYPE="PARTYTYPE.PTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFPARTYTYPE="PARTYTYPE.PTDESCRIPTION_STR";
	//public static final String COLUMN_DFPARTYTYPE="PTDESCRIPTION";
  public static final String COLUMN_DFPARTYTYPE="PTDESCRIPTION_STR";
	//--------------//

  public static final String QUALIFIED_COLUMN_DFPARTYNAME="PARTYPROFILE.PARTYNAME";
	public static final String COLUMN_DFPARTYNAME="PARTYNAME";
	public static final String QUALIFIED_COLUMN_DFPARTYCOMPANY="PARTYPROFILE.PARTYCOMPANYNAME";
	public static final String COLUMN_DFPARTYCOMPANY="PARTYCOMPANYNAME";
	public static final String QUALIFIED_COLUMN_DFPARTYPHONE="CONTACT.CONTACTPHONENUMBER";
	public static final String COLUMN_DFPARTYPHONE="CONTACTPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFPARTYFAX="CONTACT.CONTACTFAXNUMBER";
	public static final String COLUMN_DFPARTYFAX="CONTACTFAXNUMBER";
	public static final String QUALIFIED_COLUMN_DFPARTYPROFILEID="PARTYDEALASSOC.PARTYPROFILEID";
	public static final String COLUMN_DFPARTYPROFILEID="PARTYPROFILEID";
	public static final String QUALIFIED_COLUMN_DFDEALID="PARTYDEALASSOC.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFPARTYSHORTNAME="PARTYPROFILE.PTPSHORTNAME";
	public static final String COLUMN_DFPARTYSHORTNAME="PTPSHORTNAME";
	public static final String QUALIFIED_COLUMN_DFPARTYPHONEEXT="CONTACT.CONTACTPHONENUMBEREXTENSION";
	public static final String COLUMN_DFPARTYPHONEEXT="CONTACTPHONENUMBEREXTENSION";

  //--Release2.1--//
  //// New field to pass the partyId to the BXResources.
  public static final String QUALIFIED_COLUMN_DFPARTYTYPEID = "PARTYTYPE.PARTYTYPEID";
	public static final String COLUMN_DFPARTYTYPEID = "PARTYTYPEID";

  //--DJ_PT_CR--start//
	public static final String QUALIFIED_COLUMN_DFTRANSITNUMBER="PARTYPROFILE.PTPBUSINESSID";
	public static final String COLUMN_DFTRANSITNUMBER="PTPBUSINESSID";

	public static final String QUALIFIED_COLUMN_DFADDRESSLINE1="ADDR.ADDRESSLINE1";
	public static final String COLUMN_DFADDRESSLINE1="ADDRESSLINE1";

	public static final String QUALIFIED_COLUMN_DFCITY="ADDR.CITY";
	public static final String COLUMN_DFCITY="CITY";

    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID="PARTYPROFILE.INSTITUTIONPROFILEID";
    public static final String COLUMN_DFINSTITUTIONID="INSTITUTIONPROFILEID";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPARTYTYPE,
				COLUMN_DFPARTYTYPE,
				QUALIFIED_COLUMN_DFPARTYTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPARTYNAME,
				COLUMN_DFPARTYNAME,
				QUALIFIED_COLUMN_DFPARTYNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPARTYCOMPANY,
				COLUMN_DFPARTYCOMPANY,
				QUALIFIED_COLUMN_DFPARTYCOMPANY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPARTYPHONE,
				COLUMN_DFPARTYPHONE,
				QUALIFIED_COLUMN_DFPARTYPHONE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPARTYFAX,
				COLUMN_DFPARTYFAX,
				QUALIFIED_COLUMN_DFPARTYFAX,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPARTYPROFILEID,
				COLUMN_DFPARTYPROFILEID,
				QUALIFIED_COLUMN_DFPARTYPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPARTYSHORTNAME,
				COLUMN_DFPARTYSHORTNAME,
				QUALIFIED_COLUMN_DFPARTYSHORTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPARTYPHONEEXT,
				COLUMN_DFPARTYPHONEEXT,
				QUALIFIED_COLUMN_DFPARTYPHONEEXT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    //--Release2.1--//
    //// New field to pass the partyId to BXResources.
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPARTYTYPEID,
				COLUMN_DFPARTYTYPEID,
				QUALIFIED_COLUMN_DFPARTYTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

     //--DJ_PT_CR--start//
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTRANSITNUMBER,
				COLUMN_DFTRANSITNUMBER,
				QUALIFIED_COLUMN_DFTRANSITNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRESSLINE1,
				COLUMN_DFADDRESSLINE1,
				QUALIFIED_COLUMN_DFADDRESSLINE1,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCITY,
				COLUMN_DFCITY,
				QUALIFIED_COLUMN_DFCITY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
     //--DJ_PT_CR--end//
		
	    FIELD_SCHEMA.addFieldDescriptor(
	            new QueryFieldDescriptor(
	              FIELD_DFINSTITUTIONID,
	              COLUMN_DFINSTITUTIONID,
	              QUALIFIED_COLUMN_DFINSTITUTIONID,
	              java.math.BigDecimal.class,
	              false,
	              false,
	              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	              "",
	              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	              ""));
	}

}

