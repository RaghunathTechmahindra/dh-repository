package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;

/**
 *
 *
 *
 */
public class pgCommitmentOfferRepeated4TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgCommitmentOfferRepeated4TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(200);
		setPrimaryModelClass( doDocConditionModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getCbDocAction().setValue(CHILD_CBDOCACTION_RESET_VALUE);
		getStDocConditionLabel().setValue(CHILD_STDOCCONDITIONLABEL_RESET_VALUE);
		getHdDocDocTrackId().setValue(CHILD_HDDOCDOCTRACKID_RESET_VALUE);
		getBtDocDetail().setValue(CHILD_BTDOCDETAIL_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_CBDOCACTION,ComboBox.class);
		registerChild(CHILD_STDOCCONDITIONLABEL,StaticTextField.class);
		registerChild(CHILD_HDDOCDOCTRACKID,HiddenField.class);
		registerChild(CHILD_BTDOCDETAIL,Button.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
//				modelList.add(getdoDocConditionModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By Billy 26Nov2002
    cbDocActionOptions.populate(getRequestContext());

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
			handler.pageGetState(this.getParentViewBean());
			boolean retval = handler.checkDisplayThisRow("Repeated4");
			handler.pageSaveState();
			return retval;
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_CBDOCACTION))
		{
			ComboBox child = new ComboBox( this,
				getdoDocConditionModel(),
				CHILD_CBDOCACTION,
				doDocConditionModel.FIELD_DFACTION,
				CHILD_CBDOCACTION_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbDocActionOptions);
			return child;
		}
		else
		if (name.equals(CHILD_STDOCCONDITIONLABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDocConditionModel(),
				CHILD_STDOCCONDITIONLABEL,
				doDocConditionModel.FIELD_DFDOCUMENTLABEL,
				CHILD_STDOCCONDITIONLABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDDOCDOCTRACKID))
		{
			HiddenField child = new HiddenField(this,
				getdoDocConditionModel(),
				CHILD_HDDOCDOCTRACKID,
				doDocConditionModel.FIELD_DFDEFDOCTRACKID,
				CHILD_HDDOCDOCTRACKID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDOCDETAIL))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDOCDETAIL,
				CHILD_BTDOCDETAIL,
				CHILD_BTDOCDETAIL_RESET_VALUE,
				null);
				return child;

		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbDocAction()
	{
		return (ComboBox)getChild(CHILD_CBDOCACTION);
	}

	/**
	 *
	 *
	 */
	static class CbDocActionOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbDocActionOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 26Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection<String[]> c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                            "DOCUMENTSTATUS", languageId);
        Iterator<String[]> l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public StaticTextField getStDocConditionLabel()
	{
		return (StaticTextField)getChild(CHILD_STDOCCONDITIONLABEL);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdDocDocTrackId()
	{
		return (HiddenField)getChild(CHILD_HDDOCDOCTRACKID);
	}


	/**
	 *
	 *
	 */
	public void handleBtDocDetailRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btDocDetail_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());
		handler.navigateToDetail(4, handler.getRowNdxFromWebEventMethod(event));
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtDocDetail()
	{
		return (Button)getChild(CHILD_BTDOCDETAIL);
	}


	/**
	 *
	 *
	 */
	public doDocConditionModel getdoDocConditionModel()
	{
		if (doDocCondition == null)
			doDocCondition = (doDocConditionModel) getModel(doDocConditionModel.class);
		return doDocCondition;
	}


	/**
	 *
	 *
	 */
	public void setdoDocConditionModel(doDocConditionModel model)
	{
			doDocCondition = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_CBDOCACTION="cbDocAction";
	public static final String CHILD_CBDOCACTION_RESET_VALUE="";
  //--Release2.1--//
  //private static CbDocActionOptionList cbDocActionOptions=new CbDocActionOptionList();
	private CbDocActionOptionList cbDocActionOptions=new CbDocActionOptionList();
	public static final String CHILD_STDOCCONDITIONLABEL="stDocConditionLabel";
	public static final String CHILD_STDOCCONDITIONLABEL_RESET_VALUE="";
	public static final String CHILD_HDDOCDOCTRACKID="hdDocDocTrackId";
	public static final String CHILD_HDDOCDOCTRACKID_RESET_VALUE="";
	public static final String CHILD_BTDOCDETAIL="btDocDetail";
	public static final String CHILD_BTDOCDETAIL_RESET_VALUE=" ";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDocConditionModel doDocCondition=null;
  private ConditionsReviewHandler handler=new ConditionsReviewHandler();

}

