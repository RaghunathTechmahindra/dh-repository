package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doApplicantAssetsSelectModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAssetId();

	
	/**
	 * 
	 * 
	 */
	public void setDfAssetId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAssetTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfAssetTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAssetDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfAssetDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAssetValue();

	
	/**
	 * 
	 * 
	 */
	public void setDfAssetValue(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfIncludeInNetworth();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncludeInNetworth(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPercentIncludedInNetworth();

	
	/**
	 * 
	 * 
	 */
	public void setDfPercentIncludedInNetworth(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFASSETID="dfAssetId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFASSETTYPEID="dfAssetTypeId";
	public static final String FIELD_DFASSETDESC="dfAssetDesc";
	public static final String FIELD_DFASSETVALUE="dfAssetValue";
	public static final String FIELD_DFINCLUDEINNETWORTH="dfIncludeInNetworth";
	public static final String FIELD_DFPERCENTINCLUDEDINNETWORTH="dfPercentIncludedInNetworth";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

