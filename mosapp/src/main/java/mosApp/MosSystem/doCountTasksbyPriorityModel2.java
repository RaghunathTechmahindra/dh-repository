package mosApp.MosSystem;

import com.iplanet.jato.model.sql.SelectQueryModel;

public interface doCountTasksbyPriorityModel2 extends SelectQueryModel {

	public static final String FIELD_DFPRIORITYID = "dfPriorityId";
	public static final String FIELD_DFPRIORITYLABEL = "dfPriorityLabel";
	public static final String FIELD_DFPRIORITYCOUNT = "dfPriorityCount";

}
