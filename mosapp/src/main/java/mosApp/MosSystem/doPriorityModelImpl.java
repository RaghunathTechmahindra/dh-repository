package mosApp.MosSystem;


import java.sql.SQLException;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doPriorityModelImpl extends QueryModelBase
	implements doPriorityModel
{
	/**
	 *
	 *
	 */
	public doPriorityModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

          //logger = SysLog.getSysLogger("DOMWQPM");
          //logger.debug("MWQ@PriorityBeforeExecute::SQL: " + sql);
          return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
          //--Release2.1--//
          //// To override all the Description fields and populate them from BXResource
          //// based on the Language ID from the SessionStateModel
          String defaultInstanceStateName =
              getRequestContext().getModelManager().getDefaultModelInstanceName(
              SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)
              getRequestContext().getModelManager().getModel(
              SessionStateModel.class,
              defaultInstanceStateName,
              true);

          int languageId = theSessionState.getLanguageId();
          int institutionId = theSessionState.getDealInstitutionId();
          if (institutionId == 0)
          {
              institutionId = theSessionState.getUserInstitutionId();
          }

          ////logger = SysLog.getSysLogger("MWQ");
          ////logger.debug("MWQ@PriorityAfterExecute::LanguageId: " + languageId);

          while (this.next()) {
            this.setDfPriorityLabel(BXResources.getPickListDescription(
                institutionId, "PRIORITY", this.getDfPriorityId().intValue(), languageId));
          }

          //Reset Location
          this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public String getDfPriorityLabel()
	{
		return (String)getValue(FIELD_DFPRIORITYLABEL);
	}


	/**
	 *
	 *
	 */
	public void setDfPriorityLabel(String value)
	{
		setValue(FIELD_DFPRIORITYLABEL,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPriorityId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPRIORITYID);
	}


	/**
	 *
	 *
	 */
	public void setDfPriorityId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPRIORITYID,value);
	}


	/**
	 * MetaData object test method to verify the SYNTHETIC new
	 * field for the reogranized SQL_TEMPLATE query.
	 *
	 */
  /**
	public void setResultSet(ResultSet value)
	{
  		logger = SysLog.getSysLogger("METADATA");

  		try
  		{
        super.setResultSet(value);
   			if( value != null)
   			{
       			ResultSetMetaData metaData = value.getMetaData();
       			int columnCount = metaData.getColumnCount();
       			logger.debug("===============================================");
            logger.debug("Testing MetaData Object for new synthetic fields for" +
                          " doPriorityModel");
       			logger.debug("NumberColumns: " + columnCount);
         		for(int i = 0; i < columnCount ; i++)
          		{
                 	logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
          		}
          		logger.debug("================================================");
      		}
  		}
  		catch (SQLException e)
  		{
    		    logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
  		}
 	}
  **/


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

        //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
        //// (see detailed description in the desing doc as well).
        //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
        //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
        //// accordingly.

        //--Release2.1--//
        //// Not necessary to modify the SQL because the PriorityId is already included.
        //// For More details please refer to afterExecute method.

       //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	////public static final String SELECT_SQL_TEMPLATE="SELECT ALL PRIORITY.PDESCRIPTION, PRIORITY.PRIORITYID FROM PRIORITY  __WHERE__  ORDER BY PRIORITY.PRIORITYID  ASC";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL PRIORITY.PDESCRIPTION SYNTHETICPRIORDESCRIPTION, PRIORITY.PRIORITYID FROM PRIORITY  __WHERE__  ORDER BY PRIORITY.PRIORITYID  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME="PRIORITY";
	public static final String STATIC_WHERE_CRITERIA=" PRIORITY.PRIORITYID <> 0 ";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	////public static final String QUALIFIED_COLUMN_DFPRIORITYLABEL=".";
	////public static final String COLUMN_DFPRIORITYLABEL="";
	public static final String QUALIFIED_COLUMN_DFPRIORITYLABEL="PRIORITY.SYNTHETICPRIORDESCRIPTION";
	public static final String COLUMN_DFPRIORITYLABEL="SYNTHETICPRIORDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFPRIORITYID="PRIORITY.PRIORITYID";
	public static final String COLUMN_DFPRIORITYID="PRIORITYID";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		////FIELD_SCHEMA.addFieldDescriptor(
		////	new QueryFieldDescriptor(
		////		FIELD_DFPRIORITYLABEL,
		////		COLUMN_DFPRIORITYLABEL,
		////		QUALIFIED_COLUMN_DFPRIORITYLABEL,
		////		String.class,
		////		false,
		////		false,
		////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		////		"",
		////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		////		""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIORITYLABEL,
				COLUMN_DFPRIORITYLABEL,
				QUALIFIED_COLUMN_DFPRIORITYLABEL,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"PRIORITY.PDESCRIPTION",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"PRIORITY.PDESCRIPTION"));


		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIORITYID,
				COLUMN_DFPRIORITYID,
				QUALIFIED_COLUMN_DFPRIORITYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

