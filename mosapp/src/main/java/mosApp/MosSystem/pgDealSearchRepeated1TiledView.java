package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.WebActions;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.StaticTextField;

/**
 *
 *
 *
 */
public class pgDealSearchRepeated1TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealSearchRepeated1TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doRowGeneratorModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStDealNumber().setValue(CHILD_STDEALNUMBER_RESET_VALUE);
		getStPrimBorrower().setValue(CHILD_STPRIMBORROWER_RESET_VALUE);
		getStApplicantName().setValue(CHILD_STAPPLICANTNAME_RESET_VALUE);
		getStApplicantHmTel().setValue(CHILD_STAPPLICANTHMTEL_RESET_VALUE);
		getStApplicantAddr().setValue(CHILD_STAPPLICANTADDR_RESET_VALUE);
		getStSourceName().setValue(CHILD_STSOURCENAME_RESET_VALUE);
		getStSourceAppId().setValue(CHILD_STSOURCEAPPID_RESET_VALUE);
		getStDealStatus().setValue(CHILD_STDEALSTATUS_RESET_VALUE);
		getStApplicantWorkTel().setValue(CHILD_STAPPLICANTWORKTEL_RESET_VALUE);
		getStPropertyAddr().setValue(CHILD_STPROPERTYADDR_RESET_VALUE);
		getStUWName().setValue(CHILD_STUWNAME_RESET_VALUE);
		getStAdminName().setValue(CHILD_STADMINNAME_RESET_VALUE);
		getStFunderName().setValue(CHILD_STFUNDERNAME_RESET_VALUE);
		getHdRowNum().setValue(CHILD_HDROWNUM_RESET_VALUE);
		getBtSelect().setValue(CHILD_BTSELECT_RESET_VALUE);
		getBtSnapShotDeal().setValue(CHILD_BTSNAPSHOTDEAL_RESET_VALUE);
    //--> Ticket#129 -- Support search by Servicing Mortgage #
    //--> By Billy 26Nov2003
    getStServicingNum().setValue(CHILD_STSERVICINGNUM_RESET_VALUE);
        getStInstitutionLabel().setValue(CHILD_STINSTITUTIONLABEL_RESET_VALUE);
        getStInstitutionName().setValue(CHILD_STINSTITUTIONNAME_RESET_VALUE);
    //========================================================
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STDEALNUMBER,StaticTextField.class);
		registerChild(CHILD_STPRIMBORROWER,StaticTextField.class);
		registerChild(CHILD_STAPPLICANTNAME,StaticTextField.class);
		registerChild(CHILD_STAPPLICANTHMTEL,StaticTextField.class);
		registerChild(CHILD_STAPPLICANTADDR,StaticTextField.class);
		registerChild(CHILD_STSOURCENAME,StaticTextField.class);
		registerChild(CHILD_STSOURCEAPPID,StaticTextField.class);
		registerChild(CHILD_STDEALSTATUS,StaticTextField.class);
		registerChild(CHILD_STAPPLICANTWORKTEL,StaticTextField.class);
		registerChild(CHILD_STPROPERTYADDR,StaticTextField.class);
		registerChild(CHILD_STUWNAME,StaticTextField.class);
		registerChild(CHILD_STADMINNAME,StaticTextField.class);
		registerChild(CHILD_STFUNDERNAME,StaticTextField.class);
		registerChild(CHILD_HDROWNUM,HiddenField.class);
		registerChild(CHILD_BTSELECT,Button.class);
		registerChild(CHILD_BTSNAPSHOTDEAL,Button.class);
    //--> Ticket#129 -- Support search by Servicing Mortgage #
    //--> By Billy 26Nov2003
    registerChild(CHILD_STSERVICINGNUM,StaticTextField.class);
        registerChild(CHILD_STINSTITUTIONLABEL, StaticTextField.class);
        registerChild(CHILD_STINSTITUTIONNAME, StaticTextField.class);
    //========================================================
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoRowGeneratorModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		// The following code block was migrated from the Repeated1_onBeforeDisplayEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.checkAndSyncDOWithCursorInfo(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// The following code block was migrated from the Repeated1_onBeforeRowDisplayEvent method
      DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		  handler.pageGetState(this.getParentViewBean());
		  boolean rc = handler.populateRepeatedFields(getTileIndex());
		  handler.pageSaveState();

		  return rc;
		}

		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
        if (name.equals(CHILD_STDEALNUMBER))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STDEALNUMBER,
                CHILD_STDEALNUMBER,
                CHILD_STDEALNUMBER_RESET_VALUE,
                null);
            return child;
        }
        else
            if (name.equals(CHILD_STINSTITUTIONLABEL))
            {
                StaticTextField child = new StaticTextField(this,
                    getDefaultModel(),
                    CHILD_STINSTITUTIONLABEL,
                    CHILD_STINSTITUTIONLABEL,
                    CHILD_STINSTITUTIONLABEL_RESET_VALUE,
                    null);
                return child;
            }
            else
                if (name.equals(CHILD_STINSTITUTIONNAME))
                {
                    StaticTextField child = new StaticTextField(this,
                        getDefaultModel(),
                        CHILD_STINSTITUTIONNAME,
                        CHILD_STINSTITUTIONNAME,
                        CHILD_STINSTITUTIONNAME_RESET_VALUE,
                        null);
                    return child;
                }
            else
		if (name.equals(CHILD_STPRIMBORROWER))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPRIMBORROWER,
				CHILD_STPRIMBORROWER,
				CHILD_STPRIMBORROWER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPLICANTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAPPLICANTNAME,
				CHILD_STAPPLICANTNAME,
				CHILD_STAPPLICANTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPLICANTHMTEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAPPLICANTHMTEL,
				CHILD_STAPPLICANTHMTEL,
				CHILD_STAPPLICANTHMTEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPLICANTADDR))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAPPLICANTADDR,
				CHILD_STAPPLICANTADDR,
				CHILD_STAPPLICANTADDR_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSOURCENAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STSOURCENAME,
				CHILD_STSOURCENAME,
				CHILD_STSOURCENAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSOURCEAPPID))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STSOURCEAPPID,
				CHILD_STSOURCEAPPID,
				CHILD_STSOURCEAPPID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALSTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STDEALSTATUS,
				CHILD_STDEALSTATUS,
				CHILD_STDEALSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPLICANTWORKTEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAPPLICANTWORKTEL,
				CHILD_STAPPLICANTWORKTEL,
				CHILD_STAPPLICANTWORKTEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYADDR))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPROPERTYADDR,
				CHILD_STPROPERTYADDR,
				CHILD_STPROPERTYADDR_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUWNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STUWNAME,
				CHILD_STUWNAME,
				CHILD_STUWNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STADMINNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STADMINNAME,
				CHILD_STADMINNAME,
				CHILD_STADMINNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STFUNDERNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STFUNDERNAME,
				CHILD_STFUNDERNAME,
				CHILD_STFUNDERNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDROWNUM))
		{
			HiddenField child = new HiddenField(this,
				getdoRowGeneratorModel(),
				CHILD_HDROWNUM,
				doRowGeneratorModel.FIELD_DFROWNDX,
				CHILD_HDROWNUM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTSELECT))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSELECT,
				CHILD_BTSELECT,
				CHILD_BTSELECT_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTSNAPSHOTDEAL))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSNAPSHOTDEAL,
				CHILD_BTSNAPSHOTDEAL,
				CHILD_BTSNAPSHOTDEAL_RESET_VALUE,
				null);
				return child;

		}
    //--> Ticket#129 -- Support search by Servicing Mortgage #
    //--> By Billy 26Nov2003
    else
		if (name.equals(CHILD_STSERVICINGNUM))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STSERVICINGNUM,
				CHILD_STSERVICINGNUM,
				CHILD_STSERVICINGNUM_RESET_VALUE,
				null);
			return child;
		}
    //========================================================
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


    /**
    *
    *
    */
   public StaticTextField getStDealNumber()
   {
       return (StaticTextField)getChild(CHILD_STDEALNUMBER);
   }

   /**
   *
   *
   */
  public StaticTextField getStInstitutionLabel()
  {
      return (StaticTextField)getChild(CHILD_STINSTITUTIONLABEL);
  }
  /**
  *
  *
  */
 public StaticTextField getStInstitutionName()
 {
     return (StaticTextField)getChild(CHILD_STINSTITUTIONNAME);
 }

	/**
	 *
	 *
	 */
	public StaticTextField getStPrimBorrower()
	{
		return (StaticTextField)getChild(CHILD_STPRIMBORROWER);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApplicantName()
	{
		return (StaticTextField)getChild(CHILD_STAPPLICANTNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApplicantHmTel()
	{
		return (StaticTextField)getChild(CHILD_STAPPLICANTHMTEL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApplicantAddr()
	{
		return (StaticTextField)getChild(CHILD_STAPPLICANTADDR);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSourceName()
	{
		return (StaticTextField)getChild(CHILD_STSOURCENAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSourceAppId()
	{
		return (StaticTextField)getChild(CHILD_STSOURCEAPPID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealStatus()
	{
		return (StaticTextField)getChild(CHILD_STDEALSTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApplicantWorkTel()
	{
		return (StaticTextField)getChild(CHILD_STAPPLICANTWORKTEL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyAddr()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYADDR);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUWName()
	{
		return (StaticTextField)getChild(CHILD_STUWNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAdminName()
	{
		return (StaticTextField)getChild(CHILD_STADMINNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStFunderName()
	{
		return (StaticTextField)getChild(CHILD_STFUNDERNAME);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdRowNum()
	{
		return (HiddenField)getChild(CHILD_HDROWNUM);
	}


	/**
	 *
	 *
	 */
	public void handleBtSelectRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btSelect_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());
		handler.handleSelectDeal(handler.getRowNdxFromWebEventMethod(event));
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtSelect()
	{
		return (Button)getChild(CHILD_BTSELECT);
	}


	/**
	 *
	 *
	 */
	public void handleBtSnapShotDealRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btSnapShotDeal_onWebEvent method
    DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());
		handler.handleSelectDealNumber(handler.getRowNdxFromWebEventMethod(event));
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtSnapShotDeal()
	{
		return (Button)getChild(CHILD_BTSNAPSHOTDEAL);
	}


	/**
	 *
	 *
	 */
	public doRowGeneratorModel getdoRowGeneratorModel()
	{
		if (doRowGenerator == null)
			doRowGenerator = (doRowGeneratorModel) getModel(doRowGeneratorModel.class);
		return doRowGenerator;
	}


	/**
	 *
	 *
	 */
	public void setdoRowGeneratorModel(doRowGeneratorModel model)
	{
			doRowGenerator = model;
	}


	//--> New method to manually set the current Display Offset
  //--> Current in JATO there is no method to mamually set the display offset, that's why
  //--> we have to make our own method.
  //--> Note : This method may need to be implemented in all TiledViews with Forward, Backward buttons.
  //--> By Billy 22July2002
  public void setCurrentDisplayOffset(int offset) throws Exception
  {
    setWebActionModelOffset(offset);
    handleWebAction(WebActions.ACTION_REFRESH);
  }
  //=====================================================================================

  //--> Ticket#129 -- Support search by Servicing Mortgage #
  //--> By Billy 26Nov2003
  public StaticTextField getStServicingNum()
	{
		return (StaticTextField)getChild(CHILD_STSERVICINGNUM);
	}
  //========================================================

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STDEALNUMBER="stDealNumber";
	public static final String CHILD_STDEALNUMBER_RESET_VALUE="";
	public static final String CHILD_STPRIMBORROWER="stPrimBorrower";
	public static final String CHILD_STPRIMBORROWER_RESET_VALUE="";
	public static final String CHILD_STAPPLICANTNAME="stApplicantName";
	public static final String CHILD_STAPPLICANTNAME_RESET_VALUE="";
	public static final String CHILD_STAPPLICANTHMTEL="stApplicantHmTel";
	public static final String CHILD_STAPPLICANTHMTEL_RESET_VALUE="";
	public static final String CHILD_STAPPLICANTADDR="stApplicantAddr";
	public static final String CHILD_STAPPLICANTADDR_RESET_VALUE="";
	public static final String CHILD_STSOURCENAME="stSourceName";
	public static final String CHILD_STSOURCENAME_RESET_VALUE="";
	public static final String CHILD_STSOURCEAPPID="stSourceAppId";
	public static final String CHILD_STSOURCEAPPID_RESET_VALUE="";
	public static final String CHILD_STDEALSTATUS="stDealStatus";
	public static final String CHILD_STDEALSTATUS_RESET_VALUE="";
	public static final String CHILD_STAPPLICANTWORKTEL="stApplicantWorkTel";
	public static final String CHILD_STAPPLICANTWORKTEL_RESET_VALUE="";
	public static final String CHILD_STPROPERTYADDR="stPropertyAddr";
	public static final String CHILD_STPROPERTYADDR_RESET_VALUE="";
	public static final String CHILD_STUWNAME="stUWName";
	public static final String CHILD_STUWNAME_RESET_VALUE="";
	public static final String CHILD_STADMINNAME="stAdminName";
	public static final String CHILD_STADMINNAME_RESET_VALUE="";
	public static final String CHILD_STFUNDERNAME="stFunderName";
	public static final String CHILD_STFUNDERNAME_RESET_VALUE="";
	public static final String CHILD_HDROWNUM="hdRowNum";
	public static final String CHILD_HDROWNUM_RESET_VALUE="";
	public static final String CHILD_BTSELECT="btSelect";
	public static final String CHILD_BTSELECT_RESET_VALUE="Custom";
	public static final String CHILD_BTSNAPSHOTDEAL="btSnapShotDeal";
	public static final String CHILD_BTSNAPSHOTDEAL_RESET_VALUE=" ";
  //--> Ticket#129 -- Support search by Servicing Mortgage #
  //--> By Billy 26Nov2003
	  public static final String CHILD_STSERVICINGNUM="stServicingNum";
	    public static final String CHILD_STSERVICINGNUM_RESET_VALUE="";
        public static final String CHILD_STINSTITUTIONLABEL="stInstitutionLabel";
        public static final String CHILD_STINSTITUTIONLABEL_RESET_VALUE="";
        public static final String CHILD_STINSTITUTIONNAME="stInstitutionName";
        public static final String CHILD_STINSTITUTIONNAME_RESET_VALUE="";
  //========================================================



	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doRowGeneratorModel doRowGenerator=null;
  private DealSearchHandler handler=new DealSearchHandler();
}

