package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doPasswordCorrectModelImpl extends QueryModelBase
	implements doPasswordCorrectModel
{
	/**
	 *
	 *
	 */
	public doPasswordCorrectModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{

	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfUSERID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFUSERID);
	}


	/**
	 *
	 *
	 */
	public void setDfUSERID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFUSERID,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfLASTPASSWORDCHANGE()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFLASTPASSWORDCHANGE);
	}


	/**
	 *
	 *
	 */
	public void setDfLASTPASSWORDCHANGE(java.sql.Timestamp value)
	{
		setValue(FIELD_DFLASTPASSWORDCHANGE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCURRENTGRACECOUNT()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCURRENTGRACECOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfCURRENTGRACECOUNT(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCURRENTGRACECOUNT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPASSWORD()
	{
		return (String)getValue(FIELD_DFPASSWORD);
	}


	/**
	 *
	 *
	 */
	public void setDfPASSWORD(String value)
	{
		setValue(FIELD_DFPASSWORD,value);
	}


	/**
	 *
	 *
	 */
	public String getDfUSERLOGIN()
	{
		return (String)getValue(FIELD_DFUSERLOGIN);
	}


	/**
	 *
	 *
	 */
	public void setDfUSERLOGIN(String value)
	{
		setValue(FIELD_DFUSERLOGIN,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL USERSECURITYPOLICY.USERID, USERSECURITYPOLICY.LASTPASSWORDCHANGE, USERSECURITYPOLICY.CURRENTGRACECOUNT, USERSECURITYPOLICY.PASSWORD, USERPROFILE.USERLOGIN FROM USERSECURITYPOLICY, USERPROFILE  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="USERSECURITYPOLICY, USERPROFILE";
	public static final String STATIC_WHERE_CRITERIA=" (USERPROFILE.USERPROFILEID  =  USERSECURITYPOLICY.USERID)";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFUSERID="USERSECURITYPOLICY.USERID";
	public static final String COLUMN_DFUSERID="USERID";
	public static final String QUALIFIED_COLUMN_DFLASTPASSWORDCHANGE="USERSECURITYPOLICY.LASTPASSWORDCHANGE";
	public static final String COLUMN_DFLASTPASSWORDCHANGE="LASTPASSWORDCHANGE";
	public static final String QUALIFIED_COLUMN_DFCURRENTGRACECOUNT="USERSECURITYPOLICY.CURRENTGRACECOUNT";
	public static final String COLUMN_DFCURRENTGRACECOUNT="CURRENTGRACECOUNT";
	public static final String QUALIFIED_COLUMN_DFPASSWORD="USERSECURITYPOLICY.PASSWORD";
	public static final String COLUMN_DFPASSWORD="PASSWORD";
	public static final String QUALIFIED_COLUMN_DFUSERLOGIN="USERPROFILE.USERLOGIN";
	public static final String COLUMN_DFUSERLOGIN="USERLOGIN";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERID,
				COLUMN_DFUSERID,
				QUALIFIED_COLUMN_DFUSERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLASTPASSWORDCHANGE,
				COLUMN_DFLASTPASSWORDCHANGE,
				QUALIFIED_COLUMN_DFLASTPASSWORDCHANGE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCURRENTGRACECOUNT,
				COLUMN_DFCURRENTGRACECOUNT,
				QUALIFIED_COLUMN_DFCURRENTGRACECOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPASSWORD,
				COLUMN_DFPASSWORD,
				QUALIFIED_COLUMN_DFPASSWORD,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERLOGIN,
				COLUMN_DFUSERLOGIN,
				QUALIFIED_COLUMN_DFUSERLOGIN,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

