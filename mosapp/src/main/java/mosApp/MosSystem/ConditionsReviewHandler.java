package mosApp.MosSystem;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import MosSystem.Mc;
import MosSystem.Sc;
import com.filogix.util.Xc;

import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.ChangeMapValue;
import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.PartyProfile;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.commitment.CCM;
import com.basis100.deal.conditions.ConditionHandler;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.security.DealStatusManager;
import com.basis100.deal.util.DBA;
import com.basis100.entity.FinderException;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.entity.Page;

import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;

import com.filogix.externallinks.condition.management.ConditionUpdateRequester;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.CheckBox;
import com.iplanet.jato.view.html.HtmlDisplayFieldBase;
import com.iplanet.jato.view.html.ImageField;
import com.iplanet.jato.view.html.ListBox;
import com.iplanet.jato.view.html.TextField;


// Special usage of Condition Review PageEntry members:
	//
	// 1) pageCondition3
	//
	//    Value - true :  Condition are fine - just display - no rebuild
	//
	//          - false: Default conditions to be (re)build upon page generation.
	//
	// 2) pageCondition4
	//
	//	  Used during validation of input .... PROGRAMMER PLEASE MORE!
	//
	// 3) pageCondition5
	//
	//	  Set to diaplay Resolve Deal Button
	//      - New requirement from Product , please refer to changes ticket # Id131
	//      - true : display the button
	//      - false or ViewOnly : hide the button
	//
  //--TD_CONDR_CR--start--//
  // 4) pageCondition6
  // Option to display the Condition Details screen as a DocTracking Details
  // screen with a possibility to edit Default and Standard Conditions
  //       -- "Y" (display as DocTracking Details screen)
  //       -- default = N (display original Condition Detail screen)
  //--TD_CONDR_CR--end--//

public class ConditionsReviewHandler extends PageHandlerCommon
	implements Cloneable, Sc
{

	private static final int ALERT_LABEL_ABSENT=1;
	private static final int ALERT_TEXT_ABSENT=2;

  //--TD_CONDR_CR--start//
  public static final int SECTION_DEFAULT_CONDITIONS=1;
	public static final int SECTION_STANDARD_CONDITIONS=2;
	
  //--TD_CONDR_CR--end//
	
	//Duplicate Default Conditions
	private boolean ddcUpdate = true;
	/**
	 *
	 *
	 */
	public ConditionsReviewHandler cloneSS()
	{
		return(ConditionsReviewHandler) super.cloneSafeShallow();

	}


	//This method is used to populate the fields in the header
	//with the appropriate information
	public void populatePageDisplayFields()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		populatePageShellDisplayFields();
		// Quick Link Menu display
		displayQuickLinkMenu();
		populateTaskNavigator(pg);
		populatePageDealSummarySnapShot();
		populatePreviousPagesLinks();

 		//// SYNCADD.
 		// Added Target to Std and Cust condition section -- By BILLY 07June2002
 		getCurrNDPage().setDisplayFieldValue("stTargetStdCon", new String(TARGET_TAG));
 		getCurrNDPage().setDisplayFieldValue("stTargetCustCon", new String(TARGET_TAG));

    //--Release2.1--TD_DTS_CR--start//
    ViewBean thePage = getCurrNDPage();
		populateFieldValidation(thePage);
    //--Release2.1--TD_DTS_CR--end//

    //--DJ_CL_CR009--start--//
    //// Temporarily commented out to suppress always as per Product request. Until the CR_009 is finalized.
    ////if (PropertiesCache.getInstance().getProperty("com.basis100.conditions.displaydjproducecommitmentcheckbox", "N").equals("N"))
    ////{
      startSupressContent("stDisplayDJCheckBoxStart", thePage);
      endSupressContent("stDisplayDJCheckBoxEnd", thePage);
    ////}
    //--DJ_CL_CR009--end--//
	}
	
	/**
	 * This method is called upon rendering tile display.
	 * @param rowIndx
	 */
	protected void setButtonsDisplayFields(int rowIndx) {
		StringBuffer BtnNamePrefix = new StringBuffer();
		BtnNamePrefix.append(pgCommitmentOfferViewBean.CHILD_REPEATED3);
		BtnNamePrefix.append("/");
		String lessBtnName = BtnNamePrefix.toString() + pgCommitmentOfferRepeated3TiledView.CHILD_IMDEFCONDBTNLESS;
		String moreBtnName = BtnNamePrefix.toString() + pgCommitmentOfferRepeated3TiledView.CHILD_IMDEFCONDBTNMORE;
//		String defaultConditionName = BtnNamePrefix.toString() + pgCommitmentOfferRepeated3TiledView.CHILD_TBDEFCONDITION;
		String viewDefCondBtnName = BtnNamePrefix.toString() + pgCommitmentOfferRepeated3TiledView.CHILD_IMVIEWDEFCONDBTN;
//		String displayDefaultName = BtnNamePrefix.toString() + pgCommitmentOfferRepeated3TiledView.CHILD_HDDISPLAYDEFAULTCONDITION;
//		String useDefaultName = BtnNamePrefix.toString() + pgCommitmentOfferRepeated3TiledView.CHILD_CHUSEDEFAULTCONDITION;
		
		ViewBean currNDPage = getCurrNDPage();
		
		ImageField lessBtnField = (ImageField) currNDPage
        .getDisplayField(lessBtnName);
		lessBtnField.setValue("../images/arrowy_left.gif");
		
		ImageField moreBtnField = (ImageField) currNDPage
        .getDisplayField(moreBtnName);
		moreBtnField.setValue("../images/arrowy_right.gif");
		
		ImageField viewDefaultBtnField = (ImageField) currNDPage
		.getDisplayField(viewDefCondBtnName);
		if(this.theSessionState.getLanguageId() == 0) {
			viewDefaultBtnField.setValue("../images/view_default.gif");
		} else {
			viewDefaultBtnField.setValue("../images/view_default_fr.gif");
		}
		
//		TextField defCondition = (TextField)currNDPage.getDisplayField(defaultConditionName);
		
//		boolean displayDefaultValue = "Y".equalsIgnoreCase((String)currNDPage.getDisplayFieldValue(displayDefaultName));
//		if(!displayDefaultValue) {
//			defCondition.setExtraHtml("style=\"display:none;\"");
//			lessBtnField.setExtraHtml("style=\"display:none;\"");
//			moreBtnField.setExtraHtml("style=\"display:none;\"");
//		} else {
//			//viewDefaultBtnField.setExtraHtml("style=\"display:none;\"");
//		}
		
	}

	/**
	 *
	 *
	 */
	public String setAddStdConditionButtonHTML()
	{
		if(displayEditButton() == true)
      return "<a href=\"javascript:openAddConditionDialog(1)\"><img src=\"../images/add_stdcondition.gif\" ALIGN=TOP width=133 height=15 alt=\"\" border=\"0\"></a>";
		else
      return "";
    //ViewBean currNDPage = getCurrNDPage();
		//currNDPage.setDisplayFieldValue("stAddStdButtonHTML", new String(html));
	}

	/**
	 *
	 *
	 */
	public String setAddCustConditionButtonHTML()
	{
		if(displayEditButton() == true)
      return "<a href=\"javascript:openAddConditionDialog(2)\"><img src=\"../images/add_custcondition.gif\" ALIGN=TOP width=133 height=15 alt=\"\" border=\"0\"></a>";
		else
      return "";
    //ViewBean currNDPage = getCurrNDPage();
		//currNDPage.setDisplayFieldValue("stAddCustButtonHTML", new String(html));
	}

	//
	// Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
	//
	public void setupBeforePageGeneration()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		TiledView vo = null;
		if (pg.getSetupBeforeGenerationCalled() == true) return;

		pg.setSetupBeforeGenerationCalled(true);
		int numRows = 0;

		setupUWDealSummarySnapShotDO(pg);

    //--TD_CONDR_CR--start--//
    boolean docTrackingDetailsStyle = false; // default
    docTrackingDetailsStyle 
        = (PropertiesCache.getInstance()
            .getProperty(theSessionState.getDealInstitutionId(), 
                         "com.basis100.conditions.allowtoeditdefstdconds", "N")).equals("Y");
    if(docTrackingDetailsStyle == true){
        pg.setPageCondition6(true);
    } else if (docTrackingDetailsStyle == false){ //default
        pg.setPageCondition6(false);
    }
    //logger.debug("CRH@setupBeforePageGeneration:PageCondition6: " + pg.getPageCondition6());
    //--TD_CONDR_CR--end--//

		try
		{
			int dealId = pg.getPageDealId();
			int copyId = pg.getPageDealCID();
			
			// rebuild conditions?
			if (pg.getPageCondition3() == false)
			{
				SessionResourceKit srk = getSessionResourceKit();
				pg.setPageCondition3(true);
				try
				{
					srk.beginTransaction();
					ConditionHandler ch = new ConditionHandler(srk);
					MasterDeal md = new MasterDeal(srk, null, dealId);
					if (md == null)
					{
						throw(new Exception("MasterDeal is Not Found"));
					}
					//ch.buildSystemGeneratedDocumentTracking(new DealPK(dealId, md.getGoldCopyId()));
					ch.buildSystemGeneratedDocumentTracking(new DealPK(dealId, copyId));
					srk.commitTransaction();
				}
				catch(Exception e)
				{
					srk.cleanTransaction();
					setStandardFailMessage();
					logger.error("Condition Review:Error occured during Default DocumentTracking  generation:");
					logger.error(e);
				}
			}
			ViewBean currNDPage = getCurrNDPage();
			PageCursorInfo tds1 = getPageCursorInfo(pg, "Repeated1");
			PageCursorInfo tds2 = getPageCursorInfo(pg, "Repeated2");
			PageCursorInfo tds3 = getPageCursorInfo(pg, "Repeated3");

			//Added DocTracking Conditions -- By BILLY 23April2002
			PageCursorInfo tds4 = getPageCursorInfo(pg, "Repeated4");
      //// SYNCADD.
 			//Split Signed Doc conditions and make it protected -- by BILLY 13June2002
 			PageCursorInfo tds5 = getPageCursorInfo(pg, "Repeated5");

			Hashtable pst = pg.getPageStateTable();
			if (pst == null)
			{
				logger.trace("------NO PST!");
			}
			QueryModelBase conDo =(QueryModelBase)
          (RequestManager.getRequestContext().getModelManager().getModel(doDefConditionModel.class));
			conDo.clearUserWhereCriteria();
			conDo.addUserWhereCriterion("dfDealId", "=", new String("" + dealId));
			conDo.addUserWhereCriterion("dfCopyId", "=", new String("" + copyId));
      //--Release2.1--//
      //--> Setup current displaying LanguageID
      //--> By Billy 25Nov2002
      conDo.addUserWhereCriterion("dfLanguagePrederenceId", "=",
          new Integer(theSessionState.getLanguageId()));
      //=======================================
			conDo.executeSelect(null);
			numRows = conDo.getSize();
			logger.trace("@ConditionsReviewHandler.setupBeforePageGeneration: Number default conditions = " + numRows);
			if (tds1 == null)
			{
				String voName = "Repeated1";
				tds1 = new PageCursorInfo("Repeated1", voName, numRows, numRows);
				tds1.setRefresh(true);
				pst.put(tds1.getName(), tds1);
			}
			else
			{
				tds1.setTotalRows(numRows);
				tds1.setRowsPerPage(numRows);
			}
			vo =(TiledView)(getCurrNDPage().getChild(tds1.getVoName()));
			vo.setMaxDisplayTiles(numRows);

			//Added DocTracking Conditions -- By BILLY 23April2002
			conDo =(QueryModelBase)
          (RequestManager.getRequestContext().getModelManager().getModel(doDocConditionModel.class));
			conDo.clearUserWhereCriteria();
			conDo.addUserWhereCriterion("dfDealId", "=", new String("" + dealId));
			conDo.addUserWhereCriterion("dfCopyId", "=", new String("" + copyId));
      //--Release2.1--//
      //--> Setup current displaying LanguageID
      //--> By Billy 25Nov2002
      conDo.addUserWhereCriterion("dfLanguagePrederenceId", "=",
          new Integer(theSessionState.getLanguageId()));
      //=======================================
			conDo.executeSelect(null);
			numRows = conDo.getSize();
			logger.trace("@ConditionsReviewHandler.setupBeforePageGeneration: Number DocTracking conditions = " + numRows);
			if (tds4 == null)
			{
				String voName = "Repeated4";
				tds4 = new PageCursorInfo("Repeated4", voName, numRows, numRows);
				tds4.setRefresh(true);
				pst.put(tds4.getName(), tds4);
			}
			else
			{
				tds4.setTotalRows(numRows);
				tds4.setRowsPerPage(numRows);
			}
			vo =(TiledView)(getCurrNDPage().getChild(tds4.getVoName()));
			vo.setMaxDisplayTiles(numRows);

			//===========================================================================================
			//// SYNCADD.
			//Split Signed Doc conditions and make it protected -- by BILLY 13June2002
			conDo =(QueryModelBase)
          (RequestManager.getRequestContext().getModelManager().getModel(doSignConditionModel.class));

			conDo.clearUserWhereCriteria();
			conDo.addUserWhereCriterion("dfDealId", "=", new String("" + dealId));
			conDo.addUserWhereCriterion("dfCopyId", "=", new String("" + copyId));
      //--Release2.1--//
      //--> Setup current displaying LanguageID
      //--> By Billy 25Nov2002
      conDo.addUserWhereCriterion("dfLanguagePrederenceId", "=",
          new Integer(theSessionState.getLanguageId()));
      //=======================================
      //logger.debug("BILLY ===> The SQL : " + conDo.getSelectSQL());
			conDo.executeSelect(null);
			numRows = conDo.getSize();
			logger.trace("@ConditionsReviewHandler.setupBeforePageGeneration: Number Signed DocTracking conditions = " + numRows);
			if (tds5 == null)
			{
				String voName = "Repeated5";
				tds5 = new PageCursorInfo("Repeated5", voName, numRows, numRows);
				tds5.setRefresh(true);
				pst.put(tds5.getName(), tds5);
			}
			else
			{
				tds5.setTotalRows(numRows);
				tds5.setRowsPerPage(numRows);
			}

			vo =(TiledView)(getCurrNDPage().getChild(tds5.getVoName()));
			vo.setMaxDisplayTiles(numRows);

			//===========================================================================================


			conDo =(QueryModelBase)
          (RequestManager.getRequestContext().getModelManager().getModel(doStdConditionModel.class));
			conDo.clearUserWhereCriteria();
			conDo.addUserWhereCriterion("dfDealId", "=", new String("" + dealId));
			conDo.addUserWhereCriterion("dfCopyId", "=", new String("" + copyId));
      //--Release2.1--//
      //--> Setup current displaying LanguageID
      //--> By Billy 25Nov2002
      conDo.addUserWhereCriterion("dfLanguagePrederenceId", "=",
          new Integer(theSessionState.getLanguageId()));
      //=======================================
			conDo.executeSelect(null);
			numRows = conDo.getSize();
			logger.trace("@ConditionsReviewHandler.setupBeforePageGeneration: Number standard conditions = " + numRows);
			if (tds2 == null)
			{
				String voName = "Repeated2";
				tds2 = new PageCursorInfo("Repeated2", voName, numRows, numRows);
				tds2.setRefresh(true);
				pst.put(tds2.getName(), tds2);
			}
			else
			{
				tds2.setTotalRows(numRows);
				tds2.setRowsPerPage(numRows);
			}
			vo =(TiledView)(getCurrNDPage().getChild(tds2.getVoName()));
			vo.setMaxDisplayTiles(numRows);

			conDo =(QueryModelBase)
          (RequestManager.getRequestContext().getModelManager().getModel(doCusConditionModel.class));
			conDo.clearUserWhereCriteria();
			conDo.addUserWhereCriterion("dfDealId", "=", new String("" + dealId));
			conDo.addUserWhereCriterion("dfCopyId", "=", new String("" + copyId));
      //--Release2.1--//
      //--> Setup current displaying LanguageID
      //--> Note : Custom Condition always created with language id = 0
      //--> By Billy 25Nov2002
      //// As per confirmation from the Product Team Custom Condition should be
      //// created for both languages.
      ////conDo.addUserWhereCriterion("dfLanguagePrederenceId", "=", new Integer(0));
      conDo.addUserWhereCriterion("dfLanguagePrederenceId", "=", new Integer(theSessionState.getLanguageId()));
      //=======================================
			conDo.executeSelect(null);
			numRows = conDo.getSize();
			logger.trace("@ConditionsReviewHandler.setupBeforePageGeneration: Number custom conditions = " + numRows);
			if (tds3 == null)
			{
				String voName = "Repeated3";
				tds3 = new PageCursorInfo("Repeated3", voName, numRows, numRows);
				tds3.setRefresh(true);
				pst.put(tds3.getName(), tds3);
			}
			else
			{
				tds3.setTotalRows(numRows);
				tds3.setRowsPerPage(numRows);
			}
			vo =(TiledView)(getCurrNDPage().getChild(tds3.getVoName()));
			vo.setMaxDisplayTiles(numRows);

			if (tds1.isTotalRowsChanged())
			{
				// number of rows has changed - unconditional display of first page
				vo =(TiledView)(getCurrNDPage().getChild(tds1.getVoName()));
				tds1.setCurrPageNdx(0);
				vo.resetTileIndex();
				tds1.setTotalRowsChanged(false);
			}

			//Added DocTracking Conditions -- By BILLY 23April2002
			if (tds4.isTotalRowsChanged())
			{
				// number of rows has changed - unconditional display of first page
				vo =(TiledView)(getCurrNDPage().getChild(tds4.getVoName()));
				tds4.setCurrPageNdx(0);
				vo.resetTileIndex();
				tds4.setTotalRowsChanged(false);
			}

			//==============================================================================
			if (tds2.isTotalRowsChanged())
			{
				// number of rows has changed - unconditional display of first page
				vo =(TiledView)(getCurrNDPage().getChild(tds2.getVoName()));
				tds2.setCurrPageNdx(0);
				vo.resetTileIndex();
				tds2.setTotalRowsChanged(false);
			}
			if (tds3.isTotalRowsChanged())
			{
				vo =(TiledView)(getCurrNDPage().getChild(tds3.getVoName()));
				tds3.setCurrPageNdx(0);
				vo.resetTileIndex();
				tds3.setTotalRowsChanged(false);
			}
		}
		catch(Exception ex)
		{
			setStandardFailMessage();
			logger.error("Condition Review . Problem encountered during Data Object running ");
			logger.error(ex.getMessage());
		}
		setDDCUpdate(true);
	}


	/**
	 *
	 *
	 */
	public void handleSubmit()
	{
		handleSubmit(false);
	}

	/**
	 *
	 *
	 */
	public void handleSubmit(boolean isResolveDeal)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();

		if (pg.getPageCondition4() == true)
		{
			switch(pg.getPageCriteriaId1())
			{
				case ALERT_LABEL_ABSENT :
				{
					setActiveMessageToAlert(BXResources.getSysMsg("CONDITION_REVIEW_DOCUMENT_LABEL_ABSENT", theSessionState.getLanguageId()),
              ActiveMsgFactory.ISCUSTOMCONFIRM);
					break;
				}
				case ALERT_TEXT_ABSENT :
				{
					setActiveMessageToAlert(BXResources.getSysMsg("CONDITION_REVIEW_DOCUMENT_TEXT_ABSENT", theSessionState.getLanguageId()),
              ActiveMsgFactory.ISCUSTOMCONFIRM);
					break;
				}
			}
			return;
		}

		if (isResolveDeal)
		{
			handleResolveDealSubmit(false, false);
		}
		else
		{
			handleSubmitStandard();
		}
	}

	// Override framework method to save current inputs on the Condition Review screen
	public void saveData(PageEntry pg, boolean calledInTransaction)
	{
		try
		{
			SessionResourceKit srk = getSessionResourceKit();
			pg.setPageCondition4(false);
			pg.setPageCriteriaId1(0);
			if (! calledInTransaction) srk.beginTransaction();
			updateDefaultConditionStatus(pg, srk);

			// Added DocTracking Conditions -- By BILLY 23April2002
			updateDocConditionStatus(pg, srk);
			updateStandardConditionStatus(pg, srk);

			//Duplicate Default Conditions
			String DDCprop = PropertiesCache.getInstance().getProperty(theSessionState.
					getDealInstitutionId(),Xc.DUPLICATE_DEFAULT_CONDITIONS, "Y");

			if (DDCprop.equalsIgnoreCase("Y")
					&& isFromUWWS()
					&& getDDCUpdate() )
				updateFreeFormConditionsDDC(pg,srk);
			else 
				updateFreeFormCondition(pg, srk);

			setDDCUpdate(true);
			
			pg.setModified(true);
			if (! calledInTransaction) srk.commitTransaction();
		}
		catch(Exception e)
		{
			if (! calledInTransaction) srk.cleanTransaction();
			logger.error("Condition Review@saveData");
			logger.error(e);
			setStandardFailMessage();
			return;
		}

	}


	/**
	 *
	 *
	 */
	public boolean checkDisplayThisRow(String cursorName)
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			PageCursorInfo tds =(PageCursorInfo) pst.get(cursorName);
			if (tds.getTotalRows() <= 0) return false;
			return true;
		}
		catch(Exception e)
		{
			logger.error("Condition review@checkDisplayThisRow");
			logger.error(e);
			return false;
		}
	}

	/**
	 *
	 *
	 */
	private void updateDefaultConditionStatus(PageEntry pg, SessionResourceKit srk)
		throws Exception
	{
		updateConditionStatus(pg, srk, "Repeated1/hdDefDocTrackId", "Repeated1/cbDefAction");
	}


	// Added DocTracking Conditions -- By BILLY 23April2002

	private void updateDocConditionStatus(PageEntry pg, SessionResourceKit srk)
		throws Exception
	{
		updateConditionStatus(pg, srk, "Repeated4/hdDocDocTrackId", "Repeated4/cbDocAction");
	}


	/**
	 *
	 *
	 */
	private void updateStandardConditionStatus(PageEntry pg, SessionResourceKit srk)
		throws Exception
	{
		updateConditionStatus(pg, srk, "Repeated2/hdStdDocTrackId", "Repeated2/cbStdAction");
	}


	/**
	 *
	 *
	 */
	private void updateConditionStatus(PageEntry pg, SessionResourceKit srk, String indexField, String statusField)
		throws Exception
	{
		int statusId = 0;
		int docTrackId = 0;
		int copyId = pg.getPageDealCID();

		Vector hdDocTrackId = this.getRepeatedFieldValues(indexField);
		Vector cbStatus = this.getRepeatedFieldValues(statusField);

		if(hdDocTrackId != null && hdDocTrackId.size() > 0)
		{
			// There are multiple row values in this display field
			int vectorSize =hdDocTrackId.size();
			for(int i = 0;	i < vectorSize;	i ++)
			{
        docTrackId = com.iplanet.jato.util.TypeConverter.asInt(hdDocTrackId.get(i), -1);
				if (docTrackId != -1)
				{
					DocumentTracking docTrack = new DocumentTracking(srk, docTrackId, copyId);
					if (cbStatus.get(i) != null)
					{
						statusId = com.iplanet.jato.util.TypeConverter.asInt(cbStatus.get(i));
						docTrack.setDocumentStatusId(statusId);
					}
					docTrack.ejbStore();
				}
			}
		}
	}


	/**
	 *
	 *
	 */
  /**
	public void updateFreeFormCondition(PageEntry pg, SessionResourceKit srk)
		throws Exception
	{
		int docTrackid = 0;
		int actionId = 0;
		int roleId = 0;
		int copyId = pg.getPageDealCID();

		Vector hdCusDocTrackId = getRepeatedFieldValues("Repeated3/hdCusDocTrackId");
		Vector tbCusCondition = getRepeatedFieldValues("Repeated3/tbCusCondition");
		Vector tbCusDocumentLabel = getRepeatedFieldValues("Repeated3/tbCusDocumentLabel");
		Vector cbCusAction = getRepeatedFieldValues("Repeated3/cbCusAction");
		Vector cbCusResponsibility = getRepeatedFieldValues("Repeated3/cbCusResponsibility");

		if(hdCusDocTrackId != null && hdCusDocTrackId.size() > 0)
		{
			// There are multiple row values in this display field
			int vectorSize = hdCusDocTrackId.size();
			for(int i = 0; i < vectorSize; i ++)
			{
        docTrackid = com.iplanet.jato.util.TypeConverter.asInt(hdCusDocTrackId.get(i), -1);
				if (docTrackid == -1) continue;

				//--Release2.1--//
        //// The Product Team requires the creation of Custom Condition in both languages.
        DocumentTracking freeForm = new DocumentTracking(srk, docTrackid, copyId);

				String documentText = com.iplanet.jato.util.TypeConverter.asString(tbCusCondition.get(i));

				//--Release2.1--//
        //// The Product Team requires the creation of Custom Condition in both languages.
				if (documentText != null && documentText.trim().length() > 0)
				{
					freeForm.setDocumentTextForLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH, documentText);
				}
				else
				{
          //--Release2.1--//
          //// The Product Team requires the creation of Custom Condition in both languages.
          freeForm.setDocumentTextForLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH, " ");

					pg.setPageCondition4(true);
					pg.setPageCriteriaId1(ALERT_TEXT_ABSENT);
				}

				String documentLabel = com.iplanet.jato.util.TypeConverter.asString(tbCusDocumentLabel.get(i));
        //--Release2.1--//
        //// The Product Team requires the creation of Custom Condition in both languages.
				if (documentLabel != null && documentLabel.trim().length() > 0)
				{
					freeForm.setDocumentLabelForLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH, documentLabel);
				}
				else
				{
					freeForm.setDocumentLabelForLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH, " ");

					pg.setPageCondition4(true);
					pg.setPageCriteriaId1(ALERT_LABEL_ABSENT);
				}

				if(com.iplanet.jato.util.TypeConverter.asInt(cbCusAction.get(i), -1) == -1)
				{
					actionId = com.iplanet.jato.util.TypeConverter.asInt(cbCusAction.get(i));
          //--Release2.1--//
          //// The Product Team requires the creation of Custom Condition in both languages.
					freeForm.setDocumentStatusId(actionId);
				}
				if(com.iplanet.jato.util.TypeConverter.asInt(cbCusResponsibility.get(i), -1) == -1)
				{
					roleId = com.iplanet.jato.util.TypeConverter.asInt(cbCusResponsibility.get(i));
          //--Release2.1--//
          //// The Product Team requires the creation of Custom Condition in both languages.
					freeForm.setDocumentResponsibilityRoleId(roleId);

					setResponsibleId(srk, pg, freeForm);
				}
        //--Release2.1--//
        //// The Product Team requires the creation of Custom Condition in both languages.
				freeForm.ejbStore();
			} // for
		} // else
		return;
	}
  **/

///////////////////////

	public void updateFreeFormConditionsDDC(PageEntry pg, SessionResourceKit srk)
		throws Exception
	{
		int docTrackid = 0;
		int actionId = 0;
		int roleId = 0;
		int copyId = pg.getPageDealCID();
		String useDefault = "N";
		String displayDefault = "N";

		Vector hdCusDocTrackId = getRepeatedFieldValues("Repeated3/hdCusDocTrackId");
		Vector tbCusCondition = getRepeatedFieldValues("Repeated3/tbCusCondition");
		Vector chUseDefaultCondition = getRepeatedFieldValues("Repeated3/chUseDefaultCondition");
		Vector hdDisplayDefaultCondition = getRepeatedFieldValues("Repeated3/hdDisplayDefaultCondition");
		Vector tbCusDocumentLabel = getRepeatedFieldValues("Repeated3/tbCusDocumentLabel");
		Vector cbCusAction = getRepeatedFieldValues("Repeated3/cbCusAction");
		Vector cbCusResponsibility = getRepeatedFieldValues("Repeated3/cbCusResponsibility");

		if(hdCusDocTrackId != null && hdCusDocTrackId.size() > 0)
		{
			// There are multiple row values in this display field
			int vectorSize = hdCusDocTrackId.size();
			for(int i = 0; i < vectorSize; i ++)
			{
				docTrackid = com.iplanet.jato.util.TypeConverter.asInt(hdCusDocTrackId.get(i), -1);
				if (docTrackid == -1) continue;

				//--Release2.1--//
				//// The Product Team requires the creation of Custom Condition in both languages.
				DocumentTracking freeForm = new DocumentTracking(srk, docTrackid, copyId);

				/* Duplicate Conditions work...
				 * Check the value of the Use Default Condition checkbox.
				 * If Y, retrieve english and french text from conditionverbiage table, and save to doctracking,
				 * also set DocumentTrackingSourceId to system.
				 * Otherwise set text from screen to both english and french rows of doctrackin.
				 */
				String englishText;
				String frenchText;
				String englishLabel;
				String frenchLabel;
				
				//Get the use default checkbox value.
				useDefault = com.iplanet.jato.util.TypeConverter.asString(chUseDefaultCondition.get(i));
				if(null == useDefault) useDefault = "N";
				//checkbox values are defined as "Y" and "N" in viewbean
//				if (chUseDefaultCondition.get(i) != null && chUseDefaultCondition.get(i).toString().equalsIgnoreCase("Y")) {
				if ("Y".equalsIgnoreCase(useDefault)) {
					//we want to use the text from the default condition
					freeForm.setDocumentTrackingSourceId(Mc.DOCUMENT_TRACKING_SOURCE_SYSTEM_GENERATED);

					//restore the bilinguality (yes, I know that's not a word) by fetching both languages from DB
					JdbcExecutor jExec = srk.getJdbcExecutor();

					englishText = " ";
					frenchText = " ";
					frenchLabel = " ";
					englishLabel = " ";

					int key = jExec.execute("select conditiontext,conditionlabel from conditionverbiage where languagepreferenceid = 0 and conditionid = " + freeForm.getConditionId());
					while(jExec.next(key))
					{
						englishText = jExec.getString(key, 1); 
						englishLabel = jExec.getString(key, 2);
						break;
					}
					jExec.closeData(key);
					
					if (englishText == null) englishText = " ";
					freeForm.setDocumentTextForLanguage(0, englishText); 

					if (englishLabel == null) englishLabel = " ";
					freeForm.setDocumentLabelForLanguage(0, englishLabel); 
					
					
					//doing this in two distinct fetches because some lenders might not have french conditions
					key = jExec.execute("select conditiontext,conditionlabel from conditionverbiage where languagepreferenceid = 1 and conditionid = " + freeForm.getConditionId());
					while(jExec.next(key))
					{
						frenchText = jExec.getString(key, 1);
						frenchLabel = jExec.getString(key, 2);
						break;
					}
					jExec.closeData(key);
					
					if (frenchText == null) frenchText = " ";
					freeForm.setDocumentTextForLanguage(1, frenchText); 
					
					if (frenchLabel == null) frenchLabel = " ";
					freeForm.setDocumentLabelForLanguage(1, frenchLabel); 

				}
				else {
					//keep using the custom text, both english and french records get the same text, as always
					
					englishText = (String)(tbCusCondition.get(i));
					String documentLabel = (String)(tbCusDocumentLabel.get(i));

					if (englishText != null && englishText.trim().length() > 0
							&& documentLabel != null && documentLabel.trim().length() > 0) {
						freeForm.setDocumentTextForLanguage(0, englishText); //// English
						freeForm.setDocumentTextForLanguage(1, englishText); //// French
						freeForm.setDocumentLabelForLanguage(0, documentLabel); //// English
						freeForm.setDocumentLabelForLanguage(1, documentLabel); //// French
					}
					else {
						freeForm.setDocumentTextForLanguage(0, " "); ////English
						freeForm.setDocumentTextForLanguage(1, " "); //// French
						freeForm.setDocumentLabelForLanguage(0, " "); //// English
						freeForm.setDocumentLabelForLanguage(1, " ");  //// French
						
						pg.setPageCondition4(true);
						pg.setPageCriteriaId1(ALERT_TEXT_ABSENT);
					}
                }
				// end of Duplicate Condition
				// keep action and role from screen, even when taking default.
				
				actionId = com.iplanet.jato.util.TypeConverter.asInt(cbCusAction.get(i));
				freeForm.setDocumentStatusId(actionId);

				roleId = com.iplanet.jato.util.TypeConverter.asInt(cbCusResponsibility.get(i));
				freeForm.setDocumentResponsibilityRoleId(roleId);

				setResponsibleId(srk, pg, freeForm);
				
				useDefault = com.iplanet.jato.util.TypeConverter.asString(chUseDefaultCondition.get(i));
				freeForm.setUseDefaultChecked(useDefault);
				
				displayDefault = com.iplanet.jato.util.TypeConverter.asString(hdDisplayDefaultCondition.get(i));
				freeForm.setDisplayDefault(displayDefault);

				freeForm.ejbStore();

			} // for
		} // else
		return;
	}

	public void updateFreeFormCondition(PageEntry pg, SessionResourceKit srk)
	throws Exception
	{
		int docTrackid = 0;
		int actionId = 0;
		int roleId = 0;
		int copyId = pg.getPageDealCID();
	
		Vector hdCusDocTrackId = getRepeatedFieldValues("Repeated3/hdCusDocTrackId");
		Vector tbCusCondition = getRepeatedFieldValues("Repeated3/tbCusCondition");
		Vector tbCusDocumentLabel = getRepeatedFieldValues("Repeated3/tbCusDocumentLabel");
		Vector cbCusAction = getRepeatedFieldValues("Repeated3/cbCusAction");
		Vector cbCusResponsibility = getRepeatedFieldValues("Repeated3/cbCusResponsibility");
	
		if(hdCusDocTrackId != null && hdCusDocTrackId.size() > 0)
		{
			// There are multiple row values in this display field
			int vectorSize = hdCusDocTrackId.size();
			for(int i = 0; i < vectorSize; i ++)
			{
	    docTrackid = com.iplanet.jato.util.TypeConverter.asInt(hdCusDocTrackId.get(i), -1);
				if (docTrackid == -1) continue;
	
				//--Release2.1--//
	    //// The Product Team requires the creation of Custom Condition in both languages.
	    DocumentTracking freeForm = new DocumentTracking(srk, docTrackid, copyId);
	
				String documentText = (String)(tbCusCondition.get(i));
	
	    //logger.debug("CRH@FreeText: " + documentText);
				//--Release2.1--//
	    //// The Product Team requires the creation of Custom Condition in both languages.
				if (documentText != null && documentText.trim().length() > 0)
				{
					////freeFormEngl.setDocumentTextForLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH, documentText);
	      freeForm.setDocumentTextForLanguage(0, documentText); //// English
					freeForm.setDocumentTextForLanguage(1, documentText); //// French
				}
				else
				{
	      //--Release2.1--//
	      //// The Product Team requires the creation of Custom Condition in both languages.
	      ////freeForm.setDocumentTextForLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH, " ");
	      freeForm.setDocumentTextForLanguage(0, " "); ////English
	      freeForm.setDocumentTextForLanguage(1, " "); //// French
	
					pg.setPageCondition4(true);
					pg.setPageCriteriaId1(ALERT_TEXT_ABSENT);
				}
	
				String documentLabel = (String)(tbCusDocumentLabel.get(i));
	    //--Release2.1--//
	    //// The Product Team requires the creation of Custom Condition in both languages.
				if (documentLabel != null && documentLabel.trim().length() > 0)
				{
					////freeForm.setDocumentLabelForLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH, documentLabel);
					freeForm.setDocumentLabelForLanguage(0, documentLabel); //// English
					freeForm.setDocumentLabelForLanguage(1, documentLabel); //// French
				}
				else
				{
					////freeForm.setDocumentLabelForLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH, " ");
					freeForm.setDocumentLabelForLanguage(0, " "); //// English
					freeForm.setDocumentLabelForLanguage(1, " ");  //// French
	
					pg.setPageCondition4(true);
					pg.setPageCriteriaId1(ALERT_LABEL_ABSENT);
				}
				//// IMPORTANT!!! Wrong iMT auto-convertion
				////if(com.iplanet.jato.util.TypeConverter.asInt(cbCusAction.get(i), -1) == -1)
				////{
					actionId = com.iplanet.jato.util.TypeConverter.asInt(cbCusAction.get(i));
	      freeForm.setDocumentStatusId(actionId);
	
				////}
				//// IMPORTANT!!! Wrong iMT auto-convertion
				////if(com.iplanet.jato.util.TypeConverter.asInt(cbCusResponsibility.get(i), -1) == -1)
				////{
					roleId = com.iplanet.jato.util.TypeConverter.asInt(cbCusResponsibility.get(i));
	      freeForm.setDocumentResponsibilityRoleId(roleId);
	
					setResponsibleId(srk, pg, freeForm);
				////}
	
				freeForm.ejbStore();
	
			} // for
		} // else
		return;
	}
	/**
	 *
	 *
	 */
  //--Release2.1--//
  //// Original method for the creation of custom conditions (with one record with
  //// custom preference language in the DocumentTrackingVerbiage table. Product
  //// Team requests now to create a custom condition in a style of Standard condition
  //// (two records in the DocumentTrackingVerbiage table for both languages).
  //// Submit action on update of the documentText should propagate this change
  //// to the BOTH records (English and French) nevertheless what the session language is
  //// it at the moment.
  //// The method below provides this new logic. This method SHOULD STAY here since
  //// it will be renamed and used for the new TD's ChangeRequest (joining Condition
  //// and DocTracking screens).
  /**
	public void addFreeFormCondition()
	{
		SessionResourceKit srk = getSessionResourceKit();
		PageEntry pg = getTheSessionState().getCurrentPage();

		try
		{
			PageCursorInfo tds = getPageCursorInfo(pg, "Repeated3");

			// Modified to handle multiple selections -- by BILLY 24July2001
			ListBox theStdConditionCtl =(ListBox)(getCurrNDPage().getChild("lbCustCondition"));
			Object theSelectedConds[] = theStdConditionCtl.getValues();
			if (theSelectedConds.length <= 0)
			{
				setActiveMessageToAlert(BXResources.getSysMsg("CONDITION_REVIEW_ADD_TYPE", theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMCONFIRM);
				return;
			}

			//logger.debug("BILLY ==> the number of conditions selected = " + theSelectedConds.size ());
			// Add all selected Conditions
      int conditionId = 0;
      for(int i = 0; i < theSelectedConds.length;	i ++)
			{
				conditionId = com.iplanet.jato.util.TypeConverter.asInt(theSelectedConds[i], 0);

				logger.trace("BILLY ===> Cust conditionId " + "(" + i + ") = " + conditionId + "(" + theStdConditionCtl.getOptions().getValueLabel(conditionId+"") + ")");
				srk.beginTransaction();

        //--Release2.1--//
        //// The Product Team requires the creation of Custom Condition in both languages.
        DocumentTracking freeForm = new DocumentTracking(srk);

				ConditionHandler ch = new ConditionHandler(srk);

        int sessionLangId = theSessionState.getLanguageId();
        int oppositeLangId = 1; // default is French since default Session LangId is English:0

        if (sessionLangId == 0)
           oppositeLangId = 1;
        else if (sessionLangId == 1)
           oppositeLangId = 0;

				if (conditionId == 0)
				{
					logger.debug("CRH@AddFreeFormCondition::BuildFreeFormConditionMode");
          ch.buildFreeFormCondition(new DealPK(pg.getPageDealId(), pg.getPageDealCID()), " ", " ", sessionLangId
          ch.buildFreeFormConditionOppLang(new DealPK(pg.getPageDealId(), pg.getPageDealCID()), " ", " ", oppositeLangId);
				}
				else
				{
					// Copy from Standard condition
					Condition condition = new Condition(srk, conditionId);

          logger.debug("CRH@AddFreeFormCondition::ConditionId: " + conditionId);

					////int langId = com.iplanet.jato.util.TypeConverter.asInt(getCurrNDPage().getDisplayFieldValue("cbLanguagePreference"));
					////ch.buildFreeFormCondition(new DealPK(pg.getPageDealId(), pg.getPageDealCID()), condition.getConditionTextByLanguage(langId), condition.getConditionLabelByLanguage(langId));

					ch.buildFreeFormCondition(new DealPK(pg.getPageDealId(), pg.getPageDealCID()),
                                        conditionEngl.getConditionTextByLanguage(0),
                                        conditionEngl.getConditionLabelByLanguage(0),
                                        sessionLangId);

				}

				tds.setTotalRows(tds.getTotalRows() + 1);
				tds.setTotalRowsChanged(true);
				srk.commitTransaction();
			}
			// for loop

			return;
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Condition Review.Add Free Form Got Error!!!");
			logger.error(e);
			setStandardFailMessage();
			return;
		}

	}
**/
//////////////////////

  //--Release2.1--//
  //// Original method for the creation of custom conditions (with one record with
  //// custom preference language in the DocumentTrackingVerbiage table. Product
  //// Team requests now to create a custom condition in a style of Standard condition
  //// (two records in the DocumentTrackingVerbiage table for both languages).
  //// Submit action on update of the documentText should propagate this change
  //// to the BOTH records (English and French) nevertheless what the session language is
  //// it at the moment.
  //// The method below provides this new logic. This method SHOULD STAY here since
  //// it will be renamed and used for the new TD's ChangeRequest (joining Condition
  //// and DocTracking screens).
	public void addFreeFormCondition()
	{
		SessionResourceKit srk = getSessionResourceKit();
		PageEntry pg = getTheSessionState().getCurrentPage();

		try
		{
			PageCursorInfo tds = getPageCursorInfo(pg, "Repeated3");

			// Modified to handle multiple selections -- by BILLY 24July2001
			ListBox theStdConditionCtl =(ListBox)(getCurrNDPage().getChild("lbCustCondition"));
			Object theSelectedConds[] = theStdConditionCtl.getValues();
			if (theSelectedConds.length <= 0)
			{
				setActiveMessageToAlert(BXResources.getSysMsg("CONDITION_REVIEW_ADD_TYPE", theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMCONFIRM);
				return;
			}

			logger.debug("CRH@The number of conditions selected = " + theSelectedConds.length);
			// Add all selected Conditions
			int conditionId = 0;

			Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());

      for(int i = 0; i < theSelectedConds.length;	i ++)
			{
				conditionId = com.iplanet.jato.util.TypeConverter.asInt(theSelectedConds[i], 0);

        logger.trace("CRH@Custom conditionId " + "(" + i + ") = " +conditionId + "(" +
                    theStdConditionCtl.getOptions().getValueLabel(conditionId+"") + ")");

        srk.beginTransaction();

			  //DocumentTracking freeForm = new DocumentTracking(srk);

	 		  ConditionHandler ch = new ConditionHandler(srk);
        if(conditionId == 0)
        {
			    ch.buildFreeFormCondition(conditionId, new DealPK(pg.getPageDealId(),pg.getPageDealCID())," "," ");
        }
			  else
        {
          // Copy from Standard condition
          Condition condition = new Condition(srk, conditionId);

          ////int langId = theSessionState.getLanguageId();
          //--Release2.1--TD_DTS_CR--//
          //// Based on the User choice system now should propagate the condition text
          //// for two languages.
          int langId = new Integer(getCurrNDPage().getDisplayFieldValue("cbLanguagePreference").toString()).intValue();

          ch.buildFreeFormCondition(conditionId, new DealPK(pg.getPageDealId(), pg.getPageDealCID()),
            condition.getConditionTextByLanguage(langId), condition.getConditionLabelByLanguage(langId));
        }

			  tds.setTotalRows(tds.getTotalRows() + 1);

			  tds.setTotalRowsChanged(true);

			  srk.commitTransaction();
      } // for loop

			return;
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Condition Review.Add Free Form Got Error!!!");
			logger.error(e);
			setStandardFailMessage();
			return;
		}

	}

	/**
	 *
	 *
	 */
	public void addStandardCondition()
	{
		SessionResourceKit srk = getSessionResourceKit();

		PageEntry pg = getTheSessionState().getCurrentPage();

		try
		{
			PageCursorInfo tds = getPageCursorInfo(pg, "Repeated2");

			// Modified to handle multiple selections -- by BILLY 24July2001
			ListBox theStdConditionCtl =(ListBox)(getCurrNDPage().getChild("lbStdCondition"));
			Object theSelectedConds[] = theStdConditionCtl.getValues();
			if (theSelectedConds.length <= 0)
			{
				setActiveMessageToAlert(BXResources.getSysMsg("CONDITION_REVIEW_ADD_TYPE", theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMCONFIRM);
				return;
			}

			//logger.debug("BILLY ==> the number of conditions selected = " + theSelectedConds.size ());
			// Add all selected Conditions
			int conditionId = 0;

			// Preparation for the common data
			Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
			int sourceOfBusinessCategory = deal.getSourceOfBusinessProfileId();
			int applicationId = com.iplanet.jato.util.TypeConverter.asInt(pg.getPageDealApplicationId().trim());
			for(int i = 0;	i < theSelectedConds.length; i ++)
			{
				conditionId = com.iplanet.jato.util.TypeConverter.asInt(theSelectedConds[i], 0);

        logger.trace("BILLY ===> Std conditionId " + "(" + i + ") = " +conditionId + "(" + theStdConditionCtl.getOptions().getValueLabel(conditionId+"") + ")");
				if(conditionId == 0)	continue;

				srk.beginTransaction();
				Condition condition = new Condition(srk, conditionId);
				DocumentTracking dt = new DocumentTracking(srk);
				dt.create(condition, pg.getPageDealId(), pg.getPageDealCID(), Mc.DOC_STATUS_OPEN, Mc.DOCUMENT_TRACKING_SOURCE_USER_SELECTED);

				//About  Responsibility Role Id :
				// after create() it's been assigned by Condition ResponsibilityRoleId
				// Standard Condition got some additional logic:
				int roleId = dt.getDocumentResponsibilityRoleId();

				//logger.trace("-------- Role Id" +roleId + "(" + i + ")");
				if (roleId == Mc.CRR_SOURCE_OF_BUSINESS)
				{
					if (sourceOfBusinessCategory == Mc.SOB_CATEGORY_TYPE_DIRECT || sourceOfBusinessCategory == Mc.SOB_CATEGORY_TYPE_INTERNET)
					{
						dt.setDocumentResponsibilityRoleId(Mc.CRR_USER_TYPE_APPLICANT);

						//logger.trace("--------- USER_TYPE_APPLICANT" + "(" + i + ")");
					}
				}
				dt.setApplicationId(applicationId);
				setResponsibleId(srk, pg, dt);
				dt.ejbStore();
				srk.commitTransaction();
				tds.setTotalRows(tds.getTotalRows() + 1);
				tds.setTotalRowsChanged(true);
			}

			// for loop
			return;
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setStandardFailMessage();
			logger.error(e);
			return;
		}

	}


	/**
	 *
	 *
	 */
	public int setResponsibleId(SessionResourceKit srk, PageEntry pg, DocumentTracking dt)
	{
		int responsibleId = - 1;

		try
		{
			switch(dt.getDocumentResponsibilityRoleId())
			{
				case Mc.CRR_USER_TYPE_APPLICANT :
				{
					Borrower borrower = new Borrower(srk, null);
					ArrayList borList =(ArrayList) borrower
											.findByDeal(new DealPK(pg.getPageDealId(), 
																   pg.getPageDealCID()));
					responsibleId =((Borrower) borList.get(0)).getBorrowerId();
					break;
				}
				// SEAN Ticket #2497 Dec 09, 2005:
				// comment out  because we done have these values in database.
				//case Mc.CRR_USER_TYPE_JR_ADMIN : case Mc.CRR_USER_TYPE_ADMIN : case Mc.CRR_USER_TYPE_SR_ADMIN :
				//{
				//	Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
				//	responsibleId = deal.getAdministratorId();
				//	break;
				//}
				// delete CRR_USER_TYPE_JR_UNDERWRITER and CRR_USER_TYPE_SR_UNDERWRITER, beause we only have
				// CRR_USER_TYPE_UNDERWRITER in database
				case Mc.CRR_USER_TYPE_UNDERWRITER :
				{
					Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
					responsibleId = deal.getUnderwriterUserId();
					break;
				}
				// comment out  because we done have these values in database.
				//case Mc.CRR_USER_TYPE_SUPERVISOR : case Mc.CRR_USER_TYPE_BRANCH_MGR : case Mc.CRR_USER_TYPE_SENIOR_MGR :
				//{
				//	Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
				//	UserProfile up = new UserProfile(srk);
				//	up.findByPrimaryKey(new UserProfileBeanPK(deal.getUnderwriterUserId()));
				//	responsibleId = up.getManagerId();
				//	break;
				//}
				// SEAN Ticket #2497 END
				case Mc.CRR_PARTY_TYPE_SOLICITOR :
				{
					PartyProfile pp = new PartyProfile(srk);
					pp.setSilentMode(true);
					pp.findByDealSolicitor(pg.getPageDealId());
					responsibleId = pp.getPartyProfileId();
					break;
				}
				case Mc.CRR_PARTY_TYPE_APPRAISER :
				{
					PartyProfile pp = new PartyProfile(srk);
					ArrayList arrayList =(ArrayList) pp.findByDealAndType(new DealPK(pg.getPageDealId(), 
                   pg.getPageDealCID()), Mc.PARTY_TYPE_APPRAISER);
					if (arrayList.size() > 0) responsibleId =((PartyProfile) arrayList.get(0)).getPartyProfileId();
					break;
				}
				case Mc.CRR_SOURCE_OF_BUSINESS :
				{
					Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
					responsibleId = deal.getSourceOfBusinessProfileId();
					break;
				}
			}

			//end of switch
			dt.setDocumentResponsibilityId(responsibleId);
			return responsibleId;
		}
		catch(FinderException fe)
		{
			logger.warning("ConditionReview: " +
        BXResources.getSysMsg("CONDITION_REVIEW_RESPONSIBLE_NOT_FOUND", 0) +
        ": DealId=" + pg.getPageDealId() + "; CopyId=" + pg.getPageDealCID());

			// As requested by Product, not to display any error messages -- By Billy 02May2001
			//setActiveMessageToAlert(Sc.CONDITION_REVIEW_RESPONSIBLE_NOT_FOUND,ActiveMsgFactory.ISCUSTOMCONFIRM);
			// Assign to a PartyId which should never exists
			dt.setDocumentResponsibilityId(- 1);
			return - 1;
		}
		catch(Exception e)
		{
			logger.error("Exception @ConditionReviewHandler.getResponsibleId: Locating responsible party, DealId=" + pg.getPageDealId() + "; CopyId=" + pg.getPageDealCID() + " - ignored");
			logger.error(e);
			return - 1;
		}

	}


	/**
	 *
	 *
	 */
	public void handleDelete(int pageRowNdx, String sourceName)
	{
		PageEntry pg = theSessionState.getCurrentPage();

		if (disableEditButton(pg) == true) return;

		String args =(sourceName + "|" + pageRowNdx);

		setActiveMessageToAlert(BXResources.getSysMsg("CONDITION_REVIEW_DELETE", theSessionState.getLanguageId()),
      ActiveMsgFactory.ISCUSTOMDIALOG, args);

	}


	/**
	 *
	 *
	 */
	public void handleCustomActMessageOk(String[] args)
	{
		PageEntry pg = theSessionState.getCurrentPage();
		SessionResourceKit srk = getSessionResourceKit();
		SysLogger logger = srk.getSysLogger();

                //--CervusPhaseII--start--//
                ViewBean thePage = getCurrNDPage();

                if (args [ 0 ].equals("OVERRIDE_DEAL_LOCK_YES"))
                {
                  logger.trace("CRH@handleCustomActMessageOk_REJECT_COND_YES: Override deal lock");
                  provideOverrideDealLockActivity(pg, srk, thePage);
                  return;
                }

                if (args [ 0 ].equals("OVERRIDE_DEAL_LOCK_NO"))
                {
                  logger.trace("CRH@handleCustomActMessageOk_REJECT_COND_NO: Return to previous screen");

                  theSessionState.setActMessage(null);
                  theSessionState.overrideDealLock(true);
                  handleCancelStandard(false);

                  return;
                }
                //--CervusPhaseII--end--//

		if (args [ 0 ].equals("COMMITMENT_REQ_YES"))
		{
			try
			{
				// User requested to send the Commitment Letter and Broker Upload
				CCM ccm = new CCM(srk);
				Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
				srk.beginTransaction();
				deal.setCommitmentProduced("N");
				ccm.requestCommitmentDoc(srk, deal, false);
				ccm.requestElectronicResponseToBroker(srk, deal);
				deal.ejbStore();
				navigateToNextPage();
				srk.commitTransaction();
			}
			catch(Exception e)
			{
				srk.cleanTransaction();
				logger.error("Exception encountered @ConditionReviewHandler.handleCustomActMessageOk() - while requesting for Commitment documents.");
				logger.error(e);
				setStandardFailMessage();
			}
			return;
		}
		else if (args [ 0 ].equals("COMMITMENT_REQ_NO"))
		{
			// Commitment Letter and Broker Upload not required
			try
			{
				srk.beginTransaction();
				navigateToNextPage();
				srk.commitTransaction();
			}
			catch(Exception e)
			{
				// Shouldn't happen !!
				srk.cleanTransaction();
				logger.error("Exception encountered @ConditionReviewHandler.handleCustomActMessageOk() - ignore.");
				logger.error(e);
			}
			return;
		}
		else if (args [ 0 ].equals("COMMITMENT_REQ_YES_RESOLVE"))
		{
			handleResolveDealSubmit(true, true);
			return;
		}
		else if (args [ 0 ].equals("COMMITMENT_REQ_NO_RESOLVE"))
		{
			handleResolveDealSubmit(true, false);
			return;
		}

		StringTokenizer tk = new StringTokenizer(args [ 0 ], "|");

		String sourceName = tk.nextToken();

		int pageRowNdx = com.iplanet.jato.util.TypeConverter.asInt(tk.nextToken());

		if (sourceName.equals("btStdDeleteCondition"))
		{
			deleteStandardCondition(pageRowNdx);
 			//// SYNCADD.
 			//Added Target to reload the page -- By Billy 07June2002
 			theSessionState.getCurrentPage().setLoadTarget(TARGET_CONREVIEW_STD);
			return;
		}

		if (sourceName.equals("btCusDeleteCondition"))
		{
			deleteCustomCondition(pageRowNdx);
 			//// SYNCADD.
 			//Added Target to reload the page -- By Billy 07June2002
 			theSessionState.getCurrentPage().setLoadTarget(TARGET_CONREVIEW_CUST);
			return;
		}

		return;

	}


	/**
	 *
	 *
	 */
	public void deleteStandardCondition(int rowIndex)
	{
		deleteCondition(2, rowIndex);

	}


	/**
	 *
	 *
	 */
	public void deleteCustomCondition(int rowIndex)
	{
		deleteCondition(3, rowIndex);

	}


	/**
	 *
	 *
	 */
	private void deleteCondition(int sectionIndex, int rowIndex)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();

		int docTrackId = 0;

		try
		{
			SessionResourceKit srk = getSessionResourceKit();
			Hashtable pst = pg.getPageStateTable();
			PageCursorInfo tds = getPageCursorInfo(pg, "Repeated" + sectionIndex);
			Vector docTrackObj = this.getRepeatedFieldValues("Repeated" + sectionIndex + "/hd" +(sectionIndex == 2 ? "Std" : "Cus") + "DocTrackId");
			if (docTrackObj == null || docTrackObj.size() <= 0)
			{
				logger.trace("---------- DELETE ID is NULL");
				return;
			}
			String docTrackString = null;
			docTrackString = (String)(docTrackObj.get(rowIndex).toString());
      logger.debug("BILLY ===> @deleteCondition : rowIndex = " + rowIndex + " : docTrackString = " + docTrackString);
			docTrackId = com.iplanet.jato.util.TypeConverter.asInt(docTrackObj.get(rowIndex), -1);
			if(docTrackId == -1)
			{
				logger.trace("------------NON_NUMERIC");
				return;
			}

			srk.beginTransaction();
			srk.setInterimAuditOn(false); //4.4 Transaction History
			DocumentTracking dt = new DocumentTracking(srk, docTrackId, pg.getPageDealCID());
			dt.ejbRemove();
			srk.restoreAuditOn(); //4.4 Transaction History
			srk.commitTransaction();
			tds.setTotalRows(tds.getTotalRows() - 1);
			tds.setTotalRowsChanged(true);
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setStandardFailMessage();
			logger.error("Problem encountered removing document track: " + "deal Id = " + pg.getPageDealId() + ", copy Id = " + pg.getPageDealCID() + ", doc track Id = " + docTrackId);
			logger.error(e);
			return;
		}

	}


	/**
	 *
	 *
	 */
	public void handleAdd(String sourceName)
	{
		PageEntry pg = theSessionState.getCurrentPage();

		if (disableEditButton(pg) == true) return;

		if (sourceName.equals("btStdAddCondition"))
		{
			addStandardCondition();
 			//// SYNCADD.
 			//Added Target to reload the page -- By Billy 07June2002
 			theSessionState.getCurrentPage().setLoadTarget(TARGET_CONREVIEW_STD);
		}

		if (sourceName.equals("btCusAddCondition"))
		{
			addFreeFormCondition();
 			//// SYNCADD.
 			//Added Target to reload the page -- By Billy 07June2002
 			theSessionState.getCurrentPage().setLoadTarget(TARGET_CONREVIEW_CUST);
		}
		
		return;
	}


	/**
	 *
	 *
	 */
	public void handleSubmitStandard()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		SessionResourceKit srk = getSessionResourceKit();

		if (validateData(pg, srk) == false) return;

		// an active and/or passive message will have been set in this case
		try
		{
			srk.beginTransaction();
			standardAdoptTxCopy(pg, true);

			//New requirement -- if Deal.status = CommitOffered (13) or ConditionOut (16)
			//                   Then update the DocumentDuedate and Status as we does when
			//                   Approving the deal.  -- By Billy 23July2001
			Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
			CalcMonitor dcm = CalcMonitor.getMonitor(srk);
			deal.setCalcMonitor(dcm);
			int theStatusId = deal.getStatusId();
			if (theStatusId == Mc.DEAL_COMMIT_OFFERED || theStatusId == Mc.DEAL_CONDITIONS_OUT || theStatusId == Mc.DEAL_PRE_APPROVAL_OFFERED)
			{
				// update document tracking conditions
				ConditionHandler cd = new ConditionHandler(srk);
				cd.updateForPDecision(deal);

				// Prompt User if produce a commitment
				setActiveMessageToAlert(BXResources.getSysMsg("CONDITION_REVIEW_POST_DECISION_COMMITMENT_REQ", theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMDIALOG2, "COMMITMENT_REQ_YES", "COMMITMENT_REQ_NO");

				//=============================================================================
				// Manually triger the calc 60 (DocumentDueDate)
				//  Should be some other way to do this !!! -- Just a quick and dirty fix !!!
				//    by BILLY 25July2001
				ChangeMapValue changeMapValue =(ChangeMapValue)(deal.getChangeValueMap().get("returnDate"));
				if (changeMapValue != null)
				{
					// Set the changed flag to force re-Calc of DocumentDueDate (Calc60)
					changeMapValue.setChangeFlag(true);
				}
				dcm.inputEntity(deal);
				doCalculation(dcm);

				//==============================================================================
	            insertConditionUpdateRequest(pg);
				srk.commitTransaction();
				return;
			}
			// this function is applicable only for to the deal in post-approval status (statusId >= 11)
			if (theStatusId>=Mc.DEAL_APPROVED){
			    insertConditionUpdateRequest(pg);
			}
			navigateToNextPage();
			srk.commitTransaction();
			
			
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Condition Review.handleSubmitStandard");
			logger.error(e);
			setStandardFailMessage();
			return;
		}

	}


	/**
	 *
	 *
	 */
	public boolean validateData(PageEntry pe, SessionResourceKit srk)
	{
		return true;
	}

  //--TD_CONDR_CR--start--//
	/**
	 *
	 *
	 */
	public void navigateToDetailDocTrackStyle(int sectionIndex, int rowNdx)
	{
		logger.trace("ConditionReviewHandler@navigateToDetailDocTrackingStyle, rowNdxIn: " + rowNdx);

		String section = null;
		String docTrackIdName = null;
		String hdRoleId = null;
		String hdRequestDate = null;
		String hdStatusDate = null;
		String cbDocStatus = null;
		String hdDocText = null;
		String hdRoleStr = null;
		String hdDocLabel = null;
		String hdCopyId = null;

		try
		{
			PageEntry pg = theSessionState.getCurrentPage();
			SessionResourceKit srk = getSessionResourceKit();

			if (sectionIndex == SECTION_DEFAULT_CONDITIONS)
			{
				section = "Repeated1";
				docTrackIdName = "Repeated1/hdDefDocTrackId";
				hdRoleId = "Repeated1/hdDefRoleId";
				hdRequestDate = "Repeated1/hdDefRequestDate";
				hdStatusDate = "Repeated1/hdDefStatusDate";
				cbDocStatus = "Repeated1/cbDefAction";
				hdDocText = "Repeated1/hdDefDocText";
				hdRoleStr = "Repeated1/hdDefResponsibility";
				hdDocLabel = "Repeated1/hdDefDocumentLabel";
				hdCopyId = "Repeated1/hdDefCopyId";
			}
			else if (sectionIndex == this.SECTION_STANDARD_CONDITIONS)
			{
				section = "Repeated2";
				docTrackIdName = "Repeated2/hdStdDocTrackId";
				hdRoleId = "Repeated2/hdStdRoleId";
				hdRequestDate = "Repeated2/hdStdRequestDate";
				hdStatusDate = "Repeated2/hdStdStatusDate";
				cbDocStatus = "Repeated2/cbStdAction";
				hdDocText = "Repeated2/hdStdDocText";
				hdRoleStr = "Repeated2/hdStdResponsibility";
				hdDocLabel = "Repeated2/hdStdDocumentLabel";
				hdCopyId = "Repeated2/hdStdCopyId";
			}

			PageCursorInfo tds = getPageCursorInfo(pg, section);
			Hashtable pst = pg.getPageStateTable();

      //// Do not allow to edit page in View Only mode.
      if (pg.isEditable() == false)
      {
        setStandardViewOnlyMessage();
        return;
      }

			if (tds.getTotalRows() <= 0)
        return;

			rowNdx = getCursorAbsoluteNdx(tds, rowNdx);

			logger.debug("CRH@navigateToDetailDocTrackStyle::AbsoluteNdx: " + rowNdx + ", Section: " + section);

			String roleId = getRepeatedField((hdRoleId).toString(), rowNdx).toString();

			if (roleId == null || roleId.equals("")) {
			  return;
			}

            logger.debug("CRH@navigateToDetail::RoleId: " + roleId); // Catherine, 14-Jul-05, fix for #1715

			PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_DOC_TRACKING_DETAIL, true);
			Hashtable subPst = pgEntry.getPageStateTable();
			String docTrackId = getRepeatedField((docTrackIdName).toString(), rowNdx).toString();
            logger.debug("CRH@navigateToDetail::DocTrackId: " + docTrackId);

			subPst.put("dfDocTrackId", new Integer(getIntValue(docTrackId)));
			subPst.put("dfRoleId", new Integer(getIntValue(roleId)));

      String reqDate = "";
      if(hdRequestDate == null || hdRequestDate.equals("")) {
        reqDate = getRepeatedField((hdRequestDate).toString(), rowNdx).toString();
        logger.debug("CRH@navigateToDetailDocTrackStyle::reqDate: " + reqDate);
      } else {
        reqDate = "";
      }

			String statusDate = "";
      if(hdStatusDate == null || hdStatusDate.equals("")) {
        statusDate = getRepeatedField((hdStatusDate).toString(), rowNdx).toString();
        logger.debug("CRH@navigateToDetailDocTrackStyle::StatusDate: " + statusDate);
      } else {
        statusDate = "";
      }

			subPst.put("dfRequestDate", reqDate);
			subPst.put("dfStatusDate", statusDate);

      //--Release2.1--TD_DTS_CR--start//
      Vector docDetails = null;
      if (pst.get("DOCDETAILS") == null)
          docDetails = new Vector();
      else
          docDetails = (Vector) pst.get("DOCDETAILS");

      DocumentWrapper dw = new DocumentWrapper();
      dw.setDocTrackingName(docTrackId);
      dw.setTiledIndexForDocId(rowNdx);
      dw.setProductId(new Integer(docTrackId).intValue());

      docDetails.add(dw);

      pst.put("DOCDETAILS", docDetails);
      //--Release2.1--TD_DTS_CR--end//

			int statusId = 0;

      String statusField = getRepeatedField((cbDocStatus).toString(), rowNdx).toString();
      //logger.debug("CRH@navigateToDetailDocTrackStyle::StatusField: " + statusField);

      statusId = new Integer(statusField).intValue();
      logger.debug("CRH@navigateToDetailDocTrackStyle::StatusId: " + statusId);

      doDocStatusModelImpl statusDo = (doDocStatusModelImpl)
                                      (RequestManager.getRequestContext().getModelManager().getModel(doDocStatusModel.class));

			statusDo.clearUserWhereCriteria();
			statusDo.addUserWhereCriterion("dfDocStatusId", "=", new Integer(statusId));

      //--Release2.1--start//
      //// Setup current displaying LanguageID
      ////statusDo.addUserWhereCriterion("dfLanguagePrederenceId", "=",
      ////    new Integer(theSessionState.getLanguageId()));

      ////logger.debug("DTH@setupBeforePageGeneration::The SQL : " + statusDo.getSelectSQL());
      //--Release2.1--end//

      String statusLabel = "";

      try
      {
        ResultSet rs = statusDo.executeSelect(null);

        if(rs == null || statusDo.getSize() <= 0)
        {
          logger.debug("CRH@navigateToDetailDocTrackStyle::No row returned!!");
          statusLabel = "";
        }
        else if(statusDo.getSize() > 0)
        {
           Object oStatLabel = statusDo.getValue(statusDo.FIELD_DFDOCSTATUSDESC);
           //logger.debug("CRH@navigateToDetailDocTrackStyle::StatLabel: " +  oStatLabel.toString());
        }
      }
      catch (ModelControlException mce)
      {
        logger.debug("CRH@navigateToDetailDocTrackStyle::MCEException: " + mce);
      }

      catch (SQLException sqle)
      {
        logger.debug("CRH@navigateToDetailDocTrackStyle::SQLException: " + sqle);
      }

      subPst.put("dfDocStatus", statusLabel);

			subPst.put("dfDocText", getRepeatedField((hdDocText).toString(), rowNdx).toString());
			subPst.put("dfRoleStr", getRepeatedField((hdRoleStr).toString(), rowNdx).toString());
			subPst.put("dfDocLabel", getRepeatedField((hdDocLabel).toString(), rowNdx).toString());
			subPst.put("dfCopyId", new Integer(getRepeatedField((hdCopyId).toString(), rowNdx).toString()));

			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage(true);
		}
		catch(Exception e)
		{
			setStandardFailMessage();
			logger.error("Exception @CRH@navigateToDetailDocTrackStyle");
			logger.error(e);
		}
	}
  //--TD_CONDR_CR--end--//

	/**
	 *
	 *
	 */
	public void navigateToDetail(int sectionIndex, int rowNdx)
	{
		String section = null;

		String docTrackIdName = null;

		try
		{
			if (sectionIndex == 1)
			{
				section = "Repeated1";
				docTrackIdName = "Repeated1/hdDefDocTrackId";
			}
			else if (sectionIndex == 2)
			{
				section = "Repeated2";
				docTrackIdName = "Repeated2/hdStdDocTrackId";

				//// SYNCADD.
				//Added Target to reload the page -- By Billy 07June2002
				theSessionState.getCurrentPage().setLoadTarget(TARGET_CONREVIEW_STD);
			}
			//// SYNCADD.
			//Split Signed Doc conditions and make it protected -- by BILLY 13June2002
			else if (sectionIndex == 4)  // is 4 (DocTracking Conditions)
			{
				section = "Repeated4";
				docTrackIdName = "Repeated4/hdDocDocTrackId";
			}
			else  // is 5 (Signed DocTracking Conditions)
			{
				section = "Repeated5";
				docTrackIdName = "Repeated5/hdSignDocDocTrackId";
			}

			PageEntry pg = getTheSessionState().getCurrentPage();
			PageCursorInfo tds = getPageCursorInfo(pg, section);
			Hashtable pst = pg.getPageStateTable();
			if (tds.getTotalRows() <= 0) return;
			rowNdx = getCursorAbsoluteNdx(tds, rowNdx);
			PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_CONDITIONS_DETAIL, true);
			String docTrackId = this.getRepeatedField(docTrackIdName, rowNdx);
			if (docTrackId == null)
			{
				return;
			}
			if (docTrackId.equals(""))
			{
				return;
			}
			Hashtable subPst = pgEntry.getPageStateTable();
			subPst.put("dfDocTrackId", docTrackId);
			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage(true);
		}
		catch(Exception e)
		{
			setStandardFailMessage();
			logger.error("Exception @ConditionsReviewHandler.navigateToDetail");
			logger.error(e);
		}

	}


	// ================================================================================
	// New requirement to add Resolve Deal Button -- Billy 12Nov2001
	/** setupDealResolutionPageEntry:
  *
  * Return a PageEntry for Deal Resolution Screen.
  *
  * Notes: If worksheet used in task orientation an attempt is made to find a Deal Resolution
  *        page entry in the task links (would expect it to immediately follow the worksheet -
  *        however, if not found a new PAgeEntry is created.
  *
  **/
	private PageEntry setupDealResolutionPageEntry(PageEntry pg)
	{
		PageEntry dealResPg = null;

		PageEntry pgTemp = pg;

		while(pgTemp.getNextPE() != null)
		{
			pgTemp = pgTemp.getNextPE();
			if (pgTemp.getPageId() == Mc.PGNM_DEAL_RESOLUTION)
			{
				dealResPg = pgTemp;
				break;
			}
		}

		if (dealResPg == null)
		{
			// make it!
			Page pgProperties = getPage(Sc.PGNM_DEAL_RESOLUTION);

			// setup page entry
			dealResPg = new PageEntry();
			dealResPg.setPageId(pgProperties.getPageId());
			dealResPg.setPageName(pgProperties.getPageName());
			dealResPg.setPageLabel(pgProperties.getPageLabel());
			dealResPg.setDealPage(pgProperties.getDealPage());
			dealResPg.setPageTaskId(0);
			dealResPg.setPageTaskSequence(0);
		}

		dealResPg.setPageDealId(pg.getPageDealId());

		dealResPg.setPageDealCID(pg.getPageDealCID());

		dealResPg.setPageDealApplicationId(pg.getPageDealApplicationId());

		dealResPg.setPageMosUserId(pg.getPageMosUserId());

		dealResPg.setPageMosUserTypeId(pg.getPageMosUserTypeId());
        
        dealResPg.setDealInstitutionId(pg.getDealInstitutionId());


		// Set Current page to non-sub page, otherwise, it will bring you back to the worksheet
		pg.setSubPage(false);


		// mark the page to indicate transfer from undrwriter worksheet
		Hashtable pst = dealResPg.getPageStateTable();

		if (pst == null)
		{
			pst = new Hashtable();
			dealResPg.setPageStateTable(pst);
		}

		// Deal Resolution will not allow an approval resolution unless it finds this key
		pst.put("RESOLVE_FROM_UWORKSHEET", "WHATEVER");

		return dealResPg;

	}


	/**
	 *
	 *
	 */
	public void handleResolveDealSubmit(boolean isConfirmed, boolean isCommitmentRequested)
	{
		logger.trace("--T--> @ConditionsReviewHandler.handleResolveDealSubmit:");

		PageEntry pg = theSessionState.getCurrentPage();

		SessionResourceKit srk = getSessionResourceKit();

		SysLogger logger = srk.getSysLogger();

		try
		{
			Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
			if (isConfirmed == false)
			{
				if (validateData(pg, srk) == false) return;

				// an active and/or passive message will have been set in this case
				// first ensure selected scenario is allowed
				logger.trace("@ConditionsReviewHandler.handleResolveDealSubmitlveDeal: ensure resolve allowed (deal status)");
				DealStatusManager dsm = DealStatusManager.getInstance(srk);
				int cs = deal.getStatusId();
				int statusCat = dsm.getStatusCategory(cs, srk);
				if (statusCat != Mc.DEAL_STATUS_CATEGORY_DECISION || cs == Mc.DEAL_APPROVED)
				{
					setActiveMessageToAlert(BXResources.getSysMsg("UW_DEAL_RESOLUTION_NOT_ALLOWED_STATUS_CATEGORY", theSessionState.getLanguageId()),
              ActiveMsgFactory.ISCUSTOMCONFIRM);
					return;
				}
				logger.trace("@ConditionsReviewHandler.handleResolveDealSubmit: ensure resolve allowed (scenario unlocked)");

				// selection of a locked scenario only allowed for external approval review/request
				if (deal.getScenarioLocked() != null && deal.getScenarioLocked().equals("Y") &&(!(cs == Mc.DEAL_EXTERNAL_APPROVAL_REVIEW || cs == Mc.DEAL_HIGHER_APPROVAL_REQUESTED)))
				{
					setActiveMessageToAlert(BXResources.getSysMsg("UW_CANNOT_RESOLVE_LOCKED_SCENARIO", theSessionState.getLanguageId()),
              ActiveMsgFactory.ISCUSTOMCONFIRM);
					return;
				}

				//========================================================================================================
				// Do the normal Stuffs as submit button does
				srk.beginTransaction();

				// force to drop to the level of the UW page (Parent page)
				standardAdoptTxCopy(pg, true);
				standardAdoptTxCopy(pg.getParentPage(), true);

				//New requirement -- if Deal.status = CommitOffered (13) or ConditionOut (16)
				//                   Then update the DocumentDuedate and Status as we does when
				//                   Approving the deal.  -- By Billy 23July2001
				CalcMonitor dcm = CalcMonitor.getMonitor(srk);
				deal.setCalcMonitor(dcm);
				int theStatusId = deal.getStatusId();
				if (theStatusId == Mc.DEAL_COMMIT_OFFERED || theStatusId == Mc.DEAL_CONDITIONS_OUT  || theStatusId == Mc.DEAL_PRE_APPROVAL_OFFERED)
				{
					// update document tracking conditions
					ConditionHandler cd = new ConditionHandler(srk);
					cd.updateForPDecision(deal);

					// Prompt User if produce a commitment
					setActiveMessageToAlert(BXResources.getSysMsg("CONDITION_REVIEW_POST_DECISION_COMMITMENT_REQ", theSessionState.getLanguageId()),
              ActiveMsgFactory.ISCUSTOMDIALOG2, "COMMITMENT_REQ_YES_RESOLVE", "COMMITMENT_REQ_NO_RESOLVE");

					//=============================================================================
					// Manually triger the calc 60 (DocumentDueDate)
					//  Should be some other way to do this !!! -- Just a quick and dirty fix !!!
					//    by BILLY 25July2001
					ChangeMapValue changeMapValue =(ChangeMapValue)(deal.getChangeValueMap().get("returnDate"));
					if (changeMapValue != null)
					{
						// Set the changed flag to force re-Calc of DocumentDueDate (Calc60)
						changeMapValue.setChangeFlag(true);
					}
					dcm.inputEntity(deal);
					doCalculation(dcm);
					srk.commitTransaction();
					return;
				}

				//========================================================================================================
			}

			//if Confirmed
			// Confirmed -- continue to the next step
			if (isConfirmed == true) srk.beginTransaction();

			// User requested to send the Commitment Letter and Broker Upload
			CCM ccm = new CCM(srk);

			// Check if Commitment Letter requested
			if (isCommitmentRequested == true)
			{
				deal.setCommitmentProduced("N");
				ccm.requestCommitmentDoc(srk, deal, false);
				ccm.requestElectronicResponseToBroker(srk, deal);
			}
			if (deal.getScenarioRecommended() == null || ! deal.getScenarioRecommended().equals("Y"))
			{
				ccm.setAsRecommendedScenario(deal);
			}
			deal.ejbStore();
			logger.trace("--T--> @ConditionsReviewHandler.handleResolveDealSubmit: to deal resolution");

			// navigate to Deal Resolution
			PageEntry dealResolutionPg = setupDealResolutionPageEntry(pg);
			getSavedPages().setNextPage(dealResolutionPg);
			srk.commitTransaction();
			navigateToNextPage(true);
			return;
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Exception @ConditionsReviewHandler.handleResolveDealSubmit().");
			logger.error(e);
		}


		// an exception has occured - ensure we navigate away from page since we can't guar
		// presence of a tx copy on regeneration
		navigateAwayFromFromPage(true);

	}


	/**
	 *
	 *
	 */
	public boolean displayResolveDealButton()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		if (pg.isEditable() == true)
		{
			// Check if Required to display the Resolve Deal utton from UW screen
			if (pg.getPageCondition5() == true) return true;
		}

		return false;
	}

  //--Release2.1--TD_DTS_CR--start//
	/**
	 *
	 *
	 */
	private void populateFieldValidation(ViewBean thePage)
	{
		String theExtraHtml = "";

		// the OnChange string to be set on the fields
		HtmlDisplayFieldBase df;

		//// Validation for the special characters ('<', '>', '"', '&' since
    //// they scrude up the document generation.
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated3/tbCusCondition"); // textarea
		theExtraHtml = "onBlur=\"isFieldHasSpecialChar();\"";
		df.setExtraHtml(theExtraHtml);

		df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated3/tbCusDocumentLabel"); // textbox
		theExtraHtml = "onBlur=\"isFieldHasSpecialChar();\"";
		df.setExtraHtml(theExtraHtml);
	}
   //--Release2.1--TD_DTS_CR--end//
	
	//Express4.2 GR - Condition Management
	private void insertConditionUpdateRequest(PageEntry pg) {
        logger.debug("@ConditionsReviewHandler.insertConditionUpdateRequest: Beginning to insert condition update request.");
        try {
            ConditionUpdateRequester requester = new ConditionUpdateRequester(srk);
            
            int result = requester.insertConditionUpdateRequest(pg.getPageDealId(), true);
            if(result < -1) {
                logger.debug("@ConditionsReviewHandler.insertConditionUpdateRequest: Failed to insert condition update request.");
            }
            else {
                logger.debug("@ConditionsReviewHandler.insertConditionUpdateRequest: Inserted condition update request successfully.");
            }
        } catch (Exception e) {            
            setStandardFailMessage();
            logger.error(e);
            logger.error("@ConditionsReviewHandler.insertConditionUpdateRequest: Failed to retrieve deal id.");
            return;
        }   
    }
	
	public boolean isFromUWWS() {
		if(null == theSessionState) {
			logger.error("@ConditionsReviewHandler.isFromUWWS: handler is not properly initialized");
			return false;
		}
		return theSessionState.getCurrentPage().isSubPage();
	}
	
	public void setDDCUpdate(boolean b) {
		ddcUpdate = b;
	}
	
	public boolean getDDCUpdate() {
		return ddcUpdate;
	}
	
}

 //--Release2.1--TD_DTS_CR--start//
class DocumentWrapper {
	     private String docTrackingName;
	     private int docTrackingId;
       private int tiledIndex;

   // "getters" methods.
    public String getDocTrackingName(){
        return docTrackingName;
    }
    public int getDocTrackingId(){
        return docTrackingId;
    }
    public int getTiledIndexForDocId(){
        return tiledIndex;
    }

   // "setters" methods.
    public void setDocTrackingName(String aDocTrackingName){
            docTrackingName = aDocTrackingName;
    }
    public void setProductId(int aDocTrackingId){
            docTrackingId = aDocTrackingId;
    }
    public void setTiledIndexForDocId(int aTiledIndex){
            tiledIndex = aTiledIndex;
    }
   
}
 //--Release2.1--TD_DTS_CR--end//
