package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;

import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.TextField;

/**
 *
 *
 *
 */
public class pgApplicantEntryRepeatedCreditReferenceTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgApplicantEntryRepeatedCreditReferenceTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(100);
		setPrimaryModelClass( doApplicantCreditRefsSelectModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getHdCreditRefId().setValue(CHILD_HDCREDITREFID_RESET_VALUE);
		getHdCreditRefCopyId().setValue(CHILD_HDCREDITREFCOPYID_RESET_VALUE);
		getCbCreditRefsType().setValue(CHILD_CBCREDITREFSTYPE_RESET_VALUE);
		getTxReferenceDesc().setValue(CHILD_TXREFERENCEDESC_RESET_VALUE);
		getTxInstitutionName().setValue(CHILD_TXINSTITUTIONNAME_RESET_VALUE);
		getTxTimeWithRefYears().setValue(CHILD_TXTIMEWITHREFYEARS_RESET_VALUE);
		getTxTimeWithRefMonths().setValue(CHILD_TXTIMEWITHREFMONTHS_RESET_VALUE);
		getTxAccountName().setValue(CHILD_TXACCOUNTNAME_RESET_VALUE);
		getTxCurrentBalance().setValue(CHILD_TXCURRENTBALANCE_RESET_VALUE);
		getBtDeleteCreditRef().setValue(CHILD_BTDELETECREDITREF_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_HDCREDITREFID,HiddenField.class);
		registerChild(CHILD_HDCREDITREFCOPYID,HiddenField.class);
		registerChild(CHILD_CBCREDITREFSTYPE,ComboBox.class);
		registerChild(CHILD_TXREFERENCEDESC,TextField.class);
		registerChild(CHILD_TXINSTITUTIONNAME,TextField.class);
		registerChild(CHILD_TXTIMEWITHREFYEARS,TextField.class);
		registerChild(CHILD_TXTIMEWITHREFMONTHS,TextField.class);
		registerChild(CHILD_TXACCOUNTNAME,TextField.class);
		registerChild(CHILD_TXCURRENTBALANCE,TextField.class);
		registerChild(CHILD_BTDELETECREDITREF,Button.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By Billy 05Nov2002
    ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

    //Set up all ComboBox options
    cbCreditRefsTypeOptions.populate(getRequestContext());

    handler.pageSaveState();
		//========================================================================
		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated RepeatedCreditReference_onBeforeRowDisplayEvent code here
      int rowNum = this.getTileIndex();
      boolean ret = true;

      ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
      handler.pageGetState(this.getParentViewBean());

      // Modified by BILLY 16Jan2001
      // Get Data Object
      doApplicantCreditRefsSelectModelImpl theObj =(doApplicantCreditRefsSelectModelImpl)getdoApplicantCreditRefsSelectModel();

      if (theObj.getNumRows() > 0) handler.setApplicantCreditRefDisplayFields(theObj, rowNum);
      else ret = false;

      handler.pageSaveState();

      return ret;
		}
		return movedToRow;
	}

	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
    return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_HDCREDITREFID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantCreditRefsSelectModel(),
				CHILD_HDCREDITREFID,
				doApplicantCreditRefsSelectModel.FIELD_DFCREDITREFID,
				CHILD_HDCREDITREFID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDCREDITREFCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantCreditRefsSelectModel(),
				CHILD_HDCREDITREFCOPYID,
				doApplicantCreditRefsSelectModel.FIELD_DFCOPYID,
				CHILD_HDCREDITREFCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBCREDITREFSTYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantCreditRefsSelectModel(),
				CHILD_CBCREDITREFSTYPE,
				doApplicantCreditRefsSelectModel.FIELD_DFCREDITREFERENCETYPEID,
				CHILD_CBCREDITREFSTYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbCreditRefsTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXREFERENCEDESC))
		{
			TextField child = new TextField(this,
				getdoApplicantCreditRefsSelectModel(),
				CHILD_TXREFERENCEDESC,
				doApplicantCreditRefsSelectModel.FIELD_DFREFERENCEDESC,
				CHILD_TXREFERENCEDESC_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXINSTITUTIONNAME))
		{
			TextField child = new TextField(this,
				getdoApplicantCreditRefsSelectModel(),
				CHILD_TXINSTITUTIONNAME,
				doApplicantCreditRefsSelectModel.FIELD_DFINSTITUTIONNAME,
				CHILD_TXINSTITUTIONNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXTIMEWITHREFYEARS))
		{
			TextField child = new TextField(this,
				//getDefaultModel(),
        getdoApplicantCreditRefsSelectModel(),
				CHILD_TXTIMEWITHREFYEARS,
				CHILD_TXTIMEWITHREFYEARS,
				CHILD_TXTIMEWITHREFYEARS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXTIMEWITHREFMONTHS))
		{
			TextField child = new TextField(this,
				//getDefaultModel(),
        getdoApplicantCreditRefsSelectModel(),
				CHILD_TXTIMEWITHREFMONTHS,
				CHILD_TXTIMEWITHREFMONTHS,
				CHILD_TXTIMEWITHREFMONTHS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXACCOUNTNAME))
		{
			TextField child = new TextField(this,
				getdoApplicantCreditRefsSelectModel(),
				CHILD_TXACCOUNTNAME,
				doApplicantCreditRefsSelectModel.FIELD_DFACCOUNTNUMBER,
				CHILD_TXACCOUNTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXCURRENTBALANCE))
		{
			TextField child = new TextField(this,
				getdoApplicantCreditRefsSelectModel(),
				CHILD_TXCURRENTBALANCE,
				doApplicantCreditRefsSelectModel.FIELD_DFCURRENTBALANCE,
				CHILD_TXCURRENTBALANCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETECREDITREF))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETECREDITREF,
				CHILD_BTDELETECREDITREF,
				CHILD_BTDELETECREDITREF_RESET_VALUE,
				null);
				return child;

		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdCreditRefId()
	{
		return (HiddenField)getChild(CHILD_HDCREDITREFID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdCreditRefCopyId()
	{
		return (HiddenField)getChild(CHILD_HDCREDITREFCOPYID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbCreditRefsType()
	{
		return (ComboBox)getChild(CHILD_CBCREDITREFSTYPE);
	}

	/**
	 *
	 *
	 */
	static class CbCreditRefsTypeOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbCreditRefsTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 21Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "CREDITREFTYPE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public TextField getTxReferenceDesc()
	{
		return (TextField)getChild(CHILD_TXREFERENCEDESC);
	}


	/**
	 *
	 *
	 */
	public TextField getTxInstitutionName()
	{
		return (TextField)getChild(CHILD_TXINSTITUTIONNAME);
	}


	/**
	 *
	 *
	 */
	public TextField getTxTimeWithRefYears()
	{
		return (TextField)getChild(CHILD_TXTIMEWITHREFYEARS);
	}


	/**
	 *
	 *
	 */
	public TextField getTxTimeWithRefMonths()
	{
		return (TextField)getChild(CHILD_TXTIMEWITHREFMONTHS);
	}


	/**
	 *
	 *
	 */
	public TextField getTxAccountName()
	{
		return (TextField)getChild(CHILD_TXACCOUNTNAME);
	}


	/**
	 *
	 *
	 */
	public TextField getTxCurrentBalance()
	{
		return (TextField)getChild(CHILD_TXCURRENTBALANCE);
	}


	/**
	 *
	 *
	 */
	public void handleBtDeleteCreditRefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		// The following code block was migrated from the btDeleteCreditRef_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDelete(false, 9, handler.getRowNdxFromWebEventMethod(event), "RepeatedCreditReference.hdCreditRefId", "creditReferenceId", "RepeatedCreditReference.hdCreditRefCopyId", "CreditReference");

		handler.postHandlerProtocol();

		return PROCEED;
		*/
   ApplicantEntryHandler handler = (ApplicantEntryHandler)this.handler.cloneSS();
   handler.preHandlerProtocol(this.getParentViewBean());
   handler.handleDelete(false, 9, handler.getRowNdxFromWebEventMethod(event),
                      "RepeatedCreditReference/hdCreditRefId", "creditReferenceId",
                      "RepeatedCreditReference/hdCreditRefCopyId", "CreditReference");
   handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtDeleteCreditRef()
	{
		return (Button)getChild(CHILD_BTDELETECREDITREF);
	}


	/**
	 *
	 *
	 */
	public String endBtDeleteCreditRefDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btDeleteCreditRef_onBeforeHtmlOutputEvent method
		ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public doApplicantCreditRefsSelectModel getdoApplicantCreditRefsSelectModel()
	{
		if (doApplicantCreditRefsSelect == null)
			doApplicantCreditRefsSelect = (doApplicantCreditRefsSelectModel) getModel(doApplicantCreditRefsSelectModel.class);
		return doApplicantCreditRefsSelect;
	}


	/**
	 *
	 *
	 */
	public void setdoApplicantCreditRefsSelectModel(doApplicantCreditRefsSelectModel model)
	{
			doApplicantCreditRefsSelect = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_HDCREDITREFID="hdCreditRefId";
	public static final String CHILD_HDCREDITREFID_RESET_VALUE="";
	public static final String CHILD_HDCREDITREFCOPYID="hdCreditRefCopyId";
	public static final String CHILD_HDCREDITREFCOPYID_RESET_VALUE="";
	public static final String CHILD_CBCREDITREFSTYPE="cbCreditRefsType";
	public static final String CHILD_CBCREDITREFSTYPE_RESET_VALUE="";
  //--Release2.1--//
  //private static CbCreditRefsTypeOptionList cbCreditRefsTypeOptions=new CbCreditRefsTypeOptionList();
	private CbCreditRefsTypeOptionList cbCreditRefsTypeOptions=new CbCreditRefsTypeOptionList();
	public static final String CHILD_TXREFERENCEDESC="txReferenceDesc";
	public static final String CHILD_TXREFERENCEDESC_RESET_VALUE="";
	public static final String CHILD_TXINSTITUTIONNAME="txInstitutionName";
	public static final String CHILD_TXINSTITUTIONNAME_RESET_VALUE="";
	public static final String CHILD_TXTIMEWITHREFYEARS="txTimeWithRefYears";
	public static final String CHILD_TXTIMEWITHREFYEARS_RESET_VALUE="";
	public static final String CHILD_TXTIMEWITHREFMONTHS="txTimeWithRefMonths";
	public static final String CHILD_TXTIMEWITHREFMONTHS_RESET_VALUE="";
	public static final String CHILD_TXACCOUNTNAME="txAccountName";
	public static final String CHILD_TXACCOUNTNAME_RESET_VALUE="";
	public static final String CHILD_TXCURRENTBALANCE="txCurrentBalance";
	public static final String CHILD_TXCURRENTBALANCE_RESET_VALUE="";
	public static final String CHILD_BTDELETECREDITREF="btDeleteCreditRef";
	public static final String CHILD_BTDELETECREDITREF_RESET_VALUE=" ";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doApplicantCreditRefsSelectModel doApplicantCreditRefsSelect=null;

  private ApplicantEntryHandler handler=new ApplicantEntryHandler();

  public SysLogger logger;

}

