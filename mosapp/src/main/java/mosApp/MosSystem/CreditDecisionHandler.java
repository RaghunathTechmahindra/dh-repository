package mosApp.MosSystem;

import java.text.*;
import java.util.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.commitment.CCM;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.workflow.*;
import com.basis100.workflow.entity.*;
import com.basis100.workflow.pk.*;
import com.basis100.entity.*;
import com.basis100.picklist.BXResources;
import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.jdbcservices.jdbcexecutor.*;
import com.basis100.deal.security.*;
import com.basis100.deal.util.DBA;
import com.basis100.deal.util.Parameters;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.filogix.externallinks.adjudication.util.*;
import com.filogix.externallinks.adjudication.requestprocessor.*;

import MosSystem.*;

import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.html.*;
import com.iplanet.jato.view.event.*;

/**
 * <p>Title: CreditDecisionHandler </p>
 *
 * <p>Description: Business logic handler for all credit decision related pages </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.0
 * @version 1.4 <br>
 * Date: 18/10/2006 <br>
 * Author: GCD Implementation Team<br>
 * Change: Updated PostedInterestRate field to PostedRate as per spec, defect #1031
 * @version 1.6<br>
 * Date: 10/20/2006 <br>
 * Change: Added request status Response Available to check before hiding Details button
 * on CD screen, defect #1300
 *
 */
public class CreditDecisionHandler
	extends PageHandlerCommon
	implements Cloneable, Sc {
	protected static final String goldCopy = "1";
	private static String responseId = "0";
	private static String requestId = "0";
	private static int numberOfBorrowers = 0;

	/**
	 *
	 *
	 */
	public CreditDecisionHandler cloneSS() {
		return (CreditDecisionHandler)super.cloneSafeShallow();

	}

	/** populatePageDisplayFields
	 * This method is used to populate the fields in the header
	 * with the appropriate information
	 *
	 * @param none<br>
	 * @return none<br>
	 * @version 1.1 <br>
	 * Date: 07/27/2006<br>
	 * Author: NBC/PP Implementation Team <br>
	 * Change: <br>
	 * 	Added call to specific method to populate the Credit Decision screen
	 */
	public void populatePageDisplayFields() {
		SessionStateModelImpl theSessionState = getTheSessionState();

		PageEntry pg = getTheSessionState().getCurrentPage();
		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");
		//Hashtable pst = pg.getPageStateTable();

		if (pg.getPageId() == Mc.PGNM_CREDIT_DECISION) {
			populatePageShellDisplayFields();
			populateTaskNavigator(pg);
			populatePageDealSummarySnapShot();
			populatePreviousPagesLinks();
			populatePageDisplayFieldsCreditDecision();

			// populate the response field if it's available.
			Vector borrowerIds = getRepeatedFieldValues(
				"Repeated1/hdApplicantId");
			for (int i = 0; i < borrowerIds.size(); i++) {
				setRepeatedField("Repeated1/hdApplicantResponseId", i,
								 responseId);
			}
			getCurrNDPage().setDisplayFieldValue("hdResponseId", responseId);
			getCurrNDPage().setDisplayFieldValue("hdRequestId", requestId);
		}

	}

	/** populatePageDisplayFieldsCreditDecision
	 * This method is used to customize the fields on the Credit Decision screen
	 *
	 * @param none<br>
	 * @return none<br>
	 */
	public void populatePageDisplayFieldsCreditDecision() {

		// All fields on this screen must be read-only / disabled
		// Decision Messages is read-only through the JSP, to allow use of scrollbars.
		closeAccessToTextBox("txGlobalCreditBureau");
		closeAccessToTextBox("txGlobalCreditBureauScore");
		closeAccessToTextBox("txGlobalRiskRating");
		closeAccessToTextBox("txGlobalInternalScore");
		closeAccessToTextBox("txCreditDecisionStatus");
		closeAccessToTextBox("txCreditDecision");
		closeAccessToTextBox("txLastUpdatedDate");
		closeAccessToTextBox("txPostedRate");
		closeAccessToTextBox("txActualPaymentTerm");
		closeAccessToTextBox("txAmortizationTerm");
		closeAccessToTextBox("txMonthlyPayment");
		closeAccessToTextBox("txProduct");
		closeAccessToTextBox("txDownPayment");
		closeAccessToTextBox("txLTV");
		closeAccessToTextBox("txLocation");
		closeAccessToTextBox("txOccupiedBy");
		closeAccessToTextBox("txPropertyType");
		closeAccessToTextBox("txMortgageInsurer");
		closeAccessToTextBox("txMIStatus");
		closeAccessToTextBox("txMIDecision");
		closeAccessToTextBox("txAmountInsured");
		closeAccessToTextBox("txPrimaryApplicantRisk");
		closeAccessToTextBox("txSecondaryApplicantRisk");
		closeAccessToTextBox("txMarketRisk");
		closeAccessToTextBox("txPropertyRisk");
		closeAccessToTextBox("txNeighbourhoodRisk");
		closeAccessToTextBox("txCombinedGDS");
		closeAccessToTextBox("txCombinedTDS");
		closeAccessToTextBox("txTotalNetWorth");
		closeAccessToTextBox("txTotalIncome");
		closeAccessToTextBox("txTotalOtherIncome");
		closeAccessToTextBox("txTotalCombinedIncome");

	}

	/**
	 * setupBeforePageGeneration
	 * 	Sets up the page, before the models are executed (calls the models)
	 *
	 */
	public void setupBeforePageGeneration() {
		PageEntry pg = theSessionState.getCurrentPage();

		try {
			if (pg.getSetupBeforeGenerationCalled() == true) {
				return;
			}

			pg.setSetupBeforeGenerationCalled(true);

			if (pg.getPageId() == Mc.PGNM_CD_APPLICANT_DETAILS) {

				setupBeforePageGenerationCDApplicantDetails();
			}
			if (pg.getPageId() == Mc.PGNM_CREDIT_DECISION) {

			  	setupDealSummarySnapShotDO(pg);

				// Add any custom model binding here
				//or and any field customization logic here
				// Setup cursor parameters here, if necessary
				//........................................
				// set the criteria for the populating data objects


				//(String) getCurrNDPage().getDisplayFieldValue("hdResponseId");
				String dealId = String.valueOf(pg.getPageDealId());
				String copyId = String.valueOf(pg.getPageDealCID());

				// AdjudicationMain: need dealid & copyid
				executeAdjudicationMainModel(pg, copyId, dealId);

				// ApplicantMain: need dealid & copyid
				executeApplicantMainModel(pg, copyId, dealId);

				//	ApplicantIncome: need dealid & copyid
				executeApplicantIncomeModel(pg, copyId, dealId);

				// set default value for responseId, requestId
				responseId = "0";
				requestId = "0";

				// AdjudicationResponse: need dealid & copyid
				executeAdjudicationResponseModel(pg, copyId, dealId);
//				logger.debug("CDH@setupBeforePageGeneration :: Displaying Credit Decision Page responseid " + responseId );

				// if responseId = 0, no response is available for this request.
				// the next two methods check for a responseId = 0 and clear the result set if this is true

				// ResponseDecisionMessages: need dealid & copyid & responseId
				executeResponseDecisionMessagesModel(pg, copyId, dealId,
					responseId);

				// ApplicantResponse: need dealid & copyid
				executeApplicantResponseModel(pg, copyId, dealId, responseId);

			}

		}
		catch (Exception e) {
			logger.error(
				"Exception @CreditDecisionHandler.setupBeforePageGeneration()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}

	}

//GCD start

	/**
	 * setupBeforePageGenerationCDApplicantDetails
	 *
	 * Setup for CD application details screen.
	 *
	 * @version 1.8 <br>
	 * Date: 11/02/2006 <br>
	 * Change: Retrieve copyid from page
	 */
	public void setupBeforePageGenerationCDApplicantDetails() {
		logger.trace(
			"--T--> @CreditDecisionHandler.setupBeforePageGeneration(CDApplicantDetails):");

		PageEntry pg = theSessionState.getCurrentPage();
		//Hashtable pst = pg.getPageStateTable();

		// borrowerid, copyid, applicantNumber, responseId
		String borowerId = String.valueOf(pg.getPageCriteriaId1());
		String copyId = String.valueOf(pg.getPageDealCID());
		String applicantNumberId = String.valueOf(pg.getPageCriteriaId2());
		String responseId = String.valueOf(pg.getPageCriteriaId3());

		doCDApplicantDetailsModel theDO = (doCDApplicantDetailsModel)
			RequestManager.getRequestContext().getModelManager().getModel(
			doCDApplicantDetailsModel.class);
		theDO.clearUserWhereCriteria();
		theDO.addUserWhereCriterion("dfResponseId", "=", new String(responseId));
		theDO.addUserWhereCriterion("dfApplicantNumber", "=",
									new String(applicantNumberId));
		theDO.addUserWhereCriterion("dfBorrowerId", "=", new String(borowerId));
		theDO.addUserWhereCriterion("dfCopyId", "=", new String(copyId));
		try {
			this.executeModel(theDO, "CDH@executeApplicantDetailsModel", true);
		}
		catch (Exception e) {
			logger.error("CDH@executeCDApplicantDetailsModelImpl:: exception " +
						 e);
		}

	}

	/**
	 * executeAdjudicationMainModel
	 * 	Executes model to retrieve deal data available prior to a GCD request
	 *
	 * @param pg PageEntry
	 * @param copyId String
	 * @param dealId String
	 */
	private void executeAdjudicationMainModel(PageEntry pg, String copyId,
											  String dealId) {
		doAdjudicationMainBNCModelImpl theDO = (doAdjudicationMainBNCModelImpl)
			(RequestManager.getRequestContext().getModelManager().getModel(
			doAdjudicationMainBNCModel.class));

		theDO.clearUserWhereCriteria();
		theDO.addUserWhereCriterion(theDO.FIELD_DFDEALID, "=",
									String.valueOf(dealId));
		theDO.addUserWhereCriterion(theDO.FIELD_DFCOPYID, "=",
									String.valueOf(copyId));

		this.executeModel(theDO, "CDH@executeAdjudicationMainModel", true);

	}

	/**
	 * executeAdjudicationResponseModel
	 * 	Executes model to retrieve deal data available after  a GCD request
	 *
	 * @param pg PageEntry
	 * @param copyId String
	 * @param dealId String
	 */
	private void executeAdjudicationResponseModel(PageEntry pg, String copyId,
												  String dealId) {

		doAdjudicationResponseBNCModelImpl theDO = (
			doAdjudicationResponseBNCModelImpl)
			(RequestManager.getRequestContext().getModelManager().getModel(
			doAdjudicationResponseBNCModel.class));

		theDO.clearUserWhereCriteria();
		theDO.addUserWhereCriterion(theDO.FIELD_DFDEALID, "=",
									String.valueOf(dealId));
		// SCR#1305 - start
		theDO.addUserWhereCriterion(theDO.FIELD_DFCOPYID, "=", String.valueOf(copyId));
		// SCR#1305 - end

		try {
			this.executeModel(theDO, "CDH@executeAdjudicationResponseModel", true);
			ResultSet rs = theDO.executeSelect(null);

			if (rs != null) {
			  	if (theDO.getSize() > 0) {
    				// set class variables request and response ids
    				java.math.BigDecimal responseIdTemp = theDO.getDfResponseId();
    				if (responseIdTemp != null) {
    					responseId = responseIdTemp.toString();
    				}
    				java.math.BigDecimal requestIdTemp = theDO.getDfRequestId();
    				if (requestIdTemp != null) {
    					requestId = requestIdTemp.toString();
    				}
			  	}
			}

		}
		catch (Exception e) {
			logger.error("CDH@executeAdjudicationResponseModel:: exception " +
						 e);
		}
	}

	/**
	 * executeResponseDecisionMessagesModel
	 * 	Executes model to retrieve deal data available after a GCD request
	 *
	 * @param pg PageEntry
	 * @param copyId String
	 * @param dealId String
	 */
	private void executeResponseDecisionMessagesModel(PageEntry pg,
		String copyId, String dealId, String responseId) {
		doAdjudicationResponseBNCDecisionMessagesModelImpl theDO = (
			doAdjudicationResponseBNCDecisionMessagesModelImpl)
			(RequestManager.getRequestContext().getModelManager().getModel(
			doAdjudicationResponseBNCDecisionMessagesModel.class));

		int responseIdInt = new Integer(responseId).intValue();
		if (responseIdInt == 0) {
			theDO.clear();
		}
		else {
			theDO.clearUserWhereCriteria();
			theDO.addUserWhereCriterion(theDO.FIELD_DFRESPONSEID, "=",
										responseId);

			try {
				this.executeModel(theDO,
								  "CDH@executeResponseDecisionMessagesModel", true);
			}
			catch (Exception e) {
				logger.error(
					"CDH@executeResponseDecisionMessagesModel:: exception " + e);
			}
		}
	}

	/**
	 * excuteApplicantMainModel
	 * 	Executes model to retrieve applicant data available prior to a request, used on a tiled view
	 *
	 * @param pg PageEntry
	 * @param copyId String
	 * @param dealId String
	 */
	private void executeApplicantMainModel(PageEntry pg, String copyId,
										   String dealId) {
		doAdjudicationApplicantMainBNCModelImpl theDO = (
			doAdjudicationApplicantMainBNCModelImpl)
			(RequestManager.getRequestContext().getModelManager().getModel(
			doAdjudicationApplicantMainBNCModel.class));

		theDO.clearUserWhereCriteria();
		theDO.addUserWhereCriterion(theDO.FIELD_DFDEALID, "=",
									String.valueOf(dealId));
		theDO.addUserWhereCriterion(theDO.FIELD_DFCOPYID, "=",
									String.valueOf(copyId));

		////===============================================================

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		Hashtable pst = pg.getPageStateTable();
		numberOfBorrowers = countBorrowers(srk, pg);

		if (tds == null) {

			// determine number of borrowers
			String voName = "Repeated1";
			int perPage = ( (TiledView) getCurrNDPage().getChild(voName)).
				getMaxDisplayTiles();

			// set up and save task display state object
			tds = new PageCursorInfo("DEFAULT", voName, perPage,
									 numberOfBorrowers);
			tds.setRefresh(true);

		}

		try {
			theDO.executeSelect(null);
			tds.setTotalRows(theDO.getSize());
		}
		catch (Exception ex) {
			setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
			logger.error("Problem encountered getting CreditDecisionHandler");
			logger.error(ex);
			//return;
		}

		// determine if we will force display of first page
		if (tds.isTotalRowsChanged()) {
			// number of rows has changed - unconditional display of first page
			TiledView vo = (TiledView) (getCurrNDPage().getChild(tds.getVoName()));
			tds.setCurrPageNdx(0);
			//vo.goToFirst();
			try {
				vo.resetTileIndex();
			}
			catch (ModelControlException mce) {
				logger.warning("CreditDecisionHandler@executeApplicantMainModel::Exception when resetTileIndex: " +
							   mce);
			}

			tds.setTotalRowsChanged(false);
		}
	}

	/**
	 * executeApplicantIncomeModel
	 * 	Executes model to retrieve applicant data available prior to a request, used on a tiled view
	 *
	 * @param pg PageEntry
	 * @param copyId String
	 * @param dealId String
	 */
	private void executeApplicantIncomeModel(PageEntry pg, String copyId,
											 String dealId) {
		doAdjudicationApplicantIncomeBNCModelImpl theDO = (
			doAdjudicationApplicantIncomeBNCModelImpl)
			(RequestManager.getRequestContext().getModelManager().getModel(
			doAdjudicationApplicantIncomeBNCModel.class));

		theDO.clearUserWhereCriteria();
		theDO.addUserWhereCriterion(theDO.FIELD_DFDEALID, "=",
									String.valueOf(dealId));
		theDO.addUserWhereCriterion(theDO.FIELD_DFCOPYID, "=",
									String.valueOf(copyId));

		////===============================================================

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		Hashtable pst = pg.getPageStateTable();

		if (tds == null) {

			// determine number of borrowers
			String voName = "Repeated1";
			int perPage = ( (TiledView) getCurrNDPage().getChild(voName)).
				getMaxDisplayTiles();

			// set up and save task display state object
			tds = new PageCursorInfo("DEFAULT", voName, perPage,
									 numberOfBorrowers);
			tds.setRefresh(true);

		}

		try {
			theDO.executeSelect(null);
			tds.setTotalRows(theDO.getSize());
		}
		catch (Exception ex) {
			setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
			logger.error("Problem encountered getting CreditDecisionHandler");
			logger.error(ex);
			//return;
		}

		// determine if we will force display of first page
		if (tds.isTotalRowsChanged()) {
			// number of rows has changed - unconditional display of first page
			TiledView vo = (TiledView) (getCurrNDPage().getChild(tds.getVoName()));
			tds.setCurrPageNdx(0);
			//vo.goToFirst();
			try {
				vo.resetTileIndex();
			}
			catch (ModelControlException mce) {
				logger.warning("CreditDecisionHandler@executeApplicantIncomeModel::Exception when resetTileIndex: " +
							   mce);
			}

			tds.setTotalRowsChanged(false);
		}
	}

	/**
	 * excuteApplicantResponseModel
	 * 	Executes model to retrieve applicant data availablle prior to a request, used on a tiled view
	 *
	 * @param pg PageEntry
	 * @param copyId String
	 * @param dealId String
	 */
	private void executeApplicantResponseModel(PageEntry pg, String copyId,
											   String dealId, String responseId) {
		doAdjudicationApplicantResponseBNCModelImpl theDO = (
			doAdjudicationApplicantResponseBNCModelImpl)
			(RequestManager.getRequestContext().getModelManager().getModel(
			doAdjudicationApplicantResponseBNCModel.class));

// Ticket#NBC216587 temporary fix
//  doAdjudicationApplicantResponseBNCModel is always executed by tiled view class.
//  if this method skips adding user criterion, this model will extract huge amount of data.
//  this fix requires reconsideration throughly	
// 		
//		int responseIdInt = new Integer(responseId).intValue();
//		if (responseIdInt == 0) {
//			theDO.clear();
//		}
//		else {
			theDO.clearUserWhereCriteria();
			theDO.addUserWhereCriterion(theDO.FIELD_DFDEALID, "=", dealId);
			theDO.addUserWhereCriterion(theDO.FIELD_DFCOPYID, "=", copyId);
			theDO.addUserWhereCriterion(theDO.FIELD_DFRESPONSEID, "=",
										responseId);

			////===============================================================

			PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

			Hashtable pst = pg.getPageStateTable();

			if (tds == null) {
				// first invocation ...
				// clear 'check deal notes flag' if necessary - calls workflowd
				// checkAcknowledgeNotes(getSessionResourceKit(), pg);

				// determine number of borrowers
				String voName = "Repeated1";
				int perPage = ( (TiledView) getCurrNDPage().getChild(voName)).
					getMaxDisplayTiles();

				// set up and save task display state object
				tds = new PageCursorInfo("DEFAULT", voName, perPage,
										 numberOfBorrowers);
				tds.setRefresh(true);

			}

			try {
				theDO.executeSelect(null);
				tds.setTotalRows(theDO.getSize());
			}
			catch (Exception ex) {
				setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
				logger.error(
					"Problem encountered getting CreditDecisionHandler");
				logger.error(ex);
				//return;
			}

			// determine if we will force display of first page
			if (tds.isTotalRowsChanged()) {
				// number of rows has changed - unconditional display of first page
				TiledView vo = (TiledView) (getCurrNDPage().getChild(tds.
					getVoName()));
				tds.setCurrPageNdx(0);
				//vo.goToFirst();
				try {
					vo.resetTileIndex();
				}
				catch (ModelControlException mce) {
					logger.warning("CreditDecisionHandler@executeApplicantResponseModel::Exception when resetTileIndex: " +
								   mce);
				}

				tds.setTotalRowsChanged(false);
			}

		//}
	}

	/**
	 * countBorrowers
	 * 	Returns the number of borrowers on a deal
	 *
	 * @param srk
	 * @param pg
	 * @return
	 */
	private int countBorrowers(SessionResourceKit srk, PageEntry pg) {
		int count = 1;

		try {
			JdbcExecutor jExec = srk.getJdbcExecutor();
			String sql =
				"SELECT COUNT(DISTINCT BORROWERID) FROM BORROWER WHERE DEALID = " +
				pg.getPageId() + " AND COPYID = " + pg.getPageDealCID(); ;

			int key = jExec.execute(sql);
			while (jExec.next(key)) {
				count = jExec.getInt(key, 1);
				break;
			}
			jExec.closeData(key);
		}
		catch (Exception e) {
			;
		}

		return count;

	}

	/**
	 * handleSubmit
	 * 	Handles the click of the OK button
	 *
	 * @version <br>
	 * Date: 10/17/2006 <br>
	 * Author: GCD Implementation Team <br>
	 * Change: <br>
	 * 	Call to handleCancelStandard to bring user back to work queue.
	 *
	 */
	public void handleSubmit() {
		PageEntry pg = theSessionState.getCurrentPage();

		SessionResourceKit srk = getSessionResourceKit();

		try {
			srk.beginTransaction();

			// set request status to Response Received if it is currently Response Available
			int currentRequestStatusId = getIdValueFromPage(
				"hdCreditDecisionRequestStatusId");
			int currentRequestId = getIdValueFromPage("hdRequestId");
			if (currentRequestStatusId ==
				Mc.REQUEST_STATUS_ID_RESPONSE_AVAILABLE &&
				currentRequestId != 0) {
				// SCR#1305 - start
				// adopt the copy
				standardAdoptTxCopy(pg, true);
				// SCR#1305 - end
				RequestPK requestPK = new RequestPK(currentRequestId,
					pg.getPageDealCID());
				Request currentRequest = new Request(srk);
				currentRequest = currentRequest.findByPrimaryKey(requestPK);
				currentRequest.setRequestStatusId(Mc.
					REQUEST_STATUS_ID_RESPONSE_RECEIVED);
				currentRequest.ejbStore();
				
				// Ticket# NBC165396
				this.workflowTrigger(2, srk, pg, null);
				
				// SCR#1305 - start
			}
			else {
				// Drop transactional copy, and return to previous page
				standardDropTxCopy(pg, false);
				// SCR#1305 - end
			}
			srk.commitTransaction();

			navigateToNextPage(true);
		}
		catch (Exception e) {
			srk.cleanTransaction();
			logger.error("Exception @CreditDecisionHandler.handleSubmit()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}
	}

	/**
	 * handleGetCreditDecision
	 * 	Performs the call to the processor to start the credit decision request process
	 *
	 * @param none<br>
	 * @return none
	 *
	 * @version <br>
	 * Date: 10/17/2006<br>
	 * Author: GCD Implementation Team<br>
	 * Change: <br>
	 * 	Moved call to adopt transactional copy and added call to set current copy as recommended scenario
	 *
	 */
	public void handleGetCreditDecision() {
		SessionResourceKit srk = getSessionResourceKit();
		PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

		try {
			Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
			int dealId = pg.getPageDealId();
			int copyId = pg.getPageDealCID();

			// Create a list of objects to store borrower ids to requested credit bureau to send
			//	 to the processor.  The processor will save this info to the database.
			Vector borrowerIds = new Vector();
			borrowerIds = getRepeatedFieldValues("Repeated1/hdApplicantId");

			LinkedList borrowerCreditBureau = new LinkedList();

			//Ticket 1296--start//
			for (int i = 0; i < borrowerIds.size(); i++) {
				String creditBureau = getDisplayField(i, pg.getPageName(),
					"Repeated1", "cbApplicantRequestedCreditBureau");

				int borrowerId = com.iplanet.jato.util.TypeConverter.asInt(
					borrowerIds.elementAt(i), -1);
				int creditBureauId = Integer.parseInt(creditBureau);

				BorrowerToBureau currentElement = new BorrowerToBureau(
					borrowerId, creditBureauId);
				borrowerCreditBureau.add(currentElement);
			}
			//Ticket 1296--end//

			DealPK dealPK = new DealPK(dealId, copyId);

			int languageId = theSessionState.getLanguageId();

			// call GCD processor method to generate the request, run business rules,
			//	 save information to the database & call web services

			PassiveMessage pm = runBusinessRules(theSessionState, pg, srk);

			if (pm.getCritical() == true) {
				logger.error(
					"Error @handleGetCreditDecision: GCD business rules failed");
				pm.setGenerate(true);
				theSessionState.setPasMessage(pm);
				setActiveMessageToAlert(BXResources.getSysMsg(
					"GCD_VALIDATION_CANCEL_ERROR_MSG",
					theSessionState.getLanguageId()),
										ActiveMsgFactory.ISCUSTOMCONFIRM);
				return;
			}

			CreditDecisionHelper processor = new CreditDecisionHelper();
			processor.processRequest(srk, dealPK, languageId, theSessionState,
									 pg, borrowerCreditBureau);

			// Adopt the transactional copy
			srk.beginTransaction();
			standardAdoptTxCopy(pg, true);
			deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
			if (deal.getScenarioRecommended() == null ||
				!deal.getScenarioRecommended().equals("Y")) {
				// set current scenario as the recommended scenario
				CCM ccm = new CCM(srk);
				ccm.setAsRecommendedScenario(deal);
				deal.ejbStore();
			}
			srk.commitTransaction();
		}
		catch (Exception ex) {
			srk.cleanTransaction();
			logger.error("Exception @handleGetCreditDecision :: " +
						 ex.getMessage());
			setActiveMessageToAlert(BXResources.getSysMsg(
				"TASK_RELATED_UNEXPECTED_FAILURE",
				theSessionState.getLanguageId()),
									ActiveMsgFactory.ISCUSTOMCONFIRM);
		}
	}

	/**
	 * handleApplicantDetails
	 * 	Performs the calls to bring up the Applicant Details page
	 *
	 * @param applicantNumber<br>
	 * @return none
	 * @version 1.8 <br>
	 * Date: 11/02/2006 <br>
	 * Change: Removed copyid from page criteria
	 */
	public void handleApplicantDetails(int applicantNumber) {
		PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

		try {

			// go directly to the details page;
			PageEntry pgEntry = setupSubPagePageEntry(pg,
				Mc.PGNM_CD_APPLICANT_DETAILS);
			//Hashtable subPst = pgEntry.getPageStateTable();

			String borrowerId = getRepeatedField( ("Repeated1/hdApplicantId").
												 toString(), applicantNumber);
			String appResponseId = getRepeatedField( (
				"Repeated1/hdApplicantResponseId").toString(), applicantNumber);

			// borrowerid,  applicantNumber, responseId
			pgEntry.setPageCriteriaId1( (new Integer(borrowerId)).intValue());
			pgEntry.setPageCriteriaId2(applicantNumber);
			pgEntry.setPageCriteriaId3( (new Integer(appResponseId)).intValue());

			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage();
		}
		catch (Exception e) {
			logger.error("Exception @handleApplicantDetails :: " + e.getMessage());
		}

	}

	/**
	 * handleViewRemoveDetailsButton
	 * 	Hide details button if<br>
	 * 		Request status of the deal is not "Response Received"
	 *
	 * @return boolean <br>
	 * 	true - generate the button
	 * 	false - hide the button
	 */
	public boolean handleViewRemoveDetailsButton() {
		PageEntry pg = theSessionState.getCurrentPage();

		if (pg.getPageDealId() <= 0) {
			return false;
		}

		//Hashtable pst = pg.getPageStateTable();

		java.math.BigDecimal tempStatusId = (java.math.BigDecimal)
			getCurrNDPage().getDisplayFieldValue(
			"hdCreditDecisionRequestStatusId");
		int requestStatusId = 0;
		if (tempStatusId != null) {
			requestStatusId = tempStatusId.intValue();

		}

		tempStatusId = (java.math.BigDecimal) getCurrNDPage().
			getDisplayFieldValue("hdCreditDecisionRequestStatusId");
		int dealStatusId = 0;
		if (tempStatusId != null) {
			dealStatusId = tempStatusId.intValue();
		}

		// hide button if response has been not been received
		if (requestStatusId != Mc.REQUEST_STATUS_ID_RESPONSE_RECEIVED &&
			requestStatusId != Mc.REQUEST_STATUS_ID_RESPONSE_AVAILABLE) {
			return false;
		}

		return true;
	}

	/**
	 * handleViewRemoveGetCreditDecisionButton
	 * 	Determines whether to hide or show the Get Credit Decision button
	 *
	 * @return boolean
	 * 	true - show the button
	 * 	false - hide the button
	 */
	public boolean handleViewRemoveGetCreditDecisionButton() {
		PageEntry pg = theSessionState.getCurrentPage();

		if (pg.getPageDealId() <= 0) {
			return false;
		}

		//Hashtable pst = pg.getPageStateTable();

		java.math.BigDecimal tempStatusId = (java.math.BigDecimal)
			getCurrNDPage().getDisplayFieldValue(
			"hdCreditDecisionRequestStatusId");
		int requestStatusId = 0;
		if (tempStatusId != null) {
			requestStatusId = tempStatusId.intValue();
		}

		tempStatusId = (java.math.BigDecimal) getCurrNDPage().
			getDisplayFieldValue("hdDealStatusCategoryId");
		int dealStatusCategoryId = 0;
		if (tempStatusId != null) {
			dealStatusCategoryId = tempStatusId.intValue();
			if (dealStatusCategoryId ==
				Mc.DEAL_STATUS_CATEGORY_FINAL_DISPOSITION) {
				return false;
			}
		}

		// Check if "Pending Credit Decision" task is open & hidden
		if (isFoundPendingCreditDecisionTask(pg.getPageDealId())) {
			return false;
		}

		return true;
	}

	/**
	 * isFoundPendingCreditDecisionTask
	 * 	Check for an existing Pending Credit Decision task for the current user and deal
	 *
	 * @param dealId<br>
	 * @return boolean <br>
	 * 	true - a Pending Credit Decision task was found,<br>
	 * 	false - otherwise
	 */
	private boolean isFoundPendingCreditDecisionTask(int dealId) {
		boolean isCorrectTask = false;
		try {
			// Get all tasks that are assigned to this deal and find the
			AssignedTask dealTask = new AssignedTask(srk);

			Vector allDealTasks = dealTask.findByDeal(dealId);
			for (int i = 0; i < allDealTasks.size(); i++) {
				dealTask = (AssignedTask) allDealTasks.get(i);
				if (dealTask.getTaskId() ==
					Mc.ASSIGNED_TASK_PENDING_CREDIT_DECISION) {
					// found the task, check the status and visible flag
					if (dealTask.getTaskStatusId() == Sc.TASK_OPEN &&
						dealTask.getVisibleFlag() == Sc.TASK_VISIBLEFLAG_HIDDEN) {
						isCorrectTask = true;
					}
				}
			}
		}
		catch (Exception e) {
			logger.error("CDH@isFoundPendingCreditDecisionTask :: exception " +
						 e.getMessage());
		}

		return isCorrectTask;
	}

	/**
	 * This method will run the business rules for GCD.
	 */
	private PassiveMessage runBusinessRules(SessionStateModel theSessionState,
											PageEntry pe,
											SessionResourceKit srk) throws
		Exception {
		String logString = "CreditDecisionHandler.runBusinessRules - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method");
		PassiveMessage pm = new PassiveMessage();

		BusinessRuleExecutor brExec = new BusinessRuleExecutor();

		Vector v = new Vector();
		v.add("GCD-%");

		pm = brExec.BREValidator( (SessionStateModelImpl) theSessionState, pe,
								 srk, pm, v, true);

		brExec.close();

		return pm;
	}

	//Ticket 1296--start//
	// get the field value from a tiled view directly from the HttpServletRequest.
	private String getDisplayField(int rowNdx, String pageName, String tileName,
								   String fieldName) {
		HttpServletRequest _req = RequestManager.getRequest();
		String paramName = pageName + "_" + tileName + "[" +
			rowNdx + "]_" + fieldName;
		String field = _req.getParameter(paramName);
		return field;
	}

	//Ticket 1296--end//

}
