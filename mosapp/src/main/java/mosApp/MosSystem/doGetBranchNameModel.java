package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doGetBranchNameModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBranchProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBranchProfileId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBranchName();

	
	/**
	 * 
	 * 
	 */
	public void setDfBranchName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBranchBusinessId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBranchBusinessId(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBranchShortName();

	
	/**
	 * 
	 * 
	 */
	public void setDfBranchShortName(String value);
	
    public java.math.BigDecimal getDfInstitutionId();
    public void setDfInstitutionId(java.math.BigDecimal id);  
  
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBRANCHPROFILEID="dfBranchProfileId";
	public static final String FIELD_DFBRANCHNAME="dfBranchName";
	public static final String FIELD_DFBRANCHBUSINESSID="dfBranchBusinessId";
	public static final String FIELD_DFBRANCHSHORTNAME="dfBranchShortName";
	
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

