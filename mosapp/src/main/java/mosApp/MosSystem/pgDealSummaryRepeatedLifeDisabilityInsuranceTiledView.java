package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgDealSummaryRepeatedLifeDisabilityInsuranceTiledView extends RequestHandlingTiledViewBase
  implements TiledView, RequestHandler
{
  /**
   *
   *
   */
  public pgDealSummaryRepeatedLifeDisabilityInsuranceTiledView(View parent, String name)
  {
    super(parent, name);
    setMaxDisplayTiles(10);
    setPrimaryModelClass( doUWGDSTDSApplicantDetailsModel.class );
    registerChildren();
    initialize();
  }


  /**
   *
   *
   */
  protected void initialize()
  {
  }


  /**
   *
   *
   */
  public void resetChildren()
  {
    getHdLDApplicantId().setValue(CHILD_HDLDAPPLICANTID_RESET_VALUE);
    getHdLDApplicantCopyId().setValue(CHILD_HDLDAPPLICANTCOPYID_RESET_VALUE);
    getStLDFirstName().setValue(CHILD_STLDFIRSTNAME_RESET_VALUE);
    getStLDMiddleInitial().setValue(CHILD_STLDMIDDLEINITIAL_RESET_VALUE);
    getStLDLastName().setValue(CHILD_STLDLASTNAME_RESET_VALUE);
    getStLDApplicantType().setValue(CHILD_STLDAPPLICANTTYPE_RESET_VALUE);
    getStInsProportionsDesc().setValue(CHILD_STINSURANCEPROPORTIONS_RESET_VALUE);
    getStLifeCoverage().setValue(CHILD_STLIFECOVERAGE_RESET_VALUE);
    getStDisabilityCoverage().setValue(CHILD_STDISABILITYCOVERAGE_RESET_VALUE);
    ////getStLifeDisabilityPremium().setValue(CHILD_STLIFEDISABILITYPREMIUM_RESET_VALUE);
    //--DJ_CR201.2--start//
    getStGuarantorOtherLoans().setValue(CHILD_STGUARANTOROTHERLOANS_RESET_VALUE);
    //--DJ_CR201.2--end//
  }


  /**
   *
   *
   */
  protected void registerChildren()
  {
    registerChild(CHILD_HDLDAPPLICANTID,HiddenField.class);
    registerChild(CHILD_HDLDAPPLICANTCOPYID,HiddenField.class);
    registerChild(CHILD_STLDFIRSTNAME,StaticTextField.class);
    registerChild(CHILD_STLDMIDDLEINITIAL,StaticTextField.class);
    registerChild(CHILD_STLDLASTNAME,StaticTextField.class);
    registerChild(CHILD_STLDAPPLICANTTYPE,StaticTextField.class);
    registerChild(CHILD_STINSURANCEPROPORTIONS,StaticTextField.class);
    registerChild(CHILD_STLIFECOVERAGE,StaticTextField.class);
    registerChild(CHILD_STDISABILITYCOVERAGE,StaticTextField.class);
    ////registerChild(CHILD_STLIFEDISABILITYPREMIUM,StaticTextField.class);
    //--DJ_CR201.2--start//
    registerChild(CHILD_STGUARANTOROTHERLOANS,StaticTextField.class);
    //--DJ_CR201.2--end//
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Model management methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  public Model[] getWebActionModels(int executionType)
  {
    List modelList=new ArrayList();
    switch(executionType)
    {
      case MODEL_TYPE_RETRIEVE:
        ;
        break;

      case MODEL_TYPE_UPDATE:
        ;
        break;

      case MODEL_TYPE_DELETE:
        ;
        break;

      case MODEL_TYPE_INSERT:
        ;
        break;

      case MODEL_TYPE_EXECUTE:
        ;
        break;
    }
    return (Model[])modelList.toArray(new Model[0]);
  }


  ////////////////////////////////////////////////////////////////////////////////
  // View flow control methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  public void beginDisplay(DisplayEvent event)
    throws ModelControlException
  {

    // This is the analog of NetDynamics repeated_onBeforeDisplayEvent
    // Ensure the primary model is non-null; if null, it would cause havoc
    // with the various methods dependent on the primary model
    if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    super.beginDisplay(event);
    resetTileIndex();
  }


  /**
   *
   *
   */
  public boolean nextTile()
    throws ModelControlException
  {

    // This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
    boolean movedToRow = super.nextTile();

    if (movedToRow)
    {
      // Put migrated repeated_onBeforeRowDisplayEvent code here
    }

    return movedToRow;

  }


  /**
   *
   *
   */
  public boolean beforeModelExecutes(Model model, int executionContext)
  {

    // This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
    return super.beforeModelExecutes(model, executionContext);

  }


  /**
   *
   *
   */
  public void afterModelExecutes(Model model, int executionContext)
  {

    // This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
    super.afterModelExecutes(model, executionContext);

  }


  /**
   *
   *
   */
  public void afterAllModelsExecute(int executionContext)
  {

    // This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
    super.afterAllModelsExecute(executionContext);

  }


  /**
   *
   *
   */
  public void onModelError(Model model, int executionContext, ModelControlException exception)
    throws ModelControlException
  {

    // This is the analog of NetDynamics repeated_onDataObjectErrorEvent
    super.onModelError(model, executionContext, exception);

  }


  ////////////////////////////////////////////////////////////////////////////////
  // Child manipulation methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  protected View createChild(String name)
  {
    if (name.equals(CHILD_HDLDAPPLICANTID))
    {
      HiddenField child = new HiddenField(this,
        getdoUWGDSTDSApplicantDetailsModel(),
        CHILD_HDLDAPPLICANTID,
        doUWGDSTDSApplicantDetailsModel.FIELD_DFBORROWERID,
        CHILD_HDLDAPPLICANTID_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_HDLDAPPLICANTCOPYID))
    {
      HiddenField child = new HiddenField(this,
        getdoUWGDSTDSApplicantDetailsModel(),
        CHILD_HDLDAPPLICANTCOPYID,
        doUWGDSTDSApplicantDetailsModel.FIELD_DFCOPYID,
        CHILD_HDLDAPPLICANTCOPYID_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STLDFIRSTNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getdoUWGDSTDSApplicantDetailsModel(),
        CHILD_STLDFIRSTNAME,
        doUWGDSTDSApplicantDetailsModel.FIELD_DFFIRSTNAME,
        CHILD_STLDFIRSTNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STLDMIDDLEINITIAL))
    {
      StaticTextField child = new StaticTextField(this,
        getdoUWGDSTDSApplicantDetailsModel(),
        CHILD_STLDMIDDLEINITIAL,
        doUWGDSTDSApplicantDetailsModel.FIELD_DFMIDDLEINITIAL,
        CHILD_STLDMIDDLEINITIAL_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STLDLASTNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getdoUWGDSTDSApplicantDetailsModel(),
        CHILD_STLDLASTNAME,
        doUWGDSTDSApplicantDetailsModel.FIELD_DFLASTNAME,
        CHILD_STLDLASTNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STLDAPPLICANTTYPE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoUWGDSTDSApplicantDetailsModel(),
        CHILD_STLDAPPLICANTTYPE,
        doUWGDSTDSApplicantDetailsModel.FIELD_DFBORROWERTYPE,
        CHILD_STLDAPPLICANTTYPE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STINSURANCEPROPORTIONS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoUWGDSTDSApplicantDetailsModel(),
        CHILD_STINSURANCEPROPORTIONS,
        doUWGDSTDSApplicantDetailsModel.FIELD_DFINSURANCEPROPORTIONSID,
        CHILD_STINSURANCEPROPORTIONS_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STLIFECOVERAGE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoUWGDSTDSApplicantDetailsModel(),
        CHILD_STLIFECOVERAGE,
        doUWGDSTDSApplicantDetailsModel.FIELD_DFLIFESTATUSID,
        CHILD_STLIFECOVERAGE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STDISABILITYCOVERAGE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoUWGDSTDSApplicantDetailsModel(),
        CHILD_STDISABILITYCOVERAGE,
        doUWGDSTDSApplicantDetailsModel.FIELD_DFDISABILITYSTATUSID,
        CHILD_STDISABILITYCOVERAGE_RESET_VALUE,
        null);
      return child;
    }
    /**
    else
    if (name.equals(CHILD_STLIFEDISABILITYPREMIUM))
    {
      StaticTextField child = new StaticTextField(this,
        getdoUWGDSTDSApplicantDetailsModel(),
        CHILD_STLIFEDISABILITYPREMIUM,
        doUWGDSTDSApplicantDetailsModel.FIELD_DFCOMBINEDLDPREMIUM,
        CHILD_STLIFEDISABILITYPREMIUM_RESET_VALUE,
        null);
      return child;
    }
    **/
    //--DJ_CR201.2--start//
    else
    if (name.equals(CHILD_STGUARANTOROTHERLOANS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoUWGDSTDSApplicantDetailsModel(),
        CHILD_STGUARANTOROTHERLOANS,
        doUWGDSTDSApplicantDetailsModel.FIELD_DFGUARANTOROTHERLOANS,
        CHILD_STGUARANTOROTHERLOANS_RESET_VALUE,
        null);
      return child;
    }
    //--DJ_CR201.2--end//
    else
      throw new IllegalArgumentException("Invalid child name [" + name + "]");
  }


  /**
   *
   *
   */
  public HiddenField getHdLDApplicantId()
  {
    return (HiddenField)getChild(CHILD_HDLDAPPLICANTID);
  }


  /**
   *
   *
   */
  public HiddenField getHdLDApplicantCopyId()
  {
    return (HiddenField)getChild(CHILD_HDLDAPPLICANTCOPYID);
  }


  /**
   *
   *
   */
  public StaticTextField getStLDFirstName()
  {
    return (StaticTextField)getChild(CHILD_STLDFIRSTNAME);
  }


  /**
   *
   *
   */
  public StaticTextField getStLDMiddleInitial()
  {
    return (StaticTextField)getChild(CHILD_STLDMIDDLEINITIAL);
  }


  /**
   *
   *
   */
  public StaticTextField getStLDLastName()
  {
    return (StaticTextField)getChild(CHILD_STLDLASTNAME);
  }


  /**
   *
   *
   */
  public StaticTextField getStLDApplicantType()
  {
    return (StaticTextField)getChild(CHILD_STLDAPPLICANTTYPE);
  }

  /**
   *
   *
   */
  public StaticTextField getStInsProportionsDesc()
  {
    return (StaticTextField)getChild(CHILD_STINSURANCEPROPORTIONS);
  }

  /**
   *
   *
   */
  public StaticTextField getStLifeCoverage()
  {
    return (StaticTextField)getChild(CHILD_STLIFECOVERAGE);
  }

  /**
   *
   *
   */
  public StaticTextField getStDisabilityCoverage()
  {
    return (StaticTextField)getChild(CHILD_STDISABILITYCOVERAGE);
  }

  /**
   *
   *
   */
  /**
  public StaticTextField getStLifeDisabilityPremium()
  {
    return (StaticTextField)getChild(CHILD_STLIFEDISABILITYPREMIUM);
  }
  **/
  //--DJ_LDI_CR--end--//

  /**
   *
   *
   */
  public doUWGDSTDSApplicantDetailsModel getdoUWGDSTDSApplicantDetailsModel()
  {
    if (doUWGDSTDSApplicantDetails == null)
      doUWGDSTDSApplicantDetails = (doUWGDSTDSApplicantDetailsModel) getModel(doUWGDSTDSApplicantDetailsModel.class);
    return doUWGDSTDSApplicantDetails;
  }


  /**
   *
   *
   */
  public void setdoUWGDSTDSApplicantDetailsModel(doUWGDSTDSApplicantDetailsModel model)
  {
      doUWGDSTDSApplicantDetails = model;
  }


  //--DJ_CR201.2--start//
  /**
   *
   *
   */
  public StaticTextField getStGuarantorOtherLoans()
  {
    return (StaticTextField)getChild(CHILD_STGUARANTOROTHERLOANS);
  }

  //--> Bug fix to support Bilingual : By Billy 19May2004
  public String endStGuarantorOtherLoansDisplay(ChildContentDisplayEvent event)
  {
    DealSummaryHandler handler = (DealSummaryHandler)this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());
    String rc = handler.setYesOrNo(event.getContent());
    handler.pageSaveState();

    return rc;
  }

  //========================================================

  //--DJ_CR201.2--end//

  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////
  // Child accessors
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Child rendering methods
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Repeated event methods
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////

  public static Map FIELD_DESCRIPTORS;
  public static final String CHILD_HDLDAPPLICANTID="hdLDApplicantId";
  public static final String CHILD_HDLDAPPLICANTID_RESET_VALUE="";

  public static final String CHILD_HDLDAPPLICANTCOPYID="hdLDApplicantCopyId";
  public static final String CHILD_HDLDAPPLICANTCOPYID_RESET_VALUE="";

  public static final String CHILD_STLDFIRSTNAME="stLDFirstName";
  public static final String CHILD_STLDFIRSTNAME_RESET_VALUE="";

  public static final String CHILD_STLDMIDDLEINITIAL="stLDMiddleInitial";
  public static final String CHILD_STLDMIDDLEINITIAL_RESET_VALUE="";

  public static final String CHILD_STLDLASTNAME="stLDLastName";
  public static final String CHILD_STLDLASTNAME_RESET_VALUE="";

  public static final String CHILD_STLDAPPLICANTTYPE="stLDApplicantType";
  public static final String CHILD_STLDAPPLICANTTYPE_RESET_VALUE="";

  public static final String CHILD_STINSURANCEPROPORTIONS="stInsProportions";
  public static final String CHILD_STINSURANCEPROPORTIONS_RESET_VALUE="";

  public static final String CHILD_STLIFECOVERAGE="stLifeCoverage";
  public static final String CHILD_STLIFECOVERAGE_RESET_VALUE="";

  public static final String CHILD_STDISABILITYCOVERAGE="stDisabilityCoverage";
  public static final String CHILD_STDISABILITYCOVERAGE_RESET_VALUE="";

  ////public static final String CHILD_STLIFEDISABILITYPREMIUM="stLifeDisabilityPremium";
  ////public static final String CHILD_STLIFEDISABILITYPREMIUM_RESET_VALUE="";

  //--DJ_CR201.2--start//
  public static final String CHILD_STGUARANTOROTHERLOANS="stGuarantorOtherLoans";
  public static final String CHILD_STGUARANTOROTHERLOANS_RESET_VALUE="";
  //--DJ_CR201.2--end//

  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////

  private doUWGDSTDSApplicantDetailsModel doUWGDSTDSApplicantDetails=null;
  private DealSummaryHandler handler=new DealSummaryHandler();
}

