package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 * <p>Title: doDealSummarySnapShotModel</p>
 *
 * <p>Description: interface for deal summary snap shot</p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.2
 *
 */
public interface doDealSummarySnapShotModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public String getDfApplicationID();

	
	/**
	 * 
	 * 
	 */
	public void setDfApplicationID(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStatusDescr();

	
	/**
	 * 
	 * 
	 */
	public void setDfStatusDescr(String value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfStatusDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfStatusDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSFShortName();

	
	/**
	 * 
	 * 
	 */
	public void setDfSFShortName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSOBPShortName();

	
	/**
	 * 
	 * 
	 */
	public void setDfSOBPShortName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLOBDescr();

	
	/**
	 * 
	 * 
	 */
	public void setDfLOBDescr(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfDealTypeDescr();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealTypeDescr(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLoanPurposeDescr();

	
	/**
	 * 
	 * 
	 */
	public void setDfLoanPurposeDescr(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalPurchasePrice();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalPurchasePrice(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalLoanAmt();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalLoanAmt(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPaymentTermDescr();

	
	/**
	 * 
	 * 
	 */
	public void setDfPaymentTermDescr(String value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfEstClosingDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfEstClosingDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealID();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealID(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSpecialFeature();

	
	/**
	 * 
	 * 
	 */
	public void setDfSpecialFeature(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);
	
	
	//-- ========== SCR#859 begins ========== --//
	//-- by Neil on Feb 15, 2005
	public String getDfServicingMortgageNumber();
	public void setDfServicingMortgageNumber(String value);	
	//-- ========== SCR#859 ends ========== --//
	
//	***** Change by NBC Impl. Team - Version 1.2 - Start *****//

	/**
	 * getDfCashBackInDollars
	 *
	 * @param None <br>
	 *  
	 * @return BigDecimal : the result of getDfCashBackInDollars <br>
	 */
	public java.math.BigDecimal getDfCashBackInDollars();
	/**
	 * setDfCashBackInDollars
	 *
	 * @param BigDecimal <br>
	 *  
	 * @return Void : the result of setDfCashBackInDollars <br>
	 */
	public void setDfCashBackInDollars(java.math.BigDecimal value);
	/**
	 * getDfCashBackInPercentage
	 *
	 * @param None <br>
	 *  
	 * @return BigDecimal : the result of getDfCashBackInPercentage <br>
	 */
	public java.math.BigDecimal getDfCashBackInPercentage();
	/**
	 * setDfCashBackInPercentage
	 *
	 * @param BigDecimal <br>
	 *  
	 * @return void : the result of setDfCashBackInPercentage <br>
	 */
	public void setDfCashBackInPercentage(java.math.BigDecimal value);
	/**
	 * getDfCashBackOverrideInDollars
	 *
	 * @param None <br>
	 *  
	 * @return String : the result of getDfCashBackOverrideInDollars <br>
	 */
	public String getDfCashBackOverrideInDollars();
	/**
	 * setDfCashBackOverrideInDollars
	 *
	 * @param String <br>
	 *  
	 * @return void : the result of setDfCashBackOverrideInDollars <br>
	 */
	public void setDfCashBackOverrideInDollars(String value);
	
  
  /**
   * 
   * 
   */
  public java.math.BigDecimal getDfInstitutionID();

  
  /**
   * 
   * 
   */
  public void setDfInstitutionID(java.math.BigDecimal value);
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFAPPLICATIONID="dfApplicationID";
	public static final String FIELD_DFSTATUSDESCR="dfStatusDescr";
	public static final String FIELD_DFSTATUSDATE="dfStatusDate";
	public static final String FIELD_DFSFSHORTNAME="dfSFShortName";
	public static final String FIELD_DFSOBPSHORTNAME="dfSOBPShortName";
	public static final String FIELD_DFLOBDESCR="dfLOBDescr";
	public static final String FIELD_DFDEALTYPEDESCR="dfDealTypeDescr";
	public static final String FIELD_DFLOANPURPOSEDESCR="dfLoanPurposeDescr";
	public static final String FIELD_DFTOTALPURCHASEPRICE="dfTotalPurchasePrice";
	public static final String FIELD_DFTOTALLOANAMT="dfTotalLoanAmt";
	public static final String FIELD_DFPAYMENTTERMDESCR="dfPaymentTermDescr";
	public static final String FIELD_DFESTCLOSINGDATE="dfEstClosingDate";
	public static final String FIELD_DFDEALID="dfDealID";
	public static final String FIELD_DFSPECIALFEATURE="dfSpecialFeature";
	public static final String FIELD_DFCOPYID="dfCopyId";
	
	//-- ========== SCR#859 begins ========== --//
	//-- by Neil on Feb 15, 2005
	public static final String FIELD_DFSERVICINGMORTGAGENUMBER = "dfServicingMortgageNumber";
	//-- ========== SCR#859 ends ========== --//
//	 ***** Change by NBC Impl. Team - Version 1.2 - Start *****//

	public static final String FIELD_DFCASHBACKINDOLLARS = "dfCashBackInDollars";

	public static final String FIELD_DFCASHBACKINPERCENTAGE = "dfCashBackInPercentage";

	public static final String FIELD_DFCASHBACKOVERRIDEINDOLLARS = "dfCashBackOverrideInDollars";

	// ***** Change by NBC Impl. Team - Version 1.2 - End*****//
	
	public static final String FIELD_DFSTATUSID_STR = "dfDealStatusId";
  
  public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
  
    public String getDfDealStatusId();
    public void setDfDealStatusId(String value);
    
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

