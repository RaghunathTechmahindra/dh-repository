package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.event.TiledViewRequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.CheckBox;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.ImageField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

/**
 * <p>Title: pgMortCompRepeated1TiledView</p>
 * <p>Description: TiledView Class for Mortgage Component section on Component Details Screen</p>
 *
 * @author MCM Team
 * @version 1.0 (Initial Version � Jun 12, 2008)
 * @version 1.1 fixed bugs: Recalc Button -> Button
 * @version 1.2 fixed bugs: use doComponentMortgageModel for all field except buttons for save 
 * @version 1.3 fixed Cosmetic Issue: Section Title is only one, line for the others.
 * @version 1.4 delete Component section - XS_2.32 Modified - handleBtDeleteMTGCompRequest()
 * @version 1.5 July 25, 2008 set MAXCOMPTILE_SIZE = 10 for artf750856 
 */
public class pgComponentDetailsMTGTiledView extends RequestHandlingTiledViewBase
        implements TiledView, RequestHandler {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(pgComponentDetailsMTGTiledView.class);

    /**
     * <p>constructor</p>
     */
    public pgComponentDetailsMTGTiledView(View parent, String name) {
        super(parent, name);        
        // MCM Team for artf750856
        setMaxDisplayTiles(pgComponentDetailsViewBean.MAXCOMPTILE_SIZE);
        setPrimaryModelClass(doComponentMortgageModel.class);
        registerChildren();
        initialize();
    }

    /**
     * <p>initializer</p>
     */
    protected void initialize() {
    }

    /**
     * <p>createChild</p>
     * <p>Description: createChild method for Jato Framework</p>
     * 
     * @param name:String - child name
     * @return View 
     */
    protected View createChild(String name) {
        
        if (name.equals(CHILD_HDCOMPONENTIDMTG)) {
            HiddenField child 
                = new HiddenField(this,
                        getdoComponentMortgageModel(),
                        CHILD_HDCOMPONENTIDMTG,
                        doComponentMortgageModel.FIELD_DFCOMPONENTID,
                        CHILD_HDCOMPONENTIDMTG_RESET_VALUE,
                    null);
            return child;
        } else if (name.equals(CHILD_HDCOPYIDMTG)) {
            HiddenField child 
                = new HiddenField(this,
                        getdoComponentMortgageModel(),
                        CHILD_HDCOPYIDMTG,
                        doComponentMortgageModel.FIELD_DFCOPYID,
                        CHILD_HDCOPYIDMTG_RESET_VALUE,
                    null);
            return child;
            // MCM Team: fixed cosmetic issue, STARTS
        } else if (name.equals(CHILD_STMTGTITLE)) {
            StaticTextField child 
                = new StaticTextField(this,
                        getDefaultModel(),
                        CHILD_STMTGTITLE,
                        CHILD_STMTGTITLE,
                        CHILD_STMTGTITLE,
                    null);
            return child;
        } else if (name.equals(CHILD_IMMTGSECTIONDEVIDER)) {
            ImageField child 
                = new ImageField(this,
                        getDefaultModel(),
                        CHILD_IMMTGSECTIONDEVIDER,
                        CHILD_IMMTGSECTIONDEVIDER,
                        CHILD_IMMTGSECTIONDEVIDER,
                    null);
            return child;
            // MCM Team: fixed cosmetic issue, ENDS
        } else if (name.equals(CHILD_CBCOMPONENTTYPE)) {
            ComboBox child 
                = new ComboBox(this,
                    getdoComponentMortgageModel(),
                    CHILD_CBCOMPONENTTYPE,
                    doComponentMortgageModel.FIELD_DFCOMPONENTTYPE,
                    CHILD_CBCOMPONENTTYPE_RESET_VALUE,
                    null);
          child.setLabelForNoneSelected("");
          child.setOptions(cbComponentTypeOptions);
          return child;
        } else if (name.equals(CHILD_TXMORTGAGEAMOUNT)){
            TextField child 
                = new TextField(this,
                        getdoComponentMortgageModel(),
                        CHILD_TXMORTGAGEAMOUNT,
                        doComponentMortgageModel.FIELD_DFMORTGAGEAMOUNT,
                        CHILD_TXMORTGAGEAMOUNT_RESET_VALUE,
                        null);            
            return child;
        } else if (name.equals(CHILD_STMIPREMIUM)){
            StaticTextField child 
                = new StaticTextField(this,
                        getdoComponentMortgageModel(),
                        CHILD_STMIPREMIUM,
                        CHILD_STMIPREMIUM,
                        CHILD_STMIPREMIUM_RESET_VALUE,
                        null);            
            return child;
        } else if (name.equals(CHILD_CHALLOCATEMIPREM)){
            CheckBox child 
                = new CheckBox(this,
                        getdoComponentMortgageModel(),
                        CHILD_CHALLOCATEMIPREM,
                        doComponentMortgageModel.FIELD_DFMIALLOCAGTEFLAG,
                        "Y", "N", false,  null);
            return child;
        } else if (name.equals(CHILD_STTOTALAMOUNT)){
            StaticTextField child 
                = new StaticTextField(this,
                        getdoComponentMortgageModel(),
                        CHILD_STTOTALAMOUNT,
                        doComponentMortgageModel.FIELD_DFTOTALMORTGAGEAMOUNT,
                        CHILD_STTOTALAMOUNT_RESET_VALUE,
                        null);
            return child;
        } else if (name.equals(CHILD_TXAMORTIZATIONPERIODYEARS)){
            TextField child 
                = new TextField(this,
                        getdoComponentMortgageModel(),
                        CHILD_TXAMORTIZATIONPERIODYEARS,
                        CHILD_TXAMORTIZATIONPERIODYEARS,
                        CHILD_TXAMORTIZATIONPERIODYEARS_RESET_VALUE,
                        null);
            return child;
        } else if (name.equals(CHILD_TXAMORTIZATIONPERIODMONTHS)){
            TextField child 
                = new TextField(this,
                        getdoComponentMortgageModel(),
                        CHILD_TXAMORTIZATIONPERIODMONTHS,
                        CHILD_TXAMORTIZATIONPERIODMONTHS,
                        CHILD_TXAMORTIZATIONPERIODMONTHS_RESET_VALUE,
                        null);
            return child;
        } else if (name.equals(CHILD_TXEFFECTAPYEARS)){
          TextField child 
              = new TextField(this,
                      getdoComponentMortgageModel(),
                      CHILD_TXEFFECTAPYEARS,
                      CHILD_TXEFFECTAPYEARS,
                      CHILD_TXEFFECTAPYEARS_RESET_VALUE,
                      null);
          return child;
          } else if (name.equals(CHILD_TXEFFECTAPMONTHS)){
          TextField child 
              = new TextField(this,
                      getdoComponentMortgageModel(),
                      CHILD_TXEFFECTAPMONTHS,
                      CHILD_TXEFFECTAPMONTHS,
                      CHILD_TXEFFECTAPMONTHS_RESET_VALUE,
                      null);
          return child;
          } else if (name.equals(CHILD_TBPAYMENTTERMDESCRIPTION)){
              TextField child 
                  = new TextField(this,
                          getdoComponentMortgageModel(),
                          CHILD_TBPAYMENTTERMDESCRIPTION,
                          doComponentMortgageModel.FIELD_DFPAYMENTTERMDESC,
                          CHILD_TBPAYMENTTERMDESCRIPTION_RESET_VALUE,
                          null);
              return child;
          } else if (name.equals(CHILD_HDPAYMENTTERMID)){
              HiddenField child 
                  = new HiddenField(this,
                          getdoComponentMortgageModel(),
                          CHILD_HDPAYMENTTERMID,
                          doComponentMortgageModel.FIELD_DFPAYMENTTERMID,
                          CHILD_TBPAYMENTTERMDESCRIPTION_RESET_VALUE,
                          null);
              return child;
        } else if (name.equals(CHILD_TXACTUALPAYMENTTERMMONTHS)){
            TextField child 
                = new TextField(this,
                        getdoComponentMortgageModel(),
                        CHILD_TXACTUALPAYMENTTERMMONTHS,
                        CHILD_TXACTUALPAYMENTTERMMONTHS,
                        CHILD_TXACTUALPAYMENTTERMMONTHS_RESET_VALUE,
                        null);
            return child;
        } else if (name.equals(CHILD_TXACTUALPAYMENTTERMYEARS)){
            TextField child 
                = new TextField(this,
                        getdoComponentMortgageModel(),
                        CHILD_TXACTUALPAYMENTTERMYEARS,
                        CHILD_TXACTUALPAYMENTTERMYEARS,
                        CHILD_TXACTUALPAYMENTTERMYEARS_RESET_VALUE,
                        null);
            return child;
        } else if (name.equals(CHILD_CBPAYMENTFREQUENCY)) {
            ComboBox child = new ComboBox(this, 
                    getdoComponentMortgageModel(),
                    CHILD_CBPAYMENTFREQUENCY,
                    doComponentMortgageModel.FIELD_DFPAYMENTFREQUENCYID,
                    CHILD_CBPAYMENTFREQUENCY_RESET_VALUE, null);
              child.setLabelForNoneSelected("");
              child.setOptions(cbPaymentFrequencyOptions);
            return child;
        } else if (name.equals(CHILD_CBPREPAYMENTPENALTY)) {
            ComboBox child = new ComboBox(this, getdoComponentMortgageModel(),
                    CHILD_CBPREPAYMENTPENALTY,
                    doComponentMortgageModel.FIELD_DFPREPAYMENTOPTIONSID,
                    CHILD_CBPREPAYMENTPENALTY_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbPrepaymentPenaltyOptions);
            return child;
        } else if (name.equals(CHILD_CBPRIVILEGEPAYMENTOPTION)) {
            ComboBox child = new ComboBox(this, getdoComponentMortgageModel(),
                    CHILD_CBPRIVILEGEPAYMENTOPTION,
                    doComponentMortgageModel.FIELD_DFPRIVILEGEPAYMENTID,
                    CHILD_CBPRIVILEGEPAYMENTOPTION_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbPrivilegePaymentOptions);
            return child;
        } else if (name.equals(CHILD_TXCOMMISIONCODE)) {
                TextField child = new TextField(this, getdoComponentMortgageModel(),
                        CHILD_TXCOMMISIONCODE,
                        doComponentMortgageModel.FIELD_DFCOMMISSIONCODE,
                        CHILD_TXCOMMISIONCODE_RESET_VALUE, null);

                return child;
        } else if (name.equals(CHILD_TXCASHBACKINPERCENTAGE)) {
            TextField child = new TextField(this,
                    getdoComponentMortgageModel(),
                    CHILD_TXCASHBACKINPERCENTAGE,
                    doComponentMortgageModel.FIELD_DFCASHBACKPERCENTAGE,
                    CHILD_TXCASHBACKINPERCENTAGE_RESET_VALUE, null);
            return child;
        }    else if (name.equals(CHILD_TXCASHBACKINDOLLARS)) {
                TextField child = new TextField(this,
                        getdoComponentMortgageModel(), CHILD_TXCASHBACKINDOLLARS,
                        doComponentMortgageModel.FIELD_DFCASHBACKAMOUNT,
                        CHILD_TXCASHBACKINDOLLARS_RESET_VALUE, null);
                return child;
            } else if (name.equals(CHILD_CHCASHBACKOVERRIDE)) {
                CheckBox child = new CheckBox(
                        this,
                        getdoComponentMortgageModel(),
                        CHILD_CHCASHBACKOVERRIDE,
                        doComponentMortgageModel.FIELD_DFCASHBACKAMOUNTOVERRIDE,
                        "Y",  "N", false, null);
                return child;
            } else if (name.equals(CHILD_CBREPAYMENTTYPE)) {
                ComboBox child = new ComboBox(this, getdoComponentMortgageModel(),
                        CHILD_CBREPAYMENTTYPE,
                        doComponentMortgageModel.FIELD_DFREPAYMENTTYPEID,
                        CHILD_CBREPAYMENTTYPE_RESET_VALUE, null);
                return child;
            } else if (name.equals(CHILD_CBRATELOCKIN)) {
                ComboBox child = new ComboBox(this, getdoComponentMortgageModel(),
                        CHILD_CBRATELOCKIN,
                        doComponentMortgageModel.FIELD_DFRATELOCK,
                        CHILD_CBRATELOCKIN_RESET_VALUE, null);
                child.setLabelForNoneSelected("");
                child.setOptions(cbMtgRateLockedInOptions);
                return child;
            } else if (name.equals(CHILD_TXADDITIONALINFO)) {
                  TextField child =
                    new TextField(this, getdoComponentMortgageModel(),
                            CHILD_TXADDITIONALINFO,
                            doComponentMortgageModel.FIELD_DFADDITIONALINFORMATION,
                            CHILD_TXADDITIONALINFO_RESET_VALUE, null);
                  return child;
            } else if (name.equals(CHILD_CBMTGPRODUCT)) {
                ComboBox child = new ComboBox(this, getdoComponentMortgageModel(),
                        CHILD_CBMTGPRODUCT,
                        doComponentMortgageModel.FIELD_DFMTGPRODTID,
                        CHILD_CBMTGPRODUCT_RESET_VALUE, null);
                child.setLabelForNoneSelected("");
//                child.setOptions(cbMtgProductOptions);
                return child;
            } else if (name.equals(CHILD_CBPOSTEDINTERESTRATE)) {
                ComboBox child = new ComboBox(this, 
                        getdoComponentMortgageModel(),
                        CHILD_CBPOSTEDINTERESTRATE, 
                        doComponentMortgageModel.FIELD_DFPRICINGRATEINVENTORYID,
                        CHILD_CBPOSTEDINTERESTRATE_RESET_VALUE, null);
                child.setLabelForNoneSelected("");
                child.setOptions(cbMtgPostedInterestRateOptions);
                return child;
            } else if (name.equals(CHILD_TXDISCOUNT)) {
                TextField child = new TextField(this, getdoComponentMortgageModel(),
                        CHILD_TXDISCOUNT,
                        doComponentMortgageModel.FIELD_DFDISCOUNT,
                        CHILD_TXDISCOUNT_RESET_VALUE, null);

                return child;
            } else if (name.equals(CHILD_TXPREMIUM)) {
                TextField child = new TextField(this, getdoComponentMortgageModel(),
                        CHILD_TXPREMIUM,
                        doComponentMortgageModel.FIELD_DFPREMIUM,
                        CHILD_TXPREMIUM_RESET_VALUE, null);
                return child;
            } else if (name.equals(CHILD_TXBUYDOWN)) {
                TextField child = new TextField(this, getdoComponentMortgageModel(),
                        CHILD_TXBUYDOWN,
                        doComponentMortgageModel.FIELD_DFBUYDOWNRATE,
                        CHILD_TXBUYDOWN_RESET_VALUE, null);
                return child;
            } else if (name.equals(CHILD_STNETRATE)) {
                StaticTextField child = new StaticTextField(this, 
                        getdoComponentMortgageModel(), CHILD_STNETRATE,
                        doComponentMortgageModel.FIELD_DFNETINTRESTRATE,
                        CHILD_STNETRATE_RESET_VALUE, null);
                return child;
            } else if (name.equals(CHILD_STPIPAYMENT)) {
                StaticTextField child = new StaticTextField(this,
                        getdoComponentMortgageModel(), CHILD_STPIPAYMENT,
                        doComponentMortgageModel.FIELD_DFPIPAYMENTAMOUNT,
                        CHILD_STPIPAYMENT_RESET_VALUE, null);
                return child;
            } else if (name.equals(CHILD_TXADDITIONALPIPAY)) {
                TextField child = new TextField(this, getdoComponentMortgageModel(),
                        CHILD_TXADDITIONALPIPAY,
                        doComponentMortgageModel.FIELD_DFADDITIONALPRINCIPAL,
                        CHILD_TXADDITIONALPIPAY_RESET_VALUE, null);
                return child;
            } else if (name.equals(CHILD_STPROPERTYTAX)) {
                StaticTextField child = new StaticTextField(this,
                        getdoComponentMortgageModel(), CHILD_STPROPERTYTAX,
                        doComponentMortgageModel.FIELD_DFESCROWPAYMENTAMOUNT,
                        CHILD_STPROPERTYTAX_RESET_VALUE, null);
                return child;
            } else if (name.equals(CHILD_CHALLOCATETAXESCROW)) {
                CheckBox child = new CheckBox(
                        this,
                        getdoComponentMortgageModel(),
                        CHILD_CHALLOCATETAXESCROW,
                        doComponentMortgageModel.FIELD_DFPROPERTYTAXALLOCATEFLAG,
                        "Y",  "N", false, null);
                return child;
            } else if (name.equals(CHILD_STTOTALPAYMENT)) {
                StaticTextField child = new StaticTextField(this,
                        getdoComponentMortgageModel(), CHILD_STTOTALPAYMENT,
                        doComponentMortgageModel.FIELD_DFTOTALPAYMENTAMOUNT,
                        CHILD_STTOTALPAYMENT_RESET_VALUE, null);
                return child;
            } else if (name.equals(CHILD_TXHOLDBACKAMOUNT)) {
                TextField child = new TextField(this, getdoComponentMortgageModel(),
                        CHILD_TXHOLDBACKAMOUNT,
                        doComponentMortgageModel.FIELD_DFADVANCEHOLD,
                        CHILD_TXHOLDBACKAMOUNT_RESET_VALUE, null);
                return child;
            } else if (name.equals(CHILD_TXRATEGUARANTEEPERIOD)) {

                    TextField child = new TextField(this, getdoComponentMortgageModel(),
                            CHILD_TXRATEGUARANTEEPERIOD,
                            doComponentMortgageModel.FIELD_DFRATEGUARANTEEPERIOD,
                            CHILD_TXRATEGUARANTEEPERIOD_RESET_VALUE, null);
                    return child;
            } else if (name.equals(CHILD_CBEXISTINGACOUNTFLAG)) {
                ComboBox child = new ComboBox(this, getdoComponentMortgageModel(),
                        CHILD_CBEXISTINGACOUNTFLAG,
                        doComponentMortgageModel.FIELD_DFEXISTINGACOUNTINDICATOR,
                        CHILD_CBEXISTINGACOUNTFLAG_RESET_VALUE, null);
                child.setLabelForNoneSelected("");
                child.setOptions(cbExisitingAccountOptions);
                return child;
            } else if (name.equals(CHILD_TXEXISTINGACOUNTREF)) {
                TextField child = new TextField(this, getdoComponentMortgageModel(),
                        CHILD_TXEXISTINGACOUNTREF,
                        doComponentMortgageModel.FIELD_DFEXISTINGACOUNTNUMBER,
                        CHILD_TXEXISTINGACOUNTREF_RESET_VALUE, null);
                return child;
            } else if (name.equals(CHILD_CBFIRSTPAYMENTDATEMONTH)) {
                ComboBox child = new ComboBox(this, 
                        getdoComponentMortgageModel(),
                        CHILD_CBFIRSTPAYMENTDATEMONTH,
                        CHILD_CBFIRSTPAYMENTDATEMONTH,
                        CHILD_CBFIRSTPAYMENTDATEMONTH_RESET_VALUE, null);
                child.setLabelForNoneSelected("");
                child.setOptions(cbMtgFirstPaymentDateMonthOptions);
                return child;
            } else if (name.equals(CHILD_TXFIRSTPAYMENTDATEDAY)) {
                TextField child = new TextField(this, 
                        getdoComponentMortgageModel(),
                        CHILD_TXFIRSTPAYMENTDATEDAY,
                        CHILD_TXFIRSTPAYMENTDATEDAY,
                        CHILD_TXFIRSTPAYMENTDATEDAY_RESET_VALUE, null);
                return child;
            } else if (name.equals(CHILD_TXFIRSTPAYMENTDATEYEAR)) {
                TextField child = new TextField(this, 
                        getdoComponentMortgageModel(),
                        CHILD_TXFIRSTPAYMENTDATEYEAR,
                        CHILD_TXFIRSTPAYMENTDATEYEAR,
                        CHILD_TXFIRSTPAYMENTDATEYEAR_RESET_VALUE, null);

                return child;
            } else if (name.equals(CHILD_STMATURITYDATE)) {
                StaticTextField child = new StaticTextField(this,
                        getdoComponentMortgageModel(), CHILD_STMATURITYDATE,
                        doComponentMortgageModel.FIELD_DFMATURITYDATE,
                        CHILD_STMATURITYDATE_RESET_VALUE, null);
                return child;
            } else if (name.equals(CHILD_BTRECALCMTGCOMP)) {
                Button child = new Button(
                    this, getDefaultModel(), CHILD_BTRECALCMTGCOMP,
                    CHILD_BTRECALCMTGCOMP, CHILD_BTRECALCMTGCOMP_RESET_VALUE,
                    null);
                return child;
            } else if (name.equals(CHILD_BTDELETEMTGCOMP)) {
                Button child = new Button(
                    this, getDefaultModel(), CHILD_BTDELETEMTGCOMP,
                    CHILD_BTDELETEMTGCOMP, CHILD_BTDELETEMTGCOMP_RESET_VALUE,
                    null);
                return child;
            }
        /***************MCM Impl team changes starts - XS_2.28*******************/
            
            else if (name.equals(CHILD_STMTGPRODUCT)) {
                TextField child = new TextField(this,
                        getDefaultModel(), CHILD_STMTGPRODUCT,
                        CHILD_STMTGPRODUCT,
                        CHILD_STMTGPRODUCT, null);
                return child;
            } 
            else if (name.equals(CHILD_STREPAYMENTTYPE)) {
                TextField child = new TextField(this,
                        getDefaultModel(), CHILD_STREPAYMENTTYPE,
                        CHILD_STREPAYMENTTYPE,
                        CHILD_STREPAYMENTTYPE, null);
                return child;
            } else if (name.equals(CHILD_STPOSTEDINTERESTRATE)) {
                TextField child = new TextField(this,
                        getDefaultModel(), CHILD_STPOSTEDINTERESTRATE,
                        CHILD_STPOSTEDINTERESTRATE,
                        CHILD_STPOSTEDINTERESTRATE, null);
                return child;
            } else if (name.equals(CHILD_STRATELOCK)) {
                TextField child = new TextField(this,
                        getDefaultModel(), CHILD_STRATELOCK,
                        CHILD_STRATELOCK,
                        CHILD_STRATELOCK, null);
                return child;
            } else if (name.equals(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT)) {
                StaticTextField child = new StaticTextField(this, getDefaultModel(),
                        CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT,
                        CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT,
                        CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT_RESET_VALUE, null);
                return child;
            } else if (name.equals(CHILD_TXPOSTEDRATE)) {
                TextField child =
                    new TextField(this, 
                            getdoComponentMortgageModel(),
                            CHILD_TXPOSTEDRATE,
                            doComponentMortgageModel.FIELD_DFPOSTEDRATE,
                            CHILD_TXPOSTEDRATE_RESET_VALUE, null);
                return child;
            }
        /***************MCM Impl team changes ends - XS_2.28*********************/

            else throw new IllegalArgumentException("Invalid child name [" + name + "]");
    }

    /**
     * <p>
     * registerChildren
     * </p>
     * <p>
     * Description: registerChildren method for Jato Framework
     * </p>
     * 
     * @param
     * @return
     */
    protected void registerChildren() {
        
        registerChild(CHILD_HDCOMPONENTIDMTG, HiddenField.class);
        registerChild(CHILD_HDCOPYIDMTG, HiddenField.class);
        // MCM Team: fixed cosmetic issue, STARTS
        registerChild(CHILD_STMTGTITLE, StaticTextField.class);
        registerChild(CHILD_IMMTGSECTIONDEVIDER, ImageField.class);
        // MCM Team: fixed cosmetic issue, ENDS
        registerChild(CHILD_CBCOMPONENTTYPE, ComboBox.class);
        registerChild(CHILD_TXMORTGAGEAMOUNT, TextField.class);
        registerChild(CHILD_STMIPREMIUM, StaticTextField.class);
        registerChild(CHILD_CHALLOCATEMIPREM, CheckBox.class);
        registerChild(CHILD_STTOTALAMOUNT, StaticTextField.class);
        registerChild(CHILD_TXAMORTIZATIONPERIODYEARS, TextField.class);
        registerChild(CHILD_TXAMORTIZATIONPERIODMONTHS, TextField.class);
        registerChild(CHILD_TXEFFECTAPYEARS, TextField.class);
        registerChild(CHILD_TXEFFECTAPMONTHS, TextField.class);
        registerChild(CHILD_TBPAYMENTTERMDESCRIPTION, TextField.class);
        registerChild(CHILD_HDPAYMENTTERMID, HiddenField.class);
        registerChild(CHILD_TXACTUALPAYMENTTERMMONTHS, TextField.class);
        registerChild(CHILD_TXACTUALPAYMENTTERMYEARS, TextField.class);
        registerChild(CHILD_CBPAYMENTFREQUENCY, ComboBox.class);
        registerChild(CHILD_CBPREPAYMENTPENALTY, ComboBox.class);
        registerChild(CHILD_CBPRIVILEGEPAYMENTOPTION, ComboBox.class);
        registerChild(CHILD_TXCOMMISIONCODE, TextField.class);
        registerChild(CHILD_TXCASHBACKINPERCENTAGE, TextField.class);
        registerChild(CHILD_TXCASHBACKINDOLLARS, TextField.class);
        registerChild(CHILD_CHCASHBACKOVERRIDE, CheckBox.class);
        registerChild(CHILD_CBREPAYMENTTYPE, ComboBox.class);
        registerChild(CHILD_CBRATELOCKIN, ComboBox.class);
        registerChild(CHILD_TXADDITIONALINFO, TextField.class);
        registerChild(CHILD_CBMTGPRODUCT, ComboBox.class);
        registerChild(CHILD_CBPOSTEDINTERESTRATE, ComboBox.class);
        registerChild(CHILD_TXDISCOUNT, TextField.class);
        registerChild(CHILD_TXPREMIUM, TextField.class);
        registerChild(CHILD_TXBUYDOWN, TextField.class);
        registerChild(CHILD_STNETRATE, TextField.class);
        registerChild(CHILD_STPIPAYMENT, StaticTextField.class);
        registerChild(CHILD_TXADDITIONALPIPAY, TextField.class);
        registerChild(CHILD_STPROPERTYTAX, StaticTextField.class);
        registerChild(CHILD_CHALLOCATETAXESCROW, CheckBox.class);
        registerChild(CHILD_STTOTALPAYMENT, StaticTextField.class);
        registerChild(CHILD_TXHOLDBACKAMOUNT, TextField.class);
        registerChild(CHILD_TXRATEGUARANTEEPERIOD, TextField.class);
        registerChild(CHILD_CBEXISTINGACOUNTFLAG, TextField.class);
        registerChild(CHILD_TXEXISTINGACOUNTREF, TextField.class);
        registerChild(CHILD_CBFIRSTPAYMENTDATEMONTH, TextField.class);
        registerChild(CHILD_TXFIRSTPAYMENTDATEDAY, TextField.class);
        registerChild(CHILD_TXFIRSTPAYMENTDATEYEAR, TextField.class);
        registerChild(CHILD_STMATURITYDATE, TextField.class);
        registerChild(CHILD_BTRECALCMTGCOMP, Button.class);
        registerChild(CHILD_BTDELETEMTGCOMP, Button.class);
        /***************MCM Impl team changes starts - XS_2.28*******************/
        registerChild(CHILD_STPOSTEDINTERESTRATE, TextField.class);
        registerChild(CHILD_STMTGPRODUCT, TextField.class);
        registerChild(CHILD_STREPAYMENTTYPE, TextField.class);
        registerChild(CHILD_STRATELOCK, TextField.class);
        registerChild(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT, StaticTextField.class);
        registerChild(CHILD_TXPOSTEDRATE, TextField.class);
        /***************MCM Impl team changes ends - XS_2.28*********************/
    }
    
    /**
     * <p>resetChildren</p>
     * <p>Description: resetChildren method for Jato Framework</p>
     * 
     * @param 
     * @return
     */
    public void resetChildren() {

        super.resetChildren();
        
        getHdMtgComponentId().setValue(CHILD_HDCOMPONENTIDMTG_RESET_VALUE);
        getHdMtgCopyId().setValue(CHILD_HDCOMPONENTIDMTG_RESET_VALUE);
        // MCM Team: fixed cosmetic issue, STARTS
        getStMtgTitle().setValue(CHILD_STMTGDTITLE_RESET_VALUE);
        getImMtgSectionDevider().setValue(CHILD_IMMTGSECTIONDEVIDER_RESET_VALUE);
        // MCM Team: fixed cosmetic issue, ENDS
        getCbComponentType().setValue(CHILD_CBCOMPONENTTYPE_RESET_VALUE);
        getTxMortgageAmount().setValue(CHILD_TXMORTGAGEAMOUNT_RESET_VALUE);
        getStMIPremium().setValue(CHILD_STMIPREMIUM_RESET_VALUE);
        getChAllocateMIPremium().setValue(CHILD_CHALLOCATEMIPREM_RESET_VALUE);
        getStTotalAmount().setValue(CHILD_STTOTALAMOUNT_RESET_VALUE);
        getTxAmortizationPeriodYears().setValue(CHILD_TXAMORTIZATIONPERIODYEARS_RESET_VALUE);
        getTxAmortizationPeriodMonths().setValue(CHILD_TXAMORTIZATIONPERIODMONTHS_RESET_VALUE);
        getTxEffectiveAPYears().setValue(CHILD_TXEFFECTAPYEARS_RESET_VALUE);
        getTxEffectiveAPMonths().setValue(CHILD_TXEFFECTAPMONTHS_RESET_VALUE);
        getTbPaymentTermDescription().setValue(CHILD_TBPAYMENTTERMDESCRIPTION_RESET_VALUE);
        getHdPaymentTermId().setValue(CHILD_HDPAYMENTTERMID_RESET_VALUE);
        getTxActualPaymentTermMonths().setValue(CHILD_TXACTUALPAYMENTTERMMONTHS_RESET_VALUE);
        getTxActualPaymentTermYears().setValue(CHILD_TXACTUALPAYMENTTERMYEARS_RESET_VALUE);
        getCbPaymentFrequency().setValue(CHILD_CBPAYMENTFREQUENCY_RESET_VALUE);
        getCbRepaymentType().setValue(CHILD_CBPREPAYMENTPENALTY_RESET_VALUE);
        getTxCashbackPercentage().setValue(CHILD_TXCASHBACKINPERCENTAGE_RESET_VALUE);
        getTxCashbackInDollars().setValue(CHILD_TXCASHBACKINDOLLARS_RESET_VALUE);
        getCbRepaymentType().setValue(CHILD_CBREPAYMENTTYPE_RESET_VALUE);
        getCbRateLockin().setValue(CHILD_CBRATELOCKIN_RESET_VALUE);
        getCbPostedInterestRate().setValue(CHILD_CBPOSTEDINTERESTRATE_RESET_VALUE);
        getTxBuydown().setValue(CHILD_TXBUYDOWN_RESET_VALUE);
        getStNetrate().setValue(CHILD_STNETRATE_RESET_VALUE);
        getStPIPayment().setValue(CHILD_STPIPAYMENT_RESET_VALUE);
        getTxAdditionalInfo().setValue(CHILD_TXADDITIONALPIPAY_RESET_VALUE);
        getStPropertyTax().setValue(CHILD_STPROPERTYTAX_RESET_VALUE);
        getStTotalpayment().setValue(CHILD_STTOTALPAYMENT_RESET_VALUE);
        getTxHoldbackAmount().setValue(CHILD_TXHOLDBACKAMOUNT_RESET_VALUE);
        getTxRateGuaranteePeriod().setValue(CHILD_TXRATEGUARANTEEPERIOD_RESET_VALUE);
        getCbExistingAccountFlag().setValue(CHILD_CBEXISTINGACOUNTFLAG_RESET_VALUE);
        getTxExistingAccountRef().setValue(CHILD_TXEXISTINGACOUNTREF_RESET_VALUE);
        getCbFirstPaymentDateMonth().setValue(CHILD_CBFIRSTPAYMENTDATEMONTH_RESET_VALUE);
        getTxFirstPaymentDateDay().setValue(CHILD_TXFIRSTPAYMENTDATEDAY_RESET_VALUE);
        getTxFirstPaymentDateYear().setValue(CHILD_TXFIRSTPAYMENTDATEYEAR_RESET_VALUE);
        getStMaturisydate().setValue(CHILD_STMATURITYDATE_RESET_VALUE);
        getCbMtgProduct().setValue(CHILD_CBMTGPRODUCT_RESET_VALUE);
        getBtRecalcMTGComp().setValue(CHILD_BTRECALCMTGCOMP_RESET_VALUE);
        getBtDeleteMTGComp().setValue(CHILD_BTDELETEMTGCOMP_RESET_VALUE);
        getStMtgTitle().setValue(CHILD_STMTGDTITLE_RESET_VALUE);
        getImMtgSectionDevider().setValue(CHILD_IMMTGSECTIONDEVIDER_RESET_VALUE);

        /***************MCM Impl team changes starts - XS_2.28*******************/
        getStPostedInterestRate().setValue(CHILD_STPOSTEDINTERESTRATE_RESET_VALUE);
        getStMtgProduct().setValue(CHILD_STMTGPRODUCT_RESET_VALUE);
        getStRepaymentType().setValue(CHILD_STREPAYMENTTYPE_RESET_VALUE);
        getStRateLock().setValue(CHILD_STRATELOCK_RESET_VALUE);
        getStInitDyanmicListJavaScript().setValue(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT_RESET_VALUE);
        getTxPostedRate().setValue(CHILD_TXPOSTEDRATE_RESET_VALUE);
        /***************MCM Impl team changes ends - XS_2.28*********************/
        
    }

    /**
     * <p>nextTile</p>
     * <p>Description: display next mortgage section tiled view </p>
     */
    public boolean nextTile() throws ModelControlException {

        boolean movedToRow = super.nextTile();
        if (movedToRow) {
            ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler
            .cloneSS();
            handler.pageGetState(this.getParentViewBean());
            handler.setMTGCompDisplayFields(getTileIndex());
            handler.pageSaveState();
        }
        return movedToRow;
    }
    
    // geters starts    
    public HiddenField getHdMtgComponentId() {
        return (HiddenField) getChild(CHILD_HDCOMPONENTIDMTG);
    }
    
    public HiddenField getHdMtgCopyId() {
        return (HiddenField) getChild(CHILD_HDCOPYIDMTG);
    }
    
    // MCM Team: fixed cosmetic issue, STARTS
    public StaticTextField getStMtgTitle() {
        return (StaticTextField) getChild(CHILD_STMTGTITLE);
    }
    
    public ImageField getImMtgSectionDevider() {
        return (ImageField) getChild(CHILD_IMMTGSECTIONDEVIDER);
    }
    // MCM Team: fixed cosmetic issue, ENDS
    
    public ComboBox getCbComponentType() {
        return (ComboBox) getChild(CHILD_CBCOMPONENTTYPE);
    }
    
    public TextField getTxMortgageAmount() {
        return (TextField) getChild(CHILD_TXMORTGAGEAMOUNT);
    }

    public StaticTextField getStMIPremium() {
        return (StaticTextField) getChild(CHILD_STMIPREMIUM);
    }

    public CheckBox getChAllocateMIPremium() {
        return (CheckBox) getChild(CHILD_CHALLOCATEMIPREM);
    }

    public StaticTextField getStTotalAmount() {
        return (StaticTextField) getChild(CHILD_STTOTALAMOUNT);
    }

    public TextField getTxAmortizationPeriodYears() {
        return (TextField) getChild(CHILD_TXAMORTIZATIONPERIODYEARS);
    }

    public TextField getTxAmortizationPeriodMonths() {
        return (TextField) getChild(CHILD_TXAMORTIZATIONPERIODMONTHS);
    }

    public TextField getTxEffectiveAPYears() {
        return (TextField) getChild(CHILD_TXEFFECTAPYEARS);
    }

    public TextField getTxEffectiveAPMonths() {
        return (TextField) getChild(CHILD_TXEFFECTAPMONTHS);
    }

    public TextField getTbPaymentTermDescription() {
        return (TextField) getChild(CHILD_TBPAYMENTTERMDESCRIPTION);
    }

    public HiddenField getHdPaymentTermId() {
        return (HiddenField) getChild(CHILD_HDPAYMENTTERMID);
    }

    public TextField getTxActualPaymentTermMonths() {
        return (TextField) getChild(CHILD_TXACTUALPAYMENTTERMMONTHS);
    }

    public TextField getTxActualPaymentTermYears() {
        return (TextField) getChild(CHILD_TXACTUALPAYMENTTERMYEARS);
    }

    public ComboBox getCbPaymentFrequency() {
        return (ComboBox) getChild(CHILD_CBPAYMENTFREQUENCY);
    }

    public ComboBox getCbPrepaymentPenalty() {
        return (ComboBox) getChild(CHILD_CBPREPAYMENTPENALTY);
    }

    public ComboBox getCbPrivilagePaymentOption() {
        return (ComboBox) getChild(CHILD_CBPRIVILEGEPAYMENTOPTION);
    }

    public TextField getTxCommisionCode() {
        return (TextField) getChild(CHILD_TXCOMMISIONCODE);
    }

    public TextField getTxCashbackPercentage() {
        return (TextField) getChild(CHILD_TXCASHBACKINPERCENTAGE);
    }

    public TextField getTxCashbackInDollars() {
        return (TextField) getChild(CHILD_TXCASHBACKINDOLLARS);
    }

    public CheckBox getChCashbackOverride() {
        return (CheckBox) getChild(CHILD_CHCASHBACKOVERRIDE);
    }

    public ComboBox getCbRepaymentType() {
        return (ComboBox) getChild(CHILD_CBREPAYMENTTYPE);
    }

    public ComboBox getCbRateLockin() {
        return (ComboBox) getChild(CHILD_CBRATELOCKIN);
    }

    public TextField getTxAdditionalInfo() {
        return (TextField) getChild(CHILD_TXADDITIONALINFO);
    }

    public ComboBox getMtgProduct() {
        return (ComboBox) getChild(CHILD_CBMTGPRODUCT);
    }

    public ComboBox getCbPostedInterestRate() {
        return (ComboBox) getChild(CHILD_CBPOSTEDINTERESTRATE);
    }

    public TextField getTxDiscount() {
        return (TextField) getChild(CHILD_TXDISCOUNT);
    }

    public TextField getTxPremium() {
        return (TextField) getChild(CHILD_TXPREMIUM);
    }

    public TextField getTxBuydown() {
        return (TextField) getChild(CHILD_TXBUYDOWN);
    }

    public StaticTextField getStNetrate() {
        return (StaticTextField) getChild(CHILD_STNETRATE);
    }

    public StaticTextField getStPIPayment() {
        return (StaticTextField) getChild(CHILD_STPIPAYMENT);
    }

    public TextField getTxAdditionalPIPay() {
        return (TextField) getChild(CHILD_TXADDITIONALPIPAY);
    }

    public StaticTextField getStPropertyTax() {
        return (StaticTextField) getChild(CHILD_STPROPERTYTAX);
    }

    public CheckBox getChAllocateTaxEscrow() {
        return (CheckBox) getChild(CHILD_CHALLOCATETAXESCROW);
    }

    public StaticTextField getStTotalpayment() {
        return (StaticTextField) getChild(CHILD_STTOTALPAYMENT);
    }

    public TextField getTxHoldbackAmount() {
        return (TextField) getChild(CHILD_TXHOLDBACKAMOUNT);
    }

    public TextField getTxRateGuaranteePeriod() {
        return (TextField) getChild(CHILD_TXRATEGUARANTEEPERIOD);
    }

    public ComboBox getCbExistingAccountFlag() {
        return (ComboBox) getChild(CHILD_CBEXISTINGACOUNTFLAG);
    }

    public TextField getTxExistingAccountRef() {
        return (TextField) getChild(CHILD_TXEXISTINGACOUNTREF);
    }

    public ComboBox getCbFirstPaymentDateMonth() {
        return (ComboBox) getChild(CHILD_CBFIRSTPAYMENTDATEMONTH);
    }

    public TextField getTxFirstPaymentDateDay() {
        return (TextField) getChild(CHILD_TXFIRSTPAYMENTDATEDAY);
    }

    public TextField getTxFirstPaymentDateYear() {
        return (TextField) getChild(CHILD_TXFIRSTPAYMENTDATEYEAR);
    }

    public StaticTextField getStMaturisydate() {
        return (StaticTextField) getChild(CHILD_STMATURITYDATE);
    }

    public ComboBox getCbMtgProduct() {
        return (ComboBox) getChild(CHILD_CBMTGPRODUCT);
    }
    
    public Button getBtRecalcMTGComp() {
        return (Button) getChild(CHILD_BTRECALCMTGCOMP);
    }

    public Button getBtDeleteMTGComp() {
        return (Button) getChild(CHILD_BTDELETEMTGCOMP);
    }
    // geters ends    
    
    /**
     * <p>handleBtRecalcMTGCompRequest</p>
     * <p>Description: Handle clicking Recalclate</p>
     * @param event: RequestInvocationEvent
     * @return 
     * @author MCM Team
     * -- XS 2.27
     */
    public void handleBtRecalcMTGCompRequest(RequestInvocationEvent event)
        throws ServletException, IOException {
        _log.trace("Web event invoked: "+getClass().getName()+".BtRecalcMTGComp");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this.getParentViewBean());
        handler.handleRecalc();
        handler.postHandlerProtocol();    
        return;
    }

    /**
     * <p>handleBtDeleteMTGCompRequest</p>
     * <p>Description: Handle clicking Delete</p>
     * @param event: RequestInvocationEvent
     * @return 
     * @author MCM Team
     * @version 1.0-- XS 2.27
     * @version 1.1 XS 2.32 delete component
     */
    public void handleBtDeleteMTGCompRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _log.trace("Web event invoked: "+getClass().getName()+".BtDeleteMTGComp");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this.getParentViewBean());
        int tileIndx = ((TiledViewRequestInvocationEvent)(event)).getTileNumber();
        handler.handleDeleteMTGComp(tileIndx);
        handler.postHandlerProtocol();    
        return;
    }

    /***************MCM Impl team changes starts - XS_2.28*******************/
    
    public TextField getStPostedInterestRate() {
        return (TextField) getChild(CHILD_STPOSTEDINTERESTRATE);
    }
    public TextField getStMtgProduct() {
        return (TextField) getChild(CHILD_STMTGPRODUCT);
    }
    public TextField getStRepaymentType() {
        return (TextField) getChild(CHILD_STREPAYMENTTYPE);
    }
    public TextField getStRateLock() {
        return (TextField) getChild(CHILD_STRATELOCK);
    }
    public StaticTextField getStInitDyanmicListJavaScript() {
        return (StaticTextField) getChild(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT);
    }
    public TextField getTxPostedRate() {
        return (TextField) getChild(CHILD_TXPOSTEDRATE);
    }
    /***************MCM Impl team changes ends - XS_2.28*********************/
    
    /**
     * <p>getWebActionModels</p>
     * <p>Description: get Auto executed models</p>
     *
     * @param int: executionType
     * @return Model[]
     */
    public Model[] getWebActionModels(int executionType) {
        List modelList = new ArrayList();
        switch (executionType) {
            case MODEL_TYPE_RETRIEVE:
                ;
                break;

            case MODEL_TYPE_UPDATE:
                ;
                break;

            case MODEL_TYPE_DELETE:
                ;
                break;

            case MODEL_TYPE_INSERT:
                ;
                break;

            case MODEL_TYPE_EXECUTE:
                ;
                break;
        }
        return (Model[]) modelList.toArray(new Model[0]);
    }

    /**
     * <p>beginDisplay</p>
     * <p>Description: beginDisplay method for Jato Framework</p>
     * 
     * @param event: DisplayEvent
     * @return
     */
    public void beginDisplay(DisplayEvent event) throws ModelControlException {

        ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler.cloneSS();
        handler.pageGetState(this.getParentViewBean());

        if (getPrimaryModel() == null) {
            throw new ModelControlException("Primary model is null");
        }
        
        String yesStr = BXResources.getGenericMsg("YES_LABEL", handler
                .getTheSessionState().getLanguageId());
        String noStr = BXResources.getGenericMsg("NO_LABEL", handler
                .getTheSessionState().getLanguageId());

        cbComponentTypeOptions.populate(getRequestContext());
        cbMtgRateLockedInOptions.setOptions(new String[] { yesStr, noStr },
                new String[] { "Y", "N" });
        cbExisitingAccountOptions.setOptions(new String[] { yesStr, noStr },
                new String[] { "Y", "N" });
        
        cbPaymentFrequencyOptions.populate(getRequestContext());

        cbPrepaymentPenaltyOptions.populate(getRequestContext());

        cbPrivilegePaymentOptions.populate(getRequestContext());

        super.beginDisplay(event);
        resetTileIndex();
    }

    /**
     * <p>beforeModelExecutes</p>
     * <p>Description: beforeModelExecutes method for Jato Framework</p>
     * 
     * @param model: Model
     * @param executionContext: int
     * @return boolean
     */
    public boolean beforeModelExecutes(Model model, int executionContext) {

        return super.beforeModelExecutes(model, executionContext);
    }

    /**
     * <p>afterModelExecutes</p>
     * <p>Description: beforeModelExecutes method for Jato Framework</p>
     * 
     * @param model: Model
     * @param executionContext: int
     * @return
     */
    public void afterModelExecutes(Model model, int executionContext) {

        // This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
        super.afterModelExecutes(model, executionContext);

    }

    /**
     * <p>afterModelExecutes</p>
     * <p>Description: afterModelExecutes method for Jato Framework</p>
     * 
     * @param executionContext: int
     * @return
     */
    public void afterAllModelsExecute(int executionContext) {
        super.afterAllModelsExecute(executionContext);

    }

    /**
     * <p>onModelError</p>     * 
     * <p>Description: onModelError method for Jato Framework</p>
     * 
     * @param model: Model
     * @param executionContext: int
     * @param exception: ModelControlException
     * @return
     */
    public void onModelError(Model model, int executionContext,
                             ModelControlException exception) throws
        ModelControlException {

        super.onModelError(model, executionContext, exception);
    }

    public void setdoComponentMortgageModel( doComponentMortgageModel model) {
        doComponentMortgageModel = model;
    }

    public doComponentMortgageModel getdoComponentMortgageModel() {
        if (doComponentMortgageModel == null) {
            doComponentMortgageModel = (doComponentMortgageModel) getModel(
                    doComponentMortgageModel.class);
        }
        return doComponentMortgageModel;
    }

    // //////////////////////////////////////////////////////////////////////////////
    // Class variables
    // //////////////////////////////////////////////////////////////////////////////

    public static Map FIELD_DESCRIPTORS;
    public static final String CHILD_TBDEALID = "tbDealId";
    public static final String CHILD_CBCOMPONENTTYPE = "cbComponentType";
    public static final String CHILD_CBCOMPONENTTYPE_RESET_VALUE = "";
    public static final String CHILD_TXMORTGAGEAMOUNT = "txMortgageAmount";
    public static final String CHILD_TXMORTGAGEAMOUNT_RESET_VALUE = "";
    public static final String CHILD_STMIPREMIUM = "stMIPremium";
    public static final String CHILD_STMIPREMIUM_RESET_VALUE = "";
    public static final String CHILD_CHALLOCATEMIPREM = "chAllocateMIPremium";
    public static final String CHILD_CHALLOCATEMIPREM_RESET_VALUE = "";
    public static final String CHILD_STTOTALAMOUNT = "stTotalAmount";
    public static final String CHILD_STTOTALAMOUNT_RESET_VALUE = "";
    public static final String CHILD_TXAMORTIZATIONPERIODYEARS = "txAmortizationPeriodYears";
    public static final String CHILD_TXAMORTIZATIONPERIODYEARS_RESET_VALUE = "";
    public static final String CHILD_TXAMORTIZATIONPERIODMONTHS = "txAmortizationPeriodMonths";
    public static final String CHILD_TXAMORTIZATIONPERIODMONTHS_RESET_VALUE = "";
    public static final String CHILD_TXEFFECTAPYEARS = "txEffectAPYears";
    public static final String CHILD_TXEFFECTAPYEARS_RESET_VALUE = "";
    public static final String CHILD_TXEFFECTAPMONTHS = "txEffectAPMonths";
    public static final String CHILD_TXEFFECTAPMONTHS_RESET_VALUE = "";
    public static final String CHILD_TBPAYMENTTERMDESCRIPTION = "tbPaymentTermDescription";
    public static final String CHILD_TBPAYMENTTERMDESCRIPTION_RESET_VALUE = "";
    public static final String CHILD_HDPAYMENTTERMID = "hdPaymentTermId";
    public static final String CHILD_HDPAYMENTTERMID_RESET_VALUE = "";
    public static final String CHILD_TXACTUALPAYMENTTERMMONTHS = "txActualPayMonths";
    public static final String CHILD_TXACTUALPAYMENTTERMMONTHS_RESET_VALUE = "";
    public static final String CHILD_TXACTUALPAYMENTTERMYEARS = "txActualPayYears";
    public static final String CHILD_TXACTUALPAYMENTTERMYEARS_RESET_VALUE = "";
    public static final String CHILD_CBPAYMENTFREQUENCY = "cbPaymentFrequency";
    public static final String CHILD_CBPAYMENTFREQUENCY_RESET_VALUE = "";
    public static final String CHILD_CBPREPAYMENTPENALTY = "cbPrePaymentPenalty";
    public static final String CHILD_CBPREPAYMENTPENALTY_RESET_VALUE = "";
    public static final String CHILD_CBPRIVILEGEPAYMENTOPTION = "cbPrivilegePaymentOption";
    public static final String CHILD_CBPRIVILEGEPAYMENTOPTION_RESET_VALUE = "";
    public static final String CHILD_TXCOMMISIONCODE = "txCommissionCode";
    public static final String CHILD_TXCOMMISIONCODE_RESET_VALUE = "";
    public static final String CHILD_TXCASHBACKINPERCENTAGE = "txCashBackInPercentage";
    public static final String CHILD_TXCASHBACKINPERCENTAGE_RESET_VALUE = "";
    public static final String CHILD_TXCASHBACKINDOLLARS = "txCashbackAmount";
    public static final String CHILD_TXCASHBACKINDOLLARS_RESET_VALUE = "";
    public static final String CHILD_CHCASHBACKOVERRIDE = "chCashbackOverride";
    public static final String CHILD_CHCASHBACKOVERRIDE_RESET_VALUE = "";
    public static final String CHILD_CBREPAYMENTTYPE = "cbRepaymentType";
    public static final String CHILD_CBREPAYMENTTYPE_RESET_VALUE = "";
    public static final String CHILD_CBRATELOCKIN = "cbRateLockedIn";
    public static final String CHILD_CBRATELOCKIN_RESET_VALUE = "";
    public static final String CHILD_TXADDITIONALINFO = "txMtgAdditionalInfo";
    public static final String CHILD_TXADDITIONALINFO_RESET_VALUE = "";
    public static final String CHILD_CBMTGPRODUCT = "cbMtgProduct";
    public static final String CHILD_CBMTGPRODUCT_RESET_VALUE = "";
    public static final String CHILD_CBPOSTEDINTERESTRATE = "cbMtgPostedInterestRate";
    public static final String CHILD_CBPOSTEDINTERESTRATE_RESET_VALUE = "";
    public static final String CHILD_TXDISCOUNT = "txDiscount";
    public static final String CHILD_TXDISCOUNT_RESET_VALUE = "";
    public static final String CHILD_TXPREMIUM = "txPremium";
    public static final String CHILD_TXPREMIUM_RESET_VALUE = "";
    public static final String CHILD_TXBUYDOWN = "txBuyDown";
    public static final String CHILD_TXBUYDOWN_RESET_VALUE = "";
    public static final String CHILD_STNETRATE = "stNetRate";
    public static final String CHILD_STNETRATE_RESET_VALUE = "";
    public static final String CHILD_STPIPAYMENT = "stPIPayment";
    public static final String CHILD_STPIPAYMENT_RESET_VALUE = "";
    public static final String CHILD_TXADDITIONALPIPAY = "txAdditionalPIPay";
    public static final String CHILD_TXADDITIONALPIPAY_RESET_VALUE = "";
    public static final String CHILD_STPROPERTYTAX = "stPropertyTax";
    public static final String CHILD_STPROPERTYTAX_RESET_VALUE = "";
    public static final String CHILD_CHALLOCATETAXESCROW = "chAllocateTaxEscrow";
    public static final String CHILD_STTOTALESCROW_RESET_VALUE = "";
    public static final String CHILD_STTOTALPAYMENT = "stTotalPayment";
    public static final String CHILD_STTOTALPAYMENT_RESET_VALUE = "";
    public static final String CHILD_TXHOLDBACKAMOUNT = "txHoldbackAmount";
    public static final String CHILD_TXHOLDBACKAMOUNT_RESET_VALUE = "";
    public static final String CHILD_TXRATEGUARANTEEPERIOD = "txRateGuaranteePeriod";
    public static final String CHILD_TXRATEGUARANTEEPERIOD_RESET_VALUE = "";
    public static final String CHILD_CBEXISTINGACOUNTFLAG = "cbExistingAccountFlag";
    public static final String CHILD_CBEXISTINGACOUNTFLAG_RESET_VALUE = "";
    public static final String CHILD_TXEXISTINGACOUNTREF = "txExistingAccountRef";
    public static final String CHILD_TXEXISTINGACOUNTREF_RESET_VALUE = "";
    public static final String CHILD_CBFIRSTPAYMENTDATEMONTH = "cbFirstPaymentDateMonth";
    public static final String CHILD_CBFIRSTPAYMENTDATEMONTH_RESET_VALUE = "";
    public static final String CHILD_TXFIRSTPAYMENTDATEDAY = "txFirstPaymentDateDay";
    public static final String CHILD_TXFIRSTPAYMENTDATEDAY_RESET_VALUE = "";
    public static final String CHILD_TXFIRSTPAYMENTDATEYEAR = "txFirstPaymentDateYear";
    public static final String CHILD_TXFIRSTPAYMENTDATEYEAR_RESET_VALUE = "";
    public static final String CHILD_STMATURITYDATE = "stMaturityDate";
    public static final String CHILD_STMATURITYDATE_RESET_VALUE = "";
    public static final String CHILD_BTRECALCMTGCOMP = "btRecalcMTGComp";
    public static final String CHILD_BTRECALCMTGCOMP_RESET_VALUE = "";
    public static final String CHILD_BTDELETEMTGCOMP = "btDeleteMTGComp";
    public static final String CHILD_BTDELETEMTGCOMP_RESET_VALUE = "";
    public static final String CHILD_HDCOMPONENTIDMTG = "hdComponentIdMTG";
    public static final String CHILD_HDCOMPONENTIDMTG_RESET_VALUE = "";
    public static final String CHILD_HDCOPYIDMTG = "hdCopyIdMTG";
    public static final String CHILD_HDCOPYIDMTG_RESET_VALUE = "";
    
    // MCM Team: fixed cosmetic issue, STARTS
    // i.e. Mortgages display only once
    // line after that
    public static final String CHILD_STMTGTITLE = "stMTGTitle";
    public static final String CHILD_STMTGDTITLE_RESET_VALUE = "";    
    public static final String CHILD_IMMTGSECTIONDEVIDER = "imMTGSectionDevider";
    public static final String CHILD_IMMTGSECTIONDEVIDER_RESET_VALUE = "";
    // MCM Team: fixed cosmetic issue, ENDS
    
    private OptionList cbComponentTypeOptions = new CbComponentTypeOptionList();    
    private OptionList cbPaymentFrequencyOptions = new CbPaymentFrequencyOptions();
    private OptionList cbPrepaymentPenaltyOptions = new CbPrepaymentPenaltyOptions();
    private OptionList cbPrivilegePaymentOptions = new CbPrivilegePaymentOptions();
//    private static OptionList cbMtgProductOptions = new OptionList(
//            new String[] {}, new String[] {});                              
    private OptionList cbMtgPostedInterestRateOptions = new OptionList(
            new String[] {}, new String[] {});
    private OptionList cbMtgRateLockedInOptions = new OptionList();
    private OptionList cbExisitingAccountOptions = new OptionList();    
    private OptionList cbMtgFirstPaymentDateMonthOptions = new OptionList(
            new String[] {}, new String[] {});


    /***************MCM Impl team changes starts - XS_2.28*******************/
    
    public static final String CHILD_STPOSTEDINTERESTRATE = "stPostedInterestRate";
    public static final String CHILD_STPOSTEDINTERESTRATE_RESET_VALUE = "";

    public static final String CHILD_STMTGPRODUCT = "stMtgProduct";
    public static final String CHILD_STMTGPRODUCT_RESET_VALUE = "";

    public static final String CHILD_STREPAYMENTTYPE = "stRepaymentType";
    public static final String CHILD_STREPAYMENTTYPE_RESET_VALUE = "";

    public static final String CHILD_STRATELOCK = "stRateLock";
    public static final String CHILD_STRATELOCK_RESET_VALUE = "";
    
    public static final String CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT = "stInitJavaScript"; 
    public static final String CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT_RESET_VALUE = "";
    
    public static final String CHILD_TXPOSTEDRATE = "txPostedRate";
    public static final String CHILD_TXPOSTEDRATE_RESET_VALUE = "";
    
    /***************MCM Impl team changes ends - XS_2.28*********************/
    

    /**
     * <p>CbComponentTypeOptionList</p>
     * <p>inner Class for ComponentType comboBox</p>
     * 
     * @author MCM Implimentation Team
     *
     */
    class CbComponentTypeOptionList 
        extends pgUWorksheetViewBean.BaseComboBoxOptionList {

        CbComponentTypeOptionList() {
        }

        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
                .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
                .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "COMPONENTTYPE",
                    cbComponentTypeOptions);
        }
    }

    /**
     * <p>CbPaymentFrequencyOptions</p>
     * <p>inner Class for PaymentFrequenty comboBox</p>
     * 
     * @author MCM Implimentation Team
     *
     */
    class CbPaymentFrequencyOptions 
        extends pgUWorksheetViewBean.BaseComboBoxOptionList {

        CbPaymentFrequencyOptions() {
        }

        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
                .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
                .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "PAYMENTFREQUENCY",
                    cbPaymentFrequencyOptions);
        }
    }

    /**
     * <p>CbPrepaymentPenaltyOptions</p>
     * <p>inner Class for PrepaymentPenalty comboBox</p>
     * 
     * @author MCM Implementation Team
     *
     */
    class CbPrepaymentPenaltyOptions 
        extends pgUWorksheetViewBean.BaseComboBoxOptionList {

        CbPrepaymentPenaltyOptions() {
        }

        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
                .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
                .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "PREPAYMENTOPTIONS",
                    cbPrepaymentPenaltyOptions);

        }
    }

    /**
     * <p>CbPrivilegePaymentOptions</p>
     * <p>inner Class for PrivilegePayment comboBox</p>
     * 
     * @author MCM Implementation Team
     *
     */
     class CbPrivilegePaymentOptions 
        extends pgUWorksheetViewBean.BaseComboBoxOptionList {

        CbPrivilegePaymentOptions() {
        }

        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
                .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
                .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "PRIVILEGEPAYMENT",
                    cbPrivilegePaymentOptions);
        }
    }

    // //////////////////////////////////////////////////////////////////////////
    // Instance variables
    // //////////////////////////////////////////////////////////////////////////

    private doComponentMortgageModel doComponentMortgageModel = null;
    private ComponentDetailsHandler handler = new ComponentDetailsHandler();
    
}
