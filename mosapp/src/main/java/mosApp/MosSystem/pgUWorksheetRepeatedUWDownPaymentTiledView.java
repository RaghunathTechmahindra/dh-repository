package mosApp.MosSystem;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import MosSystem.Sc;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.event.TiledViewRequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.TextField;



/**
 *
 *
 *
 */
public class pgUWorksheetRepeatedUWDownPaymentTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgUWorksheetRepeatedUWDownPaymentTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doUWDealDownPaymentModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getHdUWDownPaymentSourceId().setValue(CHILD_HDUWDOWNPAYMENTSOURCEID_RESET_VALUE);
		getHdUWDownPaymentCopyId().setValue(CHILD_HDUWDOWNPAYMENTCOPYID_RESET_VALUE);
		getCbUWDownPaymentSource().setValue(CHILD_CBUWDOWNPAYMENTSOURCE_RESET_VALUE);
		getTxUWDPSDescription().setValue(CHILD_TXUWDPSDESCRIPTION_RESET_VALUE);
		getTxUWDownPayment().setValue(CHILD_TXUWDOWNPAYMENT_RESET_VALUE);
		getBtDeleteDownPayment().setValue(CHILD_BTDELETEDOWNPAYMENT_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_HDUWDOWNPAYMENTSOURCEID,HiddenField.class);
		registerChild(CHILD_HDUWDOWNPAYMENTCOPYID,HiddenField.class);
		registerChild(CHILD_CBUWDOWNPAYMENTSOURCE,ComboBox.class);
		registerChild(CHILD_TXUWDPSDESCRIPTION,TextField.class);
		registerChild(CHILD_TXUWDOWNPAYMENT,TextField.class);
		registerChild(CHILD_BTDELETEDOWNPAYMENT,Button.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--start//
    ////Populate EscrowType ComboBoxes manually here
    UWorksheetHandler handler =(UWorksheetHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

    //Setup EscrowType ComboBox options
    cbUWDownPaymentSourceOptions.populate(getRequestContext());

    handler.pageSaveState();
    //--Release2.1--end//

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
      validJSFromTiledView();
		}

		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_HDUWDOWNPAYMENTSOURCEID))
		{
			HiddenField child = new HiddenField(this,
				getdoUWDealDownPaymentModel(),
				CHILD_HDUWDOWNPAYMENTSOURCEID,
				doUWDealDownPaymentModel.FIELD_DFDOWNPAYMENTSOURCEID,
				CHILD_HDUWDOWNPAYMENTSOURCEID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDUWDOWNPAYMENTCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoUWDealDownPaymentModel(),
				CHILD_HDUWDOWNPAYMENTCOPYID,
				doUWDealDownPaymentModel.FIELD_DFCOPYID,
				CHILD_HDUWDOWNPAYMENTCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBUWDOWNPAYMENTSOURCE))
		{
			ComboBox child = new ComboBox( this,
				getdoUWDealDownPaymentModel(),
				CHILD_CBUWDOWNPAYMENTSOURCE,
				doUWDealDownPaymentModel.FIELD_DFDOWNPAYMENTSOURCETYPEID,
				CHILD_CBUWDOWNPAYMENTSOURCE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbUWDownPaymentSourceOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXUWDPSDESCRIPTION))
		{
			TextField child = new TextField(this,
				getdoUWDealDownPaymentModel(),
				CHILD_TXUWDPSDESCRIPTION,
				doUWDealDownPaymentModel.FIELD_DFDPSDESCRIPTION,
				CHILD_TXUWDPSDESCRIPTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXUWDOWNPAYMENT))
		{
			TextField child = new TextField(this,
				getdoUWDealDownPaymentModel(),
				CHILD_TXUWDOWNPAYMENT,
				doUWDealDownPaymentModel.FIELD_DFAMOUNT,
				CHILD_TXUWDOWNPAYMENT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETEDOWNPAYMENT))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETEDOWNPAYMENT,
				CHILD_BTDELETEDOWNPAYMENT,
				CHILD_BTDELETEDOWNPAYMENT_RESET_VALUE,
				null);
				return child;

		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdUWDownPaymentSourceId()
	{
		return (HiddenField)getChild(CHILD_HDUWDOWNPAYMENTSOURCEID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdUWDownPaymentCopyId()
	{
		return (HiddenField)getChild(CHILD_HDUWDOWNPAYMENTCOPYID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbUWDownPaymentSource()
	{
		return (ComboBox)getChild(CHILD_CBUWDOWNPAYMENTSOURCE);
	}

	/**
	 *
	 *
	 */
	static class CbUWDownPaymentSourceOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbUWDownPaymentSourceOptionList()
		{

		}


		/**
		 *
		 *
		 */
   //--Release2.1--//
   //// Populate the comboBox from the ResourceBundle.
		public void populate(RequestContext rc)
		{
      try
      {
        ////Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc
            (theSessionState.getDealInstitutionId(),"DOWNPAYMENTSOURCETYPE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }

	}



	/**
	 *
	 *
	 */
	public TextField getTxUWDPSDescription()
	{
		return (TextField)getChild(CHILD_TXUWDPSDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public TextField getTxUWDownPayment()
	{
		return (TextField)getChild(CHILD_TXUWDOWNPAYMENT);
	}


	/**
	 *
	 *
	 */
	public void handleBtDeleteDownPaymentRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
    //logger = SysLog.getSysLogger("PGUWRUWDPTV");

    UWorksheetHandler handler =(UWorksheetHandler) this.handler.cloneSS();

    //logger.debug("pgUWRUWDPTV@handleBtDeleteDownPaymentRequest::Parent: " + (pgUWorksheetViewBean) this.getParent());

    handler.preHandlerProtocol((ViewBean) this.getParent());

    int tileIndx = ((TiledViewRequestInvocationEvent)(event)).getTileNumber();
    //logger.debug("pgUWRUWDPTV@handleBtDeleteDownPaymentRequest::TileIndex: " + tileIndx);

		handler.handleDelete(false,
                         3,
                          tileIndx,
                         "RepeatedUWDownPayment/hdUWDownPaymentSourceId",
                         "downPaymentSourceId",
                         "RepeatedUWDownPayment/hdUWDownPaymentCopyId",
                         "DownPaymentSource");

		//logger.debug("pgUWRUWDPTV@handleBtDeleteDownPaymentRequest::After handleDelete)");

    handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtDeleteDownPayment()
	{
		return (Button)getChild(CHILD_BTDELETEDOWNPAYMENT);
	}


	/**
	 *
	 *
	 */
	public String endBtDeleteDownPaymentDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btDeleteDownPayment_onBeforeHtmlOutputEvent method
    UWorksheetHandler handler =(UWorksheetHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public doUWDealDownPaymentModel getdoUWDealDownPaymentModel()
	{
		if (doUWDealDownPayment == null)
			doUWDealDownPayment = (doUWDealDownPaymentModel) getModel(doUWDealDownPaymentModel.class);
		return doUWDealDownPayment;
	}


	/**
	 *
	 *
	 */
	public void setdoUWDealDownPaymentModel(doUWDealDownPaymentModel model)
	{
			doUWDealDownPayment = model;
	}


  public void validJSFromTiledView()
  {
          logger = SysLog.getSysLogger("PGUWRUWDPTV");

          //// Currently it is called from the page. Should be moved to the
          //// TiledView utility classes to generalize the parent (ViewBean name/casting).
          //// ValidationPath is temporarily hardcoded as well. Should be replaced with
          //// the picklist mechanism along with the move to the utility class.

          pgUWorksheetViewBean viewBean = (pgUWorksheetViewBean) getParentViewBean();

          try
          {
            String downpmntValidPath =  Sc.DVALID_UW_TILE_DOWNPAYMENT_PROP_FILE_NAME;

            //logger.debug("PGUWRUWDPTV@validJSFromTiledView::ValidPath: " + downpmntValidPath);

            JSValidationRules downpmntValidObj = new JSValidationRules(downpmntValidPath, logger, viewBean);
            downpmntValidObj.attachJSValidationRules();
          }
          catch( Exception e )
          {
              logger.warning("DataValidJSException: " + e);
          }
  }


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_HDUWDOWNPAYMENTSOURCEID="hdUWDownPaymentSourceId";
	public static final String CHILD_HDUWDOWNPAYMENTSOURCEID_RESET_VALUE="";
	public static final String CHILD_HDUWDOWNPAYMENTCOPYID="hdUWDownPaymentCopyId";
	public static final String CHILD_HDUWDOWNPAYMENTCOPYID_RESET_VALUE="";
	public static final String CHILD_CBUWDOWNPAYMENTSOURCE="cbUWDownPaymentSource";
	public static final String CHILD_CBUWDOWNPAYMENTSOURCE_RESET_VALUE="";

  //--Release2.1--//
	////private static CbUWDownPaymentSourceOptionList cbUWDownPaymentSourceOptions=new CbUWDownPaymentSourceOptionList();
	private CbUWDownPaymentSourceOptionList cbUWDownPaymentSourceOptions=new CbUWDownPaymentSourceOptionList();

	public static final String CHILD_TXUWDPSDESCRIPTION="txUWDPSDescription";
	public static final String CHILD_TXUWDPSDESCRIPTION_RESET_VALUE="";
	public static final String CHILD_TXUWDOWNPAYMENT="txUWDownPayment";
	public static final String CHILD_TXUWDOWNPAYMENT_RESET_VALUE="";
	public static final String CHILD_BTDELETEDOWNPAYMENT="btDeleteDownPayment";
	public static final String CHILD_BTDELETEDOWNPAYMENT_RESET_VALUE=" ";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doUWDealDownPaymentModel doUWDealDownPayment=null;
  public SysLogger logger;
	private UWorksheetHandler handler=new UWorksheetHandler();

}

