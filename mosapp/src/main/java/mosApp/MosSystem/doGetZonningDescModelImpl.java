package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doGetZonningDescModelImpl extends QueryModelBase
	implements doGetZonningDescModel
{
	/**
	 *
	 *
	 */
	public doGetZonningDescModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 19Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
	int institutionId = theSessionState.getDealInstitutionId();
    while(this.next())
    {
      //Convert Zoning Desc
      this.setDfZoningDesc(BXResources.getPickListDescription(
          institutionId, "PROPERTYZONING", this.getDfZoningId().intValue(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfZoningId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFZONINGID);
	}


	/**
	 *
	 *
	 */
	public void setDfZoningId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFZONINGID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfZoningDesc()
	{
		return (String)getValue(FIELD_DFZONINGDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfZoningDesc(String value)
	{
		setValue(FIELD_DFZONINGDESC,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";

	public static final String SELECT_SQL_TEMPLATE="SELECT ALL PROPERTYZONING.PROPERTYZONINGID, PROPERTYZONING.PROPERTYZONINGDESCRIPTION FROM PROPERTYZONING  __WHERE__  ";
	
	public static final String MODIFYING_QUERY_TABLE_NAME="PROPERTYZONING";
	
	public static final String STATIC_WHERE_CRITERIA="";
	
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFZONINGID="PROPERTYZONING.PROPERTYZONINGID";
	public static final String COLUMN_DFZONINGID="PROPERTYZONINGID";
	public static final String QUALIFIED_COLUMN_DFZONINGDESC="PROPERTYZONING.PROPERTYZONINGDESCRIPTION";
	public static final String COLUMN_DFZONINGDESC="PROPERTYZONINGDESCRIPTION";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFZONINGID,
				COLUMN_DFZONINGID,
				QUALIFIED_COLUMN_DFZONINGID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFZONINGDESC,
				COLUMN_DFZONINGDESC,
				QUALIFIED_COLUMN_DFZONINGDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

