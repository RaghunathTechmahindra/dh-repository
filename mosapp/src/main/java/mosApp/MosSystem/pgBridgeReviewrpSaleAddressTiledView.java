package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgBridgeReviewrpSaleAddressTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgBridgeReviewrpSaleAddressTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doBridgeSalePropertyDetailsModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStPropertyAddressLine1().setValue(CHILD_STPROPERTYADDRESSLINE1_RESET_VALUE);
		getStPropertyAddressLine2().setValue(CHILD_STPROPERTYADDRESSLINE2_RESET_VALUE);
		getStPropertyCity().setValue(CHILD_STPROPERTYCITY_RESET_VALUE);
		getStPropertyProvince().setValue(CHILD_STPROPERTYPROVINCE_RESET_VALUE);
		getStPropertyPostalCodeLDU().setValue(CHILD_STPROPERTYPOSTALCODELDU_RESET_VALUE);
		getStPropertyPostalCodeFSA().setValue(CHILD_STPROPERTYPOSTALCODEFSA_RESET_VALUE);
		getHdRowNum().setValue(CHILD_HDROWNUM_RESET_VALUE);
		getHdPropertyId().setValue(CHILD_HDPROPERTYID_RESET_VALUE);
		getHdPropertyCopyId().setValue(CHILD_HDPROPERTYCOPYID_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STPROPERTYADDRESSLINE1,StaticTextField.class);
		registerChild(CHILD_STPROPERTYADDRESSLINE2,StaticTextField.class);
		registerChild(CHILD_STPROPERTYCITY,StaticTextField.class);
		registerChild(CHILD_STPROPERTYPROVINCE,StaticTextField.class);
		registerChild(CHILD_STPROPERTYPOSTALCODELDU,StaticTextField.class);
		registerChild(CHILD_STPROPERTYPOSTALCODEFSA,StaticTextField.class);
		registerChild(CHILD_HDROWNUM,HiddenField.class);
		registerChild(CHILD_HDPROPERTYID,HiddenField.class);
		registerChild(CHILD_HDPROPERTYCOPYID,HiddenField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoBridgeSalePropertyDetailsModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}

		return movedToRow;


		// The following code block was migrated from the rpSaleAddress_onBeforeRowDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		if (event.getRowIndex() > 0) return PROCEED;

		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		int retval = handler.getRowDisplaySetting("doBridgeSalePropertyDetails");

		handler.pageSaveState();

		return retval;
		*/
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STPROPERTYADDRESSLINE1))
		{
			StaticTextField child = new StaticTextField(this,
				getdoBridgeSalePropertyDetailsModel(),
				CHILD_STPROPERTYADDRESSLINE1,
				doBridgeSalePropertyDetailsModel.FIELD_DFADDRESSLINE1,
				CHILD_STPROPERTYADDRESSLINE1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYADDRESSLINE2))
		{
			StaticTextField child = new StaticTextField(this,
				getdoBridgeSalePropertyDetailsModel(),
				CHILD_STPROPERTYADDRESSLINE2,
				doBridgeSalePropertyDetailsModel.FIELD_DFADDRESSLINE2,
				CHILD_STPROPERTYADDRESSLINE2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYCITY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoBridgeSalePropertyDetailsModel(),
				CHILD_STPROPERTYCITY,
				doBridgeSalePropertyDetailsModel.FIELD_DFCITY,
				CHILD_STPROPERTYCITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYPROVINCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoBridgeSalePropertyDetailsModel(),
				CHILD_STPROPERTYPROVINCE,
				doBridgeSalePropertyDetailsModel.FIELD_DFPROVINCENAME,
				CHILD_STPROPERTYPROVINCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYPOSTALCODELDU))
		{
			StaticTextField child = new StaticTextField(this,
				getdoBridgeSalePropertyDetailsModel(),
				CHILD_STPROPERTYPOSTALCODELDU,
				doBridgeSalePropertyDetailsModel.FIELD_DFPOSTALCODELDU,
				CHILD_STPROPERTYPOSTALCODELDU_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYPOSTALCODEFSA))
		{
			StaticTextField child = new StaticTextField(this,
				getdoBridgeSalePropertyDetailsModel(),
				CHILD_STPROPERTYPOSTALCODEFSA,
				doBridgeSalePropertyDetailsModel.FIELD_DFPOSTALCODEFSA,
				CHILD_STPROPERTYPOSTALCODEFSA_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDROWNUM))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_HDROWNUM,
				CHILD_HDROWNUM,
				CHILD_HDROWNUM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDPROPERTYID))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_HDPROPERTYID,
				CHILD_HDPROPERTYID,
				CHILD_HDPROPERTYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDPROPERTYCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_HDPROPERTYCOPYID,
				CHILD_HDPROPERTYCOPYID,
				CHILD_HDPROPERTYCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyAddressLine1()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYADDRESSLINE1);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyAddressLine2()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYADDRESSLINE2);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyCity()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYCITY);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyProvince()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYPROVINCE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyPostalCodeLDU()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYPOSTALCODELDU);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyPostalCodeFSA()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYPOSTALCODEFSA);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdRowNum()
	{
		return (HiddenField)getChild(CHILD_HDROWNUM);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdPropertyId()
	{
		return (HiddenField)getChild(CHILD_HDPROPERTYID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdPropertyCopyId()
	{
		return (HiddenField)getChild(CHILD_HDPROPERTYCOPYID);
	}


	/**
	 *
	 *
	 */
	public doBridgeSalePropertyDetailsModel getdoBridgeSalePropertyDetailsModel()
	{
		if (doBridgeSalePropertyDetails == null)
			doBridgeSalePropertyDetails = (doBridgeSalePropertyDetailsModel) getModel(doBridgeSalePropertyDetailsModel.class);
		return doBridgeSalePropertyDetails;
	}


	/**
	 *
	 *
	 */
	public void setdoBridgeSalePropertyDetailsModel(doBridgeSalePropertyDetailsModel model)
	{
			doBridgeSalePropertyDetails = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STPROPERTYADDRESSLINE1="stPropertyAddressLine1";
	public static final String CHILD_STPROPERTYADDRESSLINE1_RESET_VALUE="";
	public static final String CHILD_STPROPERTYADDRESSLINE2="stPropertyAddressLine2";
	public static final String CHILD_STPROPERTYADDRESSLINE2_RESET_VALUE="";
	public static final String CHILD_STPROPERTYCITY="stPropertyCity";
	public static final String CHILD_STPROPERTYCITY_RESET_VALUE="";
	public static final String CHILD_STPROPERTYPROVINCE="stPropertyProvince";
	public static final String CHILD_STPROPERTYPROVINCE_RESET_VALUE="";
	public static final String CHILD_STPROPERTYPOSTALCODELDU="stPropertyPostalCodeLDU";
	public static final String CHILD_STPROPERTYPOSTALCODELDU_RESET_VALUE="";
	public static final String CHILD_STPROPERTYPOSTALCODEFSA="stPropertyPostalCodeFSA";
	public static final String CHILD_STPROPERTYPOSTALCODEFSA_RESET_VALUE="";
	public static final String CHILD_HDROWNUM="hdRowNum";
	public static final String CHILD_HDROWNUM_RESET_VALUE="";
	public static final String CHILD_HDPROPERTYID="hdPropertyId";
	public static final String CHILD_HDPROPERTYID_RESET_VALUE="";
	public static final String CHILD_HDPROPERTYCOPYID="hdPropertyCopyId";
	public static final String CHILD_HDPROPERTYCOPYID_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doBridgeSalePropertyDetailsModel doBridgeSalePropertyDetails=null;

}

