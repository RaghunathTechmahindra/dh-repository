/*
 * @(#)doOutsourcedClosingServiceModelImpl.java    2006-1-19
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package mosApp.MosSystem;

import java.math.BigDecimal;
import java.sql.Timestamp;

import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.basis100.log.*;

import com.basis100.picklist.BXResources;

/**
 * doOutsourcedClosingServiceModelImpl - is the implementation for
 * doOutsourcedClosingServiceModel.
 *
 * @version   1.0 2006-1-19
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class doOutsourcedClosingServiceModelImpl extends QueryModelBase
    implements doOutsourcedClosingServiceModel {

    // The logger
    private static Log _log =
        LogFactory.getLog(doOutsourcedClosingServiceModelImpl.class);

    // static definitions:
    public static final String DATA_SOURCE_NAME = "jdbc/orcl";
    // the select SQL template.
    public static final String SELECT_SQL_TEMPLATE =
        "SELECT ALL " +
        "SERVICEREQUEST.SERVICEPROVIDERREFNO, " +
        "SERVICEREQUEST.REQUESTID, " +
        "SERVICEREQUEST.PROPERTYID, " +        
        "SERVICEPRODUCT.SERVICEPROVIDERID, " +
        "to_char(REQUEST.REQUESTSTATUSID) STATUSID_STR, " +
        "REQUEST.REQUESTSTATUSID, " +
        "REQUEST.SERVICEPRODUCTID, " +
        "REQUEST.STATUSMESSAGE, " +
        "REQUEST.STATUSDATE, " +
        "REQUEST.REQUESTID, " +
        "REQUEST.DEALID, " +
        "REQUEST.COPYID " +
        "FROM " +
        "SERVICEREQUEST, REQUEST, SERVICEPRODUCT " +
        "__WHERE__ ORDER BY REQUEST.STATUSDATE DESC";
    // the query table name.
    public static final String MODIFYING_QUERY_TABLE_NAME =
        "SERVICEREQUEST, REQUEST, SERVICEPRODUCT";
    // the static query criteria.
    public static final String STATIC_WHERE_CRITERIA =
        "(REQUEST.INSTITUTIONPROFILEID = SERVICEREQUEST.INSTITUTIONPROFILEID) AND " +
        "(REQUEST.INSTITUTIONPROFILEID = SERVICEPRODUCT.INSTITUTIONPROFILEID) AND " +
        "(SERVICEREQUEST.REQUESTID = REQUEST.REQUESTID) AND " +
        "(REQUEST.SERVICEPRODUCTID = SERVICEPRODUCT.SERVICEPRODUCTID) AND " + 
        "(SERVICEPRODUCT.SERVICETYPEID = 3) AND " + 
        "(SERVICEPRODUCT.SERVICESUBTYPEID = 6) ";

    // the query field schema.
    public static final QueryFieldSchema FIELD_SCHEMA =
        new QueryFieldSchema();
   

    // column definition for each field.
    public static final String QUALIFIED_COLUMN_DFOCSDEALID =
        "REQUEST.DEALID";
    public static final String COLUMN_DFOCSDEALID =
        "DEALID";

    public static final String QUALIFIED_COLUMN_DFOCSCOPYID =
        "REQUEST.COPYID";
    public static final String COLUMN_DFOCSCOPYID =
        "COPYID";
 
    public static final String QUALIFIED_COLUMN_DFOCSREQUESTID =
        "REQUEST.REQUESTID";
    public static final String COLUMN_DFOCSREQUESTID =
        "REQUESTID";
    
    public static final String QUALIFIED_COLUMN_DFOCSPROVIDERID =
        "SERVICEPRODUCT.SERVICEPROVIDERID";
    public static final String COLUMN_DFOCSPROVIDERID =
        "SERVICEPROVIDERID";

    public static final String QUALIFIED_COLUMN_DFOCSPRODUCTID =
        "REQUEST.SERVICEPRODUCTID";
    public static final String COLUMN_DFOCSPRODUCTID =
        "SERVICEPRODUCTID";
    
    public static final String QUALIFIED_COLUMN_DFOCSREQUESTREFNUM =
        "SERVICEREQUEST.SERVICEPROVIDERREFNO";
    public static final String COLUMN_DFOCSREQUESTREFNUM =
        "SERVICEPROVIDERREFNO";
    
    public static final String QUALIFIED_COLUMN_DFOCSREQUESTSTATUS =
        "REQUEST.STATUSID_STR";
    public static final String COLUMN_DFOCSREQUESTSTATUS =
        "STATUSID_STR";

    public static final String QUALIFIED_COLUMN_DFOCSREQUESTSTATUSID =
        "REQUEST.REQUESTSTATUSID";
    public static final String COLUMN_DFOCSREQUESTSTATUSID =
        "REQUESTSTATUSID";

  
    public static final String QUALIFIED_COLUMN_DFOCSREQUESTSTATUSMSG =
        "REQUEST.STATUSMESSAGE";
    public static final String COLUMN_DFOCSREQUESTSTATUSMSG =
        "STATUSMESSAGE";

    public static final String QUALIFIED_COLUMN_DFOCSREQUESTSTATUSDATE =
        "REQUEST.STATUSDATE";
    public static final String COLUMN_DFOCSREQUESTSTATUSDATE =
        "STATUSDATE";

    // build the relationship between the column definition and the bound field
    // name, which are defined in the interface.
    static {
         FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFOCSDEALID,
                    COLUMN_DFOCSDEALID,
                    QUALIFIED_COLUMN_DFOCSDEALID,
                    BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFOCSCOPYID,
                    COLUMN_DFOCSCOPYID,
                    QUALIFIED_COLUMN_DFOCSCOPYID,
                    BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
        
       FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFOCSPROVIDERID,
                    COLUMN_DFOCSPROVIDERID,
                    QUALIFIED_COLUMN_DFOCSPROVIDERID,
                    BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFOCSPRODUCTID,
                COLUMN_DFOCSPRODUCTID,
                QUALIFIED_COLUMN_DFOCSPRODUCTID,
                BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""
            )
        );
        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFOCSREQUESTREFNUM,
                COLUMN_DFOCSREQUESTREFNUM,
                QUALIFIED_COLUMN_DFOCSREQUESTREFNUM,
                String.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""
            )
        );
        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFOCSREQUESTSTATUS,
                COLUMN_DFOCSREQUESTSTATUS,
                QUALIFIED_COLUMN_DFOCSREQUESTSTATUS,
                String.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""
            )
        );

		FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFOCSREQUESTSTATUSID,
                COLUMN_DFOCSREQUESTSTATUSID,
                QUALIFIED_COLUMN_DFOCSREQUESTSTATUSID,
                BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""
            )
        );
        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFOCSREQUESTSTATUSMSG,
                COLUMN_DFOCSREQUESTSTATUSMSG,
                QUALIFIED_COLUMN_DFOCSREQUESTSTATUSMSG,
                String.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""
            )
        );
        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFOCSREQUESTSTATUSDATE,
                COLUMN_DFOCSREQUESTSTATUSDATE,
                QUALIFIED_COLUMN_DFOCSREQUESTSTATUSDATE,
                Timestamp.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""
            )
        );
        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFOCSREQUESTID,
                    COLUMN_DFOCSREQUESTID,
                    QUALIFIED_COLUMN_DFOCSREQUESTID,
                    BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
    }

    /**
     * Constructor function
     */
    public doOutsourcedClosingServiceModelImpl() {

        super();
        setDataSourceName(DATA_SOURCE_NAME);
        setSelectSQLTemplate(SELECT_SQL_TEMPLATE);
        setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);
        setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);
        setFieldSchema(FIELD_SCHEMA);
        initialize();
    }

    /**
     * initializing.  do nothing for now.
     */
    public void initialize() {}

    /**
     * before execute the SQL.
     */
    protected String beforeExecute(ModelExecutionContext context,
                                   int queryType, String sql)
        throws ModelControlException {

        SysLog.trace(this.getClass(), "SQL ********** before Execute: " + sql);
        
        if (_log.isDebugEnabled())
            _log.debug("SQL before Execute: " + sql);
        return sql;
    }

    /**
     * after execute, we need change some language sensitive value for the
     * result.
     */
    protected void afterExecute(ModelExecutionContext context,
                                int queryType)
        throws ModelControlException {

        // try to get language id from the session state model.
        String defaultInstanceStateName =
            getRequestContext().getModelManager().
            getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl sessionState = (SessionStateModelImpl)
            getRequestContext().getModelManager().
            getModel(SessionStateModel.class,
                     defaultInstanceStateName, true);
        int languageId = sessionState.getLanguageId();
		int institutionId = sessionState.getDealInstitutionId();

        // get the status from the BXResources and set to the field.
        _log.debug("Trying to get the status from BXResources");
        
        if(getSize()>0) {
            setDfOCSRequestStatus(
                    BXResources.
                        getPickListDescription(institutionId, "REQUESTSTATUS",
                                getDfOCSRequestStatus(),
                                    languageId));
        }

        _log.info("OCS model size " + getSize());        

    }
    
    /**
     * getter and setter implementation.
     */
    
    public BigDecimal getDfOCSProviderId() {
        return (BigDecimal) getValue(FIELD_DFOCSPROVIDERID);
    }
    
    public void setDfOCSProviderId(BigDecimal id) {
        setValue(FIELD_DFOCSPROVIDERID, id);
    }

    public BigDecimal getDfOCSProductId() {
        return (BigDecimal) getValue(FIELD_DFOCSPRODUCTID);
    }
    
    public void setDfOCSProductId(BigDecimal id) {
        setValue(FIELD_DFOCSPRODUCTID, id);
    }

    public String getDfOCSRequestRefNum() {
        return (String) getValue(FIELD_DFOCSREQUESTREFNUM);
    }
    
    public void setDfOCSRequestRefNum(String num) {
        setValue(FIELD_DFOCSREQUESTREFNUM, num);
    }
     
    public String getDfOCSRequestStatus() {
        return (String) getValue(FIELD_DFOCSREQUESTSTATUS);
    }
    
    public void setDfOCSRequestStatus(String status) {
        setValue(FIELD_DFOCSREQUESTSTATUS, status);
    }

    public BigDecimal getDfOCSRequestStatusId() {
        return (BigDecimal) getValue(FIELD_DFOCSREQUESTSTATUSID);
    }
    
    public void setDfOCSRequestStatusId (BigDecimal dfOCSRequestStatusId) {
        setValue(FIELD_DFOCSREQUESTSTATUSID, dfOCSRequestStatusId);
    }

    public String getDfOCSRequestStatusMsg() {
        return (String) getValue(FIELD_DFOCSREQUESTSTATUSMSG);
    }
    
    public void setDfOCSRequestStatusMsg(String msg) {
        setValue(FIELD_DFOCSREQUESTSTATUSMSG, msg);
    }

    public Timestamp getDfOCSRequestStatusDate() {
        return (Timestamp) getValue(FIELD_DFOCSREQUESTSTATUSDATE);
    }
    
    public void setDfOCSRequestStatusDate(Timestamp date) {
        setValue(FIELD_DFOCSREQUESTSTATUSDATE, date);
    }
    
    public BigDecimal getDfOCSRequestId() {
        return (BigDecimal) getValue(FIELD_DFOCSREQUESTID);
    }
    
    public void setDfOCSRequestId(BigDecimal id) {
        setValue(FIELD_DFOCSREQUESTID, id);
    }

    public BigDecimal getDfOCSCopyId() {
        return (BigDecimal) getValue(FIELD_DFOCSCOPYID);
    }
    
    public void setDfOCSCopyId(BigDecimal id) {
        setValue(FIELD_DFOCSCOPYID, id);
    }

}
