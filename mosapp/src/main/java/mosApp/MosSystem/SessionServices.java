package mosApp.MosSystem;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpSession;

import MosSystem.Sc;

import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.security.DLM;
import com.basis100.deal.security.DealEditControl;
import com.basis100.deal.security.PLM;
import com.basis100.deal.util.StringUtil;
import com.iplanet.jato.ModelManager;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestManager;

import org.terracotta.modules.concurrent.collections.ConcurrentStringMap;

/**
 *
 *
 */
public class SessionServices extends Object
{
    static boolean ualogEnable = "Y".equals(PropertiesCache.getInstance().getInstanceProperty
            ("com.filogix.express.userActivityLog", "N"));
	SessionServices instance=new SessionServices();
	static boolean initDone=false;
	static SysLogger logger=null;
	static final String DEFAULT_INSTANCE_STATE_NAME = "mosApp.MosSystem.SessionStateModel";
	// global session object names ...

	private static final String ALL_USERS="ALL_USERS_GSO";
	private static final String SAVED_STATE_POOL="SAVED_STATE_POOL_GSO";
	// Currently keep lists local to signleton since going to PE not strictly necessary ...
	// active users by session ids (i.e. key is session id)

	private static Map allUsersBySessionId=new ConcurrentStringMap();
	// active sessions (ids) by login ids (i.e.key is login id)

	protected static Map allSessionIdsByLoginId=new ConcurrentStringMap();
	// saved (previously used) uese session objects by login ids

	private static Map savedSessionObjects=new ConcurrentStringMap();
	// previously used sessions - expried sessions that were used for interface activity (vs. simply
	// timed out without proceeding beyong signon page)

	private static Map previouslyUsedBySessionId=new ConcurrentStringMap();
	// invalid active sessions - list of session (ids) that were invalided via a subsequent login (i.e.
	// forced login for the user id when active session detected).

	private static Map invalidalidatedSessions=new ConcurrentStringMap();

        //--CervusPhaseII--start--//
        protected static Map allHttpSessionsByLoginId=new ConcurrentStringMap();
        // saved (previously used) uese session objects by login ids
        //--CervusPhaseII--end--//

	// Constructor: private - class inmplements singleton

	private SessionServices()
	{
	}


	/**
	 *
	 *
	 */
	public static void init(SysLogger aLogger)
	{
		initDone = true;

		logger = aLogger;

	}


	/**
	 *
	 *
	 */
	public static boolean isActiveSession(String loginId)
	{
		Object o = allSessionIdsByLoginId.get(loginId.toUpperCase());

		if (o != null) return true;

		return false;

	}


	/**
	 *
	 *
	 */
	public static void trackNewSession(HttpSession session)
	{
		if (! initDone) return;

		//String sessionId = session.getId();
		//Changed for Terracotta to get native session id
		String sessionId = getNativeSessionId(session);

		// check for session already being tracked
		//logger.info("SessionServices.trackNewSession: Session (ND) Id = " + sessionId);

		String loginId =(String) allUsersBySessionId.get(sessionId);

		if (loginId != null && ! loginId.equals(""))
		{
			// we were already tracking this session - if login Id is not empty automatically
			// remove it from active list
			logger.info("SessionServices.trackNewSession: Session (ND) Id = " + sessionId + ", Associated with Login Id = " + loginId + ": removed as active user");
			removeActiveSession(session, false);

			// not preemptive since we do not
		}

		// this is initial session tracking - i.e. session not yet associated with a login Id
		allUsersBySessionId.put(sessionId, "");

	}


	/**
	 *
	 *
	 */
	public static void addActiveSessionLoginId(String loginId, HttpSession session, SessionResourceKit srk)
		throws Exception
	{
		if (! initDone) return;


		// check to see if new session invalidating an existing active session
		String existingSessionId =(String) allSessionIdsByLoginId.get(loginId);

		if (existingSessionId != null)
		{
			// mark session invalidated and clean up as much as possible
			invalidateSession(existingSessionId, loginId, srk);
		}

		loginId = loginId.toUpperCase();


		// add login Id to sessions being tracked
		//String sessionId = session.getId();
		//Changed for Terracotta to get native session id
		String sessionId = getNativeSessionId(session);

		logger.info("SessionServices.addActiveSessionLoginId: Login Id = " + loginId + ", Session (ND) Id = " + sessionId);

		allUsersBySessionId.put(sessionId, loginId);


		// add to users being tracked
		allSessionIdsByLoginId.put(loginId, sessionId);

               // test this
               //--CervusPhaseII--start--//
               allHttpSessionsByLoginId.put(loginId, session);
               //--CervusPhaseII--end--//
	}


	/**
	 *
	 *
	 */
	private static void invalidateSession(String sessionId, String loginId, SessionResourceKit srk)
		throws Exception
	{

		// add session to list of invalidated sessions
		invalidalidatedSessions.put(sessionId, loginId);

		JdbcExecutor jExec = srk.getJdbcExecutor();
		int key = jExec.execute("Select USERPROFILEID, INSTITUTIONPROFILEID from USERPROFILE where USERLOGIN = '" + loginId + "'");
		while(jExec.next(key))
		{
			int userProfileId = jExec.getInt(key, 1);
			int institutionProfileID = jExec.getInt(key, 2);
			srk.getExpressState().setDealInstitutionId(institutionProfileID);
			clearDealLock(srk, userProfileId, loginId);
		}
		jExec.closeData(key);
		srk.getExpressState().cleanAllIds();
	}


	/**
	 *
	 *
	 */
	public static boolean isSessionInvalidated(HttpSession session)
	{
		//CSpSessionId sId = session.getSessionId();

		//String sessionId = session.getId();
		//Changed for Terracotta to get native session id
		String sessionId = getNativeSessionId(session);

		return isSessionInvalidated(sessionId);

	}


	/**
	 *
	 *
	 */
	public static boolean isSessionInvalidated(String sessionId)
	{
		if (sessionId == null) return false;

		String chk =(String) invalidalidatedSessions.get(sessionId);

		if (chk != null) return true;

		return false;

	}


	/**
	 *
	 *
	 */
	public static boolean retrieveSavedSessionObjects(String loginId, HttpSession session)
	{
		if (! initDone) return false;

		loginId = loginId.toUpperCase();

		// retrieve last used session objects for login Id
		SavedObjects SO =(SavedObjects) savedSessionObjects.get(loginId);

		if (SO != null && SO.notEmpty())
		{
			logger.debug("SessionServices.retrieveSavedSessionObjects: Login Id  = " + loginId + ", saved session objects retrieved");
			session.setAttribute(Sc.US_SAVED_PAGES, SO.getSavedPages());

      //--> Here some testing implementation need to confirm
      //--> By BILLY 11July2002
      //--> Transfer the SessionState Model to the current context, means make it pointing to the
      //    new HTTPSession
      //--> This is not necessary as it may cause Memory leak
      //--> Modified by Billy 11April2003
      //SO.getTheSessionState().setRequestContext(RequestManager.getRequestContext());
      //=============================================================================
      //Set it to the HTTPSession and use it instead down the road
      //--> Discussion :: May consider to use Sc.US_THE_SESSION as name instead
      //--> Not necessary to do this -- By Billy 22April2003
      /*
      String defaultInstanceStateName =
			       RequestManager.getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
			session.setAttribute(defaultInstanceStateName, SO.getTheSessionState());
      */
			return true;
		}

		return false;
	}


	/**
	 *
	 *
	 */
	public static boolean removeActiveSession(HttpSession session, boolean timeout)
	{
		if (! initDone) return false;

		logger.debug("SessionServices.removeActiveSession: session = " + session + ", timeout = " + timeout);

		if (session == null) return false;

		//CSpSessionId sId = session.getSessionId();

		//String sessionId = session.getId();
		//Changed for Terracotta to get native session id
		String sessionId = getNativeSessionId(session);

		return removeActiveSession(session, sessionId, timeout);

	}

	/**
	 *
	 *
	 */
	public static boolean removeActiveSession(String sessionId, boolean timeout)
	{
		if (! initDone) return false;

		logger.debug("SessionServices.removeActiveSession: sessionId = " + sessionId + ", timeout = " + timeout);

		if (sessionId == null) return false;

		return removeActiveSession(null, sessionId, timeout);

	}

	/**
	 *
	 * remove from sessions being tracked
	 */
	public static boolean removeActiveSession(HttpSession session, String sessionId, boolean timeout)
	{

		try {
			// some activities avoided for invalidated session
			boolean isInvalidatedSession = false;

			if (sessionId != null) isInvalidatedSession = isSessionInvalidated(sessionId);
			else isInvalidatedSession = isSessionInvalidated(session);

			String existingLoginId = null;

			existingLoginId =(sessionId == null) ? null :(String) allUsersBySessionId.get(sessionId);

			boolean haveLoginId =(existingLoginId != null) &&(! existingLoginId.equals(""));

			logger.info("SessionServices.removeActiveSession: Session Id = " + sessionId + ", Login Id = " +((haveLoginId) ? existingLoginId : "{None}") + ", Time out = " +((timeout) ? "Yes" : "No") + ", Invalidated session = " +((isInvalidatedSession) ? "Yes" : "No"));
			logger.debug("SessionServices.removeActiveSession: Session:  = " + session);

			if (haveLoginId &&(session != null))
			{

			    // moved up retrieving defaultInstanceStateName here since it's used 2 sections and the code was duplicated since 4.3GR 
			    //--> Discussion :: May consider to use Sc.US_THE_SESSION as name instead
			    String defaultInstanceStateName;
			    RequestContext rc = RequestManager.getRequestContext();
			    if(null != rc) { //RequestManager available.
			        ModelManager mm = rc.getModelManager();
			        defaultInstanceStateName = mm.getDefaultModelInstanceName(SessionStateModel.class);
			    } else { //RequestManager not available -- Use hard-coded name.
			        defaultInstanceStateName = DEFAULT_INSTANCE_STATE_NAME;
			    }
				
			    SessionStateModelImpl theSessionState 
			        = (SessionStateModelImpl) session.getAttribute(defaultInstanceStateName);

			    // ensure that the user session is cleaned up
			    SessionResourceKit srk = new SessionResourceKit("System");
			    try {					
                                if (ualogEnable) {
                                    //--STRESSTEST--start--// 
                                    String msg = "Session Id = " + sessionId + ", Login Id = " +((haveLoginId) ? existingLoginId : "{None}") + ", Time out = " +((timeout) ? "Yes" : "No") + ", Invalidated session = " +((isInvalidatedSession) ? "Yes" : "No"); 
                
                                    int id = - 1; 
                                    if (theSessionState != null) id = theSessionState.getSessionUserId(); 
                                    // Added to write Logout info from UserProfile to UserActivity log file 
                                    // Get UserProfile 
                                    UserProfile up = new UserProfile(srk); 
                                    if(id > 0) 
                                        up.findByPrimaryKey(new UserProfileBeanPK(id, theSessionState.getUserInstitutionId())); 
                                    else 
                                        up = null; 
                                        
                                    UserActivityLogger.logUser(up, msg, "SOUT"); 
                                    //--STRESSTEST--end--// 
        
            	                }
                                srk.beginTransaction();
                	cleanUserSession(sessionId, theSessionState,((haveLoginId) ? existingLoginId : "{None}"), srk, timeout);
    				srk.commitTransaction();
			    } catch(Exception e) {
				srk.cleanTransaction();
				logger.error("SessionServices.removeActiveSession:: Failure cleaning up existing session - ignored");
				logger.error(e);
			    }finally{
				srk.freeResources();
			    }

			    if (isInvalidatedSession == false) {
				// save existing session objects used with existing login Id
			        // deleted creating SavedOjects since it's never used after
			        logger.info("End Session: Login Id = " + existingLoginId + ", Saving session objects");
			        // deleted store savedSessionObject since it's memory leak and never used after - 4.3GR
			        previouslyUsedBySessionId.put(sessionId, new java.util.Date());
			        //session.remove(Sc.US_SAVED_PAGES);
			        session.removeAttribute(Sc.US_SAVED_PAGES);
			        //session.remove(Sc.US_THE_SESSION);
			        //--> Discussion :: May consider to use Sc.US_THE_SESSION as name instead
			        // deleted getting defaultInstanceStateName since it's already there above - 4.3GR

			        session.removeAttribute(defaultInstanceStateName);
			    }
			}
			
			// remove from sessions being tracked
			if (sessionId != null)
			{
				allUsersBySessionId.remove(sessionId);
				logger.info("Session tracking - session dropped, id = " + sessionId);
			}


			// remove from login Ids being tracked
			if (haveLoginId == true && isInvalidatedSession == false)
			{
				allSessionIdsByLoginId.remove(existingLoginId);
				logger.info("Session tracking - login Id dropped, id = " + existingLoginId);
			}
			
	        //Added to fix the memory leak
	        if (haveLoginId == true)
	        {
	            allHttpSessionsByLoginId.remove(existingLoginId);
	        }
	        //Added to fix the memory leak end 
	        
		} catch (Exception e) {
			// do nothing to avoid application error.
			logger.error(StringUtil.stack2string(e));
		}

		return true;

	}

	/**
	 *
	 *
	 */
	public static boolean previouslyUsedSession(String sessionId)
	{
		java.util.Date usedDateTime =(java.util.Date) previouslyUsedBySessionId.get(sessionId);

		if (usedDateTime == null) return false;

		previouslyUsedBySessionId.remove(sessionId);

		return true;

	}


	/**
	 *
	 *
	 */
	protected static void cleanUserSession(String sessionId, 
			SessionStateModelImpl theSessionState, String loginId, 
			SessionResourceKit srk, boolean timeout)
	throws Exception
	{

		// free deal lock if any one in place by user - this is not done for invalidated sessions since
		// a lock is removed at the time session was invalidated
		String invalidated =(String)(invalidalidatedSessions.get(sessionId));

		if (invalidated != null)
		{
			// abandoned session - simply removed from invalidated list (no need to track further)
			invalidalidatedSessions.remove(sessionId);
		}
		else
		{
			if (theSessionState == null) return;
			// free lock - if one

			for(int institutionProfileId : theSessionState.institutionList()){

				int userProfileId = theSessionState.getUserProfileId(institutionProfileId);
				srk.getExpressState().setDealInstitutionId(institutionProfileId);

				if(timeout == true){
					clearDealLockBySessionID(srk, sessionId);
					clearPageLockBySessionID(srk, sessionId);
				}
				else{
					clearDealLock(srk, userProfileId, loginId);
					clearPageLock(srk, userProfileId, loginId);
				}

				// Added to write Logout info to UserProfile -- By BILLY 19April2002
				if (userProfileId > 0 || !loginId.equals(""))
				{
					// Get UserProfile
					UserProfile up = new UserProfile(srk);
					up.findByPrimaryKey(
							new UserProfileBeanPK(
									userProfileId, institutionProfileId));
					// Set Logon Flag to N
					up.setLoginFlag("N");
					// Set Last Logout time to Current time
					up.setLastLogoutTime(new Date());
					up.ejbStore();
				}
			}
		}

		if (theSessionState == null) return;
		int dealInstitutionId = theSessionState.getDealInstitutionId();
		if (dealInstitutionId < 0 ) return;

		srk.getExpressState().setDealInstitutionId(dealInstitutionId);

		// free any transactional copies
		DealEditControl dec = theSessionState.getDec();

		if (dec != null)
		{
			try
			{
				dec.dropToTxLevel(0, srk, null);
			}
			catch(Exception e)
			{
				logger.error("End Session:cleanUserSession:: Failure removing deal transactional copies --> Session Id = " + sessionId + ", Login Id = " + loginId);
				logger.error(e);
			}
		}

		theSessionState.setDec(null);


		// ensure no message objects/special objects
		theSessionState.setActMessage(null);

		theSessionState.setPasMessage(null);

		theSessionState.setJmpState(null);

	}


	/**
	 *
	 *
	 */
	private static void clearDealLock(SessionResourceKit srk, int userProfileId, String loginId)
		throws Exception
	{
		logger.debug("SessionServices.clearDealLock: loginId  = " + loginId);
		try
		{
			if (userProfileId == - 1)
			{
				JdbcExecutor jExec = srk.getJdbcExecutor();
				int key = jExec.execute("Select USERPROFILEID from USERPROFILE where USERLOGIN = '" + loginId + "'");
				while(jExec.next(key))
				{
					userProfileId = jExec.getInt(key, 1);
					break;
				}
				jExec.closeData(key);
			}
			DLM dlm = DLM.getInstance();
			boolean inTx = srk.isInTransaction();
			dlm.unlockAnyByUser(userProfileId, srk, inTx);
		}
		catch(Exception e)
		{
			String msg = "SessionServices.clearDealLock:: Failure removing deal lock --> Login Id = " + loginId + " - ignored";
			logger.error(msg);
			logger.error(e);
			throw new Exception(msg);
		}

	}
	
	
	/**
	 * clearDealLockBySessionID
	 * @param srk
	 * @param SessionID Session ID
	 * @throws Exception
	 */
	private static void clearDealLockBySessionID(SessionResourceKit srk, String SessionID)
		throws Exception
	{
		logger.debug("SessionServices.clearDealLockBySessionID: SessionID  = " + SessionID);
		try
		{
			DLM dlm = DLM.getInstance();
			boolean inTx = srk.isInTransaction();
			dlm.unlockAnyBySessionID(srk, inTx, SessionID);
		}
		catch(Exception e)
		{
			String msg = "SessionServices.clearDealLockBySessionID:: Failure removing deal lock --> SessionID = " + SessionID + " - ignored";
			logger.error(msg);
			logger.error(e);
			throw new Exception(msg);
		}

	}


	//******************************************
	//* Make this class Serializable to use it as
	//* cusom UserSession object
	//******************************************

	public void deserialize(DataInput inputStream)
		throws IOException
	{
	}


	/**
	 *
	 *
	 */
	//public int getType()
	//{
	//	return CSpValue.USER_TYPE;
	//}


	/**
	 *
	 *
	 */
	public void serialize(DataOutput outputStream)
		throws IOException
	{
	}
	
	//Added for Terracotta to get native session id without the dot.
	//Clement 2008-10-17
	private static String getNativeSessionId(HttpSession session){
		String id = session.getId();
		return trimSessionId(id);
	}
	
	private static String trimSessionId(String sessionId) {
		if(sessionId == null){
			return null;
		} else {
			if(sessionId.indexOf(".") >= 0){
				sessionId = sessionId.substring(0,sessionId.indexOf("."));
			}
			return sessionId;
		}
	}
	
	//Added for commitment letter preview
	//Clement 2009-02-05
	public static boolean isSessionLogedIn(String sessionId, String userId) throws Exception{
		sessionId = trimSessionId(sessionId);
		String loginId =(String) allUsersBySessionId.get(sessionId);
		return loginId.equals(userId);
	}
	
	/**
	* Clear Page for given user profile id or loginId
	*
	*/
	private static void clearPageLock(SessionResourceKit srk,
			int userProfileId, String loginId) throws Exception {
		logger.debug("SessionServices.clearPageLock: loginId  = " + loginId);
		try {
			if (userProfileId == -1) {
				JdbcExecutor jExec = srk.getJdbcExecutor();
				int key = jExec
						.execute("Select USERPROFILEID from USERPROFILE where USERLOGIN = '"
								+ loginId + "'");
				while (jExec.next(key)) {
					userProfileId = jExec.getInt(key, 1);
					break;
				}
				jExec.closeData(key);
			}
			PLM plm = PLM.getInstance();
			boolean inTx = srk.isInTransaction();
			plm.unlockAnyByUser(userProfileId, srk, inTx);
		} catch (Exception e) {
			String msg = "SessionServices.clearPageLock:: Failure removing Page lock --> Login Id = "
					+ loginId + " - ignored";
			logger.error(msg);
			throw new Exception(msg);
		}

	}
	
	
	/**
	 * clearPageLockBySessionID
	 * @param srk
	 * @param SessionID Session ID
	 * @throws Exception
	 */
	private static void clearPageLockBySessionID(SessionResourceKit srk, String SessionID)
		throws Exception
	{
		logger.debug("SessionServices.clearPageLockBySessionID: SessionID  = " + SessionID);
		try
		{
			PLM plm = PLM.getInstance();
			boolean inTx = srk.isInTransaction();
			plm.unlockAnyBySessionID(srk, inTx, SessionID);
		}
		catch(Exception e)
		{
			String msg = "SessionServices.clearPageLockBySessionID:: Failure removing deal lock --> SessionID = " + SessionID + " - ignored";
			logger.error(msg);
			logger.error(e);
			throw new Exception(msg);
		}

	}

}

