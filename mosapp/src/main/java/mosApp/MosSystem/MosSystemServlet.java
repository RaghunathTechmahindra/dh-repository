package mosApp.MosSystem;

/**
 * 14/Nov/2007 DVG #DG660 NBC226009: Sylvia tremblay deals not assigned to agent 
 * 24/Aug/2007 DVG #DG622 Express to Target Download Process 
		- at end of ingested deal workflow module, request upload if flagged as such
 * 24/Aug/2007 DVG #DG620 general thread naming improvement
 * 14/Jan/2005 DVG #DG126 BMO-CCAPS workflow notification
 */


import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import mosApp.ModelTypeMapImpl;
import mosApp.mosAppServletBase;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.commitment.CCM;
import com.basis100.deal.commitment.CCMFactory;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.IEntityBeanPK;
import com.basis100.deal.pk.SourceFirmProfilePK;
import com.basis100.deal.security.DLM;
import com.basis100.deal.security.DealStatusManager;
import com.basis100.deal.security.PDC;
import com.basis100.deal.security.PLM;
import com.basis100.deal.util.BusinessCalendar;
import com.basis100.deal.util.DBA;
import com.basis100.deal.util.DealIntegrityChecker;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.util.thread.DaySchedule;
import com.basis100.util.thread.JobScheduler;
import com.basis100.workflow.WFETrigger;
import com.basis100.workflow.WorkflowAssistBean;
import com.basis100.workflow.notification.NotificationMonitor;
import com.basis100.workflow.notification.WFNotificationEvent;
import com.basis100.workflow.notification.WorkflowNotificationListener;
import com.basis100.workflow.queuedaemon.PostDatedFundCheckDaemon;
import com.basis100.workflow.queuedaemon.TaskMaintenanceDaemon;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestContextImpl;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.ViewBeanManager;
import com.iplanet.jato.view.ViewBean;

import com.filogix.externallinks.services.ExchangeFolderCreator;
import com.filogix.util.Xc;


public class MosSystemServlet extends mosAppServletBase implements Xc {
	
	private static final long serialVersionUID = 1L;

	public MosSystemServlet() {
		super();
	}

	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		final PropertiesCache pc = PropertiesCache.getInstance();

		try {

			logger = ResourceManager.getSysLogger("System");
			logger.setClass(getClass());		//#DG660
			logger.info("--> MosSystemProject@initProjectEnviornment (Version:"+ Sc.BX_VERSION + ")");
			logger.info("--> java.library.path = <"+ System.getProperty("java.library.path") + ">");

			try {
				
				ThreadLocalEntityCache.suspend(); //4.4GR

				logger.info("--> Initialize Resource Manager");

				//pc.init();
				//ResourceManager.init(Config.getProperty(SYS_PROPERTIES_LOCATION),
				//		pc.getProperty(COM_BASIS100_RESOURCE_CONNECTIONPOOLNAME));
				ResourceManager.init();
			}
			catch (Exception exc) {
				System.err.println(exc);
				logger.error(exc);
			}

			SessionResourceKit srk = new SessionResourceKit("{System}");
			srk.getJdbcExecutor();		//#DG660 cause the connection to get created		
			srk.setIndefiniteUseJdbcConnection();		//#DG660		
			logger.info("--> Initialize PickList Data");

			PicklistData.init();

			new BXResources();

			try {
				logger.info("--> Initialize PDC singleton");
				PDC.init(srk);
			}
			catch (Exception exc) {
				System.err.println(exc);
				logger.error(exc);
			}

			try {
				logger.info("--> Initialize DLM singleton");
				DLM.init(srk);
				PLM.init(srk);//5.0
			}
			catch (Exception exc) {
				System.err.println(exc);
				logger.error(exc);
			}

			logger.info("--> Initialize DSM singleton");
			DealStatusManager.getInstance(srk);

			logger.info("--> Initialize Behavior from system properties");

			setBehaviorForSystemProperties();

			logger.info("--> Initialize Business Calendar");
			initBusinessCal(srk, logger);

			logger.info("--> Perform Deal Integrity Checks/Corrections");
			boolean checksDisabled = (pc.getInstanceProperty(
//					boolean checksDisabled = (pc.getProperty(
					COM_BASIS100_STARTUP_DISABLE_INTEGRITY_CHECKS, "N")).equals("Y");
			if (checksDisabled) {
				logger.info("... Deal Integrity Checks disabled");
			}
			else {
				DealIntegrityChecker dic = new DealIntegrityChecker();
				dic.performIntegriryChecks(srk, logger, false);
			}

			// Init BusinessRule Cach  -- by BILLY 04Oct2001
			boolean isBRCachMode = (pc.getInstanceProperty(
//					boolean isBRCachMode = (pc.getProperty(
					COM_BASIS100_STARTUP_ENABLE_BUSINESS_RULE_CACH, "N")).equals("Y");
			if (isBRCachMode) {
				logger.info("... BusinessRule Cach Enabled");
				BusinessRuleExecutor brExec = new BusinessRuleExecutor();
				brExec.initCach(srk);
			}

			// Init Login Flags -- By BILLY 19April2002
			if ((pc.getInstanceProperty(
//					if ((pc.getProperty(
					COM_BASIS100_STARTUP_INITLOGINFLAGS, "N")).equals("Y")) {
				UserProfile up = new UserProfile(srk);
				srk.beginTransaction();
				up.initLoginFlags();
				srk.commitTransaction();
			}
			
			/* 
			 * CIBC DTV. Mapping models based on newly introduced 
			 * parameter: com.basis100.conditions.dtverbiage.generateonthefly 
			 */ 
			String isGenerateOnTheFly = PropertiesCache.getInstance().getInstanceProperty("com.basis100.conditions.dtverbiage.generateonthefly", "N");
			ModelTypeMapImpl.mapLater(isGenerateOnTheFly);
			
			srk.freeResources();
		}
		catch (Exception e) {
			try {
				logger.error("** CRITICAL ERROR STARTING SYSTEM **");
				logger.error(e);
			}
			catch (Exception ee) {
				;
			}
			return;
		}

		logger.info("--> Start Notification Monitor");

		boolean nmDiagMode = (pc.getInstanceProperty(
//				boolean nmDiagMode = (pc.getProperty(
				COM_BASIS100_NOTIFICATION_MONITOR_DIAG_MODE, "N")).equals("Y");

		notificationNom = NotificationMonitor.getInstance(nmDiagMode);
		//Changed for Terrocatta 
		notificationNom.init();
		
		// add notification listener - if not done already
		if (notificationListener == null) {
			notificationListener = new NotificationListener(logger);
			notificationNom.addListener(notificationListener);
		}
		
		
		//--> Here we should init the MIProcessHandler, otherwise the MIReply will not be handled until someone goto MI screen
		//--> Bug Fix By Billy 18Feb2005
		//--> Move it up to call it before start notification monitor for terracotta chnanges By Clement 2Jun2008
		com.basis100.deal.miprocess.MIProcessHandler.getInstance();
		//====================================================================================================================

		notificationNom.startMonitor();

		logger.info("--> Start Session Tracking Services");

		//--> SessionService should init here
		//--> By BILLY 11July2002
		//// This should be moved into the proper place!!!!!!!! OnNewSession method.
		SessionServices.init(logger);
		//=========================================================

		// create a job scheduler - used to control task maintenance daemon
		jobScheduler = new JobScheduler(0);

		logger.info("--> Start Assigned Task Maintenance Daemon");

		boolean tmdDiagMode = (pc.getInstanceProperty(
//				boolean tmdDiagMode = (pc.getProperty(
				COM_BASIS100_TASK_MAINT_DAEMON_DIAG_MODE, "N")).equals("Y");

		taskMaintDaemon = startTaskMaintenanceDaemon(jobScheduler, tmdDiagMode, false);

		logger.info("--> Start Post Dated Funding Checking Daemon");

		PostDatedFundCheckDaemon.startPostDatedFundCheckDaemon(jobScheduler);

		// The following code block was migrated from the this_onAfterInitEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		 SysLogger logger = ResourceManager.getSysLogger("System");
		 logger.info("--> MosSystemProject@this_onAfterInitEvent");
		 ///// shutdownEnviornment();
		 initProjectEnviornment();
		 return;
		 */
	}

	/**
	 *
	 *
	 */
	public void destroy() {
		shutdownEnvironment(); //#DG620
		super.destroy();
	}

	/**
	 *
	 *
	 */
	protected void initializeRequestContext(RequestContext requestContext) {
		super.initializeRequestContext(requestContext);

		//logger.debug("InitializeRequestContext after super.initializeRequestContext");

		// Set a view bean manager in the request context.  This must be
		// done at the module level because the view bean manager is
		// module specifc.
		//
		// TODO: Is this a problem for ViewBeans that cross module boundaries?
		ViewBeanManager viewBeanManager = new ViewBeanManager(requestContext,PACKAGE_NAME);
		//logger.debug("InitializeRequestContext after new ViewBeanManager");

		((RequestContextImpl) requestContext).setViewBeanManager(viewBeanManager);
		//logger.debug("InitializeRequestContext after setViewBeanManager");
	}

	/**
	 *
	 *
	 */
	public String getModuleURL() {

		// The superclass can be configured from init params specified at
		// deployment time.  If the superclass has been configured with
		// a different module URL, it will return a non-null value here.
		// If it has not been configured with a different URL, we use our
		// (hopefully) sensible default.
		String result = super.getModuleURL();

		//// This debug brings the NullPointer exception
		////logger.debug("getModelURL after the super.");

		if (result != null) {
			return result;
		}
		else {
			return DEFAULT_MODULE_URL;
		}

	}

	/**
	 *
	 *
	 */
	protected void onNewSession(RequestContext requestContext)
			throws ServletException {
		super.onNewSession(requestContext);

		//logger.debug("onNewSession after the super");

		/*MigrationToDo  Migration Recommendation
		 If this module is part of a multi-module application, you may be better off
		 moving all of the session event logic to the current class' superclass.
		 Placing session event logic in the superclass will ensure that it will be
		 invoked regardless of which module servlet handles the session related events.*/

		// The following code block was migrated from the this_onNewSessionEvent method
		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		////CSpSession session = event.getActiveSession();
		HttpSession session = RequestManager.getRequestContext().getRequest()
				.getSession();

		logger.debug("New HttpSession@onNewSession: " + session);

		SessionServices.trackNewSession(session);

		//--> Set Session TimeOut value
		//--> Just for testing purpose here as the iPlanet Service setting should overrise it
		//--> By Billy 16July2002
		//--> Set default to 90mins
		//session.setMaxInactiveInterval(90*60);
		////return;

	}

	/**
	 *
	 * comment for 4.3GR
	 * This method does have nothing to do except log
	 */
	protected void onSessionTimeout(RequestContext requestContext)
			throws ServletException {
		/*MigrationToDo  Migration Recommendation
		 If this module is part of a multi-module application, you may be better off
		 moving all of the session event logic to the current class' superclass.
		 Placing session event logic in the superclass will ensure that it will be
		 invoked regardless of which module servlet handles the session related events.*/

		// The following code block was migrated from the this_onSessionTimeoutEvent method
		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		// via Web Event from expired client session
		//--> the following was adjusted by BILLY 11July2002
		SysLogger logger = ResourceManager.getSysLogger("System");
		HttpSession theSession = requestContext.getRequest().getSession();

		//--> The following for Testing purpose only -- By Billy 21Mar2003
		/*
		 Enumeration tmpEm = requestContext.getRequest().getParameterNames();
		 for(int iPara = 0 ; tmpEm.hasMoreElements(); iPara++)
		 {
		 String currPara = tmpEm.nextElement().toString();
		 logger.debug("@onSessionTimeout ==> The Request ParameterName[" + iPara + "]::" + currPara);
		 logger.debug("@onSessionTimeout ==> The Request ParameterValue[" + iPara + "]::" + requestContext.getRequest().getParameter(currPara));
		 }
		 tmpEm = requestContext.getRequest().getHeaderNames();
		 for(int iPara = 0 ; tmpEm.hasMoreElements(); iPara++)
		 {
		 String currPara = tmpEm.nextElement().toString();
		 logger.debug("@onSessionTimeout ==> The Request HeaderName[" + iPara + "]::" + currPara);
		 logger.debug("@onSessionTimeout ==> The Request HeaderValue[" + iPara + "]::" + requestContext.getRequest().getHeader(currPara));
		 }
		 tmpEm = requestContext.getRequest().getAttributeNames();
		 for(int iPara = 0 ; tmpEm.hasMoreElements(); iPara++)
		 {
		 String currPara = tmpEm.nextElement().toString();
		 logger.debug("@onSessionTimeout ==> The Request AttributeName[" + iPara + "]::" + currPara);
		 logger.debug("@onSessionTimeout ==> The Request AttributeValue[" + iPara + "]::" + requestContext.getRequest().getAttribute(currPara));
		 }
		 logger.debug("@onSessionTimeout ==> The RequestedSessionID = "
		 + requestContext.getRequest().getRequestedSessionId());
		 logger.debug("@onSessionTimeout ==> The isRequestedSessionIdFromCookie = "
		 + requestContext.getRequest().isRequestedSessionIdFromCookie());
		 logger.debug("@onSessionTimeout ==> The isRequestedSessionIdFromURL = "
		 + requestContext.getRequest().isRequestedSessionIdFromURL());
		 logger.debug("@onSessionTimeout ==> The isRequestedSessionIdValid = "
		 + requestContext.getRequest().isRequestedSessionIdValid());
		 */
		//==================================================================
		//--> Try to get the previous sessionId
		//--> Fixed to get the expried session by from HTTPRequest
		//--> By Billy 21Mar2003
		String currSessionId = null;
		if (theSession != null) {
			currSessionId = theSession.getId();
		}
		String expiredSessionId = requestContext.getRequest()
				.getRequestedSessionId();

		logger
				.info("--> MosSystemProject@onSessionTimeoutEvent: session timed-out, Current sessionId = "
						+ currSessionId + " :: Expried SessionId = " + expiredSessionId);

//		if (expiredSessionId != null) {
			//--> Remove the stored session and clean up login states
		    // comment for 4.2GR
		    // when user comes back to the appliation, then old sesion previouslyUsedSession will be cleaned
		    // but if not, then the entry stays forever. This is MEMORY LEAK.
		    // moved to ExpressApplicationServlet.processRequest that is called after this method by Jato
//                    SessionServices.previouslyUsedSession(expiredSessionId);

		    // 4.3GR comment 
		    // deleted since when session got expired, it this will be called from MosappSessionListener.sessionDestroyed
//			SessionServices.removeActiveSession(theSession, expiredSessionId, true);

		    // deleted since MosAppSesionListener.processRequest do the same
//			if (SessionServices.previouslyUsedSession(expiredSessionId) == true) {
//				logger.info("--> MosSystemProject@onSessionTimeoutEvent: previous session was active - force to Session Ended page !!");
//				ViewBean thePage = requestContext.getViewBeanManager().getViewBean(
//						pgSessionEndedViewBean.class);
//				thePage.forwardTo(requestContext);
//				return;
//			}
//		}
//		logger.info("--> MosSystemProject@onSessionTimeoutEvent: Unknow reason to be here but force to Session Ended page !!");
//		ViewBean thePage = requestContext.getViewBeanManager().getViewBean(
//				pgSessionEndedViewBean.class);
//		thePage.forwardTo(requestContext);

		//--> This should be called, it will throw as Exception by default
		//--> Comment out by BILLY 16July2002
		//super.onSessionTimeout(requestContext);
	}

	/**
	 *
	 *
	 */
	protected void onBeforeRequest(RequestContext requestContext)
			throws ServletException {
		super.onBeforeRequest(requestContext);
		//logger.debug("onBeforeRequest after the super");

	}

	/** Commented out method has the obsolete signature incoming from JATO 1.1.1.
	 */
	/**
	 protected void onInitializeHandler(RequestContext requestContext, ViewBean viewBean)
	 	throws ServletException	 {
	 	super.onInitializeHandler(requestContext, viewBean);
	 }	 ***/

	protected void onInitializeHandler(RequestContext requestContext,
			RequestHandler requestHandler) throws ServletException {
		super.onInitializeHandler(requestContext, requestHandler);
		//logger.debug("onInitializeHandler after the super");
	}

	/**
	 *
	 *
	 */
	protected void onAfterRequest(RequestContext requestContext) {
		super.onAfterRequest(requestContext);
		//logger.debug("onAfterRequest after the super");
		//--> In order to avoid memory leaking we have to clear all references to ViewBean Manager
		//--> and ModelManager
		//--> By Billy 23April2003
		if (requestContext instanceof RequestContextImpl) {
			((RequestContextImpl) requestContext).removeRequestCompletionListeners();
			((RequestContextImpl) requestContext).setModelManager(null);
			((RequestContextImpl) requestContext).setViewBeanManager(null);
		}
		//==============================================
	}

	/**
	 *
	 *
	 */
	protected void onRequestHandlerNotSpecified(RequestContext requestContext)
			throws ServletException {

		// The ApplicationServletBase version throws a ServletException
		// So if you have special handling, you may not want to call
		// the super version.
		super.onRequestHandlerNotSpecified(requestContext);
	}

	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	private static void setBehaviorForSystemProperties() {
		// page button control during view only mode
		String visibleYN = PropertiesCache.getInstance().getInstanceProperty(
//				String visibleYN = PropertiesCache.getInstance().getProperty(
				COM_BASIS100_CONTROLS_VIEWMODEBEHAVIOR_VISIBLE);

		if (visibleYN != null && visibleYN.trim().equals("N")) {
			PageEntry.setHideInViewMode(true);
		}
		else {
			PageEntry.setHideInViewMode(false);
		}
	}

	/**
	 *
	 *
	 */
	private static void initBusinessCal(SessionResourceKit srk, SysLogger logger) {
		try {
			InstitutionProfile ip = getInstitutionProfile(srk);

			logger.info("Institution Profile from initBusinessCal: " + ip);

			// initialize a business calendar instance to trigger one-time initialization
			BusinessCalendar bc = new BusinessCalendar(srk, ip.getTimeZoneEntryId());
		}
		catch (Exception e) {
			logger
					.error("Exception @initBusinessCal: Initializing Business Calendar - ignored");
			logger.error(e);
		}
	}

	/**
	 *
	 *
	 */
	private static InstitutionProfile getInstitutionProfile(SessionResourceKit srk)
			throws Exception {
		InstitutionProfile ip = new InstitutionProfile(srk);

		ip = ip.findByFirst();

		return ip;
	}

	/**
	 *
	 *
	 */
	private static TaskMaintenanceDaemon startTaskMaintenanceDaemon(
			JobScheduler jobScheduler, boolean verbose, boolean testing) {
		// daily run schedule for the task maintenance daemon:
		//
		//      Production: active 4:00am - 9:00pm
		//      Testing:    run every other 5 min period during day
		DaySchedule daySchedule = null;

		int hours[] = null;

		int mins[] = null;

		if (testing) {
			hours = DaySchedule.generateScheduleTransition(5, true);
			mins = DaySchedule.generateScheduleTransition(5, false);
			daySchedule = new DaySchedule(false, hours, mins);
		}
		else {
			hours = new int[2];
			int startHr = com.basis100.deal.util.TypeConverter
					.intTypeFrom((PropertiesCache.getInstance().getInstanceProperty(
//							.intTypeFrom((PropertiesCache.getInstance().getProperty(
							COM_BASIS100_WORKFLOW_TASK_MAINTENANCE_DAEMON_STARTHOUR, "4")), 0);
			int endHr = com.basis100.deal.util.TypeConverter.intTypeFrom(
					(PropertiesCache.getInstance().getInstanceProperty(
//							(PropertiesCache.getInstance().getProperty(
							COM_BASIS100_WORKFLOW_TASK_MAINTENANCE_DAEMON_ENDHOUR, "21")), 0);
			hours[0] = startHr;
			hours[1] = endHr;

			daySchedule = new DaySchedule(false, hours);
		}

		jobScheduler.executeAtAndRepeat(TaskMaintenanceDaemon.getInstance(verbose),
				new Date(), JobScheduler.WEEK_DAILY, JobScheduler.FOREVER, daySchedule);

		return TaskMaintenanceDaemon.getInstance();
	}

	/**
	 *
	 *
	 */
	protected static void shutdownEnvironment() {
		if (isResourceAvailable == false) {
			return;
		}

		SysLogger logger = ResourceManager.getSysLogger("System");

		logger.info("--> MosSystemProject@shutdownEnviornment");

		logger.info("--> Stop Notification Monitor");

		notificationNom.stopMonitor();

		logger.info("--> Shutdown Task Maintenance Daemon");

		taskMaintDaemon.stopDaemon();

		// stop job scheduler
		jobScheduler.shutdown();

		logger.info("--> Shut down Resource Manager");

		ResourceManager.shutdown();

		logger.info("--> Shut down logger");

		SysLog.shutdown();

		isResourceAvailable = false;
	}

	/**
	 *
	 *
	 */

	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	public static final String DEFAULT_MODULE_URL = "../mosApp/MosSystem"; ////"../MigtoolboxSampleAdd"
	public static String PACKAGE_NAME = getPackageName(MosSystemServlet.class.getName());

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	private static JobScheduler jobScheduler;
	private static NotificationMonitor notificationNom;
	private static NotificationListener notificationListener = null;
	private static TaskMaintenanceDaemon taskMaintDaemon = null;
	//#DG620 public static boolean isResourceAvailable = false;
	public static boolean isResourceAvailable = true;
	SysLogger logger;

	private static String registryUrl;
	private static String lenderName;
	private static String taskCheckerName;
}

class NotificationListener implements WorkflowNotificationListener, Xc {
	SysLogger logger;

	public NotificationListener(SysLogger logger) {
		this.logger = logger;
	}

	public void workflowNotified(WFNotificationEvent evt) {
		int type = evt.getWorkflowNotificationType();
		SessionResourceKit srk = new SessionResourceKit("WFListener");
    
    // set VPD for ML- Sep, 24, 2007
    srk.getExpressState().setDealInstitutionId(evt.getInstitutionProfileId());
		logger.info("New WorkFlow notified with type = " + type 
				+ " :: Dealid = " + evt.getDealId() + " InstitutionProfileId = " + evt.getInstitutionProfileId());

		switch (type) {
			case Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL:
			case Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_B: // #DG126 bmo
			case Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_WCP: {
				doNewDeal(evt, type, srk);
				break;
			}

			case Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_WUP: {
				doNewDealWUP(evt, type, srk);
				break;
			}
			// ====================================================================================================================
			// --> Ticket#371 :: Change for Cervus :: Send Upload for Cervus
			// --> By Billy 28May2004
			case Mc.WORKFLOW_NOTIFICATION_TYPE_TASK_TRIGGER_TYPE_2_WUP: {
				doTask2WUP(evt, type, srk);
				break;
			}
				// =================================================================================================================
			case Mc.WORKFLOW_NOTIFICATION_TYPE_TASK_TRIGGER_TYPE_2_WCP: {
				doTask2WCP(evt, type, srk);
				break;
			}
			case Mc.WORKFLOW_NOTIFICATION_TYPE_TASK_TRIGGER_TYPE_2: {
				doTask2(evt, type, srk);
				break;
			}

				// --> Special handle to refresh System Properties and Picklist
				// --> By Billy 06June2003
			case 99: {
				// --> Reset Properties
				System.out.println(getClass().getName() + ':'
						+ "Refreshing System Properties and Picklist ...");
				logger.info("Refreshing System Properties and Picklist ...");

				PropertiesCache.getInstance().init();
				// --> force to ReNew PickList
				PicklistData.init(true);

				System.out.println(getClass().getName() + ':'
						+ "Refreshing System Properties and Picklist COMPLETED !!");
				logger.info("Refreshing System Properties and Picklist COMPLETED !!");

				break;
			}
				// ===================================================================================
		}

		//#DG622 request upload
		// added institutionProfileId since 4.3GR
		if(PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), 
			COM_FILOGIX_INGESTION_WORKFLOW_UPLOAD, "N").equalsIgnoreCase("Y"))
			doTaskUpload(evt, srk);
		
		srk.freeResources();

		return;
	}

	private void doNewDeal(WFNotificationEvent evt, int type,
			SessionResourceKit srk) {
		int dealId = evt.getDealId();

		int copyId = 1; //default
		try {
			MasterDeal md = new MasterDeal(srk, null, evt.getDealId());
			copyId = md.getGoldCopyId();
		}
		catch (Exception e) {
			logger.warning("MosSystem Main : Failed to get Gold CopyId - fallback to use copy 1 :: "+ e);
		}

		WorkflowAssistBean wfa = new WorkflowAssistBean();

		if (wfa.generateInitialTasks(dealId, copyId, srk) == false) {
			logger.error("Deal from ingestion :: id = " + dealId+ " :: No initial tasks generated");
		}
		else 
		{
			// New requirement - to send DealSummary Package if requested
			boolean isSend = (PropertiesCache.getInstance().getProperty(-1, 
					COM_BASIS100_WORKFLOW_SENDDEALSUMMARYPACKAGE, "N")).equals("Y");
			try {
				Deal deal = DBA.getDeal(srk, dealId, copyId);
				createExchangeFolder(srk,deal);
				if (isSend == true) {
					DocumentRequest.requestDealSummaryPackage(srk, deal);
					//--> New Requirement to send CAP link doc.
					//--> By Billy 10Mar2003
					//--> Check if request CapLink -- By Billy 15April2003
				}
				if (type == Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_WCP) {
					DocumentRequest.requestTDCTCapLink(srk, deal);
					//========================================
				}
				else if (type == Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_B) { //#DG126 bmo
					DocumentRequest.requestCCASUpload(srk, deal);
				}
			}
			catch (Exception e) {
				logger.error("Deal from ingestion :: id = " + dealId
						+ " :: failed to send DealSummaryPackage or CAP link !!"+e);
			}
		}
	}

	private void doTask2(WFNotificationEvent evt, int type, SessionResourceKit srk) {
		try {
			int dealId = evt.getDealId();
			int copyId = 1;
			int underwriterId = 1;
			try {
				// 3.2.2GR fixed for FXP23146 to use senarioRecommended copy for TaskGeneration
				MasterDeal md = new MasterDeal(srk, null, dealId);
				copyId = md.getGoldCopyId();
				Deal target = new Deal(srk, null, dealId, copyId);
				underwriterId = target.getUnderwriterUserId();
				target = target.findByRecommendedScenario(new DealPK(md.getDealId(), copyId), true);
				underwriterId = target.getUnderwriterUserId();
				copyId = target.getCopyId();
				// 3.2.2GR fixed for FXP23146 
			} catch (Exception e) {
				// use copyid = 1 or G copyId
			}
		    logger.trace("initial tasks generation run for dealid = " + dealId + " copyId = " + copyId);
		    srk.beginTransaction();
		    // =====================================================================================
			// =====================================================================================
			//DocumentRequest.requestTDCTCapLink(srk, target);
			// 3.2.2GR fixed for FXP23146 to use senarioRecommended copy for TaskGeneration
			WFETrigger.workflowTrigger(2, srk, underwriterId, dealId,
					copyId, -1, null);
			// 3.2.2GR fixed for FXP23146 
			srk.commitTransaction();
		}
		catch (Exception e) {
			srk.cleanTransaction();
			String msg = (evt != null) ? ("" + evt.getDealId())
					: "{cannot determine - null event}";
			srk.getSysLogger().error(
					"MosSystem Main : Failed to trigger workflow for: " + msg);
			srk.getSysLogger().error(e);
		}
	}

	private void doTask2WCP(WFNotificationEvent evt, int type,
			SessionResourceKit srk) {
		try {
			MasterDeal md = new MasterDeal(srk, null, evt.getDealId());
			int goldCopyId = md.getGoldCopyId();
			Deal target = new Deal(srk, null, md.getDealId(), goldCopyId);
			int underwriterId = target.getUnderwriterUserId();
			
			// fixed same as doTask2 for 4.2GR STARTS
            target = target.findByRecommendedScenario(new DealPK(md.getDealId(), goldCopyId), true);
            underwriterId = target.getUnderwriterUserId();
            int copyId = target.getCopyId();

			srk.beginTransaction();
			DocumentRequest.requestTDCTCapLink(srk, target);
			WFETrigger.workflowTrigger(2, srk, underwriterId, md.getDealId(),
			        copyId, -1, null);
            // fixed same as doTask2 for 4.2GR ENDS
			srk.commitTransaction();
		}
		catch (Exception e) {
			srk.cleanTransaction();
			String msg = (evt != null) ? ("" + evt.getDealId())
					: "{cannot determine - null event}";
			srk.getSysLogger().error(
					"MosSystem Main : Failed to trigger workflow for: " + msg);
			srk.getSysLogger().error(e);
		}
	}

	private void doTask2WUP(WFNotificationEvent evt, int type,
			SessionResourceKit srk) {
		try {
			MasterDeal md = new MasterDeal(srk, null, evt.getDealId());
			int goldCopyId = md.getGoldCopyId();
			Deal deal = new Deal(srk, null, md.getDealId(), goldCopyId);
			int underwriterId = deal.getUnderwriterUserId();

			srk.beginTransaction();
			com.basis100.deal.commitment.CCM ccm = new com.basis100.deal.commitment.CCM(srk);
			ccm.sendServicingUpload(srk, deal, Mc.SERVICING_UPLOAD_TYPE_INGEST, 0);
			//--> Ticket#522 : Change Request - to upload for the new ingested deal for Resubmission
			//--> By Billy 19Aug2004
			// Check and get the resubmission dealid
			if (deal.getReferenceDealTypeId() == Mc.DEAL_REF_TYPE_RESUBMITTED_AS_DEAL) {
				// get the reference dealid and copyid
				int newDealId = -1;
				int newRefDealId = -1;
				int newCopyId = -1;
				Deal newDeal = null;
				try {
					newDealId = Integer.parseInt(deal.getReferenceDealNumber());
					newCopyId = (new MasterDeal(srk, null, newDealId)).getGoldCopyId();
					newDeal = DBA.getDeal(srk, newDealId, newCopyId);
					newRefDealId = Integer.parseInt(newDeal.getReferenceDealNumber());
				}
				catch (Exception ex) {
					srk.getSysLogger().warning(
							"MosSystem Main : Failed to get Reference Deal : current delid = "
									+ deal.getDealId());
					srk.getSysLogger().warning(ex.toString());
				}

				//--> Check to make sure we got the right reference deal
				if (newDealId > 0
						&& newDeal != null
						&& newRefDealId == deal.getDealId()
						&& newDeal.getReferenceDealTypeId() == Mc.DEAL_REF_TYPE_ORIGINAL_DEAL_RESUBMIT) {
					ccm.sendServicingUpload(srk, newDeal,Mc.SERVICING_UPLOAD_TYPE_INGEST, 0);
				}
			}
			//========================== End of changes : ticket#522 =================================
			WFETrigger.workflowTrigger(2, srk, underwriterId, md.getDealId(),
					goldCopyId, -1, null);
			srk.commitTransaction();
		}
		catch (Exception e) {
			srk.cleanTransaction();
			String msg = (evt != null) ? ("" + evt.getDealId())
					: "{cannot determine - null event}";
			srk.getSysLogger().error(
					"MosSystem Main : Failed to trigger workflow for: " + msg);
			srk.getSysLogger().error(e);
		}
	}

	private void doNewDealWUP(WFNotificationEvent evt, int type,
			SessionResourceKit srk) {
		int dealId = evt.getDealId();
		int copyId = 1; //default
		try {
			MasterDeal md = new MasterDeal(srk, null, evt.getDealId());
			copyId = md.getGoldCopyId();
		}
		catch (Exception e) {
			logger.warning("MosSystem Main : Failed to get Gold CopyId - fallback to use copy 1 :: "+ e);
		}

		try {
			Deal deal = DBA.getDeal(srk, dealId, copyId);

			//--> Ticket#522 : Change Request - to upload if IC Rejected
			//--> By Billy 19Aug2004
			if (deal.getStatusId() != Mc.DEAL_DENIED
					|| deal.getDenialReasonId() != Mc.DENIAL_REASON_IC_REJECT) {
				//--> Commented the following out for Cervus Phase II
				//--> By Billy 06Dec2004
				/*// Execute Auto MI Process here
				 WFProcessBase theMIProc = new AutoMIProcess(deal, srk);
				 if(theMIProc.Execute(true) == WFProcessBase.ERROR)				 {
				 // Execute error !!
				 logger.error("Deal from ingestion :: id = " + dealId + " :: Error executing AutoMIProcess !!");
				 }				 */
				//--> Instead it should init the WorkFlow as usual
				//--> By Billy 06Dec2004
				WorkflowAssistBean wfa = new WorkflowAssistBean();
				if (wfa.generateInitialTasks(dealId, copyId, srk) == false) {
					logger.error("Deal from ingestion :: id = " + dealId+ " :: No initial tasks generated");
				}
				else {
					createExchangeFolder(srk,deal);
					srk.beginTransaction();
					// New requirement - to send DealSummary Package if requested
					boolean isSend = (PropertiesCache.getInstance().getProperty(-1, 
							COM_BASIS100_WORKFLOW_SENDDEALSUMMARYPACKAGE, "N")).equals("Y");

					if (isSend == true) {
						DocumentRequest.requestDealSummaryPackage(srk, deal);
					}
					srk.commitTransaction();
				}
			}

			// Catherine for Billy, bug fix, 5-May-05
			srk.beginTransaction();
			/*LEN255548
			String env = PropertiesCache.getInstance().getProperty(COM_FILOGIX_ENVIRONMENT, CCMFactory.EXPRESS_ENV_NONE);
			CCM ccm = CCMFactory.getInstance().getCCM(env, srk);*/
			CCM ccm = new CCM(srk);
			ccm.sendServicingUpload(srk, deal, Mc.SERVICING_UPLOAD_TYPE_INGEST, 0);
			srk.commitTransaction();
			//================ End of changes : ticket#522 ====================================
		}
		catch (Exception e) {
			logger.error("Deal from ingestion :: id = " + dealId
					+ " :: failed to send DealSummaryPackage or Cervus Upload !!"+e);
		}
	}

	//#DG622 request upload
	/**
	 *	request upload when a deal gets ingested 
	 * @param evt the notification event
	 * @param srk the db connection
	 */
	private void doTaskUpload(WFNotificationEvent evt, SessionResourceKit srk) {
		int dealId = evt.getDealId();
		logger.info("@doTaskBrokUpload start: Dealid = " + dealId);
		if(dealId <= 0)
			return;
		try {
			
			// only handle ingestion notification types
			switch (evt.getWorkflowNotificationType()) {
				case Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL:
				case Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_B:
				case Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_WCP: 
				case Mc.WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_WUP: 
				case Mc.WORKFLOW_NOTIFICATION_TYPE_TASK_TRIGGER_TYPE_2_WUP: 
				case Mc.WORKFLOW_NOTIFICATION_TYPE_TASK_TRIGGER_TYPE_2_WCP: 
				case Mc.WORKFLOW_NOTIFICATION_TYPE_TASK_TRIGGER_TYPE_2: 
					break;
				default:
					return;
			}
			
			Deal deal = new Deal(srk, null);
			
			/* there is a requirement to prevent new fields to get automatically sent
			 in this upload to client - this would break their schema validation.

			 DOCUMENT_GENERAL_TYPE_GEMONEY_UPLOAD is used here because it was 
			 originally configured to go thru a final transformation to block any new
			 fields. But ANY document that uses some 'broker.upload' as a basis 
			 (DOCUMENTPROFILE.DOCUMENTSCHEMAPATH) and some appropriate final 
			 transformation like 'GEMoneyUpload.xsl' 
			 (DOCUMENTPROFILE.DOCUMENTTEMPLATEPATH) fits perfectly well.
			 */
			//is it really necesssary to get the deal here??
			//deal.findByNonTransactional(dealId);
			deal.setDealId(dealId);

			DocumentRequest.requestDocByBusRules(srk,deal,DOCUMENT_GENERAL_TYPE_GEMONEY_UPLOAD, null);			
		}
		catch (Exception e) {
			logger.error("@doTaskBrokUpload: Dealid = " + dealId
					+ " :: failed to request Upload !!"+e);
		}
	}
	private void createExchangeFolder(SessionResourceKit srk, Deal deal)
	{
		 //------ 2008-12-23 Exchange Folder Creation Start ---- 
	    // Section 3 - Create Deal Folder in Exchange 2.0 if not exist yet.
	    logger.trace("INGESTION: Create Deal Folder in Exchange 2.0.");

	    try

	    {
	    	ExchangeFolderCreator dci = new ExchangeFolderCreator(srk);
	    	int folderIndicator = dci.getFolderCreationIndicator();

	    	logger.debug("INGESTION: folderIndicator: " + folderIndicator);

	      if (folderIndicator == Mc.DO_NOT_CREATE_DOC_CENTRAL_DEAL_FOLDER ||
	          folderIndicator == Mc.CREATE_DOC_CENTRAL_DEAL_FOLDER_ON_APPROVAL)
	      {
	        // do nothing, in the second case folder creation is postponed until deal approval.
	        logger.trace("INGESTION: Do not create Doc Central Deal folder");
	      }
	      else if (folderIndicator == Mc.CREATE_DOC_CENTRAL_DEAL_FOLDER_ON_SUBMISSION)
	      {
	          // Check first whether the folder for this deal is created or not.
	          logger.trace("INGESTION: Create Doc Central Deal folder");

	          int dealid = deal.getDealId();
	          int copyid = deal.getCopyId();

	          logger.debug("INGESTION: dealidToExchange2: " + dealid);
	          logger.debug("INGESTION: copyidToExchange2: " + copyid);

	          deal = new Deal(srk, null, dealid, copyid);

	          // Just sanity check, it should not be here at the moment.
	          String folderIdentifier = "";

	          logger.debug("INGESTION: DealFoundId: " + deal.getDealId());
	          logger.debug("INGESTION: DealFoundCopyId: " + deal.getCopyId());
	          logger.debug("INGESTION: DealFoundCopyType: " + deal.getCopyType());
	          logger.debug("INGESTION: FolderExists?: " + dci.folderExists(deal));


	          if (!dci.folderExists(deal)) {
	            folderIdentifier = dci.createFolder(deal);

	            String sfpIdentifier = deal.getSourceFirmProfile().
	              getSourceFirmName();
	            logger.debug("INGESTION: sobIdentifierToDocCentral: " +
	                         sfpIdentifier);

	            int sfpId = deal.getSourceFirmProfileId();

	            SourceFirmProfile SFPBean =
	              new SourceFirmProfile(srk).findByPrimaryKey(
	              new SourceFirmProfilePK(sfpId));

	            logger.debug("INGESTION: sobIdentifierToDocCentralFromBean: " +
	                         SFPBean.getSourceFirmName());
	            try
        	    {
	            	DealHistoryLogger dhl = DealHistoryLogger.getInstance(srk);	            	     
	        	    int upid = srk.getExpressState().getUserProfileId();
		            if (folderIdentifier != null && !folderIdentifier.trim().equals("")) {
		            	dhl.log(dealid,
		        	    		  copyid,
		        	              dhl.docCentralFolderCreationInfo(SFPBean, folderIdentifier),
		        	              Mc.DEAL_TX_TYPE_DOCCENTRAL_FOLDER_CREATION,
		        	              upid );
		            }
		            else
		            {
		            	String DEAL_FOLDER_DOC_CENTRAL_CREATION_FAILED = "Deal Folder creation failed in DocCentral";
		            	dhl.log(dealid,
		        	    		  copyid,
		        	    		  DEAL_FOLDER_DOC_CENTRAL_CREATION_FAILED,
		        	              Mc.DEAL_TX_TYPE_DOCCENTRAL_FOLDER_CREATION,
		        	              upid );
		            }
        	    }
        	    catch(Exception ex)
        	    {
        	      StringBuffer msg = new StringBuffer("Deal History Entry logging failed after Successful DocCentral Folder creation");
        	      msg.append("DealId: ").append(deal.getDealId());
        	      msg.append("CopyId: ").append(deal.getCopyId());
        	      msg.append("FolderIdentifier: ").append(folderIdentifier);
        	      logger.error(msg.toString());
        	      logger.error(ex);
        	    }
	          }
	        }
	    }
	    catch(Exception ex)
	    {
	      String msg = "INGESTION:  NOTE: Failed to create folder "+
	                   " Deal: " + deal.getDealId();
	      logger.error(msg);
	      //// Add this function
	      ////handler.logDocCentralFolderFailure(srk, beanPK, SFPBean, folderIdentifier);
	    }
	    //--DocCentral_FolderCreate--21Sep--2004--end--//

	}
}
