package mosApp.MosSystem;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import MosSystem.Sc;

import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.WebActions;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.event.TiledViewRequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.RadioButtonGroup;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;


/**
 *
 *  DJ_CR136: Insure ONLY type of borrower.
 *
 */
public class pgLifeDisabilityInsuranceRepeated2TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgLifeDisabilityInsuranceRepeated2TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doInsureOnlyApplicantModel.class);
		registerChildren();
		initialize();
	}

	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
    getTxInsureOnlyFirstName().setValue(CHILD_TXINSUREONLYFIRSTNAME_RESET_VALUE);
    getTxInsureOnlyLastName().setValue(CHILD_TXINSUREONLYLASTNAME_RESET_VALUE);
		getStPrimaryInsureOnlyFlag().setValue(CHILD_STPRIMARYINSUREONLYFLAG_RESET_VALUE);
		getCbInsureOnlyGender().setValue(CHILD_CBINSUREONLYGENDER_RESET_VALUE);
		getStInsureOnlyAge().setValue(CHILD_STINSUREONLYAGE_RESET_VALUE);
		getRbLifeInsurance().setValue(CHILD_RBLIFEINSURANCE_RESET_VALUE);
		getRbDisabilityInsurance().setValue(CHILD_RBDISABILITYINSURANCE_RESET_VALUE);
		getHdInsureOnlyId().setValue(CHILD_HDINSUREONLYID_RESET_VALUE);
		getHdInsureOnlyGenderId().setValue(CHILD_HDINSUREONLYGENDERID_RESET_VALUE);
		getCbInsureOnlySmoker().setValue(CHILD_CBINSUREONLYSMOKER_RESET_VALUE);
    getBtDeleteApplicant().setValue(CHILD_BTDELETEAPPLICANT_RESET_VALUE);
    getCbIOApplicantDOBMonth().setValue(CHILD_CBIOAPPLICANTDOBMONTH_RESET_VALUE);
    getTxIOApplicantDOBDay().setValue(CHILD_TXIOAPPLICANTDOBDAY_RESET_VALUE);
    getTxIOApplicantDOBYear().setValue(CHILD_TXIOAPPLICANTDOBYEAR_RESET_VALUE);
    getHdCopyId().setValue(CHILD_HDCOPYID_RESET_VALUE);
	}

	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
    registerChild(CHILD_TXINSUREONLYFIRSTNAME,TextField.class);
    registerChild(CHILD_TXINSUREONLYLASTNAME,TextField.class);
		registerChild(CHILD_STPRIMARYINSUREONLYFLAG,StaticTextField.class);
		registerChild(CHILD_CBINSUREONLYGENDER,ComboBox.class);
		registerChild(CHILD_STINSUREONLYAGE,StaticTextField.class);
		registerChild(CHILD_RBLIFEINSURANCE,RadioButtonGroup.class);
		registerChild(CHILD_RBDISABILITYINSURANCE,RadioButtonGroup.class);
		registerChild(CHILD_HDINSUREONLYID,HiddenField.class);
		registerChild(CHILD_HDINSUREONLYGENDERID,HiddenField.class);
		registerChild(CHILD_CBINSUREONLYSMOKER,StaticTextField.class);
    registerChild(CHILD_BTDELETEAPPLICANT,Button.class);
    registerChild(CHILD_CBIOAPPLICANTDOBMONTH, ComboBox.class);
    registerChild(CHILD_TXIOAPPLICANTDOBDAY, TextField.class);
    registerChild(CHILD_TXIOAPPLICANTDOBYEAR, TextField.class);
    registerChild(CHILD_HDCOPYID,HiddenField.class);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoInsureOnlyApplicantModel());

				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    logger = SysLog.getSysLogger("pgLDITV");
		handler.pageGetState(this.getParentViewBean());

    String acceptedStr = BXResources.getGenericMsg("ACCEPTED_LABEL", handler.getTheSessionState().getLanguageId());
    String refusedStr = BXResources.getGenericMsg("REFUSED_LABEL", handler.getTheSessionState().getLanguageId());
    String ineligibleStr = BXResources.getGenericMsg("INELIGIBLE_LABEL", handler.getTheSessionState().getLanguageId());

    //Setup Options for RadioButton groups
    rbLifeInsuranceOptions.setOptions(new String[]{acceptedStr, refusedStr, ineligibleStr},new String[]{"A", "R", "I"});
    rbDisabilityInsuranceOptions.setOptions(new String[]{acceptedStr, refusedStr, ineligibleStr},new String[]{"A", "R", "I"});

    //Set up all ComboBox options
    cbInsureOnlyGenderOptions.populate(getRequestContext());
    cbInsureOnlySmokerOptions.populate(getRequestContext());


		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}

	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
    boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			int rowNum = this.getTileIndex();
      boolean ret = true;

      validJSFromTiledView();

      LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
			handler.pageGetState(this.getParentViewBean());
      logger.debug("LDTV2@RetvalBefore: " + ret + " , RowIndx: " + rowNum);

			ret = handler.checkDisplayThisRow("Repeated2");
     logger.debug("LDTV2@RetvalAfter: " + ret + " , RowIndx: " + rowNum);

     doInsureOnlyApplicantModelImpl m =(doInsureOnlyApplicantModelImpl)this.getdoInsureOnlyApplicantModel();

      if (m.getSize() > 0 && ret == true)
      {
        logger.debug("LDTV2@nextTile::Location: " + m.getLocation());
        handler.setIOApplicantMainDisplayFields(m, rowNum);
      }
      else { ret = false; }
      handler.pageSaveState();

      return ret;
		}

		return movedToRow;
	}

  public void setCurrentDisplayOffset(int offset) throws Exception
  {
    setWebActionModelOffset(offset);
    handleWebAction(WebActions.ACTION_FIRST);
  }

	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
    if (name.equals(CHILD_TXINSUREONLYFIRSTNAME))
    {
      TextField child = new TextField(this,
      getdoInsureOnlyApplicantModel(),
      CHILD_TXINSUREONLYFIRSTNAME,
      doInsureOnlyApplicantModel.FIELD_DFINSUREONLYAPPLICANTFIRSTNAME,
      CHILD_TXINSUREONLYFIRSTNAME_RESET_VALUE,
      null);
      return child;
    }
    else
    if (name.equals(CHILD_TXINSUREONLYLASTNAME))
    {
      TextField child = new TextField(this,
      getdoInsureOnlyApplicantModel(),
      CHILD_TXINSUREONLYLASTNAME,
      doInsureOnlyApplicantModel.FIELD_DFINSUREONLYAPPLICANTLASTNAME,
      CHILD_TXINSUREONLYLASTNAME_RESET_VALUE,
      null);
      return child;
    }
		else
		if (name.equals(CHILD_STPRIMARYINSUREONLYFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getdoInsureOnlyApplicantModel(),
				CHILD_STPRIMARYINSUREONLYFLAG,
       doInsureOnlyApplicantModel.FIELD_DFPRIMARYINSUREONLYAPPLICANTFLAG,
				CHILD_STPRIMARYINSUREONLYFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STINSUREONLYAGE))
		{
			StaticTextField child = new StaticTextField(this,
      getdoInsureOnlyApplicantModel(),
      CHILD_STINSUREONLYAGE,
      doInsureOnlyApplicantModel.FIELD_DFAGE,
      CHILD_STINSUREONLYAGE_RESET_VALUE,
      null);
			return child;
		}
    else
		if (name.equals(CHILD_RBLIFEINSURANCE))
		{
			RadioButtonGroup child = new RadioButtonGroup( this,
				getdoInsureOnlyApplicantModel(),
				CHILD_RBLIFEINSURANCE,
				doInsureOnlyApplicantModel.FIELD_DFLIFESTATUSVALUE,
				CHILD_RBLIFEINSURANCE_RESET_VALUE,
				null);
      child.setLabelForNoneSelected("");
			child.setOptions(rbLifeInsuranceOptions);
			return child;
		}
		else
		if (name.equals(CHILD_RBDISABILITYINSURANCE))
		{
			RadioButtonGroup child = new RadioButtonGroup( this,
				getdoInsureOnlyApplicantModel(),
				CHILD_RBDISABILITYINSURANCE,
				doInsureOnlyApplicantModel.FIELD_DFLDISABILITYSTATUSVALUE,
				CHILD_RBDISABILITYINSURANCE_RESET_VALUE,
				null);
      child.setLabelForNoneSelected("");
			child.setOptions(rbDisabilityInsuranceOptions);
			return child;
		}
    else
		if (name.equals(CHILD_HDINSUREONLYID))
		{
			HiddenField child = new HiddenField(this,
				getdoInsureOnlyApplicantModel(),
				CHILD_HDINSUREONLYID,
				doInsureOnlyApplicantModel.FIELD_DFINSUREONLYAPPLICANTID,
				CHILD_HDINSUREONLYID_RESET_VALUE,
				null);
			return child;
		}
    else
    if (name.equals(CHILD_HDCOPYID))
    {
      HiddenField child = new HiddenField(this,
        getdoInsureOnlyApplicantModel(),
        CHILD_HDCOPYID,
        doInsureOnlyApplicantModel.FIELD_DFCOPYID,
        CHILD_HDCOPYID_RESET_VALUE,
        null);
      return child;
    }
		else
    if (name.equals(CHILD_HDINSUREONLYGENDERID))
		{
			HiddenField child = new HiddenField(this,
				getdoInsureOnlyApplicantModel(),
				CHILD_HDINSUREONLYGENDERID,
				doInsureOnlyApplicantModel.FIELD_DFINSUREONLYAPPLICANTGENDERID,
				CHILD_HDINSUREONLYGENDERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBINSUREONLYSMOKER))
		{
			ComboBox child = new ComboBox( this,
				getdoInsureOnlyApplicantModel(),
				CHILD_CBINSUREONLYSMOKER,
				doInsureOnlyApplicantModel.FIELD_DFSMOKERSTATUSID,
				CHILD_CBINSUREONLYSMOKER_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbInsureOnlySmokerOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBINSUREONLYGENDER))
		{
			ComboBox child = new ComboBox( this,
				getdoInsureOnlyApplicantModel(),
				CHILD_CBINSUREONLYGENDER,
				doInsureOnlyApplicantModel.FIELD_DFINSUREONLYAPPLICANTGENDERID,
				CHILD_CBINSUREONLYGENDER_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbInsureOnlyGenderOptions);
			return child;
		}
    else
    if (name.equals(CHILD_BTDELETEAPPLICANT))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTDELETEAPPLICANT,
        CHILD_BTDELETEAPPLICANT,
        CHILD_BTDELETEAPPLICANT_RESET_VALUE,
        null);
        return child;
    }
    else
    if(name.equals(CHILD_CBIOAPPLICANTDOBMONTH))
    {
      ComboBox child = new ComboBox(this,
        //getDefaultModel(),
        getdoInsureOnlyApplicantModel(),
        CHILD_CBIOAPPLICANTDOBMONTH,
        CHILD_CBIOAPPLICANTDOBMONTH,
        CHILD_CBIOAPPLICANTDOBMONTH_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbIOApplicantDOBMonthOptions);
      return child;
    }
    else
    if(name.equals(CHILD_TXIOAPPLICANTDOBDAY))
    {
      TextField child = new TextField(this,
        //getDefaultModel(),
        getdoInsureOnlyApplicantModel(),
        CHILD_TXIOAPPLICANTDOBDAY,
        CHILD_TXIOAPPLICANTDOBDAY,
        CHILD_TXIOAPPLICANTDOBDAY_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXIOAPPLICANTDOBYEAR))
    {
      TextField child = new TextField(this,
        //getDefaultModel(),
        getdoInsureOnlyApplicantModel(),
        CHILD_TXIOAPPLICANTDOBYEAR,
        CHILD_TXIOAPPLICANTDOBYEAR,
        CHILD_TXIOAPPLICANTDOBYEAR_RESET_VALUE,
        null);
      return child;
    }
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


  /**
   *
   *
   */
  public TextField getTxInsureOnlyFirstName()
  {
    return (TextField)getChild(CHILD_TXINSUREONLYFIRSTNAME);
  }

  /**
   *
   *
   */
  public TextField getTxInsureOnlyLastName()
  {
    return (TextField)getChild(CHILD_TXINSUREONLYLASTNAME);
  }

	/**
	 *
	 *
	 */
	public StaticTextField getStInsureOnlySmoker()
	{
		return (StaticTextField)getChild(CHILD_STSMOKER);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStPrimaryInsureOnlyFlag()
	{
		return (StaticTextField)getChild(CHILD_STPRIMARYINSUREONLYFLAG);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStInsureOnlyAge()
	{
		return (StaticTextField)getChild(CHILD_STINSUREONLYAGE);
	}

	/**
	 *
	 *
	 */
	public RadioButtonGroup getRbLifeInsurance()
	{
		return (RadioButtonGroup)getChild(CHILD_RBLIFEINSURANCE);
	}

	/**
	 *
	 *
	 */
	public RadioButtonGroup getRbDisabilityInsurance()
	{
		return (RadioButtonGroup)getChild(CHILD_RBDISABILITYINSURANCE);
	}


	/**
	 *
	 *
	 */
	public String endBtDetailsDisplay(ChildContentDisplayEvent event)
	{
		return event.getContent();
	}

	/**
	 *
	 *
	 */
	public HiddenField getHdInsureOnlyId()
	{
		return (HiddenField)getChild(CHILD_HDINSUREONLYID);
	}

  /**
   *
   *
   */
  public HiddenField getHdCopyId()
  {
    return (HiddenField)getChild(CHILD_HDCOPYID);
  }

	/**
	 *
	 *
	 */
	public HiddenField getHdInsureOnlyGenderId()
	{
		return (HiddenField)getChild(CHILD_HDINSUREONLYGENDERID);
	}

	/**
	 *
	 *
	 */
	public ComboBox getCbInsureOnlyGender()
	{
		return (ComboBox)getChild(CHILD_CBINSUREONLYGENDER);
	}

	/**
	 *
	 *
	 */
	static class CbInsureOnlyGenderOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbInsureOnlyGenderOptionList(){  }

		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                            "BORROWERGENDER", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}

	/**
	 *
	 *
	 */
	public ComboBox getCbInsureOnlySmoker()
	{
		return (ComboBox)getChild(CHILD_CBINSUREONLYSMOKER);
	}

	/**
	 *
	 *
	 */
	static class CbInsureOnlySmokerOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbInsureOnlySmokerOptionList(){  }

		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                            "BORROWERSMOKER", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}

  /**
   *
   *
   */
  public void handleBtDeleteApplicantRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    logger = SysLog.getSysLogger("PGUWRUWDPTV");

    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    handler.preHandlerProtocol((ViewBean) this.getParent());

    int tileIndx = ((TiledViewRequestInvocationEvent)(event)).getTileNumber();
    logger.debug("pgLDIR2@handleBtDeleteInsureOnlyAppl::TileIndex: " + tileIndx);

    handler.handleDelete(false,
                         13,
                          tileIndx,
                         "Repeated2/hdInsureOnlyApplicantId",
                         "InsureOnlyApplicantId",
                         "Repeated2/hdCopyId",
                         "InsureOnlyApplicant");

    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtDeleteApplicant()
  {
    return (Button)getChild(CHILD_BTDELETEAPPLICANT);
  }

  /**
   *
   *
   */
  public ComboBox getCbIOApplicantDOBMonth()
  {
    return(ComboBox)getChild(CHILD_CBIOAPPLICANTDOBMONTH);
  }

  /**
   *
   *
   */
  public TextField getTxIOApplicantDOBDay()
  {
    return(TextField)getChild(CHILD_TXIOAPPLICANTDOBDAY);
  }

  /**
   *
   *
   */
  public TextField getTxIOApplicantDOBYear()
  {
    return(TextField)getChild(CHILD_TXIOAPPLICANTDOBYEAR);
  }


  /**
   *
   *
   */
  public String endBtDeleteApplicantDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btDeleteDownPayment_onBeforeHtmlOutputEvent method
    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());

    boolean rc = handler.displayEditButton();
    handler.pageSaveState();

    if(rc == true)
      return event.getContent();
    else
      return "";

  }

	/**
	 *
	 *
	 */
	public doInsureOnlyApplicantModel getdoInsureOnlyApplicantModel()
	{
		if (doInsureOnlyApplicantInfo == null)
			doInsureOnlyApplicantInfo = (doInsureOnlyApplicantModel) getModel(doInsureOnlyApplicantModel.class);
		return doInsureOnlyApplicantInfo;
	}

	/**
	 *
	 *
	 */
	public void setdoInsureOnlyApplicantModel(doInsureOnlyApplicantModel model)
	{
			doInsureOnlyApplicantInfo = model;
	}

  public void validJSFromTiledView()
  {
          logger = SysLog.getSysLogger("LDITV2");

          pgLifeDisabilityInsuranceViewBean viewBean = (pgLifeDisabilityInsuranceViewBean) getParentViewBean();

          try
          {
            String insureOnlyValidPath = Sc.DVALID_LIFE_DIS_INSURE_ONLY_APPL_PROPS_FILE_NAME;

            logger.debug("LDITV2@validJSFromTiledView::ValidPath: " + insureOnlyValidPath);

            JSValidationRules insureOnlyValidObj = new JSValidationRules(insureOnlyValidPath, logger, viewBean);
            insureOnlyValidObj.attachJSValidationRules();
          }
          catch( Exception e )
          {
              logger.warning("DataValidJSException: " + e);
          }
  }

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;

  public static final String CHILD_TXINSUREONLYFIRSTNAME="txInsureOnlyApplicantFirstName";
  public static final String CHILD_TXINSUREONLYFIRSTNAME_RESET_VALUE="";

  public static final String CHILD_TXINSUREONLYLASTNAME="txInsureOnlyApplicantLastName";
  public static final String CHILD_TXINSUREONLYLASTNAME_RESET_VALUE="";

	public static final String CHILD_STPRIMARYINSUREONLYFLAG="stPrimaryInsureOnlyApplicantFlag";
	public static final String CHILD_STPRIMARYINSUREONLYFLAG_RESET_VALUE="";

  public static final String CHILD_STSMOKER="stInsureOnlyApplicantSmoker";
	public static final String CHILD_STSMOKER_RESET_VALUE="";

	public static final String CHILD_STINSUREONLYAGE="stInsureOnlyApplicantAge";
	public static final String CHILD_STINSUREONLYAGE_RESET_VALUE="";

	protected OptionList rbLifeInsuranceOptions=new OptionList(new String[]{},new String[]{});
	public static final String CHILD_RBLIFEINSURANCE="rbLifeInsurance";
	public static final String CHILD_RBLIFEINSURANCE_RESET_VALUE="";

	protected OptionList rbDisabilityInsuranceOptions=new OptionList(new String[]{},new String[]{});
	public static final String CHILD_RBDISABILITYINSURANCE="rbDisabilityInsurance";
	public static final String CHILD_RBDISABILITYINSURANCE_RESET_VALUE="";

	public static final String CHILD_HDINSUREONLYID="hdInsureOnlyApplicantId";
	public static final String CHILD_HDINSUREONLYID_RESET_VALUE="";

  public static final String CHILD_HDCOPYID="hdCopyId";
  public static final String CHILD_HDCOPYID_RESET_VALUE="";

	public static final String CHILD_HDINSUREONLYGENDERID="hdInsureOnlyApplicantGenderId";
	public static final String CHILD_HDINSUREONLYGENDERID_RESET_VALUE="";

  public static final String CHILD_CBINSUREONLYGENDER="cbInsureOnlyApplicantGender";
	public static final String CHILD_CBINSUREONLYGENDER_RESET_VALUE="";

  private CbInsureOnlyGenderOptionList cbInsureOnlyGenderOptions=new CbInsureOnlyGenderOptionList();

  public static final String CHILD_CBINSUREONLYSMOKER="cbInsureOnlyApplicantSmoker";
	public static final String CHILD_CBINSUREONLYSMOKER_RESET_VALUE="";

  private CbInsureOnlySmokerOptionList cbInsureOnlySmokerOptions=new CbInsureOnlySmokerOptionList();

  public static final String CHILD_BTDELETEAPPLICANT="btDeleteApplicant";
  public static final String CHILD_BTDELETEAPPLICANT_RESET_VALUE="";

  public static final String CHILD_CBIOAPPLICANTDOBMONTH = "cbIOApplicantDOBMonth";
  public static final String CHILD_CBIOAPPLICANTDOBMONTH_RESET_VALUE = "";
  private OptionList cbIOApplicantDOBMonthOptions = new OptionList(new String[]
    {}
    , new String[]
    {});

  public static final String CHILD_TXIOAPPLICANTDOBDAY = "txIOApplicantDOBDay";
  public static final String CHILD_TXIOAPPLICANTDOBDAY_RESET_VALUE = "";

  public static final String CHILD_TXIOAPPLICANTDOBYEAR = "txIOApplicantDOBYear";
  public static final String CHILD_TXIOAPPLICANTDOBYEAR_RESET_VALUE = "";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doInsureOnlyApplicantModel doInsureOnlyApplicantInfo=null;
	private LifeDisabilityInsuranceHandler handler=new LifeDisabilityInsuranceHandler();
  public SysLogger logger;

}

