package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doMainViewBridgeModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBridgeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBridgeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBridgePurpose();

	
	/**
	 * 
	 * 
	 */
	public void setDfBridgePurpose(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLender();

	
	/**
	 * 
	 * 
	 */
	public void setDfLender(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPaymentTerm();

	
	/**
	 * 
	 * 
	 */
	public void setDfPaymentTerm(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPostedInterestRate();

	
	/**
	 * 
	 * 
	 */
	public void setDfPostedInterestRate(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfRateDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfRateDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDiscount();

	
	/**
	 * 
	 * 
	 */
	public void setDfDiscount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfNetRate();

	
	/**
	 * 
	 * 
	 */
	public void setDfNetRate(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBridgeLoanAmount();

	
	/**
	 * 
	 * 
	 */
	public void setDfBridgeLoanAmount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfActualPaymentTerm();

	
	/**
	 * 
	 * 
	 */
	public void setDfActualPaymentTerm(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfMaturityDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfMaturityDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBridgePremium();

	
	/**
	 * 
	 * 
	 */
	public void setDfBridgePremium(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBridgePricingProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBridgePricingProfileId(java.math.BigDecimal value);
	
	  public java.math.BigDecimal getDfInstitutionId();
	  public void setDfInstitutionId(java.math.BigDecimal id);
	  

	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBRIDGEID="dfBridgeId";
	public static final String FIELD_DFBRIDGEPURPOSE="dfBridgePurpose";
	public static final String FIELD_DFLENDER="dfLender";
	public static final String FIELD_DFPAYMENTTERM="dfPaymentTerm";
	public static final String FIELD_DFPOSTEDINTERESTRATE="dfPostedInterestRate";
	public static final String FIELD_DFRATEDATE="dfRateDate";
	public static final String FIELD_DFDISCOUNT="dfDiscount";
	public static final String FIELD_DFNETRATE="dfNetRate";
	public static final String FIELD_DFBRIDGELOANAMOUNT="dfBridgeLoanAmount";
	public static final String FIELD_DFACTUALPAYMENTTERM="dfActualPaymentTerm";
	public static final String FIELD_DFMATURITYDATE="dfMaturityDate";
	public static final String FIELD_DFBRIDGEPREMIUM="dfBridgePremium";
	public static final String FIELD_DFBRIDGEPRICINGPROFILEID="dfBridgePricingProfileId";
	
	  public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";

	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

