package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

import com.basis100.picklist.BXResources;
import com.filogix.express.web.jato.ExpressViewBeanBase; 
/**
 *
 *
 *
 */
public class pgPortfolioStatsViewBean extends ExpressViewBeanBase

{
	/**
	 *
	 *
	 */
	public pgPortfolioStatsViewBean()
	{
		super(PAGE_NAME);
		setDefaultDisplayURL(DEFAULT_DISPLAY_URL);

		registerChildren();

		initialize();

	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		View superReturn = super.createChild(name);  
		if (superReturn != null) {  
		return superReturn;  
		} else if (name.equals(CHILD_TBDEALID)) 
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBDEALID,
				CHILD_TBDEALID,
				CHILD_TBDEALID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPAGENAMES))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES_RESET_VALUE,
				null);

      //--Release2.1--//
      //Modified to set NonSelected Label from CHILD_CBPAGENAMES_NONSELECTED_LABEL
      //--> By John 08Nov2002
      child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
      //==========================================================================

			child.setOptions(cbPageNamesOptions);
			return child;
		}
		else
		if (name.equals(CHILD_BTPROCEED))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPROCEED,
				CHILD_BTPROCEED,
				CHILD_BTPROCEED_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF1))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF1,
				CHILD_HREF1,
				CHILD_HREF1_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF2))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF2,
				CHILD_HREF2,
				CHILD_HREF2_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF3))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF3,
				CHILD_HREF3,
				CHILD_HREF3_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF4))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF4,
				CHILD_HREF4,
				CHILD_HREF4_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF5))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF5,
				CHILD_HREF5,
				CHILD_HREF5_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF6))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF6,
				CHILD_HREF6,
				CHILD_HREF6_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF7))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF7,
				CHILD_HREF7,
				CHILD_HREF7_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF8))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF8,
				CHILD_HREF8,
				CHILD_HREF8_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF9))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF9,
				CHILD_HREF9,
				CHILD_HREF9_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF10))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF10,
				CHILD_HREF10,
				CHILD_HREF10_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCOMPANYNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTODAYDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUSERNAMETITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTSUBMIT))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSUBMIT,
				CHILD_BTSUBMIT,
				CHILD_BTSUBMIT_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTWORKQUEUELINK))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_CHANGEPASSWORDHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLHISTORY))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOONOTES))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLSEARCH))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLLOG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STERRORFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_DETECTALERTTASKS))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTCANCEL))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTCANCEL,
				CHILD_BTCANCEL,
				CHILD_BTCANCEL_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STDATEMASK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STDATEMASK,
				CHILD_STDATEMASK,
				CHILD_STDATEMASK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_SESSIONUSERID))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASOK,
				CHILD_STPMHASOK,
				CHILD_STPMHASOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMTITLE,
				CHILD_STPMTITLE,
				CHILD_STPMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMONOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMONOK,
				CHILD_STPMONOK,
				CHILD_STPMONOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGS,
				CHILD_STPMMSGS,
				CHILD_STPMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMTITLE,
				CHILD_STAMTITLE,
				CHILD_STAMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGS,
				CHILD_STAMMSGS,
				CHILD_STAMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMDIALOGMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMBUTTONSHTML))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTOK))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTOK,
				CHILD_BTOK,
				CHILD_BTOK_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_CBINSTITUTIONS))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBINSTITUTIONS,
				CHILD_CBINSTITUTIONS,
				CHILD_CBINSTITUTIONS_RESET_VALUE,
				null);

      //--Release2.1--//
      //Modified to set NonSelected Label from CHILD_CBINSTITUTIONS_NONSELECTED_LABEL
      //--> By John 08Nov2002
      child.setLabelForNoneSelected(CHILD_CBINSTITUTIONS_NONSELECTED_LABEL);
      //==========================================================================

			child.setOptions(cbInstitutionsOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBREGIONS))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBREGIONS,
				CHILD_CBREGIONS,
				CHILD_CBREGIONS_RESET_VALUE,
				null);

      //--Release2.1--//
      //Modified to set NonSelected Label from CHILD_CBREGIONS_NONSELECTED_LABEL
      //--> By John 08Nov2002
      child.setLabelForNoneSelected(CHILD_CBREGIONS_NONSELECTED_LABEL);
      //==========================================================================

			child.setOptions(cbRegionsOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBBRANCHES))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBBRANCHES,
				CHILD_CBBRANCHES,
				CHILD_CBBRANCHES_RESET_VALUE,
				null);

      //--Release2.1--//
      //Modified to set NonSelected Label from CHILD_CBBRANCHES_NONSELECTED_LABEL
      //--> By John 08Nov2002
      child.setLabelForNoneSelected(CHILD_CBBRANCHES_NONSELECTED_LABEL);
      //==========================================================================

			child.setOptions(cbBranchesOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBGROUPS))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBGROUPS,
				CHILD_CBGROUPS,
				CHILD_CBGROUPS_RESET_VALUE,
				null);

      //--Release2.1--//
      //Modified to set NonSelected Label from CHILD_CBGROUPS_NONSELECTED_LABEL
      //--> By John 08Nov2002
      child.setLabelForNoneSelected(CHILD_CBGROUPS_NONSELECTED_LABEL);
      //==========================================================================

			child.setOptions(cbGroupsOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBUSERS))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBUSERS,
				CHILD_CBUSERS,
				CHILD_CBUSERS_RESET_VALUE,
				null);

      //--Release2.1--//
      //Modified to set NonSelected Label from CHILD_CBUSERS_NONSELECTED_LABEL
      //--> By John 08Nov2002
      child.setLabelForNoneSelected(CHILD_CBUSERS_NONSELECTED_LABEL);
      //==========================================================================

			child.setOptions(cbUsersOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBSOURCEFIRM))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBSOURCEFIRM,
				CHILD_CBSOURCEFIRM,
				CHILD_CBSOURCEFIRM_RESET_VALUE,
				null);

      //--Release2.1--//
      //Modified to set NonSelected Label from CHILD_CBSOURCEFIRM_NONSELECTED_LABEL
      //--> By John 08Nov2002
      child.setLabelForNoneSelected(CHILD_CBSOURCEFIRM_NONSELECTED_LABEL);
      //==========================================================================

			child.setOptions(cbSourceFirmOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TBDAYFROM))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBDAYFROM,
				CHILD_TBDAYFROM,
				CHILD_TBDAYFROM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBDAYTO))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBDAYTO,
				CHILD_TBDAYTO,
				CHILD_TBDAYTO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBYEARFROM))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBYEARFROM,
				CHILD_TBYEARFROM,
				CHILD_TBYEARFROM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBYEARTO))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBYEARTO,
				CHILD_TBYEARTO,
				CHILD_TBYEARTO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBMONTHFROM))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBMONTHFROM,
				CHILD_CBMONTHFROM,
				CHILD_CBMONTHFROM_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbMonthFromOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBMONTHTO))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBMONTHTO,
				CHILD_CBMONTHTO,
				CHILD_CBMONTHTO_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbMonthToOptions);
			return child;
		}
    //// Hidden button to trigger the ActiveMessage 'fake' submit button
    //// via the new BX ActMessageCommand class
    else
    if (name.equals(CHILD_BTACTMSG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTACTMSG,
				CHILD_BTACTMSG,
				CHILD_BTACTMSG_RESET_VALUE,
				new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));
				return child;
    }
    //--Release2.1--//
    //// New link to toggle language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new session
    //// language id throughout the all modulus.
		else
		if (name.equals(CHILD_TOGGLELANGUAGEHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF_RESET_VALUE,
				null);
				return child;
    }
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		super.resetChildren(); 
		getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
		getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
		getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
		getHref1().setValue(CHILD_HREF1_RESET_VALUE);
		getHref2().setValue(CHILD_HREF2_RESET_VALUE);
		getHref3().setValue(CHILD_HREF3_RESET_VALUE);
		getHref4().setValue(CHILD_HREF4_RESET_VALUE);
		getHref5().setValue(CHILD_HREF5_RESET_VALUE);
		getHref6().setValue(CHILD_HREF6_RESET_VALUE);
		getHref7().setValue(CHILD_HREF7_RESET_VALUE);
		getHref8().setValue(CHILD_HREF8_RESET_VALUE);
		getHref9().setValue(CHILD_HREF9_RESET_VALUE);
		getHref10().setValue(CHILD_HREF10_RESET_VALUE);
		getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
		getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
		getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
		getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
		getBtSubmit().setValue(CHILD_BTSUBMIT_RESET_VALUE);
		getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
		getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
		getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
		getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
		getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
		getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
		getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
		getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
		getBtCancel().setValue(CHILD_BTCANCEL_RESET_VALUE);
		getStDateMask().setValue(CHILD_STDATEMASK_RESET_VALUE);
		getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
		getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
		getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
		getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
		getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
		getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
		getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
		getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
		getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
		getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
		getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
		getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
		getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
		getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
		getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
		getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
		getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
		getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
		getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
		getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
		getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
		getBtOk().setValue(CHILD_BTOK_RESET_VALUE);
		getCbInstitutions().setValue(CHILD_CBINSTITUTIONS_RESET_VALUE);
		getCbRegions().setValue(CHILD_CBREGIONS_RESET_VALUE);
		getCbBranches().setValue(CHILD_CBBRANCHES_RESET_VALUE);
		getCbGroups().setValue(CHILD_CBGROUPS_RESET_VALUE);
		getCbUsers().setValue(CHILD_CBUSERS_RESET_VALUE);
		getCbSourceFirm().setValue(CHILD_CBSOURCEFIRM_RESET_VALUE);
		getTbDayFrom().setValue(CHILD_TBDAYFROM_RESET_VALUE);
		getTbDayTo().setValue(CHILD_TBDAYTO_RESET_VALUE);
		getTbYearFrom().setValue(CHILD_TBYEARFROM_RESET_VALUE);
		getTbYearTo().setValue(CHILD_TBYEARTO_RESET_VALUE);
		getCbMonthFrom().setValue(CHILD_CBMONTHFROM_RESET_VALUE);
		getCbMonthTo().setValue(CHILD_CBMONTHTO_RESET_VALUE);
    //// new hidden button to activate 'fake' submit button from the ActiveMessage class.
    getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
		getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		super.registerChildren();
		registerChild(CHILD_TBDEALID,TextField.class);
		registerChild(CHILD_CBPAGENAMES,ComboBox.class);
		registerChild(CHILD_BTPROCEED,Button.class);
		registerChild(CHILD_HREF1,HREF.class);
		registerChild(CHILD_HREF2,HREF.class);
		registerChild(CHILD_HREF3,HREF.class);
		registerChild(CHILD_HREF4,HREF.class);
		registerChild(CHILD_HREF5,HREF.class);
		registerChild(CHILD_HREF6,HREF.class);
		registerChild(CHILD_HREF7,HREF.class);
		registerChild(CHILD_HREF8,HREF.class);
		registerChild(CHILD_HREF9,HREF.class);
		registerChild(CHILD_HREF10,HREF.class);
		registerChild(CHILD_STPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STCOMPANYNAME,StaticTextField.class);
		registerChild(CHILD_STTODAYDATE,StaticTextField.class);
		registerChild(CHILD_STUSERNAMETITLE,StaticTextField.class);
		registerChild(CHILD_BTSUBMIT,Button.class);
		registerChild(CHILD_BTWORKQUEUELINK,Button.class);
		registerChild(CHILD_CHANGEPASSWORDHREF,HREF.class);
		registerChild(CHILD_BTTOOLHISTORY,Button.class);
		registerChild(CHILD_BTTOONOTES,Button.class);
		registerChild(CHILD_BTTOOLSEARCH,Button.class);
		registerChild(CHILD_BTTOOLLOG,Button.class);
		registerChild(CHILD_STERRORFLAG,StaticTextField.class);
		registerChild(CHILD_DETECTALERTTASKS,HiddenField.class);
		registerChild(CHILD_BTCANCEL,Button.class);
		registerChild(CHILD_STDATEMASK,StaticTextField.class);
		registerChild(CHILD_SESSIONUSERID,HiddenField.class);
		registerChild(CHILD_STPMGENERATE,StaticTextField.class);
		registerChild(CHILD_STPMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STPMHASINFO,StaticTextField.class);
		registerChild(CHILD_STPMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STPMHASOK,StaticTextField.class);
		registerChild(CHILD_STPMTITLE,StaticTextField.class);
		registerChild(CHILD_STPMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STPMONOK,StaticTextField.class);
		registerChild(CHILD_STPMMSGS,StaticTextField.class);
		registerChild(CHILD_STPMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMGENERATE,StaticTextField.class);
		registerChild(CHILD_STAMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STAMHASINFO,StaticTextField.class);
		registerChild(CHILD_STAMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STAMTITLE,StaticTextField.class);
		registerChild(CHILD_STAMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STAMMSGS,StaticTextField.class);
		registerChild(CHILD_STAMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMDIALOGMSG,StaticTextField.class);
		registerChild(CHILD_STAMBUTTONSHTML,StaticTextField.class);
		registerChild(CHILD_BTOK,Button.class);
		registerChild(CHILD_CBINSTITUTIONS,ComboBox.class);
		registerChild(CHILD_CBREGIONS,ComboBox.class);
		registerChild(CHILD_CBBRANCHES,ComboBox.class);
		registerChild(CHILD_CBGROUPS,ComboBox.class);
		registerChild(CHILD_CBUSERS,ComboBox.class);
		registerChild(CHILD_CBSOURCEFIRM,ComboBox.class);
		registerChild(CHILD_TBDAYFROM,TextField.class);
		registerChild(CHILD_TBDAYTO,TextField.class);
		registerChild(CHILD_TBYEARFROM,TextField.class);
		registerChild(CHILD_TBYEARTO,TextField.class);
		registerChild(CHILD_CBMONTHFROM,ComboBox.class);
		registerChild(CHILD_CBMONTHTO,ComboBox.class);
    //// New hidden button implementation.
    registerChild(CHILD_BTACTMSG,Button.class);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
		registerChild(CHILD_TOGGLELANGUAGEHREF,HREF.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
    PortfolioStatsHandler handler = (PortfolioStatsHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By John 08Nov2002
    //Setup Options for cbPageNames
    //cbPageNamesOptions..populate(getRequestContext());
	int langId = handler.getTheSessionState().getLanguageId();
    cbPageNamesOptions.populate(getRequestContext(), langId);

    //Setup NoneSelected Label for cbPageNames
    CHILD_CBPAGENAMES_NONSELECTED_LABEL =
    BXResources.getGenericMsg("CBPAGENAMES_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    //Check if the cbPageNames is already created
    if(getCbPageNames() != null)
      getCbPageNames().setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);

    //Setup Options for cbInstitutions
    cbInstitutionsOptions.populate(getRequestContext());

    //Setup NoneSelected Label for cbInstitutions
    CHILD_CBINSTITUTIONS_NONSELECTED_LABEL =
    BXResources.getGenericMsg("CBINSTITUTIONS_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    //Check if the cbPageNames is already created
    if(getCbInstitutions() != null)
      getCbInstitutions().setLabelForNoneSelected(CHILD_CBINSTITUTIONS_NONSELECTED_LABEL);

    //Setup Options for cbRegions
    cbRegionsOptions.populate(getRequestContext());

    //Setup NoneSelected Label for cbRegions
    CHILD_CBREGIONS_NONSELECTED_LABEL =
    BXResources.getGenericMsg("CBREGIONS_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    //Check if the cbRegions is already created
    if(getCbRegions() != null)
      getCbRegions().setLabelForNoneSelected(CHILD_CBREGIONS_NONSELECTED_LABEL);

    //Setup Options for cbBranches
    cbBranchesOptions.populate(getRequestContext());

    //Setup NoneSelected Label for cbBranches
    CHILD_CBBRANCHES_NONSELECTED_LABEL =
    BXResources.getGenericMsg("CBBRANCHES_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    //Check if the cbBranches is already created
    if(getCbBranches() != null)
      getCbBranches().setLabelForNoneSelected(CHILD_CBBRANCHES_NONSELECTED_LABEL);

    //Setup Options for cbGroups
    cbGroupsOptions.populate(getRequestContext());

    //Setup NoneSelected Label for cbGroups
    CHILD_CBGROUPS_NONSELECTED_LABEL =
    BXResources.getGenericMsg("CBGROUPS_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    //Check if the cbGroups is already created
    if(getCbGroups() != null)
      getCbGroups().setLabelForNoneSelected(CHILD_CBGROUPS_NONSELECTED_LABEL);

    //Setup Options for cbUsers
    cbUsersOptions.populate(getRequestContext());

    //Setup NoneSelected Label for cbUsers
    CHILD_CBUSERS_NONSELECTED_LABEL =
    BXResources.getGenericMsg("CBUSERS_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    //Check if the cbUsers is already created
    if(getCbUsers() != null)
      getCbUsers().setLabelForNoneSelected(CHILD_CBUSERS_NONSELECTED_LABEL);

    //Setup Options for cbSourceFirm
    cbSourceFirmOptions.populate(getRequestContext());

    //Setup NoneSelected Label for cbSourceFirm
    CHILD_CBSOURCEFIRM_NONSELECTED_LABEL =
    BXResources.getGenericMsg("CBSOURCEFIRM_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    //Check if the cbSourceFirm is already created
    if(getCbSourceFirm() != null)
      getCbSourceFirm().setLabelForNoneSelected(CHILD_CBSOURCEFIRM_NONSELECTED_LABEL);

    //Setup Options for cbMonthFrom
    cbMonthFromOptions.populate(getRequestContext());

    //Setup Options for cbMonthTo
    cbMonthToOptions.populate(getRequestContext());

    //=====================================

    handler.pageSaveState();
    super.beginDisplay(event);
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		handler.setupBeforePageGeneration();

		handler.pageSaveState();

		// This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		handler.populatePageDisplayFields();

		handler.pageSaveState();

		// This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics this_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	/**
	 *
	 *
	 */
	public TextField getTbDealId()
	{
		return (TextField)getChild(CHILD_TBDEALID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPageNames()
	{
		return (ComboBox)getChild(CHILD_CBPAGENAMES);
	}

	/**
	 *
	 *
	 */
  //--DJ_Ticket661--start--//
  //Modified to sort by PageLabel based on the selected language
  static class CbPageNamesOptionList extends GotoOptionList
 {
    /**
     *
     *
     */
    CbPageNamesOptionList() {

    }


    public void populate(RequestContext rc) {
      Connection c = null;
      try {
        clear();
        SelectQueryModel m = null;

        SelectQueryExecutionContext eContext = new
          SelectQueryExecutionContext(
          (Connection)null,

          DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,

          DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null) {
          m = new doPageNameLabelModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else {
          m = (SelectQueryModel)rc.getModelManager().getModel(doPageNameLabelModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        // Sort the results from DataObject
        TreeMap sorted = new TreeMap();
        while (m.next()) {
          Object dfPageLabel =
            m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
          String label = (dfPageLabel == null ? "" : dfPageLabel.toString());
          Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
          String value = (dfPageId == null ? "" : dfPageId.toString());
          String[] theVal = new String[2];
          theVal[0] = label;
          theVal[1] = value;
          sorted.put(label, theVal);
        }

        // Set the sorted list to the optionlist
        Iterator theList = sorted.values().iterator();
        String[] theVal = new String[2];
        while (theList.hasNext()) {
          theVal = (String[]) (theList.next());
          add(theVal[0], theVal[1]);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      finally {
        try {
          if (c != null)
            c.close();
        }
        catch (SQLException ex) {
              // ignore
        }
      }
    }
 }
 //--DJ_Ticket661--end--//

	/**
	 *
	 *
	 */
	public void handleBtProceedRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleGoPage();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtProceed()
	{
		return (Button)getChild(CHILD_BTPROCEED);
	}


	/**
	 *
	 *
	 */
	public void handleHref1Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(0);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref1()
	{
		return (HREF)getChild(CHILD_HREF1);
	}


	/**
	 *
	 *
	 */
	public void handleHref2Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(1);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref2()
	{
		return (HREF)getChild(CHILD_HREF2);
	}


	/**
	 *
	 *
	 */
	public void handleHref3Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(2);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref3()
	{
		return (HREF)getChild(CHILD_HREF3);
	}


	/**
	 *
	 *
	 */
	public void handleHref4Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(3);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref4()
	{
		return (HREF)getChild(CHILD_HREF4);
	}


	/**
	 *
	 *
	 */
	public void handleHref5Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(4);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref5()
	{
		return (HREF)getChild(CHILD_HREF5);
	}


	/**
	 *
	 *
	 */
	public void handleHref6Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(5);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref6()
	{
		return (HREF)getChild(CHILD_HREF6);
	}


	/**
	 *
	 *
	 */
	public void handleHref7Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(6);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref7()
	{
		return (HREF)getChild(CHILD_HREF7);
	}


	/**
	 *
	 *
	 */
	public void handleHref8Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(7);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref8()
	{
		return (HREF)getChild(CHILD_HREF8);
	}


	/**
	 *
	 *
	 */
	public void handleHref9Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(8);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref9()
	{
		return (HREF)getChild(CHILD_HREF9);
	}


	/**
	 *
	 *
	 */
	public void handleHref10Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(9);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref10()
	{
		return (HREF)getChild(CHILD_HREF10);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCompanyName()
	{
		return (StaticTextField)getChild(CHILD_STCOMPANYNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTodayDate()
	{
		return (StaticTextField)getChild(CHILD_STTODAYDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUserNameTitle()
	{
		return (StaticTextField)getChild(CHILD_STUSERNAMETITLE);
	}


	/**
	 *
	 *
	 */
	public void handleBtSubmitRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleSubmit();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtSubmit()
	{
		return (Button)getChild(CHILD_BTSUBMIT);
	}


	/**
	 *
	 *
	 */
	public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayWorkQueue();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtWorkQueueLink()
	{
		return (Button)getChild(CHILD_BTWORKQUEUELINK);
	}


	/**
	 *
	 *
	 */
	public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleChangePassword();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getChangePasswordHref()
	{
		return (HREF)getChild(CHILD_CHANGEPASSWORDHREF);
	}

  //--Release2.1--start//
  //// Two new methods providing the business logic for the new link to toggle language.
  //// When touched this link should reload the page in opposite language (french versus english)
  //// and set this new session value.

	/**
	 *
	 *
	 */
	public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this, true);

		handler.handleToggleLanguage();

		handler.postHandlerProtocol();
	}

	/**
	 *
	 *
	 */
	public HREF getToggleLanguageHref()
	{
		return (HREF)getChild(CHILD_TOGGLELANGUAGEHREF);
	}

  //--Release2.1--end//
	/**
	 *
	 *
	 */
	public void handleBtToolHistoryRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayDealHistory();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtToolHistory()
	{
		return (Button)getChild(CHILD_BTTOOLHISTORY);
	}


	/**
	 *
	 *
	 */
	public void handleBtTooNotesRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayDealNotes();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtTooNotes()
	{
		return (Button)getChild(CHILD_BTTOONOTES);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolSearchRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDealSearch();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtToolSearch()
	{
		return (Button)getChild(CHILD_BTTOOLSEARCH);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolLogRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleSignOff();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtToolLog()
	{
		return (Button)getChild(CHILD_BTTOOLLOG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStErrorFlag()
	{
		return (StaticTextField)getChild(CHILD_STERRORFLAG);
	}


	/**
	 *
	 *
	 */
	public HiddenField getDetectAlertTasks()
	{
		return (HiddenField)getChild(CHILD_DETECTALERTTASKS);
	}


	/**
	 *
	 *
	 */
	public void handleBtCancelRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleCancelStandard();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtCancel()
	{
		return (Button)getChild(CHILD_BTCANCEL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDateMask()
	{
		return (StaticTextField)getChild(CHILD_STDATEMASK);
	}


	/**
	 *
	 *
	 */
	public HiddenField getSessionUserId()
	{
		return (HiddenField)getChild(CHILD_SESSIONUSERID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STPMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STPMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasOk()
	{
		return (StaticTextField)getChild(CHILD_STPMHASOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STPMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmOnOk()
	{
		return (StaticTextField)getChild(CHILD_STPMONOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STAMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STAMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmDialogMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMDIALOGMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmButtonsHtml()
	{
		return (StaticTextField)getChild(CHILD_STAMBUTTONSHTML);
	}


	/**
	 *
	 *
	 */
	public void handleBtOkRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleCancelStandard();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtOk()
	{
		return (Button)getChild(CHILD_BTOK);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbInstitutions()
	{
		return (ComboBox)getChild(CHILD_CBINSTITUTIONS);
	}

	/**
	 *
	 *
	 */
	static class CbInstitutionsOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbInstitutionsOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
			Connection c = null;
      Statement query = null;
			try {
				clear();
				if(rc == null) {
					c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
				}
				else {
					c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
				}

				query = c.createStatement();
				ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);

				while(results.next()) {
					Object INSTITUTIONPROFILEID = results.getObject("INSTITUTIONPROFILEID");
					Object INSTITUTIONPROFILE_INSTITUTIONNAME = results.getObject("INSTITUTIONNAME");

					String label = (INSTITUTIONPROFILE_INSTITUTIONNAME == null?"":INSTITUTIONPROFILE_INSTITUTIONNAME.toString());

					String value = (INSTITUTIONPROFILEID == null?"":INSTITUTIONPROFILEID.toString());

					add(label, value);
				}
        results.close();
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			finally {
				try {
					if(c != null) c.close();
          if(query != null) query.close();
				}
				catch (SQLException  ex) {
					// ignore
				}
			}
		}
		private static String FETCH_DATA_STATEMENT="SELECT INSTITUTIONPROFILE.INSTITUTIONNAME, INSTITUTIONPROFILE.INSTITUTIONPROFILEID FROM INSTITUTIONPROFILE";

	}



	/**
	 *
	 *
	 */
	public ComboBox getCbRegions()
	{
		return (ComboBox)getChild(CHILD_CBREGIONS);
	}

	/**
	 *
	 *
	 */
	static class CbRegionsOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbRegionsOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
            (SessionStateModelImpl)rc.getModelManager().getModel(
                            SessionStateModel.class,
                            defaultInstanceStateName,
                            true);

        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(
                theSessionState.getDealInstitutionId(),"REGIONPROFILE", languageId);

        Iterator l = c.iterator();
        String[] theVal = new String[2];

        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex)
      {
				ex.printStackTrace();
			}
		}
	}



	/**
	 *
	 *
	 */
	public ComboBox getCbBranches()
	{
		return (ComboBox)getChild(CHILD_CBBRANCHES);
	}

	/**
	 *
	 *
	 */
	static class CbBranchesOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbBranchesOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
			Connection c = null;
      Statement query = null;
			try {
				clear();
				if(rc == null) {
					c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
				}
				else {
					c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
				}

				query = c.createStatement();
				ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);

				while(results.next()) {
					Object BRANCHPROFILE_BRANCHNAME = results.getObject("BRANCHNAME");
					Object BRANCHPROFILEID = results.getObject("BRANCHPROFILEID");

					String label = (BRANCHPROFILE_BRANCHNAME == null?"":BRANCHPROFILE_BRANCHNAME.toString());

					String value = (BRANCHPROFILEID == null?"":BRANCHPROFILEID.toString());

					add(label, value);
				}
        results.close();
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			finally {
				try {
					if(c != null) c.close();
          if(query != null) query.close();
				}
				catch (SQLException  ex) {
					// ignore
				}
			}
		}
		private static String FETCH_DATA_STATEMENT="SELECT BRANCHPROFILE.BRANCHNAME, BRANCHPROFILE.BRANCHPROFILEID FROM BRANCHPROFILE";

	}



	/**
	 *
	 *
	 */
	public ComboBox getCbGroups()
	{
		return (ComboBox)getChild(CHILD_CBGROUPS);
	}

	/**
	 *
	 *
	 */
	static class CbGroupsOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbGroupsOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
            (SessionStateModelImpl)rc.getModelManager().getModel(
                            SessionStateModel.class,
                            defaultInstanceStateName,
                            true);

        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(
                theSessionState.getDealInstitutionId(),"GROUPPROFILE", languageId);

        Iterator l = c.iterator();
        String[] theVal = new String[2];

        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex)
      {
				ex.printStackTrace();
			}
		}
	}



	/**
	 *
	 *
	 */
	public ComboBox getCbUsers()
	{
		return (ComboBox)getChild(CHILD_CBUSERS);
	}

	/**
	 *
	 *
	 */
	static class CbUsersOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbUsersOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
			Connection c = null;
      Statement query = null;
			try {
				clear();
				if(rc == null) {
					c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
				}
				else {
					c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
				}

				query = c.createStatement();
				ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);

				while(results.next()) {
					Object USERPROFILEID = results.getObject("USERPROFILEID");
					Object USERPROFILE_USERLOGIN = results.getObject("USERLOGIN");

					String label = (USERPROFILE_USERLOGIN == null?"":USERPROFILE_USERLOGIN.toString());

					String value = (USERPROFILEID == null?"":USERPROFILEID.toString());

					add(label, value);
				}
        results.close();
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			finally {
				try {
					if(c != null) c.close();
          if(query != null) query.close();
				}
				catch (SQLException  ex) {
					// ignore
				}
			}
		}
		private static String FETCH_DATA_STATEMENT="SELECT USERPROFILE.USERLOGIN, USERPROFILE.USERPROFILEID FROM USERPROFILE";

	}



	/**
	 *
	 *
	 */
	public ComboBox getCbSourceFirm()
	{
		return (ComboBox)getChild(CHILD_CBSOURCEFIRM);
	}

	/**
	 *
	 *
	 */
	static class CbSourceFirmOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbSourceFirmOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
			Connection c = null;
      Statement query = null;
			try {
				clear();
				if(rc == null) {
					c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
				}
				else {
					c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
				}

				query = c.createStatement();
				ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);

				while(results.next()) {
					Object SOURCEFIRMPROFILEID = results.getObject("SOURCEFIRMPROFILEID");
					Object SOURCEFIRMPROFILE_SOURCEFIRMNAME = results.getObject("SOURCEFIRMNAME");

					String label = (SOURCEFIRMPROFILE_SOURCEFIRMNAME == null?"":SOURCEFIRMPROFILE_SOURCEFIRMNAME.toString());

					String value = (SOURCEFIRMPROFILEID == null?"":SOURCEFIRMPROFILEID.toString());

					add(label, value);
				}
        results.close();
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			finally {
				try {
					if(c != null) c.close();
          if(query != null) query.close();
				}
				catch (SQLException  ex) {
					// ignore
				}
			}
		}
		private static String FETCH_DATA_STATEMENT="SELECT SOURCEFIRMPROFILE.SOURCEFIRMNAME, SOURCEFIRMPROFILE.SOURCEFIRMPROFILEID FROM SOURCEFIRMPROFILE";

	}



	/**
	 *
	 *
	 */
	public TextField getTbDayFrom()
	{
		return (TextField)getChild(CHILD_TBDAYFROM);
	}


	/**
	 *
	 *
	 */
	public TextField getTbDayTo()
	{
		return (TextField)getChild(CHILD_TBDAYTO);
	}


	/**
	 *
	 *
	 */
	public TextField getTbYearFrom()
	{
		return (TextField)getChild(CHILD_TBYEARFROM);
	}


	/**
	 *
	 *
	 */
	public TextField getTbYearTo()
	{
		return (TextField)getChild(CHILD_TBYEARTO);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbMonthFrom()
	{
		return (ComboBox)getChild(CHILD_CBMONTHFROM);
	}

	/**
	 *
	 *
	 */
	static class CbMonthFromOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbMonthFromOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
            (SessionStateModelImpl)rc.getModelManager().getModel(
                            SessionStateModel.class,
                            defaultInstanceStateName,
                            true);

        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(
                theSessionState.getDealInstitutionId(),"MONTHS", languageId);

        Iterator l = c.iterator();
        String[] theVal = new String[2];

        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex)
      {
				ex.printStackTrace();
			}
		}
	}



	/**
	 *
	 *
	 */
	public ComboBox getCbMonthTo()
	{
		return (ComboBox)getChild(CHILD_CBMONTHTO);
	}

	/**
	 *
	 *
	 */
	static class CbMonthToOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbMonthToOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
            (SessionStateModelImpl)rc.getModelManager().getModel(
                            SessionStateModel.class,
                            defaultInstanceStateName,
                            true);

        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(
                theSessionState.getDealInstitutionId(),"MONTHS", languageId);

        Iterator l = c.iterator();
        String[] theVal = new String[2];

        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex)
      {
				ex.printStackTrace();
			}
		}
	}

  //// New hidden button for the ActiveMessage 'fake' submit button.
  public Button getBtActMsg()
	{
		return (Button)getChild(CHILD_BTACTMSG);
	}

  //// Populate previous links new set of methods to populate Href display Strings.
  public String endHref1Display(ChildContentDisplayEvent event)
	{
    PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 0);
		handler.pageSaveState();

    return rc;
  }
  public String endHref2Display(ChildContentDisplayEvent event)
	{
    PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 1);
		handler.pageSaveState();

    return rc;
  }
  public String endHref3Display(ChildContentDisplayEvent event)
	{
    PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 2);
		handler.pageSaveState();

    return rc;
  }
  public String endHref4Display(ChildContentDisplayEvent event)
	{
    PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 3);
		handler.pageSaveState();

    return rc;
  }
  public String endHref5Display(ChildContentDisplayEvent event)
	{
    PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 4);
		handler.pageSaveState();

    return rc;
  }
  public String endHref6Display(ChildContentDisplayEvent event)
	{
    PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 5);
		handler.pageSaveState();

    return rc;
  }
  public String endHref7Display(ChildContentDisplayEvent event)
	{
    PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 6);
		handler.pageSaveState();

    return rc;
  }
  public String endHref8Display(ChildContentDisplayEvent event)
	{
    PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 7);
		handler.pageSaveState();

    return rc;
  }
  public String endHref9Display(ChildContentDisplayEvent event)
	{
    PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 8);
		handler.pageSaveState();

    return rc;
  }
  public String endHref10Display(ChildContentDisplayEvent event)
	{
    PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 9);
		handler.pageSaveState();

    return rc;
  }

  //// This method overrides the getDefaultURL() framework JATO method and it is located
  //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
  //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
  //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
  //// The full method is still in PHC base class. It should care the
  //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
  public String getDisplayURL()
  {
       PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();
       handler.pageGetState(this);

       String url=getDefaultDisplayURL();

       int languageId = handler.theSessionState.getLanguageId();

       //// Call the language specific URL (business delegation done in the BXResource).
       if(url != null && !url.trim().equals("") && !url.trim().equals("/")) {
          url = BXResources.getBXUrl(url, languageId);
       }
       else {
          url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
       }

      return url;
  }

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////


	public void handleActMessageOK(String[] args)
	{
		PortfolioStatsHandler handler =(PortfolioStatsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleActMessageOK(args);

		handler.postHandlerProtocol();
	}

	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String PAGE_NAME="pgPortfolioStats";
  //// It is a variable now (see explanation in the getDisplayURL() method above.
	////public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgPortfolioStats.jsp";
	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_TBDEALID="tbDealId";
	public static final String CHILD_TBDEALID_RESET_VALUE="";
	public static final String CHILD_CBPAGENAMES="cbPageNames";
	public static final String CHILD_CBPAGENAMES_RESET_VALUE="";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 08Nov2002
  //private static CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  private CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  //Added the Variable for NonSelected Label
  private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";
  //==================================================================

	public static final String CHILD_BTPROCEED="btProceed";
	public static final String CHILD_BTPROCEED_RESET_VALUE="Proceed";
	public static final String CHILD_HREF1="Href1";
	public static final String CHILD_HREF1_RESET_VALUE="";
	public static final String CHILD_HREF2="Href2";
	public static final String CHILD_HREF2_RESET_VALUE="";
	public static final String CHILD_HREF3="Href3";
	public static final String CHILD_HREF3_RESET_VALUE="";
	public static final String CHILD_HREF4="Href4";
	public static final String CHILD_HREF4_RESET_VALUE="";
	public static final String CHILD_HREF5="Href5";
	public static final String CHILD_HREF5_RESET_VALUE="";
	public static final String CHILD_HREF6="Href6";
	public static final String CHILD_HREF6_RESET_VALUE="";
	public static final String CHILD_HREF7="Href7";
	public static final String CHILD_HREF7_RESET_VALUE="";
	public static final String CHILD_HREF8="Href8";
	public static final String CHILD_HREF8_RESET_VALUE="";
	public static final String CHILD_HREF9="Href9";
	public static final String CHILD_HREF9_RESET_VALUE="";
	public static final String CHILD_HREF10="Href10";
	public static final String CHILD_HREF10_RESET_VALUE="";
	public static final String CHILD_STPAGELABEL="stPageLabel";
	public static final String CHILD_STPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STCOMPANYNAME="stCompanyName";
	public static final String CHILD_STCOMPANYNAME_RESET_VALUE="";
	public static final String CHILD_STTODAYDATE="stTodayDate";
	public static final String CHILD_STTODAYDATE_RESET_VALUE="";
	public static final String CHILD_STUSERNAMETITLE="stUserNameTitle";
	public static final String CHILD_STUSERNAMETITLE_RESET_VALUE="";
	public static final String CHILD_BTSUBMIT="btSubmit";
	public static final String CHILD_BTSUBMIT_RESET_VALUE=" ";
	public static final String CHILD_BTWORKQUEUELINK="btWorkQueueLink";
	public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE=" ";
	public static final String CHILD_CHANGEPASSWORDHREF="changePasswordHref";
	public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE="";
	public static final String CHILD_BTTOOLHISTORY="btToolHistory";
	public static final String CHILD_BTTOOLHISTORY_RESET_VALUE=" ";
	public static final String CHILD_BTTOONOTES="btTooNotes";
	public static final String CHILD_BTTOONOTES_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLSEARCH="btToolSearch";
	public static final String CHILD_BTTOOLSEARCH_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLLOG="btToolLog";
	public static final String CHILD_BTTOOLLOG_RESET_VALUE=" ";
	public static final String CHILD_STERRORFLAG="stErrorFlag";
	public static final String CHILD_STERRORFLAG_RESET_VALUE="N";
	public static final String CHILD_DETECTALERTTASKS="detectAlertTasks";
	public static final String CHILD_DETECTALERTTASKS_RESET_VALUE="";
	public static final String CHILD_BTCANCEL="btCancel";
	public static final String CHILD_BTCANCEL_RESET_VALUE=" ";
	public static final String CHILD_STDATEMASK="stDateMask";
	public static final String CHILD_STDATEMASK_RESET_VALUE="";
	public static final String CHILD_SESSIONUSERID="sessionUserId";
	public static final String CHILD_SESSIONUSERID_RESET_VALUE="";
	public static final String CHILD_STPMGENERATE="stPmGenerate";
	public static final String CHILD_STPMGENERATE_RESET_VALUE="";
	public static final String CHILD_STPMHASTITLE="stPmHasTitle";
	public static final String CHILD_STPMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STPMHASINFO="stPmHasInfo";
	public static final String CHILD_STPMHASINFO_RESET_VALUE="";
	public static final String CHILD_STPMHASTABLE="stPmHasTable";
	public static final String CHILD_STPMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STPMHASOK="stPmHasOk";
	public static final String CHILD_STPMHASOK_RESET_VALUE="";
	public static final String CHILD_STPMTITLE="stPmTitle";
	public static final String CHILD_STPMTITLE_RESET_VALUE="";
	public static final String CHILD_STPMINFOMSG="stPmInfoMsg";
	public static final String CHILD_STPMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STPMONOK="stPmOnOk";
	public static final String CHILD_STPMONOK_RESET_VALUE="";
	public static final String CHILD_STPMMSGS="stPmMsgs";
	public static final String CHILD_STPMMSGS_RESET_VALUE="";
	public static final String CHILD_STPMMSGTYPES="stPmMsgTypes";
	public static final String CHILD_STPMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMGENERATE="stAmGenerate";
	public static final String CHILD_STAMGENERATE_RESET_VALUE="";
	public static final String CHILD_STAMHASTITLE="stAmHasTitle";
	public static final String CHILD_STAMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STAMHASINFO="stAmHasInfo";
	public static final String CHILD_STAMHASINFO_RESET_VALUE="";
	public static final String CHILD_STAMHASTABLE="stAmHasTable";
	public static final String CHILD_STAMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STAMTITLE="stAmTitle";
	public static final String CHILD_STAMTITLE_RESET_VALUE="";
	public static final String CHILD_STAMINFOMSG="stAmInfoMsg";
	public static final String CHILD_STAMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STAMMSGS="stAmMsgs";
	public static final String CHILD_STAMMSGS_RESET_VALUE="";
	public static final String CHILD_STAMMSGTYPES="stAmMsgTypes";
	public static final String CHILD_STAMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMDIALOGMSG="stAmDialogMsg";
	public static final String CHILD_STAMDIALOGMSG_RESET_VALUE="";
	public static final String CHILD_STAMBUTTONSHTML="stAmButtonsHtml";
	public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE="";
	public static final String CHILD_BTOK="btOk";
	public static final String CHILD_BTOK_RESET_VALUE=" ";
	public static final String CHILD_CBINSTITUTIONS="cbInstitutions";
	public static final String CHILD_CBINSTITUTIONS_RESET_VALUE="";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 08Nov2002
  //private static CbInstitutionsOptionList cbInstitutionsOptions=new CbInstitutionsOptionList();
  private CbInstitutionsOptionList cbInstitutionsOptions=new CbInstitutionsOptionList();

  //Added the Variable for NonSelected Label
  private String CHILD_CBINSTITUTIONS_NONSELECTED_LABEL = "";
  //==================================================================

	public static final String CHILD_CBREGIONS="cbRegions";
	public static final String CHILD_CBREGIONS_RESET_VALUE="";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 08Nov2002
  //private static CbRegionsOptionList cbRegionsOptions=new CbRegionsOptionList();
  private CbRegionsOptionList cbRegionsOptions=new CbRegionsOptionList();

  //Added the Variable for NonSelected Label
  private String CHILD_CBREGIONS_NONSELECTED_LABEL = "";
  //==================================================================

	public static final String CHILD_CBBRANCHES="cbBranches";
	public static final String CHILD_CBBRANCHES_RESET_VALUE="";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 08Nov2002
  //private static CbBranchesOptionList cbBranchesOptions=new CbBranchesOptionList();
  private CbBranchesOptionList cbBranchesOptions=new CbBranchesOptionList();

  //Added the Variable for NonSelected Label
  private String CHILD_CBBRANCHES_NONSELECTED_LABEL = "";
  //==================================================================

	public static final String CHILD_CBGROUPS="cbGroups";
	public static final String CHILD_CBGROUPS_RESET_VALUE="";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 08Nov2002
  //private static CbGroupsOptionList cbGroupsOptions=new CbGroupsOptionList();
  private CbGroupsOptionList cbGroupsOptions=new CbGroupsOptionList();

  //Added the Variable for NonSelected Label
  private String CHILD_CBGROUPS_NONSELECTED_LABEL = "";
  //==================================================================

	public static final String CHILD_CBUSERS="cbUsers";
	public static final String CHILD_CBUSERS_RESET_VALUE="";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 08Nov2002
  //private static CbUsersOptionList cbUsersOptions=new CbUsersOptionList();
  private CbUsersOptionList cbUsersOptions=new CbUsersOptionList();

  //Added the Variable for NonSelected Label
  private String CHILD_CBUSERS_NONSELECTED_LABEL = "";
  //==================================================================

	public static final String CHILD_CBSOURCEFIRM="cbSourceFirm";
	public static final String CHILD_CBSOURCEFIRM_RESET_VALUE="";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 08Nov2002
  //private static CbSourceFirmOptionList cbSourceFirmOptions=new CbSourceFirmOptionList();
  private CbSourceFirmOptionList cbSourceFirmOptions=new CbSourceFirmOptionList();

  //Added the Variable for NonSelected Label
  private String CHILD_CBSOURCEFIRM_NONSELECTED_LABEL = "";
  //==================================================================

	public static final String CHILD_TBDAYFROM="tbDayFrom";
	public static final String CHILD_TBDAYFROM_RESET_VALUE="";
	public static final String CHILD_TBDAYTO="tbDayTo";
	public static final String CHILD_TBDAYTO_RESET_VALUE="";
	public static final String CHILD_TBYEARFROM="tbYearFrom";
	public static final String CHILD_TBYEARFROM_RESET_VALUE="";
	public static final String CHILD_TBYEARTO="tbYearTo";
	public static final String CHILD_TBYEARTO_RESET_VALUE="";
	public static final String CHILD_CBMONTHFROM="cbMonthFrom";
	public static final String CHILD_CBMONTHFROM_RESET_VALUE="";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 08Nov2002
  //private static CbMonthFromOptionList cbMonthFromOptions=new CbMonthFromOptionList();
  private CbMonthFromOptionList cbMonthFromOptions=new CbMonthFromOptionList();
  //==================================================================

  public static final String CHILD_CBMONTHTO="cbMonthTo";
  public static final String CHILD_CBMONTHTO_RESET_VALUE="";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 08Nov2002
  //private static CbMonthToOptionList cbMonthToOptions=new CbMonthToOptionList();
  private CbMonthToOptionList cbMonthToOptions=new CbMonthToOptionList();
  //==================================================================

  //// New hidden button implementing the 'fake' one from the ActiveMessage class.
  public static final String CHILD_BTACTMSG="btActMsg";
	public static final String CHILD_BTACTMSG_RESET_VALUE="ActMsg";
  //--Release2.1--//
  //// New link to toggle the language. When touched this link should reload the
  //// page in opposite language (french versus english) and set this new language
  //// session id throughout all modulus.
	public static final String CHILD_TOGGLELANGUAGEHREF="toggleLanguageHref";
	public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	protected String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgPortfolioStats.jsp";

	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	// MigrationToDo : Migrate custom member
	private PortfolioStatsHandler handler=new PortfolioStatsHandler();

}

