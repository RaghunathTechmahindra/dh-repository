package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

public class pgDealSummaryRepeatedSOBTiledView extends
		RequestHandlingTiledViewBase implements TiledView, RequestHandler {


	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	private doUWSourceDetailsModel doUWSourceDetails = null;
	private DealSummaryHandler handler = new DealSummaryHandler();
	
	

    public static final String CHILD_STSOURCE = "stSource";
    public static final String CHILD_STSOURCE_RESET_VALUE = "";
    
    public static final String CHILD_STUWSOURCEFIRM = "stUWSourceFirm";
    public static final String CHILD_STUWSOURCEFIRM_RESET_VALUE = "";

    public static final String CHILD_STUWSOURCEADDRESSLINE1 = "stUWSourceAddressLine1";
    public static final String CHILD_STUWSOURCEADDRESSLINE1_RESET_VALUE = "";

    public static final String CHILD_STUWSOURCECITY = "stUWSourceCity";
    public static final String CHILD_STUWSOURCECITY_RESET_VALUE = "";

    public static final String CHILD_STUWSOURCEPROVINCE = "stUWSourceProvince";
    public static final String CHILD_STUWSOURCEPROVINCE_RESET_VALUE = "";

    public static final String CHILD_STUWSOURCECONTACTPHONE = "stUWSourceContactPhone";
    public static final String CHILD_STUWSOURCECONTACTPHONE_RESET_VALUE = "";

    public static final String CHILD_STUWSOURCEFAX = "stUWSourceFax";
    public static final String CHILD_STUWSOURCEFAX_RESET_VALUE = "";

	public static final String CHILD_STSOURCEFIRSTNAME = "stSourceFirstName";
    public static final String CHILD_STSOURCEFIRSTNAME_RESET_VALUE = "";

    public static final String CHILD_STSOURCELASTNAME = "stSourceLastName";
    public static final String CHILD_STSOURCELASTNAME_RESET_VALUE = "";

	public static final String CHILD_STCONTACTEMAILADDRESS = "stContactEmailAddress";
    public static final String CHILD_STCONTACTEMAILADDRESS_RESET_VALUE = "";
    
    public static final String CHILD_STLICENSENUMBER = "stLicenseNumber";
    public static final String CHILD_STLICENSENUMBER_RESET_VALUE = "";

    public static final String CHILD_STPSDESCRIPTION = "stPsDescription";
    public static final String CHILD_STPSDESCRIPTION_RESET_VALUE = "";

    public static final String CHILD_STSYSTEMTYPEDESCRIPTION = "stSystemTypeDescription";
    public static final String CHILD_STSYSTEMTYPEDESCRIPTION_RESET_VALUE = "";
    
    public static final String CHILD_STSOURCENUMBER = "stSourceNumber";
    public static final String CHILD_STSOURCENUMBER_RESET_VALUE = "";

   
	public pgDealSummaryRepeatedSOBTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doUWSourceDetailsModel.class );
		registerChildren();
		initialize();
	}
	
	protected void initialize()
	{
	}

	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoUWSourceDetailsModel());
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}
	
	
	public doUWSourceDetailsModel getdoUWSourceDetailsModel()
	{
		if (doUWSourceDetails == null)
			doUWSourceDetails = (doUWSourceDetailsModel) getModel(doUWSourceDetailsModel.class);
		return doUWSourceDetails;
	}
	
	public void setdoUWSourceDetailsModel(doUWSourceDetailsModel model)
	{
			doUWSourceDetails = model;
	}
	
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}

	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}

	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}
	
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);
	}
	
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STSOURCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWSourceDetailsModel(),
				CHILD_STSOURCE,
				doUWSourceDetailsModel.FIELD_DFSOBPSHORTNAME,
				CHILD_STSOURCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUWSOURCEFIRM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWSourceDetailsModel(),
				CHILD_STUWSOURCEFIRM,
				doUWSourceDetailsModel.FIELD_DFSOURCEFIRMNAME,
				CHILD_STUWSOURCEFIRM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUWSOURCEADDRESSLINE1))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWSourceDetailsModel(),
				CHILD_STUWSOURCEADDRESSLINE1,
				doUWSourceDetailsModel.FIELD_DFADDRESSLINE1,
				CHILD_STUWSOURCEADDRESSLINE1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUWSOURCECITY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWSourceDetailsModel(),
				CHILD_STUWSOURCECITY,
				doUWSourceDetailsModel.FIELD_DFCITY,
				CHILD_STUWSOURCECITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUWSOURCEPROVINCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWSourceDetailsModel(),
				CHILD_STUWSOURCEPROVINCE,
				doUWSourceDetailsModel.FIELD_DFPROVINCEABBR,
				CHILD_STUWSOURCEPROVINCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUWSOURCECONTACTPHONE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWSourceDetailsModel(),
				CHILD_STUWSOURCECONTACTPHONE,
				doUWSourceDetailsModel.FIELD_DFCONTACTPHONENUMBER,
				CHILD_STUWSOURCECONTACTPHONE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUWSOURCEFAX))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWSourceDetailsModel(),
				CHILD_STUWSOURCEFAX,
				doUWSourceDetailsModel.FIELD_DFCONTACTFAXNUMBER,
				CHILD_STUWSOURCEFAX_RESET_VALUE,
				null);
			return child;
		}
		else	
		if (name.equals(CHILD_STSOURCEFIRSTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWSourceDetailsModel(),
				CHILD_STSOURCEFIRSTNAME,
				doUWSourceDetailsModel.FIELD_DFCONTACTFIRSTNAME,
				CHILD_STSOURCEFIRSTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSOURCELASTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWSourceDetailsModel(),
				CHILD_STSOURCELASTNAME,
				doUWSourceDetailsModel.FIELD_DFCONTACTLASTNAME,
				CHILD_STSOURCELASTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCONTACTEMAILADDRESS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWSourceDetailsModel(),
				CHILD_STCONTACTEMAILADDRESS,
				doUWSourceDetailsModel.FIELD_DFCONTACTEMAILADDRESS,
				CHILD_STCONTACTEMAILADDRESS_RESET_VALUE,
				null);
			return child;
		}
        else
        if (name.equals(CHILD_STLICENSENUMBER))
        {
            StaticTextField child = new StaticTextField(this,
                getdoUWSourceDetailsModel(),
                CHILD_STLICENSENUMBER,
                doUWSourceDetailsModel.FIELD_DFLICENSENUMBER,
                CHILD_STLICENSENUMBER_RESET_VALUE,
                null);
            return child;
        }
		else
		if (name.equals(CHILD_STPSDESCRIPTION))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWSourceDetailsModel(),
				CHILD_STPSDESCRIPTION,
				doUWSourceDetailsModel.FIELD_DFPSDESCRIPTION,
				CHILD_STPSDESCRIPTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSYSTEMTYPEDESCRIPTION))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWSourceDetailsModel(),
				CHILD_STSYSTEMTYPEDESCRIPTION,
				doUWSourceDetailsModel.FIELD_DFSYSTEMTYPEDESCRIPTION,
				CHILD_STSYSTEMTYPEDESCRIPTION_RESET_VALUE,
				null);
			return child;
		}
		else if (name.equals(CHILD_STSOURCENUMBER))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWSourceDetailsModel(),
				CHILD_STSOURCENUMBER,
				doUWSourceDetailsModel.FIELD_DFSORTORDER,
				CHILD_STSOURCENUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");

	}

	public boolean nextTile() throws ModelControlException
	{
	
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();
	
		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}
	
		return movedToRow;
	
	}

	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		// The following code block was migrated from the RepeatedTDS_onBeforeDisplayEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.setupNumOfRepeatedSOB(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}
/*
	public Model getDefaultModel() {
		// TODO Auto-generated method stub
		return doUWSourceDetails;
	}*/


	public void resetChildren()
	{
		getStSource().setValue(CHILD_STSOURCE_RESET_VALUE);
		getStSourceFirstName().setValue(CHILD_STSOURCEFIRSTNAME_RESET_VALUE);
		getStSourceLastName().setValue(CHILD_STSOURCELASTNAME_RESET_VALUE);
		getStUWSourceFirm().setValue(CHILD_STUWSOURCEFIRM_RESET_VALUE);
		getStUWSourceAddressLine1().setValue(CHILD_STUWSOURCEADDRESSLINE1_RESET_VALUE);
		getStUWSourceCity().setValue(CHILD_STUWSOURCECITY_RESET_VALUE);
		getStUWSourceProvince().setValue(CHILD_STUWSOURCEPROVINCE_RESET_VALUE);
		getStUWSourceContactPhone().setValue(CHILD_STUWSOURCECONTACTPHONE_RESET_VALUE);
		getStUWSourceFax().setValue(CHILD_STUWSOURCEFAX_RESET_VALUE);
		getStContactEmailAddress().setValue(CHILD_STCONTACTEMAILADDRESS_RESET_VALUE);
        getStLicenseNumber().setValue(CHILD_STLICENSENUMBER_RESET_VALUE);
		getStPsDescription().setValue(CHILD_STPSDESCRIPTION_RESET_VALUE);
		getStSystemTypeDescription().setValue(CHILD_STSYSTEMTYPEDESCRIPTION_RESET_VALUE);
        getSourceNumber().setValue(CHILD_STSOURCENUMBER_RESET_VALUE);
	}

	
	public void registerChildren()
	{
        registerChild(CHILD_STSOURCE, StaticTextField.class);
        registerChild(CHILD_STUWSOURCEFIRM, StaticTextField.class);
        registerChild(CHILD_STUWSOURCEADDRESSLINE1, StaticTextField.class);
        registerChild(CHILD_STUWSOURCECITY, StaticTextField.class);
        registerChild(CHILD_STUWSOURCEPROVINCE, StaticTextField.class);
        registerChild(CHILD_STUWSOURCECONTACTPHONE, StaticTextField.class);
        registerChild(CHILD_STUWSOURCEFAX, StaticTextField.class);
        registerChild(CHILD_STSOURCEFIRSTNAME, StaticTextField.class);
        registerChild(CHILD_STSOURCELASTNAME, StaticTextField.class);
        registerChild(CHILD_STCONTACTEMAILADDRESS, StaticTextField.class);
        registerChild(CHILD_STLICENSENUMBER, StaticTextField.class);
        registerChild(CHILD_STPSDESCRIPTION, StaticTextField.class);
        registerChild(CHILD_STSYSTEMTYPEDESCRIPTION, StaticTextField.class);
        registerChild(CHILD_STSOURCENUMBER, StaticTextField.class);
	}

   public StaticTextField getStSource() {
       return (StaticTextField) getChild(CHILD_STSOURCE);
   }

   public StaticTextField getStSourceFirstName() {
       return (StaticTextField) getChild(CHILD_STSOURCEFIRSTNAME);
   }

   public StaticTextField getStSourceLastName() {
       return (StaticTextField) getChild(CHILD_STSOURCELASTNAME);
   }
   public StaticTextField getStUWSourceFirm() {
       return (StaticTextField) getChild(CHILD_STUWSOURCEFIRM);
   }

   public StaticTextField getStUWSourceAddressLine1() {
       return (StaticTextField) getChild(CHILD_STUWSOURCEADDRESSLINE1);
   }

   public StaticTextField getStUWSourceCity() {
       return (StaticTextField) getChild(CHILD_STUWSOURCECITY);
   }

   public StaticTextField getStUWSourceProvince() {
       return (StaticTextField) getChild(CHILD_STUWSOURCEPROVINCE);
   }

   public StaticTextField getStUWSourceContactPhone() {
       return (StaticTextField) getChild(CHILD_STUWSOURCECONTACTPHONE);
   }

   public StaticTextField getStUWSourceFax() {
       return (StaticTextField) getChild(CHILD_STUWSOURCEFAX);
   }

   public StaticTextField getStContactEmailAddress() {
       return (StaticTextField) getChild(CHILD_STCONTACTEMAILADDRESS);
   }
   
   public StaticTextField getStLicenseNumber() {
       return (StaticTextField) getChild(CHILD_STLICENSENUMBER);
   }
   
   public StaticTextField getStPsDescription() {
       return (StaticTextField) getChild(CHILD_STPSDESCRIPTION);
   }

   public StaticTextField getStSystemTypeDescription() {
       return (StaticTextField) getChild(CHILD_STSYSTEMTYPEDESCRIPTION);
   }

   public StaticTextField getSourceNumber() {
       return (StaticTextField) getChild(CHILD_STSOURCENUMBER);
   }
   
}
