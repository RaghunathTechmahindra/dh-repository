package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 * <p>
 * Title: doDetailViewBorrowersModelImpl
 * </p>
 * 
 * <p>
 * Description: Model IMPL class for Applicant detials part of the deal summary
 * screen
 *
 *
 * @author
 * @version 1.1 Date: 8/01/006 <br>
 *          Author: NBC/PP Implementation Team <br>
 *          Change: <br>
 *          Added 5 new fields in the applicant details section
 *
 */
public class doDetailViewBorrowersModelImpl extends QueryModelBase
	implements doDetailViewBorrowersModel
{
	/**
	 *
	 *
	 */
	public doDetailViewBorrowersModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;
	}


	/**
	 * afterExecute
	 *
	 * @param 2 inputs  ModelExecutionContext,queryType <br>
	 *
	 * @return void : the result of afterExecute <br>
	 * @version 1.1 
	 * Date: 7/27/006 <br>
	 * Author:  <br>
	 * 
	 * Change:  <br>
 *	Added 4 pick lists  <br>

	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 05Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert SALUTATION Desc.
      this.setDfSalutation(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "SALUTATION", this.getDfSalutation(), languageId));
      //Convert BORROWER type Desc.
      this.setDfApplicantType(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "BORROWERTYPE", this.getDfApplicantType(), languageId));
      //Convert MARITALSTATUS desc.
      this.setDfMaritalStatus(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "MARITALSTATUS", this.getDfMaritalStatus(), languageId));
      //Convert CITIZENSHIPTYPE desc.
      this.setDfCitizenshipStatus(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "CITIZENSHIPTYPE", this.getDfCitizenshipStatus(), languageId));
      //Convert BANKRUPTCYSTATUS desc.
      this.setDfBankruptcyStatus(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "BANKRUPTCYSTATUS", this.getDfBankruptcyStatus(), languageId));
      //Convert LANGUAGEPREFERENCE desc.
      this.setDfLanguagePreference(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "LANGUAGEPREFERENCE", this.getDfLanguagePreference(), languageId));
//			***** Change by NBC Impl. Team - Version 1.1 - Start *****//
			this.setDfGender(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
					"BORROWERGENDER", this.getDfGender(), languageId));

			this.setDfSmoker(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),"SMOKESTATUS",
					this.getDfSmoker(), languageId));

			this.setDfSolicitation(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
					"SOLICITATION", this.getDfSolicitation(), languageId));

			this.setDfPreferredMethodOfContact(BXResources
					.getPickListDescription(theSessionState.getDealInstitutionId(),"PREFMETHODOFCONTACT", this
							.getDfPreferredMethodOfContact(), languageId));


//			***** Change by NBC Impl. Team - Version 1.1 - End*****//
           this.setDfSuffix(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(), "SUFFIX", this.getDfSuffix(), languageId));  
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


//	 ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
	/**
	 * getDfGender
	 *
	 * @param None <br>
	 *
	 * @return String : the result of getDfGender <br>
	 */
	public String getDfGender() {
		return (String) getValue(FIELD_DFGENDER);
	}
	/**
	 * setDfGender
	 *
	 * @param String <br>
	 *
	 * @return Void : the result of setDfGender <br>
	 */
	
	public void setDfGender(String value) {
		setValue(FIELD_DFGENDER, value);
	}

	/**
	 * getDfSolicitation
	 *
	 * @param None <br>
	 *
	 * @return String : the result of getDfSolicitation <br>
	 */
	public String getDfSolicitation() {
		return (String) getValue(FIELD_DFSOLICITATION);
	}
	/**
	 * setDfSolicitation
	 *
	 * @param String <br>
	 *
	 * @return Void : the result of setDfSolicitation <br>
	 */
	
	public void setDfSolicitation(String value) {
		setValue(FIELD_DFSOLICITATION, value);
	}

	/**
	 * getDfSmoker
	 *
	 * @param None <br>
	 *
	 * @return String : the result of getDfSmoker <br>
	 */
	public String getDfSmoker() {
		return (String) getValue(FIELD_DFSMOKER);
	}
	/**
	 * setDfSmoker
	 *
	 * @param String <br>
	 *
	 * @return Void : the result of setDfSmoker <br>
	 */
	
	public void setDfSmoker(String value) {
		setValue(FIELD_DFSMOKER, value);
	}

	/**
	 * getDfEmployeeNumber
	 *
	 * @param None <br>
	 *
	 * @return String : the result of getDfEmployeeNumber <br>
	 */
	public String getDfEmployeeNumber() {
		return (String) getValue(FIELD_DFEMPLOYEENUMBER);
	}
	/**
	 * setDfEmployeeNumber
	 *
	 * @param String <br>
	 *
	 * @return Void : the result of setDfEmployeeNumber <br>
	 */
	public void setDfEmployeeNumber(String value) {
		setValue(FIELD_DFEMPLOYEENUMBER, value);
	}

	/**
	 * getDfPreferredMethodOfContact
	 *
	 * @param None <br>
	 *
	 * @return String : the result of getDfPreferredMethodOfContact <br>
	 */
	public String getDfPreferredMethodOfContact() {
		return (String) getValue(FIELD_DFPREFERREDMETHODOFCONTACT);
	}
	/**
	 * setDfPreferredMethodOfContact
	 *
	 * @param String <br>
	 *
	 * @return Void : the result of setDfPreferredMethodOfContact <br>
	 */
	public void setDfPreferredMethodOfContact(String value) {
		setValue(FIELD_DFPREFERREDMETHODOFCONTACT, value);
	}


	/**
	 *
	 *
	 */
	public String getDfSalutation()
	{
		return (String)getValue(FIELD_DFSALUTATION);
	}


	/**
	 *
	 *
	 */
	public void setDfSalutation(String value)
	{
		setValue(FIELD_DFSALUTATION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfFirstName()
	{
		return (String)getValue(FIELD_DFFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfFirstName(String value)
	{
		setValue(FIELD_DFFIRSTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMiddleName()
	{
		return (String)getValue(FIELD_DFMIDDLENAME);
	}


	/**
	 *
	 *
	 */
	public void setDfMiddleName(String value)
	{
		setValue(FIELD_DFMIDDLENAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLastName()
	{
		return (String)getValue(FIELD_DFLASTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfLastName(String value)
	{
		setValue(FIELD_DFLASTNAME,value);
	}

	/**
	 *
	 *
	 */
	public String getDfSuffix()
	{
		return (String)getValue(FIELD_DFSUFFIX);
	}


	/**
	 *
	 *
	 */
	public void setDfSuffix(String value)
	{
		setValue(FIELD_DFSUFFIX,value);
	}
	/**
	 *
	 *
	 */
	public String getDfApplicantType()
	{
		return (String)getValue(FIELD_DFAPPLICANTTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfApplicantType(String value)
	{
		setValue(FIELD_DFAPPLICANTTYPE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPrimaryBorrower()
	{
		return (String)getValue(FIELD_DFPRIMARYBORROWER);
	}


	/**
	 *
	 *
	 */
	public void setDfPrimaryBorrower(String value)
	{
		setValue(FIELD_DFPRIMARYBORROWER,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfDateOfBirth()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFDATEOFBIRTH);
	}


	/**
	 *
	 *
	 */
	public void setDfDateOfBirth(java.sql.Timestamp value)
	{
		setValue(FIELD_DFDATEOFBIRTH,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMaritalStatus()
	{
		return (String)getValue(FIELD_DFMARITALSTATUS);
	}


	/**
	 *
	 *
	 */
	public void setDfMaritalStatus(String value)
	{
		setValue(FIELD_DFMARITALSTATUS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSINNo()
	{
		return (String)getValue(FIELD_DFSINNO);
	}


	/**
	 *
	 *
	 */
	public void setDfSINNo(String value)
	{
		setValue(FIELD_DFSINNO,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCitizenshipStatus()
	{
		return (String)getValue(FIELD_DFCITIZENSHIPSTATUS);
	}


	/**
	 *
	 *
	 */
	public void setDfCitizenshipStatus(String value)
	{
		setValue(FIELD_DFCITIZENSHIPSTATUS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNumberOfDependants()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFNUMBEROFDEPENDANTS);
	}


	/**
	 *
	 *
	 */
	public void setDfNumberOfDependants(java.math.BigDecimal value)
	{
		setValue(FIELD_DFNUMBEROFDEPENDANTS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfExistingCleint()
	{
		return (String)getValue(FIELD_DFEXISTINGCLEINT);
	}


	/**
	 *
	 *
	 */
	public void setDfExistingCleint(String value)
	{
		setValue(FIELD_DFEXISTINGCLEINT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfReferenceClientNo()
	{
		return (String)getValue(FIELD_DFREFERENCECLIENTNO);
	}


	/**
	 *
	 *
	 */
	public void setDfReferenceClientNo(String value)
	{
		setValue(FIELD_DFREFERENCECLIENTNO,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNoOfTimesBankrupt()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFNOOFTIMESBANKRUPT);
	}


	/**
	 *
	 *
	 */
	public void setDfNoOfTimesBankrupt(java.math.BigDecimal value)
	{
		setValue(FIELD_DFNOOFTIMESBANKRUPT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBankruptcyStatus()
	{
		return (String)getValue(FIELD_DFBANKRUPTCYSTATUS);
	}


	/**
	 *
	 *
	 */
	public void setDfBankruptcyStatus(String value)
	{
		setValue(FIELD_DFBANKRUPTCYSTATUS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStaffOfLender()
	{
		return (String)getValue(FIELD_DFSTAFFOFLENDER);
	}


	/**
	 *
	 *
	 */
	public void setDfStaffOfLender(String value)
	{
		setValue(FIELD_DFSTAFFOFLENDER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfHomePhoneNo()
	{
		return (String)getValue(FIELD_DFHOMEPHONENO);
	}


	/**
	 *
	 *
	 */
	public void setDfHomePhoneNo(String value)
	{
		setValue(FIELD_DFHOMEPHONENO,value);
	}


	/**
	 *
	 *
	 */
	public String getDfWorkPhoneNo()
	{
		return (String)getValue(FIELD_DFWORKPHONENO);
	}


	/**
	 *
	 *
	 */
	public void setDfWorkPhoneNo(String value)
	{
		setValue(FIELD_DFWORKPHONENO,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCellPhoneNo()
	{
		return (String)getValue(FIELD_DFCELLPHONENO);
	}


	/**
	 *
	 *
	 */
	public void setDfCellPhoneNo(String value)
	{
		setValue(FIELD_DFCELLPHONENO,value);
	}
	
	/**
	 *
	 *
	 */
	public String getDfFaxNumber()
	{
		return (String)getValue(FIELD_DFFAXNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfFaxNumber(String value)
	{
		setValue(FIELD_DFFAXNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmailAddress()
	{
		return (String)getValue(FIELD_DFEMAILADDRESS);
	}


	/**
	 *
	 *
	 */
	public void setDfEmailAddress(String value)
	{
		setValue(FIELD_DFEMAILADDRESS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLanguagePreference()
	{
		return (String)getValue(FIELD_DFLANGUAGEPREFERENCE);
	}


	/**
	 *
	 *
	 */
	public void setDfLanguagePreference(String value)
	{
		setValue(FIELD_DFLANGUAGEPREFERENCE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBorrowerWorkPhoneExt()
	{
		return (String)getValue(FIELD_DFBORROWERWORKPHONEEXT);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerWorkPhoneExt(String value)
	{
		setValue(FIELD_DFBORROWERWORKPHONEEXT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCreditScore()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCREDITSCORE);
	}


	/**
	 *
	 *
	 */
	public void setDfCreditScore(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCREDITSCORE,value);
	}


	/**
	 *
	 *
	 */
	public String getSnapShotBorrowerName()
	{
		return (String)getValue(FIELD_SNAPSHOTBORROWERNAME);
	}


	/**
	 *
	 *
	 */
	public void setSnapShotBorrowerName(String value)
	{
		setValue(FIELD_SNAPSHOTBORROWERNAME,value);
	}

	  public java.math.BigDecimal getDfInstitutionId() {
		    return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
		  }

		  public void setDfInstitutionId(java.math.BigDecimal value) {
		    setValue(FIELD_DFINSTITUTIONID, value);
		  }
	/**
	 * MetaData object test method to verify the SYNTHETIC new
	 * field for the reogranized SQL_TEMPLATE query.
	 *
	 */
  /**
	public void setResultSet(ResultSet value)
	{
  		logger = SysLog.getSysLogger("METADATA");

  		try
  		{
        super.setResultSet(value);
   			if( value != null)
   			{
       			ResultSetMetaData metaData = value.getMetaData();
       			int columnCount = metaData.getColumnCount();
       			logger.debug("===============================================");
            logger.debug("Testing MetaData Object for new synthetic fields for" +
                          " doDetailViewBorrowersModel");
       			logger.debug("NumberColumns: " + columnCount);
         		for(int i = 0; i < columnCount ; i++)
          		{
                 	logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
          		}
          		logger.debug("================================================");
      		}
  		}
  		catch (SQLException e)
  		{
    		    logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
  		}
 	}
  **/

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.

  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 15Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	////public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT BORROWER.DEALID, BORROWER.BORROWERID, SALUTATION.SDESCRIPTION, BORROWER.BORROWERFIRSTNAME, BORROWER.BORROWERMIDDLEINITIAL, BORROWER.BORROWERLASTNAME, BORROWERTYPE.BTDESCRIPTION, BORROWER.PRIMARYBORROWERFLAG, BORROWER.BORROWERBIRTHDATE, MARITALSTATUS.MSDESCRIPTION, BORROWER.SOCIALINSURANCENUMBER, CITIZENSHIPTYPE.CTDESCRIPTION, BORROWER.NUMBEROFDEPENDENTS, BORROWER.EXISTINGCLIENT, BORROWER.CLIENTREFERENCENUMBER, BORROWER.NUMBEROFTIMESBANKRUPT, BANKRUPTCYSTATUS.BANKRUPTCYSTATUSDESCRIPTION, BORROWER.STAFFOFLENDER, BORROWER.BORROWERHOMEPHONENUMBER, BORROWER.BORROWERWORKPHONENUMBER, BORROWER.BORROWERFAXNUMBER, BORROWER.BORROWEREMAILADDRESS, LANGUAGEPREFERENCE.LPDESCRIPTION, BORROWER.COPYID, BORROWER.BORROWERWORKPHONEEXTENSION, BORROWER.CREDITSCORE, BORROWER.BORROWERLASTNAME||','||                      substr(BORROWER. BORROWERFIRSTNAME,1,1) FROM BORROWER, LANGUAGEPREFERENCE, BORROWERTYPE, SALUTATION, MARITALSTATUS, CITIZENSHIPTYPE, BANKRUPTCYSTATUS  __WHERE__  ";
	//--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
  //public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT BORROWER.DEALID, BORROWER.BORROWERID, SALUTATION.SDESCRIPTION, BORROWER.BORROWERFIRSTNAME, BORROWER.BORROWERMIDDLEINITIAL, BORROWER.BORROWERLASTNAME, BORROWERTYPE.BTDESCRIPTION, BORROWER.PRIMARYBORROWERFLAG, BORROWER.BORROWERBIRTHDATE, MARITALSTATUS.MSDESCRIPTION, BORROWER.SOCIALINSURANCENUMBER, CITIZENSHIPTYPE.CTDESCRIPTION, BORROWER.NUMBEROFDEPENDENTS, BORROWER.EXISTINGCLIENT, BORROWER.CLIENTREFERENCENUMBER, BORROWER.NUMBEROFTIMESBANKRUPT, BANKRUPTCYSTATUS.BANKRUPTCYSTATUSDESCRIPTION, BORROWER.STAFFOFLENDER, BORROWER.BORROWERHOMEPHONENUMBER, BORROWER.BORROWERWORKPHONENUMBER, BORROWER.BORROWERFAXNUMBER, BORROWER.BORROWEREMAILADDRESS, LANGUAGEPREFERENCE.LPDESCRIPTION, BORROWER.COPYID, BORROWER.BORROWERWORKPHONEEXTENSION, BORROWER.CREDITSCORE, BORROWER.BORROWERLASTNAME ||','|| substr(BORROWER. BORROWERFIRSTNAME,1,1) SNAPSHOTBORROWERNAME FROM BORROWER, LANGUAGEPREFERENCE, BORROWERTYPE, SALUTATION, MARITALSTATUS, CITIZENSHIPTYPE, BANKRUPTCYSTATUS  __WHERE__  ";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT DISTINCT BORROWER.DEALID, BORROWER.BORROWERID, to_char(BORROWER.SALUTATIONID) SALUTATIONID_STR, "+
    "BORROWER.BORROWERFIRSTNAME, BORROWER.BORROWERMIDDLEINITIAL, BORROWER.BORROWERLASTNAME, to_char(BORROWER.SUFFIXID) BORROWERSUFFIXID_STR, "+
    "to_char(BORROWER.BORROWERTYPEID) BORROWERTYPEID_STR, BORROWER.PRIMARYBORROWERFLAG, BORROWER.BORROWERBIRTHDATE, "+
    "to_char(BORROWER.MARITALSTATUSID) MARITALSTATUSID_STR, BORROWER.SOCIALINSURANCENUMBER, to_char(BORROWER.CITIZENSHIPTYPEID) CITIZENSHIPTYPEID_STR, "+
    "BORROWER.NUMBEROFDEPENDENTS, BORROWER.EXISTINGCLIENT, BORROWER.CLIENTREFERENCENUMBER, "+
    "BORROWER.NUMBEROFTIMESBANKRUPT, to_char(BORROWER.BANKRUPTCYSTATUSID) BANKRUPTCYSTATUSID_STR, "+
    "BORROWER.STAFFOFLENDER, BORROWER.BORROWERHOMEPHONENUMBER, BORROWER.BORROWERCELLPHONENUMBER, BORROWER.BORROWERWORKPHONENUMBER, "+
    "BORROWER.BORROWERFAXNUMBER, BORROWER.BORROWEREMAILADDRESS, to_char(BORROWER.LANGUAGEPREFERENCEID) LANGUAGEPREFERENCEID_STR, "+
    "BORROWER.COPYID, BORROWER.BORROWERWORKPHONEEXTENSION, BORROWER.CREDITSCORE, "+
    "BORROWER.BORROWERLASTNAME ||','|| substr(BORROWER. BORROWERFIRSTNAME,1,1) SNAPSHOTBORROWERNAME, "+
//			***** Change by NBC Impl. Team - Version 1.1 - Start *****//
			"to_char(BORROWER.BORROWERGENDERID) BORROWERGENDERID_STR, to_char(BORROWER.SOLICITATIONID) SOLICITATIONID_STR, to_char(BORROWER.PREFCONTACTMETHODID) PREFCONTACTMETHODID_STR , BORROWER.EMPLOYEENUMBER , to_char(BORROWER.SMOKESTATUSID) SMOKESTATUSID_STR, "+
"BORROWER.INSTITUTIONPROFILEID "+
//			***** Change by NBC Impl. Team - Version 1.1 - End*****//
			" FROM BORROWER  " + "__WHERE__  ";
	//--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //public static final String MODIFYING_QUERY_TABLE_NAME="BORROWER, LANGUAGEPREFERENCE, BORROWERTYPE, SALUTATION, MARITALSTATUS, CITIZENSHIPTYPE, BANKRUPTCYSTATUS";
	public static final String MODIFYING_QUERY_TABLE_NAME=
    "BORROWER ";
	//--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
  //public static final String STATIC_WHERE_CRITERIA=" (BORROWER.SALUTATIONID  =  SALUTATION.SALUTATIONID) AND (BORROWER.BORROWERTYPEID  =  BORROWERTYPE.BORROWERTYPEID) AND (BORROWER.LANGUAGEPREFERENCEID  =  LANGUAGEPREFERENCE.LANGUAGEPREFERENCEID) AND (BORROWER.MARITALSTATUSID  =  MARITALSTATUS.MARITALSTATUSID) AND (BORROWER.CITIZENSHIPTYPEID  =  CITIZENSHIPTYPE.CITIZENSHIPTYPEID) AND (BORROWER.BANKRUPTCYSTATUSID  =  BANKRUPTCYSTATUS.BANKRUPTCYSTATUSID)";
	public static final String STATIC_WHERE_CRITERIA="";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="BORROWER.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFBORROWERID="BORROWER.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFFIRSTNAME="BORROWER.BORROWERFIRSTNAME";
	public static final String COLUMN_DFFIRSTNAME="BORROWERFIRSTNAME";
	public static final String QUALIFIED_COLUMN_DFMIDDLENAME="BORROWER.BORROWERMIDDLEINITIAL";
	public static final String COLUMN_DFMIDDLENAME="BORROWERMIDDLEINITIAL";
	public static final String QUALIFIED_COLUMN_DFLASTNAME="BORROWER.BORROWERLASTNAME";
	public static final String COLUMN_DFLASTNAME="BORROWERLASTNAME";
    public static final String QUALIFIED_COLUMN_DFSUFFIX="BORROWER.BORROWERSUFFIXID_STR";
    public static final String COLUMN_DFSUFFIX="BORROWERSUFFIXID_STR";
	public static final String QUALIFIED_COLUMN_DFPRIMARYBORROWER="BORROWER.PRIMARYBORROWERFLAG";
	public static final String COLUMN_DFPRIMARYBORROWER="PRIMARYBORROWERFLAG";
	public static final String QUALIFIED_COLUMN_DFDATEOFBIRTH="BORROWER.BORROWERBIRTHDATE";
	public static final String COLUMN_DFDATEOFBIRTH="BORROWERBIRTHDATE";
	public static final String QUALIFIED_COLUMN_DFSINNO="BORROWER.SOCIALINSURANCENUMBER";
	public static final String COLUMN_DFSINNO="SOCIALINSURANCENUMBER";
	public static final String QUALIFIED_COLUMN_DFNUMBEROFDEPENDANTS="BORROWER.NUMBEROFDEPENDENTS";
	public static final String COLUMN_DFNUMBEROFDEPENDANTS="NUMBEROFDEPENDENTS";
	public static final String QUALIFIED_COLUMN_DFEXISTINGCLEINT="BORROWER.EXISTINGCLIENT";
	public static final String COLUMN_DFEXISTINGCLEINT="EXISTINGCLIENT";
	public static final String QUALIFIED_COLUMN_DFREFERENCECLIENTNO="BORROWER.CLIENTREFERENCENUMBER";
	public static final String COLUMN_DFREFERENCECLIENTNO="CLIENTREFERENCENUMBER";
	public static final String QUALIFIED_COLUMN_DFNOOFTIMESBANKRUPT="BORROWER.NUMBEROFTIMESBANKRUPT";
	public static final String COLUMN_DFNOOFTIMESBANKRUPT="NUMBEROFTIMESBANKRUPT";
	public static final String QUALIFIED_COLUMN_DFSTAFFOFLENDER="BORROWER.STAFFOFLENDER";
	public static final String COLUMN_DFSTAFFOFLENDER="STAFFOFLENDER";
	public static final String QUALIFIED_COLUMN_DFHOMEPHONENO="BORROWER.BORROWERHOMEPHONENUMBER";
	public static final String COLUMN_DFHOMEPHONENO="BORROWERHOMEPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFWORKPHONENO="BORROWER.BORROWERWORKPHONENUMBER";
	public static final String COLUMN_DFWORKPHONENO="BORROWERWORKPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFCELLPHONENO="BORROWER.BORROWERCELLPHONENUMBER";
	public static final String COLUMN_DFCELLPHONENO="BORROWERCELLPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFFAXNUMBER="BORROWER.BORROWERFAXNUMBER";
	public static final String COLUMN_DFFAXNUMBER="BORROWERFAXNUMBER";
	public static final String QUALIFIED_COLUMN_DFEMAILADDRESS="BORROWER.BORROWEREMAILADDRESS";
	public static final String COLUMN_DFEMAILADDRESS="BORROWEREMAILADDRESS";
	public static final String QUALIFIED_COLUMN_DFCOPYID="BORROWER.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFBORROWERWORKPHONEEXT="BORROWER.BORROWERWORKPHONEEXTENSION";
	public static final String COLUMN_DFBORROWERWORKPHONEEXT="BORROWERWORKPHONEEXTENSION";
	public static final String QUALIFIED_COLUMN_DFCREDITSCORE="BORROWER.CREDITSCORE";
	public static final String COLUMN_DFCREDITSCORE="CREDITSCORE";
	////public static final String QUALIFIED_COLUMN_SNAPSHOTBORROWERNAME=".";
	////public static final String COLUMN_SNAPSHOTBORROWERNAME="";
	public static final String QUALIFIED_COLUMN_SNAPSHOTBORROWERNAME="BORROWER.SYNTSNAPSHOTBORROWERNAME";
	public static final String COLUMN_SNAPSHOTBORROWERNAME="SNAPSHOTBORROWERNAME";

  //--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
  //public static final String QUALIFIED_COLUMN_DFSALUTATION="SALUTATION.SDESCRIPTION";
	//public static final String COLUMN_DFSALUTATION="SDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFSALUTATION="BORROWER.SALUTATIONID_STR";
	public static final String COLUMN_DFSALUTATION="SALUTATIONID_STR";
  //public static final String QUALIFIED_COLUMN_DFAPPLICANTTYPE="BORROWERTYPE.BTDESCRIPTION";
	//public static final String COLUMN_DFAPPLICANTTYPE="BTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFAPPLICANTTYPE="BORROWER.BORROWERTYPEID_STR";
	public static final String COLUMN_DFAPPLICANTTYPE="BORROWERTYPEID_STR";
  //public static final String QUALIFIED_COLUMN_DFMARITALSTATUS="MARITALSTATUS.MSDESCRIPTION";
	//public static final String COLUMN_DFMARITALSTATUS="MSDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFMARITALSTATUS="BORROWER.MARITALSTATUSID_STR";
	public static final String COLUMN_DFMARITALSTATUS="MARITALSTATUSID_STR";
  //public static final String QUALIFIED_COLUMN_DFCITIZENSHIPSTATUS="CITIZENSHIPTYPE.CTDESCRIPTION";
	//public static final String COLUMN_DFCITIZENSHIPSTATUS="CTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFCITIZENSHIPSTATUS="BORROWER.CITIZENSHIPTYPEID_STR";
	public static final String COLUMN_DFCITIZENSHIPSTATUS="CITIZENSHIPTYPEID_STR";
  //public static final String QUALIFIED_COLUMN_DFBANKRUPTCYSTATUS="BANKRUPTCYSTATUS.BANKRUPTCYSTATUSDESCRIPTION";
	//public static final String COLUMN_DFBANKRUPTCYSTATUS="BANKRUPTCYSTATUSDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFBANKRUPTCYSTATUS="BORROWER.BANKRUPTCYSTATUSID_STR";
	public static final String COLUMN_DFBANKRUPTCYSTATUS="BANKRUPTCYSTATUSID_STR";
  //public static final String QUALIFIED_COLUMN_DFLANGUAGEPREFERENCE="LANGUAGEPREFERENCE.LPDESCRIPTION";
	//public static final String COLUMN_DFLANGUAGEPREFERENCE="LPDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFLANGUAGEPREFERENCE="BORROWER.LANGUAGEPREFERENCEID_STR";
	public static final String COLUMN_DFLANGUAGEPREFERENCE="LANGUAGEPREFERENCEID_STR";

	public static final String QUALIFIED_COLUMN_DFSOLICITATION = "BORROWER.SOLICITATIONID_STR";

	public static final String COLUMN_DFSOLICITATION = "SOLICITATIONID_STR";
//	***** Change by NBC Impl. Team - Version 1.1 - Start *****//
	public static final String QUALIFIED_COLUMN_DFGENDER = "BORROWER.BORROWERGENDERID_STR";

	public static final String COLUMN_DFGENDER = "BORROWERGENDERID_STR";

	public static final String QUALIFIED_COLUMN_DFSMOKER = "BORROWER.SMOKESTATUSID_STR";

	public static final String COLUMN_DFSMOKER = "SMOKESTATUSID_STR";

	public static final String QUALIFIED_COLUMN_DFPREFCONTACTMETHODID = "BORROWER.PREFCONTACTMETHODID_STR";

	public static final String COLUMN_DFPREFCONTACTMETHODID = "PREFCONTACTMETHODID_STR";

	public static final String QUALIFIED_COLUMN_DFEMPLOYEENUMBER = "BORROWER.EMPLOYEENUMBER";

	public static final String COLUMN_DFEMPLOYEENUMBER = "EMPLOYEENUMBER";
	
	  public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
		    "BORROWER.INSTITUTIONPROFILEID";
		  public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
	
//	***** Change by NBC Impl. Team - Version 1.1 - End*****//
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	//[[SPIDER_EVENTS BEGIN

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSALUTATION,
				COLUMN_DFSALUTATION,
				QUALIFIED_COLUMN_DFSALUTATION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFIRSTNAME,
				COLUMN_DFFIRSTNAME,
				QUALIFIED_COLUMN_DFFIRSTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIDDLENAME,
				COLUMN_DFMIDDLENAME,
				QUALIFIED_COLUMN_DFMIDDLENAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLASTNAME,
				COLUMN_DFLASTNAME,
				QUALIFIED_COLUMN_DFLASTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFSUFFIX,
                COLUMN_DFSUFFIX,
                QUALIFIED_COLUMN_DFSUFFIX,
                String.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));
        
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPLICANTTYPE,
				COLUMN_DFAPPLICANTTYPE,
				QUALIFIED_COLUMN_DFAPPLICANTTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIMARYBORROWER,
				COLUMN_DFPRIMARYBORROWER,
				QUALIFIED_COLUMN_DFPRIMARYBORROWER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDATEOFBIRTH,
				COLUMN_DFDATEOFBIRTH,
				QUALIFIED_COLUMN_DFDATEOFBIRTH,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMARITALSTATUS,
				COLUMN_DFMARITALSTATUS,
				QUALIFIED_COLUMN_DFMARITALSTATUS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSINNO,
				COLUMN_DFSINNO,
				QUALIFIED_COLUMN_DFSINNO,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCITIZENSHIPSTATUS,
				COLUMN_DFCITIZENSHIPSTATUS,
				QUALIFIED_COLUMN_DFCITIZENSHIPSTATUS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNUMBEROFDEPENDANTS,
				COLUMN_DFNUMBEROFDEPENDANTS,
				QUALIFIED_COLUMN_DFNUMBEROFDEPENDANTS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEXISTINGCLEINT,
				COLUMN_DFEXISTINGCLEINT,
				QUALIFIED_COLUMN_DFEXISTINGCLEINT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFERENCECLIENTNO,
				COLUMN_DFREFERENCECLIENTNO,
				QUALIFIED_COLUMN_DFREFERENCECLIENTNO,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNOOFTIMESBANKRUPT,
				COLUMN_DFNOOFTIMESBANKRUPT,
				QUALIFIED_COLUMN_DFNOOFTIMESBANKRUPT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBANKRUPTCYSTATUS,
				COLUMN_DFBANKRUPTCYSTATUS,
				QUALIFIED_COLUMN_DFBANKRUPTCYSTATUS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTAFFOFLENDER,
				COLUMN_DFSTAFFOFLENDER,
				QUALIFIED_COLUMN_DFSTAFFOFLENDER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFHOMEPHONENO,
				COLUMN_DFHOMEPHONENO,
				QUALIFIED_COLUMN_DFHOMEPHONENO,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFWORKPHONENO,
				COLUMN_DFWORKPHONENO,
				QUALIFIED_COLUMN_DFWORKPHONENO,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCELLPHONENO,
				COLUMN_DFCELLPHONENO,
				QUALIFIED_COLUMN_DFCELLPHONENO,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFAXNUMBER,
				COLUMN_DFFAXNUMBER,
				QUALIFIED_COLUMN_DFFAXNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMAILADDRESS,
				COLUMN_DFEMAILADDRESS,
				QUALIFIED_COLUMN_DFEMAILADDRESS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLANGUAGEPREFERENCE,
				COLUMN_DFLANGUAGEPREFERENCE,
				QUALIFIED_COLUMN_DFLANGUAGEPREFERENCE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERWORKPHONEEXT,
				COLUMN_DFBORROWERWORKPHONEEXT,
				QUALIFIED_COLUMN_DFBORROWERWORKPHONEEXT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCREDITSCORE,
				COLUMN_DFCREDITSCORE,
				QUALIFIED_COLUMN_DFCREDITSCORE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		////FIELD_SCHEMA.addFieldDescriptor(
		////	new QueryFieldDescriptor(
		////		FIELD_SNAPSHOTBORROWERNAME,
		////		COLUMN_SNAPSHOTBORROWERNAME,
		////		QUALIFIED_COLUMN_SNAPSHOTBORROWERNAME,
		////		String.class,
		////		false,
		////		false,
		////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		////		"",
		////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		////		""));

		//// Adjustment of the FieldDescriptor improperly converted by iMT tool for
    //// the ComputedColumn.
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_SNAPSHOTBORROWERNAME,
				COLUMN_SNAPSHOTBORROWERNAME,
				QUALIFIED_COLUMN_SNAPSHOTBORROWERNAME,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"BORROWER.BORROWERLASTNAME ||','|| substr(BORROWER. BORROWERFIRSTNAME,1,1)",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"BORROWER.BORROWERLASTNAME ||','|| substr(BORROWER. BORROWERFIRSTNAME,1,1)"));

		FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
				FIELD_DFSOLICITATION, COLUMN_DFSOLICITATION,
				QUALIFIED_COLUMN_DFSOLICITATION, String.class, false, false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
//		***** Change by NBC Impl. Team - Version 1.1 - Start *****//
		FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
				FIELD_DFGENDER, COLUMN_DFGENDER, QUALIFIED_COLUMN_DFGENDER,
				String.class, false, false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

		FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
				FIELD_DFSMOKER, COLUMN_DFSMOKER, QUALIFIED_COLUMN_DFSMOKER,
				String.class, false, false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

		FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
				FIELD_DFEMPLOYEENUMBER, COLUMN_DFEMPLOYEENUMBER,
				QUALIFIED_COLUMN_DFEMPLOYEENUMBER, String.class, false, false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

		FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
				FIELD_DFPREFERREDMETHODOFCONTACT, COLUMN_DFPREFCONTACTMETHODID,
				QUALIFIED_COLUMN_DFPREFCONTACTMETHODID, String.class, false,
				false, QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"", QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
//		***** Change by NBC Impl. Team - Version 1.1 - End*****//
	    FIELD_SCHEMA.addFieldDescriptor(
	            new QueryFieldDescriptor(
	              FIELD_DFINSTITUTIONID,
	              COLUMN_DFINSTITUTIONID,
	              QUALIFIED_COLUMN_DFINSTITUTIONID,
	              java.math.BigDecimal.class,
	              false,
	              false,
	              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	              "",
	              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	              ""));
	}

}

