package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doDetailViewTDSModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfFirstName();

	
	/**
	 * 
	 * 
	 */
	public void setDfFirstName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfMiddleName();

	
	/**
	 * 
	 * 
	 */
	public void setDfMiddleName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLastName();

	
	/**
	 * 
	 * 
	 */
	public void setDfLastName(String value);

	
    /**
     * 
     * 
     */
    public String getDfSuffix();

    
    /**
     * 
     * 
     */
    public void setDfSuffix(String value);
    
	/**
	 * 
	 * 
	 */
	public String getDfApplicantType();

	
	/**
	 * 
	 * 
	 */
	public void setDfApplicantType(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfTDS(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTDS3Yr();

	
	/**
	 * 
	 * 
	 */
	public void setDfTDS3Yr(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);
	
	  public java.math.BigDecimal getDfInstitutionId();
	  public void setDfInstitutionId(java.math.BigDecimal id);
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFFIRSTNAME="dfFirstName";
	public static final String FIELD_DFMIDDLENAME="dfMiddleName";
	public static final String FIELD_DFLASTNAME="dfLastName";
	public static final String FIELD_DFAPPLICANTTYPE="dfApplicantType";
	public static final String FIELD_DFTDS="dfTDS";
	public static final String FIELD_DFTDS3YR="dfTDS3Yr";
	public static final String FIELD_DFCOPYID="dfCopyId";
	 public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
    public static final String FIELD_DFSUFFIX="dfSuffix";
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

