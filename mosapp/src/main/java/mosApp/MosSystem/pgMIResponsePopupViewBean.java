package mosApp.MosSystem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.Deal;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.jato.ExpressViewBeanBase;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.DisplayField;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

public class pgMIResponsePopupViewBean extends ExpressViewBeanBase {

    private final static Logger logger = 
        LoggerFactory.getLogger(pgMIResponsePopupViewBean.class);
    
    private static final String PAGE_NAME = "pgMIResponsePopup";
    private static final String DEFAULT_DISPLAY_URL = "/mosApp/MosSystem/pgMIResponsePopup.jsp";

    private static final String CHILD_STDEALID = "stDealId";
    private static final String CHILD_STDEALID_RESET_VALUE = "";
    private static final String CHILD_TXMIRESPONSE = "txMIResponse";
    private static final String CHILD_TXMIRESPONSE_RESET_VALUE = "";

    private int dealId;
    private int copyId;
    private int institutionProfileId;
    private int languageId;

    public pgMIResponsePopupViewBean() {
        super(PAGE_NAME);
        setDefaultDisplayURL(DEFAULT_DISPLAY_URL);
        
        PageHandlerCommon handler = new PageHandlerCommon();
        try{
            handler.pageGetState(this);
            PageEntry page = handler.getTheSessionState().getCurrentPage();
            dealId = page.getPageDealId();
            copyId = page.getPageDealCID();
            institutionProfileId = page.getDealInstitutionId();
            languageId = handler.getTheSessionState().getLanguageId();
        }finally{
            handler.unsetSessionResourceKit();
        }
        
    }

    protected View createChild(String name) {
        if (CHILD_STDEALID.equals(name)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STDEALID, CHILD_STDEALID,
                    CHILD_STDEALID_RESET_VALUE, null);
            return child;
        } else if (CHILD_TXMIRESPONSE.equals(name)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TXMIRESPONSE, CHILD_TXMIRESPONSE,
                    CHILD_TXMIRESPONSE_RESET_VALUE, null);
            return child;
        }
        return super.createChild(name);
    }

    public void beginDisplay(DisplayEvent event) throws ModelControlException {

        SessionResourceKit srk = new SessionResourceKit();
        try {
            srk.getExpressState().setDealInstitutionId(this.institutionProfileId);
            
            Deal deal = new Deal(srk, null, this.dealId, this.copyId);
            
            ((DisplayField) this.getChild(CHILD_STDEALID)).setValue(deal.getDealId());
            ((DisplayField) this.getChild(CHILD_TXMIRESPONSE)).setValue(deal.getMortgageInsuranceResponse());
            
            super.beginDisplay(event);
            
        } catch (Exception e) {
            logger.error("Exception occured", e);
        } finally {
            srk.freeResources();
        }
    }
    
    public String getDisplayURL() {
        
        return BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
    }

}
