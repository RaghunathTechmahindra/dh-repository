package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgSourceofBusSearchrpSourceBusinessTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgSourceofBusSearchrpSourceBusinessTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doRowGeneratorModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStSourceName().setValue(CHILD_STSOURCENAME_RESET_VALUE);
		getStSourceFirmResult().setValue(CHILD_STSOURCEFIRMRESULT_RESET_VALUE);
		getStSourceFirmSysType().setValue(CHILD_STSOURCEFIRMSYSTYPE_RESET_VALUE);
		getStSourceCategory().setValue(CHILD_STSOURCECATEGORY_RESET_VALUE);
		getStStatus().setValue(CHILD_STSTATUS_RESET_VALUE);
		getStCity().setValue(CHILD_STCITY_RESET_VALUE);
		getStProvince().setValue(CHILD_STPROVINCE_RESET_VALUE);
		getStPhoneNumber().setValue(CHILD_STPHONENUMBER_RESET_VALUE);
		getStFaxNumber().setValue(CHILD_STFAXNUMBER_RESET_VALUE);
		getHdRowNum().setValue(CHILD_HDROWNUM_RESET_VALUE);
		getBtSelect().setValue(CHILD_BTSELECT_RESET_VALUE);
		getBtDetails().setValue(CHILD_BTDETAILS_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STSOURCENAME,StaticTextField.class);
		registerChild(CHILD_STSOURCEFIRMRESULT,StaticTextField.class);
		registerChild(CHILD_STSOURCEFIRMSYSTYPE,StaticTextField.class);
		registerChild(CHILD_STSOURCECATEGORY,StaticTextField.class);
		registerChild(CHILD_STSTATUS,StaticTextField.class);
		registerChild(CHILD_STCITY,StaticTextField.class);
		registerChild(CHILD_STPROVINCE,StaticTextField.class);
		registerChild(CHILD_STPHONENUMBER,StaticTextField.class);
		registerChild(CHILD_STFAXNUMBER,StaticTextField.class);
		registerChild(CHILD_HDROWNUM,HiddenField.class);
		registerChild(CHILD_BTSELECT,Button.class);
		registerChild(CHILD_BTDETAILS,Button.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoRowGeneratorModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    SourceBusinessSearchHandler handler =(SourceBusinessSearchHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.checkAndSyncDOWithCursorInfo(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
      SourceBusinessSearchHandler handler =(SourceBusinessSearchHandler) this.handler.cloneSS();

      handler.pageGetState(this.getParentViewBean());

      boolean rc = handler.populateRepeatedFields(getTileIndex());

      handler.pageSaveState();
		}

		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STSOURCENAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STSOURCENAME,
				CHILD_STSOURCENAME,
				CHILD_STSOURCENAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSOURCEFIRMRESULT))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STSOURCEFIRMRESULT,
				CHILD_STSOURCEFIRMRESULT,
				CHILD_STSOURCEFIRMRESULT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSOURCEFIRMSYSTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STSOURCEFIRMSYSTYPE,
				CHILD_STSOURCEFIRMSYSTYPE,
				CHILD_STSOURCEFIRMSYSTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSOURCECATEGORY))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STSOURCECATEGORY,
				CHILD_STSOURCECATEGORY,
				CHILD_STSOURCECATEGORY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STSTATUS,
				CHILD_STSTATUS,
				CHILD_STSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCITY))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCITY,
				CHILD_STCITY,
				CHILD_STCITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROVINCE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPROVINCE,
				CHILD_STPROVINCE,
				CHILD_STPROVINCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPHONENUMBER))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPHONENUMBER,
				CHILD_STPHONENUMBER,
				CHILD_STPHONENUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STFAXNUMBER))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STFAXNUMBER,
				CHILD_STFAXNUMBER,
				CHILD_STFAXNUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDROWNUM))
		{
			HiddenField child = new HiddenField(this,
				getdoRowGeneratorModel(),
				CHILD_HDROWNUM,
				doRowGeneratorModel.FIELD_DFROWNDX,
				CHILD_HDROWNUM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTSELECT))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSELECT,
				CHILD_BTSELECT,
				CHILD_BTSELECT_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTDETAILS))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDETAILS,
				CHILD_BTDETAILS,
				CHILD_BTDETAILS_RESET_VALUE,
				null);
				return child;

		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSourceName()
	{
		return (StaticTextField)getChild(CHILD_STSOURCENAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSourceFirmResult()
	{
		return (StaticTextField)getChild(CHILD_STSOURCEFIRMRESULT);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSourceFirmSysType()
	{
		return (StaticTextField)getChild(CHILD_STSOURCEFIRMSYSTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSourceCategory()
	{
		return (StaticTextField)getChild(CHILD_STSOURCECATEGORY);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStStatus()
	{
		return (StaticTextField)getChild(CHILD_STSTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCity()
	{
		return (StaticTextField)getChild(CHILD_STCITY);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStProvince()
	{
		return (StaticTextField)getChild(CHILD_STPROVINCE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPhoneNumber()
	{
		return (StaticTextField)getChild(CHILD_STPHONENUMBER);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStFaxNumber()
	{
		return (StaticTextField)getChild(CHILD_STFAXNUMBER);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdRowNum()
	{
		return (HiddenField)getChild(CHILD_HDROWNUM);
	}


	/**
	 *
	 *
	 */
	public void handleBtSelectRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		SourceBusinessSearchHandler handler =(SourceBusinessSearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this.getParentViewBean());

		handler.handleSelectSourceBusiness(handler.getRowNdxFromWebEventMethod(event));

		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtSelect()
	{
		return (Button)getChild(CHILD_BTSELECT);
	}


	/**
	 *
	 *
	 */
	public String endBtSelectDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btSelect_onBeforeHtmlOutputEvent method
		SourceBusinessSearchHandler handler =(SourceBusinessSearchHandler) this.handler.cloneSS();

		handler.pageGetState(getParentViewBean());

		boolean rc = handler.handleViewSelectButton();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public void handleBtDetailsRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		SourceBusinessSearchHandler handler =(SourceBusinessSearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this.getParentViewBean());

		handler.handleDetailSourceBusiness(handler.getRowNdxFromWebEventMethod(event));

		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtDetails()
	{
		return (Button)getChild(CHILD_BTDETAILS);
	}


	/**
	 *
	 *
	 */
	public doRowGeneratorModel getdoRowGeneratorModel()
	{
		if (doRowGenerator == null)
			doRowGenerator = (doRowGeneratorModel) getModel(doRowGeneratorModel.class);
		return doRowGenerator;
	}


	/**
	 *
	 *
	 */
	public void setdoRowGeneratorModel(doRowGeneratorModel model)
	{
			doRowGenerator = model;
	}

  //--> New method to manually set the current Display Offset
  //--> Current in JATO there is no method to mamually set the display offset, that's why
  //--> we have to make our own method.
  //--> Note : This method may need to be implemented in all TiledViews with Forward, Backward buttons.
  //--> By Billy 22July2002
  public void setCurrentDisplayOffset(int offset) throws Exception
  {
    setWebActionModelOffset(offset);
    handleWebAction(WebActions.ACTION_REFRESH);
  }
  //=====================================================================================


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STSOURCENAME="stSourceName";
	public static final String CHILD_STSOURCENAME_RESET_VALUE="";
	public static final String CHILD_STSOURCEFIRMRESULT="stSourceFirmResult";
	public static final String CHILD_STSOURCEFIRMRESULT_RESET_VALUE="";
	public static final String CHILD_STSOURCEFIRMSYSTYPE="stSourceFirmSysType";
	public static final String CHILD_STSOURCEFIRMSYSTYPE_RESET_VALUE="";
	public static final String CHILD_STSOURCECATEGORY="stSourceCategory";
	public static final String CHILD_STSOURCECATEGORY_RESET_VALUE="";
	public static final String CHILD_STSTATUS="stStatus";
	public static final String CHILD_STSTATUS_RESET_VALUE="";
	public static final String CHILD_STCITY="stCity";
	public static final String CHILD_STCITY_RESET_VALUE="";
	public static final String CHILD_STPROVINCE="stProvince";
	public static final String CHILD_STPROVINCE_RESET_VALUE="";
	public static final String CHILD_STPHONENUMBER="stPhoneNumber";
	public static final String CHILD_STPHONENUMBER_RESET_VALUE="";
	public static final String CHILD_STFAXNUMBER="stFaxNumber";
	public static final String CHILD_STFAXNUMBER_RESET_VALUE="";
	public static final String CHILD_HDROWNUM="hdRowNum";
	public static final String CHILD_HDROWNUM_RESET_VALUE="";
	public static final String CHILD_BTSELECT="btSelect";
	public static final String CHILD_BTSELECT_RESET_VALUE="Custom";
	public static final String CHILD_BTDETAILS="btDetails";
	public static final String CHILD_BTDETAILS_RESET_VALUE="Custom";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doRowGeneratorModel doRowGenerator=null;
	private SourceBusinessSearchHandler handler=new SourceBusinessSearchHandler();

}

