package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;
import com.basis100.picklist.BXResources;

/**
 *
 *
 *
 */
public class pgUserAdministrationRepeated1TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgUserAdministrationRepeated1TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(100);
		setPrimaryModel((DatasetModel) getDefaultModel() );
		registerChildren();
		initialize();
	}

	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}

	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStScreenName().setValue(CHILD_STSCREENNAME_RESET_VALUE);
		getStOverideIndicator().setValue(CHILD_STOVERIDEINDICATOR_RESET_VALUE);
		getCbPageAccessTypes().setValue(CHILD_CBPAGEACCESSTYPES_RESET_VALUE);
		getStBlankSpace().setValue(CHILD_STBLANKSPACE_RESET_VALUE);
		getHdPageId().setValue(CHILD_HDPAGEID_RESET_VALUE);
	}

	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STSCREENNAME,StaticTextField.class);
		registerChild(CHILD_STOVERIDEINDICATOR,StaticTextField.class);
		registerChild(CHILD_CBPAGEACCESSTYPES,ComboBox.class);
		registerChild(CHILD_STBLANKSPACE,StaticTextField.class);
		registerChild(CHILD_HDPAGEID,HiddenField.class);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}

	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if(getPrimaryModel() == null)
		{
			throw new ModelControlException("Primary model is null");
		}

		// The following code block was migrated from the Repeated1_onBeforeDisplayEvent method
		userAdminHandler handler =(userAdminHandler) this.handler.cloneSS();
		handler.pageGetState(getParentViewBean());
		handler.setupNumOfRepeatedRows(this);

    //Set up all ComboBox options
    cbPageAccessTypesOptions.populate(getRequestContext());

		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();

	}

	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
      // The following code block was migrated from the Repeated1_onBeforeRowDisplayEvent method
			userAdminHandler handler =(userAdminHandler) this.handler.cloneSS();
		  handler.pageGetState(getParentViewBean());
		  boolean retval = handler.populateRepeatedDisplayFields(getTileIndex());
		  handler.pageSaveState();

		  return(retval);
		}
		return movedToRow;
  }

	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}

	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}

	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}

	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}

	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STSCREENNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STSCREENNAME,
				CHILD_STSCREENNAME,
				CHILD_STSCREENNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STOVERIDEINDICATOR))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STOVERIDEINDICATOR,
				CHILD_STOVERIDEINDICATOR,
				CHILD_STOVERIDEINDICATOR_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPAGEACCESSTYPES))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBPAGEACCESSTYPES,
				CHILD_CBPAGEACCESSTYPES,
				CHILD_CBPAGEACCESSTYPES_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbPageAccessTypesOptions);
			return child;
		}
		else
		if (name.equals(CHILD_STBLANKSPACE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBLANKSPACE,
				CHILD_STBLANKSPACE,
				CHILD_STBLANKSPACE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDPAGEID))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_HDPAGEID,
				CHILD_HDPAGEID,
				CHILD_HDPAGEID_RESET_VALUE,
				null);
			return child;
		}
		else
		{
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	        }
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStScreenName()
	{
		return (StaticTextField)getChild(CHILD_STSCREENNAME);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStOverideIndicator()
	{
		return (StaticTextField)getChild(CHILD_STOVERIDEINDICATOR);
	}

	/**
	 *
	 *
	 */
	public ComboBox getCbPageAccessTypes()
	{
		return (ComboBox)getChild(CHILD_CBPAGEACCESSTYPES);
	}

	/**
	 *
	 *
	 */
	static class CbPageAccessTypesOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbPageAccessTypesOptionList()
		{

		}

		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 27Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(
                theSessionState.getDealInstitutionId(),"ACCESSTYPE", languageId);

        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
    }
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStBlankSpace()
	{
		return (StaticTextField)getChild(CHILD_STBLANKSPACE);
	}

	/**
	 *
	 *
	 */
	public HiddenField getHdPageId()
	{
		return (HiddenField)getChild(CHILD_HDPAGEID);
	}

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STSCREENNAME="stScreenName";
	public static final String CHILD_STSCREENNAME_RESET_VALUE="";
	public static final String CHILD_STOVERIDEINDICATOR="stOverideIndicator";
	public static final String CHILD_STOVERIDEINDICATOR_RESET_VALUE="";
	public static final String CHILD_CBPAGEACCESSTYPES="cbPageAccessTypes";
	public static final String CHILD_CBPAGEACCESSTYPES_RESET_VALUE="";

  //--Release2.1--//
	//private static CbPageAccessTypesOptionList cbPageAccessTypesOptions=new CbPageAccessTypesOptionList();
  private CbPageAccessTypesOptionList cbPageAccessTypesOptions=new CbPageAccessTypesOptionList();
	public static final String CHILD_STBLANKSPACE="stBlankSpace";
	public static final String CHILD_STBLANKSPACE_RESET_VALUE="";
	public static final String CHILD_HDPAGEID="hdPageId";
	public static final String CHILD_HDPAGEID_RESET_VALUE="";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

  private userAdminHandler handler=new userAdminHandler();
}
