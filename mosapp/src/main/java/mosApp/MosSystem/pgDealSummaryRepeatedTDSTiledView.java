package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgDealSummaryRepeatedTDSTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealSummaryRepeatedTDSTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(100);
		setPrimaryModelClass( doDetailViewTDSModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStApplicantLastNameTDS().setValue(CHILD_STAPPLICANTLASTNAMETDS_RESET_VALUE);
		getStApplicantFirstNameTDS().setValue(CHILD_STAPPLICANTFIRSTNAMETDS_RESET_VALUE);
		getStApplicantInitialTDS().setValue(CHILD_STAPPLICANTINITIALTDS_RESET_VALUE);
		getStApplicantSuffixTDS().setValue(CHILD_STAPPLICANTSUFFIXTDS_RESET_VALUE);
		getStApplicantTypeTDS().setValue(CHILD_STAPPLICANTTYPETDS_RESET_VALUE);
		getStTDS().setValue(CHILD_STTDS_RESET_VALUE);
		getSt3YrTDS().setValue(CHILD_ST3YRTDS_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STAPPLICANTLASTNAMETDS,StaticTextField.class);
		registerChild(CHILD_STAPPLICANTFIRSTNAMETDS,StaticTextField.class);
		registerChild(CHILD_STAPPLICANTINITIALTDS,StaticTextField.class);
		registerChild(CHILD_STAPPLICANTTYPETDS,StaticTextField.class);
		registerChild(CHILD_STTDS,StaticTextField.class);
		registerChild(CHILD_ST3YRTDS,StaticTextField.class);
		registerChild(CHILD_STAPPLICANTSUFFIXTDS,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDetailViewTDSModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		// The following code block was migrated from the RepeatedTDS_onBeforeDisplayEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.setupNumOfRepeatedTDS(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STAPPLICANTLASTNAMETDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewTDSModel(),
				CHILD_STAPPLICANTLASTNAMETDS,
				doDetailViewTDSModel.FIELD_DFLASTNAME,
				CHILD_STAPPLICANTLASTNAMETDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPLICANTFIRSTNAMETDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewTDSModel(),
				CHILD_STAPPLICANTFIRSTNAMETDS,
				doDetailViewTDSModel.FIELD_DFFIRSTNAME,
				CHILD_STAPPLICANTFIRSTNAMETDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPLICANTINITIALTDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewTDSModel(),
				CHILD_STAPPLICANTINITIALTDS,
				doDetailViewTDSModel.FIELD_DFMIDDLENAME,
				CHILD_STAPPLICANTINITIALTDS_RESET_VALUE,
				null);
			return child;
		}
		if (name.equals(CHILD_STAPPLICANTSUFFIXTDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewTDSModel(),
				CHILD_STAPPLICANTSUFFIXTDS,
				doDetailViewTDSModel.FIELD_DFSUFFIX,
				CHILD_STAPPLICANTSUFFIXTDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPLICANTTYPETDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewTDSModel(),
				CHILD_STAPPLICANTTYPETDS,
				doDetailViewTDSModel.FIELD_DFAPPLICANTTYPE,
				CHILD_STAPPLICANTTYPETDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewTDSModel(),
				CHILD_STTDS,
				doDetailViewTDSModel.FIELD_DFTDS,
				CHILD_STTDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_ST3YRTDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewTDSModel(),
				CHILD_ST3YRTDS,
				doDetailViewTDSModel.FIELD_DFTDS3YR,
				CHILD_ST3YRTDS_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApplicantLastNameTDS()
	{
		return (StaticTextField)getChild(CHILD_STAPPLICANTLASTNAMETDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApplicantFirstNameTDS()
	{
		return (StaticTextField)getChild(CHILD_STAPPLICANTFIRSTNAMETDS);
	}

	
	/**
	 *
	 *
	 */
	public StaticTextField getStApplicantSuffixTDS()
	{
		return (StaticTextField)getChild(CHILD_STAPPLICANTSUFFIXTDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApplicantInitialTDS()
	{
		return (StaticTextField)getChild(CHILD_STAPPLICANTINITIALTDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApplicantTypeTDS()
	{
		return (StaticTextField)getChild(CHILD_STAPPLICANTTYPETDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTDS()
	{
		return (StaticTextField)getChild(CHILD_STTDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getSt3YrTDS()
	{
		return (StaticTextField)getChild(CHILD_ST3YRTDS);
	}


	/**
	 *
	 *
	 */
	public doDetailViewTDSModel getdoDetailViewTDSModel()
	{
		if (doDetailViewTDS == null)
			doDetailViewTDS = (doDetailViewTDSModel) getModel(doDetailViewTDSModel.class);
		return doDetailViewTDS;
	}


	/**
	 *
	 *
	 */
	public void setdoDetailViewTDSModel(doDetailViewTDSModel model)
	{
			doDetailViewTDS = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STAPPLICANTLASTNAMETDS="stApplicantLastNameTDS";
	public static final String CHILD_STAPPLICANTLASTNAMETDS_RESET_VALUE="";
	public static final String CHILD_STAPPLICANTFIRSTNAMETDS="stApplicantFirstNameTDS";
	public static final String CHILD_STAPPLICANTFIRSTNAMETDS_RESET_VALUE="";
	public static final String CHILD_STAPPLICANTINITIALTDS="stApplicantInitialTDS";
	public static final String CHILD_STAPPLICANTINITIALTDS_RESET_VALUE="";
	public static final String CHILD_STAPPLICANTSUFFIXTDS="stApplicantSuffixTDS";
	public static final String CHILD_STAPPLICANTSUFFIXTDS_RESET_VALUE="";
	public static final String CHILD_STAPPLICANTTYPETDS="stApplicantTypeTDS";
	public static final String CHILD_STAPPLICANTTYPETDS_RESET_VALUE="";
	public static final String CHILD_STTDS="stTDS";
	public static final String CHILD_STTDS_RESET_VALUE="";
	public static final String CHILD_ST3YRTDS="st3YrTDS";
	public static final String CHILD_ST3YRTDS_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDetailViewTDSModel doDetailViewTDS=null;
  private DealSummaryHandler handler=new DealSummaryHandler();

}

