package mosApp.MosSystem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.ChildDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.html.StaticTextField;


/**
 *
 *
 *
 */
public class pgDealProgressRepeated1TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealProgressRepeated1TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doDealProgressTasksModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStTaskDescription().setValue(CHILD_STTASKDESCRIPTION_RESET_VALUE);
		getStTaskStatus().setValue(CHILD_STTASKSTATUS_RESET_VALUE);
		getStTaskPriority().setValue(CHILD_STTASKPRIORITY_RESET_VALUE);
		getStTaskDueDate().setValue(CHILD_STTASKDUEDATE_RESET_VALUE);
		getStTimeStatus().setValue(CHILD_STTIMESTATUS_RESET_VALUE);
		getStAssignedTo().setValue(CHILD_STASSIGNEDTO_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STTASKDESCRIPTION,StaticTextField.class);
		registerChild(CHILD_STTASKSTATUS,StaticTextField.class);
		registerChild(CHILD_STTASKPRIORITY,StaticTextField.class);
		registerChild(CHILD_STTASKDUEDATE,StaticTextField.class);
		registerChild(CHILD_STTIMESTATUS,StaticTextField.class);
		registerChild(CHILD_STASSIGNEDTO,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDealProgressTasksModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			DealProgressHandler handler =(DealProgressHandler) this.handler.cloneSS();
			handler.pageGetState(this.getParentViewBean());
			handler.populatedRepeatedFields(this.getTileIndex());
			handler.pageSaveState();
		}

		return movedToRow;

	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STTASKDESCRIPTION))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealProgressTasksModel(),
				CHILD_STTASKDESCRIPTION,
				doDealProgressTasksModel.FIELD_DFTASK_TASKLABEL,
				CHILD_STTASKDESCRIPTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTASKSTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealProgressTasksModel(),
				CHILD_STTASKSTATUS,
				doDealProgressTasksModel.FIELD_DFTASKSTATUS_TSDESCRIPTION,
				CHILD_STTASKSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTASKPRIORITY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealProgressTasksModel(),
				CHILD_STTASKPRIORITY,
				doDealProgressTasksModel.FIELD_DFPRIORITY_PDESCRIPTION,
				CHILD_STTASKPRIORITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTASKDUEDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealProgressTasksModel(),
				CHILD_STTASKDUEDATE,
				doDealProgressTasksModel.FIELD_DFDUETIMESTAMP,
				CHILD_STTASKDUEDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTIMESTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealProgressTasksModel(),
				CHILD_STTIMESTATUS,
				doDealProgressTasksModel.FIELD_DFTMDESCRIPTION,
				CHILD_STTIMESTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STASSIGNEDTO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STASSIGNEDTO,
				CHILD_STASSIGNEDTO,
				CHILD_STASSIGNEDTO_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTaskDescription()
	{
		return (StaticTextField)getChild(CHILD_STTASKDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTaskStatus()
	{
		return (StaticTextField)getChild(CHILD_STTASKSTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTaskPriority()
	{
		return (StaticTextField)getChild(CHILD_STTASKPRIORITY);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTaskDueDate()
	{
		return (StaticTextField)getChild(CHILD_STTASKDUEDATE);
	}


	/**
	 *
	 *
	 */
	public boolean beginStTaskDueDateDisplay(ChildDisplayEvent event)
	{
		//Object value = getStTaskDueDate().getValue();
    DealProgressHandler handler =(DealProgressHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.convertToUserTimeZone(this.getParentViewBean(), "Repeated1/stTaskDueDate");
		handler.pageSaveState();
		return true;

	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTimeStatus()
	{
		return (StaticTextField)getChild(CHILD_STTIMESTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAssignedTo()
	{
		return (StaticTextField)getChild(CHILD_STASSIGNEDTO);
	}


	/**
	 *
	 *
	 */
	public doDealProgressTasksModel getdoDealProgressTasksModel()
	{
		if (doDealProgressTasks == null)
			doDealProgressTasks = (doDealProgressTasksModel) getModel(doDealProgressTasksModel.class);
		return doDealProgressTasks;
	}


	/**
	 *
	 *
	 */
	public void setdoDealProgressTasksModel(doDealProgressTasksModel model)
	{
			doDealProgressTasks = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STTASKDESCRIPTION="stTaskDescription";
	public static final String CHILD_STTASKDESCRIPTION_RESET_VALUE="";
	public static final String CHILD_STTASKSTATUS="stTaskStatus";
	public static final String CHILD_STTASKSTATUS_RESET_VALUE="";
	public static final String CHILD_STTASKPRIORITY="stTaskPriority";
	public static final String CHILD_STTASKPRIORITY_RESET_VALUE="";
	public static final String CHILD_STTASKDUEDATE="stTaskDueDate";
	public static final String CHILD_STTASKDUEDATE_RESET_VALUE="";
	public static final String CHILD_STTIMESTATUS="stTimeStatus";
	public static final String CHILD_STTIMESTATUS_RESET_VALUE="";
	public static final String CHILD_STASSIGNEDTO="stAssignedTo";
	public static final String CHILD_STASSIGNEDTO_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDealProgressTasksModel doDealProgressTasks=null;

  private DealProgressHandler handler=new DealProgressHandler();


}

