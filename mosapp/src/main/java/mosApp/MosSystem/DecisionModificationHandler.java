package mosApp.MosSystem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.Locale;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.commitment.CCM;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealHistory;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.util.DBA;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.ComboBox;


/**
 *
 *
 */
public class DecisionModificationHandler extends PageHandlerCommon
	implements Cloneable, Sc
{
	/**
	 *
	 *
	 */
	public DecisionModificationHandler()
	{
		super();
	}


	/**
	 *
	 *
	 */
	public DecisionModificationHandler(PageHandlerCommon other)
	{
		super();
		initFromOther(other);

	}


	/**
	 *
	 *
	 */
	public DecisionModificationHandler cloneSS()
	{
		return(DecisionModificationHandler) super.cloneSafeShallow();

	}


	//This method is used to populate the fields in the header
	//with the appropriate information

	public void populatePageDisplayFields()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		populatePageShellDisplayFields();

		// Quick Link Menu display
		displayQuickLinkMenu();

		populateTaskNavigator(pg);

		populatePageDealSummarySnapShot();

		populatePreviousPagesLinks();


		/// CUSTOM POPULATION GOES HERE (e.g. set page specific display fields, etc)
		showPageFields(pg);

	}


	//////////////////////////////////////////////////////////////////////////

	private void showPageFields(PageEntry pg)
	{
		int dealId = pg.getPageDealId();

		int copyId = pg.getPageDealCID();

		Deal deal = null;

		try
		{
			deal = DBA.getDeal(getSessionResourceKit(), dealId, copyId);
			Hashtable pst = pg.getPageStateTable();
			if (pst == null)
			{
				pst = new Hashtable();
				pst.put("SELECTEDECMODTYPE", new Integer(0));
				pg.setPageStateTable(pst);
			}
			Integer selval =(Integer) pst.get("SELECTEDECMODTYPE");
			ComboBox cbxdecmodtype =(ComboBox) getCurrNDPage().getDisplayField("cbDecisionModificationType");
      String decmodBoxValue = (String) cbxdecmodtype.getValue();

      ////cbxdecmodtype.select(selval).intValue();

      int decmodNdx = 0;

      try
      {
        decmodNdx = (new Integer(decmodBoxValue)).intValue();
        logger.debug("DMH@showPageFields::SortBoxIndex: " + decmodNdx);

      }
      catch(Exception e)
      {
        //// Any Problem default to 0 (default ioption)
        decmodNdx = 0;
        logger.debug("DMH@showPageFields::Default decmodNdx option");
      }
		}
		catch(Exception exc)
		{
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Decision Modification :  error in getting deal object!!!  Message: " + exc.getMessage());
			logger.error(exc);
			return;
		}

    //--Release2.1--//
    //  Modified to use respective locale
    //  This ONLY takes into account ENGLISH and FRENCH.
		SimpleDateFormat formatter = new SimpleDateFormat(" MMM dd, yyyy ",
      (this.theSessionState.getLanguageId() == Mc.LANGUAGE_PREFERENCE_FRENCH ?
       Locale.CANADA_FRENCH :
       Locale.ENGLISH));
    //--------------//

		//=================================================================
		//   commitment issue date
		//=================================================================
		java.util.Date issueDate = deal.getCommitmentIssueDate();

		if (issueDate == null)
		{
			getCurrNDPage().setDisplayFieldValue("stDealIssueDate", new String("-"));
		}
		else
		{
			getCurrNDPage().setDisplayFieldValue("stDealIssueDate", new String(formatter.format(issueDate)));
		}


		//=================================================================
		//   commitment expiration date
		//=================================================================
		java.util.Date expirationDate = deal.getCommitmentExpirationDate();

		if (expirationDate == null)
		{
			getCurrNDPage().setDisplayFieldValue("stDealExpirationDate", new String("-"));
		}
		else
		{
			getCurrNDPage().setDisplayFieldValue("stDealExpirationDate", new String(formatter.format(expirationDate)));
		}


		//=================================================================
		//	commitment accept date
		//=================================================================
		java.util.Date acceptDate = deal.getCommitmentAcceptDate();

		if (acceptDate == null)
		{
			getCurrNDPage().setDisplayFieldValue("stDealAcceptDate", new String("-"));
		}
		else
		{
			getCurrNDPage().setDisplayFieldValue("stDealAcceptDate", new String(formatter.format(acceptDate)));
		}


		//=================================================================
		//	primary property address
		//=================================================================
		try
		{
			Integer filterDealId = new Integer(deal.getDealId());
			Integer filterCopyId = new Integer(deal.getCopyId());
			String filterPrimaryType = new String("Y");
			logger.trace("DealId & CopyId @ find property address " + filterDealId + "," + filterCopyId);
      doPropertyAddressModelImpl propAddr = (doPropertyAddressModelImpl)
                                      (RequestManager.getRequestContext().getModelManager().getModel(doPropertyAddressModel.class));

			propAddr.clearUserWhereCriteria();
			propAddr.addUserWhereCriterion("dfPropertyDealId", "=", filterDealId);
			propAddr.addUserWhereCriterion("dfPropertyCopyId", "=", filterCopyId);
			propAddr.addUserWhereCriterion("dfPrimaryPropertyFlag", "=", filterPrimaryType);

			////propAddr.execute();
      try
      {
        ResultSet rs = propAddr.executeSelect(null);

        if(rs == null || propAddr.getSize() < 0)
        {
          logger.debug("MWQ@showPageFields::No row returned!!");
        }
        else if(propAddr.getSize() == 0)
        {
          getCurrNDPage().setDisplayFieldValue("stStreetNumber", new String("-"));
          getCurrNDPage().setDisplayFieldValue("stStreetName", new String("-"));
          getCurrNDPage().setDisplayFieldValue("stStreetType", new String("-"));
          getCurrNDPage().setDisplayFieldValue("stStreetDirection", new String("-"));
          getCurrNDPage().setDisplayFieldValue("stAddressLine2", new String("-"));
        }
      }
      catch (ModelControlException mce)
      {
        logger.debug("MWQ@showPageFields::MCEException: " + mce);
      }

      catch (SQLException sqle)
      {
        logger.debug("MWQ@showPageFields::SQLException: " + sqle);
      }

			////if (propAddr.getLastResults().getNumRows() == 0)
			////{
			////	getCurrNDPage().setDisplayFieldValue("stStreetNumber", new String("-"));
			////	getCurrNDPage().setDisplayFieldValue("stStreetName", new String("-"));
			////	getCurrNDPage().setDisplayFieldValue("stStreetType", new String("-"));
			////	getCurrNDPage().setDisplayFieldValue("stStreetDirection", new String("-"));
			////	getCurrNDPage().setDisplayFieldValue("stAddressLine2", new String("-"));
			////}
		}
		catch(Exception ex)
		{
			logger.error("MWQ@showPageFields:: Exception at finding property address.");
			return;
		}

	}


	//////////////////////////////////////////////////////////////////////////
	//
	// Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
	//

	public void setupBeforePageGeneration()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		if (pg.getSetupBeforeGenerationCalled() == true) return;

		pg.setSetupBeforeGenerationCalled(true);


		// -- //
		setupDealSummarySnapShotDO(pg);

	}


	public void handleCustomActMessageOk(String[] args)
	{
          //--CervusPhaseII--start--//
          PageEntry pg = theSessionState.getCurrentPage();
          SessionResourceKit srk = getSessionResourceKit();
          ViewBean thePage = getCurrNDPage();

          if (args [ 0 ].equals("OVERRIDE_DEAL_LOCK_YES"))
          {
            logger.trace("DMH@handleCustomActMessageOk_REJECT_COND_YES: Override deal lock");
            provideOverrideDealLockActivity(pg, srk, thePage);
            return;
          }

          if (args [ 0 ].equals("OVERRIDE_DEAL_LOCK_NO"))
          {
            logger.trace("DMH@handleCustomActMessageOk_REJECT_COND_NO: Return to previous screen");

            theSessionState.setActMessage(null);
            theSessionState.overrideDealLock(true);
            handleCancelStandard(false);

            return;
          }
          //--CervusPhaseII--end--//

          // only dialog is confirmation of re-underwrite
          handleSubmit(true, Mc.DM_COMPLETE_RE_UWR_REQUESTED);
          return;
	}


	//////////////////////////////////////////////////////////////////////////

	public void handleSubmit()
	{
		handleSubmit(false, - 1);

	}


	/**
	 *
	 *
	 */
	public void handleSubmit(boolean confirmed, int selVal)
	{
		if (selVal == - 1) selVal = getSelectedDecModif();

		SessionResourceKit srk = getSessionResourceKit();

		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		pst.put("SELECTEDECMODTYPE", new Integer(selVal));

		try
		{
			// first any validation of request
			switch(selVal)
			{
				case Mc.DM_COMPLETE_RE_UWR_REQUESTED : if (confirmed == false)
				{
					// Check if action allowed
					CCM ccm = new CCM(srk);
					Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
					if (ccm.checkIfActionAllowed(deal))
            setActiveMessageToAlert(BXResources.getSysMsg("DEC_MOD_REUNDERWRITING_REQUEST", theSessionState.getLanguageId()),
              ActiveMsgFactory.ISCUSTOMDIALOG);
					else
            setActiveMessageToAlert(BXResources.getSysMsg("CCM_ACTION_NOT_ALLOWED_MI_CHECK_FAILED", theSessionState.getLanguageId()),
              ActiveMsgFactory.ISCUSTOMCONFIRM);
				}
				break;
			}

			// perform the request
			perfromDecisionModificationRequest(selVal, pg, srk, true);
		}
		catch(Exception exc1)
		{
			srk.cleanTransaction();
			logger.error("Exception @DecisionModificationHandler@handleSubmit: case = " + selVal);
			logger.error(exc1);
			setStandardFailMessage();
		}

	}

  //// SYNCADD.
  //// Added SOB Resubmission handling -- By Billy 12June2002.
	/**
	 *
	 *
	 */
	public void perfromDecisionModificationRequest(int selVal, PageEntry pg, SessionResourceKit srk, boolean navigateToDealNote)
		throws Exception
	{
      perfromDecisionModificationRequest(selVal, pg, srk, navigateToDealNote, false);
  }
	/**
	 *
	 *
	 */
	public void perfromDecisionModificationRequest(int selVal, PageEntry pg, SessionResourceKit srk, boolean navigateToDealNote, boolean clearResubmission)
		throws Exception
	{
		Deal deal = null;

		try
		{
			srk.beginTransaction();
			standardAdoptTxCopy(pg, true);
			deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

      //// SYNCADD.
      //// Added SOB Resubmission handling -- By Billy 12June2002.
      if(clearResubmission == true)
        deal.setResubmissionFlag("N");
		}
		catch(Exception exc)
		{
			throw new Exception("Exception getting deal information, dealId = " + pg.getPageDealId() + ", copyId = " + pg.getPageDealCID());
		}

		DealHistory hLog = null;

		switch(selVal)
		{
			case Mc.DM_COMPLETE_RE_UWR_REQUESTED :
// ALERT - APPEARS TO BE MISSING HISTORY LOG (as in other cases)
			deal.setDecisionModificationTypeId(Mc.DM_NO_MOD_REQUESTED);
			deal.setCommitmentProduced("N");
			CCM ccm = new CCM(getSessionResourceKit());
			ccm.cancelCommitment(deal, false, 'P');
			deal.ejbStore();
	  /*
	   * NBC Implementation: Support for RFD -start
	   */		
    	  //check if lender profile supports RFD
	     	LenderProfile dealLender = deal.getLenderProfile();
	     	boolean rfdSupported = false;
	     	
	      if (dealLender != null){
	     	   rfdSupported = dealLender.isSupportRateFloatDown();
	      }
	     	if (rfdSupported)
	        {
	    	   // set floatdowncompletedflag on deal inorder to close the RFD Task
	     	   deal.setFloatDownCompletedFlag("Y");
	           deal.ejbStore();
	           
	           // Trigger workflow to close the existing RFD task
			workflowTrigger(2, srk, pg, null);

	     	   deal.setFloatDownTaskCreatedFlag("N");
	     	   deal.setFloatDownCompletedFlag("N");
	        
	           deal.ejbStore();

	      }
	       else
	           {
	            // RFD is not supported, 
	            workflowTrigger(2, srk, pg, null);
	       }
	  /*
	   * NBC Implementation: Support for RFD - End
	   */ 	

			srk.commitTransaction();
			if (navigateToDealNote) navigateToDealNotes(pg,
        BXResources.getSysMsg("DEC_MOD_ENTER_RECEIVED_STATUS_NOTE", theSessionState.getLanguageId()));
			break;
			case Mc.DM_NON_CRITICAL_NO_DOC_PREP : deal.setDecisionModificationTypeId(Mc.DM_NON_CRITICAL_NO_DOC_PREP);
			deal.setCommitmentProduced("Y");
			deal.ejbStore();
			hLog = new DealHistory(srk);
			hLog.create(deal.getDealId(), srk.getExpressState().getUserProfileId(), Mc.DEAL_TX_TYPE_DATA_CHANGE, deal.getStatusId(), "Dec Mod: Non Critical Change with No Doc Prep. ", new java.util.Date());
			hLog.ejbStore();
			workflowTrigger(2, srk, pg, null);
			srk.commitTransaction();
			if (navigateToDealNote) navigateToDealNotes(pg,
        BXResources.getSysMsg("DEC_MOD_ENTER_NON_CRITICAL_CHANGE_NOTE", theSessionState.getLanguageId()));
			break;
			case Mc.DM_NON_CRITICAL_DOC_PREP : deal.setDecisionModificationTypeId(Mc.DM_NON_CRITICAL_DOC_PREP);
			deal.setCommitmentProduced("N");
			deal.ejbStore();
			hLog = new DealHistory(srk);
			hLog.create(deal.getDealId(), srk.getExpressState().getUserProfileId(), Mc.DEAL_TX_TYPE_DATA_CHANGE, deal.getStatusId(), "Dec Mod: Non Critical Change with Doc Prep. ", new java.util.Date());
			hLog.ejbStore();
			workflowTrigger(2, srk, pg, null);
			srk.commitTransaction();
			if (navigateToDealNote) navigateToDealNotes(pg,
        BXResources.getSysMsg("DEC_MOD_ENTER_NON_CRITICAL_CHANGE_NOTE", theSessionState.getLanguageId()));
			break;
            // SEAN Ticket #1351 : handle the default case, just invoke the cancel button's behavior.
            default :
                //throw new Exception("Invalid Deal Modification Type  = " + selVal);
                srk.commitTransaction();
                handleCancelStandard(false);
                break;
            // SEAN Ticket #1351 END
		}

	}


	//////////////////////////////////////////////////////////////////////////

	public void navigateToDealNotes(PageEntry pe, String messageToUser)
	{
		PageEntry pgNext = setupMainPagePageEntry(Sc.PGNM_DEAL_NOTES_ID, 
		                                          pe.getPageDealId(), 
		                                          pe.getPageDealCID(), 
		                                          pe.getDealInstitutionId());

		if (messageToUser != null) setActiveMessageToAlert(messageToUser, ActiveMsgFactory.ISCUSTOMCONFIRM);

		getSavedPages().setNextPage(pgNext);

		navigateToNextPage(true);

	}


	//////////////////////////////////////////////////////////////////////////

	public int getSelectedDecModif()
	{
		ComboBox cbxDecisionModType =(ComboBox) getCurrNDPage().getDisplayField("cbDecisionModificationType");

		String valSelected = null;

		try
		{
			valSelected = (String) cbxDecisionModType.getValue();
			if (valSelected == null || valSelected.toString().length() == 0)
          return - 1;
		}
		catch(Exception e)
		{
			return - 1;
		}

		return new Integer(valSelected).intValue();

	}

	////////////////////////////////////////////////////////////////////////////
  //// This method is moved to the PHC base class.
  /**
	public int displayViewOnlyTag(DisplayField field)
	{
		SessionState theSession = getTheSession();

		PageEntry pg = theSessionState.getCurrentPage();

		if (pg.isEditable() == false)
		{
			field.setHtmlText("<IMG NAME=\"stViewOnlyTag\" SRC=\"/eNet_images/ViewOnly.gif\" ALIGN=TOP>");
			return CSpPage.PROCEED;
		}

		return CSpPage.SKIP;
	}
  **/

}

