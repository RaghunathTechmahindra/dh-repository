package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doMultyDealAssignUserModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfUserProfileId();


	/**
	 *
	 *
	 */
	public void setDfUserProfileId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfNameAndType();


	/**
	 *
	 *
	 */
	public void setDfNameAndType(String value);

	//--Release2.1--//
	/**
	 *
	 *
	 */
	public String getDfUserDescription();


	/**
	 *
	 *
	 */
	public void setDfUserDescription(String value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfUserTypeId();


	/**
	 *
	 *
	 */
	public void setDfUserTypeId(java.math.BigDecimal value);



	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFUSERPROFILEID="dfUserProfileId";
	public static final String FIELD_DFNAMEANDTYPE="dfNameAndType";

	//--Release2.1--//
  //// 7. In addition to the regular 6 steps to adjust the computed column of fly
  //// another step should be done as well: the 'fake' hidden fields should be created
  //// to override the UserTypeDescripton in the afterExecute() method.
  public static final String FIELD_DFUTDESCRIPTION="dfUtDescription";
  public static final String FIELD_DFUSERTYPEID="dfUserTypeId";
  public static final String FIELD_DFUSERLOGIN="dfUserLogin";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

