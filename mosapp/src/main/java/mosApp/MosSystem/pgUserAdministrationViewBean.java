package mosApp.MosSystem;

import com.basis100.picklist.BXResources;

import com.filogix.express.web.jato.ExpressViewBeanBase;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.view.CommandFieldDescriptor;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.ChildDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.CheckBox;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HREF;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.ListBox;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

import config.UserAdminConfigManager;

import mosApp.SQLConnectionManagerImpl;

import java.io.IOException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;


/**
 *
 *
 *
 */
public class pgUserAdministrationViewBean
  extends ExpressViewBeanBase
{
  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////
  public static final String PAGE_NAME = "pgUserAdministration";

  //// It is a variable now (see explanation in the getDisplayURL() method
  // above.
  ////public static final String
  // DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgUserAdministration.jsp";
  public static Map FIELD_DESCRIPTORS;
  public static final String CHILD_TBDEALID = "tbDealId";
  public static final String CHILD_TBDEALID_RESET_VALUE = "";
  public static final String CHILD_CBPAGENAMES = "cbPageNames";
  public static final String CHILD_CBPAGENAMES_RESET_VALUE = "";

  //==================================================================
  public static final String CHILD_BTPROCEED = "btProceed";
  public static final String CHILD_BTPROCEED_RESET_VALUE = "Proceed";
  public static final String CHILD_HREF1 = "Href1";
  public static final String CHILD_HREF1_RESET_VALUE = "";
  public static final String CHILD_HREF2 = "Href2";
  public static final String CHILD_HREF2_RESET_VALUE = "";
  public static final String CHILD_HREF3 = "Href3";
  public static final String CHILD_HREF3_RESET_VALUE = "";
  public static final String CHILD_HREF4 = "Href4";
  public static final String CHILD_HREF4_RESET_VALUE = "";
  public static final String CHILD_HREF5 = "Href5";
  public static final String CHILD_HREF5_RESET_VALUE = "";
  public static final String CHILD_HREF6 = "Href6";
  public static final String CHILD_HREF6_RESET_VALUE = "";
  public static final String CHILD_HREF7 = "Href7";
  public static final String CHILD_HREF7_RESET_VALUE = "";
  public static final String CHILD_HREF8 = "Href8";
  public static final String CHILD_HREF8_RESET_VALUE = "";
  public static final String CHILD_HREF9 = "Href9";
  public static final String CHILD_HREF9_RESET_VALUE = "";
  public static final String CHILD_HREF10 = "Href10";
  public static final String CHILD_HREF10_RESET_VALUE = "";
  public static final String CHILD_STPAGELABEL = "stPageLabel";
  public static final String CHILD_STPAGELABEL_RESET_VALUE = "";
  public static final String CHILD_STCOMPANYNAME = "stCompanyName";
  public static final String CHILD_STCOMPANYNAME_RESET_VALUE = "";
  public static final String CHILD_STTODAYDATE = "stTodayDate";
  public static final String CHILD_STTODAYDATE_RESET_VALUE = "";
  public static final String CHILD_STUSERNAMETITLE = "stUserNameTitle";
  public static final String CHILD_STUSERNAMETITLE_RESET_VALUE = "";
  public static final String CHILD_BTSUBMIT = "btSubmit";
  public static final String CHILD_BTSUBMIT_RESET_VALUE = " ";
  public static final String CHILD_BTWORKQUEUELINK = "btWorkQueueLink";
  public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE = " ";
  public static final String CHILD_CHANGEPASSWORDHREF = "changePasswordHref";
  public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE = "";
  public static final String CHILD_BTTOOLHISTORY = "btToolHistory";
  public static final String CHILD_BTTOOLHISTORY_RESET_VALUE = " ";
  public static final String CHILD_BTTOONOTES = "btTooNotes";
  public static final String CHILD_BTTOONOTES_RESET_VALUE = " ";
  public static final String CHILD_BTTOOLSEARCH = "btToolSearch";
  public static final String CHILD_BTTOOLSEARCH_RESET_VALUE = " ";
  public static final String CHILD_BTTOOLLOG = "btToolLog";
  public static final String CHILD_BTTOOLLOG_RESET_VALUE = " ";
  public static final String CHILD_STERRORFLAG = "stErrorFlag";
  public static final String CHILD_STERRORFLAG_RESET_VALUE = "N";
  public static final String CHILD_DETECTALERTTASKS = "detectAlertTasks";
  public static final String CHILD_DETECTALERTTASKS_RESET_VALUE = "";
  public static final String CHILD_BTCANCEL = "btCancel";
  public static final String CHILD_BTCANCEL_RESET_VALUE = " ";
  public static final String CHILD_BTPREVTASKPAGE = "btPrevTaskPage";
  public static final String CHILD_BTPREVTASKPAGE_RESET_VALUE = " ";
  public static final String CHILD_STPREVTASKPAGELABEL = "stPrevTaskPageLabel";
  public static final String CHILD_STPREVTASKPAGELABEL_RESET_VALUE = "";
  public static final String CHILD_BTNEXTTASKPAGE = "btNextTaskPage";
  public static final String CHILD_BTNEXTTASKPAGE_RESET_VALUE = " ";
  public static final String CHILD_STNEXTTASKPAGELABEL = "stNextTaskPageLabel";
  public static final String CHILD_STNEXTTASKPAGELABEL_RESET_VALUE = "";
  public static final String CHILD_STTASKNAME = "stTaskName";
  public static final String CHILD_STTASKNAME_RESET_VALUE = "";
  public static final String CHILD_SESSIONUSERID = "sessionUserId";
  public static final String CHILD_SESSIONUSERID_RESET_VALUE = "";
  public static final String CHILD_STPMGENERATE = "stPmGenerate";
  public static final String CHILD_STPMGENERATE_RESET_VALUE = "";
  public static final String CHILD_STPMHASTITLE = "stPmHasTitle";
  public static final String CHILD_STPMHASTITLE_RESET_VALUE = "";
  public static final String CHILD_STPMHASINFO = "stPmHasInfo";
  public static final String CHILD_STPMHASINFO_RESET_VALUE = "";
  public static final String CHILD_STPMHASTABLE = "stPmHasTable";
  public static final String CHILD_STPMHASTABLE_RESET_VALUE = "";
  public static final String CHILD_STPMHASOK = "stPmHasOk";
  public static final String CHILD_STPMHASOK_RESET_VALUE = "";
  public static final String CHILD_STPMTITLE = "stPmTitle";
  public static final String CHILD_STPMTITLE_RESET_VALUE = "";
  public static final String CHILD_STPMINFOMSG = "stPmInfoMsg";
  public static final String CHILD_STPMINFOMSG_RESET_VALUE = "";
  public static final String CHILD_STPMONOK = "stPmOnOk";
  public static final String CHILD_STPMONOK_RESET_VALUE = "";
  public static final String CHILD_STPMMSGS = "stPmMsgs";
  public static final String CHILD_STPMMSGS_RESET_VALUE = "";
  public static final String CHILD_STPMMSGTYPES = "stPmMsgTypes";
  public static final String CHILD_STPMMSGTYPES_RESET_VALUE = "";
  public static final String CHILD_STAMGENERATE = "stAmGenerate";
  public static final String CHILD_STAMGENERATE_RESET_VALUE = "";
  public static final String CHILD_STAMHASTITLE = "stAmHasTitle";
  public static final String CHILD_STAMHASTITLE_RESET_VALUE = "";
  public static final String CHILD_STAMHASINFO = "stAmHasInfo";
  public static final String CHILD_STAMHASINFO_RESET_VALUE = "";
  public static final String CHILD_STAMHASTABLE = "stAmHasTable";
  public static final String CHILD_STAMHASTABLE_RESET_VALUE = "";
  public static final String CHILD_STAMTITLE = "stAmTitle";
  public static final String CHILD_STAMTITLE_RESET_VALUE = "";
  public static final String CHILD_STAMINFOMSG = "stAmInfoMsg";
  public static final String CHILD_STAMINFOMSG_RESET_VALUE = "";
  public static final String CHILD_STAMMSGS = "stAmMsgs";
  public static final String CHILD_STAMMSGS_RESET_VALUE = "";
  public static final String CHILD_STAMMSGTYPES = "stAmMsgTypes";
  public static final String CHILD_STAMMSGTYPES_RESET_VALUE = "";
  public static final String CHILD_STAMDIALOGMSG = "stAmDialogMsg";
  public static final String CHILD_STAMDIALOGMSG_RESET_VALUE = "";
  public static final String CHILD_STAMBUTTONSHTML = "stAmButtonsHtml";
  public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE = "";
  public static final String CHILD_BTADDNEWUSER = "btAddNewUser";
  public static final String CHILD_BTADDNEWUSER_RESET_VALUE = "Add New User";
  public static final String CHILD_CBUSERNAMES = "cbUserNames";
  public static final String CHILD_CBUSERNAMES_RESET_VALUE = "";

  //===============
  public static final String CHILD_CBPROFILESTATUS = "cbProfileStatus";
  public static final String CHILD_CBPROFILESTATUS_RESET_VALUE = "";
  public static final String CHILD_BTGO = "btGo";
  public static final String CHILD_BTGO_RESET_VALUE = "Go";
  public static final String CHILD_CBPARTNERPROFILENAMES = "cbPartnerProfileNames";
  public static final String CHILD_CBPARTNERPROFILENAMES_RESET_VALUE = "";

  //=================
  public static final String CHILD_CBSTANDIN = "cbStandIn";
  public static final String CHILD_CBSTANDIN_RESET_VALUE = "";

  //=================
  public static final String CHILD_CBGROUPPROFILENAME = "cbGroupProfileName";
  public static final String CHILD_CBGROUPPROFILENAME_RESET_VALUE = "";

  //=================
  public static final String CHILD_CBHIGHERAPPROVERS = "cbHigherApprovers";
  public static final String CHILD_CBHIGHERAPPROVERS_RESET_VALUE = "";

  //=================
  public static final String CHILD_CBJOINTAPPROVERS = "cbJointApprovers";
  public static final String CHILD_CBJOINTAPPROVERS_RESET_VALUE = "";

  //=================
  public static final String CHILD_CBSALUTATIONS = "cbSalutations";
  public static final String CHILD_CBSALUTATIONS_RESET_VALUE = "";
  public static final String CHILD_CBMANAGERS = "cbManagers";
  public static final String CHILD_CBMANAGERS_RESET_VALUE = "";

  //=================
  public static final String CHILD_CBBRANCHNAMES = "cbBranchNames";
  public static final String CHILD_CBBRANCHNAMES_RESET_VALUE = "";

  //=================
  public static final String CHILD_CBREGIONNAMES = "cbRegionNames";
  public static final String CHILD_CBREGIONNAMES_RESET_VALUE = "";

  //=================
  public static final String CHILD_CBINSTITUTIONNAMES = "cbInstitutionNames";
  public static final String CHILD_CBINSTITUTIONNAMES_RESET_VALUE = "";
  public static final String CHILD_CBLANGUAGES = "cbLanguages";
  public static final String CHILD_CBLANGUAGES_RESET_VALUE = "N";
  public static final String CHILD_CBTASKALERT = "cbTaskAlert";
  public static final String CHILD_CBTASKALERT_RESET_VALUE = "N";

  //-- ========== SCR#537 ends ========== --//
  //--Release2.1--//
  //private static OptionList cbLanguagesOptions=new OptionList(new
  // String[]{"No", "Yes"},new String[]{"N", "Y"});
  public static final String CHILD_CBUSERTYPES = "cbUserTypes";
  public static final String CHILD_CBUSERTYPES_RESET_VALUE = "";
  public static final String CHILD_TBUSERTITLE = "tbUserTitle";
  public static final String CHILD_TBUSERTITLE_RESET_VALUE = "";
  public static final String CHILD_TBUSERFIRSTNAME = "tbUserFirstName";
  public static final String CHILD_TBUSERFIRSTNAME_RESET_VALUE = "";
  public static final String CHILD_TBUSERINITIALNAME = "tbUserInitialName";
  public static final String CHILD_TBUSERINITIALNAME_RESET_VALUE = "";
  public static final String CHILD_TBUSERLASTNAME = "tbUserLastName";
  public static final String CHILD_TBUSERLASTNAME_RESET_VALUE = "";
  public static final String CHILD_TBLOGINID = "tbLoginId";
  public static final String CHILD_TBLOGINID_RESET_VALUE = "";
  public static final String CHILD_HDLOGINID = "hdLoginId";
  public static final String CHILD_HDLOGINID_RESET_VALUE = "";
  public static final String CHILD_TBBUSINESSID = "tbBusinessId";
  public static final String CHILD_TBBUSINESSID_RESET_VALUE = "";
  public static final String CHILD_TBWORKPHONENUMBERAREACODE = "tbWorkPhoneNumberAreaCode";
  public static final String CHILD_TBWORKPHONENUMBERAREACODE_RESET_VALUE = "";
  public static final String CHILD_TBWORKPHONENUMFIRSTTHREEDIGITS =
    "tbWorkPhoneNumFirstThreeDigits";
  public static final String CHILD_TBWORKPHONENUMFIRSTTHREEDIGITS_RESET_VALUE = "";
  public static final String CHILD_TBWORKPHONENUMLASTFOURDIGITS = "tbWorkPhoneNumLastFourDigits";
  public static final String CHILD_TBWORKPHONENUMLASTFOURDIGITS_RESET_VALUE = "";
  public static final String CHILD_TBWORKPHONENUMEXTENSION = "tbWorkPhoneNumExtension";
  public static final String CHILD_TBWORKPHONENUMEXTENSION_RESET_VALUE = "";
  public static final String CHILD_TBFAXNUMBERAREACODE = "tbFaxNumberAreaCode";
  public static final String CHILD_TBFAXNUMBERAREACODE_RESET_VALUE = "";
  public static final String CHILD_TBFAXNUMFIRSTTHREEDIGITS = "tbFaxNumFirstThreeDigits";
  public static final String CHILD_TBFAXNUMFIRSTTHREEDIGITS_RESET_VALUE = "";
  public static final String CHILD_TBFAXNUMLASTFOURDIGITS = "tbFaxNumLastFourDigits";
  public static final String CHILD_TBFAXNUMLASTFOURDIGITS_RESET_VALUE = "";
  public static final String CHILD_TBEMAILADDRESS = "tbEmailAddress";
  public static final String CHILD_TBEMAILADDRESS_RESET_VALUE = "";
  public static final String CHILD_TBPASSWORD = "tbPassword";
  public static final String CHILD_TBPASSWORD_RESET_VALUE = "";
  public static final String CHILD_TBVERIFYPASSWORD = "tbVerifyPassword";
  public static final String CHILD_TBVERIFYPASSWORD_RESET_VALUE = "";
  public static final String CHILD_TBLINEOFBUSINESS1 = "tbLineOfBusiness1";
  public static final String CHILD_TBLINEOFBUSINESS1_RESET_VALUE = "";
  public static final String CHILD_TBLINEOFBUSINESS2 = "tbLineOfBusiness2";
  public static final String CHILD_TBLINEOFBUSINESS2_RESET_VALUE = "";
  public static final String CHILD_TBLINEOFBUSINESS3 = "tbLineOfBusiness3";
  public static final String CHILD_TBLINEOFBUSINESS3_RESET_VALUE = "";
  public static final String CHILD_STUSERLABEL = "stUserLabel";
  public static final String CHILD_STUSERLABEL_RESET_VALUE = "";
  public static final String CHILD_BTSCREENACCESS = "btScreenAccess";
  public static final String CHILD_BTSCREENACCESS_RESET_VALUE = "Screen Access";
  public static final String CHILD_REPEATED1 = "Repeated1";
  public static final String CHILD_STBEGINHIDINGSELANDGOBUTTON = "stBeginHidingSelAndGoButton";
  public static final String CHILD_STBEGINHIDINGSELANDGOBUTTON_RESET_VALUE = "";
  public static final String CHILD_STENDHIDINGSELANDGOBUTTON = "stEndHidingSelAndGoButton";
  public static final String CHILD_STENDHIDINGSELANDGOBUTTON_RESET_VALUE = "";
  public static final String CHILD_STBEGINHIDINGSCREENACC = "stBeginHidingScreenAcc";
  public static final String CHILD_STBEGINHIDINGSCREENACC_RESET_VALUE = "";
  public static final String CHILD_STENDHIDINGSCREENACC = "stEndHidingScreenAcc";
  public static final String CHILD_STENDHIDINGSCREENACC_RESET_VALUE = "";
  public static final String CHILD_HD2ROWNUM = "hd2RowNum";
  public static final String CHILD_HD2ROWNUM_RESET_VALUE = "";
  public static final String CHILD_STINSTITUTIONARRAY = "stInstitutionArray";
  public static final String CHILD_STINSTITUTIONARRAY_RESET_VALUE = "";
  public static final String CHILD_STREGIONARRAY = "stRegionArray";
  public static final String CHILD_STREGIONARRAY_RESET_VALUE = "";
  public static final String CHILD_STBRANCHARRAY = "stBranchArray";
  public static final String CHILD_STBRANCHARRAY_RESET_VALUE = "";
  public static final String CHILD_STGROUPARRAY = "stGroupArray";
  public static final String CHILD_STGROUPARRAY_RESET_VALUE = "";
  public static final String CHILD_STPAGEACCESSARRAY = "stPageAccessArray";
  public static final String CHILD_STPAGEACCESSARRAY_RESET_VALUE = "";
  public static final String CHILD_STOLDSELECTEDHIGHERAPP = "stOldSelectedHigherApp";
  public static final String CHILD_STOLDSELECTEDHIGHERAPP_RESET_VALUE = "";
  public static final String CHILD_STOLDSELECTEDJOINTAPP = "stOldSelectedJointApp";
  public static final String CHILD_STOLDSELECTEDJOINTAPP_RESET_VALUE = "";
  public static final String CHILD_CHFORCETOCHANGEPW = "chForceToChangePW";
  public static final String CHILD_CHFORCETOCHANGEPW_RESET_VALUE = "Y";
  public static final String CHILD_CHLOCKPW = "chLockPW";
  public static final String CHILD_CHLOCKPW_RESET_VALUE = "N";
  public static final String CHILD_CHNEWPW = "chNewPW";
  public static final String CHILD_CHNEWPW_RESET_VALUE = "Y";
  public static final String CHILD_STLASTLOGINTIME = "stLastLoginTime";
  public static final String CHILD_STLASTLOGINTIME_RESET_VALUE = "";
  public static final String CHILD_STLASTLOGOUTTIME = "stLastLogoutTime";
  public static final String CHILD_STLASTLOGOUTTIME_RESET_VALUE = "";
  public static final String CHILD_STLOGINFLAG = "stLoginFlag";
  public static final String CHILD_STLOGINFLAG_RESET_VALUE = "";

  //--> Hidden ActMessageButton (FINDTHISLATER)
  //--> Test by BILLY 15July2002
  public static final String CHILD_BTACTMSG = "btActMsg";
  public static final String CHILD_BTACTMSG_RESET_VALUE = "ActMsg";

  //--Release2.1--//
  //// New link to toggle the language. When touched this link should reload
  // the
  //// page in opposite language (french versus english) and set this new
  // language
  //// session id throughout all modulus.
  public static final String CHILD_TOGGLELANGUAGEHREF = "toggleLanguageHref";
  public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE = "";

  //--Release2.1--//
  //--> Added Prefered Language ComboBox
  //--> By Billy 09Dec2002
  public static final String CHILD_CBDEFAULTLANGUAGE = "cbDefaultLanguage";
  public static final String CHILD_CBDEFAULTLANGUAGE_RESET_VALUE = "";

  //=================================================
  //--> CRF#37 :: Source/Firm to Group routing for TD
  //=================================================
  //--> Change to support multiple groups assignnment
  //--> By Billy 11Aug2003
  public static final String CHILD_LBGROUPS = "lbGroups";
  public static final String CHILD_LBGROUPS_RESET_VALUE = "";
  public static final String CHILD_TBNEWGROUPNAME = "tbNewGroupName";
  public static final String CHILD_TBNEWGROUPNAME_RESET_VALUE = "";
  public static final String CHILD_BTCREATENEWGROUP = "btCreateNewGroup";
  public static final String CHILD_BTCREATENEWGROUP_RESET_VALUE = "";
  public static final String CHILD_STBEGINHIDINGNEWGROUPBUTTON = "stBeginHidingNewGroupButton";
  public static final String CHILD_STBEGINHIDINGNEWGROUPBUTTON_RESET_VALUE = "";
  public static final String CHILD_STENDHIDINGNEWGROUPBUTTON = "stEndHidingNewGroupButton";
  public static final String CHILD_STENDHIDINGNEWGROUPBUTTON_RESET_VALUE = "";

  //-- ========== SCR#239   ends ========== --//

  //--Release2.1--//
  //private static CbPageNamesOptionList cbPageNamesOptions=new
  // CbPageNamesOptionList();
  private CbPageNamesOptionList cbPageNamesOptions = new CbPageNamesOptionList();

  //Added the Variable for NonSelected Label
  private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";

  //--Release2.1--//
  //private static CbUserNamesOptionList cbUserNamesOptions=new
  // CbUserNamesOptionList();
  private CbUserNamesOptionList cbUserNamesOptions = new CbUserNamesOptionList();
  private String CHILD_CBUSERNAMES_NONSELECTED_LABEL = "";

  //--Release2.1--//
  //private static CbProfileStatusOptionList cbProfileStatusOptions=new
  // CbProfileStatusOptionList();
  private CbProfileStatusOptionList cbProfileStatusOptions = new CbProfileStatusOptionList();

  //--Release2.1--//
  //private static CbPartnerProfileNamesOptionList
  // cbPartnerProfileNamesOptions=new CbPartnerProfileNamesOptionList();
  private CbPartnerProfileNamesOptionList cbPartnerProfileNamesOptions =
    new CbPartnerProfileNamesOptionList();
  private String CHILD_CBPARTNERPROFILENAMES_NONSELECTED_LABEL = "";

  //--Release2.1--//
  //private static CbStandInOptionList cbStandInOptions=new
  // CbStandInOptionList();
  private CbStandInOptionList cbStandInOptions = new CbStandInOptionList();
  private String CHILD_CBSTANDIN_NONSELECTED_LABEL = "";

  //--Release2.1--//
  //private static CbGroupProfileNameOptionList cbGroupProfileNameOptions=new
  // CbGroupProfileNameOptionList();
  private CbGroupProfileNameOptionList cbGroupProfileNameOptions =
    new CbGroupProfileNameOptionList();
  private String CHILD_CBGROUPPROFILENAME_NONSELECTED_LABEL = "";

  //--Release2.1--//
  //private static CbHigherApproversOptionList cbHigherApproversOptions=new
  // CbHigherApproversOptionList();
  private CbHigherApproversOptionList cbHigherApproversOptions = new CbHigherApproversOptionList();
  private String CHILD_CBHIGHERAPPROVERS_NONSELECTED_LABEL = "";

  //--Release2.1--//
  //private static CbJointApproversOptionList cbJointApproversOptions=new
  // CbJointApproversOptionList();
  private CbJointApproversOptionList cbJointApproversOptions = new CbJointApproversOptionList();
  private String CHILD_CBJOINTAPPROVERS_NONSELECTED_LABEL = "";

  //--Release2.1--//
  //private static CbSalutationsOptionList cbSalutationsOptions=new
  // CbSalutationsOptionList();
  private CbSalutationsOptionList cbSalutationsOptions = new CbSalutationsOptionList();

  //--Release2.1--//
  //private static CbManagersOptionList cbManagersOptions=new
  // CbManagersOptionList();
  private CbManagersOptionList cbManagersOptions = new CbManagersOptionList();
  private String CHILD_CBMANAGERS_NONSELECTED_LABEL = "";

  //--Release2.1--//
  //private static CbBranchNamesOptionList cbBranchNamesOptions=new
  // CbBranchNamesOptionList();
  private CbBranchNamesOptionList cbBranchNamesOptions = new CbBranchNamesOptionList();
  private String CHILD_CBBRANCHNAMES_NONSELECTED_LABEL = "";

  //--Release2.1--//
  //private static CbRegionNamesOptionList cbRegionNamesOptions=new
  // CbRegionNamesOptionList();
  private CbRegionNamesOptionList cbRegionNamesOptions = new CbRegionNamesOptionList();
  private String CHILD_CBREGIONNAMES_NONSELECTED_LABEL = "";

  //--Release2.1--//
  //private static CbInstitutionNamesOptionList cbInstitutionNamesOptions=new
  // CbInstitutionNamesOptionList();
  private CbInstitutionNamesOptionList cbInstitutionNamesOptions =
    new CbInstitutionNamesOptionList();
  private String CHILD_CBINSTITUTIONNAMES_NONSELECTED_LABEL = "";

  //=================
  private OptionList cbLanguagesOptions = new OptionList();

  //-- ========== SCR#537 begins ========== --//
  //-- by Neil at Dec/02/2004
  private OptionList cbTaskAlertOptions = new OptionList();

  //--Release2.1--//
  //private static CbUserTypesOptionList cbUserTypesOptions=new
  // CbUserTypesOptionList();
  private CbUserTypesOptionList cbUserTypesOptions = new CbUserTypesOptionList();
  private CbDefaultLanguageOptionList cbDefaultLanguageOptions = new CbDefaultLanguageOptionList();

  // Note shared the Options with CbGroupProfileName
  //private CbGroupProfileNameOptionList cbGroupProfileNameOptions=new
  // CbGroupProfileNameOptionList();
  private String CHILD_LBGROUPS_NONSELECTED_LABEL = "";

  //-- ========== SCR#239 begins ========== --//
  //-- by Neil on Jan/04/2005
  private boolean isAddNewUserMode = false;
  //-- ========== SCR#239 ends ========== --//

  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////
  private doRowGenerator2Model doRowGenerator2 = null;
  
  
  protected String DEFAULT_DISPLAY_URL = "/mosApp/MosSystem/pgUserAdministration.jsp";

  ////////////////////////////////////////////////////////////////////////////
  // Custom Members - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////
  // MigrationToDo : Migrate custom member
  private userAdminHandler handler = new userAdminHandler();

  /**
   *
   *
   */
  public pgUserAdministrationViewBean()
  {
    super(PAGE_NAME);
    setDefaultDisplayURL(DEFAULT_DISPLAY_URL);
    registerChildren();
    initialize();
  }

  /**
   *
   *
   */
  protected void initialize()
  {
  }

  ////////////////////////////////////////////////////////////////////////////////
  // Child manipulation methods
  ////////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   */
  protected View createChild(String name)
  {
      View superReturn = super.createChild(name);  
      if (superReturn != null) {  
      return superReturn;  
      } else if (name.equals(CHILD_TBDEALID))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBDEALID, CHILD_TBDEALID,
          CHILD_TBDEALID_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_CBPAGENAMES))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBPAGENAMES, CHILD_CBPAGENAMES,
          CHILD_CBPAGENAMES_RESET_VALUE, null);

      //--Release2.1--//
      //child.setLabelForNoneSelected("Choose a Page");
      child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
      child.setOptions(cbPageNamesOptions);

      return child;
    }
    else if (name.equals(CHILD_BTPROCEED))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTPROCEED, CHILD_BTPROCEED,
          CHILD_BTPROCEED_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF1))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF1, CHILD_HREF1, CHILD_HREF1_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF2))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF2, CHILD_HREF2, CHILD_HREF2_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF3))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF3, CHILD_HREF3, CHILD_HREF3_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF4))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF4, CHILD_HREF4, CHILD_HREF4_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF5))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF5, CHILD_HREF5, CHILD_HREF5_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF6))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF6, CHILD_HREF6, CHILD_HREF6_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF7))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF7, CHILD_HREF7, CHILD_HREF7_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF8))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF8, CHILD_HREF8, CHILD_HREF8_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF9))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF9, CHILD_HREF9, CHILD_HREF9_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF10))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF10, CHILD_HREF10, CHILD_HREF10_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPAGELABEL))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPAGELABEL, CHILD_STPAGELABEL,
          CHILD_STPAGELABEL_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STCOMPANYNAME))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STCOMPANYNAME, CHILD_STCOMPANYNAME,
          CHILD_STCOMPANYNAME_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STTODAYDATE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STTODAYDATE, CHILD_STTODAYDATE,
          CHILD_STTODAYDATE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STUSERNAMETITLE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STUSERNAMETITLE, CHILD_STUSERNAMETITLE,
          CHILD_STUSERNAMETITLE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTSUBMIT))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTSUBMIT, CHILD_BTSUBMIT,
          CHILD_BTSUBMIT_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTWORKQUEUELINK))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTWORKQUEUELINK, CHILD_BTWORKQUEUELINK,
          CHILD_BTWORKQUEUELINK_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_CHANGEPASSWORDHREF))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_CHANGEPASSWORDHREF, CHILD_CHANGEPASSWORDHREF,
          CHILD_CHANGEPASSWORDHREF_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTTOOLHISTORY))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTTOOLHISTORY, CHILD_BTTOOLHISTORY,
          CHILD_BTTOOLHISTORY_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTTOONOTES))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTTOONOTES, CHILD_BTTOONOTES,
          CHILD_BTTOONOTES_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTTOOLSEARCH))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTTOOLSEARCH, CHILD_BTTOOLSEARCH,
          CHILD_BTTOOLSEARCH_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTTOOLLOG))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTTOOLLOG, CHILD_BTTOOLLOG,
          CHILD_BTTOOLLOG_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STERRORFLAG))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STERRORFLAG, CHILD_STERRORFLAG,
          CHILD_STERRORFLAG_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_DETECTALERTTASKS))
    {
      HiddenField child =
        new HiddenField(this, getDefaultModel(), CHILD_DETECTALERTTASKS, CHILD_DETECTALERTTASKS,
          CHILD_DETECTALERTTASKS_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTCANCEL))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTCANCEL, CHILD_BTCANCEL,
          CHILD_BTCANCEL_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTPREVTASKPAGE))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTPREVTASKPAGE, CHILD_BTPREVTASKPAGE,
          CHILD_BTPREVTASKPAGE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPREVTASKPAGELABEL))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPREVTASKPAGELABEL,
          CHILD_STPREVTASKPAGELABEL, CHILD_STPREVTASKPAGELABEL_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTNEXTTASKPAGE))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTNEXTTASKPAGE, CHILD_BTNEXTTASKPAGE,
          CHILD_BTNEXTTASKPAGE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STNEXTTASKPAGELABEL))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STNEXTTASKPAGELABEL,
          CHILD_STNEXTTASKPAGELABEL, CHILD_STNEXTTASKPAGELABEL_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STTASKNAME))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STTASKNAME, CHILD_STTASKNAME,
          CHILD_STTASKNAME_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_SESSIONUSERID))
    {
      HiddenField child =
        new HiddenField(this, getDefaultModel(), CHILD_SESSIONUSERID, CHILD_SESSIONUSERID,
          CHILD_SESSIONUSERID_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMGENERATE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMGENERATE, CHILD_STPMGENERATE,
          CHILD_STPMGENERATE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMHASTITLE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMHASTITLE, CHILD_STPMHASTITLE,
          CHILD_STPMHASTITLE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMHASINFO))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMHASINFO, CHILD_STPMHASINFO,
          CHILD_STPMHASINFO_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMHASTABLE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMHASTABLE, CHILD_STPMHASTABLE,
          CHILD_STPMHASTABLE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMHASOK))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMHASOK, CHILD_STPMHASOK,
          CHILD_STPMHASOK_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMTITLE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMTITLE, CHILD_STPMTITLE,
          CHILD_STPMTITLE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMINFOMSG))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMINFOMSG, CHILD_STPMINFOMSG,
          CHILD_STPMINFOMSG_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMONOK))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMONOK, CHILD_STPMONOK,
          CHILD_STPMONOK_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMMSGS))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMMSGS, CHILD_STPMMSGS,
          CHILD_STPMMSGS_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMMSGTYPES))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMMSGTYPES, CHILD_STPMMSGTYPES,
          CHILD_STPMMSGTYPES_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMGENERATE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMGENERATE, CHILD_STAMGENERATE,
          CHILD_STAMGENERATE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMHASTITLE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMHASTITLE, CHILD_STAMHASTITLE,
          CHILD_STAMHASTITLE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMHASINFO))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMHASINFO, CHILD_STAMHASINFO,
          CHILD_STAMHASINFO_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMHASTABLE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMHASTABLE, CHILD_STAMHASTABLE,
          CHILD_STAMHASTABLE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMTITLE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMTITLE, CHILD_STAMTITLE,
          CHILD_STAMTITLE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMINFOMSG))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMINFOMSG, CHILD_STAMINFOMSG,
          CHILD_STAMINFOMSG_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMMSGS))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMMSGS, CHILD_STAMMSGS,
          CHILD_STAMMSGS_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMMSGTYPES))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMMSGTYPES, CHILD_STAMMSGTYPES,
          CHILD_STAMMSGTYPES_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMDIALOGMSG))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMDIALOGMSG, CHILD_STAMDIALOGMSG,
          CHILD_STAMDIALOGMSG_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMBUTTONSHTML))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMBUTTONSHTML, CHILD_STAMBUTTONSHTML,
          CHILD_STAMBUTTONSHTML_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTADDNEWUSER))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTADDNEWUSER, CHILD_BTADDNEWUSER,
          CHILD_BTADDNEWUSER_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_CBUSERNAMES))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBUSERNAMES, CHILD_CBUSERNAMES,
          CHILD_CBUSERNAMES_RESET_VALUE, null);

      //--Release2.1--//
      child.setLabelForNoneSelected(CHILD_CBUSERNAMES_NONSELECTED_LABEL);
      child.setOptions(cbUserNamesOptions);

      return child;
    }
    else if (name.equals(CHILD_CBPROFILESTATUS))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBPROFILESTATUS, CHILD_CBPROFILESTATUS,
          CHILD_CBPROFILESTATUS_RESET_VALUE, null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbProfileStatusOptions);

      return child;
    }
    else if (name.equals(CHILD_BTGO))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTGO, CHILD_BTGO, CHILD_BTGO_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_CBPARTNERPROFILENAMES))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBPARTNERPROFILENAMES,
          CHILD_CBPARTNERPROFILENAMES, CHILD_CBPARTNERPROFILENAMES_RESET_VALUE, null);

      //--Release2.1--//
      child.setLabelForNoneSelected(CHILD_CBPARTNERPROFILENAMES_NONSELECTED_LABEL);
      child.setOptions(cbPartnerProfileNamesOptions);

      return child;
    }
    else if (name.equals(CHILD_CBSTANDIN))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBSTANDIN, CHILD_CBSTANDIN,
          CHILD_CBSTANDIN_RESET_VALUE, null);

      //--Release2.1--//
      child.setLabelForNoneSelected(CHILD_CBSTANDIN_NONSELECTED_LABEL);
      child.setOptions(cbStandInOptions);

      return child;
    }
    else if (name.equals(CHILD_CBGROUPPROFILENAME))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBGROUPPROFILENAME, CHILD_CBGROUPPROFILENAME,
          CHILD_CBGROUPPROFILENAME_RESET_VALUE, null);

      //--Release2.1--//
      child.setLabelForNoneSelected(CHILD_CBGROUPPROFILENAME_NONSELECTED_LABEL);
      child.setOptions(cbGroupProfileNameOptions);

      return child;
    }
    else if (name.equals(CHILD_CBHIGHERAPPROVERS))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBHIGHERAPPROVERS, CHILD_CBHIGHERAPPROVERS,
          CHILD_CBHIGHERAPPROVERS_RESET_VALUE, null);

      //--Release2.1--//
      child.setLabelForNoneSelected(CHILD_CBHIGHERAPPROVERS_NONSELECTED_LABEL);
      child.setOptions(cbHigherApproversOptions);

      return child;
    }
    else if (name.equals(CHILD_CBJOINTAPPROVERS))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBJOINTAPPROVERS, CHILD_CBJOINTAPPROVERS,
          CHILD_CBJOINTAPPROVERS_RESET_VALUE, null);

      //--Release2.1--//
      child.setLabelForNoneSelected(CHILD_CBJOINTAPPROVERS_NONSELECTED_LABEL);
      child.setOptions(cbJointApproversOptions);

      return child;
    }
    else if (name.equals(CHILD_CBSALUTATIONS))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBSALUTATIONS, CHILD_CBSALUTATIONS,
          CHILD_CBSALUTATIONS_RESET_VALUE, null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbSalutationsOptions);

      return child;
    }
    else if (name.equals(CHILD_CBMANAGERS))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBMANAGERS, CHILD_CBMANAGERS,
          CHILD_CBMANAGERS_RESET_VALUE, null);

      //--Release2.1--//
      child.setLabelForNoneSelected(CHILD_CBMANAGERS_NONSELECTED_LABEL);
      child.setOptions(cbManagersOptions);

      return child;
    }
    else if (name.equals(CHILD_CBBRANCHNAMES))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBBRANCHNAMES, CHILD_CBBRANCHNAMES,
          CHILD_CBBRANCHNAMES_RESET_VALUE, null);

      //--Release2.1--//
      child.setLabelForNoneSelected(CHILD_CBBRANCHNAMES_NONSELECTED_LABEL);
      child.setOptions(cbBranchNamesOptions);

      return child;
    }
    else if (name.equals(CHILD_CBREGIONNAMES))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBREGIONNAMES, CHILD_CBREGIONNAMES,
          CHILD_CBREGIONNAMES_RESET_VALUE, null);

      //--Release2.1--//
      child.setLabelForNoneSelected(CHILD_CBREGIONNAMES_NONSELECTED_LABEL);
      child.setOptions(cbRegionNamesOptions);

      return child;
    }
    else if (name.equals(CHILD_CBINSTITUTIONNAMES))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBINSTITUTIONNAMES, CHILD_CBINSTITUTIONNAMES,
          CHILD_CBINSTITUTIONNAMES_RESET_VALUE, null);

      //--Release2.1--//
      child.setLabelForNoneSelected(CHILD_CBINSTITUTIONNAMES_NONSELECTED_LABEL);
      child.setOptions(cbInstitutionNamesOptions);

      return child;
    }
    else if (name.equals(CHILD_CBLANGUAGES))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBLANGUAGES, CHILD_CBLANGUAGES,
          CHILD_CBLANGUAGES_RESET_VALUE, null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbLanguagesOptions);

      return child;
    }

    //-- ========== SCR#537 begins ========== --//
    //-- by Neil at Dec/02/2004
    else if (name.equals(CHILD_CBTASKALERT))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBTASKALERT, CHILD_CBTASKALERT,
          CHILD_CBTASKALERT_RESET_VALUE, null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbTaskAlertOptions);

      return child;
    }

    //-- ========== SCR#537 ends ========== --//
    else if (name.equals(CHILD_CBUSERTYPES))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBUSERTYPES, CHILD_CBUSERTYPES,
          CHILD_CBUSERTYPES_RESET_VALUE, null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbUserTypesOptions);

      return child;
    }
    else if (name.equals(CHILD_TBUSERTITLE))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBUSERTITLE, CHILD_TBUSERTITLE,
          CHILD_TBUSERTITLE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TBUSERFIRSTNAME))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBUSERFIRSTNAME, CHILD_TBUSERFIRSTNAME,
          CHILD_TBUSERFIRSTNAME_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TBUSERINITIALNAME))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBUSERINITIALNAME, CHILD_TBUSERINITIALNAME,
          CHILD_TBUSERINITIALNAME_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TBUSERLASTNAME))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBUSERLASTNAME, CHILD_TBUSERLASTNAME,
          CHILD_TBUSERLASTNAME_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TBLOGINID))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBLOGINID, CHILD_TBLOGINID,
          CHILD_TBLOGINID_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HDLOGINID))
    {
      HiddenField child =
        new HiddenField(this, getDefaultModel(), CHILD_HDLOGINID, CHILD_HDLOGINID,
          CHILD_HDLOGINID_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TBBUSINESSID))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBBUSINESSID, CHILD_TBBUSINESSID,
          CHILD_TBBUSINESSID_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TBWORKPHONENUMBERAREACODE))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBWORKPHONENUMBERAREACODE,
          CHILD_TBWORKPHONENUMBERAREACODE, CHILD_TBWORKPHONENUMBERAREACODE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TBWORKPHONENUMFIRSTTHREEDIGITS))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBWORKPHONENUMFIRSTTHREEDIGITS,
          CHILD_TBWORKPHONENUMFIRSTTHREEDIGITS, CHILD_TBWORKPHONENUMFIRSTTHREEDIGITS_RESET_VALUE,
          null);

      return child;
    }
    else if (name.equals(CHILD_TBWORKPHONENUMLASTFOURDIGITS))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBWORKPHONENUMLASTFOURDIGITS,
          CHILD_TBWORKPHONENUMLASTFOURDIGITS, CHILD_TBWORKPHONENUMLASTFOURDIGITS_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TBWORKPHONENUMEXTENSION))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBWORKPHONENUMEXTENSION,
          CHILD_TBWORKPHONENUMEXTENSION, CHILD_TBWORKPHONENUMEXTENSION_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TBFAXNUMBERAREACODE))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBFAXNUMBERAREACODE,
          CHILD_TBFAXNUMBERAREACODE, CHILD_TBFAXNUMBERAREACODE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TBFAXNUMFIRSTTHREEDIGITS))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBFAXNUMFIRSTTHREEDIGITS,
          CHILD_TBFAXNUMFIRSTTHREEDIGITS, CHILD_TBFAXNUMFIRSTTHREEDIGITS_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TBFAXNUMLASTFOURDIGITS))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBFAXNUMLASTFOURDIGITS,
          CHILD_TBFAXNUMLASTFOURDIGITS, CHILD_TBFAXNUMLASTFOURDIGITS_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TBEMAILADDRESS))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBEMAILADDRESS, CHILD_TBEMAILADDRESS,
          CHILD_TBEMAILADDRESS_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TBPASSWORD))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBPASSWORD, CHILD_TBPASSWORD,
          CHILD_TBPASSWORD_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TBVERIFYPASSWORD))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBVERIFYPASSWORD, CHILD_TBVERIFYPASSWORD,
          CHILD_TBVERIFYPASSWORD_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TBLINEOFBUSINESS1))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBLINEOFBUSINESS1, CHILD_TBLINEOFBUSINESS1,
          CHILD_TBLINEOFBUSINESS1_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TBLINEOFBUSINESS2))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBLINEOFBUSINESS2, CHILD_TBLINEOFBUSINESS2,
          CHILD_TBLINEOFBUSINESS2_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TBLINEOFBUSINESS3))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBLINEOFBUSINESS3, CHILD_TBLINEOFBUSINESS3,
          CHILD_TBLINEOFBUSINESS3_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STUSERLABEL))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STUSERLABEL, CHILD_STUSERLABEL,
          CHILD_STUSERLABEL_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTSCREENACCESS))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTSCREENACCESS, CHILD_BTSCREENACCESS,
          CHILD_BTSCREENACCESS_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_REPEATED1))
    {
      pgUserAdministrationRepeated1TiledView child =
        new pgUserAdministrationRepeated1TiledView(this, CHILD_REPEATED1);

      return child;
    }
    else if (name.equals(CHILD_STBEGINHIDINGSELANDGOBUTTON))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STBEGINHIDINGSELANDGOBUTTON,
          CHILD_STBEGINHIDINGSELANDGOBUTTON, CHILD_STBEGINHIDINGSELANDGOBUTTON_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STENDHIDINGSELANDGOBUTTON))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STENDHIDINGSELANDGOBUTTON,
          CHILD_STENDHIDINGSELANDGOBUTTON, CHILD_STENDHIDINGSELANDGOBUTTON_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STBEGINHIDINGSCREENACC))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STBEGINHIDINGSCREENACC,
          CHILD_STBEGINHIDINGSCREENACC, CHILD_STBEGINHIDINGSCREENACC_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STENDHIDINGSCREENACC))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STENDHIDINGSCREENACC,
          CHILD_STENDHIDINGSCREENACC, CHILD_STENDHIDINGSCREENACC_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HD2ROWNUM))
    {
      HiddenField child =
        new HiddenField(this, getdoRowGenerator2Model(), CHILD_HD2ROWNUM,
          doRowGenerator2Model.FIELD_DFROWNDX, CHILD_HD2ROWNUM_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STINSTITUTIONARRAY))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STINSTITUTIONARRAY,
          CHILD_STINSTITUTIONARRAY, CHILD_STINSTITUTIONARRAY_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STREGIONARRAY))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STREGIONARRAY, CHILD_STREGIONARRAY,
          CHILD_STREGIONARRAY_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STBRANCHARRAY))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STBRANCHARRAY, CHILD_STBRANCHARRAY,
          CHILD_STBRANCHARRAY_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STGROUPARRAY))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STGROUPARRAY, CHILD_STGROUPARRAY,
          CHILD_STGROUPARRAY_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPAGEACCESSARRAY))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPAGEACCESSARRAY,
          CHILD_STPAGEACCESSARRAY, CHILD_STPAGEACCESSARRAY_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STOLDSELECTEDHIGHERAPP))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STOLDSELECTEDHIGHERAPP,
          CHILD_STOLDSELECTEDHIGHERAPP, CHILD_STOLDSELECTEDHIGHERAPP_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STOLDSELECTEDJOINTAPP))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STOLDSELECTEDJOINTAPP,
          CHILD_STOLDSELECTEDJOINTAPP, CHILD_STOLDSELECTEDJOINTAPP_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_CHFORCETOCHANGEPW))
    {
      CheckBox child =
        new CheckBox(this, getDefaultModel(), CHILD_CHFORCETOCHANGEPW, CHILD_CHFORCETOCHANGEPW,
          "Y", "N", true, null);

      return child;
    }
    else if (name.equals(CHILD_CHLOCKPW))
    {
      CheckBox child =
        new CheckBox(this, getDefaultModel(), CHILD_CHLOCKPW, CHILD_CHLOCKPW, "Y", "N", false, null);

      return child;
    }
    else if (name.equals(CHILD_CHNEWPW))
    {
      CheckBox child =
        new CheckBox(this, getDefaultModel(), CHILD_CHNEWPW, CHILD_CHNEWPW, "Y", "N", true, null);

      return child;
    }
    else if (name.equals(CHILD_STLASTLOGINTIME))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STLASTLOGINTIME, CHILD_STLASTLOGINTIME,
          CHILD_STLASTLOGINTIME_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STLASTLOGOUTTIME))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STLASTLOGOUTTIME,
          CHILD_STLASTLOGOUTTIME, CHILD_STLASTLOGOUTTIME_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STLOGINFLAG))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STLOGINFLAG, CHILD_STLOGINFLAG,
          CHILD_STLOGINFLAG_RESET_VALUE, null);

      return child;
    }

    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> by BILLY 15July2002
    else if (name.equals(CHILD_BTACTMSG))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTACTMSG, CHILD_BTACTMSG,
          CHILD_BTACTMSG_RESET_VALUE,
          new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));

      return child;
    }

    //--Release2.1--//
    //// New link to toggle language. When touched this link should reload
    // the
    //// page in opposite language (french versus english) and set this new
    // session
    //// language id throughout the all modulus.
    else if (name.equals(CHILD_TOGGLELANGUAGEHREF))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_TOGGLELANGUAGEHREF, CHILD_TOGGLELANGUAGEHREF,
          CHILD_TOGGLELANGUAGEHREF_RESET_VALUE, null);

      return child;
    }

    //--Release2.1--//
    //--> Added Prefered Language ComboBox
    //--> By Billy 09Dec2002
    else if (name.equals(CHILD_CBDEFAULTLANGUAGE))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBDEFAULTLANGUAGE, CHILD_CBDEFAULTLANGUAGE,
          CHILD_CBDEFAULTLANGUAGE_RESET_VALUE, null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbDefaultLanguageOptions);

      return child;
    }

    //===================================
    //=================================================
    //--> CRF#37 :: Source/Firm to Group routing for TD
    //=================================================
    //--> Change to support multiple groups assignnment
    //--> By Billy 11Aug2003
    else if (name.equals(CHILD_LBGROUPS))
    {
      ListBox child =
        new ListBox(this, getDefaultModel(), CHILD_LBGROUPS, CHILD_LBGROUPS,
          CHILD_LBGROUPS_RESET_VALUE, null);
      child.setLabelForNoneSelected(CHILD_LBGROUPS_NONSELECTED_LABEL);
      child.setOptions(cbGroupProfileNameOptions);
      child.setMultiSelect(true);

      return child;
    }
    else if (name.equals(CHILD_TBNEWGROUPNAME))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBNEWGROUPNAME, CHILD_TBNEWGROUPNAME,
          CHILD_TBNEWGROUPNAME_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTCREATENEWGROUP))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTCREATENEWGROUP, CHILD_BTCREATENEWGROUP,
          CHILD_BTCREATENEWGROUP_RESET_VALUE, null);

      return child;
    }

    if (name.equals(CHILD_STBEGINHIDINGNEWGROUPBUTTON))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STBEGINHIDINGNEWGROUPBUTTON,
          CHILD_STBEGINHIDINGNEWGROUPBUTTON, CHILD_STBEGINHIDINGNEWGROUPBUTTON_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STENDHIDINGNEWGROUPBUTTON))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STENDHIDINGNEWGROUPBUTTON,
          CHILD_STENDHIDINGNEWGROUPBUTTON, CHILD_STENDHIDINGNEWGROUPBUTTON_RESET_VALUE, null);

      return child;
    }

    //===============================================
    else
    {
      throw new IllegalArgumentException("Invalid child name [" + name + "]");
    }
  }

  /**
   *
   *
   */
  public void resetChildren()
  {
      super.resetChildren();
      getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
    getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
    getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
    getHref1().setValue(CHILD_HREF1_RESET_VALUE);
    getHref2().setValue(CHILD_HREF2_RESET_VALUE);
    getHref3().setValue(CHILD_HREF3_RESET_VALUE);
    getHref4().setValue(CHILD_HREF4_RESET_VALUE);
    getHref5().setValue(CHILD_HREF5_RESET_VALUE);
    getHref6().setValue(CHILD_HREF6_RESET_VALUE);
    getHref7().setValue(CHILD_HREF7_RESET_VALUE);
    getHref8().setValue(CHILD_HREF8_RESET_VALUE);
    getHref9().setValue(CHILD_HREF9_RESET_VALUE);
    getHref10().setValue(CHILD_HREF10_RESET_VALUE);
    getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
    getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
    getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
    getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
    getBtSubmit().setValue(CHILD_BTSUBMIT_RESET_VALUE);
    getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
    getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
    getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
    getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
    getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
    getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
    getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
    getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
    getBtCancel().setValue(CHILD_BTCANCEL_RESET_VALUE);
    getBtPrevTaskPage().setValue(CHILD_BTPREVTASKPAGE_RESET_VALUE);
    getStPrevTaskPageLabel().setValue(CHILD_STPREVTASKPAGELABEL_RESET_VALUE);
    getBtNextTaskPage().setValue(CHILD_BTNEXTTASKPAGE_RESET_VALUE);
    getStNextTaskPageLabel().setValue(CHILD_STNEXTTASKPAGELABEL_RESET_VALUE);
    getStTaskName().setValue(CHILD_STTASKNAME_RESET_VALUE);
    getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
    getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
    getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
    getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
    getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
    getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
    getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
    getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
    getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
    getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
    getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
    getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
    getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
    getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
    getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
    getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
    getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
    getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
    getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
    getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
    getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
    getBtAddNewUser().setValue(CHILD_BTADDNEWUSER_RESET_VALUE);
    getCbUserNames().setValue(CHILD_CBUSERNAMES_RESET_VALUE);
    getCbProfileStatus().setValue(CHILD_CBPROFILESTATUS_RESET_VALUE);
    getBtGo().setValue(CHILD_BTGO_RESET_VALUE);
    getCbPartnerProfileNames().setValue(CHILD_CBPARTNERPROFILENAMES_RESET_VALUE);
    getCbStandIn().setValue(CHILD_CBSTANDIN_RESET_VALUE);
    getCbGroupProfileName().setValue(CHILD_CBGROUPPROFILENAME_RESET_VALUE);
    getCbHigherApprovers().setValue(CHILD_CBHIGHERAPPROVERS_RESET_VALUE);
    getCbJointApprovers().setValue(CHILD_CBJOINTAPPROVERS_RESET_VALUE);
    getCbSalutations().setValue(CHILD_CBSALUTATIONS_RESET_VALUE);
    getCbManagers().setValue(CHILD_CBMANAGERS_RESET_VALUE);
    getCbBranchNames().setValue(CHILD_CBBRANCHNAMES_RESET_VALUE);
    getCbRegionNames().setValue(CHILD_CBREGIONNAMES_RESET_VALUE);
    getCbInstitutionNames().setValue(CHILD_CBINSTITUTIONNAMES_RESET_VALUE);
    getCbLanguages().setValue(CHILD_CBLANGUAGES_RESET_VALUE);

    //-- ========== SCR#537 begins ========== --//
    //-- by Neil at Dec/02/2004
    getCbTaskAlert().setValue(CHILD_CBTASKALERT_RESET_VALUE);

    //-- ========== SCR#537 ends ========== --//
    getCbUserTypes().setValue(CHILD_CBUSERTYPES_RESET_VALUE);
    getTbUserTitle().setValue(CHILD_TBUSERTITLE_RESET_VALUE);
    getTbUserFirstName().setValue(CHILD_TBUSERFIRSTNAME_RESET_VALUE);
    getTbUserInitialName().setValue(CHILD_TBUSERINITIALNAME_RESET_VALUE);
    getTbUserLastName().setValue(CHILD_TBUSERLASTNAME_RESET_VALUE);
    getTbLoginId().setValue(CHILD_TBLOGINID_RESET_VALUE);
    getHdLoginId().setValue(CHILD_HDLOGINID_RESET_VALUE);
    getTbBusinessId().setValue(CHILD_TBBUSINESSID_RESET_VALUE);
    getTbWorkPhoneNumberAreaCode().setValue(CHILD_TBWORKPHONENUMBERAREACODE_RESET_VALUE);
    getTbWorkPhoneNumFirstThreeDigits().setValue(CHILD_TBWORKPHONENUMFIRSTTHREEDIGITS_RESET_VALUE);
    getTbWorkPhoneNumLastFourDigits().setValue(CHILD_TBWORKPHONENUMLASTFOURDIGITS_RESET_VALUE);
    getTbWorkPhoneNumExtension().setValue(CHILD_TBWORKPHONENUMEXTENSION_RESET_VALUE);
    getTbFaxNumberAreaCode().setValue(CHILD_TBFAXNUMBERAREACODE_RESET_VALUE);
    getTbFaxNumFirstThreeDigits().setValue(CHILD_TBFAXNUMFIRSTTHREEDIGITS_RESET_VALUE);
    getTbFaxNumLastFourDigits().setValue(CHILD_TBFAXNUMLASTFOURDIGITS_RESET_VALUE);
    getTbEmailAddress().setValue(CHILD_TBEMAILADDRESS_RESET_VALUE);
    getTbPassword().setValue(CHILD_TBPASSWORD_RESET_VALUE);
    getTbVerifyPassword().setValue(CHILD_TBVERIFYPASSWORD_RESET_VALUE);
    getTbLineOfBusiness1().setValue(CHILD_TBLINEOFBUSINESS1_RESET_VALUE);
    getTbLineOfBusiness2().setValue(CHILD_TBLINEOFBUSINESS2_RESET_VALUE);
    getTbLineOfBusiness3().setValue(CHILD_TBLINEOFBUSINESS3_RESET_VALUE);
    getStUserLabel().setValue(CHILD_STUSERLABEL_RESET_VALUE);
    getBtScreenAccess().setValue(CHILD_BTSCREENACCESS_RESET_VALUE);
    getRepeated1().resetChildren();
    getStBeginHidingSelAndGoButton().setValue(CHILD_STBEGINHIDINGSELANDGOBUTTON_RESET_VALUE);
    getStEndHidingSelAndGoButton().setValue(CHILD_STENDHIDINGSELANDGOBUTTON_RESET_VALUE);
    getStBeginHidingScreenAcc().setValue(CHILD_STBEGINHIDINGSCREENACC_RESET_VALUE);
    getStEndHidingScreenAcc().setValue(CHILD_STENDHIDINGSCREENACC_RESET_VALUE);
    getHd2RowNum().setValue(CHILD_HD2ROWNUM_RESET_VALUE);
    getStInstitutionArray().setValue(CHILD_STINSTITUTIONARRAY_RESET_VALUE);
    getStRegionArray().setValue(CHILD_STREGIONARRAY_RESET_VALUE);
    getStBranchArray().setValue(CHILD_STBRANCHARRAY_RESET_VALUE);
    getStGroupArray().setValue(CHILD_STGROUPARRAY_RESET_VALUE);
    getStPageAccessArray().setValue(CHILD_STPAGEACCESSARRAY_RESET_VALUE);
    getStOldSelectedHigherApp().setValue(CHILD_STOLDSELECTEDHIGHERAPP_RESET_VALUE);
    getStOldSelectedJointApp().setValue(CHILD_STOLDSELECTEDJOINTAPP_RESET_VALUE);
    getChForceToChangePW().setValue(CHILD_CHFORCETOCHANGEPW_RESET_VALUE);
    getChLockPW().setValue(CHILD_CHLOCKPW_RESET_VALUE);
    getChNewPW().setValue(CHILD_CHNEWPW_RESET_VALUE);
    getStLastLoginTime().setValue(CHILD_STLASTLOGINTIME_RESET_VALUE);
    getStLastLogoutTime().setValue(CHILD_STLASTLOGOUTTIME_RESET_VALUE);
    getStLoginFlag().setValue(CHILD_STLOGINFLAG_RESET_VALUE);

    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> Test by BILLY 15July2002
    getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);

    //--Release2.1--//
    //// New link to toggle the language. When touched this link should
    // reload the
    //// page in opposite language (french versus english) and set this new
    // language
    //// session value id throughout all modulus.
    getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);

    //--Release2.1--//
    //--> Added Prefered Language ComboBox
    //--> By Billy 09Dec2002
    getCbDefaultLanguage().setValue(CHILD_CBDEFAULTLANGUAGE_RESET_VALUE);

    //=================================================
    //--> CRF#37 :: Source/Firm to Group routing for TD
    //=================================================
    //--> Change to support multiple groups assignnment
    //--> By Billy 11Aug2003
    getLbGroups().setValue(CHILD_LBGROUPS_RESET_VALUE);
    getTbNewGroupName().setValue(CHILD_TBNEWGROUPNAME_RESET_VALUE);
    getBtCreateNewGroup().setValue(CHILD_BTCREATENEWGROUP_RESET_VALUE);
    getStBeginHidingNewGroupButton().setValue(CHILD_STBEGINHIDINGNEWGROUPBUTTON_RESET_VALUE);
    getStEndHidingNewGroupButton().setValue(CHILD_STENDHIDINGNEWGROUPBUTTON_RESET_VALUE);

    //=================================================
  }

  /**
   *
   *
   */
  protected void registerChildren()
  {
      super.registerChildren();
      registerChild(CHILD_TBDEALID, TextField.class);
    registerChild(CHILD_CBPAGENAMES, ComboBox.class);
    registerChild(CHILD_BTPROCEED, Button.class);
    registerChild(CHILD_HREF1, HREF.class);
    registerChild(CHILD_HREF2, HREF.class);
    registerChild(CHILD_HREF3, HREF.class);
    registerChild(CHILD_HREF4, HREF.class);
    registerChild(CHILD_HREF5, HREF.class);
    registerChild(CHILD_HREF6, HREF.class);
    registerChild(CHILD_HREF7, HREF.class);
    registerChild(CHILD_HREF8, HREF.class);
    registerChild(CHILD_HREF9, HREF.class);
    registerChild(CHILD_HREF10, HREF.class);
    registerChild(CHILD_STPAGELABEL, StaticTextField.class);
    registerChild(CHILD_STCOMPANYNAME, StaticTextField.class);
    registerChild(CHILD_STTODAYDATE, StaticTextField.class);
    registerChild(CHILD_STUSERNAMETITLE, StaticTextField.class);
    registerChild(CHILD_BTSUBMIT, Button.class);
    registerChild(CHILD_BTWORKQUEUELINK, Button.class);
    registerChild(CHILD_CHANGEPASSWORDHREF, HREF.class);
    registerChild(CHILD_BTTOOLHISTORY, Button.class);
    registerChild(CHILD_BTTOONOTES, Button.class);
    registerChild(CHILD_BTTOOLSEARCH, Button.class);
    registerChild(CHILD_BTTOOLLOG, Button.class);
    registerChild(CHILD_STERRORFLAG, StaticTextField.class);
    registerChild(CHILD_DETECTALERTTASKS, HiddenField.class);
    registerChild(CHILD_BTCANCEL, Button.class);
    registerChild(CHILD_BTPREVTASKPAGE, Button.class);
    registerChild(CHILD_STPREVTASKPAGELABEL, StaticTextField.class);
    registerChild(CHILD_BTNEXTTASKPAGE, Button.class);
    registerChild(CHILD_STNEXTTASKPAGELABEL, StaticTextField.class);
    registerChild(CHILD_STTASKNAME, StaticTextField.class);
    registerChild(CHILD_SESSIONUSERID, HiddenField.class);
    registerChild(CHILD_STPMGENERATE, StaticTextField.class);
    registerChild(CHILD_STPMHASTITLE, StaticTextField.class);
    registerChild(CHILD_STPMHASINFO, StaticTextField.class);
    registerChild(CHILD_STPMHASTABLE, StaticTextField.class);
    registerChild(CHILD_STPMHASOK, StaticTextField.class);
    registerChild(CHILD_STPMTITLE, StaticTextField.class);
    registerChild(CHILD_STPMINFOMSG, StaticTextField.class);
    registerChild(CHILD_STPMONOK, StaticTextField.class);
    registerChild(CHILD_STPMMSGS, StaticTextField.class);
    registerChild(CHILD_STPMMSGTYPES, StaticTextField.class);
    registerChild(CHILD_STAMGENERATE, StaticTextField.class);
    registerChild(CHILD_STAMHASTITLE, StaticTextField.class);
    registerChild(CHILD_STAMHASINFO, StaticTextField.class);
    registerChild(CHILD_STAMHASTABLE, StaticTextField.class);
    registerChild(CHILD_STAMTITLE, StaticTextField.class);
    registerChild(CHILD_STAMINFOMSG, StaticTextField.class);
    registerChild(CHILD_STAMMSGS, StaticTextField.class);
    registerChild(CHILD_STAMMSGTYPES, StaticTextField.class);
    registerChild(CHILD_STAMDIALOGMSG, StaticTextField.class);
    registerChild(CHILD_STAMBUTTONSHTML, StaticTextField.class);
    registerChild(CHILD_BTADDNEWUSER, Button.class);
    registerChild(CHILD_CBUSERNAMES, ComboBox.class);
    registerChild(CHILD_CBPROFILESTATUS, ComboBox.class);
    registerChild(CHILD_BTGO, Button.class);
    registerChild(CHILD_CBPARTNERPROFILENAMES, ComboBox.class);
    registerChild(CHILD_CBSTANDIN, ComboBox.class);
    registerChild(CHILD_CBGROUPPROFILENAME, ComboBox.class);
    registerChild(CHILD_CBHIGHERAPPROVERS, ComboBox.class);
    registerChild(CHILD_CBJOINTAPPROVERS, ComboBox.class);
    registerChild(CHILD_CBSALUTATIONS, ComboBox.class);
    registerChild(CHILD_CBMANAGERS, ComboBox.class);
    registerChild(CHILD_CBBRANCHNAMES, ComboBox.class);
    registerChild(CHILD_CBREGIONNAMES, ComboBox.class);
    registerChild(CHILD_CBINSTITUTIONNAMES, ComboBox.class);
    registerChild(CHILD_CBLANGUAGES, ComboBox.class);

    //-- ========== SCR#537 begins ========== --//
    //-- by Neil on Dec/02/2004
    registerChild(CHILD_CBTASKALERT, ComboBox.class);

    //-- ========== SCR#537 ends ========== --//
    registerChild(CHILD_CBUSERTYPES, ComboBox.class);
    registerChild(CHILD_TBUSERTITLE, TextField.class);
    registerChild(CHILD_TBUSERFIRSTNAME, TextField.class);
    registerChild(CHILD_TBUSERINITIALNAME, TextField.class);
    registerChild(CHILD_TBUSERLASTNAME, TextField.class);
    registerChild(CHILD_TBLOGINID, TextField.class);
    registerChild(CHILD_HDLOGINID, HiddenField.class);
    registerChild(CHILD_TBBUSINESSID, TextField.class);
    registerChild(CHILD_TBWORKPHONENUMBERAREACODE, TextField.class);
    registerChild(CHILD_TBWORKPHONENUMFIRSTTHREEDIGITS, TextField.class);
    registerChild(CHILD_TBWORKPHONENUMLASTFOURDIGITS, TextField.class);
    registerChild(CHILD_TBWORKPHONENUMEXTENSION, TextField.class);
    registerChild(CHILD_TBFAXNUMBERAREACODE, TextField.class);
    registerChild(CHILD_TBFAXNUMFIRSTTHREEDIGITS, TextField.class);
    registerChild(CHILD_TBFAXNUMLASTFOURDIGITS, TextField.class);
    registerChild(CHILD_TBEMAILADDRESS, TextField.class);
    registerChild(CHILD_TBPASSWORD, TextField.class);
    registerChild(CHILD_TBVERIFYPASSWORD, TextField.class);
    registerChild(CHILD_TBLINEOFBUSINESS1, TextField.class);
    registerChild(CHILD_TBLINEOFBUSINESS2, TextField.class);
    registerChild(CHILD_TBLINEOFBUSINESS3, TextField.class);
    registerChild(CHILD_STUSERLABEL, StaticTextField.class);
    registerChild(CHILD_BTSCREENACCESS, Button.class);
    registerChild(CHILD_REPEATED1, pgUserAdministrationRepeated1TiledView.class);
    registerChild(CHILD_STBEGINHIDINGSELANDGOBUTTON, StaticTextField.class);
    registerChild(CHILD_STENDHIDINGSELANDGOBUTTON, StaticTextField.class);
    registerChild(CHILD_STBEGINHIDINGSCREENACC, StaticTextField.class);
    registerChild(CHILD_STENDHIDINGSCREENACC, StaticTextField.class);
    registerChild(CHILD_HD2ROWNUM, HiddenField.class);
    registerChild(CHILD_STINSTITUTIONARRAY, StaticTextField.class);
    registerChild(CHILD_STREGIONARRAY, StaticTextField.class);
    registerChild(CHILD_STBRANCHARRAY, StaticTextField.class);
    registerChild(CHILD_STGROUPARRAY, StaticTextField.class);
    registerChild(CHILD_STPAGEACCESSARRAY, StaticTextField.class);
    registerChild(CHILD_STOLDSELECTEDHIGHERAPP, StaticTextField.class);
    registerChild(CHILD_STOLDSELECTEDJOINTAPP, StaticTextField.class);
    registerChild(CHILD_CHFORCETOCHANGEPW, CheckBox.class);
    registerChild(CHILD_CHLOCKPW, CheckBox.class);
    registerChild(CHILD_CHNEWPW, CheckBox.class);
    registerChild(CHILD_STLASTLOGINTIME, StaticTextField.class);
    registerChild(CHILD_STLASTLOGOUTTIME, StaticTextField.class);
    registerChild(CHILD_STLOGINFLAG, StaticTextField.class);

    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> Test by BILLY 15July2002
    registerChild(CHILD_BTACTMSG, Button.class);

    //--Release2.1--//
    //// New link to toggle the language. When touched this link should
    // reload the
    //// page in opposite language (french versus english) and set this new
    // language
    //// session value id throughout all modulus.
    registerChild(CHILD_TOGGLELANGUAGEHREF, HREF.class);

    //--Release2.1--//
    //--> Added Default Language ComboBox
    //--> By Billy 09Dec2002
    registerChild(CHILD_CBDEFAULTLANGUAGE, ComboBox.class);

    //=================================================
    //--> CRF#37 :: Source/Firm to Group routing for TD
    //=================================================
    //--> Change to support multiple groups assignnment
    //--> By Billy 11Aug2003
    registerChild(CHILD_LBGROUPS, ListBox.class);
    registerChild(CHILD_TBNEWGROUPNAME, TextField.class);
    registerChild(CHILD_BTCREATENEWGROUP, Button.class);
    registerChild(CHILD_STBEGINHIDINGNEWGROUPBUTTON, StaticTextField.class);
    registerChild(CHILD_STENDHIDINGNEWGROUPBUTTON, StaticTextField.class);

    //=================================================
  }

  ////////////////////////////////////////////////////////////////////////////////
  // Model management methods
  ////////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   */
  public Model[] getWebActionModels(int executionType)
  {
    List modelList = new ArrayList();

    switch (executionType)
    {
    case MODEL_TYPE_RETRIEVE:
      modelList.add(getdoRowGenerator2Model());
      ;

      break;

    case MODEL_TYPE_UPDATE:
      break;

    case MODEL_TYPE_DELETE:
      ;

      break;

    case MODEL_TYPE_INSERT:
      ;

      break;

    case MODEL_TYPE_EXECUTE:
      ;

      break;
    }

    return (Model[]) modelList.toArray(new Model[0]);
  }

  ////////////////////////////////////////////////////////////////////////////////
  // View flow control methods
  ////////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   */
  public void beginDisplay(DisplayEvent event)
    throws ModelControlException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //Setup Options for cbPageNames
//    cbPageNamesOptions..populate(getRequestContext());
    int langId = handler.getTheSessionState().getLanguageId();
    cbPageNamesOptions.populate(getRequestContext(), langId);

    //Setup NoneSelected Label for cbPageNames
    CHILD_CBPAGENAMES_NONSELECTED_LABEL =
      BXResources.getGenericMsg("CBPAGENAMES_NONSELECTED_LABEL",
        handler.getTheSessionState().getLanguageId());

    //Check if the cbPageNames is already created
    if (getCbPageNames() != null)
    {
      getCbPageNames().setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
    }

    //Populate ProfileStatus options
    cbProfileStatusOptions.populate(getRequestContext());

    //Populate Salutations options
    cbSalutationsOptions.populate(getRequestContext());

    //Populate InstitutionNames options
    cbInstitutionNamesOptions.populate(getRequestContext());

    //Populate UserTypes options
    cbUserTypesOptions.populate(getRequestContext());

    //Populate DefaultLanguage options
    cbDefaultLanguageOptions.populate(getRequestContext());

    //Setup the Yes/No Options
    String yesStr =
      BXResources.getGenericMsg("YES_LABEL", handler.getTheSessionState().getLanguageId());
    String noStr =
      BXResources.getGenericMsg("NO_LABEL", handler.getTheSessionState().getLanguageId());
    cbLanguagesOptions.setOptions(new String[] { noStr, yesStr }, new String[] { "N", "Y" });

    //-- ========== SCR#537 begins ========== --//
    //-- by Neil at Dec/02/2004
    cbTaskAlertOptions.setOptions(new String[] { noStr, yesStr }, new String[] { "N", "Y" });
    //-- ========== SCR#537 ends ========== --//
    //Note: all the other Combo box options are populated in the
    // userAdminHandler
    //Setup NoneSelected Labels
    CHILD_CBUSERNAMES_NONSELECTED_LABEL =
      BXResources.getGenericMsg("CBUSERNAMES_NONSELECTED_LABEL",
        handler.getTheSessionState().getLanguageId());

    if (this.getCbUserNames() != null)
    {
      getCbUserNames().setLabelForNoneSelected(CHILD_CBUSERNAMES_NONSELECTED_LABEL);
    }

    CHILD_CBPARTNERPROFILENAMES_NONSELECTED_LABEL =
      BXResources.getGenericMsg("CBPARTNERPROFILENAMES_NONSELECTED_LABEL",
        handler.getTheSessionState().getLanguageId());

    if (this.getCbPartnerProfileNames() != null)
    {
      getCbPartnerProfileNames().setLabelForNoneSelected(CHILD_CBPARTNERPROFILENAMES_NONSELECTED_LABEL);
    }

    CHILD_CBSTANDIN_NONSELECTED_LABEL =
      BXResources.getGenericMsg("CBSTANDIN_NONSELECTED_LABEL",
        handler.getTheSessionState().getLanguageId());

    if (this.getCbStandIn() != null)
    {
      getCbStandIn().setLabelForNoneSelected(CHILD_CBSTANDIN_NONSELECTED_LABEL);
    }

    CHILD_CBGROUPPROFILENAME_NONSELECTED_LABEL =
      BXResources.getGenericMsg("CBGROUPPROFILENAME_NONSELECTED_LABEL",
        handler.getTheSessionState().getLanguageId());

    if (this.getCbGroupProfileName() != null)
    {
      getCbGroupProfileName().setLabelForNoneSelected(CHILD_CBGROUPPROFILENAME_NONSELECTED_LABEL);
    }

    if (this.getLbGroups() != null)
    {
      getLbGroups().setLabelForNoneSelected(CHILD_CBGROUPPROFILENAME_NONSELECTED_LABEL);
    }

    CHILD_CBHIGHERAPPROVERS_NONSELECTED_LABEL =
      BXResources.getGenericMsg("CBHIGHERAPPROVERS_NONSELECTED_LABEL",
        handler.getTheSessionState().getLanguageId());

    if (this.getCbHigherApprovers() != null)
    {
      getCbHigherApprovers().setLabelForNoneSelected(CHILD_CBHIGHERAPPROVERS_NONSELECTED_LABEL);
    }

    CHILD_CBJOINTAPPROVERS_NONSELECTED_LABEL =
      BXResources.getGenericMsg("CBJOINTAPPROVERS_NONSELECTED_LABEL",
        handler.getTheSessionState().getLanguageId());

    if (this.getCbJointApprovers() != null)
    {
      getCbJointApprovers().setLabelForNoneSelected(CHILD_CBJOINTAPPROVERS_NONSELECTED_LABEL);
    }

    CHILD_CBMANAGERS_NONSELECTED_LABEL =
      BXResources.getGenericMsg("CBMANAGERS_NONSELECTED_LABEL",
        handler.getTheSessionState().getLanguageId());

    if (this.getCbManagers() != null)
    {
      getCbManagers().setLabelForNoneSelected(CHILD_CBMANAGERS_NONSELECTED_LABEL);
    }

    CHILD_CBBRANCHNAMES_NONSELECTED_LABEL =
      BXResources.getGenericMsg("CBBRANCHNAMES_NONSELECTED_LABEL",
        handler.getTheSessionState().getLanguageId());

    if (this.getCbBranchNames() != null)
    {
      getCbBranchNames().setLabelForNoneSelected(CHILD_CBBRANCHNAMES_NONSELECTED_LABEL);
    }

    CHILD_CBREGIONNAMES_NONSELECTED_LABEL =
      BXResources.getGenericMsg("CBREGIONNAMES_NONSELECTED_LABEL",
        handler.getTheSessionState().getLanguageId());

    if (this.getCbRegionNames() != null)
    {
      getCbRegionNames().setLabelForNoneSelected(CHILD_CBREGIONNAMES_NONSELECTED_LABEL);
    }

    CHILD_CBINSTITUTIONNAMES_NONSELECTED_LABEL =
      BXResources.getGenericMsg("CBINSTITUTIONNAMES_NONSELECTED_LABEL",
        handler.getTheSessionState().getLanguageId());

    if (this.getCbInstitutionNames() != null)
    {
      getCbInstitutionNames().setLabelForNoneSelected(CHILD_CBINSTITUTIONNAMES_NONSELECTED_LABEL);
    }

    handler.pageSaveState();
    super.beginDisplay(event);
  }

  /**
   *
   *
   */
  public boolean beforeModelExecutes(Model model, int executionContext)
  {
    // The following code block was migrated from the
    // this_onBeforeDataObjectExecuteEvent method
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);
    handler.setupBeforePageGeneration();
    handler.pageSaveState();

    return super.beforeModelExecutes(model, executionContext);
  }

  /**
   *
   *
   */
  public void afterModelExecutes(Model model, int executionContext)
  {
    // This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
    super.afterModelExecutes(model, executionContext);
  }

  /**
   *
   *
   */
  public void afterAllModelsExecute(int executionContext)
  {
    // This is the analog of NetDynamics
    // this_onAfterAllDataObjectsExecuteEvent
    super.afterAllModelsExecute(executionContext);

    // The following code block was migrated from the
    // this_onBeforeRowDisplayEvent method
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);
    handler.populatePageDisplayFields();
    handler.pageSaveState();
  }

  /**
   *
   *
   */
  public void onModelError(Model model, int executionContext, ModelControlException exception)
    throws ModelControlException
  {
    // This is the analog of NetDynamics this_onDataObjectErrorEvent
    super.onModelError(model, executionContext, exception);
  }

  /**
   *
   *
   */
  public TextField getTbDealId()
  {
    return (TextField) getChild(CHILD_TBDEALID);
  }

  /**
   *
   *
   */
  public ComboBox getCbPageNames()
  {
    return (ComboBox) getChild(CHILD_CBPAGENAMES);
  }

  //--DJ_Ticket661--end--//

  /**
   *
   *
   */
  public void handleBtProceedRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleGoPage();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtProceed()
  {
    return (Button) getChild(CHILD_BTPROCEED);
  }

  /**
   *
   *
   */
  public void handleHref1Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(0);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref1()
  {
    return (HREF) getChild(CHILD_HREF1);
  }

  /**
   *
   *
   */
  public void handleHref2Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(1);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref2()
  {
    return (HREF) getChild(CHILD_HREF2);
  }

  /**
   *
   *
   */
  public void handleHref3Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(2);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref3()
  {
    return (HREF) getChild(CHILD_HREF3);
  }

  /**
   *
   *
   */
  public void handleHref4Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(3);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref4()
  {
    return (HREF) getChild(CHILD_HREF4);
  }

  /**
   *
   *
   */
  public void handleHref5Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(4);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref5()
  {
    return (HREF) getChild(CHILD_HREF5);
  }

  /**
   *
   *
   */
  public void handleHref6Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(5);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref6()
  {
    return (HREF) getChild(CHILD_HREF6);
  }

  /**
   *
   *
   */
  public void handleHref7Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(6);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref7()
  {
    return (HREF) getChild(CHILD_HREF7);
  }

  /**
   *
   *
   */
  public void handleHref8Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(7);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref8()
  {
    return (HREF) getChild(CHILD_HREF8);
  }

  /**
   *
   *
   */
  public void handleHref9Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(8);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref9()
  {
    return (HREF) getChild(CHILD_HREF9);
  }

  /**
   *
   *
   */
  public void handleHref10Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleOpenDialogLink(9);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getHref10()
  {
    return (HREF) getChild(CHILD_HREF10);
  }

  /**
   *
   *
   */
  public StaticTextField getStPageLabel()
  {
    return (StaticTextField) getChild(CHILD_STPAGELABEL);
  }

  /**
   *
   *
   */
  public StaticTextField getStCompanyName()
  {
    return (StaticTextField) getChild(CHILD_STCOMPANYNAME);
  }

  /**
   *
   *
   */
  public StaticTextField getStTodayDate()
  {
    return (StaticTextField) getChild(CHILD_STTODAYDATE);
  }

  /**
   *
   *
   */
  public StaticTextField getStUserNameTitle()
  {
    return (StaticTextField) getChild(CHILD_STUSERNAMETITLE);
  }

  /**
   *
   *
   */
  public void handleBtSubmitRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleSubmitStandard();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtSubmit()
  {
    return (Button) getChild(CHILD_BTSUBMIT);
  }

  /**
   *
   *
   */
  public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleDisplayWorkQueue();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtWorkQueueLink()
  {
    return (Button) getChild(CHILD_BTWORKQUEUELINK);
  }

  /**
   *
   *
   */
  public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleChangePassword();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getChangePasswordHref()
  {
    return (HREF) getChild(CHILD_CHANGEPASSWORDHREF);
  }

  //--Release2.1--start//
  //// Two new methods providing the business logic for the new link to
  // toggle language.
  //// When touched this link should reload the page in opposite language
  // (french versus english)
  //// and set this new session value.

  /**
   *
   *
   */
  public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this, true);
    handler.handleToggleLanguage();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getToggleLanguageHref()
  {
    return (HREF) getChild(CHILD_TOGGLELANGUAGEHREF);
  }

  //--Release2.1--end//

  /**
   *
   *
   */
  public void handleBtToolHistoryRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleDisplayDealHistory();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtToolHistory()
  {
    return (Button) getChild(CHILD_BTTOOLHISTORY);
  }

  /**
   *
   *
   */
  public void handleBtTooNotesRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleDisplayDealNotes();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtTooNotes()
  {
    return (Button) getChild(CHILD_BTTOONOTES);
  }

  /**
   *
   *
   */
  public void handleBtToolSearchRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleDealSearch();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtToolSearch()
  {
    return (Button) getChild(CHILD_BTTOOLSEARCH);
  }

  /**
   *
   *
   */
  public void handleBtToolLogRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleSignOff();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtToolLog()
  {
    return (Button) getChild(CHILD_BTTOOLLOG);
  }

  /**
   *
   *
   */
  public StaticTextField getStErrorFlag()
  {
    return (StaticTextField) getChild(CHILD_STERRORFLAG);
  }

  /**
   *
   *
   */
  public HiddenField getDetectAlertTasks()
  {
    return (HiddenField) getChild(CHILD_DETECTALERTTASKS);
  }

  /**
   *
   *
   */
  public void handleBtCancelRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleCancel();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtCancel()
  {
    return (Button) getChild(CHILD_BTCANCEL);
  }

  /**
   *
   *
   */
  public void handleBtPrevTaskPageRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handlePrevTaskPage();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtPrevTaskPage()
  {
    return (Button) getChild(CHILD_BTPREVTASKPAGE);
  }

  /**
   *
   *
   */
  public String endBtPrevTaskPageDisplay(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    boolean rc = handler.generatePrevTaskPage();
    handler.pageSaveState();

    if (rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public StaticTextField getStPrevTaskPageLabel()
  {
    return (StaticTextField) getChild(CHILD_STPREVTASKPAGELABEL);
  }

  /**
   *
   *
   */
  public String endStPrevTaskPageLabelDisplay(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    boolean rc = handler.generatePrevTaskPage();
    handler.pageSaveState();

    if (rc = true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public void handleBtNextTaskPageRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleNextTaskPage();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtNextTaskPage()
  {
    return (Button) getChild(CHILD_BTNEXTTASKPAGE);
  }

  /**
   *
   *
   */
  public String endBtNextTaskPageDisplay(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    boolean rc = handler.generateNextTaskPage();
    handler.pageSaveState();

    if (rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public StaticTextField getStNextTaskPageLabel()
  {
    return (StaticTextField) getChild(CHILD_STNEXTTASKPAGELABEL);
  }

  /**
   *
   *
   */
  public String endStNextTaskPageLabelDisplay(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    boolean rc = handler.generateNextTaskPage();
    handler.pageSaveState();

    if (rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public StaticTextField getStTaskName()
  {
    return (StaticTextField) getChild(CHILD_STTASKNAME);
  }

  /**
   *
   *
   */
  public String endStTaskNameDisplay(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    boolean rc = handler.generateTaskName();
    handler.pageSaveState();

    if (rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public HiddenField getSessionUserId()
  {
    return (HiddenField) getChild(CHILD_SESSIONUSERID);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmGenerate()
  {
    return (StaticTextField) getChild(CHILD_STPMGENERATE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmHasTitle()
  {
    return (StaticTextField) getChild(CHILD_STPMHASTITLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmHasInfo()
  {
    return (StaticTextField) getChild(CHILD_STPMHASINFO);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmHasTable()
  {
    return (StaticTextField) getChild(CHILD_STPMHASTABLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmHasOk()
  {
    return (StaticTextField) getChild(CHILD_STPMHASOK);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmTitle()
  {
    return (StaticTextField) getChild(CHILD_STPMTITLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmInfoMsg()
  {
    return (StaticTextField) getChild(CHILD_STPMINFOMSG);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmOnOk()
  {
    return (StaticTextField) getChild(CHILD_STPMONOK);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmMsgs()
  {
    return (StaticTextField) getChild(CHILD_STPMMSGS);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmMsgTypes()
  {
    return (StaticTextField) getChild(CHILD_STPMMSGTYPES);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmGenerate()
  {
    return (StaticTextField) getChild(CHILD_STAMGENERATE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmHasTitle()
  {
    return (StaticTextField) getChild(CHILD_STAMHASTITLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmHasInfo()
  {
    return (StaticTextField) getChild(CHILD_STAMHASINFO);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmHasTable()
  {
    return (StaticTextField) getChild(CHILD_STAMHASTABLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmTitle()
  {
    return (StaticTextField) getChild(CHILD_STAMTITLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmInfoMsg()
  {
    return (StaticTextField) getChild(CHILD_STAMINFOMSG);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmMsgs()
  {
    return (StaticTextField) getChild(CHILD_STAMMSGS);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmMsgTypes()
  {
    return (StaticTextField) getChild(CHILD_STAMMSGTYPES);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmDialogMsg()
  {
    return (StaticTextField) getChild(CHILD_STAMDIALOGMSG);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmButtonsHtml()
  {
    return (StaticTextField) getChild(CHILD_STAMBUTTONSHTML);
  }

  /**
   *
   *
   */
  public void handleBtAddNewUserRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
	//-- ========== SCR#239 begins ========== --//
	//-- by Neil on Jan/27/2005
    isAddNewUserMode = true;
	//-- ========== SCR#239 ends ========== --//

    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleAddNewUser();
    handler.postHandlerProtocol();
	//-- ========== SCR#239 begins ========== --//
	//-- by Neil on Jan/27/2005
    //isAddNewUserMode = false;
	//-- ========== SCR#239 ends ========== --//
  }

  /**
   *
   *
   */
  public Button getBtAddNewUser()
  {
    return (Button) getChild(CHILD_BTADDNEWUSER);
  }

  /**
   *
   *
   */
  public ComboBox getCbUserNames()
  {
    return (ComboBox) getChild(CHILD_CBUSERNAMES);
  }

  /**
   *
   *
   */
  public ComboBox getCbProfileStatus()
  {
    return (ComboBox) getChild(CHILD_CBPROFILESTATUS);
  }

  /**
   *
   *
   */
  public void handleBtGoRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleDisplayUserDetails();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtGo()
  {
    return (Button) getChild(CHILD_BTGO);
  }

  /**
   *
   *
   */
  public ComboBox getCbPartnerProfileNames()
  {
    return (ComboBox) getChild(CHILD_CBPARTNERPROFILENAMES);
  }

  /**
   *
   *
   */
  public ComboBox getCbStandIn()
  {
    return (ComboBox) getChild(CHILD_CBSTANDIN);
  }

  /**
   *
   *
   */
  public ComboBox getCbGroupProfileName()
  {
    return (ComboBox) getChild(CHILD_CBGROUPPROFILENAME);
  }

  /**
   *
   *
   */
  public ComboBox getCbHigherApprovers()
  {
    return (ComboBox) getChild(CHILD_CBHIGHERAPPROVERS);
  }

  /**
   *
   *
   */
  public boolean beginCbHigherApproversDisplay(ChildDisplayEvent event)
  {
    // The following code block was migrated from the
    // cbHigherApprovers_onBeforeDisplayEvent method
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);
    handler.setDefHighJointApprover(getCbHigherApprovers());
    handler.pageSaveState();

    return true;
  }

  /**
   *
   *
   */
  public ComboBox getCbJointApprovers()
  {
    return (ComboBox) getChild(CHILD_CBJOINTAPPROVERS);
  }

  /**
   *
   *
   */
  public boolean beginCbJointApproversDisplay(ChildDisplayEvent event)
  {
    // The following code block was migrated from the
    // cbJointApprovers_onBeforeDisplayEvent method
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);
    handler.setDefHighJointApprover(getCbJointApprovers());
    handler.pageSaveState();

    return true;
  }

  /**
   *
   *
   */
  public ComboBox getCbSalutations()
  {
    return (ComboBox) getChild(CHILD_CBSALUTATIONS);
  }

  /**
   *
   *
   */
  public ComboBox getCbManagers()
  {
    return (ComboBox) getChild(CHILD_CBMANAGERS);
  }

  /**
   *
   *
   */
  public ComboBox getCbBranchNames()
  {
    return (ComboBox) getChild(CHILD_CBBRANCHNAMES);
  }

  /**
   *
   *
   */
  public ComboBox getCbRegionNames()
  {
    return (ComboBox) getChild(CHILD_CBREGIONNAMES);
  }

  /**
   *
   *
   */
  public ComboBox getCbInstitutionNames()
  {
    return (ComboBox) getChild(CHILD_CBINSTITUTIONNAMES);
  }

  /**
   *
   *
   */
  public ComboBox getCbLanguages()
  {
    return (ComboBox) getChild(CHILD_CBLANGUAGES);
  }

  //-- ========== SCR#537 begins ========== --//

  /**
   * DOCUMENT ME!
   *
   * @return
   */
  public ComboBox getCbTaskAlert()
  {
    return (ComboBox) getChild(CHILD_CBTASKALERT);
  }

  //-- ========== SCR#537 ends ========== --//

  /**
   *
   *
   */
  public ComboBox getCbUserTypes()
  {
    return (ComboBox) getChild(CHILD_CBUSERTYPES);
  }

  /**
   *
   *
   */

  //--> Added special handle to check if field is hidden
  //--> By Billy 07Mar2003
  public String endCbUserTypesDisplay(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String finalHtml;
    boolean isHidden = handler.isFieldHidden(event.getChildName());

    if (isHidden == false)
    {
      finalHtml = handler.removeJScriptFunction(event.getContent());
    }
    else
    {
      finalHtml = UserAdminConfigManager.HIDDENTAG;
    }

    handler.pageSaveState();

    return finalHtml;
  }

  /**
   *
   *
   */
  public TextField getTbUserTitle()
  {
    return (TextField) getChild(CHILD_TBUSERTITLE);
  }

  /**
   *
   *
   */
  public TextField getTbUserFirstName()
  {
    return (TextField) getChild(CHILD_TBUSERFIRSTNAME);
  }

  /**
   *
   *
   */
  public TextField getTbUserInitialName()
  {
    return (TextField) getChild(CHILD_TBUSERINITIALNAME);
  }

  /**
   *
   *
   */
  public TextField getTbUserLastName()
  {
    return (TextField) getChild(CHILD_TBUSERLASTNAME);
  }

  /**
   *
   *
   */
  public TextField getTbLoginId()
  {
    return (TextField) getChild(CHILD_TBLOGINID);
  }

  /**
  *
  *
  */
  public HiddenField getHdLoginId()
  {
   return (HiddenField) getChild(CHILD_HDLOGINID);
  }
 
  /**
   *
   *
   */
  public TextField getTbBusinessId()
  {
    return (TextField) getChild(CHILD_TBBUSINESSID);
  }

  /**
   *
   *
   */
  public TextField getTbWorkPhoneNumberAreaCode()
  {
    return (TextField) getChild(CHILD_TBWORKPHONENUMBERAREACODE);
  }

  /**
   *
   *
   */
  public TextField getTbWorkPhoneNumFirstThreeDigits()
  {
    return (TextField) getChild(CHILD_TBWORKPHONENUMFIRSTTHREEDIGITS);
  }

  /**
   *
   *
   */
  public TextField getTbWorkPhoneNumLastFourDigits()
  {
    return (TextField) getChild(CHILD_TBWORKPHONENUMLASTFOURDIGITS);
  }

  /**
   *
   *
   */
  public TextField getTbWorkPhoneNumExtension()
  {
    return (TextField) getChild(CHILD_TBWORKPHONENUMEXTENSION);
  }

  /**
   *
   *
   */
  public TextField getTbFaxNumberAreaCode()
  {
    return (TextField) getChild(CHILD_TBFAXNUMBERAREACODE);
  }

  /**
   *
   *
   */
  public TextField getTbFaxNumFirstThreeDigits()
  {
    return (TextField) getChild(CHILD_TBFAXNUMFIRSTTHREEDIGITS);
  }

  /**
   *
   *
   */
  public TextField getTbFaxNumLastFourDigits()
  {
    return (TextField) getChild(CHILD_TBFAXNUMLASTFOURDIGITS);
  }

  /**
   *
   *
   */
  public TextField getTbEmailAddress()
  {
    return (TextField) getChild(CHILD_TBEMAILADDRESS);
  }

  /**
   *
   *
   */
  public TextField getTbPassword()
  {
    return (TextField) getChild(CHILD_TBPASSWORD);
  }

  /**
   *
   *
   */
  public TextField getTbVerifyPassword()
  {
    return (TextField) getChild(CHILD_TBVERIFYPASSWORD);
  }

  /**
   *
   *
   */
  public TextField getTbLineOfBusiness1()
  {
    return (TextField) getChild(CHILD_TBLINEOFBUSINESS1);
  }

  /**
   *
   *
   */
  public TextField getTbLineOfBusiness2()
  {
    return (TextField) getChild(CHILD_TBLINEOFBUSINESS2);
  }

  /**
   *
   *
   */
  public TextField getTbLineOfBusiness3()
  {
    return (TextField) getChild(CHILD_TBLINEOFBUSINESS3);
  }

  /**
   *
   *
   */
  public StaticTextField getStUserLabel()
  {
    return (StaticTextField) getChild(CHILD_STUSERLABEL);
  }

  /**
   *
   *
   */
  public String endStUserLabelDisplay(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String finalHtml = handler.generateUserLabel();
    handler.pageSaveState();

    return finalHtml;
  }

  /**
   *
   *
   */
  public void handleBtScreenAccessRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleScreenAccess();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtScreenAccess()
  {
    return (Button) getChild(CHILD_BTSCREENACCESS);
  }

  /**
   *
   *
   */
  public pgUserAdministrationRepeated1TiledView getRepeated1()
  {
    return (pgUserAdministrationRepeated1TiledView) getChild(CHILD_REPEATED1);
  }

  /**
   *
   *
   */
  public StaticTextField getStBeginHidingSelAndGoButton()
  {
    return (StaticTextField) getChild(CHILD_STBEGINHIDINGSELANDGOBUTTON);
  }

  /**
   *
   *
   */
  public String endStBeginHidingSelAndGoButtonDisplay(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String finalHtml = handler.generateBeginingOfSelectUserComment();
    handler.pageSaveState();

    return finalHtml;
  }

  /**
   *
   *
   */
  public StaticTextField getStEndHidingSelAndGoButton()
  {
    return (StaticTextField) getChild(CHILD_STENDHIDINGSELANDGOBUTTON);
  }

  /**
   *
   *
   */
  public String endStEndHidingSelAndGoButtonDisplay(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String finalHtml = handler.generateEndingOfSelectUserComment();
    handler.pageSaveState();

    return finalHtml;
  }

  /**
   *
   *
   */
  public StaticTextField getStBeginHidingScreenAcc()
  {
    return (StaticTextField) getChild(CHILD_STBEGINHIDINGSCREENACC);
  }

  /**
   *
   *
   */
  public String endStBeginHidingScreenAccDisplay(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String finalHtml = handler.generateBeginingOfScreenAccComment();
    handler.pageSaveState();

    return finalHtml;
  }

  /**
   *
   *
   */
  public StaticTextField getStEndHidingScreenAcc()
  {
    return (StaticTextField) getChild(CHILD_STENDHIDINGSCREENACC);
  }

  /**
   *
   *
   */
  public String endStEndHidingScreenAccDisplay(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String finalHtml = handler.generateEndingOfScreenAccComment();
    handler.pageSaveState();

    return finalHtml;
  }

  /**
   *
   *
   */
  public HiddenField getHd2RowNum()
  {
    return (HiddenField) getChild(CHILD_HD2ROWNUM);
  }

  /**
   *
   *
   */
  public StaticTextField getStInstitutionArray()
  {
    return (StaticTextField) getChild(CHILD_STINSTITUTIONARRAY);
  }

  /**
   *
   *
   */
  public StaticTextField getStRegionArray()
  {
    return (StaticTextField) getChild(CHILD_STREGIONARRAY);
  }

  /**
   *
   *
   */
  public StaticTextField getStBranchArray()
  {
    return (StaticTextField) getChild(CHILD_STBRANCHARRAY);
  }

  /**
   *
   *
   */
  public StaticTextField getStGroupArray()
  {
    return (StaticTextField) getChild(CHILD_STGROUPARRAY);
  }

  /**
   *
   *
   */
  public StaticTextField getStPageAccessArray()
  {
    return (StaticTextField) getChild(CHILD_STPAGEACCESSARRAY);
  }

  /**
   *
   *
   */
  public StaticTextField getStOldSelectedHigherApp()
  {
    return (StaticTextField) getChild(CHILD_STOLDSELECTEDHIGHERAPP);
  }

  /**
   *
   *
   */
  public String endStOldSelectedHigherAppDisplay(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String finalHtml = handler.sendSelectedHighJointApprover("stOldSelectedHigherApp");
    handler.pageSaveState();

    return finalHtml;
  }

  /**
   *
   *
   */
  public StaticTextField getStOldSelectedJointApp()
  {
    return (StaticTextField) getChild(CHILD_STOLDSELECTEDJOINTAPP);
  }

  /**
   *
   *
   */
  public String endStOldSelectedJointAppDisplay(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String finalHtml = handler.sendSelectedHighJointApprover("stOldSelectedJointApp");
    handler.pageSaveState();

    return finalHtml;
  }

  /**
   *
   *
   */
  public CheckBox getChForceToChangePW()
  {
    return (CheckBox) getChild(CHILD_CHFORCETOCHANGEPW);
  }

  /**
   *
   *
   */
  public CheckBox getChLockPW()
  {
    return (CheckBox) getChild(CHILD_CHLOCKPW);
  }

  /**
   *
   *
   */
  public CheckBox getChNewPW()
  {
    return (CheckBox) getChild(CHILD_CHNEWPW);
  }

  /**
   *
   *
   */
  public StaticTextField getStLastLoginTime()
  {
    return (StaticTextField) getChild(CHILD_STLASTLOGINTIME);
  }

  /**
   *
   *
   */
  public StaticTextField getStLastLogoutTime()
  {
    return (StaticTextField) getChild(CHILD_STLASTLOGOUTTIME);
  }

  /**
   *
   *
   */
  public StaticTextField getStLoginFlag()
  {
    return (StaticTextField) getChild(CHILD_STLOGINFLAG);
  }

  /**
   *
   *
   */
  public doRowGenerator2Model getdoRowGenerator2Model()
  {
    if (doRowGenerator2 == null)
    {
      doRowGenerator2 = (doRowGenerator2Model) getModel(doRowGenerator2Model.class);
    }

    return doRowGenerator2;
  }

  /**
   *
   *
   */
  public void setdoRowGenerator2Model(doRowGenerator2Model model)
  {
    doRowGenerator2 = model;
  }

  //--> Hidden ActMessageButton (FINDTHISLATER)
  //--> Test by BILLY 15July2002
  public Button getBtActMsg()
  {
    return (Button) getChild(CHILD_BTACTMSG);
  }

  //===========================================
  //--> Addition methods to propulate Href display String
  //--> Test by BILLY 07Aug2002
  public String endHref1Display(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 0);
    handler.pageSaveState();

    return rc;
  }

  public String endHref2Display(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 1);
    handler.pageSaveState();

    return rc;
  }

  public String endHref3Display(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 2);
    handler.pageSaveState();

    return rc;
  }

  public String endHref4Display(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 3);
    handler.pageSaveState();

    return rc;
  }

  public String endHref5Display(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 4);
    handler.pageSaveState();

    return rc;
  }

  public String endHref6Display(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 5);
    handler.pageSaveState();

    return rc;
  }

  public String endHref7Display(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 6);
    handler.pageSaveState();

    return rc;
  }

  public String endHref8Display(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 7);
    handler.pageSaveState();

    return rc;
  }

  public String endHref9Display(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 8);
    handler.pageSaveState();

    return rc;
  }

  public String endHref10Display(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 9);
    handler.pageSaveState();

    return rc;
  }

  //=====================================================
  //// This method overrides the getDefaultURL() framework JATO method and it
  // is located
  //// in each ViewBean. This allows not to stay with the JATO ViewBean
  // extension,
  //// otherwise each ViewBean a.k.a page should extend its Handler (to be
  // called by the framework)
  //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
  //// The full method is still in PHC base class. It should care the
  //// the url="/" case (non-initialized defaultURL) by the BX framework
  // methods.
  public String getDisplayURL()
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String url = getDefaultDisplayURL();
    int languageId = handler.theSessionState.getLanguageId();

    //// Call the language specific URL (business delegation done in the
    // BXResource).
    if ((url != null) && !url.trim().equals("") && !url.trim().equals("/"))
    {
      url = BXResources.getBXUrl(url, languageId);
    }
    else
    {
      url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
    }

    return url;
  }

  //--Release2.1--//
  //--> Added Default Language ComboBox
  //--> By Billy 09Dec2002
  public ComboBox getCbDefaultLanguage()
  {
    return (ComboBox) getChild(CHILD_CBDEFAULTLANGUAGE);
  }

  //--> End Display events for checking if filed is Hidden
  //--> By Billy 07Mar2003
  //--> Method to check if the field is Hidden and display the Standard
  //--> Hidden Tag if hidden.
  private String commonEndDisplayHandle(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    boolean isHidden = handler.isFieldHidden(event.getChildName());
    handler.pageSaveState();

    if (isHidden == true)
    {
      return UserAdminConfigManager.HIDDENTAG;
    }
    else
    {
      return event.getContent();
    }
  }

  public String endCbProfileStatusDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  //--> This needs special handle
  //public String endCbUserTypesDisplay(ChildContentDisplayEvent event)
  //{
  //	return commonEndDisplayHandle(event);
  //}
  public String enTtbUserTitleDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endCbSalutationsDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endTbUserFirstNameDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endTbUserInitialNameDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endTbUserLastNameDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endTbLoginIdDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endTbBusinessIdDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endTbWorkPhoneNumberAreaCodeDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endTbWorkPhoneNumFirstThreeDigitsDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endTbWorkPhoneNumLastFourDigitsDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endTbWorkPhoneNumExtensionDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endTbFaxNumberAreaCodeDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endTbFaxNumFirstThreeDigitsDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endTbFaxNumLastFourDigitsDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endTbEmailAddressDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endCbLanguagesDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  //-- ========== SCR#537 begins ========== --//
  //-- by Neil on Dec/02/2004
  public String endCbTaskAlertDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  //-- ========== SCR#537 ends ========== --//
  public String endCbDefaultLanguageDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endCbInstitutionNamesDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endCbRegionNamesDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endCbBranchNamesDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endCbGroupProfileNameDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endCbHigherApproversDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endCbJointApproversDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endCbManagersDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endCbPartnerProfileNamesDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endCbStandInDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endTbPasswordDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endTbVerifyPasswordDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endChForceToChangePWDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endChLockPWDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endChNewPWDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endStLoginFlagDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endStLastLoginTimeDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endStLastLogoutTimeDisplay(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endTbLineOfBusiness1Display(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endTbLineOfBusiness2Display(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  public String endTbLineOfBusiness3Display(ChildContentDisplayEvent event)
  {
    return commonEndDisplayHandle(event);
  }

  // Special handle for Buttons
  public String endBtAddNewUserDisplay(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    boolean isHidden = handler.isFieldHidden(event.getChildName());
    handler.pageSaveState();

    if (isHidden == true)
    {
      return "";
    }
    else
    {
      return event.getContent();
    }
  }

  //-- ========== SCR#239 begins ========== --//
  //-- by Neil on Jan/04/2005
  public String endBtSubmitDisplay(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);
    handler.pageSaveState();

    String userId = getCbUserNames().getValue().toString();

    // SEAN Ticket #1255 (May 24, 2005): show or hide the submit button.
    // According to the legacy code, pageCondition3 is used to indicate the new
    // user status.  We we just use it here.
    PageEntry pg = handler.getTheSessionState().getCurrentPage();
    boolean isNewuser = pg.getPageCondition3();
    // SEAN Ticket #1255 END

    // for useradmin browsing, if no user selected, hide "Submit" button
    // otherwise display it.
    // Or for useradmin creation, always display "Submit" button
    if (userId.equals("") && (isNewuser == false))
    {
      return "";
    }
    else
    {
      return event.getContent();
    }
  }

  //-- ========== SCR#239   ends ========== --//
  public String endBtScreenAccessDisplay(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    boolean isHidden = handler.isFieldHidden(event.getChildName());
    handler.pageSaveState();

    if (isHidden == true)
    {
      return "";
    }
    else
    {
      return event.getContent();
    }
  }

  //==========================================================================
  //=================================================
  //--> CRF#37 :: Source/Firm to Group routing for TD
  //=================================================
  //--> Change to support multiple groups assignnment
  //--> By Billy 11Aug2003
  public ListBox getLbGroups()
  {
    return (ListBox) getChild(CHILD_LBGROUPS);
  }

  public TextField getTbNewGroupName()
  {
    return (TextField) getChild(CHILD_TBNEWGROUPNAME);
  }

  public Button getBtCreateNewGroup()
  {
    return (Button) getChild(CHILD_BTCREATENEWGROUP);
  }

  public void handleBtCreateNewGroupRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleCreateGroup();
    handler.postHandlerProtocol();
  }

  public StaticTextField getStBeginHidingNewGroupButton()
  {
    return (StaticTextField) getChild(CHILD_STBEGINHIDINGNEWGROUPBUTTON);
  }

  public String endStBeginHidingNewGroupButtonDisplay(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String finalHtml = handler.generateBeginingOfNewGroupComment();
    handler.pageSaveState();

    return finalHtml;
  }

  public StaticTextField getStEndHidingNewGroupButton()
  {
    return (StaticTextField) getChild(CHILD_STENDHIDINGNEWGROUPBUTTON);
  }

  public String endStEndHidingNewGroupButtonDisplay(ChildContentDisplayEvent event)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String finalHtml = handler.generateEndingOfNewGroupComment();
    handler.pageSaveState();

    return finalHtml;
  }

  //==================================================
  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////
  // Custom Methods - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////
  public void handleActMessageOK(String[] args)
  {
    userAdminHandler handler = (userAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleActMessageOK(args);
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */

  //--DJ_Ticket661--start--//
  //Modified to sort by PageLabel based on the selected language
  static class CbPageNamesOptionList
    extends GotoOptionList
  {
    /**
     *
     *
     */
    CbPageNamesOptionList()
    {
    }

    public void populate(RequestContext rc)
    {
      Connection c = null;

      try
      {
        clear();

        SelectQueryModel m = null;
        SelectQueryExecutionContext eContext =
          new SelectQueryExecutionContext((Connection) null,
            DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
            DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null)
        {
          m = new doPageNameLabelModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else
        {
          m = (SelectQueryModel) rc.getModelManager().getModel(doPageNameLabelModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        // Sort the results from DataObject
        TreeMap sorted = new TreeMap();

        while (m.next())
        {
          Object dfPageLabel = m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
          String label = ((dfPageLabel == null) ? "" : dfPageLabel.toString());
          Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
          String value = ((dfPageId == null) ? "" : dfPageId.toString());
          String[] theVal = new String[2];
          theVal[0] = label;
          theVal[1] = value;
          sorted.put(label, theVal);
        }

        // Set the sorted list to the optionlist
        Iterator theList = sorted.values().iterator();
        String[] theVal = new String[2];

        while (theList.hasNext())
        {
          theVal = (String[]) (theList.next());
          add(theVal[0], theVal[1]);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
      finally
      {
        try
        {
          if (c != null)
          {
            c.close();
          }
        }
        catch (SQLException ex)
        {
          // ignore
        }
      }
    }
  }

  /**
   *
   *
   */
  static class CbUserNamesOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbUserNamesOptionList()
    {
    }

    /**
     *
     *
     */
    public void populate(RequestContext rc)
    {
      Connection c = null;

      try
      {
        clear();

        SelectQueryModel m = null;
        SelectQueryExecutionContext eContext =
          new SelectQueryExecutionContext((Connection) null,
            DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
            DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null)
        {
          m = new doUserAdminProfileInfoModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else
        {
          m = (SelectQueryModel) rc.getModelManager().getModel(doUserAdminProfileInfoModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        while (m.next())
        {
          Object dfUserLogin = m.getValue(doUserAdminProfileInfoModel.FIELD_DFUSERLOGIN);
          Object dfLastName = m.getValue(doUserAdminProfileInfoModel.FIELD_DFLASTNAME);
          Object dfFirstName = m.getValue(doUserAdminProfileInfoModel.FIELD_DFFIRSTNAME);
          Object dfContactId = m.getValue(doUserAdminProfileInfoModel.FIELD_DFCONTACTID);
          String label =
            ((dfFirstName == null) ? "" : dfFirstName.toString()) + " "
            + ((dfLastName == null) ? "" : dfLastName.toString()) + "  "
            + ((dfUserLogin == null) ? "" : dfUserLogin.toString());
          String value = ((dfContactId == null) ? "" : dfContactId.toString());
          add(label, value);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
      finally
      {
        try
        {
          if (c != null)
          {
            c.close();
          }
        }
        catch (SQLException ex)
        {
          // ignore
        }
      }
    }
  }

  /**
   *
   *
   */
  static class CbProfileStatusOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbProfileStatusOptionList()
    {
    }

    /**
     *
     *
     */

    //--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 27Nov2002
    public void populate(RequestContext rc)
    {
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
          (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
            defaultInstanceStateName, true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(
                theSessionState.getDealInstitutionId(),"PROFILESTATUS", languageId);

        Iterator l = c.iterator();
        String[] theVal = new String[2];

        while (l.hasNext())
        {
          theVal = (String[]) (l.next());
          add(theVal[1], theVal[0]);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

  /**
   *
   *
   */
  static class CbPartnerProfileNamesOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbPartnerProfileNamesOptionList()
    {
    }

    /**
     *
     *
     */
    public void populate(RequestContext rc)
    {
      Connection c = null;

      try
      {
        clear();

        SelectQueryModel m = null;
        SelectQueryExecutionContext eContext =
          new SelectQueryExecutionContext((Connection) null,
            DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
            DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null)
        {
          m = new doUserAdminProfileInfoModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else
        {
          m = (SelectQueryModel) rc.getModelManager().getModel(doUserAdminProfileInfoModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        while (m.next())
        {
          Object dfUserLogin = m.getValue(doUserAdminProfileInfoModel.FIELD_DFUSERLOGIN);
          Object dfUserProfileId = m.getValue(doUserAdminProfileInfoModel.FIELD_DFUSERPROFILEID);
          Object dfLastName = m.getValue(doUserAdminProfileInfoModel.FIELD_DFLASTNAME);
          Object dfFirstName = m.getValue(doUserAdminProfileInfoModel.FIELD_DFFIRSTNAME);
          String label =
            ((dfFirstName == null) ? "" : dfFirstName.toString()) + " "
            + ((dfLastName == null) ? "" : dfLastName.toString()) + " "
            + ((dfUserLogin == null) ? "" : dfUserLogin.toString());
          String value = ((dfUserProfileId == null) ? "" : dfUserProfileId.toString());
          add(label, value);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
      finally
      {
        try
        {
          if (c != null)
          {
            c.close();
          }
        }
        catch (SQLException ex)
        {
          // ignore
        }
      }
    }
  }

  /**
   *
   *
   */
  static class CbStandInOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbStandInOptionList()
    {
    }

    /**
     *
     *
     */
    public void populate(RequestContext rc)
    {
      Connection c = null;

      try
      {
        clear();

        SelectQueryModel m = null;
        SelectQueryExecutionContext eContext =
          new SelectQueryExecutionContext((Connection) null,
            DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
            DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null)
        {
          m = new doUserAdminProfileInfoModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else
        {
          m = (SelectQueryModel) rc.getModelManager().getModel(doUserAdminProfileInfoModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        while (m.next())
        {
          Object dfUserLogin = m.getValue(doUserAdminProfileInfoModel.FIELD_DFUSERLOGIN);
          Object dfUserProfileId = m.getValue(doUserAdminProfileInfoModel.FIELD_DFUSERPROFILEID);
          Object dfLastName = m.getValue(doUserAdminProfileInfoModel.FIELD_DFLASTNAME);
          Object dfFirstName = m.getValue(doUserAdminProfileInfoModel.FIELD_DFFIRSTNAME);
          String label =
            ((dfFirstName == null) ? "" : dfFirstName.toString()) + " "
            + ((dfLastName == null) ? "" : dfLastName.toString()) + "  "
            + ((dfUserLogin == null) ? "" : dfUserLogin.toString());
          String value = ((dfUserProfileId == null) ? "" : dfUserProfileId.toString());
          add(label, value);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
      finally
      {
        try
        {
          if (c != null)
          {
            c.close();
          }
        }
        catch (SQLException ex)
        {
          // ignore
        }
      }
    }
  }

  /**
   *
   *
   */
  static class CbGroupProfileNameOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbGroupProfileNameOptionList()
    {
    }

    /**
     *
     *
     */
    public void populate(RequestContext rc)
    {
      Connection c = null;

      try
      {
        clear();

        SelectQueryModel m = null;
        SelectQueryExecutionContext eContext =
          new SelectQueryExecutionContext((Connection) null,
            DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
            DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null)
        {
          m = new doUserAdminGetGroupsModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else
        {
          m = (SelectQueryModel) rc.getModelManager().getModel(doUserAdminGetGroupsModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        while (m.next())
        {
          Object dfGroupProfileId = m.getValue(doUserAdminGetGroupsModel.FIELD_DFGROUPPROFILEID);
          Object dfGroupName = m.getValue(doUserAdminGetGroupsModel.FIELD_DFGROUPNAME);
          String label = ((dfGroupName == null) ? "" : dfGroupName.toString());
          String value = ((dfGroupProfileId == null) ? "" : dfGroupProfileId.toString());
          add(label, value);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
      finally
      {
        try
        {
          if (c != null)
          {
            c.close();
          }
        }
        catch (SQLException ex)
        {
          // ignore
        }
      }
    }
  }

  /**
   *
   *
   */
  static class CbHigherApproversOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbHigherApproversOptionList()
    {
    }

    /**
     *
     *
     */
    public void populate(RequestContext rc)
    {
      Connection c = null;

      try
      {
        clear();

        SelectQueryModel m = null;
        SelectQueryExecutionContext eContext =
          new SelectQueryExecutionContext((Connection) null,
            DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
            DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null)
        {
          m = new doUserAdminGetJointAppModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else
        {
          m = (SelectQueryModel) rc.getModelManager().getModel(doUserAdminGetJointAppModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        while (m.next())
        {
          Object dfUserLogin = m.getValue(doUserAdminGetJointAppModel.FIELD_DFUSERLOGIN);
          Object dfContactLastName =
            m.getValue(doUserAdminGetJointAppModel.FIELD_DFCONTACTLASTNAME);
          Object dfUserProfileId = m.getValue(doUserAdminGetJointAppModel.FIELD_DFUSERPROFILEID);
          Object dfContactFirstName =
            m.getValue(doUserAdminGetJointAppModel.FIELD_DFCONTACTFIRSTNAME);
          String label =
            ((dfContactFirstName == null) ? "" : dfContactFirstName.toString()) + " "
            + ((dfContactLastName == null) ? "" : dfContactLastName.toString()) + " "
            + ((dfUserLogin == null) ? "" : dfUserLogin.toString());
          String value = ((dfUserProfileId == null) ? "" : dfUserProfileId.toString());
          add(label, value);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
      finally
      {
        try
        {
          if (c != null)
          {
            c.close();
          }
        }
        catch (SQLException ex)
        {
          // ignore
        }
      }
    }
  }

  /**
   *
   *
   */
  static class CbJointApproversOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbJointApproversOptionList()
    {
    }

    /**
     *
     *
     */
    public void populate(RequestContext rc)
    {
      Connection c = null;

      try
      {
        clear();

        SelectQueryModel m = null;
        SelectQueryExecutionContext eContext =
          new SelectQueryExecutionContext((Connection) null,
            DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
            DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null)
        {
          m = new doUserAdminGetJointAppModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else
        {
          m = (SelectQueryModel) rc.getModelManager().getModel(doUserAdminGetJointAppModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        while (m.next())
        {
          Object dfUserLogin = m.getValue(doUserAdminGetJointAppModel.FIELD_DFUSERLOGIN);
          Object dfContactLastName =
            m.getValue(doUserAdminGetJointAppModel.FIELD_DFCONTACTLASTNAME);
          Object dfUserProfileId = m.getValue(doUserAdminGetJointAppModel.FIELD_DFUSERPROFILEID);
          Object dfContactFirstName =
            m.getValue(doUserAdminGetJointAppModel.FIELD_DFCONTACTFIRSTNAME);
          String label =
            ((dfContactFirstName == null) ? "" : dfContactFirstName.toString()) + " "
            + ((dfContactLastName == null) ? "" : dfContactLastName.toString()) + " "
            + ((dfUserLogin == null) ? "" : dfUserLogin.toString());
          String value = ((dfUserProfileId == null) ? "" : dfUserProfileId.toString());
          add(label, value);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
      finally
      {
        try
        {
          if (c != null)
          {
            c.close();
          }
        }
        catch (SQLException ex)
        {
          // ignore
        }
      }
    }
  }

  /**
   *
   *
   */
  static class CbSalutationsOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbSalutationsOptionList()
    {
    }

    /**
     *
     *
     */

    //--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 27Nov2002
    public void populate(RequestContext rc)
    {
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
          (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
            defaultInstanceStateName, true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(
                theSessionState.getDealInstitutionId(),"SALUTATION", languageId);

        Iterator l = c.iterator();
        String[] theVal = new String[2];

        while (l.hasNext())
        {
          theVal = (String[]) (l.next());
          add(theVal[1], theVal[0]);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

  /**
   *
   *
   */
  static class CbManagersOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbManagersOptionList()
    {
    }

    /**
     *
     *
     */
    public void populate(RequestContext rc)
    {
      Connection c = null;

      try
      {
        clear();

        SelectQueryModel m = null;
        SelectQueryExecutionContext eContext =
          new SelectQueryExecutionContext((Connection) null,
            DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
            DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null)
        {
          m = new doUserAdminProfileInfoModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else
        {
          m = (SelectQueryModel) rc.getModelManager().getModel(doUserAdminProfileInfoModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        while (m.next())
        {
          Object dfUserLogin = m.getValue(doUserAdminProfileInfoModel.FIELD_DFUSERLOGIN);
          Object dfUserProfileId = m.getValue(doUserAdminProfileInfoModel.FIELD_DFUSERPROFILEID);
          Object dfLastName = m.getValue(doUserAdminProfileInfoModel.FIELD_DFLASTNAME);
          Object dfFirstName = m.getValue(doUserAdminProfileInfoModel.FIELD_DFFIRSTNAME);
          String label =
            ((dfFirstName == null) ? "" : dfFirstName.toString()) + " "
            + ((dfLastName == null) ? "" : dfLastName.toString()) + "  "
            + ((dfUserLogin == null) ? "" : dfUserLogin.toString());
          String value = ((dfUserProfileId == null) ? "" : dfUserProfileId.toString());
          add(label, value);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
      finally
      {
        try
        {
          if (c != null)
          {
            c.close();
          }
        }
        catch (SQLException ex)
        {
          // ignore
        }
      }
    }
  }

  /**
   *
   *
   */
  static class CbBranchNamesOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbBranchNamesOptionList()
    {
    }

    /**
     *
     *
     */
    public void populate(RequestContext rc)
    {
      Connection c = null;

      try
      {
        clear();

        SelectQueryModel m = null;
        SelectQueryExecutionContext eContext =
          new SelectQueryExecutionContext((Connection) null,
            DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
            DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null)
        {
          m = new doUserAdminGetBranchesModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else
        {
          m = (SelectQueryModel) rc.getModelManager().getModel(doUserAdminGetBranchesModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        while (m.next())
        {
          Object dfBranchName = m.getValue(doUserAdminGetBranchesModel.FIELD_DFBRANCHNAME);
          Object dfBranchProfileId =
            m.getValue(doUserAdminGetBranchesModel.FIELD_DFBRANCHPROFILEID);
          String label = ((dfBranchName == null) ? "" : dfBranchName.toString());
          String value = ((dfBranchProfileId == null) ? "" : dfBranchProfileId.toString());
          add(label, value);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
      finally
      {
        try
        {
          if (c != null)
          {
            c.close();
          }
        }
        catch (SQLException ex)
        {
          // ignore
        }
      }
    }
  }

  /**
   *
   *
   */
  static class CbRegionNamesOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbRegionNamesOptionList()
    {
    }

    /**
     *
     *
     */
    public void populate(RequestContext rc)
    {
      Connection c = null;

      try
      {
        clear();

        SelectQueryModel m = null;
        SelectQueryExecutionContext eContext =
          new SelectQueryExecutionContext((Connection) null,
            DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
            DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null)
        {
          m = new doUserAdminGetRegionsModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else
        {
          m = (SelectQueryModel) rc.getModelManager().getModel(doUserAdminGetRegionsModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        while (m.next())
        {
          Object dfRegionName = m.getValue(doUserAdminGetRegionsModel.FIELD_DFREGIONNAME);
          Object dfRegionProfileId = m.getValue(doUserAdminGetRegionsModel.FIELD_DFREGIONPROFILEID);
          String label = ((dfRegionName == null) ? "" : dfRegionName.toString());
          String value = ((dfRegionProfileId == null) ? "" : dfRegionProfileId.toString());
          add(label, value);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
      finally
      {
        try
        {
          if (c != null)
          {
            c.close();
          }
        }
        catch (SQLException ex)
        {
          // ignore
        }
      }
    }
  }

  /**
   *
   *
   */
  static class CbInstitutionNamesOptionList
    extends OptionList
  {

    /**
     *
     *
     */
    CbInstitutionNamesOptionList()
    {
    }

    /**
     *
     *
     */

    //--> Modified to fix the Open Cursor problem :
    //--> Have to close the statement and ResultSet otherwise it will cause
    //--> "ORA-01000 maximum open cursors exceeded" Exception
    //--> Modified by BILLY 06June2003
    public void populate(RequestContext rc)
    {
        String defaultInstanceStateName = rc.getModelManager()
                    .getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
                    .getModelManager().getModel(SessionStateModel.class,
                            defaultInstanceStateName, true);

        String FETCH_DATA_STATEMENT = "SELECT INSTITUTIONPROFILE.INSTITUTIONNAME, INSTITUTIONPROFILE.INSTITUTIONPROFILEID "
                    + "FROM INSTITUTIONPROFILE WHERE INSTITUTIONPROFILE.INSTITUTIONPROFILEID = "
                    + theSessionState.getDealInstitutionId();
      
      Connection c = null;
      Statement query = null;

      try
      {
        clear();

        if (rc == null)
        {
          c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
        }
        else
        {
          c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
        }

        query = c.createStatement();

        ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);

        while (results.next())
        {
          Object INSTITUTIONPROFILEID = results.getObject("INSTITUTIONPROFILEID");
          Object INSTITUTIONPROFILE_INSTITUTIONNAME = results.getObject("INSTITUTIONNAME");
          String label =
            ((INSTITUTIONPROFILE_INSTITUTIONNAME == null) ? ""
            : INSTITUTIONPROFILE_INSTITUTIONNAME.toString());
          String value = ((INSTITUTIONPROFILEID == null) ? "" : INSTITUTIONPROFILEID.toString());
          add(label, value);
        }

        results.close();
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
      finally
      {
        try
        {
          if (c != null)
          {
            c.close();
          }

          if (query != null)
          {
            query.close();
          }
        }
        catch (SQLException ex)
        {
          // ignore
        }
      }
    }
  }

  /**
   *
   *
   */
  static class CbUserTypesOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbUserTypesOptionList()
    {
    }

    /**
     *
     *
     */

    //--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 27Nov2002
    public void populate(RequestContext rc)
    {
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
          (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
            defaultInstanceStateName, true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(
                theSessionState.getDealInstitutionId(),"USERTYPE", languageId);

        Iterator l = c.iterator();
        String[] theVal = new String[2];

        while (l.hasNext())
        {
          theVal = (String[]) (l.next());

          //FXP21168: Since there is no gurantee on getting values (names or ids) of these 3 types in MOSAPP and no access to property names of other instances, hard-coding them is the only way.
          if (!"99".equals(theVal[0]) && !"98".equals(theVal[0]) && !"97".equals(theVal[0]))
            add(theVal[1], theVal[0]);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

  static class CbDefaultLanguageOptionList
    extends OptionList
  {
    CbDefaultLanguageOptionList()
    {
    }

    public void populate(RequestContext rc)
    {
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
          (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
            defaultInstanceStateName, true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(
                theSessionState.getDealInstitutionId(),"LANGUAGEPREFERENCE", languageId);

        Iterator l = c.iterator();
        String[] theVal = new String[2];

        while (l.hasNext())
        {
          theVal = (String[]) (l.next());
          add(theVal[1], theVal[0]);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

}
