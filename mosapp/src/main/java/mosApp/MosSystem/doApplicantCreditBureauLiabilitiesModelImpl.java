package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doApplicantCreditBureauLiabilitiesModelImpl extends QueryModelBase
	implements doApplicantCreditBureauLiabilitiesModel
{
	/**
	 *
	 *
	 */
	public doApplicantCreditBureauLiabilitiesModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code

		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    logger = SysLog.getSysLogger("DOACBL");
    logger.debug("DOACBL@afterExecute::Size: " + this.getSize());
    logger.debug("DOACBL@afterExecute::SQLTemplate: " + this.getSelectSQL());

    //To override all the Description fields by populating from BXResource
    //--> By Billy 05Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert LIABILITYTYPE Descriptions
      this.setDfLiabilityTypeDescription(BXResources.getPickListDescription(
          theSessionState.getDealInstitutionId(),
          "LIABILITYTYPE", this.getDfLiabilityTypeDescription(), languageId));
    }

   //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIABILITYID);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIABILITYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIABILITYTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIABILITYTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLiabilityTypeDescription()
	{
		return (String)getValue(FIELD_DFLIABILITYTYPEDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityTypeDescription(String value)
	{
		setValue(FIELD_DFLIABILITYTYPEDESCRIPTION,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIABILITYAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIABILITYAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityMonthlyPayment()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIABILITYMONTHLYPAYMENT);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityMonthlyPayment(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIABILITYMONTHLYPAYMENT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLiabilityDescription()
	{
		return (String)getValue(FIELD_DFLIABILITYDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityDescription(String value)
	{
		setValue(FIELD_DFLIABILITYDESCRIPTION,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPercentOutGDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPERCENTOUTGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPercentOutGDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPERCENTOUTGDS,value);
	}


    /**
     * This method declaration returns the value of field the CreditBureauLiabilityLimit.
     * 
     * @return dfCreditBureauLiabilityLimit
     */
    public java.math.BigDecimal getDfCreditBureauLiabilityLimit() {
        return (java.math.BigDecimal) getValue(FIELD_DFCREDITBUREAULIABILITYLIMIT);
    }

    /**
     * This method declaration sets the CreditBureauLiabilityLimit.
     * 
     * @param value: the new CreditBureauLiabilityLimit
     */
    public void setDfCreditBureauLiabilityLimit(java.math.BigDecimal value) {
        setValue(FIELD_DFCREDITBUREAULIABILITYLIMIT, value);
    }

    /**
     * This method declaration returns the value of field the CreditBureauIndicator.
     * 
     * @return dfCreditBureauIndicator
     */
    public String getDfCreditBureauIndicator() {
        return (String) getValue(FIELD_DFCREDITBUREAUINDICATOR);
    }

    /**
     * This method declaration sets the CreditBureauIndicator.
     * 
     * @param value: the new CreditBureauIndicator
     */
    public void setDfCreditBureauIndicator(String value) {
        setValue(FIELD_DFCREDITBUREAUINDICATOR, value);
    }

    /**
     * This method declaration returns the value of field the MaturityDate.
     * 
     * @return dfMaturityDate
     */
    public java.sql.Timestamp getDfMaturityDate() {
        return (java.sql.Timestamp) getValue(FIELD_DFMATURITYDATE);
    }

    /**
     * This method declaration sets the MaturityDate.
     * 
     * @param value: the new dfMaturityDate
     */
    public void setDfMaturityDate(java.sql.Timestamp value) {
        setValue(FIELD_DFMATURITYDATE, value);
    }
    
	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
  public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 21Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL LIABILITY.LIABILITYID, LIABILITY.LIABILITYTYPEID, LIABILITYTYPE.LDESCRIPTION, LIABILITY.COPYID, LIABILITY.BORROWERID, LIABILITY.LIABILITYAMOUNT, LIABILITY.LIABILITYMONTHLYPAYMENT, LIABILITY.LIABILITYDESCRIPTION, LIABILITY.PERCENTOUTGDS FROM LIABILITY, LIABILITYTYPE  __WHERE__  ORDER BY LIABILITY.LIABILITYTYPEID  ASC, LIABILITY.LIABILITYDESCRIPTION  ASC, LIABILITY.LIABILITYAMOUNT  ASC";
	
	//FXP23719 - MCM Nov 28, 2008 - start
	/*
	public static final String SELECT_SQL_TEMPLATE=
	    "SELECT DISTINCT LIABILITY.LIABILITYID, LIABILITY.LIABILITYTYPEID, "+
	    "to_char(LIABILITY.LIABILITYTYPEID) LIABILITYTYPEID_STR, LIABILITY.COPYID, "+
	    "LIABILITY.BORROWERID, LIABILITY.LIABILITYAMOUNT, LIABILITY.LIABILITYMONTHLYPAYMENT, "+
	    "LIABILITY.LIABILITYDESCRIPTION, LIABILITY.PERCENTOUTGDS "+
	    "FROM LIABILITY, BORROWER  "+
	    "__WHERE__  "+
	    "ORDER BY LIABILITY.LIABILITYTYPEID  ASC, LIABILITY.LIABILITYDESCRIPTION  ASC, "+
	    "LIABILITY.LIABILITYAMOUNT  ASC";
	 */
	public static final String SELECT_SQL_TEMPLATE=
	    "SELECT LIABILITY.LIABILITYID, LIABILITY.LIABILITYTYPEID, "+
	    "to_char(LIABILITY.LIABILITYTYPEID) LIABILITYTYPEID_STR, LIABILITY.COPYID, "+
	    "LIABILITY.BORROWERID, LIABILITY.LIABILITYAMOUNT, LIABILITY.LIABILITYMONTHLYPAYMENT, "+
	    "LIABILITY.LIABILITYDESCRIPTION, LIABILITY.PERCENTOUTGDS, LIABILITY.CREDITLIMIT, LIABILITY.CREDITBUREAURECORDINDICATOR, LIABILITY.MATURITYDATE "+
	    "FROM LIABILITY "+
	    "__WHERE__  "+
	    "ORDER BY LIABILITY.LIABILITYTYPEID  ASC, LIABILITY.LIABILITYDESCRIPTION  ASC, "+
	    "LIABILITY.LIABILITYAMOUNT  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME="LIABILITY";
	public static final String STATIC_WHERE_CRITERIA="";
	//FXP23719 - MCM Nov 28, 2008 - end
	
  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFLIABILITYID="LIABILITY.LIABILITYID";
	public static final String COLUMN_DFLIABILITYID="LIABILITYID";
	public static final String QUALIFIED_COLUMN_DFLIABILITYTYPEID="LIABILITY.LIABILITYTYPEID";
	public static final String COLUMN_DFLIABILITYTYPEID="LIABILITYTYPEID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="LIABILITY.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFBORROWERID="LIABILITY.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFLIABILITYAMOUNT="LIABILITY.LIABILITYAMOUNT";
	public static final String COLUMN_DFLIABILITYAMOUNT="LIABILITYAMOUNT";
	public static final String QUALIFIED_COLUMN_DFLIABILITYMONTHLYPAYMENT="LIABILITY.LIABILITYMONTHLYPAYMENT";
	public static final String COLUMN_DFLIABILITYMONTHLYPAYMENT="LIABILITYMONTHLYPAYMENT";
	public static final String QUALIFIED_COLUMN_DFLIABILITYDESCRIPTION="LIABILITY.LIABILITYDESCRIPTION";
	public static final String COLUMN_DFLIABILITYDESCRIPTION="LIABILITYDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFPERCENTOUTGDS="LIABILITY.PERCENTOUTGDS";
	public static final String COLUMN_DFPERCENTOUTGDS="PERCENTOUTGDS";
    public static final String QUALIFIED_COLUMN_DFCREDITBUREAULIABILITYLIMIT="LIABILITY.CREDITLIMIT";
    public static final String COLUMN_DFCREDITBUREAULIABILITYLIMIT="CREDITLIMIT";
    public static final String QUALIFIED_COLUMN_DFCREDITBUREAUINDICATOR="LIABILITY.CREDITBUREAURECORDINDICATOR";
    public static final String COLUMN_DFCREDITBUREAUINDICATOR="CREDITBUREAURECORDINDICATOR";
    public static final String QUALIFIED_COLUMN_DFMATURITYDATE="LIABILITY.MATURITYDATE";
    public static final String COLUMN_DFMATURITYDATE="MATURITYDATE";

  //--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFLIABILITYTYPEDESCRIPTION="LIABILITYTYPE.LDESCRIPTION";
	//public static final String COLUMN_DFLIABILITYTYPEDESCRIPTION="LDESCRIPTION";
    public static final String QUALIFIED_COLUMN_DFLIABILITYTYPEDESCRIPTION="LIABILITY.LIABILITYTYPEID_STR";
	public static final String COLUMN_DFLIABILITYTYPEDESCRIPTION="LIABILITYTYPEID_STR";
    
    
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYID,
				COLUMN_DFLIABILITYID,
				QUALIFIED_COLUMN_DFLIABILITYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYTYPEID,
				COLUMN_DFLIABILITYTYPEID,
				QUALIFIED_COLUMN_DFLIABILITYTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYTYPEDESCRIPTION,
				COLUMN_DFLIABILITYTYPEDESCRIPTION,
				QUALIFIED_COLUMN_DFLIABILITYTYPEDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYAMOUNT,
				COLUMN_DFLIABILITYAMOUNT,
				QUALIFIED_COLUMN_DFLIABILITYAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYMONTHLYPAYMENT,
				COLUMN_DFLIABILITYMONTHLYPAYMENT,
				QUALIFIED_COLUMN_DFLIABILITYMONTHLYPAYMENT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYDESCRIPTION,
				COLUMN_DFLIABILITYDESCRIPTION,
				QUALIFIED_COLUMN_DFLIABILITYDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPERCENTOUTGDS,
				COLUMN_DFPERCENTOUTGDS,
				QUALIFIED_COLUMN_DFPERCENTOUTGDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
        
        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFCREDITBUREAULIABILITYLIMIT,
                COLUMN_DFCREDITBUREAULIABILITYLIMIT,
                QUALIFIED_COLUMN_DFCREDITBUREAULIABILITYLIMIT,
                java.math.BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                FIELD_DFCREDITBUREAUINDICATOR,
                COLUMN_DFCREDITBUREAUINDICATOR,
                QUALIFIED_COLUMN_DFCREDITBUREAUINDICATOR,
                String.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));
        
        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                FIELD_DFMATURITYDATE,
                COLUMN_DFMATURITYDATE,
                QUALIFIED_COLUMN_DFMATURITYDATE,
                String.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));
	}


    public void setDfCreditbureauindicatoro(String value) {
        // TODO Auto-generated method stub
        
    }

}

