package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

public interface doUWLOCRepaymentTypeModel
    extends QueryModel, SelectQueryModel
{
  public java.math.BigDecimal getDfDealId();

  public void setDfDealId(java.math.BigDecimal value);

  public java.math.BigDecimal getDfCopyId();

  public void setDfCopyId(java.math.BigDecimal value);

  public java.math.BigDecimal getDfLOCRepaymentTypeId();

  public void setDfLOCRepaymentTypeId(java.math.BigDecimal value);

  public String getDfLOCRepaymentTypeDescription();

  public void setDfLOCRepaymentTypeDescription(String value);

  // //////////////////////////////////////////////////////////////////////////
  // Class variables
  // //////////////////////////////////////////////////////////////////////////

  public static final String FIELD_DFDEALID = "dfDealId";

  public static final String FIELD_DFCOPYID = "dfCopyId";

  public static final String FIELD_DFLOCREPAYMENTTYPEID = "dfLOCRepaymentTypeId";

  public static final String FIELD_DFLOCREPAYMENTTYPEDESCRIPTION = "dfLOCRepaymentTypeDescription";

  // //////////////////////////////////////////////////////////////////////////
  // Instance variables
  // //////////////////////////////////////////////////////////////////////////

}
