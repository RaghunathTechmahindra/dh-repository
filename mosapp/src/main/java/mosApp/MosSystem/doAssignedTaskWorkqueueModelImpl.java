package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doAssignedTaskWorkqueueModelImpl extends QueryModelBase
	implements doAssignedTaskWorkqueueModel
{
	/**
	 *
	 *
	 */
	public doAssignedTaskWorkqueueModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By John 25Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();

    //  Task Description
    // this class is used for Sentinel page that is Deal page so that dealInstituionProfileId 
    // could be retreived session
    this.setDfTaskDesc(BXResources.getPickListDescription(
            theSessionState.getUserInstitutionId(),                                                 
      "TASK741", this.getDfTaskDesc(), languageId));

    //  Status
    this.setDfTaskStatusDesc(BXResources.getPickListDescription(
            theSessionState.getUserInstitutionId(),                                                 
      "TASKSTATUS", this.getDfTaskStatusDesc(), languageId));

    //  Priority
    this.setDfPriorityDesc(BXResources.getPickListDescription(
            theSessionState.getUserInstitutionId(),                                                 
      "PRIORITY", this.getDfPriorityDesc(), languageId));

    //  Warning
    this.setDfMilestone(BXResources.getPickListDescription(
            theSessionState.getUserInstitutionId(),
      "TIMEMILESTONE", this.getDfMilestone(), languageId));
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public String getDfTaskDesc()
	{
		return (String)getValue(FIELD_DFTASKDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfTaskDesc(String value)
	{
		setValue(FIELD_DFTASKDESC,value);
	}


	/**
	 *
	 *
	 */
	public String getDfTaskStatusDesc()
	{
		return (String)getValue(FIELD_DFTASKSTATUSDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfTaskStatusDesc(String value)
	{
		setValue(FIELD_DFTASKSTATUSDESC,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPriorityDesc()
	{
		return (String)getValue(FIELD_DFPRIORITYDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfPriorityDesc(String value)
	{
		setValue(FIELD_DFPRIORITYDESC,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfDueDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFDUEDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfDueDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFDUEDATE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMilestone()
	{
		return (String)getValue(FIELD_DFMILESTONE);
	}


	/**
	 *
	 *
	 */
	public void setDfMilestone(String value)
	{
		setValue(FIELD_DFMILESTONE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAssignedUser()
	{
		return (String)getValue(FIELD_DFASSIGNEDUSER);
	}


	/**
	 *
	 *
	 */
	public void setDfAssignedUser(String value)
	{
		setValue(FIELD_DFASSIGNEDUSER,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAssignedTaskWorkqueueId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFASSIGNEDTASKWORKQUEUEID);
	}


	/**
	 *
	 *
	 */
	public void setDfAssignedTaskWorkqueueId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFASSIGNEDTASKWORKQUEUEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfUnderwriterId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFUNDERWRITERID);
	}


	/**
	 *
	 *
	 */
	public void setDfUnderwriterId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFUNDERWRITERID,value);
	}


	/**
	 * MetaData object test method to verify the SYNTHETIC new
	 * field for the reogranized SQL_TEMPLATE query.
	 *
	 */
  /**
	public void setResultSet(ResultSet value)
	{
  		logger = SysLog.getSysLogger("METADATA");

  		try
  		{
        super.setResultSet(value);
   			if( value != null)
   			{
       			ResultSetMetaData metaData = value.getMetaData();
       			int columnCount = metaData.getColumnCount();
       			logger.debug("===============================================");
            logger.debug("Testing MetaData Object for new synthetic fields for" +
                          " doAssignedTaskWorkqueueModel");
       			logger.debug("NumberColumns: " + columnCount);
         		for(int i = 0; i < columnCount ; i++)
          		{
                 	logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
          		}
          		logger.debug("================================================");
      		}
  		}
  		catch (SQLException e)
  		{
    		    logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
  		}
 	}
  **/


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 05Nov2002

	public static final String DATA_SOURCE_NAME="jdbc/orcl";

  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
/* old
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL TASK.TASKNAME, TASKSTATUS.TSDESCRIPTION, PRIORITY.PDESCRIPTION, " +
    "ASSIGNEDTASKSWORKQUEUE.DUETIMESTAMP, TIMEMILESTONE.TMDESCRIPTION, " +
    "CONTACT.CONTACTLASTNAME || ' ' || SUBSTR(CONTACT.CONTACTFIRSTNAME,0,1 ) SYNTHETICASSIGNEDUSER, " +
    "ASSIGNEDTASKSWORKQUEUE.ASSIGNEDTASKSWORKQUEUEID, DEAL.UNDERWRITERUSERID " +
    "FROM ASSIGNEDTASKSWORKQUEUE, TASKSTATUS, PRIORITY, TIMEMILESTONE, " +
    "USERPROFILE, CONTACT, TASK, DEAL  __WHERE__  " +
    "ORDER BY PRIORITY.PRIORITYID  ASC, ASSIGNEDTASKSWORKQUEUE.DUETIMESTAMP  ASC";
*/

	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL to_char(TASK.TASKID) TASKID_STR, " +
    "to_char(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID) TASKSTATUSID_STR, " +
    "to_char(ASSIGNEDTASKSWORKQUEUE.PRIORITYID) PRIORITYID_STR, " +
    "ASSIGNEDTASKSWORKQUEUE.DUETIMESTAMP, " +
    "to_char(ASSIGNEDTASKSWORKQUEUE.TIMEMILESTONEID) TIMEMILESTONEID_STR, " +
    "CONTACT.CONTACTLASTNAME || ' ' || SUBSTR(CONTACT.CONTACTFIRSTNAME,0,1 ) SYNTHETICASSIGNEDUSER, " +
    "ASSIGNEDTASKSWORKQUEUE.ASSIGNEDTASKSWORKQUEUEID, DEAL.UNDERWRITERUSERID " +
    "FROM ASSIGNEDTASKSWORKQUEUE, USERPROFILE, CONTACT, TASK, DEAL  __WHERE__  " +
    "ORDER BY ASSIGNEDTASKSWORKQUEUE.PRIORITYID  ASC, ASSIGNEDTASKSWORKQUEUE.DUETIMESTAMP  ASC";

  //--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //  old
  //  public static final String MODIFYING_QUERY_TABLE_NAME=
  //    "ASSIGNEDTASKSWORKQUEUE, TASKSTATUS, PRIORITY, TIMEMILESTONE, " +
  //    "USERPROFILE, CONTACT, TASK, DEAL";

  public static final String MODIFYING_QUERY_TABLE_NAME=
    "ASSIGNEDTASKSWORKQUEUE, USERPROFILE, CONTACT, TASK, DEAL";

  //--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
/* old
  public static final String STATIC_WHERE_CRITERIA=
  " ((USERPROFILE.CONTACTID = CONTACT.CONTACTID AND " +
  "ASSIGNEDTASKSWORKQUEUE.USERPROFILEID = USERPROFILE.USERPROFILEID AND " +
  "ASSIGNEDTASKSWORKQUEUE.TIMEMILESTONEID = TIMEMILESTONE.TIMEMILESTONEID AND " +
  "ASSIGNEDTASKSWORKQUEUE.PRIORITYID = PRIORITY.PRIORITYID AND " +
  "ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID = TASKSTATUS.TASKSTATUSID AND " +
  "DEAL.DEALID = ASSIGNEDTASKSWORKQUEUE.DEALID AND " +
  "((ASSIGNEDTASKSWORKQUEUE.TASKID - 50000) = TASK.TASKID)) AND " +
  "CONTACT.COPYID = 1 AND DEAL.COPYTYPE = 'G') ";
*/

  public static final String STATIC_WHERE_CRITERIA=
  " ((USERPROFILE.CONTACTID = CONTACT.CONTACTID AND " +
  "ASSIGNEDTASKSWORKQUEUE.USERPROFILEID = USERPROFILE.USERPROFILEID AND " +
  "DEAL.DEALID = ASSIGNEDTASKSWORKQUEUE.DEALID AND " +
  "DEAL.INSTITUTIONPROFILEID = CONTACT.INSTITUTIONPROFILEID AND " +
  "DEAL.INSTITUTIONPROFILEID = USERPROFILE.INSTITUTIONPROFILEID AND " +
  "DEAL.INSTITUTIONPROFILEID = ASSIGNEDTASKSWORKQUEUE.INSTITUTIONPROFILEID AND " +
  "DEAL.INSTITUTIONPROFILEID = TASK.INSTITUTIONPROFILEID AND " +
  "((ASSIGNEDTASKSWORKQUEUE.TASKID - 50000) = TASK.TASKID)) AND " +
  "CONTACT.COPYID = 1 AND DEAL.COPYTYPE = 'G') ";

	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();

  //--Release2.1--//
	//public static final String QUALIFIED_COLUMN_DFTASKDESC="TASK.TASKNAME";
	//public static final String COLUMN_DFTASKDESC="TASKNAME";
	//public static final String QUALIFIED_COLUMN_DFTASKSTATUSDESC="TASKSTATUS.TSDESCRIPTION";
	//public static final String COLUMN_DFTASKSTATUSDESC="TSDESCRIPTION";
  //public static final String QUALIFIED_COLUMN_DFPRIORITYDESC="PRIORITY.PDESCRIPTION";
	//public static final String COLUMN_DFPRIORITYDESC="PDESCRIPTION";
	//public static final String QUALIFIED_COLUMN_DFMILESTONE="TIMEMILESTONE.TMDESCRIPTION";
	//public static final String COLUMN_DFMILESTONE="TMDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFTASKDESC="TASK.TASKID_STR";
	public static final String COLUMN_DFTASKDESC="TASKID_STR";
  public static final String QUALIFIED_COLUMN_DFTASKSTATUSDESC="TASKSTATUS.TASKSTATUSID_STR";
	public static final String COLUMN_DFTASKSTATUSDESC="TASKSTATUSID_STR";
  public static final String QUALIFIED_COLUMN_DFPRIORITYDESC="PRIORITY.PRIORITYID_STR";
	public static final String COLUMN_DFPRIORITYDESC="PRIORITYID_STR";
	public static final String QUALIFIED_COLUMN_DFMILESTONE="TIMEMILESTONE.TIMEMILESTONEID_STR";
	public static final String COLUMN_DFMILESTONE="TIMEMILESTONEID_STR";
  //--------------//

	public static final String QUALIFIED_COLUMN_DFDUEDATE="ASSIGNEDTASKSWORKQUEUE.DUETIMESTAMP";
	public static final String COLUMN_DFDUEDATE="DUETIMESTAMP";
	////public static final String QUALIFIED_COLUMN_DFASSIGNEDUSER=".";
	////public static final String COLUMN_DFASSIGNEDUSER="";
	public static final String QUALIFIED_COLUMN_DFASSIGNEDUSER="CONTACT.SYNTHETICASSIGNEDUSER";
	public static final String COLUMN_DFASSIGNEDUSER="SYNTHETICASSIGNEDUSER";
	public static final String QUALIFIED_COLUMN_DFASSIGNEDTASKWORKQUEUEID="ASSIGNEDTASKSWORKQUEUE.ASSIGNEDTASKSWORKQUEUEID";
	public static final String COLUMN_DFASSIGNEDTASKWORKQUEUEID="ASSIGNEDTASKSWORKQUEUEID";
	public static final String QUALIFIED_COLUMN_DFUNDERWRITERID="DEAL.UNDERWRITERUSERID";
	public static final String COLUMN_DFUNDERWRITERID="UNDERWRITERUSERID";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTASKDESC,
				COLUMN_DFTASKDESC,
				QUALIFIED_COLUMN_DFTASKDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTASKSTATUSDESC,
				COLUMN_DFTASKSTATUSDESC,
				QUALIFIED_COLUMN_DFTASKSTATUSDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIORITYDESC,
				COLUMN_DFPRIORITYDESC,
				QUALIFIED_COLUMN_DFPRIORITYDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDUEDATE,
				COLUMN_DFDUEDATE,
				QUALIFIED_COLUMN_DFDUEDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMILESTONE,
				COLUMN_DFMILESTONE,
				QUALIFIED_COLUMN_DFMILESTONE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		//// Improper converted (by iMT tool) FieldDescriptor for the ComputedColumn.
    ////FIELD_SCHEMA.addFieldDescriptor(
		////	new QueryFieldDescriptor(
		////		FIELD_DFASSIGNEDUSER,
		////		COLUMN_DFASSIGNEDUSER,
		////		QUALIFIED_COLUMN_DFASSIGNEDUSER,
		////		String.class,
		////		false,
		////		false,
		////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		////		"",
		////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		////		""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFASSIGNEDUSER,
				COLUMN_DFASSIGNEDUSER,
				QUALIFIED_COLUMN_DFASSIGNEDUSER,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"CONTACT.CONTACTLASTNAME || ' ' || SUBSTR(CONTACT.CONTACTFIRSTNAME,0,1 )",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"CONTACT.CONTACTLASTNAME || ' ' || SUBSTR(CONTACT.CONTACTFIRSTNAME,0,1 )"));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFASSIGNEDTASKWORKQUEUEID,
				COLUMN_DFASSIGNEDTASKWORKQUEUEID,
				QUALIFIED_COLUMN_DFASSIGNEDTASKWORKQUEUEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUNDERWRITERID,
				COLUMN_DFUNDERWRITERID,
				QUALIFIED_COLUMN_DFUNDERWRITERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
	}

}

