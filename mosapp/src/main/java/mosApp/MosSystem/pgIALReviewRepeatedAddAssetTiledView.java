package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgIALReviewRepeatedAddAssetTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgIALReviewRepeatedAddAssetTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doBorrowerEntryAssetModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getTbAssetDescription().setValue(CHILD_TBASSETDESCRIPTION_RESET_VALUE);
		getTbAssetValue().setValue(CHILD_TBASSETVALUE_RESET_VALUE);
		getCbAssetIncludeInGDS().setValue(CHILD_CBASSETINCLUDEINGDS_RESET_VALUE);
		getBtDeleteAsset().setValue(CHILD_BTDELETEASSET_RESET_VALUE);
		getStIndex().setValue(CHILD_STINDEX_RESET_VALUE);
		getHdAssetId().setValue(CHILD_HDASSETID_RESET_VALUE);
		getHdAssetBorrowerId().setValue(CHILD_HDASSETBORROWERID_RESET_VALUE);
		getCbAssetIncludeInTDS().setValue(CHILD_CBASSETINCLUDEINTDS_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_TBASSETDESCRIPTION,TextField.class);
		registerChild(CHILD_TBASSETVALUE,TextField.class);
		registerChild(CHILD_CBASSETINCLUDEINGDS,ComboBox.class);
		registerChild(CHILD_BTDELETEASSET,Button.class);
		registerChild(CHILD_STINDEX,StaticTextField.class);
		registerChild(CHILD_HDASSETID,HiddenField.class);
		registerChild(CHILD_HDASSETBORROWERID,HiddenField.class);
		registerChild(CHILD_CBASSETINCLUDEINTDS,ComboBox.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoBorrowerEntryAssetModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}

		return movedToRow;


		// The following code block was migrated from the RepeatedAddAsset_onBeforeRowDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		//CSpHtml.sendMessage("inc @RepeatedAddAsset "+sumofrowsForAsset);
		sumofrowsForAsset ++;

		int rowno = event.getRowIndex();

		doBorrowerEntryAsset doborasset =(doBorrowerEntryAsset) getModel(doBorrowerEntryAssetModel.class);

		CSpDBResultTable resultbl = doborasset.getLastResults().getResultTable();

		assetGdsvalue = resultbl.getValue(rowno, 4).toString();

		String assetid = resultbl.getValue(rowno, 1).toString();


		//CSpHtml.sendMessage("For "+assetid + "  Gds Value " +assetGdsvalue);
		if ((assetGdsvalue == null) ||(assetGdsvalue.trim().length() < 1)) assetGdsvalue = "N";


		//get the row index
		/$int rowno = event.getRowIndex();

				Object index = new Integer(rowno + 1);
				CSpRepeated rep = (CSpRepeated)getCommonRepeated("RepeatedAddAsset");
				rep.setDisplayFieldValue("stIndex", index);$/
		return;
		*/
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_TBASSETDESCRIPTION))
		{
			TextField child = new TextField(this,
				getdoBorrowerEntryAssetModel(),
				CHILD_TBASSETDESCRIPTION,
				doBorrowerEntryAssetModel.FIELD_DFASSETDESCRIPTION,
				CHILD_TBASSETDESCRIPTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBASSETVALUE))
		{
			TextField child = new TextField(this,
				getdoBorrowerEntryAssetModel(),
				CHILD_TBASSETVALUE,
				doBorrowerEntryAssetModel.FIELD_DFASSETVALUE,
				CHILD_TBASSETVALUE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBASSETINCLUDEINGDS))
		{
			ComboBox child = new ComboBox( this,
				getdoBorrowerEntryAssetModel(),
				CHILD_CBASSETINCLUDEINGDS,
				doBorrowerEntryAssetModel.FIELD_DFASSETINCLUDINGGDS,
				CHILD_CBASSETINCLUDEINGDS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbAssetIncludeInGDSOptions);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETEASSET))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETEASSET,
				CHILD_BTDELETEASSET,
				CHILD_BTDELETEASSET_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STINDEX))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STINDEX,
				CHILD_STINDEX,
				CHILD_STINDEX_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDASSETID))
		{
			HiddenField child = new HiddenField(this,
				getdoBorrowerEntryAssetModel(),
				CHILD_HDASSETID,
				doBorrowerEntryAssetModel.FIELD_DFASSETID,
				CHILD_HDASSETID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDASSETBORROWERID))
		{
			HiddenField child = new HiddenField(this,
				getdoBorrowerEntryAssetModel(),
				CHILD_HDASSETBORROWERID,
				doBorrowerEntryAssetModel.FIELD_DFBORROWERID,
				CHILD_HDASSETBORROWERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBASSETINCLUDEINTDS))
		{
			ComboBox child = new ComboBox( this,
				getdoBorrowerEntryAssetModel(),
				CHILD_CBASSETINCLUDEINTDS,
				doBorrowerEntryAssetModel.FIELD_DFASSETINCLUDINGTDS,
				CHILD_CBASSETINCLUDEINTDS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbAssetIncludeInTDSOptions);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public TextField getTbAssetDescription()
	{
		return (TextField)getChild(CHILD_TBASSETDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public TextField getTbAssetValue()
	{
		return (TextField)getChild(CHILD_TBASSETVALUE);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbAssetIncludeInGDS()
	{
		return (ComboBox)getChild(CHILD_CBASSETINCLUDEINGDS);
	}


	/**
	 *
	 *
	 */
	public Button getBtDeleteAsset()
	{
		return (Button)getChild(CHILD_BTDELETEASSET);
	}


	/**
	 *
	 *
	 */
	public String endBtDeleteAssetDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btDeleteAsset_onBeforeHtmlOutputEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		//CSpHtml.sendMessage("@btDeleteAsset_onBeforeHtmlOutputEvent");
		try
		{
			CSpButton buttonHtml =(CSpButton) event.getSource();

			//CSpHtml.sendMessage("Changed2" + buttonHtml);
			String htmlstr = "<INPUT TYPE=IMAGE NAME=\"deleteAction(Asset," + sumofrowsForAsset + ")\" VALUE=\"Delete\" SRC=\"/eNet_images/delete.gif\" ALIGN=TOP  width=52 height=15 alt=\"\" border=\"0\">";

			//CSpHtml.sendMessage("htmlstr " + htmlstr);
			buttonHtml.setHtmlText(htmlstr);
		}
		catch(Exception ex)
		{
			CSpHtml.sendMessage("Excp@html output " + ex.toString());
		}

		return;
		*/


		return event.getContent();
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIndex()
	{
		return (StaticTextField)getChild(CHILD_STINDEX);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdAssetId()
	{
		return (HiddenField)getChild(CHILD_HDASSETID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdAssetBorrowerId()
	{
		return (HiddenField)getChild(CHILD_HDASSETBORROWERID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbAssetIncludeInTDS()
	{
		return (ComboBox)getChild(CHILD_CBASSETINCLUDEINTDS);
	}


	/**
	 *
	 *
	 */
	public doBorrowerEntryAssetModel getdoBorrowerEntryAssetModel()
	{
		if (doBorrowerEntryAsset == null)
			doBorrowerEntryAsset = (doBorrowerEntryAssetModel) getModel(doBorrowerEntryAssetModel.class);
		return doBorrowerEntryAsset;
	}


	/**
	 *
	 *
	 */
	public void setdoBorrowerEntryAssetModel(doBorrowerEntryAssetModel model)
	{
			doBorrowerEntryAsset = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_TBASSETDESCRIPTION="tbAssetDescription";
	public static final String CHILD_TBASSETDESCRIPTION_RESET_VALUE="";
	public static final String CHILD_TBASSETVALUE="tbAssetValue";
	public static final String CHILD_TBASSETVALUE_RESET_VALUE="";
	public static final String CHILD_CBASSETINCLUDEINGDS="cbAssetIncludeInGDS";
	public static final String CHILD_CBASSETINCLUDEINGDS_RESET_VALUE="N";
	private OptionList cbAssetIncludeInGDSOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
	public static final String CHILD_BTDELETEASSET="btDeleteAsset";
	public static final String CHILD_BTDELETEASSET_RESET_VALUE="Delete";
	public static final String CHILD_STINDEX="stIndex";
	public static final String CHILD_STINDEX_RESET_VALUE="";
	public static final String CHILD_HDASSETID="hdAssetId";
	public static final String CHILD_HDASSETID_RESET_VALUE="";
	public static final String CHILD_HDASSETBORROWERID="hdAssetBorrowerId";
	public static final String CHILD_HDASSETBORROWERID_RESET_VALUE="";
	public static final String CHILD_CBASSETINCLUDEINTDS="cbAssetIncludeInTDS";
	public static final String CHILD_CBASSETINCLUDEINTDS_RESET_VALUE="N";
	private OptionList cbAssetIncludeInTDSOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doBorrowerEntryAssetModel doBorrowerEntryAsset=null;

}

