package mosApp.MosSystem;

import java.math.BigDecimal;
import java.sql.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 * Description: doComponentInfoModelImpl is implementation for doComponentInfoModel
 *
 * @author: MCM Impl Team.
 * @version 1.0 2008-6-11
 * The select sql changed as part of artf751482
 */
public class doComponentInfoModelImpl extends QueryModelBase implements
        doComponentInfoModel {

    // the logger.
    static final private Log _log = LogFactory.getLog(doComponentInfoModelImpl.class);

    /**
     * Constructor function
     */
    public doComponentInfoModelImpl() {
        super();
        setDataSourceName(DATA_SOURCE_NAME);
        setSelectSQLTemplate(SELECT_SQL_TEMPLATE);
        setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);
        setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);
        setFieldSchema(FIELD_SCHEMA);
        initialize();
    }


    /**
     * 
     */
    public void initialize() {
    }


    /**
     * after execute the model, 
     */
    protected String beforeExecute(ModelExecutionContext context,
            int queryType, String sql) throws ModelControlException {

        _log.debug("doComponentInfoModelImpl.beforeExecute() :: SQL = " + sql);
        return sql;
    }


    /* 
     *
     */
    protected void afterExecute(ModelExecutionContext context, int queryType)
            throws ModelControlException {
        String defaultInstanceStateName = getRequestContext().getModelManager()
                .getDefaultModelInstanceName(SessionStateModel.class);

        while (this.next()) {
            _log.info("doComponentInfoImpl: ");
        }
        // Reset Location
        this.beforeFirst();
    }

    /* 
     *
     */
    protected void onDatabaseError(ModelExecutionContext context,
            int queryType, SQLException exception) {
    }

    /**
     * getter and setter implementation.
     */
    public BigDecimal getDfTotalAmount() {
        return (java.math.BigDecimal) getValue(FIELD_DFTOTALAMOUNT);
    }

    public void setDfTotalAmount(BigDecimal value) {
        setValue(FIELD_DFTOTALAMOUNT, value);

    }

    public BigDecimal getDfTotalCashBackAmount() {
        return (java.math.BigDecimal) getValue(FIELD_DFTOTALCASHBACKAMOUNT);

    }

    public void setDfTotalCashBackAmount(BigDecimal value) {
        setValue(FIELD_DFTOTALCASHBACKAMOUNT, value);

    }
    
    public BigDecimal getDfDealId() {
        return (java.math.BigDecimal) getValue(FIELD_DFDEALID);
    }

    public BigDecimal getDfInstitutionId() {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
    }

    public void setDfDealId(BigDecimal value) {
        setValue(FIELD_DFDEALID, value);
    }

    public void setDfInstitutionId(BigDecimal value) {
        setValue(FIELD_DFINSTITUTIONID, value);
    }

    public BigDecimal getDfCopyId() {
        return (java.math.BigDecimal) getValue(FIELD_DFCOPYID);
    }

    public void setDfCopyId(BigDecimal value) {
        setValue(FIELD_DFCOPYID, value);
    }
    
    public BigDecimal getDfTotalLoanAmount() {
        return (java.math.BigDecimal) getValue(FIELD_DFTOTALLOANAMOUNT);
    }

    public void setDfTotalLoanAmount(BigDecimal value) {
        setValue(FIELD_DFTOTALLOANAMOUNT, value);
    }

    // //////////////////////////////////////////////////////////////////////////
    // Obsolete Netdynamics Events - Require Manual Migration
    // //////////////////////////////////////////////////////////////////////////

    // //////////////////////////////////////////////////////////////////////////
    // Custom Methods - Require Manual Migration
    // //////////////////////////////////////////////////////////////////////////

    // //////////////////////////////////////////////////////////////////////////
    // Class variables
    // //////////////////////////////////////////////////////////////////////////

    public static final String DATA_SOURCE_NAME = "jdbc/orcl";

    // the select SQL template.
    public static final String SELECT_SQL_TEMPLATE = "SELECT ALL "
            + "DEAL.TOTALLOANAMOUNT, "
            + "COMPONENTSUMMARY.DEALID, "
            + "COMPONENTSUMMARY.COPYID, "
            + "COMPONENTSUMMARY.INSTITUTIONPROFILEID, "
            + "COMPONENTSUMMARY.TOTALAMOUNT, "
            + "COMPONENTSUMMARY.TOTALCASHBACKAMOUNT "
            + "FROM DEAL, COMPONENTSUMMARY "
            + "__WHERE__  ";

    // the query table name.
    public static final String MODIFYING_QUERY_TABLE_NAME = "DEAL, COMPONENTSUMMARY ";
    // the static query criteria.
    public static final String STATIC_WHERE_CRITERIA = 
        "DEAL.DEALID = COMPONENTSUMMARY.DEALID AND COMPONENTSUMMARY.COPYID=DEAL.COPYID ";

    public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();

    // column definition for each field.
    public static final String QUALIFIED_COLUMN_DFDEALID = "COMPONENTSUMMARY.DEALID";
    public static final String COLUMN_DFDEALID = "DEALID";
    
    public static final String QUALIFIED_COLUMN_DFCOPYID = "COMPONENTSUMMARY.COPYID";
    public static final String COLUMN_DFCOPYID = "COPYID";
    
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID = "COMPONENTSUMMARY.INSTITUTIONPROFILEID";
    public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";

    public static final String QUALIFIED_COLUMN_DFCOMPONENTID = "COMPONENT.COMPONENTID";
    public static final String COLUMN_DFCOMPONENTID = "COMPONENTID";

    public static final String QUALIFIED_COLUMN_DFTOTALAMOUNT = "COMPONENTSUMMARY.TOTALAMOUNT";
    public static final String COLUMN_DFTOTALAMOUNT = "TOTALAMOUNT";

    public static final String QUALIFIED_COLUMN_DFTOTALCASHBACKAMOUNT = "COMPONENTSUMMARY.TOTALCASHBACKAMOUNT";
    public static final String COLUMN_DFTOTALCASHBACKAMOUNT = "TOTALCASHBACKAMOUNT";
    
    public static final String QUALIFIED_COLUMN_DFTOTALLOANAMOUNT = "DEAL.TOTALLOANAMOUNT";
    public static final String COLUMN_DFTOTALLOANAMOUNT = "TOTALLOANAMOUNT";

    // build the relationship between the column definition and the bound field
    // name, which are defined in the interface.
    static {

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFDEALID, COLUMN_DFDEALID,
                QUALIFIED_COLUMN_DFDEALID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCOPYID, COLUMN_DFCOPYID,
                QUALIFIED_COLUMN_DFCOPYID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFINSTITUTIONID, COLUMN_DFINSTITUTIONID,
                QUALIFIED_COLUMN_DFINSTITUTIONID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFTOTALAMOUNT, COLUMN_DFTOTALAMOUNT,
                QUALIFIED_COLUMN_DFTOTALAMOUNT, java.math.BigDecimal.class,
                false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFTOTALCASHBACKAMOUNT, COLUMN_DFTOTALCASHBACKAMOUNT,
                QUALIFIED_COLUMN_DFTOTALCASHBACKAMOUNT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFTOTALLOANAMOUNT, COLUMN_DFTOTALLOANAMOUNT,
                QUALIFIED_COLUMN_DFTOTALLOANAMOUNT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
    }

}
