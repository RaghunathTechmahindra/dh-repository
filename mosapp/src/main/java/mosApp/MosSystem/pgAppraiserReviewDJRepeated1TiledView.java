package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;
import com.basis100.picklist.BXResources;

/**
 *
 *
 *
 */
public class pgAppraiserReviewDJRepeated1TiledView
  extends RequestHandlingTiledViewBase
  implements TiledView, RequestHandler
{
  /**
   *
   *
   */
  public pgAppraiserReviewDJRepeated1TiledView(View parent, String name)
  {
    super(parent, name);
    setMaxDisplayTiles(10);
    setPrimaryModelClass(doAppraisalPropertyModel.class);
    registerChildren();
    initialize();
  }

  /**
   *
   *
   */
  protected void initialize()
  {
  }

  /**
   *
   *
   */
  public void resetChildren()
  {
    getStPrimaryProperty().setValue(CHILD_STPRIMARYPROPERTY_RESET_VALUE);
    getStPropertyAddress().setValue(CHILD_STPROPERTYADDRESS_RESET_VALUE);
    getStPropertyOccurence().setValue(CHILD_STPROPERTYOCCURENCE_RESET_VALUE);
    getStAppraiserName().setValue(CHILD_STAPPRAISERNAME_RESET_VALUE);
    getStAppraiserShortName().setValue(CHILD_STAPPRAISERSHORTNAME_RESET_VALUE);
    getStAppraiserCompanyName().setValue(CHILD_STAPPRAISERCOMPANYNAME_RESET_VALUE);
    getStAppraiserAddress().setValue(CHILD_STAPPRAISERADDRESS_RESET_VALUE);
    getStPhone().setValue(CHILD_STPHONE_RESET_VALUE);
    getStFax().setValue(CHILD_STFAX_RESET_VALUE);
    getStEmail().setValue(CHILD_STEMAIL_RESET_VALUE);
    getStPropertyPurchasePrice().setValue(CHILD_STPROPERTYPURCHASEPRICE_RESET_VALUE);
    getTbEstimatedAppraisal().setValue(CHILD_TBESTIMATEDAPPRAISAL_RESET_VALUE);
    getTbActualAppraisal().setValue(CHILD_TBACTUALAPPRAISAL_RESET_VALUE);
    getCbMonths().setValue(CHILD_CBMONTHS_RESET_VALUE);
    getTbDay().setValue(CHILD_TBDAY_RESET_VALUE);
    getTbYear().setValue(CHILD_TBYEAR_RESET_VALUE);
    getCbAppraisalSource().setValue(CHILD_CBAPPRAISALSOURCE_RESET_VALUE);
    getTbAppraiserNotes().setValue(CHILD_TBAPPRAISERNOTES_RESET_VALUE);
    getBtChangeAppraiser().setValue(CHILD_BTCHANGEAPPRAISER_RESET_VALUE);
    getBtAddAppraiser().setValue(CHILD_BTADDAPPRAISER_RESET_VALUE);
    getHbPropertyId().setValue(CHILD_HBPROPERTYID_RESET_VALUE);
    getHdAppraisalDate().setValue(CHILD_HDAPPRAISALDATE_RESET_VALUE);
    getStAppraiserExtension().setValue(CHILD_STAPPRAISEREXTENSION_RESET_VALUE);
    //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
    //--> By Billy 16Dec2003
    //New display fields for AppraisalOrder
    getStAppraisalStatus().setValue(CHILD_STAPPRAISALSTATUS_RESET_VALUE);
    getChUseBorrowerInfo().setValue(CHILD_CHUSEBORROWERINFO_RESET_VALUE);
    getTbPropertyOwnerName().setValue(CHILD_TBPROPERTYOWNERNAME_RESET_VALUE);
    getTbAOContactName().setValue(CHILD_TBAOCONTACTNAME_RESET_VALUE);
    getTbAOContactWorkPhoneNum1().setValue(CHILD_TBAOCONTACTWORKPHONENUM1_RESET_VALUE);
    getTbAOContactWorkPhoneNum2().setValue(CHILD_TBAOCONTACTWORKPHONENUM2_RESET_VALUE);
    getTbAOContactWorkPhoneNum3().setValue(CHILD_TBAOCONTACTWORKPHONENUM3_RESET_VALUE);
    getTbAOContactWorkPhoneNumExt().setValue(CHILD_TBAOCONTACTWORKPHONENUMEXT_RESET_VALUE);
    getTbAOContactCellPhoneNum1().setValue(CHILD_TBAOCONTACTCELLPHONENUM1_RESET_VALUE);
    getTbAOContactCellPhoneNum2().setValue(CHILD_TBAOCONTACTCELLPHONENUM2_RESET_VALUE);
    getTbAOContactCellPhoneNum3().setValue(CHILD_TBAOCONTACTCELLPHONENUM3_RESET_VALUE);
    getTbAOContactHomePhoneNum1().setValue(CHILD_TBAOCONTACTHOMEPHONENUM1_RESET_VALUE);
    getTbAOContactHomePhoneNum2().setValue(CHILD_TBAOCONTACTHOMEPHONENUM2_RESET_VALUE);
    getTbAOContactHomePhoneNum3().setValue(CHILD_TBAOCONTACTHOMEPHONENUM3_RESET_VALUE);
    getTbCommentsForAppraiser().setValue(CHILD_TBCOMMENTSFORAPPRAISER_RESET_VALUE);
    getStPropertyId().setValue(CHILD_STPROPERTYID_RESET_VALUE);
    //=================================================================
  }

  /**
   *
   *
   */
  protected void registerChildren()
  {
    registerChild(CHILD_STPRIMARYPROPERTY, StaticTextField.class);
    registerChild(CHILD_STPROPERTYADDRESS, StaticTextField.class);
    registerChild(CHILD_STPROPERTYOCCURENCE, StaticTextField.class);
    registerChild(CHILD_STAPPRAISERNAME, StaticTextField.class);
    registerChild(CHILD_STAPPRAISERSHORTNAME, StaticTextField.class);
    registerChild(CHILD_STAPPRAISERCOMPANYNAME, StaticTextField.class);
    registerChild(CHILD_STAPPRAISERADDRESS, StaticTextField.class);
    registerChild(CHILD_STPHONE, StaticTextField.class);
    registerChild(CHILD_STFAX, StaticTextField.class);
    registerChild(CHILD_STEMAIL, StaticTextField.class);
    registerChild(CHILD_STPROPERTYPURCHASEPRICE, StaticTextField.class);
    registerChild(CHILD_TBESTIMATEDAPPRAISAL, TextField.class);
    registerChild(CHILD_TBACTUALAPPRAISAL, TextField.class);
    registerChild(CHILD_CBMONTHS, ComboBox.class);
    registerChild(CHILD_TBDAY, TextField.class);
    registerChild(CHILD_TBYEAR, TextField.class);
    registerChild(CHILD_CBAPPRAISALSOURCE, ComboBox.class);
    registerChild(CHILD_TBAPPRAISERNOTES, TextField.class);
    registerChild(CHILD_BTCHANGEAPPRAISER, Button.class);
    registerChild(CHILD_BTADDAPPRAISER, Button.class);
    registerChild(CHILD_HBPROPERTYID, HiddenField.class);
    registerChild(CHILD_HDAPPRAISALDATE, HiddenField.class);
    registerChild(CHILD_STAPPRAISEREXTENSION, StaticTextField.class);
    //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
    //--> By Billy 16Dec2003
    //New display fields for AppraisalOrder
    registerChild(CHILD_STAPPRAISALSTATUS, StaticTextField.class);
    registerChild(CHILD_CHUSEBORROWERINFO, CheckBox.class);
    registerChild(CHILD_TBPROPERTYOWNERNAME, TextField.class);
    registerChild(CHILD_TBAOCONTACTNAME, TextField.class);
    registerChild(CHILD_TBAOCONTACTWORKPHONENUM1, TextField.class);
    registerChild(CHILD_TBAOCONTACTWORKPHONENUM2, TextField.class);
    registerChild(CHILD_TBAOCONTACTWORKPHONENUM3, TextField.class);
    registerChild(CHILD_TBAOCONTACTWORKPHONENUMEXT, TextField.class);
    registerChild(CHILD_TBAOCONTACTCELLPHONENUM1, TextField.class);
    registerChild(CHILD_TBAOCONTACTCELLPHONENUM2, TextField.class);
    registerChild(CHILD_TBAOCONTACTCELLPHONENUM3, TextField.class);
    registerChild(CHILD_TBAOCONTACTHOMEPHONENUM1, TextField.class);
    registerChild(CHILD_TBAOCONTACTHOMEPHONENUM2, TextField.class);
    registerChild(CHILD_TBAOCONTACTHOMEPHONENUM3, TextField.class);
    registerChild(CHILD_TBCOMMENTSFORAPPRAISER, TextField.class);
    registerChild(CHILD_STPROPERTYID, StaticTextField.class);
    //================================================================
  }

  ////////////////////////////////////////////////////////////////////////////////
  // Model management methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  public Model[] getWebActionModels(int executionType)
  {
    List modelList = new ArrayList();
    switch(executionType)
    {
      case MODEL_TYPE_RETRIEVE:
        modelList.add(getdoAppraisalPropertyModel()); ;
        break;

      case MODEL_TYPE_UPDATE:
        ;
        break;

      case MODEL_TYPE_DELETE:
        ;
        break;

      case MODEL_TYPE_INSERT:
        ;
        break;

      case MODEL_TYPE_EXECUTE:
        ;
        break;
    }
    return(Model[])modelList.toArray(new Model[0]);
  }

  ////////////////////////////////////////////////////////////////////////////////
  // View flow control methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  public void beginDisplay(DisplayEvent event) throws ModelControlException
  {
    // This is the analog of NetDynamics repeated_onBeforeDisplayEvent
    // Ensure the primary model is non-null; if null, it would cause havoc
    // with the various methods dependent on the primary model
    if(getPrimaryModel() == null)
    {
      throw new ModelControlException("Primary model is null");
    }

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    AppraisalHandlerDJ handler = (AppraisalHandlerDJ)this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());

    //Set up all ComboBox options
    cbAppraisalSourceOptions.populate(getRequestContext());
    //=======================================================

    super.beginDisplay(event);
    resetTileIndex();
  }

  /**
   *
   *
   */
  public boolean nextTile() throws ModelControlException
  {
    // This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
    boolean movedToRow = super.nextTile();

    if(movedToRow)
    {
      AppraisalHandlerDJ handler = (AppraisalHandlerDJ)this.handler.cloneSS();
      handler.pageGetState(this.getParentViewBean());
      handler.displayAppraiser(getTileIndex());
      handler.pageSaveState();
    }

    return movedToRow;

  }

  /**
   *
   *
   */
  public boolean beforeModelExecutes(Model model, int executionContext)
  {
    // This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
    return super.beforeModelExecutes(model, executionContext);
  }

  /**
   *
   *
   */
  public void afterModelExecutes(Model model, int executionContext)
  {
    // This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
    super.afterModelExecutes(model, executionContext);
  }

  /**
   *
   *
   */
  public void afterAllModelsExecute(int executionContext)
  {
    // This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
    super.afterAllModelsExecute(executionContext);
  }

  /**
   *
   *
   */
  public void onModelError(Model model, int executionContext, ModelControlException exception) throws ModelControlException
  {

    // This is the analog of NetDynamics repeated_onDataObjectErrorEvent
    super.onModelError(model, executionContext, exception);

  }

  ////////////////////////////////////////////////////////////////////////////////
  // Child manipulation methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  protected View createChild(String name)
  {
    if(name.equals(CHILD_STPRIMARYPROPERTY))
    {
      StaticTextField child = new StaticTextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_STPRIMARYPROPERTY,
        doAppraisalPropertyModel.FIELD_DFPRIMARYPROPERTY,
        CHILD_STPRIMARYPROPERTY_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPROPERTYADDRESS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_STPROPERTYADDRESS,
        doAppraisalPropertyModel.FIELD_DFPROPERTYADDRESS,
        CHILD_STPROPERTYADDRESS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPROPERTYOCCURENCE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_STPROPERTYOCCURENCE,
        doAppraisalPropertyModel.FIELD_DFPROPERTYOCCURENCE,
        CHILD_STPROPERTYOCCURENCE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAPPRAISERNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAPPRAISERNAME,
        CHILD_STAPPRAISERNAME,
        CHILD_STAPPRAISERNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAPPRAISERSHORTNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAPPRAISERSHORTNAME,
        CHILD_STAPPRAISERSHORTNAME,
        CHILD_STAPPRAISERSHORTNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAPPRAISERCOMPANYNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAPPRAISERCOMPANYNAME,
        CHILD_STAPPRAISERCOMPANYNAME,
        CHILD_STAPPRAISERCOMPANYNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAPPRAISERADDRESS))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAPPRAISERADDRESS,
        CHILD_STAPPRAISERADDRESS,
        CHILD_STAPPRAISERADDRESS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPHONE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPHONE,
        CHILD_STPHONE,
        CHILD_STPHONE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STFAX))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STFAX,
        CHILD_STFAX,
        CHILD_STFAX_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STEMAIL))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STEMAIL,
        CHILD_STEMAIL,
        CHILD_STEMAIL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPROPERTYPURCHASEPRICE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_STPROPERTYPURCHASEPRICE,
        doAppraisalPropertyModel.FIELD_DFPURCHASEPRICE,
        CHILD_STPROPERTYPURCHASEPRICE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBESTIMATEDAPPRAISAL))
    {
      TextField child = new TextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_TBESTIMATEDAPPRAISAL,
        doAppraisalPropertyModel.FIELD_DFESTIMATEDAPPRAISAL,
        CHILD_TBESTIMATEDAPPRAISAL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBACTUALAPPRAISAL))
    {
      TextField child = new TextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_TBACTUALAPPRAISAL,
        doAppraisalPropertyModel.FIELD_DFACTUALAPPRAISAL,
        CHILD_TBACTUALAPPRAISAL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_CBMONTHS))
    {
      ComboBox child = new ComboBox(this,
        //getDefaultModel(),
        getdoAppraisalPropertyModel(),
        CHILD_CBMONTHS,
        CHILD_CBMONTHS,
        CHILD_CBMONTHS_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbMonthsOptions);
      return child;
    }
    else
    if(name.equals(CHILD_TBDAY))
    {
      TextField child = new TextField(this,
        //getDefaultModel(),
        getdoAppraisalPropertyModel(),
        CHILD_TBDAY,
        CHILD_TBDAY,
        CHILD_TBDAY_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBYEAR))
    {
      TextField child = new TextField(this,
        //getDefaultModel(),
        getdoAppraisalPropertyModel(),
        CHILD_TBYEAR,
        CHILD_TBYEAR,
        CHILD_TBYEAR_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_CBAPPRAISALSOURCE))
    {
      ComboBox child = new ComboBox(this,
        getdoAppraisalPropertyModel(),
        CHILD_CBAPPRAISALSOURCE,
        doAppraisalPropertyModel.FIELD_DFAPPRAISALSOURCEID,
        CHILD_CBAPPRAISALSOURCE_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbAppraisalSourceOptions);
      return child;
    }
    else
    if(name.equals(CHILD_TBAPPRAISERNOTES))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAPPRAISERNOTES,
        CHILD_TBAPPRAISERNOTES,
        CHILD_TBAPPRAISERNOTES_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_BTCHANGEAPPRAISER))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTCHANGEAPPRAISER,
        CHILD_BTCHANGEAPPRAISER,
        CHILD_BTCHANGEAPPRAISER_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_BTADDAPPRAISER))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTADDAPPRAISER,
        CHILD_BTADDAPPRAISER,
        CHILD_BTADDAPPRAISER_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HBPROPERTYID))
    {
      HiddenField child = new HiddenField(this,
        getdoAppraisalPropertyModel(),
        CHILD_HBPROPERTYID,
        doAppraisalPropertyModel.FIELD_DFPROPERTYID,
        CHILD_HBPROPERTYID_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_HDAPPRAISALDATE))
    {
      HiddenField child = new HiddenField(this,
        getdoAppraisalPropertyModel(),
        CHILD_HDAPPRAISALDATE,
        doAppraisalPropertyModel.FIELD_DFAPPRAISALDATE,
        CHILD_HDAPPRAISALDATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAPPRAISEREXTENSION))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAPPRAISEREXTENSION,
        CHILD_STAPPRAISEREXTENSION,
        CHILD_STAPPRAISEREXTENSION_RESET_VALUE,
        null);
      return child;
    }
    //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
    //--> By Billy 16Dec2003
    //New display fields for AppraisalOrder
    else
    if(name.equals(CHILD_STAPPRAISALSTATUS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_STAPPRAISALSTATUS,
        doAppraisalPropertyModel.FIELD_DFAPPRAISALSTATUSDESC,
        CHILD_STAPPRAISALSTATUS_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_CHUSEBORROWERINFO))
    {
      CheckBox child = new CheckBox(this,
        getdoAppraisalPropertyModel(),
        CHILD_CHUSEBORROWERINFO,
        doAppraisalPropertyModel.FIELD_DFUSEBORROWERINFO,
        "Y","N",
        false,
        null);

      return child;
    }
    else
    if(name.equals(CHILD_TBPROPERTYOWNERNAME))
    {
      TextField child = new TextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_TBPROPERTYOWNERNAME,
        doAppraisalPropertyModel.FIELD_DFPROPERTYOWNERNAME,
        CHILD_TBPROPERTYOWNERNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBPROPERTYOWNERNAME))
    {
      TextField child = new TextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_TBPROPERTYOWNERNAME,
        doAppraisalPropertyModel.FIELD_DFPROPERTYOWNERNAME,
        CHILD_TBPROPERTYOWNERNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTNAME))
    {
      TextField child = new TextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_TBAOCONTACTNAME,
        doAppraisalPropertyModel.FIELD_DFAOCONTACTNAME,
        CHILD_TBAOCONTACTNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTWORKPHONENUM1))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAOCONTACTWORKPHONENUM1,
        CHILD_TBAOCONTACTWORKPHONENUM1,
        CHILD_TBAOCONTACTWORKPHONENUM1_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTWORKPHONENUM2))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAOCONTACTWORKPHONENUM2,
        CHILD_TBAOCONTACTWORKPHONENUM2,
        CHILD_TBAOCONTACTWORKPHONENUM2_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTWORKPHONENUM3))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAOCONTACTWORKPHONENUM3,
        CHILD_TBAOCONTACTWORKPHONENUM3,
        CHILD_TBAOCONTACTWORKPHONENUM3_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTWORKPHONENUMEXT))
    {
      TextField child = new TextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_TBAOCONTACTWORKPHONENUMEXT,
        doAppraisalPropertyModel.FIELD_DFAOCONTACTWORKPHONENUMEXT,
        CHILD_TBAOCONTACTWORKPHONENUMEXT_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTCELLPHONENUM1))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAOCONTACTCELLPHONENUM1,
        CHILD_TBAOCONTACTCELLPHONENUM1,
        CHILD_TBAOCONTACTCELLPHONENUM1_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTCELLPHONENUM2))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAOCONTACTCELLPHONENUM2,
        CHILD_TBAOCONTACTCELLPHONENUM2,
        CHILD_TBAOCONTACTCELLPHONENUM2_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTCELLPHONENUM3))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAOCONTACTCELLPHONENUM3,
        CHILD_TBAOCONTACTCELLPHONENUM3,
        CHILD_TBAOCONTACTCELLPHONENUM3_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTHOMEPHONENUM1))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAOCONTACTHOMEPHONENUM1,
        CHILD_TBAOCONTACTHOMEPHONENUM1,
        CHILD_TBAOCONTACTHOMEPHONENUM1_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTHOMEPHONENUM2))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAOCONTACTHOMEPHONENUM2,
        CHILD_TBAOCONTACTHOMEPHONENUM2,
        CHILD_TBAOCONTACTHOMEPHONENUM2_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTHOMEPHONENUM3))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAOCONTACTHOMEPHONENUM3,
        CHILD_TBAOCONTACTHOMEPHONENUM3,
        CHILD_TBAOCONTACTHOMEPHONENUM3_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBCOMMENTSFORAPPRAISER))
    {
      TextField child = new TextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_TBCOMMENTSFORAPPRAISER,
        doAppraisalPropertyModel.FIELD_DFCOMMENTSFORAPPRAISER,
        CHILD_TBCOMMENTSFORAPPRAISER_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPROPERTYID))
    {
      StaticTextField child = new StaticTextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_STPROPERTYID,
        doAppraisalPropertyModel.FIELD_DFPROPERTYID,
        CHILD_STPROPERTYID_RESET_VALUE,
        null);
      return child;
    }

    //==================================================================
    else
    {
      throw new IllegalArgumentException("Invalid child name [" + name + "]");
    }
  }

  /**
   *
   *
   */
  public StaticTextField getStPrimaryProperty()
  {
    return(StaticTextField)getChild(CHILD_STPRIMARYPROPERTY);
  }

  /**
   *
   *
   */
  public String endStPrimaryPropertyDisplay(ChildContentDisplayEvent event)
  {
    // The following code block was migrated from the stPrimaryProperty_onBeforeHtmlOutputEvent method
    AppraisalHandlerDJ handler = (AppraisalHandlerDJ)this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());
    String rc = handler.setYesOrNo(event.getContent());
    handler.pageSaveState();

    return rc;
  }

  /**
   *
   *
   */
  public StaticTextField getStPropertyAddress()
  {
    return(StaticTextField)getChild(CHILD_STPROPERTYADDRESS);
  }

  /**
   *
   *
   */
  public StaticTextField getStPropertyOccurence()
  {
    return(StaticTextField)getChild(CHILD_STPROPERTYOCCURENCE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAppraiserName()
  {
    return(StaticTextField)getChild(CHILD_STAPPRAISERNAME);
  }

  /**
   *
   *
   */
  public StaticTextField getStAppraiserShortName()
  {
    return(StaticTextField)getChild(CHILD_STAPPRAISERSHORTNAME);
  }

  /**
   *
   *
   */
  public StaticTextField getStAppraiserCompanyName()
  {
    return(StaticTextField)getChild(CHILD_STAPPRAISERCOMPANYNAME);
  }

  /**
   *
   *
   */
  public StaticTextField getStAppraiserAddress()
  {
    return(StaticTextField)getChild(CHILD_STAPPRAISERADDRESS);
  }

  /**
   *
   *
   */
  public StaticTextField getStPhone()
  {
    return(StaticTextField)getChild(CHILD_STPHONE);
  }

  /**
   *
   *
   */
  public StaticTextField getStFax()
  {
    return(StaticTextField)getChild(CHILD_STFAX);
  }

  /**
   *
   *
   */
  public StaticTextField getStEmail()
  {
    return(StaticTextField)getChild(CHILD_STEMAIL);
  }

  /**
   *
   *
   */
  public StaticTextField getStPropertyPurchasePrice()
  {
    return(StaticTextField)getChild(CHILD_STPROPERTYPURCHASEPRICE);
  }

  /**
   *
   *
   */
  public TextField getTbEstimatedAppraisal()
  {
    return(TextField)getChild(CHILD_TBESTIMATEDAPPRAISAL);
  }

  /**
   *
   *
   */
  public TextField getTbActualAppraisal()
  {
    return(TextField)getChild(CHILD_TBACTUALAPPRAISAL);
  }

  /**
   *
   *
   */
  public ComboBox getCbMonths()
  {
    return(ComboBox)getChild(CHILD_CBMONTHS);
  }

  /**
   *
   *
   */
  public TextField getTbDay()
  {
    return(TextField)getChild(CHILD_TBDAY);
  }

  /**
   *
   *
   */
  public TextField getTbYear()
  {
    return(TextField)getChild(CHILD_TBYEAR);
  }

  /**
   *
   *
   */
  public ComboBox getCbAppraisalSource()
  {
    return(ComboBox)getChild(CHILD_CBAPPRAISALSOURCE);
  }

  /**
   *
   *
   */
  static class CbAppraisalSourceOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbAppraisalSourceOptionList()
    {

    }

    /**
     *
     *
     */
    //--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 22Nov2002
    public void populate(RequestContext rc)
    {
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
          (SessionStateModelImpl)rc.getModelManager().getModel(
          SessionStateModel.class,
          defaultInstanceStateName,
          true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "APPRAISALSOURCE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
      }
      catch(Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

  /**
   *
   *
   */
  public TextField getTbAppraiserNotes()
  {
    return(TextField)getChild(CHILD_TBAPPRAISERNOTES);
  }

  /**
   *
   *
   */
  public void handleBtChangeAppraiserRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btChangeAppraiser_onWebEvent method
    AppraisalHandlerDJ handler = (AppraisalHandlerDJ)this.handler.cloneSS();
    handler.preHandlerProtocol(this.getParentViewBean());
    handler.changeAppraiser(handler.getRowNdxFromWebEventMethod(event), "btChangeAppraiser");
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtChangeAppraiser()
  {
    return(Button)getChild(CHILD_BTCHANGEAPPRAISER);
  }

  /**
   *
   *
   */
  public String endBtChangeAppraiserDisplay(ChildContentDisplayEvent event)
  {
    // The following code block was migrated from the btChangeAppraiser_onBeforeHtmlOutputEvent method
    AppraisalHandlerDJ handler = (AppraisalHandlerDJ)this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());
    boolean rc = handler.setChangeAppraiserStatus();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public void handleBtAddAppraiserRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btAddAppraiser_onWebEvent method
    AppraisalHandlerDJ handler = (AppraisalHandlerDJ)this.handler.cloneSS();
    handler.preHandlerProtocol(this.getParentViewBean());
    handler.changeAppraiser(handler.getRowNdxFromWebEventMethod(event), "btAddAppraiser");
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtAddAppraiser()
  {
    return(Button)getChild(CHILD_BTADDAPPRAISER);
  }

  /**
   *
   *
   */
  public String endBtAddAppraiserDisplay(ChildContentDisplayEvent event)
  {
    // The following code block was migrated from the btAddAppraiser_onBeforeHtmlOutputEvent method
    AppraisalHandlerDJ handler = (AppraisalHandlerDJ)this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());
    boolean rc = handler.setAddAppraiserStatus();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public HiddenField getHbPropertyId()
  {
    return(HiddenField)getChild(CHILD_HBPROPERTYID);
  }

  /**
   *
   *
   */
  public HiddenField getHdAppraisalDate()
  {
    return(HiddenField)getChild(CHILD_HDAPPRAISALDATE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAppraiserExtension()
  {
    return(StaticTextField)getChild(CHILD_STAPPRAISEREXTENSION);
  }

  /**
   *
   *
   */
  public doAppraisalPropertyModel getdoAppraisalPropertyModel()
  {
    if(doAppraisalProperty == null)
    {
      doAppraisalProperty = (doAppraisalPropertyModel)getModel(doAppraisalPropertyModel.class);
    }
    return doAppraisalProperty;
  }

  /**
   *
   *
   */
  public void setdoAppraisalPropertyModel(doAppraisalPropertyModel model)
  {
    doAppraisalProperty = model;
  }

  //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
  //--> By Billy 16Dec2003
  //New display fields for AppraisalOrder
  public StaticTextField getStAppraisalStatus()
  {
    return(StaticTextField)getChild(CHILD_STAPPRAISALSTATUS);
  }

  public CheckBox getChUseBorrowerInfo()
  {
    return(CheckBox)getChild(CHILD_CHUSEBORROWERINFO);
  }

  public TextField getTbPropertyOwnerName()
  {
    return(TextField)getChild(CHILD_TBPROPERTYOWNERNAME);
  }

  public TextField getTbAOContactName()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTNAME);
  }

  public TextField getTbAOContactWorkPhoneNum1()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTWORKPHONENUM1);
  }

  public TextField getTbAOContactWorkPhoneNum2()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTWORKPHONENUM2);
  }

  public TextField getTbAOContactWorkPhoneNum3()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTWORKPHONENUM3);
  }

  public TextField getTbAOContactWorkPhoneNumExt()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTWORKPHONENUMEXT);
  }

  public TextField getTbAOContactCellPhoneNum1()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTCELLPHONENUM1);
  }

  public TextField getTbAOContactCellPhoneNum2()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTCELLPHONENUM2);
  }

  public TextField getTbAOContactCellPhoneNum3()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTCELLPHONENUM3);
  }

  public TextField getTbAOContactHomePhoneNum1()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTHOMEPHONENUM1);
  }

  public TextField getTbAOContactHomePhoneNum2()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTHOMEPHONENUM2);
  }

  public TextField getTbAOContactHomePhoneNum3()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTHOMEPHONENUM3);
  }

  public TextField getTbCommentsForAppraiser()
  {
    return(TextField)getChild(CHILD_TBCOMMENTSFORAPPRAISER);
  }

  public StaticTextField getStPropertyId()
 {
   return(StaticTextField)getChild(CHILD_STPROPERTYID);
 }

  //==================================================================

  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////
  // Child accessors
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Child rendering methods
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Repeated event methods
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////

  public static Map FIELD_DESCRIPTORS;
  public static final String CHILD_STPRIMARYPROPERTY = "stPrimaryProperty";
  public static final String CHILD_STPRIMARYPROPERTY_RESET_VALUE = "";
  public static final String CHILD_STPROPERTYADDRESS = "stPropertyAddress";
  public static final String CHILD_STPROPERTYADDRESS_RESET_VALUE = "";
  public static final String CHILD_STPROPERTYOCCURENCE = "stPropertyOccurence";
  public static final String CHILD_STPROPERTYOCCURENCE_RESET_VALUE = "";
  public static final String CHILD_STAPPRAISERNAME = "stAppraiserName";
  public static final String CHILD_STAPPRAISERNAME_RESET_VALUE = "";
  public static final String CHILD_STAPPRAISERSHORTNAME = "stAppraiserShortName";
  public static final String CHILD_STAPPRAISERSHORTNAME_RESET_VALUE = "";
  public static final String CHILD_STAPPRAISERCOMPANYNAME = "stAppraiserCompanyName";
  public static final String CHILD_STAPPRAISERCOMPANYNAME_RESET_VALUE = "";
  public static final String CHILD_STAPPRAISERADDRESS = "stAppraiserAddress";
  public static final String CHILD_STAPPRAISERADDRESS_RESET_VALUE = "";
  public static final String CHILD_STPHONE = "stPhone";
  public static final String CHILD_STPHONE_RESET_VALUE = "";
  public static final String CHILD_STFAX = "stFax";
  public static final String CHILD_STFAX_RESET_VALUE = "";
  public static final String CHILD_STEMAIL = "stEmail";
  public static final String CHILD_STEMAIL_RESET_VALUE = "";
  public static final String CHILD_STPROPERTYPURCHASEPRICE = "stPropertyPurchasePrice";
  public static final String CHILD_STPROPERTYPURCHASEPRICE_RESET_VALUE = "";
  public static final String CHILD_TBESTIMATEDAPPRAISAL = "tbEstimatedAppraisal";
  public static final String CHILD_TBESTIMATEDAPPRAISAL_RESET_VALUE = "";
  public static final String CHILD_TBACTUALAPPRAISAL = "tbActualAppraisal";
  public static final String CHILD_TBACTUALAPPRAISAL_RESET_VALUE = "";
  public static final String CHILD_CBMONTHS = "cbMonths";
  public static final String CHILD_CBMONTHS_RESET_VALUE = "";
  private OptionList cbMonthsOptions = new OptionList(new String[]
    {}
    , new String[]
    {});
  public static final String CHILD_TBDAY = "tbDay";
  public static final String CHILD_TBDAY_RESET_VALUE = "";
  public static final String CHILD_TBYEAR = "tbYear";
  public static final String CHILD_TBYEAR_RESET_VALUE = "";
  public static final String CHILD_CBAPPRAISALSOURCE = "cbAppraisalSource";
  public static final String CHILD_CBAPPRAISALSOURCE_RESET_VALUE = "";

  //--Release2.1--//
  //private static CbAppraisalSourceOptionList cbAppraisalSourceOptions=new CbAppraisalSourceOptionList();
  private CbAppraisalSourceOptionList cbAppraisalSourceOptions = new CbAppraisalSourceOptionList();
  public static final String CHILD_TBAPPRAISERNOTES = "tbAppraiserNotes";
  public static final String CHILD_TBAPPRAISERNOTES_RESET_VALUE = "";
  public static final String CHILD_BTCHANGEAPPRAISER = "btChangeAppraiser";
  public static final String CHILD_BTCHANGEAPPRAISER_RESET_VALUE = " ";
  public static final String CHILD_BTADDAPPRAISER = "btAddAppraiser";
  public static final String CHILD_BTADDAPPRAISER_RESET_VALUE = " ";
  public static final String CHILD_HBPROPERTYID = "hbPropertyId";
  public static final String CHILD_HBPROPERTYID_RESET_VALUE = "";
  public static final String CHILD_HDAPPRAISALDATE = "hdAppraisalDate";
  public static final String CHILD_HDAPPRAISALDATE_RESET_VALUE = "";
  public static final String CHILD_STAPPRAISEREXTENSION = "stAppraiserExtension";
  public static final String CHILD_STAPPRAISEREXTENSION_RESET_VALUE = "";
  //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
  //--> By Billy 16Dec2003
  //New display fields for AppraisalOrder
  public static final String CHILD_STAPPRAISALSTATUS = "stAppraisalStatus";
  public static final String CHILD_STAPPRAISALSTATUS_RESET_VALUE = "";
  public static final String CHILD_CHUSEBORROWERINFO = "chUseBorrowerInfo";
  public static final String CHILD_CHUSEBORROWERINFO_RESET_VALUE = "";
  public static final String CHILD_TBPROPERTYOWNERNAME = "tbPropertyOwnerName";
  public static final String CHILD_TBPROPERTYOWNERNAME_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTNAME = "tbAOContactName";
  public static final String CHILD_TBAOCONTACTNAME_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTWORKPHONENUM1 = "tbAOContactWorkPhoneNum1";
  public static final String CHILD_TBAOCONTACTWORKPHONENUM1_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTWORKPHONENUM2 = "tbAOContactWorkPhoneNum2";
  public static final String CHILD_TBAOCONTACTWORKPHONENUM2_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTWORKPHONENUM3 = "tbAOContactWorkPhoneNum3";
  public static final String CHILD_TBAOCONTACTWORKPHONENUM3_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTWORKPHONENUMEXT = "tbAOContactWorkPhoneNumExt";
  public static final String CHILD_TBAOCONTACTWORKPHONENUMEXT_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTCELLPHONENUM1 = "tbAOContactCellPhoneNum1";
  public static final String CHILD_TBAOCONTACTCELLPHONENUM1_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTCELLPHONENUM2 = "tbAOContactCellPhoneNum2";
  public static final String CHILD_TBAOCONTACTCELLPHONENUM2_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTCELLPHONENUM3 = "tbAOContactCellPhoneNum3";
  public static final String CHILD_TBAOCONTACTCELLPHONENUM3_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTHOMEPHONENUM1 = "tbAOContactHomePhoneNum1";
  public static final String CHILD_TBAOCONTACTHOMEPHONENUM1_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTHOMEPHONENUM2 = "tbAOContactHomePhoneNum2";
  public static final String CHILD_TBAOCONTACTHOMEPHONENUM2_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTHOMEPHONENUM3 = "tbAOContactHomePhoneNum3";
  public static final String CHILD_TBAOCONTACTHOMEPHONENUM3_RESET_VALUE = "";
  public static final String CHILD_TBCOMMENTSFORAPPRAISER = "tbCommentsForAppraiser";
  public static final String CHILD_TBCOMMENTSFORAPPRAISER_RESET_VALUE = "";
  public static final String CHILD_STPROPERTYID = "stPropertyId";
  public static final String CHILD_STPROPERTYID_RESET_VALUE = "";
  //============================================================


  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////

  private doAppraisalPropertyModel doAppraisalProperty = null;
  private AppraisalHandlerDJ handler = new AppraisalHandlerDJ();
}
