package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doMainViewPrimaryPropertyModelImpl extends QueryModelBase
	implements doMainViewPrimaryPropertyModel
{
	/**
	 *
	 *
	 */
	public doMainViewPrimaryPropertyModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 20Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert PROVINCE name.
      this.setDfProvince(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "PROVINCE", this.getDfProvince(), languageId));
      //Convert PROPERTYUSAGE Desc.
      this.setDfPorpertyUsage(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "PROPERTYUSAGE", this.getDfPorpertyUsage(), languageId));
      //Convert OCCUPANCYTYPE Desc.
      this.setDfOccupancy(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "OCCUPANCYTYPE", this.getDfOccupancy(), languageId));
      //Convert PROPERTYTYPE Desc.
      this.setPropertyType(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "PROPERTYTYPE", this.getPropertyType(), languageId));
      //Convert STREETTYPE Desc.
      this.setDfStreetTypeDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "STREETTYPE", this.getDfStreetTypeDesc(), languageId));
      //Convert STREETDIRECTION Desc.
      this.setDfStreetDirectionDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "STREETDIRECTION", this.getDfStreetDirectionDesc(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPrimaryPropertyFlag()
	{
		return (String)getValue(FIELD_DFPRIMARYPROPERTYFLAG);
	}


	/**
	 *
	 *
	 */
	public void setDfPrimaryPropertyFlag(String value)
	{
		setValue(FIELD_DFPRIMARYPROPERTYFLAG,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStreetNumber()
	{
		return (String)getValue(FIELD_DFSTREETNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfStreetNumber(String value)
	{
		setValue(FIELD_DFSTREETNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStreetName()
	{
		return (String)getValue(FIELD_DFSTREETNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfStreetName(String value)
	{
		setValue(FIELD_DFSTREETNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAddressLine2()
	{
		return (String)getValue(FIELD_DFADDRESSLINE2);
	}


	/**
	 *
	 *
	 */
	public void setDfAddressLine2(String value)
	{
		setValue(FIELD_DFADDRESSLINE2,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCity()
	{
		return (String)getValue(FIELD_DFCITY);
	}


	/**
	 *
	 *
	 */
	public void setDfCity(String value)
	{
		setValue(FIELD_DFCITY,value);
	}


	/**
	 *
	 *
	 */
	public String getDfProvince()
	{
		return (String)getValue(FIELD_DFPROVINCE);
	}


	/**
	 *
	 *
	 */
	public void setDfProvince(String value)
	{
		setValue(FIELD_DFPROVINCE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPostalCode1()
	{
		return (String)getValue(FIELD_DFPOSTALCODE1);
	}


	/**
	 *
	 *
	 */
	public void setDfPostalCode1(String value)
	{
		setValue(FIELD_DFPOSTALCODE1,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPostalCode2()
	{
		return (String)getValue(FIELD_DFPOSTALCODE2);
	}


	/**
	 *
	 *
	 */
	public void setDfPostalCode2(String value)
	{
		setValue(FIELD_DFPOSTALCODE2,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPorpertyUsage()
	{
		return (String)getValue(FIELD_DFPORPERTYUSAGE);
	}


	/**
	 *
	 *
	 */
	public void setDfPorpertyUsage(String value)
	{
		setValue(FIELD_DFPORPERTYUSAGE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfOccupancy()
	{
		return (String)getValue(FIELD_DFOCCUPANCY);
	}


	/**
	 *
	 *
	 */
	public void setDfOccupancy(String value)
	{
		setValue(FIELD_DFOCCUPANCY,value);
	}


	/**
	 *
	 *
	 */
	public String getPropertyType()
	{
		return (String)getValue(FIELD_PROPERTYTYPE);
	}


	/**
	 *
	 *
	 */
	public void setPropertyType(String value)
	{
		setValue(FIELD_PROPERTYTYPE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPurchasePrice()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public void setDfPurchasePrice(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPURCHASEPRICE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEstimatedAppraisalValue()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFESTIMATEDAPPRAISALVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfEstimatedAppraisalValue(java.math.BigDecimal value)
	{
		setValue(FIELD_DFESTIMATEDAPPRAISALVALUE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfActualAppraisalValue()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFACTUALAPPRAISALVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfActualAppraisalValue(java.math.BigDecimal value)
	{
		setValue(FIELD_DFACTUALAPPRAISALVALUE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLandValue()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLANDVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfLandValue(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLANDVALUE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStreetTypeDesc()
	{
		return (String)getValue(FIELD_DFSTREETTYPEDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfStreetTypeDesc(String value)
	{
		setValue(FIELD_DFSTREETTYPEDESC,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStreetDirectionDesc()
	{
		return (String)getValue(FIELD_DFSTREETDIRECTIONDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfStreetDirectionDesc(String value)
	{
		setValue(FIELD_DFSTREETDIRECTIONDESC,value);
	}

	  public java.math.BigDecimal getDfInstitutionId() {
		    return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
		  }
	
	  public void setDfInstitutionId(java.math.BigDecimal value) {
	    setValue(FIELD_DFINSTITUTIONID, value);
	  }
	  
	/**
	 *
	 *
	 */
	public String getDfUnitNumber()
	{
		return (String)getValue(FIELD_DFUNITNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfUnitNumber(String value)
	{
		setValue(FIELD_DFUNITNUMBER,value);
	}

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 19Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL PROPERTY.DEALID, PROPERTY.PRIMARYPROPERTYFLAG, PROPERTY.PROPERTYSTREETNUMBER, PROPERTY.PROPERTYSTREETNAME, PROPERTY.PROPERTYADDRESSLINE2, PROPERTY.PROPERTYCITY, PROVINCE.PROVINCENAME, PROPERTY.PROPERTYPOSTALFSA, PROPERTY.PROPERTYPOSTALLDU, PROPERTYUSAGE.PUDESCRIPTION, OCCUPANCYTYPE.OTDESCRIPTION, PROPERTYTYPE.PTDESCRIPTION, PROPERTY.PURCHASEPRICE, PROPERTY.ESTIMATEDAPPRAISALVALUE, PROPERTY.ACTUALAPPRAISALVALUE, PROPERTY.LANDVALUE, PROPERTY.COPYID, STREETTYPE.STDESCRIPTION, STREETDIRECTION.SDDESCRIPTION FROM PROPERTY, PROPERTYUSAGE, PROPERTYTYPE, OCCUPANCYTYPE, PROVINCE, STREETTYPE, STREETDIRECTION  __WHERE__  ";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL PROPERTY.DEALID, PROPERTY.PRIMARYPROPERTYFLAG, PROPERTY.PROPERTYSTREETNUMBER, "+
    "PROPERTY.PROPERTYSTREETNAME, PROPERTY.PROPERTYADDRESSLINE2, PROPERTY.PROPERTYCITY, "+
    "to_char(PROPERTY.PROVINCEID) PROVINCEID_STR, PROPERTY.PROPERTYPOSTALFSA, "+
    "PROPERTY.PROPERTYPOSTALLDU, to_char(PROPERTY.PROPERTYUSAGEID) PROPERTYUSAGEID_STR, "+
    "to_char(PROPERTY.OCCUPANCYTYPEID) OCCUPANCYTYPEID_STR, to_char(PROPERTY.PROPERTYTYPEID) PROPERTYTYPEID_STR, "+
    "PROPERTY.PURCHASEPRICE, PROPERTY.ESTIMATEDAPPRAISALVALUE, PROPERTY.ACTUALAPPRAISALVALUE, "+
    "PROPERTY.LANDVALUE, PROPERTY.COPYID, to_char(PROPERTY.STREETTYPEID) STREETTYPEID_STR, "+
    "to_char(PROPERTY.STREETDIRECTIONID) STREETDIRECTIONID_STR, "+
    "PROPERTY.INSTITUTIONPROFILEID, "+
    "PROPERTY.UNITNUMBER " +
    " FROM PROPERTY "+
    " __WHERE__  ";
	//--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //public static final String MODIFYING_QUERY_TABLE_NAME="PROPERTY, PROPERTYUSAGE, PROPERTYTYPE, OCCUPANCYTYPE, PROVINCE, STREETTYPE, STREETDIRECTION";
	public static final String MODIFYING_QUERY_TABLE_NAME=
    "PROPERTY";
	//--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
  //public static final String STATIC_WHERE_CRITERIA=" (((PROPERTY.PROPERTYUSAGEID  =  PROPERTYUSAGE.PROPERTYUSAGEID) AND (PROPERTY.PROPERTYTYPEID  =  PROPERTYTYPE.PROPERTYTYPEID) AND (PROPERTY.OCCUPANCYTYPEID  =  OCCUPANCYTYPE.OCCUPANCYTYPEID) AND (PROPERTY.PROVINCEID  =  PROVINCE.PROVINCEID) AND (PROPERTY.STREETTYPEID  =  STREETTYPE.STREETTYPEID) AND (PROPERTY.STREETDIRECTIONID  =  STREETDIRECTION.STREETDIRECTIONID)) AND PROPERTY.PRIMARYPROPERTYFLAG = 'Y')";
	public static final String STATIC_WHERE_CRITERIA=
    "PROPERTY.PRIMARYPROPERTYFLAG = 'Y'";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="PROPERTY.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFPRIMARYPROPERTYFLAG="PROPERTY.PRIMARYPROPERTYFLAG";
	public static final String COLUMN_DFPRIMARYPROPERTYFLAG="PRIMARYPROPERTYFLAG";
	public static final String QUALIFIED_COLUMN_DFSTREETNUMBER="PROPERTY.PROPERTYSTREETNUMBER";
	public static final String COLUMN_DFSTREETNUMBER="PROPERTYSTREETNUMBER";
	public static final String QUALIFIED_COLUMN_DFSTREETNAME="PROPERTY.PROPERTYSTREETNAME";
	public static final String COLUMN_DFSTREETNAME="PROPERTYSTREETNAME";
	public static final String QUALIFIED_COLUMN_DFADDRESSLINE2="PROPERTY.PROPERTYADDRESSLINE2";
	public static final String COLUMN_DFADDRESSLINE2="PROPERTYADDRESSLINE2";
	public static final String QUALIFIED_COLUMN_DFCITY="PROPERTY.PROPERTYCITY";
	public static final String COLUMN_DFCITY="PROPERTYCITY";
	public static final String QUALIFIED_COLUMN_DFPOSTALCODE1="PROPERTY.PROPERTYPOSTALFSA";
	public static final String COLUMN_DFPOSTALCODE1="PROPERTYPOSTALFSA";
	public static final String QUALIFIED_COLUMN_DFPOSTALCODE2="PROPERTY.PROPERTYPOSTALLDU";
	public static final String COLUMN_DFPOSTALCODE2="PROPERTYPOSTALLDU";
	public static final String QUALIFIED_COLUMN_DFPURCHASEPRICE="PROPERTY.PURCHASEPRICE";
	public static final String COLUMN_DFPURCHASEPRICE="PURCHASEPRICE";
	public static final String QUALIFIED_COLUMN_DFESTIMATEDAPPRAISALVALUE="PROPERTY.ESTIMATEDAPPRAISALVALUE";
	public static final String COLUMN_DFESTIMATEDAPPRAISALVALUE="ESTIMATEDAPPRAISALVALUE";
	public static final String QUALIFIED_COLUMN_DFACTUALAPPRAISALVALUE="PROPERTY.ACTUALAPPRAISALVALUE";
	public static final String COLUMN_DFACTUALAPPRAISALVALUE="ACTUALAPPRAISALVALUE";
	public static final String QUALIFIED_COLUMN_DFLANDVALUE="PROPERTY.LANDVALUE";
	public static final String COLUMN_DFLANDVALUE="LANDVALUE";
	public static final String QUALIFIED_COLUMN_DFCOPYID="PROPERTY.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";

	//--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
  //public static final String QUALIFIED_COLUMN_DFPROVINCE="PROVINCE.PROVINCENAME";
	//public static final String COLUMN_DFPROVINCE="PROVINCENAME";
  public static final String QUALIFIED_COLUMN_DFPROVINCE="PROPERTY.PROVINCEID_STR";
	public static final String COLUMN_DFPROVINCE="PROVINCEID_STR";
  //public static final String QUALIFIED_COLUMN_DFPORPERTYUSAGE="PROPERTYUSAGE.PUDESCRIPTION";
	//public static final String COLUMN_DFPORPERTYUSAGE="PUDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFPORPERTYUSAGE="PROPERTY.PROPERTYUSAGEID_STR";
	public static final String COLUMN_DFPORPERTYUSAGE="PROPERTYUSAGEID_STR";
  //public static final String QUALIFIED_COLUMN_DFOCCUPANCY="OCCUPANCYTYPE.OTDESCRIPTION";
	//public static final String COLUMN_DFOCCUPANCY="OTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFOCCUPANCY="PROPERTY.OCCUPANCYTYPEID_STR";
	public static final String COLUMN_DFOCCUPANCY="OCCUPANCYTYPEID_STR";
  //public static final String QUALIFIED_COLUMN_PROPERTYTYPE="PROPERTYTYPE.PTDESCRIPTION";
	//public static final String COLUMN_PROPERTYTYPE="PTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_PROPERTYTYPE="PROPERTY.PROPERTYTYPEID_STR";
	public static final String COLUMN_PROPERTYTYPE="PROPERTYTYPEID_STR";
  //public static final String QUALIFIED_COLUMN_DFSTREETTYPEDESC="STREETTYPE.STDESCRIPTION";
	//public static final String COLUMN_DFSTREETTYPEDESC="STDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFSTREETTYPEDESC="PROPERTY.STREETTYPEID_STR";
	public static final String COLUMN_DFSTREETTYPEDESC="STREETTYPEID_STR";
  //public static final String QUALIFIED_COLUMN_DFSTREETDIRECTIONDESC="STREETDIRECTION.SDDESCRIPTION";
	//public static final String COLUMN_DFSTREETDIRECTIONDESC="SDDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFSTREETDIRECTIONDESC="PROPERTY.STREETDIRECTIONID_STR";
	public static final String COLUMN_DFSTREETDIRECTIONDESC="STREETDIRECTIONID_STR";

  public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
	    "PROPERTY.INSTITUTIONPROFILEID";
  public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
  
  public static final String QUALIFIED_COLUMN_DFUNITNUMBER="PROPERTY.UNITNUMBER";
  public static final String COLUMN_DFUNITNUMBER="UNITNUMBER";
  
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIMARYPROPERTYFLAG,
				COLUMN_DFPRIMARYPROPERTYFLAG,
				QUALIFIED_COLUMN_DFPRIMARYPROPERTYFLAG,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTREETNUMBER,
				COLUMN_DFSTREETNUMBER,
				QUALIFIED_COLUMN_DFSTREETNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTREETNAME,
				COLUMN_DFSTREETNAME,
				QUALIFIED_COLUMN_DFSTREETNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRESSLINE2,
				COLUMN_DFADDRESSLINE2,
				QUALIFIED_COLUMN_DFADDRESSLINE2,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCITY,
				COLUMN_DFCITY,
				QUALIFIED_COLUMN_DFCITY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROVINCE,
				COLUMN_DFPROVINCE,
				QUALIFIED_COLUMN_DFPROVINCE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPOSTALCODE1,
				COLUMN_DFPOSTALCODE1,
				QUALIFIED_COLUMN_DFPOSTALCODE1,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPOSTALCODE2,
				COLUMN_DFPOSTALCODE2,
				QUALIFIED_COLUMN_DFPOSTALCODE2,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPORPERTYUSAGE,
				COLUMN_DFPORPERTYUSAGE,
				QUALIFIED_COLUMN_DFPORPERTYUSAGE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFOCCUPANCY,
				COLUMN_DFOCCUPANCY,
				QUALIFIED_COLUMN_DFOCCUPANCY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_PROPERTYTYPE,
				COLUMN_PROPERTYTYPE,
				QUALIFIED_COLUMN_PROPERTYTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPURCHASEPRICE,
				COLUMN_DFPURCHASEPRICE,
				QUALIFIED_COLUMN_DFPURCHASEPRICE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFESTIMATEDAPPRAISALVALUE,
				COLUMN_DFESTIMATEDAPPRAISALVALUE,
				QUALIFIED_COLUMN_DFESTIMATEDAPPRAISALVALUE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFACTUALAPPRAISALVALUE,
				COLUMN_DFACTUALAPPRAISALVALUE,
				QUALIFIED_COLUMN_DFACTUALAPPRAISALVALUE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLANDVALUE,
				COLUMN_DFLANDVALUE,
				QUALIFIED_COLUMN_DFLANDVALUE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTREETTYPEDESC,
				COLUMN_DFSTREETTYPEDESC,
				QUALIFIED_COLUMN_DFSTREETTYPEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTREETDIRECTIONDESC,
				COLUMN_DFSTREETDIRECTIONDESC,
				QUALIFIED_COLUMN_DFSTREETDIRECTIONDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
	    FIELD_SCHEMA.addFieldDescriptor(
	            new QueryFieldDescriptor(
	              FIELD_DFINSTITUTIONID,
	              COLUMN_DFINSTITUTIONID,
	              QUALIFIED_COLUMN_DFINSTITUTIONID,
	              java.math.BigDecimal.class,
	              false,
	              false,
	              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	              "",
	              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	              ""));
	    
	    FIELD_SCHEMA.addFieldDescriptor(
	            new QueryFieldDescriptor(
	              FIELD_DFUNITNUMBER,
	              COLUMN_DFUNITNUMBER,
	              QUALIFIED_COLUMN_DFUNITNUMBER,
	              String.class,
	              false,
	              false,
	              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	              "",
	              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	              ""));
	        
	}

}

