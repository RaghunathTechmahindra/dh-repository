package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgDealSummaryRepeatedGDSTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealSummaryRepeatedGDSTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(100);
		setPrimaryModelClass( doDetailViewGDSModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStApplicantLastNameGDS().setValue(CHILD_STAPPLICANTLASTNAMEGDS_RESET_VALUE);
		getStApplicantTypeGDS().setValue(CHILD_STAPPLICANTTYPEGDS_RESET_VALUE);
		getStGDS().setValue(CHILD_STGDS_RESET_VALUE);
		getSt3YrGDS().setValue(CHILD_ST3YRGDS_RESET_VALUE);
		getStApplicantFirstNameGDS().setValue(CHILD_STAPPLICANTFIRSTNAMEGDS_RESET_VALUE);
		getStApplicantInitialGDS().setValue(CHILD_STAPPLICANTINITIALGDS_RESET_VALUE);
		getStApplicantSuffixGDS().setValue(CHILD_STAPPLICANTSUFFIXGDS_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STAPPLICANTLASTNAMEGDS,StaticTextField.class);
		registerChild(CHILD_STAPPLICANTTYPEGDS,StaticTextField.class);
		registerChild(CHILD_STGDS,StaticTextField.class);
		registerChild(CHILD_ST3YRGDS,StaticTextField.class);
		registerChild(CHILD_STAPPLICANTFIRSTNAMEGDS,StaticTextField.class);
		registerChild(CHILD_STAPPLICANTINITIALGDS,StaticTextField.class);
		registerChild(CHILD_STAPPLICANTSUFFIXGDS,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDetailViewGDSModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		// The following code block was migrated from the RepeatedGDS_onBeforeDisplayEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.setupNumOfRepeatedGDS(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STAPPLICANTLASTNAMEGDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewGDSModel(),
				CHILD_STAPPLICANTLASTNAMEGDS,
				doDetailViewGDSModel.FIELD_DFLASTNAME,
				CHILD_STAPPLICANTLASTNAMEGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPLICANTTYPEGDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewGDSModel(),
				CHILD_STAPPLICANTTYPEGDS,
				doDetailViewGDSModel.FIELD_DFAPPLICANTTYPE,
				CHILD_STAPPLICANTTYPEGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STGDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewGDSModel(),
				CHILD_STGDS,
				doDetailViewGDSModel.FIELD_DFGDS,
				CHILD_STGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_ST3YRGDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewGDSModel(),
				CHILD_ST3YRGDS,
				doDetailViewGDSModel.FIELD_DFGDS3YR,
				CHILD_ST3YRGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPLICANTFIRSTNAMEGDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewGDSModel(),
				CHILD_STAPPLICANTFIRSTNAMEGDS,
				doDetailViewGDSModel.FIELD_DFFIRSTNAME,
				CHILD_STAPPLICANTFIRSTNAMEGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPLICANTINITIALGDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewGDSModel(),
				CHILD_STAPPLICANTINITIALGDS,
				doDetailViewGDSModel.FIELD_DFMIDDLENAME,
				CHILD_STAPPLICANTINITIALGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPLICANTSUFFIXGDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewGDSModel(),
				CHILD_STAPPLICANTSUFFIXGDS,
				doDetailViewGDSModel.FIELD_DFSUFFIX,
				CHILD_STAPPLICANTSUFFIXGDS_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApplicantLastNameGDS()
	{
		return (StaticTextField)getChild(CHILD_STAPPLICANTLASTNAMEGDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApplicantTypeGDS()
	{
		return (StaticTextField)getChild(CHILD_STAPPLICANTTYPEGDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStGDS()
	{
		return (StaticTextField)getChild(CHILD_STGDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getSt3YrGDS()
	{
		return (StaticTextField)getChild(CHILD_ST3YRGDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApplicantFirstNameGDS()
	{
		return (StaticTextField)getChild(CHILD_STAPPLICANTFIRSTNAMEGDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApplicantInitialGDS()
	{
		return (StaticTextField)getChild(CHILD_STAPPLICANTINITIALGDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApplicantSuffixGDS()
	{
		return (StaticTextField)getChild(CHILD_STAPPLICANTSUFFIXGDS);
	}

	/**
	 *
	 *
	 */
	public doDetailViewGDSModel getdoDetailViewGDSModel()
	{
		if (doDetailViewGDS == null)
			doDetailViewGDS = (doDetailViewGDSModel) getModel(doDetailViewGDSModel.class);
		return doDetailViewGDS;
	}


	/**
	 *
	 *
	 */
	public void setdoDetailViewGDSModel(doDetailViewGDSModel model)
	{
			doDetailViewGDS = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STAPPLICANTLASTNAMEGDS="stApplicantLastNameGDS";
	public static final String CHILD_STAPPLICANTLASTNAMEGDS_RESET_VALUE="";
	public static final String CHILD_STAPPLICANTTYPEGDS="stApplicantTypeGDS";
	public static final String CHILD_STAPPLICANTTYPEGDS_RESET_VALUE="";
	public static final String CHILD_STGDS="stGDS";
	public static final String CHILD_STGDS_RESET_VALUE="";
	public static final String CHILD_ST3YRGDS="st3YrGDS";
	public static final String CHILD_ST3YRGDS_RESET_VALUE="";
	public static final String CHILD_STAPPLICANTFIRSTNAMEGDS="stApplicantFirstNameGDS";
	public static final String CHILD_STAPPLICANTFIRSTNAMEGDS_RESET_VALUE="";
	public static final String CHILD_STAPPLICANTINITIALGDS="stApplicantInitialGDS";
	public static final String CHILD_STAPPLICANTINITIALGDS_RESET_VALUE="";
	public static final String CHILD_STAPPLICANTSUFFIXGDS="stApplicantSuffixGDS";
	public static final String CHILD_STAPPLICANTSUFFIXGDS_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDetailViewGDSModel doDetailViewGDS=null;
  private DealSummaryHandler handler=new DealSummaryHandler();

}

