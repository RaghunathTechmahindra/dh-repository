package mosApp.MosSystem;

/**
 * 11/Oct/2007 DVG #DG638 LEN217180: Malcolm Whitton Condition Text exceeds 4000 characters
 * 22/Aug/2006 DVG #DG490 #4421  NBCQA2 Express Dev - 'Illegal characters on Condition screen
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.PartyProfile;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.entity.SourceOfBusinessProfile;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.entity.DocumentTracking.DocTrackingVerbiage;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.SourceOfBusinessProfilePK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.util.DBA;
import com.basis100.deal.util.TypeConverter;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.util.StringTokenizer2;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.HtmlDisplayFieldBase;
import com.iplanet.jato.view.html.TextField;

import config.DocTrackingDetailsConfigManager;

import MosSystem.*;



/**
 *
 *
 */
public class DocTrackDetailHandler extends PageHandlerCommon
	implements Cloneable, Sc
{

	// 1) pageCondition7. Used during validation of Standard/Custom Condition input
	//
	//	  Value - true : Force to produce the commitment letter for the condition changed
  //                   in the DocTrackingDetails screen.
  //          - false : Default status, regular page flow.
	//
  //--Release2.1--TD_DTS_CR--end//

  private static final String CONFIGMGR = "CONFIGMGR";

  //--TD_CONDR_CR--start--//
  private static final int COMMITMENT_OFFER = 19;
  private static final int CONDITION_DETAIL = 519;
  //--TD_CONDR_CR--end--//

	/**
	 *
	 *
	 */
	public DocTrackDetailHandler cloneSS()
	{
		return(DocTrackDetailHandler) super.cloneSafeShallow();

	}


	//This method is used to populate the fields in the header
	//with the appropriate information

	public void populatePageDisplayFields()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		populatePageShellDisplayFields();
		

		// Quick Link Menu display
		displayQuickLinkMenu();
		
    //--TD_CONDR_CR--start--//
    //// Override framework method (DocTrackingDetail Page Label) if user access from the Condition
    //// Review screen.
    int parentId = pg.getParentPage().getPageId();
    //logger.debug("DTDH@PopulatePageDisplayFields:PageId: " + pg.getParentPage().getPageId());
    //logger.debug("DTDH@PopulatePageDisplayFields:PageName: " + pg.getParentPage().getPageName());
    
    //// Parent Page is CommitmentOffer (id = 19) override the label of the Condition Detail
    //// with pageId = 504: Document Tracking Detail.
    if(parentId == COMMITMENT_OFFER)
            overridePageLabel(CONDITION_DETAIL);
    //--TD_CONDR_CR--end--//

		populateTaskNavigator(pg);
		populatePreviousPagesLinks();
		populateDetails();

    //--Release2.1--TD_DTS_CR--//
    //// Customization of the access to the new modifiable fields.
    //// IMPORTANT!! In order to disable fields based on the user's settings
    //// this call must be done AFTER the JS validation call.
		populateFieldValidation(getCurrNDPage());
    setupDisabledFields(getCurrNDPage());
	}


	//
	// Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
	//

	public void setupBeforePageGeneration()
	{
		PageEntry pg = theSessionState.getCurrentPage();
    Hashtable pst = pg.getPageStateTable();

		if (pg.getSetupBeforeGenerationCalled() == true) return;

		pg.setSetupBeforeGenerationCalled(true);

		pg.setPageCondition3(false);

    //--Release2.1--TD_DTS_CR--start//
    //// Take from the Product Team and finish the conditions when the page is editable.
    pg.setEditable(true);

    //// Defauld is not include into the DocTracking letter.
    pg.setPageCondition7(false);

    //// Customize the access to the new editable fields.
    pst.put(CONFIGMGR,
        DocTrackingDetailsConfigManager.getInstance(logger, theSessionState.getSessionUserType()));

    //--Release2.1--TD_DTS_CR--end//

    // SEAN Ticket #2621 March 13, 2006: set up the deal id for the doDealNotesInfoModel.
    String dealIdStr = "" + pg.getPageDealId();
    doDealNotesInfoModelImpl theDO =(doDealNotesInfoModelImpl)
      (RequestManager.getRequestContext().getModelManager().getModel(doDealNotesInfoModel.class));

    theDO.clearUserWhereCriteria();

    theDO.addUserWhereCriterion("dfDealID", "=", dealIdStr);
    // SEAN Ticket #2621 END

		int numRows = 0;

		try
		{
			ViewBean currNDPage = getCurrNDPage();
			PageCursorInfo tds = getPageCursorInfo(pg, "Repeated1");
			int copyId = pg.getPageDealCID();

			int dfRoleId = (new Integer(pst.get("dfRoleId").toString())).intValue();

			/*if (dfRoleId == Sc.CRR_PARTY_TYPE_APPRAISER)
      //!!!!!!!!!    CRR_PARTY_TYPE_APPRAISER
			{
				PartyProfile pp = new PartyProfile(srk);
				ArrayList appraiserList =(ArrayList) pp
                    .findByDealAndType(new DealPK(pg.getPageDealId(), 
                         copyId), Mc.PARTY_TYPE_APPRAISER);

				//!!!!!!!!! different const PARTY_TYPE_APPRAISER
				numRows = appraiserList.size();
				logger.trace("DEFAULT: NUMROWS= " + numRows);
				if (tds == null)
				{
					String voName = "Repeated1";
					tds = new PageCursorInfo("Repeated1", voName, numRows, numRows);
					tds.setRefresh(true);
					pst.put(tds.getName(), tds);
				}
				else
				{
					tds.setTotalRows(numRows);
					tds.setRowsPerPage(numRows);
				}
				setRowGeneratorDOForNRows(numRows);
				pg.setPageCondition3(true);
			}*/
		}
		catch(Exception ex)
		{
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Document Tracking Details . Problem encountered during Data Object running ");
			logger.error(ex.getMessage());
		}

	}


	/**
	 *
	 *
	 */
	public void populateDetails()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		SessionResourceKit srk = getSessionResourceKit();


		//	pst values setup by caller (Document Tracking)
		try
		{
			int copyId = pg.getPageDealCID();
			Borrower borrower = new Borrower(srk, null);
			borrower.findByPrimaryBorrower(pg.getPageDealId(), copyId);
			getCurrNDPage().setDisplayFieldValue("stPrimaryBorrower", getFormattedName(borrower.getBorrowerFirstName(), 
			                                                                           borrower.getBorrowerMiddleInitial(), 
			                                                                           borrower.getBorrowerLastName()));
			getCurrNDPage().setDisplayFieldValue("stRole", pst.get("dfRoleStr"));
			getCurrNDPage().setDisplayFieldValue("stDocumentLabel", pst.get("dfDocLabel"));
			getCurrNDPage().setDisplayFieldValue("stDealId", new Integer(pg.getPageDealId()));
			getCurrNDPage().setDisplayFieldValue("stRequestDate", pst.get("dfRequestDate"));
			Object o = pst.get("dfRequestDate");
			if (o != null)
			    getCurrNDPage().setDisplayFieldValue("stDocStatusDate", pst.get("dfStatusDate"));
			else 
			    getCurrNDPage().setDisplayFieldValue("stDocStatusDate", "");
			getCurrNDPage().setDisplayFieldValue("stDocStatus", pst.get("dfDocStatus"));

      //--Release2.1--TD_DTS_CR--start//
			////getCurrNDPage().setDisplayFieldValue("stDocText", new String((String) pst.get("dfDocText")));
      TextField englDocText =(TextField) getCurrNDPage().getDisplayField("txEngDocText");



        String engText = (String) pst.get("dfDocText");
        String frenchText = "";
        Integer docTrackId = new Integer(0);
		try {
			docTrackId = ((Integer)pst.get("dfDocTrackId"));
			DocumentTracking freeForm = new DocumentTracking(srk, docTrackId.intValue(), copyId);
			engText = TypeConverter.stringTypeFrom(freeForm.getDocumentTextByLanguage(LANGUAGE_PREFERENCE_ENGLISH));
			logger.debug("DTDH@populateDetails::EngText: " + engText);
			englDocText.setValue(engText);
			frenchText = TypeConverter.stringTypeFrom(freeForm.getDocumentTextByLanguage(LANGUAGE_PREFERENCE_FRENCH));
			logger.debug("DTDH@populateDetails::FrenchText: " + frenchText);
			englDocText.setValue(engText);
		} catch (Exception e) {
			logger.debug("DTDH@populateDetails::Exception: " + e);
		}

			//--Release2.1--TD_DTS_CR--start//
      ////getCurrNDPage().setDisplayFieldValue("stFrenchText", frenchText);
      pst.put("ORIGFRENCHTEXT", frenchText);
      //LEN401576 fix. Get original english text from db instead from hidden filed 
      pst.put("ORIGENGLISHTEXT", engText);
      
      logger.debug("DTDH@populateDetails::hdDocTrackId: " + docTrackId);

      ((TextField)getCurrNDPage().getDisplayField("txFrenchDocText")).setValue(frenchText);
      ((HiddenField)getCurrNDPage().getDisplayField("hdDocTrackId")).setValue(docTrackId);
			//--Release2.1--TD_DTS_CR--end//

			int roleId = (new Integer(pst.get("dfRoleId").toString())).intValue();

			switch(roleId)
			{
				// SEAN Ticket #2497 Dec 09, 2005:
				// delete CRR_USER_TYPE_JR_UNDERWRITER and CRR_USER_TYPE_SR_UNDERWRITER, beause we only have
				// CRR_USER_TYPE_UNDERWRITER in database
                case Sc.CRR_USER_TYPE_UNDERWRITER :
				{
					roleId = Sc.CRR_USER_TYPE_UNDERWRITER;
					Deal deal = DBA.getDeal(srk, pg.getPageDealId(), copyId);
					int responsibleId = deal.getUnderwriterUserId();
					UserProfile upf = new UserProfile(srk);
					Contact contact = null;
					try
					{
						upf.findByPrimaryKey(new UserProfileBeanPK(responsibleId, theSessionState.getDealInstitutionId()));
						contact = upf.getContact();
					}
					catch(Exception e)
					{
					}
					getContactData(contact);
					break;
				}
				case Sc.CRR_USER_TYPE_APPLICANT :
				{
					Borrower borr = new Borrower(srk, null);
					try
					{
						borr = borr.findByPrimaryBorrower(pg.getPageDealId(), copyId);
					}
					catch(Exception e)
					{
					}
					logger.trace("getting Borrower Contact Data; roleId=" + roleId);
					getBorrowerContactData(borr);

					//date are taken from borrower table, not from contact
					break;
				}
				case Sc.CRR_PARTY_TYPE_SOLICITOR :
				{
					PartyProfile pp = new PartyProfile(srk);
					Contact contact = null;
					//String businessId = null;
					String partyType = null;
					//String partyName = null;
					try
					{
						//Start of FXP26404
						pp.setSilentMode(true);
						//End of FXP26404
						pp = pp.findByDealSolicitor(pg.getPageDealId());
						contact = new Contact(srk, pp.getContactId(), 1);
					}
					catch(Exception e)
					{
					}
					getContactData(contact);
					setBusinessId(pp.getPtPBusinessId());
					setContactCompanyName(pp.getPartyCompanyName()); // Ticket 302
					try
					{
						partyType = getPartyType(pp.getPartyTypeId());
					}
					catch(Exception e)
					{
						logger.error(e);
					}
					setPartyType(partyType);
					break;
				}
				case Sc.CRR_PARTY_TYPE_APPRAISER :
        //Appraisers: comment out
				{
					//fetch all appraiser parties associated with deal
					PartyProfile pp = new PartyProfile(srk);
					Collection<PartyProfile> appraisers = pp.findByDealAndType(pg.getPageDealId(), Mc.PARTY_TYPE_APPRAISER);
					
					//don't do anything is there isn't an appraiser assigned.
					//ideally there should be only one
					if (appraisers.iterator().hasNext()) 
					{
						pp = appraisers.iterator().next();
						Contact contact = pp.getContact();
						getContactData(contact);
						setBusinessId(pp.getPtPBusinessId());
						setContactCompanyName(pp.getPartyCompanyName());
						setPartyType(getPartyType(pp.getPartyTypeId()));
					}
					else
					{
						getCurrNDPage().setDisplayFieldValue("stCommentOpen", new String("<!--"));
						getCurrNDPage().setDisplayFieldValue("stCommentClose", new String("-->"));
					}
					break;
				}
				case Sc.CRR_SOURCE_OF_BUSINESS :
				{
					Deal deal = DBA.getDeal(srk, pg.getPageDealId(), copyId);
					SourceOfBusinessProfile sbp = new SourceOfBusinessProfile(srk);
					Contact contact = null;
					SourceFirmProfile sfp = new SourceFirmProfile(srk);
					String businessId = null;
					String contactCompanyName = null;
					try
					{
						int responsibleId = deal.getSourceOfBusinessProfileId();
						sbp = new SourceOfBusinessProfile(srk, responsibleId);
						contact = sbp.getContact();
						businessId = sbp.getSOBPBusinessId();
						Collection sourceFirmProfiles = sfp.findByAssociatedSourceOfBusiness((SourceOfBusinessProfilePK) sbp.getPk());
						if (sourceFirmProfiles.iterator().hasNext())
						{
							contactCompanyName =((SourceFirmProfile)(sourceFirmProfiles.iterator().next())).getSfShortName();
						}
					}
					catch(Exception e)
					{
						// problem getting source information
					}
					getContactData(contact);
					setBusinessId(businessId);
					setContactCompanyName(contactCompanyName);
					break;
				}
			}
		}
		catch(Exception e)
		{
			logger.error("Exception @DocTrackingDetails.populateDetails()");
			logger.error(e);
		}

	}


	/**
	 *
	 *
	 */
	public void populateAppraiser(int rowNdx)
	{
		PageEntry pg = theSessionState.getCurrentPage();

		if (pg.getPageCondition3() == false) return;

		logger.trace("populateAppraiser");

		//Hashtable pst = pg.getPageStateTable();

		SessionResourceKit srk = getSessionResourceKit();

		int copyId = pg.getPageDealCID();

		try
		{
			PartyProfile pp1 = new PartyProfile(srk);
			ArrayList appraiserList =(ArrayList) pp1.findByDealAndType(new DealPK(pg.getPageDealId(),
                                                                 copyId), Mc.PARTY_TYPE_APPRAISER);

			//!!!!!!!!! different const PARTY_TYPE_APPRAISER
			PartyProfile pp =(PartyProfile) appraiserList.get(rowNdx);
			int propertyId = pp.getPropertyId();
			Property prop = null;
			Contact contact = null;
			String businessId = null;
			String partyType = null;
			String partyName = null;
			try
			{
				prop = new Property(srk, null, propertyId, copyId);
			}
			catch(Exception e)
			{
			}

			// Streetnumber, Streetname,StreetType, City
			if (prop != null)
			{
				getCurrNDPage().setDisplayFieldValue("Repeated1/stPropertyAddress", new String(getAddressLineFormatted(prop.getPropertyStreetNumber()) + " " + getAddressLineFormatted(prop.getPropertyStreetName()) + " " + getAddressLineFormatted(prop.getStreetTypeDescription()) + " " + getAddressLineFormatted(prop.getPropertyCity())));
			}
			try
			{
				contact = new Contact(srk, pp.getContactId(), 1);
			}
			catch(Exception e)
			{
			}
			getAppraiserContactData(contact);
			businessId = pp.getPtPBusinessId();
			partyType = getPartyType(pp.getPartyTypeId());
			partyName = pp.getPartyName();
			getCurrNDPage().setDisplayFieldValue("Repeated1/stApprBusinessId", new String(getBusinessIdFormatted(businessId)));
			getCurrNDPage().setDisplayFieldValue("Repeated1/stApprPartyType", new String(getPartyTypeFormatted(partyType)));
			getCurrNDPage().setDisplayFieldValue("Repeated1/stApprContactCompanyName", new String(getCompanyNameFormatted(partyName)));
			logger.trace("DocumentTracking@populateAppraiser ENDS");
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("DocumentTrack Details. populateAppraiser() Exception");
			logger.error(e.getMessage());
		}

	}


	//he's just a guy: no partytype, businessid and no company name

	private void getBorrowerContactData(Borrower borrower)
		throws Exception
	{
		////logger.trace("Inside getBorrowerContactData");

		getCurrNDPage().setDisplayFieldValue("stContactName", new String(getFormattedName(borrower.getBorrowerFirstName(), borrower.getBorrowerMiddleInitial(), borrower.getBorrowerLastName())));

		getCurrNDPage().setDisplayFieldValue("stPhone", new String(getPhoneNumberFormatted(borrower.getBorrowerWorkPhoneNumber(), "" + borrower.getBorrowerWorkPhoneExtension())));

		getCurrNDPage().setDisplayFieldValue("stFax", new String(getFaxNumberFormatted(borrower.getBorrowerFaxNumber())));

		getCurrNDPage().setDisplayFieldValue("stEmail", new String(getEmailFormatted(borrower.getBorrowerEmailAddress())));

		ArrayList addrs =(ArrayList) borrower.getBorrowerAddresses();

		if (addrs.size() > 0)
		{
			getCurrNDPage().setDisplayFieldValue("stAddrLine1", new String(getAddressLineFormatted(((BorrowerAddress) addrs.get(0)).getAddr().getAddressLine1())));
			getCurrNDPage().setDisplayFieldValue("stAddrLine2", new String(getAddressLineFormatted(((BorrowerAddress) addrs.get(0)).getAddr().getAddressLine2())));
		}

		if (borrower == null)
		{
			setBusinessId(null);
		}
		else
		{
			setBusinessId(borrower.getBorrowerNumber());
		}

	}


	/**
	 *
	 *
	 */
	private void getAppraiserContactData(Contact contact)
		throws Exception
	{
		////logger.trace("getAppraiserContactData");

		if (contact == null)
		{
			return;
		}

		getCurrNDPage().setDisplayFieldValue("Repeated1/stApprContactName", new String(getFormattedName(contact.getContactFirstName(), contact.getContactMiddleInitial(), contact.getContactLastName())));

		getCurrNDPage().setDisplayFieldValue("Repeated1/stApprPhone", new String(getPhoneNumberFormatted(contact.getContactPhoneNumber(), contact.getContactPhoneNumberExtension())));

		getCurrNDPage().setDisplayFieldValue("Repeated1/stApprFax", new String(getFaxNumberFormatted(contact.getContactFaxNumber())));

		getCurrNDPage().setDisplayFieldValue("Repeated1/stApprEmail", new String(getEmailFormatted(contact.getContactEmailAddress())));

		Addr addr = contact.getAddr();

		getCurrNDPage().setDisplayFieldValue("Repeated1/stApprAddrLine1", new String(getAddressLineFormatted(addr.getAddressLine1())));

		getCurrNDPage().setDisplayFieldValue("Repeated1/stApprAddrLine2", new String(getAddressLineFormatted(addr.getAddressLine2())));

	}


	/**
	 *
	 *
	 */
	private void getContactData(Contact contact)
		throws Exception
	{
		logger.trace("getContactData");

		if (contact == null)
		{
			return;
		}

		getCurrNDPage().setDisplayFieldValue("stContactName", new String(getFormattedName(contact.getContactFirstName(), contact.getContactMiddleInitial(), contact.getContactLastName())));

		getCurrNDPage().setDisplayFieldValue("stPhone", new String(getPhoneNumberFormatted(contact.getContactPhoneNumber(), contact.getContactPhoneNumberExtension())));

		getCurrNDPage().setDisplayFieldValue("stFax", new String(getFaxNumberFormatted(contact.getContactFaxNumber())));

		getCurrNDPage().setDisplayFieldValue("stEmail", new String(getEmailFormatted(contact.getContactEmailAddress())));

		Addr addr = contact.getAddr();

		if (addr == null)
		{
			return;
		}

		getCurrNDPage().setDisplayFieldValue("stAddrLine1", new String(getAddressLineFormatted(addr.getAddressLine1())));

		getCurrNDPage().setDisplayFieldValue("stAddrLine2", new String(getAddressLineFormatted(addr.getAddressLine2())));

	}
	private final static String businessIdPrefix="<font size=2 color=3366cc>&nbsp;&nbsp;<b>Business ID:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><br><font size=2 >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	private final static String partyTypePrefix="<font size=2 color=3366cc>&nbsp;&nbsp;<b>Party Type:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><br><font size=2 >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	private final static String contactCompanyNamePrefix="<font size=2 color=3366cc>&nbsp;&nbsp;<b>Company Name:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><br><font size=2 >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    // SEAN Ticket #1817 July 28, 2005: define the HTML tags.
    private final static String PREFIX_BEGIN = "<font size=3 color=3366cc>&nbsp;&nbsp;<b>";
    private final static String PREFIX_END = ":</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><br><font size=3 >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    // SEAN Ticket #1817 END
	private final static String suffix="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>";


	/**
	 *
	 *
	 */
	private void setBusinessId(int businessId)
	{
		logger.trace("setBusinessId(int)");

		getCurrNDPage().setDisplayFieldValue("stBusinessIdPrefix", new String(businessIdPrefix));
        // SEAN Ticket #1817 July 28, 2005: Change to get the label from BXResources.BXGeneric
        String prefix = PREFIX_BEGIN +
            BXResources.getGenericMsg("BUSINESS_ID_LABEL", theSessionState.getLanguageId()) + PREFIX_END;
        getCurrNDPage().setDisplayFieldValue("stBusinessIdPrefix", new String(prefix));
        // SEAN Ticket #1817 END

		getCurrNDPage().setDisplayFieldValue("stBusinessId", new Integer(businessId));

		getCurrNDPage().setDisplayFieldValue("stBusinessIdSuffix", new String(suffix));

	}


	/**
	 *
	 *
	 */
	private void setBusinessId(String businessId)
	{
		////logger.trace("setBusinessId(String)");

		getCurrNDPage().setDisplayFieldValue("stBusinessIdPrefix", new String(businessIdPrefix));
        // SEAN Ticket #1817 July 28, 2005: Change to get the label from BXResources.BXGeneric
        String prefix = PREFIX_BEGIN +
            BXResources.getGenericMsg("BUSINESS_ID_LABEL", theSessionState.getLanguageId()) + PREFIX_END;
        getCurrNDPage().setDisplayFieldValue("stBusinessIdPrefix", new String(prefix));
        // SEAN Ticket #1817 END

		getCurrNDPage().setDisplayFieldValue("stBusinessId", new String(getBusinessIdFormatted(businessId)));

		getCurrNDPage().setDisplayFieldValue("stBusinessIdSuffix", new String(suffix));

	}


	/**
	 *
	 *
	 */
	private void setPartyType(String partyType)
	{
		////logger.trace("setPartyType(String)");

		getCurrNDPage().setDisplayFieldValue("stPartyTypePrefix", new String(partyTypePrefix));
        // SEAN Ticket #1817 July 28, 2005: Change to get the label from BXResources.BXGeneric
        String prefix = PREFIX_BEGIN +
            BXResources.getGenericMsg("PARTY_TYPE_LABEL", theSessionState.getLanguageId()) + PREFIX_END;
        getCurrNDPage().setDisplayFieldValue("stPartyTypePrefix", new String(prefix));
        // SEAN Ticket #1817 END

		getCurrNDPage().setDisplayFieldValue("stPartyType", new String(getPartyTypeFormatted(partyType)));

		getCurrNDPage().setDisplayFieldValue("stPartyTypeSuffix", new String(suffix));

	}


	/**
	 *
	 *
	 */
	private void setContactCompanyName(String contactCompanyName)
	{
		////logger.trace("setContactCompanyName(String)");

		getCurrNDPage().setDisplayFieldValue("stContactCompanyNamePrefix", new String(contactCompanyNamePrefix));
        // SEAN Ticket #1817 July 28, 2005: Change to get the label from BXResources.BXGeneric
        String prefix = PREFIX_BEGIN +
            BXResources.getGenericMsg("COMPANY_NAME_LABEL", theSessionState.getLanguageId()) + PREFIX_END;
        getCurrNDPage().setDisplayFieldValue("stContactCompanyNamePrefix", new String(prefix));
        // SEAN Ticket #1817 END

		getCurrNDPage().setDisplayFieldValue("stContactCompanyName", new String(getCompanyNameFormatted(contactCompanyName)));

		getCurrNDPage().setDisplayFieldValue("stContactCompanyNameSuffix", new String(suffix));

	}


	/**
	 *
	 *
	 */
	private String getFormattedName(String firstName, String middleInitial, String lastName)
	{
		String result = " ";

		if (firstName != null && firstName.length() > 0 && ! firstName.toLowerCase().equals("null"))
		{
			result = firstName;
		}

		if (middleInitial != null && middleInitial.length() > 0 && ! middleInitial.toLowerCase().equals("null"))
		{
			result += " " + middleInitial;
		}

		if (lastName != null && lastName.length() > 0 && ! lastName.toLowerCase().equals("null"))
		{
			result += " " + lastName;
		}

		return result;

	}


	/**
	 *
	 *
	 */
	private String getPhoneNumberFormatted(String phone, String ext)
	{
		////logger.trace("Inside PhoneNumberFormatted");

		String areaCode = " ";

		String localCode = " ";

		String extStr = " ";

		if (phone == null) return " ";

		if (ext != null && ext.length() > 0 && ! ext.toLowerCase().equals("null"))
		{
			extStr = "x" + ext;
		}

		int pl = phone.length();

		if (pl >= 3)
		{
			areaCode = "(" + phone.substring(0, 3) + ") ";
		}
		else
		{
			return phone + extStr;
		}

		if (pl >= 10)
		{
			localCode = phone.substring(3, 6) + "-" + phone.substring(6, 10);
		}
		else
		{
			return areaCode + phone.substring(3) + extStr;
		}

		return areaCode + localCode + extStr;

	}


	/**
	 *
	 *
	 */
	private String getFaxNumberFormatted(String fax)
	{
		String areaCode = null;

		String localCode = null;

		////logger.trace("Inside FaxNumberFormatted");

		if (fax == null || fax.length() == 0 || fax.toLowerCase().equals("null")) return " ";

		int pl = fax.length();

		if (pl >= 3)
		{
			areaCode = "(" + fax.substring(0, 3) + ") ";
		}
		else
		{
			return fax;
		}

		if (pl >= 10)
		{
			localCode = fax.substring(3, 6) + "-" + fax.substring(6, 10);
		}
		else
		{
			return areaCode + fax.substring(3);
		}

		return areaCode + localCode;

	}


	/**
	 *
	 *
	 */
	private String getEmailFormatted(String email)
	{
		if (email == null || email.length() == 0 || email.toLowerCase().equals("null")) return " ";
		else return email;

	}


	/**
	 *
	 *
	 */
	private String getBusinessIdFormatted(String businessId)
	{
		if (businessId == null || businessId.length() == 0 || businessId.toLowerCase().equals("null")) return " ";
		else
		{
			return businessId;
		}

	}


	/**
	 *
	 *
	 */
	private String getPartyTypeFormatted(String partyType)
	{
		if (partyType == null || partyType.length() == 0 || partyType.toLowerCase().equals("null")) return " ";
		else return partyType;

	}


	/**
	 *
	 *
	 */
	private String getCompanyNameFormatted(String companyName)
	{
		if (companyName == null || companyName.length() == 0 || companyName.toLowerCase().equals("null")) return " ";
		else return companyName;

	}


	/**
	 *
	 *
	 */
	private String getAddressLineFormatted(String addressLine)
	{
		if (addressLine == null || addressLine.length() == 0 || addressLine.toLowerCase().equals("null")) return " ";
		else return addressLine;

	}


	/**
	 *
	 *
	 */
	////public int checkDisplayThisRow(String cursorName)
	public boolean checkDisplayThisRow(String cursorName)
	{
		try
		{
			PageCursorInfo tds = getPageCursorInfo(theSessionState.getCurrentPage(), cursorName);
			if (tds != null && tds.getTotalRows() > 0)
          return true;
		}
		catch(Exception e)
		{
			logger.error("Exception @docTrackDetailHandler.checkDisplayThisRow");
			logger.error(e);
		}

		return false;

	}


	/**
	 *
	 *
	 */
	public void handleSubmit()
	{
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
    Vector docTrackingIds = theSessionState.getDocTrackingIds();

		int copyId = pg.getPageDealCID();
		int docTrackId = new Integer(getCurrNDPage().getDisplayFieldValue("hdDocTrackId").toString()).intValue();

    logger.debug("DTDH@CopyId: " + copyId);
    logger.debug("DTDH@DocTrackingId: " + docTrackId);

		// SEAN Ticket #2469 Dec 13, 2005: if English and French doc text fields
		// (the condition content area) are disabled, just return as cancel.
		Boolean disabled = pst.get("conditionDisabled") == null ? new Boolean(false) :
			(Boolean) pst.get("conditionDisabled");
		if (disabled.booleanValue()) {
			logger.debug("these two field are disabled. Just return as Cancel..");
			handleCancelStandard();

			// reset the conditionDisabled state: just remove it.
			pst.remove("conditionDisabled");
			return;
		}
		// SEAN Ticket #2469 END

		try
		{
			//--Release2.1--TD_DTS_CR--start//
      //// Update the condition text for both languages first.
      //String origEngText = new String((String) pst.get("dfDocText"));
      //LEN401576 fix. Get original english text from db instead from hidden filed 		
      String origEngText = new String((String) pst.get("ORIGENGLISHTEXT"));
      String origFrenchText = new String((String) pst.get("ORIGFRENCHTEXT"));

      logger.debug("DTDH@handleSubmit::origEnglText: " + origEngText);
      logger.debug("DTDH@handleSubmit::origFrenchText: " + origFrenchText);

      String[] docTextContentToDB =  getDocTextFieldContent(origEngText, origFrenchText);

      //// The Product Team requires the update of a Document Text in both languages.
      logger.debug("DTDH@handleSubmit::docTextContentToDB[0]: " + docTextContentToDB[0]);
      logger.debug("DTDH@handleSubmit::docTextContentToDB[1]: " + docTextContentToDB[1]);
      
   // CIBC DTV
		String isGenerateOnTheFly = PropertiesCache.getInstance().
					getInstanceProperty("com.basis100.conditions.dtverbiage.generateonthefly", "N");
		if(null != isGenerateOnTheFly && "Y".equals(isGenerateOnTheFly)) {
			DocumentTracking dt = new DocumentTracking(srk, docTrackId, copyId);
			List verb = dt.getVerb();
			if(verb != null && verb.size() == 0 ) {
				
				DocTrackingVerbiage dtv = dt.new DocTrackingVerbiage(srk, docTrackId, copyId, 0, "","");
				String label = dt.getDocumentLabelByLanguage(0);
				dtv.setDocumentLabel(label);
				dtv.setDocumentText(docTextContentToDB[0]);
				verb.add(dtv);
				dtv = dt.new DocTrackingVerbiage(srk, docTrackId, copyId, 1, "","");
				label = dt.getDocumentLabelByLanguage(1);
				dtv.setDocumentLabel(label);
				dtv.setDocumentText(docTextContentToDB[1]);
				verb.add(dtv);
				
				dt.appendVerbiage(verb, dt);
				dt.setDocumentTrackingSourceId(2);
				dt.ejbStore();
				
				pg.setPageCondition7(true);
				
			}
			else
				if(verb != null && verb.size() > 0) {
					updateConditionText(docTextContentToDB[0], copyId, docTrackId, 0); 
				    updateConditionText(docTextContentToDB[1], copyId, docTrackId, 1); 
					pg.setPageCondition7(true); 
				}

		}
		else
			if(null != isGenerateOnTheFly && "N".equals(isGenerateOnTheFly)) {
                updateConditionText(docTextContentToDB[0], copyId, docTrackId, 0); //// Update English DocText first.
				updateConditionText(docTextContentToDB[1], copyId, docTrackId, 1); //// Update French DocText.
			}
      //// IMPORTANT!! Set pgCondition7 flag to true to indicate the changed condition
      //// and produce the addition to the Conditions of Approval Outstanding letter!
      //logger.debug("DTDH@handleSubmitStandard::PgCondition7: " + pg.getPageCondition7());

      if(pg.getPageCondition7() == true)
      {
         if (docTrackingIds == null)
            docTrackingIds = new Vector();

         docTrackingIds.add(new Integer(docTrackId));
      }

      theSessionState.setDocTrackingIds(docTrackingIds);

      //logger.debug("DTDH@handleSubmitStandard::DocTrackingIdsSize: " + docTrackingIds.size());
			//--Release2.1--TD_DTS_CR--end//
      navigateToNextPage();
		}
		catch(Exception e)
		{
			logger.error("Document Tracking Detail@handleSubmit()");
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error(e);
			return;
		}

	}


	/**
	 *
	 *
	 */
	private String getPartyType(int partyTypeId)
	{
		try
		{
      doPartyTypeModelImpl partyTypeDo = (doPartyTypeModelImpl)
                                            RequestManager.getRequestContext().getModelManager().getModel(doPartyTypeModel.class);

			partyTypeDo.clearUserWhereCriteria();
			partyTypeDo.addUserWhereCriterion("dfPartyTypeId", "=", new Integer(partyTypeId));

     String partyTypeDesc = null;

      try
      {
        ResultSet rs = partyTypeDo.executeSelect(null);

        if(rs == null || partyTypeDo.getSize() <= 0)
        {
          partyTypeDesc = "";
        }
        else if(rs != null && partyTypeDo.getSize() > 0)
        {
          ////logger.debug("DTDH@getPartyType::Size of the result set: " + partyTypeDo.getSize());

            while (partyTypeDo.next()) {
                  Object oPartyTypeDo = partyTypeDo.getValue(partyTypeDo.FIELD_DFPARTYTYPEDESC);
                  partyTypeDesc = oPartyTypeDo.toString();
                  //logger.debug("DTDH@getPartyType::PartyTypeDescription: " + partyTypeDesc);
                }
            }
      }
      catch (ModelControlException mce)
      {
        logger.debug("DTDH@getPartyType::MCEException: " + mce);
      }
      catch (SQLException sqle)
      {
        logger.debug("DTDH@getPartyType::SQLException: " + sqle);
      }

			if (partyTypeDesc.equals("") || partyTypeDesc == null)
          return null;
			else return
          partyTypeDesc;

		}
		catch(Exception e)
		{
			logger.error("Document Tracking Detail@getPartyType()");
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error(e);
			return null;
		}

	}

  //--Release2.1--TD_DTS_CR--start//
  //// This method provides population both English and French Labels.
	private void updateConditionText(String updateText, int copyId, int docTrackId, int langId)
		throws Exception
	{

    PageEntry pg = theSessionState.getCurrentPage();

    if(docTrackId != -1) //// condition should be active.
		{
				//--Release2.1--//
        //// The Product Team requires the creation of Custom Condition in both languages.
        DocumentTracking freeForm = new DocumentTracking(srk, docTrackId, copyId);

        //// The Product Team requires the creation of Custom Condition in both languages.
        //// Finish this logic (replacement of the text messages in case of null);
        //// move reading and comparison into the handelSubmit() method.
				if (updateText != null && updateText.trim().length() > 0)
				{
					freeForm.setDocumentTextForLanguage(langId, updateText);
          freeForm.setDocumentTrackingSourceId(2);
				}
				else
				{
          freeForm.setDocumentTextForLanguage(langId, " ");
				}

        //// IMPORTANT!! Set pgCondition7 flag to true to indicate the changed condition
        //// and produce the commitment letter!
        pg.setPageCondition7(true);
        //// Make it as a Custom Condition.
        //logger.debug("DTDH@DocTrackSourceId: " + freeForm.getDocumentTrackingSourceId());

        freeForm.ejbStore();
		} // else
		return;
	}

  private String[] getDocTextFieldContent(String origEngText, String origFrenchText)
	{
		String [ ] retval =
		{
			"", ""
    };

		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();

    String currEngText =
          ((TextField)getCurrNDPage().getDisplayField("txEngDocText")).getValue().toString();
    String currFrenchText =
          ((TextField)getCurrNDPage().getDisplayField("txFrenchDocText")).getValue().toString();

    logger.debug("DTDH@CurrentTextEnglish: " + currEngText);
    logger.debug("DTDH@CurrentTextFrench: " + currFrenchText);

		try
		{
			if((currEngText != null && currEngText.trim().length() > 0) &&
//			   (currFrenchText == null || currFrenchText.equals("") ))
			   (currFrenchText == null || currFrenchText.trim().length() == 0 ))
      {

        retval [ 0 ] = currEngText.trim();
        retval [ 1 ] = currEngText.trim();
       }
			else if((currFrenchText != null && currFrenchText.trim().length() > 0) &&
//			        (currEngText == null || currEngText.equals("") == 0 ))
	                (currEngText == null || currEngText.trim().length() == 0 ))
      {

        retval [ 0 ] = currFrenchText.trim();
        retval [ 1 ] = currFrenchText.trim();
       }
			else if((currEngText != null && currEngText.trim().length() > 0) &&
              (currFrenchText != null && currFrenchText.trim().length() > 0) &&
              !currFrenchText.trim().equals(origFrenchText.trim()) &&
              !currEngText.trim().equals(origEngText.trim()))
      {

        retval [ 0 ] = currEngText.trim();
        retval [ 1 ] = currFrenchText.trim();
       }
			else if((currEngText != null && currEngText.trim().length() > 0) &&
              (currFrenchText != null && currFrenchText.trim().length() > 0) &&
              !(currEngText.trim()).equals(origEngText.trim()))
      {

        retval [ 0 ] = currEngText.trim();
        retval [ 1 ] = currEngText.trim();
       }
			else if((currEngText != null && currEngText.trim().length() > 0) &&
              (currFrenchText != null && currFrenchText.trim().length() > 0) &&
              !(currFrenchText.trim()).equals(origFrenchText.trim()))
      {
logger.debug("DTDH@I should not be here!!");
        retval [ 0 ] = currFrenchText.trim();
        retval [ 1 ] = currFrenchText.trim();
       }
			else if((currEngText != null && currEngText.trim().length() > 0) &&
              (currFrenchText != null && currFrenchText.trim().length() > 0) &&
              (currFrenchText.trim()).equals(origFrenchText.trim()) &&
              (currEngText.trim()).equals(origEngText.trim()))
      {

        retval [ 0 ] = origEngText.trim();
        retval [ 1 ] = origFrenchText.trim();
       }

       logger.debug("DTDH@getDocTextFieldContent::retval_0: " + retval[0]);
       logger.debug("DTDH@getDocTextFieldContent::retval__1: " + retval[1]);
		}
		catch(Exception e)
		{
			logger.trace("--T--> DTDH@getDocTextFieldContent::: in=<" + origEngText + "> - use empty phone number");
		}
		return retval;
	}

  //// Method to set fields disable for predefined userIds.
  private void setupDisabledFields(ViewBean thePage)
	{
		HtmlDisplayFieldBase df;
    Hashtable pst = getTheSessionState().getCurrentPage().getPageStateTable();
    Iterator it = ((DocTrackingDetailsConfigManager)pst.get(CONFIGMGR)).getAllDisableFields();
    if(it != null)
    {
      String fieldName = "";
      for( ; it.hasNext() ; )
      {
        fieldName = (String)(it.next());
        df = (HtmlDisplayFieldBase)thePage.getDisplayField(fieldName);
        df.setExtraHtml(df.getExtraHtml() + " disabled");
		// SEAN Ticket #2469 Dec 14, 2005
		if (fieldName.equalsIgnoreCase("txEngDocText") || fieldName.equalsIgnoreCase("txFrenchDocText"))
			pst.put("conditionDisabled", new Boolean(true));
		// SEAN Ticket #2469 END
      }
    }
  }

  //// New Method to check if file is hidden
  protected boolean isFieldHidden(String fieldName)
  {
    DocTrackingDetailsConfigManager theConfigMgr =
      (DocTrackingDetailsConfigManager)(getTheSessionState().getCurrentPage().getPageStateTable().get(CONFIGMGR));

    if(theConfigMgr != null)
    {
      return theConfigMgr.isFieldHidden(fieldName);
    }
    else
      return false;
  }

	public String removeJScriptFunction(String origHtmlStr)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();

    String resultStr = origHtmlStr;

    ////String strToReplace = pg.getPageState1();
    TextField txbox =(TextField) getCurrNDPage().getDisplayField("txFrenchDocText");
    String strToReplace = txbox.getExtraHtml();

    ////logger.debug("VLAD ===>  the Original HTML is = " + origHtmlStr);
    ////logger.debug("VLAD ===>  the strToReplace is = " + strToReplace);

    if(strToReplace != null && !strToReplace.trim().equals(""))
      resultStr = StringTokenizer2.replace(resultStr, strToReplace, " ");

    logger.debug("VLAD ===>  the Result HTML is = " + resultStr);

    return resultStr;
	}

	/**
	 *
	 *
	 */
	private void populateFieldValidation(ViewBean thePage)
	{
		// the OnChange string to be set on the fields
    PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		SessionResourceKit srk = getSessionResourceKit();
    Deal deal = null;

    try
    {
      deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
    }
    catch(Exception e)
    {
      logger.error("Exception @DTDH::popultateFieldValidation - ignored");
      logger.error(e);
    }

		HtmlDisplayFieldBase dfen;
		HtmlDisplayFieldBase dffr;
    String theExtraHtmlText = "";

		//// 1. Validation for the special characters ('<', '>', ''', '"', '&' since
    //// they scrud up the document generation.
		dfen = (HtmlDisplayFieldBase)thePage.getDisplayField("txEngDocText");
		dffr = (HtmlDisplayFieldBase)thePage.getDisplayField("txFrenchDocText");

    //#DG490 theExtraHtmlText = "onBlur=\"isFieldHasSpecialChar();\"";
    theExtraHtmlText = "onChange=\"return isFieldHasSpecialChar();\"";    //#DG490

    //// 2. Close access for editing if:
    //// 1. If Client do not want to have an option allowing to add/edit Standard
    //// and Custom Conditions on the DocumentTracking screen (parent screen)
    //// a possibility to edit ANY condition on details screen is closed.
    //// 2. If DocumentStatusId equals 3 (Approved) the edit option is closed
    //// for this particular condition.
    //// 3. If Section is Funder Section (#3 on the Document Tracking screen) edit
    //// option is closed. The reasons are as follows:
    ////    -- There is no Funder Conditions in any produced document (Commitment Letter, etc.)
    ////    -- This is a System Generated Condition, thus system will override an
    ////       editing anyway.
    //// 4. If DealStatusCategory equals 3 ("done") user should not edit the conditions.

    logger.debug("DTDH@populateDetails::DocStatus: " +  pst.get("dfDocStatus"));
    int theDealStatusId = deal.getStatusId();
    logger.debug("DTDH@populateDetails::DealStatus: " +  theDealStatusId);
    //FXP27848 added instid to get Proeprty. i.e. get value from DB
    if (PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(), "com.basis100.conditions.includestdcustomsectionsondts", "N").equals("N") ||
        (pst.get("dfDocStatus") != null && pst.get("dfDocStatus").equals("Approved")) ||
        (pst.get("sectionId") != null && (new Integer(pst.get("sectionId").toString())).intValue() == 3) ||
        theDealStatusId == Mc.DEAL_COLLAPSED ||
        theDealStatusId == Mc.DEAL_DENIED ||
        theDealStatusId == Mc.DEAL_FUNDED ||
        theDealStatusId == Mc.DEAL_TO_SERVICING)
    {
         theExtraHtmlText = " disabled";
		 // SEAN Ticket #2469 Dec 13, 2005: introduce the page state conditionDisabled,
		 // it's value should be a Boolean.  set to true is these fields disabled.
		 pst.put("conditionDisabled", new Boolean(true));
		 // SEAN Ticket #2469 END
    }

		dfen.setExtraHtml(theExtraHtmlText);
		dffr.setExtraHtml(theExtraHtmlText);

    ////logger.debug("DTDH@populateDetails::ExtraHtmlEng: " + ((TextField)getCurrNDPage().getDisplayField("txFrenchDocText")).getExtraHtml());
    ////logger.debug("DTDH@populateDetails::ExtraHtmlFrecnh: " + ((TextField)getCurrNDPage().getDisplayField("txEngDocText")).getExtraHtml());
	}
  //--Release2.1--TD_DTS_CR--end//
  //--TD_CONDR_CR--start--//
	/**
	 *
	 * Override the DocTrackingDetails page label if necessary.
	 */
	public void overridePageLabel(int pageId)
	{
		ViewBean thePage = getCurrNDPage();
    PageEntry pe = (PageEntry) theSessionState.getCurrentPage();

    int languageId = theSessionState.getLanguageId();

    String pageLabel = BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),"PAGELABEL", pageId, languageId);

		//logger.debug("CRH@overridePageLabel::Label: " + pageLabel);
    //logger.debug("CRH@overridePageLabel::id: " + pageId);

		thePage.setDisplayFieldValue("stPageLabel", new String(pageLabel));
   }
  //--TD_CONDR_CR--end--//
}

