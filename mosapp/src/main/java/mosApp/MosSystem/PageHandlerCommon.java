package mosApp.MosSystem;

/**
 * 29/Feb/2008 DVG #DG702 MLM x BDN merge adjustments
 * 01/Oct/2007 DVG #DG630 FXP18549 BDN Express- French Deal Notes page shows blank page 
 * 28/Jun/2007 DVG #DG608 LEN192740  express problem detected by development 
	- deal notes text becomes a clob and non nullable
 * 24/Jul/2007 DVG #DG612 LEN203552: Venesa Whyte Express slow - CPU at 100% 
    - livelock when setting previous page to the same (?)
 * 18/Oct/2006 DVG #DG526 #4916  DJ 3.1 - Apostrophes appear as "accent graves" when entered in a note
 * 23/Aug/2006 DVG #DG492 #  4416  COOP - SystemTypeID is NULL when entered via deal entry
 * 
 * @version 1.1 July 29, 2008 MCM team (XS 2.60) modified to handle pagelet with tileView for some methods
 * @version 1.2 Aug 27, 2008 MCM team ML Ticket merge FXP22128
 * @version 1.3 Sep 3, 2008 MCM team artf726091, for access right check for DealEntory, changed to use institutionid from screen. 
 */

import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;
import java.math.BigDecimal;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEventStatusUpdateAssoc;
import com.basis100.deal.entity.DealHistory;
import com.basis100.deal.entity.DealPropagator;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.entity.InsureOnlyApplicant;
import com.basis100.deal.entity.LifeDisPremiumsIOnlyA;
import com.basis100.deal.entity.LifeDisabilityPremiums;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.ESBOutboundQueuePK;
import com.basis100.deal.pk.InsureOnlyApplicantPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.security.ACM;
import com.basis100.deal.security.AccessResult;
import com.basis100.deal.security.ActiveMessage;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.security.DLM;
import com.basis100.deal.security.DealEditControl;
import com.basis100.deal.security.DealStatusManager;
import com.basis100.deal.security.JumpState;
import com.basis100.deal.security.PDC;
import com.basis100.deal.util.BXStringTokenizer;
import com.basis100.deal.util.DBA;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.WFEExternalTaskCompletion;
import com.basis100.workflow.WFTaskExecServices;
import com.basis100.workflow.WorkflowAssistBean;
import com.basis100.workflow.entity.Page;
import com.filogix.express.core.ExpressRuntimeException;
import com.filogix.express.legacy.mosapp.IPageHandlerCommon;
import com.filogix.express.state.ExpressState;
import com.filogix.express.web.jato.ExpressViewBeanBase;
import com.filogix.express.web.rc.util.PageInstitutionUtil;
import com.filogix.express.web.rc.util.PageInstitutionUtil.InstitutionUsage;
import com.filogix.express.web.state.InstitutionProfileDataBean;
import com.filogix.express.web.state.UserProfileDataBean;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.util.Xc;
import com.iplanet.jato.CompleteRequestException;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.util.StringTokenizer2;
import com.iplanet.jato.view.DisplayField;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledViewBase;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.event.ChildDisplayEvent;
import com.iplanet.jato.view.html.CheckBox;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HREF;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.HtmlDisplayFieldBase;
import com.iplanet.jato.view.html.ImageField;
import com.iplanet.jato.view.html.Option;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

//// Highly discussable implementation with the XXXXHandler extension of the
//// ViewBeanBase JATO class. The sequence diagrams must be verified. On the other hand,
//// This implementation allows to use the basic JATO framework elements such as
//// RequestContext() class. Theoretically, it could allow to override the JATO/iMT
//// auto-migration. Temporarily these fragments are commented out now for the compability
//// with the stand alone MWHC<--PHC<--XXXHandlerName Utility hierarchy.
//// getModel(ClassName.class), etc., could be also rewritten in the Utility hierarchy
//// using this inheritance.

public class PageHandlerCommon extends MosWebHandlerCommon
    implements Xc, Cloneable, View, ViewBean, IPageHandlerCommon
{
    // The logger
    private final static Log _log = 
        LogFactory.getLog(PageHandlerCommon.class);
    
	public static final String CHILD_SESSION_USER_ID = "sessionUserId";
	public static final String CHILD_ST_USER_NAME_TITLE = "stUserNameTitle";
	public static final String CHILD_ST_TODAY_DATE = "stTodayDate";
	public static final String CHILD_ST_COMPANY_NAME = "stCompanyName";
	public static final String CHILD_STPAGELABEL = "stPageLabel";
	public static final String CHILD_DETECTALERTTASKS = "detectAlertTasks";
	public static final String COM_BASIS100_COMMUNICATION_SSL_ENCRYPTION = "com.basis100.communication.ssl.encryption";
	public static final String COM_BASIS100_DEAL_TAXPAYORID = "com.basis100.deal.taxpayorid";
	public static final String COM_BASIS100_DEAL_PRIVILEGEPAYMENTOPTION = "com.basis100.deal.privilegepaymentoption";
	public static final String COM_BASIS100_RESOURCE_LANGUAGE = "com.basis100.resource.language";
	public static final String COM_BASIS100_GCD_SHOW_GCDSUMMARY_SECTION = "com.basis100.GCD.showGCDSummarySection";
	public static final String YEAR_SHORT = "YEAR_SHORT";
	public static final String MONTH_SHORT = "MONTH_SHORT";
	public static final String YEARS_SHORT = "YEARS_SHORT";
	public static final String MONTHS_SHORT = "MONTHS_SHORT";
	public static final String INSURER_LABEL = "INSURER_LABEL";
	public static final String LENDER_LABEL = "LENDER_LABEL";
	public static final String PKL_MONTHS = "MONTHS";
	public static final String PKL_DJPAGELABEL = "DJPAGELABEL";
	public static final String GOTO_PAGE_NOINPUT = "GOTO_PAGE_NOINPUT";
	public static final String GOTO_PAGE_NODEALNUM = "GOTO_PAGE_NODEALNUM";
	public static final String NO_MATCH_FOR_GO_DEAL_NUMBER = "NO_MATCH_FOR_GO_DEAL_NUMBER";
	public static final String DEALID_MISSING = "DEALID_MISSING";
	public static final String FUNCTION_NOT_SUPPORTED = "FUNCTION_NOT_SUPPORTED";
	public static final String NO_WORK_QUEUE_ACCESS = "NO_WORK_QUEUE_ACCESS";
	public static final String SIGN_OFF_MSG_CONFIRM = "SIGN_OFF_MSG_CONFIRM";
	public static final String PAGE_DENIED_DEFAULT = "PAGE_DENIED_DEFAULT";

    // the jato fields name for page id and institution id. field for deal id
    // defined in Sc.java
    public static final String TB_PAGE_ID = "tbPageId";
    public static final String TB_INSTITUTION_ID = "tbInstitutionId";
    //added for Exchange Link
	public static final String CHILD_FOLDERCODE = "hdFolderCode";
	public static final String COM_FILOGIX_MOSAPP_EXCHANGEURLLINK_ENGLISH = "com.filogix.mosapp.ExchangeURLLink.English";
	public static final String COM_FILOGIX_MOSAPP_EXCHANGEURLLINK_FRENCH = "com.filogix.mosapp.ExchangeURLLink.French";
	public static final String EXCHANGELINK_PATH_START = "?func=ll&objId=";
	public static final String EXCHANGELINK_PATH_END = "&objAction=browse&viewType=1";

    public static final String CHILD_STMIRESPONSE = "stMIResponse";
    public static final String CHILD_STHDFREQUENCY = "stHDFrequency";
    public static final String CHILD_STHDCHMCALERT = "stHDChmcAlert";    
    public static final String CHILD_STHDGENWORTHALERT = "stHDGenworthAlert";
    public static final String CHILD_STHDDEALLOCKED = "stHDDealLocked";
    public static final String CHILD_STHDMIRESPONSESTATUSID = "stHDMIResponseStatusId";
    public static final String CHILD_STHDMIPROVIDERID = "stHDMIProviderId";    
    public static final String CHILD_STHDSESSIONTIMEOUT = "stHDSessionTimeOut";
    
	// very temp constactors to test the version with the ViewBeanBase
	// inheritance.
  public PageHandlerCommon()
  {
    super();
                theSessionState = super.theSessionState;
  }

  public PageHandlerCommon(String name)
  {
    this(null,name); // No parent
                theSessionState = super.theSessionState;
  }

  protected PageHandlerCommon(View parent, String name)
  {
    super(parent,name);
                theSessionState = super.theSessionState;
  }

  //// It is re-implemented as a SessionModel.
  private ViewBean currNDPage;
  private TaskNavigator taskNavigator;
  private PageStateHandler pageStateHandler;
  private boolean skipJumpTrap;

  public SavedPagesModelImpl savedPages;

  //--> It's not necessary to have logger here we should use the Parent's logger
  //--> By Billy 16Oct2002
  //public SysLogger logger;
  //============================================================================

  //// This method is still here more for historical reasons. Currently the
  //// non-default URL obtaining is moved to each page (ViewBean).
  //// This is done mostly because of two reasons:
  //// 1. to call this method the extension of each method should be changed
  //// (VeawBeanBase versus the appropriage Handler).
  //// 2. first time when this method is called here the defaultURL is not initialied yet.
  //// This lead to some extra bx framework massaging.
  //// This method proves the usage of the parallel content for the internalization, for example.

 public String getDisplayURL()
 {
       String url=getDefaultDisplayURL();
       String testUrl = "";

       //logger = SysLog.getSysLogger("PHC");
       //logger.debug("PHC@getDisplayURL::DefaultURL: " + url);

       // Let's detach the .jsp extension
       if(url != null && !url.trim().equals("") && !url.trim().equals("/"))
       {
         url=url.substring(0,url.length() - 4);
         //logger.debug("PHC@getDisplayURL::Substring: " + url);
       }

       if(url.trim().equals("/"))
       {
         PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
         //logger.debug("PHC@getDisplayURL::NextPageName: " + pg.getNextPageName());
         //logger.debug("PHC@getDisplayURL::NextPE: " + pg.getNextPE());

         String test = getCurrNDPage().getQualifiedName();
         //logger.debug("PHC@getDisplayURL::QualifiedName: " + getCurrNDPage().getQualifiedName());
         //logger.debug("PHC@getDisplayURL::Name: " + getCurrNDPage().getName());
         //logger.debug("PHC@getDisplayURL::DefaultDisplayURL: " + getCurrNDPage().getDefaultDisplayURL());

         ////testUrl = pg.getPageName();

         testUrl = getTargetViewName();

         ////BXTODO: temp hardcoded to test the concept!!! READ IT from the property.
         testUrl = "/mosApp/MosSystem/" + testUrl;
         //logger.debug("PHC@getDisplayURL::ConstructedURL: " + testUrl);
       }

       //// TEST of various language options.
       HttpSession session = RequestManager.getRequestContext().getRequest().getSession();
       String language = (String)session.getAttribute("LANGUAGE");
       //logger.debug("PHC@getDisplayURL::LanguageFromHttpSession: " + language);

       if(language == null || language.trim().equals(""))
          language = (PropertiesCache.getInstance().getProperty(-1, COM_BASIS100_RESOURCE_LANGUAGE, "english"));

       //logger.debug("PHC@getDisplayURL::LanguageFromProperty: " + language);

       // Now construct a URL based on the current language.  We could do
       // anything here, including customizing the URL based on device type
       // or output markup type.
       // Note that we are not using a Locale object here to look up the
       // JSP template.
       //// The current implementation is for testing purposes only. It should be generalized
       //// and moved to the PageHandlerClass (business delegation implementation).

       if(language.equals("french"))
       {
            if(url.equals("/")) {
              url = testUrl;
            }
            url+="_"+ "fr" +".jsp";
            //logger.debug("PHC@getDisplayURL::I am in French::URL_fr: " + url);
       }
       else if(language.equals("english"))
       {
            if(url.equals("/")) {
              url = testUrl;
            }
            url = getDefaultDisplayURL();
            //logger.debug("PHC@getDisplayURL::I am in English:URL_en: " + url);
       }

      // Let's see if we can find out if the JSP for the derived name could
      // be found.  If not, we'll fall back to the default JSP.  This approach
      // should be used with caution on a server under high load.  Instead,
      // we'd probably want to cache this information or obtain it from some
      // configuration information.

      //logger.debug("PHC@getDisplayURL::Stamp before call InputStream");

      InputStream testStream=
          RequestManager.getRequestContext().getServletContext().getResourceAsStream(url);

      ////logger.debug("PHC@getDisplayURL::Stream: " + testStream.toString());

      if (testStream==null)
      {
          //// The resource was not found.  Fall back to the default.
          ////url=getDefaultDisplayURL();
          url=getDefaultDisplayURL();
          //logger.debug("PHC@getDisplayURL::I am in NULL inputStream mode");
      }
      else
      {
          //// We've found the desired JSP; we'll just use it
          try
          {
              testStream.close();
          }
          catch (Exception e)
          {
              // Can't happen, ignore
          }
      }

      return url;
  }

  public void saveData(PageEntry currPage, boolean calledInTransaction)
  {
    logger.debug("--T--> @PHC.saveData: default framework method - do nothing");

  }


  /**
   *
   *
   */
  public void updateData(boolean calledInTransaction)
    throws Exception
  {
    logger.debug("@PHC.updateData: framework default method - do nothing");
    ;
  }


  /**
   *
   *
   */
  public void updateData(ViewBean currPage, boolean calledInTransaction)
    throws Exception
  {
    logger.debug("@PHC.updateData: framework default method - do nothing");

    ;

  }


  /**
   *
   *
   */
  public void handleCustomActMessageOk(String[] args)
  {
    logger.debug("@PHC.handleCustomActMessageOk: standard framework method - do nothing");
  }

  /** handleNoDisplayActMessage
  *
  * Default handler to skip active message (OK) responses. This is a new demand from
  * the Product Team to reduce the interactive dialog activity. Expected to be overriden in
  * handler (sub-class) that uses custom dialogs - see ActiveMessage.java, property
  * responseIdentifier for details.
  *
  **/
 //--CervusPhaseII--start--//
 public void provideOverrideDealLockActivity(PageEntry pg, SessionResourceKit srk, ViewBean thePage)
 {
    logger.trace("--T--> PHC@provideOverrideDealActivity: ");

    DLM dlm = null;
    int lockerUserProfileId = 0;
    String userLogin = "";
    String lockerSessionId = "";
    HttpSession lockerHttpSession = null;

    // 1. Get session info related to the locker of a deal.
    try {
      lockerUserProfileId = dlm.getInstance().getUserIdOfLockedDeal(pg.getPageDealId(), srk);
      UserProfile up = new UserProfile(srk);
      up.findByPrimaryKey(new UserProfileBeanPK(lockerUserProfileId, pg.getDealInstitutionId()));
      userLogin = up.getUserLogin();
      lockerSessionId = SessionServices.allSessionIdsByLoginId.get(userLogin.toUpperCase()).toString();
      lockerHttpSession = (HttpSession)SessionServices.allHttpSessionsByLoginId.get(userLogin.toUpperCase());
    }
    catch (Exception e){
      logger.error("--T--> PHC@provideOverrideDealLockActivity:Exception on getting Session info: " + e);
    }

    logger.trace("--T--> PHC@provideOverrideDealLockActivity:CurrentSessionId: " + thePage.getSession().getId());
    logger.trace("--T--> PHC@provideOverrideDealLockActivity:LockerHttpSession: " + lockerHttpSession);
    logger.trace("--T--> PHC@provideOverrideDealLockActivity:UserOfLockedDealId: " + lockerUserProfileId);

    // 2. Unlock deal and clean up his/her session related objects.
    // IMPORTANT!! This is critical when the session of a locker is not expired at
    // the moment of overlocking. Without this activity two users potentially may
    // get an access to the same deal.
    try {
      dlm = DLM.getInstance();
      dlm.doUnlock(pg.getPageDealId(), srk.getExpressState().getUserProfileId(), srk, true, false);

      // Clean up the stored session and clean up login states
      SessionServices.removeActiveSession(lockerHttpSession, lockerSessionId, false);
    }
    catch (Exception ex) {
      logger.error("DTH@handleCustomActMessageOk:Exception: " + ex);
    }

    // 3. Place new lock for the overlocked deal.
    try {
      dlm = DLM.getInstance();
      dlm.lock(pg.getPageDealId(), srk.getExpressState().getUserProfileId(), srk,this.getSession().getId().toUpperCase(), "N");

     logger.trace("PHC@provideOverrideDealLockActivity::UserProfileId_NewLock: " + srk.getExpressState().getUserProfileId());
   }
   catch (Exception ex) {
     logger.error("PHC@provideOverrideDealLockActivity:Exception: " + ex);
   }

   theSessionState.setActMessage(null);
}
 //--CervusPhaseII--end--//


  public void handleNoDisplayActMessage(boolean decide)
  {
    logger.debug("@PHC.handleNoDisplayActMessageOk: standard framework method - do nothing");

    ; // do nothing - e.g. current page redisplayed
  }


  /**
   *
   *
   */
  public void noAuditIfIncomplete(PageEntry pg, Deal deal)
    throws IllegalArgumentException
  {
    //logger.debug("PHC@noAutditIfIncomplete:: start");
    if (pg == null)
    {

      ////pg = getTheSession().getCurrentPage();

      pg = (PageEntry) theSessionState.getCurrentPage();

      //logger.debug("PHC@noAutditIfIncomplete::PageDealId: " + pg.getPageDealId());
      //logger.debug("PHC@noAutditIfIncomplete::PageDealCID: " + pg.getPageDealCID());
    }

    if (deal == null)
    {
      try
      {
        deal = DBA.getDeal(getSessionResourceKit(), pg.getPageDealId(), pg.getPageDealCID(), null);
        //logger.debug("PHC@noAutditIfIncomplete::dealId: " + deal.getDealId());
        //logger.debug("PHC@noAutditIfIncomplete::copyId: " + deal.getCopyId());
        //logger.debug("PHC@noAutditIfIncomplete::statusId: " + deal.getStatusId());
        //logger.debug("PHC@noAutditIfIncomplete::isAuditable: " + deal.isAuditable());
      }
      catch(Exception ex)
      {
        logger.debug("PHC@noAutditIfIncomplete::Exception: " + ex);
      }
    }
    if (deal.getStatusId() == Mc.DEAL_INCOMPLETE) getSessionResourceKit().setInterimAuditOn(false);

  }


  /**
   *
   *
   */
  public void noAuditIfIncomplete()
  {
    getSessionResourceKit().setInterimAuditOn(false);

  }


  /**
   *
   *
   */
  public void preHandlerProtocol(ViewBean currNDPage)
  {
    logger = SysLog.getSysLogger("PHC@PreHandler");

    preHandlerProtocol(currNDPage, false);
  }


  /**
   *
   *
   */
  public void preHandlerProtocol(ViewBean currNDPage, boolean forceSkipPageSave)

  {
    memberInit();

    //logger = SysLog.getSysLogger("PHC@PreHNDL");
    //logger.debug("@PHC.preHandlerProtocol::I start the preHandlerProtocol");

    this.currNDPage = currNDPage;

    String pgName = currNDPage.getName();

    getSessionObjects();


    
    // It's not necessary, we may take out the HTTPSeceion later and it was already done in getSessionObjects()
    //--> Commented out  by BILLY 03July2002
    // theSession = RequestManager.getRequestContext().getRequest().getSession();
    //========================================================================

    // create resource kit
    ////setupSessionResourceKit("" + theSession.getSessionUserId());
    setupSessionResourceKit("" + theSessionState.getSessionUserId());

    //logger.debug("@PHC.preHandlerProtocol::SessionUserId: " + theSessionState.getSessionUserId());

    logger.debug("@PHC.preHandlerProtocol: IN, PAGE<" + pgName + " >, skipSave=<" + forceSkipPageSave + ">");

    clearMessage();

    //logger.debug("@PHC.preHandlerProtocol::After clearMessage");

    savedPages.clearPageSetFlags();

    //logger.debug("@PHC.preHandlerProtocol::After clearPageSetFlags");

    PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

    //Since 4.0 MCM : apply institutionId, Oct 28, 2008 - Hiro
    _log.debug("before calling enforceInstitutionId @preHandlerProtocol");
    enforceInstitutionId(currPg);
    _log.debug("after calling enforceInstitutionId @preHandlerProtocol");
    
    //FXP200047 fix start
    //logger.debug("preHandlerProtocol@PHC: CurrentPage InstituionId = " + currPg.getDealInstitutionId() );
    //FXP200047 fix end
    //Class testClass = currNDPage.getClass();
    //String testClassName = currNDPage.getClass().getName();
    //logger.debug("@PHC.preHandlerProtocol::CLASS: " + testClass.toString());
    //logger.debug("@PHC.preHandlerProtocol::CLASS_NAME: " + testClassName);

    //logger.debug("@PHC.preHandlerProtocol::CurrentNDPage: " + currNDPage.getName());
    //logger.debug("@PHC.preHandlerProtocol::CurrPage: " + currPg.getPageName());
    //logger.debug("@PHC.preHandlerProtocol::PageId: " + currPg.getPageId());

    // check for invalid state (e.g. invalid session objects due to event from expired/timed-out session)
    if (currPg.getPageId() == 0)
    {
      logger.debug("@PHC.preHandlerProtocol: invalid session state (likely expired session) - processing abandoned (generating exception)");

      // free resources
      unsetSessionResourceKit();

      //--CervusPhaseII--start--//
      // If the session of a deal locker is not exprired it has to be terminated
      // and the EndSession page should be displayed. IMPORTANT, otherwise there is
      // possible access to the same deal by several clients!!
      logger.info("--> @PHC.preHandlerProtocol: session of a locked deal user was active - force to Session Ended page !!");
      ViewBean theEndSessionPage = RequestManager.getRequestContext().
          getViewBeanManager().getViewBean(pgSessionEndedViewBean.class);
      theEndSessionPage.forwardTo(RequestManager.getRequestContext());
      //--CervusPhaseII--end--//

      generateExceptionIntentional();
    }

    // check to see if session was invalidated (subsequent session fro same login id)
    ////if (SessionServices.isSessionInvalidated(CSpider.getUserSession()) == true)
    ////     BXTODO: Convert to the proper JATO Session API. It includes concurrent session mngmnt,
    ////   session timeout, etc.
    //--> Modified by BILLY to call the SessionService as before -- By BILLY 11July2002
    if (SessionServices.isSessionInvalidated(theSession) == true)
    {
      handleSignOff(true, true);
      generateExceptionIntentional();
    }

    // set default display page method to "load"
    currPg.setPageDisplayMethod(Mc.DISPLAY_METHOD_LOAD);

    currPg.setLoadTarget(0);


    // validate event. Here we:
    //  . check to see if event is from (server prespective) of current page. If not:
    //      . try to determine if from page no longer displayed (e.g. generated from page
    //        that has been navigated away from - double-click on a sub-page exit control)
    //      . try to determine if from a parent of the current page (e.g. user used 'back'
    //        control.
    //String pageName = currNDPage.getName();
    //logger.debug("@PHC.preHandlerProtocol::PageName again: " + pageName);

    Page fromPage = null;

    try
    {
      PDC pdc = PDC.getInstance();
      fromPage = pdc.getPageByName(pgName);

      logger.debug("@PHC.preHandlerProtocol::FromPage: " + fromPage);
    }
    catch(Exception e)
    {
      fromPage = null;
    }


    // ensure from known page - else ignore
    if (fromPage == null)
    {
      logger.error("Exception @PHC.preHandlerProtocol: Event form unknow page, PAGE<" + pgName + "> - ignored, redisplay current page");

      postHandlerProtocol();
      //// There is no need in this now. Throwing the ClassNotFoundException()
      //// stop the request in its tracks. TODO:finish and test this.
      generateExceptionIntentional();
    }

    // ensure from current page - else allow if from parent, ignore otherwise
    //
    //    Note: Deal Entry and Deal Modification have same name - detect this special case
    //
    boolean okDealMod =(fromPage.getPageId() == Mc.PGNM_DEAL_ENTRY_ID) &&(currPg.getPageId() == Mc.PGNM_DEAL_MODIFICATION);

    //logger.debug("@PHC.preHandlerProtocol::FromPageId: " + fromPage.getPageId());
    //logger.debug("@PHC.preHandlerProtocol::CurrPgId: " + currPg.getPageId());
    //logger.debug("@PHC.preHandlerProtocol::OkDealMode: " + okDealMod);

    if (fromPage.getPageId() != currPg.getPageId() &&(! okDealMod))
    {
      // check from parent page
      PageEntry parent = currPg.getParentPage();
      //logger.debug("@PHC.preHandlerProtocol::ParentPage: " + parent);

      while(parent != null && parent.getPageId() != fromPage.getPageId())
      {
        parent = parent.getParentPage();
      }
      if (parent != null)
      {
        // event from parent page - allow it after ensuring tx copies ok
        logger.debug("@PHC.preHandlerProtocol: Event from parent page, PAGE<" + parent.getPageName() + "> - allowing - user likely used browser Back");
        ////DealEditControl dec = theSession.getDec();
        DealEditControl dec = theSessionState.getDec();

        if (dec != null && parent.getTxCopyLevel() > 0)
        {
          logger.debug("@PHC.preHandlerProtocol: Event from parent page, dropping tx copy(s) left by sub-page(s), if any");
          try
          {
            srk.beginTransaction();
            dec.dropToTxLevel(parent.getTxCopyLevel(), srk, null);
            theSessionState.setCurrentPage(parent);

            currPg = parent;
            srk.commitTransaction();
          }
          catch(Exception e)
          {
            srk.cleanTransaction();
            logger.error("Exception @PHC.preHandlerProtocol: Attempting to allow parent page event - cleaning tx copies - ignore event:" + e);		//#DG702
            generateExceptionIntentional();
          }
        }
      }
      else
      {
        // not from parent
        // logger.error("Exception @PHC.preHandlerProtocol: Unexpected event from non-current page, PAGE<" + pgName + "> ignored - possible Browser double-click");
        postHandlerProtocol();
        //// There is no need in this now. Throwing the ClassNotFoundException()
        //// stop the request in its tracks. TODO:finish and test this.
        throwCompleteRequestException();
      }
    }

    //  retrieve and store value of hidden field <detectAlertTasks> (on most pages - trap
    //  failure in case absent on some page(s)
    try
    {
      String dat = currNDPage.getDisplayFieldValue(CHILD_DETECTALERTTASKS).toString();
      //logger.debug("@PHC.preHandlerProtocol::DetectAlertTask: " + dat);

      theSessionState.setDetectAlertTasks(dat);
    }
    catch(Exception e)
    {
      logger.error("@PHC.preHandlerProtocol:Exception: " + e);
    }

    // e.g. page without this display field
    // save data for editable page (e.g. can't know if there are changes or not)
                logger.debug("@PHC.preHandlerProtocol::ForceSkipPageSave: " + forceSkipPageSave);
                logger.debug("@PHC.preHandlerProtocol::CurrPgIsEditable?: " + currPg.isEditable());

    if (forceSkipPageSave == false && currPg.isEditable() == true)
    {
      logger.debug("@PHC.preHandlerProtocol: editable page, calling saveData()");
      saveData(currPg, false);

      // restore audit production (in case method temporarilly disabled)
      getSessionResourceKit().restoreAuditOn();
    }
    //logger.debug("@PHC.preHandlerProtocol: OUT");
  }
  
  /**
   *
   *
   */
  private void generateExceptionIntentional()
  {
    //--> Changed by to Throw the CompleteRequestException
    //-->String str = null;
    //-->int i = str.length();
    //--> Modified by Billy 16Oct2002
    logger.error("@PHC.generateExceptionIntentional :: Generated CompleteRequestException to terminate further process !!");
    throwCompleteRequestException();
  }
  
  private void throwCompleteRequestException(){
	  throw new CompleteRequestException();
  }


  /**
   *
   *
   */
  public void memberInit()
  {
    //logger = SysLog.getSysLogger("PHC@MemberInit");
    taskNavigator = new TaskNavigator();
    pageStateHandler = new PageStateHandler(this);
    //logger.debug("PageStateHandler from memberInit: " + pageStateHandler);
    skipJumpTrap = false;
  }


  /**
   *
   *
   */
  public void initFromOther(PageHandlerCommon other)
  {
    //--> Should set the SessionState as well
    //--> Bug fix by BILLY 17Sept2002
    theSession = other.theSession;
    theSessionState = other.theSessionState;
    savedPages = other.savedPages;
    srk = other.srk;
    logger = other.logger;
  }


  /**
   *
   *
   */
  public boolean standardAdoptTxCopy(PageEntry pe, boolean mainPageSubmit)
    throws Exception
  {
    logger.debug("@PHC.standardAdoptTxCopy");

    ////DealEditControl dec = theSession.getDec();
    DealEditControl dec = theSessionState.getDec();

    if (dec == null || dec.isTxCopy() == false) return false;

    // ensure page is associated with a tx copy
    int pageCopyLevel = pe.getTxCopyLevel();

    logger.debug("@PHC.standardAdoptTxCopy: page=<" + pe.getPageName() + ">, tx copy level<" + pageCopyLevel + ">");

    if (pageCopyLevel <= 0) return false;

    // no tx copy for page (actually expect value -1 in this case buy <=0 safer
    // adopt the pages' tx copy

    //// TODO. This is a very temp comment to compile the util class.
    //// The com/basis100/... classes should be recompiled to reflect the
    //// modifing of MosSystem package to mosApp.MosSystem. This modification
    //// is an essential feature of JATO.
    dec.adoptTxLevel(pageCopyLevel, srk, pe);

    //FXP23325 MCM Nov 10, 2008 -- start
    //pe.setPageDealCID(dec.getCID());
    int copyId = dec.getCID();
    pe.setPageDealCID(copyId);
    if(pe.isSubPage()){
        pe.getParentPage().setPageDealCID(copyId);
    }
    //FXP23325 MCM Nov 10, 2008 -- end
    return true;
  }


  /**
   *
   *
   */
  public void standardDropTxCopy(PageEntry pe, boolean asTransaction)
    throws Exception
  {
    DealEditControl dec = theSessionState.getDec();

    int txLevel = pe.getTxCopyLevel();

    if (dec == null || pe.getTxCopyLevel() <= 0)
    {
      // isn't one!
      return;
    }

    try
    {
      if (asTransaction) srk.beginTransaction();
      dec.dropToTxLevel(txLevel - 1, srk, pe);
      pe.setPageDealCID(dec.getCID());

      // if no copies remain nuke the dec object from session
      if (dec.isTxCopy() == false)
          theSessionState.setDec(null);
      if (asTransaction)
        srk.commitTransaction();
    }
    catch(Exception e)
    {
      if (asTransaction) srk.cleanTransaction();
      logger.error("Exception @standardDropTxCopy");
      logger.error(e);
      throw new Exception("Exception @standardDropTxCopy");
    }
  }


  /**
   *
   *
   */
  public int standardCopySelection(SessionResourceKit srk, PageEntry pg)
    throws Exception
  {
    SysLogger logger = srk.getSysLogger();

    logger.debug("@PHC.standardCopySelection: IN, CID=" + pg.getPageDealCID() + " (will be determined even if already set [!= -1])");

    int cid = - 1;
    Deal deal = new Deal(srk, null);

    // get recommended scenarion (if one)
    try
    {
      deal.setSilentMode(true);
      deal = deal.findByRecommendedScenario(new DealPK(pg
                   .getPageDealId(), - 1), true);
      cid = deal.getCopyId();
    }
    catch(Exception e)
    {
      cid = - 1;
    }

    if (cid == - 1)
    {
      // determine gold copy!
      MasterDeal md = new MasterDeal(srk, null);
      md = md.findByPrimaryKey(new MasterDealPK(pg.getPageDealId()));
      cid = md.getGoldCopyId();
    }

    pg.setPageDealCID(cid);
    logger.debug("@PHC.standardCopySelection: CID=" + cid);
    return cid;
  }


    /**
     * 
     * 
     */
    public int accessByPage(PageEntry nextPage,
            SessionStateModel theSessionState, SavedPagesModel savedPages,
            SessionResourceKit srk, boolean setNoAccessMessage)
            throws Exception {

        ACM acm = theSessionState.getAcm();

        int institutionProfileId = nextPage.getDealInstitutionId();
        if (PageInstitutionUtil.getInstitutionUsage(nextPage.getPageId()) 
                == InstitutionUsage.ALL_OF_THE_USER_INSTITUTIONS) {
            institutionProfileId = theSessionState.getUserInstitutionId();
        }
        AccessResult arUser = acm.pageAccess(nextPage.getPageId(),
                theSessionState.getLanguageId(), institutionProfileId);
        int accessTypeUser = arUser.getAccessTypeID();

        if (accessTypeUser == Sc.PAGE_ACCESS_DISALLOWED
                && setNoAccessMessage == true) {
            // no access
            setActiveMessageToAlert(BXResources.getSysMsg(PAGE_DENIED_DEFAULT,
                    theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMCONFIRM);
        }

        return accessTypeUser;

    }


  /**
   *
   *
   */
  ////public int (Deal deal, PageEntry nextPage, SessionState theSession, SavedPages savedPages, SessionResourceKit srk, int accessType, boolean setNoAccessMessage)
  public int accessByDealStatus(Deal deal, PageEntry nextPage, SessionStateModel theSessionState, SavedPagesModel savedPages, SessionResourceKit srk, int accessType, boolean setNoAccessMessage)
    throws Exception
  {
    int accessTypeStatus = accessType;

    if (nextPage.isSubPage() == false)
    {
      DealStatusManager dsm = DealStatusManager.getInstance(srk);
      AccessResult arStatus = dsm.pageAccess(nextPage.getPageId(), deal.getStatusId(),
        deal.getDecisionModificationTypeId(), nextPage.getNavigationMode() == 1, theSessionState.getLanguageId(), srk);

      // ALERT 2nd last parm
      accessTypeStatus = arStatus.getAccessTypeID();
    }

    // set active message if access not allowed
    if (!(accessTypeStatus == Sc.PAGE_ACCESS_VIEW_ONLY || accessTypeStatus == Sc.PAGE_ACCESS_EDIT) && setNoAccessMessage == true)
    {
      //--Release2.1--//
      // Modified to pass LanguageId for Multilingual handling
      ActiveMessage am = ActiveMsgFactory.getActiveMessage(ActiveMsgFactory.ISCUSTOMCONFIRM, theSessionState.getLanguageId());
      am.setDialogMsg(BXResources.getSysMsg(PAGE_DENIED_DEFAULT, theSessionState.getLanguageId()));
      theSessionState.setActMessage(am);
    }
    return accessTypeStatus;
  }


  /**
   *
   *
   */
  public PageCursorInfo getPageCursorInfo(PageEntry pg, String pciName)
  {
    Hashtable pst = pg.getPageStateTable();

    if (pst == null)
    {
      pst = new Hashtable();
      pg.setPageStateTable(pst);
      return null;
    }

    PageCursorInfo pci =(PageCursorInfo) pst.get(pciName);
    return pci;
  }


  /**
   *
   *
   */
  ////public void storeCurrPageAsBackPage(SessionState theSession, SavedPages savedPages)
  public void storeCurrPageAsBackPage(HttpSession theSession, SavedPagesModelImpl savedPages)
  {
    ////PageEntry pageEntry = theSession.getCurrentPage();
    PageEntry pageEntry = (PageEntry) theSessionState.getCurrentPage();
    if ((pageEntry == null) ||("".equals(pageEntry.getPageName())))
    {
      return;
    }
    savedPages.setBackPage(pageEntry);
  }


  /**
   *
   *
   */
  ////public void storeCurrPageAsWorkQueue(SessionState theSession, SavedPages savedPages)
  public void storeCurrPageAsWorkQueue(HttpSession theSession, SavedPagesModelImpl savedPages)
  {
    ////PageEntry pageEntry = theSession.getCurrentPage();
    PageEntry pageEntry = (PageEntry) theSessionState.getCurrentPage();
    savedPages.setWorkQueue(pageEntry);
  }

  /**
   *
   *
   */
  public void setWorkQueueRefresh(boolean refresh)
  {
    PageEntry iworkqueue = savedPages.getWorkQueue();
    if (iworkqueue == null) return;

    Hashtable pagest = iworkqueue.getPageStateTable();
    if (pagest != null)
    {
      pagest.put("REFRESH", new Boolean(refresh));
    }
  }

  /**
   *
   *
   */
  ////protected void customizeDynamicLPRCombos(String lenderCbName, String productCbName, String postedRateCbName)
  protected void customizeDynamicLPRCombos(String lenderCbName,
                                           String productCbName,
                                           String postedRateCbName,
                                           String formName)
  {
    try
    {
      ComboBox cblender =(ComboBox) getCurrNDPage().getDisplayField(lenderCbName);
      ComboBox cbproduct =(ComboBox) getCurrNDPage().getDisplayField(productCbName);
      ComboBox cbpostrate =(ComboBox) getCurrNDPage().getDisplayField(postedRateCbName);

      String fullQualifiedCbProdName = formName + productCbName;
      String fullQualifiedCbPostRateName = formName + postedRateCbName;

      //logger.debug("PHC@customizeDynamicLPRCombos::FullProductName: " + fullQualifiedCbProdName);
      //logger.debug("PHC@customizeDynamicLPRCombos::FullRateName: " + fullQualifiedCbPostRateName);

      ////String onChangeLender = new String("onChange=\"getList('" + productCbName + "')\"");
      ////String onChangeProduct = new String("onChange=\"getList('" + postedRateCbName + "')\"");

      String onChangeLender = new String("onChange=\"getList('" + fullQualifiedCbProdName + "')\"");
      String onChangeProduct = new String("onChange=\"getList('" + fullQualifiedCbPostRateName + "')\"");
      String onChangeRate = new String("onChange=\"populateRateCode()\"");

      cblender.setExtraHtml(onChangeLender);
      cbproduct.setExtraHtml(onChangeProduct);
      cbpostrate.setExtraHtml(onChangeRate);

      //logger.debug("PHC@customizeDynamicLPRCombos:: finished customization");
    }
    catch(Exception e)
    {
      setStandardFailMessage();
      logger.error("Exception PageHandlerCommon@CustomizeComboBox: Problem encountered in combobox customization:" + e);		//#DG702
    }
  }

  /**
   *
   *
   */
  protected void customizeTextBox(String tbName)
  {
    try
    {
      ////CSpTextBox textbox =(CSpTextBox) getCurrNDPage().getDisplayField(tbName);
      TextField textbox = (TextField) getCurrNDPage().getDisplayField(tbName);

      // 1. Customize colors, borders, etc.,
      String changeColorStyle = new String("style=\"background-color:d1ebff\"" + " style=\"border-bottom: hidden d1ebff 0\"" + " style=\"border-left: hidden d1ebff 0\"" + " style=\"border-right: hidden d1ebff 0\"" + " style=\"border-top: hidden d1ebff 0\"" + " style=\"font-weight: bold\"");

      // 2. Disable edition
      String disableEdition = new String(" disabled");
      ////textbox.setExtraHtml(changeColorStyle + disableEdition);
      textbox.setExtraHtml(changeColorStyle + disableEdition);
    }
    catch(Exception e)
    {
      setStandardFailMessage();
      logger.error("Exception PageHandlerCommon@CustomizeTextBox: Problem encountered in combobox customization:" + e);		//#DG702
    }

  }


  /**
   *
   *
   */
  protected void customizeTextBoxWithAccess(String tbName)
  {
    try
    {
      ////CSpTextBox textbox =(CSpTextBox) getCurrNDPage().getDisplayField(tbName);
      TextField textbox =(TextField) getCurrNDPage().getDisplayField(tbName);

      // 1. Customize colors, borders, etc.,
      String changeColorStyle = new String("style=\"background-color:d1ebff\"" + " style=\"border-bottom: hidden d1ebff 0\"" + " style=\"border-left: hidden d1ebff 0\"" + " style=\"border-right: hidden d1ebff 0\"" + " style=\"border-top: hidden d1ebff 0\"" + " style=\"font-weight: bold\"" + " style=\"font-size: xx-small\"");
      textbox.setExtraHtml(changeColorStyle);
    }
    catch(Exception e)
    {
      setStandardFailMessage();
      logger.error("Exception PageHandlerCommon@CustomizeTextBox: Problem encountered in combobox customization." + tbName+":" + e);		//#DG702
    }

  }

  protected void changeColorOnly(String tbName)
  {

    try
    {
      ////CSpTextBox textbox = (CSpTextBox)getCurrNDPage().getDisplayField(tbName);
      TextField textbox = (TextField)getCurrNDPage().getDisplayField(tbName);

      // 1. Customize colors, borders, etc.,
      String changeColor = new String("style=\"background-color:d1ebff\"" +
                                      " style=\"font-weight: bold\"");

      logger.debug("TextboxString: " + changeColor);
      logger.debug("TextboxName: " + textbox);

      textbox.setExtraHtml(changeColor);

    }
    catch(Exception e)
    {
      setStandardFailMessage();
      logger.error("Exception PageHandlerCommon@changeColorOnly: Problem encountered in textbox change color method:" + e);		//#DG702
    }
  }

  protected void closeAccessToCheckBox(String chbName)
  {
    try
    {
      ////CSpCheckBox checkBox = (CSpCheckBox)getCurrNDPage().getDisplayField(chbName);
      CheckBox checkBox = (CheckBox)getCurrNDPage().getDisplayField(chbName);

      //Disable edition
      String disableEdition = new String(" disabled");
      checkBox.setExtraHtml( disableEdition );

    }
    catch(Exception e)
    {
      setStandardFailMessage();
      logger.error("Exception PageHandlerCommon@closeAccessToCheckBox: Problem encountered in checkbox customization:" + e);		//#DG702
    }
  }

  protected void closeAccessToTextBox(String tbName)
  {
    try
    {
      ////CSpTextBox textBox = (CSpTextBox)getCurrNDPage().getDisplayField(tbName);
      TextField textBox = (TextField)getCurrNDPage().getDisplayField(tbName);

      // 1. Change style.
      String changeLetterStyle = new String(" style=\"font-weight: bold\"");

      // 2. Disable edition
      String disableEdition = new String(" disabled");

      textBox.setExtraHtml(changeLetterStyle + disableEdition);
      //// Obsolete ND specific clean up. No need anymore.
      ////getCurrNDPage().clearBeforeDisplay();
    }
    catch(Exception e)
    {
      setStandardFailMessage();
      logger.error("Exception PageHandlerCommon@closeAccessToTextBox: Problem encountered in textbox customization:" + e);		//#DG702
    }
  }

  /**
    *  This method is adjusted to the JATO architecture. It is used to pass an
   *  appropriate information the HTML page in Lender-->Product-->Rate module. This module
   *  is served by the Servlet running into the JRun container. In JATO model each DisplayField
   *  (ComboBox, TextBox, etc.) is a single object. It allows to render and pass the values from
   *  these fields without the reloading of the whole page. It means that the whole Servet BasisXpress
   *  implementation should be redesign in consistency with the JATO Control Servlet and DisplayField
   *  implementation.
   */
  ////public void handleStaticPassToHtml(ViewBean pg, CSpDisplayEvent event, String staticFieldName, String doName, String dataFieldName)
  //--> Shouldn't execute the DataObject again for optimization
  //--> Modified by Billy 17July2003
  public void handleStaticPassToHtml(ViewBean pg, ChildDisplayEvent event)
  {
    PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();
    doDealEntryMainSelectModel theModel = (doDealEntryMainSelectModel)
        RequestManager.getRequestContext().getModelManager().getModel(doDealEntryMainSelectModel.class);

    int lenderProfileId = 0;
    try
    {
      if(theModel.getSize() > 0)
      {
        Object fieldValue = theModel.getDfLenderProfile();
        if(fieldValue != null)
        {
          lenderProfileId = new Integer(fieldValue.toString()).intValue();
          //logger.debug("PHC@setPhoneNoDisplayField::OnBeforeRowDisplayEvent_dfValueFromModel: " + new Integer(fieldValue.toString()).intValue());
        }
      }
    }
    catch(Exception e)
    {
      logger.debug("PHC@handleStaticPassToHtml::Exception: " + e);
      lenderProfileId = 0;
    }

    String fieldName = (String) event.getChildName();
    DisplayField fld = (DisplayField) pg.getChild(fieldName);

    //logger.debug("PHC@handleStaticPassToHtml::fieldName: " + fieldName);
    //logger.debug("PHC@handleStaticPassToHtml::fld: " + fld);

    fld.setValue((new Integer(lenderProfileId)).toString());
  }

  /**
    *  This method is used to pass an
   *  appropriate information the HTML page in Lender-->Product-->Rate module. This module
   *  is served by the Servlet running into the JRun container. In JATO model each DisplayField
   *  (ComboBox, TextBox, etc.) is a single object. It allows to render and pass the values from
   *  these fields without the reloading of the whole page. It means that the whole Servet BasisXpress
   *  implementation should be redesign in consistency with the JATO Control Servlet and DisplayField
   *  implementation.
   */
  protected String convertDateToServletFormat(GregorianCalendar date)
  {
    StringBuffer result = new StringBuffer();
    //Fill in MMM with the trailing space
    int month = date.get(Calendar.MONTH);

    try
    {
      result.append(intMonthToString(month + 1) + " ");
    }
    catch(IllegalArgumentException e)
    {
      //handle this somehow
    }

    //try
    //Fill in DD with the trailing space; if one digit number is returned, "beef" it up
    int day = date.get(Calendar.DAY_OF_MONTH);

    if (day >= 10)
    {
      result.append(day + " ");
    }
    else
    {
      result.append("0" + day + " ");
    }
    //if Fill in YYYY with the trailing space and '|' and space again
    int year = date.get(Calendar.YEAR);

    result.append(year + " | ");
    //Fill in HH with the trailing colon
    int hour = date.get(Calendar.HOUR_OF_DAY);

    if (hour >= 10)
    {
      result.append(hour + ":");
    }
    else
    {
      result.append("0" + hour + ":");
    }

    //if Fill in MM with the trailing space
    int minute = date.get(Calendar.MINUTE);

    if (minute >= 10)
    {
      result.append(minute + " ");
    }
    else
    {
      result.append("0" + minute + " ");
    }
    //if Fill in the time zone's abbreviature with the trailing space, pipe, and space
    result.append(date.getTimeZone().getDisplayName(false, 0) + " | ");

    //Fill in the seconds with the trailing colon
    int second = date.get(Calendar.SECOND);

    if (second >= 10)
    {
      result.append(second + ":");
    }
    else
    {
      result.append("0" + second + ":");
    }

    //if Fill in the milliseconds
    int millisecond = date.get(Calendar.MILLISECOND);

    if (millisecond < 10)
    {
      result.append("00" + millisecond);
    }
    else if (millisecond < 100)
    {
      result.append("0" + millisecond);
    }
    else
    {
      result.append(millisecond);
    }

    return result.toString().trim();
  }

  /**
   *
   *
   */
  private String intMonthToString(int month)
    throws IllegalArgumentException
  {
    switch(month)
    {
      case 1 : return "Jan";
      case 2 : return "Feb";
      case 3 : return "Mar";
      case 4 : return "Apr";
      case 5 : return "May";
      case 6 : return "Jun";
      case 7 : return "Jul";
      case 8 : return "Aug";
      case 9 : return "Sep";
      case 10 : return "Oct";
      case 11 : return "Nov";
      case 12 : return "Dec";
      default : throw new IllegalArgumentException("Month is not in the range 1-12!");
    }
  }


  /**
   *  This is one of the base methods of the BasisXpress framework. It is recoded in accordance
   *  with JATO framework model. See comments below.
   *
   */
  ////public int postHandlerProtocol()
  public void postHandlerProtocol()
  {
    //logger = SysLog.getSysLogger("PHC@PostHandler");
    ////PageEntry pg = theSession.getCurrentPage();

    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    logger.debug("@PHC.postHandlerProtocol: IN :: page = <" + pg.getPageName() + ">");

    //Since 3.3 ML: apply institutionId for the next screen, 
    //Feb 21, 2008 - Hiro
    _log.debug("before calling enforceInstitutionId @postHandlerProtocol");
    enforceInstitutionId(pg);
    _log.debug("after calling enforceInstitutionId @postHandlerProtocol");
    // check for session ended (if so do nothing)
    //// !!IMPORTANT!! this is a very important BasisXpress/JATO pattern
    //// Add it with more description how it works in the JATO framework.
    ////if (CSpider.getUserSession() == null) return CSpPage.PROCEED;

    //--> Exception will throw in SunOne server when getting HTTPSession after logoff
    //--> Added by Billy 18Dec2002
    try{
      HttpSession theSession = RequestManager.getRequestContext().getRequest().getSession(false);

      //logger.debug("TheSession from PostHandler: " + theSession);
      if (theSession == null)
      {
        //return;
        logger.debug("@PHC.postHandlerProtocol : HttpSession is null. Most likely User Logging Off !! ");
        throw new CompleteRequestException();
      }
      logger.debug("@PHC.postHandlerProtocol : HttpSession ID = : " + theSession.getId());
    }
    catch(Exception e)
    {
      logger.warning("@PHC.postHandlerProtocol : Exception when getting HttpSession. "+
          "Most likely User Logging Off !! :: " + e.toString());
      //--> Test by Billy 19Dec2002
      //return;
      throw new CompleteRequestException();
    }
    //================================================================================

    // restore audit production (in case handler termporarilly disabled)

    //logger.debug("@PHC.postHandlerProtocol:Stamp1");

    //--> Check if it's null just in case
    if(srk != null)
      getSessionResourceKit().restoreAuditOn();

    //logger.debug("@PHC.postHandlerProtocol:Stamp2");
    //// !!IMPORTANT!! this is a very important BasisXpress/JATO pattern
    //// Add how it should be recoded with more description how it works in
    //// the JATO framework after the final testing of the converted framework.

    // allows avoidance of redundant calls to setSetupBeforeGeneration() (e.g.
    // in cases where a page has bindings to multiple data objects on the
    // main page. To accomplish this the meth should be coded as follows:
    //
    //      public void setupBeforePageGeneration()
    //      {
    //          PageEntry pg = getTheSession().getCurrentPage();
    //
    //          if (pg.getSetupBeforeGenerationCalled() == true)
    //              return;
    //
    //          pg.setSetupBeforeGenerationCalled(true);
    //
    //          // -- //
    //
    //          .... remainder of method to follow this ...
    //
    pg.setSetupBeforeGenerationCalled(false);
    //logger.debug("@PHC.postHandlerProtocol:Stamp3");

    //--> Check if it's null just in case
    if(srk != null)
      unsetSessionResourceKit();
    //logger.debug("@PHC.postHandlerProtocol:Stamp4");

    // update session objects
    //--> Check if the session is valide -- By Billy 18July2002
    try{
      //logger.debug("@PHC.postHandlerProtocol:Stamp5");
      updateSessionObjects();
    }
    catch(IllegalStateException ie)
    {
      logger.info("HttpSession invalide (removed) :: Most likely user logging off the system. :: " + ie.toString());
      //--> Test by Billy 19Dec2002
      //return;
      throw new CompleteRequestException();
    }
    //========================================================

    // load/display next/current page

    //// This is totally re-considered in accordancy with the JATO MVC
    //// architecture (see comments below as well). There is no difference
    //// in JATO between load or display, so this difference at the end is eliminated.
    //// The generic target ViewBean is calling based on the name of the target from
    //// the GOTO functionality and JATO framework ViewBean invocation.
  ////CSpPage thePage = CSpider.getPage(pg.getPageName());

    //logger.debug("@PHC.postHandlerProtocol:Stamp6");
    String viewBeanPrefix = pg.getPageName();
    //logger.debug("PHC@PostHandlerProtocol::ViewBeanPrefix: " + viewBeanPrefix);

    ViewBean thePage = null;

    try
    {
        thePage = RequestManager.getRequestContext().getViewBeanManager().getLocalViewBean(viewBeanPrefix);
        logger.debug("PHC@PostHandlerProtocol::ThePageFromRequest: " + thePage);
    }
    catch(ClassNotFoundException e)
    {
        logger.error("Exception PHC@PostHandlerProtocol, forwarding to: " + viewBeanPrefix);
        logger.error(e);
    }
    //logger.debug("@PHC.postHandlerProtocol:Stamp7");

    thePage.forwardTo(RequestManager.getRequestContext());
  }


  /**
   *
   *
   */
  public void pageGetState(ViewBean currNDPage)
  {

    // check for session ended (if so do nothing)
    ////if (CSpider.getUserSession() == null) return;
    HttpSession theSession = RequestManager.getRequestContext().getRequest().getSession();
    if (theSession == null)
          return;

    memberInit();

    this.currNDPage = currNDPage;
    getSessionObjects();
    // create resource kit
    ////setupSessionResourceKit("" + theSession.getSessionUserId());
    setupSessionResourceKit("" + theSessionState.getSessionUserId());
    return;
  }


  /**
   *
   *
   */
  public void pageSaveState()
  {
    // check for session ended (if so do nothing)
    ////if (CSpider.getUserSession() == null) return;
    HttpSession theSession = RequestManager.getRequestContext().getRequest().getSession();
    if (theSession == null)
          return;

    // update session objects
    updateSessionObjects();
    // free resources
    unsetSessionResourceKit();
    return;
  }


  /**
   *
   *
   */
  public void handleOkStandard()
  {
    handleCancelStandard(false);
  }


  /**
   *
   *
   */
  public void handleCancelStandard()
  {
    ////PageEntry pg = getTheSession().getCurrentPage();
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
    int pageid = pg.getPageId();
    handleCancelStandard(true);
  }

  /**
   *
   * New method to reload the page if the ToggleLanguage link is touched.
   * HttpSession fragment still commented out to test the following
   * adjustments.
   */
  public void handleToggleLanguage()
  {
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
    ////HttpSession session = RequestManager.getRequestContext().getRequest().getSession();

    int pageid = pg.getPageId();

    ////logger.debug("PHC@handleToggleLanguage:PageId: " + pageid);

    int languageId = theSessionState.getLanguageId();
    logger.debug("PHC@handleToggleLanguage:Current LanguageId: " + languageId);


    if(languageId == Mc.LANGUAGE_PREFERENCE_FRENCH)
    {
       theSessionState.setLanguageId(Mc.LANGUAGE_PREFERENCE_ENGLISH);
       ////session.setAttribute("LANGUAGE", "english");
       
       //Added by Clement for MIPremium cal
       srk.setLanguageId(Mc.LANGUAGE_PREFERENCE_ENGLISH);
    }
    
    if(languageId == Mc.LANGUAGE_PREFERENCE_ENGLISH)
    {
      theSessionState.setLanguageId(Mc.LANGUAGE_PREFERENCE_FRENCH);
      ////session.setAttribute("LANGUAGE", "french");
      
      //Added by Clement for MIPremium cal
      srk.setLanguageId(Mc.LANGUAGE_PREFERENCE_FRENCH);
    }

    ////logger.debug("PHC@handleToggleLanguage:Final LanguageId: " + theSessionState.getLanguageId());
  }

  /**
   *
   *
   */
  public void handleCancelStandard(boolean warn)
  {
logger.debug("PHC@handleCancelStandard: before jumpTrap");
    if (warn == true && jumpTrap(JumpState.UNCONFIRMEDCANCEL) == true) return;
logger.debug("PHC@handleCancelStandard: after jumpTrap");
    // message set in this case
    ////PageEntry pe = theSession.getCurrentPage();
    PageEntry pe = (PageEntry) theSessionState.getCurrentPage();

    // Reset the modified flag ==> as we populating the modified from child to parent flag
    //    -- By BILLY 15Nov2001
    pe.setModified(false);
logger.debug("PHC@handleCancelStandard: after set false");

    if (pe.isSubPage() == false)
    {
logger.debug("PHC@handleCancelStandard: sub-page false");
      // just navigate away from the page - transaction copy (or never submitted deal - deal
      // entry) will be cleaned up automatically
      navigateAwayFromFromPage(false);
      return;
    }

    // for editable deal sub-page delete transactional copy (if one)
    boolean isFailed = false;
    try
    {
      standardDropTxCopy(pe, true);

      //--DJ_LDI_CR--start--//
      // Force to 'Recalculate' DLLs and hide 'Submit' button until the action.
      if(pe.getPageId() == Mc.PGMN_LIFE_DISABILITY_INSURANCE)
      {
logger.debug("PHC@handleCancelStandard: LDI page case");
        theSessionState.setIsDisplayLifeDisabilitySubmit(false);
      }
      //--DJ_LDI_CR--end--//
    }
    catch(Exception e)
    {
      logger.error("Exception @handleCancelStandard - dropping tx copy: navigate away from page");
      logger.error(e);
      pe.setSubPage(true);

      // pretend to be main page so exit all the way out!
      isFailed = true;
    }

    // now just navigate away from page - ie. return to parent or all the way out if exception
    // occured (see above) - alert message generated in latter case

    logger.debug("Katya: PHC@handleCancelStandard before navigateAwayFromFromPage");
    navigateAwayFromFromPage(isFailed);
  }


  /**
   *
   *
   */
  public void handleUnSupportedCalls()
  {
    logger.debug("@PHC.handleUnSupportedCalls");

    // if user has no work queue go to deal search
    if (savedPages.getWorkQueue() == null || savedPages.getWorkQueue().getPageId() == 0)
    {
      handleDealSearch();
      return;
    }


    // goto work queue
    handleDisplayWorkQueue();

  }


  /**
   * This method should be redesigned as there is no straight forward mapping from
   * CSpDataDrivenVisualEvent to JATO. It should be done along with the JATO's implementation
   * of the tiled views except repeatable and its derivitive PageCursorInfo tds. All default
   * ND buttons implemented in JATO as well. Temporarily commented out for compilation purposes
   * only.
   *
   * This method must be separated into the new TiledViewHandlerCommon class.
   * The First/Next/Previous/Last ND navigation model should be reimplemented as
   * an BeanAdapter Model. Current modification is more the compilation/testing.
   *
   * This method as well as other related to the TiledView JATO framework implementation
   * should be separated into the new BasisXpress TiledView Utility family.
   *
   */
  public boolean handleForwardBackwardCommon(PageEntry pg, PageCursorInfo tds, int numRows, boolean forward)
  {
    //// Obsolete name, no need in JATO world, should be renamed.
    ////pg.setPageDisplayMethod(Mc.DISPLAY_METHOD_DISP);

    ////CSpDataDrivenVisual vo =(CSpDataDrivenVisual)(getCurrNDPage().getCommonRepeated(tds.getVoName()));
    String voName = tds.getVoName();
    //logger.debug("PHC@VoName: " + voName);

    //--> What is this ??? Need Vlad's expalination ????
    //--> Modified by BILLY 22July2002
    //pgIWorkQueueRepeated1TiledView vo = ////(pgIWorkQueueRepeated1TiledView) getViewBean(pgIWorkQueueRepeated1TiledView.class);
    //                    (pgIWorkQueueRepeated1TiledView) RequestManager.getRequestContext().getViewBeanManager().getViewBean(pgIWorkQueueRepeated1TiledView.class);

    RequestHandlingTiledViewBase vo = (RequestHandlingTiledViewBase)(getCurrNDPage().getChild(voName));
    //==============================
    //logger.debug("PHC@TiledViewBean: " + vo);
    //logger.debug("PHC@TiledViewName: " + vo.getName());

    // num rows = -1 is used by caller that does not wish record number checking (e.g.
    // force first page display would be done elsewhere, or not necessary).
    if (numRows != - 1)
    {
      tds.setTotalRows(numRows);
      if (tds.isTotalRowsChanged() == true)
      {
        // number of rows has changed - unconditional display of first page
        tds.setCurrPageNdx(0);
        ////vo.goToFirst();
        //// Reset the primary model to the "before first" state and resets the display index.
        try
        {
          vo.resetTileIndex();
        }
        catch (ModelControlException mce)
        {
            logger.debug("MCException from PHC@handleForwardBackwardCommon: " + mce);
        }

        return false;
      }
    }

    if (forward)
    {
      // advance page
      tds.nextPage();
      ////vo.goToNext();
      //// Increment the current tile position to the next available tile position.
    }
    else
    {
      // advance page
      tds.prevPage();
      ////vo.goToPrev();
      //// Reset the primary model to the "before first" state and resets the display index.
      }
    return true;
  }

  /**
   *
   *
   */
  public void handleActMessageOK()
  {
    String [ ] fakeArgs = new String [ 2 ];
    fakeArgs [ 0 ] = "OK";
    fakeArgs [ 1 ] = "9999";
    handleActMessageOK(fakeArgs);
  }

  /**
   *
   *
   */
  public void handleActMessageOK(String[] args)
  {
    logger.debug("@PHC.handleActMessageOK :: numArgs=" +((args == null) ? "<none>" : "" + args.length));
    try
    {
      // get message type from active message object
      //
      // Note: not all active message object generate the OK event - we only need to
      //       cover those that do.
      ////ActiveMessage acm = theSession.getActMessage();
      ActiveMessage acm = theSessionState.getActMessage();

      int type = theSessionState.getActMessage().getMsgType();

logger.debug("@PHC.handleActMessageOK :: MessageType: " + type);
logger.debug("@PHC.handleActMessageOK :: MessageTypeFromActiveMsgFactory: " + ActiveMsgFactory.ISJUMP);
        /**
         * BEGIN FIX FOR Re-directed to IWQ when leaving Deal Entry screen[FXP-20999]
         * Mohan V Premkumar
         * 03/31/2008
         */
        JumpState js = theSessionState.getJmpState();
        /**
         * END FIX FOR Re-directed to IWQ when leaving Deal Entry screen[FXP-20999]
         */
        /**
         * BEGIN FIX FOR FXP21217
         * Mohan V Premkumar
         * 03/31/2008
         */
        if ((ActiveMsgFactory.ISJUMP == type) || (type == ActiveMsgFactory.ISDEALENTRYCONFIRMCANCEL))
      {
        /**
         * END FIX FOR FXP21217
         */
        // user continuing jump action ... get jump state to determine how to proceed
        

//temp
logger.debug("@PHC.handleActMessageOK ::JumpStateClass: " + js.getClass());
logger.debug("@PHC.handleActMessageOK ::JumpStateType: " + js.getJumpType());
logger.debug("@PHC.handleActMessageOK ::PreviousNdx: " + js.getPreviousNdx());

        switch(js.getJumpType())
        {
          case JumpState.GOTO : finishGoPage(js.getGotoPageId(), js.getGotoDealId());
          return;
          case JumpState.PREVIOUSSCREENS : handleOpenDialogLink(js.getPreviousNdx(), true);
          return;
          case JumpState.SEARCH : handleDealSearch(true);
          return;
          case JumpState.TOOLS : handleChangePassword(true);
          return;
          case JumpState.HISTORY : handleDisplayDealHistory(true);
          return;
          case JumpState.NOTES : handleDisplayDealNotes(true);
          return;
          case JumpState.WORKQUEUE : handleDisplayWorkQueue(true);
          return;
          case JumpState.TASKNEXTPAGE : handleNextTaskPage(true);
          return;
          case JumpState.TASKPREVPAGE : handlePrevTaskPage(true);
          return;
          case JumpState.LOGOFF : handleSignOff(true, false);
          return;

          // ALERT - help not done yet - may not need to be (opens browser window)
          // case JumpState.HELP:
          //    return;
          default : throw new Exception("Unknown jump type exception.");
        }
      }
      /**
       * BEGIN FIX FOR Re-directed to IWQ when leaving Deal Entry screen[FXP-20999]
       * Mohan V Premkumar
       * 03/31/2008
       */
      if (type == ActiveMsgFactory.ISPAGECONFIRMCANCEL)
      {
        logger.debug("@PHC.handleActMessageOK ::Confirm Cancel case");
        logger.debug("Katya@PHC.handleActMessageOK ::type = "
          + "ISPAGECONFIRMCANCEL" );
        handleCancelStandard(false);
        return;
      }
      /**
       * END FIX FOR Re-directed to IWQ when leaving Deal Entry screen[FXP-20999]
       */
       // New handle for Exit screen and continue w/ workflow -- By BILLY 07May2002
       if (type == ActiveMsgFactory.ISPAGECONFIRMEXIT || type == ActiveMsgFactory.ISDEALENTRYCONFIRMEXIT)
       {
        logger.debug("@PHC.handleActMessageOK ::PageConfirmExit case");
        handleExitStandard(false);
         return;
       }

      if (ActiveMsgFactory.ISFATAL == type || ActiveMsgFactory.ISCUSTOMFATAL == type || ActiveMsgFactory.ISCONFIRMENDSESSIONDIALOG == type)
      {
        //logger.debug("@PHC.handleActMessageOK ::SignOff case");
        handleSignOff(true, true);
        return;
      }

      if (ActiveMsgFactory.ISEDITSUBPAGE == type)
      {
        //logger.debug("@PHC.handleActMessageOK ::SubPage case");
        handleDisplayParentPage();
        return;
      }

      if (ActiveMsgFactory.ISCUSTOMDIALOG == type || ActiveMsgFactory.ISCUSTOMDIALOG2 == type
        || ActiveMsgFactory.ISYESNO == type)                              // Catherine, 26-Apr-05, GECF, new dialog screen

      {
        logger.debug("@PHC.handleActMessageOK ::IsCustomDialog case");
        logger.debug("@PHC.handleActMessageOK ::args[0]: " + args[0]);
        logger.debug("@PHC.handleActMessageOK ::args[1]: " + args[1]);

        handleCustomActMessageOk(args);

        return;
      }

      if (ActiveMsgFactory.ISCUSTOMLEAVEPAGE == type)
      {
        //logger.debug("@PHC.handleActMessageOK ::Cancel Standard case");
        handleCancelStandard(false);
      }

      if (ActiveMsgFactory.ISNODIALOG == type)
      {
        logger.debug("@PHC.handleActMessageOK ::NoDialog case");
        handleNoDisplayActMessage(true);
        return;
      }
    }
    catch(Exception ex)
    {
      setStandardFailMessage();
      logger.error("Exception occurred @ handleActMessageOK");
      logger.error(ex);
      return;
    }
  }


  /**
   *
   *
   */
  private void handleDisplayParentPage()
  {
    ////PageEntry pgEntry = theSessionState.getCurrentPage();
    PageEntry pgEntry = (PageEntry) theSessionState.getCurrentPage();

    if (pgEntry == null) return;

    savedPages.setNextPage(pgEntry);

    navigateToNextPage(true);

  }


  /**
   *
   *
   */
  public void handleOpenDialogLink(int queueNdx)
  {
    logger.debug("@PHC.handleOpenDialogLink: skip jump trap=" + skipJumpTrap);

    if (skipJumpTrap == false && jumpTrap(JumpState.PREVIOUSSCREENS, - 1, - 1, queueNdx, - 1) == true) return;

    savedPages.setNextPageAsPreviousPage(queueNdx);

    
    // create a new page entry rather than use queued one - i.e. emulate 'goto' behavior to avoid
    // using state information from saved page entry.
    PageEntry queued = savedPages.getNextPage();
    
    PageEntry pgEntry = setupMainPagePageEntry(queued.getPageId(), 
                                               queued.getPageDealId(), -1, 
                                               queued.getDealInstitutionId());

    // Special handle for goto previous SOB review -- Billy 03Dec2001
    if(queued.getPageId() == Sc.PGNM_SOB_REVIEW)
    {
      // Pass the deal.SourceOfBusinessId to display
      try {
        Deal deal = new Deal(srk, null);
        try{
        deal = deal.findByRecommendedScenario(new DealPK(queued
                      .getPageDealId(), -1), true);
        }
        catch(Exception e)
        {
        logger.warning("@ Setting up SOB for pervious page -- No Recommended Copy found !! Used Gold Copy as fallback. Dealid = " + queued.getPageDealId());
        deal = deal.findByGoldCopy(new DealPK(queued
                      .getPageDealId(), -1));
        }

        // In order to pass the Copyid correctly we need to set it to DealPage
        //    -- ie. in navigateToNextPage method it will set it to -1 if there is no Tx copy
        //            for non-Deal pages.
        pgEntry.setDealPage(true);

        Hashtable subPst = pgEntry.getPageStateTable();
        if(subPst == null)
        {
          subPst = new Hashtable();
          pgEntry.setPageStateTable(subPst);
        }
        //// IMPORTAMT. TODO: Uncomment when this Handler will be adjusted.
        ////subPst.put(SourceBusinessReviewHandler.SOB_ID_PASSED_PARM,
        ////        new String(""+deal.getSourceOfBusinessProfileId()));

        // Set pageCondition 3 to call workfloe trigger when submit
        pgEntry.setPageCondition3(true);
      } catch (Exception e) {
        logger.error("Exception @PageHandlerCommon.handleOpenDialogLink when setting up Task SOB review page :" + e);		//#DG702
      }
    }

    savedPages.setNextPage(pgEntry);
    navigateToNextPage(true);
  }

  /**
   *
   *
   */
  private void handleOpenDialogLink(int queueNdx, boolean skipJumpTrap)
  {
    this.skipJumpTrap = skipJumpTrap;
    handleOpenDialogLink(queueNdx);
    this.skipJumpTrap = false;
  }

  private boolean signOffConfirmed=false;


  /**
   *
   *
   */
  public void handleSignOff(boolean skipJumpTrap, boolean signOffConfirmed)
  {
    this.signOffConfirmed = signOffConfirmed;
    handleSignOff(skipJumpTrap);
    this.signOffConfirmed = false;
  }


  /**
   *
   *
   */
  private void handleSignOff(boolean skipJumpTrap)
  {
    this.skipJumpTrap = skipJumpTrap;
    handleSignOff();
    this.skipJumpTrap = false;
  }

  /**
   *
   *
   */
  public void handleSignOff()
  {
    logger.debug("@PHC.handleSignOff: skip jump trap=" + skipJumpTrap);

    if (skipJumpTrap == false && jumpTrap(JumpState.LOGOFF) == true) return;

    if (signOffConfirmed == false)
    {
      setActiveMessageToAlert(BXResources.getSysMsg(SIGN_OFF_MSG_CONFIRM, theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCONFIRMENDSESSIONDIALOG);
      return;
    }

    // free resources
    unsetSessionResourceKit();
    ViewBean thePage = null;
    try{
      thePage = RequestManager.getRequestContext().getViewBeanManager().getLocalViewBean("pgSessionEnded");
    }
    catch(Exception e)
    {
      logger.error("Exception @PageHandlerCommon.handleSignOff::" + e);		//#DG702
    }

    ////int rc = thePage.forwardTo(getRequestContext());
    thePage.forwardTo( RequestManager.getRequestContext());

    //--> Modified to use SessionService to track the sessions
    //--> By BILLY 11July2002
    ////SessionServices.removeActiveSession(CSpider.getUserSession(), false);
    HttpSession theSession = RequestManager.getRequestContext().getRequest().getSession();
    SessionServices.removeActiveSession(theSession, false);

    // remove session
    ////CSpider.removeUserSession();
    //theSession.removeAttribute("true");
    theSession.invalidate();

    //--> Note: from this point on make sure never access the HTTPSession anymore
    //-->       Need test it because the JATO framework may try to access it somehow !!!
    //======================================================================

    //// Stop the request in its tracks.
    //--> Don't do this -- Commented out by BILLY 11July2002
    //throw new CompleteRequestException();
  }

  /**
   *
   *
   */
  public void handleDisplayWorkQueue()
  {

//temp
logger.debug("PHC@handleDisplayWorkQueue::skipJumpTrap: " + skipJumpTrap);
// SEAN Ticket #1470 Oct. 24, 2005: Unnecessary Debug printout! method jumpTrap will
// trigger the active message box!  invoke here will violate the logic!
//logger.debug("PHC@handleDisplayWorkQueue::JumpState: " + jumpTrap(JumpState.WORKQUEUE));
// END Ticket #1470!

    if (skipJumpTrap == false && jumpTrap(JumpState.WORKQUEUE) == true) return;

    // ensure user has work queue
    if (savedPages.getWorkQueue() == null || savedPages.getWorkQueue().getPageId() == 0)
    {
      setActiveMessageToAlert(BXResources.getSysMsg(NO_WORK_QUEUE_ACCESS, theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
      return;
    }
    savedPages.setNextPageAsWorkQueue(true);
    navigateToNextPage(true);
  }


  /**
   *
   *
   */
  private void handleDisplayWorkQueue(boolean skipJumpTrap)
  {
    this.skipJumpTrap = skipJumpTrap;
    handleDisplayWorkQueue();
    this.skipJumpTrap = false;
  }


  /**
   *
   *
   */
  public void handleDealSearch()
  {
    PageEntry pgEntry = getSavedPages().getSearchPage();

    //logger.debug("PHC@Saved Pages from handleDealSearch: " + savedPages);

    if (skipJumpTrap == false && jumpTrap(JumpState.SEARCH) == true) return;
    // check for search page already saved - if so navigate back to it

    if (pgEntry != null)
    {
      savedPages.setNextPage(pgEntry);
      navigateToNextPage(true);
      return;
    }

    // setup page entry
    pgEntry = setupMainPagePageEntry(Sc.PGNM_DEAL_SEARCH_ID, 0, - 1, theSessionState.getUserInstitutionId());

    // set as 'next' page and navigate
    savedPages.setNextPage(pgEntry);
    navigateToNextPage(true);
  }

  /**
   *
   *
   */
  private void handleDealSearch(boolean skipJumpTrap)
  {
    this.skipJumpTrap = skipJumpTrap;
    handleDealSearch();
    this.skipJumpTrap = false;
  }

  /**
   *
   *
   */
  public void handleDisplayDealNotes()
  {
    if (skipJumpTrap == false && jumpTrap(JumpState.NOTES) == true) return;

    ////int pageId = theSession.getCurrentPage().getPageId();
    ////int pageId = theSessionState.getCurrentPage().getPageId();
    int pageId = ((PageEntry) theSessionState.getCurrentPage()).getPageId();

    Page pEntity = null;
    try
    {
      PDC pdc = PDC.getInstance();
      pEntity = pdc.getPage(pageId);
    }
    catch(Exception e)
    {
      logger.warning("Exception @PHC.handleDisplayDealNotes::" + e);		//#DG702
      setStandardFailMessage();
      return;
    }

    if (!(pEntity.getPageId() == Mc.PGNM_INDIV_WORK_QUEUE_ID) && pEntity.getDealPage() == false)
    {
      setActiveMessageToAlert(BXResources.getSysMsg(FUNCTION_NOT_SUPPORTED, theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
      return;
    }

    ////int id = theSession.getCurrentPage().getPageDealId();
    int id = ((PageEntry) theSessionState.getCurrentPage()).getPageDealId();

    if (id == 0)
    {
      setActiveMessageToAlert(BXResources.getSysMsg(DEALID_MISSING, theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
      return;
    }

    // Setup as sub page if Parent page is DealEntry/Mod (including Applicant and Property,
    //    UWWorkSheet or DealResolution
    //  -- By BILLY 23May2002
    PageEntry pgEntry = null;
    if(pageId == Mc.PGNM_DEAL_ENTRY_ID || pageId == Mc.PGNM_DEAL_MODIFICATION ||
      pageId == Mc.PGNM_APPLICANT_ENTRY || pageId == Mc.PGNM_PROPERTY_ENTRY ||
      pageId == Mc.PGNM_UNDERWRITER_WORKSHEET || pageId == Mc.PGNM_DEAL_RESOLUTION)
    {
      ////pgEntry = setupSubPagePageEntry(theSession.getCurrentPage(), Sc.PGNM_DEAL_NOTES_ID);
      pgEntry = setupSubPagePageEntry(theSessionState.getCurrentPage(), Sc.PGNM_DEAL_NOTES_ID);
    }
    else
    {
     pgEntry = setupMainPagePageEntry(Sc.PGNM_DEAL_NOTES_ID, id, -1, getGotoInstitutionId());
    }

    // set as 'next' page and navigate
    savedPages.setNextPage(pgEntry);
    navigateToNextPage(true);
  }


  /**
   *
   *
   */
  public void handleDisplayDealNotes(boolean skipJumpTrap)
  {
    this.skipJumpTrap = skipJumpTrap;
    handleDisplayDealNotes();
    this.skipJumpTrap = false;
  }


  /**
   *
   *
   */
  public void handleDisplayDealHistory()
  {
    //logger.debug("PHC@handleDisplayDealHistory:: start");
    //logger.debug("PHC@handleDisplayDealHistory::skipJumpTrap: " + skipJumpTrap);
    //logger.debug("PHC@handleDisplayDealHistory::JumpState: " + JumpState.HISTORY);
    //logger.debug("PHC@handleDisplayDealHistory::jumpTrap: " + jumpTrap(JumpState.HISTORY));

    if (skipJumpTrap == false && jumpTrap(JumpState.HISTORY) == true) return;

    ////int pageId = theSession.getCurrentPage().getPageId();
    int pageId = ((PageEntry) theSessionState.getCurrentPage()).getPageId();

    //logger.debug("PHC@handleDisplayDealHistory::PageId: " + pageId);

    Page pEntity = null;
    try
    {
      PDC pdc = PDC.getInstance();
      pEntity = pdc.getPage(pageId);
    }
    catch(Exception e)
    {
      logger.warning("Exception @PHC.handleDisplayDealNotes::" + e);		//#DG702
      setStandardFailMessage();
      return;
    }

    if (!(pEntity.getPageId() == Mc.PGNM_INDIV_WORK_QUEUE_ID) && pEntity.getDealPage() == false)
    {
      setActiveMessageToAlert(BXResources.getSysMsg(FUNCTION_NOT_SUPPORTED, theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
      return;
    }

    ////int id = theSession.getCurrentPage().getPageDealId();
    int id = ((PageEntry) theSessionState.getCurrentPage()).getPageDealId();

    //logger.debug("PHC@handleDisplayDealHistory::PageDealId: " + id);

    if (id == 0)
    {
      setActiveMessageToAlert(BXResources.getSysMsg(DEALID_MISSING, theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
      return;
    }

    PageEntry pgEntry = setupMainPagePageEntry(Sc.PGNM_DEAL_HISTORY_ID, id, - 1, getGotoInstitutionId());

    //logger.debug("PHC@handleDisplayDealHistory::After setupMainPagePageEntry");

    // set as 'next' page and navigate
    savedPages.setNextPage(pgEntry);

    //logger.debug("PHC@handleDisplayDealHistory::After setNextPage");

    navigateToNextPage(true);
    //logger.debug("PHC@handleDisplayDealHistory::After navigateToNextPage");
  }

  /**
   *
   *
   */
  private void handleDisplayDealHistory(boolean skipJumpTrap)
  {
    this.skipJumpTrap = skipJumpTrap;
    handleDisplayDealHistory();
    this.skipJumpTrap = false;
  }


  /**
   *
   *
   */
  public void handleChangePassword()
  {
    if (skipJumpTrap == false && jumpTrap(JumpState.TOOLS, - 1, - 1, - 1, 0) == true) return;

    PageEntry pgEntry = setupMainPagePageEntry(Sc.PGNM_CHANGE_PASSWORD_ID, 0, - 1, theSessionState.getUserInstitutionId());
    pgEntry.setPageCondition1(false);
    // user allowed to cancel change
    pgEntry.setPageState1("");

    HttpSession theSession = RequestManager.getRequestContext().getRequest().getSession();
    // no usage specific message
    // set current page as back page
    storeCurrPageAsBackPage(theSession, savedPages);
    theSessionState.setAuthenticationAttempts(0);

    // set as 'next' page and navigate
    savedPages.setNextPage(pgEntry);
    navigateToNextPage(true);
  }


  /**
   *
   *
   */
  private void handleChangePassword(boolean skipJumpTrap)
  {
    this.skipJumpTrap = skipJumpTrap;
    handleChangePassword();
    this.skipJumpTrap = false;
  }


  /**
   *
   *
   */
  public void handleNextTaskPage()
  {
    if (skipJumpTrap == false && jumpTrap(JumpState.TASKNEXTPAGE) == true) return;

    ////PageEntry pe = theSession.getCurrentPage();

    PageEntry pe = (PageEntry) theSessionState.getCurrentPage();
    PageEntry nextPe = pe.getNextPE();
    if (nextPe == null) return;

    // i.e. regenerate same page
    // set as 'next' page and navigate
    savedPages.setNextPage(nextPe);
    navigateToNextPage(true);
  }


  /**
   *
   *
   */
  private void handleNextTaskPage(boolean skipJumpTrap)
  {
    this.skipJumpTrap = skipJumpTrap;
    handleNextTaskPage();
    this.skipJumpTrap = false;
  }

  /**
   *
   *
   */
  public void handlePrevTaskPage()
  {
    if (skipJumpTrap == false && jumpTrap(JumpState.TASKPREVPAGE) == true) return;

    ////PageEntry pe = theSession.getCurrentPage();

    ////PageEntry pe = theSessionState.getCurrentPage();
    PageEntry pe = (PageEntry) theSessionState.getCurrentPage();

    PageEntry prevPe = pe.getPrevPE();
    if (prevPe == null) return;

    // i.e. regenerate same page
    // set as 'next' page and navigate
    savedPages.setNextPage(prevPe);
    navigateToNextPage(true);
  }

  /**
   *
   *
   */
  private void handlePrevTaskPage(boolean skipJumpTrap)
  {
    this.skipJumpTrap = skipJumpTrap;
    handlePrevTaskPage();
    this.skipJumpTrap = false;
    }

  protected String getTargetViewName()
  {
    //logger = SysLog.getSysLogger("PHC@GetTVB");
    String gotoPageLabel = getGotoPageLabel();
    int gotoPageId = getGotoPageId();

    //logger.debug("PHC@getTargetViewName::GoToPageLabel: " + gotoPageLabel + "  PageId:" + gotoPageId);

    //--> Check if PageId or PageLabel is not selected
    if (gotoPageId == 0 || gotoPageLabel == null) return null;

    // active mesage in this case
    // get (goto) page attributes
    Page gotoPageEntity = null;
    String gotoPageName = "";
    try
    {
      PDC pageCache = PDC.getInstance();
      //logger.debug("PHC@getTargetViewName::pageCache: " + pageCache);

      //--> Modified to get by PageId instead
      //gotoPageEntity = pageCache.getPageByLabel(gotoPageLabel);
      gotoPageEntity = pageCache.getPage(gotoPageId);
      //logger.debug("PHC@getTargetViewName::PageEntity: " + gotoPageEntity);

      gotoPageName = gotoPageEntity.getPageName();
      //logger.debug("PHC@getTargetViewName::PageName: " + gotoPageName);

    }
    catch(Exception ex)
    {
      setStandardFailMessage();
      logger.error("Exception PHC@getTargetViewName:: getting Page Entity: Page Label = " + gotoPageLabel);
      logger.error(ex);
    }
    return gotoPageName;
}

  public void handleGoPage()
  {
      int gotoPageId = getGotoPageId();

      logger.debug("PHC@HandleGoPage: PageId:" + gotoPageId);

      if (gotoPageId == 0) return;

      //apply insutitutionid in case the same dealid exists in different institution.
      _log.debug("before calling enforceInstitutionId @handleGoPage");
      enforceInstitutionId(gotoPageId, getGotoInstitutionId());
      _log.debug("after  calling enforceInstitutionId @handleGoPage");
      
      Page gotoPageEntity = null;
      String dealNumber = "";

      try
      {
          PDC pageCache = PDC.getInstance();
          dealNumber = getDealNumber();
          if (dealNumber != null && !dealNumber.equals("") 
                  && PageInstitutionUtil.isTheScreenNeedsDealId(gotoPageId)) {

              // Serghei.
              if("0".equals(dealNumber)) {
                  setActiveMessageToAlert(BXResources.getSysMsg("PAGE_DENIED_DEFAULT", 
                          theSessionState.getLanguageId()),
                          ActiveMsgFactory.ISCUSTOMCONFIRM);
                  return;
              }

              // get the deal using the deal number in the goto.  
              // Deal is used if the go to entry does not map to a pgViewBean 1-1
              Deal deal = new Deal(srk, null);
              //Start of FXP26408
              deal.setSilentMode(true);
              //End of FXP26408
              deal = deal.findByRecommendedScenario(
                      new DealPK(Integer.parseInt(dealNumber), -1), true);

              gotoPageEntity = pageCache.getPage(gotoPageId, deal);

              // changed gotoPageId (passed into method) to gotoPageEntity.getPageId()
              // these are different in the circumstances when pageCache.
              // getPage(int, int) returns a pg from a factory
              gotoPageId = gotoPageEntity.getPageId();
          }
          else {
              gotoPageEntity = pageCache.getPage(gotoPageId);
          }
      }
      catch(FinderException ex)
      {
          if (!dealNumber.equals("0"))
          {
              setActiveMessageToAlert(BXResources.getSysMsg("PAGE_DENIED_DEFAULT", 
                      theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
              return;
          }
      }
      catch(Exception ex)
      {
          setStandardFailMessage();
          logger.error("Exception @PageHandlerCommon.handleGoPage: getting (goto) Page Entity: Pageid = " + gotoPageId);
          logger.error(StringUtil.stack2string(ex));
      }


      // ensure deal number present if required (usually is)
      String gotoDealNumber = "";

      int gotoDealId = 0;

      if (gotoPageEntity.getDealPage() == true && gotoPageId != PGNM_DEAL_ENTRY_ID
              && PageInstitutionUtil.isTheScreenNeedsDealId(gotoPageId))
      {
          // deal number required - deal centric page
          try
          {
              gotoDealNumber = getGotoDealNumber();
              logger.debug("PHC@HandleGoPage::goToDealNumber: " + gotoDealNumber);
          }
          catch (ClassNotFoundException cnfe)
          {
              logger.debug("PHC@ClassNotFoundException: " + cnfe);
          }

          //check it's not deal add page to create new deal and invalid deal number
          if (gotoDealNumber == null) return;

          // alert message set in this case
          gotoDealId = getGotoDealId(gotoDealNumber);
          if (gotoDealId == - 1) return;

          // active message in this case
      }

      //  jump trap
      if (jumpTrap(JumpState.GOTO, gotoDealId, gotoPageId) == true)
          return;

      try
      {
          finishGoPage(gotoPageId, gotoDealId);
      }
      catch(Exception ex)
      {
          setStandardFailMessage();
          logger.error("Exception @PageHandlerCommon.handleGoPage - calling finishGoPage()");
          logger.error(StringUtil.stack2string(ex));
      }

  }


/////////////////

  /**
   *
   *
   */
  protected int getGotoDealId(String applId)
  {
    int dealId = - 1;
    logger.debug("PHC@getGotoDealId::appIdPassedIn: " + applId);

    try
    {
      String sql = "Select dealId from DEAL where applicationId = '" + applId + "'";
      //logger.debug("PHC@getGotoDealId::SQL: " + sql);

      //// This is a new line because there is no executor obtained otherwise!!
      //// Leave it. VERY TEMP solution to go futher.
      ////SessionResourceKit srk = getSessionResourceKit();

      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(sql);

      //logger.debug("PHC@getGotoDealId::Key: " + key);

      for(; jExec.next(key); )
      {
        dealId = jExec.getInt(key, 1);
        logger.debug("PHC@getGotoDealId::DealId: " + dealId);
        break;
      }
      jExec.closeData(key);
      if (dealId == - 1)
      {
        setActiveMessageToAlert(BXResources.getSysMsg(NO_MATCH_FOR_GO_DEAL_NUMBER, theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
        return - 1;
      }
    }
    catch(Exception ex)
    {
      setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
      logger.error("Exception @getDealId: GO Functionality - applcation Id provided = <" + applId + ">");
      logger.error(ex);
      return - 1;
    }

    return dealId;

  }


  /**
   *workflowTrigger type defination
   *type 0 : task completion activities
   *type 1 : initial tasks generation
   *type 2 : async task generation
   *added type 3 for workflow regression on opening tasks which is based on the EstimatedClosingDate
   *added type 4 for DealEvent Status Update. Just insert into ESBoutboundQueue table to solve 
   * the issue that not workflow is called when deal got to be "In Underwriting"
   */
  public void workflowTrigger(int type, SessionResourceKit srk, PageEntry pg, WFEExternalTaskCompletion extObj)
    throws Exception
  {
    SysLogger logger = srk.getSysLogger();
    logger.debug("@PHC.workflowTrigger: type=" + type);

    // workflow activities
    WFTaskExecServices tes = new WFTaskExecServices((PageHandlerCommon) this, pg);
    if (type == 0 || type == 2)
    {
      try
      {
        if (type == 2) tes.setAsyncStageTestGeneration(true);
        if (extObj != null) tes.setExternalTaskCompletion(extObj);
        tes.REALtaskCompleted();
        
        // since 4.2 DSU STARTS
        if (pg.dealPage && pg.getPageDealId() > 0)
            storeESBOutboundQueue(pg);
        // since 4.2 DSU ENDS

        return;
      }
      catch(Exception e)
      {
        String msg = "Exception @PHC.workflowTrigger(), type 0/2:" + e;		//#DG702
        srk.getSysLogger().error(msg);
        throw new Exception(msg, e);
      }
      catch(Error er)
      {
        String msg = "Exception (ERROR) @PHC.workflowTrigger(), type 0/2:" + er;		//#DG702
        srk.getSysLogger().error(er);
        throw new Exception(msg, er);
      }
    }

    if (type == 3)
    {
      try
      {
        tes.cancelOpenClosingTasks();
        return;
      }
      catch(Exception e)
      {
        String msg = "Exception @PHC.workflowTrigger(), type 3";
        srk.getSysLogger().error(msg);
        throw new Exception(msg);
      }
      catch(Error er)
      {
        String msg = "Exception (ERROR) @PHC.workflowTrigger(), type 3";
        srk.getSysLogger().error(er);
        throw new Exception(msg);
      }
    }

    if (type == 1)
    {
      try
      {
        WorkflowAssistBean wab = new WorkflowAssistBean();
        Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
        wab.generateInitialTasks(deal, srk);
        return;
      }
      catch(Exception e)
      {
        String msg = "Exception @PHC.workflowTrigger(), type 1:" + e;		//#DG702
        srk.getSysLogger().error(msg);
        throw new Exception(msg, e);
      }
      catch(Error er)
      {
        String msg = "Exception (ERROR) @PHC.workflowTrigger(), type 1:" + er;		//#DG702
        srk.getSysLogger().error(er);
        throw new Exception(msg, er);
      }
    }

    // since 4.2 DSU STARTS
    if (type == 4) {
        storeESBOutboundQueue(pg);
        return;
    }
    // since 4.2 DSU ENDS

    throw new Exception("PHC@workflowTrigger: invalid activation type = " + type);

  }

  /**
   * <p>storeESBOutboundQueue</p>
   * <p>description: this method stores deal information into ESBOutboundQueue table</p> 
   * @param PageEntry pg
   * @throws Exception
   */
  public void storeESBOutboundQueue(PageEntry pg) throws Exception {
      
      try {
          Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
          String supportDSU = PropertiesCache.getInstance().getProperty(
                  deal.getInstitutionProfileId(), Xc.STATUSUPDATE_DEALANDEVENT_SUPPORT);
          if ("Y".equals(supportDSU) || "y".equals(supportDSU)) {
                 
	        	// For Manual Deal originated from Deal Entry screen will have channel Media as 'M' 
	          	// Block updating the Deal event status and Condition update status for deals created for express 
	          	// FXP32829 - Dec 06 2011
	          	if(deal.getChannelMedia() != null && deal.getChannelMedia().equalsIgnoreCase(Xc.CHANNEL_MEDIA_M))
	          		return;
          	
              // check the systemTypeId for the deal is supported DealEventStatus update
              //FXP25721 - July 22
              try {
                  DealEventStatusUpdateAssoc desua = new DealEventStatusUpdateAssoc(deal.getSessionResourceKit());
                  desua.setSilentMode(true);
                  Collection<DealEventStatusUpdateAssoc> c = desua.findBySystemType(deal.getSystemTypeId());
                  if (c==null || c.size()==0) {
                      return;
                  }
              } catch (Exception rd) {
                  logger.info("Couldn't get DealEventStatusUpdateAssoc for systemtypeid=" + deal.getSystemTypeId());
                  return;
              }

              ESBOutboundQueue queue = new ESBOutboundQueue(deal.getSessionResourceKit());
              // changing for performance
              // check if unprocessed same record exist in the queue, then don't create = START
              // systemTypeId is not required since dealId unique for the institution.
              boolean exist = false;
              try {
        	  exist = queue.existUnprocessed(deal.getDealId(), ServiceConst.SERVICE_TYPE_DEALEVENT_STATUS_UPDATE);        	  
              } catch (Exception e) {
        	// do nothing  
              } 
              if (!exist) {
              ESBOutboundQueuePK pk = queue.createPrimaryKey();
              queue.create(pk, ServiceConst.SERVICE_TYPE_DEALEVENT_STATUS_UPDATE, deal.getDealId(), 
                      deal.getSystemTypeId(), srk.getExpressState().getUserProfileId());
          } 
              // check if unprocessed same record exist in the queue, then don't create = END
          } 
      }catch(Exception e) {
            String msg = "Exception @PHC.workflowTrigger(), type 1:" + e;       //#DG702
            srk.getSysLogger().error(msg);
            throw new Exception(msg, e);
      } catch(Error er) {
            String msg = "Exception (ERROR) @PHC.workflowTrigger(), type 1:" + er;      //#DG702
            srk.getSysLogger().error(er);
            throw new Exception(msg, er);
      }
      return;
  }
  
  /**
   *
   *
   */
  public PageEntry setupMainPagePageEntry(int pageId)
  {
    return setupMainPagePageEntry(pageId, - 1, - 1, getGotoInstitutionId());

  }


  /**
   *
   *
   */
  public PageEntry setupMainPagePageEntry(int pageId, int dealId)
  {
    return setupMainPagePageEntry(pageId, dealId, - 1, getGotoInstitutionId());

  }


  /**
   *
   *
   */
  public PageEntry setupMainPagePageEntry(int pageId, int dealId, int copyId, int institutionId)
  {
    Page pageEntity = null;

    Exception pageDetailsException = null;

    
    try
    {
      PDC pageCache = PDC.getInstance();
      //logger.debug("PHC@setupMainPagePageEntry::pageCache: " + pageCache);

      pageEntity = pageCache.getPage(pageId);
      //logger.debug("PHC@setupMainPagePageEntry::pageEntity: " + pageEntity);
    }
    catch(Exception e0)
    {
      pageDetailsException = e0;
      pageEntity = null;
    }

    if (pageEntity == null)
    {
      logger.error("@PHC.setupMainPagePageEntry(pageId=" + pageId + ", dealId=" + dealId + ", copyId=" + copyId + ") :: No details for page requested - likely invalid page Id");
      if (pageDetailsException != null) logger.error(pageDetailsException);
      setStandardFailMessage();
      return null;
    }

    PageEntry pgEntry = new PageEntry();

    if (pageId == PGNM_DEAL_ENTRY_ID)
    {
        //3.3 ML - change institutionid only for deal entry
        getSessionResourceKit().getExpressState().setDealInstitutionId(institutionId);
        
        Deal deal = null;
      //logger.debug("PHC@setupMainPagePageEntry::I am from DEAL_ENTRY_ID");

      try
      {
        // To fix the problem if a passive user entered DealEntry
        //  --  prevent to create Tx Deal and not used
        //logger.debug("PHC@setupMainPagePageEntry::before noAuditIfIncomplete");

        ACM acm = theSessionState.getAcm();


        /***************MCM Impl team changes starts - artf726091 *******************/
//        AccessResult arUser = acm.pageAccess(PGNM_DEAL_ENTRY_ID, 
//                theSessionState.getLanguageId(), theSessionState.getDealInstitutionId());
        AccessResult arUser = acm.pageAccess(PGNM_DEAL_ENTRY_ID,
                        theSessionState.getLanguageId(), institutionId);
        /***************MCM Impl team changes starts - artf726091 *******************/
        
        if (arUser.getAccessTypeID() != Sc.PAGE_ACCESS_DISALLOWED)
        {
            noAuditIfIncomplete();
            deal = handleAddDeal();
            //logger.debug("PHC@setupMainPagePageEntry::deal: " + deal);

            if (deal == null)
            {
              setStandardFailMessage();
              return null;
            }

            pgEntry.setPageDealId(deal.getDealId());
            pgEntry.setPageDealCID(deal.getCopyId());
            //logger.debug("PHC@setupMainPagePageEntry::PageDealId: " + deal.getDealId());
            //logger.debug("PHC@setupMainPagePageEntry::CopyId: " + deal.getCopyId());

            pgEntry.setPageDealApplicationId("" + deal.getDealId()); // are same (for now anyway!)
        }

        pgEntry.setDealEntry(true);
        //logger.debug("PHC@setupMainPagePageEntry:: I set the pgEntry as a DealEntry");
      }
      catch (Exception e)
      {
        setStandardFailMessage();
        logger.error(e);
        return null;
      }
    }
    else
    {
      if (dealId > 0)
      {
        pgEntry.setPageDealId(dealId);
        pgEntry.setPageDealApplicationId("" + dealId);
        // are same (for now anyway!)
      }
      if (pageEntity.getDealPage() == true && copyId != - 1) pgEntry.setPageDealCID(copyId);
    }


    // setup page entry
    ////pgEntry.setPageMosUserId(theSession.getCurrentPage().getPageMosUserId());
    ////pgEntry.setPageMosUserTypeId(theSession.getCurrentPage().getPageMosUserTypeId());

    PageEntry pe = (PageEntry) theSessionState.getCurrentPage();

    //logger.debug("PHC@setupMainPagePageEntry::Current PageEntryName: " + pe.getPageName());
    //logger.debug("PHC@setupMainPagePageEntry::Current PageMosUserId: " + pe.getPageMosUserId());
    //logger.debug("PHC@setupMainPagePageEntry::Current PageMosUserType: " + pe.getPageMosUserTypeId());


    //logger.debug("PHC@setupMainPagePageEntry::Target PageId: " + pageId);
    pgEntry.setPageId(pageId);

    //logger.debug("PHC@setupMainPagePageEntry::Target PageEntityName: " + pageEntity.getPageName());
    pgEntry.setPageName(pageEntity.getPageName());

    //logger.debug("PHC@setupMainPagePageEntry::Target PageEntityLabel: " + pageEntity.getPageLabel());
    pgEntry.setPageLabel(pageEntity.getPageLabel());

    pgEntry.setPageTaskId(0);

    pgEntry.setPageTaskSequence(0);

    //logger.debug("PHC@setupMainPagePageEntry::Target is DealPage: " + pageEntity.getDealPage());
    pgEntry.setDealPage(pageEntity.getDealPage());
    
    //since 3.3 ML
    pgEntry.setDealInstitutionId(institutionId);
    pgEntry.setPageMosUserId(theSessionState.getUserProfileId(institutionId));
    pgEntry.setPageMosUserTypeId(theSessionState.getUserTypeId(institutionId));

    return pgEntry;
  }


  /**
   *
   *
   */
  public PageEntry setupSubPagePageEntry(PageEntry pg, int pageId)
  {
    return setupSubPagePageEntry(pg, pageId, false);
  }


  /**
   *
   *
   */
  public PageEntry setupSubPagePageEntry(PageEntry pg, int pageId, boolean makePageStateTable)
  {
    PageEntry subPg = null;
    PageEntry pgTemp = pg;

    int i = 0;  //#DG612 prevent live lock
    while(pgTemp.getPrevPE() != null)
    {
        if(i++ > 32) {  //#DG612
            logger.trace("exceeded limit in getPrevPE");
            break;
        }
      pgTemp = pgTemp.getPrevPE();
    }

    while(pgTemp.getNextPE() != null)
    {
      pgTemp = pgTemp.getNextPE();
      if (pgTemp.getPageId() == pageId)
      {
        subPg = pgTemp;
        break;
      }
    }

    if (subPg == null)
    {
      // make it!
      Page pgProperties = getPage(pageId);

      // setup page entry
      subPg = new PageEntry();
      subPg.setPageMosUserId(pg.getPageMosUserId());
      subPg.setPageMosUserTypeId(pg.getPageMosUserTypeId());
      subPg.setPageId(pgProperties.getPageId());
      subPg.setPageName(pgProperties.getPageName());
      subPg.setPageLabel(pgProperties.getPageLabel());
      subPg.setPageTaskId(0);
      subPg.setPageTaskSequence(0);
      subPg.setDealPage(pgProperties.getDealPage());
    }

    subPg.setPageDealId(pg.getPageDealId());

    subPg.setPageDealCID(pg.getPageDealCID());

    subPg.setPageDealApplicationId(pg.getPageDealApplicationId());

    // added for ML
    subPg.setDealInstitutionId(pg.getDealInstitutionId());

    // ensure marked as sub-page to ensure we get back to UW
    subPg.setSubPage(true);
    subPg.setPrevPE(pg);

    if (makePageStateTable == true)
    {
      Hashtable pst = subPg.getPageStateTable();
      if (pst == null) subPg.setPageStateTable(new Hashtable());
    }

    return subPg;
  }


  /**
   *
   *
   */
  protected boolean jumpTrap(int jumpType, int dealId, int pageId, int previousNdx, int toolNdx)
  ////private boolean jumpTrap(int jumpType, int dealId, int pageId, int previousNdx, int toolNdx)
  {
    PageEntry pgentry = (PageEntry) theSessionState.getCurrentPage();

logger.debug("PHC@jumpTrap::jumpTrap:PageName: " + pgentry.getPageName());
logger.debug("PHC@jumpTrap::SubPage?: " + pgentry.isSubPage());
logger.debug("PHC@jumpTrap::isEditable?: " + pgentry.isEditable());
logger.debug("PHC@jumpTrap::isPageDataModified?: " + pgentry.isPageDataModified());
logger.debug("PHC@jumpTrap::isModified?: " + pgentry.isModified());
logger.debug("PHC@jumpTrap::isTaskNavigateMode?: " + pgentry.isTaskNavigateMode());
logger.debug("PHC@jumpTrap::jumpType: " + jumpType);

    JumpState js = null;
//FXP33555: 	v5.0 - Regression - Clicking work queue on UW worksheet does not give warning message
    /*if (pgentry.getPageId()== Mc.PGNM_UNDERWRITER_WORKSHEET && jumpType == JumpState.WORKQUEUE)
    {
    	setActiveMessageToAlert(ActiveMsgFactory.ISPAGECONFIRMEXIT);//return true;
        js = new JumpState(jumpType, dealId, pageId, previousNdx, toolNdx);
        theSessionState.setJmpState(js);
        return true;
    }*/

    // check for unconfirmed page cancel (jump) type - e.g. special case jump type
    // Added new jump type (Exit)  -- Exit screen and goto next workflow screen -- By BILLY 07May2002
    if (jumpType == JumpState.UNCONFIRMEDCANCEL || jumpType == JumpState.UNCONFIRMEDEXIT)
    {

logger.debug("PHC@jumpTrap::jumpTrap: UnconfirmedCancelCase");
      if (pgentry.isPageDataModified() == false)
        return false;

//    temp
      logger.debug("PHC@jumpTrap::PageCondition4: " + pgentry.getPageCondition4());
      logger.debug("PHC@jumpTrap::PageCriteria3: " + pgentry.getPageCriteriaId3());
      logger.debug("PHC@jumpTrap::PageCriteria4: " + pgentry.getPageCriteriaId4());
      logger.debug("PHC@jumpTrap::jumpTrap: DealEntry? " + pgentry.isDealEntry());
      logger.debug("PHC@jumpTrap::jumpTrap:PageName: " + pgentry.getPageName());

    if (pgentry.isDealEntry()) {

        if (jumpType == JumpState.UNCONFIRMEDEXIT) {
            setActiveMessageToAlert(ActiveMsgFactory.ISDEALENTRYCONFIRMEXIT);
        } else {
            jumpType = JumpState.WORKQUEUE;
            setActiveMessageToAlert(ActiveMsgFactory.ISDEALENTRYCONFIRMCANCEL);
        }
    }
      else // must be Deal Modification page only!! // also might be an UnderwriterWorksheet
      {
        if(jumpType == JumpState.UNCONFIRMEDEXIT)
        {
          setActiveMessageToAlert(ActiveMsgFactory.ISPAGECONFIRMEXIT);
        }
         //--CervusPhaseII--17Feb2005--start--//
        //// IMPORTANT!!! The the whole state machine for DealMod screen unlock/toggle language on
        //// any level of Active Message Layer is finished the code below will become generic!!!
        //// (obtained from the new state machine).
        //// Stay temporarily to fix the BMO build urgently to sync with the state (0,0,1) 'Cancel'
        //// without changes with any language from Deal Modification screen if no changes done.

        else if(pgentry.isPageDataModified() == true &&
                pgentry.isModified() == true &&
                pgentry.getPageCondition4() == true &&
                pgentry.getPageCriteriaId3() == 0 &&
                pgentry.getPageCriteriaId4() == 1)
        {
          pgentry.setModified(false);
          return false;
        }
        else
        {
          logger.debug("Katya::PHC@jumpTrap::jumpTrap: before setActiveMessageToAlert - ISPAGECONFIRMCANCEL");
          setActiveMessageToAlert(ActiveMsgFactory.ISPAGECONFIRMCANCEL);
        }
      }
    }
    //--CervusPhaseII--17Feb2005--end--//

    // select one of generic messages for all other jump actions ...
    // subpage - only allowed to leave by Submit/Cancel (editable variety) or OK (non-
    // editable variety) - hence always trapped
    else if (pgentry.isSubPage() == true)
    {
      // editable sub-page
      if (pgentry.isEditable() == true)
      {
       setActiveMessageToAlert(ActiveMsgFactory.ISEDITSUBPAGE);
      }
      // view (non-editable) sub-page
      else
      {
       setActiveMessageToAlert(ActiveMsgFactory.ISVIEWSUBPAGE);
      }
    }

    // main page - trap if modified data
    else
    {
      if (pgentry.isPageDataModified() == false) return false;
      if (pgentry.isDealEntry()) setActiveMessageToAlert(ActiveMsgFactory.ISDEALENTRYCONFIRMCANCEL);
      else setActiveMessageToAlert(ActiveMsgFactory.ISJUMP);
    }


    // trapped! - create state (continuation) object
    logger.debug("@PHC.jumpTrap: trapped!: dealId pageId previousNdx toolNdx = " + dealId + " " + pageId + " " + previousNdx + " " + toolNdx);

    js = new JumpState(jumpType, dealId, pageId, previousNdx, toolNdx);
    theSessionState.setJmpState(js);
    return true;
  }


  /**
   *
   *
   */
  protected boolean jumpTrap(int jumpType)
  ////private boolean jumpTrap(int jumpType)
  {
    return jumpTrap(jumpType, - 1, - 1, - 1, - 1);
  }


  /**
   *
   *
   */
  protected boolean jumpTrap(int jumpType, int dealId, int pageId)
  ////private boolean jumpTrap(int jumpType, int dealId, int pageId)
  {
    return jumpTrap(jumpType, dealId, pageId, - 1, - 1);
  }


  /**
   *
   *
   */
  private void finishGoPage(int gotoPageId, int gotoDealId)
    throws Exception
  {
    //logger.debug("From PHC@finishGoPage");
    PageEntry pgEntry = setupMainPagePageEntry(gotoPageId, gotoDealId, -1, getGotoInstitutionId());

    logger.debug("PHC@finishGoPage::pgEntry: " + pgEntry);

    if (pgEntry == null) return;

    // active (error) message set in this case
    // set page navigation mode as 'via GOTO'
    pgEntry.setNavigationMode(1);

    // set as 'next' page and navigate
    savedPages.setNextPage(pgEntry);

    //logger.debug("From PHC@finishGoPage before navigateToNextPage");
    navigateToNextPage(true);
  }


  /**
   *
   *
   */
  public Deal handleAddDeal()
    throws Exception
  {
    SessionResourceKit srk = getSessionResourceKit();
    Deal deal = null;
    try
    {
      srk.beginTransaction();
      CalcMonitor dcm = CalcMonitor.getMonitor(srk);
      MasterDeal md = MasterDeal.createDefaultTree(srk, dcm);
      deal = new Deal(srk, dcm);
      deal.findByPrimaryKey(new DealPK(md.getDealId(), 
                                       md.getGoldCopyId()));

      /*
            MasterDeal md = new MasterDeal(srk,dcm);
      MasterDealPK pk = md.createPrimaryKey();
      deal = md.create(pk);
                        */
      // deal.setBridgeId(0);
      deal.setPricingProfileId(0);
      deal.setPaymentTermId(0);
      deal.setScenarioRecommended("Y");
      deal.setScenarioLocked("N");
      deal.setApplicationDate(new java.util.Date());
      deal.setSystemTypeId(0);   //#DG492 add as default
      deal.forceChange("systemTypeId");

      // set initial status to incomplete (if not the default)
      if (deal.getStatusId() != Mc.DEAL_INCOMPLETE)
      {
        deal.setStatusId(Mc.DEAL_INCOMPLETE);
        deal.setStatusDate(new java.util.Date());
      }
      deal.setCopyType("T");

      // Set default Privilege Payment Option from Properties -- By BILLY 20Nov2001
      int defPriPayOpt = getIntValue(PropertiesCache.getInstance().getProperty(
                theSessionState.getDealInstitutionId(),                                                               
                COM_BASIS100_DEAL_PRIVILEGEPAYMENTOPTION, "0"));
      deal.setPriviligePaymentId(defPriPayOpt);

      // Set default TaxPayorId from Properties -- By BILLY 10April2002
      int defTaxPoyor = getIntValue(PropertiesCache.getInstance().getProperty(
                theSessionState.getDealInstitutionId(),                                                               
                COM_BASIS100_DEAL_TAXPAYORID, "0"));
      deal.setTaxPayorId(defTaxPoyor);

      logger.debug("the default defTaxPoyor = " + defTaxPoyor);

      deal.ejbStore();
      doCalculation(deal.getCalcMonitor());

      //log to dealHistory
      DealHistory hist = new DealHistory(getSessionResourceKit());
      
      /******* MCM team ML Ticket merge FXP22128 start ******/
      int dealInstitution = srk.getExpressState().getDealInstitutionId();  
      int userInstitution = srk.getExpressState().getUserInstitutionId();  
      int historyUserId;  

      if (dealInstitution != userInstitution) {            
          SessionResourceKit userSrk = new SessionResourceKit();  
          userSrk.getExpressState().setDealInstitutionId(userInstitution);  
          UserProfile userProfile = new UserProfile(userSrk);  
          UserProfileBeanPK userProfileBeanPK;  
          userProfileBeanPK = new UserProfileBeanPK(srk.getExpressState().getUserProfileId(),   
                  userInstitution);  
          userProfile.findByPrimaryKey(userProfileBeanPK);  
          historyUserId = userProfile.getUserProfileId(dealInstitution);  
      } else {  
          historyUserId = theSessionState.getSessionUserId();  
      }  
      
      ////hist.create(deal.getDealId(), getTheSession().getSessionUserId(), Mc.DEAL_TX_TYPE_EVENT, deal.getStatusId(), "Deal Create", new java.util.Date());
      hist.create(deal.getDealId(), historyUserId, Mc.DEAL_TX_TYPE_EVENT, deal.getStatusId(), "Deal Create", new java.util.Date());
      /******* MCM team ML Ticket merge FXP22128 end ******/
      
      srk.commitTransaction();
    }
    catch(Exception ex)
    {
      srk.cleanTransaction();
      logger.error("Error happened @handleAddDeal() ");
      logger.error(ex);
      throw new Exception();
    }
    return deal;
  }

  private String getDealNumber() throws ClassNotFoundException {
    //logger.debug("PHC@getGotoDealNumber:: start");
    ////ViewBean thePage = currNDPage;

    Class testClass = currNDPage.getClass();
    String testClassName = currNDPage.getClass().getName();

    //// Temp commented out because this call could interrupt the execution. Test more.
    ViewBean currPgTest = RequestManager.getRequestContext().getViewBeanManager().
        getViewBean(testClassName);
    ////ViewBean currPgTest = RequestManager.getRequestContext().getViewBeanManager().getViewBean(testClass);

    PageEntry thePage = (PageEntry) theSessionState.getCurrentPage();

    logger.debug("PHC@getGotoDealNumber::CurrentPage: " + thePage);

    //CSpider.getPage(theSession.getCurrentPage().getPageName());
    ////String dealNumber = thePage.getDisplayFieldValue(TB_DEAL_ID).toString().trim();
    String dealNumber = currPgTest.getDisplayFieldValue(TB_DEAL_ID).toString().trim();
    
    logger.debug("PHC@getGotoDealNumber::STRING_DEAL_NUMBER: " + dealNumber);
    return dealNumber;
  }

  /**
   *
   *
   */
  private String getGotoDealNumber() throws ClassNotFoundException
  {
    String dealNumber = getDealNumber();

    if (dealNumber.equals(""))
    {
      //--Release2.1--//
      //--> Multilingual support
      setActiveMessageToAlert(BXResources.getSysMsg(GOTO_PAGE_NODEALNUM, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
      return null;
    }

    return dealNumber;

  }


  /**
   *
   *
   */
  //--Release2.1--//
  //------CAUTION-------//
  //Beware that this method will return the PageLabel based on the Current Language.
  //That means the PDC.getPageByLabel may not work !!!
  //I provided a new method to get the PageId instead below.
  // NOTE : by BILLY 05Nov2002
  private String getGotoPageLabel()
  {
    ViewBean thePage = currNDPage;

    ComboBox comboBox =(ComboBox) thePage.getDisplayField(COMBO_BOX_NAME);
    String selectedPageLabel = comboBox.stringValue();

    logger.debug("PHC@SelectedPageLavel:" + selectedPageLabel);

    ////if (selectedPage == null)
    if (comboBox.isSelected(selectedPageLabel) == false)
    {
      logger.debug("PHC@PHC@Set ActiveMsgFactory with mode isSelected: false");
      //--Release2.1--//
      //--> Multilingual support
      setActiveMessageToAlert(BXResources.getSysMsg(GOTO_PAGE_NOINPUT, theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
      //========================
      return null;
    }

    //// No need in this now (see the comment above).
    ////String selectedPageLabel = selectedPage.getLabel().trim();

    if (selectedPageLabel == null || selectedPageLabel.equalsIgnoreCase("Choose a Page") || selectedPageLabel.equals(""))
    {
      logger.debug("PHC@Set ActiveMsgFactory with mode isSelected: true");
      //--Release2.1--//
      //--> Multilingual support
      setActiveMessageToAlert(BXResources.getSysMsg(GOTO_PAGE_NOINPUT, theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
      //========================
      return null;
    }
    return selectedPageLabel;
  }

  //--Release2.1--//
  //Method to get the PageId from cbPageName combobox
  // Created : by BILLY 05Nov2002
  private int getGotoPageId() {

    ////String dealNumber = thePage.getDisplayFieldValue(TB_DEAL_ID).toString().trim();
    String pageId = currNDPage.getDisplayFieldValue(TB_PAGE_ID).toString().trim();

    return (new Integer(pageId)).intValue();
  }

  /**
   * PAGE conbobox for GOTO includes all pages so that the combobox 
   * should not be isntitutinized
   */
  public void populatePageShellDisplayFields()
  {
logger.debug("PHC@PPSDF::before showDialogMessage()");

    showDialogMessage();

    ViewBean thePage = getCurrNDPage();
    PageEntry pe = (PageEntry) theSessionState.getCurrentPage();

    int institutionId = pe.dealPage ? theSessionState.getDealInstitutionId():theSessionState.getUserInstitutionId();
    
    //--Release2.1--start//
    //// Except the page name the pageId shoud be extracted and passed to the
    //// BXResources to obtain the language independent page name.
    int languageId = theSessionState.getLanguageId();

    //--DJ_LDI_CR--start--//
    //// In order to display or not to Life&Disability Insurance page.
    ////String pageLabel = BXResources.getPickListDescription("PAGELABEL", pe.getPageId(), languageId);

    String pageLabel = "";
    //// No Life&Disability Insurance screen.
    
    //***** WARNING ***** Dec 28, 2007 ** by Midori
    // USES com.basis100.ldinsurance.displaylifedisabilitymodule to determin if DJ or NOT
    // if other clients to use the section, it will be mixed up
    //******************************
    if (PropertiesCache.getInstance().getProperty(institutionId,COM_BASIS100_LDINSURANCE_DISPLAYLIFEDISABILITYMODULE, "N").equals("N"))
    {
       logger.debug("PHC@PPSDF::not DJ case");
       pageLabel = BXResources.getPickListDescription(institutionId, PKL_PAGELABEL, pe.getPageId(), languageId);
    }
    //// Give an access to the Life&Disability Insurance screen.
    else if (PropertiesCache.getInstance().getProperty(institutionId,COM_BASIS100_LDINSURANCE_DISPLAYLIFEDISABILITYMODULE, "N").equals("Y"))
    {
       logger.debug("PHC@PPSDF::DJ case");
       pageLabel = BXResources.getPickListDescription(institutionId,PKL_DJPAGELABEL, pe.getPageId(), languageId);
    }
    //--DJ_LDI_CR--end--//
    logger.debug("PHC@PPSDF::PageLabel: " + pageLabel);

    // USERTYPE isnot institutionized, then how should we generate combBox for ML
    String userRole = BXResources.getPickListDescription(institutionId,PKL_USER_TYPE, theSessionState.getSessionUserType(), languageId);

    //FXP20043 
    int instDeal = pe.getDealInstitutionId();
    if(instDeal<0)
        instDeal = theSessionState.getUserInstitutionId();
    
    InstitutionProfileDataBean instBean = 
        theSessionState.getUserStateDataManager().getInstitutionProfileDataBean(instDeal);
    UserProfileDataBean upBean =
        theSessionState.getUserStateDataManager().getUserProfileDataBean(instDeal);
    
    String companyName = instBean.getInstitutionName() + "-" + instBean.getBrandName();
    String todayDate = theSessionState.getSessionDateStr();
    String userNameTitle = upBean.getUserFullName() + ", " + upBean.getUserTypeDescription();

    thePage.setDisplayFieldValue(CHILD_STPAGELABEL, pageLabel);
    thePage.setDisplayFieldValue(CHILD_ST_COMPANY_NAME, companyName);
    thePage.setDisplayFieldValue(CHILD_ST_TODAY_DATE, todayDate);
    thePage.setDisplayFieldValue(CHILD_ST_USER_NAME_TITLE, userNameTitle);
    
    /**** 4.3 GR MI Response *****/
    String responseLabel = BXResources.getSysMsg("MI_RESPONSE", languageId);
    thePage.setDisplayFieldValue(CHILD_STMIRESPONSE, responseLabel);
    
    String frequency = PropertiesCache.getInstance().getProperty(
            institutionId, "com.express.mosapp.freqPollingMIResponse",
            "10");
    int frequencyValue = new Integer(frequency).intValue() * 1000;
    thePage.setDisplayFieldValue(CHILD_STHDFREQUENCY, frequencyValue);
    
    boolean chmcAlert = PropertiesCache.getInstance().getProperty(
            institutionId, "com.filogix.mosapp.MIResponseAlert.CMHC", "No")
            .equals("Yes");
    thePage.setDisplayFieldValue(CHILD_STHDCHMCALERT, chmcAlert);
    
    boolean genWorthAlert = PropertiesCache.getInstance().getProperty(
            institutionId, "com.filogix.mosapp.MIResponseAlert.Genworth",
            "No").equals("Yes");
    thePage.setDisplayFieldValue(CHILD_STHDGENWORTHALERT, genWorthAlert);

    int timeOut = RequestManager.getRequestContext().getRequest()
            .getSession().getMaxInactiveInterval();
    thePage.setDisplayFieldValue(CHILD_STHDSESSIONTIMEOUT, timeOut * 1000);

    int miStatusId = 0;
    int miProviderId = 0;
    boolean dealLock = false;
    if (pe.dealPage && pe.getPageDealId()>0) {
    if (pe.dealPage && pe.getPageDealId()>0)
        try {
            Deal deal = new Deal(srk, null);
	        deal.findByPrimaryKey(new DealPK(pe.getPageDealId(), pe
	                .getPageDealCID()));
            miStatusId = deal.getMIStatusId();
            miProviderId = deal.getMortgageInsurerId();
            dealLock = dealLocked(pe.getPageDealId());
        } catch (Exception e) {
            logger.error("error in find deal mi response" + e);
        }
    }
    thePage.setDisplayFieldValue(CHILD_STHDDEALLOCKED, dealLock);
    thePage.setDisplayFieldValue(CHILD_STHDMIRESPONSESTATUSID, miStatusId);
    thePage.setDisplayFieldValue(CHILD_STHDMIPROVIDERID, miProviderId);
    
    /**** 4.3 GR MI Response *****/

    // set detect alert tasks to current value - common to most pages, trap failure
    // where absent
    try
    {
      ////String detectAlertTasks = theSession.getDetectAlertTasks();
      String detectAlertTasks = theSessionState.getDetectAlertTasks();
      //logger.debug("PHC@PPSDF::DetectAlertTaskToJSP: " + detectAlertTasks);

      // check for ssl - if so convert flag to lower case --- applet senses need
      // for ssl via this mechanism (if detect alert tasks is lower case then ssl (https),
      // else in the clear communications (http).
      String secureSSL_YN = PropertiesCache.getInstance().getProperty(theSessionState.getUserInstitutionId(), COM_BASIS100_COMMUNICATION_SSL_ENCRYPTION, "Y");
      if ("Y".equals(secureSSL_YN)) detectAlertTasks = detectAlertTasks.toLowerCase();
      thePage.setDisplayFieldValue(CHILD_DETECTALERTTASKS, new String(detectAlertTasks));
    }
    catch(Exception e)
    {
      logger.error("PHC@populatePageShellDisplayFields::Exception: " + e);
    }

    // set user id to current value - common to most pages, trap failure where absent
    try
    {
        thePage.setDisplayFieldValue(CHILD_SESSION_USER_ID, new String("" + theSessionState.getSessionUserId()));
    }
    catch(Exception e)
    {
      ;
    }

    // set default deal number for goto panel
    ////PageEntry pg = theSession.getCurrentPage();
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    try
    {
      if (pg != null && pg.getPageDealId() > 0) thePage.setDisplayFieldValue(TB_DEAL_ID, new String("" + pg.getPageDealId()));
    }
    catch(Exception e)
    {
      ;
    }
    setupFolderCode();
  }


  /**
   *
   *
   */
  public void populatePageDealSummarySnapShot()
  {
    ViewBean thePage = getCurrNDPage();
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
    
    try
    {
      String borrowerName = theSessionState.getCurrentPage().getPageDealPrimaryBorrowerName();
      //logger.debug("PHC@PPDSSS::BorrowerIN: " + pg.getPageDealPrimaryBorrowerName());

      thePage.setDisplayFieldValue("stBorrFirstName", new String(borrowerName));

      ////SYNCADD.
      // New required by Product to add DenialReason :
      // if (Deal.statusid = 24) or (Deal.statusid = 23 and DenialReason.denialreasonid <> 0)
      // -- Have to detach the "stDealStatus" from the screen to take effect !!
      // -- By Billy 21Aug2002
      // Check if DealId > 0 and CopyId >= 0 -- Bug fix by Billy 06Sept2002
      if (pg.getPageDealId() > 0 && pg.getPageDealCID() >= 0)
      {
        int theStatusId = new Deal(srk, null).getStatusId(pg.getPageDealId(), pg.getPageDealCID());
        int theDenResId = new Deal(srk, null).getDenialReasonId(pg.getPageDealId(), pg.getPageDealCID());

        //--Release2.1//
        //-->Modified to get Deal Status from BXResource -- By Billy 05Nov2002
        //String tmpStatusDec = PicklistData.getDescription("status", theStatusId);
        int institutionId = theSessionState.getDealInstitutionId();
        if (institutionId == 0)
        {
            institutionId = theSessionState.getUserInstitutionId();
        }
        String tmpStatusDec = BXResources.getPickListDescription(institutionId,PKL_STATUS, theStatusId, theSessionState.getLanguageId());
        //=========================================================================
        //logger.debug("PHC@PPDSSS::StatusDescription: " + tmpStatusDec);

        if (theStatusId == Mc.DEAL_DENIED ||
            (theStatusId == Mc.DEAL_COLLAPSED && theDenResId != Mc.DENIAL_REASON_OTHER))
        {
          //--Release2.1//
          //-->Modified to get DenialReason from BXResource -- By Billy 05Nov2002
          //tmpStatusDec = tmpStatusDec + "/" + PicklistData.getDescription("denialreason", theDenResId);
          tmpStatusDec = tmpStatusDec + "/" + BXResources.getPickListDescription(institutionId, PKL_DENIAL_REASON, theDenResId, theSessionState.getLanguageId());
          //======================================================================
          //logger.debug("PHC@PPDSSS::StatusDescriptionSpecialStatus: " + tmpStatusDec);
        }

        thePage.setDisplayFieldValue("stDealStatus", new String(tmpStatusDec));
      }
      ////========================================================================================
    }
    catch(Exception e)
    {
      logger.error("Exception @populateDealSummarySnapShotDO");
      logger.error(e);
    }
  }

  /**
   *
   *
   */
  public void revisePrevNextButtonGeneration(PageEntry pg, PageCursorInfo tds)
  {

    // assume both buttons will display
    pg.setPageCondition1(true);
    pg.setPageCondition2(true);

    // If first page displayed then flag that the previous button should not be displayed
    if (tds.isFirstPage()) pg.setPageCondition1(false);

    // If last page displayed then flag that next button should not be displayed
    if (tds.isLastPage() == true) pg.setPageCondition2(false);
  }


  /**
   *
   *
   */
  public void populateTaskNavigator(PageEntry pg)
  {
    ViewBean thePage = getCurrNDPage();

    if (pg.isTaskNavigateMode() == false)
    {
      thePage.setDisplayFieldValue("stTaskName", "");
      thePage.setDisplayFieldValue("stNextTaskPageLabel", "");
      thePage.setDisplayFieldValue("stPrevTaskPageLabel", "");
      return;
    }

    //FXP23132, MCM, Dec 15, 2008 -- start
    //thePage.setDisplayFieldValue("stTaskName", new String("Task: " + pg.getTaskName()));
    if(pg.isTaskNavigateMode() && pg.getPageTaskId() > 0){
        String label = BXResources.getGenericMsg(
        		"TASK_NAVIGATOR_TASK_LABEL", theSessionState.getLanguageId());
        String taskName = BXResources.getPickListDescription(
                theSessionState.getDealInstitutionId(), "TASK741", 
                pg.getPageTaskId(), theSessionState.getLanguageId());
        thePage.setDisplayFieldValue("stTaskName", label + " : " + taskName);
    }
    //FXP23132, MCM, Dec 15, 2008 -- end
    
    // we no longer show the page names ....
    thePage.setDisplayFieldValue("stNextTaskPageLabel", "");

    thePage.setDisplayFieldValue("stPrevTaskPageLabel", "");

    return;

  }
  private static final int QUEUE_SIZE_MAX=10;


  /**
   *
   *  For compilation purposes this method is commented out. This one of the generic
   *  BasisXpress framework methods and should be redesigned. iMT tool didn't convert it.
   *
   *
   */

  public void populatePreviousPagesLinks()
  {
    //logger.debug("PHC@populatePreviousPagesLinks::start");
    ViewBean thePage = getCurrNDPage();
    //logger.debug("PHC@populatePreviousPagesLinks::thePage: " + thePage);

    ////CSpHref href [ ] = new CSpHref [ QUEUE_SIZE_MAX ];
    HREF href [ ] = new HREF [ QUEUE_SIZE_MAX ];

    //WARNING:
    //if the QUEUE_SIZE_MAX is not 10 then number of
    //hrefs should be changed accordingly otherwise we'll
    //either have NullPointerException or not enough hrefs
    href [ 0 ] =(HREF) thePage.getDisplayField("Href1");

    href [ 1 ] =(HREF) thePage.getDisplayField("Href2");

    href [ 2 ] =(HREF) thePage.getDisplayField("Href3");

    href [ 3 ] =(HREF) thePage.getDisplayField("Href4");

    href [ 4 ] =(HREF) thePage.getDisplayField("Href5");

    href [ 5 ] =(HREF) thePage.getDisplayField("Href6");

    href [ 6 ] =(HREF) thePage.getDisplayField("Href7");

    href [ 7 ] =(HREF) thePage.getDisplayField("Href8");

    href [ 8 ] =(HREF) thePage.getDisplayField("Href9");

    href [ 9 ] =(HREF) thePage.getDisplayField("Href10");

    PageQueue openDialog = savedPages.getPreviousPagesQueue();


    //adding all the labels and all the targets from the
    //openDialog vector to all the href's
    for(int i = 0;i < QUEUE_SIZE_MAX;i ++)
    {
      if (savedPages.getPreviousPagesLinkFlag(i))
      {
        PageEntry pageEntry = openDialog.get(i);
        //logger.debug("PHC@populatePreviousPagesLinks::pageEntry: " + pageEntry);

        String [ ] formatList = new String [ 1 ];
        formatList [ 0 ] = savedPages.getPreviousPagesLinkText(i);
        //logger.debug("PHC@populatePreviousPagesLinks::formatList[0]: " + formatList[0]);

        //String [ ] objectsNamesList = new String [ 0 ];
        //logger.debug("PHC@populatePreviousPagesLinks::objectNameList[0]: " + objectsNamesList);

        ////href [ i ].setDisplayFormat(new CSpFormattedString(formatList, objectsNamesList));
        ////href [ i ].setDisplayFormat(new String(formatList, objectsNamesList));

        ////href [ i ].setTargetNDPage(new CSpCommonPageRef(pageEntry.getPageName()));
        ////href [ i ].setTargetNDPage(new CSpCommonPageRef(pageEntry.getPageName()));

        // To prevent user to click if any outstanding request -- BILLY 07June2001
        href [ i ].setExtraHtml("onClick=\"return IsSubmitButton();\"");
      }
      else
      {
        // Bug fix by Billy -- to clear the Href link,otherwise will display the links of
        //   the other user's session.  As ND will remember the Previous Link info. if not
        //   renew.  ie. will cause problem !!
        //logger.debug("BILLY ==> Clear Link : " + i);
        ////href [ i ].setDisplayFormat(null);
        ////href [ i ].setTargetNDPage(null);
      }
    }

  }

  //--> Importany !!!
  //--> In Jato they don't support to set the Display String for the Href.
  //--> In order to achieve this we have to do the following:
  //--> 1) In JSP added special string "@@" in the Herf Tag definition
  //-->     e.g. <jato:href name="Href1">@@</jato:href>
  //--> 2) In the ViewBean of every pages we have to handle the endDispaly Events for
  //-->     every Href button and call this method to replace the "@@" to the corresponding
  //-->     Display String stored in savedPages
  //-->     e.g. public String endHref1Display(ChildContentDisplayEvent event)
  //-->          {
  //-->            iWorkQueueHandler handler =(iWorkQueueHandler) this.handler.cloneSS();
  //-->            handler.pageGetState(this);
  //-->            String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 0);
  //-->            handler.pageSaveState();
  //-->
  //-->            return rc;
  //-->          }
  //-->
  //--> By Billy 07Aug2002
  public String populatePreviousPagesLinksDisplayString(String origContent, int index)
  {
    ////logger.debug("PHC@populatePreviousPagesLinksDisplayStringOrigContent: " + origContent);
    if (savedPages.getPreviousPagesLinkFlag(index))
    {
      //--Release2.1--//
      //// The content of the messages should be updated based on the LanguageId
      //// before the repopulation of the links.
      savedPages.revisePreviousPagesLinks();
      String theDisplayStr = savedPages.getPreviousPagesLinkText(index);
      //logger.debug("PHC@populatePreviousPagesLinksDisplayString::DisplayString: " + theDisplayStr);
      ////logger.debug("PHC@populatePreviousPagesLinksDisplayString::Return: " + StringTokenizer2.replace(origContent, "@@", theDisplayStr));
      return StringTokenizer2.replace(origContent, "@@", theDisplayStr);
    }
    else
    {
      ////logger.debug("PHC@populatePreviousPagesLinksDisplayString::ReturnNoPreviousLinks: " + StringTokenizer2.replace(origContent, "@@", " "));
      return StringTokenizer2.replace(origContent, "@@", " ");
    }
  }
  //==================================================================================


  /**
   * This idiom should be rewritten along with the typical call of this function:
   *    public int stUWTargetDownPayment_onBeforeHtmlOutputEvent(CSpHtmlOutputEvent event)
   *  {
   *        UWorksheetHandler handler = (UWorksheetHandler)this.handler.cloneSS();
   *
   *        handler.pageGetState(this);
   *        int retval = handler.generateLoadTarget(Mc.TARGET_DE_DOWNPAYMENT);
   *        handler.pageSaveState();
   *        return retval;
   *    }
   *
   *  Based on the returned value the display of an appropriate ViewBean should be done or
   *  the regular process should be aborted (see detailed comment in the XXXXXX).
   *
   */
  ////public int generateLoadTarget(int loadTarget)
  ////{
  ////  if (loadTarget <= 0) return CSpPage.SKIP;

  ////  if (getTheSession().getCurrentPage().getLoadTarget() == loadTarget) return CSpPage.PROCEED;

  ////  return CSpPage.SKIP;

  ////}

  //--> Modified to return boolean -- By Billy 24July2002
  public boolean generateLoadTarget(int loadTarget)
  {
    if (loadTarget <= 0) return false;

    ////PageEntry pg = theSessionState.getCurrentPage();
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    if (pg.getLoadTarget() == loadTarget)
        return true;

    return false;

  }

  /**
   * The following idiom in ND:
   *
   * 1. <dataObject>.clearDynamicCriteria()
   * 2. getDisplayFieldValue
   * 3. addDynamicCriterion("<dataFieldName>",
   *                         CSpCriteriaSQLObject.STRING_CRITERIA_TYPE,
   *                         <CSpValue criterionValue>)
   * 4. theDO.execute()
   * 5. load the target results page
   *
   * has an equavalent in JATO as follows:
   *
   * 1. <model>.clearUserWhereCriteria()
   * 2. getDisplayFieldValue("<displayFieldValue>");
   * 3. <model>addUserWhereCriterion("<dataFieldName>",
   *                                 "<operator>",
   *                                 <objectCritVal>)
   * 4. theModel.executeSelect(null);
   * 5. <targetViewBean>.forwardTo(getRequestContext())
   *
   * Model MUST BE bounded propertly to the target ViewBean (see more on the model/view bounding issue).
   * Last two two steps is provided in the BasisXpress framework methods (see more on that).
   *
   */
  public void setupDealSummarySnapShotDO(PageEntry pg)
  {
    try
    {
      int dealId = pg.getPageDealId();
      int copyId = pg.getPageDealCID();
      int institutionId = pg.getDealInstitutionId();

      ////CSpCriteriaSQLObject theDO =(CSpCriteriaSQLObject) CSpider.getDataObject(Mc.DO_DEAL_SUMMARY_SNAPSHOT);
      ////theDO.clearDynamicCriteria();
      
      doDealSummarySnapShotModel theModel =(doDealSummarySnapShotModel)RequestManager.getRequestContext().getModelManager().getModel(doDealSummarySnapShotModel.class);
      theModel.clearUserWhereCriteria();

      if (dealId <= 0)
      {
        // set an impossible criteria
        ////theDO.addDynamicCriterion("dfDealID",
          ////                      CSpCriteriaSQLObject.EQUAL_TO_INT_OPERATOR,
          ////                      new CSpInteger(-999));
        theModel.addUserWhereCriterion("dfDealID", "=", new Integer(- 999));
        pg.setPageDealPrimaryBorrowerName(" ");

        //logger.debug("--T--> @PHC.setupDealSummarySnapShot: I am in dealid<=0 mode");
        }
        else
        {
        ////theDO.addDynamicCriterion("dfDealID",
          ////                      CSpCriteriaSQLObject.EQUAL_TO_INT_OPERATOR,
          ////                      new CSpInteger(dealId));

        ////theDO.addDynamicCriterion("dfCopyId",
          ////                      CSpCriteriaSQLObject.EQUAL_TO_INT_OPERATOR,
          ////                      new CSpInteger(copyId));

        //logger.debug("--T--> @PHC.setupDealSummarySnapShot: I am in the normal mode");

        theModel.addUserWhereCriterion("dfDealID", "=", new Integer(dealId));
        theModel.addUserWhereCriterion("dfCopyId", "=", new Integer(copyId));
        theModel.addUserWhereCriterion("dfInstitutionId", "=", new Integer(institutionId));
        
        pg.setPageDealPrimaryBorrowerName(queryPrimaryBorrowerName(dealId, copyId));

        //logger.debug("--T--> @PHC.setupDealSummarySnapShot:SQL: " + ((doDealSummarySnapShotModelImpl)theModel).getSelectSQL());

        //logger.debug("--T--> @PHC.setupDealSummarySnapShot: Primary Borrower is set up to: " + pg.getPageDealPrimaryBorrowerName());
      }
    }
    catch(Exception e)
    {
      ;
    }

  }


  /**
   * The following set of Submit/Cancel buttons should be redesigned along with a
   * typical call from a page:
   *    {
   *        DealEntryHandler handler = (DealEntryHandler)this.handler.cloneSS();
   *
   *        handler.pageGetState(this);
   *        int rc = handler.displaySubmitButton();
   *    handler.pageSaveState();
   *
   *        return rc;
   *    }
   *
   *  (see page fragment). For now return SKIP = false, return PROCEED = true;
   */
  public boolean displaySubmitButton()
  {
    ////PageEntry pg = theSession.getCurrentPage();
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    if (pg.isEditable() == true) return true;

    return false;

  }


  /**
   *
   *
   */
  public boolean displayCancelButton()
  {
    return displaySubmitButton();

  }


  /**
   * For now return SKIP = false, return PROCEED = true.
   *
   */
  public boolean displayOKButton()
  {
    boolean rc = displaySubmitButton();

    ////if (rc == 1) return CSpPage.SKIP;
    if (rc == true) return false;


    ////return CSpPage.PROCEED;
    return true;
  }


  /**
   *
   * Should be rewritten.
   */
  public boolean displayEditButton()
  {
    ////PageEntry pg = theSession.getCurrentPage();
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    ////if (pg.isDealPage() == false) return CSpPage.PROCEED;
    if (pg.isDealPage() == false) return true;

    // precaution .. don't expect calls for non-deal pages
    ////if (pg.isEditable() == true) return CSpPage.PROCEED;
    if (pg.isEditable() == true) return true;

    ////if (pg.isHideInViewMode() == true) return CSpPage.SKIP;
    if (pg.isHideInViewMode() == true) return false;

    ////return CSpPage.PROCEED;
    return true;
  }

  /**
   *
   *
   */
  public boolean disableEditButton(PageEntry pg)
  {
    if (pg.isDealPage() == false) return false;


    // precaution .. don't expect calls for non-deal pages
    if (pg.isEditable() == false)
    {
      setStandardViewOnlyMessage();
      return true;
    }

    return false;

  }


  /**
   *
   *
   */
  //--> Changed the return value to boolean
  //-->   true : PROCEED (enable display)
  //-->   false : SKIP (disable display)
  //--> By BILLY 23July2002
  public boolean generateTaskName()
  {
    return getTaskNavigator().generateTaskName(theSessionState);

  }


  /**
   *
   *
   */
  //--> Changed the return value to boolean
  //-->   true : PROCEED (enable display)
  //-->   false : SKIP (disable display)
  //--> By BILLY 23July2002
  public boolean generateNextTaskPage()
  {
    return getTaskNavigator().generateNextTaskPage(theSessionState);

  }


  /**
   *
   *
   */
  //--> Changed the return value to boolean
  //-->   true : PROCEED (enable display)
  //-->   false : SKIP (disable display)
  //--> By BILLY 23July2002
  public boolean generatePrevTaskPage()
  {
    return getTaskNavigator().generatePrevTaskPage(theSessionState);

  }


  /**
   *
   *
   */
  //--> Changed the return value to boolean
  //-->   true : PROCEED (enable display)
  //-->   false : SKIP (disable display)
  //--> By BILLY 23July2002
  public boolean generateForwardButtonStd()
  {
    return getTaskNavigator().generateForwardButtonStd(theSessionState);

  }


  /**
   *
   *
   */
  //--> Changed the return value to boolean
  //-->   true : PROCEED (enable display)
  //-->   false : SKIP (disable display)
  //--> By BILLY 23July2002
  public boolean generateBackwardButtonStd()
  {
    return getTaskNavigator().generateBackwardButtonStd(theSessionState);

  }


  /**
   *
   *
   */
   public boolean stdNextButtonGeneration()
  {
    return true;

  }

  /**
   *
   *
   */
  ////public SavedPages getSavedPages()
  public SavedPagesModelImpl getSavedPages()
  {
    return savedPages;

  }


  /**
   *
   *
   */
  public ViewBean getCurrNDPage()
  {
    return currNDPage;

  }


  /**
   * Should be re-eimplemented as a SavedPagesModel (Session Model).
   * Should be got from the JATO Model Management API.
   *
   */
  public void setSavedPages(SavedPagesModelImpl savedPages)
  {
    this.savedPages = savedPages;
  }


  /**
   *
   *
   */
  public void navigateToNextPage(boolean txRequired)
  {
    //logger.debug("PHC@navigateToNextPage(true):: start");
    SessionResourceKit srk = getSessionResourceKit();
    try
    {
      //logger.debug("PHC@navigateToNextPage(true)::srk: " + srk);
      //logger.debug("PHC@navigateToNextPage(true):: I before begin transaction");
      //logger.debug("PHC@navigateToNextPage(true)::UserProfileId: " + srk.getUserProfileId());
      //logger.debug("PHC@navigateToNextPage(true)::IsInTransaction?: " + srk.isInTransaction());
      //logger.debug("PHC@navigateToNextPage(true)::IsAuditIncomplete?: " + srk.isAuditOn());
      //logger.debug("PHC@navigateToNextPage(true)::Identity: " + srk.getIdentity());
      //logger.debug("PHC@navigateToNextPage(true)::Modified?: " + srk.getModified());

      srk.beginTransaction();
      //logger.debug("PHC@navigateToNextPage(true):: I after begin transaction");
      navigateToNextPage();
      //logger.debug("PHC@navigateToNextPage(true):: I am after navigate to nextPage");
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      logger.error("Exception in NavigateToNextPage : " + e);
      //--> ForDebug by Billy
      //logger.error(StringUtil.stack2string(e));
      //=====================
      srk.cleanTransaction();
      ////if (theSession.isActMessageSet() == true) return;
      if (theSessionState.isActMessageSet() == true)
        return;
      setStandardFailMessage();
    }

  }


  /**
   *
   *
   */
  public void navigateToNextPage()
    throws Exception
  {
    ////logger.debug("Performance Check :: @PHC.navigateToNextPage Started");
    ////getSessionResourceKit().getJdbcExecutor().startDBPerformanceCheck();
    ////logger.debug("BILLY =====> db performance Checking Started !!");

    getPageStateHandler().navigateToNextPage(theSessionState, savedPages, getSessionResourceKit());

    ////logger.debug("BILLY =====> db performance Checking Stopped :: Total Time in Millsec = " +
    ////  getSessionResourceKit().getJdbcExecutor().stopDBPerformanceCheck() +
    ////  " Total SQL = " + getSessionResourceKit().getJdbcExecutor().getDBCount());
    ////logger.debug("Performance Check :: @PHC.navigateToNextPage Ended");
  }


//  /**
//   *
//   *
//   */
//  public void cleanCurrentPage()
//    throws Exception
//  {
//    getPageStateHandler().cleanCurrentPage(theSessionState, savedPages, getSessionResourceKit(), false);
//  }


  /**
   *
   *
   */
  public void navigateAwayFromFromPage(boolean isFailure)
  {
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    // basically trigger standard navigation ensuring no 'next' page set
    try
    {
      setAwayNextPage(getSessionResourceKit(), pg, savedPages, ! isFailure);

      //--DJ_CR136--start--//
      //// In case of LifeDisability screen logic is changed
      //// because of RTP/Infocal calc legacy implementation
      if(pg.getPageId() == Mc.PGMN_LIFE_DISABILITY_INSURANCE)
      {
        logger.debug("PHC@navigateAwayFromFromPage: LDI page case");
        isFailure = false;
      }
      //--DJ_CR136--end--//

      if (isFailure) setStandardFailMessage();
      navigateToNextPage(true);
      return;
    }
    catch(Exception e)
    {
      srk.cleanTransaction();
      logger.error("Exception @PHC.navigateAwayFromFromPage: problem setting next page - ignored (to queue)");
      logger.error(e);
      try
      {
        setAwayNextPage(getSessionResourceKit(), pg, savedPages, false);

        setStandardFailMessage();
        navigateToNextPage(true);
        return;
      }
      catch(Exception e2)
      {
        srk.cleanTransaction();
      }
    }


    // real bad !!! logoff
    handleSignOff(true);

  }


  /**
   *
   *
   */
  private void setAwayNextPage(SessionResourceKit srk, PageEntry currPage, SavedPagesModelImpl savedPages, boolean backupAllowed)
  {
    if (backupAllowed == true && currPage != null && currPage.isSubPage() == true)
    {
      // back to parent
      savedPages.setNextPage(currPage.getParentPage());
      return;
    }

    //--CervusPhaseII--start--//
    boolean overrideDealLockCancelBtn = false; //default
    overrideDealLockCancelBtn = theSessionState.isOverrideDealLock();

    //logger.debug("PHC@setAwayNextPage::OverrideDealLockFromSession? " + overrideDealLockCancelBtn);

    // away from page: original handleCancelStandard(boolean value) implementation.
    if (savedPages.getWorkQueue() != null && savedPages.getWorkQueue().getPageId() > 0 && overrideDealLockCancelBtn == false)
    {
      savedPages.setNextPageAsWorkQueue();
    }

    else if (savedPages.getSearchPage() != null && overrideDealLockCancelBtn == false)
    {
      savedPages.setNextPageAsDealSearch();
    }

    // away from page when in override deal lock mode.
    if (overrideDealLockCancelBtn == true)
    {
      theSessionState.setActMessage(null);

      if( savedPages.getPreviousPagesQueue().getQueueSize() > 0  &&
          savedPages.getSearchPage() == null)
      {
        savedPages.setNextPageAsPreviousPage(savedPages.getPreviousPagesQueue().
                                             get(0).getPageId());
      }

      else if( savedPages.getPreviousPagesQueue().getQueueSize() > 0  &&
          savedPages.getSearchPage() != null)
      {
        savedPages.setNextPageAsDealSearch();
      }

      else if( savedPages.getPreviousPagesQueue().getQueueSize() == 0 )
      {
        if (savedPages.getSearchPage() != null)
        {
          savedPages.setNextPageAsDealSearch();
        }
        else if ( savedPages.getWorkQueue() != null && savedPages.getWorkQueue().getPageId() > 0 )
        {
            savedPages.setNextPageAsWorkQueue();
        }
      }

      theSessionState.overrideDealLock(false);
      savedPages.setSearchPage(null);
    } // end if override deallock mode.
    //--CervusPhaseII--end--//
  }

  /**
   *  This should be reconsidered in consistency with the new BasisXpress
   *  session Model implementation.
   *
   */
  public void getSessionObjects()
  {
    ////CSpSession session = CSpider.getUserSession();
    theSession = RequestManager.getRequestContext().getRequest().getSession();

    //We should get the SessionStateModelImp from HTTPSession here
    //--> Changed by BILLY 03July2002
    ////theSession =(SessionState) session.get(US_THE_SESSION);
    //theSession =(HttpSession) theSessionState.getValue(US_THE_SESSION);
    String defaultInstanceStateName =
      RequestManager.getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    theSessionState =
      (SessionStateModelImpl)RequestManager.getRequestContext().getModelManager().getModel(
                      SessionStateModel.class,
                      defaultInstanceStateName,
                      true,
                      true);
    //// The session could not be null. Container is responsible to provide it.
    //// This fragment should be eliminated.
    ////if (theSession == null)
    ////{
    ////    theSession = new SessionState();
    ////}

    //--> Modified by Billy to prevent Memory leak
    //--> 11April2003
    //savedPages =(SavedPagesModelImpl) theSessionState.getValue(US_SAVED_PAGES);
    savedPages =(SavedPagesModelImpl) theSession.getAttribute(US_SAVED_PAGES);
    //==========================================================================

    if (savedPages == null)
    {
      savedPages = new SavedPagesModelImpl();
    }
  }

  /**
   *  This should be reconsidered in consistency with the new BasisXpress
   *  session Model implementation.
   *
   *  see retrieveSavedSessionObjects(....) method from the SessionStateModelImpl.java
   *  class.
   *
   */
  protected void updateSessionObjects()
  {

    // ensure synchronization of signleton page entries,  individual work queue and search
    // page.
    //
    //      When the current page is one of these pages the saved pages reference to
    //      same may not necessarilly be the same object - i.e. guaranteed not to be
    //      after a round trip to the persistent engine (PE)

    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
    //logger.debug("PHC@updateSessionObjects::PageName: " + pg.getPageName());

    if (pg.getPageId() == Mc.PGNM_INDIV_WORK_QUEUE_ID) savedPages.setWorkQueue(pg);
    //--CervusPhaseII--start--//
    // There is no need to set this each time here!! (it is done in handler).
    ////else if (pg.getPageId() == Mc.PGNM_DEAL_SEARCH_ID) savedPages.setSearchPage(pg);
    //--CervusPhaseII--end--//

    ////CSpSession session = CSpider.getUserSession();
    theSession = RequestManager.getRequestContext().getRequest().getSession();
    ////session.put(US_SAVED_PAGES, savedPages);
    ////session.put(US_THE_SESSION, theSession);

    //--> Modified by Billy to prevent Memory leak
    //--> 11April2003
    //theSessionState.setValue(US_SAVED_PAGES, savedPages);
    theSession.setAttribute(US_SAVED_PAGES, savedPages);

    //It is not necessary to save it manually to the HTTPSession.
    //Because the JATO framework will do it for us when request done.
    //--> Commented out by BILLY 03July2002
    String defaultInstanceStateName =
        RequestManager.getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    //theSessionState.setValue(defaultInstanceStateName, theSession);
    //--> BUG !! -- Changed by Billy
    //theSession.setAttribute(defaultInstanceStateName, theSession);
    theSession.setAttribute(defaultInstanceStateName, theSessionState);
    //=======================================================================

    //logger.debug("PHC@updateSessionObjects::finished SessionObjects updating");
  }

  /**
   *
   * The next two methods formally adjusted to the JATO framework, but
   * in reality it seems there is no need to have them. TiledView stuff
   * should take care of it.
   */
  public void setRowGeneratorDOForNRows(int numRows)
  {
    ////CSpCriteriaSQLObject rgDO =(CSpCriteriaSQLObject) RequestManager.getRequestContext().getModelManager().getModel(doRowGeneratorModel.class);
    SelectQueryModel theModel =(SelectQueryModel) RequestManager.getRequestContext().getModelManager().
                                                      getModel(doRowGeneratorModel.class);

    theModel.clearUserWhereCriteria();

    ////rgDO.theModel.addUserWhereCriterion("dfRowNdx", CSpCriteriaSQLObject.LESS_THAN_EQUAL_TO_INT_OPERATOR, new Integer(numRows));
      theModel.addUserWhereCriterion("dfRowNdx", "<=", new Integer(numRows));

  }


  /**
   *
   *
   */
  public void setRowGenerator2DOForNRows(int numRows)
  {
    SelectQueryModel theModel =(SelectQueryModel) RequestManager.getRequestContext().getModelManager().getModel(doRowGenerator2Model.class);

    theModel.clearUserWhereCriteria();

    theModel.addUserWhereCriterion("dfRowNdx", "<=", new Integer(numRows));

  }


  /**
   *
   *
   */
  public TaskNavigator getTaskNavigator()
  {
    return taskNavigator;

  }


  /**
   *
   *
   */
  public PageStateHandler getPageStateHandler()
  {
    return pageStateHandler;

  }


  /**
   * Should be redesign in accordancy with the JATO framework. Temporarily commented out,
   * as it needs for the DealHandlerCommon/DealEntryHandler framework method only.
   */
  protected int getIdValueFromPage(String varLabel)
  {
    return getIdValueFromPage(varLabel, - 1);

  }

  /*
   *
   *
   *
   */
  protected int getIdValueFromPage(String varLabel, int rowindex)
  {
    int idval = 0;

    try
    {
      idval =(int) Double.parseDouble(readFromPage(varLabel, rowindex));
    }
    catch(Exception e)
    {
      // e.g. page without this display field
      logger.debug("Exception @PageHandlerCommon.getIdValueFromPage : Getting id value for fld<" + varLabel + ">, row = " + rowindex + " - fld likely not on page - ignored:" + e);		//#DG702
      idval = 0;
    }

    return idval;

  }

  
  /**
  * <p>readFromPage</p>
  * <p>original method before MCM that used include all parocess 
  * which currently included in the next method</p>
  *
  *
  * @param String pageVar
  * @param int rowIndex
  * @return String value from the current page
  * 
  * @version 1.1 MCM Team XS 2.60
  *
  */
  protected String readFromPage(String pageVar, int rowindex) {
      
      return readFromPage(pageVar,  rowindex, getCurrNDPage());
  }

  /**
   * <p>readFromPage</p>
   * <p>added parent viewBean to handle pagelet and tile view</p> 
   *
   * @param String pageVar
   * @param int rowIndex
   * @param parent (viewBean)
   * @return String value from the viewBean
   * 
   * @version 1.1 MCM Team XS 2.60
   */
  protected String readFromPage(String pageVar, int rowindex, ViewBean parent)
  {
    //logger.debug("PHC@readFromPage::RowIndxIn: " + rowindex);
    //logger.debug("PHC@readFromPage::PageVar: " + pageVar);

    if ((pageVar == null) ||(pageVar.equals(""))) return "";

    try
    {
      //Check if this is a TiledView
      String [] repFields = new String [2];
      StringTokenizer rep = new StringTokenizer(pageVar, "/");

      try
      {
        repFields[0] = rep.nextToken(); // repeatable Name
        repFields[1] = rep.nextToken(); // repeatable FieldName
      }
      catch(NoSuchElementException ex)
      {
        ;
      }

      //logger.debug("PHC@readFromPage::repFields[0]: " + repFields[0]);
      //logger.debug("PHC@readFromPage::repFields[1]: " + repFields[1]);

      if(repFields[0] != null && !repFields[0].trim().equals("") && repFields[1] != null && !repFields[1].trim().equals(""))
      { 
        //MCM TEAM: use parent XS 2.60
        TiledViewBase tiledView = (TiledViewBase) parent.getChild(repFields[0]);

        //--> Should try to avoid to use nextTile call. Because this will cause the NextTile event
        //--> executed multiple time and will lost some input value as well
        //--> Changed by BILLY 06Aug2002
        //tiledView.resetTileIndex();
        //while (tiledView.nextTile())
        //logger.debug("BILLY ==> PHC@readFromPage::tiledView.getNumTiles()=" + tiledView.getNumTiles());
        //while(tileCounter < tiledView.getNumTiles())
        //{
        //  tiledView.setTileIndex(tileCounter);
        //  logger.debug("PHC@readFromPage::tileCounter: " + tileCounter++ + " The FieldValue = " + tiledView.getDisplayFieldValue(repFields[1]));
        //}
        //--> The above is for debug purpose only.  Will be removed later.
        //==============================================================

        tiledView.setTileIndex(rowindex);
        //logger.debug("PHC@readFromPage:: rowindex " + rowindex + "::FieldName=" + pageVar
        //  + " ::FieldValue=" + tiledView.getDisplayFieldValue(repFields[1]));
        return (tiledView.getDisplayFieldValue(repFields[1])).toString();

      }

      ////DisplayField filedValue = getCurrNDPage().getDisplayField(pageVar);
      //MCM TEAM - XS2.60 use Parent
      HtmlDisplayFieldBase filedValue = (HtmlDisplayFieldBase)parent.getDisplayField( pageVar );
      //logger.debug("PHC@readFromPage::DisplayField: " + filedValue);

      if (filedValue == null)
      {
        logger.debug("Exception @PageHandlerCommon.readFromPage : No display fld<" + pageVar + "> on page - ignored");
        return "";
      }

      //If rowindex less than 0 or -1 then this var doesn't have repeating rows
      //so just return the string Value of this
      //// This works perfect for the non-repeatable rows.
      String st = "";
      if (rowindex < 0)
      {
        st = filedValue.getValue().toString();
        //logger.debug("PHC@readFromPage_NonRepeatable::dfValue:" + st);

        ////return filedValue.getValue().toString();
        //// or even better pure JATO idiom
        return filedValue.stringValue();
      }
      ////st =(filedValue.getValue(rowindex)).toString();
      ////return(filedValue.getValue(rowindex).toString());

      //// Should be the same getValue() signature. We take care
      //// of the index with getTileIndex() method at the moment of
      //// call in the DealHandlerCommon class.

      st =(filedValue.getValue()).toString();

      //logger.debug("PHC@readFromPage_Repeatable::dfValue:" + st);

      return(filedValue.getValue().toString());

    }
    catch(Exception e)
    {
      logger.debug("Exception @PageHandlerCommon.readFromPage : Getting value for display fld<" + pageVar + ">, row = " + rowindex + " - ignored:" + e);		//#DG702
      return "";
    }

  }

  /**
   *
   *
   */
  protected String readFromPageForTextBox(String pageVar, int rowindex)
  {
    try
    {
      Object filedValueList =(Object)(getCurrNDPage().getDisplayFieldValue(pageVar));
      if (filedValueList == null)
      {
        logger.debug("Exception @PageHandlerCommon.readFromPageForTextBox : No display fld<" + pageVar + "> on page - ignored");
        return "";
      }

      //If rowindex less than 0 or -1 then this var doesn't have repeating rows
      //so just return the string Value of this
      if (rowindex < 0) return filedValueList.toString();
      if (filedValueList instanceof Vector)
      {
        return((Vector) filedValueList).get(rowindex).toString();
      }
      else
      {
        //just a guard incase instance is still not vector but caller given
        //wrong rowindex
        return filedValueList.toString();
      }
    }
    catch(Exception e)
    {
      logger.debug("Exception @PageHandlerCommon.readFromPageForTextBox : Getting value from display fld<" + pageVar + ">, row = " + rowindex + " - ignored:" + e);		//#DG702
      return "";
    }

  }

  protected double getDoubleValue(String sNum)
  {
    if (sNum == null)
      return 0;

    sNum = sNum.trim();

    if (sNum.length() == 0)
      return 0;

    try
    {
      // handle decimal (e.g. float type) representation
      return Double.parseDouble(sNum);
    }
    catch (Exception e) {;}

    return 0;
  }


  /**
   *
   *
   */
  protected int getIntValue(String sNum)
  {
    if (sNum == null) return 0;

    sNum = sNum.trim();

    if (sNum.length() == 0) return 0;

    try
    {
      // handle decimal (e.g. float type) representation
      int ndx = sNum.indexOf(".");
      if (ndx != - 1) sNum = sNum.substring(0, ndx);
      return Integer.parseInt(sNum.trim());
    }
    catch(Exception e)
    {
      ;
    }

    return 0;

  }


  /**
   *
   *
   */
  protected int getIntValCompComboLine(String sCompositeComboLine)
  {
    if (sCompositeComboLine == null)
      return 0;

    sCompositeComboLine = sCompositeComboLine.trim();

    if (sCompositeComboLine.length() == 0)
      return 0;

    try
    {
      // handle the second int element of a composite combo box value
      int ndx = sCompositeComboLine.indexOf(",");
      if (ndx != -1)
      sCompositeComboLine = sCompositeComboLine.substring(ndx + 1);

      return getIntValue(sCompositeComboLine.trim());
    }
    catch (Exception e) {;}

    return 0;

  }


  /**
   *
   *
   */
  protected String getIntRateFromComboBox(String sRateComboLine)
  {
    if (sRateComboLine == null) return "";

    sRateComboLine = sRateComboLine.trim();

    if (sRateComboLine.length() == 0) return "";

    try
    {
      // handle the String element of a composite rate combo box value
      int ndx = sRateComboLine.indexOf("%");
      if (ndx != - 1) sRateComboLine = sRateComboLine.substring(0, ndx);
      return sRateComboLine.trim();
    }
    catch(Exception e)
    {
      ;
    }

    return "";

  }


  /**
   *
   *
   */
  protected String getDateRateFromComboBox(String sRateComboLine)
  {
    if (sRateComboLine == null) return "";

    sRateComboLine = sRateComboLine.trim();

    if (sRateComboLine.length() == 0) return "";

    try
    {
      // handle the second (date) element of a composite combo box line
      int ndx = sRateComboLine.indexOf("%");
      if (ndx != - 1) sRateComboLine = sRateComboLine.substring(ndx + 1);
      return sRateComboLine.trim();
    }
    catch(Exception e)
    {
      ;
    }

    return "";

  }


  /**
   * ClearBeforeDisplay() method is implemented in JATO framework now.
   *
   */
  protected void customCleanUpTextBox(String tbName)
  {
    try
    {
    ////CSpTextBox textbox =(CSpTextBox) getCurrNDPage().getDisplayField(tbName);
      TextField textbox = (TextField) getCurrNDPage().getDisplayField(tbName);

      // 1. Customize colors, borders, etc.,
      String changeColorStyle = new String("style=\"background-color:d1ebff\"" + " style=\"border-bottom: hidden d1ebff 0\"" + " style=\"border-left: hidden d1ebff 0\"" + " style=\"border-right: hidden d1ebff 0\"" + " style=\"border-top: hidden d1ebff 0\"" + " style=\"font-weight: bold\"" + " style=\"font-size: xx-small\"");
      textbox.setExtraHtml(changeColorStyle);

      // there is no need in this now.
      ////getCurrNDPage().clearBeforeDisplay();
    }
    catch(Exception e)
    {
      setStandardFailMessage();
      logger.error("Exception PageHandlerCommon@customCleanUpTextBox: Problem encountered in textbox: " + tbName+":" + e);		//#DG702
    }

  }


  /**
   * ClearBeforeDisplay() method is implemented in JATO framework now.
   *
   */
  protected void customDefaultTextBox(String tbName)
  {
    try
    {
      ////CSpTextBox textbox =(CSpTextBox) getCurrNDPage().getDisplayField(tbName);
      TextField textbox = (TextField) getCurrNDPage().getDisplayField(tbName);

      textbox.setExtraHtml("default");

      // there is no need in this now.
      ////getCurrNDPage().clearBeforeDisplay();
    }
    catch(Exception e)
    {
      setStandardFailMessage();
      logger.error("Exception PageHandlerCommon@customDefaultTextBox: Problem encountered in textbox: " + tbName+":" + e);		//#DG702
    }

  }

  /**
   * The following idiom in ND:
   *
   * 1. <dataObject>.clearDynamicCriteria()
   * 2. getDisplayFieldValue
   * 3. addDynamicCriterion("<dataFieldName>",
   *                         CSpCriteriaSQLObject.STRING_CRITERIA_TYPE,
   *                         <CSpValue criterionValue>)
   * 4. theDO.execute()
   * 5. load the target results page
   *
   * has an equavalent in JATO as follows:
   *
   * 1. <model>.clearUserWhereCriteria()
   * 2. getDisplayFieldValue("<displayFieldValue>");
   * 3. <model>addUserWhereCriterion("<dataFieldName>",
   *                                 "<operator>",
   *                                 <objectCritVal>)
   * 4. theModel.executeSelect(null);
   * 5. <targetViewBean>.forwardTo(getRequestContext())
   *
   * Model MUST BE bounded propertly to the target ViewBean (see more on the model/view bounding issue).
   * Last two two steps is provided in the BasisXpress framework methods (see more on that).
   *
   * The following method is already adjusted (no original code, see setupDealSummarySnapShopDO(....)
   * method for this.
   *
   */
  public void setupUWDealSummarySnapShotDO(PageEntry pg)
  {
    try
    {
      //logger.debug("--T--> @PHC.setupUWDealSummarySnapShot: check the dealid&copyId");

      int dealId = pg.getPageDealId();
      int copyId = pg.getPageDealCID();

      doUWDealSummarySnapShotModel theModel =(doUWDealSummarySnapShotModel)RequestManager.getRequestContext().getModelManager().getModel(doUWDealSummarySnapShotModel.class);

      theModel.clearUserWhereCriteria();

      if (dealId <= 0)
      {
        theModel.addUserWhereCriterion("dfDealID", "=", new Integer(- 999));
        pg.setPageDealPrimaryBorrowerName(" ");

        //logger.debug("--T--> @PHC.setupUWDealSummarySnapShot: I am in dealid<=0 mode");
      }
      else
      {
        //logger.debug("--T--> @PHC.setupUWDealSummarySnapShot: I am in the normal mode");

        theModel.addUserWhereCriterion("dfDealID", "=", new Integer(dealId));
        theModel.addUserWhereCriterion("dfCopyId", "=", new Integer(copyId));
        pg.setPageDealPrimaryBorrowerName(queryPrimaryBorrowerName(dealId, copyId));

        //logger.debug("--T--> @PHC.setupUWDealSummarySnapShot:SQL: " + ((doUWDealSummarySnapShotModelImpl)theModel).getSelectSQL());
        //logger.debug("--T--> @PHC.setupUWDealSummarySnapShot: Primary Borrower is set up to: " + pg.getPageDealPrimaryBorrowerName());
      }
    }
    catch(Exception e)
    {
      ;
    }
  }

  /**
   *
   *
   */
  protected void restrictedDynamicLPRCombos(String lenderCbName, String productCbName, String postedRateCbName)
  {
    try
    {
      ComboBox cblender =(ComboBox) getCurrNDPage().getDisplayField(lenderCbName);
      ComboBox cbproduct =(ComboBox) getCurrNDPage().getDisplayField(productCbName);
      ComboBox cbpostrate =(ComboBox) getCurrNDPage().getDisplayField(postedRateCbName);
      String onChangeLender = new String("onChange=\"getListRestricted('" + productCbName + "')\"");
      String onChangeProduct = new String("onChange=\"getListRestricted('" + postedRateCbName + "')\"");
      String onChangeRate = new String("onChange=\"populateRateCode()\"");
      cblender.setExtraHtml(onChangeLender);
      cbproduct.setExtraHtml(onChangeProduct);
      cbpostrate.setExtraHtml(onChangeRate);
    }
    catch(Exception e)
    {
      setStandardFailMessage();
      logger.error("Exception PageHandlerCommon@CustomizeComboBox: Problem encountered in combobox customization:" + e);		//#DG702
    }

  }


  /**
   *
   * Adjusted to the JATO architecture.
   *
   *
   */
  public void handleRateLockPassToHtml(ViewBean pg, ChildDisplayEvent event)
  {
    PageEntry currPg = theSessionState.getCurrentPage();

    //1. retreive the deal id value to work with the data object
    int dealId = currPg.getPageDealId();
    Integer cspDealId = new Integer(dealId);
    int copyid = currPg.getPageDealCID();

    //logger.debug("DEH@handleRateTimeStampPassToHtml::DealId: " + dealId +
    //  "::CopyId:" + copyid);

    Integer cspDealCPId = new Integer(copyid);
    String rateLock = "N";

    //1. get the data object whose data will be used to initiate
    //   the static field value
    doDealRateLockModel theModel = (doDealRateLockModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doDealRateLockModel.class);

    theModel.clearUserWhereCriteria();
    theModel.addUserWhereCriterion("dfDealId", "=", cspDealId);
    theModel.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);

    try
    {
      ResultSet rs = theModel.executeSelect(null);

      if(rs == null || theModel.getSize() < 0)
        rateLock = "N";
      else if(rs != null && theModel.getSize() > 0)
      {
          //logger.debug("PHC@handleRateLockPassToHtml::Size of the result set: " + theModel.getSize());
          while (theModel.next()) {
                Object oRateLock = theModel.getDfRateLock();
                if (oRateLock == null)
                {
                  rateLock = "";
                }
                else if(oRateLock != null)
                {
                  rateLock = oRateLock.toString();
                  //logger.debug("PHC@setPhoneNoDisplayField::OnBeforeRowDisplayEvent_dfValueFromModel: " + oRateLock.toString());
                }
              }
          }

    }
    catch (ModelControlException mce)
    {
      logger.debug("PHC@handleRateLockPassToHtml::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.debug("PHC@handleRateLockPassToHtml::SQLException: " + sqle);
    }

    //// Obsolete ND fragment.
    ////DisplayField field =(DisplayField) event.getSource();

    String fieldName = (String) event.getChildName();
    DisplayField fld = (DisplayField) pg.getChild(fieldName);

    //logger.debug("PHC@handleRateLockPassToHtml::fieldName: " + fieldName);
    //logger.debug("PHC@handleRateLockPassToHtml::fld: " + fld);

    fld.setValue(rateLock);

  }

  // Method to set the phone num display with format (###)###-#### x ###### -- by BILLY 12Feb2001

  protected void setPhoneNumTextDisplayField(SelectQueryModel dobject,
                                              String phoneNumDisplay,
                                              String dfPhoneNumName,
                                              String dfExtName,
                                              int rowNum)
  {

    try
    {

      try
      {
          String theDisplayString = "";
          String phoneNo = "";
          String extNo = "";

          Object oDfValue = dobject.getValue(dfPhoneNumName);
          if (oDfValue == null)
          {
            phoneNo = "";
          }
          else if((oDfValue != null))
          {
            phoneNo = oDfValue.toString();
            //logger.debug("PHC@setPhoneNumTextDisplayField::PhoneNoFromModel: " + oDfValue.toString());
          }

          if( !dfExtName.equals("") )
          {
            ////extNo = dobject.getValue(rowNum, dfExtName).stringValue();
            Object oExtNo = dobject.getValue(dfExtName);
            if (oExtNo == null)
            {
              extNo = "";
            }
            else if((oExtNo != null))
            {
              extNo = oExtNo.toString();
              //logger.debug("PHC@setPhoneNumTextDisplayField::ExtFromModel: " + oExtNo.toString());
            }

            theDisplayString = formatPhoneNumber(phoneNo, extNo);
          }
          else
          {
            theDisplayString = formatPhoneNumber(phoneNo);
          }

          //// row indxing is done by JATO framework in the nextTile() method in each TiledView bean implementation.
          // Set display field
          getCurrNDPage().setDisplayFieldValue(phoneNumDisplay, new String(theDisplayString));

        }
        catch (Exception e)
        {
          logger.error("Exception @setPhoneNumTextDisplayField: df=" + dfPhoneNumName + "(rowNum=" + rowNum + "), exception=" + e);		//#DG702
        }

      }
      catch (Exception e)
      {
        logger.error("Exception @setPhoneNumTextDisplayField: df=" + dfPhoneNumName + "," + dfExtName +
            "(rowNum=" + rowNum + "), exception=" + e);		//#DG702
      }
  }

/////////////////
/////////Set Terms For Display Methods
////////////////
  protected String []  getArrayFromModel(SelectQueryModel dobject,
                                         String dfName)
  {

    ResultSet rs = null;
    try
    {
      rs = dobject.executeSelect(null);
      String [] vals = new String[dobject.getSize()];
      if(rs == null || dobject.getSize() < 0)
      {
        vals[0] = "";
        return vals;
      }

      else if(rs != null && dobject.getSize() > 0)
      {
        //logger.debug("PHC@getArrayFromModel::Size of the result set: " + dobject.getSize());
        //logger.debug("PHC@getArrayFromModel:: dfIn=" + dfName);
        int cnt = 0;

          while (dobject.next())
          {
            Object oDfValue = dobject.getValue(dfName);
            if (oDfValue == null)
            {
              vals[0] = "";
            }
            else if(oDfValue != null)
            {
              vals[cnt] = oDfValue.toString();
              //logger.debug("PHC@setPhoneNoDisplayField::OnBeforeRowDisplayEvent_2_dfValueFromModel[" + cnt + "]: " + vals[cnt]);
              cnt++;
            }
          }

           return vals;
        }
    }
    catch (ModelControlException mce)
    {
      logger.debug("PHC@getMITypeIndex::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.debug("PHC@getMITypeIndex::SQLException: " + sqle);
    }

    return new String[0];
}

//// VERY IMPORTANT!!! All three original sets of the display field settings are totally
//// re-done.
////
//// 1. For each set the number of methods is reduced from four to just one, that
//// used to be called from the ND OnBeforeRowDisplay event.
////
//// 2. It serves current BX project needs because the JATO container is takes care
//// to distinguish the repeatable/non-repeatable entity. Repeatable entity must be
//// called as RepeatableName/HtmlElementName. Forward slash must be used as a separator.
////
//// 3. IMPORTANT. In the non-repeatable case the call must be done by default with the rowIndx value equals 0.
//// For example, setDealMainDisplayFields(dodealentrymain, 0).
//// For the repeatable field call the iteration through the indexing is done in the appropriate TiledView bean in the
//// tiledView.nextTile() method.
////
//// 4. IMPORTANT. The execution of a Model must be done before the calling of an appropriate method (
//// (setTermsDisplayField(...), etc). It allows escape the iteration using the tiledView.nextTile()
//// method. This iteration sets the index to its last position (by default) and as a result executes
//// the method only for the last row. See Billy's comments in the readFromPage(...) method on the related
//// issues as well.
////
//// 5. IMPORTANT. As it can be seen from the adjusted method(s) below the only row by row execution is left.
//// The movement of the execution logic to the BX handler classes for all rows and iteration through the loop
//// there brings the significant problem of data/row syncronization between the executed and Default JATO models.
//// Theoretically it is possible to do, but the implementation is postponed in case if we really need this down the road.

/////////////////
/////////Set Terms For Display Methods - this one for Main Page like no repeat attribs
////////////////


  // For setting display value to repeat row by row.  Used in OnBeforeRowDisplay event only
  //    -- By BILLY 16Jan2001
  protected void setTermsDisplayField(SelectQueryModel dobject,
                                      String yearsDisplay,
                                      String monthsDiplay,
                                      String dfName,
                                      int rowNum)
  {

    //// Getting the corresponding row data from the Model
    try
    {
        int termval = 0;

        Object oDfValue = dobject.getValue(dfName);
        if (oDfValue == null)
        {
          termval = 0;
        }
        else if((oDfValue != null))
        {
          termval = (new Integer(oDfValue.toString())).intValue();
          //logger.debug("PHC@setTermsDisplayField::dfValueFromModel: " + (new Integer(oDfValue.toString())).intValue());
        }
        //logger.debug("PHC@setTermsDisplayField::TermVal: " + termval);

        int[] terms = parseTerms(termval);

        //logger.debug("PHC@setTermsDisplayField::terms[0]: " + terms[0]);
        //logger.debug("PHC@setTermsDisplayField::terms[1]: " + terms[1]);

        //// row indxing is done by JATO framework in the nextTile() method in each TiledView bean implementation.
        ((DisplayField)getCurrNDPage().getDisplayField(yearsDisplay)).setValue((new Integer(terms[0])).toString());
        ((DisplayField)getCurrNDPage().getDisplayField(monthsDiplay)).setValue((new Integer(terms[1])).toString());

    }
    catch (Exception e)
    {
      logger.error("Exception @setTermsDisplayField: df=" + dfName + "(rowNum=" + rowNum + "), exception=" + e);		//#DG702
    }

  }

  //////////////////end of terms display field

  /////////////////
  /////////Set Phone No For Display Methods - this one for Main Page like no repeat attribs
  ////////////////

  protected void setPhoneNoDisplayField(SelectQueryModel dobject,
                                        String areaCodeDisplay,
                                        String exchCodeDisplay,
                                        String routeCodeDisplay,
                                        String dfName,
                                        int rowNum)
  {

    // Getting the corresponding row data from DataObject
    String phoneNo = "";

    Object oPhoneNo = dobject.getValue(dfName);
    if (oPhoneNo == null)
    {
      phoneNo = "";
    }
    else if(oPhoneNo != null)
    {
      phoneNo = oPhoneNo.toString();
      //logger.debug("PHC@setPhoneNoDisplayField::dfValueFromModel: " + oPhoneNo.toString());
    }

    try
    {
      String[] parsePhone = parsePhoneString(phoneNo);

      //logger.debug("PHC@setPhoneNoDisplayField::ParsePhone[0]: " + parsePhone[0]);
      //logger.debug("PHC@setPhoneNoDisplayField::ParsePhone[1]: " + parsePhone[1]);
      //logger.debug("PHC@setPhoneNoDisplayField::ParsePhone[2]: " + parsePhone[2]);

      //logger.debug("PHC@setPhoneNoDisplayField::" + (getCurrNDPage().getDisplayField(areaCodeDisplay)).getClass().getName());

      ((TextField)getCurrNDPage().getDisplayField(areaCodeDisplay)).setValue(new String(parsePhone[0]));
      ((TextField)getCurrNDPage().getDisplayField(exchCodeDisplay)).setValue(new String(parsePhone[1]));
      ((TextField)getCurrNDPage().getDisplayField(routeCodeDisplay)).setValue(new String(parsePhone[2]));

    }
    catch (Exception e)
    {
      logger.error("Exception @setPhoneNoDisplayField3_NonRepeatable: df=" + dfName + "\nException:"
        + StringUtil.stack2string(e));
    }

  }

/////////////////// End Set Phone Display methods

  //////////////Set Display Field for Date Values
  protected void setDateDisplayField(SelectQueryModel resultbl,
                                  String monthOfDate,
                                  String dayOfDate,
                                  String yearOfDate,
                                  String dfName,
                                  int rowindx)
  {
    String dfDateString = "";

    Object oDfValue = resultbl.getValue(dfName);
    if (oDfValue == null)
    {
      dfDateString = "";
    }
    else if(oDfValue != null)
    {
      dfDateString = oDfValue.toString();
      //logger.debug("PHC@setPhoneNoDisplayField::dfValueFromModel: " + oDfValue.toString());
    }


      //Display Blank date when null  -- by BILLY 11Jan2001
      ////if((resultbl.getRowData(rowindx,dbposindx)).stringValue().equals(""))
      if(dfDateString.equals(""))
      {
        //logger.debug("PHC@setDateDisplayField::Inside the dfDateString is null");
        // Clear the Date fields
        ((ComboBox)getCurrNDPage().getDisplayField(monthOfDate)).setValue(new String("0"));
        ((TextField)getCurrNDPage().getDisplayField(dayOfDate)).setValue(new String(""));
        ((TextField)getCurrNDPage().getDisplayField(yearOfDate)).setValue(new String(""));

        return;
      }

      String[] dateTokens = parseDateStringToTokens(dfDateString);

      //logger.debug("PHC@setDateDisplayField::Year: " + dateTokens[0]);
      //logger.debug("PHC@setDateDisplayField::Month: " + dateTokens[1]);
      //logger.debug("PHC@setDateDisplayField::Day: " + dateTokens[2]);

      // detect null date - no field setting
      if (dateTokens == null)
      {
        //logger.debug("PHC@setDateDisplayField::dataTokens is null");
        // Clear the Date fields
        ((ComboBox)getCurrNDPage().getDisplayField(monthOfDate)).setValue(new String("0"));
        ((TextField)getCurrNDPage().getDisplayField(dayOfDate)).setValue(new String(""));
        ((TextField)getCurrNDPage().getDisplayField(yearOfDate)).setValue(new String(""));

        return;
      }

      try
      {
        //logger.debug("PHC@setDateDisplayField::ComboBox part start");

        //// Get Option object for the Month combobox field.
        ComboBox comboOptions = (ComboBox)getCurrNDPage().getDisplayField(monthOfDate);

        //logger.debug("PHC@setDateDisplayField::StringHtmlName: " + monthOfDate);
        //logger.debug("PHC@setDateDisplayField::ComboOptionsName: " + comboOptions.getName());

        int indx = Integer.parseInt(dateTokens[1]);
        //logger.debug("PHC@setDateDisplayField::RepWithDBPositioning_ComboIndx: " + indx);

        //// obsolete ND call.
        ////combo.select(indx);

        //Set the selected option of the ComboBox
        comboOptions.setValue((new Integer(indx)).toString(), true);
      }
      catch (Exception e){;}

      //logger.debug("PHC@setDateDisplayField::Before get DisplayField");

      ((TextField)getCurrNDPage().getDisplayField(dayOfDate)).setValue(new String(dateTokens[2]));
      ((TextField)getCurrNDPage().getDisplayField(yearOfDate)).setValue(new String(dateTokens[0]));

      //logger.debug("PHC@setDateDisplayField::After get DisplayField");

  }
////////////////// End of Set Date Display methods

////////////////// Methods to fill up the Month Combo boxes ////////////////
//--> It is not ncecssary to input the repeated Name as the Input DispName should include the repeated name
//--> if it's a repeated field e.g. "RepeatedName/FieldName"
//--> Modified by Billy 06Sept2002
//protected void fillupMonths(String dispName, String repeaterName)
  protected void fillupMonths(String dispName)
  {
    try
    {
      ////CSpComboBox combo = (CSpComboBox)getCurrNDPage().getDisplayField(repeaterName+dispName);
      //--> Modified By Billy 06Sept2002
      ComboBox combo = (ComboBox)getCurrNDPage().getDisplayField(dispName);
      //===========================================================

      OptionList option = (OptionList)combo.getOptions();

      //int lim = 13; // number of months in the year plus one more for the blank entry.

      //logger.error("PHC@fillupMonths::displayNameIn: " + dispName);
      //logger.error("PHC@fillupMonths::ComboBox: " + combo);

      //// Obsolete ND fragment.
      //combo.clear();
      // Bug fixed : clear() method not working, use removeAllChildren() instead -- by BILLY 11Jan2001

      //// Obsolete ND method. TODO: should be adjusted to JATO API.
      ////combo.removeAllChildren (true);
      // ============================ End of Change -- BILLY 11Jan2001 =====================

      ////String[] months = getMonthNames();

      //--Release2.1--//
      //Modified to get the Months Label from BXResources
      //--> By Billy 14Nov2002
      Collection c = BXResources
          .getPickListValuesAndDesc(theSessionState.getDealInstitutionId(), 
                                    PKL_MONTHS, theSessionState.getLanguageId());
      Iterator l = c.iterator();

      String[] theVal = new String[2];
      option.clear();
      while(l.hasNext())
      {
        theVal = (String[])(l.next());
        option.add(theVal[1], theVal[0]);
      }
      //===================================================

      //// Obsolete ND method.
      ////for (int i=0; i<months.length; i++)
      ////{
      ////  combo.addSelectable(months[i],new CSpInteger(i));
      ////}

    }
    catch (Exception e)
    {
      logger.error("Exception PHC@fillupMonths ");
      logger.error(e);
    }
  }


////////////////// Methods to fill up the Month Combo boxes -- END -- ////////////////
////////////////////////////////////////////////////////////////////////////
  // Method to get the Security Values for Login and Password change
  protected int getSecurityPolicyValue(SessionResourceKit srk,int type)
  {
    int id = 0;

    try
    {

      String    sql = " SELECT SECURITYPOLICY.VALUE FROM SECURITYPOLICY"
              + " WHERE SECURITYPOLICY.SECURITYPOLICYID = "
              + type;

      JdbcExecutor jExec = srk.getJdbcExecutor();

      int key = jExec.execute(sql);

      while (jExec.next(key))
      {
        id = jExec.getInt(key, 1);
        break;
      }

      jExec.closeData(key);
    }
    catch (Exception e)
    {
      return id;
    }
  return id;
  }

  // ================================================================================
  // New methods from Vlad -- 03Dec2001
  protected void customTextBoxWithCourier(String tbName)
  {

    try
    {
      ////CSpTextBox textbox = (CSpTextBox)getCurrNDPage().getDisplayField(tbName);
      TextField textbox = (TextField)getCurrNDPage().getDisplayField(tbName);

      // 1. Customize colors, borders, etc.,
      String changeColorStyle = new String("style=\"background-color:d1ebff\"" +
                                         " style=\"border-bottom: hidden d1ebff 0\"" +
                                         " style=\"border-left: hidden d1ebff 0\"" +
                                         " style=\"border-right: hidden d1ebff 0\"" +
                                         " style=\"border-top: hidden d1ebff 0\"" +
                                         " style=\"font-weight: bold\"");

      // 2. Change font.
      String changeFont = new String(" BODY style=\"font-family: Courier\"");

      // 3. Change letter space.
      String letterSpacing = new String(" style=\"letter-spacing: -1pt\"");

      // 4. Disable edition.
      String disableEdition = new String(" disabled");

      textbox.setExtraHtml(changeColorStyle + changeFont + letterSpacing + disableEdition);

    }
    catch(Exception e)
    {
      setStandardFailMessage();
      logger.error("Exception PageHandlerCommon@customTextBoxWithCourier: Problem encountered in combobox customization:" + e);		//#DG702
    }

  }

  protected void noAccessToTextBoxWithCourier(String tbName)
  {
    try
    {
      ////CSpTextBox textBox = (CSpTextBox)getCurrNDPage().getDisplayField(tbName);
      TextField textBox = (TextField)getCurrNDPage().getDisplayField(tbName);

      // 1. Change style.
      String changeLetterStyle = new String(" style=\"font-weight: bold\"");

      // 2. Change font.
      String changeFont = new String(" BODY style=\"font-family: Courier\"");

      // 3. Change space
      String letterSpacing = new String(" style=\"letter-spacing: -1pt\"");

      // 4. Disable edition
      String disableEdition = new String(" disabled");

      textBox.setExtraHtml(changeFont + changeLetterStyle + letterSpacing + disableEdition);

      //// Obsolete ND specific method, no need anymore.
      ////getCurrNDPage().clearBeforeDisplay();
    }
    catch(Exception e)
    {
      setStandardFailMessage();
      logger.error("Exception PageHandlerCommon@noAccessToTextBoxWithCourier: Problem encountered in textbox customization:" + e);		//#DG702
    }
  }

  // ================================================================================

  // Exit means to navigate to the next workflow screen if any
  //  -- By Billy 05Feb2002
  //  -- Moved here as common method for different screen : 07May2002
  public void handleExitStandard(boolean warn)
  {
    logger.debug("BILLY ===> in handleExitStandard :: warn = " + warn);

    if (warn == true && jumpTrap(JumpState.UNCONFIRMEDEXIT) == true)
        return;  // message set in this case

    PageEntry               pe      = theSessionState.getCurrentPage();
    SessionResourceKit  srk     = getSessionResourceKit();

    try
    {
      srk.beginTransaction();
      standardDropTxCopy(pe, false);
      navigateToNextPage();
      srk.commitTransaction();
     }
    catch (Exception e)
    {
      srk.cleanTransaction();

      logger.error("Exception occurred @handleExitStandard()");
      logger.error(e);

      setStandardFailMessage();

      return;
    }
  }

  public void setRepeatedField(String fieldFullName, int rowNdx, String value)
  {
    try
    {
      if (fieldFullName == null || fieldFullName.trim().equals(""))
      {
        logger.trace("PHC@setRepeatedField:: There in on field: " + fieldFullName);
      }

      //Check if this is a TiledView field name
      String [] repFields = new String [2];
      StringTokenizer rep = new StringTokenizer(fieldFullName, "/");
      try
      {
        repFields[0] = rep.nextToken(); // repeatable Name
        repFields[1] = rep.nextToken(); // repeatable FieldName
      }
      catch(NoSuchElementException e)
      {
        logger.trace("PHC@setRepeatedField:: There in on field: " + fieldFullName+":" + e);		//#DG702
      }

      if(repFields[0] != null && !repFields[0].trim().equals("") && repFields[1] != null && !repFields[1].trim().equals(""))
      {
        // It's a TiledView field
        TiledViewBase tiledView = (TiledViewBase) getCurrNDPage().getChild(repFields[0]);
        tiledView.setTileIndex(rowNdx);

        ////tiledView.getDisplayFieldStringValue(repFields[1]);
        tiledView.setDisplayFieldValue(repFields[1], value);
      }
      else
      {
        // It's not a TiledView Field -- then try to get the field value from the page
        ////getCurrNDPage().getDisplayFieldValue(fieldFullName).toString();
        getCurrNDPage().setDisplayFieldValue(repFields[1], value);
      }
    }
    catch(Exception e)
    {
      logger.debug("Exception @PHC.getRepeatedField: getting string from TiledViewFiled value, row = " + rowNdx + " - return empty string:" + e);		//#DG702
    }
  }


  //--> This method is re-located from MosWebHandlerCommon
  //--> Rewritten for JATO framework
  //--> Input : fieldFullName -- the TiledView name with parent TiledView name prefix
  //-->         e.g. "TiledViewName/fieldName"
  //--> By Billy 25July2002
  public String getRepeatedField(String fieldFullName, int rowNdx)
  {
    try
    {
      if (fieldFullName == null || fieldFullName.trim().equals(""))
      {
        return "";
      }

      //Check if this is a TiledView field name
      String [] repFields = new String [2];
      StringTokenizer rep = new StringTokenizer(fieldFullName, "/");
      try
      {
        repFields[0] = rep.nextToken(); // repeatable Name
        repFields[1] = rep.nextToken(); // repeatable FieldName
      }
      catch(NoSuchElementException ex)
      { ; }

      if(repFields[0] != null && !repFields[0].trim().equals("") && repFields[1] != null && !repFields[1].trim().equals(""))
      {
        // It's a TiledView field
        TiledViewBase tiledView = (TiledViewBase) getCurrNDPage().getChild(repFields[0]);
        tiledView.setTileIndex(rowNdx);

        return tiledView.getDisplayFieldStringValue(repFields[1]);
      }
      else
      {
        // It's not a TiledView Field -- then try to get the field value from the page
        return getCurrNDPage().getDisplayFieldValue(fieldFullName).toString();
      }
    }
    catch(Exception e)
    {
      logger.debug("Exception @PHC.getRepeatedField: getting string from TiledViewFiled value, row = " + rowNdx + " - return empty string:" + e);		//#DG702
    }
    return "";
  }

  //--> Method to get all Repeated (TiledView) values in a vector
  //--> Input : fieldFullName -- the TiledView name with parent TiledView name prefix
  //-->         e.g. "TiledViewName/fieldName"
  //--> By Billy 16Aug2002
  public Vector getRepeatedFieldValues(String fieldFullName)
  {
    Vector values = new Vector();
    int tileCounter = 0;

    if ((fieldFullName == null) ||(fieldFullName.equals(""))) return values;
    Object currValue = null;

    try
    {
      //Check if this is a TiledView
      String [] repFields = new String [2];
      StringTokenizer rep = new StringTokenizer(fieldFullName, "/");
      try
      {
        repFields[0] = rep.nextToken(); // repeatable Name
        repFields[1] = rep.nextToken(); // repeatable FieldName
      }
      catch(NoSuchElementException ex)
      {
        ;
      }

      if(repFields[0] != null && !repFields[0].trim().equals("") && repFields[1] != null && !repFields[1].trim().equals(""))
      {
        // It's a TiledView field
        TiledViewBase tiledView = (TiledViewBase) getCurrNDPage().getChild(repFields[0]);
        tiledView.resetTileIndex();

        for (tileCounter=0; tileCounter<tiledView.getNumTiles();tileCounter++)
        {
          tiledView.setTileIndex(tileCounter);
          currValue = tiledView.getDisplayFieldValue(repFields[1]);
          values.add(currValue);

          logger.debug("PHC@getRepeatedFieldValues::FieldName: " + repFields[0]+"->"+repFields[1] + "  tileCounter: " + tileCounter
            + " CurrValue: " + currValue);

        }
      }
      else
      {
        // It's not a TiledView Field.  Try get the DispalyField and check the value
        currValue = getCurrNDPage().getDisplayFieldValue(fieldFullName);
        values.add(currValue);

        //logger.debug("PHC@getRepeatedFieldValues::FieldName: " + fieldFullName + " FieldValue: " + currValue);
      }
      return values;
    }
    catch(Exception ex)
    {
      logger.debug("Exception PHC@getRepeatedFieldValues : Getting value for display fld<" + fieldFullName + ">, row = "
          + tileCounter + " - ignored :: " + ex.toString());
      return values;
    }
  }

  // Method to get the list of selected check box
  //--> checkBoxName -- name of the CheckBox with Parent TiledView name
  //-->                 e.g. "RepeatedLiabilities/chbLiabilityDelete"
  //--> Rewritten by Billy 24July2002
  //========================================================================
  public Vector getSelectedRowsFromCheckBoxes(String checkBoxName)
  {
    Vector indxsReturn = new Vector();

    if ((checkBoxName == null) ||(checkBoxName.equals(""))) return indxsReturn;
    int tileCounter = 0;
    boolean currValue = false;
    String currValueStr = "";

    try
    {
      //Check if this is a TiledView
      String [] repFields = new String [2];
      StringTokenizer rep = new StringTokenizer(checkBoxName, "/");
      try
      {
        repFields[0] = rep.nextToken(); // repeatable Name
        repFields[1] = rep.nextToken(); // repeatable FieldName
      }
      catch(NoSuchElementException ex)
      {
        ;
      }

      if(repFields[0] != null && !repFields[0].trim().equals("") && repFields[1] != null && !repFields[1].trim().equals(""))
      {
        // It's a TiledView field

        TiledViewBase tiledView = (TiledViewBase) getCurrNDPage().getChild(repFields[0]);
        tiledView.resetTileIndex();

        for (tileCounter=0; tileCounter<tiledView.getNumTiles();tileCounter++)
        {
          tiledView.setTileIndex(tileCounter);
          currValue = (tiledView.getDisplayFieldBooleanValue(repFields[1]));
          currValueStr = tiledView.getDisplayFieldStringValue(repFields[1]);

          if (currValue==true)
          {
            indxsReturn.add(new Integer(tileCounter));
          }

          logger.debug("DHC@getSelectedRowsFromCheckBoxes::FieldName: " + repFields[0]+"->"+repFields[1] + "  tileCounter: " + tileCounter
              + " FieldBooleanValue: " + currValue + " FieldStringValue: " + currValueStr);

        }
      }
      else
      {
        // It's not a TiledView Field.  Try get the DispalyField and check the value
        currValueStr = (String)(getCurrNDPage().getDisplayFieldValue(checkBoxName));
        if (currValueStr!=null && currValueStr.trim().equals("T"))
        {
          indxsReturn.add(new Integer(tileCounter));
        }

        logger.debug("DHC@getSelectedRowsFromCheckBoxes::FieldName: " + checkBoxName + " FieldValue: " + currValue);
      }
      return indxsReturn;
    }
    catch(Exception ex)
    {
      logger.debug("Exception @getSelectedRowsFromCheckBoxes.readFromPage : Getting value for display fld<" + checkBoxName + ">, row = "
          + tileCounter + " - ignored :: " + ex.toString());

      return indxsReturn;
    }
  }

  //// Return array of Strings with the parsed Date String.
  public String[] parseDateStringToTokens(String dateIn)
  {
    String[] parsedDate = new String [3];
    StringTokenizer date = new StringTokenizer(dateIn, "-");

    try
    {
      if (dateIn == null || dateIn.trim().equals(""))
      {
        parsedDate[0] = new String("");
        parsedDate[1] = new String("0");
        parsedDate[2] = new String("");
      }
      else
      {
        try
        {
          parsedDate[0] = date.nextToken(); // Year
          parsedDate[1] = date.nextToken(); // Month

          String tmp = date.nextToken();

          parsedDate[2] = tmp.substring(0, 2); // Day
          //logger.debug("PHC@parseDateStringToTokens::Day: " + parsedDate[2]);
        }
        catch(NoSuchElementException e)
        {
          logger.debug("NoSuchElemException @PHC.parseDateStringToTokens:" + e);		//#DG702
        }
      }
    }
    catch(Exception e)
    {
      logger.debug("Exception @PHC.parseDateStringToTokens:" + e);		//#DG702
    }

    return parsedDate;
  }

  private String[] parsePhoneString(String phoneNo)
  {
    String [ ] retval =    {"", "", ""};
    try
    {
      // detect not present
      if (phoneNo == null || phoneNo.trim().length() == 0) return retval;
      retval [ 0 ] = phoneNo.substring(0, 3).trim();
      retval [ 1 ] = phoneNo.substring(3, 6).trim();
      retval [ 2 ] = phoneNo.substring(6).trim();

      ////logger.debug("PHC@parsePhoneString::retval_0: " + retval[0]);
      ////logger.debug("PHC@parsePhoneString::retval_1: " + retval[1]);
      ////logger.debug("PHC@parsePhoneString::retval_2: " + retval[2]);
    }
    catch(Exception e)
    {
      logger.trace("--T--> @parsePhoneString: in=<" + phoneNo + "> - use empty phone number:" + e);		//#DG702
    }
    return retval;
  }

  //// Temp routine to get the Repeated name. It should be replaced with the one
  //// that should create the TreeMap of index<-->indxValue pairs.
  //// Based on the cardinality of this TreeMap should be called either XXXXDisplay(...)
  //// method for a single row, or the iteration should be done instead.
  private String getRepeatedName(String fieldFullName)
  {
    String  repName = "";

    if (fieldFullName == null || fieldFullName.trim().equals(""))
    {
      repName = "";
    }

    //Check if this is a TiledView field name
    StringTokenizer rep = new StringTokenizer(fieldFullName, "/");
    try
    {
      repName = rep.nextToken(); // repeatable Name
      //logger.debug("PHC@parsePhoneString::RepeatableName: " + repName);
    }
    catch(NoSuchElementException ex)
    { ; }

    return repName;
  }

  //--> Modified to populate image based on Current Language
  //--> By Billy 26Nov2002
  public String displayViewOnlyTag()
  {
    PageEntry pg = theSessionState.getCurrentPage();

    String langStr = "";
    //No extension for English
    if(theSessionState.getLanguageId() != Mc.LANGUAGE_PREFERENCE_ENGLISH)
      langStr = "_" + BXResources.getLocaleCode(theSessionState.getLanguageId());

    if (pg.isEditable() == false)
    {
      return "<IMG NAME=\"stViewOnlyTag\" SRC=\"../images/ViewOnly" + langStr + ".gif\" ALIGN=TOP>";
    }
    return "";
  }
  //===================================================

  //--Release2.1--//
  //// No need anymore since the image should be translated (see above method).
  /**
  public String displayViewOnlyTag(DisplayField field)
  {

    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    if (pg.isEditable() == false)
    {
      ////field.setHtmlText("<IMG NAME=\"stViewOnlyTag\" SRC=\"/eNet_images/ViewOnly.gif\" ALIGN=TOP>");
      return "<IMG NAME=\"stViewOnlyTag\" SRC=\"../images/ViewOnly.gif\" ALIGN=TOP>";
    }
    else if (pg.isEditable() == false)
    {
      return "";
    }

    return "";
  }
  **/

  //--Release2.1--//
  //New method to set Yes/No flag based on Language
  //--> By Billy 25Nov2002
  public String setYesOrNo(String val)
  {
    if (! val.equals(""))
    {
      if (val.equalsIgnoreCase("Y"))
        //return "Yes";
        return BXResources.getGenericMsg(YES_LABEL, theSessionState.getLanguageId());
      else
        //return "No";
        return BXResources.getGenericMsg(NO_LABEL, theSessionState.getLanguageId());
    }
    return "--";
  }
  //============================================
  
  //--Release3.1--begins
  //New method to set Tenure/Insurer flag based on Language
  //--by Hiro Apr 11, 2006
  public String setLenderOrInsurer(String val)
  {
    if (! val.equals(""))
    {
      if (val.equalsIgnoreCase("L"))
        //return "Yes";
        return BXResources.getGenericMsg(LENDER_LABEL, theSessionState.getLanguageId());
      else if (val.equalsIgnoreCase("I"))
        //return "No";
        return BXResources.getGenericMsg(INSURER_LABEL, theSessionState.getLanguageId());
    }
    return "--";
  }
  //--Release3.1--ends

  //--Release2.1--start//
  //// New method allowing to eliminate the previous approach for parsing the
  //// rate string in order to get the pricingRateInventoryId.
  public void handleRateInventoryIdPassToHtml(ViewBean pg, ChildDisplayEvent event)
  {

    PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

    //2. retreive the deal id value to work with the data object
    int dealId = currPg.getPageDealId();
    int copyid = currPg.getPageDealCID();

    int rateInventoryId = 0;

    SessionResourceKit srk = getSessionResourceKit();

    try
    {
      JdbcExecutor jExec = srk.getJdbcExecutor();

      String sql =
      "SELECT DISTINCT PR.PRICINGRATEINVENTORYID" +
          " FROM  DEAL A, PRICINGRATEINVENTORY PR, MTGPROD mp" +      
          " WHERE a.institutionprofileid = pr.institutionprofileid " +
          " AND a.PRICINGPROFILEID = PR.PRICINGRATEINVENTORYID" +
          " AND a.institutionprofileid = mp.institutionprofileid " +
      " AND a.mtgprodid = mp.mtgprodid" +
      " AND a.dealId = " + dealId +
      " AND a.copyId = " + copyid;

      ////logger.debug("DEH@handleRateInventoryIdPassToHtml::SQL: " + sql);

      int key = jExec.execute(sql);

      while(jExec.next(key))
      {
        rateInventoryId = jExec.getInt(key, 1);
        ////logger.debug("DEH@handleRateInventoryIdPassToHtml::rateInventoryId: " + rateInventoryId);
      }
      jExec.closeData(key);
    }
    catch(Exception e)
    {
        String msg = "Exception @DEH::handleRateInventoryIdPassToHtml";
        logger.error(msg);
        logger.error(e);
    }

    String fieldName = (String) event.getChildName();

    DisplayField fld = (DisplayField) pg.getChild(fieldName);
    ////logger.debug("DEH@handleRateInventoryIdPassToHtml::ValuePassed: " + rateInventoryId);

    fld.setValue((new Integer(rateInventoryId)).toString());
  }

  //// New method to pass the initial PaymentTermId value to the page. This value
  //// is defaulted to "0", since this should initialize the hidden value only.
  //// Javascript populatePaymentTerm() function is resposible to populate this field
  //// with the proper value at any given moment. This value is generated by PricingLogic2
  //// class with the whole pricing logic inside.
  public void handlePaymentTermIdPassToHtml(ViewBean pg, ChildDisplayEvent event)
  {

    PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();
    SessionResourceKit srk = getSessionResourceKit();

    //1. retreive the deal id value to work with the data object
    int dealId = currPg.getPageDealId();
    int copyid = currPg.getPageDealCID();

    int id = 0;

    String fieldName = (String) event.getChildName();

    //// Set value 0 by default to obtain a proper id from the JavaScript.
    DisplayField fld = (DisplayField) pg.getChild(fieldName);
    fld.setValue((new Integer(id)).toString());
  }
  //--Release2.1--end//

  /**
   *
   *
   */

  protected void reduceAccessToComboBox(ViewBean page, String cbName,
                                        int rowindx, int langId, String tableName)
  {
    try
    {
        // added for ML - start
        // if page doesn't specify dealInstitution then???
        PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();
        // added for ML - end
        
      logger.trace("@---T---->PageHandlerCommon ---- restrict the list of the combobox for MI part: " + cbName);

      ComboBox combobox =(ComboBox) page.getDisplayField(cbName);
      OptionList optList = (OptionList) combobox.getOptions();
      //logger.debug("PHC@reduceAccessToComboBox::OptionTest: " + optList);

      String [] labels = new String[1];
      String [] values = new String[1];

      values[0] = (new Integer(rowindx)).toString();

      //--Release2.1--//
      //// Even the non-changeble labels must be translated via the BXResources.
      // modified for ML
      if (currPg.isDealPage()) {
          labels[0] = BXResources.getPickListDescription(theSessionState.getDealInstitutionId(), tableName, rowindx, langId);
      } else {
          labels[0] = BXResources.getPickListDescription(theSessionState.getUserInstitutionId(), tableName, rowindx, langId);
      }
      //logger.debug("PHC@reduceAccessToComboBox::Label: " + labels[0]);
      
      optList.setOptions(labels, values);

      // 1. Customize colors, borders, etc.,
      String changeColorStyle = new String("style=\"background-color:d1ebff\"" + " style=\"font-weight: bold\"");

      combobox.setExtraHtml(changeColorStyle);
    }
    catch(Exception e)
    {
      setStandardFailMessage();
      logger.error("Exception PageHandlerCommon@reduceAccessToComboBox: Problem encountered in combobox customization:" + e);		//#DG702
    }

  }

  //--DJ_LDI_CR--start--//
  /**
   *  Execute select operation on provided model. If <positionModel> == true the model is positioned to the first
   *  row in the model. For models used in a non-repeating context (e.g. associated with a non-repeating view where
   *  only one row of data is to be displayed on the view/page) this consistent with the JATO behavior supported
   *  for auto-executed models. In general for a non-repeating view we want to be positioned at the row to be
   *  displayed on the view. This is true even if there are multiple rows in the underlying result set (of the
   *  primary model for the view) - here we would use the sql execution offset to specify the row offset to
   *  correspond to the 'page' to be displayed.
   *
   *  For models used with tiled views we would NOT typically position the model to the first row in the returned
   *  data. This is because the display logic (e.g. see tag UseTiledView) explicitly controls positioning of the
   *  tiles (and associated data model rows.)
   *
   *  Hence when preparing data models for usage in JATO display logic the following applies:
   *
   *  . non-tiled view -> position at the row to be displayed
   *  . tiled view -> position model in "before first" state (e.g. m.beforeFirst(), usually default after execution)
   *
   */
  //// This method is moved from UW to provide this to any handler.
  protected void executeModel(SelectQueryModel model, String methodInfoForLog, boolean positionModel)
  {
    try
    {
      ResultSet rs = model.executeSelect(null);  // model will be positioned in "before first" state

      if (positionModel == true)
        model.next();
    }
    catch (ModelControlException mce)
    {
      logger.warning(methodInfoForLog + "::MCEException: " + mce);
    }
    catch (SQLException sqle)
    {
      logger.warning(methodInfoForLog + "::SQLException: " + sqle);
    }
    catch (Exception e)
    {
      logger.warning(methodInfoForLog + "::Unexpected exception executing (or positioning) model, e = " + e);
    }
  }

  public boolean displayInsuranceDetailsButton()
  {
      PageEntry pe = (PageEntry) theSessionState.getCurrentPage();

      boolean isButtonDisplay = false; // default.
      //// Not allow access to Life&Disability screen
      if (PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),
                                                    COM_BASIS100_LDINSURANCE_DISPLAYLIFEDISABILITYMODULE, "N").equals("N"))
      {
        isButtonDisplay = false;
        logger.debug("PHC@displayInsuranceDetailsButton::NoDisplay: " + isButtonDisplay);
      }
      //// Display button to access the Life&Disability screen
      else if (PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),
                                                         COM_BASIS100_LDINSURANCE_DISPLAYLIFEDISABILITYMODULE, "N").equals("Y"))
      {
         if(pe.isDealEntry() == true) {
            isButtonDisplay = false;
         } else if(pe.isDealEntry() == false) {
            isButtonDisplay = true;
         }
         logger.debug("PHC@displayInsuranceDetailsButton::YesDisplay: " + isButtonDisplay);
      }

      return isButtonDisplay;
  }

  /**
   *
   *
   */
  protected void startSupressContent(String stName, ViewBean thePage)
  {
    String supressStart = "<!--";
    // suppress
    thePage.setDisplayFieldValue(stName, new String(supressStart));

    return;
  }

  /**
   *
   *
   */
  protected void endSupressContent(String stName, ViewBean thePage)
  {
    String supressEnd = "//-->";
    // suppress
    thePage.setDisplayFieldValue(stName, new String(supressEnd));

    return;
  }

//===================================================================================================================
//--> Suggestion : put all the infocal stuffs in a separate class including the method to generate the INPUT string
//-->         from entities (current directly reading from screen fields).  This way we can easily share with DocPrep
//--> By Billy 20May2004
//===================================================================================================================
    //// Parse output from DJ Infocal calc engine.
  protected void parseInfocalResult(String outInfocal)
  {
    PageEntry pg = getTheSessionState().getCurrentPage();
    Hashtable pst = pg.getPageStateTable();

    StringTokenizer entity = new StringTokenizer(outInfocal, "|", false);

    if (entity.countTokens() == Mc.INFOCAL_ENTITIES_NUMBER_REGULAR ||
        entity.countTokens() == Mc.INFOCAL_ENTITIES_NUMBER_ERROR_CODE)
    {
      logger.trace("LDI@parseInfocalResult: " + outInfocal);
    }
    else
    {
      throw new IllegalArgumentException("LDI@parseInfocalResult: Incorrect number of entities from Infocal! " + entity.countTokens());
    }

    //// 0. Start reading the value
    //// 1. Set values to pst....
    //// 2. Finish reading....
    //// 3. Call from submit.... from main function
    //// 4. Process to entities... from main function

    while(entity.hasMoreTokens())
    {
      String  strtoken = entity.nextToken();

      TreeMap parsedEntity = new TreeMap();
      parsedEntity = parseInfocalEntity(strtoken);

      String paramName = "";
      String paramValue = "";

      Set keys = parsedEntity.entrySet();
      Iterator iter = keys.iterator();

      while(iter.hasNext())
      {
        Map.Entry entry = (Map.Entry) iter.next();
        Object key = entry.getKey();
        Object value = entry.getValue();
        paramValue = value.toString();
        paramName = key.toString();

logger.debug("LDI@parseInfocalResult::Value: " + paramValue);
logger.debug("LDI@parseInfocalResult::Key: " + key.toString());
      }

      if (paramName.equals(Mc.DJLD_DLL_CALC_ERROR_CODE) ||
          paramName.equals(Mc.DJLD_PAYMENT_PLUS_LIFE) ||
          paramName.equals(Mc.DJLD_RESIDUAL_AMOUNT) ||
          paramName.equals(Mc.DJLD_EQUIVALENT_INTEREST_RATE) ||
          paramName.equals(Mc.DJLD_INTEREST_RATE_INCREMENT_FOR_LD) ||
          paramName.equals(Mc.DJLD_LIFE_PREMIUM) ||
          paramName.equals(Mc.DJLD_PARAMETR_TO_FIND_LIFE_PREMIUM) ||
          paramName.equals(Mc.DJLD_DISABILITY_PREMIUM) ||
          paramName.equals(Mc.DJLD_EQUIVALENT_INTEREST_RATE_FOR_SECOND_MORTGAGE) ||
          paramName.equals(Mc.DJLD_DISABILITY_INSURANCE_PREMIUM) ||
          //--DJ_Ticket#499--start//
          paramName.equals(Mc.DJLD_CREDIT_COSTS) ||
          paramName.equals(Mc.DJLD_ADD_CREDIT_COSTS_INCURRED_BY_INS) ||
          paramName.equals(Mc.DJLD_TOTAL_CREDIT_COSTS))
          //--DJ_Ticket#499--end//
      {
        pst.put(new String(paramName), paramValue);
      }
      else
      {
        throw new IllegalArgumentException("LDI@parseInfocalResult: Incorrect entity label from Infocal! " + paramName);
      }
    }
  }

  ////Cases are not joined in order to provide more detailed messages, if necessary.
  protected String convertRPTErrorCodeToErrorLabel(int errorCode)
  {
    String errorLabel = "RTP_DEFAULT_ERROR_LABEL";  //default;

    switch(errorCode)
    {
      case 1021:
            errorLabel = "RTP_LOAN_TYPE_ERROR";
            break;
      case 1022:
            errorLabel = "RTP_LOAN_TYPE_ERROR";
            break;
      case 1018:
            errorLabel = "RTP_LOAN_AMOUNT_ERROR";
            break;
      case 1020:
            errorLabel = "RTP_LOAN_AMOUNT_ERROR";
            break;
      case 1002:
            errorLabel = "RTP_APPLICANT_NUM_ERROR";
            break;
      case 1004:
            errorLabel = "RTP_EFF_DATE_ERROR";
            break;
      case 1019:
            errorLabel = "RTP_BORR_GENERAL_ERROR";
            break;
      case 1017:
            errorLabel = "RTP_BORR_AGE_ERROR";
            break;
      case 1011:
            errorLabel = "RTP_BORR_GENDER_ERROR";
            break;
      case 1015:
            errorLabel = "RTP_BORR_SMOKE_ERROR";
            break;
      case 1014:
            errorLabel = "RTP_REDUCED_RATE_ERROR";
            break;
      case 1013:
            errorLabel = "RTP_REDUCED_RATE_ERROR";
            break;
      case 1008:
            errorLabel = "RTP_LIFE_PERC_COVER_ERROR";
            break;
      case 1009:
            errorLabel = "RTP_LIFE_PERC_COVER_ERROR";
            break;
      case 1010:
            errorLabel = "RTP_LIFE_PERC_COVER_ERROR";
            break;
      case 1012:
            errorLabel = "RTP_LIFE_PERC_COVER_ERROR";
            break;
      case 1023:
            errorLabel = "RTP_LIFE_PERC_COVER_ERROR";
            break;
      case 1005:
            errorLabel = "RTP_INDIVID_PREMIUM_ERROR";
            break;
      case 1006:
            errorLabel = "RTP_INDIVID_PREMIUM_ERROR";
            break;
      case 1007:
            errorLabel = "RTP_INDIVID_PREMIUM_ERROR";
            break;
      default:
           errorLabel = "RTP_DEFAULT_ERROR_LABEL";
           break;
    }
    return errorLabel;
  }

  protected int getLifeDisabilityId(String radioBoxVal)
  {
    int ret = 1; // default status is ACCEPTED.

    if(radioBoxVal == null || radioBoxVal.equals(""))
       ret = 0;

    if(radioBoxVal.equals(Mc.LIFE_DISABILITY_STATUS_ACCEPTED))
       ret = 1;
    else if(radioBoxVal.equals(Mc.LIFE_DISABILITY_STATUS_REFUSED))
       ret = 2;
    else if(radioBoxVal.equals(Mc.LIFE_DISABILITY_STATUS_INELIGIBLE))
       ret = 3;

    return ret;
  }

  //// The format of each entity in Infocal output is JS escaped name/value pair: 'ENTITY_NAME','ENTITY_VALUE'
  //--> Question to Vlad : what this method doing ????  From Billy 20May2004
  protected TreeMap parseInfocalEntity(String entity)
  {
//logger.debug("LDI@parseInfocalEntity:EntityIn: " + entity);

      TreeMap result = new TreeMap();
      StringTokenizer token = new StringTokenizer(entity, ",", false);

      String  param = token.nextToken();
      String value = entity.substring(param.length() + 2, entity.length() - 1);

logger.debug("LDI@parseInfocalEntity:EntityName: " + param.substring(1, param.length() - 1));
logger.debug("LDI@parseInfocalEntity:EntityValue: " + value);

      if (token.countTokens() != 1)
      {
        throw new IllegalArgumentException("LDI@parseInfocalResult: Incorrect number of elements in entity " + token.countTokens());
      }
      else
      {
        result.put(param.substring(1, param.length() - 1), value);
      }
     return result;
  }

  public void handleRecalcInfocal(String[] args)
  {
    //// Implementation should be done in the handler.
  }

  public void handleLDInsuranceDetails()
  {
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
    SessionResourceKit srk = getSessionResourceKit();
    CalcMonitor dcm = CalcMonitor.getMonitor(srk);

    try
    {
      srk.beginTransaction();

      // adopt transactional copy unconditionally - ensures we enter Mortgage Life and Disability Insurance
      // screen with saved (non-discardable) scenario data.
      ////standardAdoptTxCopy(pg, true);
      Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

      Borrower borrower = new Borrower(srk, dcm);
      Collection borrowers = borrower.findByDeal(new DealPK(pg
            .getPageDealId(), pg.getPageDealCID()));

logger.debug("PHC@handleLDInsuranceDetails::BorrowerNumber: " + borrowers.size());

      Iterator it = borrowers.iterator();
      int borrId = 0;

      while(it.hasNext())
      {
        borrower =(Borrower) it.next();
        borrId = borrower.getBorrowerId();

logger.debug("PHC@handleLDInsuranceDetails::BorrowerId: " + borrId);
//logger.debug("PHC@handleLDInsuranceDetails::BorrowerAge: " + borrower.getAge());
//logger.debug("PHC@handleLDInsuranceDetails::BorrowerSmoker: " + borrower.getSmokeStatusId());

        LifeDisabilityPremiums ldp = new LifeDisabilityPremiums(srk, dcm);
        Collection lifeDisPremiums = ldp
                .findByBorrower(new BorrowerPK(borrId, pg.getPageDealCID()));

        if (lifeDisPremiums.size() == 0 || lifeDisPremiums == null)
        {
          handleAddLDPremiums(borrower, dcm);

//logger.debug("PHC@handleLDInsuranceDetails::AfterCreateEntiryStamp");
        }
      } // end while

      //--DJ_CR136--start--//
      // Display Insure Only Applicant type of Life&Disability insurance if any.
      InsureOnlyApplicant ioa = new InsureOnlyApplicant(srk, dcm);
      Collection insureOnlyAppls = ioa.findByDeal(new DealPK( pg.getPageDealId(), 
                                                              pg.getPageDealCID()));

logger.debug("PHC@handleLDInsuranceDetails::InsureOnlyApplsNumber: " + insureOnlyAppls.size());

      Iterator itioa = insureOnlyAppls.iterator();
      int insureOnlyApplId = 0;

      while(itioa.hasNext())
      {
        ioa =(InsureOnlyApplicant) itioa.next();
        insureOnlyApplId = ioa.getInsureOnlyApplicantId();

//temp
//logger.debug("PHC@handleLDInsuranceDetails::IOAId: " + insureOnlyApplId);
logger.debug("PHC@handleLDInsuranceDetails::IOAAge: " + ioa.getInsureOnlyApplicantAge());
logger.debug("PHC@handleLDInsuranceDetails::IOASmoker: " + ioa.getSmokeStatusId());
logger.debug("PHC@handleLDInsuranceDetails::IOAGender: " + ioa.getInsureOnlyApplicantGenderId());
logger.debug("PHC@handleLDInsuranceDetails::IOALifeStatusId: " + ioa.getLifeStatusId());
logger.debug("PHC@handleLDInsuranceDetails::IOADisabilityStatusId: " + ioa.getDisabilityStatusId());

        LifeDisPremiumsIOnlyA ldpioa = new LifeDisPremiumsIOnlyA(srk, dcm);
        Collection lifeDisPremiumsIOA = ldpioa.findByInsureOnlyApplicant(new InsureOnlyApplicantPK(insureOnlyApplId, pg.getPageDealCID()));

        if (lifeDisPremiumsIOA.size() == 0)
        {
          handleAddLDPremiumsInsureOnly(ioa, dcm);
        }
logger.debug("PHC@handleLDInsuranceDetails::IOAPremiumsSize: " + lifeDisPremiumsIOA.size());

      } // end while
      //--DJ_CR136--end--//

      PageEntry ldiPg = setupSubPagePageEntry(pg, Mc.PGMN_LIFE_DISABILITY_INSURANCE, true);

      getSavedPages().setNextPage(ldiPg);
      navigateToNextPage();
      srk.commitTransaction();
      return;
    }
    catch(Exception e)
    {
      srk.cleanTransaction();
      logger.error("handleLDInsuranceDetails().");
      logger.error(e);
      setStandardFailMessage();
    }
  }

  //--DJ_CR136--start--//
  /**
   *
   *
   */
  protected void handleAddLDPremiumsInsureOnly(InsureOnlyApplicant applicant, CalcMonitor dcm)
    throws Exception
  {
  logger.debug("LDH@handleLifeLisPremiumsCreate::handleAddLDPremiumsInsureOnly: " + applicant.getPk().getId());

    try
    {
      DBA.createEntity(this.getSessionResourceKit(), "LifeDisPremiumsIOnlyA", applicant, dcm, true);
    }
    catch(Exception ex)
    {
      logger.error("Exception @LifeDisabilityPremiums");
      logger.error(ex);
      throw new Exception("Add entity handler exception");
    }
  }
  //--DJ_CR136--end--//

  /**
   *
   *
   */
  private void handleAddLDPremiums(Borrower borrower, CalcMonitor dcm)
    throws Exception
  {
    try
    {
      DBA.createEntity(this.getSessionResourceKit(), "LifeDisabilityPremiums", borrower, dcm);
    }
    catch(Exception ex)
    {
      logger.error("Exception @LifeDisabilityPremiums");
      logger.error(ex);
      throw new Exception("Add entity handler exception");
    }

  }

  public boolean isDJClient()
  {
    boolean isDJClient = false; // default.
    // regular execution path
    if (PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),
                                                  COM_BASIS100_CALC_ISDJCLIENT, "N").equals("N"))
    {
       isDJClient = false;
    }
    // DJ execution path
    else if (PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),
                                                       COM_BASIS100_CALC_ISDJCLIENT, "N").equals("Y"))
    {
       isDJClient = true;
    }
    logger.debug("PHC@isDJClient::isDisplayBranchOriginationLabel? " + isDJClient);

    return isDJClient;
  }
  //--DJ_LDI_CR--end--//

  //--DocCentral_FolderCreate--21Sep--2004--start--//
  public void handleDocCentralAPI(String[] args)
  {
    //// Implementation should be done in the handler.
  }
  //--DocCentral_FolderCreate--21Sep--2004--end--//
  //========================================================
  
  //--Release3.1--begins
  //--by Hiro Apr 27, 2006
  // pulled up

  public String setMonthsToYearsMonths(String val)
  {
    if (!val.equals(""))
      return convertMonthToYears(getIntValue(val));
    else
      return "";
  }

  public String convertMonthToYears(int period)
  {
    //String mthsStr = " Mths";
    String mthsStr = " " + BXResources.getGenericMsg(MONTHS_SHORT, theSessionState.getLanguageId());
    //String yearStr = " Yrs ";
    String yearStr = " " + BXResources.getGenericMsg(YEARS_SHORT, theSessionState.getLanguageId()) + " ";
  
    if (period < 12)
    {
        if (period == 1 || period == 0)
        //mthsStr = " Mth ";
        mthsStr = " " + BXResources.getGenericMsg(MONTH_SHORT, theSessionState.getLanguageId()) + " ";
        return(period + mthsStr);
    }
    else
    {
        int yearNum = period / 12;
        int mthNum = period % 12;
        if (yearNum == 1)
        //yearStr = " Yr ";
        yearStr = " " + BXResources.getGenericMsg(YEAR_SHORT, theSessionState.getLanguageId()) + " ";
        if (mthNum == 1 || mthNum == 0)
        //mthsStr = " Mth";
        mthsStr = " " + BXResources.getGenericMsg(MONTH_SHORT, theSessionState.getLanguageId());
        return(yearNum + yearStr + mthNum + mthsStr);
    }
  
  }
  //--Release3.1--ends

  //***** Change by NBC/PP Implementation Team - GCD - Start *****//
  
  public boolean isShowGCDSumarySection()
  {
    return PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(), 
                                                     COM_BASIS100_GCD_SHOW_GCDSUMMARY_SECTION, "Y").equals("Y");
  }
  //***** Change by NBC/PP Implementation Team - GCD - End *****//

  /*
  *********************** WARNING FOR ML ********************* 
  * TODO: TEMPORARY SOLUTION
  * MUST FIX PROPERTY
  */
 private int getGotoInstitutionId() {
     /*
      * FXP19959 - FIX BEGIN
      */
    int returnValue;
    ////String dealNumber = thePage.getDisplayFieldValue(TB_DEAL_ID).toString().trim();
    String theId = (String) currNDPage.
        getDisplayFieldValue(TB_INSTITUTION_ID);
    
    if (theId == null || theId.trim().length() == 0) {
        returnValue = theSessionState.getDealInstitutionId();
    } else {
        returnValue = Integer.parseInt(theId.trim());
    }

    if (returnValue < 0) {
        returnValue = theSessionState.getUserInstitutionId();
    }

    return returnValue;
    /*
     * FXP19959 - FIX END
     */
 }
 
 /**
  * apply institutionId for the next screen.
  * since 3.3
  */
 public void enforceInstitutionId(PageEntry pg) {

     _log.debug("** PHC.enforceInstitutionId(PageEntry) start ************************");
     if (_log.isDebugEnabled()) {
         try{
             _log.debug("PageEntry: pageid = " + pg.getPageId()
                     + " institutionid = " + pg.getDealInstitutionId()
                     + " pageDealid = " + pg.getPageDealId()
                     + " pageCopyid = " + pg.getPageDealCID()
                     + " pageMosUserId = " + pg.getPageMosUserId()
                     + " pageMosUserTypeId = " + pg.getPageMosUserTypeId());
         }catch(Throwable ignore){}
     }

     enforceInstitutionId(pg.getPageId(), pg.getDealInstitutionId());
     
     _log.debug("** PHC.enforceInstitutionId(PageEntry) end **************************");
 }
     
	/**
	 * apply institutionId for the next screen.
	 * since 3.3
	 */
    public void enforceInstitutionId(int pageid, int institutionid) {
        
        if (_log.isDebugEnabled()) {
            try{
                _log.debug(">>>>> enforceInstitutionId(int, int) start >>>>>");
                srk.getConnection(); //dummy. activate srk in order to show debug data.
                _log.debug("before VPD = " + srk.getActualVPDStateForDebug());
                _log.debug("before theSessionState: userId = " + theSessionState.getSessionUserId()
                        + " userInstitutionId = " + theSessionState.getUserInstitutionId()
                        + " currnt Pageid = " + theSessionState.getCurrentPage().getPageId()
                        + " dealInstitutionId (current page's institutionid) = " 
                        + theSessionState.getDealInstitutionId());
            }catch(Throwable ignore){}
        }
        
        try {
            switch (PageInstitutionUtil.getInstitutionUsage(pageid)) {
            case ALL_OF_THE_USER_INSTITUTIONS:
                enforceUserInstitutionId();
                break;
            case ONE_OF_THE_USER_INSTITUTIONS:
            case DEAL_INSTITUION:
            default:
                enforceDealInstitutionId(institutionid);
                break;
            }
        } catch (ExpressRuntimeException assertion) {
            assertion.printStackTrace();
            _log.error(">>>>> Assertion Error >>>>> " + assertion.getMessage() + 
                    " pageid="+ pageid +" institutionid=" + institutionid);
            _log.error(StringUtil.stack2string(assertion));
            throw assertion;
        }
        
        if (_log.isDebugEnabled()) {
            try{
                _log.debug("after  VPD = " + srk.getActualVPDStateForDebug());
                _log.debug("after  theSessionState: userId = " + theSessionState.getSessionUserId()
                        + " userInstitutionId = " + theSessionState.getUserInstitutionId()
                        + " currnt Pageid = " + theSessionState.getCurrentPage().getPageId()
                        + " dealInstitutionId (current page's institutionid) = " 
                        + theSessionState.getDealInstitutionId());
                _log.debug(">>>>> enforceInstitutionId(int, int) end >>>>>");
            }catch(Throwable ignore){}
        }
    }

    /**
     * apply deal institutionId for the next screen.
     * since 3.3
     */
    private void enforceDealInstitutionId(int institutionid) {
        
        if (institutionid < 0)
            throw new ExpressRuntimeException(
                    "DealInstitutionId is missing in PageEntry@PHC.enforceInstitutionId");
        
        int userProfID4CurrDealInst 
            = theSessionState.getUserProfileId(institutionid);
        
        if (userProfID4CurrDealInst < 0)
            throw new ExpressRuntimeException(
                    "UserProfileId is missing in PageEntry@PHC.enforceInstitutionId");

        theSessionState.switchInstitutionUserData(institutionid);
        
        ExpressState expressState = theSessionState.getExpressState();
        //we have tot set userid first because lots of classes get userid via ExressState
        expressState.setUserIds(userProfID4CurrDealInst, institutionid);
        // apply Deal InstitutionId finally
        expressState.setDealInstitutionId(institutionid);
        _log.debug("===== Deal InstitutionId (" + institutionid + ") was assigned");
    }

    /**
     * apply user institutionId for the next screen.
     * since 3.3
     */
    private void enforceUserInstitutionId() {
        int sessUserId = theSessionState.getSessionUserId();
        int sessUserInstId = theSessionState.getUserInstitutionId();
        if (sessUserId < 0 || sessUserInstId < 0)
            throw new ExpressRuntimeException(
                    "SessionUser info is missing@PHC.enforceInstitutionId" +
                    " userid=" + sessUserId + " institutionid=" + sessUserInstId);

        //apply user institutionid
        theSessionState.getExpressState().setUserIds(sessUserId, sessUserInstId);
        _log.debug("===== User InstitutionId which is " +
        		"all InstitutionIDs that this user (userid,institutionid =" +
        		sessUserId + "," + sessUserInstId + ") belongs to was assigned ");
    }
 
    //Exchange Link set up function.  Fetches foldercode and builds exchange URL
    public boolean setupFolderCode()
    {
    	PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
    	doDCFolderModelImpl dodcfolder =(doDCFolderModelImpl) (RequestManager.getRequestContext().getModelManager().getModel(doDCFolderModelImpl.class));
		dodcfolder.clearUserWhereCriteria();
		dodcfolder.addUserWhereCriterion("dfDealId", "=", pg.getPageDealId());
		logger.debug("--- ANU --- dodcfolder == " + dodcfolder.toString());
		this.executeModel(dodcfolder, "DCFolder", true);
		JdbcExecutor jExec = srk.getJdbcExecutor();
		int creationId = 0;
		try
		{
			int key = jExec.execute("select dcfoldercreationid from dcconfig where institutionprofileid = " + pg.getDealInstitutionId());
			while(jExec.next(key))
			{
				creationId = jExec.getInt(key, 1);
				break;
			}
			jExec.closeData(key);
		}
		catch (Exception e)
		{
			//likely as not this means exchange is not configured
			//if database is down, we'll know in other ways....
			creationId = 0;
		}
		
		boolean isExchangeTwo = PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),COM_FILOGIX_EXCHANGE_VERSION, "0").equals("2");
		
		if (creationId == 0 || !isExchangeTwo)
		{	
			getCurrNDPage().setDisplayFieldValue(CHILD_FOLDERCODE, new String("off"));
			return false;
		}
		else
		{
		String folderCode = dodcfolder.getDfDCFolderCode();
		if (folderCode != null && folderCode != "" )
		{
			String URL = new String();
			if (theSessionState.getLanguageId() == Mc.LANGUAGE_PREFERENCE_ENGLISH)
				URL = PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(), 
					COM_FILOGIX_MOSAPP_EXCHANGEURLLINK_ENGLISH);
			else
				URL = PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(), 
					COM_FILOGIX_MOSAPP_EXCHANGEURLLINK_FRENCH);
			URL = URL + EXCHANGELINK_PATH_START + folderCode + EXCHANGELINK_PATH_END;
			getCurrNDPage().setDisplayFieldValue(CHILD_FOLDERCODE, new String(URL));
			return true;
		}
		else
		{
			getCurrNDPage().setDisplayFieldValue(CHILD_FOLDERCODE, new String("-1"));
			return false;
		}
    }
    }

    
    /**
     * if deal is locked
     * @param dealId
     * @return
     * @throws Exception
     */
    private boolean dealLocked(int dealId) throws Exception {
        boolean locked = false;
        if (DLM.getInstance().isLockedByUser(dealId,
                srk.getExpressState().getUserProfileId(), srk)) {
            locked = true;
        }
        return locked;
    }
   
    /**
     * <p> displayQuickLinkMenu </p>
     * 
     *  To display Quick links menu on selected screens
     * @param 
     * @return
     * @throws 
     */
    protected void displayQuickLinkMenu() {
    	
    	PropertiesCache cache = PropertiesCache.getInstance();
    	ViewBean thePage = getCurrNDPage(); 
    	
    	doQuickLinkModel theDO = null;
    	UserProfile up = null;    	 
    	ImageField img = null;
    	
    	String showQLMenu = "N"; 
    	String urlList = ""; 
    	int institutionProfileId = -1;
    
    	HiddenField hiddenField = null;
    	HiddenField hdSelectedInstProfile = null;
    	
    	StaticTextField stInstProfileList = null;
    	    		
    	hiddenField = (HiddenField) thePage.getDisplayField(ExpressViewBeanBase.CHILD_HDQUICKLINKENABLED);
		stInstProfileList = (StaticTextField) thePage.getDisplayField(ExpressViewBeanBase.CHILD_STINSTPROFILELIST);
		hdSelectedInstProfile = (HiddenField) thePage.getDisplayField(ExpressViewBeanBase.CHILD_HDSELECTEDINSTPROFILEID);
		
		
		// check for IWQ / MWQ and MWQ Search screen 
		PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
		// in Work Queue / Master Work Queue and Master Work Queue search screen
		// if no deals - display the quick link profile.
		// the behaviour in other screen - get deal information from express state 
		// display the Quick Links
		
		if (pg.getPageId() == Mc.PGNM_INDIV_WORK_QUEUE_ID || 
				 pg.getPageId() == Mc.PGNM_MASTER_WORK_QUEUE_ID  || 
				 pg.getPageId() == Mc.PGNM_MASTER_WORK_QUEUE_SEARCH_ID ) {
			
			PageCursorInfo tds = null;
			Hashtable pst = pg.getPageStateTable();
			Map loggedUserInstMap = null; 
			
			 if( pg.getPageId() == Mc.PGNM_MASTER_WORK_QUEUE_SEARCH_ID )
				 tds = (PageCursorInfo) pst.get("MWQSDefault");
			 else
				 tds = (PageCursorInfo) pst.get("DEFAULT");
			
			 UserProfile loggedUser = null;		
			
			 // Getting Institution accessable by user
			 try {
				 loggedUser = new UserProfile(srk);
				 loggedUserInstMap = loggedUser.getQuickLinksEnabledInstitutions(theSessionState.getUserLogin());
			 }	
			 catch(Exception e) {
			    logger.error("PHC :displayQuickLinkMenu() - error while fetching user profile object " +  e.getMessage());	
			 }
			
			 // No deal in IWQ, MWQ and MWQ Search  
			 if(tds != null && tds.getTotalRows() == 0) {

       				 String selectedInstProfile = (String) hdSelectedInstProfile.getValue();
   
       				 // Multi Institution Access for users
       				 if( loggedUserInstMap != null && loggedUserInstMap.size() > 1){
       					 
       					 stInstProfileList.setValue(displayInstitutionProfile(loggedUserInstMap,  theSessionState.getLanguageId()));
       					
       					 if(selectedInstProfile != null && selectedInstProfile.length() == 0) {
       						 showQLMenu = "Y";
       						 hiddenField.setValue(showQLMenu);
       						 institutionProfileId = theSessionState.getUserInstitutionId();
       					 }
       					 else {
       						
       						stInstProfileList.setValue(displayInstitutionProfile(loggedUserInstMap,  theSessionState.getLanguageId()));
       						institutionProfileId = Integer.parseInt(selectedInstProfile);
       						// Display institution name - same as one selected from Quick Link Profile.
       						InstitutionProfileDataBean instBean = 
       					        theSessionState.getUserStateDataManager().getInstitutionProfileDataBean(institutionProfileId);
       						String companyName = instBean.getInstitutionName() + "-" + instBean.getBrandName();
       						thePage.setDisplayFieldValue(CHILD_ST_COMPANY_NAME, companyName);
       						
       						hdSelectedInstProfile.setValue("");
       					 }
       				 } else {
       					 // display the quick link for single institution for which user has access
       					
       					for (Object key: loggedUserInstMap.keySet()) 
       			    	{
       			    		String InstValue =  key.toString();
       			    		try 
       			    	    {	
       			    			institutionProfileId = Integer.parseInt(InstValue);
       			    	    }
       						catch (Exception ex) {
       							logger.error("PHC:displayInstitutionProfile() : Error while getting institutions name from Id:" + ex.getMessage());
       						}
       			    	}
       					if (institutionProfileId != theSessionState.getUserInstitutionId())
       					{  //fxp31722
       						// Display institution name - same as one selected from Quick Link Profile.
       						institutionProfileId = institutionProfileId == -1 ? loggedUser.getInstitutionId(): institutionProfileId;
       						
       						InstitutionProfileDataBean instBean = 
       					        theSessionState.getUserStateDataManager().getInstitutionProfileDataBean(institutionProfileId);
       						String companyName = instBean.getInstitutionName() + "-" + instBean.getBrandName();
       						thePage.setDisplayFieldValue(CHILD_ST_COMPANY_NAME, companyName);
       					}
       					
       			} 
       				 // display quick links for user intitution
				 
			 } else {
				 institutionProfileId = theSessionState.getDealInstitutionId();
				 stInstProfileList.setValue(displayInstitutionProfile(loggedUserInstMap,  theSessionState.getLanguageId()));
			 }
		 } else
			 institutionProfileId = theSessionState.getDealInstitutionId();
		
		// Display quick links only if quick links are enabled.
		if(cache.getProperty(institutionProfileId,pgQuickLinksViewBean.QUICKLINKS_ENABLED, "N").equalsIgnoreCase("Y")){
    		_log.debug("displayQuickLinkMenu():  quick links enabled");
    		theDO = (doQuickLinkModel) (RequestManager
    				.getRequestContext().getModelManager()
    				.getModel(doQuickLinkModel.class));
    		theDO.clearUserWhereCriteria(); 
    		 up = getUserProfile(institutionProfileId);
    		 //  Display Quick links based on deal selected 
    		 if(cache.getProperty(institutionProfileId,pgQuickLinksViewBean.QUICKLINKS_ENABLED_ALL_BRANCHES, "Y").equalsIgnoreCase("N")){
    			 _log.debug("displayQuickLinkMenu():  quick links All Branches enabled"); 
    		 	 
    			theDO.addUserWhereCriterion(doQuickLinkModel.FIELD_DFBRANCHPROFILEID, "=", String.valueOf(up.getBranchId()));
    			theDO.addUserWhereCriterion(doQuickLinkModel.FIELD_DFINSTITUTIONPROFILEID, "=", institutionProfileId);
    		 }	else {
    			 theDO.addUserWhereCriterion(doQuickLinkModel.FIELD_DFBRANCHPROFILEID, " is ", "NULL");
    			 theDO.addUserWhereCriterion(doQuickLinkModel.FIELD_DFINSTITUTIONPROFILEID, "=", institutionProfileId);
    		 }
    		 // getting quick link data from model 
    		 executeModel(theDO, "PageCommonHandler@DisplayQuickLinkMenu", false);
    		 _log.debug("displayQuickLinkMenu  executeModel done"); 
    		 try{
    			 
    			 // iterate through the model and populate the quick links
        		 for(int rowNum = 0; rowNum < theDO.getSize(); rowNum++) {
        			 theDO.setLocation(rowNum);
        			 if(theDO.getDfSortOrder().intValue() == 1) {
        				 img = (ImageField) thePage.getDisplayField(ExpressViewBeanBase.CHILD_IMQL1);
        		 
        				 if(theDO.getDfFavicon() != null) {
        					 img.setValue(theDO.getDfFavicon(), true);
        					 img.setExtraHtml(getAdditionalTagForImage(theDO)); 
        					 showQLMenu = "Y";
        				 }
        				 else
        					 img.setVisible(false);   
        				 continue;
        			 } else if(theDO.getDfSortOrder().intValue() == 2) {
        				 img = (ImageField) thePage.getDisplayField(ExpressViewBeanBase.CHILD_IMQL2);
        		 
        				 if(theDO.getDfFavicon()!= null){
        					 img.setValue(theDO.getDfFavicon(), true); 
        					 img.setExtraHtml(getAdditionalTagForImage(theDO));
        					 showQLMenu = "Y";
        				 }
        				 else 
        		    		 img.setVisible(false);        				 
        				 
        			 continue;
        			 } else if(theDO.getDfSortOrder().intValue() == 3) {
        				 img = (ImageField) thePage.getDisplayField(ExpressViewBeanBase.CHILD_IMQL3);
        		 
        				 if(theDO.getDfFavicon()!= null) {
        					 img.setValue(theDO.getDfFavicon(), true); 
        					 img.setExtraHtml(getAdditionalTagForImage(theDO));
        					 showQLMenu = "Y";
        				 }
        				 else
        					 img.setVisible(false);   
        				 
        				 continue;
        			 } else {
        				 if(theDO.getDfUrlLink()!= null) {
        					         					 
        					 if( theSessionState.getLanguageId() == Mc.LANGUAGE_PREFERENCE_FRENCH) 
        						 urlList += addURL( theDO.getDfLinkNameFrench(),theDO.getDfUrlLink());
        					 else if( theSessionState.getLanguageId() == Mc.LANGUAGE_PREFERENCE_ENGLISH)
        						 urlList += addURL( theDO.getDfLinkNameEnglish(),theDO.getDfUrlLink());
        					
        					 showQLMenu = "Y";
        				 }
        			 }
        		 }
        		 
        		 if(urlList.length() != 0) {
        			
        			// display quick link list as drop down
            		 StaticTextField stQuickLinkList = (StaticTextField) thePage.getDisplayField(ExpressViewBeanBase.CHILD_STQUICKLINKLIST);
            		 
            		 stQuickLinkList.setValue(populateQuickLinkList(urlList,  theSessionState.getLanguageId()));
         		 	showQLMenu = "Y";
         		 	
        		 } 
        			
        		 
    		 }catch(Exception e){
    			 e.getMessage();
    		 }
    	 } 
    	
    	 hiddenField.setValue(showQLMenu);
    	    			
    	 if("N".equalsIgnoreCase(showQLMenu)) {
    		disableImages(thePage);
    		stInstProfileList.setValue("");
    		
    	 }
     }
    
    
    private void disableImages(ViewBean thePage){
	    ImageField imQL = (ImageField) thePage.getDisplayField(ExpressViewBeanBase.CHILD_IMQL1);
		imQL.setVisible(false);
		
		imQL = (ImageField) thePage.getDisplayField(ExpressViewBeanBase.CHILD_IMQL2);
		imQL.setVisible(false);
		
		imQL = (ImageField) thePage.getDisplayField(ExpressViewBeanBase.CHILD_IMQL3);
		imQL.setVisible(false);
    }
    
    
    /**
     * getAdditionalTagForImage
     * @param  urlLink 
     * @return extra html to be added to image
     * @throws 
     */
    private String getAdditionalTagForImage(doQuickLinkModel theDO){
    	
    	StringBuffer sbExtraHTML = new StringBuffer();
		 sbExtraHTML.append(" onClick='javascript:OpenQuickLink(\"" + theDO.getDfUrlLink() + "\");'" );
		 sbExtraHTML.append(" onError='setDefaultImage(this);'");
		 sbExtraHTML.append(" alt='" );
		 if( theSessionState.getLanguageId() == Mc.LANGUAGE_PREFERENCE_ENGLISH)
			 sbExtraHTML.append(theDO.getDfLinkNameEnglish());
		 else
			 sbExtraHTML.append(theDO.getDfLinkNameFrench());
		 sbExtraHTML.append( "'");
		 return sbExtraHTML.toString();
    }
    
    /**
     * populateQuickLinkList
     * @param  urlName 
     * @param  url
     * @return extra html to be added to image
     * @throws 
     */    
    private String addURL(String urlName, String url){
    	if(urlName != null && urlName.length() > 35)
    		urlName = urlName.substring(0, 35);
    	 return (" <li> <a href='#' onClick='OpenQuickLinkList(\"" + url + "\")'>" + urlName + "</a></li>");
    }
 
    /**
     * populateQuickLinkList
     * @param  urlList 
     * @param  langaugaId
     * @return extra html to be added to image
     * @throws 
     */
    private String populateQuickLinkList(String urlList, int langaugaId){
    	StringBuffer sbExtraHTML = new StringBuffer();
    	sbExtraHTML.append(" <span id='qllist' onclick='toggleQLDD()'>" );
		 
		if(langaugaId == Mc.LANGUAGE_PREFERENCE_FRENCH) 
			sbExtraHTML.append("   <img id='more' src='../images/more_sm_fr.gif'>");
		else if(langaugaId == Mc.LANGUAGE_PREFERENCE_ENGLISH)
			sbExtraHTML.append("   <img id='more' src='../images/more_sm.gif'>");
			
		sbExtraHTML.append("</span>	 ");
		sbExtraHTML.append(" <span  id='qldd' onmouseout='hideQLDD()' onmousemove='showQLDD()'>");
		sbExtraHTML.append(" <ul> ");
		sbExtraHTML.append( urlList );
		sbExtraHTML.append(" </ul> ");
		sbExtraHTML.append("</span>");
		return sbExtraHTML.toString();
	}
   
    /**
     * displayInstitutionProfile
     * @param  loggedUserInstMap 
     * @param  langaugaId
     * @param  flag
     * @return extra html to be added to image
     * @throws 
     */
    private String displayInstitutionProfile(Map loggedUserInstMap, int langaugaId){
    	
    	StringBuffer sbExtraHTML = new StringBuffer();
    	sbExtraHTML.append("<td valign=center align=right width='15%' >");
    	
    	sbExtraHTML.append(" <span id='instsec' onclick='");
    	sbExtraHTML.append("toggleInstDD()' ;>" );
		if(langaugaId == Mc.LANGUAGE_PREFERENCE_FRENCH) 
			sbExtraHTML.append("   <img id='institionprofile' src='../images/QLinksProfile_fr.gif' >");
		else if(langaugaId == Mc.LANGUAGE_PREFERENCE_ENGLISH)
			sbExtraHTML.append("   <img id='institionprofile' src='../images/QLinksProfile.gif' >");
			
		sbExtraHTML.append("</span>	 ");
		sbExtraHTML.append(" <span  ");
		sbExtraHTML.append("id='instdd' ");
		sbExtraHTML.append("onmouseout='hideInstDD()' onmousemove='showInstDD()'");
		sbExtraHTML.append(" <ul> ");
		
		for (Object key: loggedUserInstMap.keySet()) 
    	{
    		String InstValue =  key.toString();
    		String instLabels = null;
    		try 
    	    {	
    			instLabels = (String) BXResources.getInstitutionName(Integer.parseInt(InstValue));
    			
    			sbExtraHTML.append (" <li> <a href='#' onClick='selectedInst(\"" + InstValue + "\")'>" + instLabels + "</a></li>");
    	    }
			catch (Exception ex) {
				logger.error("PHC:displayInstitutionProfile() : Error while getting institutions name from Id:" + ex.getMessage());
			}
    	}
		
		sbExtraHTML.append(" </ul> ");
		sbExtraHTML.append("</span>");
		sbExtraHTML.append("<td>");
		return sbExtraHTML.toString();
		
    	
    }
    
  
    
    /**
     * getUserProfile
     * @return UserProfile userprofile object
     * @throws 
     */
    private UserProfile getUserProfile(int institutionProfileId) {
    	
    	UserProfile up = null;
    
    	try  {
			up = new UserProfile(srk);
	 		
	 		up.findByPrimaryKey(new UserProfileBeanPK(theSessionState.
	 				getUserProfileId(institutionProfileId),institutionProfileId));
	 		 if ((up.getGroupIdsInObjArray() != null) && (up.getGroupIdsInObjArray().length > 0))
	         {
	 			up.setBranchId(up.getUserBranchInfo(up.getGroupIdsInObjArray()[0].toString(),institutionProfileId));
	         }
	 	}
	 	catch(RemoteException re){
	 		_log.error("displayQuickLinkMenu():  Failed to get Branch details for logged in user failed: " + re.getMessage());
	 	}
	 	catch(FinderException fe){
	 		_log.error("displayQuickLinkMenu():  Failed to get Branch details for logged in user failed: " + fe.getMessage());
	 	}catch(Exception e){
	 		_log.error("displayQuickLinkMenu():  Failed to get Branch details for logged in user failed: " + e.getMessage());
	 	}
	 	return up;
    }
    //4.3GR, Update Total Loan Amount -- start
    //5.0MI, moved up to PHC, added propagation 
    protected static final String MI_PREMIUM_UPDATED = "MI_PREMIUM_UPDATED";
    
    protected boolean displayAMLForUpdatedMIPremium(Deal deal) {

        // if already notified, do nothing
        if ("Y".equals(deal.getMIPremiumAmountChangeNortified())) return false;

        // display AML only when mipremimum is updated
        // if(deal.getMIPremiumAmount() == 0.0) return false;
        if (deal.getMIPremiumAmount() == deal.getMIPremiumAmountPrevious()) return false;

        Object[] replaceArgs = { 
                deal.getMIPremiumAmount(), deal.getMIPremiumAmountPrevious(), 
                deal.getTotalLoanAmount(), deal.getNetLoanAmount() };

        String msg = BXResources.getSysMsg("MI_PREMIUM_UPDATED",
            theSessionState.getLanguageId(), replaceArgs);

        ActiveMessage am = setActiveMessageToAlert(msg,
            ActiveMsgFactory.ISCUSTOMDIALOG, MI_PREMIUM_UPDATED);

        /*
         * setActiveMessageToAlert method with ActiveMsgFactory.ISCUSTOMDIALOG
         * creates instance of CustomDialogMsg. this class generates two buttons
         * which are "ok" and "cancel", but we don�t want to display "cancel"
         * here. the following code will delete "cancel" button and
         * re-populating "ok" button.
         */
        am.setButtonHtml(Mc.OK_SERVER_EXECUTE_BTN);
        am.setButtonHtml(BXStringTokenizer.replace(am.getButtonHtml(), "!OK!",
            BXResources.getGenericMsg("AM_OK_IMAGE", theSessionState
                    .getLanguageId())));
        am.setResponseIdentifier2(null);
        return true;
    }
    
    public void handleOKOnAMLForUpdatedMIPremium(){

     PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
     SessionResourceKit srk = getSessionResourceKit();
     try
     {
         srk.beginTransaction();

         CalcMonitor dcm = CalcMonitor.getMonitor(srk);
         Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID(), dcm); //FXP32240, added dcm
         deal.forceChange("MIPremiumAmount");
         dcm.inputEntity(deal);
         doCalculation(dcm);

         //5.0 propagate MIPremiumAmountChangeNortified
         // deal.setMIPremiumAmountChangeNortified("Y");
         // deal.ejbStore();
         DealPropagator dealPropagator = new DealPropagator(deal, null);
         dealPropagator.setMIPremiumAmountChangeNortified("Y");
         dealPropagator.ejbStore();

         standardAdoptTxCopy(pg, true);
         srk.commitTransaction();

         // Must do this before Copy the page entry -- otherwise the modify flag will be incorrect
         srk.setModified(false);
         pg.setModified(false);

         // navigate to self to ensure creation of tx copy on generation
         getSavedPages().setNextPage(PageEntry.getPageEntryAsCopy(pg));
         navigateToNextPage(true);
         return;

     } catch (Exception e) {
         _log.error("Exception @handleMIPremiumUpdated", e);
         srk.cleanTransaction();
     }
     //on error, show error message and navigate to IWQ
     navigateAwayFromFromPage(true);
    }
    //4.3GR, Update Total Loan Amount -- end
}

