package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doDealInformationModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfEffectiveAmortization();

	
	/**
	 * 
	 * 
	 */
	public void setDfEffectiveAmortization(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfRateDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfRateDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPostedInterestRate();

	
	/**
	 * 
	 * 
	 */
	public void setDfPostedInterestRate(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfNetRate();

	
	/**
	 * 
	 * 
	 */
	public void setDfNetRate(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfRateLock();

	
	/**
	 * 
	 * 
	 */
	public void setDfRateLock(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDiscount();

	
	/**
	 * 
	 * 
	 */
	public void setDfDiscount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPremium();

	
	/**
	 * 
	 * 
	 */
	public void setDfPremium(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBuydownRate();

	
	/**
	 * 
	 * 
	 */
	public void setDfBuydownRate(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPIPayment();

	
	/**
	 * 
	 * 
	 */
	public void setDfPIPayment(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalPayment();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalPayment(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfFirstPaymentDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfFirstPaymentDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfMaturityDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfMaturityDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public String getDfRefSourceAppNo();

	
	/**
	 * 
	 * 
	 */
	public void setDfRefSourceAppNo(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfCrossSell();

	
	/**
	 * 
	 * 
	 */
	public void setDfCrossSell(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalPurchasePrice();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalPurchasePrice(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalEstimatedValue();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalEstimatedValue(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfActualAppraisedValue();

	
	/**
	 * 
	 * 
	 */
	public void setDfActualAppraisedValue(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLender();

	
	/**
	 * 
	 * 
	 */
	public void setDfLender(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfServicingMortgageNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfServicingMortgageNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPreAppEstPurchasePrice();

	
	/**
	 * 
	 * 
	 */
	public void setDfPreAppEstPurchasePrice(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfRateCode();

	
	/**
	 * 
	 * 
	 */
	public void setDfRateCode(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfProductName();

	
	/**
	 * 
	 * 
	 */
	public void setDfProductName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfInvestor();

	
	/**
	 * 
	 * 
	 */
	public void setDfInvestor(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFEFFECTIVEAMORTIZATION="dfEffectiveAmortization";
	public static final String FIELD_DFRATEDATE="dfRateDate";
	public static final String FIELD_DFPOSTEDINTERESTRATE="dfPostedInterestRate";
	public static final String FIELD_DFNETRATE="dfNetRate";
	public static final String FIELD_DFRATELOCK="dfRateLock";
	public static final String FIELD_DFDISCOUNT="dfDiscount";
	public static final String FIELD_DFPREMIUM="dfPremium";
	public static final String FIELD_DFBUYDOWNRATE="dfBuydownRate";
	public static final String FIELD_DFPIPAYMENT="dfPIPayment";
	public static final String FIELD_DFTOTALPAYMENT="dfTotalPayment";
	public static final String FIELD_DFFIRSTPAYMENTDATE="dfFirstPaymentDate";
	public static final String FIELD_DFMATURITYDATE="dfMaturityDate";
	public static final String FIELD_DFREFSOURCEAPPNO="dfRefSourceAppNo";
	public static final String FIELD_DFCROSSSELL="dfCrossSell";
	public static final String FIELD_DFTOTALPURCHASEPRICE="dfTotalPurchasePrice";
	public static final String FIELD_DFTOTALESTIMATEDVALUE="dfTotalEstimatedValue";
	public static final String FIELD_DFACTUALAPPRAISEDVALUE="dfActualAppraisedValue";
	public static final String FIELD_DFLENDER="dfLender";
	public static final String FIELD_DFSERVICINGMORTGAGENUMBER="dfServicingMortgageNumber";
	public static final String FIELD_DFPREAPPESTPURCHASEPRICE="dfPreAppEstPurchasePrice";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFRATECODE="dfRateCode";
	public static final String FIELD_DFPRODUCTNAME="dfProductName";
	public static final String FIELD_DFINVESTOR="dfInvestor";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

