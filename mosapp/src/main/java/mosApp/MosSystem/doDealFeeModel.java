package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doDealFeeModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfFeeAmount();

	
	/**
	 * 
	 * 
	 */
	public void setDfFeeAmount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfFeePaymentDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfFeePaymentDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfFeePaymentMethodId();

	
	/**
	 * 
	 * 
	 */
	public void setDfFeePaymentMethodId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfFeeStatusId();

	
	/**
	 * 
	 * 
	 */
	public void setDfFeeStatusId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealFeeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealFeeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfFeeTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfFeeTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);
	
	
	  public java.math.BigDecimal getDfInstitutionId();
	  public void setDfInstitutionId(java.math.BigDecimal id);
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFFEEAMOUNT="dfFeeAmount";
	public static final String FIELD_DFFEEPAYMENTDATE="dfFeePaymentDate";
	public static final String FIELD_DFFEEPAYMENTMETHODID="dfFeePaymentMethodId";
	public static final String FIELD_DFFEESTATUSID="dfFeeStatusId";
	public static final String FIELD_DFDEALFEEID="dfDealFeeId";
	public static final String FIELD_DFFEETYPEID="dfFeeTypeId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	
	 public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

