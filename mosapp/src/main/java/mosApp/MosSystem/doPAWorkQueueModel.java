package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doPAWorkQueueModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public String getDfCONTACTLASTNAME();

	
	/**
	 * 
	 * 
	 */
	public void setDfCONTACTLASTNAME(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSFSHORTNAME();

	
	/**
	 * 
	 * 
	 */
	public void setDfSFSHORTNAME(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfASSIGNEDTASKSWORKQUEUEID();

	
	/**
	 * 
	 * 
	 */
	public void setDfASSIGNEDTASKSWORKQUEUEID(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDEALID();


	
	/**
	 * 
	 * 
	 */
	public void setDfDEALID(java.math.BigDecimal value);


	
	/**
	 * 
	 * 
	 */
	public String getDfAPPLICATIONID();

	
	/**
	 * 
	 * 
	 */
	public void setDfAPPLICATIONID(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTASKID();

	
	/**
	 * 
	 * 
	 */
	public void setDfTASKID(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfTASK_TASKLABEL();

	
	/**
	 * 
	 * 
	 */
	public void setDfTASK_TASKLABEL(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfTASKSTATUS_TSDESCRIPTION();

	
	/**
	 * 
	 * 
	 */
	public void setDfTASKSTATUS_TSDESCRIPTION(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPRIORITY_PDESCRIPTION();

	
	/**
	 * 
	 * 
	 */
	public void setDfPRIORITY_PDESCRIPTION(String value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfDUETIMESTAMP();

	
	/**
	 * 
	 * 
	 */
	public void setDfDUETIMESTAMP(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public String getDfTMDESCRIPTION();

	
	/**
	 * 
	 * 
	 */
	public void setDfTMDESCRIPTION(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCOPYID();

	
	/**
	 * 
	 * 
	 */
	public void setDfCOPYID(java.math.BigDecimal value);
	
	
    /**
     * 
     * MCM team ML Ticket merge FXP22063
     */
    public java.math.BigDecimal getDfINSTITUTIONPROFILEID();

    /**
     * 
     * MCM team ML Ticket merge FXP22063
     */
    public void setDfINSTITUTIONPROFILEID(java.math.BigDecimal value);
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFCONTACTLASTNAME="dfCONTACTLASTNAME";
	public static final String FIELD_DFSFSHORTNAME="dfSFSHORTNAME";
	public static final String FIELD_DFASSIGNEDTASKSWORKQUEUEID="dfASSIGNEDTASKSWORKQUEUEID";
	public static final String FIELD_DFDEALID="dfDEALID";
    /****** MCM team ML Ticket merge FXP22063 ******/
    public static final String FIELD_DFINSTITUTIONPROFILEID="dfINSTITUTIONPROFILEID";
    /****** MCM team ML Ticket merge FXP22063 ******/
    public static final String FIELD_DFAPPLICATIONID="dfAPPLICATIONID";

	public static final String FIELD_DFTASKID="dfTASKID";
	public static final String FIELD_DFTASK_TASKLABEL="dfTASK_TASKLABEL";
	public static final String FIELD_DFTASKSTATUS_TSDESCRIPTION="dfTASKSTATUS_TSDESCRIPTION";
	public static final String FIELD_DFPRIORITY_PDESCRIPTION="dfPRIORITY_PDESCRIPTION";
	public static final String FIELD_DFDUETIMESTAMP="dfDUETIMESTAMP";
	public static final String FIELD_DFTMDESCRIPTION="dfTMDESCRIPTION";
	public static final String FIELD_DFCOPYID="dfCOPYID";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

