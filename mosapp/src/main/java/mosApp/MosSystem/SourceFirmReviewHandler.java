package mosApp.MosSystem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.entity.GroupProfile;
import com.filogix.express.legacy.mosapp.ISourceFirmReviewHandler;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.ViewBean;

// Special usage of Conditions:
	//
	// 1) pageCondition3
	//
	//    Value - true:  Run trigger workflow when submit
	//
	//          - false: Default - not calling workflow

public class SourceFirmReviewHandler extends PageHandlerCommon
	implements Cloneable, Sc, ISourceFirmReviewHandler
{
	// Key name to pass the SOB Id

	// following used as key in page state table to indicate if user has edit permission

	public static final String USER_CAN_EDIT="USER_CAN_EDIT";


	/**
	 *
	 *
	 */
	public SourceFirmReviewHandler cloneSS()
	{
		return(SourceFirmReviewHandler) super.cloneSafeShallow();

	}


	/////////////////////////////////////////////////////////////////////////

	public void preHandlerProtocol(ViewBean currNDPage)
	{
		super.preHandlerProtocol(currNDPage);


		// sefault page load method to display()
		PageEntry pg = theSessionState.getCurrentPage();

		pg.setPageDisplayMethod(Mc.DISPLAY_METHOD_DISP);

	}


	/////////////////////////////////////////////////////////////////////////
	// This method is used to populate the fields in the header
	// with the appropriate information

	public void populatePageDisplayFields()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		Hashtable pst = pg.getPageStateTable();

		ViewBean theNDPage = getCurrNDPage();


		//////////// manually populate display fields /////////////////
		////CSpCriteriaSQLObject theDO =(CSpCriteriaSQLObject) getModel(doSourceFirmModel.class);
    doSourceFirmModel theDO = (doSourceFirmModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doSourceFirmModel.class);

		// Set PhoneNum with extension
		////setPhoneNumTextDisplayField(theDO, "stWorkPhoneNumber", "dfSBPPhone", "dfSBPPhoneExt");
		setPhoneNumTextDisplayField(theDO, "stWorkPhoneNumber", theDO.FIELD_DFSBPPHONE, theDO.FIELD_DFSBPPHONEEXT, 0);

		// Set Fax # without extension
		////setPhoneNumTextDisplayField(theDO, "stFaxNumber", "dfSBPFax", "");
		setPhoneNumTextDisplayField(theDO, "stFaxNumber", theDO.FIELD_DFSBPFAX, "", 0);

		//// Get the user profile from the Model.
    String sSBPUserProfileId = "";

    Object oSBPUserProfileId  = theDO.getValue(theDO.FIELD_DFSBPUSERPROFILEID);
    if (oSBPUserProfileId  == null)
    {
      sSBPUserProfileId = "";
    }
    else if((oSBPUserProfileId  != null))
    {
      sSBPUserProfileId  = oSBPUserProfileId .toString();
      //logger.debug("SBRH@populatePageDisplayFields::SBPUserProfileId FromModel: " + (new Integer(oSBPUserProfileId .toString())).intValue());
    }

    if(sSBPUserProfileId != null && !sSBPUserProfileId.equals(""))
    {
      Integer SBUserProfileId = new Integer(sSBPUserProfileId);

      // Set Associated User Name
      ////CSpCriteriaSQLObject theUserDO =(CSpCriteriaSQLObject) getModel(doSourceUserModel.class);
      doSourceUserModel theUserDO = (doSourceUserModel)
                                              RequestManager.getRequestContext().getModelManager().getModel(doSourceUserModel.class);

      theUserDO.clearUserWhereCriteria();

      ////theUserDO.addUserWhereCriterion("dfUserProfileId", "=", theDO.getValue(0, "dfSBPUserProfileId"));
      theUserDO.addUserWhereCriterion("dfUserProfileId", "=", SBUserProfileId);

      ////theUserDO.execute();

      ////if (theUserDO != null && theUserDO.getNumRows() > 0) theNDPage.setDisplayFieldValue("stSourceUserName", theUserDO.getValue(0, "dfUsername"));
      ////else theNDPage.setDisplayFieldValue("stSourceUserName", new String("--None--"));

      String userName = "";

       try
       {
          ResultSet rsUser = theUserDO.executeSelect(null);
          logger.debug("SFRH@populatePageDisplayFields::::ResultSet: " + rsUser);

          ////((QueryModelBase)theModel).setResultSet(rs);
          if (rsUser != null && theUserDO.getSize() > 0)
          {
            Object oUserName = theUserDO.getValue(theUserDO.FIELD_DFUSERNAME);
            if (oUserName == null)
            {
              userName = "";
            }
            else if((oUserName != null))
            {
              userName = oUserName.toString();
              logger.debug("SFRH@populatePageDisplayFields::UserNameFromModel: " + userName.toString());
            }

            theNDPage.setDisplayFieldValue("stSourceUserName", userName);
          }
          else
              theNDPage.setDisplayFieldValue("stSourceUserName", new String("--None--"));

        }
        catch (ModelControlException mce)
        {
          logger.debug("SFRH@populatePageDisplayFields::MCEException: " + mce);
        }

        catch (SQLException sqle)
        {
          logger.debug("SFRH@populatePageDisplayFields::SQLException: " + sqle);
        }
    }

    //=================================================
    //--> CRF#37 :: Source/Firm to Group routing for TD
    //=================================================
    //--> Added new fields to handle SourceFirm to Group association
    //--> By Billy 19Aug2003
    // Set Associated Group Name
    String sSFPGroupId = "";

    Object oSFPGroupId  = theDO.getValue(theDO.FIELD_DFSBPGROUPPROFILEID);
    if (oSFPGroupId  == null)
    {
      sSFPGroupId = "";
    }
    else
    {
      sSFPGroupId  = oSFPGroupId.toString();
      logger.debug("SFRH@populatePageDisplayFields::SFPGroupId From Model: " + oSFPGroupId.toString());
    }

    if(sSFPGroupId != null && !sSFPGroupId.equals(""))
    {
      try
      {
        int groupId = Integer.parseInt(sSFPGroupId);

        GroupProfile theGp = new GroupProfile(srk, groupId);
        theNDPage.setDisplayFieldValue("stSourceGroupName", theGp.getGroupName());
      }
      catch(Exception eGp)
      {
        logger.debug("SFRH@populatePageDisplayFields::Exception when setting up Group Name: " + eGp);
      }
    }
    //==================================================

		///////////// End manual population of DisplayFields ////////////////
		populatePageShellDisplayFields();

		populatePageDealSummarySnapShot();

		displayDSSConditional(pg, getCurrNDPage());

	}


	/////////////////////////////////////////////////////////////////////////

	private void displayDSSConditional(PageEntry pg, ViewBean thePage)
	{
		String suppressStart = "<!--";

		String suppressEnd = "//-->";

		if (pg == null || pg.getPageDealId() <= 0)
		{
			// suppress
			thePage.setDisplayFieldValue("stIncludeDSSstart", new String(suppressStart));
			thePage.setDisplayFieldValue("stIncludeDSSend", new String(suppressEnd));
			return;
		}


		// display
		thePage.setDisplayFieldValue("stIncludeDSSstart", new String(""));

		thePage.setDisplayFieldValue("stIncludeDSSend", new String(""));

		return;

	}


	/////////////////////////////////////////////////////////////////////////

	public void setupBeforePageGeneration()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();


		// get SOB profile id
		String pId =(String) pst.get(SFIRM_ID_PASSED_PARM);


		// determine if user can modify existing party information
		if (getUserAccessType(Mc.PGNM_SFIRM_ADD) == Sc.PAGE_ACCESS_EDIT) pst.put(USER_CAN_EDIT, USER_CAN_EDIT);

		////CSpCriteriaSQLObject theDO =(CSpCriteriaSQLObject) getModel(doSourceFirmModel.class);
    doSourceFirmModel theDO = (doSourceFirmModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doSourceFirmModel.class);

		theDO.clearUserWhereCriteria();

		if (pId != null)
        theDO.addUserWhereCriterion("dfSBPFirmProfileId", "=", new String(pId));

		setupDealSummarySnapShotDO(pg);

	}


	/**
	 *
	 *
	 */
	public void handleAddSourceFirm()
	{
		try
		{
			////SessionState theSession = getTheSession();
			PageEntry pg = theSessionState.getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");
			PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_SFIRM_ADD, true);
			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage(true);
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setStandardFailMessage();
			logger.error("Error @SourceFirmReviewHandler.handleAddSourceFirm()");
			logger.error(e);
		}

	}


	/**
	 *
	 *
	 */
	////public int handleViewModifyButton()
	public boolean handleViewModifyButton()
  	{
		PageEntry pg = theSessionState.getCurrentPage();

		try
		{
			Hashtable pst = pg.getPageStateTable();
			if (pst.get(USER_CAN_EDIT) != null)
          return true;
		}
		catch(Exception e)
		{
			;
		}

		return false;
	}


	/**
	 *
	 *
	 */
	public void handleModifySF()
	{
		try
		{
			PageEntry pg = theSessionState.getCurrentPage();
			PageEntry editSFPage = setupSubPagePageEntry(pg, Mc.PGNM_SFIRM_ADD, true);
			Hashtable subPst = editSFPage.getPageStateTable();
			String SFProfileId = getCurrNDPage().getDisplayFieldValue("hdSFPId").toString();
			subPst.put(SFIRM_ID_PASSED_PARM, SFProfileId);
			getSavedPages().setNextPage(editSFPage);
			navigateToNextPage(true);
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setStandardFailMessage();
			logger.error("Error @SourceFirmReviewHandler.handleModifySF()");
			logger.error(e);
		}

	}


	/**
	 *
	 *
	 */
	public void handleOKSpecial()
	{
		PageEntry pg = theSessionState.getCurrentPage();


		// Check if PageCondition 3 set ==> call workflow
		if (pg.getPageCondition3() == true)
		{
			try
			{
				srk.beginTransaction();
				workflowTrigger(2, srk, pg, null);
				srk.commitTransaction();
			}
			catch(Exception e)
			{
				srk.cleanTransaction();
				logger.error("Error @SourceFirmReviewHandler.handleOKSpecial: ");
				logger.error(e);
			}
		}

		this.handleCancelStandard();

	}

}

