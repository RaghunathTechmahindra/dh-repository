package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doLifeDisabilityInsuranceInfoModelImpl extends QueryModelBase
  implements doLifeDisabilityInsuranceInfoModel
{
  /**
   *
   *
   */
  public doLifeDisabilityInsuranceInfoModelImpl()
  {
    super();
    setDataSourceName(DATA_SOURCE_NAME);

    setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

    setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

    setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

    setFieldSchema(FIELD_SCHEMA);

    initialize();

  }


  /**
   *
   *
   */
  public void initialize()
  {
  }


  /**
   *
   *
   */
  protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
    throws ModelControlException
  {
    logger = SysLog.getSysLogger("LDII");
    logger.debug("VLADLD ===> LDIP@SQLBeforeExecute: " + sql);

    return sql;

  }


  /**
   *
   *
   */
  protected void afterExecute(ModelExecutionContext context, int queryType)
    throws ModelControlException
  {

    logger = SysLog.getSysLogger("LDII");
    logger.debug("DOLDI@afterExecute::Size: " + this.getSize());
    //logger.debug("DOLDI@afterExecute::SQLTemplate: " + this.getSelectSQL());

    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
      getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();

    while(this.next())
    {
      //  Translate string content if necessary.
    }
  }


  /**
   *
   *
   */
  protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
  {
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfDealId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
  }


  /**
   *
   *
   */
  public void setDfDealId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFDEALID,value);
  }


  /**
   *
   *
   */
  public java.math.BigDecimal getDfCopyId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
  }


  /**
   *
   *
   */
  public void setDfCopyId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFCOPYID,value);
  }
  /**
   *
   *
   */
  public void setDfBorrowerId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFBORROWERID,value);
  }


  /**
   *
   *
   */

  public java.math.BigDecimal getDfBorrowerId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
  }

  /**
   *
   *
   */
  public String getDfFirstBorrowerName()
  {
    return (String)getValue(FIELD_DFBORROWERFIRSTNAME);
  }

  /**
   *
   *
   */
  public void setDfBorrowerFirstName(String value)
  {
    setValue(FIELD_DFBORROWERFIRSTNAME,value);
  }

  /**
   *
   *
   */
  public String getDfMiddleBorrowerInitial()
  {
    return (String)getValue(FIELD_DFBORROWERMIDDLEINITIAL);
  }

  /**
   *
   *
   */
  public void setDfMiddleFirstInitial(String value)
  {
    setValue(FIELD_DFBORROWERMIDDLEINITIAL,value);
  }

  /**
   *
   *
   */
  public String getDfLastBorrowerName()
  {
    return (String)getValue(FIELD_DFBORROWERLASTNAME);
  }

  /**
   *
   *
   */
  public void setDfBorrowerLastName(String value)
  {
    setValue(FIELD_DFBORROWERLASTNAME,value);
  }

  /**
   *
   *
   */
  public String getDfCopyType()
  {
    return (String)getValue(FIELD_DFCOPYTYPE);
  }

  /**
   *
   *
   */
  public void setDfCopyType(String value)
  {
    setValue(FIELD_DFCOPYTYPE,value);
  }

  /**
   *
   *
   */
  public void setDfCopyID(java.math.BigDecimal value)
  {
    setValue(FIELD_DFCOPYID,value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfLifePercentCoverageReq()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFLIFEPERCENTCOVERAGEREQ);
  }

  /**
   *
   *
   */
  public void setDfLifePercentCoverageReq(java.math.BigDecimal value)
  {
    setValue(FIELD_DFLIFEPERCENTCOVERAGEREQ,value);
  }

  /**
   *
   *
   */
  public void setDfLifePremium(java.math.BigDecimal value)
  {
    setValue(FIELD_DFLIFEPREMIUM,value);
  }

  /**
   *
   *
   */

  public java.math.BigDecimal getDfLifePremium()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFLIFEPREMIUM);
  }

  /**
   *
   *
   */
  public void setDfDisabilityPremium(java.math.BigDecimal value)
  {
    setValue(FIELD_DFDISABILITYPREMIUM,value);
  }

  /**
   *
   *
   */

  public java.math.BigDecimal getDfDisabilityPremium()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFDISABILITYPREMIUM);
  }

  /**
   *
   *
   */
  public void setDfCombinedLDPremium(java.math.BigDecimal value)
  {
    setValue(FIELD_DFCOMBINEDLDPREMIUM,value);
  }

  /**
   *
   *
   */

  public java.math.BigDecimal getDfCombindedLDPremium()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDLDPREMIUM);
  }

  /**
   *
   *
   */
  public void setDfPmntPlusLifeDisability(java.math.BigDecimal value)
  {
    setValue(FIELD_DFPMNTPLUSLIFEDISABILITY,value);
  }

  /**
   *
   *
   */

  public java.math.BigDecimal getDfPmntPlusLifeDisability()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFPMNTPLUSLIFEDISABILITY);
  }

  /**
   *
   *
   */
  public void setDfInterestRateIncrement(java.math.BigDecimal value)
  {
    setValue(FIELD_DFINTERESTRATEINCREMENT,value);
  }

  /**
   *
   *
   */

  public java.math.BigDecimal getDfInterestRateIncrement()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFINTERESTRATEINCREMENT);
  }

  /**
   *
   *
   */
  public void setDfLifeDisabilityPremiumsId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFLIFEDISABILITYPREMIUMSID,value);
  }

  /**
   *
   *
   */

  public java.math.BigDecimal getDfLifeDisabilityPremiumsId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFLIFEDISABILITYPREMIUMSID);
  }

  /**
   *
   *
   */
  public void setDfTotalLoanAmount(java.math.BigDecimal value)
  {
    setValue(FIELD_DFTOTALLOANAMOUNT,value);
  }

  /**
   *
   *
   */

  public java.math.BigDecimal getDfTotalLoanAmount()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFTOTALLOANAMOUNT);
  }

  /**
   *
   *
   */
  public void setDfLenderProfileId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFLENDERPROFILEID,value);
  }

  /**
   *
   *
   */

  public java.math.BigDecimal getDfLenderProfileId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFLENDERPROFILEID);
  }

  /**
   *
   *
   */
  public String getDfRateCode()
  {
    return (String)getValue(FIELD_DFLDRATECODE);
  }

  /**
   *
   *
   */
  public void setDfRateCode(String value)
  {
    setValue(FIELD_DFLDRATECODE,value);
  }

  /**
   *
   *
   */
  public void setDfLDRate(java.math.BigDecimal value)
  {
    setValue(FIELD_DFLDRATE,value);
  }

  /**
   *
   *
   */

  public java.math.BigDecimal getDfLDRate()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFLDRATE);
  }

  /**
   *
   *
   */
  public void setDfBorrowerGenderId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFBORROWERGENDERID,value);
  }

  /**
   *
   *
   */

  public java.math.BigDecimal getDfBorrowerGenderId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFBORROWERGENDERID);
  }

  /**
   *
   *
   */
  public void setDfBorrowerSmokerId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFBORROWERSMOKERID,value);
  }

  /**
   *
   *
   */

  public java.math.BigDecimal getDfBorrowerSmokerId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFBORROWERSMOKERID);
  }

  /**
   *
   *
   */
  public String getDfLifeStatusDesc()
  {
    return (String)getValue(FIELD_DFLIFESTATUSDESC);
  }

  /**
   *
   *
   */
  public void setDfLifeStatusDesc(String value)
  {
    setValue(FIELD_DFLIFESTATUSDESC,value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfLifeStatusId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFLIFESTATUSID);
  }

  /**
   *
   *
   */
  public void setDfLifeStatusId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFLIFESTATUSID,value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfDisabilityStatusId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFDISABILITYSTATUSID);
  }

  /**
   *
   *
   */
  public void setDfDisabilityStatusId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFDISABILITYSTATUSID,value);
  }

  /**
   *
   *
   */
  public String getDfDisabilityStatusDesc()
  {
    return (String)getValue(FIELD_DFDISABILITYSTATUSDESC);
  }

  /**
   *
   *
   */
  public void setDfDisabilityStatusDesc(String value)
  {
    setValue(FIELD_DFDISABILITYSTATUSDESC,value);
  }

  /**
   *
   *
   */
  public void setDfLifeStatusValue(String value)
  {
    setValue(FIELD_DFLIFESTATUSVALUE,value);
  }

  /**
   *
   *
   */
  public String getDfLifeStatusValue()
  {
    return (String)getValue(FIELD_DFLIFESTATUSVALUE);
  }

  /**
   *
   *
   */
  public void setDfDisabilityStatusValue(String value)
  {
    setValue(FIELD_DFLDISABILITYSTATUSVALUE,value);
  }

  /**
   *
   *
   */
  public String getDfDisabilityStatusValue()
  {
    return (String)getValue(FIELD_DFLDISABILITYSTATUSVALUE);
  }

  /**
   *
   *
   */
  public String getDfLDInsuranceAgeDesc()
  {
    return (String)getValue(FIELD_DFLDINSURANCEAGEDESC);
  }

  /**
   *
   *
   */
  public void setDfInsuranceAgeDesc(String value)
  {
    setValue(FIELD_DFLDINSURANCEAGEDESC,value);
  }

  /**
   *
   *
   */
  public String getDfBorrowerGenderDesc()
  {
    return (String)getValue(FIELD_DFBORROWERGENDERDESC);
  }

  /**
   *
   *
   */
  public void setDfBorrowerGenderDesc(String value)
  {
    setValue(FIELD_DFBORROWERGENDERDESC,value);
  }

  /**
   *
   *
   */
  public String getDfBorrowerSmoker()
  {
    return (String)getValue(FIELD_DFSMOKER);
  }

  /**
   *
   *
   */
  public void setDfBorrowerSmoker(String value)
  {
    setValue(FIELD_DFSMOKER,value);
  }

  /**
   *
   *
   */
  public String getDfPrimaryBorrowerFlag()
  {
    return (String)getValue(FIELD_DFPRIMARYBORROWERFLAG);
  }

  /**
   *
   *
   */
  public void setDfPrimaryBorrowerFlag(String value)
  {
    setValue(FIELD_DFPRIMARYBORROWERFLAG,value);
  }

  /**
   *
   *
   */
  public void setDfLDInsuranceTypeId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFLDINSURANCETYPEID,value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfLDInsuranceTypeId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFLDINSURANCETYPEID);
  }

  /**
   *
   *
   */
  public String getDfLDInsuranceTypeDesc()
  {
    return (String)getValue(FIELD_DFLDINSURANCETYPEDESC);
  }

  /**
   *
   *
   */
  public void setDfInsuranceTypeDesc(String value)
  {
    setValue(FIELD_DFLDINSURANCETYPEDESC,value);
  }

  /**
   *
   *
   */
  public String getDfIPDesc()
  {
    return (String)getValue(FIELD_DFIPDESC);
  }

  /**
   *
   *
   */
  public void setDfIPDesc(String value)
  {
    setValue(FIELD_DFIPDESC,value);
  }

  /**
   *
   *
   */
  public void setDfAge(java.math.BigDecimal value)
  {
    setValue(FIELD_DFAGE,value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfAge()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFAGE);
  }


  /**
   *
   *
   */
  public java.sql.Timestamp getDfBorrowerBirthDate()
  {
    return (java.sql.Timestamp)getValue(FIELD_DFBORROWERBIRTHDATE);
  }

  /**
   *
   *
   */
  public void setDfBorrowerBirthDate(java.sql.Timestamp value)
  {
    setValue(FIELD_DFBORROWERBIRTHDATE,value);
  }

  /**
   * MetaData object test method to verify the SYNTHETIC new
   * field for the reogranized SQL_TEMPLATE query.
   *
   */
  /**
  public void setResultSet(ResultSet value)
  {
      logger = SysLog.getSysLogger("METADATA");

      try
      {
         super.setResultSet(value);
         if( value != null)
         {
             ResultSetMetaData metaData = value.getMetaData();
             int columnCount = metaData.getColumnCount();
             logger.debug("===============================================");
                        logger.debug("Testing MetaData Object for new synthetic fields for" +
                               " doDealEntrySourceInfoModel");
             logger.debug("NumberColumns: " + columnCount);
             for(int i = 0; i < columnCount ; i++)
              {
                   logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
              }
              logger.debug("================================================");
          }
      }
      catch (SQLException e)
      {
            logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
      }
   }
  **/

  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Custom Methods - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  public SysLogger logger;

  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////

  public static final String DATA_SOURCE_NAME="jdbc/orcl";

  public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL BORROWER.BORROWERID, BORROWER.BORROWERFIRSTNAME, BORROWER.BORROWERMIDDLEINITIAL, " +
    "BORROWER.BORROWERLASTNAME, BORROWER.LIFEPERCENTCOVERAGEREQ, DEAL.LIFEPREMIUM, " +
    "BORROWER.BORROWERBIRTHDATE, BORROWER.AGE, BORROWER.PRIMARYBORROWERFLAG, " +
    "BORROWER.BORROWERGENDERID, DEAL.DISABILITYPREMIUM, DEAL.COMBINEDPREMIUM, " +
    "BORROWER.LIFESTATUSID, BORROWER.DISABILITYSTATUSID, BORROWER.SMOKESTATUSID, " +
    "DEAL.PMNTPLUSLIFEDISABILITY, DEAL.INTERESTRATEINCREMENT, " +
    "LIFEDISABILITYPREMIUMS.LIFEDISABILITYPREMIUMSID, DEAL.COPYID, DEAL.COPYTYPE, " +
    "DEAL.DEALID, DEAL.COPYID, DEAL.TOTALLOANAMOUNT, " +
    "LENDERPROFILE.LENDERPROFILEID, LDINSURANCERATES.LDRATECODE, " +
    "LDINSURANCERATES.LDRATE, LIFESTATUS.LIFESTATUSVALUE, DISABILITYSTATUS.DISABILITYSTATUSVALUE, " +
    "LIFESTATUS.LIFESTATUSDESC, DISABILITYSTATUS.DISABILITYSTATUSDESC, " +
    "LDINSURANCEAGERANGES.LDINSURANCEAGEDESC, BORROWERGENDER.BORROWERGENDERDESC, " +
    "LDINSURANCETYPE.LDINSURANCETYPEID, LDINSURANCETYPE.LDINSURANCETYPEDESC, " +
    "INSURANCEPROPORTIONS.IPDESC, SMOKESTATUS.SMOKER " +
    "FROM BORROWER, LIFEDISABILITYPREMIUMS, DEAL, LENDERPROFILE, " +
    "LDINSURANCERATES, LIFESTATUS, DISABILITYSTATUS, " +
    "LDINSURANCEAGERANGES, BORROWERGENDER, LDINSURANCETYPE, " +
    "INSURANCEPROPORTIONS, SMOKESTATUS __WHERE__ ORDER BY BORROWER.BORROWERID  ASC";

  public static final String MODIFYING_QUERY_TABLE_NAME=
    "BORROWER, LIFEDISABILITYPREMIUMS, DEAL, LENDERPROFILE, " +
    "LDINSURANCERATES, LIFESTATUS, DISABILITYSTATUS, " +
    "LDINSURANCEAGERANGES, BORROWERGENDER, LDINSURANCETYPE, INSURANCEPROPORTIONS, SMOKESTATUS ";

  public static final String STATIC_WHERE_CRITERIA =
    "(BORROWER.COPYID  =  LIFEDISABILITYPREMIUMS.COPYID) AND " +
    "(BORROWER.BORROWERID = LIFEDISABILITYPREMIUMS.BORROWERID) AND " +
    "(DEAL.COPYID  =  BORROWER.COPYID) AND " +
    "(DEAL.DEALID = BORROWER.DEALID) AND " +
    "(LENDERPROFILE.LENDERPROFILEID = LIFEDISABILITYPREMIUMS.LENDERPROFILEID) AND " +
    "(LIFEDISABILITYPREMIUMS.LDINSURANCERATEID = LDINSURANCERATES.LDINSURANCERATEID) AND " +
    "(LDINSURANCEAGERANGES.LDINSURANCEAGEID = LDINSURANCERATES.LDINSURANCEAGEID) AND " +
    "(LDINSURANCEAGERANGES.BORROWERGENDERID = BORROWERGENDER.BORROWERGENDERID) AND " +
    "(LDINSURANCERATES.LDINSURANCETYPEID = LDINSURANCETYPE.LDINSURANCETYPEID) AND " +
    "(BORROWER.INSURANCEPROPORTIONSID = INSURANCEPROPORTIONS.INSURANCEPROPORTIONSID) AND " +
    "(BORROWER.LIFESTATUSID = LIFESTATUS.LIFESTATUSID) AND " +
    "(BORROWER.DISABILITYSTATUSID = DISABILITYSTATUS.DISABILITYSTATUSID) AND " +
    "(BORROWER.SMOKESTATUSID = SMOKESTATUS.SMOKESTATUSID) AND " +
    "(DEAL.INSTITUTIONPROFILEID = BORROWER.INSTITUTIONPROFILEID) AND " +
    "(DEAL.INSTITUTIONPROFILEID = LIFEDISABILITYPREMIUMS.INSTITUTIONPROFILEID) AND " +
    "(DEAL.INSTITUTIONPROFILEID = LENDERPROFILE.INSTITUTIONPROFILEID) ";    

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();

  public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
  public static final String COLUMN_DFDEALID="DEALID";

  public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
  public static final String COLUMN_DFCOPYID="COPYID";

  public static final String QUALIFIED_COLUMN_DFBORROWERID="BORROWER.BORROWERID";
  public static final String COLUMN_DFBORROWERID="BORROWERID";

  public static final String QUALIFIED_COLUMN_DFBORROWERFIRSTNAME="BORROWER.BORROWERFIRSTNAME";
  public static final String COLUMN_DFBORROWERFIRSTNAME="BORROWERFIRSTNAME";

  public static final String QUALIFIED_COLUMN_DFBORROWERMIDDLEINITIAL="BORROWER.BORROWERMIDDLEINITIAL";
  public static final String COLUMN_DFBORROWERMIDDLEINITIAL="BORROWERMIDDLEINITIAL";

  public static final String QUALIFIED_COLUMN_DFBORROWERLASTNAME="BORROWER.BORROWERLASTNAME";
  public static final String COLUMN_DFBORROWERLASTNAME="BORROWERLASTNAME";

  public static final String QUALIFIED_COLUMN_DFPRIMARYBORROWERFLAG="BORROWER.PRIMARYBORROWERFLAG";
  public static final String COLUMN_DFPRIMARYBORROWERFLAG="PRIMARYBORROWERFLAG";

  public static final String QUALIFIED_COLUMN_DFLIFEPERCENTCOVERAGEREQ="BORROWER.LIFEPERCENTCOVERAGEREQ";
  public static final String COLUMN_DFLIFEPERCENTCOVERAGEREQ="LIFEPERCENTCOVERAGEREQ";

  public static final String QUALIFIED_COLUMN_BORROWERBIRTHDATE="BORROWER.BORROWERBIRTHDATE";
  public static final String COLUMN_BORROWERBIRTHDATE="BORROWERBIRTHDATE";

  public static final String QUALIFIED_COLUMN_AGE="BORROWER.AGE";
  public static final String COLUMN_AGE="AGE";

  public static final String QUALIFIED_COLUMN_BORROWERGENDERID="BORROWER.BORROWERGENDERID";
  public static final String COLUMN_BORROWERGENDERID="BORROWERGENDERID";

  public static final String QUALIFIED_COLUMN_DFLIFEPREMIUM="DEAL.LIFEPREMIUM";
  public static final String COLUMN_DFLIFEPREMIUM="LIFEPREMIUM";

  public static final String QUALIFIED_COLUMN_DFDISABILITYPREMIUM="DEAL.DISABILITYPREMIUM";
  public static final String COLUMN_DFDISABILITYPREMIUM="DISABILITYPREMIUM";

  public static final String QUALIFIED_COLUMN_DFCOMBINEDLDPREMIUM="DEAL.COMBINEDPREMIUM";
  public static final String COLUMN_DFCOMBINEDLDPREMIUM="COMBINEDPREMIUM";

  public static final String QUALIFIED_COLUMN_DFPMNTPLUSLIFEDISABILITY="DEAL.PMNTPLUSLIFEDISABILITY";
  public static final String COLUMN_DFPMNTPLUSLIFEDISABILITY="PMNTPLUSLIFEDISABILITY";

  public static final String QUALIFIED_COLUMN_DFINTERESTRATEINCREMENT="DEAL.INTERESTRATEINCREMENT";
  public static final String COLUMN_DFINTERESTRATEINCREMENT="INTERESTRATEINCREMENT";

  public static final String QUALIFIED_COLUMN_DFLIFEDISABILITYPREMIUMSID="LIFEDISABILITYPREMIUMS.LIFEDISABILITYPREMIUMSID";
  public static final String COLUMN_DFLIFEDISABILITYPREMIUMSID="LIFEDISABILITYPREMIUMSID";

  public static final String QUALIFIED_COLUMN_DFTOTALLOANAMOUNT="DEAL.TOTALLOANAMOUNT";
  public static final String COLUMN_DFTOTALLOANAMOUNT="TOTALLOANAMOUNT";

  public static final String QUALIFIED_COLUMN_DFLENDERPROFILEID="LENDERPROFILE.LENDERPROFILEID";
  public static final String COLUMN_DFLENDERPROFILEID="LENDERPROFILEID";

  public static final String QUALIFIED_COLUMN_COPYID="DEAL.COPYID";
  public static final String COLUMN_COPYID="COPYID";

  public static final String QUALIFIED_COLUMN_COPYTYPE="DEAL.COPYTYPE";
  public static final String COLUMN_COPYTYPE="COPYTYPE";

  public static final String QUALIFIED_COLUMN_DFLDRATECODE="LDINSURANCERATES.LDRATECODE";
  public static final String COLUMN_DFLDRATECODE="LDRATECODE";

  public static final String QUALIFIED_COLUMN_DFLDRATE="LDINSURANCERATES.LDRATE";
  public static final String COLUMN_DFLDRATE="LDRATE";

  public static final String QUALIFIED_COLUMN_DFLIFESTATUSDESC="LIFESTATUS.LIFESTATUSDESC";
  public static final String COLUMN_DFLIFESTATUSDESC="LIFESTATUSDESC";

  public static final String QUALIFIED_COLUMN_DFDISABILITYSTATUSDESC="DISABILITYSTATUS.DISABILITYSTATUSDESC";
  public static final String COLUMN_DFDISABILITYSTATUSDESC="DISABILITYSTATUSDESC";

  public static final String QUALIFIED_COLUMN_DFLIFESTATUSVALUE="LIFESTATUS.LIFESTATUSVALUE";
  public static final String COLUMN_DFLIFESTATUSVALUE="LIFESTATUSVALUE";

  public static final String QUALIFIED_COLUMN_DFLDISABILITYSTATUSVALUE="DISABILITYSTATUS.DISABILITYSTATUSVALUE";
  public static final String COLUMN_DFDISABILITYSTATUSVALUE="DISABILITYSTATUSVALUE";

  public static final String QUALIFIED_COLUMN_DFLDINSURANCEAGEDESC="LDINSURANCEAGERANGES.LDINSURANCEAGEDESC";
  public static final String COLUMN_DFLDINSURANCEAGEDESC="LDINSURANCEAGEDESC";

  public static final String QUALIFIED_COLUMN_DFBORROWERGENDERDESC="BORROWERGENDER.BORROWERGENDERDESC";
  public static final String COLUMN_DFBORROWERGENDERDESC="BORROWERGENDERDESC";

  public static final String QUALIFIED_COLUMN_DFLDINSURANCETYPEID="LDINSURANCETYPE.LDINSURANCETYPEID";
  public static final String COLUMN_DFLDINSURANCETYPEID="LDINSURANCETYPEID";

  public static final String QUALIFIED_COLUMN_DFLDINSURANCETYPEDESC="LDINSURANCETYPE.LDINSURANCETYPEDESC";
  public static final String COLUMN_DFLDINSURANCETYPEDESC="LDINSURANCETYPEDESC";

  public static final String QUALIFIED_COLUMN_DFIPDESC="INSURANCEPROPORTIONS.IPDESC";
  public static final String COLUMN_DFIPDESC="IPDESC";

  public static final String QUALIFIED_COLUMN_DFSMOKER="SMOKESTATUS.SMOKER";
  public static final String COLUMN_DFSMOKER="SMOKER";

  public static final String QUALIFIED_COLUMN_DFLIFESTATUSID="BORROWERGENDER.LIFESTATUSID";
  public static final String COLUMN_DFLIFESTATUSID="LIFESTATUSID";

  public static final String QUALIFIED_COLUMN_DFDISABILITYSTATUSID="BORROWER.DISABILITYSTATUSID";
  public static final String COLUMN_DFDISABILITYSTATUSID="DISABILITYSTATUSID";

  public static final String QUALIFIED_COLUMN_DFBORROWERSMOKERID="BORROWER.SMOKESTATUSID";
  public static final String COLUMN_DFBORROWERSMOKERID="SMOKESTATUSID";

  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////
  // Custom Members - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////

  static
  {
    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFDEALID,
        COLUMN_DFDEALID,
        QUALIFIED_COLUMN_DFDEALID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFCOPYID,
        COLUMN_DFCOPYID,
        QUALIFIED_COLUMN_DFCOPYID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFBORROWERID,
        COLUMN_DFBORROWERID,
        QUALIFIED_COLUMN_DFBORROWERID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFBORROWERFIRSTNAME,
        COLUMN_DFBORROWERFIRSTNAME,
        QUALIFIED_COLUMN_DFBORROWERFIRSTNAME,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFBORROWERMIDDLEINITIAL,
        COLUMN_DFBORROWERMIDDLEINITIAL,
        QUALIFIED_COLUMN_DFBORROWERMIDDLEINITIAL,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFBORROWERLASTNAME,
        COLUMN_DFBORROWERLASTNAME,
        QUALIFIED_COLUMN_DFBORROWERLASTNAME,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFPRIMARYBORROWERFLAG,
        COLUMN_DFPRIMARYBORROWERFLAG,
        QUALIFIED_COLUMN_DFPRIMARYBORROWERFLAG,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

      FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFBORROWERBIRTHDATE,
        COLUMN_BORROWERBIRTHDATE,
        QUALIFIED_COLUMN_BORROWERBIRTHDATE,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

      FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFAGE,
        COLUMN_AGE,
        QUALIFIED_COLUMN_AGE,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

      FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFBORROWERGENDERID,
        COLUMN_BORROWERGENDERID,
        QUALIFIED_COLUMN_BORROWERGENDERID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFLIFEPERCENTCOVERAGEREQ,
        COLUMN_DFLIFEPERCENTCOVERAGEREQ,
        QUALIFIED_COLUMN_DFLIFEPERCENTCOVERAGEREQ,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFLIFEPREMIUM,
        COLUMN_DFLIFEPREMIUM,
        QUALIFIED_COLUMN_DFLIFEPREMIUM,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

      FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFCOPYID,
        COLUMN_COPYID,
        QUALIFIED_COLUMN_COPYID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

      FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFCOPYTYPE,
        COLUMN_COPYTYPE,
        QUALIFIED_COLUMN_COPYTYPE,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFDISABILITYPREMIUM,
        COLUMN_DFDISABILITYPREMIUM,
        QUALIFIED_COLUMN_DFDISABILITYPREMIUM,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFCOMBINEDLDPREMIUM,
        COLUMN_DFCOMBINEDLDPREMIUM,
        QUALIFIED_COLUMN_DFCOMBINEDLDPREMIUM,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFPMNTPLUSLIFEDISABILITY,
        COLUMN_DFPMNTPLUSLIFEDISABILITY,
        QUALIFIED_COLUMN_DFPMNTPLUSLIFEDISABILITY,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFINTERESTRATEINCREMENT,
        COLUMN_DFINTERESTRATEINCREMENT,
        QUALIFIED_COLUMN_DFINTERESTRATEINCREMENT,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFLIFEDISABILITYPREMIUMSID,
        COLUMN_DFLIFEDISABILITYPREMIUMSID,
        QUALIFIED_COLUMN_DFLIFEDISABILITYPREMIUMSID,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFTOTALLOANAMOUNT,
        COLUMN_DFTOTALLOANAMOUNT,
        QUALIFIED_COLUMN_DFTOTALLOANAMOUNT,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFLENDERPROFILEID,
        COLUMN_DFLENDERPROFILEID,
        QUALIFIED_COLUMN_DFLENDERPROFILEID,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFLDRATECODE,
        COLUMN_DFLDRATECODE,
        QUALIFIED_COLUMN_DFLDRATECODE,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFLDRATE,
        COLUMN_DFLDRATE,
        QUALIFIED_COLUMN_DFLDRATE,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFLIFESTATUSDESC,
        COLUMN_DFLIFESTATUSDESC,
        QUALIFIED_COLUMN_DFLIFESTATUSDESC,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFDISABILITYSTATUSDESC,
        COLUMN_DFDISABILITYSTATUSDESC,
        QUALIFIED_COLUMN_DFDISABILITYSTATUSDESC,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFLIFESTATUSVALUE,
        COLUMN_DFLIFESTATUSVALUE,
        QUALIFIED_COLUMN_DFLIFESTATUSVALUE,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFLDISABILITYSTATUSVALUE,
        COLUMN_DFDISABILITYSTATUSVALUE,
        QUALIFIED_COLUMN_DFLDISABILITYSTATUSVALUE,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFLDINSURANCEAGEDESC,
        COLUMN_DFLDINSURANCEAGEDESC,
        QUALIFIED_COLUMN_DFLDINSURANCEAGEDESC,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFBORROWERGENDERDESC,
        COLUMN_DFBORROWERGENDERDESC,
        QUALIFIED_COLUMN_DFBORROWERGENDERDESC,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFLDINSURANCETYPEID,
        COLUMN_DFLDINSURANCETYPEID,
        QUALIFIED_COLUMN_DFLDINSURANCETYPEID,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFLDINSURANCETYPEDESC,
        COLUMN_DFLDINSURANCETYPEDESC,
        QUALIFIED_COLUMN_DFLDINSURANCETYPEDESC,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFIPDESC,
        COLUMN_DFIPDESC,
        QUALIFIED_COLUMN_DFIPDESC,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFSMOKER,
        COLUMN_DFSMOKER,
        QUALIFIED_COLUMN_DFSMOKER,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFLIFESTATUSID,
        COLUMN_DFLIFESTATUSID,
        QUALIFIED_COLUMN_DFLIFESTATUSID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFDISABILITYSTATUSID,
        COLUMN_DFDISABILITYSTATUSID,
        QUALIFIED_COLUMN_DFDISABILITYSTATUSID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFBORROWERSMOKERID,
        COLUMN_DFBORROWERSMOKERID,
        QUALIFIED_COLUMN_DFBORROWERSMOKERID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));
  }
}
