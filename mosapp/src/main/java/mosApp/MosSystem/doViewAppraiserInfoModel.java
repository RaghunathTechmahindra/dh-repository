package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doViewAppraiserInfoModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAppraiserContactId();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserContactId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAppraiserFirstName();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserFirstName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAppraiserInitial();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserInitial(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAppraiserLastName();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserLastName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAppraiserPhoneNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserPhoneNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAppraiserFaxNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserFaxNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAppraiserPhoneExtension();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserPhoneExtension(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAppraiserAddr1();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserAddr1(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAppraiserAddr2();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserAddr2(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAppraiserCity();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserCity(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAppraiserPostFSA();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserPostFSA(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAppraiserPostLDU();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserPostLDU(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAppraiserPartytypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserPartytypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);
	
    public java.math.BigDecimal getDfInstitutionId();
    public void setDfInstitutionId(java.math.BigDecimal id);  
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFAPPRAISERCONTACTID="dfAppraiserContactId";
	public static final String FIELD_DFAPPRAISERFIRSTNAME="dfAppraiserFirstName";
	public static final String FIELD_DFAPPRAISERINITIAL="dfAppraiserInitial";
	public static final String FIELD_DFAPPRAISERLASTNAME="dfAppraiserLastName";
	public static final String FIELD_DFAPPRAISERPHONENUMBER="dfAppraiserPhoneNumber";
	public static final String FIELD_DFAPPRAISERFAXNUMBER="dfAppraiserFaxNumber";
	public static final String FIELD_DFAPPRAISERPHONEEXTENSION="dfAppraiserPhoneExtension";
	public static final String FIELD_DFAPPRAISERADDR1="dfAppraiserAddr1";
	public static final String FIELD_DFAPPRAISERADDR2="dfAppraiserAddr2";
	public static final String FIELD_DFAPPRAISERCITY="dfAppraiserCity";
	public static final String FIELD_DFAPPRAISERPOSTFSA="dfAppraiserPostFSA";
	public static final String FIELD_DFAPPRAISERPOSTLDU="dfAppraiserPostLDU";
	public static final String FIELD_DFAPPRAISERPARTYTYPEID="dfAppraiserPartytypeId";
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFPROPERTYID="dfPropertyId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

