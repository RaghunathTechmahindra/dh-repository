package mosApp.MosSystem;

import java.sql.SQLException;


import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *  DJ_CR136.
 *  Insure ONLY type of Borrower implementation
 */
public class doInsureOnlyApplicantModelImpl extends QueryModelBase
  implements doInsureOnlyApplicantModel
{
  /**
   *
   *
   */
  public doInsureOnlyApplicantModelImpl()
  {
    super();
    setDataSourceName(DATA_SOURCE_NAME);

    setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

    setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

    setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

    setFieldSchema(FIELD_SCHEMA);

    initialize();

  }


  /**
   *
   *
   */
  public void initialize()
  {
  }


  /**
   *
   *
   */
  protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
    throws ModelControlException
  {
    logger = SysLog.getSysLogger("IOAP");
    logger.debug("VLAD ===> IOAP@SQLBeforeExecute: " + sql);

    return sql;

  }


  /**
   *
   *
   */
  protected void afterExecute(ModelExecutionContext context, int queryType)
    throws ModelControlException
  {

    logger = SysLog.getSysLogger("IOAP");
    logger.debug("IOAP@afterExecute::Size: " + this.getSize());
    //logger.debug("IOA@afterExecute::SQLTemplate: " + this.getSelectSQL());

    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
      getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();

    while(this.next())
    {
      //  Translate string content if necessary.
    }
  }


  /**
   *
   *
   */
  protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
  {
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfDealId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
  }


  /**
   *
   *
   */
  public void setDfDealId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFDEALID,value);
  }


  /**
   *
   *
   */
  public java.math.BigDecimal getDfCopyId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
  }


  /**
   *
   *
   */
  public void setDfCopyId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFCOPYID,value);
  }
  /**
   *
   *
   */
  public void setDfInsureOnlyApplicantId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFINSUREONLYAPPLICANTID,value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfLifePercentCoverageReq()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFLIFEPERCENTCOVERAGEREQ);
  }

  /**
   *
   *
   */
  public void setDfLifePercentCoverageReq(java.math.BigDecimal value)
  {
    setValue(FIELD_DFLIFEPERCENTCOVERAGEREQ,value);
  }

  /**
   *
   *
   */

  public java.math.BigDecimal getDfInsureOnlyApplicantId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFINSUREONLYAPPLICANTID);
  }

  /**
   *
   *
   */
  public String getDfFirstInsureOnlyApplicantName()
  {
    return (String)getValue(FIELD_DFINSUREONLYAPPLICANTFIRSTNAME);
  }

  /**
   *
   *
   */
  public void setDfInsureOnlyApplicantFirstName(String value)
  {
    setValue(FIELD_DFINSUREONLYAPPLICANTFIRSTNAME,value);
  }


  /**
   *
   *
   */
  public String getDfLastInsureOnlyApplicantName()
  {
    return (String)getValue(FIELD_DFINSUREONLYAPPLICANTLASTNAME);
  }

  /**
   *
   *
   */
  public void setDfInsureOnlyApplicantLastName(String value)
  {
    setValue(FIELD_DFINSUREONLYAPPLICANTLASTNAME,value);
  }

  /**
   *
   *
   */
  public void setDfCopyID(java.math.BigDecimal value)
  {
    setValue(FIELD_DFCOPYID,value);
  }

  /**
   *
   *
   */
  public void setDfInsureOnlyApplicantGenderId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFINSUREONLYAPPLICANTGENDERID,value);
  }

  /**
   *
   *
   */

  public java.math.BigDecimal getDfInsureOnlyApplicantGenderId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFINSUREONLYAPPLICANTGENDERID);
  }

  /**
   *
   *
   */
  public void setDfSmokerStatusId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFSMOKERSTATUSID,value);
  }

  /**
   *
   *
   */

  public java.math.BigDecimal getDfSmokerStatusId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFSMOKERSTATUSID);
  }

  /**
   *
   *
   */
  public String getDfLifeStatusDesc()
  {
    return (String)getValue(FIELD_DFLIFESTATUSDESC);
  }

  /**
   *
   *
   */
  public void setDfLifeStatusDesc(String value)
  {
    setValue(FIELD_DFLIFESTATUSDESC,value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfLifeStatusId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFLIFESTATUSID);
  }

  /**
   *
   *
   */
  public void setDfLifeStatusId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFLIFESTATUSID,value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfDisabilityStatusId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFDISABILITYSTATUSID);
  }

  /**
   *
   *
   */
  public void setDfDisabilityStatusId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFDISABILITYSTATUSID,value);
  }

  /**
   *
   *
   */
  public String getDfDisabilityStatusDesc()
  {
    return (String)getValue(FIELD_DFDISABILITYSTATUSDESC);
  }

  /**
   *
   *
   */
  public void setDfDisabilityStatusDesc(String value)
  {
    setValue(FIELD_DFDISABILITYSTATUSDESC,value);
  }

  /**
   *
   *
   */
  public void setDfLifeStatusValue(String value)
  {
    setValue(FIELD_DFLIFESTATUSVALUE,value);
  }

  /**
   *
   *
   */
  public String getDfLifeStatusValue()
  {
    return (String)getValue(FIELD_DFLIFESTATUSVALUE);
  }

  /**
   *
   *
   */
  public void setDfDisabilityStatusValue(String value)
  {
    setValue(FIELD_DFLDISABILITYSTATUSVALUE,value);
  }

  /**
   *
   *
   */
  public String getDfDisabilityStatusValue()
  {
    return (String)getValue(FIELD_DFLDISABILITYSTATUSVALUE);
  }

  /**
   *
   *
   */
  public String getDfInsureOnlyApplicantSmoker()
  {
    return (String)getValue(FIELD_DFSMOKER);
  }

  /**
   *
   *
   */
  public void setDfInsureOnlyApplicantSmoker(String value)
  {
    setValue(FIELD_DFSMOKER,value);
  }

  /**
   *
   *
   */
  public String getDfPrimaryInsureOnlyApplicantFlag()
  {
    return (String)getValue(FIELD_DFPRIMARYINSUREONLYAPPLICANTFLAG);
  }

  /**
   *
   *
   */
  public void setDfPrimaryInsureOnlyApplicantFlag(String value)
  {
    setValue(FIELD_DFPRIMARYINSUREONLYAPPLICANTFLAG,value);
  }

  /**
   *
   *
   */
  public void setDfAge(java.math.BigDecimal value)
  {
    setValue(FIELD_DFAGE,value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfAge()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFAGE);
  }


  /**
   *
   *
   */
  public java.sql.Timestamp getDfInsureOnlyApplicantBirthDate()
  {
    return (java.sql.Timestamp)getValue(FIELD_DFINSUREONLYAPPLICANTBIRTHDATE);
  }

  /**
   *
   *
   */
  public void setDfInsureOnlyApplicantBirthDate(java.sql.Timestamp value)
  {
    setValue(FIELD_DFINSUREONLYAPPLICANTBIRTHDATE,value);
  }

  /**
   * MetaData object test method to verify the SYNTHETIC new
   * field for the reogranized SQL_TEMPLATE query.
   *
   */
  /**
  public void setResultSet(ResultSet value)
  {
      logger = SysLog.getSysLogger("METADATA");

      try
      {
         super.setResultSet(value);
         if( value != null)
         {
             ResultSetMetaData metaData = value.getMetaData();
             int columnCount = metaData.getColumnCount();
             logger.debug("===============================================");
                        logger.debug("Testing MetaData Object for new synthetic fields for" +
                               " doDealEntrySourceInfoModel");
             logger.debug("NumberColumns: " + columnCount);
             for(int i = 0; i < columnCount ; i++)
              {
                   logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
              }
              logger.debug("================================================");
          }
      }
      catch (SQLException e)
      {
            logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
      }
   }
  **/

  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Custom Methods - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  public SysLogger logger;

  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////

  public static final String DATA_SOURCE_NAME="jdbc/orcl";

  public static final String SELECT_SQL_TEMPLATE=
  "SELECT ALL INSUREONLYAPPLICANT.INSUREONLYAPPLICANTID, INSUREONLYAPPLICANT.INSUREONLYAPPLICANTBIRTHDATE, " +
  "INSUREONLYAPPLICANT.INSUREONLYAPPLICANTLASTNAME, INSUREONLYAPPLICANT.INSUREONLYAPPLICANTFIRSTNAME, " +
  "INSUREONLYAPPLICANT.LIFEPERCENTCOVERAGEREQ, " +
  "INSUREONLYAPPLICANT.PRIMARYINSUREONLYAPPLICANTFLAG, INSUREONLYAPPLICANT.INSUREONLYAPPLICANTGENDERID, " +
  "INSUREONLYAPPLICANT.LIFESTATUSID, INSUREONLYAPPLICANT.DISABILITYSTATUSID, " +
  "INSUREONLYAPPLICANT.SMOKESTATUSID, LIFEDISPREMIUMSIONLYA.LIFEDISPREMIUMSIONLYAID, " +
  "DEAL.DEALID, DEAL.COPYID, LIFESTATUS.LIFESTATUSVALUE, " +
  "DISABILITYSTATUS.DISABILITYSTATUSVALUE, LIFESTATUS.LIFESTATUSDESC, " +
  "DISABILITYSTATUS.DISABILITYSTATUSDESC, BORROWERGENDER.BORROWERGENDERDESC, " +
  "LDINSURANCETYPE.LDINSURANCETYPEID, LDINSURANCETYPE.LDINSURANCETYPEDESC, " +
  "SMOKESTATUS.SMOKER, INSUREONLYAPPLICANT.INSUREONLYAPPLICANTAGE " +
  "FROM INSUREONLYAPPLICANT, LIFEDISPREMIUMSIONLYA, " +
  "DEAL, LENDERPROFILE, LDINSURANCERATES, LIFESTATUS, " +
  "DISABILITYSTATUS, LDINSURANCEAGERANGES, BORROWERGENDER, " +
  "LDINSURANCETYPE, INSURANCEPROPORTIONS, SMOKESTATUS __WHERE__ ORDER BY INSUREONLYAPPLICANT.INSUREONLYAPPLICANTID  ASC ";

  public static final String MODIFYING_QUERY_TABLE_NAME=
  "INSUREONLYAPPLICANT, LIFEDISPREMIUMSIONLYA, " +
  "DEAL, LENDERPROFILE, LDINSURANCERATES, LIFESTATUS, " +
  "DISABILITYSTATUS, LDINSURANCEAGERANGES, BORROWERGENDER, " +
  "LDINSURANCETYPE, INSURANCEPROPORTIONS, SMOKESTATUS ";

  public static final String STATIC_WHERE_CRITERIA=
  "(INSUREONLYAPPLICANT.COPYID  =  LIFEDISPREMIUMSIONLYA.COPYID) " +
  "AND (INSUREONLYAPPLICANT.INSUREONLYAPPLICANTID = LIFEDISPREMIUMSIONLYA.INSUREONLYAPPLICANTID) " +
  "AND (DEAL.COPYID  =  INSUREONLYAPPLICANT.COPYID) " +
  "AND (DEAL.DEALID = INSUREONLYAPPLICANT.DEALID) " +
  "AND (LENDERPROFILE.LENDERPROFILEID = LIFEDISPREMIUMSIONLYA.LENDERPROFILEID) " +
  "AND (LIFEDISPREMIUMSIONLYA.LDINSURANCERATEID = LDINSURANCERATES.LDINSURANCERATEID) " +
  "AND (LDINSURANCEAGERANGES.LDINSURANCEAGEID = LDINSURANCERATES.LDINSURANCEAGEID) " +
  "AND (LDINSURANCEAGERANGES.BORROWERGENDERID = BORROWERGENDER.BORROWERGENDERID) " +
  "AND (LDINSURANCERATES.LDINSURANCETYPEID = LDINSURANCETYPE.LDINSURANCETYPEID) " +
  "AND (INSUREONLYAPPLICANT.INSURANCEPROPORTIONSID = INSURANCEPROPORTIONS.INSURANCEPROPORTIONSID) " +
  "AND (INSUREONLYAPPLICANT.LIFESTATUSID = LIFESTATUS.LIFESTATUSID) " +
  "AND (INSUREONLYAPPLICANT.DISABILITYSTATUSID = DISABILITYSTATUS.DISABILITYSTATUSID) " +
  "AND (INSUREONLYAPPLICANT.SMOKESTATUSID = SMOKESTATUS.SMOKESTATUSID) ";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();

  public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
  public static final String COLUMN_DFDEALID="DEALID";

  public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
  public static final String COLUMN_DFCOPYID="COPYID";

  public static final String QUALIFIED_COLUMN_DFINSUREONLYAPPLICANTID="INSUREONLYAPPLICANT.INSUREONLYAPPLICANTID";
  public static final String COLUMN_DFINSUREONLYAPPLICANTID="INSUREONLYAPPLICANTID";

  public static final String QUALIFIED_COLUMN_DFINSUREONLYAPPLICANTFIRSTNAME="INSUREONLYAPPLICANT.INSUREONLYAPPLICANTFIRSTNAME";
  public static final String COLUMN_DFINSUREONLYAPPLICANTFIRSTNAME="INSUREONLYAPPLICANTFIRSTNAME";

  public static final String QUALIFIED_COLUMN_DFINSUREONLYAPPLICANTLASTNAME="INSUREONLYAPPLICANT.INSUREONLYAPPLICANTLASTNAME";
  public static final String COLUMN_DFINSUREONLYAPPLICANTLASTNAME="INSUREONLYAPPLICANTLASTNAME";

  public static final String QUALIFIED_COLUMN_DFPRIMARYINSUREONLYAPPLICANTFLAG="INSUREONLYAPPLICANT.PRIMARYINSUREONLYAPPLICANTFLAG";
  public static final String COLUMN_DFPRIMARYINSUREONLYAPPLICANTFLAG="PRIMARYINSUREONLYAPPLICANTFLAG";

  public static final String QUALIFIED_COLUMN_DFLIFEPERCENTCOVERAGEREQ="INSUREONLYAPPLICANT.LIFEPERCENTCOVERAGEREQ";
  public static final String COLUMN_DFLIFEPERCENTCOVERAGEREQ="LIFEPERCENTCOVERAGEREQ";

  public static final String QUALIFIED_COLUMN_INSUREONLYAPPLICANTBIRTHDATE="INSUREONLYAPPLICANT.INSUREONLYAPPLICANTBIRTHDATE";
  public static final String COLUMN_INSUREONLYAPPLICANTBIRTHDATE="INSUREONLYAPPLICANTBIRTHDATE";

  public static final String QUALIFIED_COLUMN_AGE="INSUREONLYAPPLICANT.INSUREONLYAPPLICANTAGE";
  public static final String COLUMN_AGE="INSUREONLYAPPLICANTAGE";

  public static final String QUALIFIED_COLUMN_INSUREONLYAPPLICANTGENDERID="INSUREONLYAPPLICANT.INSUREONLYAPPLICANTGENDERID";
  public static final String COLUMN_INSUREONLYAPPLICANTGENDERID="INSUREONLYAPPLICANTGENDERID";

  public static final String QUALIFIED_COLUMN_DFLIFESTATUSDESC="LIFESTATUS.LIFESTATUSDESC";
  public static final String COLUMN_DFLIFESTATUSDESC="LIFESTATUSDESC";

  public static final String QUALIFIED_COLUMN_DFDISABILITYSTATUSDESC="DISABILITYSTATUS.DISABILITYSTATUSDESC";
  public static final String COLUMN_DFDISABILITYSTATUSDESC="DISABILITYSTATUSDESC";

  public static final String QUALIFIED_COLUMN_DFLIFESTATUSVALUE="LIFESTATUS.LIFESTATUSVALUE";
  public static final String COLUMN_DFLIFESTATUSVALUE="LIFESTATUSVALUE";

  public static final String QUALIFIED_COLUMN_DFLDISABILITYSTATUSVALUE="DISABILITYSTATUS.DISABILITYSTATUSVALUE";
  public static final String COLUMN_DFDISABILITYSTATUSVALUE="DISABILITYSTATUSVALUE";

  public static final String QUALIFIED_COLUMN_DFLDINSURANCETYPEID="LDINSURANCETYPE.LDINSURANCETYPEID";
  public static final String COLUMN_DFLDINSURANCETYPEID="LDINSURANCETYPEID";

  public static final String QUALIFIED_COLUMN_DFSMOKER="SMOKESTATUS.SMOKER";
  public static final String COLUMN_DFSMOKER="SMOKER";

  public static final String QUALIFIED_COLUMN_DFLIFESTATUSID="INSUREONLYAPPLICANT.LIFESTATUSID";
  public static final String COLUMN_DFLIFESTATUSID="LIFESTATUSID";

  public static final String QUALIFIED_COLUMN_DFDISABILITYSTATUSID="INSUREONLYAPPLICANT.DISABILITYSTATUSID";
  public static final String COLUMN_DFDISABILITYSTATUSID="DISABILITYSTATUSID";

  public static final String QUALIFIED_COLUMN_DFSMOKERSTATUSID="INSUREONLYAPPLICANT.SMOKESTATUSID";
  public static final String COLUMN_DFSMOKERSTATUSID="SMOKESTATUSID";

  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////
  // Custom Members - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////

  static
  {
    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFDEALID,
        COLUMN_DFDEALID,
        QUALIFIED_COLUMN_DFDEALID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFCOPYID,
        COLUMN_DFCOPYID,
        QUALIFIED_COLUMN_DFCOPYID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFINSUREONLYAPPLICANTID,
        COLUMN_DFINSUREONLYAPPLICANTID,
        QUALIFIED_COLUMN_DFINSUREONLYAPPLICANTID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFINSUREONLYAPPLICANTFIRSTNAME,
        COLUMN_DFINSUREONLYAPPLICANTFIRSTNAME,
        QUALIFIED_COLUMN_DFINSUREONLYAPPLICANTFIRSTNAME,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFINSUREONLYAPPLICANTLASTNAME,
        COLUMN_DFINSUREONLYAPPLICANTLASTNAME,
        QUALIFIED_COLUMN_DFINSUREONLYAPPLICANTLASTNAME,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFPRIMARYINSUREONLYAPPLICANTFLAG,
        COLUMN_DFPRIMARYINSUREONLYAPPLICANTFLAG,
        QUALIFIED_COLUMN_DFPRIMARYINSUREONLYAPPLICANTFLAG,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

      FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFINSUREONLYAPPLICANTBIRTHDATE,
        COLUMN_INSUREONLYAPPLICANTBIRTHDATE,
        QUALIFIED_COLUMN_INSUREONLYAPPLICANTBIRTHDATE,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

      FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
          FIELD_DFLIFEPERCENTCOVERAGEREQ,
          COLUMN_DFLIFEPERCENTCOVERAGEREQ,
          QUALIFIED_COLUMN_DFLIFEPERCENTCOVERAGEREQ,
          java.math.BigDecimal.class,
          false,
          false,
          QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
          "",
          QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
          ""));

      FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFAGE,
        COLUMN_AGE,
        QUALIFIED_COLUMN_AGE,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

      FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFINSUREONLYAPPLICANTGENDERID,
        COLUMN_INSUREONLYAPPLICANTGENDERID,
        QUALIFIED_COLUMN_INSUREONLYAPPLICANTGENDERID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFLIFESTATUSDESC,
        COLUMN_DFLIFESTATUSDESC,
        QUALIFIED_COLUMN_DFLIFESTATUSDESC,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFDISABILITYSTATUSDESC,
        COLUMN_DFDISABILITYSTATUSDESC,
        QUALIFIED_COLUMN_DFDISABILITYSTATUSDESC,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFLIFESTATUSVALUE,
        COLUMN_DFLIFESTATUSVALUE,
        QUALIFIED_COLUMN_DFLIFESTATUSVALUE,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFLDISABILITYSTATUSVALUE,
        COLUMN_DFDISABILITYSTATUSVALUE,
        QUALIFIED_COLUMN_DFLDISABILITYSTATUSVALUE,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFSMOKER,
        COLUMN_DFSMOKER,
        QUALIFIED_COLUMN_DFSMOKER,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFLIFESTATUSID,
        COLUMN_DFLIFESTATUSID,
        QUALIFIED_COLUMN_DFLIFESTATUSID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFDISABILITYSTATUSID,
        COLUMN_DFDISABILITYSTATUSID,
        QUALIFIED_COLUMN_DFDISABILITYSTATUSID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFSMOKERSTATUSID,
        COLUMN_DFSMOKERSTATUSID,
        QUALIFIED_COLUMN_DFSMOKERSTATUSID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));
  }
}
