package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.basis100.picklist.BXResources;

import java.text.*;

/**
 *
 *
 *
 */
public class doGetLivingSpaceUnitOfMeasureModelImpl extends QueryModelBase
	implements doGetLivingSpaceUnitOfMeasureModel
{
	/**
	 *
	 *
	 */
	public doGetLivingSpaceUnitOfMeasureModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 19Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
	int institutionId = theSessionState.getDealInstitutionId();
    while(this.next())
    {
      //Convert LIVINGSPACEUNITOFMEASURE Desc.
      this.setDfLivingSpaceUnitMeasureDesc(BXResources.getPickListDescription(
          institutionId, "LIVINGSPACEUNITOFMEASURE", this.getDfLivingSpaceUnitOfMeasureId().intValue(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLivingSpaceUnitOfMeasureId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIVINGSPACEUNITOFMEASUREID);
	}


	/**
	 *
	 *
	 */
	public void setDfLivingSpaceUnitOfMeasureId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIVINGSPACEUNITOFMEASUREID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLivingSpaceUnitMeasureDesc()
	{
		return (String)getValue(FIELD_DFLIVINGSPACEUNITMEASUREDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfLivingSpaceUnitMeasureDesc(String value)
	{
		setValue(FIELD_DFLIVINGSPACEUNITMEASUREDESC,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  //--Release2.1--//
  //Not need to modify the SQL and the Multilingual Desc. is populated in afterExecute() method
  //--> By Billy 19Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL LIVINGSPACEUNITOFMEASURE.LIVINGSPACEUNITOFMEASUREID, LIVINGSPACEUNITOFMEASURE.LSUDESCRIPTION FROM LIVINGSPACEUNITOFMEASURE  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="LIVINGSPACEUNITOFMEASURE";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFLIVINGSPACEUNITOFMEASUREID="LIVINGSPACEUNITOFMEASURE.LIVINGSPACEUNITOFMEASUREID";
	public static final String COLUMN_DFLIVINGSPACEUNITOFMEASUREID="LIVINGSPACEUNITOFMEASUREID";
	public static final String QUALIFIED_COLUMN_DFLIVINGSPACEUNITMEASUREDESC="LIVINGSPACEUNITOFMEASURE.LSUDESCRIPTION";
	public static final String COLUMN_DFLIVINGSPACEUNITMEASUREDESC="LSUDESCRIPTION";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIVINGSPACEUNITOFMEASUREID,
				COLUMN_DFLIVINGSPACEUNITOFMEASUREID,
				QUALIFIED_COLUMN_DFLIVINGSPACEUNITOFMEASUREID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIVINGSPACEUNITMEASUREDESC,
				COLUMN_DFLIVINGSPACEUNITMEASUREDESC,
				QUALIFIED_COLUMN_DFLIVINGSPACEUNITMEASUREDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

