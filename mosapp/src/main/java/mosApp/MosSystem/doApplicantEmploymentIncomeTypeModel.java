package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doApplicantEmploymentIncomeTypeModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getMOS_INCOMETYPE_INCOMETYPEID();

	
	/**
	 * 
	 * 
	 */
	public void setMOS_INCOMETYPE_INCOMETYPEID(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getMOS_INCOMETYPE_ITDESCRIPTION();

	
	/**
	 * 
	 * 
	 */
	public void setMOS_INCOMETYPE_ITDESCRIPTION(String value);

	
	/**
	 * 
	 * 
	 */
	public String getMOS_INCOMETYPE_EMPLOYMENTRELATED();

	
	/**
	 * 
	 * 
	 */
	public void setMOS_INCOMETYPE_EMPLOYMENTRELATED(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getMOS_INCOMETYPE_GDSINCLUSION();

	
	/**
	 * 
	 * 
	 */
	public void setMOS_INCOMETYPE_GDSINCLUSION(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getMOS_INCOMETYPE_TDSINCLUSION();

	
	/**
	 * 
	 * 
	 */
	public void setMOS_INCOMETYPE_TDSINCLUSION(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_MOS_INCOMETYPE_INCOMETYPEID="MOS_INCOMETYPE_INCOMETYPEID";
	public static final String FIELD_MOS_INCOMETYPE_ITDESCRIPTION="MOS_INCOMETYPE_ITDESCRIPTION";
	public static final String FIELD_MOS_INCOMETYPE_EMPLOYMENTRELATED="MOS_INCOMETYPE_EMPLOYMENTRELATED";
	public static final String FIELD_MOS_INCOMETYPE_GDSINCLUSION="MOS_INCOMETYPE_GDSINCLUSION";
	public static final String FIELD_MOS_INCOMETYPE_TDSINCLUSION="MOS_INCOMETYPE_TDSINCLUSION";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

