package mosApp.MosSystem;

/**
 * 14/Mar/2007 DVG #DG586 FXP16091  Hide UserType Options from Sys Admins 
 */

import java.sql.SQLException;

import MosSystem.Mc;

import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doUserAdminGetJointAppModelImpl extends QueryModelBase
	implements doUserAdminGetJointAppModel
{
	/**
	 *
	 *
	 */
	public doUserAdminGetJointAppModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfUserProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFUSERPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfUserProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFUSERPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfUserLogin()
	{
		return (String)getValue(FIELD_DFUSERLOGIN);
	}


	/**
	 *
	 *
	 */
	public void setDfUserLogin(String value)
	{
		setValue(FIELD_DFUSERLOGIN,value);
	}


	/**
	 *
	 *
	 */
	public String getDfContactFirstName()
	{
		return (String)getValue(FIELD_DFCONTACTFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfContactFirstName(String value)
	{
		setValue(FIELD_DFCONTACTFIRSTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfContactLastName()
	{
		return (String)getValue(FIELD_DFCONTACTLASTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfContactLastName(String value)
	{
		setValue(FIELD_DFCONTACTLASTNAME,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL USERPROFILE.USERPROFILEID, USERPROFILE.USERLOGIN, "+
    "CONTACT.CONTACTFIRSTNAME, CONTACT.CONTACTLASTNAME "+
    "FROM CONTACT, USERPROFILE  "+
    "__WHERE__  "+
    "ORDER BY CONTACT.CONTACTFIRSTNAME  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME="CONTACT, USERPROFILE";
	public static final String STATIC_WHERE_CRITERIA=
    " (((CONTACT.CONTACTID  =  USERPROFILE.CONTACTID)) AND "+
    /* #DG586 rewritten    
    "USERPROFILE.USERTYPEID <> 0 AND "+
    "USERPROFILE.USERTYPEID <> 1 AND "+
    "USERPROFILE.USERTYPEID <> 2 AND "+
    "USERPROFILE.USERTYPEID <> 3 AND "+
    "USERPROFILE.USERTYPEID <> 4 AND "+
    "USERPROFILE.USERTYPEID <> 5 AND "+
    "USERPROFILE.USERTYPEID <> 13) ";
	  */
    " (not USERPROFILE.USERTYPEID in (0,1,2,3,4,5,13)) "    
    +" and USERPROFILE.USERTYPEID < "+Mc.USER_TYPE_SPECIAL +')';		//#DG586 hide special users	
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFUSERPROFILEID="USERPROFILE.USERPROFILEID";
	public static final String COLUMN_DFUSERPROFILEID="USERPROFILEID";
	public static final String QUALIFIED_COLUMN_DFUSERLOGIN="USERPROFILE.USERLOGIN";
	public static final String COLUMN_DFUSERLOGIN="USERLOGIN";
	public static final String QUALIFIED_COLUMN_DFCONTACTFIRSTNAME="CONTACT.CONTACTFIRSTNAME";
	public static final String COLUMN_DFCONTACTFIRSTNAME="CONTACTFIRSTNAME";
	public static final String QUALIFIED_COLUMN_DFCONTACTLASTNAME="CONTACT.CONTACTLASTNAME";
	public static final String COLUMN_DFCONTACTLASTNAME="CONTACTLASTNAME";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERPROFILEID,
				COLUMN_DFUSERPROFILEID,
				QUALIFIED_COLUMN_DFUSERPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERLOGIN,
				COLUMN_DFUSERLOGIN,
				QUALIFIED_COLUMN_DFUSERLOGIN,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTACTFIRSTNAME,
				COLUMN_DFCONTACTFIRSTNAME,
				QUALIFIED_COLUMN_DFCONTACTFIRSTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTACTLASTNAME,
				COLUMN_DFCONTACTLASTNAME,
				QUALIFIED_COLUMN_DFCONTACTLASTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

