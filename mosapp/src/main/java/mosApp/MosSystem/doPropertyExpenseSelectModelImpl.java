package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doPropertyExpenseSelectModelImpl extends QueryModelBase
	implements doPropertyExpenseSelectModel
{
	/**
	 *
	 *
	 */
	public doPropertyExpenseSelectModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyExpenseId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYEXPENSEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyExpenseId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYEXPENSEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyExpenseTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYEXPENSETYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyExpenseTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYEXPENSETYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyExpenseDesc()
	{
		return (String)getValue(FIELD_DFPROPERTYEXPENSEDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyExpenseDesc(String value)
	{
		setValue(FIELD_DFPROPERTYEXPENSEDESC,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyExpensePeriodId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYEXPENSEPERIODID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyExpensePeriodId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYEXPENSEPERIODID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyExpenseAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYEXPENSEAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyExpenseAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYEXPENSEAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyExpenseIncludeInGDS()
	{
		return (String)getValue(FIELD_DFPROPERTYEXPENSEINCLUDEINGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyExpenseIncludeInGDS(String value)
	{
		setValue(FIELD_DFPROPERTYEXPENSEINCLUDEINGDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyExpensePercentageIncludedGDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyExpensePercentageIncludedGDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDGDS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyExpenseIncludeTDS()
	{
		return (String)getValue(FIELD_DFPROPERTYEXPENSEINCLUDETDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyExpenseIncludeTDS(String value)
	{
		setValue(FIELD_DFPROPERTYEXPENSEINCLUDETDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyExpensePercentageIncludedTDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyExpensePercentageIncludedTDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDTDS,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE
        ="SELECT distinct PROPERTYEXPENSE.PROPERTYEXPENSEID, " +
                "PROPERTYEXPENSE.PROPERTYID, PROPERTYEXPENSE.COPYID, " +
                "PROPERTYEXPENSE.PROPERTYEXPENSETYPEID, " +
                "PROPERTYEXPENSE.PROPERTYEXPENSEDESCRIPTION, " +
                "PROPERTYEXPENSE.PROPERTYEXPENSEPERIODID, " +
                "PROPERTYEXPENSE.PROPERTYEXPENSEAMOUNT, " +
                "PROPERTYEXPENSE.PEINCLUDEINGDS, PROPERTYEXPENSE.PEPERCENTINGDS, " +
                "PROPERTYEXPENSE.PEINCLUDEINTDS, PROPERTYEXPENSE.PEPERCENTINTDS " +
                "FROM PROPERTYEXPENSE  __WHERE__  " +
                "ORDER BY PROPERTYEXPENSE.PROPERTYEXPENSEID  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME="PROPERTYEXPENSE";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSEID="PROPERTYEXPENSE.PROPERTYEXPENSEID";
	public static final String COLUMN_DFPROPERTYEXPENSEID="PROPERTYEXPENSEID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYID="PROPERTYEXPENSE.PROPERTYID";
	public static final String COLUMN_DFPROPERTYID="PROPERTYID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="PROPERTYEXPENSE.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSETYPEID="PROPERTYEXPENSE.PROPERTYEXPENSETYPEID";
	public static final String COLUMN_DFPROPERTYEXPENSETYPEID="PROPERTYEXPENSETYPEID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSEDESC="PROPERTYEXPENSE.PROPERTYEXPENSEDESCRIPTION";
	public static final String COLUMN_DFPROPERTYEXPENSEDESC="PROPERTYEXPENSEDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSEPERIODID="PROPERTYEXPENSE.PROPERTYEXPENSEPERIODID";
	public static final String COLUMN_DFPROPERTYEXPENSEPERIODID="PROPERTYEXPENSEPERIODID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSEAMOUNT="PROPERTYEXPENSE.PROPERTYEXPENSEAMOUNT";
	public static final String COLUMN_DFPROPERTYEXPENSEAMOUNT="PROPERTYEXPENSEAMOUNT";
	public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSEINCLUDEINGDS="PROPERTYEXPENSE.PEINCLUDEINGDS";
	public static final String COLUMN_DFPROPERTYEXPENSEINCLUDEINGDS="PEINCLUDEINGDS";
	public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDGDS="PROPERTYEXPENSE.PEPERCENTINGDS";
	public static final String COLUMN_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDGDS="PEPERCENTINGDS";
	public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSEINCLUDETDS="PROPERTYEXPENSE.PEINCLUDEINTDS";
	public static final String COLUMN_DFPROPERTYEXPENSEINCLUDETDS="PEINCLUDEINTDS";
	public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDTDS="PROPERTYEXPENSE.PEPERCENTINTDS";
	public static final String COLUMN_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDTDS="PEPERCENTINTDS";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYEXPENSEID,
				COLUMN_DFPROPERTYEXPENSEID,
				QUALIFIED_COLUMN_DFPROPERTYEXPENSEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYID,
				COLUMN_DFPROPERTYID,
				QUALIFIED_COLUMN_DFPROPERTYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYEXPENSETYPEID,
				COLUMN_DFPROPERTYEXPENSETYPEID,
				QUALIFIED_COLUMN_DFPROPERTYEXPENSETYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYEXPENSEDESC,
				COLUMN_DFPROPERTYEXPENSEDESC,
				QUALIFIED_COLUMN_DFPROPERTYEXPENSEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYEXPENSEPERIODID,
				COLUMN_DFPROPERTYEXPENSEPERIODID,
				QUALIFIED_COLUMN_DFPROPERTYEXPENSEPERIODID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYEXPENSEAMOUNT,
				COLUMN_DFPROPERTYEXPENSEAMOUNT,
				QUALIFIED_COLUMN_DFPROPERTYEXPENSEAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYEXPENSEINCLUDEINGDS,
				COLUMN_DFPROPERTYEXPENSEINCLUDEINGDS,
				QUALIFIED_COLUMN_DFPROPERTYEXPENSEINCLUDEINGDS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDGDS,
				COLUMN_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDGDS,
				QUALIFIED_COLUMN_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDGDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYEXPENSEINCLUDETDS,
				COLUMN_DFPROPERTYEXPENSEINCLUDETDS,
				QUALIFIED_COLUMN_DFPROPERTYEXPENSEINCLUDETDS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDTDS,
				COLUMN_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDTDS,
				QUALIFIED_COLUMN_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDTDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

