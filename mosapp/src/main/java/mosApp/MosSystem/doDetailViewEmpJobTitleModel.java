package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doDetailViewEmpJobTitleModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfJobTitleId();

	
	/**
	 * 
	 * 
	 */
	public void setDfJobTitleId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfJobTitleDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfJobTitleDesc(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFJOBTITLEID="dfJobTitleId";
	public static final String FIELD_DFJOBTITLEDESC="dfJobTitleDesc";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

