package mosApp.MosSystem;

import static mosApp.MosSystem.doUnderwriter1Model.FIELD_DFUSERPROFILEID;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_CBDEALSTATUSFILTER;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_CBSORTOPTIONS;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_CBSTATUSFILTER;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_CBTASKEXPIRYDATEENDFILTER;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_CBTASKEXPIRYDATEENDFILTER_RESET_VALUE;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_CBTASKEXPIRYDATESTARTFILTER;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_CBTASKEXPIRYDATESTARTFILTER_RESET_VALUE;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_CBTASKNAMEFILTER;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_CBUSERFILTER;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_CHISDISPLAYHIDDEN;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_HDCURRENTFIRSTROWID;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_HDSELECTEDROWID;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_LBMWQBRANCHES;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_LBMWQGROUPS;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_MULTIACCESS;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_REPEATED1;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_ROWSDISPLAYED;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_STLOCATIONFULL;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_STSORTOPTION;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_STSTATUSFILTER;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_STTASKNAME;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_STTASKNAMEFULL;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_STUSERFILTER;
import static mosApp.MosSystem.pgMWorkQueueSearchViewBean.CHILD_TBDEALIDFILTER;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import mosApp.MosSystem.models.MasterWorkQueueSearchCountModel;
import mosApp.MosSystem.models.MasterWorkQueueSearchCountModelImpl;
import mosApp.MosSystem.models.MasterWorkQueueSearchModel;
import mosApp.MosSystem.models.MasterWorkQueueSearchModelImpl;
import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.entity.BranchProfile;
import com.basis100.deal.entity.GroupProfile;
import com.basis100.deal.entity.RegionProfile;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.ListBox;
import com.iplanet.jato.view.html.Option;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.TextField;

public class MasterWorkQueueSearchHandler extends PageHandlerCommon implements Cloneable, Sc {

	private static final SysLogger logger = SysLog.getSysLogger("MWQSH");
	static final String PAGE_STATE_TABLE_KEY_MWQS_CRITERIA = "MWQSCriteria";
	private static final String PAGE_STATE_TABLE_KEY_MWQS_REFRESH = "MWQSRefresh";
	private static final String PAGE_STATE_TABLE_KEY_MWQS_PCINAME = "MWQSDefault";
	private static final String PAGE_STATE_TABLE_KEY_MWQS_BY_BRANCHES = "MWQSByBranches";
	private static final String PAGE_STATE_TABLE_KEY_MWQS_BY_GROUPS = "MWQSByGroups";
	private static final String PAGE_STATE_TABLE_KEY_MWQS_BY_LOCATIONS = "MWQSByLocations";
	private static final String PAGE_STATE_TABLE_KEY_MWQS_TOGGLE_VISIBILITY = "MWQSToggleVisibility";
	private static final String PAGE_STATE_TABLE_KEY_MWQS_BY_EXPIRY_DATE_START = "MWQSByExpiryDateStart";
	private static final String PAGE_STATE_TABLE_KEY_MWQS_BY_EXPIRY_DATE_END = "MWQSByExpiryDateEnd";
	static final String PAGE_STATE_TABLE_KEY_MWQS_REUSE = "MWQSReuseCriteria";
	static final String PAGE_STATE_TABLE_KEY_MWQS_DISPLAY_ROWS = "MWQSDisplayRows";

	static final int REALM_TYPE_ANY = 0;
	static final int REALM_TYPE_REGION = 1;
	static final int REALM_TYPE_BRANCH = 2;
	static final int REALM_TYPE_GROUP = 3;
	static final int TRANCATED_LOCATION_LENGTH = 13;
	static final int TRANCATED_TASKNAME_LENGTH = 9;

	SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

	public MasterWorkQueueSearchHandler cloneSS() {
		return (MasterWorkQueueSearchHandler) super.cloneSafeShallow();
	}

	public void preHandlerProtocol(ViewBean currNDPage) {
		super.preHandlerProtocol(currNDPage);
		PageEntry pg = theSessionState.getCurrentPage();
		pg.setPageDisplayMethod(Mc.DISPLAY_METHOD_DISP);
	}

	public void setupBeforePageGeneration() {
		PageEntry pg = theSessionState.getCurrentPage();
		if (pg.getSetupBeforeGenerationCalled() == true)
			return;
		pg.setSetupBeforeGenerationCalled(true);
		PageCursorInfo tds = getPageCursorInfo(pg, PAGE_STATE_TABLE_KEY_MWQS_PCINAME);
		Hashtable pst = pg.getPageStateTable();

		boolean refresh = false;
		Boolean theRefreshFlag = (Boolean) pst.get(PAGE_STATE_TABLE_KEY_MWQS_REFRESH);
		if (theRefreshFlag != null)
			refresh = theRefreshFlag.booleanValue();

		//// Set all parameters is tds is null.
		if ((tds == null) || (refresh == true)) {
			//// Reset Refresh flag from PST first, new feature.
			pst.put(PAGE_STATE_TABLE_KEY_MWQS_REFRESH, false);
			// determine number of task (shown) per display page
			int tasksPerPage = ((TiledView) (getCurrNDPage().getChild(CHILD_REPEATED1))).getMaxDisplayTiles();
			// set up and save task display state object
			tds = new PageCursorInfo(PAGE_STATE_TABLE_KEY_MWQS_PCINAME, CHILD_REPEATED1, tasksPerPage, 0);
			// last (0) - don't know total number yet!
			tds.setRefresh(true);
			tds.setSortOption(Mc.MWQ_SORT_DUE_DATE);
			pg.setPageCriteriaId2(0);
			pg.setPageCriteriaId1(-1);
			pg.setPageCriteriaId3(0);
			pg.setMWQTaskNameFilter("");
			pg.setPageCriteriaId6(-1);
			//// New filter options: by Task Name (PageCriteriaId4) and by Region (PageCriteriaId5).
			//// Default 'All' option.
			pg.setPageCondition5(false);
			pg.setPageCondition6(false);
			pg.setPageCondition7(false);
			// Added checkbox to display Hidden tasks or not
			pg.setPageCondition3(false);

			setFilterCriteria();

			tds.setFilterOption(0, false);
			pst.put(tds.getName(), tds);
			//// The logic of getLocationCriteria(pst) filters if current user is a manager of his/her
			//// R)egion, (B)ranch, (G)roup. In case he a manager of some outer RBG, for now there is no filtering
			getLocationCriteria(pst);
		}

		String voName1 = "Repeated2";
		PageCursorInfo tds1 = getPageCursorInfo(pg, voName1);
		if (tds1 == null) {
			// determine number of task (shown) per display page
			int tasksPerPage = ((TiledView) (getCurrNDPage().getChild(voName1))).getMaxDisplayTiles();
			// set up and save task display state object
			tds1 = new PageCursorInfo(voName1, voName1, tasksPerPage, 0);
			// last (0) - don't know total number yet!
			tds1.setRefresh(true);
			pst.put(tds1.getName(), tds1);
		}

		int numOfRows = 0;
		doPriorityModel outstandDO = (doPriorityModel) (RequestManager.getRequestContext().getModelManager().getModel(doPriorityModel.class));

		try {
			ResultSet rs = outstandDO.executeSelect(null);
			if ((rs == null) || (outstandDO.getSize() < 0)) {
				logger.debug("MWQ@setupBeforePageGen::No row returned!!");
				numOfRows = 0;
			} else if ((rs != null) && (outstandDO.getSize() > 0)) {
				numOfRows = outstandDO.getSize();
			}
		} catch (ModelControlException mce) {
			logger.debug("MWQ@setupBeforePageGen::MCEException: " + mce);
		} catch (SQLException sqle) {
			logger.debug("MWQ@setupBeforePageGen::SQLException: " + sqle);
		}

		tds1.setTotalRows(numOfRows);

		//--TD_MWQ_CR--start--//
		//// TD wants to display outstanding tasks counted by Task Name
		//// currently for two tasks:
		//// Decusion Deal (taskId = 3) and
		//// Source Resubmission (taskId = 258).
		String voName3 = "Repeated3";
		PageCursorInfo tds3 = getPageCursorInfo(pg, voName3);
		if (tds3 == null) {
			// determine number of task (shown) per display page
			int tasksPerPage = ((TiledView) (getCurrNDPage().getChild(voName3))).getMaxDisplayTiles();
			// set up and save task display state object
			tds3 = new PageCursorInfo(voName3, voName3, tasksPerPage, 0);
			// last (0) - don't know total number yet!
			tds3.setRefresh(true);
			pst.put(tds3.getName(), tds3);
		}

		doTasksOutstandingsModel outstandTaskByName = (doTasksOutstandingsModel) (RequestManager.getRequestContext().getModelManager().getModel(doTasksOutstandingsModel.class));
		try {
			ResultSet rs = outstandTaskByName.executeSelect(null);
			if ((rs == null) || (outstandTaskByName.getSize() < 0)) {
				numOfRows = 0;
			} else if ((rs != null) && (outstandTaskByName.getSize() > 0)) {
				numOfRows = outstandTaskByName.getSize();
			}
		} catch (ModelControlException mce) {
			logger.debug("MWQ@setupBeforePageGen::MCEException: " + mce);
		} catch (SQLException sqle) {
			logger.debug("MWQ@setupBeforePageGen::SQLException: " + sqle);
		}
		tds3.setTotalRows(numOfRows);

		// Performance enhancement : elimination of the 2nd call of the DataObject
		// the DealId, ApplicationId, CopyId and WFTaskId will be stored and retrieved in the hidden values on each row.
		// ensure data objects have correct criteria ...
		MasterWorkQueueSearchModelImpl wqDO = (MasterWorkQueueSearchModelImpl) (RequestManager.getRequestContext().getModelManager().getModel(MasterWorkQueueSearchModel.class));
		MasterWorkQueueSearchCountModelImpl wqCountDO = (MasterWorkQueueSearchCountModelImpl) (RequestManager.getRequestContext().getModelManager().getModel(MasterWorkQueueSearchCountModel.class));

		try {
			// Set both DOs up based on the filters user picked
			buildDataObject(pg, tds, wqDO, wqCountDO);
			ResultSet rs = wqCountDO.executeSelect(null);
			if ((rs != null) && (wqCountDO.getSize() > 0)) {
				int tmpCount = getIntValue(wqCountDO.getValue(MasterWorkQueueSearchCountModel.FIELD_DFWQNUMOFROWS).toString());
				tds.setTotalRows(tmpCount);
			} else {
				tds.setTotalRows(0);
			}
		} catch (Exception ex) {
			setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
			logger.error(ex);
			return;
		}

		// determine if we will force display of first page
		if (tds.isTotalRowsChanged() || tds.isCriteriaChanged()) {
			// number of rows has changed - unconditional display of first page
			pgMWorkQueueSearchRepeated1TiledView vo = ((pgMWorkQueueSearchViewBean) getCurrNDPage()).getRepeated1();
			tds.setCurrPageNdx(0);
			////Reset the primary model to the "before first" state and reset the display index.
			try {
				vo.resetTileIndex();
			} catch (ModelControlException mce1) {
				logger.error(mce1);
			}

			tds.setTotalRowsChanged(false);
			tds.setCriteriaChanged(false);

			pst.put("DEALID", 0);
			pst.put("COPYID", 0);
			pst.put("DEALAPPID", "0");
			pst.put("INSTITUTIONID", -1);
			pst.put("MULTIACCESS", "" + theSessionState.isMultiAccess());
			tds.setRelSelectedRowNdx(-1);
		}

		// set application Id for deal summary snapshot
		setDealForSnapshot(pg, tds);
		setupDealSummarySnapShotDO(pg);

	}

	////////////////////////////////////////////////////////////////////////
	private void setDealForSnapshot(PageEntry pg, PageCursorInfo tds) {
		if (tds.getTotalRows() <= 0) {
			pg.setPageDealApplicationId("0");
			pg.setPageDealId(0);

			return;
		}

		try {
			//--> Performance enhancement : elimination of the 2nd call of the DataObject
			//--> the DealId, ApplicationId, CopyId and WFTaskId will be stored and retrieved in
			//--> the hidden values on each row.
			//--> By Billy 21Jan2004
			Hashtable pst = pg.getPageStateTable();
			int dealId = ((Integer) pst.get("DEALID")).intValue();
			int copyId = ((Integer) pst.get("COPYID")).intValue();
			String appId = (String) pst.get("DEALAPPID");
			int institutionId = ((Integer) pst.get("INSTITUTIONID")).intValue();

			//FXP21616, do not set deal info for the first time
			if ( dealId != 0 ){
				pg.setPageDealId(dealId);
				pg.setPageDealCID(copyId);
				pg.setPageDealApplicationId(appId);
				pg.setDealInstitutionId(institutionId);
				pg.setPageMosUserId(theSessionState.getUserProfileId(institutionId));
				pg.setPageMosUserTypeId(theSessionState.getUserTypeId(institutionId));
			}
			//===================================================================================
		} catch (Exception e) {
			pg.setPageDealApplicationId("0");
			logger.error("Error setting deal id for summary snap shot");
		}
	}
	
	String getPageConditions(PageEntry pg, Hashtable pst) {
		StringBuffer sqlBuff = null;

		// I. Display or not display hidden tasks.
		String str = null;

		if (pg.getPageCondition3() == false) {
			str = " ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 1 ";
			sqlBuff = new StringBuffer(str);
		} else if (pg.getPageCondition3() == true) {
			str = " (ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 0 OR ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 1)";
			sqlBuff = new StringBuffer(str);
		}

		// II. TD requests to set up two extra filters: by Task Name and by Region/Branch/Group.
		// 1. By Task Name.
		if (pg.getMWQTaskNameFilter().length() > 0) {
			sqlBuff.append(getTaskNameCriteria(pg.getMWQTaskNameFilter()));
		}

		// 2. By Branch(es).
		if (pg.getPageCondition5() == true) {
			String branchCriteria = getInstitutionCriteria((Map<Integer, Integer>) pst.get(PAGE_STATE_TABLE_KEY_MWQS_BY_BRANCHES));
			sqlBuff.append(branchCriteria);
		}

		// 3. By Groups.
		if (pg.getPageCondition6() == true) {
			Vector groups = (Vector) pst.get(PAGE_STATE_TABLE_KEY_MWQS_BY_GROUPS);

			if (groups != null) {
				if (groups.size() == 1) {
					str = " AND (GROUPPROFILE.GROUPPROFILEID = ";
					sqlBuff.append(str);
					sqlBuff.append(groups.elementAt(0));
					sqlBuff.append(") ");
				} else if (groups.size() > 1) {
					for (int i = 0; i < groups.size(); i++) {
						if (i == 0) {
							str = " AND (GROUPPROFILE.GROUPPROFILEID = ";
						} else {
							str = " OR (GROUPPROFILE.GROUPPROFILEID = ";
						}
						sqlBuff.append(str);
						sqlBuff.append(groups.elementAt(i));
					}

					sqlBuff.append(") ");
				}
			}
		}

		//-- 4.By Deal Status.
		// Deal Status Filtering has to include boundary case 0: Incomplete status
		if (pg.getPageCriteriaId6() >= 0) {
			str = " AND (DEAL.STATUSID = ";
			sqlBuff.append(str);
			sqlBuff.append(pg.getPageCriteriaId6());
			sqlBuff.append(") ");
		}

		// TD requests to display tasks narrowed by branch.
		// This option is implemented for region
		// and group as well for other clients.
		int realmId = getRealm();

		// If a narrow criteria exists and both branch and group filtering
		// criteria are false the locationCriteria should be applied.
		if ((realmId > 0) && (pg.getPageCondition5() == false) && (pg.getPageCondition6() == false)) {
			pg.setPageCondition7(true);
		}

		if (pg.getPageCondition7() == true) {
			String locationCriteria = addLocationCriteria(pst, realmId);
			sqlBuff.append(locationCriteria);
		}

		String sql = sqlBuff.toString();
		logger.debug("MWSH@getPageConditions::SqlStrBefore_STATIC_WHERE_CRIT = " + sql);

		return sql;
	}

	private void resetFilterCriteria() {
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();
		pst.remove(PAGE_STATE_TABLE_KEY_MWQS_REUSE);
		pst.put(PAGE_STATE_TABLE_KEY_MWQS_CRITERIA, "");
		pst.put(PAGE_STATE_TABLE_KEY_MWQS_BY_BRANCHES, (new HashMap<Integer, Integer>()));
		((ComboBox) (getCurrNDPage().getChild(CHILD_CBUSERFILTER))).setValue("", true);
		((ComboBox) getCurrNDPage().getDisplayField(CHILD_CBDEALSTATUSFILTER)).setValue("", true);

		((TextField) getCurrNDPage().getChild(CHILD_CBTASKEXPIRYDATESTARTFILTER)).setValue(CHILD_CBTASKEXPIRYDATESTARTFILTER_RESET_VALUE);
		((TextField) getCurrNDPage().getChild(CHILD_CBTASKEXPIRYDATEENDFILTER)).setValue(CHILD_CBTASKEXPIRYDATEENDFILTER_RESET_VALUE);
		pst.remove(PAGE_STATE_TABLE_KEY_MWQS_BY_EXPIRY_DATE_START);
		pst.remove(PAGE_STATE_TABLE_KEY_MWQS_BY_EXPIRY_DATE_END);

	}

	private void setFilterCriteria() {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		String sqlStr = "";
		if (pg.getPageCriteriaId1() >= 0) {
			doUnderwriter1Model dtDo = (doUnderwriter1Model) (RequestManager.getRequestContext().getModelManager().getModel(doUnderwriter1Model.class));

			dtDo.clearUserWhereCriteria();
			dtDo.addUserWhereCriterion(FIELD_DFUSERPROFILEID, "=", pg.getPageCriteriaId1());

			String firstName = null;
			String lastName = null;

			try {
				ResultSet rs = dtDo.executeSelect(null);

				if ((rs != null) && (dtDo.getSize() > 0)) {
					firstName = dtDo.getValue(doUnderwriter1ModelImpl.FIELD_DFFIRSTNAME).toString();
					lastName = dtDo.getValue(doUnderwriter1ModelImpl.FIELD_DFLASTNAME).toString();
				} else {
					logger.error("MWQSH@setFilterCriteria::Problem when getting the Underwriter info !!");
				}

				sqlStr = " C1.CONTACTLASTNAME LIKE '" + lastName + "' AND SUBSTR(C1.CONTACTFIRSTNAME,1,1) LIKE '" + firstName.substring(0, 1) + "' ";
				logger.debug("MWQSH@setFilterCriteria::sqlStr: " + sqlStr);
			} catch (Exception e) {
				logger.error(e);
			}
		}

		// Now based on filter options it should be dynamically setup based on selected option.
		int taskStatusMapper = MWQ_STATUS_TASK_MAPPER[pg.getPageCriteriaId2()];

		if (!sqlStr.trim().equals("")) {
			sqlStr += " AND ";
		}

		switch (taskStatusMapper) {
		case Sc.TASK_OPEN: // open, 1
			sqlStr += " assignedtasksworkqueue.taskstatusid = " + Sc.TASK_OPEN;
			break;

		case Sc.TASK_COMPLETE: // complete, 3
			sqlStr += " assignedtasksworkqueue.taskstatusid = " + Sc.TASK_COMPLETE;
			break;

		case Mc.MWQ_TASK_NOT_ONHOLD: // on hold, 10
			sqlStr += " assignedtasksworkqueue.taskstatusid = " + Sc.TASK_OPEN;
			sqlStr += " AND DEAL.HOLDREASONID <> " + Mc.HOLD_REASON_NONE;
			break;

		case Mc.MWQ_STATUS_TASK_ALL: // all, 9
			sqlStr += " ((assignedtasksworkqueue.taskstatusid = " + Sc.TASK_COMPLETE + ") " + " OR (assignedtasksworkqueue.taskstatusid = " + Sc.TASK_OPEN + ") )";
			break;

		default: // OPEN
			sqlStr += " assignedtasksworkqueue.taskstatusid = " + Sc.TASK_OPEN;
			break;
		}

		if (pg.getPageCriteriaId3() > 0) {
			if (!sqlStr.trim().equals(""))
				sqlStr += " AND ";
			sqlStr += "( assignedtasksworkqueue.dealid = " + pg.getPageCriteriaId3() + ") ";
		}

		Date expiryDateStart = (Date) pst.get(PAGE_STATE_TABLE_KEY_MWQS_BY_EXPIRY_DATE_START);
		Date expiryDateEnd = (Date) pst.get(PAGE_STATE_TABLE_KEY_MWQS_BY_EXPIRY_DATE_END);
		if (expiryDateStart != null)
			sqlStr += (" AND assignedtasksworkqueue.endtimestamp>='" + TypeConverter.stringTypeFrom(expiryDateStart)) + "' ";
		if (expiryDateEnd != null)
			sqlStr += (" AND assignedtasksworkqueue.endtimestamp<='" + TypeConverter.stringTypeFrom(expiryDateEnd)) + "' ";

		// Deal Status Filtering has to include boundary case 0: Incomplete status
		if (pg.getPageCriteriaId6() >= 0) {
			if (!sqlStr.trim().equals(""))
				sqlStr += " AND ";
			sqlStr += ("DEAL.STATUSID = " + pg.getPageCriteriaId6());
		}

		//// TD requests to set up two extra filters: by Task Name and by Region.
		//// Set up Task Name filter.
		//// 1. New filter by Task Name.
		if (pg.getMWQTaskNameFilter().length() > 0) {
			sqlStr += getTaskNameCriteria(pg.getMWQTaskNameFilter());
		}

		// // 2. New filter by location criteria (Region, Branch, Group).
		if ((pst != null) && ((pg.getPageCondition5() == true) || (pg.getPageCondition6() == true)) && (pg.getPageCondition7() == false)) {
			Map<Integer, LocationWrapper> locationMap = (Map<Integer, LocationWrapper>) pst.get(PAGE_STATE_TABLE_KEY_MWQS_BY_LOCATIONS);
			Iterator locationIter = locationMap.keySet().iterator();
			while (locationIter.hasNext()) {

				int instId = Integer.parseInt(locationIter.next().toString());
				LocationWrapper locationWrapper = (LocationWrapper) (locationMap.get(instId));

				if (locationWrapper != null) {
					//// 2. Add Bransh(es) Filter Criteria.
					if (pg.getPageCondition5() == true) {
						String branchCriteria = getInstitutionCriteria((Map<Integer, Integer>) pst.get(PAGE_STATE_TABLE_KEY_MWQS_BY_BRANCHES));
						sqlStr += branchCriteria;

					}

					//// 3. Add Group(s) Filter Criteria.
					if (pg.getPageCondition6() == true) {
						Vector groups = (Vector) pst.get(PAGE_STATE_TABLE_KEY_MWQS_BY_GROUPS);
						String andOr = " AND "; // default
						if (groups != null) {
							if (groups.size() == 1) {
								sqlStr += (" AND (GROUPPROFILE.GROUPPROFILEID = " + groups.elementAt(0));
								sqlStr += ") ";
							} else if (groups.size() > 1) {
								for (int i = 0; i < groups.size(); i++) {
									andOr = (i == 0) ? " AND (" : " OR ";
									sqlStr += (andOr + "GROUPPROFILE.GROUPPROFILEID = " + groups.elementAt(i));
								}
								sqlStr += ") ";
							}
						}
					}
				}
			}
		}

		if (pg.getPageCondition3() == false) {
			sqlStr += " AND ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 1";
		} else if (pg.getPageCondition3() == true) {
			sqlStr += " AND (ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 0 OR ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 1)";
		}

		logger.debug("MWQSH@setFilterCriteria::SQL put into FilterCriteria: " + sqlStr);
		pst.put(PAGE_STATE_TABLE_KEY_MWQS_CRITERIA, sqlStr);
	}

	//This method is used to populate the fields in the header with the appropriate information
	public void populatePageDisplayFields() {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds = (PageCursorInfo) pst.get(PAGE_STATE_TABLE_KEY_MWQS_PCINAME);
		populatePageShellDisplayFields();

		// Quick Link Menu display
		displayQuickLinkMenu();
		
		populatePageDealSummarySnapShot();
		populatePreviousPagesLinks();
		revisePrevNextButtonGeneration(pg, tds);
		//// Populate sort and Task Status options
		populateSortOptions(tds);
		populateTaskStatusOptions();

		String rowsRpt = "";
		if (tds.getTotalRows() > 0)
			rowsRpt = tds.getFirstDisplayRowNumber() + "-" + tds.getLastDisplayRowNumber() + " of [" + tds.getTotalRows() + "]";
		getCurrNDPage().setDisplayFieldValue(CHILD_ROWSDISPLAYED, rowsRpt);
		getCurrNDPage().setDisplayFieldValue(CHILD_HDSELECTEDROWID, "" + tds.getRelSelectedRowNdx());
		getCurrNDPage().setDisplayFieldValue(CHILD_HDCURRENTFIRSTROWID, "" + tds.getFirstDisplayRowNumber());

		populateSortFilterParams(pg, tds);
		
		// FXP30170, 4.4, Oct 08, 2010, saving/reapplying screen scroll position -- start
		applyWindowScrollPosition(); 
		// FXP30170, 4.4, Oct 08, 2010, saving/reapplying screen scroll position -- end
		
	}

	public void handleForwardButton() {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds = (PageCursorInfo) pst.get(PAGE_STATE_TABLE_KEY_MWQS_PCINAME);
		handleForwardBackwardCommon(pg, tds, -1, true);

		pst.put("DEALID", 0);
		pst.put("COPYID", 0);
		pst.put("DEALAPPID", "0");
		tds.setRelSelectedRowNdx(-1);
	}

	public void handleBackwardButton() {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds = (PageCursorInfo) pst.get(PAGE_STATE_TABLE_KEY_MWQS_PCINAME);
		handleForwardBackwardCommon(pg, tds, -1, false);

		pst.put("DEALID", 0);
		pst.put("COPYID", 0);
		pst.put("DEALAPPID", "0");
		tds.setRelSelectedRowNdx(-1);
	}

	//I had to split the main DataObject doMasterWorkQueue and stem a silly doTaskStatus d.o.
	// because Oracle failed on the doMasterWorkQueue having 12 tables relation
	public void populatePickListFields(int taskNdx) {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds = (PageCursorInfo) pst.get(PAGE_STATE_TABLE_KEY_MWQS_PCINAME);

		try {
			// highligt line if current row - else standard background color
			String colorStr = "FFFFCC";
			if (tds.getRelSelectedRowNdx() == taskNdx) {
				colorStr = "FFFFFF";
			}
			getCurrNDPage().setDisplayFieldValue("Repeated1/bgColor", colorStr);
			if (theSessionState.isMultiAccess() == false) {
				getCurrNDPage().setDisplayFieldValue("Repeated1/stInstitutionId", "");
			}
		} catch (Exception e) {
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error(e);
		}
	}

	//--> Performance enhancement : elimination of the 2nd call of the DataObject
	//--> the DealId, ApplicationId, CopyId and WFTaskId will be stored and retrieved in
	//--> the hidden values on each row.
	public void handleReassign(int taskNdx) {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds = (PageCursorInfo) pst.get(PAGE_STATE_TABLE_KEY_MWQS_PCINAME);

		try {
			tds.setRelSelectedRowNdx(taskNdx);
			String institutionId = getRepeatedField("Repeated1/hdInstitutionId", taskNdx);
			//--> Performance enhancement : elimination of the 2nd call of the DataObject
			//--> the DealId, ApplicationId, CopyId and WFTaskId will be stored and retrieved in
			//--> the hidden values on each row.
			// Set the Ids in PST
			pst.put("DEALID", getIntValue(getRepeatedField("Repeated1/hdDealId", taskNdx)));
			pst.put("COPYID", getIntValue(getRepeatedField("Repeated1/hdCopyId", taskNdx)));
			pst.put("DEALAPPID", getRepeatedField("Repeated1/hdCopyId", taskNdx));
			pst.put("INSTITUTIONID", new Integer(institutionId));
			//=====================================================================================
			PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_TASK_REASSIGN, true);
			int WQTaskId = getIntValue(getRepeatedField("Repeated1/hdWqID", taskNdx));
			int WQCopyId = getIntValue(getRepeatedField("Repeated1/hdCopyId", taskNdx));
			int WQDealId = getIntValue(getRepeatedField("Repeated1/hdDealId", taskNdx));

			pgEntry.setPageCriteriaId1(WQTaskId);
			pgEntry.setPageCriteriaId2(WQCopyId);
			pgEntry.setPageCondition3(true);
			int intInstitutionId = Integer.parseInt(institutionId);
			pgEntry.setDealInstitutionId(intInstitutionId);
			pgEntry.setPageMosUserId(theSessionState.getUserProfileId(intInstitutionId));
			pgEntry.setPageMosUserTypeId(theSessionState.getUserTypeId(intInstitutionId));

			pgEntry.setPageDealId(WQDealId);
            
            //FXP30170, 4.4, Oct 08, 2010, saving/reapplying screen scroll position
            saveWindowScrollPosition();
            
			getSavedPages().setNextPage(pgEntry);
			pst.put(PAGE_STATE_TABLE_KEY_MWQS_REUSE, true);
			navigateToNextPage(true);

		} catch (Exception e) {
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error(e);
		}
	}

	public void handleSnapShotButton(int relTaskNdx) {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds = (PageCursorInfo) pst.get(PAGE_STATE_TABLE_KEY_MWQS_PCINAME);
		tds.setRelSelectedRowNdx(relTaskNdx);

		//--> Performance enhancement : elimination of the 2nd call of the DataObject
		//--> the DealId, ApplicationId, CopyId and WFTaskId will be stored and retrieved in
		//--> the hidden values on each row.
		// Set the Ids in PST
		pst.put("DEALID", getIntValue(getRepeatedField("Repeated1/hdDealId", relTaskNdx)));
		pst.put("COPYID", getIntValue(getRepeatedField("Repeated1/hdCopyId", relTaskNdx)));
		pst.put("DEALAPPID", getRepeatedField("Repeated1/hdCopyId", relTaskNdx));
		pst.put("INSTITUTIONID", getIntValue(getRepeatedField("Repeated1/hdInstitutionId", relTaskNdx)));
		
        //FXP30170, 4.4, Oct 08, 2010, saving/reapplying screen scroll position
        saveWindowScrollPosition();
        
	}

	public void handleUserToUserReassign() {
		PageEntry pg = theSessionState.getCurrentPage();
		try {
			PageEntry pageEntry = setupSubPagePageEntry(pg, Mc.PGNM_TASK_REASSIGN, true);
			pageEntry.setPageDealId(0);
			getSavedPages().setNextPage(pageEntry);
			navigateToNextPage(true);
		} catch (Exception e) {
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error(e);
		}
	}

	private void buildDataObject(PageEntry pg, PageCursorInfo tds, MasterWorkQueueSearchModel wqDo, MasterWorkQueueSearchCountModel wqCountDo) throws Exception {
		wqDo.clearUserWhereCriteria();
		wqCountDo.clearUserWhereCriteria();

		Hashtable pst = pg.getPageStateTable();
		String sqlStr = "";

		String orderBy = getSortOrder(tds.getSortOption());
		Boolean toggleVisiblity = (Boolean) pst.get(PAGE_STATE_TABLE_KEY_MWQS_TOGGLE_VISIBILITY);

		if (toggleVisiblity != null && toggleVisiblity) {
			pst.remove(PAGE_STATE_TABLE_KEY_MWQS_TOGGLE_VISIBILITY);
			String filterCriteria = (String) pst.get(PAGE_STATE_TABLE_KEY_MWQS_CRITERIA);
			if (filterCriteria != null && filterCriteria.trim().length() > 0)
				sqlStr += " AND " + filterCriteria;
		} else {
			int iTaskStatusMapper = MWQ_STATUS_TASK_MAPPER[pg.getPageCriteriaId2()];
			switch (iTaskStatusMapper) {
			case Sc.TASK_OPEN: // open, 1
				sqlStr += " AND assignedtasksworkqueue.taskstatusid = " + Sc.TASK_OPEN;
				break;
			case Sc.TASK_COMPLETE: // complete, 3
				sqlStr += " AND assignedtasksworkqueue.taskstatusid = " + Sc.TASK_COMPLETE;
				break;
			case Mc.MWQ_TASK_NOT_ONHOLD: // on hold, 10
				sqlStr += " AND assignedtasksworkqueue.taskstatusid = " + Sc.TASK_OPEN;
				sqlStr += " AND DEAL.HOLDREASONID <> " + Mc.HOLD_REASON_NONE;
				break;
			case Mc.MWQ_STATUS_TASK_ALL: // all, 9
				sqlStr += " AND ((assignedtasksworkqueue.taskstatusid = " + Sc.TASK_COMPLETE + ") " + " OR (assignedtasksworkqueue.taskstatusid = " + Sc.TASK_OPEN + ") )";
				break;
			default: // OPEN
				sqlStr += " AND assignedtasksworkqueue.taskstatusid = " + Sc.TASK_OPEN;
				break;
			}

			if (pg.getPageCriteriaId3() > 0) {
				sqlStr += " AND (assignedtasksworkqueue.dealid = " + pg.getPageCriteriaId3() + ") ";
			}

			Date expiryDateStart = (Date) pst.get(PAGE_STATE_TABLE_KEY_MWQS_BY_EXPIRY_DATE_START);
			Date expiryDateEnd = (Date) pst.get(PAGE_STATE_TABLE_KEY_MWQS_BY_EXPIRY_DATE_END);
			if (expiryDateStart != null)
				sqlStr += (" AND assignedtasksworkqueue.endtimestamp>='" + TypeConverter.stringTypeFrom(expiryDateStart)) + "' ";
			if (expiryDateEnd != null)
				sqlStr += (" AND assignedtasksworkqueue.endtimestamp<='" + TypeConverter.stringTypeFrom(expiryDateEnd)) + "' ";

			//// I. TD requests to set up two extra filters: by Task Name and by Region/Branch/Group.
			//// 1. By Task Name.
			if (pg.getMWQTaskNameFilter().length() > 0) {
				sqlStr += getTaskNameCriteria(pg.getMWQTaskNameFilter());
			}

			//-- added filter by "Deal Status" (PageCriteriaId6)
			// Deal Status Filtering has to include boundary case 0: Incomplete status
			if (pg.getPageCriteriaId6() >= 0)
				sqlStr += (" AND DEAL.STATUSID = " + pg.getPageCriteriaId6());

			//// 2. By Branch(es).
			if (pg.getPageCondition5() == true) {
				String branchCriteria = getInstitutionCriteria((Map<Integer, Integer>) pst.get(PAGE_STATE_TABLE_KEY_MWQS_BY_BRANCHES));
				sqlStr += branchCriteria;
			}

			//// 3. By Groups.
			if (pg.getPageCondition6() == true) {
				Vector groups = (Vector) pst.get(PAGE_STATE_TABLE_KEY_MWQS_BY_GROUPS);
				String andOr = " AND ";

				if (groups != null) {
					if (groups.size() == 1) {
						sqlStr += (" AND (GROUPPROFILE.GROUPPROFILEID = " + groups.elementAt(0));
						sqlStr += ") ";
					} else if (groups.size() > 1) {
						for (int i = 0; i < groups.size(); i++) {
							andOr = (i == 0) ? " AND (" : " OR ";
							sqlStr += (andOr + "GROUPPROFILE.GROUPPROFILEID = " + groups.elementAt(i));
						}
						sqlStr += ") ";
					}
				}
			}

			if (pg.getPageCriteriaId1() >= 0) {
				doUnderwriter1Model dtDo = (doUnderwriter1Model) (RequestManager.getRequestContext().getModelManager().getModel(doUnderwriter1Model.class));
				dtDo.clearUserWhereCriteria();
				dtDo.addUserWhereCriterion(FIELD_DFUSERPROFILEID, "=", pg.getPageCriteriaId1());

				//// BX: separate the following fragment into the private routine in this class (3 times occurance).
				String firstName = null;
				String lastName = null;

				try {
					ResultSet rs = dtDo.executeSelect(null);

					if ((rs != null) && (dtDo.getSize() > 0)) {
						firstName = dtDo.getValue(doUnderwriter1ModelImpl.FIELD_DFFIRSTNAME).toString();
						lastName = dtDo.getValue(doUnderwriter1ModelImpl.FIELD_DFLASTNAME).toString();
					} else {
						logger.error("MWQSH@buildDataObject::Problem when getting the Underwriter info !!");
						////never happens, but return just in case.
						return;
					}

					if (firstName.equals("") || (firstName == null) || lastName.equals("") || (lastName == null)) {
						////for bad user profiles like 0 ID or something else wrong.
						return;
					}

					//// IMPORTANT!!
					//// TD wants to see the Source Firm/Source of Business along with primary Borrower.
					//// As a result the 'CONTACT' table is aliased in MasterWorkQueueSearchModel and,
					//// therefore, should be alised (to C1 'CONTACT' instance) in this additional
					//// search criteria to escape an SQL invalid identifier exception in the joint query.
					sqlStr += (" AND C1.CONTACTLASTNAME LIKE '" + lastName + "' AND SUBSTR(C1.CONTACTFIRSTNAME,1,1) LIKE '" + firstName.substring(0, 1) + "'");
				} catch (Exception e) {
					logger.error(e);
				}
			}

			if (pg.getPageCondition3() == false) {
				sqlStr += " AND ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 1";
			} else if (pg.getPageCondition3() == true) {
				sqlStr += " AND (ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 0 OR ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 1)";
			}

			//// TD requests to display tasks narrowed by branch. This option is implemented for region and group as well for other clients.
			int realmId = getRealm();

			//// If a narrow criteria exists and both branch and group filtering criteria
			//// are false the locationCriteria should be applied.
			if ((realmId > 0) && (pg.getPageCondition5() == false) && (pg.getPageCondition6() == false)) {
				pg.setPageCondition7(true);
			}

			if (pg.getPageCondition7() == true) {
				String locationCriteria = addLocationCriteria(pst, realmId);
				sqlStr = sqlStr + locationCriteria;
			}
		}

		if (sqlStr.equals("") || (sqlStr == null)) {
			((MasterWorkQueueSearchModelImpl) wqDo).setStaticWhereCriteriaString(MasterWorkQueueSearchModelImpl.STATIC_WHERE_CRITERIA + " order by " + orderBy);
			((MasterWorkQueueSearchCountModelImpl) wqCountDo).setStaticWhereCriteriaString(MasterWorkQueueSearchCountModelImpl.STATIC_WHERE_CRITERIA);
		} else if (!sqlStr.equals("") && (sqlStr != null)) {
			((MasterWorkQueueSearchModelImpl) wqDo).setStaticWhereCriteriaString(MasterWorkQueueSearchModelImpl.STATIC_WHERE_CRITERIA + sqlStr + " order by " + orderBy);
			((MasterWorkQueueSearchCountModelImpl) wqCountDo).setStaticWhereCriteriaString(MasterWorkQueueSearchCountModelImpl.STATIC_WHERE_CRITERIA + sqlStr);
		}
	}

	public void handleFilterButton() {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();

		String sqlStr = "";
		boolean toggleVisiblity;
		boolean currentValue = getCurrNDPage().getDisplayFieldValue(CHILD_CHISDISPLAYHIDDEN).toString().equals("T");
		String storedCriteria = (String) pst.get(PAGE_STATE_TABLE_KEY_MWQS_CRITERIA);
		String displayInVisible = "ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 0";
		if (storedCriteria != null && storedCriteria.trim().length() > 0)
			toggleVisiblity = (currentValue && !storedCriteria.contains(displayInVisible)) || (!currentValue && storedCriteria.contains(displayInVisible));
		else
			toggleVisiblity = currentValue;
		pst.put(PAGE_STATE_TABLE_KEY_MWQS_TOGGLE_VISIBILITY, toggleVisiblity);

		if (toggleVisiblity) {
			pg.setPageCondition3(currentValue);
			if (storedCriteria != null && storedCriteria.trim().length() > 0) {
				sqlStr = storedCriteria.substring(0, storedCriteria.lastIndexOf(" AND ")) + " AND ";
			}

			if (currentValue) {
				sqlStr += " (ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 0 OR ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 1)";
			} else {
				sqlStr += " ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 1";
			}

			logger.debug("MWQSH@setFilterCriteria::SQL put into FilterCriteria: " + sqlStr);

			pst.put(PAGE_STATE_TABLE_KEY_MWQS_CRITERIA, sqlStr);
		} else {

			ComboBox userFilterBox = (ComboBox) getCurrNDPage().getDisplayField(CHILD_CBUSERFILTER);
			String sFilterValue = (String) userFilterBox.getValue();
			int userFltrNdx = 0;

			try {
				userFltrNdx = (new Integer(sFilterValue)).intValue();
				pg.setPageCriteriaId1(userFltrNdx);
			} catch (Exception e) {
				//// Any Problem default to 0 (default ioption)
				userFltrNdx = 0;
				pg.setPageCriteriaId1(-1);
			}

			ComboBox statusFilterBox = (ComboBox) getCurrNDPage().getDisplayField(CHILD_CBSTATUSFILTER);
			String statusFilterValue = (String) statusFilterBox.getValue();
			int statusFltrNdx = 0;

			try {
				statusFltrNdx = (new Integer(statusFilterValue)).intValue();
				pg.setPageCriteriaId2(statusFltrNdx);
			} catch (Exception e) {
				//// Any Problem default to 0 (default ioption)
				statusFltrNdx = 0;
				pg.setPageCriteriaId2(-1);
			}

			ComboBox dealStatusFilterBox = (ComboBox) getCurrNDPage().getDisplayField(CHILD_CBDEALSTATUSFILTER);
			String dealStatusFilterValue = (String) dealStatusFilterBox.getValue();
			int dealStatusFltrNdx = 0;

			try {
				dealStatusFltrNdx = (new Integer(dealStatusFilterValue)).intValue();
				pg.setPageCriteriaId6(dealStatusFltrNdx);
			} catch (Exception e) {
				//// Any Problem default to 0 (default ioption)
				dealStatusFltrNdx = 0;
				pg.setPageCriteriaId6(-1);
			}

			TextField dealIdFilter = (TextField) getCurrNDPage().getDisplayField(CHILD_TBDEALIDFILTER);
			String dealIdFilterValue = (String) dealIdFilter.getValue();

			if (!dealIdFilterValue.equals("") && (dealIdFilterValue != null)) {
				pg.setPageCriteriaId3((new Integer(dealIdFilter.getValue().toString())).intValue());
			} else {
				pg.setPageCriteriaId3(0);
			}

			// Added checkbox to display Hidden tasks or not
			if (getCurrNDPage().getDisplayFieldValue(CHILD_CHISDISPLAYHIDDEN).toString().equals("T")) {
				pg.setPageCondition3(true);
			} else {
				pg.setPageCondition3(false);
			}

			//// 1. New Filter by Task Name.
			ComboBox taskNameFilterBox = (ComboBox) getCurrNDPage().getDisplayField(CHILD_CBTASKNAMEFILTER);
			String taskNameFilterValue = (String) taskNameFilterBox.getValue();
			try {
				pg.setMWQTaskNameFilter(taskNameFilterValue);
			} catch (Exception e) {
				//// Any Problem default to 0 (default option)
				pg.setMWQTaskNameFilter("");
			}

			//// 2. New Filter by Branch.
			ListBox theBranches = (ListBox) (getCurrNDPage().getChild(CHILD_LBMWQBRANCHES));
			Object[] theSelectedBranches = theBranches.getValues();
			if (theSelectedBranches.length > 0) {
				int branchId = 0;
				Map<Integer, Integer> branchMap = new HashMap<Integer, Integer>();
				for (int i = 0; i < theSelectedBranches.length; i++) {
					if (!theSelectedBranches[i].toString().equals("")) {
						String branchStr = theSelectedBranches[i].toString();
						String[] result = branchStr.split(", ");
						branchId = Integer.valueOf(result[0]).intValue();
						int instId = Integer.valueOf(result[1]).intValue();
						branchMap.put(branchId, instId);
					}
				}
				pg.setPageCondition5(true);
				pst.put(PAGE_STATE_TABLE_KEY_MWQS_BY_BRANCHES, branchMap);
			}

			//// 3. New Filter by Group.
			ListBox theGroups = (ListBox) (getCurrNDPage().getChild(CHILD_LBMWQGROUPS));
			Object[] theSelectedGroups = theGroups.getValues();
			if (theSelectedGroups.length > 0) {
				int groupId = 0;
				Vector<Integer> groups = new Vector<Integer>();
				for (int i = 0; i < theSelectedGroups.length; i++) {
					if (!theSelectedGroups[i].toString().equals("")) {
						groupId = com.iplanet.jato.util.TypeConverter.asInt(theSelectedGroups[i], 0);
						groups.add(groupId);
					}
				}

				pg.setPageCondition6(true);
				pg.setPageCondition7(false);
				pst.put(PAGE_STATE_TABLE_KEY_MWQS_BY_GROUPS, groups);
			}

			// expiry date range
			TextField expiryDateStartTextFeild = (TextField) (getCurrNDPage().getChild(CHILD_CBTASKEXPIRYDATESTARTFILTER));
			String expiryDateStartString = (expiryDateStartTextFeild == null) ? null : (String) (expiryDateStartTextFeild.getValue());
			Date expiryDateStart;
			try {
				expiryDateStart = dateFormat.parse(expiryDateStartString);
				expiryDateStartString = dateFormat.format(expiryDateStart);
				pst.put(PAGE_STATE_TABLE_KEY_MWQS_BY_EXPIRY_DATE_START, expiryDateStart);
			} catch (Exception e) { //NullPointerException and ParseException
				expiryDateStart = null;
				expiryDateStartString = CHILD_CBTASKEXPIRYDATESTARTFILTER_RESET_VALUE;
				pst.remove(PAGE_STATE_TABLE_KEY_MWQS_BY_EXPIRY_DATE_START);
			}
			if (expiryDateStartTextFeild != null) {
				expiryDateStartTextFeild.setValue(expiryDateStartString);
			}

			TextField expiryDateEndTextFeild = (TextField) (getCurrNDPage().getChild(CHILD_CBTASKEXPIRYDATEENDFILTER));
			String expiryDateEndString = (expiryDateEndTextFeild == null) ? null : (String) (expiryDateEndTextFeild.getValue());
			Date expiryDateEnd;
			try {
				expiryDateEnd = dateFormat.parse(expiryDateEndString);
				expiryDateEndString = dateFormat.format(expiryDateEnd);
				pst.put(PAGE_STATE_TABLE_KEY_MWQS_BY_EXPIRY_DATE_END, expiryDateEnd);
			} catch (Exception e) { //NullPointerException and ParseException
				expiryDateEnd = null;
				expiryDateEndString = CHILD_CBTASKEXPIRYDATEENDFILTER_RESET_VALUE;
				pst.remove(PAGE_STATE_TABLE_KEY_MWQS_BY_EXPIRY_DATE_END);
			}
			if (expiryDateEndTextFeild != null)
				expiryDateEndTextFeild.setValue(expiryDateEndString);

			setFilterCriteria();
		}
		PageCursorInfo tds = getPageCursorInfo(pg, PAGE_STATE_TABLE_KEY_MWQS_PCINAME);
		tds.setCriteriaChanged(true);
	}

	public void handleSortButton() {
		PageEntry pg = theSessionState.getCurrentPage();
		PageCursorInfo tds = getPageCursorInfo(pg, PAGE_STATE_TABLE_KEY_MWQS_PCINAME);
		ComboBox sortBox = (ComboBox) getCurrNDPage().getDisplayField(CHILD_CBSORTOPTIONS);
		String sortBoxValue = (String) sortBox.getValue();
		int sortNdx = 0;
		try {
			sortNdx = (new Integer(sortBoxValue)).intValue();
			tds.setSortOption(sortNdx);
		} catch (Exception e) {
			//// Any Problem default to 0 (default ioption)
			sortNdx = 0;
		}
		tds.setCriteriaChanged(true);
	}

	public void handleResetButton() {
		PageEntry pg = theSessionState.getCurrentPage();
		PageCursorInfo tds = getPageCursorInfo(pg, PAGE_STATE_TABLE_KEY_MWQS_PCINAME);
		pg.setPageCriteriaId1(-1);
		resetFilterCriteria();
		tds.setSortOption(0);
		pg.setPageCriteriaId2(0);
		pg.setPageCriteriaId3(0);
		pg.setMWQTaskNameFilter("");
		pg.setPageCriteriaId6(-1);
		pg.setPageCondition5(false);
		pg.setPageCondition6(false);
		pg.setPageCondition7(false);
		pg.setPageCondition3(false);
		tds.setCriteriaChanged(true);
		Hashtable pst = pg.getPageStateTable();
		pst.put(PAGE_STATE_TABLE_KEY_MWQS_REFRESH, true);
	}

	private void populateSortOptions(PageCursorInfo tds) {

		if (theSessionState.isMultiAccess())
			getCurrNDPage().setDisplayFieldValue(CHILD_MULTIACCESS, "Y");
		else
			getCurrNDPage().setDisplayFieldValue(CHILD_MULTIACCESS, "N");
		ComboBox sortBox = (ComboBox) getCurrNDPage().getDisplayField(CHILD_CBSORTOPTIONS);

		Collection theSList = BXResources.getPickListValuesAndDesc(this.theSessionState.getUserInstitutionId(), "MWORKQUEUESORTORDERS", this.theSessionState.getLanguageId());
		int lim = theSList.size();
		Iterator theList = theSList.iterator();
		String[] theVal = new String[2];
		String[] labels = new String[lim];
		String[] values = new String[lim];

		for (int i = 0; i < lim; i++) {
			theVal = (String[]) (theList.next());
			labels[i] = theVal[1];
			values[i] = theVal[0];
		}

		OptionList sortOption = pgMWorkQueueSearchViewBean.cbSortOptionsOptions;
		sortOption.setOptions(labels, values);
		int selectedNdx = tds.getSortOption();
		if ((selectedNdx < 0) || (selectedNdx >= lim))
			selectedNdx = 0;
		sortBox.setValue((new Integer(selectedNdx)).toString(), true);
	}

	private void populateTaskStatusOptions() {
		PageEntry pg = theSessionState.getCurrentPage();

		ComboBox statFilter = (ComboBox) getCurrNDPage().getDisplayField(CHILD_CBSTATUSFILTER);
		Option[] optionsOrig = statFilter.getOptions().toOptionArray();

		//// Manual population of the TaskStatus Combobox.
		int lim = Mc.MWQ_STATUS_TASK_OPTIONS_LENGTH;

		String[] labels = new String[lim];
		String[] values = new String[lim];

		String onholdTasksLbl = BXResources.getGenericMsg("MWQ_STATUS_TASK_CBOPTIONS_ONHOLD_LABEL", theSessionState.getLanguageId());
		String openTasksLbl = BXResources.getGenericMsg("MWQ_STATUS_TASK_CBOPTIONS_OPEN_LABEL", theSessionState.getLanguageId());
		String completedTasksLbl = BXResources.getGenericMsg("MWQ_STATUS_TASK_CBOPTIONS_COMPLITE_LABEL", theSessionState.getLanguageId());
		String allTasksLbl = BXResources.getGenericMsg("MWQ_STATUS_TASK_CBOPTIONS_ALL_LABEL", theSessionState.getLanguageId());

		String[] MWQ_STATUS_TASK_OPTIONS = { openTasksLbl, completedTasksLbl, onholdTasksLbl, allTasksLbl };
		for (int i = 0; i < lim; i++) {
			labels[i] = MWQ_STATUS_TASK_OPTIONS[i];
			values[i] = (new Integer(i)).toString();
		}

		OptionList statusTaskOption = pgMWorkQueueSearchViewBean.cbStatusFilterOptions;
		statusTaskOption.setOptions(labels, values);
		int selectedNdx = pg.getPageCriteriaId2();
		if ((selectedNdx < 0) || (selectedNdx >= lim))
			selectedNdx = 0;
		statFilter.setValue((new Integer(selectedNdx)).toString(), true);
	}

	private String getSortOrder(int sortType) {
		String dueDatePriority = "ASSIGNEDTASKSWORKQUEUE.dueTimeStamp, ASSIGNEDTASKSWORKQUEUE.priorityId";
		String sql = null;

		switch (sortType) {
		case Mc.MWQ_SORT_ORDER_PRIORITY:
			sql = " ASSIGNEDTASKSWORKQUEUE.priorityId, ASSIGNEDTASKSWORKQUEUE.dueTimeStamp ";
			break;
		case Mc.MWQ_SORT_DUE_DATE:
			sql = dueDatePriority;
			break;
		case Mc.MWQ_SORT_BORROWER:
			sql = "BORROWER.borrowerLastName, " + dueDatePriority;
			break;
		case Mc.MWQ_SORT_DEAL_NUMBER:
			sql = "ASSIGNEDTASKSWORKQUEUE.dealId ASC, " + dueDatePriority;
			break;
		case Mc.MWQ_SORT_DEAL_NUMBER_DSC:
			sql = "ASSIGNEDTASKSWORKQUEUE.dealId DESC, " + dueDatePriority;
			break;
		case Mc.MWQ_SORT_TASK_DESC:
			sql = "WORKFLOWTASK.taskname, " + dueDatePriority;
			break;
		case Mc.MWQ_SORT_TASK_STATUS:
			sql = "ASSIGNEDTASKSWORKQUEUE.taskStatusID, " + dueDatePriority;
			break;
		case Mc.MWQ_SORT_WARNING:
			sql = "ASSIGNEDTASKSWORKQUEUE.timeMilestoneID, " + dueDatePriority;
			break;
		case Mc.MWQ_SORT_DEAL_STATUS:
			sql = "DEAL.STATUSID, " + dueDatePriority;
			break;
		default:
			sql = dueDatePriority;
			break;
		}
		return setSchemaName(sql);
	}

	private void populateSortFilterParams(PageEntry pg, PageCursorInfo tds) {
		try {
			int filterOptValue = pg.getPageCriteriaId1();
			int sortOptValue = tds.getSortOption();
			int statusFilterValue = pg.getPageCriteriaId2();
			int dealIdFilterValue = pg.getPageCriteriaId3();

			Hashtable pst = pg.getPageStateTable();
			Map<Integer, Integer> branches = (Map<Integer, Integer>) pst.get(PAGE_STATE_TABLE_KEY_MWQS_BY_BRANCHES);
			Vector groups = (Vector) pst.get(PAGE_STATE_TABLE_KEY_MWQS_BY_GROUPS);

			String stTaskName = "";

			String branchesFilterDesc = getBranchFilterDesc();
			String groupsFilterDesc = getGroupFilterDesc();

			ListBox theBranches = (ListBox) (getCurrNDPage().getChild(CHILD_LBMWQBRANCHES));
			//ListBox theGroups = (ListBox) (getCurrNDPage().getChild(CHILD_LBMWQGROUPS));

			ComboBox filterBox = (ComboBox) (getCurrNDPage().getDisplayField(CHILD_CBUSERFILTER));
			ComboBox sortBox = (ComboBox) (getCurrNDPage().getDisplayField(CHILD_CBSORTOPTIONS));
			ComboBox statusFilterBox = (ComboBox) (getCurrNDPage().getDisplayField(CHILD_CBSTATUSFILTER));

			////1. Prepare the Summary Task Description static text for setup.
			ComboBox taskNameFilterBox = (ComboBox) getCurrNDPage().getDisplayField(CHILD_CBTASKNAMEFILTER);
			String taskNameFilterValue = (String) taskNameFilterBox.getValue();

			if (taskNameFilterValue.length() > 1) {
				String taskName = taskNameFilterValue.substring(2, taskNameFilterValue.length() - 2);
				String[] result = taskName.split("\\}, \\{");
				String[] inner = result[0].split("=");
				int instId = Integer.valueOf(inner[0]).intValue();
				int taskId = Integer.valueOf(inner[1]).intValue();
				stTaskName = BXResources.getPickListDescription(instId, "TASK741", taskId, theSessionState.getLanguageId());
			} else {
				stTaskName = "";
			}

			if (branches != null) {
				Iterator instIds = branches.keySet().iterator();
				int size = branches.size();
				Object[] listVector = new Object[size];
				for (int i = 0; i < size; i++) {
					Integer branchId = (Integer) instIds.next();
					int branchIdVal = branchId.intValue();
					int instId = ((Integer) branches.get(branchId)).intValue();
					listVector[i] = branchIdVal + ", " + instId;
				}
				theBranches.setValues(listVector, true);
			}

			//// 2. Populate Location Filter Part if com.basis100.masterworkqueue.filtertasks property
			//// narrowing initial task display by region, branch, or group.
			int realmId = getRealm();
			if (realmId > 0) {
				String locationCriteria = getLocationPathLabel(srk, realmId, pst);
				getCurrNDPage().setDisplayFieldValue(CHILD_STUSERFILTER, locationCriteria);
				getCurrNDPage().setDisplayFieldValue(CHILD_STLOCATIONFULL, locationCriteria);
			} else {
				getCurrNDPage().setDisplayFieldValue(CHILD_STUSERFILTER, "");
				getCurrNDPage().setDisplayFieldValue(CHILD_STLOCATIONFULL, "");
			}

			// ensure selected user is current selection in choice list and populate selected user name field
			if (filterOptValue < 0) {
				String dealLabel = BXResources.getGenericMsg("MWQ_DEAL_LABEL", theSessionState.getLanguageId());
				String numberLabel = BXResources.getGenericMsg("MWQ_NUMBER_LABEL", theSessionState.getLanguageId());

				//// 1. Populate the rest of the filter's parts.
				if (pg.getMWQTaskNameFilter().length() > 0) {
					//// Assign the whole content to the ToolTip.
					getCurrNDPage().setDisplayFieldValue(CHILD_STTASKNAME, stTaskName);
					//// Truncate the text for displaying on the screen.
					if (stTaskName.length() > TRANCATED_TASKNAME_LENGTH) {
						String trancTaskName = stTaskName.substring(0, TRANCATED_TASKNAME_LENGTH) + "...";
						getCurrNDPage().setDisplayFieldValue(CHILD_STTASKNAME, trancTaskName);
						getCurrNDPage().setDisplayFieldValue(CHILD_STTASKNAMEFULL, stTaskName);
					} else {
						getCurrNDPage().setDisplayFieldValue(CHILD_STTASKNAME, stTaskName);
						getCurrNDPage().setDisplayFieldValue(CHILD_STTASKNAMEFULL, stTaskName);
					}
				} else {
					getCurrNDPage().setDisplayFieldValue(CHILD_STTASKNAME, BXResources.getGenericMsg("MWQ_STATUS_TASK_CBOPTIONS_ALL_LABEL", theSessionState.getLanguageId()));
					getCurrNDPage().setDisplayFieldValue(CHILD_STTASKNAMEFULL, BXResources.getGenericMsg("MWQ_STATUS_TASK_CBOPTIONS_ALL_LABEL", theSessionState.getLanguageId()));
				}

				if (dealIdFilterValue > 0) {
					getCurrNDPage().setDisplayFieldValue(CHILD_STUSERFILTER, "" + "(" + dealLabel + " " + numberLabel + " " + dealIdFilterValue + ")");
				} else if (pg.getPageCondition5() || pg.getPageCondition6()) {
					//// Assign the whole content to the ToolTip.
					getCurrNDPage().setDisplayFieldValue(CHILD_STLOCATIONFULL, branchesFilterDesc + groupsFilterDesc);
					//// Truncate the text for displaying on the screen.
					if ((branchesFilterDesc.length() + groupsFilterDesc.length()) > TRANCATED_LOCATION_LENGTH) {
						String temp = branchesFilterDesc + groupsFilterDesc;
						String trancLocation = temp.substring(0, TRANCATED_LOCATION_LENGTH) + "...";
						getCurrNDPage().setDisplayFieldValue(CHILD_STUSERFILTER, trancLocation);
					} else {
						getCurrNDPage().setDisplayFieldValue(CHILD_STUSERFILTER, branchesFilterDesc + groupsFilterDesc);
					}
				} else if (realmId == 0) {
					getCurrNDPage().setDisplayFieldValue(CHILD_STUSERFILTER, "");
					getCurrNDPage().setDisplayFieldValue(CHILD_STLOCATIONFULL, "");
				}
			} else {
				filterBox.setValue(filterOptValue);
				try {
					doUnderwriter1ModelImpl theDO = (doUnderwriter1ModelImpl) (RequestManager.getRequestContext().getModelManager().getModel(doUnderwriter1Model.class));
					theDO.clearUserWhereCriteria();
					theDO.addUserWhereCriterion(FIELD_DFUSERPROFILEID, "=", filterOptValue);
					String userName = null;
					try {
						ResultSet rs = theDO.executeSelect(null);
						if ((rs != null) && (theDO.getSize() > 0)) {
							userName = theDO.getValue(doUnderwriter1ModelImpl.FIELD_DFUSERNAME).toString();
						} else {
							logger.error("MWQSH@populateSortFilterParams::Problem when getting the userName !!");
						}
					} catch (Exception e) {
						logger.error(e);
					}

					if (dealIdFilterValue > 0) {
						getCurrNDPage().setDisplayFieldValue(CHILD_STUSERFILTER, userName + "(Deal # " + dealIdFilterValue + ")");
					} else {
						//// Assign the whole content to the ToolTip.
						getCurrNDPage().setDisplayFieldValue(CHILD_STLOCATIONFULL, branchesFilterDesc + groupsFilterDesc + userName);

						//// Truncate the text for displaying on the screen.
						if ((branchesFilterDesc.length() + groupsFilterDesc.length() + userName.length()) > TRANCATED_LOCATION_LENGTH) {
							String temp = branchesFilterDesc + groupsFilterDesc + userName;
							String trancLocation = temp.substring(0, TRANCATED_LOCATION_LENGTH) + "...";
							getCurrNDPage().setDisplayFieldValue(CHILD_STUSERFILTER, trancLocation);
						} else {
							getCurrNDPage().setDisplayFieldValue(CHILD_STUSERFILTER, branchesFilterDesc + groupsFilterDesc + userName);
						}
					}
				} catch (Exception e) {
					getCurrNDPage().setDisplayFieldValue(CHILD_STUSERFILTER, " ");
					logger.error("Exception MWQH@populateSortFilterParams: getting selected user name - ignored");
					logger.error(e);
				}
			}

			String defaultLabel = BXResources.getGenericMsg("MWQ_SORT_ORDER_OPTIONS_DEFAULT_LABEL", theSessionState.getLanguageId());
			String priorityLabel = BXResources.getGenericMsg("MWQ_SORT_ORDER_OPTIONS_PRIORITY_LABEL", theSessionState.getLanguageId());
			String dealASCLabel = BXResources.getGenericMsg("MWQ_SORT_ORDER_OPTIONS_DEAL_NUMBER_ASC_LABEL", theSessionState.getLanguageId());
			String dealDECLabel = BXResources.getGenericMsg("MWQ_SORT_ORDER_OPTIONS_DEAL_NUMBER_DEC_LABEL", theSessionState.getLanguageId());
			String statusLabel = BXResources.getGenericMsg("MWQ_SORT_ORDER_OPTIONS_STATUS_LABEL", theSessionState.getLanguageId());
			String borrNameLabel = BXResources.getGenericMsg("MWQ_SORT_ORDER_OPTIONS_BORROWER_NAME_LABEL", theSessionState.getLanguageId());
			String warningLabel = BXResources.getGenericMsg("MWQ_SORT_ORDER_OPTIONS_WARNING_LABEL", theSessionState.getLanguageId());
			String taskDescLabel = BXResources.getGenericMsg("MWQ_SORT_ORDER_OPTIONS_TASK_DESCRIPTION_LABEL", theSessionState.getLanguageId());
			String dealStatusLabel = BXResources.getGenericMsg("MWQ_SORT_ORDER_OPTIONS_DEAL_STATUS_LABEL", theSessionState.getLanguageId());
			String[] MWQ_SORT_ORDER_OPTIONS = { defaultLabel, priorityLabel, dealASCLabel, dealDECLabel, statusLabel, borrNameLabel, warningLabel, taskDescLabel, dealStatusLabel };

			String openTasksLbl = BXResources.getGenericMsg("MWQ_STATUS_TASK_CBOPTIONS_OPEN_LABEL", theSessionState.getLanguageId());
			String completedTasksLbl = BXResources.getGenericMsg("MWQ_STATUS_TASK_CBOPTIONS_COMPLITE_LABEL", theSessionState.getLanguageId());
			String allTasksLbl = BXResources.getGenericMsg("MWQ_STATUS_TASK_CBOPTIONS_ALL_LABEL", theSessionState.getLanguageId());
			String onholdTasksLbl = BXResources.getGenericMsg("MWQ_STATUS_TASK_CBOPTIONS_ONHOLD_LABEL", theSessionState.getLanguageId());
			String[] MWQ_STATUS_TASK_OPTIONS = { openTasksLbl, completedTasksLbl, onholdTasksLbl, allTasksLbl };

			// sort option
			if (sortOptValue == 0) {
				sortOptValue = Mc.MWQ_SORT_DUE_DATE;
			}

			sortBox.setValue(sortOptValue);
			statusFilterBox.setValue(statusFilterValue);

			// populate sort option selected
			getCurrNDPage().setDisplayFieldValue(CHILD_STSORTOPTION, MWQ_SORT_ORDER_OPTIONS[sortOptValue]);
			getCurrNDPage().setDisplayFieldValue(CHILD_STSTATUSFILTER, MWQ_STATUS_TASK_OPTIONS[statusFilterValue]);

			// populate DealId Filter value
			if (dealIdFilterValue > 0)
				getCurrNDPage().setDisplayFieldValue(CHILD_TBDEALIDFILTER, dealIdFilterValue);
			else
				getCurrNDPage().setDisplayFieldValue(CHILD_TBDEALIDFILTER, "");

			// Added checkbox to display Hidden tasks or not
			if (pg.getPageCondition3())
				getCurrNDPage().setDisplayFieldValue(CHILD_CHISDISPLAYHIDDEN, "T");
			else
				getCurrNDPage().setDisplayFieldValue(CHILD_CHISDISPLAYHIDDEN, "F");

			if (pg.getMWQTaskNameFilter().length() > 0) {
				//// Assign the whole content to the ToolTip.
				getCurrNDPage().setDisplayFieldValue(CHILD_STTASKNAME, stTaskName);
				//// Truncate the text for displaying on the screen.
				if (stTaskName.length() > TRANCATED_TASKNAME_LENGTH) {
					String trancTaskName = stTaskName.substring(0, TRANCATED_TASKNAME_LENGTH) + "...";
					getCurrNDPage().setDisplayFieldValue(CHILD_STTASKNAME, trancTaskName);
					getCurrNDPage().setDisplayFieldValue(CHILD_STTASKNAMEFULL, stTaskName);
				} else {
					getCurrNDPage().setDisplayFieldValue(CHILD_STTASKNAME, stTaskName);
					getCurrNDPage().setDisplayFieldValue(CHILD_STTASKNAMEFULL, stTaskName);
				}
			} else {
				getCurrNDPage().setDisplayFieldValue(CHILD_STTASKNAME, BXResources.getGenericMsg("MWQ_STATUS_TASK_CBOPTIONS_ALL_LABEL", theSessionState.getLanguageId()));
				getCurrNDPage().setDisplayFieldValue(CHILD_STTASKNAMEFULL, BXResources.getGenericMsg("MWQ_STATUS_TASK_CBOPTIONS_ALL_LABEL", theSessionState.getLanguageId()));
			}
		} catch (Exception e) {
			logger.error("Exception @populateSortFilterParams: ignored");
			logger.error(StringUtil.stack2string(e));
		}
	}

	private String getBranchFilterDesc() {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		//// 1. Form the branch filter criteria.
		ListBox theBranches = (ListBox) (getCurrNDPage().getChild(CHILD_LBMWQBRANCHES));
		Object[] theSelectedBranches = theBranches.getValues();
		String selBranches = "";
		if (theSelectedBranches.length > 0) {
			int branchId = 0;
			for (int i = 0; i < theSelectedBranches.length; i++) {
				if (!theSelectedBranches[i].toString().equals("")) {
					String branchStr = theSelectedBranches[i].toString();
					String[] result = branchStr.split(", ");
					branchId = Integer.valueOf(result[0]).intValue();
					theSelectedBranches[i].toString();
					selBranches += BXResources.getPickListDescription(this.theSessionState.getUserInstitutionId(), "BRANCHPROFILE", branchId, theSessionState.getLanguageId());
					if (theSelectedBranches.length == 1) {
						selBranches += "/";
					} else if (theSelectedBranches.length >= 1) {
						if (i < (theSelectedBranches.length - 1)) {
							selBranches += "+";
						} else {
							selBranches += "/";
						}
					}
				}
			}
		}
		return selBranches;
	}

	private String getGroupFilterDesc() {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		//// 1. Form the group filter criteria.
		ListBox theGroups = (ListBox) (getCurrNDPage().getChild(CHILD_LBMWQGROUPS));
		Object[] theSelectedGroups = theGroups.getValues();
		String selGroups = "";

		if (theSelectedGroups.length > 0) {
			int groupId = 0;
			for (int i = 0; i < theSelectedGroups.length; i++) {
				if (!theSelectedGroups[i].toString().equals("")) {
					groupId = com.iplanet.jato.util.TypeConverter.asInt(theSelectedGroups[i], 0);
					selGroups += BXResources.getPickListDescription(this.theSessionState.getUserInstitutionId(), "GROUPPROFILE", groupId, theSessionState.getLanguageId());
					if (theSelectedGroups.length == 1) {
						selGroups += "/";
					} else if (theSelectedGroups.length >= 1) {
						if (i < (theSelectedGroups.length - 1)) {
							selGroups += "+";
						} else {
							selGroups += "/";
						}
					}
				}
			}
		}
		return selGroups;
	}

	private String getLocationPathLabel(SessionResourceKit srk, int locationLevel, Hashtable pst) {
		if (pst == null)
			return "";
		LocationWrapper locationWrapper = (LocationWrapper) (pst.get(PAGE_STATE_TABLE_KEY_MWQS_BY_LOCATIONS));
		String locationLabel = "";
		if (locationWrapper == null)
			return locationLabel;

		switch (locationLevel) {
		case REALM_TYPE_ANY:
			return locationLabel;
		case REALM_TYPE_REGION: {
			locationLabel = BXResources.getPickListDescription(this.theSessionState.getUserInstitutionId(), "REGIONPROFILE", locationWrapper.getRegionProfileId(), theSessionState.getLanguageId());
			return locationLabel;
		}
		case REALM_TYPE_BRANCH: {
			//String region = BXResources.getPickListDescription(this.theSessionState.getUserInstitutionId(), "REGIONPROFILE", locationWrapper.getRegionProfileId(), theSessionState.getLanguageId());
			locationLabel = BXResources.getPickListDescription(this.theSessionState.getUserInstitutionId(), "BRANCHPROFILE", locationWrapper.getBranchProfileId(), theSessionState.getLanguageId());
			//// Currently we are using only branch label. Down the road when the group part
			//// will be used it could be customized based on clients' desire.
			////locationLabel = region + "/" + branch;
			//locationLabel = branch;
			return locationLabel;
		}
		case REALM_TYPE_GROUP: {
			String region = BXResources.getPickListDescription(this.theSessionState.getUserInstitutionId(), "REGIONPROFILE", locationWrapper.getRegionProfileId(), theSessionState.getLanguageId());
			String branch = BXResources.getPickListDescription(this.theSessionState.getUserInstitutionId(), "BRANCHPROFILE", locationWrapper.getBranchProfileId(), theSessionState.getLanguageId());
			String group = BXResources.getPickListDescription(this.theSessionState.getUserInstitutionId(), "GROUPPROFILE", locationWrapper.getGroupProfileId(), theSessionState.getLanguageId());
			locationLabel = region + "/" + branch + "/" + group;
			return locationLabel;
		}
		default:
			return locationLabel;
		}
	}

	//This is a implementation of the Task view accessibility logic required by TD.
	//
	//		If UserProfile.UserType = 'System Administrator'
	//			All tasks/deals are available.
	//		ElseIf UserProfile = ManagerID in one of the Region Profiles
	//			All tasks/deals for that region are available.
	//		ElseIf UserProfile = ManagerID in one of the Branch Profiles
	//			All tasks/deals for that branch are available.
	//		ElseIf UserProfile = SuperviserID in one of the Group Profiles
	//			All tasks/deals for that group are available.
	//		Else
	//			nothing can be displayed.
	private String addLocationCriteria(Hashtable pst, int narrower) {
		if (pst == null)
			return "";
		LocationWrapper locationWrapper = (LocationWrapper) (pst.get(PAGE_STATE_TABLE_KEY_MWQS_BY_LOCATIONS));
		String sqlStr = "";
		if (locationWrapper == null)
			return sqlStr;

		switch (narrower) {
		case REALM_TYPE_ANY:
			return sqlStr;
		case REALM_TYPE_REGION: {
			sqlStr = " AND ";
			sqlStr += ("(BRANCHPROFILE.REGIONPROFILEID = " + locationWrapper.getRegionProfileId() + ")");
			return sqlStr;
		}
		case REALM_TYPE_BRANCH: {
			sqlStr = " AND ";
			sqlStr += ("(BRANCHPROFILE.REGIONPROFILEID = " + locationWrapper.getRegionProfileId() + ")");
			sqlStr += " AND ";
			sqlStr += ("(BRANCHPROFILE.BRANCHPROFILEID = " + locationWrapper.getBranchProfileId() + ")");
			return sqlStr;
		}
		case REALM_TYPE_GROUP: {
			sqlStr = " AND ";
			sqlStr += ("(BRANCHPROFILE.REGIONPROFILEID = " + locationWrapper.getRegionProfileId() + ")");
			sqlStr += " AND ";
			sqlStr += ("(BRANCHPROFILE.BRANCHPROFILEID = " + locationWrapper.getBranchProfileId() + ")");
			sqlStr += " AND ";
			sqlStr += ("(GROUPPROFILE.GROUPPROFILEID = " + locationWrapper.getGroupProfileId() + ")");
			return sqlStr;
		}
		default:
			return sqlStr;
		}
	}

	private void getLocationCriteria(Hashtable pst) {
		PageEntry pg = theSessionState.getCurrentPage();
		SessionResourceKit srk = getSessionResourceKit();
		int userId = pg.getPageMosUserId();
		try {
			Map<Integer, LocationWrapper> locationWrappers = new HashMap<Integer, LocationWrapper>();
			Map<Integer, LocationCriteriaHelper> locationCriteriaHelpers = new HashMap<Integer, LocationCriteriaHelper>();
			for (Integer institutionId : theSessionState.institutionList()) {
				int groupProfileId = theSessionState.getGroupProfileId(institutionId);
				GroupProfile group = new GroupProfile(srk, groupProfileId);
				int branchProfileId = group.getBranchProfileId();
				BranchProfile branch = new BranchProfile(srk, branchProfileId);
				int regionProfileId = branch.getRegionProfileId();
				RegionProfile region = new RegionProfile(srk, regionProfileId);
				//// Create wrapper object.
				LocationWrapper lw = new LocationWrapper();
				lw.setRegionProfileId(regionProfileId);
				lw.setBranchProfileId(branchProfileId);
				lw.setGroupProfileId(groupProfileId);
				locationWrappers.put(institutionId, lw);
				LocationCriteriaHelper location = new LocationCriteriaHelper(REALM_TYPE_ANY, 0);
				//// BX: finalize with the Product Team the userId relationship for this
				//// CR: whether it is user dependant or not.
				if (region.getRegionManagerUserId() == userId) {
					location = new LocationCriteriaHelper(REALM_TYPE_REGION, regionProfileId);
					logger.trace("MWQ@getLocationCriteria::CaseRegionManagerUserId");
				} else if (branch.getBranchManagerUserId() == userId) {
					location = new LocationCriteriaHelper(REALM_TYPE_BRANCH, branchProfileId);
					logger.trace("MWQ@getLocationCriteria::CaseBranchManagerUserId");
				} else if (group.getSupervisorUserId() == userId) {
					location = new LocationCriteriaHelper(REALM_TYPE_GROUP, groupProfileId);
					logger.trace("MWQ@getLocationCriteria::CaseSupervisorUserId");

				}
				locationCriteriaHelpers.put(institutionId, location);
			}
			pst.put(PAGE_STATE_TABLE_KEY_MWQS_BY_LOCATIONS, locationWrappers);
			pst.put("locationCriteria", locationCriteriaHelpers);
		} catch (Exception e) {
			logger.error(e);
			setStandardFailMessage();
		}
	}

	//TODO: move to constructor
	private int getRealm() {
		String filterTasks = PropertiesCache.getInstance().getProperty(theSessionState.getUserInstitutionId(), "com.basis100.masterworkqueue.filtertasks", "N");
		if ("R".equals(filterTasks)) {
			return 1;
		}
		if ("B".equals(filterTasks)) {
			return 2;
		}
		if ("G".equals(filterTasks)) {
			return 3;
		}
		return 0;
	}

	// Method to sync the Current Displayed Row between the Repeated Object and the CursorInfo.
	public void checkAndSyncDOWithCursorInfo(RequestHandlingTiledViewBase theRepeat) {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds = (PageCursorInfo) pst.get(PAGE_STATE_TABLE_KEY_MWQS_PCINAME);
		try {
			((pgMWorkQueueSearchRepeated1TiledView) theRepeat).setCurrentDisplayOffset(tds.getFirstDisplayRowNumber() - 1);
		} catch (Exception mce) {
			logger.error("MWQH@checkAndSyncDOWithCursorInfo::Exception: ");
			logger.error(mce);
		}
	}

	/**
	 * According to SCR#750, both <i>TaskStatus</i> and <i>HoldReason</i> should share the same
	 * TextField component for displaying. Usually, the TextField is used to display TaskStatus.
	 * When the DEAL.HOLDREASONID is not <i>0</i>,  however, it will be used to display
	 * HoldReason description. This method changes the binding for that TextField to HoldReason
	 * instead of TaskStatus.
	 */
	protected void setTaskStatusWithHoldReason() throws ModelControlException {
		MasterWorkQueueSearchModel doMWQ = (MasterWorkQueueSearchModel) (RequestManager.getRequestContext().getModelManager().getModel(MasterWorkQueueSearchModel.class));
		String holdReason = null;
		int holdReasonId = 0;
		String taskStatus = null;
		BigDecimal instId = doMWQ.getDfInstitutionId();
		if (doMWQ.getSize() > 0) {
			Object dfHoldReasonValue = doMWQ.getValue(MasterWorkQueueSearchModelImpl.FIELD_DFHOLDREASONDESCRIPTION);
			Object dfHoldReasonIdValue = doMWQ.getValue(MasterWorkQueueSearchModelImpl.FIELD_DFHOLDREASONID);
			Object dfTaskStatusValue = doMWQ.getValue(MasterWorkQueueSearchModelImpl.FIELD_DFSTATUSDESC);

			if (dfHoldReasonValue == null) {
				holdReason = null;
			} else {
				holdReason = dfHoldReasonValue.toString();
			}
			if (dfHoldReasonIdValue == null) {
				throw new ModelControlException("Invalide HOLDREASONID found.");
			} else {
				holdReasonId = new Integer(dfHoldReasonIdValue.toString()).intValue();
			}
			if (dfTaskStatusValue == null) {
				throw new ModelControlException("Invalide TASKSTATUS found.");
			} else {
				taskStatus = dfTaskStatusValue.toString();
			}
		} else {
			logger.info("@MasterWorkQueueSearchHandler.setTaskStatusWithHoldReason: There is no task for this user.");
		}

		// set TaskStatus with HoldReason if holdReasonId is not 0.
		if ((holdReasonId == 0) || (holdReason == null)) {
			// display with TaskStatus
			getCurrNDPage().setDisplayFieldValue("Repeated1/stTaskStatus", taskStatus);
			// keep the default style.
			getCurrNDPage().setDisplayFieldValue("Repeated1/taskStatusOrHoldReasonStyle", "");
		} else {
			// display with HoldReason
			long currentTime = System.currentTimeMillis();
			Timestamp dfDueTime = (Timestamp) doMWQ.getValue(MasterWorkQueueSearchModelImpl.FIELD_DFDUETIMESTAMP);
			long dueTime = dfDueTime.getTime();

			//if (!onHoldNotDisplay(instId.intValue()) || dueTime > currentTime) {
			if (!"Y".equalsIgnoreCase(PropertiesCache.getInstance().getProperty(instId.intValue(), "deal.onhold.stop.message.display.when.task.expires", "N")) || dueTime > currentTime) {
				String strOnHold = BXResources.getGenericMsg("ON_HOLD_LABEL", theSessionState.getLanguageId());
				getCurrNDPage().setDisplayFieldValue("Repeated1/stTaskStatus", strOnHold + " / " + holdReason);
				// change style.
				getCurrNDPage().setDisplayFieldValue("Repeated1/taskStatusOrHoldReasonStyle", "color: #FF0000;");
			}

		}
	}

	/*
	 * method to generate criteria for branch
	 */
	private String getInstitutionCriteria(Map<Integer, Integer> branchMap) {
		Iterator<Integer> institutionIds = branchMap.keySet().iterator();
		int size = branchMap.size();
		StringBuffer userBuffer = new StringBuffer();
		for (int i = 0; i < size; i++) {
			Integer branchId = institutionIds.next();
			int branchIdVal = branchId.intValue();
			int instId = ((Integer) branchMap.get(branchId)).intValue();
			if (i > 0)
				userBuffer.append(" OR ");
			userBuffer.append("(BRANCHPROFILE.branchProfileID = " + branchIdVal + " AND BRANCHPROFILE.institutionProfileID = " + instId + ")");
		}
		if (size > 1) {
			userBuffer.insert(0, "(");
			userBuffer.append(")");
		}
		if (size > 0)
			userBuffer.insert(0, " AND ");
		return userBuffer.toString();
	}

	/*
	 * method to generate criteria string for taskname
	 */
	private String getTaskNameCriteria(String taskNameFilter) {
		Map<Integer, Integer> taskMap = new HashMap<Integer, Integer>();
		String taskName = taskNameFilter.substring(2, taskNameFilter.length() - 2);
		String[] result = taskName.split("\\}, \\{");
		for (int x = 0; x < result.length; x++) {
			String[] inner = result[x].split("=");
			for (int i = 0; i < inner.length; i++) {
				taskMap.put(new Integer(inner[0]), new Integer(inner[1]));
			}
		}
		Iterator<Integer> institutionIds = taskMap.keySet().iterator();
		int size = taskMap.size();
		StringBuffer userBuffer = new StringBuffer();

		for (int i = 0; i < size; i++) {
			Integer institutionId = institutionIds.next();
			int instId = institutionId.intValue();
			int taskId = ((Integer) taskMap.get(institutionId)).intValue();
			if (i > 0)
				userBuffer.append(" OR ");
			userBuffer.append("(ASSIGNEDTASKSWORKQUEUE.taskID = " + taskId + " AND ASSIGNEDTASKSWORKQUEUE.institutionProfileID = " + instId + ")");
		}
		if (size > 1) {
			userBuffer.insert(0, "(");
			userBuffer.append(")");
		}
		if (size > 0)
			userBuffer.insert(0, " AND ");
		return userBuffer.toString();
	}

	public boolean checkDisplayThisRow(String cursorName) {
		try {
			PageEntry pg = theSessionState.getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			PageCursorInfo tds = (PageCursorInfo) pst.get(cursorName);
			if (tds.getTotalRows() <= 0) {
				return false;
			}
			return true;
		} catch (Exception e) {
			logger.error("Exception @checkDisplayThisRow: Section " + cursorName);
			logger.error(e);
			return false;
		}
	}

	public void setTaskCountByPriority(int rowNdx, Integer priorityId) {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		String taskCount = "0";
		String sqlStr = "";
		String strCrit = "";

		try {
			doCountTasksbyPriorityModel doPSum = (doCountTasksbyPriorityModel) (RequestManager.getRequestContext().getModelManager().getModel(doCountTasksbyPriorityModel.class));
			((doCountTasksbyPriorityModelImpl) doPSum).clearUserWhereCriteria();

			//// IMPORTANT: AddDynamicStrCriterion() method has no analog in JATO
			//// Model search criteria. To simulate it we need first to create/execute
			//// the appropriate sql and add the regular JATO addUserWhereCriterion
			//// after this. This approach should be implemented in the
			//// PartySearchHandler as well.
			////doPSum.addDynamicStrCriterion(" ASSIGNEDTASKSWORKQUEUE.PRIORITYID = " + priorityId + " ");
			sqlStr += ("(ASSIGNEDTASKSWORKQUEUE.PRIORITYID = " + priorityId.toString() + ")");

			//// Because we could not to append the theModel.STATIC_WHERE_CRITERIA
			//// this method is eliminated and filter criteria is set up right here.
			////applyFilterCriteria(doPSum);
			sqlStr += " AND DEAL.COPYTYPE <> 'T'";

			strCrit = (String) pst.get(PAGE_STATE_TABLE_KEY_MWQS_CRITERIA);

			if ((strCrit != null) && (strCrit.trim().length() > 0)) {
				sqlStr += (" AND " + strCrit);
			}

			if (!sqlStr.trim().equals("")) {
				sqlStr += (" AND " + getPageConditions(pg, pst));
			} else {
				sqlStr += getPageConditions(pg, pst);
			}

			String str = doCountTasksbyPriorityModelImpl.STATIC_WHERE_CRITERIA + " AND " + sqlStr;
			doCountTasksbyPriorityModelImpl doPSumImpl = (doCountTasksbyPriorityModelImpl) doPSum;
			doPSumImpl.setStaticWhereCriteriaString(str);

			logger.debug("MWQH@setTaskCountByPriority::SQL before execute = " + doPSumImpl.getSelectSQL());

			ResultSet rs = doPSum.executeSelect(null);

			if ((rs != null) && (doPSum.getSize() > 0)) {
				taskCount = doPSum.getValue(doCountTasksbyPriorityModelImpl.FIELD_DFPRIORITYCOUNT).toString();
			} else {
				logger.error("MWQH@setTaskCountByPriority::Problem when getting the TaskCount info !!");
			}
		} catch (Exception e) {
			logger.error(e);
			logger.error("MWQH@setTaskCountByPriority::Index = " + rowNdx + ", PriorityId = " + priorityId);
		}

		getCurrNDPage().setDisplayFieldValue("Repeated2/stPriorityCount", taskCount);
	}

	public void setTaskCountByTaskName(int rowNdx, Integer taskId) {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		String taskCount = "0";
		String sqlStr = "";
		String strCrit = "";

		try {
			doCountTasksByTaskNameModel mTaskNum = (doCountTasksByTaskNameModel) (RequestManager.getRequestContext().getModelManager().getModel(doCountTasksByTaskNameModel.class));

			doCountTasksByTaskNameModelImpl mTaskNumImpl = (doCountTasksByTaskNameModelImpl) mTaskNum;
			mTaskNumImpl.clearUserWhereCriteria();

			StringBuffer sqlBuff = new StringBuffer("(ASSIGNEDTASKSWORKQUEUE.TASKID = ");
			sqlBuff.append(taskId);
			sqlBuff.append(") ");

			sqlBuff.append(" AND DEAL.COPYTYPE <> 'T'");

			strCrit = (String) pst.get(PAGE_STATE_TABLE_KEY_MWQS_CRITERIA);

			if ((strCrit != null) && (strCrit.trim().length() > 0)) {
				sqlBuff.append(" AND ");
				sqlBuff.append(strCrit);
			}

			sqlStr += sqlBuff.toString();

			if (!sqlStr.trim().equals("")) {
				sqlStr += (" AND " + getPageConditions(pg, pst));
			} else {
				sqlStr += getPageConditions(pg, pst);
			}

			mTaskNumImpl.setStaticWhereCriteriaString(doCountTasksByTaskNameModelImpl.STATIC_WHERE_CRITERIA + " AND " + sqlStr);

			logger.debug("MWQH@setTaskCountByTaskName::SQL before execute = " + mTaskNumImpl.getSelectSQL());

			ResultSet rs = mTaskNum.executeSelect(null);

			if ((rs != null) && (mTaskNum.getSize() > 0)) {
				taskCount = mTaskNum.getValue(doCountTasksByTaskNameModelImpl.FIELD_DFTASKNAMECOUNT).toString();
			} else {
				logger.error("MWQH@setTaskCountByTaskName::Problem when getting the TaskCount info !!");
			}
		} catch (Exception e) {
			logger.error(e);
			logger.error("MWQH@setTaskCountByTaskName::Index = " + rowNdx + ", TaskId = " + taskId);
		}

		getCurrNDPage().setDisplayFieldValue("Repeated3/stTaskNameCount", taskCount);
	}

	//TODO: remove this kind of madness
	public class LocationWrapper {
		private int regionProfileId;
		private int branchProfileId;
		private int groupProfileId;

		public int getRegionProfileId() {
			return regionProfileId;
		}

		public int getBranchProfileId() {
			return branchProfileId;
		}

		public int getGroupProfileId() {
			return groupProfileId;
		}

		public void setRegionProfileId(int aRegionProfileId) {
			regionProfileId = aRegionProfileId;
		}

		public void setBranchProfileId(int aBranchProfileId) {
			branchProfileId = aBranchProfileId;
		}

		public void setGroupProfileId(int aGroupProfileId) {
			groupProfileId = aGroupProfileId;
		}
	}

	public class LocationCriteriaHelper extends Vector {
		public LocationCriteriaHelper(int realmType, int realmId) {
			clear();
			addElement(new Integer(realmType));
			addElement(new Integer(realmId));
		}

		public int getRealmType() {
			return new Integer(get(0).toString()).intValue();
		}

		public int getRealmId() {
			return new Integer(get(1).toString()).intValue();
		}
	}

	void changeDisplayRowsFlag(boolean display) {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		pst.put(PAGE_STATE_TABLE_KEY_MWQS_DISPLAY_ROWS, Boolean.valueOf(display));
	}
	
	boolean isRowsDisplayed(){
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		Boolean display = (Boolean) pst.get(PAGE_STATE_TABLE_KEY_MWQS_DISPLAY_ROWS);
		return (display != null && display.booleanValue());
	}

    
    //FXP30170, 4.4, Oct 08, 2010, saving/resetting screen scroll position  -- start
    private void saveWindowScrollPosition(){
    	
    	PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		if (pst == null)
		{
			pst = new Hashtable();
			pg.setPageStateTable(pst);
		}
    	pst.put(pgMWorkQueueSearchViewBean.CHILD_TXWINDOWSCROLLPOSITION, "");
    	
    	ViewBean thePage = getCurrNDPage();
    	HttpServletRequest request = thePage.getRequestContext().getRequest();
    	String scrollPos = request.getParameter(pgMWorkQueueSearchViewBean.CHILD_TXWINDOWSCROLLPOSITION);
    	
    	if(scrollPos == null || scrollPos.length() < 1)return; // do nothing
    	pst.put(pgMWorkQueueSearchViewBean.CHILD_TXWINDOWSCROLLPOSITION, scrollPos);
    }
    
    private void applyWindowScrollPosition(){
    	    	
    	PageEntry pg = theSessionState.getCurrentPage();
    	Hashtable pst = pg.getPageStateTable();
    	if (pst == null) return; // do nothing
    	
    	String scrollPos = (String)pst.get(pgMWorkQueueSearchViewBean.CHILD_TXWINDOWSCROLLPOSITION);
    	pst.put(pgMWorkQueueSearchViewBean.CHILD_TXWINDOWSCROLLPOSITION, ""); // clean
    	if (scrollPos == null || scrollPos.length() < 1) return; //do nothing
    	
    	ViewBean thePage = getCurrNDPage();
    	HttpServletRequest request = thePage.getRequestContext().getRequest();
    	request.setAttribute(pgMWorkQueueSearchViewBean.CHILD_TXWINDOWSCROLLPOSITION, scrollPos);
    	
    }
    //FXP30170, 4.4, Oct 08, 2010, saving/resetting screen scroll position  -- start
    
}
