package mosApp.MosSystem;
/**
 * Nov 13, 2008, FXP23399 MCM changed optionList for Status combo to be populated all the time.
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import com.basis100.log.SysLogger;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.StaticTextField;


/**
 *
 *
 *
 */
public class pgDocTrackingRepeated2TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDocTrackingRepeated2TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(100);
		setPrimaryModelClass( doDocTrackOrderedModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStOrdDocumentLabel().setValue(CHILD_STORDDOCUMENTLABEL_RESET_VALUE);
		getCbOrdDocStatus().setValue(CHILD_CBORDDOCSTATUS_RESET_VALUE);
		getStOrdDocStatusDate().setValue(CHILD_STORDDOCSTATUSDATE_RESET_VALUE);
		getStOrdByPass().setValue(CHILD_STORDBYPASS_RESET_VALUE);
		getStOrdDueDate().setValue(CHILD_STORDDUEDATE_RESET_VALUE);
		getStOrdResponsibility().setValue(CHILD_STORDRESPONSIBILITY_RESET_VALUE);
		getHdOrdDocTrackId().setValue(CHILD_HDORDDOCTRACKID_RESET_VALUE);
		getHdOrdCopyId().setValue(CHILD_HDORDCOPYID_RESET_VALUE);
		getHdOrdRoleId().setValue(CHILD_HDORDROLEID_RESET_VALUE);
		getHdOrdRequestDate().setValue(CHILD_HDORDREQUESTDATE_RESET_VALUE);
		getHdOrdStatusDate().setValue(CHILD_HDORDSTATUSDATE_RESET_VALUE);
		getHdOrdDocStatus().setValue(CHILD_HDORDDOCSTATUS_RESET_VALUE);
		getHdOrdDocText().setValue(CHILD_HDORDDOCTEXT_RESET_VALUE);
		getHdOrdResponsibility().setValue(CHILD_HDORDRESPONSIBILITY_RESET_VALUE);
		getHdOrdDocumentLabel().setValue(CHILD_HDORDDOCUMENTLABEL_RESET_VALUE);
		getBtOrdDetails().setValue(CHILD_BTORDDETAILS_RESET_VALUE);
    //// One one more hidden field added to escape the over-massaging of
    //// an appropriate model in the Handler.
		getHdByPass().setValue(CHILD_HDBYPASS_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STORDDOCUMENTLABEL,StaticTextField.class);
		registerChild(CHILD_CBORDDOCSTATUS,ComboBox.class);
		registerChild(CHILD_STORDDOCSTATUSDATE,StaticTextField.class);
		registerChild(CHILD_STORDBYPASS,StaticTextField.class);
		registerChild(CHILD_STORDDUEDATE,StaticTextField.class);
		registerChild(CHILD_STORDRESPONSIBILITY,StaticTextField.class);
		registerChild(CHILD_HDORDDOCTRACKID,HiddenField.class);
		registerChild(CHILD_HDORDCOPYID,HiddenField.class);
		registerChild(CHILD_HDORDROLEID,HiddenField.class);
		registerChild(CHILD_HDORDREQUESTDATE,HiddenField.class);
		registerChild(CHILD_HDORDSTATUSDATE,HiddenField.class);
		registerChild(CHILD_HDORDDOCSTATUS,HiddenField.class);
		registerChild(CHILD_HDORDDOCTEXT,HiddenField.class);
		registerChild(CHILD_HDORDRESPONSIBILITY,HiddenField.class);
		registerChild(CHILD_HDORDDOCUMENTLABEL,HiddenField.class);
		registerChild(CHILD_BTORDDETAILS,Button.class);
    //// One one more hidden field added to escape the over-massaging of
    //// an appropriate model in the Handler.
		registerChild(CHILD_HDBYPASS,HiddenField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
//				modelList.add(getdoDocTrackOrderedModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();
    boolean ret = true;

    int rowNum = this.getTileIndex();

		if (movedToRow)
		{
      // Put migrated repeated_onBeforeRowDisplayEvent code here
      DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();

      handler.pageGetState(this.getParentViewBean());

      //logger = SysLog.getSysLogger("pgDTR2TV");
      //logger.debug("pgDTR2TV@nextTile::rowNum: " + rowNum);

      ret = handler.checkDisplayThisRow("Repeated2");
      //logger.debug("pgDTR2TV@nextTile::RetValue: " + ret);

      QueryModelBase theObj =(QueryModelBase) getdoDocTrackOrderedModel();

      //logger.debug("pgDTR2TV@nextTile::Size: " + theObj.getSize());
      //logger.debug("pgDTR2TV@nextTile::Location: " + theObj.getLocation());

      if (theObj.getNumRows() > 0 && ret == true)
      {
          handler.populateDisplayRow(rowNum,
                                        "Repeated2",
                                        doDocTrackOrderedModel.class,
                                        DocTrackingHandler.SECTION_ORDERED);
      }

      handler.pageSaveState();

      return ret;
		}

		return movedToRow;


		// The following code block was migrated from the Repeated2_onBeforeRowDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		int retval = SKIP;

		DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		if ((retval = handler.checkDisplayThisRow("Repeated2")) == PROCEED) handler.populateDisplayRow(event.getRowIndex(), "Repeated2", "doDocTrackOrdered", DocTrackingHandler.SECTION_ORDERED);

		handler.pageSaveState();

		return retval;
		*/
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STORDDOCUMENTLABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDocTrackOrderedModel(),
				CHILD_STORDDOCUMENTLABEL,
				doDocTrackOrderedModel.FIELD_DFDOCUMENTLABEL,
				CHILD_STORDDOCUMENTLABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBORDDOCSTATUS))
		{
			ComboBox child = new ComboBox( this,
				getdoDocTrackOrderedModel(),
				CHILD_CBORDDOCSTATUS,
				doDocTrackOrderedModel.FIELD_DFDOCSTATUSID,
				CHILD_CBORDDOCSTATUS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			//FXP23399 MCM Nov 13, 2008, being thread un-safe, this field should be populted by handler
			//child.setOptions(cbOrdDocStatusOptions);
			return child;
		}
		else
		if (name.equals(CHILD_STORDDOCSTATUSDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDocTrackOrderedModel(),
				CHILD_STORDDOCSTATUSDATE,
				doDocTrackOrderedModel.FIELD_DFSTATUSCHANGEDATE,
				CHILD_STORDDOCSTATUSDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STORDBYPASS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STORDBYPASS,
				CHILD_STORDBYPASS,
				CHILD_STORDBYPASS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STORDDUEDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDocTrackOrderedModel(),
				CHILD_STORDDUEDATE,
				doDocTrackOrderedModel.FIELD_DFDOCDUEDATE,
				CHILD_STORDDUEDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STORDRESPONSIBILITY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDocTrackOrderedModel(),
				CHILD_STORDRESPONSIBILITY,
				doDocTrackOrderedModel.FIELD_DFRESPONSIBILITY,
				CHILD_STORDRESPONSIBILITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDORDDOCTRACKID))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackOrderedModel(),
				CHILD_HDORDDOCTRACKID,
				doDocTrackOrderedModel.FIELD_DFDOCTRACKID,
				CHILD_HDORDDOCTRACKID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDORDCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackOrderedModel(),
				CHILD_HDORDCOPYID,
				doDocTrackOrderedModel.FIELD_DFCOPYID,
				CHILD_HDORDCOPYID_RESET_VALUE,
				null);
    return child;
		}
		else
		if (name.equals(CHILD_HDORDROLEID))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackOrderedModel(),
				CHILD_HDORDROLEID,
				doDocTrackOrderedModel.FIELD_DFROLEID,
				CHILD_HDORDROLEID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDORDREQUESTDATE))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackOrderedModel(),
				CHILD_HDORDREQUESTDATE,
				doDocTrackOrderedModel.FIELD_DFREQUESTDATE,
				CHILD_HDORDREQUESTDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDORDSTATUSDATE))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackOrderedModel(),
				CHILD_HDORDSTATUSDATE,
				doDocTrackOrderedModel.FIELD_DFSTATUSCHANGEDATE,
				CHILD_HDORDSTATUSDATE_RESET_VALUE,
      null);
			return child;
		}
		else
		if (name.equals(CHILD_HDORDDOCSTATUS))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackOrderedModel(),
				CHILD_HDORDDOCSTATUS,
				doDocTrackOrderedModel.FIELD_DFSTATUS,
				CHILD_HDORDDOCSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDORDDOCTEXT))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackOrderedModel(),
				CHILD_HDORDDOCTEXT,
				doDocTrackOrderedModel.FIELD_DFDOCUMENTTEXT,
				CHILD_HDORDDOCTEXT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDORDRESPONSIBILITY))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackOrderedModel(),
				CHILD_HDORDRESPONSIBILITY,
				doDocTrackOrderedModel.FIELD_DFRESPONSIBILITY,
				CHILD_HDORDRESPONSIBILITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDORDDOCUMENTLABEL))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackOrderedModel(),
				CHILD_HDORDDOCUMENTLABEL,
				doDocTrackOrderedModel.FIELD_DFDOCUMENTLABEL,
				CHILD_HDORDDOCUMENTLABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTORDDETAILS))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTORDDETAILS,
				CHILD_BTORDDETAILS,
				CHILD_BTORDDETAILS_RESET_VALUE,
				null);
				return child;

		}
    //// One one more hidden field added to escape the over-massaging of
    //// an appropriate model in the Handler.
		else
		if (name.equals(CHILD_HDBYPASS))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackOrderedModel(),
				CHILD_HDBYPASS,
				doDocTrackOrderedModel.FIELD_DFBYPASS,
				CHILD_HDBYPASS_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStOrdDocumentLabel()
	{
		return (StaticTextField)getChild(CHILD_STORDDOCUMENTLABEL);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbOrdDocStatus()
	{
		return (ComboBox)getChild(CHILD_CBORDDOCSTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStOrdDocStatusDate()
	{
		return (StaticTextField)getChild(CHILD_STORDDOCSTATUSDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStOrdByPass()
	{
		return (StaticTextField)getChild(CHILD_STORDBYPASS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStOrdDueDate()
	{
		return (StaticTextField)getChild(CHILD_STORDDUEDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStOrdResponsibility()
	{
		return (StaticTextField)getChild(CHILD_STORDRESPONSIBILITY);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdOrdDocTrackId()
	{
		return (HiddenField)getChild(CHILD_HDORDDOCTRACKID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdOrdCopyId()
	{
		return (HiddenField)getChild(CHILD_HDORDCOPYID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdOrdRoleId()
	{
		return (HiddenField)getChild(CHILD_HDORDROLEID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdOrdRequestDate()
	{
		return (HiddenField)getChild(CHILD_HDORDREQUESTDATE);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdOrdStatusDate()
	{
		return (HiddenField)getChild(CHILD_HDORDSTATUSDATE);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdOrdDocStatus()
	{
		return (HiddenField)getChild(CHILD_HDORDDOCSTATUS);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdOrdDocText()
	{
		return (HiddenField)getChild(CHILD_HDORDDOCTEXT);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdOrdResponsibility()
	{
		return (HiddenField)getChild(CHILD_HDORDRESPONSIBILITY);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdOrdDocumentLabel()
	{
		return (HiddenField)getChild(CHILD_HDORDDOCUMENTLABEL);
	}

	/**
	 *
	 *    One one more hidden field added to escape the over-massaging of
   *    an appropriate model in the Handler.
	 */
	public HiddenField getHdByPass()
	{
		return (HiddenField)getChild(CHILD_HDBYPASS);
	}

	/**
	 *
	 *
	 */
	public void handleBtOrdDetailsRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this.getParentViewBean());

		handler.navigateToDetail(DocTrackingHandler.SECTION_ORDERED, handler.getRowNdxFromWebEventMethod(event));

		handler.postHandlerProtocol();

		// The following code block was migrated from the btOrdDetails_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.navigateToDetail(DocTrackingHandler.SECTION_ORDERED, handler.getRowNdxFromWebEventMethod(event));

		handler.postHandlerProtocol();

		return;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtOrdDetails()
	{
		return (Button)getChild(CHILD_BTORDDETAILS);
	}


	/**
	 *
	 *
	 */
	public doDocTrackOrderedModel getdoDocTrackOrderedModel()
	{
		if (doDocTrackOrdered == null)
			doDocTrackOrdered = (doDocTrackOrderedModel) getModel(doDocTrackOrderedModel.class);
		return doDocTrackOrdered;
	}


	/**
	 *
	 *
	 */
	public void setdoDocTrackOrderedModel(doDocTrackOrderedModel model)
	{
			doDocTrackOrdered = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STORDDOCUMENTLABEL="stOrdDocumentLabel";
	public static final String CHILD_STORDDOCUMENTLABEL_RESET_VALUE="";
	public static final String CHILD_CBORDDOCSTATUS="cbOrdDocStatus";
	public static final String CHILD_CBORDDOCSTATUS_RESET_VALUE="";
	//FXP23399 MCM Nov 13, 2008, being thread un-safe, this OptionList should be populted by handler 
	//private static OptionList cbOrdDocStatusOptions=new OptionList(new String[]{},new String[]{});
	public static final String CHILD_STORDDOCSTATUSDATE="stOrdDocStatusDate";
	public static final String CHILD_STORDDOCSTATUSDATE_RESET_VALUE="";
	public static final String CHILD_STORDBYPASS="stOrdByPass";
	public static final String CHILD_STORDBYPASS_RESET_VALUE="";
	public static final String CHILD_STORDDUEDATE="stOrdDueDate";
	public static final String CHILD_STORDDUEDATE_RESET_VALUE="";
	public static final String CHILD_STORDRESPONSIBILITY="stOrdResponsibility";
	public static final String CHILD_STORDRESPONSIBILITY_RESET_VALUE="";
	public static final String CHILD_HDORDDOCTRACKID="hdOrdDocTrackId";
	public static final String CHILD_HDORDDOCTRACKID_RESET_VALUE="";
	public static final String CHILD_HDORDCOPYID="hdOrdCopyId";
	public static final String CHILD_HDORDCOPYID_RESET_VALUE="";
	public static final String CHILD_HDORDROLEID="hdOrdRoleId";
	public static final String CHILD_HDORDROLEID_RESET_VALUE="";
	public static final String CHILD_HDORDREQUESTDATE="hdOrdRequestDate";
	public static final String CHILD_HDORDREQUESTDATE_RESET_VALUE="";
	public static final String CHILD_HDORDSTATUSDATE="hdOrdStatusDate";
	public static final String CHILD_HDORDSTATUSDATE_RESET_VALUE="";
	public static final String CHILD_HDORDDOCSTATUS="hdOrdDocStatus";
	public static final String CHILD_HDORDDOCSTATUS_RESET_VALUE="";
	public static final String CHILD_HDORDDOCTEXT="hdOrdDocText";
	public static final String CHILD_HDORDDOCTEXT_RESET_VALUE="";
	public static final String CHILD_HDORDRESPONSIBILITY="hdOrdResponsibility";
	public static final String CHILD_HDORDRESPONSIBILITY_RESET_VALUE="";
	public static final String CHILD_HDORDDOCUMENTLABEL="hdOrdDocumentLabel";
	public static final String CHILD_HDORDDOCUMENTLABEL_RESET_VALUE="";
	public static final String CHILD_BTORDDETAILS="btOrdDetails";
	public static final String CHILD_BTORDDETAILS_RESET_VALUE=" ";
  //// One one more hidden field added to escape the over-massaging of
  //// an appropriate model in the Handler.
	public static final String CHILD_HDBYPASS="hdByPass";
	public static final String CHILD_HDBYPASS_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDocTrackOrderedModel doDocTrackOrdered=null;
	private DocTrackingHandler handler=new DocTrackingHandler();
  public SysLogger logger;
}

