package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doLifeDisabilityInsuranceInfoModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();

	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();

	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */

  public java.math.BigDecimal getDfBorrowerId();

	/**
	 *
	 *
	 */
	public String getDfFirstBorrowerName();

	/**
	 *
	 *
	 */
	public void setDfBorrowerFirstName(String value);

	/**
	 *
	 *
	 */
	public String getDfLastBorrowerName();

	/**
	 *
	 *
	 */
	public void setDfBorrowerLastName(String value);

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfLifePercentCoverageReq();

	/**
	 *
	 *
	 */
	public void setDfLifePercentCoverageReq(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public void setDfLifePremium(java.math.BigDecimal value);

	/**
	 *
	 *
	 */

  public java.math.BigDecimal getDfLifePremium();

	/**
	 *
	 *
	 */
	public void setDfDisabilityPremium(java.math.BigDecimal value);

	/**
	 *
	 *
	 */

  public java.math.BigDecimal getDfDisabilityPremium();

	/**
	 *
	 *
	 */
	public void setDfPmntPlusLifeDisability(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfPmntPlusLifeDisability();

	/**
	 *
	 *
	 */
	public void setDfInterestRateIncrement(java.math.BigDecimal value);

	/**
	 *
	 *
	 */

  public java.math.BigDecimal getDfInterestRateIncrement();

	/**
	 *
	 *
	 */
	public void setDfLifeDisabilityPremiumsId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */

  public java.math.BigDecimal getDfLifeDisabilityPremiumsId();

	/**
	 *
	 *
	 */
	public void setDfTotalLoanAmount(java.math.BigDecimal value);

	/**
	 *
	 *
	 */

  public java.math.BigDecimal getDfTotalLoanAmount();

	/**
	 *
	 *
	 */
	public void setDfLenderProfileId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */

  public java.math.BigDecimal getDfLenderProfileId();

	/**
	 *
	 *
	 */
	public String getDfRateCode();

	/**
	 *
	 *
	 */
	public void setDfRateCode(String value);

	/**
	 *
	 *
	 */
	public void setDfLDRate(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfLDRate();

	/**
	 *
	 *
	 */
	public String getDfLifeStatusDesc();

	/**
	 *
	 *
	 */
	public void setDfLifeStatusDesc(String value);

	/**
	 *
	 *
	 */
	public String getDfDisabilityStatusDesc();

	/**
	 *
	 *
	 */
	public void setDfDisabilityStatusDesc(String value);

	/**
	 *
	 *
	 */
	public String getDfLDInsuranceAgeDesc();

	/**
	 *
	 *
	 */
	public void setDfInsuranceAgeDesc(String value);

	/**
	 *
	 *
	 */
	public String getDfBorrowerGenderDesc();

	/**
	 *
	 *
	 */
	public void setDfBorrowerGenderDesc(String value);

	/**
	 *
	 *
	 */
	public void setDfLDInsuranceTypeId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */

  public java.math.BigDecimal getDfLDInsuranceTypeId();

	/**
	 *
	 *
	 */
	public String getDfLDInsuranceTypeDesc();

	/**
	 *
	 *
	 */
	public void setDfInsuranceTypeDesc(String value);

	/**
	 *
	 *
	 */
	public String getDfIPDesc();

	/**
	 *
	 *
	 */
	public void setDfIPDesc(String value);

	/**
	 *
	 *
	 */
	public void setDfAge(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfAge();


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfBorrowerBirthDate();

	/**
	 *
	 *
	 */
	public void setDfBorrowerBirthDate(java.sql.Timestamp value);

	/**
	 *
	 *
	 */
	public String getDfCopyType();

	/**
	 *
	 *
	 */
	public void setDfCopyType(String value);

	/**
	 *
	 *
	 */
	public void setDfCopyID(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public String getDfMiddleBorrowerInitial();

	/**
	 *
	 *
	 */
	public void setDfMiddleFirstInitial(String value);

	/**
	 *
	 *
	 */
	public String getDfPrimaryBorrowerFlag();

	/**
	 *
	 *
	 */
	public void setDfPrimaryBorrowerFlag(String value);

	/**
	 *
	 *
	 */
	public void setDfBorrowerGenderId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfBorrowerGenderId();

	/**
	 *
	 *
	 */
	public void setDfCombinedLDPremium(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfCombindedLDPremium();

	/**
	 *
	 *
	 */
	public void setDfLifeStatusValue(String value);

	/**
	 *
	 *
	 */
  public String getDfLifeStatusValue();

	/**
	 *
	 *
	 */
	public void setDfDisabilityStatusValue(String value);

	/**
	 *
	 *
	 */
  public String getDfDisabilityStatusValue();

	/**
	 *
	 *
	 */
	public String getDfBorrowerSmoker();

	/**
	 *
	 *
	 */
	public void setDfBorrowerSmoker(String value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLifeStatusId();

	/**
	 *
	 *
	 */
	public void setDfLifeStatusId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDisabilityStatusId();

	/**
	 *
	 *
	 */
	public void setDfDisabilityStatusId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public void setDfBorrowerSmokerId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfBorrowerSmokerId();

	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
  public static final String FIELD_DFBORROWERID="dfBorrowerId";
  public static final String FIELD_DFBORROWERFIRSTNAME="dfBorrowerFirstName";
  public static final String FIELD_DFBORROWERMIDDLEINITIAL="dfBorrowerMiddleInitial";
  public static final String FIELD_DFBORROWERLASTNAME="dfBorrowerLastName";
	public static final String FIELD_DFPRIMARYBORROWERFLAG="dfPrimaryBorrowerFlag";
  public static final String FIELD_DFLIFEPERCENTCOVERAGEREQ="dfLifePercentCoverageReq";
  public static final String FIELD_DFLIFEPREMIUM="dfLifePremium";
  public static final String FIELD_DFDISABILITYPREMIUM="dfDisabilityPremium";
	public static final String FIELD_DFCOMBINEDLDPREMIUM="dfCombinedLDPremium";
  public static final String FIELD_DFPMNTPLUSLIFEDISABILITY="dfPmntPlusLifeDisability";
  public static final String FIELD_DFINTERESTRATEINCREMENT="dfInterestRateIncrement";
  public static final String FIELD_DFLIFEDISABILITYPREMIUMSID="dfLifeDisabilityPremiumsId";
  public static final String FIELD_DFTOTALLOANAMOUNT="dfTotalLoanAmount";
  public static final String FIELD_DFLENDERPROFILEID="dfLenderProfileId";
  public static final String FIELD_DFLDRATECODE="dfRateCode";
  public static final String FIELD_DFLDRATE="dfLDRate";
  //public static final String FIELD_DFLDPROFILEDESC="dfLDProfileDesc";"dfLifeDisabilityStatusId"
  public static final String FIELD_DFLIFESTATUSDESC="dfLifeStatusDesc";
  public static final String FIELD_DFDISABILITYSTATUSDESC="dfDisabilityStatusDesc";
  public static final String FIELD_DFLIFESTATUSVALUE="dfLifeStatusValue";
  public static final String FIELD_DFLDISABILITYSTATUSVALUE="dfDisabilityStatusValue";
  public static final String FIELD_DFLDINSURANCEAGEDESC="dfLDInsuranceAgeDesc";
  public static final String FIELD_DFBORROWERGENDERDESC="dfBorrowerGenderDesc";
  public static final String FIELD_DFLDINSURANCETYPEID="dfLDInsuranceTypeId";
  public static final String FIELD_DFIPDESC="dfIPDesc";
  public static final String FIELD_DFLDINSURANCETYPEDESC="dfLDInsuranceTypeDesc";
  public static final String FIELD_DFAGE="dfAge";
  public static final String FIELD_DFBORROWERBIRTHDATE="dfBorrowerBirthDate";
  public static final String FIELD_DFCOPYTYPE="dfCopyType";
  public static final String FIELD_DFBORROWERGENDERID="dfBorrowerGenderId";
  public static final String FIELD_DFSMOKER="dfSmoker";
  public static final String FIELD_DFLIFESTATUSID="dfLifeStatusId";
  public static final String FIELD_DFDISABILITYSTATUSID="dfDisabilityStatusId";
  public static final String FIELD_DFBORROWERSMOKERID="dfBorrowerSmokerId";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

