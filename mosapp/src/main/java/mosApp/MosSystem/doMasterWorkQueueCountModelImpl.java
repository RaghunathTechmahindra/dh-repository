package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;
import com.basis100.picklist.BXResources;

//--> Performance enhancement : elimination of the 2nd call of the DataObject
//--> the DealId, ApplicationId, CopyId and WFTaskId will be stored and retrieved in
//--> the hidden values on each row.
//--> By Billy 16Jan2004


/**
 *
 *
 *
 */
public class doMasterWorkQueueCountModelImpl extends QueryModelBase
  implements doMasterWorkQueueCountModel
{
  /**
   *
   *
   */
  public doMasterWorkQueueCountModelImpl()
  {
    super();
    setDataSourceName(DATA_SOURCE_NAME);
    setSelectSQLTemplate(SELECT_SQL_TEMPLATE);
    setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);
    setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);
    setFieldSchema(FIELD_SCHEMA);

    initialize();
  }


  /**
   *
   *
   */
  public void initialize()
  {
  }


  ////////////////////////////////////////////////////////////////////////////////
  // NetDynamics-migrated events
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
    throws ModelControlException
  {

    // TODO: Migrate specific onBeforeExecute code
    //logger = SysLog.getSysLogger("DOMWQCM");
    //logger.debug("DOMWQPA@afterExecute::Size: " + this.getSize());
    //logger.debug("DOMWQPA@beforeExecute::SQL: " + sql);

    return sql;

  }


  /**
   *
   *
   */
  protected void afterExecute(ModelExecutionContext context, int queryType)
    throws ModelControlException
  {
  }


  /**
   *
   *
   */
  protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
  {
  }


  /**
   *
   *
   */
  public java.math.BigDecimal getDfWQNumOfRows()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFWQNUMOFROWS);
  }


  /**
   *
   *
   */
  public void setDfWQNumOfRows(java.math.BigDecimal value)
  {
    setValue(FIELD_DFWQNUMOFROWS,value);
  }


  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////
  // Custom Methods - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////

  public static final String DATA_SOURCE_NAME="jdbc/orcl";

  public static final String SELECT_SQL_TEMPLATE;
  /**
   * BEGIN FIX FOR Performance Issue on Master Work queue [FXP21615]
   */
  static{
      StringBuffer sql = new StringBuffer();
      sql.append(" SELECT COUNT (*) TOTAL_COUNT ");
      sql.append(" FROM ASSIGNEDTASKSWORKQUEUE ,");
      sql.append("     DEAL, USERPROFILE, GROUPPROFILE, BRANCHPROFILE, CONTACT c1  ");
      sql.append(" __WHERE__ ");
      SELECT_SQL_TEMPLATE = sql.toString();
  }
  
  public static final String MODIFYING_QUERY_TABLE_NAME=
  "ASSIGNEDTASKSWORKQUEUE, DEAL, USERPROFILE, GROUPPROFILE, BRANCHPROFILE, CONTACT c1 ";

  public static final String STATIC_WHERE_CRITERIA;
  static {
      StringBuffer sql = new StringBuffer();
      sql.append(" deal.institutionprofileid =  assignedtasksworkqueue.institutionprofileid  ");
      sql.append(" AND deal.dealid = assignedtasksworkqueue.dealid  "); 
      sql.append(" AND assignedtasksworkqueue.institutionprofileid = userprofile.institutionprofileid  "); 
      sql.append(" AND assignedtasksworkqueue.userprofileid = userprofile.userprofileid   ");       
      sql.append(" AND userprofile.institutionprofileid = groupprofile.institutionprofileid  "); 
      sql.append(" AND userprofile.groupprofileid = groupprofile.groupprofileid  "); 
      sql.append(" AND branchprofile.institutionprofileid = groupprofile.institutionprofileid  "); 
      sql.append(" AND branchprofile.branchprofileid = groupprofile.branchprofileid  ");        
      sql.append(" AND userprofile.institutionprofileid = c1.institutionprofileid  "); 
      sql.append(" AND userprofile.contactid = c1.contactid  "); 
      sql.append(" AND DEAL.SCENARIORECOMMENDED                     = 'Y'");
      sql.append(" AND ASSIGNEDTASKSWORKQUEUE.TASKID                < 50000");
      sql.append(" AND DEAL.COPYTYPE                               <> 'T' ");
      sql.append(" AND c1.copyid                               = 1 ");
      STATIC_WHERE_CRITERIA = sql.toString();
  }
  /**
   * END FIX FOR Performance Issue on Master Work queue [FXP21615]
   */
  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
  public static final String QUALIFIED_COLUMN_DFWQNUMOFROWS="ASSIGNEDTASKSWORKQUEUE.ASSIGNEDTASKSWORKQUEUEID.TOTAL_COUNT";
  public static final String COLUMN_DFWQNUMOFROWS="TOTAL_COUNT";

  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////


  ////////////////////////////////////////////////////////////////////////////
  // Custom Members - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////

  static
  {
    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFWQNUMOFROWS,
        COLUMN_DFWQNUMOFROWS,
        QUALIFIED_COLUMN_DFWQNUMOFROWS,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));
  }
}

