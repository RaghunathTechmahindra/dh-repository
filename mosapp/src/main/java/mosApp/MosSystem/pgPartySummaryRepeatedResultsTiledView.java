package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

import com.basis100.log.*;
import MosSystem.*;

/**
 *
 *
 *
 */
public class pgPartySummaryRepeatedResultsTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgPartySummaryRepeatedResultsTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doPartySummaryModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStPartyType().setValue(CHILD_STPARTYTYPE_RESET_VALUE);
		getStPartyName().setValue(CHILD_STPARTYNAME_RESET_VALUE);
		getStPartyCompanyName().setValue(CHILD_STPARTYCOMPANYNAME_RESET_VALUE);
		getStPartyPhoneNumber().setValue(CHILD_STPARTYPHONENUMBER_RESET_VALUE);
		getStPartyFaxNumber().setValue(CHILD_STPARTYFAXNUMBER_RESET_VALUE);
		getBtDelete().setValue(CHILD_BTDELETE_RESET_VALUE);
		getBtDetails().setValue(CHILD_BTDETAILS_RESET_VALUE);
		getHdPartyProfileId().setValue(CHILD_HDPARTYPROFILEID_RESET_VALUE);
		getHdPartyCopyId().setValue(CHILD_HDPARTYCOPYID_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STPARTYTYPE,StaticTextField.class);
		registerChild(CHILD_STPARTYNAME,StaticTextField.class);
		registerChild(CHILD_STPARTYCOMPANYNAME,StaticTextField.class);
		registerChild(CHILD_STPARTYPHONENUMBER,StaticTextField.class);
		registerChild(CHILD_STPARTYFAXNUMBER,StaticTextField.class);
		registerChild(CHILD_BTDELETE,Button.class);
		registerChild(CHILD_BTDETAILS,Button.class);
		registerChild(CHILD_HDPARTYPROFILEID,HiddenField.class);
		registerChild(CHILD_HDPARTYCOPYID,HiddenField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoPartySummaryModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--DJ_PT_CR--start//
    PartySummaryHandler handler =(PartySummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.checkAndSyncDOWithCursorInfo(this);
		handler.pageSaveState();
    //--DJ_PT_CR--end//

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
      //// Get Row Index first.
      int rowNum = this.getTileIndex();
      boolean ret = true;

		  PartySummaryHandler handler =(PartySummaryHandler) this.handler.cloneSS();
      handler.pageGetState(this.getParentViewBean());

      //logger = SysLog.getSysLogger("pgPSRRTV");

      //logger.debug("pgPSRRTV@nextTile::rowNum: " + rowNum);

      doPartySummaryModelImpl theObj =(doPartySummaryModelImpl) getdoPartySummaryModel();
      //logger.debug("pgPSRRTV@nextTile::Size: " + theObj.getSize());

      if (theObj.getNumRows() > 0)
      {
        //logger.debug("pgPSRRTV@nextTile::Location: " + theObj.getLocation());
        handler.populateRepeatedFields(theObj, rowNum);
      }
      else
        ret = false;

      handler.pageSaveState();

      return ret;
		}

		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STPARTYTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPartySummaryModel(),
				CHILD_STPARTYTYPE,
				doPartySummaryModel.FIELD_DFPARTYTYPE,
				CHILD_STPARTYTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPARTYNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPartySummaryModel(),
				CHILD_STPARTYNAME,
				doPartySummaryModel.FIELD_DFPARTYSHORTNAME,
				CHILD_STPARTYNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPARTYCOMPANYNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPartySummaryModel(),
				CHILD_STPARTYCOMPANYNAME,
				doPartySummaryModel.FIELD_DFPARTYCOMPANY,
				CHILD_STPARTYCOMPANYNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPARTYPHONENUMBER))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPARTYPHONENUMBER,
				CHILD_STPARTYPHONENUMBER,
				CHILD_STPARTYPHONENUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPARTYFAXNUMBER))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPARTYFAXNUMBER,
				CHILD_STPARTYFAXNUMBER,
				CHILD_STPARTYFAXNUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETE,
				CHILD_BTDELETE,
				CHILD_BTDELETE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTDETAILS))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDETAILS,
				CHILD_BTDETAILS,
				CHILD_BTDETAILS_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HDPARTYPROFILEID))
		{
			HiddenField child = new HiddenField(this,
				getdoPartySummaryModel(),
				CHILD_HDPARTYPROFILEID,
				doPartySummaryModel.FIELD_DFPARTYPROFILEID,
				CHILD_HDPARTYPROFILEID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDPARTYCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_HDPARTYCOPYID,
				CHILD_HDPARTYCOPYID,
				CHILD_HDPARTYCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPartyType()
	{
		return (StaticTextField)getChild(CHILD_STPARTYTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPartyName()
	{
		return (StaticTextField)getChild(CHILD_STPARTYNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPartyCompanyName()
	{
		return (StaticTextField)getChild(CHILD_STPARTYCOMPANYNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPartyPhoneNumber()
	{
		return (StaticTextField)getChild(CHILD_STPARTYPHONENUMBER);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPartyFaxNumber()
	{
		return (StaticTextField)getChild(CHILD_STPARTYFAXNUMBER);
	}


	/**
	 *
	 *
	 */
	public void handleBtDeleteRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		PartySummaryHandler handler =(PartySummaryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this.getParentViewBean());

		handler.handleDeleteButton(false, event);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtDelete()
	{
		return (Button)getChild(CHILD_BTDELETE);
	}


	/**
	 *
	 *
	 */
  //--DJ_PT_CR--start//
  //// For Party Type 'Origination Branch' (DJ) do NOT display 'Remove From Deal' button.
	public String endBtDeleteDisplay(ChildContentDisplayEvent event)
	{
    PartySummaryHandler handler =(PartySummaryHandler) this.handler.cloneSS();
    logger = SysLog.getSysLogger("pgPSRRTV");
    logger.debug("pgPSRRTVendBtDelete: stamp1");

		handler.pageGetState(this.getParentViewBean());

    boolean rc = true; //default: generate.

    rc = handler.handleViewRemovePartyButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
  }

  public void setCurrentDisplayOffset(int offset) throws Exception
  {
    setWebActionModelOffset(offset);
    handleWebAction(WebActions.ACTION_REFRESH);
    // Have to re-execute the Model here.  Otherwise, not working
    executeAutoRetrievingModels();
  }

  //--DJ_PT_CR--end//

	/**
	 *
	 *
	 */
	public void handleBtDetailsRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		PartySummaryHandler handler =(PartySummaryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this.getParentViewBean());

		handler.handleDetailsButton(event);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtDetails()
	{
		return (Button)getChild(CHILD_BTDETAILS);
	}


	/**
	 *
	 *
	 */
	public String endBtDetailsDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btDetails_onBeforeHtmlOutputEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		PartySummaryHandler handler =(PartySummaryHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		int rc = handler.reviseDetailsPartyButtonGeneration();

		handler.pageSaveState();

		return rc;
		*/


		return event.getContent();
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdPartyProfileId()
	{
		return (HiddenField)getChild(CHILD_HDPARTYPROFILEID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdPartyCopyId()
	{
		return (HiddenField)getChild(CHILD_HDPARTYCOPYID);
	}


	/**
	 *
	 *
	 */
	public doPartySummaryModel getdoPartySummaryModel()
	{
		if (doPartySummary == null)
			doPartySummary = (doPartySummaryModel) getModel(doPartySummaryModel.class);
		return doPartySummary;
	}


	/**
	 *
	 *
	 */
	public void setdoPartySummaryModel(doPartySummaryModel model)
	{
			doPartySummary = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STPARTYTYPE="stPartyType";
	public static final String CHILD_STPARTYTYPE_RESET_VALUE="";
	public static final String CHILD_STPARTYNAME="stPartyName";
	public static final String CHILD_STPARTYNAME_RESET_VALUE="";
	public static final String CHILD_STPARTYCOMPANYNAME="stPartyCompanyName";
	public static final String CHILD_STPARTYCOMPANYNAME_RESET_VALUE="";
	public static final String CHILD_STPARTYPHONENUMBER="stPartyPhoneNumber";
	public static final String CHILD_STPARTYPHONENUMBER_RESET_VALUE="";
	public static final String CHILD_STPARTYFAXNUMBER="stPartyFaxNumber";
	public static final String CHILD_STPARTYFAXNUMBER_RESET_VALUE="";
	public static final String CHILD_BTDELETE="btDelete";
	public static final String CHILD_BTDELETE_RESET_VALUE="Delete";
	public static final String CHILD_BTDETAILS="btDetails";
	public static final String CHILD_BTDETAILS_RESET_VALUE="Details";
	public static final String CHILD_HDPARTYPROFILEID="hdPartyProfileId";
	public static final String CHILD_HDPARTYPROFILEID_RESET_VALUE="";
	public static final String CHILD_HDPARTYCOPYID="hdPartyCopyId";
	public static final String CHILD_HDPARTYCOPYID_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doPartySummaryModel doPartySummary=null;
	private PartySummaryHandler handler=new PartySummaryHandler();
  public SysLogger logger;

}

