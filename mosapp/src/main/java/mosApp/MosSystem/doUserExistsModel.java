package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doUserExistsModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfUSERPROFILEID();

	
	/**
	 * 
	 * 
	 */
	public void setDfUSERPROFILEID(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfUSERLOGIN();

	
	/**
	 * 
	 * 
	 */
	public void setDfUSERLOGIN(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfUSERTYPEID();

	
	/**
	 * 
	 * 
	 */
	public void setDfUSERTYPEID(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfGROUPPROFILEID();

	
	/**
	 * 
	 * 
	 */
	public void setDfGROUPPROFILEID(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfHigherApproverUerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfHigherApproverUerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfJOINTAPPROVERUSERID();

	
	/**
	 * 
	 * 
	 */
	public void setDfJOINTAPPROVERUSERID(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCONTACTID();

	
	/**
	 * 
	 * 
	 */
	public void setDfCONTACTID(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPROFILESTATUSID();

	
	/**
	 * 
	 * 
	 */
	public void setDfPROFILESTATUSID(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSTANDARDACCESS();

	
	/**
	 * 
	 * 
	 */
	public void setDfSTANDARDACCESS(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBILINGUAL();

	
	/**
	 * 
	 * 
	 */
	public void setDfBILINGUAL(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfUTDESCRIPTION();

	
	/**
	 * 
	 * 
	 */
	public void setDfUTDESCRIPTION(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfCONTACTFIRSTNAME();

	
	/**
	 * 
	 * 
	 */
	public void setDfCONTACTFIRSTNAME(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfCONTACTMIDDLEINITIAL();

	
	/**
	 * 
	 * 
	 */
	public void setDfCONTACTMIDDLEINITIAL(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfCONTACTLASTNAME();

	
	/**
	 * 
	 * 
	 */
	public void setDfCONTACTLASTNAME(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFUSERPROFILEID="dfUSERPROFILEID";
	public static final String FIELD_DFUSERLOGIN="dfUSERLOGIN";
	public static final String FIELD_DFUSERTYPEID="dfUSERTYPEID";
	public static final String FIELD_DFGROUPPROFILEID="dfGROUPPROFILEID";
	public static final String FIELD_DFHIGHERAPPROVERUERID="dfHigherApproverUerId";
	public static final String FIELD_DFJOINTAPPROVERUSERID="dfJOINTAPPROVERUSERID";
	public static final String FIELD_DFCONTACTID="dfCONTACTID";
	public static final String FIELD_DFPROFILESTATUSID="dfPROFILESTATUSID";
	public static final String FIELD_DFSTANDARDACCESS="dfSTANDARDACCESS";
	public static final String FIELD_DFBILINGUAL="dfBILINGUAL";
	public static final String FIELD_DFUTDESCRIPTION="dfUTDESCRIPTION";
	public static final String FIELD_DFCONTACTFIRSTNAME="dfCONTACTFIRSTNAME";
	public static final String FIELD_DFCONTACTMIDDLEINITIAL="dfCONTACTMIDDLEINITIAL";
	public static final String FIELD_DFCONTACTLASTNAME="dfCONTACTLASTNAME";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

