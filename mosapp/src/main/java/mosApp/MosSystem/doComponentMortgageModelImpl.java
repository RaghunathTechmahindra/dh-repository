package mosApp.MosSystem;

import java.math.BigDecimal;
import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 * Title: doComponentMortgageModelImpl
 * <p>
 * Description: Implementation of doCotermponentMortgageModel Interface.
 * @author MCM Impl Team.
 * @version 1.0 09-June-2008 XS_16.7 Initial version
 * @version 1.1 18-June-2008 XS_2.27/28a added fields for componentMortgage section
 *   - modified SELECT_SQL_TEMPLATE, MODIFYING_QUERY_TABLE_NAME,STATIC_WHERE_CRITERIA, 
 * @version 1.2 23-June-2008 XS_16.7 - Modified  STATIC_WHERE_CRITERIA for ordering the join condition. 
 * @version 1.3 23-June-2008 XS_2.27 - Fixed RateLock type to String
 * @version 1.4 24-June-2008 XS_16.13 - afterExwcute() method has been updated to get the picklist discription for
 *                                      Repayment Type,Payment Frequency,Prepayment Option,PrvilegePayment Option.
 * @version 1.5 14-Oct-2008 Bugfix FXP22958: Added new fields to display MI Premium amount in Deal Summary Screen                                     
 */
public class doComponentMortgageModelImpl extends QueryModelBase implements
        doComponentMortgageModel
{

    public static final String DATA_SOURCE_NAME = "jdbc/orcl";

    public static final String SELECT_SQL_TEMPLATE = "SELECT ALL COMPONENT.DEALID,COMPONENT.COMPONENTID, COMPONENT.COPYID, "
            + " COMPONENT.INSTITUTIONPROFILEID, to_char(COMPONENT.COMPONENTTYPEID)COMPONENTTYPEID_STR,"
            + " to_char(COMPONENT.MTGPRODID) MTGPRODID_STR, COMPONENTMORTGAGE.TOTALMORTGAGEAMOUNT, "
            + " COMPONENTMORTGAGE.MIALLOCATEFLAG, COMPONENTMORTGAGE.TOTALPAYMENTAMOUNT, COMPONENTMORTGAGE.PROPERTYTAXALLOCATEFLAG,"
            + " COMPONENTMORTGAGE.NETINTERESTRATE, COMPONENTMORTGAGE.DISCOUNT, COMPONENTMORTGAGE.PREMIUM, COMPONENTMORTGAGE.BUYDOWNRATE, "
            + " DEAL.MIPREMIUMAMOUNT, COMPONENTMORTGAGE.MORTGAGEAMOUNT, COMPONENTMORTGAGE.AMORTIZATIONTERM, DEAL.MIPREMIUMAMOUNT, "
            + " COMPONENTMORTGAGE.AMORTIZATIONTERM,  COMPONENTMORTGAGE.EFFECTIVEAMORTIZATIONMONTHS, " 
            + " MTGPROD.PAYMENTTERMID, "
            + " COMPONENTMORTGAGE.ACTUALPAYMENTTERM, COMPONENTMORTGAGE.PAYMENTFREQUENCYID,  COMPONENTMORTGAGE.PREPAYMENTOPTIONSID, "
            + " COMPONENTMORTGAGE.PRIVILEGEPAYMENTID, COMPONENTMORTGAGE.COMMISSIONCODE, COMPONENTMORTGAGE.CASHBACKPERCENT, "
            + " COMPONENTMORTGAGE.CASHBACKAMOUNT,  COMPONENTMORTGAGE.CASHBACKAMOUNTOVERRIDE, COMPONENT.REPAYMENTTYPEID, "
            + " COMPONENTMORTGAGE.RATELOCK, COMPONENT.ADDITIONALINFORMATION, COMPONENT.MTGPRODID, COMPONENT.PRICINGRATEINVENTORYID, "
            + " COMPONENTMORTGAGE.PANDIPAYMENTAMOUNT, COMPONENTMORTGAGE.ADDITIONALPRINCIPAL, COMPONENTMORTGAGE.PROPERTYTAXESCROWAMOUNT,  "
            + " COMPONENTMORTGAGE.ADVANCEHOLD, COMPONENTMORTGAGE.RATEGUARANTEEPERIOD, "
            + " COMPONENTMORTGAGE.EXISTINGACCOUNTINDICATOR, COMPONENTMORTGAGE.EXISTINGACCOUNTNUMBER, COMPONENTMORTGAGE.FIRSTPAYMENTDATE, "
            + " COMPONENTMORTGAGE.MATURITYDATE, COMPONENT.POSTEDRATE, DEAL.TAXPAYORID, DEAL.MIUPFRONT "
            + " FROM COMPONENT, COMPONENTMORTGAGE, MTGPROD, DEAL  "
            + "__WHERE__  ORDER BY COMPONENT.COMPONENTID ";

    public static final String MODIFYING_QUERY_TABLE_NAME = "COMPONENT, COMPONENTMORTGAGE, MTGPROD,DEAL";
  
    public static final String STATIC_WHERE_CRITERIA = "MTGPROD.INSTITUTIONPROFILEID = COMPONENT.INSTITUTIONPROFILEID " 
        + " AND COMPONENT.INSTITUTIONPROFILEID = COMPONENTMORTGAGE.INSTITUTIONPROFILEID "
        + " AND DEAL.INSTITUTIONPROFILEID=COMPONENT.INSTITUTIONPROFILEID "
        + " AND COMPONENT.DEALID = DEAL.DEALID "
        + " AND COMPONENT.COPYID = DEAL.COPYID "
        + " AND COMPONENT.COMPONENTID = COMPONENTMORTGAGE.COMPONENTID " 
        + " AND COMPONENT.COPYID = COMPONENTMORTGAGE.COPYID "
        + " AND MTGPROD.MTGPRODID = COMPONENT.MTGPRODID "
        + " AND COMPONENT.COMPONENTTYPEID=1 " ;
        

    public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();

    public static final String QUALIFIED_COLUMN_DFCOMPONENTID = "COMPONENT.COMPONENTID";
    public static final String COLUMN_DFCOMPONENTID = "COMPONENTID";

    public static final String QUALIFIED_COLUMN_DFDEALID = "COMPONENT.DEALID";
    public static final String COLUMN_DFDEALID = "DEALID";

    public static final String QUALIFIED_COLUMN_DFISTITUTIONPROFILEID = "COMPONENT.INSTITUTIONPROFILEID";
    public static final String COLUMN_DFISTITUTIONPROFILEID = "INSTITUTIONPROFILEID";

    public static final String QUALIFIED_COLUMN_DFCOPYID = "COMPONENT.COPYID";
    public static final String COLUMN_DFCOPYID = "COPYID";

    public static final String QUALIFIED_COLUMN_DFCOMPONENTTYPE = "COMPONENT.COMPONENTTYPEID_STR";
    public static final String COLUMN_DFCOMPONENTTYPE = "COMPONENTTYPEID_STR";

    public static final String QUALIFIED_COLUMN_DFPRODUCTNAME = "COMPONENT.MTGPRODID_STR";
    public static final String COLUMN_DFPRODUCTNAME = "MTGPRODID_STR";

    public static final String QUALIFIED_COLUMN_DFCOMPONENTMORTGAGEAMOUNT = "COMPONENTMORTGAGE.TOTALMORTGAGEAMOUNT";
    public static final String COLUMN_DFCOMPONENTMORTGAGEAMOUNT = "TOTALMORTGAGEAMOUNT";

    public static final String QUALIFIED_COLUMN_DFINCLUDEMIPREMIUMFLAG = "COMPONENTMORTGAGE.MIALLOCATEFLAG";
    public static final String COLUMN_DFINCLUDEMIPREMIUMFLAG = "MIALLOCATEFLAG";

    public static final String QUALIFIED_COLUMN_DFTOTALPAYMENTAMOUNT = "COMPONENTMORTGAGE.TOTALPAYMENTAMOUNT";
    public static final String COLUMN_DFTOTALPAYMENTAMOUNT = "TOTALPAYMENTAMOUNT";

    public static final String QUALIFIED_COLUMN_DFALLOCATETAXESCROW = "COMPONENTMORTGAGE.PROPERTYTAXALLOCATEFLAG";
    public static final String COLUMN_DFALLOCATETAXESCROW = "PROPERTYTAXALLOCATEFLAG";

    public static final String QUALIFIED_COLUMN_DFNETINTERESTRATE = "COMPONENTMORTGAGE.NETINTERESTRATE";
    public static final String COLUMN_DFNETINTERESTRATE = "NETINTERESTRATE";

    public static final String QUALIFIED_COLUMN_DFDISCOUNT = "COMPONENTMORTGAGE.DISCOUNT";
    public static final String COLUMN_DFDISCOUNT = "DISCOUNT";

    public static final String QUALIFIED_COLUMN_DFPREMIUM = "COMPONENTMORTGAGE.PREMIUM";
    public static final String COLUMN_DFPREMIUM = "PREMIUM";

    public static final String QUALIFIED_COLUMN_DFBUYDOWNRATE = "COMPONENTMORTGAGE.BUYDOWNRATE";
    public static final String COLUMN_DFBUYDOWNRATE = "BUYDOWNRATE";
    
    /***************MCM Impl team changes starts - XS_2.27, 2.28 *******************/
    
    public static final String QUALIFIED_COLUMN_DFMIPREMIUM = "DEAL.MIPREMIUMAMOUNT";
    public static final String COLUMN_DFMIPREMIUM = "MIPREMIUMAMOUNT";
    
    public static final String QUALIFIED_COLUMN_DFMORTGAGEAMOUNT = "COMPONENTMORTGAGE.MORTGAGEAMOUNT";
    public static final String COLUMN_DFMORTGAGEAMOUNT = "MORTGAGEAMOUNT";

    public static final String QUALIFIED_COLUMN_DFAMORTIZATIONTERM = "COMPONENTMORTGAGE.AMORTIZATIONTERM";
    public static final String COLUMN_DFAMORTIZATIONTERM = "AMORTIZATIONTERM";

    public static final String QUALIFIED_COLUMN_DFEFFECTIVEAMORTIZATIONINMONTHS = "COMPONENTMORTGAGE.EFFECTIVEAMORTIZATIONMONTHS";
    public static final String COLUMN_DFEFFECTIVEAMORTIZATIONMONTHS = "EFFECTIVEAMORTIZATIONMONTHS";

    public static final String QUALIFIED_COLUMN_DFPAYMENTTERMID = "MTGPROD.PAYMENTTERMID";
    public static final String COLUMN_DFPAYMENTTERMID = "PAYMENTTERMID";

    public static final String QUALIFIED_COLUMN_DFACTUALPAYMENTTERM = "COMPONENTMORTGAGE.ACTUALPAYMENTTERM";
    public static final String COLUMN_DFACTUALPAYMENTTERM = "ACTUALPAYMENTTERM";

    public static final String QUALIFIED_COLUMN_DFPAYMENTFREQUENCYID = "COMPONENTMORTGAGE.PAYMENTFREQUENCYID";
    public static final String COLUMN_DFPAYMENTFREQUENCYID = "PAYMENTFREQUENCYID";
    
    public static final String QUALIFIED_COLUMN_DFPREPAYMENTOPTIONSID = "COMPONENTMORTGAGE.PREPAYMENTOPTIONSID";
    public static final String COLUMN_DFPREPAYMENTOPTIONSID = "PREPAYMENTOPTIONSID";
    
    public static final String QUALIFIED_COLUMN_DFPRIVILEGEPAYMENTID = "COMPONENTMORTGAGE.PRIVILEGEPAYMENTID";
    public static final String COLUMN_DFPRIVILEGEPAYMENTID = "PRIVILEGEPAYMENTID";
    
    public static final String QUALIFIED_COLUMN_DFCOMMISSIONCODE = "COMPONENTMORTGAGE.COMMISSIONCODE";
    public static final String COLUMN_DFCOMMISSIONCODE = "COMMISSIONCODE";
    
    public static final String QUALIFIED_COLUMN_DFCASHBACKPERCENT = "COMPONENTMORTGAGE.CASHBACKPERCENT";
    public static final String COLUMN_DFCASHBACKPERCENT = "CASHBACKPERCENT";
    
    public static final String QUALIFIED_COLUMN_DFCASHBACKAMOUNT = "COMPONENTMORTGAGE.CASHBACKAMOUNT";
    public static final String COLUMN_DFCASHBACKAMOUNT = "CASHBACKAMOUNT";
    
    public static final String QUALIFIED_COLUMN_DFCASHBACKAMOUNTOVERRIDE = "COMPONENTMORTGAGE.CASHBACKAMOUNTOVERRIDE";
    public static final String COLUMN_DFCASHBACKAMOUNTOVERRIDE = "CASHBACKAMOUNTOVERRIDE";
    
    public static final String QUALIFIED_COLUMN_DFREPAYMENTTYPEID = "COMPONENT.REPAYMENTTYPEID";
    public static final String COLUMN_DFREPAYMENTTYPEID = "REPAYMENTTYPEID";
    
    public static final String QUALIFIED_COLUMN_DFRATELOCK = "COMPONENTMORTGAGE.RATELOCK";
    public static final String COLUMN_DFRATELOCK = "RATELOCK";
    
    public static final String QUALIFIED_COLUMN_DFADDITIONALINFORMATIOND = "COMPONENT.ADDITIONALINFORMATION";
    public static final String COLUMN_DFADDITIONALINFORMATION = "ADDITIONALINFORMATION";
    
    public static final String QUALIFIED_COLUMN_DFMTGPRODID = "COMPONENT.MTGPRODID";
    public static final String COLUMN_DFMTGPRODID = "MTGPRODID";
    
    public static final String QUALIFIED_COLUMN_DFPRICINGRATEINVENTORYID = "COMPONENT.PRICINGRATEINVENTORYID";
    public static final String COLUMN_DFPRICINGRATEINVENTORYID = "PRICINGRATEINVENTORYID";
    
    public static final String QUALIFIED_COLUMN_DFPIPAYMENTAMOUNT = "COMPONENTMORTGAGE.PANDIPAYMENTAMOUNT";
    public static final String COLUMN_DFPIPAYMENTAMOUNT = "PANDIPAYMENTAMOUNT";

    public static final String QUALIFIED_COLUMN_DFADDITIONALPRINCIPAL = "COMPONENTMORTGAGE.ADDITIONALPRINCIPAL";
    public static final String COLUMN_DFADDITIONALPRINCIPAL = "ADDITIONALPRINCIPAL";

    public static final String QUALIFIED_COLUMN_DFESCROWPAYMENTAMOUNT = "COMPONENTMORTGAGE.PROPERTYTAXESCROWAMOUNT";
    public static final String COLUMN_DFESCROWPAYMENTAMOUNT = "PROPERTYTAXESCROWAMOUNT";

//    public static final String QUALIFIED_COLUMN_DFPROPERTYTAXALLOCATEFLAG = "COMPONENTMORTGAGE.PROPERTYTAXALLOCATEFLAG";
//    public static final String COLUMN_DFPROPERTYTAXALLOCATEFLAG = "PROPERTYTAXALLOCATEFLAG";

    public static final String QUALIFIED_COLUMN_DFADVANCEHOLD = "COMPONENTMORTGAGE.ADVANCEHOLD";
    public static final String COLUMN_DFADVANCEHOLD = "ADVANCEHOLD";

    public static final String QUALIFIED_COLUMN_DFRATEGUARANTEEPERIOD = "COMPONENTMORTGAGE.RATEGUARANTEEPERIOD";
    public static final String COLUMN_DFRATEGUARANTEEPERIOD = "RATEGUARANTEEPERIOD";

    public static final String QUALIFIED_COLUMN_DFEXISTINGACCOUNTINDICATOR = "COMPONENTMORTGAGE.EXISTINGACCOUNTINDICATOR";
    public static final String COLUMN_DFEXISTINGACCOUNTINDICATOR = "EXISTINGACCOUNTINDICATOR";

    public static final String QUALIFIED_COLUMN_DFEXISTINGACCOUNTNUMBER = "COMPONENTMORTGAGE.EXISTINGACCOUNTNUMBER";
    public static final String COLUMN_DFEXISTINGACCOUNTNUMBER = "EXISTINGACCOUNTNUMBER";

    public static final String QUALIFIED_COLUMN_DFFIRSTPAYMENTDATE = "COMPONENTMORTGAGE.FIRSTPAYMENTDATE";
    public static final String COLUMN_DFFIRSTPAYMENTDATE = "FIRSTPAYMENTDATE";

    public static final String QUALIFIED_COLUMN_DFMATURITYDATE = "COMPONENTMORTGAGE.MATURITYDATE";
    public static final String COLUMN_DFMATURITYDATE = "MATURITYDATE";
    
    public static final String QUALIFIED_COLUMN_DFTAXPAYORID = "DEAL.TAXPAYORID";
    public static final String COLUMN_DFTAXPAYORID = "TAXPAYORID";
    
    public static final String QUALIFIED_COLUMN_DFPOSTEDRATE = "COMPONENT.POSTEDRATE";
    public static final String COLUMN_DFPOSTEDRATE = "POSTEDRATE";

    public static final String QUALIFIED_COLUMN_DFMIUPFRONT="DEAL.MIUPFRONT";
    public static final String COLUMN_DFMIUPFRONT="MIUPFRONT";
    

    /***************MCM Impl team changes ends - XS_2.27, 2.28 *******************/

	/**
	 * This the Static query field discriptor for mapping the database column to
	 * the model fields.
	 */
	static {
		FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
				FIELD_DFCOMPONENTID, COLUMN_DFCOMPONENTID,
				QUALIFIED_COLUMN_DFCOMPONENTID, java.math.BigDecimal.class,
				false, false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
		
		FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
				FIELD_DFDEALID, COLUMN_DFDEALID, QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class, false, false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCOPYID, COLUMN_DFCOPYID, QUALIFIED_COLUMN_DFCOPYID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFISNTITUTIONID, COLUMN_DFISTITUTIONPROFILEID,
                QUALIFIED_COLUMN_DFISTITUTIONPROFILEID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCOMPONENTTYPE, COLUMN_DFCOMPONENTTYPE,
                QUALIFIED_COLUMN_DFCOMPONENTTYPE, String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPRODUCTNAME, COLUMN_DFPRODUCTNAME,
                QUALIFIED_COLUMN_DFPRODUCTNAME, String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFTOTALMORTGAGEAMOUNT, COLUMN_DFCOMPONENTMORTGAGEAMOUNT,
                QUALIFIED_COLUMN_DFCOMPONENTMORTGAGEAMOUNT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFMIALLOCAGTEFLAG, COLUMN_DFINCLUDEMIPREMIUMFLAG,
                QUALIFIED_COLUMN_DFINCLUDEMIPREMIUMFLAG, String.class, false,
                false, QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "", QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFTOTALPAYMENTAMOUNT, COLUMN_DFTOTALPAYMENTAMOUNT,
                QUALIFIED_COLUMN_DFTOTALPAYMENTAMOUNT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPROPERTYTAXALLOCATEFLAG, COLUMN_DFALLOCATETAXESCROW,
                QUALIFIED_COLUMN_DFALLOCATETAXESCROW, String.class, false,
                false, QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "", QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFNETINTRESTRATE, COLUMN_DFNETINTERESTRATE,
                QUALIFIED_COLUMN_DFNETINTERESTRATE, java.math.BigDecimal.class,
                false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFDISCOUNT, COLUMN_DFDISCOUNT,
                QUALIFIED_COLUMN_DFDISCOUNT, java.math.BigDecimal.class, false,
                false, QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "", QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPREMIUM, COLUMN_DFPREMIUM, QUALIFIED_COLUMN_DFPREMIUM,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFBUYDOWNRATE, COLUMN_DFBUYDOWNRATE,
                QUALIFIED_COLUMN_DFBUYDOWNRATE, java.math.BigDecimal.class,
                false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        
        /***************MCM Impl team changes starts - XS_2.27, 2.28 *******************/

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCOMPONENTID, COLUMN_DFCOMPONENTID,
                QUALIFIED_COLUMN_DFCOMPONENTID, java.math.BigDecimal.class,
                false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFDEALID, COLUMN_DFDEALID, QUALIFIED_COLUMN_DFDEALID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFMIPREMIUM, COLUMN_DFMIPREMIUM, QUALIFIED_COLUMN_DFMIPREMIUM,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFMORTGAGEAMOUNT, COLUMN_DFMORTGAGEAMOUNT, QUALIFIED_COLUMN_DFMORTGAGEAMOUNT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFAMORTIZATIONTERM, COLUMN_DFAMORTIZATIONTERM, QUALIFIED_COLUMN_DFAMORTIZATIONTERM,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFEFFECTIVEAMORTIZATIONINMONTHS, COLUMN_DFEFFECTIVEAMORTIZATIONMONTHS, 
                QUALIFIED_COLUMN_DFEFFECTIVEAMORTIZATIONINMONTHS,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPAYMENTTERMID, COLUMN_DFPAYMENTTERMID, 
                QUALIFIED_COLUMN_DFPAYMENTTERMID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFACTUALPAYMENTTERM, COLUMN_DFACTUALPAYMENTTERM, 
                QUALIFIED_COLUMN_DFACTUALPAYMENTTERM,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPAYMENTFREQUENCYID, COLUMN_DFPAYMENTFREQUENCYID, 
                QUALIFIED_COLUMN_DFPAYMENTFREQUENCYID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPREPAYMENTOPTIONSID, COLUMN_DFPREPAYMENTOPTIONSID, 
                QUALIFIED_COLUMN_DFPREPAYMENTOPTIONSID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPRIVILEGEPAYMENTID, COLUMN_DFPRIVILEGEPAYMENTID, 
                QUALIFIED_COLUMN_DFPRIVILEGEPAYMENTID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCOMMISSIONCODE, COLUMN_DFCOMMISSIONCODE, 
                QUALIFIED_COLUMN_DFCOMMISSIONCODE,
               String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCASHBACKPERCENTAGE, COLUMN_DFCASHBACKPERCENT, 
                QUALIFIED_COLUMN_DFCASHBACKPERCENT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCASHBACKAMOUNT, COLUMN_DFCASHBACKAMOUNT, 
                QUALIFIED_COLUMN_DFCASHBACKAMOUNT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCASHBACKAMOUNTOVERRIDE, COLUMN_DFCASHBACKAMOUNTOVERRIDE, 
                QUALIFIED_COLUMN_DFCASHBACKAMOUNTOVERRIDE,
                String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFREPAYMENTTYPEID, COLUMN_DFREPAYMENTTYPEID, 
                COLUMN_DFREPAYMENTTYPEID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFRATELOCK, COLUMN_DFRATELOCK, 
                QUALIFIED_COLUMN_DFRATELOCK,
                String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFADDITIONALINFORMATION, COLUMN_DFADDITIONALINFORMATION, 
                QUALIFIED_COLUMN_DFADDITIONALINFORMATIOND,
                String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFMTGPRODTID, COLUMN_DFMTGPRODID, 
                QUALIFIED_COLUMN_DFMTGPRODID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPRICINGRATEINVENTORYID, COLUMN_DFPRICINGRATEINVENTORYID, 
                QUALIFIED_COLUMN_DFPRICINGRATEINVENTORYID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPIPAYMENTAMOUNT, COLUMN_DFPIPAYMENTAMOUNT, 
                QUALIFIED_COLUMN_DFPIPAYMENTAMOUNT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFADDITIONALPRINCIPAL, COLUMN_DFADDITIONALPRINCIPAL, 
                QUALIFIED_COLUMN_DFADDITIONALPRINCIPAL,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFESCROWPAYMENTAMOUNT, COLUMN_DFESCROWPAYMENTAMOUNT, 
                QUALIFIED_COLUMN_DFESCROWPAYMENTAMOUNT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

//        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
//                FIELD_DFPROPERTYTAXALLOCATEFLAG, COLUMN_DFPROPERTYTAXALLOCATEFLAG, 
//                QUALIFIED_COLUMN_DFPROPERTYTAXALLOCATEFLAG,
//                String.class, false, false,
//                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
//                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFADVANCEHOLD, COLUMN_DFADVANCEHOLD, 
                QUALIFIED_COLUMN_DFADVANCEHOLD,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFRATEGUARANTEEPERIOD, COLUMN_DFRATEGUARANTEEPERIOD, 
                QUALIFIED_COLUMN_DFRATEGUARANTEEPERIOD,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFEXISTINGACOUNTINDICATOR, COLUMN_DFEXISTINGACCOUNTINDICATOR, 
                QUALIFIED_COLUMN_DFEXISTINGACCOUNTINDICATOR,
                String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFEXISTINGACOUNTNUMBER, COLUMN_DFEXISTINGACCOUNTNUMBER, 
                QUALIFIED_COLUMN_DFEXISTINGACCOUNTNUMBER,
                // MCM Impl Team : bug fixed STARTS XS_2.27
                String.class, false, false,
                // MCM Impl Team : bug fixed ENDS XS_2.27
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFFIRSTPAYMENTDATE, COLUMN_DFFIRSTPAYMENTDATE, 
                QUALIFIED_COLUMN_DFFIRSTPAYMENTDATE,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFMATURITYDATE, COLUMN_DFMATURITYDATE, 
                QUALIFIED_COLUMN_DFMATURITYDATE,
                java.sql.Timestamp.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFTAXPAYORID, COLUMN_DFTAXPAYORID, 
                QUALIFIED_COLUMN_DFTAXPAYORID,
                java.sql.Timestamp.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFMIUPFRONT, COLUMN_DFMIUPFRONT, 
                QUALIFIED_COLUMN_DFMIUPFRONT,
                String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPOSTEDRATE, COLUMN_DFPOSTEDRATE, 
                QUALIFIED_COLUMN_DFPOSTEDRATE,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        /***************MCM Impl team changes ends - XS_2.27, 2.28 *******************/
        
	}
    /**
     * This constructor initialises an instance of this model, setting basic
     * attributes.
     */
    public doComponentMortgageModelImpl()
    {
        super();
        setDataSourceName(DATA_SOURCE_NAME);

        setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

        setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

        setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

        setFieldSchema(FIELD_SCHEMA);

        initialize();

    }

    /**
     * Initialize.
     */
    public void initialize()
    {

    }

    /**
     * BeforeExecute.
     * @param context
     *            the context
     * @param queryType
     *            the query type
     * @param sql
     *            the sql
     * @return list of model model[]
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    protected String beforeExecute(ModelExecutionContext context,
            int queryType, String sql) throws ModelControlException
    {

    	return sql;

    }

    /**
     * This method is used to handle any display logic after executing the
     * model.
     * @param context
     *            the context
     * @param queryType
     *            the query type
     * @return list of model model[]
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     * @version 1.4 24-June-2008 XS_16.13 - afterExwcute() method has been updated to get the picklist discription for
     *                                      Repayment Type,Payment Frequency,Prepayment Option,PrvilegePayment Option.
     */
    protected void afterExecute(ModelExecutionContext context, int queryType)
            throws ModelControlException
    {

        String defaultInstanceStateName = getRequestContext().getModelManager()
                .getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl) getRequestContext()
                .getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName, true);
        int languageId = theSessionState.getLanguageId();
        int institutionId = theSessionState.getDealInstitutionId();

        while (this.next())
        {

            this.setDfComponentType(BXResources.getPickListDescription(
                    institutionId, "COMPONENTTYPE", this.getDfComponentType(),
                    languageId));

            this.setDfProductName(BXResources.getPickListDescription(
                    institutionId, "MTGPROD", this.getDfProductName(),
                    languageId));

            /***************MCM Impl team changes start - XS_2.27, 2.28 *******************/
            this.setDfPaymentTermDesc(BXResources.getPickListDescription(
                    institutionId, "PAYMENTTERM", this.getDfPaymentTermId().intValue(),
                    languageId));

            /***************MCM Impl team changes ends - XS_2.27, 2.28 *******************/
            
            /***************MCM Impl team changes starts - XS_16.13 *******************/
            this.setDfRePaymentType(BXResources.getPickListDescription(
                    institutionId, "REPAYMENTTYPE", this.getDfRepaymentTypeId().intValue(),
                    languageId));
            this.setDfPaymentFrequency(BXResources.getPickListDescription(
                    institutionId, "PAYMENTFREQUENCY", this.getDfPaymentFrequencyId().intValue(),
                    languageId));
            this.setDfPrePaymentOption(BXResources.getPickListDescription(
                    institutionId, "PREPAYMENTOPTIONS", this.getDfPrepaymentOptionsId().intValue(),
                    languageId));
            this.setDfPrivilegePaymentOption(BXResources.getPickListDescription(
                    institutionId, "PRIVILEGEPAYMENT", this.getDfPrivilegePaymentId().intValue(),
                    languageId));
            /***************MCM Impl team changes ends - XS_16.13 *******************/
            
            /*******************************BUG Fix :FXP22958*****************************/
            //MI Premium Amount
             if("Y".equals(this.getDfMiAllocateFlag()))
                     this.setDfSummaryMiPremiumAmount(this.getDfMIPremium());
             else
                    this.setDfSummaryMiPremiumAmount(new BigDecimal(0.00));
             //Tax Escrow Amount
             if("Y".equals(this.getDfPropertyTaxFlag()))
                 this.setDfSummaryTaxEscrowAmount(this.getDfEscrowPaymentAmount());
             else
                this.setDfSummaryTaxEscrowAmount(new BigDecimal(0.00));
             /****************************************************************************/
            
        }
     
        // Reset Location
        this.beforeFirst();

    }

    /**
     * OnDatabaseError.
     * @param context
     *            the context
     * @param queryType
     *            the query type
     * @param exception
     *            the exception
     * @return list of model model[]
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    protected void onDatabaseError(ModelExecutionContext context,
            int queryType, SQLException exception)
    {

    }

    public java.math.BigDecimal getDfComponentId()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFCOMPONENTID);

    }

    public void setDfComponentId(java.math.BigDecimal value)
    {

        setValue(FIELD_DFCOMPONENTID, value);

    }

    public java.math.BigDecimal getDfCopyId()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFCOPYID);

    }

    public void setDfCopyId(java.math.BigDecimal value)
    {

        setValue(FIELD_DFCOPYID, value);

    }

    public BigDecimal getDfInstitutionId()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFISNTITUTIONID);

    }

    public void setDfInstitutionId(BigDecimal value)
    {

        setValue(FIELD_DFISNTITUTIONID, value);

    }

    public void setDfTotalMortgageAmount(BigDecimal value)
    {

        setValue(FIELD_DFTOTALMORTGAGEAMOUNT, value);

    }

    public java.math.BigDecimal getDfTotalMortgageAmount()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFTOTALMORTGAGEAMOUNT);

    }

    public BigDecimal getDfBuyDownRate()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFBUYDOWNRATE);

    }

    public String getDfComponentType()
    {

        return (String) getValue(FIELD_DFCOMPONENTTYPE);

    }

    public BigDecimal getDfDiscount()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFDISCOUNT);

    }

    public String getDfMiAllocateFlag()
    {

        return (String) getValue(FIELD_DFMIALLOCAGTEFLAG);

    }

    public BigDecimal getDfNetInstrestRate()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFNETINTRESTRATE);

    }

    public BigDecimal getDfPremium()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFPREMIUM);

    }

    public String getDfProductName()
    {

        return (String) getValue(FIELD_DFPRODUCTNAME);

    }

    public String getDfPropertyTaxFlag()
    {

        return (String) getValue(FIELD_DFPROPERTYTAXALLOCATEFLAG);

    }

    public BigDecimal getDfTotalPaymentAmount()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFTOTALPAYMENTAMOUNT);

    }

    public void setDfBuyDownRate(BigDecimal value)
    {

        setValue(FIELD_DFBUYDOWNRATE, value);

    }

    public void setDfComponentType(String value)
    {

        setValue(FIELD_DFCOMPONENTTYPE, value);

    }

    public void setDfDiscount(BigDecimal value)
    {

        setValue(FIELD_DFDISCOUNT, value);

    }

    public void setDfMiAllocateFlag(String value)
    {

        setValue(FIELD_DFMIALLOCAGTEFLAG, value);

    }

    public void setDfNetInstrestRate(BigDecimal value)
    {

        setValue(FIELD_DFNETINTRESTRATE, value);

    }

    public void setDfPremium(BigDecimal value)
    {

        setValue(FIELD_DFPREMIUM, value);

    }

    public void setDfProductName(String value)
    {

        setValue(FIELD_DFPRODUCTNAME, value);

    }

    public void setDfPropertyTaxFlag(String value)
    {

        setValue(FIELD_DFPROPERTYTAXALLOCATEFLAG, value);

    }

    public void setDfTotalLocAmount(BigDecimal value)
    {

    }

    public void setDfTotalPaymentAmount(BigDecimal value)
    {

        setValue(FIELD_DFTOTALPAYMENTAMOUNT, value);

    }

    public BigDecimal getDfDealId()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFDEALID);

    }

    public void setDfDealId(BigDecimal value)
    {

        setValue(FIELD_DFDEALID, value);

    }
    
    /***************MCM Impl team changes starts - XS_2.27, 2.28 *******************/
    /**
     * This method declaration returns the value of field the MIPremium.
     * @return the dfMIPremium
     */
    public java.math.BigDecimal getDfMIPremium(){
        return (java.math.BigDecimal) getValue(FIELD_DFMIPREMIUM);
    }

    /**
     * This method declaration sets the MIPremium.
     * @param value
     *            the new dfMIPremium
     */
    public void setDfMIPremium(java.math.BigDecimal value) {
        setValue(FIELD_DFMIPREMIUM, value);        
    }
    /**
     * This method declaration returns the value of field the MortgageAmount.
     * @return the dfMortgageAmount
     */
    public java.math.BigDecimal getDfMortgageAmount(){
        return (java.math.BigDecimal) getValue(FIELD_DFMORTGAGEAMOUNT);        
    }

    /**
     * This method declaration sets the MortgageAmount.
     * @param value
     *            the new dfMortgageAmount
     */
    public void setDfMortgageAmount(java.math.BigDecimal value) {
        setValue(FIELD_DFMORTGAGEAMOUNT, value);                
    }

    /**
     * This method declaration returns the value of field the AmortizationTerm.
     * @return the dfAmortizationTerm
     */
    public java.math.BigDecimal getDfAmortizationTerm() {
        return (java.math.BigDecimal) getValue(FIELD_DFAMORTIZATIONTERM);                
    }

    /**
     * This method declaration sets the AmortizationTerm.
     * @param value
     *            the new dfAmortizationTerm
     */
    public void setDfAmortizationTerm(java.math.BigDecimal value) {
        setValue(FIELD_DFAMORTIZATIONTERM, value);                        
    }
    
    /**
     * This method declaration returns the value of field the EffectiveAmortizationInMonths.
     * @return the dfEffectiveAmortizationInMonths
     */
    public java.math.BigDecimal getDfEffectiveAmortizationInMonths() {
        return (java.math.BigDecimal) getValue(FIELD_DFEFFECTIVEAMORTIZATIONINMONTHS);                
    }
    /**
     * This method declaration sets the EffectiveAmortizationInMonths.
     * @param value
     *            the new dfEffectiveAmortizationInMonths
     */
    public void setDfEffectiveAmortizationInMonths(java.math.BigDecimal value) {
        setValue(FIELD_DFEFFECTIVEAMORTIZATIONINMONTHS, value);                        
    }
    /**
     * This method declaration returns the value of field the PaymentTermId.
     * @return the dfPaymentTermId
     */
    public java.math.BigDecimal getDfPaymentTermId(){
        return (java.math.BigDecimal) getValue(FIELD_DFPAYMENTTERMID);                
    }
    /**
     * This method declaration sets the PaymentTermDesc.
     * @param value
     *            the new dfPaymentTermId
     */
    public void setDfPaymentTermId(java.math.BigDecimal value) {
        setValue(FIELD_DFPAYMENTTERMID, value);
    }               
    /**
     * This method declaration returns the value of field the PaymentTermDesc.
     * @return the dfPaymentTermDesc
     */
    public String getDfPaymentTermDesc(){
        return (String) getValue(FIELD_DFPAYMENTTERMDESC);
    }
    /**
     * This method declaration sets the PaymentTermDesc.
     * @param value
     *            the new dfPaymentTermDesc
     */
    public void setDfPaymentTermDesc(String value) {
        setValue(FIELD_DFPAYMENTTERMDESC, value);
    }
    /**
     * This method declaration returns the value of field the ActualPaymentTerm.
     * @return the dfActualPaymentTerm
     */
    public java.math.BigDecimal getDfActualPaymentTerm() {
        return (java.math.BigDecimal) getValue(FIELD_DFACTUALPAYMENTTERM);                        
    }

    /**
     * This method declaration sets the ActualPaymentTerm.
     * @param value: the new dfActualPaymentTerm
     */
    public void setDfActualPaymentTerm(java.math.BigDecimal value) {
        setValue(FIELD_DFACTUALPAYMENTTERM, value);
    }    
    /**
     * This method declaration returns the value of field the PaymentFrequencyId.
     * @return dfPaymentFrequencyId
     */
    public java.math.BigDecimal getDfPaymentFrequencyId() {
        return (java.math.BigDecimal) getValue(FIELD_DFPAYMENTFREQUENCYID);
    }

    /**
     * This method declaration sets the PaymentFrequencyId.
     * @param value: the new dfPaymentFrequencyId
     */
    public void setDfPaymentFrequencyId(java.math.BigDecimal value) {
        setValue(FIELD_DFPAYMENTFREQUENCYID, value);
    }
    /**
     * This method declaration returns the value of field the PrepaymentOptionsId.
     * @return dfPrepaymentOptionsId
     */
    public java.math.BigDecimal getDfPrepaymentOptionsId() {
        return (java.math.BigDecimal) getValue(FIELD_DFPREPAYMENTOPTIONSID);        
    }

    /**
     * This method declaration sets the PrepaymentOptionsId.
     * @param value: the new dfPrepaymentOptionsId
     */
    public void setDfPrepaymentOptionsId(java.math.BigDecimal value) {
        setValue(FIELD_DFPREPAYMENTOPTIONSID, value);
    }
    
    /**
     * This method declaration returns the value of field the PrivilegePaymentId.
     * @return dfPrivilegePaymentId
     */
    public java.math.BigDecimal getDfPrivilegePaymentId() {
        return (java.math.BigDecimal) getValue(FIELD_DFPRIVILEGEPAYMENTID);        
    }

    /**
     * This method declaration sets the PrivilegePaymentId.
     * @param value: the new dfPrivilegePaymentId
     */
    public void setDfPrivilegePaymentId(java.math.BigDecimal value) {
        setValue(FIELD_DFPRIVILEGEPAYMENTID, value);
    }
    /**
     * This method declaration returns the value of field the CommisionCode.
     * @return dfCommisionCode
     */
    public String getDfCommissionCode() {
        return (String) getValue(FIELD_DFCOMMISSIONCODE);        
    }

    /**
     * This method declaration sets the CommisionCode.
     * @param value: the new dfCommisionCode
     */
    public void setDfCommissionCode(String value) {
        setValue(FIELD_DFCOMMISSIONCODE, value);
    }
    /**
     * This method declaration returns the value of field the CashbackPercentage.
     * @return dfCashbackPercentage
     */
    public java.math.BigDecimal getDfCashbackPercentage() {
        return (java.math.BigDecimal) getValue(FIELD_DFCASHBACKPERCENTAGE);        
    }

    /**
     * This method declaration sets the CashbackPercentage.
     * @param value: the new dfCashbackPercentage
     */
    public void setDfCashbackPercentage(java.math.BigDecimal value) {
        setValue(FIELD_DFPRIVILEGEPAYMENTID, value);
    }
    /**
     * This method declaration returns the value of field the CashbackAmount.
     * @return dfCashbackAmount
     */
    public java.math.BigDecimal getDfCashbackAmount() {
        return (java.math.BigDecimal) getValue(FIELD_DFCASHBACKAMOUNT);        
    }

    /**
     * This method declaration sets the CashbackAmount.
     * @param value: the new dfCashbackAmount
     */
    public void setDfCashbackAmount(java.math.BigDecimal value){
        setValue(FIELD_DFCASHBACKAMOUNT, value);
    }
    
    /**
     * This method declaration returns the value of field the CashbackOverride.
     * @return dfCashbackOverride
     */
    public String getDfCashbackAmountOverride() {
        return (String) getValue(FIELD_DFCASHBACKAMOUNTOVERRIDE);        
    }

    /**
     * This method declaration sets the CashbackOverride.
     * @param value: the new dfCashbackOverride
     */
    public void setDfCashbackAmountOverride(String value){
        setValue(FIELD_DFCASHBACKAMOUNTOVERRIDE, value);
    }
    
    /**
     * This method declaration returns the value of field the RepaymentTypeId.
     * @return dfRepaymentTypeId
     */
    public java.math.BigDecimal getDfRepaymentTypeId() {
        return (java.math.BigDecimal) getValue(FIELD_DFREPAYMENTTYPEID);        
    }

    /**
     * This method declaration sets the RepaymentTypeId.
     * @param value: the new dfRepaymentTypeId
     */
    public void setDfRepaymentTypeId(java.math.BigDecimal value) {
        setValue(FIELD_DFREPAYMENTTYPEID, value);
    }
    /**
     * This method declaration returns the value of field the RateLockedIn.
     * @return dfRateLockedIn
     */
    public String getDfRateLockedIn() {
        return (String) getValue(FIELD_DFRATELOCK);        
    }

    /**
     * This method declaration sets the RateLockedIn.
     * @param value: the new dfRateLockedIn
     */
    public void setDfRateLockedIn(String value) {
        setValue(FIELD_DFRATELOCK, value);
    }

    /**
     * This method declaration returns the value of field the AdditionalInformation.
     * @return dfAdditionalInformation
     */
    public String getDfAdditionalInformation() {
        return (String) getValue(FIELD_DFADDITIONALINFORMATION);        
    }

    /**
     * This method declaration sets the AdditionalInformation.
     * @param value: the new dfAdditionalInformation
     */
    public void setDfAdditionalInformation(String value) {
        setValue(FIELD_DFADDITIONALINFORMATION, value);        
    }

    /**
     * This method declaration returns the value of field the MtgProdId.
     * @return dfMtgProdId
     */
    public java.math.BigDecimal getDfMtgProdId() {
        return (java.math.BigDecimal) getValue(FIELD_DFMTGPRODTID);        
    }

    /**
     * This method declaration sets the MtgProdId.
     * @param value: the new dfMtgProdId
     */
    public void setDfMtgProdId(java.math.BigDecimal value) {
        setValue(FIELD_DFMTGPRODTID, value);        
    }
    
    /**
     * This method declaration returns the value of field the PricingRateInventoryId.
     * @return dfPricingRateInventoryId
     */
    public java.math.BigDecimal getDfPricingRateInventoryId() {
        return (java.math.BigDecimal) getValue(FIELD_DFPRICINGRATEINVENTORYID);        
    }

    /**
     * This method declaration sets the PricingInventryId.
     * @param value: the new dfPricingInventryRateId
     */
    public void setDfPricingRateInventoryId(java.math.BigDecimal value) {
        setValue(FIELD_DFPRICINGRATEINVENTORYID, value);        
    }
    
    /**
     * This method declaration returns the value of field the PandIPaymentAmount.
     * @return dfPIPaymentAmount
     */
    public java.math.BigDecimal getDfPIPaymentAmount() {
        return (java.math.BigDecimal) getValue(FIELD_DFPIPAYMENTAMOUNT);        
    }

    /**
     * This method declaration sets the PandIPaymentAmount.
     * @param value: the new dfPIPaymentAmount
     */
    public void setDfPIPaymentAmount(java.math.BigDecimal value) {
        setValue(FIELD_DFPIPAYMENTAMOUNT, value);        
    }
    
    /**
     * This method declaration returns the value of field the AdditionalPrincipal.
     * @return dfAdditionalPrincipal
     */
    public java.math.BigDecimal getDfAdditionalPrincipal() {
        return (java.math.BigDecimal) getValue(FIELD_DFADDITIONALPRINCIPAL);        
    }

    /**
     * This method declaration sets the AdditionalPrincipal.
     * @param value: the new dfAdditionalPrincipal
     */
    public void setDfAdditionalPrincipal(java.math.BigDecimal value){
        setValue(FIELD_DFADDITIONALPRINCIPAL, value);
    }
    /**
     * This method declaration returns the value of field the EscrowPaymentAmount.
     * @return EscrowPaymentAmount
     */
    public java.math.BigDecimal getDfEscrowPaymentAmount(){
        return (java.math.BigDecimal) getValue(FIELD_DFESCROWPAYMENTAMOUNT);        
    }
    /**
     * This method declaration sets the AdvancedHold.
     * @param value: the new EscrowPaymentAmount
     */
    public void setDfEscrowPaymentAmount(java.math.BigDecimal value){
        setValue(FIELD_DFESCROWPAYMENTAMOUNT, value);
    }
    /**
     * This method declaration returns the value of field the AdvancedHold.
     * @return dfAdvancedHold
     */
    public java.math.BigDecimal getDfAdvancedHold(){
        return (java.math.BigDecimal) getValue(FIELD_DFADVANCEHOLD);        
    }

    /**
     * This method declaration sets the RateGuaranteePeriod.
     * @param value: the new DfAdvancedHold
     */
    public void setDfAdvancedHold(java.math.BigDecimal value){
        setValue(FIELD_DFADVANCEHOLD, value);
    }
    /**
     * This method declaration returns the value of field the RateGuaranteePeriod.
     * @return dfRateGuaranteePeriod
     */
    public java.math.BigDecimal  getDfRateGuaranteePeriod(){
        return (java.math.BigDecimal ) getValue(FIELD_DFRATEGUARANTEEPERIOD);        
    }
    /**
     * This method declaration sets the ExsitingAccountIndicator.
     * @param value: the new dfRateGuaranteePeriod
     */
    public void setDfRateGuaranteePeriod(java.math.BigDecimal value){
        setValue(FIELD_DFADVANCEHOLD, value);
    }
    /**
     * This method declaration returns the value of field the ExsitingAccountIndicator.
     * @return dfExsitingAccountIndicator
     */
    public String getDfExsitingAccountIndicator(){
        return (String) getValue(FIELD_DFEXISTINGACOUNTINDICATOR);        
    }
    /**
     * This method declaration sets the ExsitingAccountNumber.
     * @param value: the new dfExsitingAccountIndicator
     */
    public void setDfExsitingAccountIndicator(String value){
        setValue(FIELD_DFEXISTINGACOUNTINDICATOR, value);
    }
    /**
     * This method declaration returns the value of field the ExsitingAccountNumber.
     * @return dfExsitingAccountNumber
     */
    public String getDfExsitingAccountNumber(){
        return (String) getValue(FIELD_DFEXISTINGACOUNTNUMBER);        
    }
    /**
     * This method declaration sets the ExsitingAccountNumber.
     * @param value: the new dfExsitingAccountNumber
     */
    public void setDfExsitingAccountNumber(String value){
        setValue(FIELD_DFEXISTINGACOUNTNUMBER, value);
    }
    /**
     * This method declaration returns the value of field the FirstPaymentDate.
     * @return dfFirstPaymentDate
     */
    public  java.sql.Timestamp getDfFirstPaymentDate(){
        return (java.sql.Timestamp) getValue(FIELD_DFFIRSTPAYMENTDATE);        
    }

    /**
     * This method declaration sets the FirstPaymentDate.
     * @param value: the new dfFirstPaymentDate
     */
    public void setDfFirstPaymentDate(java.sql.Timestamp value){
        setValue(FIELD_DFFIRSTPAYMENTDATE, value);        
    }

    /**
     * This method declaration returns the value of field the MaturityDate.
     * @return dfMaturityDate
     */
    public  java.sql.Timestamp getDfMaturityDate() {
        return (java.sql.Timestamp) getValue(FIELD_DFMATURITYDATE);        
    }

    /**
     * This method declaration sets the MaturityDate.
     * @param value: the new dfMaturityDate
     */
    public void setDfMaturityDate(java.sql.Timestamp value) {
        setValue(FIELD_DFMATURITYDATE, value);        
    }
    
    /**
     * This method declaration returns the value of field the TaxPayorId on Deal.
     * @return dfTaxPayorId
     */
    public  java.math.BigDecimal getDfTaxPayorId() {
        return (java.math.BigDecimal) getValue(FIELD_DFTAXPAYORID);        
    }

    /**
     * This method declaration sets the MaturityDate.
     * @param value: the new dfTaxPayorId
     */
    public void setDfTaxPayorId(java.math.BigDecimal value) {
        setValue(FIELD_DFTAXPAYORID, value);        
    }
    
    public BigDecimal getDfPostedRate() {
        return (java.math.BigDecimal) getValue(FIELD_DFPOSTEDRATE);    
    }

    public void setDfPostedRate(BigDecimal value) {
        setValue(FIELD_DFPOSTEDRATE, value);   
    }

    /**
     * This method declaration returns the value of field the MIUpfront on Deal.
     * @return dfTaxPayorId
     */
    public  String getDfMIUpfront() {
        return (String) getValue(FIELD_DFMIUPFRONT);        
    }

    /**
     * This method declaration sets the MaturityDate.
     * @param value: the new dfTaxPayorId
     */
    
    public void setDfMIUpfront(String value) {
        setValue(FIELD_DFMIUPFRONT, value);        
    }
    /***************MCM Impl team changes ends - XS_2.27, 2.28 *******************/

/********************************MCM Impl Team changes starts - XS_16.13********************/
    public String getDfPaymentFrequency()
    {
        return (String) getValue(FIELD_DFPAYMENTFREQUENCY); 
    }

    public String getDfPrePaymentOption()
    {
        return (String) getValue(FIELD_DFPREPAYMENTOPTION); 
    }

    public String getDfPrivilegePaymentOption()
    {
        return (String) getValue(FIELD_DFPRIVILEGEPAYMENTOPTION);
    }

    public String getDfRePaymentType()
    {
        return (String) getValue(FIELD_DFREPAYMENTTYPE);
    }

    public void setDfPaymentFrequency(String value)
    {
        setValue(FIELD_DFPAYMENTFREQUENCY, value);  
        
    }

    public void setDfPrePaymentOption(String value)
    {
        setValue(FIELD_DFPREPAYMENTOPTION, value);  
        
        
    }

    public void setDfPrivilegePaymentOption(String value)
    {
        setValue(FIELD_DFPRIVILEGEPAYMENTOPTION, value);  
        
    }

    public void setDfRePaymentType(String value)
    {
        setValue(FIELD_DFREPAYMENTTYPE, value);  
        
        
    }
    /********************************MCM Impl Team changes ends - XS_16.13********************/

    public BigDecimal getDfSummaryMiPremiumAmount()
    {
        return (java.math.BigDecimal) getValue(FIELD_DFDEALSUMMARYMIPREMIUMAMOUNT);     
    }

    public void setDfSummaryMiPremiumAmount(BigDecimal value)
    {
        setValue(FIELD_DFDEALSUMMARYMIPREMIUMAMOUNT, value);
        
    }
    public BigDecimal getDfSummaryTaxEscrowAmount()
    {
        return (java.math.BigDecimal) getValue(FIELD_DFDEALSUMMARYTAXESCROWAMOUNT);     
    }

    public void setDfSummaryTaxEscrowAmount(BigDecimal value)
    {
        setValue(FIELD_DFDEALSUMMARYTAXESCROWAMOUNT, value);
        
    }
}


