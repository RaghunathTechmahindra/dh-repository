package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doPartyTypeModelImpl extends QueryModelBase
	implements doPartyTypeModel
{
	/**
	 *
	 *
	 */
	public doPartyTypeModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public String getDfPartyTypeDesc()
	{
		return (String)getValue(FIELD_DFPARTYTYPEDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfPartyTypeDesc(String value)
	{
		setValue(FIELD_DFPARTYTYPEDESC,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPartyTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPARTYTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPartyTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPARTYTYPEID,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL PARTYTYPE.PTDESCRIPTION, PARTYTYPE.PARTYTYPEID FROM PARTYTYPE  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="PARTYTYPE";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFPARTYTYPEDESC="PARTYTYPE.PTDESCRIPTION";
	public static final String COLUMN_DFPARTYTYPEDESC="PTDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFPARTYTYPEID="PARTYTYPE.PARTYTYPEID";
	public static final String COLUMN_DFPARTYTYPEID="PARTYTYPEID";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPARTYTYPEDESC,
				COLUMN_DFPARTYTYPEDESC,
				QUALIFIED_COLUMN_DFPARTYTYPEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPARTYTYPEID,
				COLUMN_DFPARTYTYPEID,
				QUALIFIED_COLUMN_DFPARTYTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

