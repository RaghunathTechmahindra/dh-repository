package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doBridgeReviewNewModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public String getDfBridgePurpose();

	
	/**
	 * 
	 * 
	 */
	public void setDfBridgePurpose(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLender();

	
	/**
	 * 
	 * 
	 */
	public void setDfLender(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfProduct();

	
	/**
	 * 
	 * 
	 */
	public void setDfProduct(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPaymentTermDescription();

	
	/**
	 * 
	 * 
	 */
	public void setDfPaymentTermDescription(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfRateCode();

	
	/**
	 * 
	 * 
	 */
	public void setDfRateCode(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPostedInterestRate();

	
	/**
	 * 
	 * 
	 */
	public void setDfPostedInterestRate(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDiscount();

	
	/**
	 * 
	 * 
	 */
	public void setDfDiscount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPremium();

	
	/**
	 * 
	 * 
	 */
	public void setDfPremium(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfNetRate();

	
	/**
	 * 
	 * 
	 */
	public void setDfNetRate(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBridgeLoanAmount();

	
	/**
	 * 
	 * 
	 */
	public void setDfBridgeLoanAmount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfMaturityDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfMaturityDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBridgeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBridgeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfRateDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfRateDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfDealRateDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealRateDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealPostedRate();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealPostedRate(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfProductId();

	
	/**
	 * 
	 * 
	 */
	public void setDfProductId(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBRIDGEPURPOSE="dfBridgePurpose";
	public static final String FIELD_DFLENDER="dfLender";
	public static final String FIELD_DFPRODUCT="dfProduct";
	public static final String FIELD_DFPAYMENTTERMDESCRIPTION="dfPaymentTermDescription";
	public static final String FIELD_DFRATECODE="dfRateCode";
	public static final String FIELD_DFPOSTEDINTERESTRATE="dfPostedInterestRate";
	public static final String FIELD_DFDISCOUNT="dfDiscount";
	public static final String FIELD_DFPREMIUM="dfPremium";
	public static final String FIELD_DFNETRATE="dfNetRate";
	public static final String FIELD_DFBRIDGELOANAMOUNT="dfBridgeLoanAmount";
	public static final String FIELD_DFMATURITYDATE="dfMaturityDate";
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFBRIDGEID="dfBridgeId";
	public static final String FIELD_DFRATEDATE="dfRateDate";
	public static final String FIELD_DFDEALRATEDATE="dfDealRateDate";
	public static final String FIELD_DFDEALPOSTEDRATE="dfDealPostedRate";
	public static final String FIELD_DFPRODUCTID="dfProductId";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

