package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *  <p>Title: doQualifyingDetailsModel</p> 
 *  <p>Description: This is the interface class for doQualifyingDetailsModelImpl used to retrieve fields </p>
 *
 * @author MCM Impl team
 * @version 1.0 06-JUN-2008 XS_2.13 Initial Version 
 * 
 */

public interface doQualifyingDetailsModel extends QueryModel, SelectQueryModel
{


    public static final String FIELD_DFDEALID = "dfDealId";
    public static final String FIELD_DFINSTITUTIONPROFILEID = "dfInstitutionProfileId";
    public static final String FIELD_DFQUALIFYRATE = "dfQualifyRate";
    public static final String FIELD_DFINTERESTCOMPOUNDINGID = "dfInterestCompoundingId";
    public static final String FIELD_DFAMORTIZATIONTERM = "dfAmortizationTerm";
    public static final String FIELD_DFREPAYMENTTYPEID = "dfRepaymentTypeId";
    public static final String FIELD_DFPANDIPAYMENTAMOUNTQUALIFY = "dfPAndIPaymentAmountQualify";
    public static final String FIELD_DFQUALIFYGDS = "dfQualifyGds";
    public static final String FIELD_DFQUALIFYTDS = "dfQualifyTds";

    public java.math.BigDecimal getDfDealId();	
    public java.math.BigDecimal getDfInstitutionProfileId();	
    public java.math.BigDecimal getDfQualifyRate();
    public String getDfInterestCompoundingId();
    public java.math.BigDecimal getDfAmortizationTerm();
    public String getDfRepaymentTypeId();
    public java.math.BigDecimal getDfPAndIPaymentAmountQualify();
    public java.math.BigDecimal getDfQualifyGds();
    public java.math.BigDecimal getDfQualifyTds();

    public void setDfDealId(java.math.BigDecimal value);
    public void setDfInstitutionProfileId(java.math.BigDecimal value);
    public void setDfQualifyRate(java.math.BigDecimal value);	 
    public void setDfInterestCompoundingId(String value);
    public void setDfAmortizationTerm(java.math.BigDecimal value);
    public void setDfRepaymentTypeId(String value);
    public void setDfPAndIPaymentAmountQualify(java.math.BigDecimal value);
    public void setDfQualifyGds(java.math.BigDecimal value);
    public void setDfQualifyTds(java.math.BigDecimal value);

}

