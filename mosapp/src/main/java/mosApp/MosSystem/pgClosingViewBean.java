package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mosApp.*;

import com.filogix.express.web.rc.RequestProcessException;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.jato.ExpressViewBeanBase; 
/**
 *
 *
 *
 */
public class pgClosingViewBean extends ExpressViewBeanBase

{
    private static Log _log =
        LogFactory.getLog(pgClosingViewBean.class);
    
    /**
     *
     */
    public pgClosingViewBean()
    {
        super(PAGE_NAME);
        setDefaultDisplayURL(DEFAULT_DISPLAY_URL);
        registerChildren();
        initialize();
    }


    /**
     *
     *
     */
    protected void initialize()
    {
    }


    ////////////////////////////////////////////////////////////////////////////////
    // Child manipulation methods
    ////////////////////////////////////////////////////////////////////////////////
    /**
     *
     *
     */
    protected View createChild(String name)
    {
    	View superReturn = super.createChild(name);  
    	if (superReturn != null) {  
    	return superReturn;  
    	} else if (name.equals(CHILD_TBDEALID)) 
        {
            TextField child = new TextField(this,
                getDefaultModel(),
                CHILD_TBDEALID,
                CHILD_TBDEALID,
                CHILD_TBDEALID_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_CBPAGENAMES))
        {
            ComboBox child = new ComboBox( this,
                getDefaultModel(),
                CHILD_CBPAGENAMES,
                CHILD_CBPAGENAMES,
                CHILD_CBPAGENAMES_RESET_VALUE,
                null);

      //--Release2.1--//
      //Modified to set NonSelected Label from CHILD_CBPAGENAMES_NONSELECTED_LABEL
      //--> By John 08Nov2002
      child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
      //==========================================================================

            child.setOptions(cbPageNamesOptions);
            return child;
        }
        else
        if (name.equals(CHILD_BTPROCEED))
        {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTPROCEED,
                CHILD_BTPROCEED,
                CHILD_BTPROCEED_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_HREF1))
        {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF1,
                CHILD_HREF1,
                CHILD_HREF1_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_HREF2))
        {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF2,
                CHILD_HREF2,
                CHILD_HREF2_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_HREF3))
        {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF3,
                CHILD_HREF3,
                CHILD_HREF3_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_HREF4))
        {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF4,
                CHILD_HREF4,
                CHILD_HREF4_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_HREF5))
        {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF5,
                CHILD_HREF5,
                CHILD_HREF5_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_HREF6))
        {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF6,
                CHILD_HREF6,
                CHILD_HREF6_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_HREF7))
        {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF7,
                CHILD_HREF7,
                CHILD_HREF7_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_HREF8))
        {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF8,
                CHILD_HREF8,
                CHILD_HREF8_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_HREF9))
        {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF9,
                CHILD_HREF9,
                CHILD_HREF9_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_HREF10))
        {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF10,
                CHILD_HREF10,
                CHILD_HREF10_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_STPAGELABEL))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPAGELABEL,
                CHILD_STPAGELABEL,
                CHILD_STPAGELABEL_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STCOMPANYNAME))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STCOMPANYNAME,
                CHILD_STCOMPANYNAME,
                CHILD_STCOMPANYNAME_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STTODAYDATE))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STTODAYDATE,
                CHILD_STTODAYDATE,
                CHILD_STTODAYDATE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STUSERNAMETITLE))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STUSERNAMETITLE,
                CHILD_STUSERNAMETITLE,
                CHILD_STUSERNAMETITLE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STDEALID))
        {
            StaticTextField child = new StaticTextField(this,
                getdoDealSummarySnapShotModel(),
                CHILD_STDEALID,
                doDealSummarySnapShotModel.FIELD_DFAPPLICATIONID,
                CHILD_STDEALID_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STBORRFIRSTNAME))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STBORRFIRSTNAME,
                CHILD_STBORRFIRSTNAME,
                CHILD_STBORRFIRSTNAME_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STDEALSTATUS))
        {
            StaticTextField child = new StaticTextField(this,
                getdoDealSummarySnapShotModel(),
                CHILD_STDEALSTATUS,
                doDealSummarySnapShotModel.FIELD_DFSTATUSDESCR,
                CHILD_STDEALSTATUS_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STDEALSTATUSDATE))
        {
            StaticTextField child = new StaticTextField(this,
                getdoDealSummarySnapShotModel(),
                CHILD_STDEALSTATUSDATE,
                doDealSummarySnapShotModel.FIELD_DFSTATUSDATE,
                CHILD_STDEALSTATUSDATE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STSOURCEFIRM))
        {
            StaticTextField child = new StaticTextField(this,
                getdoDealSummarySnapShotModel(),
                CHILD_STSOURCEFIRM,
                doDealSummarySnapShotModel.FIELD_DFSFSHORTNAME,
                CHILD_STSOURCEFIRM_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STSOURCE))
        {
            StaticTextField child = new StaticTextField(this,
                getdoDealSummarySnapShotModel(),
                CHILD_STSOURCE,
                doDealSummarySnapShotModel.FIELD_DFSOBPSHORTNAME,
                CHILD_STSOURCE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STLOB))
        {
            StaticTextField child = new StaticTextField(this,
                getdoDealSummarySnapShotModel(),
                CHILD_STLOB,
                doDealSummarySnapShotModel.FIELD_DFLOBDESCR,
                CHILD_STLOB_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STDEALTYPE))
        {
            StaticTextField child = new StaticTextField(this,
                getdoDealSummarySnapShotModel(),
                CHILD_STDEALTYPE,
                doDealSummarySnapShotModel.FIELD_DFDEALTYPEDESCR,
                CHILD_STDEALTYPE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STDEALPURPOSE))
        {
            StaticTextField child = new StaticTextField(this,
                getdoDealSummarySnapShotModel(),
                CHILD_STDEALPURPOSE,
                doDealSummarySnapShotModel.FIELD_DFLOANPURPOSEDESCR,
                CHILD_STDEALPURPOSE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STPURCHASEPRICE))
        {
            StaticTextField child = new StaticTextField(this,
                getdoDealSummarySnapShotModel(),
                CHILD_STPURCHASEPRICE,
                doDealSummarySnapShotModel.FIELD_DFTOTALPURCHASEPRICE,
                CHILD_STPURCHASEPRICE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STPMTTERM))
        {
            StaticTextField child = new StaticTextField(this,
                getdoDealSummarySnapShotModel(),
                CHILD_STPMTTERM,
                doDealSummarySnapShotModel.FIELD_DFPAYMENTTERMDESCR,
                CHILD_STPMTTERM_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STESTCLOSINGDATE))
        {
            StaticTextField child = new StaticTextField(this,
                getdoDealSummarySnapShotModel(),
                CHILD_STESTCLOSINGDATE,
                doDealSummarySnapShotModel.FIELD_DFESTCLOSINGDATE,
                CHILD_STESTCLOSINGDATE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STSPECIALFEATURE))
        {
            StaticTextField child = new StaticTextField(this,
                getdoDealSummarySnapShotModel(),
                CHILD_STSPECIALFEATURE,
                doDealSummarySnapShotModel.FIELD_DFSPECIALFEATURE,
                CHILD_STSPECIALFEATURE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_BTSUBMIT))
        {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTSUBMIT,
                CHILD_BTSUBMIT,
                CHILD_BTSUBMIT_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_BTWORKQUEUELINK))
        {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTWORKQUEUELINK,
                CHILD_BTWORKQUEUELINK,
                CHILD_BTWORKQUEUELINK_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_CHANGEPASSWORDHREF))
        {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_CHANGEPASSWORDHREF,
                CHILD_CHANGEPASSWORDHREF,
                CHILD_CHANGEPASSWORDHREF_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_BTTOOLHISTORY))
        {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTTOOLHISTORY,
                CHILD_BTTOOLHISTORY,
                CHILD_BTTOOLHISTORY_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_BTTOONOTES))
        {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTTOONOTES,
                CHILD_BTTOONOTES,
                CHILD_BTTOONOTES_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_BTTOOLSEARCH))
        {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTTOOLSEARCH,
                CHILD_BTTOOLSEARCH,
                CHILD_BTTOOLSEARCH_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_BTTOOLLOG))
        {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTTOOLLOG,
                CHILD_BTTOOLLOG,
                CHILD_BTTOOLLOG_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_STERRORFLAG))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STERRORFLAG,
                CHILD_STERRORFLAG,
                CHILD_STERRORFLAG_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_DETECTALERTTASKS))
        {
            HiddenField child = new HiddenField(this,
                getDefaultModel(),
                CHILD_DETECTALERTTASKS,
                CHILD_DETECTALERTTASKS,
                CHILD_DETECTALERTTASKS_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STTOTALLOANAMOUNT))
        {
            StaticTextField child = new StaticTextField(this,
                getdoDealSummarySnapShotModel(),
                CHILD_STTOTALLOANAMOUNT,
                doDealSummarySnapShotModel.FIELD_DFTOTALLOANAMT,
                CHILD_STTOTALLOANAMOUNT_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_BTCANCEL))
        {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTCANCEL,
                CHILD_BTCANCEL,
                CHILD_BTCANCEL_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_STDATEMASK))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STDATEMASK,
                CHILD_STDATEMASK,
                CHILD_STDATEMASK_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_BTPREVTASKPAGE))
        {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTPREVTASKPAGE,
                CHILD_BTPREVTASKPAGE,
                CHILD_BTPREVTASKPAGE_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_STPREVTASKPAGELABEL))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPREVTASKPAGELABEL,
                CHILD_STPREVTASKPAGELABEL,
                CHILD_STPREVTASKPAGELABEL_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_BTNEXTTASKPAGE))
        {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTNEXTTASKPAGE,
                CHILD_BTNEXTTASKPAGE,
                CHILD_BTNEXTTASKPAGE_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_STNEXTTASKPAGELABEL))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STNEXTTASKPAGELABEL,
                CHILD_STNEXTTASKPAGELABEL,
                CHILD_STNEXTTASKPAGELABEL_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STTASKNAME))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STTASKNAME,
                CHILD_STTASKNAME,
                CHILD_STTASKNAME_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_SESSIONUSERID))
        {
            HiddenField child = new HiddenField(this,
                getDefaultModel(),
                CHILD_SESSIONUSERID,
                CHILD_SESSIONUSERID,
                CHILD_SESSIONUSERID_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STPARTYNAME))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPARTYNAME,
                CHILD_STPARTYNAME,
                CHILD_STPARTYNAME_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STPARTYCOMPANYNAME))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPARTYCOMPANYNAME,
                CHILD_STPARTYCOMPANYNAME,
                CHILD_STPARTYCOMPANYNAME_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STCITY))
        {
            StaticTextField child = new StaticTextField(this,
                getdoClosingActivityInfoModel(),
                CHILD_STCITY,
                doClosingActivityInfoModel.FIELD_DFCITY,
                CHILD_STCITY_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_HDADDRID))
        {
            HiddenField child = new HiddenField(this,
                getdoClosingActivityInfoModel(),
                CHILD_HDADDRID,
                doClosingActivityInfoModel.FIELD_DFADDRID,
                CHILD_HDADDRID_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STADDRESSLINE1))
        {
            StaticTextField child = new StaticTextField(this,
                getdoClosingActivityInfoModel(),
                CHILD_STADDRESSLINE1,
                doClosingActivityInfoModel.FIELD_DFADDRESSLINE1,
                CHILD_STADDRESSLINE1_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STADDRESSLINE2))
        {
            StaticTextField child = new StaticTextField(this,
                getdoClosingActivityInfoModel(),
                CHILD_STADDRESSLINE2,
                doClosingActivityInfoModel.FIELD_DFADDRESSLINE2,
                CHILD_STADDRESSLINE2_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STPROVINCEABBREVIATION))
        {
            StaticTextField child = new StaticTextField(this,
                getdoClosingActivityInfoModel(),
                CHILD_STPROVINCEABBREVIATION,
                doClosingActivityInfoModel.FIELD_DFPROVINCEABBREVIATION,
                CHILD_STPROVINCEABBREVIATION_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STPOSTALFSALDU))
        {
            StaticTextField child = new StaticTextField(this,
                getdoClosingActivityInfoModel(),
                CHILD_STPOSTALFSALDU,
                doClosingActivityInfoModel.FIELD_DFPOSTALFSALDU,
                CHILD_STPOSTALFSALDU_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STCONTACTPHONENUMBER))
        {
            StaticTextField child = new StaticTextField(this,
                getdoClosingActivityInfoModel(),
                CHILD_STCONTACTPHONENUMBER,

                doClosingActivityInfoModel.FIELD_DFPHONENUMBER,
                CHILD_STCONTACTPHONENUMBER_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STCONTACTPHONENUMEXTENSION))
        {
            StaticTextField child = new StaticTextField(this,
                getdoClosingActivityInfoModel(),
                CHILD_STCONTACTPHONENUMEXTENSION,
                doClosingActivityInfoModel.FIELD_DFPHONENUMBEREXTENSION,
                CHILD_STCONTACTPHONENUMEXTENSION_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STCONTACTFAXNUMBER))
        {
            StaticTextField child = new StaticTextField(this,
                getdoClosingActivityInfoModel(),
                CHILD_STCONTACTFAXNUMBER,
                doClosingActivityInfoModel.FIELD_DFFAXNUMBER,
                CHILD_STCONTACTFAXNUMBER_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STCONTACTEMAILADDRESS))
        {
            StaticTextField child = new StaticTextField(this,
                getdoClosingActivityInfoModel(),
                CHILD_STCONTACTEMAILADDRESS,
                doClosingActivityInfoModel.FIELD_DFEMAILADDRESS,
                CHILD_STCONTACTEMAILADDRESS_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_TBINSTRUCTIONS))
        {
            TextField child = new TextField(this,
                getDefaultModel(),
                CHILD_TBINSTRUCTIONS,
                CHILD_TBINSTRUCTIONS,
                CHILD_TBINSTRUCTIONS_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_BTASSIGNSOLICITOR))
        {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTASSIGNSOLICITOR,
                CHILD_BTASSIGNSOLICITOR,
                CHILD_BTASSIGNSOLICITOR_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_BTCHANGESOLICITOR))
        {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTCHANGESOLICITOR,
                CHILD_BTCHANGESOLICITOR,
                CHILD_BTCHANGESOLICITOR_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_STESTIMATEDCLOSEDATE))
        {
            StaticTextField child = new StaticTextField(this,
                getdoClosingDetailsModel(),
                CHILD_STESTIMATEDCLOSEDATE,
                doClosingDetailsModel.FIELD_DFESTIMATEDCLOSINGDATE,
                CHILD_STESTIMATEDCLOSEDATE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STINTERESTADJUSTMENTDATE))
        {
            StaticTextField child = new StaticTextField(this,
                getdoClosingDetailsModel(),
                CHILD_STINTERESTADJUSTMENTDATE,
                doClosingDetailsModel.FIELD_DFINTERESTADJUSTMENTDATE,
                CHILD_STINTERESTADJUSTMENTDATE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STIADNUMDAYSDESC))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STIADNUMDAYSDESC,
                CHILD_STIADNUMDAYSDESC,
                CHILD_STIADNUMDAYSDESC_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STFIRSTPAYMENTDATE))
        {
            StaticTextField child = new StaticTextField(this,
                getdoClosingDetailsModel(),
                CHILD_STFIRSTPAYMENTDATE,
                doClosingDetailsModel.FIELD_DFFIRSTPAYMENTDATE,
                CHILD_STFIRSTPAYMENTDATE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STFIRSTPAYMENTDATEMTHLYDESC))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STFIRSTPAYMENTDATEMTHLYDESC,
                CHILD_STFIRSTPAYMENTDATEMTHLYDESC,
                CHILD_STFIRSTPAYMENTDATEMTHLYDESC_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STMATURITYDATE))
        {
            StaticTextField child = new StaticTextField(this,
                getdoClosingDetailsModel(),
                CHILD_STMATURITYDATE,
                doClosingDetailsModel.FIELD_DFMATURITYDATE,
                CHILD_STMATURITYDATE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STFORCEFIRSTMONTHDESC))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STFORCEFIRSTMONTHDESC,
                CHILD_STFORCEFIRSTMONTHDESC,
                CHILD_STFORCEFIRSTMONTHDESC_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STPERDIEMINTEREST))
        {
            StaticTextField child = new StaticTextField(this,
                getdoClosingDetailsModel(),
                CHILD_STPERDIEMINTEREST,
                doClosingDetailsModel.FIELD_DFPERDIEMINTEREST,
                CHILD_STPERDIEMINTEREST_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STINTERESTRATE))
        {
            StaticTextField child = new StaticTextField(this,
                getdoClosingDetailsModel(),
                CHILD_STINTERESTRATE,
                doClosingDetailsModel.FIELD_DFNETINTERESTRATE,
                CHILD_STINTERESTRATE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STINTERESTCOMPOUNDMSG))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STINTERESTCOMPOUNDMSG,
                CHILD_STINTERESTCOMPOUNDMSG,
                CHILD_STINTERESTCOMPOUNDMSG_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STLOANAMOUNT))
        {
            StaticTextField child = new StaticTextField(this,
                getdoClosingDetailsModel(),
                CHILD_STLOANAMOUNT,
                doClosingDetailsModel.FIELD_DFTOTALLOANBRIDGEAMOUNT,
                CHILD_STLOANAMOUNT_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STIADINTEREST))
        {
            StaticTextField child = new StaticTextField(this,
                getdoClosingDetailsModel(),
                CHILD_STIADINTEREST,
                doClosingDetailsModel.FIELD_DFIADINTEREST,
                CHILD_STIADINTEREST_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STHOLDBACKAMOUNT))
        {
            StaticTextField child = new StaticTextField(this,
                getdoClosingDetailsModel(),
                CHILD_STHOLDBACKAMOUNT,
                doClosingDetailsModel.FIELD_DFADVANCEHOLDBACK,
                CHILD_STHOLDBACKAMOUNT_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_RPTDEALFEES))
        {
            pgClosingrptDealFeesTiledView child = new pgClosingrptDealFeesTiledView(this,
                CHILD_RPTDEALFEES);
            return child;
        }
        else
        if (name.equals(CHILD_STPMGENERATE))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMGENERATE,
                CHILD_STPMGENERATE,
                CHILD_STPMGENERATE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STPMHASTITLE))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMHASTITLE,
                CHILD_STPMHASTITLE,
                CHILD_STPMHASTITLE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STPMHASINFO))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMHASINFO,
                CHILD_STPMHASINFO,
                CHILD_STPMHASINFO_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STPMHASTABLE))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMHASTABLE,
                CHILD_STPMHASTABLE,
                CHILD_STPMHASTABLE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STPMHASOK))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMHASOK,
                CHILD_STPMHASOK,
                CHILD_STPMHASOK_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STPMTITLE))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMTITLE,
                CHILD_STPMTITLE,
                CHILD_STPMTITLE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STPMINFOMSG))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMINFOMSG,
                CHILD_STPMINFOMSG,
                CHILD_STPMINFOMSG_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STPMONOK))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMONOK,
                CHILD_STPMONOK,
                CHILD_STPMONOK_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STPMMSGS))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMMSGS,
                CHILD_STPMMSGS,
                CHILD_STPMMSGS_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STPMMSGTYPES))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMMSGTYPES,
                CHILD_STPMMSGTYPES,
                CHILD_STPMMSGTYPES_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STAMGENERATE))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMGENERATE,
                CHILD_STAMGENERATE,
                CHILD_STAMGENERATE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STAMHASTITLE))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMHASTITLE,
                CHILD_STAMHASTITLE,
                CHILD_STAMHASTITLE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STAMHASINFO))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMHASINFO,
                CHILD_STAMHASINFO,
                CHILD_STAMHASINFO_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STAMHASTABLE))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMHASTABLE,
                CHILD_STAMHASTABLE,
                CHILD_STAMHASTABLE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STAMTITLE))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMTITLE,
                CHILD_STAMTITLE,
                CHILD_STAMTITLE_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STAMINFOMSG))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMINFOMSG,
                CHILD_STAMINFOMSG,
                CHILD_STAMINFOMSG_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STAMMSGS))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMMSGS,
                CHILD_STAMMSGS,
                CHILD_STAMMSGS_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STAMMSGTYPES))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMMSGTYPES,
                CHILD_STAMMSGTYPES,
                CHILD_STAMMSGTYPES_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STAMDIALOGMSG))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMDIALOGMSG,
                CHILD_STAMDIALOGMSG,
                CHILD_STAMDIALOGMSG_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_STAMBUTTONSHTML))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMBUTTONSHTML,
                CHILD_STAMBUTTONSHTML,
                CHILD_STAMBUTTONSHTML_RESET_VALUE,
                null);
            return child;
        }
        else
        if (name.equals(CHILD_BTOK))
        {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTOK,
                CHILD_BTOK,
                CHILD_BTOK_RESET_VALUE,
                null);
                return child;

        }
        else
        if (name.equals(CHILD_STVIEWONLYTAG))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STVIEWONLYTAG,
                CHILD_STVIEWONLYTAG,
                CHILD_STVIEWONLYTAG_RESET_VALUE,
                null);
            return child;
        }
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> by BILLY 15July2002
    else
    if (name.equals(CHILD_BTACTMSG))
        {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTACTMSG,
                CHILD_BTACTMSG,
                CHILD_BTACTMSG_RESET_VALUE,
                new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));
                return child;
    }
    //===========================================
    //// Separation of the date and the string label
        else
        if (name.equals(CHILD_STONEMONTHAFTERIAD))
        {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STONEMONTHAFTERIAD,
                CHILD_STONEMONTHAFTERIAD,
                CHILD_STONEMONTHAFTERIAD_RESET_VALUE,
                null);
            return child;
        }
    //--Release2.1--//
    //// New link to toggle language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new session
    //// language id throughout the all modulus.
        else
        if (name.equals(CHILD_TOGGLELANGUAGEHREF))
        {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_TOGGLELANGUAGEHREF,
                CHILD_TOGGLELANGUAGEHREF,
                CHILD_TOGGLELANGUAGEHREF_RESET_VALUE,
                null);
                return child;
        }
        // SEAN Closing service Jan 17, 2006: Creating the children.
        // Produce Solicitor Package checkbox.
        else if (name.equals(CHILD_CHPRODUCESOLICITORPKG)) {
            CheckBox child = new CheckBox(this,
                                          getDefaultModel(),
                                          CHILD_CHPRODUCESOLICITORPKG,
                                          CHILD_CHPRODUCESOLICITORPKG,
                                          "Y", "N", false, null);
            return child;
        }
        // outsourced closing service
        // ------ provider combobox
        else if (name.equals(CHILD_CBOCSPROVIDER)) {
            ComboBox child = new ComboBox(this,
                                          getdoOutsourcedClosingServiceModel(),
                                          CHILD_CBOCSPROVIDER,
                                          doOutsourcedClosingServiceModel.FIELD_DFOCSPROVIDERID,
                                          CHILD_CBOCSPROVIDER_RESET_VALUE,
                                          null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbOCSProviderOptions);

            return child;
        }
        else if (name.equals(CHILD_CBOCSPRODUCT)) {
            ComboBox child = new ComboBox(this,
                                          getdoOutsourcedClosingServiceModel(),
                                          CHILD_CBOCSPRODUCT,
                                          doOutsourcedClosingServiceModel.FIELD_DFOCSPRODUCTID,
                                          CHILD_CBOCSPRODUCT_RESET_VALUE,
                                          null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbOCSProductOptions);

            return child;
        }
        else if (name.equals(CHILD_STOCSREQUESTREFNUM)) {
            StaticTextField child =
                new StaticTextField(this,
                                    getdoOutsourcedClosingServiceModel(),
                                    CHILD_STOCSREQUESTREFNUM,
                                    doOutsourcedClosingServiceModel.FIELD_DFOCSREQUESTREFNUM,
                                    CHILD_STOCSREQUESTREFNUM_RESET_VALUE,
                                    null);
            return child;
        }
        else if (name.equals(CHILD_STOCSREQUESTSTATUS)) {
            StaticTextField child =
                new StaticTextField(this,
                                    getdoOutsourcedClosingServiceModel(),
                                    CHILD_STOCSREQUESTSTATUS,
                                    doOutsourcedClosingServiceModel.FIELD_DFOCSREQUESTSTATUS,
                                    CHILD_STOCSREQUESTSTATUS_RESET_VALUE,
                                    null);
            return child;
        }
		else if (name.equals(CHILD_STOCSREQUESTSTATUSID)) {
            StaticTextField child =
                new StaticTextField(this,
                                    getdoOutsourcedClosingServiceModel(),
                                    CHILD_STOCSREQUESTSTATUSID,
                                    doOutsourcedClosingServiceModel.FIELD_DFOCSREQUESTSTATUSID,
                                    CHILD_STOCSREQUESTSTATUSID_RESET_VALUE,
                                    null);
            return child;
        }
        else if (name.equals(CHILD_STOCSREQUESTSTATUSMSG)) {
            StaticTextField child =
                new StaticTextField(this,
                                    getdoOutsourcedClosingServiceModel(),
                                    CHILD_STOCSREQUESTSTATUSMSG,
                                    doOutsourcedClosingServiceModel.FIELD_DFOCSREQUESTSTATUSMSG,
                                    CHILD_STOCSREQUESTSTATUSMSG_RESET_VALUE,
                                    null);
            return child;
        }
        else if (name.equals(CHILD_STOCSREQUESTSTATUSDATE)) {
            StaticTextField child =
                new StaticTextField(this,
                                    getdoOutsourcedClosingServiceModel(),
                                    CHILD_STOCSREQUESTSTATUSDATE,
                                    doOutsourcedClosingServiceModel.FIELD_DFOCSREQUESTSTATUSDATE,
                                    CHILD_STOCSREQUESTSTATUSDATE_RESET_VALUE,
                                    null);
            return child;
        }
        else if (name.equals(CHILD_BTOCSUPDATE)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTOCSUPDATE,
                CHILD_BTOCSUPDATE,
                CHILD_BTOCSUPDATE_RESET_VALUE,
                null);
                return child;

        }
        else if (name.equals(CHILD_BTOCSREQUEST)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTOCSREQUEST,
                CHILD_BTOCSREQUEST,
                CHILD_BTOCSREQUEST_RESET_VALUE,
                null);
                return child;

        }

        else if (name.equals(CHILD_BTOCSCANCEL)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTOCSCANCEL,
                CHILD_BTOCSCANCEL,
                CHILD_BTOCSCANCEL_RESET_VALUE,
                null);
                return child;

        }
        // Fixed Price Closing Package
        // ------ Procider combobox
        else if (name.equals(CHILD_CBFPCPROVIDER)) {
            ComboBox child = new ComboBox(this,
                            getdoFixedPriceClosingInfoModel(),
                            CHILD_CBFPCPROVIDER,
                            doFixedPriceClosingInfoModel.FIELD_DFFPCPROVIDERID,
                            CHILD_CBFPCPROVIDER_RESET_VALUE,
                            null);            
            child.setLabelForNoneSelected("");
            child.setOptions(cbFPCProviderOptions);
            return child;
        }
        else if (name.equals(CHILD_CBFPCPRODUCT)) {
            ComboBox child = new ComboBox(this,
                            getdoFixedPriceClosingInfoModel(),
                            CHILD_CBFPCPRODUCT,
                            doFixedPriceClosingInfoModel.FIELD_DFFPCPRODUCTID,
                            CHILD_CBFPCPRODUCT_RESET_VALUE,
                            null);            
            child.setLabelForNoneSelected("");
            child.setOptions(cbFPCProductOptions);
            return child;
        }
        // SEAN Closing service END
        else if (name.equals(CHILD_STFPCREQUESTSTATUS)) {
            StaticTextField child =
                new StaticTextField(this,
                                    getdoFixedPriceClosingInfoModel(),
                                    CHILD_STFPCREQUESTSTATUS,
                                    doFixedPriceClosingInfoModel.FIELD_DFFPCREQUESTSTATUS,
                                    CHILD_STFPCREQUESTSTATUS_RESET_VALUE,
                                    null);
                return child;
        }
		else if (name.equals(CHILD_STFPCREQUESTSTATUSID)) {
            StaticTextField child =
                new StaticTextField(this,
                                    getdoFixedPriceClosingInfoModel(),
                                    CHILD_STFPCREQUESTSTATUSID,
                                    doFixedPriceClosingInfoModel.FIELD_DFFPCREQUESTSTATUSID,
                                    CHILD_STFPCREQUESTSTATUSID_RESET_VALUE,
                                    null);
                return child;
        }
        else if (name.equals(CHILD_STFPCREQUESTDATE)) {
            StaticTextField child =
                new StaticTextField(this,
                                    getdoFixedPriceClosingInfoModel(),
                                    CHILD_STFPCREQUESTDATE,
                                    doFixedPriceClosingInfoModel.FIELD_DFFPCREQUESTDATE,
                                    CHILD_STFPCREQUESTDATE_RESET_VALUE,
                                    null);
                return child;
        }
        else if (name.equals(CHILD_CHFPCBORROWERINFO)) {
            CheckBox child = new CheckBox(this,
                                      getDefaultModel(),
                                      CHILD_CHFPCBORROWERINFO,
                                      CHILD_CHFPCBORROWERINFO,
                                      "Y", "N", false, null);
            return child;
        }
        if (name.equals(CHILD_BTFPCSUBMITREFERRAL))
        {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTFPCSUBMITREFERRAL,
                CHILD_BTFPCSUBMITREFERRAL,
                CHILD_BTFPCSUBMITREFERRAL_RESET_VALUE,
                null);
                return child;
        }
   
        else if (name.equals(CHILD_STFPCCONTACTFIRSTNAME)) {
            TextField child =
                new TextField(this,
                                    getdoFixedPriceClosingInfoModel(),
                                    CHILD_STFPCCONTACTFIRSTNAME,
                                    doFixedPriceClosingInfoModel.FIELD_DFFPCCONTACTFIRSTNAME,
                                    CHILD_STFPCCONTACTFIRSTNAME_RESET_VALUE,
                                    null);
                return child;
        }
        else if (name.equals(CHILD_STFPCCONTACTLASTNAME)) {
            TextField child =
                new TextField(this,
                                    getdoFixedPriceClosingInfoModel(),
                                    CHILD_STFPCCONTACTLASTNAME,
                                    doFixedPriceClosingInfoModel.FIELD_DFFPCCONTACTLASTNAME,
                                    CHILD_STFPCCONTACTLASTNAME_RESET_VALUE,
                                    null);
                return child;
        }
        else if (name.equals(CHILD_TBFPCEMAIL)) {
            TextField child =
                new TextField(this,
                                    getdoFixedPriceClosingInfoModel(),
                                    CHILD_TBFPCEMAIL,
                                    doFixedPriceClosingInfoModel.FIELD_DFFPCEMAIL,
                                    CHILD_TBFPCEMAIL_RESET_VALUE,
                                    null);
                return child;
        }   
        else if (name.equals(CHILD_TBFPCWORKPHONEAREACODE)) {
            TextField child =
                new TextField(this,
                                    getdoFixedPriceClosingInfoModel(),
                                    CHILD_TBFPCWORKPHONEAREACODE,
                                    doFixedPriceClosingInfoModel.FIELD_DFFPCWORKPHONEAREACODE,
                                    CHILD_TBFPCWORKPHONEAREACODE_RESET_VALUE,
                                    null);
                return child;
        }        
        else if (name.equals(CHILD_TBFPCWORKPHONEFIRSTTHREEDIGITS)) {
            TextField child =
                new TextField(this,
                                    getdoFixedPriceClosingInfoModel(),
                                    CHILD_TBFPCWORKPHONEFIRSTTHREEDIGITS,
                                    doFixedPriceClosingInfoModel.FIELD_DFFPCWORKPHONEFIRSTTHREEDIGITS,
                                    CHILD_TBFPCWORKPHONEFIRSTTHREEDIGITS_RESET_VALUE,
                                    null);
                return child;
        }  
        else if (name.equals(CHILD_TBFPCWORKPHONELASTFOURDIGITS)) {
            TextField child =
                new TextField(this,
                                    getdoFixedPriceClosingInfoModel(),
                                    CHILD_TBFPCWORKPHONELASTFOURDIGITS,
                                    doFixedPriceClosingInfoModel.FIELD_DFFPCWORKPHONELASTFOURDIGITS,
                                    CHILD_TBFPCWORKPHONELASTFOURDIGITS_RESET_VALUE,
                                    null);
                return child;
        }  


        else if (name.equals(CHILD_TBFPCWORKPHONENUMEXTENSION)) {
            TextField child =
                new TextField(this,
                                    getdoFixedPriceClosingInfoModel(),
                                    CHILD_TBFPCWORKPHONENUMEXTENSION,
                                    doFixedPriceClosingInfoModel.FIELD_DFFPCWORKPHONENUMEXTENSION,
                                    CHILD_TBFPCWORKPHONENUMEXTENSION_RESET_VALUE,
                                    null);
                return child;
        }
        else if (name.equals(CHILD_TBFPCCELLPHONEAREACODE)) {
            TextField child =
                new TextField(this,
                                    getdoFixedPriceClosingInfoModel(),
                                    CHILD_TBFPCCELLPHONEAREACODE,
                                    doFixedPriceClosingInfoModel.FIELD_DFFPCCELLPHONEAREACODE,
                                    CHILD_TBFPCCELLPHONEAREACODE_RESET_VALUE,
                                    null);
                return child;
        }        
        else if (name.equals(CHILD_TBFPCCELLPHONEFIRSTTHREEDIGITS)) {
            TextField child =
                new TextField(this,
                                    getdoFixedPriceClosingInfoModel(),
                                    CHILD_TBFPCCELLPHONEFIRSTTHREEDIGITS,
                                    doFixedPriceClosingInfoModel.FIELD_DFFPCCELLPHONEFIRSTTHREEDIGITS,
                                    CHILD_TBFPCCELLPHONEFIRSTTHREEDIGITS_RESET_VALUE,
                                    null);
                return child;
        }

        else if (name.equals(CHILD_TBFPCCELLPHONELASTFOURDIGITS)) {
            TextField child =
                new TextField(this,
                                    getdoFixedPriceClosingInfoModel(),
                                    CHILD_TBFPCCELLPHONELASTFOURDIGITS,
                                    doFixedPriceClosingInfoModel.FIELD_DFFPCCELLPHONELASTFOURDIGITS,
                                    CHILD_TBFPCCELLPHONELASTFOURDIGITS_RESET_VALUE,
                                    null);
                return child;
        }  
        else if (name.equals(CHILD_TBFPCHOMEPHONEAREACODE)) {
            TextField child =
                new TextField(this,
                                    getdoFixedPriceClosingInfoModel(),
                                    CHILD_TBFPCHOMEPHONEAREACODE,
                                    doFixedPriceClosingInfoModel.FIELD_DFFPCHOMEPHONEAREACODE,
                                    CHILD_TBFPCHOMEPHONEAREACODE_RESET_VALUE,
                                    null);
                return child;
        }        
        else if (name.equals(CHILD_TBFPCHOMEPHONEFIRSTTHREEDIGITS)) {
            TextField child =
                new TextField(this,
                                    getdoFixedPriceClosingInfoModel(),
                                    CHILD_TBFPCHOMEPHONEFIRSTTHREEDIGITS,
                                    doFixedPriceClosingInfoModel.FIELD_DFFPCHOMEPHONEFIRSTTHREEDIGITS,
                                    CHILD_TBFPCHOMEPHONEFIRSTTHREEDIGITS_RESET_VALUE,
                                    null);
                return child;
        }   

        else if (name.equals(CHILD_TBFPCHOMEPHONELASTFOURDIGITS)) {
            TextField child =
                new TextField(this,
                                    getdoFixedPriceClosingInfoModel(),
                                    CHILD_TBFPCHOMEPHONELASTFOURDIGITS,
                                    doFixedPriceClosingInfoModel.FIELD_DFFPCHOMEPHONELASTFOURDIGITS,
                                    CHILD_TBFPCHOMEPHONELASTFOURDIGITS_RESET_VALUE,
                                    null);
                return child;
        }  
        else if (name.equals(CHILD_STFPCCOMMENTS)) {
            StaticTextField child =
                new StaticTextField(this,
                                    getdoFixedPriceClosingInfoModel(),
                                    CHILD_STFPCCOMMENTS,
                                    doFixedPriceClosingInfoModel.FIELD_DFFPCCOMMENTS,
                                    CHILD_STFPCCOMMENTS_RESET_VALUE,
                                    null);
                return child;
        }
        else if (name.equals(CHILD_STFPCREQUESTMESSAGE)) {
            StaticTextField child =
                new StaticTextField(this,
                                    getdoFixedPriceClosingInfoModel(),
                                    CHILD_STFPCREQUESTMESSAGE,
                                    doFixedPriceClosingInfoModel.FIELD_DFFPCREQUESTMESSAGE,
                                    CHILD_STFPCREQUESTMESSAGE_RESET_VALUE,
                                    null);
            return child;
        } 
        else if (name.equals(CHILD_STOCSHIDDENSTART)) {
            StaticTextField child =
                new StaticTextField(this,
                                    getDefaultModel(),
                                    CHILD_STOCSHIDDENSTART,
                                    CHILD_STOCSHIDDENSTART,
                                    CHILD_STOCSHIDDENSTART_RESET_VALUE,
                                    null);
            return child;
        } 
        else if (name.equals(CHILD_STFPCCOMMENTHIDDEN)) {
            StaticTextField child =
                new StaticTextField(this,
                                    getDefaultModel(),
                                    CHILD_STFPCCOMMENTHIDDEN,
                                    CHILD_STFPCCOMMENTHIDDEN,
                                    CHILD_STFPCCOMMENTHIDDEN_RESET_VALUE,
                                    null);
            return child;
        }
        else if (name.equals(CHILD_STOCSHIDDENEND)) {
            StaticTextField child =
                new StaticTextField(this,
                                    getDefaultModel(),
                                    CHILD_STOCSHIDDENEND,
                                    CHILD_STOCSHIDDENEND,
                                    CHILD_STOCSHIDDENEND_RESET_VALUE,
                                    null);
            return child;
        }
        else if (name.equals(CHILD_STFPCHIDDENSTART)) {
            StaticTextField child =
                new StaticTextField(this,
                                    getDefaultModel(),
                                    CHILD_STFPCHIDDENSTART,
                                    CHILD_STFPCHIDDENSTART,
                                    CHILD_STFPCHIDDENSTART_RESET_VALUE,
                                    null);
            return child;
        } else if (name.equals(CHILD_STFPCHIDDENEND)) {
            StaticTextField child =
                new StaticTextField(this,
                                    getDefaultModel(),
                                    CHILD_STFPCHIDDENEND,
                                    CHILD_STFPCHIDDENEND,
                                    CHILD_STFPCHIDDENEND_RESET_VALUE,
                                    null);
            return child;
        } else if (name.equals(CHILD_STHDDEALID)) {
              HiddenField child = new HiddenField(this, 
                                    getdoClosingDetailsModel(), 
                                    CHILD_STHDDEALID,
                                    doClosingDetailsModel.FIELD_DFDEALID,
                                    CHILD_STHDDEALID_RESET_VALUE, null);  
              return child;
        } else if (name.equals(CHILD_STHDDEALCOPYID)) {
              HiddenField child = new HiddenField(this, 
                        getdoClosingDetailsModel(), 
                        CHILD_STHDDEALCOPYID,
                        doClosingDetailsModel.FIELD_DFCOPYID,
                        CHILD_STHDDEALID_RESET_VALUE, null);  
              return child;
        } else if(name.equals(CHILD_STHDPRODUCTID)) {
              HiddenField child = new HiddenField(this,
                getdoOutsourcedClosingServiceModel(),
                CHILD_STHDPRODUCTID,
                doOutsourcedClosingServiceModel.FIELD_DFOCSPRODUCTID,
                CHILD_STHDPRODUCTID_RESET_VALUE,
                null);
              return child;
         }
         else if(name.equals(CHILD_STHDFPCPROVIDERID)) {
             HiddenField child = new HiddenField(this,
                     getdoFixedPriceClosingInfoModel(),
                     CHILD_STHDFPCPROVIDERID,
                     doFixedPriceClosingInfoModel.FIELD_DFFPCPROVIDERID,
                     CHILD_STHDFPCPROVIDERID_RESET_VALUE,
                     null);
                   return child;
         }
         else if(name.equals(CHILD_STHDFPCPRODUCTID)) {
             HiddenField child = new HiddenField(this,
                     getdoFixedPriceClosingInfoModel(),
                     CHILD_STHDFPCPRODUCTID,
                     doFixedPriceClosingInfoModel.FIELD_DFFPCPRODUCTID,
                     CHILD_STHDFPCPRODUCTID_RESET_VALUE,
                     null);
                   return child;
         }
         else if(name.equals(CHILD_STHDFPCPROVIDERREFNO)) {
             HiddenField child = new HiddenField(this,
                     getdoFixedPriceClosingInfoModel(),
                     CHILD_STHDFPCPROVIDERREFNO,
                     doFixedPriceClosingInfoModel.FIELD_DFFPCPROVIDERREFNO,
                     CHILD_STHDFPCPROVIDERREFNO_RESET_VALUE,
                     null);
                   return child;
         }
         else if(name.equals(CHILD_STHDOCSREQUESTID)) {
             HiddenField child = new HiddenField(this,
                     getdoOutsourcedClosingServiceModel(),
                     CHILD_STHDOCSREQUESTID,
                     doOutsourcedClosingServiceModel.FIELD_DFOCSREQUESTID,
                     CHILD_STHDOCSREQUESTID_RESET_VALUE,
                     null);
                   return child;
         }
         else if(name.equals(CHILD_STHDFPCREQUESTID)) {
             HiddenField child = new HiddenField(this,
                     getdoFixedPriceClosingInfoModel(),
                     CHILD_STHDFPCREQUESTID,
                     doFixedPriceClosingInfoModel.FIELD_DFFPCREQUESTID,
                     CHILD_STHDFPCREQUESTID_RESET_VALUE,
                     null);
                   return child;
         }
         else if(name.equals(CHILD_STHDOCSCOPYID)) {
             HiddenField child = new HiddenField(this,
                     getdoOutsourcedClosingServiceModel(),
                     CHILD_STHDOCSCOPYID,
                     doOutsourcedClosingServiceModel.FIELD_DFOCSCOPYID,
                     CHILD_STHDOCSCOPYID_RESET_VALUE,
                     null);
                   return child;
         }
         else if(name.equals(CHILD_STHDFPCCOPYID)) {
             HiddenField child = new HiddenField(this,
                     getdoFixedPriceClosingInfoModel(),
                     CHILD_STHDFPCCOPYID,
                     doFixedPriceClosingInfoModel.FIELD_DFFPCCOPYID,
                     CHILD_STHDFPCCOPYID_RESET_VALUE,
                     null);
                   return child;
         }
         else if(name.equals(CHILD_STHDFPCCONTACTID)) {
             HiddenField child = new HiddenField(this,
                     getdoFixedPriceClosingInfoModel(),
                     CHILD_STHDFPCCONTACTID,
                     doFixedPriceClosingInfoModel.FIELD_DFFPCCONTACTID,
                     CHILD_STHDFPCCONTACTID_RESET_VALUE,
                     null);
                   return child;
         }
		 else if(name.equals(CHILD_STHDFPCBORROWERID)) {
             HiddenField child = new HiddenField(this,
                     getdoFixedPriceClosingInfoModel(),
                     CHILD_STHDFPCBORROWERID,
                     doFixedPriceClosingInfoModel.FIELD_DFFPCBORROWERID,
                     CHILD_STHDFPCBORROWERID_RESET_VALUE,
                     null);
                   return child;
         }
        //Jerry end Closing
        else
            throw new IllegalArgumentException("Invalid child name [" + name + "]");
    }


    /**
     *
     *
     */
    public void resetChildren()
    {
    	super.resetChildren(); 
    	getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
        getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
        getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
        getHref1().setValue(CHILD_HREF1_RESET_VALUE);
        getHref2().setValue(CHILD_HREF2_RESET_VALUE);
        getHref3().setValue(CHILD_HREF3_RESET_VALUE);
        getHref4().setValue(CHILD_HREF4_RESET_VALUE);
        getHref5().setValue(CHILD_HREF5_RESET_VALUE);
        getHref6().setValue(CHILD_HREF6_RESET_VALUE);
        getHref7().setValue(CHILD_HREF7_RESET_VALUE);
        getHref8().setValue(CHILD_HREF8_RESET_VALUE);
        getHref9().setValue(CHILD_HREF9_RESET_VALUE);
        getHref10().setValue(CHILD_HREF10_RESET_VALUE);
        getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
        getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
        getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
        getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
        getStDealId().setValue(CHILD_STDEALID_RESET_VALUE);
        getStBorrFirstName().setValue(CHILD_STBORRFIRSTNAME_RESET_VALUE);
        getStDealStatus().setValue(CHILD_STDEALSTATUS_RESET_VALUE);
        getStDealStatusDate().setValue(CHILD_STDEALSTATUSDATE_RESET_VALUE);
        getStSourceFirm().setValue(CHILD_STSOURCEFIRM_RESET_VALUE);
        getStSource().setValue(CHILD_STSOURCE_RESET_VALUE);
        getStLOB().setValue(CHILD_STLOB_RESET_VALUE);
        getStDealType().setValue(CHILD_STDEALTYPE_RESET_VALUE);
        getStDealPurpose().setValue(CHILD_STDEALPURPOSE_RESET_VALUE);
        getStPurchasePrice().setValue(CHILD_STPURCHASEPRICE_RESET_VALUE);

        getStPmtTerm().setValue(CHILD_STPMTTERM_RESET_VALUE);
        getStEstClosingDate().setValue(CHILD_STESTCLOSINGDATE_RESET_VALUE);
        getStSpecialFeature().setValue(CHILD_STSPECIALFEATURE_RESET_VALUE);
        getBtSubmit().setValue(CHILD_BTSUBMIT_RESET_VALUE);
        getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
        getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
        getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
        getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
        getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
        getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
        getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
        getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
        getStTotalLoanAmount().setValue(CHILD_STTOTALLOANAMOUNT_RESET_VALUE);
        getBtCancel().setValue(CHILD_BTCANCEL_RESET_VALUE);
        getStDateMask().setValue(CHILD_STDATEMASK_RESET_VALUE);
        getBtPrevTaskPage().setValue(CHILD_BTPREVTASKPAGE_RESET_VALUE);
        getStPrevTaskPageLabel().setValue(CHILD_STPREVTASKPAGELABEL_RESET_VALUE);
        getBtNextTaskPage().setValue(CHILD_BTNEXTTASKPAGE_RESET_VALUE);
        getStNextTaskPageLabel().setValue(CHILD_STNEXTTASKPAGELABEL_RESET_VALUE);
        getStTaskName().setValue(CHILD_STTASKNAME_RESET_VALUE);
        getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
        getStPartyName().setValue(CHILD_STPARTYNAME_RESET_VALUE);
        getStPartyCompanyName().setValue(CHILD_STPARTYCOMPANYNAME_RESET_VALUE);
        getStCity().setValue(CHILD_STCITY_RESET_VALUE);
        getHdAddrId().setValue(CHILD_HDADDRID_RESET_VALUE);
        getStAddressLine1().setValue(CHILD_STADDRESSLINE1_RESET_VALUE);
        getStAddressLine2().setValue(CHILD_STADDRESSLINE2_RESET_VALUE);
        getStProvinceAbbreviation().setValue(CHILD_STPROVINCEABBREVIATION_RESET_VALUE);
        getStPostalFSALDU().setValue(CHILD_STPOSTALFSALDU_RESET_VALUE);
        getStContactPhoneNumber().setValue(CHILD_STCONTACTPHONENUMBER_RESET_VALUE);
        getStContactPhoneNumExtension().setValue(CHILD_STCONTACTPHONENUMEXTENSION_RESET_VALUE);
        getStContactFaxNumber().setValue(CHILD_STCONTACTFAXNUMBER_RESET_VALUE);
        getStContactEmailAddress().setValue(CHILD_STCONTACTEMAILADDRESS_RESET_VALUE);
        getTbInstructions().setValue(CHILD_TBINSTRUCTIONS_RESET_VALUE);
        getBtAssignSolicitor().setValue(CHILD_BTASSIGNSOLICITOR_RESET_VALUE);
        getBtChangeSolicitor().setValue(CHILD_BTCHANGESOLICITOR_RESET_VALUE);
        getStEstimatedCloseDate().setValue(CHILD_STESTIMATEDCLOSEDATE_RESET_VALUE);
        getStInterestAdjustmentDate().setValue(CHILD_STINTERESTADJUSTMENTDATE_RESET_VALUE);
        getStIADNumDaysDesc().setValue(CHILD_STIADNUMDAYSDESC_RESET_VALUE);
        getStFirstPaymentDate().setValue(CHILD_STFIRSTPAYMENTDATE_RESET_VALUE);
        getStFirstPaymentDateMthlyDesc().setValue(CHILD_STFIRSTPAYMENTDATEMTHLYDESC_RESET_VALUE);
        getStMaturityDate().setValue(CHILD_STMATURITYDATE_RESET_VALUE);
        getStForceFirstMonthDesc().setValue(CHILD_STFORCEFIRSTMONTHDESC_RESET_VALUE);
        getStPerDiemInterest().setValue(CHILD_STPERDIEMINTEREST_RESET_VALUE);
        getStInterestRate().setValue(CHILD_STINTERESTRATE_RESET_VALUE);
        getStInterestCompoundMsg().setValue(CHILD_STINTERESTCOMPOUNDMSG_RESET_VALUE);
        getStLoanAmount().setValue(CHILD_STLOANAMOUNT_RESET_VALUE);
        getStIADInterest().setValue(CHILD_STIADINTEREST_RESET_VALUE);
        getStHoldBackAmount().setValue(CHILD_STHOLDBACKAMOUNT_RESET_VALUE);
        getRptDealFees().resetChildren();
        getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
        getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
        getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
        getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
        getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
        getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
        getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
        getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
        getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
        getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
        getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
        getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
        getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
        getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
        getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
        getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
        getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
        getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
        getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
        getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
        getBtOK().setValue(CHILD_BTOK_RESET_VALUE);
        getStViewOnlyTag().setValue(CHILD_STVIEWONLYTAG_RESET_VALUE);
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> Test by BILLY 15July2002
    getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);
    //=================================================
    //// Separation of the date and the string label
        getStOneMonthAfterAid().setValue(CHILD_STONEMONTHAFTERIAD_RESET_VALUE);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
        getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);

        // SEAN Closing service Jan 17, 2006:  reset the default value.
        // Produce Solicitor Package check box
        getChProduceSolicitorPkg().setValue(CHILD_CHPRODUCESOLICITORPKG_RESET_VALUE);
        // outsource closing service
        // ------ provider combobox.
        getCbOCSProvider().setValue(CHILD_CBOCSPROVIDER_RESET_VALUE);
        getCbOCSProduct().setValue(CHILD_CBOCSPRODUCT_RESET_VALUE);
        getStOCSRequestRefNum().setValue(CHILD_STOCSREQUESTREFNUM_RESET_VALUE);
        getStOCSRequestStatus().setValue(CHILD_STOCSREQUESTSTATUS_RESET_VALUE);
		getStOCSRequestStatusId().setValue(CHILD_STOCSREQUESTSTATUSID_RESET_VALUE);
        getStOCSRequestStatusMsg().setValue(CHILD_STOCSREQUESTSTATUSMSG_RESET_VALUE);
        getStOCSRequestStatusDate().setValue(CHILD_STOCSREQUESTSTATUSDATE_RESET_VALUE);

        getBtOCSUpdate().setValue(CHILD_BTOCSUPDATE_RESET_VALUE);
        getBtOCSRequest().setValue(CHILD_BTOCSREQUEST_RESET_VALUE);
        getBtOCSCancel().setValue(CHILD_BTOCSCANCEL_RESET_VALUE);  //CANCEL

        // Fixed Price Closing Package
        // ------ Provider combobox
        getCbFPCProvider().setValue(CHILD_CBFPCPROVIDER_RESET_VALUE);
        getCbFPCProduct().setValue(CHILD_CBFPCPRODUCT_RESET_VALUE);
        // SEAN Closing service END
        //FPC begin
        getStFPCRequestStatus().setValue(CHILD_STFPCREQUESTSTATUS_RESET_VALUE);
		getStFPCRequestStatusId().setValue(CHILD_STFPCREQUESTSTATUSID_RESET_VALUE);
        getStFPCRequestDate().setValue(CHILD_STFPCREQUESTDATE_RESET_VALUE);
        getChFPCBorrowerInfo().setValue(CHILD_CHFPCBORROWERINFO_RESET_VALUE);
//      getBtFPCSubmitReferral().setValue(CHILD_BTFPCSUBMITREFERRAL_RESET_VALUE);
        getStFPCContactFirstName().setValue(CHILD_STFPCCONTACTFIRSTNAME_RESET_VALUE);
        getStFPCContactLastName().setValue(CHILD_STFPCCONTACTLASTNAME_RESET_VALUE);
        getTbFPCEmail().setValue(CHILD_TBFPCEMAIL_RESET_VALUE);
        getTbFPCWorkPhoneAreaCode().setValue(CHILD_TBFPCWORKPHONEAREACODE_RESET_VALUE);
        getTbFPCWorkPhoneFirstThreeDigits().setValue(CHILD_TBFPCWORKPHONEFIRSTTHREEDIGITS_RESET_VALUE);
        getTbFPCWorkPhoneLastFourDigits().setValue(CHILD_TBFPCWORKPHONELASTFOURDIGITS_RESET_VALUE);
        getTbFPCWorkPhoneNumExtension().setValue(CHILD_TBFPCWORKPHONENUMEXTENSION_RESET_VALUE);
        getTbFPCCellPhoneAreaCode().setValue(CHILD_TBFPCCELLPHONEAREACODE_RESET_VALUE);
        getTbFPCCellPhoneFirstThreeDigits().setValue(CHILD_TBFPCCELLPHONEFIRSTTHREEDIGITS_RESET_VALUE);
        getTbFPCCellPhoneLastFourDigits().setValue(CHILD_TBFPCCELLPHONELASTFOURDIGITS_RESET_VALUE);
        getTbFPCHomePhoneAreaCode().setValue(CHILD_TBFPCHOMEPHONEAREACODE_RESET_VALUE);
        getTbFPCHomePhoneFirstThreeDigits().setValue(CHILD_TBFPCHOMEPHONEFIRSTTHREEDIGITS_RESET_VALUE);
        getTbFPCHomePhoneLastFourDigits().setValue(CHILD_TBFPCHOMEPHONELASTFOURDIGITS_RESET_VALUE);
        getStFPCComments().setValue(CHILD_STFPCCOMMENTS_RESET_VALUE);
        
        getStFpcCommentHidden().setValue(CHILD_STFPCCOMMENTHIDDEN_RESET_VALUE);

        getStOCSHiddenStart().setValue(CHILD_STOCSHIDDENSTART_RESET_VALUE);
        getStOCSHiddenEnd().setValue(CHILD_STOCSHIDDENEND_RESET_VALUE);
        getStFPCHiddenStart().setValue(CHILD_STFPCHIDDENSTART_RESET_VALUE);
        getStFPCHiddenEnd().setValue(CHILD_STFPCHIDDENEND_RESET_VALUE);
        getStFPCRequestMessage().setValue(CHILD_STFPCREQUESTMESSAGE_RESET_VALUE);
        getHdProductId().setValue(CHILD_STHDPRODUCTID_RESET_VALUE);
        getHdFpcProviderId().setValue(CHILD_STHDFPCPROVIDERID_RESET_VALUE);
        getHdFpcProductId().setValue(CHILD_STHDFPCPRODUCTID_RESET_VALUE);
        getHdFpcProviderRefNo().setValue(CHILD_STHDFPCPROVIDERREFNO_RESET_VALUE);
        getHdOcsRequestId().setValue(CHILD_STHDOCSREQUESTID_RESET_VALUE);
        getHdFpcRequestId().setValue(CHILD_STHDFPCREQUESTID_RESET_VALUE);
        getHdOcsCopyId().setValue(CHILD_STHDOCSCOPYID_RESET_VALUE);
        getHdFpcCopyId().setValue(CHILD_STHDFPCCOPYID_RESET_VALUE);
        getHdFpcContactId().setValue(CHILD_STHDFPCCONTACTID_RESET_VALUE);
        getHdFpcBorrowerId().setValue(CHILD_STHDFPCBORROWERID_RESET_VALUE);

         //Jerry FPC END
    }


    /**
     *
     *
     */
    protected void registerChildren()
    {
    	super.registerChildren();
    	registerChild(CHILD_TBDEALID,TextField.class);
        registerChild(CHILD_CBPAGENAMES,ComboBox.class);
        registerChild(CHILD_BTPROCEED,Button.class);
        registerChild(CHILD_HREF1,HREF.class);
        registerChild(CHILD_HREF2,HREF.class);
        registerChild(CHILD_HREF3,HREF.class);
        registerChild(CHILD_HREF4,HREF.class);
        registerChild(CHILD_HREF5,HREF.class);
        registerChild(CHILD_HREF6,HREF.class);
        registerChild(CHILD_HREF7,HREF.class);
        registerChild(CHILD_HREF8,HREF.class);
        registerChild(CHILD_HREF9,HREF.class);
        registerChild(CHILD_HREF10,HREF.class);
        registerChild(CHILD_STPAGELABEL,StaticTextField.class);
        registerChild(CHILD_STCOMPANYNAME,StaticTextField.class);
        registerChild(CHILD_STTODAYDATE,StaticTextField.class);
        registerChild(CHILD_STUSERNAMETITLE,StaticTextField.class);
        registerChild(CHILD_STDEALID,StaticTextField.class);
        registerChild(CHILD_STBORRFIRSTNAME,StaticTextField.class);
        registerChild(CHILD_STDEALSTATUS,StaticTextField.class);
        registerChild(CHILD_STDEALSTATUSDATE,StaticTextField.class);
        registerChild(CHILD_STSOURCEFIRM,StaticTextField.class);
        registerChild(CHILD_STSOURCE,StaticTextField.class);
        registerChild(CHILD_STLOB,StaticTextField.class);
        registerChild(CHILD_STDEALTYPE,StaticTextField.class);
        registerChild(CHILD_STDEALPURPOSE,StaticTextField.class);
        registerChild(CHILD_STPURCHASEPRICE,StaticTextField.class);
        registerChild(CHILD_STPMTTERM,StaticTextField.class);
        registerChild(CHILD_STESTCLOSINGDATE,StaticTextField.class);
        registerChild(CHILD_STSPECIALFEATURE,StaticTextField.class);
        registerChild(CHILD_BTSUBMIT,Button.class);
        registerChild(CHILD_BTWORKQUEUELINK,Button.class);
        registerChild(CHILD_CHANGEPASSWORDHREF,HREF.class);
        registerChild(CHILD_BTTOOLHISTORY,Button.class);
        registerChild(CHILD_BTTOONOTES,Button.class);
        registerChild(CHILD_BTTOOLSEARCH,Button.class);
        registerChild(CHILD_BTTOOLLOG,Button.class);
        registerChild(CHILD_STERRORFLAG,StaticTextField.class);
        registerChild(CHILD_DETECTALERTTASKS,HiddenField.class);
        registerChild(CHILD_STTOTALLOANAMOUNT,StaticTextField.class);
        registerChild(CHILD_BTCANCEL,Button.class);
        registerChild(CHILD_STDATEMASK,StaticTextField.class);
        registerChild(CHILD_BTPREVTASKPAGE,Button.class);
        registerChild(CHILD_STPREVTASKPAGELABEL,StaticTextField.class);
        registerChild(CHILD_BTNEXTTASKPAGE,Button.class);
        registerChild(CHILD_STNEXTTASKPAGELABEL,StaticTextField.class);
        registerChild(CHILD_STTASKNAME,StaticTextField.class);
        registerChild(CHILD_SESSIONUSERID,HiddenField.class);
        registerChild(CHILD_STPARTYNAME,StaticTextField.class);
        registerChild(CHILD_STPARTYCOMPANYNAME,StaticTextField.class);

        registerChild(CHILD_STCITY,StaticTextField.class);
        registerChild(CHILD_HDADDRID,HiddenField.class);
        registerChild(CHILD_STADDRESSLINE1,StaticTextField.class);
        registerChild(CHILD_STADDRESSLINE2,StaticTextField.class);
        registerChild(CHILD_STPROVINCEABBREVIATION,StaticTextField.class);
        registerChild(CHILD_STPOSTALFSALDU,StaticTextField.class);
        registerChild(CHILD_STCONTACTPHONENUMBER,StaticTextField.class);
        registerChild(CHILD_STCONTACTPHONENUMEXTENSION,StaticTextField.class);
        registerChild(CHILD_STCONTACTFAXNUMBER,StaticTextField.class);
        registerChild(CHILD_STCONTACTEMAILADDRESS,StaticTextField.class);
        registerChild(CHILD_TBINSTRUCTIONS,TextField.class);
        registerChild(CHILD_BTASSIGNSOLICITOR,Button.class);
        registerChild(CHILD_BTCHANGESOLICITOR,Button.class);
        registerChild(CHILD_STESTIMATEDCLOSEDATE,StaticTextField.class);
        registerChild(CHILD_STINTERESTADJUSTMENTDATE,StaticTextField.class);
        registerChild(CHILD_STIADNUMDAYSDESC,StaticTextField.class);
        registerChild(CHILD_STFIRSTPAYMENTDATE,StaticTextField.class);
        registerChild(CHILD_STFIRSTPAYMENTDATEMTHLYDESC,StaticTextField.class);
        registerChild(CHILD_STMATURITYDATE,StaticTextField.class);
        registerChild(CHILD_STFORCEFIRSTMONTHDESC,StaticTextField.class);
        registerChild(CHILD_STPERDIEMINTEREST,StaticTextField.class);
        registerChild(CHILD_STINTERESTRATE,StaticTextField.class);
        registerChild(CHILD_STINTERESTCOMPOUNDMSG,StaticTextField.class);
        registerChild(CHILD_STLOANAMOUNT,StaticTextField.class);
        registerChild(CHILD_STIADINTEREST,StaticTextField.class);
        registerChild(CHILD_STHOLDBACKAMOUNT,StaticTextField.class);
        registerChild(CHILD_RPTDEALFEES,pgClosingrptDealFeesTiledView.class);
        registerChild(CHILD_STPMGENERATE,StaticTextField.class);
        registerChild(CHILD_STPMHASTITLE,StaticTextField.class);
        registerChild(CHILD_STPMHASINFO,StaticTextField.class);
        registerChild(CHILD_STPMHASTABLE,StaticTextField.class);
        registerChild(CHILD_STPMHASOK,StaticTextField.class);
        registerChild(CHILD_STPMTITLE,StaticTextField.class);
        registerChild(CHILD_STPMINFOMSG,StaticTextField.class);
        registerChild(CHILD_STPMONOK,StaticTextField.class);
        registerChild(CHILD_STPMMSGS,StaticTextField.class);
        registerChild(CHILD_STPMMSGTYPES,StaticTextField.class);
        registerChild(CHILD_STAMGENERATE,StaticTextField.class);
        registerChild(CHILD_STAMHASTITLE,StaticTextField.class);
        registerChild(CHILD_STAMHASINFO,StaticTextField.class);
        registerChild(CHILD_STAMHASTABLE,StaticTextField.class);
        registerChild(CHILD_STAMTITLE,StaticTextField.class);
        registerChild(CHILD_STAMINFOMSG,StaticTextField.class);
        registerChild(CHILD_STAMMSGS,StaticTextField.class);
        registerChild(CHILD_STAMMSGTYPES,StaticTextField.class);
        registerChild(CHILD_STAMDIALOGMSG,StaticTextField.class);
        registerChild(CHILD_STAMBUTTONSHTML,StaticTextField.class);
        registerChild(CHILD_BTOK,Button.class);
        registerChild(CHILD_STVIEWONLYTAG,StaticTextField.class);
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> Test by BILLY 15July2002
    registerChild(CHILD_BTACTMSG,Button.class);
    //===========================================
    //// Separation of the date and the string label
    registerChild(CHILD_STONEMONTHAFTERIAD,StaticTextField.class);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
        registerChild(CHILD_TOGGLELANGUAGEHREF,HREF.class);

        // SEAN Closing service Jan 17, 2006: registering child.
        // Produce Solicitor Package checkbox.
        registerChild(CHILD_CHPRODUCESOLICITORPKG, CheckBox.class);
        // outsourced closing service
        // ------ provider combobox.
        registerChild(CHILD_CBOCSPROVIDER, ComboBox.class);
        registerChild(CHILD_CBOCSPRODUCT, ComboBox.class);
        registerChild(CHILD_STOCSREQUESTREFNUM, StaticTextField.class);
        registerChild(CHILD_STOCSREQUESTSTATUS, StaticTextField.class);
		registerChild(CHILD_STOCSREQUESTSTATUSID, StaticTextField.class);
        registerChild(CHILD_STOCSREQUESTSTATUSMSG, StaticTextField.class);
        registerChild(CHILD_STOCSREQUESTSTATUSDATE, StaticTextField.class);

        registerChild(CHILD_BTOCSUPDATE, Button.class);
        registerChild(CHILD_BTOCSREQUEST, Button.class);
        registerChild(CHILD_BTOCSCANCEL, Button.class);


        // FPC Fixed Price Closing Package
        // ------ Provider combobox.
        registerChild(CHILD_CBFPCPROVIDER, ComboBox.class);
        registerChild(CHILD_CBFPCPRODUCT, ComboBox.class);
        // SEAN Closing service END
        registerChild(CHILD_STFPCREQUESTSTATUS, StaticTextField.class);
		registerChild(CHILD_STFPCREQUESTSTATUSID, StaticTextField.class);
        registerChild(CHILD_STFPCREQUESTDATE,  StaticTextField.class);
        registerChild(CHILD_CHFPCBORROWERINFO, CheckBox.class);
        registerChild(CHILD_BTFPCSUBMITREFERRAL, Button.class);
        registerChild(CHILD_STFPCCONTACTFIRSTNAME, TextField.class);
        registerChild(CHILD_STFPCCONTACTLASTNAME, TextField.class);
        registerChild(CHILD_TBFPCEMAIL, TextField.class);
        registerChild(CHILD_TBFPCWORKPHONEAREACODE, TextField.class);
        registerChild(CHILD_TBFPCWORKPHONEFIRSTTHREEDIGITS, TextField.class);
        registerChild(CHILD_TBFPCWORKPHONELASTFOURDIGITS, TextField.class);
        registerChild(CHILD_TBFPCWORKPHONENUMEXTENSION, TextField.class);
        registerChild(CHILD_TBFPCCELLPHONEAREACODE, TextField.class);
        registerChild(CHILD_TBFPCCELLPHONEFIRSTTHREEDIGITS, TextField.class);
        registerChild(CHILD_TBFPCCELLPHONELASTFOURDIGITS, TextField.class);
        registerChild(CHILD_TBFPCHOMEPHONEAREACODE, TextField.class);
        registerChild(CHILD_TBFPCHOMEPHONEFIRSTTHREEDIGITS, TextField.class);
        registerChild(CHILD_TBFPCHOMEPHONELASTFOURDIGITS, TextField.class);
        registerChild(CHILD_STFPCCOMMENTS, StaticTextField.class);
        
        registerChild(CHILD_STFPCCOMMENTHIDDEN, StaticTextField.class);
        
        registerChild(CHILD_STOCSHIDDENSTART, StaticTextField.class);
        registerChild(CHILD_STOCSHIDDENEND, StaticTextField.class);
        registerChild(CHILD_STFPCHIDDENSTART, StaticTextField.class);
        registerChild(CHILD_STFPCHIDDENEND, StaticTextField.class);

        registerChild(CHILD_STHDDEALID, StaticTextField.class);
        registerChild(CHILD_STHDDEALCOPYID, StaticTextField.class);
        registerChild(CHILD_STFPCREQUESTMESSAGE, StaticTextField.class);
        registerChild(CHILD_STHDPRODUCTID, StaticTextField.class);
        registerChild(CHILD_STHDFPCPROVIDERID, StaticTextField.class);
        registerChild(CHILD_STHDFPCPRODUCTID, StaticTextField.class);
        registerChild(CHILD_STHDFPCPROVIDERREFNO, StaticTextField.class);
        registerChild(CHILD_STHDOCSREQUESTID, HiddenField.class);
        registerChild(CHILD_STHDFPCREQUESTID, HiddenField.class);
        registerChild(CHILD_STHDOCSCOPYID, HiddenField.class);
        registerChild(CHILD_STHDFPCCOPYID, HiddenField.class);
        registerChild(CHILD_STHDFPCCONTACTID, HiddenField.class);
        registerChild(CHILD_STHDFPCBORROWERID, HiddenField.class);

        //Jerry end FPC
  
}


    ////////////////////////////////////////////////////////////////////////////////
    // Model management methods
    ////////////////////////////////////////////////////////////////////////////////
    /**
     *
     *
     */
    public Model[] getWebActionModels(int executionType)
    {
        List modelList=new ArrayList();
        switch(executionType)
        {
            case MODEL_TYPE_RETRIEVE:
                modelList.add(getdoDealSummarySnapShotModel());;
                modelList.add(getdoClosingActivityInfoModel());;
                modelList.add(getdoClosingDetailsModel());;
                // SEAN Closing Service January 23, 2006: add the model to model list.
                modelList.add(getdoOutsourcedClosingServiceModel());
                // SEAN Closing Service END.
                modelList.add(getdoFixedPriceClosingInfoModel());
                break;

            case MODEL_TYPE_UPDATE:
                ;
                break;

            case MODEL_TYPE_DELETE:
                ;
                break;

            case MODEL_TYPE_INSERT:
                ;
                break;

            case MODEL_TYPE_EXECUTE:
                ;
                break;
        }
        return (Model[])modelList.toArray(new Model[0]);
    }


    ////////////////////////////////////////////////////////////////////////////////
    // View flow control methods
    ////////////////////////////////////////////////////////////////////////////////
    /**
     *
     *
     */
    public void beginDisplay(DisplayEvent event)
        throws ModelControlException
    {
    ClosingActivityHandler handler = (ClosingActivityHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By John 08Nov2002
    //Setup Options for cbPageNames
    //cbPageNamesOptions.populate(getRequestContext());
	int langId = handler.getTheSessionState().getLanguageId();
    cbPageNamesOptions.populate(getRequestContext(), langId);
    //Setup NoneSelected Label for cbPageNames
    CHILD_CBPAGENAMES_NONSELECTED_LABEL =
    BXResources.getGenericMsg("CBPAGENAMES_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    //Check if the cbPageNames is already created
    if(getCbPageNames() != null)
      getCbPageNames().setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);

    //=====================================
    // SEAN Closing service January 2006: invoking the populating method.
    // populate the provider combobox. for both provider combobox.
    cbOCSProviderOptions.populate(getRequestContext());
//    cbOCSProductOptions.populate(getRequestContext());
    // SEAN Closing service END
    //FPC Closing
   // cbFPCProviderOptions.populate(getRequestContext());
 //   cbFPCProductOptions.populate(getRequestContext());
    //FPC Closing end
    

    handler.pageSaveState();
    super.beginDisplay(event);
    }


    /**
     *
     *
     */
    public boolean beforeModelExecutes(Model model, int executionContext)
    {
        // The following code block was migrated from the this_onBeforeDataObjectExecuteEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        handler.setupBeforePageGeneration();
        handler.pageSaveState();

    return super.beforeModelExecutes(model, executionContext);
    }


    /**
     *
     *
     */
    public void afterModelExecutes(Model model, int executionContext)
    {
        // This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
        super.afterModelExecutes(model, executionContext);
    }


    /**
     *
     *
     */
    public void afterAllModelsExecute(int executionContext)
    {
        // This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
        super.afterAllModelsExecute(executionContext);
        // The following code block was migrated from the this_onBeforeRowDisplayEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        handler.populatePageDisplayFields();
        handler.pageSaveState();
    }


    /**
     *
     *
     */
    public void onModelError(Model model, int executionContext, ModelControlException exception)
        throws ModelControlException
    {

        // This is the analog of NetDynamics this_onDataObjectErrorEvent
        super.onModelError(model, executionContext, exception);

    }


    /**
     *
     *
     */
    public TextField getTbDealId()
    {
        return (TextField)getChild(CHILD_TBDEALID);
    }


    /**
     *
     *
     */
    public ComboBox getCbPageNames()
    {
        return (ComboBox)getChild(CHILD_CBPAGENAMES);
    }

    /**
     *
     *
     */
  //--DJ_Ticket661--start--//
  //Modified to sort by PageLabel based on the selected language
  static class CbPageNamesOptionList extends GotoOptionList
 {
    /**
     *
     *
     */
    CbPageNamesOptionList() {

    }


    public void populate(RequestContext rc) {
      Connection c = null;
      try {
        clear();
        SelectQueryModel m = null;

        SelectQueryExecutionContext eContext = new
          SelectQueryExecutionContext(
          (Connection)null,

          DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,

          DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null) {
          m = new doPageNameLabelModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else {
          m = (SelectQueryModel)rc.getModelManager().getModel(doPageNameLabelModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        // Sort the results from DataObject
        TreeMap sorted = new TreeMap();
        while (m.next()) {
          Object dfPageLabel =
            m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
          String label = (dfPageLabel == null ? "" : dfPageLabel.toString());
          Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
          String value = (dfPageId == null ? "" : dfPageId.toString());
          String[] theVal = new String[2];
          theVal[0] = label;
          theVal[1] = value;
          sorted.put(label, theVal);
        }

        // Set the sorted list to the optionlist
        Iterator theList = sorted.values().iterator();
        String[] theVal = new String[2];
        while (theList.hasNext()) {
          theVal = (String[]) (theList.next());
          add(theVal[0], theVal[1]);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      finally {
        try {
          if (c != null)
            c.close();
        }
        catch (SQLException ex) {
              // ignore
        }
      }
    }
 }
 //--DJ_Ticket661--end--//

    /**
     *
     *
     */
    public void handleBtProceedRequest(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the btProceed_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleGoPage();
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public Button getBtProceed()
    {
        return (Button)getChild(CHILD_BTPROCEED);
    }


    /**
     *
     *
     */
    public void handleHref1Request(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the Href1_onWebEvent method
    ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(0);
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public HREF getHref1()
    {
        return (HREF)getChild(CHILD_HREF1);
    }


    /**
     *
     *
     */
    public void handleHref2Request(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the Href2_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(1);
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public HREF getHref2()
    {
        return (HREF)getChild(CHILD_HREF2);
    }


    /**
     *
     *
     */
    public void handleHref3Request(RequestInvocationEvent event)
        throws ServletException, IOException
    {
    // The following code block was migrated from the Href3_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(2);
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public HREF getHref3()
    {
        return (HREF)getChild(CHILD_HREF3);
    }


    /**
     *
     *
     */
    public void handleHref4Request(RequestInvocationEvent event)
        throws ServletException, IOException
    {
    // The following code block was migrated from the Href4_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(3);
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public HREF getHref4()
    {
        return (HREF)getChild(CHILD_HREF4);
    }


    /**
     *
     *
     */
    public void handleHref5Request(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the Href5_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(4);
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public HREF getHref5()
    {
        return (HREF)getChild(CHILD_HREF5);
    }


    /**
     *
     *
     */
    public void handleHref6Request(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the Href6_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(5);
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public HREF getHref6()
    {
        return (HREF)getChild(CHILD_HREF6);
    }


    /**
     *
     *
     */
    public void handleHref7Request(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the Href7_onWebEvent method
    ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(6);
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public HREF getHref7()
    {
        return (HREF)getChild(CHILD_HREF7);
    }


    /**
     *
     *
     */
    public void handleHref8Request(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the Href8_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(7);
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public HREF getHref8()
    {
        return (HREF)getChild(CHILD_HREF8);
    }


    /**
     *
     *
     */
    public void handleHref9Request(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the Href9_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(8);
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public HREF getHref9()
    {
        return (HREF)getChild(CHILD_HREF9);
    }


    /**
     *
     *
     */
    public void handleHref10Request(RequestInvocationEvent event)
        throws ServletException, IOException
    {
    // The following code block was migrated from the Href10_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(9);
    handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public HREF getHref10()
    {
        return (HREF)getChild(CHILD_HREF10);
    }


    /**
     *
     *
     */
    public StaticTextField getStPageLabel()
    {
        return (StaticTextField)getChild(CHILD_STPAGELABEL);
    }


    /**
     *
     *
     */
    public StaticTextField getStCompanyName()
    {
        return (StaticTextField)getChild(CHILD_STCOMPANYNAME);
    }


    /**
     *
     *
     */
    public StaticTextField getStTodayDate()
    {
        return (StaticTextField)getChild(CHILD_STTODAYDATE);
    }


    /**
     *
     *
     */
    public StaticTextField getStUserNameTitle()
    {
        return (StaticTextField)getChild(CHILD_STUSERNAMETITLE);
    }


    /**
     *
     *
     */
    public StaticTextField getStDealId()
    {
        return (StaticTextField)getChild(CHILD_STDEALID);
    }


    /**
     *
     *
     */
    public StaticTextField getStBorrFirstName()
    {
        return (StaticTextField)getChild(CHILD_STBORRFIRSTNAME);
    }


    /**
     *
     *
     */
    public StaticTextField getStDealStatus()
    {
        return (StaticTextField)getChild(CHILD_STDEALSTATUS);
    }


    /**
     *
     *
     */
    public StaticTextField getStDealStatusDate()
    {
        return (StaticTextField)getChild(CHILD_STDEALSTATUSDATE);
    }


    /**
     *
     *
     */
    public StaticTextField getStSourceFirm()
    {
        return (StaticTextField)getChild(CHILD_STSOURCEFIRM);
    }


    /**
     *
     *
     */
    public StaticTextField getStSource()
    {
        return (StaticTextField)getChild(CHILD_STSOURCE);
    }


    /**
     *
     *
     */
    public StaticTextField getStLOB()
    {
        return (StaticTextField)getChild(CHILD_STLOB);
    }


    /**
     *
     *
     */
    public StaticTextField getStDealType()
    {
        return (StaticTextField)getChild(CHILD_STDEALTYPE);
    }


    /**
     *
     *
     */
    public StaticTextField getStDealPurpose()
    {
        return (StaticTextField)getChild(CHILD_STDEALPURPOSE);
    }


    /**
     *
     *
     */
    public StaticTextField getStPurchasePrice()
    {
        return (StaticTextField)getChild(CHILD_STPURCHASEPRICE);
    }


    /**
     *
     *
     */
    public StaticTextField getStPmtTerm()
    {
        return (StaticTextField)getChild(CHILD_STPMTTERM);
    }


    /**
     *
     *
     */
    public StaticTextField getStEstClosingDate()
    {
        return (StaticTextField)getChild(CHILD_STESTCLOSINGDATE);
    }


    /**
     *
     *
     */
    public StaticTextField getStSpecialFeature()
    {
        return (StaticTextField)getChild(CHILD_STSPECIALFEATURE);
    }


    /**
     *
     *
     */
    public void handleBtSubmitRequest(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the btSubmit_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
         handler.handleSubmit(false);
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public Button getBtSubmit()
    {
        return (Button)getChild(CHILD_BTSUBMIT);
    }


    /**
     *
     *
     */
    public String endBtSubmitDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btSubmit_onBeforeHtmlOutputEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        boolean rc = handler.displaySubmitButton();
        handler.pageSaveState();

        if(rc == true)
            return event.getContent();
        else
            return "";
    }


    /**
     *
     *
     */
    public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the btWorkQueueLink_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleDisplayWorkQueue();
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public Button getBtWorkQueueLink()
    {
        return (Button)getChild(CHILD_BTWORKQUEUELINK);
    }


    /**
     *
     *
     */
    public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
        throws ServletException, IOException
    {
    // The following code block was migrated from the changePasswordHref_onWebEvent method
    ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleChangePassword();
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public HREF getChangePasswordHref()
    {
        return (HREF)getChild(CHILD_CHANGEPASSWORDHREF);
    }

  //--Release2.1--start//
  //// Two new methods providing the business logic for the new link to toggle language.
  //// When touched this link should reload the page in opposite language (french versus english)
  //// and set this new session value.
    /**
     *
     *
     */
    public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this, true);

        handler.handleToggleLanguage();

        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public HREF getToggleLanguageHref()
    {
        return (HREF)getChild(CHILD_TOGGLELANGUAGEHREF);
    }
  //--Release2.1--end//

    /**
     *
     *
     */
    public void handleBtToolHistoryRequest(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the btToolHistory_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleDisplayDealHistory();
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public Button getBtToolHistory()
    {
        return (Button)getChild(CHILD_BTTOOLHISTORY);
    }


    /**
     *
     *
     */
    public void handleBtTooNotesRequest(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the btTooNotes_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleDisplayDealNotes();
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public Button getBtTooNotes()
    {
        return (Button)getChild(CHILD_BTTOONOTES);
    }


    /**
     *
     *
     */
    public void handleBtToolSearchRequest(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the btToolSearch_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleDealSearch();
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public Button getBtToolSearch()
    {
        return (Button)getChild(CHILD_BTTOOLSEARCH);
    }


    /**
     *
     *
     */
    public void handleBtToolLogRequest(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the btToolLog_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleSignOff();
        handler.postHandlerProtocol();
    }
    
    //Jerry closing start
    /*
     * FPC Closing handle submitreferral button 
     */
    /*
    public void handleBtFPCSubmitReferralRequest(RequestInvocationEvent event)
        throws ServletException, IOException
    {
            // The following code block was migrated from the btToolLog_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleFPCSubmitReferral();
        handler.postHandlerProtocol();
    }
    */
    /*
     public void handleBtOCSCancelRequest(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the btCancel_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOCSCancel();
        handler.postHandlerProtocol();
    }

    
    public void handleBtOCSUpdateRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the btCancel_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOCSUpdate();
        handler.postHandlerProtocol();
    }
    */
    public void handleBtOCSRequestRequest(RequestInvocationEvent event)
    {
        // The following code block was migrated from the btOK_onBeforeHtmlOutputEvent method
       // ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        //handler.preHandlerProtocol(this);
        //handler.handleOCSRequest();
        //handler.postHandlerProtocol();
    } 
    

    /**
    *control the display of request button in OCS 
    */
/*
   public String endBtOCSRequestDisplay(ChildContentDisplayEvent event)
   {
       // The following code block was migrated from the btOK_onBeforeHtmlOutputEvent method
       ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
       handler.pageGetState(this);       
       
       boolean rc = (handler.theSessionState.getIsDisplayRequestSubmit())&&(handler.displayOCSRequestButton("btOCSRequest"));
       
       //boolean rc = (handler.theSessionState.getIsDisplayRequestSubmit());
 
       handler.pageSaveState();

       if(rc == true)
           return event.getContent();
       else
           return "";
       
   }

   public String endBtOCSUpdateDisplay(ChildContentDisplayEvent event)
   {
       // The following code block was migrated from the btOK_onBeforeHtmlOutputEvent method
       ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
       handler.pageGetState(this);
       
       boolean rc = (handler.theSessionState.getIsDisplayUpdateSubmit())&&(handler.displayOCSUpdateButton("btOCSUpdate"));
       
       //boolean rc = (handler.theSessionState.getIsDisplayUpdateSubmit());

       handler.pageSaveState();

       if(rc == true)
           return event.getContent();
       else
           return "";
   }*/
    //Jerry closing end

    
    /*
     * 
     */
    public Button getBtToolLog()
    {
        return (Button)getChild(CHILD_BTTOOLLOG);
    }


    /**
     *
     *
     */
    public StaticTextField getStErrorFlag()
    {
        return (StaticTextField)getChild(CHILD_STERRORFLAG);
    }


    /**
     *
     *
     */
    public HiddenField getDetectAlertTasks()
    {
        return (HiddenField)getChild(CHILD_DETECTALERTTASKS);
    }


    /**
     *
     *
     */
    public StaticTextField getStTotalLoanAmount()
    {
        return (StaticTextField)getChild(CHILD_STTOTALLOANAMOUNT);
    }


    /**
     *
     *
     */
    public void handleBtCancelRequest(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the btCancel_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleCancelStandard();
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public Button getBtCancel()
    {
        return (Button)getChild(CHILD_BTCANCEL);
    }


    /**
     *
     *
     */
    public String endBtCancelDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btCancel_onBeforeHtmlOutputEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        boolean rc = handler.displayCancelButton();
        handler.pageSaveState();

        if(rc == true)
          return event.getContent();
        else
            return "";
    }


    /**
     *
     *
     */
    public StaticTextField getStDateMask()
    {
        return (StaticTextField)getChild(CHILD_STDATEMASK);
    }


    /**
     *
     *
     */
    public void handleBtPrevTaskPageRequest(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the btPrevTaskPage_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handlePrevTaskPage();
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public Button getBtPrevTaskPage()
    {
        return (Button)getChild(CHILD_BTPREVTASKPAGE);
    }


    /**
     *
     *
     */
    public String endBtPrevTaskPageDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btPrevTaskPage_onBeforeHtmlOutputEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        boolean rc = handler.generatePrevTaskPage();
        handler.pageSaveState();

        if(rc == true)
          return event.getContent();
    else
      return "";
    }


    /**
     *
     *
     */
    public StaticTextField getStPrevTaskPageLabel()
    {
        return (StaticTextField)getChild(CHILD_STPREVTASKPAGELABEL);
    }


    /**
     *
     *
     */
    public String endStPrevTaskPageLabelDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the stPrevTaskPageLabel_onBeforeHtmlOutputEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        boolean rc = handler.generatePrevTaskPage();
        handler.pageSaveState();

        if(rc == true)
          return event.getContent();
    else
      return "";
    }


    /**
     *
     *
     */
    public void handleBtNextTaskPageRequest(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the btNextTaskPage_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleNextTaskPage();
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public Button getBtNextTaskPage()
    {
        return (Button)getChild(CHILD_BTNEXTTASKPAGE);
    }


    /**
     *
     *
     */
    public String endBtNextTaskPageDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btNextTaskPage_onBeforeHtmlOutputEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        boolean rc = handler.generateNextTaskPage();
        handler.pageSaveState();

        if(rc == true)
          return event.getContent();
    else
      return "";
    }


    /**
     *
     *
     */
    public StaticTextField getStNextTaskPageLabel()
    {
        return (StaticTextField)getChild(CHILD_STNEXTTASKPAGELABEL);
    }


    /**
     *
     *
     */
    public String endStNextTaskPageLabelDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the stNextTaskPageLabel_onBeforeHtmlOutputEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        boolean rc = handler.generateNextTaskPage();
        handler.pageSaveState();

        if(rc == true)
          return event.getContent();
    else
      return "";
    }


    /**
     *
     *
     */
    public StaticTextField getStTaskName()
    {
        return (StaticTextField)getChild(CHILD_STTASKNAME);
    }


    /**
     *
     *
     */
    public String endStTaskNameDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the stTaskName_onBeforeHtmlOutputEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        boolean rc = handler.generateTaskName();
        handler.pageSaveState();

        if(rc == true)
          return event.getContent();
    else
      return "";
    }


    /**
     *
     *
     */
    public HiddenField getSessionUserId()
    {
        return (HiddenField)getChild(CHILD_SESSIONUSERID);
    }


    /**
     *
     *
     */
    public StaticTextField getStPartyName()
    {
        return (StaticTextField)getChild(CHILD_STPARTYNAME);
    }


    /**
     *
     *
     */
    public StaticTextField getStPartyCompanyName()
    {
        return (StaticTextField)getChild(CHILD_STPARTYCOMPANYNAME);
    }


    /**
     *
     *
     */
    public StaticTextField getStCity()
    {
        return (StaticTextField)getChild(CHILD_STCITY);
    }


    /**
     *
     *
     */
    public HiddenField getHdAddrId()
    {
        return (HiddenField)getChild(CHILD_HDADDRID);
    }


    /**
     *
     *
     */
    public StaticTextField getStAddressLine1()
    {
        return (StaticTextField)getChild(CHILD_STADDRESSLINE1);
    }


    /**
     *
     *
     */
    public StaticTextField getStAddressLine2()
    {
        return (StaticTextField)getChild(CHILD_STADDRESSLINE2);
    }


    /**
     *
     *
     */
    public StaticTextField getStProvinceAbbreviation()
    {
        return (StaticTextField)getChild(CHILD_STPROVINCEABBREVIATION);
    }


    /**
     *
     *
     */
    public StaticTextField getStPostalFSALDU()
    {
        return (StaticTextField)getChild(CHILD_STPOSTALFSALDU);
    }


    /**
     *
     *
     */
    public StaticTextField getStContactPhoneNumber()
    {
        return (StaticTextField)getChild(CHILD_STCONTACTPHONENUMBER);
    }


    /**
     *
     *
     */
    public StaticTextField getStContactPhoneNumExtension()
    {
        return (StaticTextField)getChild(CHILD_STCONTACTPHONENUMEXTENSION);
    }


    /**
     *
     *
     */
    public String endStContactPhoneNumExtensionDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the stContactPhoneNumExtension_onBeforeHtmlOutputEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        String rc = handler.displayPhoneExtension(event.getContent());
        handler.pageSaveState();

        return rc;
    }


    /**
     *
     *
     */
    public StaticTextField getStContactFaxNumber()
    {
        return (StaticTextField)getChild(CHILD_STCONTACTFAXNUMBER);
    }


    /**
     *
     *
     */
    public StaticTextField getStContactEmailAddress()
    {
        return (StaticTextField)getChild(CHILD_STCONTACTEMAILADDRESS);
    }


    /**
     *
     *
     */
    public TextField getTbInstructions()
    {
        return (TextField)getChild(CHILD_TBINSTRUCTIONS);
    }


    /**
     *
     *
     */
    public void handleBtAssignSolicitorRequest(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the btAssignSolicitor_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleAssignSolicitor("btAssignSolicitor");
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public Button getBtAssignSolicitor()
    {
        return (Button)getChild(CHILD_BTASSIGNSOLICITOR);
    }


    /**
     *
     *
     */
    public String endBtAssignSolicitorDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btAssignSolicitor_onBeforeHtmlOutputEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        boolean rc = handler.generateAssignSolicitor();
        handler.pageSaveState();

        if(rc == true)
          return event.getContent();
        else
          return "";
    }

    /**
     *
     *
     */
    public void handleBtChangeSolicitorRequest(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

    handler.handleAssignSolicitor("btChangeSolicitor");

        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public Button getBtChangeSolicitor()
    {
        return (Button)getChild(CHILD_BTCHANGESOLICITOR);
    }


    /**
     *
     *
     */
    public String endBtChangeSolicitorDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btChangeSolicitor_onBeforeHtmlOutputEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        boolean rc = handler.generateChangeSolicitor();
        handler.pageSaveState();

        if(rc == true)
          return event.getContent();
    else
      return "";
    }


    /**
     *
     *
     */
    public StaticTextField getStEstimatedCloseDate()
    {
        return (StaticTextField)getChild(CHILD_STESTIMATEDCLOSEDATE);
    }


    /**
     *
     *
     */
    public StaticTextField getStInterestAdjustmentDate()
    {
        return (StaticTextField)getChild(CHILD_STINTERESTADJUSTMENTDATE);
    }

    /**
     *
     *  Separation of the static label and date value.
     */
    public StaticTextField getStOneMonthAfterAid()
    {
        return (StaticTextField)getChild(CHILD_STONEMONTHAFTERIAD);
    }

    /**
     *
     *
     */
    public StaticTextField getStIADNumDaysDesc()
    {
        return (StaticTextField)getChild(CHILD_STIADNUMDAYSDESC);
    }


    /**
     *
     *
     */
    public StaticTextField getStFirstPaymentDate()
    {
        return (StaticTextField)getChild(CHILD_STFIRSTPAYMENTDATE);
    }


    /**
     *
     *
     */
    public StaticTextField getStFirstPaymentDateMthlyDesc()
    {
        return (StaticTextField)getChild(CHILD_STFIRSTPAYMENTDATEMTHLYDESC);
    }


    /**
     *
     *
     */
    public StaticTextField getStMaturityDate()
    {
        return (StaticTextField)getChild(CHILD_STMATURITYDATE);
    }


    /**
     *
     *
     */
    public StaticTextField getStForceFirstMonthDesc()
    {
        return (StaticTextField)getChild(CHILD_STFORCEFIRSTMONTHDESC);
    }


    /**
     *
     *
     */
    public StaticTextField getStPerDiemInterest()
    {
        return (StaticTextField)getChild(CHILD_STPERDIEMINTEREST);
    }


    /**
     *
     *
     */
    public StaticTextField getStInterestRate()
    {
        return (StaticTextField)getChild(CHILD_STINTERESTRATE);
    }


    /**
     *
     *
     */
    public StaticTextField getStInterestCompoundMsg()
    {
        return (StaticTextField)getChild(CHILD_STINTERESTCOMPOUNDMSG);
    }


    /**
     *
     *
     */
    public StaticTextField getStLoanAmount()
    {
        return (StaticTextField)getChild(CHILD_STLOANAMOUNT);
    }


    /**
     *
     *
     */
    public StaticTextField getStIADInterest()
    {
        return (StaticTextField)getChild(CHILD_STIADINTEREST);
    }


    /**
     *
     *
     */
    public StaticTextField getStHoldBackAmount()
    {
        return (StaticTextField)getChild(CHILD_STHOLDBACKAMOUNT);
    }


    /**
     *
     *
     */
    public pgClosingrptDealFeesTiledView getRptDealFees()
    {
        return (pgClosingrptDealFeesTiledView)getChild(CHILD_RPTDEALFEES);
    }


    /**
     *
     *
     */
    public StaticTextField getStPmGenerate()
    {
        return (StaticTextField)getChild(CHILD_STPMGENERATE);
    }


    /**
     *
     *
     */
    public StaticTextField getStPmHasTitle()
    {
        return (StaticTextField)getChild(CHILD_STPMHASTITLE);
    }


    /**
     *
     *
     */
    public StaticTextField getStPmHasInfo()
    {
        return (StaticTextField)getChild(CHILD_STPMHASINFO);
    }


    /**
     *
     *
     */
    public StaticTextField getStPmHasTable()
    {
        return (StaticTextField)getChild(CHILD_STPMHASTABLE);
    }


    /**
     *
     *
     */
    public StaticTextField getStPmHasOk()
    {
        return (StaticTextField)getChild(CHILD_STPMHASOK);
    }


    /**
     *
     *
     */
    public StaticTextField getStPmTitle()
    {
        return (StaticTextField)getChild(CHILD_STPMTITLE);
    }


    /**
     *
     *
     */
    public StaticTextField getStPmInfoMsg()
    {
        return (StaticTextField)getChild(CHILD_STPMINFOMSG);
    }


    /**
     *
     *
     */
    public StaticTextField getStPmOnOk()
    {
        return (StaticTextField)getChild(CHILD_STPMONOK);
    }


    /**
     *
     *
     */
    public StaticTextField getStPmMsgs()
    {
        return (StaticTextField)getChild(CHILD_STPMMSGS);
    }


    /**
     *
     *
     */
    public StaticTextField getStPmMsgTypes()
    {
        return (StaticTextField)getChild(CHILD_STPMMSGTYPES);
    }


    /**
     *
     *
     */
    public StaticTextField getStAmGenerate()
    {
        return (StaticTextField)getChild(CHILD_STAMGENERATE);
    }


    /**
     *
     *
     */
    public StaticTextField getStAmHasTitle()
    {
        return (StaticTextField)getChild(CHILD_STAMHASTITLE);
    }


    /**
     *
     *
     */
    public StaticTextField getStAmHasInfo()
    {
        return (StaticTextField)getChild(CHILD_STAMHASINFO);
    }


    /**
     *
     *
     */
    public StaticTextField getStAmHasTable()
    {
        return (StaticTextField)getChild(CHILD_STAMHASTABLE);
    }


    /**
     *
     *
     */
    public StaticTextField getStAmTitle()
    {
        return (StaticTextField)getChild(CHILD_STAMTITLE);
    }


    /**
     *
     *
     */
    public StaticTextField getStAmInfoMsg()
    {
        return (StaticTextField)getChild(CHILD_STAMINFOMSG);
    }


    /**
     *
     *
     */
    public StaticTextField getStAmMsgs()
    {
        return (StaticTextField)getChild(CHILD_STAMMSGS);
    }


    /**
     *
     *
     */
    public StaticTextField getStAmMsgTypes()
    {
        return (StaticTextField)getChild(CHILD_STAMMSGTYPES);
    }


    /**
     *
     *
     */
    public StaticTextField getStAmDialogMsg()
    {
        return (StaticTextField)getChild(CHILD_STAMDIALOGMSG);
    }


    /**
     *
     *
     */
    public StaticTextField getStAmButtonsHtml()
    {
        return (StaticTextField)getChild(CHILD_STAMBUTTONSHTML);
    }


    /**
     *
     *
     */
    public void handleBtOKRequest(RequestInvocationEvent event)
        throws ServletException, IOException
    {
        // The following code block was migrated from the btOK_onWebEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleCancelStandard();
        handler.postHandlerProtocol();
    }


    /**
     *
     *
     */
    public Button getBtOK()
    {
        return (Button)getChild(CHILD_BTOK);
    }


    /**
     *
     *
     */
    public String endBtOKDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btOK_onBeforeHtmlOutputEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        boolean rc = handler.displayOKButton();
        handler.pageSaveState();

        if(rc == true)
          return event.getContent();
    else
      return "";
    }


    /**
     *
     *
     */
    public StaticTextField getStViewOnlyTag()
    {
        return (StaticTextField)getChild(CHILD_STVIEWONLYTAG);
    }


    /**
     *
     *
     */
    public String endStViewOnlyTagDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the stViewOnlyTag_onBeforeHtmlOutputEvent method
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        String rc = handler.displayViewOnlyTag();
        handler.pageSaveState();

        return rc;
    }

    // SEAN Closing service Jan 17, 2006: getter and setter.
    // Produce Solicitor Package checkbox.
    public CheckBox getChProduceSolicitorPkg() {

        return (CheckBox) getChild(CHILD_CHPRODUCESOLICITORPKG);
    }

    // Outsourced closing service
    // ------ provider:
    public ComboBox getCbOCSProvider() {

        return (ComboBox) getChild(CHILD_CBOCSPROVIDER);
    }
    public ComboBox getCbOCSProduct() {

        return (ComboBox) getChild(CHILD_CBOCSPRODUCT);
    }
    public StaticTextField getStOCSRequestRefNum() {

        return (StaticTextField) getChild(CHILD_STOCSREQUESTREFNUM);
    }
    public StaticTextField getStOCSRequestStatus() {

        return (StaticTextField) getChild(CHILD_STOCSREQUESTSTATUS);
    }

	public StaticTextField getStOCSRequestStatusId() {

        return (StaticTextField) getChild(CHILD_STOCSREQUESTSTATUSID);
    }

    public StaticTextField getStOCSRequestStatusMsg() {

        return (StaticTextField) getChild(CHILD_STOCSREQUESTSTATUSMSG);
    }
    public StaticTextField getStOCSRequestStatusDate() {

        return (StaticTextField) getChild(CHILD_STOCSREQUESTSTATUSDATE);
    }
    
    public Button getBtOCSUpdate() {
        return (Button) getChild(CHILD_BTOCSUPDATE);
    }

    public Button getBtOCSRequest() {
        return (Button) getChild(CHILD_BTOCSREQUEST);
    }

    public Button getBtOCSCancel() {
        return (Button) getChild(CHILD_BTOCSCANCEL);
    }
    
    
    // Fixed Price Closing Package
    // ------ Provider combo box.
    public ComboBox getCbFPCProvider() {

        return (ComboBox) getChild(CHILD_CBFPCPROVIDER);
    }

    public ComboBox getCbFPCProduct() {

        return (ComboBox) getChild(CHILD_CBFPCPRODUCT);
    }

    // SEAN Closing service END

    
    //FPC Closing service
    public StaticTextField getStFPCRequestStatus() {
        return (StaticTextField) getChild(CHILD_STFPCREQUESTSTATUS);
    }


    public StaticTextField getStFPCRequestStatusId() {
        return (StaticTextField) getChild(CHILD_STFPCREQUESTSTATUSID);
    }

	public StaticTextField getStFPCRequestDate() {
        return (StaticTextField) getChild(CHILD_STFPCREQUESTDATE);
    }
    
    public CheckBox getChFPCBorrowerInfo() {
        return (CheckBox) getChild(CHILD_CHFPCBORROWERINFO);
    }
    
    public TextField getStFPCContactFirstName() {
        return(TextField) getChild(CHILD_STFPCCONTACTFIRSTNAME);
    }
    
    public TextField getStFPCContactLastName() {
        return(TextField) getChild(CHILD_STFPCCONTACTLASTNAME);
    }   
    
    public TextField getTbFPCEmail() {
        return(TextField) getChild(CHILD_TBFPCEMAIL);
    }


    public TextField getTbFPCWorkPhoneAreaCode() {
        return (TextField) getChild(CHILD_TBFPCWORKPHONEAREACODE);
    }
    
    public TextField getTbFPCWorkPhoneFirstThreeDigits() {
        return (TextField) getChild(CHILD_TBFPCWORKPHONEFIRSTTHREEDIGITS);
    }
    
    public TextField getTbFPCWorkPhoneLastFourDigits() {
        return (TextField) getChild(CHILD_TBFPCWORKPHONELASTFOURDIGITS);
    }

    public TextField getTbFPCWorkPhoneNumExtension() {
        return (TextField) getChild(CHILD_TBFPCWORKPHONENUMEXTENSION);
    }
    
    public TextField getTbFPCCellPhoneAreaCode() {
        return (TextField) getChild(CHILD_TBFPCCELLPHONEAREACODE);
    }
    
    public TextField getTbFPCCellPhoneFirstThreeDigits() {
        return (TextField) getChild(CHILD_TBFPCCELLPHONEFIRSTTHREEDIGITS);
    }

    public TextField getTbFPCCellPhoneLastFourDigits() {
        return (TextField) getChild(CHILD_TBFPCCELLPHONELASTFOURDIGITS);
    }

    public TextField getTbFPCHomePhoneAreaCode() {
        return (TextField) getChild(CHILD_TBFPCHOMEPHONEAREACODE);
    }
    
    public TextField getTbFPCHomePhoneFirstThreeDigits() {
        return (TextField) getChild(CHILD_TBFPCHOMEPHONEFIRSTTHREEDIGITS);
    }
    
    public TextField getTbFPCHomePhoneLastFourDigits() {
        return (TextField) getChild(CHILD_TBFPCHOMEPHONELASTFOURDIGITS);
    }

    public StaticTextField getStFPCComments() {
        return (StaticTextField) getChild(CHILD_STFPCCOMMENTS);
    }
    
    public Button getbtFPCSubmitReferral() {
        return (Button) getChild(CHILD_BTFPCSUBMITREFERRAL);
    }
    
    
    public StaticTextField getStFpcCommentHidden() {
        return (StaticTextField) getChild(CHILD_STFPCCOMMENTHIDDEN);
    }


    public StaticTextField getStOCSHiddenStart() {
        return (StaticTextField) getChild(CHILD_STOCSHIDDENSTART);
    }

    public StaticTextField getStOCSHiddenEnd() {
        return (StaticTextField) getChild(CHILD_STOCSHIDDENEND);
    }
    
    public StaticTextField getStFPCHiddenStart() {
        return (StaticTextField) getChild(CHILD_STFPCHIDDENSTART);
    }

    public StaticTextField getStFPCHiddenEnd() {
        return (StaticTextField) getChild(CHILD_STFPCHIDDENEND);
    }
    
    public StaticTextField getStHDDealId()
    {
        return (StaticTextField) getChild(CHILD_STHDDEALID);
    }
    
    public StaticTextField getStHDDealCopyId()
    {
        return (StaticTextField) getChild(CHILD_STHDDEALCOPYID);
    }
    
    public StaticTextField getStFPCRequestMessage()
    {
        return (StaticTextField) getChild(CHILD_STFPCREQUESTMESSAGE);
    }
    
    
    public StaticTextField getHdProductId()
    {
        return (StaticTextField) getChild(CHILD_STHDPRODUCTID);
    }
     
    
    public StaticTextField getHdFpcProviderId()
    {
        return (StaticTextField) getChild(CHILD_STHDFPCPROVIDERID);
    }

    public StaticTextField getHdFpcProductId()
    {
        return (StaticTextField) getChild(CHILD_STHDFPCPRODUCTID);
    }

    public StaticTextField getHdFpcProviderRefNo()
    {
        return (StaticTextField) getChild(CHILD_STHDFPCPROVIDERREFNO);
    }
    
    
    public HiddenField getHdOcsRequestId()
    {
        return (HiddenField) getChild(CHILD_STHDOCSREQUESTID);
    }
        
    public HiddenField getHdFpcRequestId()
    {
        return (HiddenField) getChild(CHILD_STHDFPCREQUESTID);
    }
        
    public HiddenField getHdOcsCopyId()
    {
        return (HiddenField) getChild(CHILD_STHDOCSCOPYID);
    }

    public HiddenField getHdFpcCopyId()
    {
        return (HiddenField) getChild(CHILD_STHDFPCCOPYID);
    }
    
    public HiddenField getHdFpcContactId()
    {
        return (HiddenField) getChild(CHILD_STHDFPCCONTACTID);
    }

	public HiddenField getHdFpcBorrowerId()
    {
        return (HiddenField) getChild(CHILD_STHDFPCBORROWERID);
    }
    //Jerry closing end
    /**
     *
     *
     */
    public doDealSummarySnapShotModel getdoDealSummarySnapShotModel()
    {
        if (doDealSummarySnapShot == null)
            doDealSummarySnapShot = (doDealSummarySnapShotModel) getModel(doDealSummarySnapShotModel.class);
        return doDealSummarySnapShot;
    }


    /**
     *
     *
     */
    public void setdoDealSummarySnapShotModel(doDealSummarySnapShotModel model)
    {
            doDealSummarySnapShot = model;
    }


    /**
     *
     *
     */
    public doClosingActivityInfoModel getdoClosingActivityInfoModel()
    {
        if (doClosingActivityInfo == null)
            doClosingActivityInfo = (doClosingActivityInfoModel) getModel(doClosingActivityInfoModel.class);
        return doClosingActivityInfo;
    }


    /**
     *
     *
     */
    public void setdoClosingActivityInfoModel(doClosingActivityInfoModel model)
    {
            doClosingActivityInfo = model;
    }

    // SEAN Closing service Jan 20, 2006: getter and setter for models.
    public doOutsourcedClosingServiceModel
        getdoOutsourcedClosingServiceModel(){

        if (_doOutsourcedClosingServiceModel == null)
            _doOutsourcedClosingServiceModel =
                (doOutsourcedClosingServiceModel)
                getModel(doOutsourcedClosingServiceModel.class);
        return _doOutsourcedClosingServiceModel;
    }

    public void setdoOutsourcedClosingServiceModel(doOutsourcedClosingServiceModel model) {

        _doOutsourcedClosingServiceModel = model;
    }
    // SEAN Closing service END

    
    //FPC Closing model
    public doFixedPriceClosingInfoModel
        getdoFixedPriceClosingInfoModel(){

        if (_doFixedPriceClosingInfoModel == null)
            _doFixedPriceClosingInfoModel =
                (doFixedPriceClosingInfoModel)
                getModel(doFixedPriceClosingInfoModel.class);
        return _doFixedPriceClosingInfoModel;
    }

    public void setdoFixedPriceClosingInfoModel(doFixedPriceClosingInfoModel model) {

        _doFixedPriceClosingInfoModel = model;
    }
    //FPC Closing end
    
    
    /**
     *
     *
     */
    public doClosingDetailsModel getdoClosingDetailsModel()
    {
        if (doClosingDetails == null)
            doClosingDetails = (doClosingDetailsModel) getModel(doClosingDetailsModel.class);
        return doClosingDetails;
    }


    /**
     *
     *
     */
    public void setdoClosingDetailsModel(doClosingDetailsModel model)
    {
            doClosingDetails = model;
    }


  //--> Hidden ActMessageButton (FINDTHISLATER)
  //--> Test by BILLY 15July2002
  public Button getBtActMsg()
    {
        return (Button)getChild(CHILD_BTACTMSG);
    }
  //===========================================

  //--> Addition methods to propulate Href display String
  //--> Test by BILLY 07Aug2002
  public String endHref1Display(ChildContentDisplayEvent event)
    {
    ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
    handler.pageGetState(this);
        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 0);
        handler.pageSaveState();

    return rc;
  }
  public String endHref2Display(ChildContentDisplayEvent event)
    {
    ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
    handler.pageGetState(this);
        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 1);
        handler.pageSaveState();

    return rc;
  }
  public String endHref3Display(ChildContentDisplayEvent event)
    {
    ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
    handler.pageGetState(this);
        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 2);
        handler.pageSaveState();

    return rc;
  }
  public String endHref4Display(ChildContentDisplayEvent event)
    {
    ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
    handler.pageGetState(this);
        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 3);
        handler.pageSaveState();

    return rc;
  }
  public String endHref5Display(ChildContentDisplayEvent event)
    {
    ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
    handler.pageGetState(this);
        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 4);
        handler.pageSaveState();

    return rc;
  }
  public String endHref6Display(ChildContentDisplayEvent event)
    {
    ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
    handler.pageGetState(this);
        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 5);
        handler.pageSaveState();

    return rc;
  }
  public String endHref7Display(ChildContentDisplayEvent event)
    {
    ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
    handler.pageGetState(this);
        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 6);
        handler.pageSaveState();

    return rc;
  }
  public String endHref8Display(ChildContentDisplayEvent event)
    {
    ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
    handler.pageGetState(this);
        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 7);
        handler.pageSaveState();

    return rc;
  }
  public String endHref9Display(ChildContentDisplayEvent event)
    {
    ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
    handler.pageGetState(this);
        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 8);
        handler.pageSaveState();

    return rc;
  }
  public String endHref10Display(ChildContentDisplayEvent event)
    {
    ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
    handler.pageGetState(this);
        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 9);
        handler.pageSaveState();

    return rc;
  }
  //=====================================================
  //// This method overrides the getDefaultURL() framework JATO method and it is located
  //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
  //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
  //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
  //// The full method is still in PHC base class. It should care the
  //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
  public String getDisplayURL()
  {
       ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
       handler.pageGetState(this);

       String url=getDefaultDisplayURL();

       int languageId = handler.theSessionState.getLanguageId();

       //// Call the language specific URL (business delegation done in the BXResource).
       if(url != null && !url.trim().equals("") && !url.trim().equals("/")) {
          url = BXResources.getBXUrl(url, languageId);
       }
       else {
          url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
       }

      return url;
  }


    ////////////////////////////////////////////////////////////////////////////
    // Obsolete Netdynamics Events - Require Manual Migration
    ////////////////////////////////////////////////////////////////////////////



    ////////////////////////////////////////////////////////////////////////////
    // Custom Methods - Require Manual Migration
    ////////////////////////////////////////////////////////////////////////////

    public void handleActMessageOK(String[] args)
    {
        ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleActMessageOK(args);
        handler.postHandlerProtocol();
    }




    ////////////////////////////////////////////////////////////////////////////
    // Class variables
    ////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////
    // Class variables
    ////////////////////////////////////////////////////////////////////////////////

    public static final String PAGE_NAME="pgClosing";
  //// It is a variable now (see explanation in the getDisplayURL() method above.
    ////public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgClosing.jsp";
    public static Map FIELD_DESCRIPTORS;
    public static final String CHILD_TBDEALID="tbDealId";
    public static final String CHILD_TBDEALID_RESET_VALUE="";
    public static final String CHILD_CBPAGENAMES="cbPageNames";
    public static final String CHILD_CBPAGENAMES_RESET_VALUE="";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 08Nov2002
  //private static CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  private CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  //Added the Variable for NonSelected Label
  private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";
  //==================================================================

    public static final String CHILD_BTPROCEED="btProceed";
    public static final String CHILD_BTPROCEED_RESET_VALUE="Proceed";
    public static final String CHILD_HREF1="Href1";
    public static final String CHILD_HREF1_RESET_VALUE="";
    public static final String CHILD_HREF2="Href2";
    public static final String CHILD_HREF2_RESET_VALUE="";
    public static final String CHILD_HREF3="Href3";
    public static final String CHILD_HREF3_RESET_VALUE="";
    public static final String CHILD_HREF4="Href4";
    public static final String CHILD_HREF4_RESET_VALUE="";
    public static final String CHILD_HREF5="Href5";
    public static final String CHILD_HREF5_RESET_VALUE="";
    public static final String CHILD_HREF6="Href6";
    public static final String CHILD_HREF6_RESET_VALUE="";
    public static final String CHILD_HREF7="Href7";
    public static final String CHILD_HREF7_RESET_VALUE="";
    public static final String CHILD_HREF8="Href8";
    public static final String CHILD_HREF8_RESET_VALUE="";
    public static final String CHILD_HREF9="Href9";
    public static final String CHILD_HREF9_RESET_VALUE="";
    public static final String CHILD_HREF10="Href10";
    public static final String CHILD_HREF10_RESET_VALUE="";
    public static final String CHILD_STPAGELABEL="stPageLabel";
    public static final String CHILD_STPAGELABEL_RESET_VALUE="";
    public static final String CHILD_STCOMPANYNAME="stCompanyName";
    public static final String CHILD_STCOMPANYNAME_RESET_VALUE="";
    public static final String CHILD_STTODAYDATE="stTodayDate";
    public static final String CHILD_STTODAYDATE_RESET_VALUE="";
    public static final String CHILD_STUSERNAMETITLE="stUserNameTitle";
    public static final String CHILD_STUSERNAMETITLE_RESET_VALUE="";
    public static final String CHILD_STDEALID="stDealId";
    public static final String CHILD_STDEALID_RESET_VALUE="";
    public static final String CHILD_STBORRFIRSTNAME="stBorrFirstName";
    public static final String CHILD_STBORRFIRSTNAME_RESET_VALUE="";
    public static final String CHILD_STDEALSTATUS="stDealStatus";
    public static final String CHILD_STDEALSTATUS_RESET_VALUE="";
    public static final String CHILD_STDEALSTATUSDATE="stDealStatusDate";
    public static final String CHILD_STDEALSTATUSDATE_RESET_VALUE="";
    public static final String CHILD_STSOURCEFIRM="stSourceFirm";
    public static final String CHILD_STSOURCEFIRM_RESET_VALUE="";
    public static final String CHILD_STSOURCE="stSource";
    public static final String CHILD_STSOURCE_RESET_VALUE="";
    public static final String CHILD_STLOB="stLOB";
    public static final String CHILD_STLOB_RESET_VALUE="";
    public static final String CHILD_STDEALTYPE="stDealType";
    public static final String CHILD_STDEALTYPE_RESET_VALUE="";
    public static final String CHILD_STDEALPURPOSE="stDealPurpose";
    public static final String CHILD_STDEALPURPOSE_RESET_VALUE="";
    public static final String CHILD_STPURCHASEPRICE="stPurchasePrice";
    public static final String CHILD_STPURCHASEPRICE_RESET_VALUE="";
    public static final String CHILD_STPMTTERM="stPmtTerm";
    public static final String CHILD_STPMTTERM_RESET_VALUE="";
    public static final String CHILD_STESTCLOSINGDATE="stEstClosingDate";
    public static final String CHILD_STESTCLOSINGDATE_RESET_VALUE="";
    public static final String CHILD_STSPECIALFEATURE="stSpecialFeature";
    public static final String CHILD_STSPECIALFEATURE_RESET_VALUE="";
    public static final String CHILD_BTSUBMIT="btSubmit";
    public static final String CHILD_BTSUBMIT_RESET_VALUE=" ";
    public static final String CHILD_BTWORKQUEUELINK="btWorkQueueLink";
    public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE=" ";
    public static final String CHILD_CHANGEPASSWORDHREF="changePasswordHref";
    public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE="";
    public static final String CHILD_BTTOOLHISTORY="btToolHistory";
    public static final String CHILD_BTTOOLHISTORY_RESET_VALUE=" ";
    public static final String CHILD_BTTOONOTES="btTooNotes";
    public static final String CHILD_BTTOONOTES_RESET_VALUE=" ";
    public static final String CHILD_BTTOOLSEARCH="btToolSearch";
    public static final String CHILD_BTTOOLSEARCH_RESET_VALUE=" ";
    public static final String CHILD_BTTOOLLOG="btToolLog";
    public static final String CHILD_BTTOOLLOG_RESET_VALUE=" ";
    public static final String CHILD_STERRORFLAG="stErrorFlag";
    public static final String CHILD_STERRORFLAG_RESET_VALUE="N";
    public static final String CHILD_DETECTALERTTASKS="detectAlertTasks";
    public static final String CHILD_DETECTALERTTASKS_RESET_VALUE="";
    public static final String CHILD_STTOTALLOANAMOUNT="stTotalLoanAmount";
    public static final String CHILD_STTOTALLOANAMOUNT_RESET_VALUE="";
    public static final String CHILD_BTCANCEL="btCancel";
    public static final String CHILD_BTCANCEL_RESET_VALUE=" ";
    public static final String CHILD_STDATEMASK="stDateMask";
    public static final String CHILD_STDATEMASK_RESET_VALUE="";
    public static final String CHILD_BTPREVTASKPAGE="btPrevTaskPage";
    public static final String CHILD_BTPREVTASKPAGE_RESET_VALUE=" ";
    public static final String CHILD_STPREVTASKPAGELABEL="stPrevTaskPageLabel";
    public static final String CHILD_STPREVTASKPAGELABEL_RESET_VALUE="";
    public static final String CHILD_BTNEXTTASKPAGE="btNextTaskPage";
    public static final String CHILD_BTNEXTTASKPAGE_RESET_VALUE=" ";
    public static final String CHILD_STNEXTTASKPAGELABEL="stNextTaskPageLabel";
    public static final String CHILD_STNEXTTASKPAGELABEL_RESET_VALUE="";
    public static final String CHILD_STTASKNAME="stTaskName";
    public static final String CHILD_STTASKNAME_RESET_VALUE="";
    public static final String CHILD_SESSIONUSERID="sessionUserId";
    public static final String CHILD_SESSIONUSERID_RESET_VALUE="";
    public static final String CHILD_STPARTYNAME="stPartyName";
    public static final String CHILD_STPARTYNAME_RESET_VALUE="";
    public static final String CHILD_STPARTYCOMPANYNAME="stPartyCompanyName";
    public static final String CHILD_STPARTYCOMPANYNAME_RESET_VALUE="";
    public static final String CHILD_STCITY="stCity";
    public static final String CHILD_STCITY_RESET_VALUE="";
    public static final String CHILD_HDADDRID="hdAddrId";
    public static final String CHILD_HDADDRID_RESET_VALUE="";
    public static final String CHILD_STADDRESSLINE1="stAddressLine1";
    public static final String CHILD_STADDRESSLINE1_RESET_VALUE="";
    public static final String CHILD_STADDRESSLINE2="stAddressLine2";
    public static final String CHILD_STADDRESSLINE2_RESET_VALUE="";
    public static final String CHILD_STPROVINCEABBREVIATION="stProvinceAbbreviation";
    public static final String CHILD_STPROVINCEABBREVIATION_RESET_VALUE="";
    public static final String CHILD_STPOSTALFSALDU="stPostalFSALDU";
    public static final String CHILD_STPOSTALFSALDU_RESET_VALUE="";
    public static final String CHILD_STCONTACTPHONENUMBER="stContactPhoneNumber";
    public static final String CHILD_STCONTACTPHONENUMBER_RESET_VALUE="";
    public static final String CHILD_STCONTACTPHONENUMEXTENSION="stContactPhoneNumExtension";
    public static final String CHILD_STCONTACTPHONENUMEXTENSION_RESET_VALUE="";
    public static final String CHILD_STCONTACTFAXNUMBER="stContactFaxNumber";
    public static final String CHILD_STCONTACTFAXNUMBER_RESET_VALUE="";
    public static final String CHILD_STCONTACTEMAILADDRESS="stContactEmailAddress";
    public static final String CHILD_STCONTACTEMAILADDRESS_RESET_VALUE="";
    public static final String CHILD_TBINSTRUCTIONS="tbInstructions";
    public static final String CHILD_TBINSTRUCTIONS_RESET_VALUE="";
    public static final String CHILD_BTASSIGNSOLICITOR="btAssignSolicitor";
    public static final String CHILD_BTASSIGNSOLICITOR_RESET_VALUE="Assign Solicitor";
    public static final String CHILD_BTCHANGESOLICITOR="btChangeSolicitor";
    public static final String CHILD_BTCHANGESOLICITOR_RESET_VALUE="Change Solicitor";
    public static final String CHILD_STESTIMATEDCLOSEDATE="stEstimatedCloseDate";
    public static final String CHILD_STESTIMATEDCLOSEDATE_RESET_VALUE="";
    public static final String CHILD_STINTERESTADJUSTMENTDATE="stInterestAdjustmentDate";
    public static final String CHILD_STINTERESTADJUSTMENTDATE_RESET_VALUE="";
    public static final String CHILD_STIADNUMDAYSDESC="stIADNumDaysDesc";
    public static final String CHILD_STIADNUMDAYSDESC_RESET_VALUE="";
    public static final String CHILD_STFIRSTPAYMENTDATE="stFirstPaymentDate";
    public static final String CHILD_STFIRSTPAYMENTDATE_RESET_VALUE="";
    public static final String CHILD_STFIRSTPAYMENTDATEMTHLYDESC="stFirstPaymentDateMthlyDesc";
    public static final String CHILD_STFIRSTPAYMENTDATEMTHLYDESC_RESET_VALUE="";
    public static final String CHILD_STMATURITYDATE="stMaturityDate";
    public static final String CHILD_STMATURITYDATE_RESET_VALUE="";
    public static final String CHILD_STFORCEFIRSTMONTHDESC="stForceFirstMonthDesc";
    public static final String CHILD_STFORCEFIRSTMONTHDESC_RESET_VALUE="";
    public static final String CHILD_STPERDIEMINTEREST="stPerDiemInterest";
    public static final String CHILD_STPERDIEMINTEREST_RESET_VALUE="";
    public static final String CHILD_STINTERESTRATE="stInterestRate";
    public static final String CHILD_STINTERESTRATE_RESET_VALUE="";
    public static final String CHILD_STINTERESTCOMPOUNDMSG="stInterestCompoundMsg";
    public static final String CHILD_STINTERESTCOMPOUNDMSG_RESET_VALUE="";
    public static final String CHILD_STLOANAMOUNT="stLoanAmount";
    public static final String CHILD_STLOANAMOUNT_RESET_VALUE="";
    public static final String CHILD_STIADINTEREST="stIADInterest";
    public static final String CHILD_STIADINTEREST_RESET_VALUE="";
    public static final String CHILD_STHOLDBACKAMOUNT="stHoldBackAmount";
    public static final String CHILD_STHOLDBACKAMOUNT_RESET_VALUE="";
    public static final String CHILD_RPTDEALFEES="rptDealFees";
    public static final String CHILD_STPMGENERATE="stPmGenerate";
    public static final String CHILD_STPMGENERATE_RESET_VALUE="";
    public static final String CHILD_STPMHASTITLE="stPmHasTitle";
    public static final String CHILD_STPMHASTITLE_RESET_VALUE="";
    public static final String CHILD_STPMHASINFO="stPmHasInfo";
    public static final String CHILD_STPMHASINFO_RESET_VALUE="";
    public static final String CHILD_STPMHASTABLE="stPmHasTable";
    public static final String CHILD_STPMHASTABLE_RESET_VALUE="";
    public static final String CHILD_STPMHASOK="stPmHasOk";
    public static final String CHILD_STPMHASOK_RESET_VALUE="";
    public static final String CHILD_STPMTITLE="stPmTitle";
    public static final String CHILD_STPMTITLE_RESET_VALUE="";
    public static final String CHILD_STPMINFOMSG="stPmInfoMsg";
    public static final String CHILD_STPMINFOMSG_RESET_VALUE="";
    public static final String CHILD_STPMONOK="stPmOnOk";
    public static final String CHILD_STPMONOK_RESET_VALUE="";
    public static final String CHILD_STPMMSGS="stPmMsgs";
    public static final String CHILD_STPMMSGS_RESET_VALUE="";
    public static final String CHILD_STPMMSGTYPES="stPmMsgTypes";
    public static final String CHILD_STPMMSGTYPES_RESET_VALUE="";
    public static final String CHILD_STAMGENERATE="stAmGenerate";
    public static final String CHILD_STAMGENERATE_RESET_VALUE="";
    public static final String CHILD_STAMHASTITLE="stAmHasTitle";
    public static final String CHILD_STAMHASTITLE_RESET_VALUE="";
    public static final String CHILD_STAMHASINFO="stAmHasInfo";
    public static final String CHILD_STAMHASINFO_RESET_VALUE="";
    public static final String CHILD_STAMHASTABLE="stAmHasTable";
    public static final String CHILD_STAMHASTABLE_RESET_VALUE="";
    public static final String CHILD_STAMTITLE="stAmTitle";
    public static final String CHILD_STAMTITLE_RESET_VALUE="";
    public static final String CHILD_STAMINFOMSG="stAmInfoMsg";
    public static final String CHILD_STAMINFOMSG_RESET_VALUE="";
    public static final String CHILD_STAMMSGS="stAmMsgs";
    public static final String CHILD_STAMMSGS_RESET_VALUE="";
    public static final String CHILD_STAMMSGTYPES="stAmMsgTypes";
    public static final String CHILD_STAMMSGTYPES_RESET_VALUE="";
    public static final String CHILD_STAMDIALOGMSG="stAmDialogMsg";
    public static final String CHILD_STAMDIALOGMSG_RESET_VALUE="";
    public static final String CHILD_STAMBUTTONSHTML="stAmButtonsHtml";
    public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE="";
    public static final String CHILD_BTOK="btOK";
    public static final String CHILD_BTOK_RESET_VALUE="OK";
    public static final String CHILD_STVIEWONLYTAG="stViewOnlyTag";
    public static final String CHILD_STVIEWONLYTAG_RESET_VALUE="";
  //--> Hidden ActMessageButton (FINDTHISLATER)
  //--> Test by BILLY 15July2002
  public static final String CHILD_BTACTMSG="btActMsg";
    public static final String CHILD_BTACTMSG_RESET_VALUE="ActMsg";
  //// Separation of the date and the string label
    public static final String CHILD_STONEMONTHAFTERIAD="stOneMonthAfterIAD";
    public static final String CHILD_STONEMONTHAFTERIAD_RESET_VALUE="";
  //--Release2.1--//
  //// New link to toggle the language. When touched this link should reload the
  //// page in opposite language (french versus english) and set this new language
  //// session id throughout all modulus.
    public static final String CHILD_TOGGLELANGUAGEHREF="toggleLanguageHref";
    public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE="";

    // SEAN Closing service January 17, 2005: child name definition.
    // Produce Solicitor Package value is 'Y' OR 'N'
    public static final String CHILD_CHPRODUCESOLICITORPKG = "chProduceSolicitorPkg";
    public static final String CHILD_CHPRODUCESOLICITORPKG_RESET_VALUE = "N";
    // Options list for both provider combobox.
    private CbOCSProviderOptions cbOCSProviderOptions = new CbOCSProviderOptions();
    private CbOCSProductOptions cbOCSProductOptions = new CbOCSProductOptions();
    //FPC begin
    private CbFPCProviderOptions cbFPCProviderOptions = new CbFPCProviderOptions();
    private CbFPCProductOptions cbFPCProductOptions = new CbFPCProductOptions();
    //FPC end
    // Outsourced closing service section:
    // ------ provider combobox.
    public static final String CHILD_CBOCSPROVIDER = "cbOCSProvider";
    public static final String CHILD_CBOCSPROVIDER_RESET_VALUE = "";
    public static final String CHILD_CBOCSPRODUCT = "cbOCSProduct";
    public static final String CHILD_CBOCSPRODUCT_RESET_VALUE = "";
    public static final String CHILD_STOCSREQUESTREFNUM =
        "stOCSRequestRefNum";
    public static final String CHILD_STOCSREQUESTREFNUM_RESET_VALUE = "";
    public static final String CHILD_STOCSREQUESTSTATUS =
        "stOCSRequestStatus";
    public static final String CHILD_STOCSREQUESTSTATUS_RESET_VALUE = "";

    public static final String CHILD_STOCSREQUESTSTATUSID =
        "stOCSRequestStatusId";
    public static final String CHILD_STOCSREQUESTSTATUSID_RESET_VALUE = "";

    public static final String CHILD_STOCSREQUESTSTATUSMSG =
        "stOCSRequestStatusMsg";
    public static final String CHILD_STOCSREQUESTSTATUSMSG_RESET_VALUE = "";
    public static final String CHILD_STOCSREQUESTSTATUSDATE =
        "stOCSRequestStatusDate";
    public static final String CHILD_STOCSREQUESTSTATUSDATE_RESET_VALUE = "";

    public static final String CHILD_BTOCSUPDATE = "btOCSUpdate";
    public static final String CHILD_BTOCSUPDATE_RESET_VALUE = "";

    public static final String CHILD_BTOCSREQUEST = "btOCSRequest";
    public static final String CHILD_BTOCSREQUEST_RESET_VALUE = "";
    
    public static final String CHILD_BTOCSCANCEL = "btOCSCancel";
    public static final String CHILD_BTOCSCANCEL_RESET_VALUE = "";

    // fixed price closing package section:
    // ------ product combobox.
    public static final String CHILD_CBFPCPROVIDER = "cbFPCProvider";
    public static final String CHILD_CBFPCPROVIDER_RESET_VALUE = "";

    public static final String CHILD_CBFPCPRODUCT = "cbFPCProduct";
    public static final String CHILD_CBFPCPRODUCT_RESET_VALUE = "";
    // SEAN Closing service END
     
    public static final String CHILD_STFPCREQUESTSTATUS = "stFPCRequestStatus";
    public static final String CHILD_STFPCREQUESTSTATUS_RESET_VALUE = "";
    
    public static final String CHILD_STFPCREQUESTSTATUSID = "stFPCRequestStatusId";
    public static final String CHILD_STFPCREQUESTSTATUSID_RESET_VALUE = "";

    public static final String CHILD_STFPCREQUESTDATE = "stFPCRequestDate";
    public static final String CHILD_STFPCREQUESTDATE_RESET_VALUE = "";
    
    public static final String CHILD_CHFPCBORROWERINFO = "chFPCBorrowerInfo";
    public static final String CHILD_CHFPCBORROWERINFO_RESET_VALUE ="";
    
    public static final String CHILD_BTFPCSUBMITREFERRAL = "btFPCSubmitReferral";
    public static final String CHILD_BTFPCSUBMITREFERRAL_RESET_VALUE = "";
    
    public static final String CHILD_STFPCCONTACTFIRSTNAME = "stFPCContactFirstName";
    public static final String CHILD_STFPCCONTACTFIRSTNAME_RESET_VALUE = "";
    
    public static final String CHILD_STFPCCONTACTLASTNAME = "stFPCContactLastName";
    public static final String CHILD_STFPCCONTACTLASTNAME_RESET_VALUE = "";

    public static final String CHILD_TBFPCEMAIL = "tbFPCEmail";
    public static final String CHILD_TBFPCEMAIL_RESET_VALUE ="";
    
    public static final String CHILD_TBFPCWORKPHONEAREACODE = "tbFPCWorkPhoneAreaCode";
    public static final String CHILD_TBFPCWORKPHONEAREACODE_RESET_VALUE ="";
        
    public static final String CHILD_TBFPCWORKPHONEFIRSTTHREEDIGITS = "tbFPCWorkPhoneFirstThreeDigits";
    public static final String CHILD_TBFPCWORKPHONEFIRSTTHREEDIGITS_RESET_VALUE ="";

    public static final String CHILD_TBFPCWORKPHONELASTFOURDIGITS = "tbFPCWorkPhoneLastFourDigits";
    public static final String CHILD_TBFPCWORKPHONELASTFOURDIGITS_RESET_VALUE ="";

    public static final String CHILD_TBFPCWORKPHONENUMEXTENSION = "tbFPCWorkPhoneNumExtension";
    public static final String CHILD_TBFPCWORKPHONENUMEXTENSION_RESET_VALUE ="";
    
    public static final String CHILD_TBFPCCELLPHONEAREACODE = "tbFPCCellPhoneAreaCode";
    public static final String CHILD_TBFPCCELLPHONEAREACODE_RESET_VALUE ="";
        
    public static final String CHILD_TBFPCCELLPHONEFIRSTTHREEDIGITS = "tbFPCCellPhoneFirstThreeDigits";
    public static final String CHILD_TBFPCCELLPHONEFIRSTTHREEDIGITS_RESET_VALUE ="";

    public static final String CHILD_TBFPCCELLPHONELASTFOURDIGITS = "tbFPCCellPhoneLastFourDigits";
    public static final String CHILD_TBFPCCELLPHONELASTFOURDIGITS_RESET_VALUE ="";
    
    public static final String CHILD_TBFPCHOMEPHONEAREACODE = "tbFPCHomePhoneAreaCode";
    public static final String CHILD_TBFPCHOMEPHONEAREACODE_RESET_VALUE = "";
        
    public static final String CHILD_TBFPCHOMEPHONEFIRSTTHREEDIGITS = "tbFPCHomePhoneFirstThreeDigits";
    public static final String CHILD_TBFPCHOMEPHONEFIRSTTHREEDIGITS_RESET_VALUE = "";

    public static final String CHILD_TBFPCHOMEPHONELASTFOURDIGITS = "tbFPCHomePhoneLastFourDigits";
    public static final String CHILD_TBFPCHOMEPHONELASTFOURDIGITS_RESET_VALUE = "";
    
    public static final String CHILD_STFPCCOMMENTS = "stFPCComments";
    public static final String CHILD_STFPCCOMMENTS_RESET_VALUE = "";
    
    public static final String CHILD_STOCSHIDDENSTART = "stOCSHiddenStart";
    public static final String CHILD_STOCSHIDDENSTART_RESET_VALUE = "";
    public static final String CHILD_STOCSHIDDENEND = "stOCSHiddenEnd";
    public static final String CHILD_STOCSHIDDENEND_RESET_VALUE = "";
    
    public static final String CHILD_STFPCCOMMENTHIDDEN = "stFpcCommentHidden";
    public static final String CHILD_STFPCCOMMENTHIDDEN_RESET_VALUE = "hidden";

    public static final String CHILD_STFPCHIDDENSTART = "stFPCHiddenStart";
    public static final String CHILD_STFPCHIDDENSTART_RESET_VALUE = "";
    public static final String CHILD_STFPCHIDDENEND = "stFPCHiddenEnd";
    public static final String CHILD_STFPCHIDDENEND_RESET_VALUE = "";
    
    public static final String CHILD_STHDDEALID = "stHDDealId";
    public static final String CHILD_STHDDEALID_RESET_VALUE = "";
    
    public static final String CHILD_STHDDEALCOPYID = "stHDDealCopyId";
    public static final String CHILD_STHDDEALCOPYID_RESET_VALUE = "";
    
    public static final String CHILD_STFPCREQUESTMESSAGE = "stFPCRequestMessage";
    public static final String CHILD_STFPCREQUESTMESSAGE_RESET_VALUE = "";
    
    public static final String CHILD_STHDPRODUCTID = "sthdProductId";
    public static final String CHILD_STHDPRODUCTID_RESET_VALUE = "";
    

    public static final String CHILD_STHDFPCPROVIDERID = "sthdFpcProviderId";
    public static final String CHILD_STHDFPCPROVIDERID_RESET_VALUE = "";

    public static final String CHILD_STHDFPCPRODUCTID = "sthdFpcProductId";
    public static final String CHILD_STHDFPCPRODUCTID_RESET_VALUE = "";

    public static final String CHILD_STHDFPCPROVIDERREFNO = "sthdFpcProviderRefNo";
    public static final String CHILD_STHDFPCPROVIDERREFNO_RESET_VALUE = "";
    
    public static final String CHILD_STHDOCSREQUESTID = "hdOcsRequestId";
    public static final String CHILD_STHDOCSREQUESTID_RESET_VALUE = "";

    public static final String CHILD_STHDFPCREQUESTID = "hdFpcRequestId";
    public static final String CHILD_STHDFPCREQUESTID_RESET_VALUE = "";

    public static final String CHILD_STHDOCSCOPYID = "hdOcsCopyId";
    public static final String CHILD_STHDOCSCOPYID_RESET_VALUE = "";

    public static final String CHILD_STHDFPCCOPYID = "hdFpcCopyId";
    public static final String CHILD_STHDFPCCOPYID_RESET_VALUE = "";

    public static final String CHILD_STHDFPCCONTACTID = "hdFpcContactId";
    public static final String CHILD_STHDFPCCONTACTID_RESET_VALUE = "";

    public static final String CHILD_STHDFPCBORROWERID = "hdFpcBorrowerId";
    public static final String CHILD_STHDFPCBORROWERID_RESET_VALUE = "";
    //--end of FPC closing
    
    ////////////////////////////////////////////////////////////////////////////
    // Instance variables
    ////////////////////////////////////////////////////////////////////////////

    private doDealSummarySnapShotModel doDealSummarySnapShot=null;
    private doClosingActivityInfoModel doClosingActivityInfo=null;
    private doClosingDetailsModel doClosingDetails=null;
    // SEAN Closing Service Jan 20, 2006: Define the models.
    private doOutsourcedClosingServiceModel _doOutsourcedClosingServiceModel = null;
    // SEAN Closing Service END
    //FPC Closing Service
    private doFixedPriceClosingInfoModel _doFixedPriceClosingInfoModel = null;
    //FPC Closing End
    protected String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgClosing.jsp";

    ////////////////////////////////////////////////////////////////////////////
    // Custom Members - Require Manual Migration
    ////////////////////////////////////////////////////////////////////////////
    private ClosingActivityHandler handler=new ClosingActivityHandler();

    // SEAN Closing Service January 19, 2005:  populating the combo box options.
    /**
     * Option list for the closing service provider - outsourced closing service.
     */
    class CbOCSProviderOptions
        extends pgUWorksheetViewBean.BaseComboBoxOptionList {
        /**
         * constructor.
         */
        CbOCSProviderOptions() {
        }
        /**
         * populate the options from the bxresources bundle.
         */
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
              rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
              (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                                                                    defaultInstanceStateName,
                                                                    true);

            int languageId = theSessionState.getLanguageId();
            super.populateOptionList(rc, languageId, "SERVICEPROVIDER", cbOCSProviderOptions);            
        }
    }

    class CbOCSProductOptions
        extends pgUWorksheetViewBean.BaseComboBoxOptionList {
        /**
         * constructor.
         */
        CbOCSProductOptions() {
        }
        /**
         * populate the options from the bxresources bundle.
         */
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
              rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
              (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                                                                    defaultInstanceStateName,
                                                                    true);
            int languageId = theSessionState.getLanguageId();
            super.populateOptionList(rc, languageId, "SERVICEPRODUCT", cbOCSProductOptions);
        }
    }
    // SEAN Closing Service END
    
    //FPC Closing Service
    class CbFPCProviderOptions
        extends pgUWorksheetViewBean.BaseComboBoxOptionList {
    
        /**
         * constructor.
         */
        CbFPCProviderOptions() {
        }
    
        /**
         * populate the options from the bxresources bundle.
         */
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
              rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
              (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                                                                    defaultInstanceStateName,
                                                                    true);
            int languageId = theSessionState.getLanguageId();
            super.populateOptionList(rc, languageId, "SERVICEPROVIDER", cbFPCProviderOptions);
        }
    }

    
    class CbFPCProductOptions
        extends pgUWorksheetViewBean.BaseComboBoxOptionList {
    
        /**
         * constructor.
         */
        CbFPCProductOptions() {
        }
    
        /**
         * populate the options from the bxresources bundle.
         */
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
              rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
              (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                                                                    defaultInstanceStateName,
                                                                    true);
    
            int languageId = theSessionState.getLanguageId();
            super.populateOptionList(rc, languageId, "SERVICEPRODUCT", cbFPCProductOptions);
        }
    }
}

