package mosApp.MosSystem;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.util.DBA;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.filogix.express.core.ExpressRuntimeException;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.util.TypeConverter;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.CheckBox;
import com.iplanet.jato.view.html.RadioButtonGroup;

import MosSystem.Sc;

/**
 * Description: This handler can handle modification of data and display data
 * 
 * @author: MCM Impl Team.
 * @version 1.0 2008-6-11
 * @version 10-Jul-2008 Modified handleComponentDetail() method
 * 
 * @version 1.1 17-July-2008 XS_2.8 Added tiles for each components
 * @version 1.2 21-July-2008 XS_2.8 Modified to handle multiple parents
 * @version 1.3 July 23, 2008 MCM Team: 
 *              fixed to show empty, i.e. not display 0 if field is empty
 * @version 1.4 July 25, 2008 MCM Team: 
 *              fixed to artf751260 
 * @version 1.5 July 29, 2008 MCM Team: 
 *              modified for XS 2.60, add check box JS handling in setMTGCompDisplayFields,
 *              setLOCCompDisplayFields 
 * @version 1.6 artf763316 Aug 21, 2008 : replacing XS_2.60.
 * @version 1.7 artf763359, artf763351, artf763355 Aug 25, 2008
 */
public class ComponentInfoHandler extends DealHandlerCommon implements
        Cloneable, Sc {

    private static Log _log =
        LogFactory.getLog(ComponentInfoHandler.class);

    /**
     * This Constructor.
     */
    public ComponentInfoHandler() {
        super();
        theSessionState = super.theSessionState;
        savedPages = super.savedPages;
    }

    /**
     * This Constructor.
     * @param name
     */
    public ComponentInfoHandler(String name) {
        this(null, name);
        theSessionState = super.theSessionState;
        savedPages = super.savedPages;
    }

    /**
     * This Constructor.
     * @param parent
     * @param name
     */
    protected ComponentInfoHandler(View parent, String name) {
        super(parent, name);
        theSessionState = super.theSessionState;
        savedPages = super.savedPages;
    }

    /**
     * This Constructor resturn the instance of this class.
     * 
     */
    public ComponentInfoHandler cloneSS() {
        return (ComponentInfoHandler) super.cloneSafeShallow();

    }

    /**
     * Description: This method is used to populate the fields in the response
     * header with the appropriate information
     * 
     * @version 1.3 MCM Team: July 23, 2008 
     *              fixed to show empty, i.e. not display 0 if field is empty
     * @version 1.7 artf763359, artf763351, artf763355 Aug 25, 2008
     */
    public void populatePageDisplayFields() {

        ViewBean currNDPage = getCurrNDPage();

        QueryModelBase theCompModel = (QueryModelBase) (RequestManager
                .getRequestContext().getModelManager()
                .getModel(doComponentInfoModel.class));

        if (theCompModel.getValue("dfTotalAmount") == null 
                || theCompModel.getValue("dfTotalLoanAmount") == null) {
            
            /****** artf763359, artf763351, artf763355 starts *******/
            PageEntry pg = theSessionState.getCurrentPage();
            int dealId = pg.getPageDealId();
            int copyId = pg.getPageDealCID();
            
            Deal deal = null;
            try {
                deal = new Deal(srk, null, dealId, copyId);
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (FinderException e) {
                e.printStackTrace();
            }
            
            double totalAmount = deal.getTotalLoanAmount();           
            currNDPage.setDisplayFieldValue("stUnallocatedAmount", Double
                    .toString(totalAmount));
            currNDPage.setDisplayFieldValue("stTotalAmount", Double
                    .toString(0.0));
            currNDPage.setDisplayFieldValue("stCashbackAmount", Double
                    .toString(0.0));
            /****** artf763359, artf763351, artf763355 ends *******/
        } else {
            double totalAmount = TypeConverter.asDouble(theCompModel
                    .getValue("dfTotalAmount"));
    
            double totalLoanAmount = TypeConverter.asDouble(theCompModel
                    .getValue("dfTotalLoanAmount"));
    
            double unallocatedAmount = totalLoanAmount - totalAmount;
            
            currNDPage.setDisplayFieldValue("stUnallocatedAmount", Double
                    .toString(unallocatedAmount));
        }

    }

    /**
     * Description: The field values are populated between ViewBean and Model.
     * This method is executed before the JSP page generation.
     * 
     * @version 1.1 July 18, 2008 added execution for each component model
     */
    public void setupBeforePageGeneration() {
        PageEntry pg = theSessionState.getCurrentPage();
        int dealId = pg.getPageDealId();
        int copyId = pg.getPageDealCID();

        QueryModelBase theComponentDo = (QueryModelBase) RequestManager
                .getRequestContext().getModelManager().getModel(
                        doComponentInfoModel.class);
        theComponentDo.clearUserWhereCriteria();
        theComponentDo.addUserWhereCriterion("dfDealId", "=", new Integer(
                dealId));
        theComponentDo.addUserWhereCriterion("dfCopyId", "=", new Integer(
                copyId));
        
        /***************MCM Impl team changes starts - XS_2.8 *******************/
        executeCriteriaMTGModel(pg, dealId, copyId );
        executeCriteriaLOCModel(pg, dealId, copyId );
        executeCriteriaLoanModel(pg, dealId, copyId );
        executeCriteriaCreditCardModel(pg, dealId, copyId );
        executeCriteriaOverdraftModel(pg, dealId, copyId );
        /***************MCM Impl team changes ends - XS_2.8 *******************/
    }

    /**
     * <p>executeCriteriaMTGModel</p>
     * <p>Description: execute Component Mortgage Model</p>
     * 
     * @param pg
     * @param dealId
     * @param copyId
     * 
     * @version 1.1 July 17, 2008 MCM Team: initial version for XS_2.8 
     */
    protected void executeCriteriaMTGModel(PageEntry pg, int dealId, int copyId) {

        doComponentMortgageModel theDO = (
                doComponentMortgageModel)
                (RequestManager.getRequestContext().getModelManager().getModel(
                        doComponentMortgageModel.class));

        theDO.clearUserWhereCriteria();
        theDO.addUserWhereCriterion(theDO.FIELD_DFDEALID, "=",
                String.valueOf(dealId));
        theDO.addUserWhereCriterion(theDO.FIELD_DFCOPYID, "=",
                String.valueOf(copyId));
        executeModel(theDO, "CDH@@executeCriteriaMTGModel", false);
    }

    /**
     * <p>setMTGCompDisplayFields</p>
     * <p>Description: setup component Mortgage Tile view </p>
     * 
     * @param pg
     * @param dealId
     * @param copyId
     * 
     * @version 1.1 July 17, 2008 MCM Team: initial version for XS_2.8 
     * @version 1.5 July 19, 2008 MCM Team: added JS for check box rule for XS_2.60
     * @version 1.6 Aug 21, 2008 deleted most of the codes.
     */
    protected void setMTGCompDisplayFields(int rowNum)
    {
        doComponentMortgageModel dobject = (doComponentMortgageModel) (RequestManager
                .getRequestContext().getModelManager()
                .getModel(doComponentMortgageModel.class));
        try{ 
            dobject.setLocation(rowNum);  
        } catch (Exception e){
            _log.error(StringUtil.stack2string(e));
            throw new ExpressRuntimeException(e.getMessage(), e);
        }

        //String tileViewPrefix = pgComponentInfoPageletViewBean.CHILD_TILEDMTGCOMP + "/"; 
        //String tileViewPrefix0 = pgComponentInfoPageletViewBean.CHILD_TILEDMTGCOMP; 
        
    }

    /**
     * <p>executeCriteriaLOCModel</p>
     * <p>Description: execute Component LOC Model</p>
     * 
     * @param pg
     * @param dealId
     * @param copyId
     * 
     * @version 1.1 July 17, 2008 MCM Team: initial version for XS_2.8 
     */
    protected void executeCriteriaLOCModel(PageEntry pg, int dealId, int copyId) {

        doComponentLocModel theDO = (
                doComponentLocModel)
                (RequestManager.getRequestContext().getModelManager().getModel(
                        doComponentLocModel.class));

        theDO.clearUserWhereCriteria();
        theDO.addUserWhereCriterion(theDO.FIELD_DFDEALID, "=",
                String.valueOf(dealId));
        theDO.addUserWhereCriterion(theDO.FIELD_DFCOPYID, "=",
                String.valueOf(copyId));
        executeModel(theDO, "CDH@@executeCriteriaLOCModel", false);
    }

    /**
     * <p>setLOCCompDisplayFields</p>
     * <p>Description: setup component LOC Tile view </p>
     * 
     * @param pg
     * @param dealId
     * @param copyId
     * 
     * @version 1.1 July 17, 2008 MCM Team: initial version for XS_2.8 
     * @version 1.5 July 19, 2008 MCM Team: added JS for check box rule for XS_2.60
     * @version 1.6 Aug 21, 2008 deleted most of the codes.
     */
    protected void setLOCCompDisplayFields(int rowNum)
    {
        doComponentLocModel dobject = (doComponentLocModel) (RequestManager
                .getRequestContext().getModelManager()
                .getModel(doComponentLocModel.class));
        try{ 
            dobject.setLocation(rowNum);  
        } catch (Exception e){
            _log.error(StringUtil.stack2string(e));
            throw new ExpressRuntimeException(e.getMessage(), e);
        }
        
    }

    /**
     * <p>executeCriteriaLoanModel</p>
     * <p>Description: execute Component Loan Model</p>
     * 
     * @param pg
     * @param dealId
     * @param copyId
     * 
     * @version 1.1 July 17, 2008 MCM Team: initial version for XS_2.8 
     */
    protected void executeCriteriaLoanModel(PageEntry pg, int dealId, int copyId) {

        doComponentLoanModel theDO = (
                doComponentLoanModel)
                (RequestManager.getRequestContext().getModelManager().getModel(
                        doComponentLoanModel.class));

        theDO.clearUserWhereCriteria();
        theDO.addUserWhereCriterion(theDO.FIELD_DFDEALID, "=",
                String.valueOf(dealId));
        theDO.addUserWhereCriterion(theDO.FIELD_DFCOPYID, "=",
                String.valueOf(copyId));
        executeModel(theDO, "CDH@@executeCriteriaLoanModel", false);
    }

    /**
     * <p>executeCriteriaCreditCardModel</p>
     * <p>Description: execute Component Credit card Model</p>
     * 
     * @param pg
     * @param dealId
     * @param copyId
     * 
     * @version 1.1 July 17, 2008 MCM Team: initial version for XS_2.8 
     */
    protected void executeCriteriaCreditCardModel(PageEntry pg, int dealId, int copyId) {

        doComponentCreditCardModel theDO = (
                doComponentCreditCardModel)
                (RequestManager.getRequestContext().getModelManager().getModel(
                        doComponentCreditCardModel.class));

        theDO.clearUserWhereCriteria();
        theDO.addUserWhereCriterion(theDO.FIELD_DFDEALID, "=",
                String.valueOf(dealId));
        theDO.addUserWhereCriterion(theDO.FIELD_DFCOPYID, "=",
                String.valueOf(copyId));
        executeModel(theDO, "CDH@@executeCriteriaCreditCardModel", false);
    }

    /**
     * <p>executeCriteriaOverdraftModel</p>
     * <p>Description: execute Component overdraft Model</p>
     * 
     * @param pg
     * @param dealId
     * @param copyId
     * 
     * @version 1.1 July 17, 2008 MCM Team: initial version for XS_2.8 
     */
    protected void executeCriteriaOverdraftModel(PageEntry pg, int dealId, int copyId) {

        doComponentOverDraftModel theDO = (
                doComponentOverDraftModel)
                (RequestManager.getRequestContext().getModelManager().getModel(
                        doComponentOverDraftModel.class));

        theDO.clearUserWhereCriteria();
        theDO.addUserWhereCriterion(theDO.FIELD_DFDEALID, "=",
                String.valueOf(dealId));
        theDO.addUserWhereCriterion(theDO.FIELD_DFCOPYID, "=",
                String.valueOf(copyId));
        executeModel(theDO, "CDH@@executeCriteriaOverdraftModel", false);
    }

    /**
     * Description: Handle Component Detail button
     * @version 1.0 Initial version
     * @version 1.1 10-Jul-2008 Added trigger for Calcualtion
     * @version 1.4 July 25, 2008 MCM Team: 
     *              fixed to artf751260 
     */
    public void handleComponentDetail() {
        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
        //V1.1 starts
        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());
       //V1.1 ends
        
        int pageId = PGNM_COMPONENT_DETAILS;
        PageEntry componentPg = setupSubPagePageEntry(pg, pageId);
        getSavedPages().setNextPage(componentPg);
        
        try {
            // MCM Team fixed for artf751260 STARTS
            Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID(), dcm);            // Force to trogger calc by TotalLoanAmount
            deal.forceChange("netLoanAmount");
            dcm.inputEntity(deal);
            dcm.calc();
            
            //v1.1 Added a trigger for calculations starts
//            doCalculation(dcm);
            //v1.1 ends
            // MCM Team fixed for artf751260 ENDS
            navigateToNextPage();
        } catch (Exception e) {
            _log.error("Exception navigate to Component Details", e);
            e.printStackTrace();
        }
    }

    /**
     * Description: Override from DealHandlerCommon
     * @version 1.0 Initial version for XS 2.8
     */
    public void SaveEntity(ViewBean webPage)
        throws Exception {
        
    }
    

}
