package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;
import com.basis100.picklist.BXResources;

import com.basis100.log.*;

/**
 *
 *
 *
 */
public class pgApplicantEntryRepeatedEmploymentTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgApplicantEntryRepeatedEmploymentTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(100);
		setPrimaryModelClass( doApplicantEmploymentIncomeSelectModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getHdEmploymentId().setValue(CHILD_HDEMPLOYMENTID_RESET_VALUE);
		getHdEmploymentCopyId().setValue(CHILD_HDEMPLOYMENTCOPYID_RESET_VALUE);
		getHdContactId().setValue(CHILD_HDCONTACTID_RESET_VALUE);
		getHdContactCopyId().setValue(CHILD_HDCONTACTCOPYID_RESET_VALUE);
		getHdIncomeId().setValue(CHILD_HDINCOMEID_RESET_VALUE);
		getHdIncomeCopyId().setValue(CHILD_HDINCOMECOPYID_RESET_VALUE);
		getHdAddrId().setValue(CHILD_HDADDRID_RESET_VALUE);
		getHdAddrCopyId().setValue(CHILD_HDADDRCOPYID_RESET_VALUE);
		getTxEmployerName().setValue(CHILD_TXEMPLOYERNAME_RESET_VALUE);
		getCbEmploymentStatus().setValue(CHILD_CBEMPLOYMENTSTATUS_RESET_VALUE);
		getCbEmploymentType().setValue(CHILD_CBEMPLOYMENTTYPE_RESET_VALUE);
		getTxEmployerPhoneAreaCode().setValue(CHILD_TXEMPLOYERPHONEAREACODE_RESET_VALUE);
		getTxEmployerPhoneExchange().setValue(CHILD_TXEMPLOYERPHONEEXCHANGE_RESET_VALUE);
		getTxEmployerPhoneRoute().setValue(CHILD_TXEMPLOYERPHONEROUTE_RESET_VALUE);
		getTxEmployerPhoneExt().setValue(CHILD_TXEMPLOYERPHONEEXT_RESET_VALUE);
		getTxEmployerFaxAreaCode().setValue(CHILD_TXEMPLOYERFAXAREACODE_RESET_VALUE);
		getTxEmployerFaxExchange().setValue(CHILD_TXEMPLOYERFAXEXCHANGE_RESET_VALUE);
		getTxEmployerFaxRoute().setValue(CHILD_TXEMPLOYERFAXROUTE_RESET_VALUE);
		getTxEmployerEmailAddress().setValue(CHILD_TXEMPLOYEREMAILADDRESS_RESET_VALUE);
		getTxEmployerMailingAddressLine1().setValue(CHILD_TXEMPLOYERMAILINGADDRESSLINE1_RESET_VALUE);
		getTxEmployerCity().setValue(CHILD_TXEMPLOYERCITY_RESET_VALUE);
		getCbEmployerProvince().setValue(CHILD_CBEMPLOYERPROVINCE_RESET_VALUE);
		getTxEmployerPostalCodeFSA().setValue(CHILD_TXEMPLOYERPOSTALCODEFSA_RESET_VALUE);
		getTxEmployerPostalCodeLDU().setValue(CHILD_TXEMPLOYERPOSTALCODELDU_RESET_VALUE);
		getTxEmployerMailingAddressLine2().setValue(CHILD_TXEMPLOYERMAILINGADDRESSLINE2_RESET_VALUE);
		getCbJobTitle().setValue(CHILD_CBJOBTITLE_RESET_VALUE);
		getHdJobTitle().setValue(CHILD_HDJOBTITLE_RESET_VALUE);
		getTxJobTitle().setValue(CHILD_TXJOBTITLE_RESET_VALUE);
		getCbOccupation().setValue(CHILD_CBOCCUPATION_RESET_VALUE);
		getCbIndustrySector().setValue(CHILD_CBINDUSTRYSECTOR_RESET_VALUE);
		getTxTimeAtJobYears().setValue(CHILD_TXTIMEATJOBYEARS_RESET_VALUE);
		getTxTimeAtJobMonths().setValue(CHILD_TXTIMEATJOBMONTHS_RESET_VALUE);
		getCbIncomeType().setValue(CHILD_CBINCOMETYPE_RESET_VALUE);
		getTxIncomeDesc().setValue(CHILD_TXINCOMEDESC_RESET_VALUE);
		getCbIncomePeriod().setValue(CHILD_CBINCOMEPERIOD_RESET_VALUE);
		getTxIncomeAmount().setValue(CHILD_TXINCOMEAMOUNT_RESET_VALUE);
		getCbEmployIncludeInGDS().setValue(CHILD_CBEMPLOYINCLUDEINGDS_RESET_VALUE);
		getTxEmployPercentageIncludeInGDS().setValue(CHILD_TXEMPLOYPERCENTAGEINCLUDEINGDS_RESET_VALUE);
		getHdEmployPercentageIncludeInGDS().setValue(CHILD_HDEMPLOYPERCENTAGEINCLUDEINGDS_RESET_VALUE);
		getCbEmployIncludeInTDS().setValue(CHILD_CBEMPLOYINCLUDEINTDS_RESET_VALUE);
		getTxEmployPercentageIncludeInTDS().setValue(CHILD_TXEMPLOYPERCENTAGEINCLUDEINTDS_RESET_VALUE);
		getHdEmployPercentageIncludeInTDS().setValue(CHILD_HDEMPLOYPERCENTAGEINCLUDEINTDS_RESET_VALUE);
		getBtDeleteEmployment().setValue(CHILD_BTDELETEEMPLOYMENT_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_HDEMPLOYMENTID,HiddenField.class);
		registerChild(CHILD_HDEMPLOYMENTCOPYID,HiddenField.class);
		registerChild(CHILD_HDCONTACTID,HiddenField.class);
		registerChild(CHILD_HDCONTACTCOPYID,HiddenField.class);
		registerChild(CHILD_HDINCOMEID,HiddenField.class);
		registerChild(CHILD_HDINCOMECOPYID,HiddenField.class);
		registerChild(CHILD_HDADDRID,HiddenField.class);
		registerChild(CHILD_HDADDRCOPYID,HiddenField.class);
		registerChild(CHILD_TXEMPLOYERNAME,TextField.class);
		registerChild(CHILD_CBEMPLOYMENTSTATUS,ComboBox.class);
		registerChild(CHILD_CBEMPLOYMENTTYPE,ComboBox.class);
		registerChild(CHILD_TXEMPLOYERPHONEAREACODE,TextField.class);
		registerChild(CHILD_TXEMPLOYERPHONEEXCHANGE,TextField.class);
		registerChild(CHILD_TXEMPLOYERPHONEROUTE,TextField.class);
		registerChild(CHILD_TXEMPLOYERPHONEEXT,TextField.class);
		registerChild(CHILD_TXEMPLOYERFAXAREACODE,TextField.class);
		registerChild(CHILD_TXEMPLOYERFAXEXCHANGE,TextField.class);
		registerChild(CHILD_TXEMPLOYERFAXROUTE,TextField.class);
		registerChild(CHILD_TXEMPLOYEREMAILADDRESS,TextField.class);
		registerChild(CHILD_TXEMPLOYERMAILINGADDRESSLINE1,TextField.class);
		registerChild(CHILD_TXEMPLOYERCITY,TextField.class);
		registerChild(CHILD_CBEMPLOYERPROVINCE,ComboBox.class);
		registerChild(CHILD_TXEMPLOYERPOSTALCODEFSA,TextField.class);
		registerChild(CHILD_TXEMPLOYERPOSTALCODELDU,TextField.class);
		registerChild(CHILD_TXEMPLOYERMAILINGADDRESSLINE2,TextField.class);
		registerChild(CHILD_CBJOBTITLE,ComboBox.class);
		registerChild(CHILD_HDJOBTITLE,HiddenField.class);
		registerChild(CHILD_TXJOBTITLE,TextField.class);
		registerChild(CHILD_CBOCCUPATION,ComboBox.class);
		registerChild(CHILD_CBINDUSTRYSECTOR,ComboBox.class);
		registerChild(CHILD_TXTIMEATJOBYEARS,TextField.class);
		registerChild(CHILD_TXTIMEATJOBMONTHS,TextField.class);
		registerChild(CHILD_CBINCOMETYPE,ComboBox.class);
		registerChild(CHILD_TXINCOMEDESC,TextField.class);
		registerChild(CHILD_CBINCOMEPERIOD,ComboBox.class);
		registerChild(CHILD_TXINCOMEAMOUNT,TextField.class);
		registerChild(CHILD_CBEMPLOYINCLUDEINGDS,ComboBox.class);
		registerChild(CHILD_TXEMPLOYPERCENTAGEINCLUDEINGDS,TextField.class);
		registerChild(CHILD_HDEMPLOYPERCENTAGEINCLUDEINGDS,HiddenField.class);
		registerChild(CHILD_CBEMPLOYINCLUDEINTDS,ComboBox.class);
		registerChild(CHILD_TXEMPLOYPERCENTAGEINCLUDEINTDS,TextField.class);
		registerChild(CHILD_HDEMPLOYPERCENTAGEINCLUDEINTDS,HiddenField.class);
		registerChild(CHILD_BTDELETEEMPLOYMENT,Button.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By Billy 05Nov2002
    ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

    //Set up all ComboBox options
    cbEmploymentStatusOptions.populate(getRequestContext());
    cbEmploymentTypeOptions.populate(getRequestContext());
    cbEmployerProvinceOptions.populate(getRequestContext());
    cbJobTitleOptions.populate(getRequestContext());
    cbOccupationOptions.populate(getRequestContext());
    cbIndustrySectorOptions.populate(getRequestContext());
    cbIncomeTypeOptions.populate(getRequestContext());
    cbIncomePeriodOptions.populate(getRequestContext());

    //Setup the Yes/No Options
    String yesStr = BXResources.getGenericMsg("YES_LABEL", handler.getTheSessionState().getLanguageId());
    String noStr = BXResources.getGenericMsg("NO_LABEL", handler.getTheSessionState().getLanguageId());
    cbEmployIncludeInGDSOptions.setOptions(new String[]{yesStr, noStr},new String[]{"Y", "N"});
    cbEmployIncludeInTDSOptions.setOptions(new String[]{yesStr, noStr},new String[]{"Y", "N"});
	
    handler.pageSaveState();
		//========================================================================

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated RepeatedEmployment_onBeforeRowDisplayEvent code here
      int rowNum = this.getTileIndex();
      boolean ret = true;

      ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
      handler.pageGetState(this.getParentViewBean());

      //logger = SysLog.getSysLogger("pgAERETV");

      // Modified by BILLY 16Jan2001
      // Get Data Object
      doApplicantEmploymentIncomeSelectModelImpl theObj =(doApplicantEmploymentIncomeSelectModelImpl)this.getdoApplicantEmploymentIncomeSelectModel();

      if (theObj.getSize() > 0)
      {

        //logger.debug("pgAERAATV@nextTile::Location: " + theObj.getLocation());
        //--> Important !! If the field (in TiledView) is not binded to any DataField, we must
        //--> set the default value here in order to populate the value in the Model (PrimaryModel).
        //--> Otherwise, when the page submitted (via button or Herf) the user input will not populate
        //--> Comment by BILLY 02Aug2002
        setDisplayFieldValue(CHILD_HDEMPLOYPERCENTAGEINCLUDEINGDS,
              new String(CHILD_HDEMPLOYPERCENTAGEINCLUDEINGDS_RESET_VALUE));
        setDisplayFieldValue(CHILD_HDEMPLOYPERCENTAGEINCLUDEINTDS,
              new String(CHILD_HDEMPLOYPERCENTAGEINCLUDEINTDS_RESET_VALUE));

        //--> BUG fix by Billy 02June2003
        //--> To set the default hidden field value if null
        if(getDisplayFieldValue("hdJobTitle") == null)
          setDisplayFieldValue(CHILD_HDJOBTITLE, new String(CHILD_HDJOBTITLE_RESET_VALUE));
        //=================================================

        handler.setApplicantEmploymentDisplayFields(theObj, rowNum);
      }
      else ret = false;

      handler.pageSaveState();

      return ret;
		}

		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
    return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_HDEMPLOYMENTID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_HDEMPLOYMENTID,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFEMPLOYMENTHISTORYID,
				CHILD_HDEMPLOYMENTID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDEMPLOYMENTCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_HDEMPLOYMENTCOPYID,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFCOPYID,
				CHILD_HDEMPLOYMENTCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDCONTACTID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_HDCONTACTID,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFCONTACTID,
				CHILD_HDCONTACTID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDCONTACTCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_HDCONTACTCOPYID,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFCONTACTCOPYID,
				CHILD_HDCONTACTCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDINCOMEID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_HDINCOMEID,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFINCOMEID,
				CHILD_HDINCOMEID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDINCOMECOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_HDINCOMECOPYID,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFINCOMECOPYID,
				CHILD_HDINCOMECOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDADDRID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_HDADDRID,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFADDRESSID,
				CHILD_HDADDRID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDADDRCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_HDADDRCOPYID,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFADDRCOPYID,
				CHILD_HDADDRCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXEMPLOYERNAME))
		{
			TextField child = new TextField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXEMPLOYERNAME,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFEMPLOYERNAME,
				CHILD_TXEMPLOYERNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBEMPLOYMENTSTATUS))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_CBEMPLOYMENTSTATUS,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFEMPLOYMENTSTATUSID,
				CHILD_CBEMPLOYMENTSTATUS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbEmploymentStatusOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBEMPLOYMENTTYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_CBEMPLOYMENTTYPE,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFEMPLOYMENTTYPEID,
				CHILD_CBEMPLOYMENTTYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbEmploymentTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXEMPLOYERPHONEAREACODE))
		{
			TextField child = new TextField(this,
				//getDefaultModel(),
        getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXEMPLOYERPHONEAREACODE,
				CHILD_TXEMPLOYERPHONEAREACODE,
				CHILD_TXEMPLOYERPHONEAREACODE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXEMPLOYERPHONEEXCHANGE))
		{
			TextField child = new TextField(this,
				//getDefaultModel(),
        getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXEMPLOYERPHONEEXCHANGE,
				CHILD_TXEMPLOYERPHONEEXCHANGE,
				CHILD_TXEMPLOYERPHONEEXCHANGE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXEMPLOYERPHONEROUTE))
		{
			TextField child = new TextField(this,
				//getDefaultModel(),
        getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXEMPLOYERPHONEROUTE,
				CHILD_TXEMPLOYERPHONEROUTE,
				CHILD_TXEMPLOYERPHONEROUTE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXEMPLOYERPHONEEXT))
		{
			TextField child = new TextField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXEMPLOYERPHONEEXT,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFCONTACTPHONENOEXTENSION,
				CHILD_TXEMPLOYERPHONEEXT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXEMPLOYERFAXAREACODE))
		{
			TextField child = new TextField(this,
				//getDefaultModel(),
        getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXEMPLOYERFAXAREACODE,
				CHILD_TXEMPLOYERFAXAREACODE,
				CHILD_TXEMPLOYERFAXAREACODE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXEMPLOYERFAXEXCHANGE))
		{
			TextField child = new TextField(this,
				//getDefaultModel(),
        getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXEMPLOYERFAXEXCHANGE,
				CHILD_TXEMPLOYERFAXEXCHANGE,
				CHILD_TXEMPLOYERFAXEXCHANGE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXEMPLOYERFAXROUTE))
		{
			TextField child = new TextField(this,
				//getDefaultModel(),
        getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXEMPLOYERFAXROUTE,
				CHILD_TXEMPLOYERFAXROUTE,
				CHILD_TXEMPLOYERFAXROUTE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXEMPLOYEREMAILADDRESS))
		{
			TextField child = new TextField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXEMPLOYEREMAILADDRESS,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFEMPLOYEREMAILADDRESS,
				CHILD_TXEMPLOYEREMAILADDRESS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXEMPLOYERMAILINGADDRESSLINE1))
		{
			TextField child = new TextField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXEMPLOYERMAILINGADDRESSLINE1,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFEMPLOYERADDRESSLINE1,
				CHILD_TXEMPLOYERMAILINGADDRESSLINE1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXEMPLOYERCITY))
		{
			TextField child = new TextField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXEMPLOYERCITY,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFEMPLOYERCITY,
				CHILD_TXEMPLOYERCITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBEMPLOYERPROVINCE))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_CBEMPLOYERPROVINCE,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFPROVINCEID,
				CHILD_CBEMPLOYERPROVINCE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbEmployerProvinceOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXEMPLOYERPOSTALCODEFSA))
		{
			TextField child = new TextField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXEMPLOYERPOSTALCODEFSA,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFEMPLOYERPOSTALFSA,
				CHILD_TXEMPLOYERPOSTALCODEFSA_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXEMPLOYERPOSTALCODELDU))
		{
			TextField child = new TextField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXEMPLOYERPOSTALCODELDU,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFEMPLOYERPOSTALLDU,
				CHILD_TXEMPLOYERPOSTALCODELDU_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXEMPLOYERMAILINGADDRESSLINE2))
		{
			TextField child = new TextField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXEMPLOYERMAILINGADDRESSLINE2,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFEMPLOYERADDRESSLINE2,
				CHILD_TXEMPLOYERMAILINGADDRESSLINE2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBJOBTITLE))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_CBJOBTITLE,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFJOBTITLEID,
				CHILD_CBJOBTITLE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbJobTitleOptions);
			return child;
		}
		else
		if (name.equals(CHILD_HDJOBTITLE))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_HDJOBTITLE,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFJOBTITTLE,
				CHILD_HDJOBTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXJOBTITLE))
		{
			TextField child = new TextField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXJOBTITLE,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFJOBTITTLE,
				CHILD_TXJOBTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBOCCUPATION))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_CBOCCUPATION,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFOCCUPATIONID,
				CHILD_CBOCCUPATION_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbOccupationOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBINDUSTRYSECTOR))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_CBINDUSTRYSECTOR,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFINDUSTRYSECTORID,
				CHILD_CBINDUSTRYSECTOR_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbIndustrySectorOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXTIMEATJOBYEARS))
		{
			TextField child = new TextField(this,
				//getDefaultModel(),
        getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXTIMEATJOBYEARS,
				CHILD_TXTIMEATJOBYEARS,
				CHILD_TXTIMEATJOBYEARS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXTIMEATJOBMONTHS))
		{
			TextField child = new TextField(this,
				//getDefaultModel(),
        getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXTIMEATJOBMONTHS,
				CHILD_TXTIMEATJOBMONTHS,
				CHILD_TXTIMEATJOBMONTHS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBINCOMETYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_CBINCOMETYPE,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFINCOMETYPEID,
				CHILD_CBINCOMETYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbIncomeTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXINCOMEDESC))
		{
			TextField child = new TextField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXINCOMEDESC,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFINCOMEDESC,
				CHILD_TXINCOMEDESC_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBINCOMEPERIOD))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_CBINCOMEPERIOD,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFINCOMEPERIODID,
				CHILD_CBINCOMEPERIOD_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbIncomePeriodOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXINCOMEAMOUNT))
		{
			TextField child = new TextField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXINCOMEAMOUNT,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFINCOMEAMOUNT,
				CHILD_TXINCOMEAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBEMPLOYINCLUDEINGDS))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_CBEMPLOYINCLUDEINGDS,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFINCLUDEINGDS,
				CHILD_CBEMPLOYINCLUDEINGDS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbEmployIncludeInGDSOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXEMPLOYPERCENTAGEINCLUDEINGDS))
		{
			TextField child = new TextField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXEMPLOYPERCENTAGEINCLUDEINGDS,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFPERCENTINCLUDEDINGDS,
				CHILD_TXEMPLOYPERCENTAGEINCLUDEINGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDEMPLOYPERCENTAGEINCLUDEINGDS))
		{
			HiddenField child = new HiddenField(this,
				//getDefaultModel(),
        getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_HDEMPLOYPERCENTAGEINCLUDEINGDS,
				CHILD_HDEMPLOYPERCENTAGEINCLUDEINGDS,
				CHILD_HDEMPLOYPERCENTAGEINCLUDEINGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBEMPLOYINCLUDEINTDS))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_CBEMPLOYINCLUDEINTDS,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFINCLUDEINTDS,
				CHILD_CBEMPLOYINCLUDEINTDS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbEmployIncludeInTDSOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXEMPLOYPERCENTAGEINCLUDEINTDS))
		{
			TextField child = new TextField(this,
				getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_TXEMPLOYPERCENTAGEINCLUDEINTDS,
				doApplicantEmploymentIncomeSelectModel.FIELD_DFPERCENTINCLUDEDINTDS,
				CHILD_TXEMPLOYPERCENTAGEINCLUDEINTDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDEMPLOYPERCENTAGEINCLUDEINTDS))
		{
			HiddenField child = new HiddenField(this,
				//getDefaultModel(),
        getdoApplicantEmploymentIncomeSelectModel(),
				CHILD_HDEMPLOYPERCENTAGEINCLUDEINTDS,
				CHILD_HDEMPLOYPERCENTAGEINCLUDEINTDS,
				CHILD_HDEMPLOYPERCENTAGEINCLUDEINTDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETEEMPLOYMENT))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETEEMPLOYMENT,
				CHILD_BTDELETEEMPLOYMENT,
				CHILD_BTDELETEEMPLOYMENT_RESET_VALUE,
				null);
				return child;

		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdEmploymentId()
	{
		return (HiddenField)getChild(CHILD_HDEMPLOYMENTID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdEmploymentCopyId()
	{
		return (HiddenField)getChild(CHILD_HDEMPLOYMENTCOPYID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdContactId()
	{
		return (HiddenField)getChild(CHILD_HDCONTACTID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdContactCopyId()
	{
		return (HiddenField)getChild(CHILD_HDCONTACTCOPYID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdIncomeId()
	{
		return (HiddenField)getChild(CHILD_HDINCOMEID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdIncomeCopyId()
	{
		return (HiddenField)getChild(CHILD_HDINCOMECOPYID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdAddrId()
	{
		return (HiddenField)getChild(CHILD_HDADDRID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdAddrCopyId()
	{
		return (HiddenField)getChild(CHILD_HDADDRCOPYID);
	}


	/**
	 *
	 *
	 */
	public TextField getTxEmployerName()
	{
		return (TextField)getChild(CHILD_TXEMPLOYERNAME);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbEmploymentStatus()
	{
		return (ComboBox)getChild(CHILD_CBEMPLOYMENTSTATUS);
	}

	/**
	 *
	 *
	 */
	static class CbEmploymentStatusOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbEmploymentStatusOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 21Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "EMPLOYMENTHISTORYSTATUS", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public ComboBox getCbEmploymentType()
	{
		return (ComboBox)getChild(CHILD_CBEMPLOYMENTTYPE);
	}

	/**
	 *
	 *
	 */
	static class CbEmploymentTypeOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbEmploymentTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 21Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "EMPLOYMENTHISTORYTYPE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public TextField getTxEmployerPhoneAreaCode()
	{
		return (TextField)getChild(CHILD_TXEMPLOYERPHONEAREACODE);
	}


	/**
	 *
	 *
	 */
	public TextField getTxEmployerPhoneExchange()
	{
		return (TextField)getChild(CHILD_TXEMPLOYERPHONEEXCHANGE);
	}


	/**
	 *
	 *
	 */
	public TextField getTxEmployerPhoneRoute()
	{
		return (TextField)getChild(CHILD_TXEMPLOYERPHONEROUTE);
	}


	/**
	 *
	 *
	 */
	public TextField getTxEmployerPhoneExt()
	{
		return (TextField)getChild(CHILD_TXEMPLOYERPHONEEXT);
	}


	/**
	 *
	 *
	 */
	public TextField getTxEmployerFaxAreaCode()
	{
		return (TextField)getChild(CHILD_TXEMPLOYERFAXAREACODE);
	}


	/**
	 *
	 *
	 */
	public TextField getTxEmployerFaxExchange()
	{
		return (TextField)getChild(CHILD_TXEMPLOYERFAXEXCHANGE);
	}


	/**
	 *
	 *
	 */
	public TextField getTxEmployerFaxRoute()
	{
		return (TextField)getChild(CHILD_TXEMPLOYERFAXROUTE);
	}


	/**
	 *
	 *
	 */
	public TextField getTxEmployerEmailAddress()
	{
		return (TextField)getChild(CHILD_TXEMPLOYEREMAILADDRESS);
	}


	/**
	 *
	 *
	 */
	public TextField getTxEmployerMailingAddressLine1()
	{
		return (TextField)getChild(CHILD_TXEMPLOYERMAILINGADDRESSLINE1);
	}


	/**
	 *
	 *
	 */
	public TextField getTxEmployerCity()
	{
		return (TextField)getChild(CHILD_TXEMPLOYERCITY);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbEmployerProvince()
	{
		return (ComboBox)getChild(CHILD_CBEMPLOYERPROVINCE);
	}

	/**
	 *
	 *
	 */
	static class CbEmployerProvinceOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbEmployerProvinceOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 21Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "PROVINCE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public TextField getTxEmployerPostalCodeFSA()
	{
		return (TextField)getChild(CHILD_TXEMPLOYERPOSTALCODEFSA);
	}


	/**
	 *
	 *
	 */
	public TextField getTxEmployerPostalCodeLDU()
	{
		return (TextField)getChild(CHILD_TXEMPLOYERPOSTALCODELDU);
	}


	/**
	 *
	 *
	 */
	public TextField getTxEmployerMailingAddressLine2()
	{
		return (TextField)getChild(CHILD_TXEMPLOYERMAILINGADDRESSLINE2);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbJobTitle()
	{
		return (ComboBox)getChild(CHILD_CBJOBTITLE);
	}

	/**
	 *
	 *
	 */
	static class CbJobTitleOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbJobTitleOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 21Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "JOBTITLE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public HiddenField getHdJobTitle()
	{
		return (HiddenField)getChild(CHILD_HDJOBTITLE);
	}


	/**
	 *
	 *
	 */
	public TextField getTxJobTitle()
	{
		return (TextField)getChild(CHILD_TXJOBTITLE);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbOccupation()
	{
		return (ComboBox)getChild(CHILD_CBOCCUPATION);
	}

	/**
	 *
	 *
	 */
	static class CbOccupationOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbOccupationOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 21Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "OCCUPATION", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public ComboBox getCbIndustrySector()
	{
		return (ComboBox)getChild(CHILD_CBINDUSTRYSECTOR);
	}

	/**
	 *
	 *
	 */
	static class CbIndustrySectorOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbIndustrySectorOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 21Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "INDUSTRYSECTOR", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public TextField getTxTimeAtJobYears()
	{
		return (TextField)getChild(CHILD_TXTIMEATJOBYEARS);
	}


	/**
	 *
	 *
	 */
	public TextField getTxTimeAtJobMonths()
	{
		return (TextField)getChild(CHILD_TXTIMEATJOBMONTHS);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbIncomeType()
	{
		return (ComboBox)getChild(CHILD_CBINCOMETYPE);
	}

	/**
	 *
	 *
	 */
	static class CbIncomeTypeOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbIncomeTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
			Connection c = null;
			try {
				clear();
				SelectQueryModel m = null;

				SelectQueryExecutionContext eContext=new SelectQueryExecutionContext(
					(Connection)null,
					DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
					DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

				if(rc == null) {
					m = new doApplicantEmploymentIncomeTypeModelImpl();
					c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
					eContext.setConnection(c);
				}
				else {
					m = (SelectQueryModel) rc.getModelManager().getModel(doApplicantEmploymentIncomeTypeModel.class);
				}

				m.retrieve(eContext);
				m.beforeFirst();

				while (m.next()) {
					Object MOS_INCOMETYPE_INCOMETYPEID = m.getValue(doApplicantEmploymentIncomeTypeModel.FIELD_MOS_INCOMETYPE_INCOMETYPEID);
					Object MOS_INCOMETYPE_ITDESCRIPTION = m.getValue(doApplicantEmploymentIncomeTypeModel.FIELD_MOS_INCOMETYPE_ITDESCRIPTION);

					String label = (MOS_INCOMETYPE_ITDESCRIPTION == null?"":MOS_INCOMETYPE_ITDESCRIPTION.toString());

					String value = (MOS_INCOMETYPE_INCOMETYPEID == null?"":MOS_INCOMETYPE_INCOMETYPEID.toString());

					add(label, value);
				}
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			finally {
				try {
					if(c != null)
						c.close();
				}
				catch (SQLException  ex) {
					// ignore
				}
			}
		}

	}



	/**
	 *
	 *
	 */
	public TextField getTxIncomeDesc()
	{
		return (TextField)getChild(CHILD_TXINCOMEDESC);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbIncomePeriod()
	{
		return (ComboBox)getChild(CHILD_CBINCOMEPERIOD);
	}

	/**
	 *
	 *
	 */
	static class CbIncomePeriodOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbIncomePeriodOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 21Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "INCOMEPERIOD", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public TextField getTxIncomeAmount()
	{
		return (TextField)getChild(CHILD_TXINCOMEAMOUNT);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbEmployIncludeInGDS()
	{
		return (ComboBox)getChild(CHILD_CBEMPLOYINCLUDEINGDS);
	}


	/**
	 *
	 *
	 */
	public TextField getTxEmployPercentageIncludeInGDS()
	{
		return (TextField)getChild(CHILD_TXEMPLOYPERCENTAGEINCLUDEINGDS);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdEmployPercentageIncludeInGDS()
	{
		return (HiddenField)getChild(CHILD_HDEMPLOYPERCENTAGEINCLUDEINGDS);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbEmployIncludeInTDS()
	{
		return (ComboBox)getChild(CHILD_CBEMPLOYINCLUDEINTDS);
	}


	/**
	 *
	 *
	 */
	public TextField getTxEmployPercentageIncludeInTDS()
	{
		return (TextField)getChild(CHILD_TXEMPLOYPERCENTAGEINCLUDEINTDS);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdEmployPercentageIncludeInTDS()
	{
		return (HiddenField)getChild(CHILD_HDEMPLOYPERCENTAGEINCLUDEINTDS);
	}


	/**
	 *
	 *
	 */
	public void handleBtDeleteEmploymentRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btDeleteEmployment_onWebEvent method
		ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());
		handler.handleDelete(false, 7, handler.getRowNdxFromWebEventMethod(event), "RepeatedEmployment/hdEmploymentId", "employmentHistoryId", "RepeatedEmployment/hdEmploymentCopyId", "EmploymentHistory");
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtDeleteEmployment()
	{
		return (Button)getChild(CHILD_BTDELETEEMPLOYMENT);
	}


	/**
	 *
	 *
	 */
	public String endBtDeleteEmploymentDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btDeleteEmployment_onBeforeHtmlOutputEvent method
		ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public doApplicantEmploymentIncomeSelectModel getdoApplicantEmploymentIncomeSelectModel()
	{
		if (doApplicantEmploymentIncomeSelect == null)
			doApplicantEmploymentIncomeSelect = (doApplicantEmploymentIncomeSelectModel) getModel(doApplicantEmploymentIncomeSelectModel.class);
		return doApplicantEmploymentIncomeSelect;
	}


	/**
	 *
	 *
	 */
	public void setdoApplicantEmploymentIncomeSelectModel(doApplicantEmploymentIncomeSelectModel model)
	{
			doApplicantEmploymentIncomeSelect = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_HDEMPLOYMENTID="hdEmploymentId";
	public static final String CHILD_HDEMPLOYMENTID_RESET_VALUE="";
	public static final String CHILD_HDEMPLOYMENTCOPYID="hdEmploymentCopyId";
	public static final String CHILD_HDEMPLOYMENTCOPYID_RESET_VALUE="";
	public static final String CHILD_HDCONTACTID="hdContactId";
	public static final String CHILD_HDCONTACTID_RESET_VALUE="";
	public static final String CHILD_HDCONTACTCOPYID="hdContactCopyId";
	public static final String CHILD_HDCONTACTCOPYID_RESET_VALUE="";
	public static final String CHILD_HDINCOMEID="hdIncomeId";
	public static final String CHILD_HDINCOMEID_RESET_VALUE="";
	public static final String CHILD_HDINCOMECOPYID="hdIncomeCopyId";
	public static final String CHILD_HDINCOMECOPYID_RESET_VALUE="";
	public static final String CHILD_HDADDRID="hdAddrId";
	public static final String CHILD_HDADDRID_RESET_VALUE="";
	public static final String CHILD_HDADDRCOPYID="hdAddrCopyId";
	public static final String CHILD_HDADDRCOPYID_RESET_VALUE="";
	public static final String CHILD_TXEMPLOYERNAME="txEmployerName";
	public static final String CHILD_TXEMPLOYERNAME_RESET_VALUE="";
	public static final String CHILD_CBEMPLOYMENTSTATUS="cbEmploymentStatus";
	public static final String CHILD_CBEMPLOYMENTSTATUS_RESET_VALUE="";
  //--Release2.1--//
	//private static CbEmploymentStatusOptionList cbEmploymentStatusOptions=new CbEmploymentStatusOptionList();
  private CbEmploymentStatusOptionList cbEmploymentStatusOptions=new CbEmploymentStatusOptionList();
	public static final String CHILD_CBEMPLOYMENTTYPE="cbEmploymentType";
	public static final String CHILD_CBEMPLOYMENTTYPE_RESET_VALUE="";
  //--Release2.1--//
	//private static CbEmploymentTypeOptionList cbEmploymentTypeOptions=new CbEmploymentTypeOptionList();
  private CbEmploymentTypeOptionList cbEmploymentTypeOptions=new CbEmploymentTypeOptionList();
	public static final String CHILD_TXEMPLOYERPHONEAREACODE="txEmployerPhoneAreaCode";
	public static final String CHILD_TXEMPLOYERPHONEAREACODE_RESET_VALUE="";
	public static final String CHILD_TXEMPLOYERPHONEEXCHANGE="txEmployerPhoneExchange";
	public static final String CHILD_TXEMPLOYERPHONEEXCHANGE_RESET_VALUE="";
	public static final String CHILD_TXEMPLOYERPHONEROUTE="txEmployerPhoneRoute";
	public static final String CHILD_TXEMPLOYERPHONEROUTE_RESET_VALUE="";
	public static final String CHILD_TXEMPLOYERPHONEEXT="txEmployerPhoneExt";
	public static final String CHILD_TXEMPLOYERPHONEEXT_RESET_VALUE="";
	public static final String CHILD_TXEMPLOYERFAXAREACODE="txEmployerFaxAreaCode";
	public static final String CHILD_TXEMPLOYERFAXAREACODE_RESET_VALUE="";
	public static final String CHILD_TXEMPLOYERFAXEXCHANGE="txEmployerFaxExchange";
	public static final String CHILD_TXEMPLOYERFAXEXCHANGE_RESET_VALUE="";
	public static final String CHILD_TXEMPLOYERFAXROUTE="txEmployerFaxRoute";
	public static final String CHILD_TXEMPLOYERFAXROUTE_RESET_VALUE="";
	public static final String CHILD_TXEMPLOYEREMAILADDRESS="txEmployerEmailAddress";
	public static final String CHILD_TXEMPLOYEREMAILADDRESS_RESET_VALUE="";
	public static final String CHILD_TXEMPLOYERMAILINGADDRESSLINE1="txEmployerMailingAddressLine1";
	public static final String CHILD_TXEMPLOYERMAILINGADDRESSLINE1_RESET_VALUE="";
	public static final String CHILD_TXEMPLOYERCITY="txEmployerCity";
	public static final String CHILD_TXEMPLOYERCITY_RESET_VALUE="";
	public static final String CHILD_CBEMPLOYERPROVINCE="cbEmployerProvince";
	public static final String CHILD_CBEMPLOYERPROVINCE_RESET_VALUE="";
  //--Release2.1--//
	//private static CbEmployerProvinceOptionList cbEmployerProvinceOptions=new CbEmployerProvinceOptionList();
	private CbEmployerProvinceOptionList cbEmployerProvinceOptions=new CbEmployerProvinceOptionList();
	public static final String CHILD_TXEMPLOYERPOSTALCODEFSA="txEmployerPostalCodeFSA";
	public static final String CHILD_TXEMPLOYERPOSTALCODEFSA_RESET_VALUE="";
	public static final String CHILD_TXEMPLOYERPOSTALCODELDU="txEmployerPostalCodeLDU";
	public static final String CHILD_TXEMPLOYERPOSTALCODELDU_RESET_VALUE="";
	public static final String CHILD_TXEMPLOYERMAILINGADDRESSLINE2="txEmployerMailingAddressLine2";
	public static final String CHILD_TXEMPLOYERMAILINGADDRESSLINE2_RESET_VALUE="";
	public static final String CHILD_CBJOBTITLE="cbJobTitle";
	public static final String CHILD_CBJOBTITLE_RESET_VALUE="0";
  //--Release2.1--//
	//private static CbJobTitleOptionList cbJobTitleOptions=new CbJobTitleOptionList();
  private CbJobTitleOptionList cbJobTitleOptions=new CbJobTitleOptionList();
	public static final String CHILD_HDJOBTITLE="hdJobTitle";
	public static final String CHILD_HDJOBTITLE_RESET_VALUE="";
	public static final String CHILD_TXJOBTITLE="txJobTitle";
	public static final String CHILD_TXJOBTITLE_RESET_VALUE="";
	public static final String CHILD_CBOCCUPATION="cbOccupation";
	public static final String CHILD_CBOCCUPATION_RESET_VALUE="";
  //--Release2.1--//
	//private static CbOccupationOptionList cbOccupationOptions=new CbOccupationOptionList();
  private CbOccupationOptionList cbOccupationOptions=new CbOccupationOptionList();
	public static final String CHILD_CBINDUSTRYSECTOR="cbIndustrySector";
	public static final String CHILD_CBINDUSTRYSECTOR_RESET_VALUE="";
  //--Release2.1--//
	//private static CbIndustrySectorOptionList cbIndustrySectorOptions=new CbIndustrySectorOptionList();
  private CbIndustrySectorOptionList cbIndustrySectorOptions=new CbIndustrySectorOptionList();
	public static final String CHILD_TXTIMEATJOBYEARS="txTimeAtJobYears";
	public static final String CHILD_TXTIMEATJOBYEARS_RESET_VALUE="";
	public static final String CHILD_TXTIMEATJOBMONTHS="txTimeAtJobMonths";
	public static final String CHILD_TXTIMEATJOBMONTHS_RESET_VALUE="";
	public static final String CHILD_CBINCOMETYPE="cbIncomeType";
	public static final String CHILD_CBINCOMETYPE_RESET_VALUE="";
  //--Release2.1--//
	//private static CbIncomeTypeOptionList cbIncomeTypeOptions=new CbIncomeTypeOptionList();
  private CbIncomeTypeOptionList cbIncomeTypeOptions=new CbIncomeTypeOptionList();
	public static final String CHILD_TXINCOMEDESC="txIncomeDesc";
	public static final String CHILD_TXINCOMEDESC_RESET_VALUE="";
	public static final String CHILD_CBINCOMEPERIOD="cbIncomePeriod";
	public static final String CHILD_CBINCOMEPERIOD_RESET_VALUE="";
  //--Release2.1--//
	//private static CbIncomePeriodOptionList cbIncomePeriodOptions=new CbIncomePeriodOptionList();
  private CbIncomePeriodOptionList cbIncomePeriodOptions=new CbIncomePeriodOptionList();
	public static final String CHILD_TXINCOMEAMOUNT="txIncomeAmount";
	public static final String CHILD_TXINCOMEAMOUNT_RESET_VALUE="";
	public static final String CHILD_CBEMPLOYINCLUDEINGDS="cbEmployIncludeInGDS";
	public static final String CHILD_CBEMPLOYINCLUDEINGDS_RESET_VALUE="Y";
  //--Release2.1--//
	//private static OptionList cbEmployIncludeInGDSOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
	private OptionList cbEmployIncludeInGDSOptions=new OptionList();
	public static final String CHILD_TXEMPLOYPERCENTAGEINCLUDEINGDS="txEmployPercentageIncludeInGDS";
	public static final String CHILD_TXEMPLOYPERCENTAGEINCLUDEINGDS_RESET_VALUE="";
	public static final String CHILD_HDEMPLOYPERCENTAGEINCLUDEINGDS="hdEmployPercentageIncludeInGDS";
	public static final String CHILD_HDEMPLOYPERCENTAGEINCLUDEINGDS_RESET_VALUE="";
	public static final String CHILD_CBEMPLOYINCLUDEINTDS="cbEmployIncludeInTDS";
	public static final String CHILD_CBEMPLOYINCLUDEINTDS_RESET_VALUE="Y";
  //--Release2.1--//
	//private static OptionList cbEmployIncludeInTDSOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
	private OptionList cbEmployIncludeInTDSOptions=new OptionList();
  public static final String CHILD_TXEMPLOYPERCENTAGEINCLUDEINTDS="txEmployPercentageIncludeInTDS";
	public static final String CHILD_TXEMPLOYPERCENTAGEINCLUDEINTDS_RESET_VALUE="";
	public static final String CHILD_HDEMPLOYPERCENTAGEINCLUDEINTDS="hdEmployPercentageIncludeInTDS";
	public static final String CHILD_HDEMPLOYPERCENTAGEINCLUDEINTDS_RESET_VALUE="";
	public static final String CHILD_BTDELETEEMPLOYMENT="btDeleteEmployment";
	public static final String CHILD_BTDELETEEMPLOYMENT_RESET_VALUE=" ";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doApplicantEmploymentIncomeSelectModel doApplicantEmploymentIncomeSelect=null;

  private ApplicantEntryHandler handler=new ApplicantEntryHandler();

  //public SysLogger logger;

}

