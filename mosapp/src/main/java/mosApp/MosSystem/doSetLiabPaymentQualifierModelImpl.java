package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doSetLiabPaymentQualifierModelImpl extends QueryModelBase
	implements doSetLiabPaymentQualifierModel
{
	/**
	 *
	 *
	 */
	public doSetLiabPaymentQualifierModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIABILITYTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIABILITYTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLiabilityDescription()
	{
		return (String)getValue(FIELD_DFLIABILITYDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityDescription(String value)
	{
		setValue(FIELD_DFLIABILITYDESCRIPTION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLiabPaymentQualifier()
	{
		return (String)getValue(FIELD_DFLIABPAYMENTQUALIFIER);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabPaymentQualifier(String value)
	{
		setValue(FIELD_DFLIABPAYMENTQUALIFIER,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL LIABILITYTYPE.LIABILITYTYPEID, LIABILITYTYPE.LDESCRIPTION, LIABILITYTYPE.LIABILITYPAYMENTQUALIFIER FROM LIABILITYTYPE  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="LIABILITYTYPE";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFLIABILITYTYPEID="LIABILITYTYPE.LIABILITYTYPEID";
	public static final String COLUMN_DFLIABILITYTYPEID="LIABILITYTYPEID";
	public static final String QUALIFIED_COLUMN_DFLIABILITYDESCRIPTION="LIABILITYTYPE.LDESCRIPTION";
	public static final String COLUMN_DFLIABILITYDESCRIPTION="LDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFLIABPAYMENTQUALIFIER="LIABILITYTYPE.LIABILITYPAYMENTQUALIFIER";
	public static final String COLUMN_DFLIABPAYMENTQUALIFIER="LIABILITYPAYMENTQUALIFIER";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYTYPEID,
				COLUMN_DFLIABILITYTYPEID,
				QUALIFIED_COLUMN_DFLIABILITYTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYDESCRIPTION,
				COLUMN_DFLIABILITYDESCRIPTION,
				QUALIFIED_COLUMN_DFLIABILITYDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABPAYMENTQUALIFIER,
				COLUMN_DFLIABPAYMENTQUALIFIER,
				QUALIFIED_COLUMN_DFLIABPAYMENTQUALIFIER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

