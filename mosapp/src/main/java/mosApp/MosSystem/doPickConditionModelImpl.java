package mosApp.MosSystem;

import java.sql.SQLException;

import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doPickConditionModelImpl extends QueryModelBase
	implements doPickConditionModel
{
	/**
	 *
	 *
	 */
	public doPickConditionModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfConditionId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCONDITIONID);
	}


	/**
	 *
	 *
	 */
	public void setDfConditionId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCONDITIONID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDescription()
	{
		return (String)getValue(FIELD_DFDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfDescription(String value)
	{
		setValue(FIELD_DFDESCRIPTION,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //--> In order to support Condition creation on the fly it is not possible to hardcode the
  //--> Condition Labels (descriptions) on ResourceBundle.  Instead, we have to get the Label
  //--> by specific language from the ConditVerbiage
  //--> By Billy 25Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> 1) Add joins to ConditVerbiage table
  //--> 2) convert CONDITION.CONDITIONDESC ==> CONDITIONVERBIAGE.CONDITIONLABEL
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL CONDITION.CONDITIONID, CONDITION.CONDITIONDESC FROM CONDITION  __WHERE__  ORDER BY CONDITION.CONDITIONDESC  ASC";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL CONDITION.CONDITIONID, CONDITIONVERBIAGE.CONDITIONLABEL "+
    "FROM CONDITION, CONDITIONVERBIAGE  "+
    "__WHERE__  "+
    "ORDER BY CONDITIONVERBIAGE.CONDITIONLABEL  ASC";

  public static final String MODIFYING_QUERY_TABLE_NAME="CONDITION, CONDITIONVERBIAGE";
  //--Release2.1--//
  //--> Add joins to ConditVerbiage table
	public static final String STATIC_WHERE_CRITERIA=
    " CONDITION.CONDITIONTYPEID In (0,2) AND "+
    "CONDITION.USERSELECTABLE = 'Y' AND "+
    "CONDITION.CONDITIONID = CONDITIONVERBIAGE.CONDITIONID " +
    "AND CONDITION.INSTITUTIONPROFILEID = CONDITIONVERBIAGE.INSTITUTIONPROFILEID ";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFCONDITIONID="CONDITION.CONDITIONID";
	public static final String COLUMN_DFCONDITIONID="CONDITIONID";
  //--Release2.1--//
  //--> convert CONDITION.CONDITIONDESC ==> CONDITIONVERBIAGE.CONDITIONLABEL
	//public static final String QUALIFIED_COLUMN_DFDESCRIPTION="CONDITION.CONDITIONDESC";
	//public static final String COLUMN_DFDESCRIPTION="CONDITIONDESC";
  public static final String QUALIFIED_COLUMN_DFDESCRIPTION="CONDITIONVERBIAGE.CONDITIONLABEL";
	public static final String COLUMN_DFDESCRIPTION="CONDITIONLABEL";
  //============================================================================

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONDITIONID,
				COLUMN_DFCONDITIONID,
				QUALIFIED_COLUMN_DFCONDITIONID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDESCRIPTION,
				COLUMN_DFDESCRIPTION,
				QUALIFIED_COLUMN_DFDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

