package mosApp.MosSystem;

import java.math.BigDecimal;

import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.model.sql.QueryFieldSchema;

import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.ModelControlException;
import com.basis100.resources.*;

import java.sql.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.filogix.externallinks.framework.ServiceConst;;

public class doFixedPriceClosingInfoModelImpl extends QueryModelBase implements
        doFixedPriceClosingInfoModel {

    //The logger
    private static Log _log = LogFactory.getLog(doFixedPriceClosingInfoModelImpl.class);

    // static definitions;
    public static final String DATA_SOURCE_NAME = "jdbc/orcl";
    // select SQL template.
    public static final String SELECT_SQL_TEMPLATE =
        "SELECT DISTINCT " +
        "REQUEST.REQUESTDATE, " +
        "REQUEST.STATUSDATE, " +
        "REQUEST.DEALID, " +
        "REQUEST.COPYID, " +
        "REQUEST.REQUESTID, " +
        "REQUEST.STATUSMESSAGE, " +
        "SERVICEREQUEST.SERVICEPROVIDERREFNO, " +
        "SERVICEPRODUCT.SERVICEPROVIDERID, " +
        "SERVICEPRODUCT.SERVICEPRODUCTID, " +
        "to_char(REQUEST.REQUESTSTATUSID) STATUSID_STR, " +
        "REQUEST.REQUESTSTATUSID, " +
        "SERVICEREQUESTCONTACT.SERVICEREQUESTCONTACTID, " +
        "SERVICEREQUESTCONTACT.NAMEFIRST, " +
        "SERVICEREQUESTCONTACT.NAMELAST, " +
        "SERVICEREQUESTCONTACT.EMAILADDRESS, " +
        "SERVICEREQUESTCONTACT.BORROWERID, " +
        "SERVICEREQUESTCONTACT.WORKAREACODE, " +
        "SERVICEREQUESTCONTACT.WORKPHONENUMBER, " +
        "SUBSTR(SERVICEREQUESTCONTACT.WORKPHONENUMBER,0,3) WORKPHONEFIRSTTHREEDIGITS, " +
        "SUBSTR(SERVICEREQUESTCONTACT.WORKPHONENUMBER,4) WORKPHONELASTFOURDIGITS, " +
        "SERVICEREQUESTCONTACT.WORKEXTENSION, " +
        "SERVICEREQUESTCONTACT.CELLAREACODE, " +
        "SERVICEREQUESTCONTACT.CELLPHONENUMBER, " +
        "SUBSTR(SERVICEREQUESTCONTACT.CELLPHONENUMBER,0,3) CELLPHONEFIRSTTHREEDIGITS, " +
        "SUBSTR(SERVICEREQUESTCONTACT.CELLPHONENUMBER,4) CELLPHONELASTFOURDIGITS, " +
        "SERVICEREQUESTCONTACT.HOMEAREACODE, " +
        "SERVICEREQUESTCONTACT.HOMEPHONENUMBER, "+
        "SUBSTR(SERVICEREQUESTCONTACT.HOMEPHONENUMBER,0,3) HOMEPHONEFIRSTTHREEDIGITS, " +
        "SUBSTR(SERVICEREQUESTCONTACT.HOMEPHONENUMBER,4) HOMEPHONELASTFOURDIGITS, " +
        "SERVICEREQUESTCONTACT.COMMENTS " +
        "FROM " +
        "REQUEST, SERVICEREQUEST, SERVICEREQUESTCONTACT, SERVICEPRODUCT " +
        "__WHERE__ ORDER BY REQUEST.REQUESTDATE DESC";


    // the query table name.
    public static final String MODIFYING_QUERY_TABLE_NAME =
        "REQUEST, SERVICEREQUEST, SERVICEREQUESTCONTACT, SERVICEPRODUCT";
    // the static query criteria.
    public static final String STATIC_WHERE_CRITERIA =
        "(REQUEST.INSTITUTIONPROFILEID = SERVICEPRODUCT.INSTITUTIONPROFILEID) AND " +
        "(REQUEST.INSTITUTIONPROFILEID = SERVICEREQUESTCONTACT.INSTITUTIONPROFILEID) AND " +
        "(SERVICEREQUESTCONTACT.REQUESTID = REQUEST.REQUESTID) AND " +
        "(REQUEST.SERVICEPRODUCTID = SERVICEPRODUCT.SERVICEPRODUCTID) AND " + 
        "(SERVICEPRODUCT.SERVICETYPEID = 3) AND " + 
        "(SERVICEPRODUCT.SERVICESUBTYPEID = 3) ";
    
    // the query field schema.
    public static final QueryFieldSchema FIELD_SCHEMA = 
        new QueryFieldSchema();

    //column definition for each field.
    public final static String QUALIFIED_COLUMN_DFFPCDEALID = "REQUEST.DEALID";
    public final static String COLUMN_DFFPCDEALID = "DEALID";
    
    public final static String QUALIFIED_COLUMN_DFFPCCOPYID = "REQUEST.COPYID";
    public final static String COLUMN_DFFPCCOPYID = "COPYID";
    
    public final static String QUALIFIED_COLUMN_DFFPCREQUESTID = "REQUEST.REQUESTID";
    public final static String COLUMN_DFFPCREQUESTID = "REQUESTID";

    public final static String QUALIFIED_COLUMN_DFFPCCONTACTID = "SERVICEREQUESTCONTACT.SERVICEREQUESTCONTACTID";
    public final static String COLUMN_DFFPCCONTACTID = "SERVICEREQUESTCONTACTID";
    
    public final static String QUALIFIED_COLUMN_DFFPCBORROWERID = "SERVICEREQUESTCONTACT.BORROWERID";
    public final static String COLUMN_DFFPCBORROWERID = "BORROWERID";

    public static final String QUALIFIED_COLUMN_DFFPCPROVIDERID =
        "SERVICEPRODUCT.SERVICEPROVIDERID";
    public static final String COLUMN_DFFPCPROVIDERID =
        "SERVICEPROVIDERID";

    public static final String QUALIFIED_COLUMN_DFFPCPRODUCTID =
        "SERVICEPRODUCT.SERVICEPRODUCTID";
    public static final String COLUMN_DFFPCPRODUCTID =
        "SERVICEPRODUCTID";
    
    public final static String QUALIFIED_COLUMN_DFFPCREQUESTSTATUS = "REQUEST.STATUSID_STR";
    public final static String COLUMN_DFFPCREQUESTSTATUS = "STATUSID_STR";


	public final static String QUALIFIED_COLUMN_DFFPCREQUESTSTATUSID = "REQUEST.REQUESTSTATUSID";
    public final static String COLUMN_DFFPCREQUESTSTATUSID = "REQUESTSTATUSID";
    
    public final static String QUALIFIED_COLUMN_DFFPCREQUESTDATE = "REQUEST.STATUSDATE";
    public final static String COLUMN_DFFPCREQUESTDATE = "STATUSDATE";

    public final static String QUALIFIED_COLUMN_DFFPCCONTACTFIRSTNAME = "SERVICEREQUESTCONTACT.NAMEFIRST";
    public final static String COLUMN_DFFPCCONTACTFIRSTNAME = "NAMEFIRST";

    public final static String QUALIFIED_COLUMN_DFFPCCONTACTLASTNAME = "SERVICEREQUESTCONTACT.NAMELAST";
    public final static String COLUMN_DFFPCCONTACTLASTNAME = "NAMELAST";
    
    public final static String QUALIFIED_COLUMN_DFFPCEMAIL = "SERVICEREQUESTCONTACT.EMAILADDRESS";
    public final static String COLUMN_DFFPCEMAIL = "EMAILADDRESS";

    public final static String QUALIFIED_COLUMN_DFFPCWORKPHONEAREACODE = "SERVICEREQUESTCONTACT.WORKAREACODE";
    public final static String COLUMN_DFFPCWORKPHONEAREACODE = "WORKAREACODE";
    
    public final static String QUALIFIED_COLUMN_DFFPCWORKPHONEFIRSTTHREEDIGITS = 
        "SERVICEREQUESTCONTACT.WORKPHONEFIRSTTHREEDIGITS";
    public final static String COLUMN_DFFPCWORKPHONEFIRSTTHREEDIGITS = "WORKPHONEFIRSTTHREEDIGITS";

    public final static String QUALIFIED_COLUMN_DFFPCWORKPHONELASTFOURDIGITS = 
        "SERVICEREQUESTCONTACT.WORKPHONELASTFOURDIGITS";
    public final static String COLUMN_DFFPCWORKPHONELASTFOURDIGITS = "WORKPHONELASTFOURDIGITS";
    
    public final static String QUALIFIED_COLUMN_DFFPCWORKPHONEEXTENSION = 
        "SERVICEREQUESTCONTACT.WORKEXTENSION";
    public final static String COLUMN_DFFPCWORKPHONEEXTENSION = "WORKEXTENSION";

    public final static String QUALIFIED_COLUMN_DFFPCCELLPHONEAREACODE = 
        "SERVICEREQUESTCONTACT.CELLAREACODE";
    public final static String COLUMN_DFFPCCELLPHONEAREACODE = "CELLAREACODE";
    
    public final static String QUALIFIED_COLUMN_DFFPCCELLPHONEFIRSTTHREEDIGITS = 
        "SERVICEREQUESTCONTACT.CELLPHONEFIRSTTHREEDIGITS";
    public final static String COLUMN_DFFPCCELLPHONEFIRSTTHREEDIGITS = "CELLPHONEFIRSTTHREEDIGITS";

    public final static String QUALIFIED_COLUMN_DFFPCCELLPHONELASTFOURDIGITS = 
        "SERVICEREQUESTCONTACT.CELLPHONELASTFOURDIGITS";
    public final static String COLUMN_DFFPCCELLPHONELASTFOURDIGITS = "CELLPHONELASTFOURDIGITS";

    public final static String QUALIFIED_COLUMN_DFFPCHOMEPHONEAREACODE = 
        "SERVICEREQUESTCONTACT.HOMEAREACODE";
    public final static String COLUMN_DFFPCHOMEPHONEAREACODE = "HOMEAREACODE";
    
    public final static String QUALIFIED_COLUMN_DFFPCHOMEPHONEFIRSTTHREEDIGITS = 
        "SERVICEREQUESTCONTACT.HOMEPHONEFIRSTTHREEDIGITS";
    public final static String COLUMN_DFFPCHOMEPHONEFIRSTTHREEDIGITS  = "HOMEPHONEFIRSTTHREEDIGITS";
    
    public final static String QUALIFIED_COLUMN_DFFPCHOMEPHONELASTFOURDIGITS = 
        "SERVICEREQUESTCONTACT.HOMEPHONELASTFOURDIGITS";
    public final static String COLUMN_DFFPCHOMEPHONELASTFOURDIGITS  = "HOMEPHONELASTFOURDIGITS";

    public final static String QUALIFIED_COLUMN_DFFPCCOMMENTS = "SERVICEREQUESTCONTACT.COMMENTS";
    public final static String COLUMN_DFFPCCOMMENTS = "COMMENTS";
    
    public final static String QUALIFIED_COLUMN_DFFPCREQUESTMESSAGE = "REQUEST.STATUSMESSAGE";
    public final static String COLUMN_DFFPCREQUESTMESSAGE = "STATUSMESSAGE";
    
    public final static String QUALIFIED_COLUMN_DFFPCPROVIDERREFNO = "SERVICEREQUEST.SERVICEPROVIDERREFNO";
    public final static String COLUMN_DFFPCPROVIDERREFNO = "SERVICEPROVIDERREFNO";


    // build the relationship between the column definition and the bound field
    // name, which are defined in the interface.
    static {
        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFFPCDEALID,
                    COLUMN_DFFPCDEALID,
                    QUALIFIED_COLUMN_DFFPCDEALID,
                    BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
            FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFFPCCOPYID,
                    COLUMN_DFFPCCOPYID,
                    QUALIFIED_COLUMN_DFFPCCOPYID,
                    BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
            FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFFPCCONTACTID,
                    COLUMN_DFFPCCONTACTID,
                    QUALIFIED_COLUMN_DFFPCCONTACTID,
                    BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
			FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFFPCBORROWERID,
                    COLUMN_DFFPCBORROWERID,
                    QUALIFIED_COLUMN_DFFPCBORROWERID,
                    BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
            FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFFPCPROVIDERID,
                    COLUMN_DFFPCPROVIDERID,
                    QUALIFIED_COLUMN_DFFPCPROVIDERID,
                    BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );

            FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFFPCPRODUCTID,
                    COLUMN_DFFPCPRODUCTID,
                    QUALIFIED_COLUMN_DFFPCPRODUCTID,
                    BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCREQUESTDATE,
                        COLUMN_DFFPCREQUESTDATE,
                        QUALIFIED_COLUMN_DFFPCREQUESTDATE,
                        Timestamp.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCCONTACTFIRSTNAME,
                        COLUMN_DFFPCCONTACTFIRSTNAME,
                        QUALIFIED_COLUMN_DFFPCCONTACTFIRSTNAME,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCCONTACTLASTNAME,
                        COLUMN_DFFPCCONTACTLASTNAME,
                        QUALIFIED_COLUMN_DFFPCCONTACTLASTNAME,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );

            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCEMAIL,
                        COLUMN_DFFPCEMAIL,
                        QUALIFIED_COLUMN_DFFPCEMAIL,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCWORKPHONEAREACODE,
                        COLUMN_DFFPCWORKPHONEAREACODE,
                        QUALIFIED_COLUMN_DFFPCWORKPHONEAREACODE,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCWORKPHONEFIRSTTHREEDIGITS,
                        COLUMN_DFFPCWORKPHONEFIRSTTHREEDIGITS,
                        QUALIFIED_COLUMN_DFFPCWORKPHONEFIRSTTHREEDIGITS,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );

            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCWORKPHONELASTFOURDIGITS,
                        COLUMN_DFFPCWORKPHONELASTFOURDIGITS,
                        QUALIFIED_COLUMN_DFFPCWORKPHONELASTFOURDIGITS,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCWORKPHONENUMEXTENSION,
                        COLUMN_DFFPCWORKPHONEEXTENSION,
                        QUALIFIED_COLUMN_DFFPCWORKPHONEEXTENSION,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );


            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCHOMEPHONEAREACODE,
                        COLUMN_DFFPCHOMEPHONEAREACODE,
                        QUALIFIED_COLUMN_DFFPCHOMEPHONEAREACODE,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCHOMEPHONEFIRSTTHREEDIGITS,
                        COLUMN_DFFPCHOMEPHONEFIRSTTHREEDIGITS,
                        QUALIFIED_COLUMN_DFFPCHOMEPHONEFIRSTTHREEDIGITS,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCHOMEPHONELASTFOURDIGITS,
                        COLUMN_DFFPCHOMEPHONELASTFOURDIGITS,
                        QUALIFIED_COLUMN_DFFPCHOMEPHONELASTFOURDIGITS,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCCELLPHONEAREACODE,
                        COLUMN_DFFPCCELLPHONEAREACODE,
                        QUALIFIED_COLUMN_DFFPCCELLPHONEAREACODE,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCCELLPHONEFIRSTTHREEDIGITS,
                        COLUMN_DFFPCCELLPHONEFIRSTTHREEDIGITS,
                        QUALIFIED_COLUMN_DFFPCCELLPHONEFIRSTTHREEDIGITS,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCCELLPHONELASTFOURDIGITS,
                        COLUMN_DFFPCCELLPHONELASTFOURDIGITS,
                        QUALIFIED_COLUMN_DFFPCCELLPHONELASTFOURDIGITS,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCCOMMENTS,
                        COLUMN_DFFPCCOMMENTS,
                        QUALIFIED_COLUMN_DFFPCCOMMENTS,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCBORROWERID,
                        COLUMN_DFFPCBORROWERID,
                        QUALIFIED_COLUMN_DFFPCBORROWERID,
                        BigDecimal.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCREQUESTSTATUS,
                        COLUMN_DFFPCREQUESTSTATUS,
                        QUALIFIED_COLUMN_DFFPCREQUESTSTATUS,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );         
			 FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCREQUESTSTATUSID,
                        COLUMN_DFFPCREQUESTSTATUSID,
                        QUALIFIED_COLUMN_DFFPCREQUESTSTATUSID,
                        BigDecimal.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                ); 
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCREQUESTMESSAGE,
                        COLUMN_DFFPCREQUESTMESSAGE,
                        QUALIFIED_COLUMN_DFFPCREQUESTMESSAGE,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCPROVIDERREFNO,
                        COLUMN_DFFPCPROVIDERREFNO,
                        QUALIFIED_COLUMN_DFFPCPROVIDERREFNO,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFFPCREQUESTID,
                        COLUMN_DFFPCREQUESTID,
                        QUALIFIED_COLUMN_DFFPCREQUESTID,
                        BigDecimal.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            
    }
    
    /**
     * Constructor function
     */
    public doFixedPriceClosingInfoModelImpl() {

        super();
        setDataSourceName(DATA_SOURCE_NAME);
        setSelectSQLTemplate(SELECT_SQL_TEMPLATE);
        setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);
        setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);
        setFieldSchema(FIELD_SCHEMA);
        initialize();
    }

    /**
     * initializing.  do nothing for now.
     */
    public void initialize() {}

    /**
     * before execute the SQL.
     */
    protected String beforeExecute(ModelExecutionContext context,
                                   int queryType, String sql)
        throws ModelControlException {

        SysLog.trace(this.getClass(), "SQL ********** before Execute: " + sql);
        
        if (_log.isDebugEnabled())
            _log.debug("SQL before Execute: " + sql);
        return sql;
    }

    /**
     * after execute, we need change some language sensitive value for the
     * result.
     */
    protected void afterExecute(ModelExecutionContext context,
                                int queryType)
        throws ModelControlException {

        // try to get language id from the session state model.
        String defaultInstanceStateName =
            getRequestContext().getModelManager().
            getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl sessionState = (SessionStateModelImpl)
            getRequestContext().getModelManager().
            getModel(SessionStateModel.class,
                     defaultInstanceStateName, true);
        
        int languageId = sessionState.getLanguageId();
        int institutionId = sessionState.getDealInstitutionId();
        _log.info("in doFPCmodel size " + getSize());
        

        // get the status from the BXResources and set to the field.
        if(getSize()>0)
        {
            setDfFPCRequestStatus(
                    BXResources.
                        getPickListDescription(institutionId, "REQUESTSTATUS",
                                    getDfFPCRequestStatus(),
                                        languageId));
            
            BigDecimal statusId = getDfFPCRequestStatusId();
            
            BigDecimal zero = new java.math.BigDecimal(String.valueOf(0));
            // check the request's status to see what we need.
            switch (statusId.intValue()) {
                case ServiceConst.REQUEST_STATUS_NOT_SUBMITTED:
                case ServiceConst.REQUEST_STATUS_SYSTEM_ERROR:
                case ServiceConst.REQUEST_STATUS_PROCESSING_ERROR:
                {   break;
                }
                default:
                {   setDfFPCProviderId(zero);
                    setDfFPCProductId(zero);
                    setDfFPCWorkPhoneAreaCode(null);
                    setDfFPCWorkPhoneFirstThreeDigits(null);
                    setDfFPCWorkPhoneLastFourDigits(null);
                    setDfFPCWorkPhoneNumExtension(null);
                    setDfFPCEmail(null);
                    setDfFPCHomePhoneAreaCode(null);
                    setDfFPCHomePhoneFirstThreeDigits(null);
                    setDfFPCHomePhoneLastFourDigits(null);
                    setDfFPCCellPhoneAreaCode(null);
                    setDfFPCCellPhoneFirstThreeDigits(null);
                    setDfFPCCellPhoneLastFourDigits(null);
                    setDfFPCContactFirstName(null);
                    setDfFPCContactLastName(null);
                    setDfFPCRequestDate(null);
                    setDfFPCRequestStatus(null);
                    setDfFPCComments(null);
                    break;
                }
            }
        }
        _log.info("in doFPCmodel size " + getSize());
    }
    
    public BigDecimal getDfFPCProviderId() {
        return (BigDecimal) getValue(FIELD_DFFPCPROVIDERID);
    }

    public void setDfFPCProviderId(BigDecimal id) {
        setValue(FIELD_DFFPCPROVIDERID, id);
    }

    public BigDecimal getDfFPCProductId() {
        return (BigDecimal) getValue(FIELD_DFFPCPRODUCTID);
    }

    public void setDfFPCProductId(BigDecimal id) {
        setValue(FIELD_DFFPCPRODUCTID, id);
    }


    public String getDfFPCRequestStatus() {
        return (String) getValue(FIELD_DFFPCREQUESTSTATUS);
    }
    
    public void setDfFPCRequestStatus (String dfFPCRequestStatus) {
        setValue(FIELD_DFFPCREQUESTSTATUS, dfFPCRequestStatus);
    }
    

	public BigDecimal getDfFPCRequestStatusId() {
        return (BigDecimal) getValue(FIELD_DFFPCREQUESTSTATUSID);
    }
    
    public void setDfFPCRequestStatusId (BigDecimal dfFPCRequestStatusId) {
        setValue(FIELD_DFFPCREQUESTSTATUSID, dfFPCRequestStatusId);
    }

    public Timestamp getDfFPCRequestDate() {
        return (Timestamp) getValue(FIELD_DFFPCREQUESTDATE);
    }
    
    public void setDfFPCRequestDate(Timestamp date) {
        setValue(FIELD_DFFPCREQUESTDATE, date);
    }
    
    public String getDfFPCContactFirstName() {
        return (String) getValue(FIELD_DFFPCCONTACTFIRSTNAME);
    }
    
    public void setDfFPCContactFirstName(String name) {
        setValue(FIELD_DFFPCCONTACTFIRSTNAME, name);
    }
    
    public String getDfFPCContactLastName() {
        return (String) getValue(FIELD_DFFPCCONTACTLASTNAME);
    }
    
    public void setDfFPCContactLastName(String name) {
        setValue(FIELD_DFFPCCONTACTLASTNAME, name);
    }
    
    public String getDfFPCEmail() {
        return (String) getValue(FIELD_DFFPCEMAIL);
    }

    public void setDfFPCEmail(String email) {
        setValue(FIELD_DFFPCEMAIL, email);
    }

    public String getDfFPCWorkPhoneAreaCode() {
        return (String) getValue(FIELD_DFFPCWORKPHONEAREACODE);
    }
    
    public void setDfFPCWorkPhoneAreaCode(String areaCode) {
        setValue(FIELD_DFFPCWORKPHONEAREACODE, areaCode);
    }
        
    public String getDfFPCWorkPhoneFirstThreeDigits() {
        return (String) getValue(FIELD_DFFPCWORKPHONEFIRSTTHREEDIGITS);
    }
    
    public void setDfFPCWorkPhoneFirstThreeDigits(String phoneNumber) {
        setValue(FIELD_DFFPCWORKPHONEFIRSTTHREEDIGITS, phoneNumber);
    }

    public String getDfFPCWorkPhoneLastFourDigits() {
        return (String) getValue(FIELD_DFFPCWORKPHONELASTFOURDIGITS);
    }
    
    public void setDfFPCWorkPhoneLastFourDigits(String phoneNumber) {
        setValue(FIELD_DFFPCWORKPHONELASTFOURDIGITS, phoneNumber);
    }


    public String getDfFPCWorkPhoneNumExtension() {
        return (String) getValue(FIELD_DFFPCWORKPHONENUMEXTENSION);
    }
    
    public void setDfFPCWorkPhoneNumExtension(String extension) {
        setValue(FIELD_DFFPCWORKPHONENUMEXTENSION, extension);
    }

    public String getDfFPCCellPhoneAreaCode() {
        return (String) getValue(FIELD_DFFPCCELLPHONEAREACODE);
    }
    
    public void setDfFPCCellPhoneAreaCode(String areaCode) {
        setValue(FIELD_DFFPCCELLPHONEAREACODE, areaCode);
    }
        
    public String getDfFPCCellPhoneFirstThreeDigits() {
        return (String) getValue(FIELD_DFFPCCELLPHONEFIRSTTHREEDIGITS);
    }
    
    public void setDfFPCCellPhoneFirstThreeDigits(String phoneNumber) {
        setValue(FIELD_DFFPCCELLPHONEFIRSTTHREEDIGITS, phoneNumber);
    }

    public String getDfFPCCellPhoneLastFourDigits() {
        return (String) getValue(FIELD_DFFPCCELLPHONELASTFOURDIGITS);
    }
    
    public void setDfFPCCellPhoneLastFourDigits(String phoneNumber) {
        setValue(FIELD_DFFPCCELLPHONELASTFOURDIGITS, phoneNumber);
    }

    
    public String getDfFPCHomePhoneAreaCode() {
        return (String) getValue(FIELD_DFFPCHOMEPHONEAREACODE);
    }
    
    public void setDfFPCHomePhoneAreaCode(String areaCode) {
        setValue(FIELD_DFFPCHOMEPHONEAREACODE, areaCode);
    }
        
    public String getDfFPCHomePhoneFirstThreeDigits() {
        return (String) getValue(FIELD_DFFPCHOMEPHONEFIRSTTHREEDIGITS);
    }
    
    public void setDfFPCHomePhoneFirstThreeDigits(String phoneNumber) {
        setValue(FIELD_DFFPCHOMEPHONEFIRSTTHREEDIGITS, phoneNumber);
    }

    public String getDfFPCHomePhoneLastFourDigits() {
        return (String) getValue(FIELD_DFFPCHOMEPHONELASTFOURDIGITS);
    }
    
    public void setDfFPCHomePhoneLastFourDigits(String phoneNumber) {
        setValue(FIELD_DFFPCHOMEPHONELASTFOURDIGITS, phoneNumber);
    }
    
    public String getDfFPCComments() {
        return (String) getValue(FIELD_DFFPCCOMMENTS);
    }
    
    public void setDfFPCComments(String dfFPCComments) {
        setValue(FIELD_DFFPCCOMMENTS, dfFPCComments);
    }
    
    public String getDfFPCRequestMessage() {
        return (String) getValue(FIELD_DFFPCREQUESTMESSAGE);
    }
    
    public void setDfFPCRequestMessage(String dfFPCRequestMessage) {
        setValue(FIELD_DFFPCREQUESTMESSAGE, dfFPCRequestMessage);
    }
    
    
    public BigDecimal getDfFPCRequestId() {
        return (BigDecimal) getValue(FIELD_DFFPCREQUESTID);
    }
    
    public void setDfFPCRequestId(BigDecimal id) {
        setValue(FIELD_DFFPCREQUESTID, id);
    }
    
    public BigDecimal getDfFPCCopyId() {
        return (BigDecimal) getValue(FIELD_DFFPCCOPYID);
    }
    
    public void setDfFPCCopyId(BigDecimal id) {
        setValue(FIELD_DFFPCCOPYID, id);
    }
    
    public BigDecimal getDfFPCContactId() {
        return (BigDecimal) getValue(FIELD_DFFPCCONTACTID);
    }
    
    public void setDfFPCContactId(BigDecimal id) {
        setValue(FIELD_DFFPCCONTACTID, id);
    }

    public BigDecimal getDfFPCBorrowerId() {
        return (BigDecimal) getValue(FIELD_DFFPCBORROWERID);
    }

	public void setDfFPCBorrowerId(BigDecimal id) {
        setValue(FIELD_DFFPCBORROWERID, id);
    }
}
