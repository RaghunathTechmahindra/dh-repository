package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doDealGetReferenceInfoModelImpl extends QueryModelBase
	implements doDealGetReferenceInfoModel
{
	/**
	 *
	 *
	 */
	public doDealGetReferenceInfoModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfReferenceDealNumber()
	{
		return (String)getValue(FIELD_DFREFERENCEDEALNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfReferenceDealNumber(String value)
	{
		setValue(FIELD_DFREFERENCEDEALNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfReferenceDealTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFREFERENCEDEALTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfReferenceDealTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFREFERENCEDEALTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfRefDealTypeDescription()
	{
		return (String)getValue(FIELD_DFREFDEALTYPEDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfRefDealTypeDescription(String value)
	{
		setValue(FIELD_DFREFDEALTYPEDESCRIPTION,value);
	}

	/**
	 * MetaData object test method to verify the SQL_TEMPLATE query.
	 *
	 */
   /**
        public void setResultSet(ResultSet value)
        {
            logger = SysLog.getSysLogger("METADATA");

            try
            {
                super.setResultSet(value);
                if( value != null)
                {
                    ResultSetMetaData metaData = value.getMetaData();
                    int columnCount = metaData.getColumnCount();
                    logger.debug("===============================================");
                    logger.debug("Testing MetaData Object for new synthetic fields for" +
	                 	            " doDealGetReferenceInfoModel");
                    logger.debug("NumberColumns: " + columnCount);
                    for(int i = 0; i < columnCount ; i++)
                    {
                        logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
                    }
                    logger.debug("================================================");
                  }
              }
              catch (SQLException e)
              {
                  logger.debug("Class: " + getClass().getName());
                  logger.debug("SQLException: " + e);
              }
        }
      **/
	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE
        ="SELECT distinct DEAL.DEALID, DEAL.COPYID, DEAL.REFERENCEDEALNUMBER, " +
                "REFERENCEDEALTYPE.REFERENCEDEALTYPEID, REFERENCEDEALTYPE.RDTDESCRIPTION " +
                "FROM DEAL, REFERENCEDEALTYPE  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="DEAL, REFERENCEDEALTYPE";
	public static final String STATIC_WHERE_CRITERIA
        =" (DEAL.REFERENCEDEALTYPEID  =  REFERENCEDEALTYPE.REFERENCEDEALTYPEID)" ;
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFREFERENCEDEALNUMBER="DEAL.REFERENCEDEALNUMBER";
	public static final String COLUMN_DFREFERENCEDEALNUMBER="REFERENCEDEALNUMBER";
	public static final String QUALIFIED_COLUMN_DFREFERENCEDEALTYPEID="REFERENCEDEALTYPE.REFERENCEDEALTYPEID";
	public static final String COLUMN_DFREFERENCEDEALTYPEID="REFERENCEDEALTYPEID";
	public static final String QUALIFIED_COLUMN_DFREFDEALTYPEDESCRIPTION="REFERENCEDEALTYPE.RDTDESCRIPTION";
	public static final String COLUMN_DFREFDEALTYPEDESCRIPTION="RDTDESCRIPTION";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFERENCEDEALNUMBER,
				COLUMN_DFREFERENCEDEALNUMBER,
				QUALIFIED_COLUMN_DFREFERENCEDEALNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFERENCEDEALTYPEID,
				COLUMN_DFREFERENCEDEALTYPEID,
				QUALIFIED_COLUMN_DFREFERENCEDEALTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFDEALTYPEDESCRIPTION,
				COLUMN_DFREFDEALTYPEDESCRIPTION,
				QUALIFIED_COLUMN_DFREFDEALTYPEDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

