package mosApp.MosSystem;

/**
 * 18/Oct/2006 DVG #DG526 #4916  DJ 3.1 - Apostrophes appear as "accent graves" when entered in a note
 * 17/Jan/2006 DVG #DG378 #2586  E2E TD - Error for deal #3374
         - credit report had '<','>' chars ... changed convert function to escape them
 */

import java.util.StringTokenizer;
import java.util.Vector;
import javax.servlet.http.HttpSession;

import MosSystem.Sc;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.EntityBuilder;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.security.ACM;
import com.basis100.deal.security.AccessResult;
import com.basis100.deal.security.ActiveMessage;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.security.DealEditControl;
import com.basis100.deal.security.PDC;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.util.BusinessCalendar;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.entity.Page;
import com.filogix.express.web.util.StringHelper;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.ViewBeanBase;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.event.TiledViewRequestInvocationEvent;


//// Original (iMT) inheritance.
////public class MosWebHandlerCommon extends Object
////    implements Sc, Cloneable

//// Highly discussable implementation with the XXXXHandler extension of the
//// ViewBeanBase JATO class. The sequence diagrams must be verified. On the other hand,
//// This implementation allows to use the basic JATO framework elements such as
//// RequestContext() class. Theoretically, it could allow to override the JATO/iMT
//// auto-migration. Temporarily these fragments are commented out now for the compability
//// with the stand alone MWHC<--PHC<--XXXHandlerName Utility hierarchy.
//// getModel(ClassName.class), etc., could be also rewritten in the Utility hierarchy
//// using this inheritance.

public class MosWebHandlerCommon extends ViewBeanBase
    implements Sc, Cloneable, View, ViewBean
{
    ////public SessionState theSession;
    public MosWebHandlerCommon()
    {
        super("pgIWorkQueueViewBean");
    ////super();
    }

    public MosWebHandlerCommon(String name)
    {
        this(null,name); // No parent
    }

    /**
     *
     *
     */
    protected MosWebHandlerCommon(View parent, String name)
    {
        super(parent,name);
    }

  //I don't think it's necessary to store HTTPSession, may remove it in the future
  //--> By BILLY 03July2002
  public HttpSession theSession;

  //It's not necessary to init the theSessionState here, it will be done on PreHandlerProtocol
  //--> Changed by BILLY 03July2002
    public SessionStateModelImpl theSessionState;

    protected SessionResourceKit srk;
    public SysLogger logger;

    /**
     *
     *
     */
    public Object cloneSafeShallow()
    {
        try
        {
            return clone();
        }
        catch(Exception e)
        {
            // won't fail ... but
            return null;
        }

    }


    /**
     *
     *
     */
    protected Object clone()
        throws CloneNotSupportedException
    {
        return super.clone();

    }


    /**
     *
     * Chane this later.
     */
    protected static String getSchemaName()
    {
        return "";

    }


    /**
     *
     * No need anymore. Should be deleted.
     */
    ////protected static String getDatasourceName()
    ////{
    ////    return "orcl";
        ////
    ////}


    /**
     *
     *
     */
    public SysLogger getSysLogger()
    {
        return logger;

    }


    /**
     *
     *
     */
    public ActiveMessage setStandardFailMessage()
    {
        return setActiveMessageToAlert(
      BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
      ActiveMsgFactory.ISCUSTOMCONFIRM);

    }


    /**
     *
     *
     */
    public ActiveMessage setStandardViewOnlyMessage()
    {
        return setActiveMessageToAlert(
      BXResources.getSysMsg("DISALLOWED_DURING_VIEW_ONLY_MODE", theSessionState.getLanguageId()),
      ActiveMsgFactory.ISCUSTOMCONFIRM);

    }


    /**
     *
     *
     */
    public ActiveMessage setActiveMessageToAlert(int msgType)
    {
        return setActiveMessageToAlert(null, null, msgType, null);

    }


    /**
     *
     *
     */
    public ActiveMessage setActiveMessageToAlert(String msgtext, int msgType)
    {
        return setActiveMessageToAlert(null, msgtext, msgType, null, null);

    }


    /**
     *
     *
     */
    public ActiveMessage setActiveMessageToAlert(String msgtext, int msgType, String responseIdentifier)
    {
        return setActiveMessageToAlert(null, msgtext, msgType, responseIdentifier, null);

    }


    /**
     *
     *
     */
    public ActiveMessage setActiveMessageToAlert(String msgtext, int msgType, String responseIdentifier1, String responseIdentifier2)
    {
        return setActiveMessageToAlert(null, msgtext, msgType, responseIdentifier1, responseIdentifier2);

    }


    /**
     *
     *
     */
    public ActiveMessage setActiveMessageToAlert(String tittle, String msgtext, int msgType, String responseIdentifier)
    {
        return setActiveMessageToAlert(tittle, msgtext, msgType, responseIdentifier, null);

    }


    /**
     *
     *
     */
    public ActiveMessage setActiveMessageToAlert(String tittle, String msgtext, int msgType, String responseIdentifier1, String responseIdentifier2)
    {
    PageEntry pe = (PageEntry) theSessionState.getCurrentPage();
    int languageId = theSessionState.getLanguageId();
	int institutionId = theSessionState.getDealInstitutionId();
	if (institutionId < 0)
		institutionId = theSessionState.getUserInstitutionId();
    /*
    if (pe != null)
    {
      logger.debug("MWHC@setActiveMessageToAlert:PageEntry name: " + pe.getPageName());
      logger.debug("MWHC@setActiveMessageToAlert:PageDealId: " + pe.getPageDealId());
      logger.debug("MWHC@setActiveMessageToAlert:PageLabel: " + pe.getPageLabel());
      logger.debug("MWHC@setActiveMessageToAlert:MessageType: " + msgType);
    }
    */
    // assume the generic replacement token in a message ("%") to be replaced by deal number
    ////ActiveMessage am = ActiveMsgFactory.getActiveMessage(msgType, "" + getTheSession().getCurrentPage().getPageDealId(), getTheSession().getCurrentPage().getPageLabel());
    //--Release2.1--//
    //Modified to pass LanguageId for Multilingual handling
    //--> By Billy 12Nov2002
    int pageId = pe.getPageId();
    String pageLabel = BXResources.getPickListDescription(institutionId, "PAGELABEL", pageId, languageId);  
    ActiveMessage am = ActiveMsgFactory.getActiveMessage(msgType, "" + pe.getPageDealId(), pageLabel, languageId);

    logger.debug("MWHC@setActiveMessageToAlert:ActiveMessage: " + am);

    // set message (and title) for custom types
    if (msgType == ActiveMsgFactory.ISCUSTOMCONFIRM ||
    msgType == ActiveMsgFactory.ISCUSTOMDIALOG ||
    msgType == ActiveMsgFactory.ISCUSTOMDIALOG2 ||
    msgType == ActiveMsgFactory.ISCUSTOMDIALOG_MI_REQUEST ||
    msgType == ActiveMsgFactory.ISCUSTOMFATAL ||
    msgType == ActiveMsgFactory.ISCUSTOMLEAVEPAGE ||
    msgType == ActiveMsgFactory.ISCONFIRMENDSESSIONDIALOG ||
    msgType == ActiveMsgFactory.ISYESNO )                                              // Catherine, 26-Apr-05, GECF, new dialog screen
    {
       logger.debug("MWHC@setActiveMessageToAlert:I am from customConfirm");
       // Catherine, temp debug >>>>>> DELETE AFTER TEST !!!
       logger.debug("MWHC@msgType = " + msgType);
       // Catherine, temp debug >>>>>> DELETE AFTER TEST !!!
       logger.debug("MWHC@setActiveMessageToAlert:Title: " + tittle);
       logger.debug("MWHC@setActiveMessageToAlert:msgText: " + msgtext);

       am.setTitle(tittle);
       am.setDialogMsg(msgtext);
    }

    if (msgType == ActiveMsgFactory.ISCUSTOMDIALOG ||
    msgType == ActiveMsgFactory.ISCUSTOMDIALOG2 ||
    msgType == ActiveMsgFactory.ISCUSTOMDIALOG_MI_REQUEST ||
    msgType == ActiveMsgFactory.ISCUSTOMFATAL ||
    msgType == ActiveMsgFactory.ISCUSTOMLEAVEPAGE ||
    msgType == ActiveMsgFactory.ISYESNO )                                              // Catherine, 26-Apr-05, GECF, new dialog screen
    {
       logger.debug("MWHC@setActiveMessageToAlert:I am from customDialog");
       logger.debug("MWHC@setActiveMessageToAlert:responseIdentifier1: " + responseIdentifier1);
       logger.debug("MWHC@setActiveMessageToAlert:responseIdentifier2: " + responseIdentifier2);

       am.setResponseIdentifier(responseIdentifier1);
       am.setResponseIdentifier2(responseIdentifier2);
    }
    if (msgType == ActiveMsgFactory.ISNODIALOG)
    {
     am.setNoDialog(true);
     // do nothing ... The dialog screen should not be generated in this mode.
    }

    return setActiveMessageToAlert(am);

 }


    /**
     *
     *
     */
    public ActiveMessage setActiveMessageToAlert(ActiveMessage am)
    {
        ////theSession.setActMessage(am);
        theSessionState.setActMessage(am);

    return am;

    }


    /**
     *
     *
     */
    public void showDialogMessage()
    {
    //logger = SysLog.getSysLogger("MWHC");
    //Calls Here bcose No Dialog message found so check for
        //Active message
        showDialogMessage(null);
    //logger.debug("MWHC@showDialogMessage::showDialogMessage():: Invoked");

    }


    /**
     * !!IMPORTANT!! This part should be finished.
         *
         * Currently the implementation is done only partly.
     *
     */
    public void showDialogMessage(ViewBean thePage)
    {
    //logger = SysLog.getSysLogger("MWHC");

    //logger.debug("MWHC@showDialogMessage::I am starting");

        if (thePage == null)
        {
            ////if (theSession == null) return; // should never happen, but...
            ////thePage = CSpider.getPage(theSession.getCurrentPage().getPageName());
            ///thePage.getSession().getCurrentPage();

            if (theSessionState == null) return; // should never happen, but...
            //// wrong auto-conversion.
      ////ViewBean thePage = getViewBean(pgSignOnViewBean.class);
            ////getViewBean($1ViewBean.class(....

      PageEntry pe = (PageEntry) theSessionState.getCurrentPage();

      //logger.debug("MWHC@showDialogMessage::PageName: " + pe.getPageName());

      //// This obtaining should be moved to thePage creation/call place.
      ////ViewBean thePage = RequestManager.getRequestContext().getViewBeanManager().getLocalViewBean(pe.getPageName());

      //// Get the pgSignOnViewBean directly.
      //// ViewBean pgSignon = getViewBean(pgSignOnViewBean.class);
      //// Forwarding must be done later from the showActiveMessage/showPassiveMessage.
      //// pgSignon.forwardTo(getRequestContext());

      String targetViewBeanName = pe.getPageName();

      //logger.debug("MWHC@showDialogMessage::Before ViewBean call: " + targetViewBeanName);

      try
      {
        ////targetView = RequestManager.getRequestContext().getViewBeanManager().getLocalViewBean(targetViewBeanName);
        thePage = RequestManager.getRequestContext().getViewBeanManager().getLocalViewBean(targetViewBeanName);
        //logger.debug("MWHC@showDialogMessage::targetView: " + targetView);
      }
      catch(ClassNotFoundException cnfe)
      {
        logger.debug("MWHC@showDialogMessage::ClassNotFoundException " + cnfe);
      }

      //// REDIRECTION IS DONE VIA THE POSTHANDLER METHOD.
      ////targetView.forwardTo(RequestManager.getRequestContext());

        }

    logger.debug("MWHC@showDialogMessage::thePageToShowActiveMessage(" + thePage + "):: Invoked");

        showActiveMessage(thePage);
    //logger.debug("MWHC@showDialogMessage:: afterShowActiveMessage");

        showPassiveMessage(thePage);
    //logger.debug("MWHC@showDialogMessage:: afterShowPassiveMessage");
    }


    /**
     *
     *
     */
    public void showActiveMessage(ViewBean thePage)
    {
    //logger = SysLog.getSysLogger("MWHC");

    if (thePage == null) return;

        ActiveMessage am = null;

        int pageId = 0;

     //// PageEntry pe = theSessionState.getCurrentPage();
     PageEntry pe = (PageEntry) theSessionState.getCurrentPage();

     //logger.debug("MWHC@PageName from the showActiveMessage: " + pe.getPageName());
     //logger.debug("MWHC@PageId from the showActiveMessage: " + pe.getPageId());

        try
        {
            pageId = pe.getPageId();
        }
        catch(Exception e)
        {
            pageId = 6;
        }

        // signOn's page id
        if (theSessionState != null &&(theSessionState.isActMessageSet() == true))
    {
            am = theSessionState.getActMessage();
      //logger.debug("MWHC@PageName from the showActiveMessage:AM_exists: " + am);
    }
        else
    {
            am = new ActiveMessage();
      //logger.debug("MWHC@PageName from the showActiveMessage:AM_new: " + am);
    }

        if (am == null) return;

    //logger.debug("MWHC@PageName from the showActiveMessage:ErrorFlag: " + am.getGenerate());
        thePage.setDisplayFieldValue("stErrorFlag", new String(yesNo(am.getGenerate())));


        thePage.setDisplayFieldValue("stAmGenerate", new String(yesNo(am.getGenerate())));

        thePage.setDisplayFieldValue("stAmHasTitle", new String(yesNo(am.getTitle())));

        //logger.debug("MWHC@PageName from the showActiveMessage:Title: " + am.getTitle());
    thePage.setDisplayFieldValue("stAmTitle", new String("" + am.getTitle()));

        thePage.setDisplayFieldValue("stAmHasInfo", new String(yesNo(am.getInfoMsg())));

        //logger.debug("MWHC@PageName from the showActiveMessage:InfoMessage: " + am.getInfoMsg());
    thePage.setDisplayFieldValue("stAmInfoMsg", new String("" + am.getInfoMsg()));

        thePage.setDisplayFieldValue("stAmDialogMsg", new String("" + am.getDialogMsg()));

        thePage.setDisplayFieldValue("stAmHasTable", new String(yesNo(am.getNumMessages())));

        //logger.debug("MWHC@PageName from the showActiveMessage:ListOfJS: " + formCommaDelimitedListJS(am.getMsgTypes(), false));
    thePage.setDisplayFieldValue("stAmMsgTypes", new String(formCommaDelimitedListJS(am.getMsgTypes(), false) + ", 0"));

        thePage.setDisplayFieldValue("stAmMsgs", new String(formCommaDelimitedListJS(am.getMsgs(), true)));

    String buttonsHtml = am.getButtonHtml();
        logger.debug("MWHC@PageName from the showActiveMessage:ButtonHtml: " + buttonsHtml);

    logger.debug("MWHC@PageName from the showActiveMessage:ResponseIdentifier: " + am.getResponseIdentifier());
    logger.debug("MWHC@PageName from the showActiveMessage:ResponseIdentifier2: " + am.getResponseIdentifier2());

        if (am.getResponseIdentifier() != null)
        {
            // ....handleActMessageOK(@) .... response identifier replaces the '@'
            String params = am.getResponseIdentifier() + "," + pageId;

            // actually <rId, pageId> not just rId
            buttonsHtml = replaceString("@", buttonsHtml, params);
        }

        if (am.getResponseIdentifier2() != null)
        {
            // ....handleActMessageOK(^) .... response identifier replaces the '^'
            String params = am.getResponseIdentifier2() + "," + pageId;

            // actually <rId, pageId> not just rId
            buttonsHtml = replaceString("^", buttonsHtml, params);
        }

      thePage.setDisplayFieldValue("stAmButtonsHtml", new String(buttonsHtml));
    }


    /**
     *
     *
     */
    public void showPassiveMessage(ViewBean thePage)
    {
        if (thePage == null) return;

        PassiveMessage pm = null;

        if (theSessionState != null &&(theSessionState.isPasMessageSet() == true)) {
            pm = theSessionState.getPasMessage();
    } else {
            pm = new PassiveMessage();
    }

        if (pm == null) return;

        thePage.setDisplayFieldValue("stPmGenerate", new String(yesNo(pm.getGenerate())));

        String title = pm.getTitle();

        if (title == null || title.length() == 0) title =
        BXResources.getSysMsg("ERRORS_AND_WARNINGS_WINDOW_TITLE", theSessionState.getLanguageId());

        if (title == null || title.length() == 0) thePage.setDisplayFieldValue("stPmHasTitle", new String("N"));
        else thePage.setDisplayFieldValue("stPmHasTitle", new String("Y"));

        thePage.setDisplayFieldValue("stPmTitle", new String(title));

        thePage.setDisplayFieldValue("stPmHasInfo", new String(yesNo(pm.getInfoMsg())));

        thePage.setDisplayFieldValue("stPmInfoMsg", new String("" + pm.getInfoMsg()));

        thePage.setDisplayFieldValue("stPmHasOk", new String(yesNo(pm.getHasOK())));

        thePage.setDisplayFieldValue("stPmOnOk", new String("0"));

        thePage.setDisplayFieldValue("stPmHasTable", new String(yesNo(pm.getNumMessages())));

        thePage.setDisplayFieldValue("stPmMsgTypes", new String(formCommaDelimitedListJS(pm.getMsgTypes(), false) + ", 0"));

    thePage.setDisplayFieldValue("stPmMsgs", new String(formCommaDelimitedListJS(pm.getMsgs(), true)));

    }


    /**
     *
     *
     */
    public String formCommaDelimitedList(Vector values)
    {
        String s = formCommaDelimitedListInternal(values, false);

        if (s == null) return "-1";

        return s;

    }


    /**
     *
     *
     */
    public String formCommaDelimitedListJS(Vector values, boolean isText)
    {
        String s = formCommaDelimitedListInternal(values, true);

        if (s == null)
        {
            if (isText) return "'0'";
            else return "0";
        }

        return s;

    }


    /**
     *
     *
     */
    private String formCommaDelimitedListInternal(Vector values, boolean forJS)
    {
        if (values == null || values.size() == 0) return null;

        StringBuffer sb = new StringBuffer();

        int lim = values.size();

        for(int i = 0;
        i < lim;
        ++ i)
        {
            Object val = values.elementAt(i);
            if (val instanceof String)
            {
                String quote = "'";

                // for script (JavaScript) check for embedded double quote - if found quote string with single quotes
                if (forJS &&((String) val).indexOf(quote) != - 1) quote = "\"";
                sb.append(quote + cleanForScript((String) val,(quote.getBytes()) [ 0 ]) + quote);
            }
            else sb.append("" + ((Integer)val).intValue());
            //// JATO transformation of the BasisXpress ((Integer)val).intValue()).
            ////else sb.append("" +((Integer)TypeConverter.asInt(val)));
            if (i != lim - 1) sb.append(",");
        }

        return sb.toString();

    }


    /**
     *
     *
     */
    private String cleanForScript(String s, byte quote)
    {
        return StringHelper.string2HTMLString(s);
    }


    /**
     *
     *
     */
    private String yesNo(boolean b)
    {
        if (b == true) return "Y";

        return "N";

    }


    /**
     *
     *
     */
    private String yesNo(String s)
    {
        if (s == null || s.length() == 0) return "N";

        return "Y";

    }


    /**
     *
     *
     */
    private String yesNo(int i)
    {
        if (i > 0) return "Y";

        return "N";

    }


    /**
     *
     *
     */
    public int getUserAccessType()
    {
        return getUserAccessType(- 1);

    }


    /**
     *
     *
     */
    public int getUserAccessType(int pageId)
    {
        int retType = 0;

        //// PageEntry pe = theSessionState.getCurrentPage();
    PageEntry pe = (PageEntry) theSessionState.getCurrentPage();

    //logger.debug("MWHC@PageName from the getUserAccessType: " + pe.getPageName());

        try
        {
            if (pageId == - 1)
                pageId = pe.getPageId();

            ACM acm = null;
            try
            {
                acm = theSessionState.getAcm();
            }
            catch(Exception e)
            {
                ////Treat it.
            }

            AccessResult arUser = acm.pageAccess(pageId, theSessionState
                    .getLanguageId(), theSessionState.getDealInstitutionId());
            retType = arUser.getAccessTypeID();
        }
        catch(Exception ex)
        {
            retType = 0;
        }

        return retType;

    }


    /**
     *
     *
     */
    public String queryPrimaryBorrowerName(int dealId, int copyId)
    {
        /****** MCM team ML Ticket merge FXP22045 ******/
        String nameStr = "";
        /****** MCM team ML Ticket merge FXP22045 ******/
        //logger.debug("--T--> @MWHC.queryPrimaryBorrowerName: start");

        if (dealId <= 0) return nameStr;

        try
        {
            // determine copy id if necessary
            if (copyId <= 0)
            {
                // determine gold copy!
                //
                // ALERT - flaw! - should be looking for the recommended scenario (if one)
                //
                //MasterDeal md = new MasterDeal(getSessionResourceKit(), null);
                //md = md.findByPrimaryKey(new MasterDealPK(dealId));
                //copyId = md.getGoldCopyId();
                copyId = new Deal(srk, null).findByRecommendedScenario(new DealPK(dealId, 0), true).getCopyId();


            }
            Borrower borr = new Borrower(getSessionResourceKit(), null);
            borr.setSilentMode(true);

            // no logging of unfound - no considered an error
            borr.findByPrimaryBorrower(dealId, copyId);
            nameStr = borr.formFullName();

            //logger.debug("--T--> @MWHC.queryPrimaryBorrowerName:DealId: " + dealId);
      //logger.debug("--T--> @MWHC.queryPrimaryBorrowerName:CopyId: " + copyId);
      //logger.debug("--T--> @MWHC.queryPrimaryBorrowerName:BorrowerName: " + nameStr);
        }
        catch(Exception e)
        {
            ;
        }

        return nameStr;

    }

    public String queryPrimaryBorrowerName(int dealId, int copyId, int institutionId)
    {
        String nameStr = "";

        //logger.debug("--T--> @MWHC.queryPrimaryBorrowerName: start");

        if (dealId <= 0) return nameStr;

        try
        {
            // determine copy id if necessary
            if (copyId <= 0)
            {
                // determine gold copy!
                //
                // ALERT - flaw! - should be looking for the recommended scenario (if one)
                //
                //MasterDeal md = new MasterDeal(getSessionResourceKit(), null);
                //md = md.findByPrimaryKey(new MasterDealPK(dealId));
                //copyId = md.getGoldCopyId();
                copyId = new Deal(srk, null).findByRecommendedScenario(new DealPK(dealId, 0), true).getCopyId();
            }
            Borrower borr = new Borrower(getSessionResourceKit(), null);
            borr.setSilentMode(true);

            // no logging of unfound - no considered an error
            borr.findByPrimaryBorrowerWithInstitutionId(dealId, copyId, institutionId);
            nameStr = borr.formFullName();

            //logger.debug("--T--> @MWHC.queryPrimaryBorrowerName:DealId: " + dealId);
      //logger.debug("--T--> @MWHC.queryPrimaryBorrowerName:CopyId: " + copyId);
      //logger.debug("--T--> @MWHC.queryPrimaryBorrowerName:BorrowerName: " + nameStr);
        }
        catch(Exception e)
        {
            ;
        }

        return nameStr;

    }

    /**
     *
     *
     */
    public String setSchemaName(String sql)
    {
        int ndx = - 1;

        while((ndx = sql.indexOf("%")) != - 1)
        {
            String start = sql.substring(0, ndx);
            String end = sql.substring(ndx + 1);
            sql = start + getSchemaName() + end;
        }

        return sql;

    }

    /**
     *
     *
     */
    public String setReplacement(String str, String replacement)
    {
        int ndx = - 1;

        while((ndx = str.indexOf("%")) != - 1)
        {
            String start = str.substring(0, ndx);
            String end = str.substring(ndx + 1);
            str = start + replacement + end;
        }

        return str;

    }


    /**
     *
     *
     */
    public SessionResourceKit getSessionResourceKit()
    {
        return srk;

    }


    /**
     *
     *
     */
    public void setSessionResourceKit(SessionResourceKit aSrk)
    {
        srk = aSrk;

    }


    /**
     *
     *
     */
    public void setupSessionResourceKit(String identifier)
    {
        srk = new SessionResourceKit(theSessionState.getExpressState(),
                                     identifier);

        //
        // set up logger
        //
        logger = srk.getSysLogger();

        int aid = 0;


        // set audit Id
        //
        //  . copy id of current transactional copy
        //  . 0 if no transactional copy
        //  . for non-deal page doesn't matter
        DealEditControl dec = theSessionState.getDec();

        if (dec != null)
        {
            aid = dec.getAID();
            if (aid > 0)
            {
                srk.setAuditId(aid);
                return;
            }
        }

        ////PageEntry currPage = theSession.getCurrentPage();
    PageEntry currPage = (PageEntry) theSessionState.getCurrentPage();

    //logger.debug("MWHC@PageName from the setupSessionResourceKit: " + currPage.getPageName());

        // if deal page then since no tx copies assume editing source (vs. tx copy)
        if (currPage != null && currPage.isDealPage())
        {
            srk.setAuditId(0);
            return;
        }


        // otherwise doesn't really matter
        srk.setAuditId(- 1);

    }

    public void unsetSessionResourceKit()
    {
		if (srk != null) {
			srk.freeResources();
			srk = null;
		}
    }


    /**
     * This method should be synchronized with the JATO framework
     * architecture. SessionState is converted to the SessionStateModel/
     * SessionStateModelImpl pair.
     *
     */
    ////public SessionState getTheSession()
    ////{
    ////    return theSession;
    ////}

    public HttpSession getTheSession()
    {
        return theSession;
    }

  public SessionStateModelImpl getTheSessionState()
    {
        return theSessionState;
    }

    /**
     * This method should be synchronized with the JATO framework
     * architecture. SessionState is converted to the SessionStateModel/
     * SessionStateModelImpl pair.
     *
     */
    public void setTheSessionState(SessionStateModelImpl theSessionState)
    {
        this.theSessionState = theSessionState;
    }

    public void setTheSession(HttpSession theSession)
    {
        this.theSession = theSession;
    }

    /**
     *
     *
     */
    public void clearMessage()
    {
        // active message - if one we don't nuke - rather toggle <display>
        // to ensure not set to generate
        ActiveMessage am = theSessionState.getActMessage();

        if (am != null) am.setGenerate(false);

        PassiveMessage pm = theSessionState.getPasMessage();

        if (pm != null) pm.setGenerate(false);

    }

    private static final String rowNumTag="^CSpCommand.currRowNumber=";
    private int rowNumTagLen=rowNumTag.length();

    /**
     *  The next a couple of methods redesigned and implemented by JATO itself.
     *  They should be replaced an appropriate substitution from the JATO framework.
     *
     */
  //--> Rewritten for JATO framework
  //--> By Billy 24July2002
    public int getRowNdxFromWebEventMethod(RequestInvocationEvent event)
    {
        try
        {
      return(((TiledViewRequestInvocationEvent)event).getTileNumber());
        }
        catch(Exception e)
        {
            logger.error("Exception @MosWenHandlerCommon.getRowNdxFromWebEventMethod : return 0 as fallback :: "
          + e.toString());
      return 0;
        }
    }

    /**
     *  The next a couple of methods redesigned and implemented by JATO itself.
     *  They should be replaced an appropriate substitution from the JATO framework.
   *  These methods currently in the place for the compilation purposes only.
     *
     */

    /**
     *  The next a couple of methods redesigned and implemented by JATO itself.
     *  They should be replaced an appropriate substitution from the JATO framework.
     *
     */
  //// Replaced to the proper BX TiledView base class.
    public int getCursorAbsoluteNdx(PageCursorInfo tds, int relativeSelection)
    {
        int restoreSelectionNdx = tds.getRelSelectedRowNdx();

        tds.setRelSelectedRowNdx(relativeSelection);

        int absNdx = tds.getAbsSelectedRowNdx();

        tds.setRelSelectedRowNdx(restoreSelectionNdx);

        return absNdx;

    }

    /**
     *
     *
     */
    public String replaceString(String token, String str, String replaceStr)
    {
        StringTokenizer parser = new StringTokenizer(str, token);

        String [ ] elements = null;

        String retStr = "";

        try
        {
            elements = new String [ parser.countTokens() ];
            int indx = 0;
            while(parser.hasMoreElements())
            {
                elements [ indx ] =(String) parser.nextElement();
                indx ++;
            }
            if (elements.length < 2) return str;
            for(int i = 0;
            i < elements.length;
            i ++)
            {
                if (i == 0)
                {
                    retStr = elements [ 0 ];
                    continue;
                }
                retStr = retStr + replaceStr + elements [ i ];
            }
        }
        catch(Exception ex)
        {
            logger.error("Exception occured @replaceString ");
            logger.error(ex);
            retStr = str;
        }

        return retStr;

    }


    /**
     *
     *
     */
    protected Page getPage(String pageLabel)
    {
        try
        {
            return(PDC.getInstance()).getPageByLabel(pageLabel);
        }
        catch(Exception e)
        {
            ;
        }

        return null;

    }


    /**
     *
     *
     */
    protected Page getPage(int pageId)
    {
        try
        {
            return(PDC.getInstance()).getPage(pageId);
        }
        catch(Exception e)
        {
            ;
        }

        return null;

    }
    private static String datePattern="yyyy-MM-dd HH:mm:ss.SSS";


    /**
     *
     *
     */
    public void convertToUserTimeZone(ViewBean currpage, String fieldname)
    {
        try
        {
            /// original CSpValue
            Object val = currpage.getDisplayFieldValue(fieldname);
            if ((val == null) ||(val.toString().trim().length() < 1)) return;
            String dtinStr = val.toString();

            // in mask <datePattern> above
            BusinessCalendar bcal = new BusinessCalendar(srk);
            ////String displayval = bcal.tzDisplay(dtinStr, getTheSession().getSessionUserTimeZone(), datePattern, datePattern);
            String displayval = bcal.tzDisplay(dtinStr, theSessionState.getSessionUserTimeZone(), datePattern, datePattern);
            ///
            /// logger.info("********** convertToUserTimeZone: IN=" + dtinStr + ", user tzId=" + getTheSession().getSessionUserTimeZone() +
            ///         ", display=" + displayval + ", df=" + fieldname);
            currpage.setDisplayFieldValue(fieldname, new String(displayval));
        }
        catch(Exception ex)
        {
            logger.error("Exception @PHC.convertToUserTimeZone - ignored (date/time in system timezone)");
            logger.error(ex);
        }
        catch(Throwable th)
        {
            logger.error("Exception (ERROR actually) @PHC.convertToUserTimeZone - ignored (date/time in system timezone)");
            logger.error(th);
        }

    }


    /**
     *
     *
     */
    ////public void handlePageNameFilter(doPageNameLabel pagename)
        public void handlePageNameFilter(doPageNameLabelModelImpl pagename)
    {
        ////if (pagename != null) pagename.setFilterStr(getPageNameFilterStr());
        if (pagename != null)
            pagename.setDfPageName(getPageNameFilterStr());
    }
    private static String phonePad="____________________";

    /**
     *
     *
     */
    public String getPhoneSearchString(String df1, String df2, String df3)
    {
        if (df1 == null) df1 = "";

        df1 = df1.trim();

        if (df2 == null) df2 = "";

        df2 = df2.trim();

        if (df3 == null) df3 = "";

        df3 = df3.trim();

        if (df3.length() > 0 && df2.length() < 3)
        {
            df2 = df2 + phonePad;
            df2 = df2.substring(0, 3);
        }

        if (df2.length() > 0 && df1.length() < 3)
        {
            df1 = df1 + phonePad;
            df1 = df1.substring(0, 3);
        }

        return df1 + df2 + df3;

    }


    /**
     *
     *
     */
    public String[] phoneSplit(String ph)
    {
        String parts [ ] = new String [ 3 ];

        if (ph == null)
    //    aaaxxxllll
        //    0123456789
        ph = "          ";

        if (ph.length() == 7) ph = "   " + ph;

        ph = ph + "                   ";

        parts [ 0 ] = ph.substring(0, 3).trim();

        parts [ 1 ] = ph.substring(3, 6).trim();

        parts [ 2 ] = ph.substring(6, 10).trim();

        return parts;

    }


    /**
     *
     *
     */
    public String phoneJoin(String pt1, String pt2, String pt3)
    {
        if (pt1 == null) pt1 = "";

        pt1 = pt1 + "        ";

        if (pt2 == null) pt2 = "";

        pt2 = pt2 + "        ";

        if (pt3 == null) pt3 = "";

        pt3 = pt3 + "        ";

        return pt1.substring(0, 3) + pt2.substring(0, 3) + pt3.substring(0, 4);

    }


    /**
     *
     *
     */
    public String formatPhoneNumber(String phStr)
    {
        if (phStr == null || phStr.trim().length() == 0) return "";

        if (phStr.length() <= 7)
        {
            phStr = "   " + phStr;
        }

        phStr = phStr + "                   ";


        // Modify format to (###)###-####
        return "(" + phStr.substring(0, 3) + ")" + phStr.substring(3, 6) + "-" + phStr.substring(6, 10);

    }


    /**
     *
     *
     */
    public String formatPhoneNumber(String phStr, String extStr)
    {
        String thePhoneNum = formatPhoneNumber(phStr);

        if (thePhoneNum.equals("")) return "";
        
        /***** FXP23050 Party Summary_Referral phone number fix start *****/
        else if(extStr.equals("")) 
            return thePhoneNum;
        /***** FXP23050 Party Summary_Referral phone number fix end *****/
        
        else
            return thePhoneNum + " x " + extStr;

    }


    /**
     *
     *
     */
    protected String getBorrTypeFilterStr(int typeid)
    {
        String retstr = DB_NAME + "." + BORROWER_TBL + "." + BR_TYPEID_COL + "=" + typeid;

        return retstr;

    }


    /**
     *
     *
     */
    protected String getDocTrackConditionFilterStr(int condid)
    {
        String retstr = DB_NAME + "." + DOCTRACK_TBL + "." + DT_CONDITIONID_COL + "=" + condid;

        return retstr;

    }


    /**
     *
     *
     */
    protected String getBorrAddressTypeFilterStr(int typeid)
    {
        String retstr = DB_NAME + "." + BR_ADDRESS_TBL + "." + BR_ADDR_TYPEID_COL + "=" + typeid;

        return retstr;

    }


    /**
     *
     *
     */
    protected String getPageNameFilterStr()
    {
        String attribname = DB_NAME + "." + PAGE + "." + PAGEID_COL;

        String retstr = attribname + "<>" + PGNM_DEAL_SEARCH_ID + " AND " + attribname + "<>" + PGNM_SIGN_ON_ID + " AND " + attribname + "<>" + PGNM_SIGNOFF_ID + " AND " + attribname + "<>" + PGNM_INDIV_WORK_QUEUE_ID + " AND " + attribname + "<>" + PGNM_CHANGE_PASSWORD_ID + " AND " + attribname + "<>" + PGNM_USER_PROFILES + " AND " + attribname + "<>" + PGNM_INTEREST_RATES_ADMIN;

        return retstr;

    }


    /**
     *
     *
     */
    public void doCalculation(CalcMonitor dcm)
        throws Exception
    {
        try
        {
            if (dcm == null) throw new Exception("Exception encountered in doCalculation - monitor not provided");
            dcm.calc();
        }
        catch(Throwable th)
        {
            String report = "Exception @doCalculation:";
            logger.error(report);
            logger.error(th);
            throw new Exception(report);
        }

    }


    /**
     *
     *
     */
    public DealEntity getEntity(String entityName, int primaryId, int copyId, CalcMonitor dcm)
        throws Exception
    {
        DealEntity de = null;

        try
        {
            de = EntityBuilder.constructEntity(entityName, getSessionResourceKit(), dcm, primaryId, copyId);
        }
        catch(Exception ex)
        {
            setStandardFailMessage();
            String report = "Exception @getEntity for Entity Name " + entityName;
            getSysLogger().error(report);
            getSysLogger().error(ex);
            throw new Exception(report);
        }
        catch(Throwable th)
        {
            setStandardFailMessage();
            String report = "Exception (Error actually!) @getEntity for Entity Name " + entityName;
            getSysLogger().error(report);
            getSysLogger().error(th);
            throw new Exception(report);
        }

        return de;

    }


    /**
     *
     *
     */
    public int[] parseTerms(int termsInMonth)
    {
        int [ ] retval = new int [ 2 ];

        java.math.BigInteger bi1 = java.math.BigInteger.valueOf(termsInMonth);

        java.math.BigInteger div = java.math.BigInteger.valueOf(12);

        java.math.BigInteger [ ] biret = bi1.divideAndRemainder(div);

        ////retval [ 0 ] = biret [ 0 ]TypeConverter.asInt();

        ////retval [ 1 ] = biret [ 1 ]TypeConverter.asInt();

    retval[0] = biret[0].intValue();
    retval[1] = biret[1].intValue();

        return retval;
    }


    /**
     *
     *
     */
    protected static String[] getMonthNames()
    {

        // ndx=0 : used to support case when 'no date entry' permitted
        String [ ] months =
        {
            " ", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"     }
        ;

        return months;

    }


    /**
     *
     *
     */
    protected String[] parseDateToTokens(java.util.Date dateVal)
    {
        if (dateVal == null) return null;

        String [ ] retVal =
        {
            "", "", "", ""      }
        ;

        String [ ] months = getMonthNames();

        if (dateVal == null) return retVal;

        retVal [ 0 ] = String.valueOf(dateVal.getMonth() + 1);

        retVal [ 1 ] = String.valueOf(months [ dateVal.getMonth() + 1 ]);

        retVal [ 2 ] = String.valueOf(dateVal.getDay());

        retVal [ 3 ] = String.valueOf(dateVal.getYear());

        return retVal;

    }


    /**
     *
     *
     */
    public String[] getParsedPhoneNo(String ano, int rowindx, String dfName)
    {
        String [ ] retval =
        {
            "", "", ""      }
        ;

        try
        {
            // detect not present
            if (ano == null || ano.trim().length() == 0) return retval;
            retval [ 0 ] = ano.substring(0, 3).trim();
            retval [ 1 ] = ano.substring(3, 6).trim();
            retval [ 2 ] = ano.substring(6).trim();
        }
        catch(Exception e)
        {
            logger.trace("--T--> @getParsedPhoneNo: in=<" + ano + ">, df=" + dfName + "(rowNdx=" + rowindx + ") - use empty phone number");
        }

        return retval;

    }


    /**
     *
     *
     */
    protected int parseStringToInt(String val)
    {
        try
        {
            return((int) Double.parseDouble(val));
        }
        catch(Exception e)
        {
            logger.warning("Exception @PHC.parseStringToInt: parse error in=<" + val + ">, returning 0");
        }

        return 0;

    }


/**
     *
     *
     */
  //--> Much better performance when using StringBuffer instead
  //--> Changed by Billy 05Mar2003
    public String convertToHtmString(String input)
    {
        StringBuffer retStr = new StringBuffer("<font face=\"Courier New\" size=\"2\"> ");

        int x = 0;

        for(x = 0;x < input.length();   x ++)
        {
            char c = input.charAt(x);

            // Convert Return Chars
            if ((c == '�') ||(c == 10))
            {
                retStr.append("<br>");
            }
            else if ((c == '\'') ||(c == '\"'))
            {
                retStr.append('`');
            }
            else if ((c == ' '))
            {
                retStr.append("&nbsp;");
            }
            else if ((c == '<'))     //#DG378 escape this char
            {
                retStr.append("&lt;");
            }
            else if ((c == '>'))     //#DG378 escape this char
            {
                retStr.append("&gt;");
            }
            else if (! Character.isISOControl(c))
            {
                retStr.append(c);
            }
        }
    retStr.append("</font>");
        return retStr.toString();
    }

    ////SYNCADD
    // Method to convert String to Html display format
    //  - Remove all control characters
    //  - Convert Return and LineFeed to "<br>"
    //  - Convert ' and " to `
    //  - Special handle with spaces replace "  " ==> " &nbsp;" and " " ==> " "
    /*  #DG526 moved to stringUtil

    public String convertToHtmString2(String input)
    {
        StringBuffer retStr = new StringBuffer("");

        int x = 0;

        boolean lastIsSpace = false;

        for(x = 0; x < input.length(); x++)
        {
            char c = input.charAt(x);

            // Convert Return Chars
            if ((c == '?') ||(c == 10))
            {
                retStr.append("<br>");
                lastIsSpace = false;
            }
            else if ((c == '\'') ||(c == '\"'))
            {
                retStr.append('`');
                lastIsSpace = false;
            }
            else if ((c == ' '))
            {
                if (lastIsSpace == false)
                {
                    retStr.append(c);
                    lastIsSpace = true;
                }
                else
                {
                    retStr.append("&nbsp;");
                    lastIsSpace = false;
                }
            }
            else if (! Character.isISOControl(c))
            {
                retStr.append(c);
                lastIsSpace = false;
            }
        }
        return retStr.toString();
        } */

  //--> Ticket#369 :: Change for Cervus :: added New Bank info for Party
  //--> By Billy 25May2004
  // New mathod to split the Bank Transit number
  public String[] bankTransitNumSplit(String num)
    {
    String resultStrs [ ] = new String [2];

    // Separate the BankNum and Transit Number ==> Format "???-????????"
    if(num.length() > 0)
    {
      int pos = num.indexOf('-');
      if(pos != 3)
      {
        //invalid format ==> get the first 3 chars as BankNum and the rest as Transit number
        if(num.length() >= 3)
        {
          resultStrs[0] = num.substring(0, 3);
          resultStrs[1] = num.substring(3);
        }
        else
        {
          resultStrs[0] = num.substring(0);
        }
      }
      else
      {
        //In Correct format ==> skip the "-" char
        if(num.length() >= 4)
        {
          resultStrs[0] = num.substring(0, 3);
          resultStrs[1] = num.substring(4);
        }
        else
        {
          resultStrs[0] = num.substring(0, 3);
        }
      }
    }
    return resultStrs;
  }
  //===========================================================

}

