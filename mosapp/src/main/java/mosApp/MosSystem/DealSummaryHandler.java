package mosApp.MosSystem;

/**
 * 29/Feb/2008 DVG #DG702 MLM x BDN merge adjustments
 * 17/Apr/2006 DVG #DG406 #2693  Wells Fargo Security Project - Reverse Funding Mosssys Property
        - if obfuscated hide button
 */

import java.sql.Clob;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.commitment.CCM;
import com.basis100.deal.docprep.Dc;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.WorkflowTrigger;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.mtgrates.PricingLogic2;
import com.basis100.mtgrates.RateInfo;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.util.Xc;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.DisplayField;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.ViewBean;


/**
 * <p>Title: DealSummaryHandler</p>
 *
 * <p>Description: Handler for deal summary - Brings up the deal summary page
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.0 
 * * @version 1.1  <br>
 * Date: 08/11/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  Added setBorrowerIndentifications() method - Fix for defect 4215
 * @version 1.2  <br>
 * Author:MCM Impl Team <br>
 * Date:11/06/2008
 * Change:Added new methods isComponentEligible() and handleGotoComponentDetailsPage() method 
 *        and updated the setupBeforePagegenaration() method as part of Story XS_16.7
 * MCM Impl Team
 * @vesrion 1.3 25-June-2008 XS_16.13,XS_16.15 Added new setMrtgCompDisplayFields(),setLoanCompDisplayFields for display actulaPaymentTerm,AmortizationPeriod,
 *                           EffectiveAmortozation Period for Loan and Mortgage Componend   
 * @version 1.4 24 Sep 2008  MCM Impl -Bug Fix :FXP22544   added LANGUAGEPREFERENCEID for doDealnotesModel user where creteria.
 */
public class DealSummaryHandler extends PageHandlerCommon
	implements Cloneable, Xc
{
	//#DG702 
	protected static final String NUMOFEXPENSES = "NUMOFEXPENSES";
	protected static final String VPROPERTYID = "VPROPERTYID";
	protected static final String EXPROWINDEX = "EXPROWINDEX";
	protected static final String EMPROWINDEX = "EMPROWINDEX";
	protected static final String NUMOFCREDITREF = "NUMOFCREDITREF";
	protected static final String NUMOFASSET = "NUMOFASSET";
	protected static final String NUMOFCBLIABILITY = "NUMOFCBLIABILITY";
	protected static final String NUMOFLIABILITY = "NUMOFLIABILITY";	
	protected static final String INCOME_BORROWERID = "INCOME_BORROWERID";
	protected static final String VINCOMEID = "VINCOMEID";
	protected static final String NUMOFOTHERINCOME = "NUMOFOTHERINCOME";
	protected static final String NUMOFEMPLOYMENT = "NUMOFEMPLOYMENT";
	protected static final String BORADDROWINDEX = "BORADDROWINDEX";
	protected static final String NUMOFADDRESS = "NUMOFADDRESS";
	protected static final String BORIDENROWINDEX = "BORIDENROWINDEX";
	protected static final String NUMOFIDENTIFICATIONS = "NUMOFIDENTIFICATIONS";
	protected static final String VBORROWERID = "VBORROWERID";

	

	/**
	 *
	 *
	 */
	public DealSummaryHandler cloneSS()
	{
		return(DealSummaryHandler) super.cloneSafeShallow();

	}


	////////////////////////////////////////////////////////////////
	//This method is used to populate the fields in the header
	//with the appropriate information

	public void populatePageDisplayFields() 
	{
		PageEntry pg = getTheSessionState().getCurrentPage();

		populatePageShellDisplayFields();

		// Quick Link Menu display
		displayQuickLinkMenu();
		
		populateTaskNavigator(pg);

		populatePageDealSummarySnapShot();

		populatePreviousPagesLinks();

		populateBorrowerDetails();
		ViewBean thePage = getCurrNDPage();
		/****************************MCM IMPL TEAM STARTS XS_16.7*****************************/
		 /**
		  * Logic for show/hide Component Informations section
		  */
		//Hide Component Informations section if the deal product is not component eligible 
		boolean isComponentEligible=isComponentEligible();
		if(!isComponentEligible){
		    startSupressSection(pgDealSummaryViewBean.CHILD_STINCLUDECOMPONENTINFOSECTION_START, thePage);
		    endSupressSection(pgDealSummaryViewBean.CHILD_STINCLUDECOMPONENTINFOSECTION_END, thePage);

		}
		/**
		 * Logic to hide/show Component Details button
		 */
		else //Hide 'Component Details' button if the deal product is component eligible but does not have any components         
		    if(isComponentEligible && !isComponentPresent()){

		        startSupressContent(pgDealSummaryViewBean.CHILD_STINCLUDE_COMPONENTDETAIL_START, thePage);
		        endSupressContent(pgDealSummaryViewBean.CHILD_STINCLUDE_COMPONENTDETAIL_END, thePage);

		    }
		/***************************MCM IMPL TEAM ENDS XS_16.7*****************************/  
		// Check and set Display or not the FinancingProgram field
		setDisplayFinancingProgram(pg, getCurrNDPage());
		   
		
		
    //--DJ_LDI_CR--start//
    //// Hide of display Life&Disability Insurance Section on the screen.
 
    //checkForComponentEligibleProduct(thePage);
//  Ticket #4113
    DisplayField df = ((pgDealSummaryViewBean)thePage).getDisplayField(pgDealSummaryViewBean.CHILD_STLOCINTERESTONLYMATURITYDATE);
		if (df != null & df.getValue() != null) {
		String val = df.getValue().toString();
			if (val.indexOf("1969-12-31") > -1) {
				thePage.setDisplayFieldValue(pgDealSummaryViewBean.CHILD_STLOCINTERESTONLYMATURITYDATE, "");
		}
	}
	
//  Ticket #4115
		DisplayField _locAmort = ((pgDealSummaryViewBean) thePage).getDisplayField(pgDealSummaryViewBean.CHILD_STLOCAMORTIZATION);
		if (_locAmort != null & _locAmort.getValue() != null) {
		String val = _locAmort.getValue().toString();
			if (val.equals("0")) {
				thePage.setDisplayFieldValue(pgDealSummaryViewBean.CHILD_STLOCAMORTIZATION, "");
		}
	}


    if (PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),COM_BASIS100_LDINSURANCE_DISPLAYLIFEDISABILITYMODULE, "N").equals("N"))
    {
      startSupressSection(pgDealSummaryViewBean.CHILD_STINCLUDELIFEDISSECTIONSTART, thePage);
      endSupressSection(pgDealSummaryViewBean.CHILD_STINCLUDELIFEDISSECTIONEND, thePage);
    }
    //--DJ_LDI_CR--end//
    //***** Change by NBC/PP Implementation Team - GCD - Start *****//
    if (!isShowGCDSumarySection()) {
      startSupressContent(pgDealSummaryViewBean.CHILD_ST_INCLUDE_GCDSUM_START, thePage);
      endSupressContent(pgDealSummaryViewBean.CHILD_ST_INCLUDE_GCDSUM_END, thePage);
    }
    
    //***** Change by NBC/PP Implementation Team - GCD - End *****//

  
    displayReverseFundingBtn(pg, getCurrNDPage());    //  #1676, Catherine, 2-Sep-05 
    setHiddenQualifyRateSection(thePage);

	}

	//////////////////////////////////////////////////////////////////

	public void setupBeforePageGeneration()
	{

		// set criteria for data objects to desired deal id
		//logger.debug("--- Toni ---- @setupBeforePageGeneration");
		PageEntry pg = getTheSessionState().getCurrentPage();
	
		if (pg.getSetupBeforeGenerationCalled() == true) return;

		pg.setSetupBeforeGenerationCalled(true);

		// -- //
		setupDealSummarySnapShotDO(pg);

		try
		{
			int dealId = pg.getPageDealId();
			int copyId = pg.getPageDealCID();
			logger.debug("-- T--> @DealSummaryHandler.setupBeforePageGeneration: DealID == " + dealId + " And copyid == " + copyId);
			if (dealId <= 0)
			{
				setStandardFailMessage();
				logger.error("Problem encountered displaying deal summary - invalid dealId (" + dealId + ")");
				return;
			}
			Integer cspDealId = new Integer(dealId);
			Integer cspCopyId = new Integer(copyId);

      //--DJ_CR134--start--27May2004--//
      // For DJ client execute this model to get HomeBASE product, rate, payment info (if exist)
      // dealnotes table (dealnotescategoryid = 10).
      //--Ticket#692--start--02Nov2004//
      // Open this option for every client, not property driven now.

      Integer notesCategoryId = new Integer(Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION_PRODUCT);
      executeCretarioForDealNotesHomeBASEInfo(cspDealId, notesCategoryId);
      //--Ticket#692--end--02Nov2004//
      //--DJ_CR134--end--//

			// set criteria for data objects
			doMainViewDealModelImpl domainviewdeal =(doMainViewDealModelImpl)(RequestManager.getRequestContext().getModelManager().getModel(doMainViewDealModel.class));
			domainviewdeal.clearUserWhereCriteria();
			domainviewdeal.addUserWhereCriterion("dfDealId", "=", cspDealId);
			domainviewdeal.addUserWhereCriterion("dfCopyId", "=", cspCopyId);

			logger.debug("--- ANU --- @setupBeforePageGeneration: adding where clause: DealID == " + domainviewdeal.toString() );
			try{
        domainviewdeal.executeSelect(null);
			}
      catch(Exception e)
			{
				setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
				logger.error("Problem encountered querying deal summary - domainviewdeal.succeeded() failed :: " + e.toString());
				return;
			}

			if (domainviewdeal.getSize() <= 0)
			{
				setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
				logger.error("Problem encountered querying deal summary - invalid dealId (" + dealId + ")");
				return;
			}

      //logger.error("--- Toni ---> domainviewdeal.getNumRows() == " + resultbl.getNumRows() );
			int bridgeId = getMeBridgeId(srk, dealId, copyId);

			doMainViewBridgeModelImpl domainviewbridge =(doMainViewBridgeModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doMainViewBridgeModel.class));
			domainviewbridge.clearUserWhereCriteria();
			//logger.debug("--- Toni --- bridgeid == " + bridgeId);
			domainviewbridge.addUserWhereCriterion("dfBridgeId", "=", new Integer(bridgeId));
			logger.debug("--- ANU --- bridgeid == " + domainviewbridge.toString());
			
			//logger.debug("--- Toni --- adding where clause to domainviewbridge where bridgeid == " + bridgeId);
			doMainViewBorrowerModelImpl domainviewborr =(doMainViewBorrowerModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doMainViewBorrowerModel.class));
			domainviewborr.clearUserWhereCriteria();
			domainviewborr.addUserWhereCriterion("dfDealId", "=", cspDealId);
			domainviewborr.addUserWhereCriterion("dfCopyId", "=", cspCopyId);
			logger.debug("--- ANU --- doMainViewBorrowerModelImpl == " + domainviewborr.toString());
			
			//logger.debug("--- Toni --- adding where clause to domainviewborr where dealid == " + dealId);
			doMainViewDownPaymentModelImpl domainviewdownpay =(doMainViewDownPaymentModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doMainViewDownPaymentModel.class));
			domainviewdownpay.clearUserWhereCriteria();
			domainviewdownpay.addUserWhereCriterion("dfDealId", "=", cspDealId);
			domainviewdownpay.addUserWhereCriterion("dfCopyId", "=", cspCopyId);
			logger.debug("--- ANU --- domainviewdownpay == " + domainviewdownpay.toString());
			
			/************************ MCM Impl team changes starts - XS_16.7 ****************************************/
			
			doComponentSummaryModelImpl doComponentSummaryModel =(doComponentSummaryModelImpl)
	          (RequestManager.getRequestContext().getModelManager().getModel(doComponentSummaryModel.class));
			doComponentSummaryModel.clearUserWhereCriteria();
			doComponentSummaryModel.addUserWhereCriterion("dfDealId", "=", cspDealId);
			doComponentSummaryModel.addUserWhereCriterion("dfCopyId", "=", cspCopyId);
			
			
			doComponentMortgageModelImpl doComponentMortgageModel =(doComponentMortgageModelImpl)
	          (RequestManager.getRequestContext().getModelManager().getModel(doComponentMortgageModel.class));
			doComponentMortgageModel.clearUserWhereCriteria();
			doComponentMortgageModel.addUserWhereCriterion("dfDealId", "=", cspDealId);
			doComponentMortgageModel.addUserWhereCriterion("dfCopyId", "=", cspCopyId);
			
			doComponentLocModelImpl doComponentLocModel =(doComponentLocModelImpl)
	          (RequestManager.getRequestContext().getModelManager().getModel(doComponentLocModel.class));
			doComponentLocModel.clearUserWhereCriteria();
			doComponentLocModel.addUserWhereCriterion("dfDealId", "=", cspDealId);
			doComponentLocModel.addUserWhereCriterion("dfCopyId", "=", cspCopyId);
			
			doComponentLoanModelImpl doComponentLoanModel =(doComponentLoanModelImpl)
	          (RequestManager.getRequestContext().getModelManager().getModel(doComponentLoanModel.class));
			doComponentLoanModel.clearUserWhereCriteria();
			doComponentLoanModel.addUserWhereCriterion("dfDealId", "=", cspDealId);
			doComponentLoanModel.addUserWhereCriterion("dfCopyId", "=", cspCopyId);
			
			doComponentCreditCardModelImpl doComponentCreditCardModel =(doComponentCreditCardModelImpl)
	          (RequestManager.getRequestContext().getModelManager().getModel(doComponentCreditCardModel.class));
			doComponentCreditCardModel.clearUserWhereCriteria();
			doComponentCreditCardModel.addUserWhereCriterion("dfDealId", "=", cspDealId);
			doComponentCreditCardModel.addUserWhereCriterion("dfCopyId", "=", cspCopyId);
			
			doComponentOverDraftModelImpl doComponentOverDraftModel =(doComponentOverDraftModelImpl)
	          (RequestManager.getRequestContext().getModelManager().getModel(doComponentOverDraftModel.class));
			doComponentOverDraftModel.clearUserWhereCriteria();
			doComponentOverDraftModel.addUserWhereCriterion("dfDealId", "=", cspDealId);
			doComponentOverDraftModel.addUserWhereCriterion("dfCopyId", "=", cspCopyId);

		
		/******************************* MCM Impl team changes ends - XS_16.7 *******************************************/
			
		
		//logger.debug("--- Toni --- adding where clause to domainviewdownpay where dealid == " + dealId);
			doMainViewEscrowModelImpl domainviewescrow =(doMainViewEscrowModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doMainViewEscrowModel.class));
			domainviewescrow.clearUserWhereCriteria();
			domainviewescrow.addUserWhereCriterion("dfDealId", "=", cspDealId);
			domainviewescrow.addUserWhereCriterion("dfCopyId", "=", cspCopyId);
			logger.debug("--- ANU --- domainviewescrow == " + domainviewescrow.toString());
			
			//logger.debug("--- Toni --- adding where clause to domainviewescrow where dealid == " + dealId);
			doMainViewMortgageInsurerModelImpl domainviewmortgage =(doMainViewMortgageInsurerModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doMainViewMortgageInsurerModel.class));
			domainviewmortgage.clearUserWhereCriteria();
			domainviewmortgage.addUserWhereCriterion("dfDealId", "=", cspDealId);
			domainviewmortgage.addUserWhereCriterion("dfCopyId", "=", cspCopyId);
			logger.debug("--- ANU --- domainviewmortgage == " + domainviewmortgage.toString());
			
			//logger.debug("--- Toni --- adding where clause to domainviewmortgage where dealid == " + dealId);
			doMainViewPrimaryPropertyModelImpl domainprimaryproperty =(doMainViewPrimaryPropertyModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doMainViewPrimaryPropertyModel.class));
			domainprimaryproperty.clearUserWhereCriteria();
			domainprimaryproperty.addUserWhereCriterion("dfDealId", "=", cspDealId);
			domainprimaryproperty.addUserWhereCriterion("dfCopyId", "=", cspCopyId);
			logger.debug("--- ANU --- domainprimaryproperty == " + domainprimaryproperty.toString());
			
			//logger.debug("--- Toni --- adding where clause to dodetailviewborrs where dealid == " + dealId);
			//
			//These are detail view Panels but still depend on deal id so i do
			//the filtering here with main view
			doDetailViewBorrowersModelImpl dodetailviewborrs =(doDetailViewBorrowersModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewBorrowersModel.class));
			dodetailviewborrs.clearUserWhereCriteria();
			dodetailviewborrs.addUserWhereCriterion("dfDealId", "=", cspDealId);
			dodetailviewborrs.addUserWhereCriterion("dfCopyId", "=", cspCopyId);
			logger.debug("--- ANU --- dodetailviewborrs == " + dodetailviewborrs.toString());
			
			//logger.debug("--- Toni --- adding where clause to dodetailviewborrs where dealid == " + dealId);
			//logger.debug("--- Toni --- adding where clause to dodetailviewborrs where copyid == " + cspCopyId.toString());
			doDetailViewGDSModelImpl dodetailviewGDS =(doDetailViewGDSModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewGDSModel.class));
			dodetailviewGDS.clearUserWhereCriteria();
			dodetailviewGDS.addUserWhereCriterion("dfDealId", "=", cspDealId);
			dodetailviewGDS.addUserWhereCriterion("dfCopyId", "=", cspCopyId);
			logger.debug("--- ANU --- dodetailviewGDS == " + dodetailviewGDS.toString());
			
			//logger.debug("--- Toni --- adding where clause to dodetailviewGDS where dealid == " + dealId);
			doDetailViewTDSModelImpl dodetailviewTDS =(doDetailViewTDSModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewTDSModel.class));
			dodetailviewTDS.clearUserWhereCriteria();
			dodetailviewTDS.addUserWhereCriterion("dfDealId", "=", cspDealId);
			dodetailviewTDS.addUserWhereCriterion("dfCopyId", "=", cspCopyId);
			logger.debug("--- ANU --- dodetailviewTDS == " + dodetailviewTDS.toString());
			
			
			//logger.debug("--- Toni --- adding where clause to dodetailviewTDS where dealid == " + dealId);
			doDetailViewPropertiesModelImpl dodetailviewprops =(doDetailViewPropertiesModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewPropertiesModel.class));
			dodetailviewprops.clearUserWhereCriteria();
			dodetailviewprops.addUserWhereCriterion("dfDealId", "=", cspDealId);
			dodetailviewprops.addUserWhereCriterion("dfCopyId", "=", cspCopyId);
			logger.debug("--- ANU --- dodetailviewprops == " + dodetailviewprops.toString());
			
			//logger.debug("--- Toni --- adding where clause to dodetailviewprops where dealid == " + dealId);
			doDetailViewPropertyExpensesModelImpl dodetailviewpropexp =(doDetailViewPropertyExpensesModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewPropertyExpensesModel.class));
			dodetailviewpropexp.clearUserWhereCriteria();
			dodetailviewpropexp.addUserWhereCriterion("dfCopyId", "=", cspCopyId);
			logger.debug("--- ANU --- dodetailviewpropexp == " + dodetailviewpropexp.toString());
			
			//logger.debug("--- Toni --- adding where clause to dodetailviewpropexp where copyid  == " + copyId);
			// subordinates (Liab, Income, Emp. History) have criteria set dynamically
			// during generation - see page class

			//4.4 Submission Agent
			doUWSourceDetailsModelImpl douwsourcedetails = (doUWSourceDetailsModelImpl) 
				(RequestManager.getRequestContext().getModelManager().getModel(doUWSourceDetailsModel.class));
			douwsourcedetails.clearUserWhereCriteria();
			douwsourcedetails.addUserWhereCriterion("dfDealId", "=", cspDealId);

      //--DJ_LDI_CR--start--//
			logger.debug("--- Vlad --- adding where clause to domainprimaryproperty where dealid == " + cspDealId +
                                 "; copyid == " + cspCopyId);

		  executeCretarioForUWApplicantGDSTDS(cspDealId, cspCopyId);
      //--DJ_LDI_CR--end--//

      //***** Change by NBC/PP Implementation Team - GCD - Start *****//	
	  
	  executeCretarioForAdjudicationResponseBNCModel(cspDealId, cspCopyId);

	  //***** Change by NBC/PP Implementation Team - GCD - End *****//


      //--DJ_CR134--start--27May2004--//
      //--Ticket#692--start--02Nov2004//
      // Open this option for every client, not property driven now.
//logger.debug("DSH@populatePageDisplayFields::HomeBASEProductRatePmnt: " + getHomeBASEProductRatePmnt());
      String homeBASEinfo = "";
      if (getHomeBASEProductRatePmnt() != null && !getHomeBASEProductRatePmnt().equals(""))
      {
        homeBASEinfo = getHomeBASEProductRatePmnt();
        getCurrNDPage().setDisplayFieldValue(pgDealSummaryViewBean.CHILD_STHOMEBASEPRODUCTRATEPMNT, convertToHtmString(homeBASEinfo));
      }
      else
      {
        getCurrNDPage().setDisplayFieldValue(pgDealSummaryViewBean.CHILD_STHOMEBASEPRODUCTRATEPMNT,
                                            BXResources.getGenericMsg(NOT_AVAILABLE_LABEL,  theSessionState.getLanguageId()));
      }
     //--Ticket#692--end--02Nov2004//
     //--DJ_CR134--end--//
      //Purchase Plus Improvements start
	    
	    Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
   
    double improvedValue = deal.getTotalPurchasePrice() + deal.getRefiImprovementAmount();
	    
	    getCurrNDPage().setDisplayFieldValue(pgDealSummaryViewBean.CHILD_STIMPROVEDVALUE, new Double(improvedValue));
	     
	    switch (deal.getDealPurposeId()) {
	        case Mc.DEAL_PURPOSE_PURCHASE_WITH_IMPROVEMENTS:
	            getCurrNDPage().setDisplayFieldValue(pgDealSummaryViewBean.CHILD_TXDEALPURPOSETYPEHIDDENSTART, "");
	            getCurrNDPage().setDisplayFieldValue(pgDealSummaryViewBean.CHILD_TXDEALPURPOSETYPEHIDDENEND, "");
	            break;
	        default:
	            getCurrNDPage().setDisplayFieldValue(pgDealSummaryViewBean.CHILD_TXDEALPURPOSETYPEHIDDENSTART, supresTagStart);
	            getCurrNDPage().setDisplayFieldValue(pgDealSummaryViewBean.CHILD_TXDEALPURPOSETYPEHIDDENEND, supresTagEnd);
	            break;
	    }

    //Purchase Plus Improvements end
        
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupBeforePageGeneration");
			logger.error(e);
			return;
		}

	}
	/******************************MCM Impl team changes starts - XS_16.7 ***********************************************************/
	/**
		 * This method returns true if the deal product is component eligible.
		 * 
		 * @param 
		 * @param 
		 * @version 1.0 10-06-2008 XS_16.7 Initial version
	*/
	public boolean isComponentEligible() 
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Deal deal;
		try {
      //Get deal object
			deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());

      //Get MtgProd object for the deal product id 
			MtgProd mtgProd = new MtgProd(srk, null,deal.getMtgProdId());			

      //return true if the elibible flag is 'Y'
			if (("Y").equals(mtgProd.getComponentEligibleFlag()))
				return true;
			
		} catch (RemoteException e) {

			logger
					.error("Exception @DealSummaryHandler.isComponentEligible");
			logger.error(e);
		} catch (FinderException e) {

			logger
					.error("Exception @DealSummaryHandler.isComponentEligible");
			logger.error(e);
		}
		return false;
	}
	
	/**
	 * This method checks for any component associated with the deal or not.
	 * 
	 * @param
	 * @param
	 * @version 1.0 10-06-2008 XS_16.7 Initial version
	 */
	public boolean isComponentPresent() 
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
	
		Deal deal;
		try {
      //Get deal object
			deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
      
      //Get the components for the deal
			Collection component=deal.getComponents();
      
      //Return true if there are any components
			if(component.size()!=0){
				return true;
			}
		}  catch (RemoteException e) {
			
			logger.error("Exception @DealSummaryHandler.isComponentPressent");
			logger.error(e);
		} catch (FinderException e) {
			logger.error("Exception @DealSummaryHandler.isComponentPressent");
			logger.error(e);
		} catch (Exception e) {
			logger.error("Exception @DealSummaryHandler.isComponentPressent");
			logger.error(e);
		}
		
	 		return false;
	}
	/**
	 * This navigates the user to the Components Details sub screen.
	 * 
	 * @param
	 * @param
	 * @version 1.0 10-06-2008 XS_16.7 Initial version
	 */
	public void handleGoToComponentDetailsPage()
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			PageEntry pgEntry = setupSubPagePageEntry(pg, Sc.PGNM_COMPONENT_DETAILS_DEAL_SUMMARY, true);
			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage();
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.handleGoToComponentDetailsPage: while navigating to the Component Deatails Summary subpage");
			logger.error(e);
		}

	}

	/******************************MCM Impl team changes ends - XS_16.7***********************************************************/

	//	***** Change by NBC Impl. Team - Version 1.1 - Start *****//
	/**
	 * setBorrowerIndentifications - Bring up multiple identifications details for a borrower associated to a deal
	 * Set the query - where criterea
	 * 
	 * @param int
	 * @return void : the result of setBorrowerIndentifications() <br>
	 */
	public void setBorrowerIndentifications(int rowindex) {
		PageEntry pg = getTheSessionState().getCurrentPage();
		int copyId = pg.getPageDealCID();
		Hashtable pst = pg.getPageStateTable();
		
		try {
			Vector vborrowerid = (Vector) pst.get(VBORROWERID);
			
			// Get the list of Borrower Identifications for the borrower
			doIdentificationTypeModelImpl dObject = (doIdentificationTypeModelImpl) (RequestManager
					.getRequestContext().getModelManager()
					.getModel(doIdentificationTypeModel.class));
			dObject.clearUserWhereCriteria();
			dObject.addUserWhereCriterion("dfBorrowerId", "=", new Integer(
					com.iplanet.jato.util.TypeConverter.asInt(vborrowerid
							.elementAt(rowindex))));
			dObject.addUserWhereCriterion("dfCopyId", "=", new Integer(copyId));
			
			logger.debug("--- ANU --- dObject == " + dObject.toString());
			
			dObject.executeSelect(null);
			pst.put(NUMOFIDENTIFICATIONS, new Integer(dObject.getSize()));
			pst.put(BORIDENROWINDEX, new Integer(rowindex));
		} catch (Exception e) {
			logger.error("Exception @setBorrowerIndentifications ():" + e);		//#DG702
			return;
		}

	}


//	***** Change by NBC Impl. Team - Version 1.1 - End*****//
	//////////////////////////////////////////////////////////////////
	public void setBorrowerAddresses(int rowindex)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		int copyId = pg.getPageDealCID();
		Hashtable pst = pg.getPageStateTable();

		try
		{
			Vector vborrowerid =(Vector) pst.get(VBORROWERID);
			doDetailViewBorrowerAddressesModelImpl listOfAddress =(doDetailViewBorrowerAddressesModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewBorrowerAddressesModel.class));
			listOfAddress.clearUserWhereCriteria();
			listOfAddress.addUserWhereCriterion("dfBorrowerId", "=", new Integer(com.iplanet.jato.util.TypeConverter.asInt(vborrowerid.elementAt(rowindex))));
			listOfAddress.addUserWhereCriterion("dfCopyId", "=", new Integer(copyId));

			logger.debug("--- ANU --- dObject == " + listOfAddress.toString());
			//logger.error("--- Toni --- setBorrowerAddresses vborrowerid at rowindex == " + rowindex + " value == "
			//					+ ((Integer)TypeConverter.asInt(vborrowerid.elementAt(rowindex))) );
			listOfAddress.executeSelect(null);
			pst.put(NUMOFADDRESS, new Integer(listOfAddress.getSize()));
			pst.put(BORADDROWINDEX, new Integer(rowindex));
		}
		catch(Exception e)
		{
			logger.error("Exception at setting borrower address:" + e);		//#DG702
			return;
		}

	}


	//////////////////////////////////////////////////////////////////

	public void setBorrowerIncomes(int rowindex)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		Integer borrowerId = null;

		try
		{
			Vector vborrowerid =(Vector) pst.get(VBORROWERID);

      // Get the list of Employment Incomes
      borrowerId = new Integer(com.iplanet.jato.util.TypeConverter.asInt(vborrowerid.elementAt(rowindex)));
			doDetailViewEmploymentModelImpl listofemployment =(doDetailViewEmploymentModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewEmploymentModel.class));
			listofemployment.clearUserWhereCriteria();
			listofemployment.addUserWhereCriterion("dfCopyId", "=", new Integer(pg.getPageDealCID()));
			listofemployment.addUserWhereCriterion("dfBorrowerId", "=", borrowerId);
			
			logger.debug("--- ANU --- listofemployment == " + listofemployment.toString());
			
			listofemployment.executeSelect(null);
			pst.put(NUMOFEMPLOYMENT, new Integer(listofemployment.getSize()));
			Vector vincomeid = new Vector();

			while(listofemployment.next())
      {
        vincomeid.addElement(listofemployment.getDfEmpHistIncomeId());
      }
      pst.put(VINCOMEID, vincomeid);
			pst.put(INCOME_BORROWERID, new Integer(com.iplanet.jato.util.TypeConverter.asInt(vborrowerid.elementAt(rowindex))));

      // Execute the DataModel to get all OtherIncomes
      doDetailViewEmpOtherIncomeModelImpl listofotherincomes =(doDetailViewEmpOtherIncomeModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewEmpOtherIncomeModel.class));
			listofotherincomes.clearUserWhereCriteria();
			listofotherincomes.addUserWhereCriterion("dfBorrowerId", "=", borrowerId);
			listofotherincomes.addUserWhereCriterion("dfCopyId", "=", new Integer(pg.getPageDealCID()));
			
			logger.debug("--- ANU --- listofotherincomes == " + listofotherincomes.toString());
			
			listofotherincomes.executeSelect(null);
			pst.put(NUMOFOTHERINCOME, new Integer(listofotherincomes.getSize()));
		}
		catch(Exception ex)
		{
			logger.error("Exception @setBorrowerIncomes()");
			logger.error(ex);
			return;
		}

	}


	//////////////////////////////////////////////////////////////////

	public void populateBorrowerIncomes(int rowindex, TiledView repeated)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		Integer borrowerId = null;

		try
		{
			borrowerId =(Integer)(pst.get(INCOME_BORROWERID));
			Vector vincomeId =(Vector) pst.get(VINCOMEID);
			Object incomeId =(Object)(vincomeId.elementAt(rowindex));
			doDetailViewIncomesModelImpl listofincomes =(doDetailViewIncomesModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewIncomesModel.class));
			listofincomes.clearUserWhereCriteria();
			listofincomes.addUserWhereCriterion("dfBorrowerId", "=", borrowerId);
			listofincomes.addUserWhereCriterion("dfCopyId", "=", new Integer(pg.getPageDealCID()));
			listofincomes.addUserWhereCriterion("dfIncomeId", "=", incomeId);
			logger.debug("--- ANU --- listofincomes == " + listofincomes.toString());
			
			listofincomes.executeSelect(null);

      //--> added checking to ensure we get the results -- By Billy 19Aug2002
      if(listofincomes.getSize() > 0)
      {
        Object incomeType = listofincomes.getDfIncomeType();
        Object incomeDesc = listofincomes.getDfIncomeDesc();
        Object incomePeriod = listofincomes.getDfIncomePeriod();
        Object incomeAmt = listofincomes.getDfIncomeAmt();
        Object gds = listofincomes.getDfPercentIncludedInGDS();
        Object tds = listofincomes.getDfPercentIncludedInTDS();
        repeated.setDisplayFieldValue(pgDealSummaryEmploymentTiledView.CHILD_STIDMINCOMETYPE, incomeType);
        repeated.setDisplayFieldValue(pgDealSummaryEmploymentTiledView.CHILD_STIDMINCOMEDESC, incomeDesc);
        repeated.setDisplayFieldValue(pgDealSummaryEmploymentTiledView.CHILD_STIDMINCOMEPERIOD, incomePeriod);
        repeated.setDisplayFieldValue(pgDealSummaryEmploymentTiledView.CHILD_STIDMINCOMEAMOUNT, incomeAmt);
        repeated.setDisplayFieldValue(pgDealSummaryEmploymentTiledView.CHILD_STIDMINCOMEPERCENTINCLUDEGDS, gds);
        repeated.setDisplayFieldValue(pgDealSummaryEmploymentTiledView.CHILD_STIDMINCOMEPERCENTINCLUDETDS, tds);
      }
		}
		catch(Exception ex)
		{
			logger.error("Exception @populateBorrowerIncomes()");
			logger.error(ex);
			return;
		}

	}


	//////////////////////////////////////////////////////////////////

	public void setBorrowerLiabilities(int rowindex)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();

		try
		{
			Vector vborrowerid =(Vector) pst.get(VBORROWERID);
			doDetailViewLiabilitiesModelImpl listofliabilities =(doDetailViewLiabilitiesModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewLiabilitiesModel.class));
			listofliabilities.clearUserWhereCriteria();
			listofliabilities.addUserWhereCriterion("dfBorrowerId", "=", new Integer(com.iplanet.jato.util.TypeConverter.asInt(vborrowerid.elementAt(rowindex))));
			listofliabilities.addUserWhereCriterion("dfCopyId", "=", new Integer(pg.getPageDealCID()));
			
			logger.debug("--- ANU --- listofliabilities == " + listofliabilities.toString());
			
			listofliabilities.executeSelect(null);

			//logger.error("--- Toni ---setBorrowerLiabilities  NUMOFLIABILITY == " + listofliabilities.getNumRows() );
			pst.put(NUMOFLIABILITY, new Integer(listofliabilities.getSize()));

			// Set up for Cerdit Bureau Libilities -- Billy 13Nov2001
			doDetailViewCBLiabilitiesModelImpl listofcbliabilities =(doDetailViewCBLiabilitiesModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewCBLiabilitiesModel.class));
			listofcbliabilities.clearUserWhereCriteria();
			listofcbliabilities.addUserWhereCriterion("dfBorrowerId", "=", new Integer(com.iplanet.jato.util.TypeConverter.asInt(vborrowerid.elementAt(rowindex))));
			listofcbliabilities.addUserWhereCriterion("dfCopyId", "=", new Integer(pg.getPageDealCID()));
			
			logger.debug("--- ANU --- listofcbliabilities == " + listofcbliabilities.toString());
			
			listofcbliabilities.executeSelect(null);

			//logger.error("--- Toni ---setBorrowerLiabilities  NUMOFLIABILITY == " + listofliabilities.getNumRows() );
			pst.put(NUMOFCBLIABILITY, new Integer(listofcbliabilities.getSize()));
		}
		catch(Exception e)
		{
			logger.error("Exception at setting borrower Liabilities:" + e);		//#DG702
			return;
		}

	}


	//////////////////////////////////////////////////////////////////

	public void setBorrowerAssets(int rowindex)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();

		try
		{
			Vector vborrowerid =(Vector) pst.get(VBORROWERID);
			doDetailViewAssetsModelImpl listofassets =(doDetailViewAssetsModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewAssetsModel.class));
			listofassets.clearUserWhereCriteria();
			listofassets.addUserWhereCriterion("dfBorrowerId", "=", new Integer(com.iplanet.jato.util.TypeConverter.asInt(vborrowerid.elementAt(rowindex))));
			listofassets.addUserWhereCriterion("dfCopyId", "=", new Integer(pg.getPageDealCID()));
			
			logger.debug("--- ANU --- listofassets == " + listofassets.toString());
			
			listofassets.executeSelect(null);
			pst.put(NUMOFASSET, new Integer(listofassets.getSize()));

			doDetailViewCreditRefsModelImpl listcreditrefs =(doDetailViewCreditRefsModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewCreditRefsModel.class));
			listcreditrefs.clearUserWhereCriteria();
			listcreditrefs.addUserWhereCriterion("dfBorrowerId", "=", new Integer(com.iplanet.jato.util.TypeConverter.asInt(vborrowerid.elementAt(rowindex))));
			listcreditrefs.addUserWhereCriterion("dfCopyId", "=", new Integer(pg.getPageDealCID()));
			
			logger.debug("--- ANU --- listcreditrefs == " + listcreditrefs.toString());
			
			listcreditrefs.executeSelect(null);
			pst.put(NUMOFCREDITREF, new Integer(listcreditrefs.getSize()));
		}
		catch(Exception e)
		{
			logger.error("Exception at setting borrower Assets:" + e);		//#DG702
			return;
		}

	}


	//////////////////////////////////////////////////////////////////

	public void setEmployment(int rowindex)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();

		try
		{
			pst.put(EMPROWINDEX, new Integer(rowindex));
		}
		catch(Exception e)
		{
			logger.error("Exception at setEmployment:" + e);		//#DG702
			return;
		}

	}


	//////////////////////////////////////////////////////////////////

	public void setExpenses(int rowindex)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();

		try
		{
			pst.put(EXPROWINDEX, new Integer(rowindex));
      Vector vpropertyid =(Vector) pst.get(VPROPERTYID);

			doDetailViewPropertyExpensesModelImpl listOfExpenses =(doDetailViewPropertyExpensesModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewPropertyExpensesModel.class));
			listOfExpenses.clearUserWhereCriteria();
			listOfExpenses.addUserWhereCriterion("dfPropertyId", "=", new Integer(com.iplanet.jato.util.TypeConverter.asInt(vpropertyid.elementAt(rowindex))));
			listOfExpenses.addUserWhereCriterion("dfCopyId", "=", new Integer(pg.getPageDealCID()));
			
			logger.debug("--- ANU --- listOfExpenses == " + listOfExpenses.toString());
			
			listOfExpenses.executeSelect(null);

			pst.put(NUMOFEXPENSES, new Integer(listOfExpenses.getSize()));
      //logger.debug("BILLY ===> Set NUMOFEXPENSES == " + listOfExpenses.getSize());
		}
		catch(Exception e)
		{
			logger.error("Exception at setExpenses:" + e);		//#DG702
			return;
		}

	}


	//////////////////////////////////////////////////////////////////

	public void setBranchName(String thefieldName)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();

		try
		{
			doMainViewDealModelImpl domainviewdeal =(doMainViewDealModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doMainViewDealModel.class));
			if(domainviewdeal.getSize() > 0)
			{
				Object branchprofileid = domainviewdeal.getDfBranchProfileId();

				//logger.error("--- Toni --- setBranchName 11111111111111111111111111" );
				if (branchprofileid != null && branchprofileid.toString().trim().length() != 0)
				{
					doGetBranchNameModelImpl dogetbranch =(doGetBranchNameModelImpl)
              (RequestManager.getRequestContext().getModelManager().getModel(doGetBranchNameModel.class));
					if(com.iplanet.jato.util.TypeConverter.asInt(branchprofileid) != 0)
            dogetbranch.addUserWhereCriterion("dfBranchProfileId", "=", branchprofileid);
					else
            dogetbranch.addUserWhereCriterion("dfBranchProfileId", "=", new Integer(- 1));
					
					logger.debug("--- ANU --- dogetbranch == " + dogetbranch.toString());
					
					dogetbranch.executeSelect(null);

					//logger.error("--- Toni --- setBranchName 2222222222222222222222222222222222222" );
					if (dogetbranch.getSize() > 0)
					{
						Object branchname = dogetbranch.getDfBranchShortName();
						Object businessid = dogetbranch.getDfBranchBusinessId();

						//logger.error("--- Toni --- setBranchName 3333333333333333333333333333333333" );
						if (businessid != null && businessid.toString().trim().length() != 0)
              getCurrNDPage().setDisplayFieldValue(thefieldName, new String(businessid.toString() + " - " + branchname.toString()));
						else
              getCurrNDPage().setDisplayFieldValue(thefieldName, branchname);
					}
				}
			}
		}
		catch(Exception e)
		{
			logger.error("Exception at setBranchName:" + e);		//#DG702
			return;
		}

	}


	//////////////////////////////////////////////////////////////////

	public boolean setProductName(String theFieldName)
	{

		//	  	PageEntry pg = getTheSessionState().getCurrentPage();
		//		Hashtable pst = pg.getPageStateTable();
		try
		{
			doMainViewDealModelImpl domainviewdeal =(doMainViewDealModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doMainViewDealModel.class));
			if (domainviewdeal.getSize() > 0)
			{
				Object mortgageprodid = domainviewdeal.getDfMortgageProductId();

				//logger.error("--- Toni ---setProductName  mortgageprodid == " + mortgageprodid.toString() );
				if (mortgageprodid != null && mortgageprodid.toString().trim().length() != 0)
				{
					doGetMortgageProductNameModelImpl dogetproduct =(doGetMortgageProductNameModelImpl)
              (RequestManager.getRequestContext().getModelManager().getModel(doGetMortgageProductNameModel.class));
					dogetproduct.clearUserWhereCriteria();
					dogetproduct.addUserWhereCriterion("dfMortgageProductId", "=", mortgageprodid);
					
					logger.debug("--- ANU --- dogetproduct == " + dogetproduct.toString());
					
					dogetproduct.executeSelect(null);
					if (dogetproduct.getSize() > 0)
					{
						Object product = dogetproduct.getDfMortgageProductName();

						//logger.error("--- Toni ---setProductName  product == " + product.toString() );
						if (product != null && product.toString().trim().length() != 0)
              getCurrNDPage().setDisplayFieldValue(theFieldName, product);
						else
              getCurrNDPage().setDisplayFieldValue(theFieldName, new String(""));

						//logger.error("--- Toni ---after  setting product == ");
					}
					else return false;
				}
				else return false;
			}
			else return false;
		}
		catch(Exception ex)
		{
			logger.error("Exception at setProductName : " + ex.toString());
			return false;
		}

		return true;

	}


	//////////////////////////////////////////////////////////////////

	public boolean setRateCode(String theFieldName)
	{

		//	  	PageEntry pg = getTheSessionState().getCurrentPage();
		//		Hashtable pst = pg.getPageStateTable();
		try
		{
			doMainViewDealModelImpl domainviewdeal =(doMainViewDealModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doMainViewDealModel.class));
			if (domainviewdeal.getSize() > 0)
			{
				Object pricingprofileid = domainviewdeal.getDfPricingProfileId();

				//logger.error("--- Toni ---setRateCode  pricingprofileid == " + pricingprofileid.toString() );
				if (pricingprofileid != null && pricingprofileid.toString().trim().length() != 0)
				{
					doGetRateCodeModelImpl dogetratecode =(doGetRateCodeModelImpl)
              (RequestManager.getRequestContext().getModelManager().getModel(doGetRateCodeModel.class));
					dogetratecode.clearUserWhereCriteria();

					// Bug fixed by BILLY -- user Inventory Id instead
					dogetratecode.addUserWhereCriterion("dfPricingRateInventoryId", "=", pricingprofileid);
					
					logger.debug("--- ANU --- dogetbranch == " + dogetratecode.toString());
					
					dogetratecode.executeSelect(null);
					if (dogetratecode.getSize() > 0)
					{
						Object ratecode = dogetratecode.getDfRateCode();

						//logger.error("--- Toni ---setRateCode  ratecode == " + ratecode.toString() );
						if (ratecode != null && ratecode.toString().trim().length() != 0)
              getCurrNDPage().setDisplayFieldValue(theFieldName, ratecode);
						else
              getCurrNDPage().setDisplayFieldValue(theFieldName, new String(""));
					}
					else return false;
				}
				else return false;
			}
			else return false;
		}
		catch(Exception e)
		{
			logger.error("Exception at setRateCode:" + e);		//#DG702
			return false;
		}

		return true;

	}


	//////////////////////////////////////////////////////////////////

	public void setBridgeProductName(String theFieldName)
	{

		//	  	PageEntry pg = getTheSessionState().getCurrentPage();
		//		Hashtable pst = pg.getPageStateTable();
		try
		{
			doMainViewBridgeModelImpl domainviewbridge =(doMainViewBridgeModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doMainViewBridgeModel.class));
      //--> Shouldn't execute again -- By Billy 19Aug2002
			//domainviewbridge.executeSelect(null);
			if(domainviewbridge.getSize() > 0)
			{
				Object pricingprofileid = domainviewbridge.getDfBridgePricingProfileId();

				//logger.error("--- Toni ---setBridgeProductName  pricingprofileid == " + pricingprofileid.toString() );
				if (pricingprofileid != null && pricingprofileid.toString().trim().length() != 0)
				{
					doGetMortgageProductNameModelImpl dogetproduct =(doGetMortgageProductNameModelImpl)
            (RequestManager.getRequestContext().getModelManager().getModel(doGetMortgageProductNameModel.class));
					dogetproduct.clearUserWhereCriteria();
					dogetproduct.addUserWhereCriterion("dfMortgageProductId", "=", pricingprofileid);
					
					logger.debug("--- ANU --- dogetproduct == " + dogetproduct.toString());
					
					dogetproduct.executeSelect(null);
					if (dogetproduct.getSize() > 0)
					{
						Object product = dogetproduct.getDfMortgageProductName();

						//logger.error("--- Toni ---setBridgeProductName  product == " + product.toString() );
						getCurrNDPage().setDisplayFieldValue(theFieldName, product);
					}
				}
			}
		}
		catch(Exception e)
		{
			logger.error("Exception at setBridgeProductName:" + e);		//#DG702
		}
	}


	///////////////////////////////////////////////////////////////////////
	public String setReferenceType()
	{
		doMainViewDealModelImpl domainviewdeal =(doMainViewDealModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doMainViewDealModel.class));
    try
    {
      if (domainviewdeal.getSize() > 0)
      {
        Object val = domainviewdeal.getDfReferenceDealTypeId();
        if (val != null && val.toString().trim().length() != 0)
        {
          doGetReferenceDealTypeModelImpl dogetrefdealtype =(doGetReferenceDealTypeModelImpl)
              (RequestManager.getRequestContext().getModelManager().getModel(doGetReferenceDealTypeModel.class));
          dogetrefdealtype.clearUserWhereCriteria();
          dogetrefdealtype.addUserWhereCriterion("dfReferenceDealTypeId", "=", val);
          
          logger.debug("--- ANU --- dogetrefdealtype == " + dogetrefdealtype.toString());
          
          dogetrefdealtype.executeSelect(null);
          if (dogetrefdealtype.getSize() > 0)
          {
            val = dogetrefdealtype.getDfReferenceDealTypeDesc();
            if (val != null && val.toString().trim().length() != 0)
              return val.toString();
            else
              return "";
          }
          else return "";
        }
        else return "";
      }
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.setReferenceType :: " + e.toString());
    }
    return "";
	}


	//////////////////////////////////////////////////////////////////

	public void setBridgeRateCode(String theFieldName)
	{

		//	  	PageEntry pg = getTheSessionState().getCurrentPage();
		//		Hashtable pst = pg.getPageStateTable();
		try
		{
			doMainViewBridgeModelImpl domainviewbridge =(doMainViewBridgeModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doMainViewBridgeModel.class));
			//domainviewbridge.execute();
      if (domainviewbridge.getSize() > 0)
			{
				Object pricingprofileid = domainviewbridge.getDfBridgePricingProfileId();

				//logger.error("--- Toni --- setBridgeRateCode  pricingprofileid == " + pricingprofileid.toString() );
				if (pricingprofileid != null && pricingprofileid.toString().trim().length() != 0)
				{
					doGetRateCodeModelImpl dogetratecode =(doGetRateCodeModelImpl)
              (RequestManager.getRequestContext().getModelManager().getModel(doGetRateCodeModel.class));
					dogetratecode.clearUserWhereCriteria();

					// Bug fixed by BILLY -- use Pricing Rate Inventory Id instaed
					dogetratecode.addUserWhereCriterion("dfPricingRateInventoryId", "=", pricingprofileid);
					
					logger.debug("--- ANU --- dogetratecode == " + dogetratecode.toString());
					
					dogetratecode.executeSelect(null);
					if (dogetratecode.getSize() > 0)
					{
						Object ratecode = dogetratecode.getDfRateCode();

						//logger.error("--- Toni --- setBridgeRateCode  ratecode == " + ratecode.toString() );
						getCurrNDPage().setDisplayFieldValue(theFieldName, ratecode);
					}
				}
			}
		}
		catch(Exception e)
		{
			logger.error("Exception at setBridgeRateCode:" + e);		//#DG702
			return;
		}

	}


	//////////////////////////////////////////////////////////////////

	public void setPropertyDetails(TiledView repeated, int rowindex)
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			Vector vpropertyid = null;

			//logger.error("--- Toni ---setPropertyDetails rowindex == " + rowindex);
			//			pst.put("APPRAISERROWINDEX",new Integer(rowindex));
			Object obj = pst.get(VPROPERTYID);
			if (obj instanceof Vector) vpropertyid =(Vector) obj;
			else vpropertyid = new Vector();

			//logger.error("--- Toni ---setPropertyDetails VPROPERTYID.size == " + vpropertyid.size());
			doViewAppraiserInfoModelImpl dodetailviewappraiserinfo =(doViewAppraiserInfoModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doViewAppraiserInfoModel.class));
			dodetailviewappraiserinfo.clearUserWhereCriteria();
			dodetailviewappraiserinfo.addUserWhereCriterion("dfDealId", "=", new Integer(pg.getPageDealId()));
			dodetailviewappraiserinfo.addUserWhereCriterion("dfCopyId", "=", new Integer(pg.getPageDealCID()));

			//logger.error("--- Toni ---setPropertyDetails dealid == " + pg.getPageDealId() + " copyid == " + pg.getPageDealCID());
			//logger.error("--- Toni ---setPropertyDetails VPROPERTYID at rowindex == " + rowindex + " value == "
			//					+ ((Integer)TypeConverter.asInt(vpropertyid.elementAt(rowindex))) );
			dodetailviewappraiserinfo.addUserWhereCriterion("dfPropertyId", "=", new Integer(com.iplanet.jato.util.TypeConverter.asInt(vpropertyid.elementAt(rowindex))));
			
			logger.debug("--- ANU --- dogetratecode == " + dodetailviewappraiserinfo.toString());
			
			dodetailviewappraiserinfo.executeSelect(null);
			if (dodetailviewappraiserinfo.getSize() >= 1)
			{
				Object appraiserfirstname = dodetailviewappraiserinfo.getDfAppraiserFirstName();
				Object appraiserlastname = dodetailviewappraiserinfo.getDfAppraiserLastName();
				Object appraiserinitial = dodetailviewappraiserinfo.getDfAppraiserInitial();
				Object appraiserphonenum = dodetailviewappraiserinfo.getDfAppraiserPhoneNumber();
				Object appraiserfaxnum = dodetailviewappraiserinfo.getDfAppraiserFaxNumber();
				Object appraiserphoneext = dodetailviewappraiserinfo.getDfAppraiserPhoneExtension();
				Object appraiseraddr = dodetailviewappraiserinfo.getDfAppraiserAddr1();
				repeated.setDisplayFieldValue(pgDealSummaryRepeatedPropertyDetailsTiledView.CHILD_STPDAPPRAISERFISTNAME, appraiserfirstname);
				repeated.setDisplayFieldValue(pgDealSummaryRepeatedPropertyDetailsTiledView.CHILD_STPDAPPRAISERLASTNAME, appraiserlastname);
				repeated.setDisplayFieldValue(pgDealSummaryRepeatedPropertyDetailsTiledView.CHILD_STPDAPPRAISERINITIAL, appraiserinitial);
				repeated.setDisplayFieldValue(pgDealSummaryRepeatedPropertyDetailsTiledView.CHILD_STPDAPPRAISALPHONE, appraiserphonenum);
				repeated.setDisplayFieldValue(pgDealSummaryRepeatedPropertyDetailsTiledView.CHILD_STPDAPPRAISERFAX, appraiserfaxnum);
				repeated.setDisplayFieldValue(pgDealSummaryRepeatedPropertyDetailsTiledView.CHILD_STPDAPPPHONEEXT, appraiserphoneext);
				repeated.setDisplayFieldValue(pgDealSummaryRepeatedPropertyDetailsTiledView.CHILD_STPDAPPRAISERADDRESS, appraiseraddr);
			}
		}
		catch(Exception e)
		{
			logger.error("Exception at setting borrower Assets:" + e);		//#DG702
			return;
		}

	}


	////////////////////////////////////////////////////////////////////

	public void setupNumOfRepeatedPropertyDetails(TiledView repeated1)
	{
		try
		{
			doDetailViewPropertiesModelImpl listOfProperties =(doDetailViewPropertiesModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewPropertiesModel.class));

			//logger.error("--- Toni ---setupNumOfRepeatedPropertyDetails  resultbl.getNumRows() == " + listOfProperties.getNumRows());
			//if (listOfProperties.getNumRows() == 0) return CSpPage.SKIP;
			repeated1.setMaxDisplayTiles(listOfProperties.getSize());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupNumOfRepeatedPropertyDetails");
			logger.error(e);
		}
		//return CSpPage.PROCEED;
	}


	/////////////////////////////////////////////////////////////////

	public void setupNumOfRepeatedPropertyAddressAndExpenses(TiledView repeated1)
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			Vector results = null;
			Object obj = pst.get(VPROPERTYID);
			if (obj instanceof Vector) results =(Vector) obj;
			else results = new Vector();

			//logger.error("--- Toni ---setupNumOfRepeatedPropertyAddressAndExpenses VPROPERTYID.size == " + results.size());
			//if (results.size() == 0) return CSpPage.SKIP;
			repeated1.setMaxDisplayTiles(results.size());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupNumOfRepeatedPropertyAddressAndExpenses");
			logger.error(e);
		}
		//return CSpPage.PROCEED;
	}


	//////////////////////////////////////////////////////////////////

	public void setupNumOfRepeatedPropertyExpenses(TiledView repeated1)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();

		try
		{
			//--> The following moved to method setExpenses
      //--> By Billy 22Aug2002
      /*
      Vector vpropertyid =(Vector) pst.get("VPROPERTYID");
			Integer rowindex =(Integer) pst.get("EXPROWINDEX");
			doDetailViewPropertyExpensesModelImpl listOfExpenses =(doDetailViewPropertyExpensesModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewPropertyExpensesModel.class));
			listOfExpenses.clearUserWhereCriteria();
			listOfExpenses.addUserWhereCriterion("dfPropertyId", "=", new Integer(com.iplanet.jato.util.TypeConverter.asInt(vpropertyid.elementAt(rowindex.intValue()))));
			listOfExpenses.addUserWhereCriterion("dfCopyId", "=", new Integer(pg.getPageDealCID()));
			listOfExpenses.executeSelect(null);
      pst.put("NUMOFEXPENSES", new Integer(listOfExpenses.getSize()));
      */

      //repeated1.setMaxDisplayTiles(listOfExpenses.getSize());
			Integer rowindex =(Integer) pst.get(NUMOFEXPENSES);
      if (rowindex == null)
      {
			  //logger.debug("BILLY ===> NUMOFEXPENSES == null :: default to 0");
        repeated1.setMaxDisplayTiles(0);
      }
      else
      {
        //logger.debug("BILLY ===> NUMOFEXPENSES == " + rowindex.intValue());
        repeated1.setMaxDisplayTiles(rowindex.intValue());
      }
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupNumOfRepeatedPropertyExpenses");
			logger.error(e);
		}

		//return CSpPage.PROCEED;

	}


	/////////////////////////////////////////////////////////////////
	private void populateBorrowerDetails()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		Vector vborrowerid = new Vector();
		Vector vpropertyid = new Vector();

		try
		{
			if (pst == null)
			{
				pst = new Hashtable();
				pg.setPageStateTable(pst);
			}
			doDetailViewBorrowersModelImpl listOfBorrowers =(doDetailViewBorrowersModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewBorrowersModel.class));
			
			logger.debug("--- ANU --- listOfBorrowers == " + listOfBorrowers.toString());
			
			listOfBorrowers.executeSelect(null);

			while(listOfBorrowers.next())
			{
				//logger.debug("--- Toni --- elementat i = " + i +  " =" +TypeConverter.asInt(resultbl.getValue(i,1)));
				vborrowerid.addElement(
          new Integer(com.iplanet.jato.util.TypeConverter.asInt(listOfBorrowers.getValue(listOfBorrowers.FIELD_DFBORROWERID))));
			}
			pst.put(VBORROWERID, vborrowerid);

			// Construct vector of the propertyids.
			doDetailViewPropertiesModelImpl listOfProperties =(doDetailViewPropertiesModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewPropertiesModel.class));
			
			logger.debug("--- ANU --- listOfProperties == " + listOfBorrowers.toString());
			
			listOfProperties.executeSelect(null);

			while(listOfProperties.next())
			{
				vpropertyid.addElement(
          new Integer(com.iplanet.jato.util.TypeConverter.asInt(listOfProperties.getValue(listOfProperties.FIELD_DFPROPERTYID))));
			}
			pst.put(VPROPERTYID, vpropertyid);
		}
		catch(Exception e)
		{
			logger.error("Exception at getting Property IDs Vector:" + e);		//#DG702
		}

	}


	//////////////////////////////////////////////////////////////////
	public boolean setupNumOfRepeatedBorrowerDetails(TiledView repeated1)
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			Vector results = null;
			Object obj = pst.get(VBORROWERID);
			if (obj instanceof Vector) results =(Vector) obj;
			else results = new Vector();

			//logger.error("--- Toni ---setupNumOfRepeatedBorrowerDetails vborrowerid.size == " + results.size());
			if (results.size() == 0) return false;
			repeated1.setMaxDisplayTiles(results.size());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupNumOfRepeatedBorrowerDetails");
			logger.error(e);
		}
		return true;
	}

	//////////////////////////////////////////////////////////////////
	public boolean setupNumOfRepeatedIncomeDetails(TiledView repeated1)
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			Vector results = null;
			Object obj = pst.get(VBORROWERID);
			if (obj instanceof Vector) results =(Vector) obj;
			else results = new Vector();

			//logger.error("--- Toni ---setupNumOfRepeatedIncomeDetails  vborrowerid.size == " + results.size());
			if (results.size() == 0) return false;
			repeated1.setMaxDisplayTiles(results.size());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupNumOfRepeatedIncomeDetails");
			logger.error(e);
		}
		return true;
	}


	//////////////////////////////////////////////////////////////////
	public boolean setupNumOfRepeatedAssetDetails(TiledView repeated1)
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			Vector results = null;
			Object obj = pst.get(VBORROWERID);
			if (obj instanceof Vector) results =(Vector) obj;
			else results = new Vector();

			//logger.error("--- Toni ---setupNumOfRepeatedIncomeDetails  vborrowerid.size == " + results.size());
			if (results.size() == 0) return false;
			repeated1.setMaxDisplayTiles(results.size());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupNumOfRepeatedIncomeDetails");
			logger.error(e);
		}
		return true;
	}


	//////////////////////////////////////////////////////////////////
	public boolean setupNumOfRepeatedLiabDetails(TiledView repeated1)
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			Vector results = null;
			Object obj = pst.get(VBORROWERID);
			if (obj instanceof Vector) results =(Vector) obj;
			else results = new Vector();

			//logger.error("--- Toni ---setupNumOfRepeatedIncomeDetails  vborrowerid.size == " + results.size());
			if (results.size() == 0) return false;
			repeated1.setMaxDisplayTiles(results.size());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupNumOfRepeatedIncomeDetails");
			logger.error(e);
		}
		return true;
	}


	////////////////////////////////////////////////////////////////////
	public boolean setupNumOfRepeatedApplicantAddress(TiledView repeated1)
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			Integer noOfRows = null;
			Object obj = pst.get(NUMOFADDRESS);
			if (obj instanceof Integer) noOfRows =(Integer) obj;
			else noOfRows = new Integer(0);

			//logger.error("--- Toni ---setupNumOfRepeatedApplicantAddress  noOfRows == " + noOfRows);
			if(noOfRows.intValue() == 0) return false;
      repeated1.setMaxDisplayTiles(noOfRows.intValue());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupNumOfRepeatedApplicantAddress");
			logger.error(e);
		}
		return true;
	}
	public boolean setupNumOfRepeatedApplicantIdentifications(TiledView repeated1)
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			Integer noOfRows = null;
			Object obj = pst.get(NUMOFIDENTIFICATIONS);
			if (obj instanceof Integer) noOfRows =(Integer) obj;
			else noOfRows = new Integer(0);

			//logger.error("--- Toni ---setupNumOfRepeatedApplicantAddress  noOfRows == " + noOfRows);
			if(noOfRows.intValue() == 0) return false;
      repeated1.setMaxDisplayTiles(noOfRows.intValue());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupNumOfRepeatedApplicantAddress");
			logger.error(e);
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////
	public boolean setupNumOfRepeatedEmployment(TiledView repeated1)
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			Integer noOfRows = null;
			Object obj = pst.get(NUMOFEMPLOYMENT);
			if (obj instanceof Integer) noOfRows =(Integer) obj;
			else noOfRows = new Integer(0);

			//logger.error("--- Toni ---setupNumOfRepeatedEmployment  noOfRows == " + noOfRows);
			if(noOfRows.intValue() == 0) return false;
      repeated1.setMaxDisplayTiles(noOfRows.intValue());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupNumOfRepeatedEmployment");
			logger.error(e);
		}
		return true;
	}


	////////////////////////////////////////////////////////////////////
	public boolean setupNumOfRepeatedOtherIncome(TiledView repeated1)
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			Integer noOfRows = null;
			Object obj = pst.get(NUMOFOTHERINCOME);
			if (obj instanceof Integer) noOfRows =(Integer) obj;
			else noOfRows = new Integer(0);

			//logger.error("--- Toni ---setupNumOfRepeatedOtherIncome  noOfRows == " + noOfRows);
			if(noOfRows.intValue() == 0) return false;
      repeated1.setMaxDisplayTiles(noOfRows.intValue());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupNumOfRepeatedOtherIncome");
			logger.error(e);
		}
		return true;
	}


	////////////////////////////////////////////////////////////////////
	public boolean setupNumOfRepeatedLiability(TiledView repeated1)
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			Integer noOfRows = null;
			Object obj = pst.get(NUMOFLIABILITY);
			if (obj instanceof Integer) noOfRows =(Integer) obj;
			else noOfRows = new Integer(0);

			//logger.error("--- Toni ---setupNumOfRepeatedLiability  noOfRows == " + noOfRows);
			if(noOfRows.intValue() == 0) return false;
      repeated1.setMaxDisplayTiles(noOfRows.intValue());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupNumOfRepeatedLiability");
			logger.error(e);
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////
	public boolean setupNumOfRepeatedAssets(TiledView repeated1)
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			Integer noOfRows = null;
			Object obj = pst.get(NUMOFASSET);
			if (obj instanceof Integer) noOfRows =(Integer) obj;
			else noOfRows = new Integer(0);

			//logger.error("--- Toni ---setupNumOfRepeatedAssets  noOfRows == " + noOfRows);
			if(noOfRows.intValue() == 0) return false;
      repeated1.setMaxDisplayTiles(noOfRows.intValue());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupNumOfRepeatedAssets");
			logger.error(e);
		}

		return true;

	}


	////////////////////////////////////////////////////////////////////
	public boolean setupNumOfRepeatedCreditRef(TiledView repeated1)
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			Integer noOfRows = null;
			Object obj = pst.get(NUMOFCREDITREF);
			if (obj instanceof Integer) noOfRows =(Integer) obj;
			else noOfRows = new Integer(0);

			//logger.error("--- Toni ---setupNumOfRepeatedCreditRef  noOfRows == " + noOfRows);
			if(noOfRows.intValue() == 0) return false;
      repeated1.setMaxDisplayTiles(noOfRows.intValue());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupNumOfRepeatedCreditRef");
			logger.error(e);
		}
		return true;
	}


	////////////////////////////////////////////////////////////////////
	public boolean setupNumOfRepeatedGDS(TiledView repeated1)
	{
		try
		{
			doDetailViewGDSModelImpl listOfGDS =(doDetailViewGDSModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewGDSModel.class));
      //--> need to investigate whether needs to execute the Modle here ???
      //--> Note by Billy 20Aug2002
			
			logger.debug("--- ANU --- listOfGDS == " + listOfGDS.toString());
			
			listOfGDS.executeSelect(null);
      //===================================================================

			//logger.error("--- Toni ---setupNumOfRepeatedGDS  listOfGDS.getNumRows() == " + listOfGDS.getNumRows());
			if (listOfGDS.getSize() == 0) return false;
			repeated1.setMaxDisplayTiles(listOfGDS.getSize());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupNumOfRepeatedGDS");
			logger.error(e);
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////
	public boolean setupNumOfRepeatedTDS(TiledView repeated1)
	{
		try
		{
			doDetailViewTDSModelImpl listOfTDS =(doDetailViewTDSModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewTDSModel.class));
      //--> need to investigate whether needs to execute the Modle here ???
      //--> Note by Billy 20Aug2002
			//listOfTDS.execute();
      //==================================================================

			//logger.error("--- Toni ---setupNumOfRepeatedTDS  listOfTDS.getNumRows() == " + listOfTDS.getNumRows());
			if (listOfTDS.getSize() == 0) return false;
			repeated1.setMaxDisplayTiles(listOfTDS.getSize());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupNumOfRepeatedTDS");
			logger.error(e);
		}
		return true;
	}

//4.4 Submission Agent
	public boolean setupNumOfRepeatedSOB(TiledView repeated1)
	{
		try
		{
			doUWSourceDetailsModelImpl listOfSOBs =(doUWSourceDetailsModelImpl)
	          (RequestManager.getRequestContext().getModelManager().getModel(doUWSourceDetailsModelImpl.class));

			if(listOfSOBs.getSize() == 0) return false;
				repeated1.setMaxDisplayTiles(listOfSOBs.getSize());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupNumOfRepeatedSOB");
			logger.error(e);
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////
	public int getMeBridgeId(SessionResourceKit srk, int dealid, int copyid)
	{
		int id = - 1;

		try
		{
			JdbcExecutor jExec = srk.getJdbcExecutor();
			int key = jExec.execute("SELECT BRIDGE.BRIDGEID FROM BRIDGE WHERE DEALID  = " + dealid + " AND BRIDGE.COPYID = " + copyid);
			while(jExec.next(key))
			{
				id = jExec.getInt(key, 1);
				break;
			}
			jExec.closeData(key);
		}
		catch(Exception e)
		{
			logger.error("@DealSummaryHandler.getMeBridgeId Exception at getting bridgeId:" + e);		//#DG702
		}
		return id;
	}


	/*************************************************************************
 * Hide Or Display Panels.
 **************************************************************************/	/////////////////////////////////////////////////////////////////////////////

	public String generateOrHideBeginBridgeSect()
	{
		doMainViewBridgeModelImpl domainviewbridge =(doMainViewBridgeModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doMainViewBridgeModel.class));

    try
    {
      //logger.debug("--- Toni --- Bridge1 No of ROws == " + domainviewbridge.getNumRows());
      if (domainviewbridge.getSize() <= 0)
        return supresTagStart;
      else
        return "";
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.generateOrHideBeginBridgeSect :: " + e.toString());
      return "";
    }
	}

	/////////////////////////////////////////////////////////////////////////////
	public String generateOrHideEndBridgeSect()
	{
		doMainViewBridgeModelImpl domainviewbridge =(doMainViewBridgeModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doMainViewBridgeModel.class));

    try
    {
      //logger.debug("--- Toni --- Bridge2 No of ROws == " + domainviewbridge.getNumRows());
      if (domainviewbridge.getSize() <= 0)
        return supresTagEnd;
      else
        return "";
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.generateOrHideEndBridgeSect :: " + e.toString());
      return "";
    }
	}

	// SEAN Ticket #1681 June 27, 2005: hide print deal summary bt for DJ.
	public String generateOrHideBeginPrintDealSummary()	{

		if (PropertiesCache.getInstance().
			getProperty(theSessionState.getDealInstitutionId(),COM_BASIS100_CALC_ISDJCLIENT, "N").equals("Y"))
			return supresTagStart;	 // for DJ, hide this button.
		else
			return "";	// do nothing.
	}

	public String generateOrHideEndPrintDealSummary() {

		if (PropertiesCache.getInstance().
			getProperty(theSessionState.getDealInstitutionId(),COM_BASIS100_CALC_ISDJCLIENT, "N").equals("Y"))
			return supresTagStart;	 // for DJ, hide this button.
		else
			return "";	// do nothing.
	}
	// SEAN Ticket #1681 END

	///////////////////////////////////////////////////////////////////////////////////
	public String generateOrHideBeginEscrowSect()
	{
		doMainViewEscrowModelImpl domainviewescrow =(doMainViewEscrowModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doMainViewEscrowModel.class));

    try
    {
      //logger.debug("--- Toni --- Escrow1 No of ROws == " + domainviewescrow.getNumRows());
      if (domainviewescrow.getSize() <= 0)
        return supresTagStart;
      else
        return "";
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.generateOrHideBeginEscrowSect :: " + e.toString());
      return "";
    }
	}


	///////////////////////////////////////////////////////////////////////////////////
	public String generateOrHideEndEscrowSect()
	{
		doMainViewEscrowModelImpl domainviewescrow =(doMainViewEscrowModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doMainViewEscrowModel.class));

    try
    {
      //logger.debug("--- Toni --- Escrow2 No of ROws == " + domainviewescrow.getNumRows());
      if (domainviewescrow.getSize() <= 0)
        return supresTagEnd;
      else
        return "";
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.generateOrHideEndEscrowSect :: " + e.toString());
      return "";
    }
	}

	//////////////////////////////////////////////////////////////////
	public String generateOrHideBeginAssetSect()
	{
		doDetailViewAssetsModelImpl listofassets =(doDetailViewAssetsModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewAssetsModel.class));

    try
    {
      //logger.debug("--- Toni --- Asset1 No of ROws == " + listofassets.getNumRows());
      if (listofassets.getSize() <= 0)
        return supresTagStart;
      else
        return "";
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.generateOrHideBeginAssetSect :: " + e.toString());
      return "";
    }
	}

	//////////////////////////////////////////////////////////////////
	public String generateOrHideEndAssetSect()
	{
		doDetailViewAssetsModelImpl listofassets =(doDetailViewAssetsModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewAssetsModel.class));

    try
    {
      //logger.debug("--- Toni --- Asset2 No of ROws == " + listofassets.getNumRows());
      if (listofassets.getSize() <= 0)
        return supresTagEnd;
      else
        return "";
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.generateOrHideEndAssetSect :: " + e.toString());
      return "";
    }
	}

	//////////////////////////////////////////////////////////////////
	public String generateOrHideBeginCredRefSect()
	{
		doDetailViewCreditRefsModelImpl listcreditrefs =(doDetailViewCreditRefsModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewCreditRefsModel.class));

    try
    {
      //logger.debug("--- Toni --- CredRef1 No of ROws == " + listcreditrefs.getNumRows());
      if (listcreditrefs.getSize() <= 0)
        return supresTagStart;
      else
        return "";
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.generateOrHideBeginCredRefSect :: " + e.toString());
      return "";
    }
	}

	//////////////////////////////////////////////////////////////////
	public String generateOrHideEndCredRefSect()
	{
		doDetailViewCreditRefsModelImpl listcreditrefs =(doDetailViewCreditRefsModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewCreditRefsModel.class));

    try
    {
      //logger.debug("--- Toni --- CredRef2 No of ROws == " + listcreditrefs.getNumRows());
      if (listcreditrefs.getSize() <= 0)
        return supresTagEnd;
      else
        return "";
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.generateOrHideEndCredRefSect :: " + e.toString());
      return "";
    }
	}

	//////////////////////////////////////////////////////////////////
	public String generateOrHideBeginEmpSect()
	{
		doDetailViewEmploymentModelImpl listEmployments =(doDetailViewEmploymentModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewEmploymentModel.class));

    try
    {
      //logger.debug("--- Toni --- Emp1 No of ROws == " + listEmployments.getNumRows());
      if (listEmployments.getSize() <= 0)
        return supresTagStart;
      else
        return "";
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.generateOrHideBeginEmpSect :: " + e.toString());
      return "";
    }
	}

	//////////////////////////////////////////////////////////////////
	public String generateOrHideEndEmpSect()
	{
		doDetailViewEmploymentModelImpl listEmployments =(doDetailViewEmploymentModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewEmploymentModel.class));

    try
    {
      //logger.debug("--- Toni --- Emp2 No of ROws == " + listEmployments.getNumRows());
      if (listEmployments.getSize() <= 0)
        return supresTagEnd;
      else
        return "";
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.generateOrHideEndEmpSect :: " + e.toString());
      return "";
    }
	}

	//////////////////////////////////////////////////////////////////
	public String generateOrHideBeginOtherIncSect()
	{
		doDetailViewEmpOtherIncomeModelImpl listotherincome =(doDetailViewEmpOtherIncomeModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewEmpOtherIncomeModel.class));

    try
    {
      //logger.debug("--- Toni --- OtherIncome1 No of ROws == " + listotherincome.getNumRows());
      //--> ???
      //-->listotherincome.execute();
      if (listotherincome.getSize() <= 0)
        return supresTagStart;
      else
        return "";
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.generateOrHideBeginOtherIncSect :: " + e.toString());
      return "";
    }
	}

	//////////////////////////////////////////////////////////////////
	public String generateOrHideEndOtherIncSect()
	{
		doDetailViewEmpOtherIncomeModelImpl listotherincome =(doDetailViewEmpOtherIncomeModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewEmpOtherIncomeModel.class));

    try
    {
      //logger.debug("--- Toni --- OtherIncome2 No of ROws == " + listotherincome.getNumRows());
      //--> ???
      //-->listotherincome.execute();
      if (listotherincome.getSize() <= 0)
        return supresTagEnd;
      else
        return "";
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.generateOrHideEndOtherIncSect :: " + e.toString());
      return "";
    }
	}

	//////////////////////////////////////////////////////////////////
	public String generateOrHideBeginLiabSect()
	{
		doDetailViewLiabilitiesModelImpl listofliabilities =(doDetailViewLiabilitiesModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewLiabilitiesModel.class));

    try
    {
		//logger.debug("--- Toni --- liab1 No of ROws == " + listofliabilities.getNumRows());
		if (listofliabilities.getSize() <= 0)
      return supresTagStart;
    else
      return "";
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.generateOrHideBeginLiabSect :: " + e.toString());
      return "";
    }
	}

	//////////////////////////////////////////////////////////////////
	public String generateOrHideEndLiabSect()
	{
		doDetailViewLiabilitiesModelImpl listofliabilities =(doDetailViewLiabilitiesModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewLiabilitiesModel.class));

    try
    {
      //logger.debug("--- Toni --- liab2 No of ROws == " + listofliabilities.getNumRows());
      if (listofliabilities.getSize() <= 0)
        return supresTagEnd;
      else
        return "";
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.generateOrHideEndLiabSect :: " + e.toString());
      return "";
    }
	}

	///////////////////////////////////////////////////////////////////////
  //--> Re-written by Billy 20Aug2002
	public String generateOrHideBeginPropExpSect()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		Integer noOfRows = null;

		Object obj = pst.get(NUMOFEXPENSES);

		if (obj instanceof Integer) noOfRows =(Integer) obj;
		else noOfRows = new Integer(0);

		//logger.debug("BILLY ===> Begin Expenses No of ROws == " + noOfRows.intValue());
    if (noOfRows.intValue() <= 0)
      return supresTagStart;
    else
      return "";
	}


	///////////////////////////////////////////////////////////////////////
	public String generateOrHideEndPropExpSect()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		Integer noOfRows = null;

		Object obj = pst.get(NUMOFEXPENSES);

		if (obj instanceof Integer) noOfRows =(Integer) obj;
		else noOfRows = new Integer(0);

		//logger.debug("BILLY ===> End Expenses No of ROws == " + noOfRows.intValue());
		if(noOfRows.intValue() <= 0)
      return supresTagEnd;
    else
      return "";
	}

	///////////////////////////////////////////////////////////////////////
	public void handlePrintDealSummary()
	{
		SessionResourceKit srk = getSessionResourceKit();
		PageEntry pg = getTheSessionState().getCurrentPage();

		try
		{
			//UserProfile up = null;
			//String emailto = null;
			//int profileId = -1;
			//int userId = srk.getUserProfileId();
			//up = new UserProfile(srk);
			//logger.debug("--- Toni --- ++++++++++++++ handlePrintDealSummary +++++++++++ " );
			//logger.debug("--- Toni --- userProfileId === " + srk.getUserProfileId() );
			//up.findByPrimaryKey(new UserProfileBeanPK(userId));
			//Contact cont = up.getContact();
			//emailto = cont.getContactEmailAddress();
			//logger.debug("--- Toni --- contactId === " + up.getContactId() );
			//logger.debug("--- Toni --- emailto === " + emailto);
			Deal d = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
			DocumentRequest dr = DocumentRequest.requestDealSummary(srk, d);

			//navigateToNextPage();
		}
		catch(Exception e)
		{
			logger.error("Exception occurred @DealSummaryHandler.handlePrintDealSummary() (printing the Deal Summary");
			logger.error(e);
			setStandardFailMessage();
			return;
		}

	}


	///////////////////////////////////////////////////////////////////////

	public void handlePrintDealSummaryLite()
	{
		SessionResourceKit srk = getSessionResourceKit();

		PageEntry pg = getTheSessionState().getCurrentPage();

		try
		{
			Deal d = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
			DocumentRequest dr = DocumentRequest.requestDealSummaryLite(srk, d);

			//navigateToNextPage();
		}
		catch(Exception e)
		{
			logger.error("Exception occurred @DealSummaryHandler.handlePrintDealSummaryLite()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}

	}


	/////////////////////////////////////////////////////////////////////

	public void handleGoToPartySummaryPage()
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			PageEntry pgEntry = setupSubPagePageEntry(pg, Sc.PGNM_PARTY_SUMMARY, true);

			// Set condition to hide the buttons in order to prevent user to update
			pgEntry.setPageCondition3(true);
			pgEntry.setPageCondition4(true);
			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage();
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.handleGoToPartySearchPage: while navigating to the Party Summary/Search subpage");
			logger.error(e);
		}

	}
	
        /**
         * Handler method to generate decline letter by calling DocPrep
         */
        public void handleGenerateDeclineLetter() {
          SessionResourceKit srk = getSessionResourceKit();
          PageEntry pg = getTheSessionState().getCurrentPage();
          try
          {
            Deal d = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
            
            //Ticket $4414
            int profileId = Dc.DOCUMENT_GENERAL_TYPE_RATE_BONUS_MTG_LETTER;
            if(d.getStatusId() == Mc.DEAL_DENIED)
            	profileId = Dc.DOCUMENT_GENERAL_TYPE_DECLINED_MTG_LETTER;
            DocumentRequest dr = DocumentRequest.requestDocByProfile(srk, d, profileId);
          }
          catch(Exception e)
          {
                  logger.error("Exception occurred @DealSummaryHandler.handleGenerateDeclineLetter()");
                  logger.error(e);
                  setStandardFailMessage();
                  return;
          }
        }


	///////////////////////////////////////////////////////////////////////
	public String setXExtension(String currVal)
	{
		doMainViewBorrowerModelImpl dodetailviewborrs =(doMainViewBorrowerModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doMainViewBorrowerModel.class));
    try
    {
      if (dodetailviewborrs.getSize() > 0)
      {
        Object val = dodetailviewborrs.getDfWorkPhoneNumber();
        if (val != null && val.toString().trim().length() != 0)
        {
          val = dodetailviewborrs.getDfWorkPhoneExt();
          if (val != null && val.toString().trim().length() != 0)
            return ("x" + val.toString());
          else
            return "";
        }
        else return "";
      }
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.setXExtension :: " + e.toString());
    }
    return "";
	}

	///////////////////////////////////////////////////////////////////////
	public String setXPhoneExtInsideRepeated(String currVal)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();

		try
		{
      //--> Shouldn't necessary to get the current rowindex here.
      //--> As the modle's location should already set to the current row.
      //--> Modified by Billy 20Aug2002
			//-->Object obj = pst.get("BORADDROWINDEX");
			//-->if (obj instanceof Integer) rowindex =(Integer) obj;
			//-->else rowindex = new Integer(- 1);
			doDetailViewBorrowersModelImpl dodetailviewborrs =(doDetailViewBorrowersModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewBorrowersModel.class));
			if (dodetailviewborrs.getSize() > 0)
			{
				Object val = dodetailviewborrs.getDfWorkPhoneNo();

//logger.debug("BILLY ===> @setXPhoneExtInsideRepeated currRow# = " + dodetailviewborrs.getLocation() +
//    " currWorkPhone# = " + val);

        if (val != null && val.toString().trim().length() != 0)
        {
          val = dodetailviewborrs.getDfBorrowerWorkPhoneExt();
          if (val != null && val.toString().trim().length() != 0)
            return ("x" + val.toString());
          else return "";
        }
        else return "";
			}
			else return "";
		}
		catch(Exception e)
		{
			logger.error("Exception @DealSummaryHandler.setXPhoneExtInsideRepeated");
			logger.error(e);
			return "";
		}
	}


	///////////////////////////////////////////////////////////////////////
	public String setXPhoneExtInsideEmpRepeated(String currVal)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();

		try
		{
      //--> Shouldn't necessary to get the current rowindex here.
      //--> As the modle's location should already set to the current row.
      //--> Modified by Billy 20Aug2002
			//-->Object obj = pst.get("EMPROWINDEX");
			//-->if (obj instanceof Integer) rowindex =(Integer) obj;
			//-->else rowindex = new Integer(- 1);
			doDetailViewEmploymentModelImpl dodetailviewemp =(doDetailViewEmploymentModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewEmploymentModel.class));
			if (dodetailviewemp.getSize() > 0)
      {
        Object val = dodetailviewemp.getDfEmpWorkPhoneExt();
        if (val != null && val.toString().trim().length() != 0)
          return ("x" + val.toString());
        else return "";
      }
      else return "";
		}
		catch(Exception e)
		{
			logger.error("Exception @DealSummaryHandler.setXPhoneExtInsideEmpRepeated");
			logger.error(e);
			return "";
		}
	}


	////////////////////////////////////////////////////////////////////////
	public String setXPhoneExtInsidePropDetailRepeated(String currVal)
	{
		try
		{
			doViewAppraiserInfoModelImpl dodetailviewappraiser =(doViewAppraiserInfoModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doViewAppraiserInfoModel.class));
			if(dodetailviewappraiser.getSize() > 0)
			{
				Object val = dodetailviewappraiser.getDfAppraiserPhoneExtension();

				//logger.error(" --- Toni --- setXPhoneExtInsidePropDetailRepeated dfAppraiserPhoneExtension == " + val.toString());
				if (val != null && val.toString().trim().length() != 0)
          return ("x" + val.toString());
				else return "";
			}
			else return "";
		}
		catch(Exception e)
		{
			logger.error("Exception @DealSummaryHandler.setXPhoneExtInsidePropDetailRepeated");
			logger.error(e);
			return "";
		}
	}


	////////////////////////////////////////////////////////////////////////
	public String setXPhoneExtCommon(String currVal)
	{
		try
		{
			if (currVal != null && currVal.trim().length() != 0)
        return ("x" + currVal);
			else return "";
		}
		catch(Exception e)
		{
			logger.error("Exception @DealSummaryHandler.setXPhoneExtCommon");
			logger.error(e);
			return "";
		}
	}


	//////////////////////////////////////////////////////////////////////
	public String setRateToPercentage(String val)
	{
		DecimalFormat floatFmt = new DecimalFormat("#0.00");

		if (val != null && val.trim().length() != 0)
		{
      String fomated = "0";
      Double d = null;
			try{
       d = new Double(val);
       fomated = floatFmt.format(d);
      }
      catch(Exception e)
      {
        logger.warning("Decimal Format error @DealSummaryHandler.setRateToPercentage : Val = " + val
            + " DValue = " + d + " :: " + e.toString());
      }

			//logger.debug("--- Toz ---> fomated = " + fomated);
			return (fomated + "%");
		}
		return "";
	}

	//////////////////////////////////////////////////////////////////////
	protected int getIntValue(String sNum)
	{
		if (sNum == null) return 0;

		sNum = sNum.trim();

		if (sNum.length() == 0) return 0;

		try
		{
			// handle decimial (e.g. float type) representation
			int ndx = sNum.indexOf(".");
			if (ndx != - 1) sNum = sNum.substring(0, ndx);
			return Integer.parseInt(sNum.trim());
		}
		catch(Exception e)
		{
			;
		}
		return 0;
	}


	/////////////////////////////////////////////////////////////////////
	public String setUnderWriterName(String val)
	{
    try
    {
      if (! val.equals("") && val.trim().length() != 0)
      {
        doGetUnderWriterNameModelImpl dogetuwname =(doGetUnderWriterNameModelImpl)
            (RequestManager.getRequestContext().getModelManager().getModel(doGetUnderWriterNameModel.class));
        dogetuwname.clearUserWhereCriteria();
        dogetuwname.addUserWhereCriterion("dfUserProfileId", "=", val);
        logger.debug("----------Anu------ dogetuwname:: " + dogetuwname.toString());
        
        dogetuwname.executeSelect(null);
        if (dogetuwname.getSize() > 0)
        {
          return (dogetuwname.getDfUnderWriterName().toString() + ".");
        }
      }
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.setUnderWriterName :: " + e.toString());
    }
		return "";
	}


	/////////////////////////////////////////////////////////////////////////
	public String setLivingSpaceUnitOfMeasure(String currVal)
	{
		try
    {
      doDetailViewPropertiesModelImpl dogetlivingspace =(doDetailViewPropertiesModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewPropertiesModel.class));

      Object measureid = dogetlivingspace.getDfLivingSpaceUnitOfMeasureId();

      //logger.debug("--- Toni ---> measureid == " + measureid.toString());
      if (measureid != null && measureid.toString().trim().length() != 0)
      {
        doGetLivingSpaceUnitOfMeasureModelImpl doGetLivingSpaceMeasure =(doGetLivingSpaceUnitOfMeasureModelImpl)
            (RequestManager.getRequestContext().getModelManager().getModel(doGetLivingSpaceUnitOfMeasureModel.class));
        doGetLivingSpaceMeasure.clearUserWhereCriteria();
        doGetLivingSpaceMeasure.addUserWhereCriterion("dfLivingSpaceUnitOfMeasureId", "=", measureid);
        
        logger.debug("----------Anu------ doGetLivingSpaceMeasure:: " + doGetLivingSpaceMeasure.toString());
        doGetLivingSpaceMeasure.executeSelect(null);
        if (doGetLivingSpaceMeasure.getSize() > 0)
        {
          Object val = doGetLivingSpaceMeasure.getDfLivingSpaceUnitMeasureDesc();

          //logger.debug("--- Toni ---> livingspacedesc == " + val.toString());
          if (val != null)
          {
            return (val.toString());
          }
        }
        else
          return "";
      }
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.setLivingSpaceUnitOfMeasure :: " + e.toString());
    }
		return "";
	}

	/////////////////////////////////////////////////////////////////////////
	public String setLotSizeUnitOfMeasure(String currVal)
	{
		try
    {
      doDetailViewPropertiesModelImpl dogetlivingspace =(doDetailViewPropertiesModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewPropertiesModel.class));

      Object measureid = dogetlivingspace.getDfLotSizeUnitOfMeasureId();

      //logger.debug("--- Toni ---> measureid == " + measureid.toString());
      if (measureid != null && measureid.toString().trim().length() != 0)
      {
        doGetLotSizeUnitOfMeasureModelImpl doGetLotSizeMeasure =(doGetLotSizeUnitOfMeasureModelImpl)
            (RequestManager.getRequestContext().getModelManager().getModel(doGetLotSizeUnitOfMeasureModel.class));
        doGetLotSizeMeasure.clearUserWhereCriteria();
        doGetLotSizeMeasure.addUserWhereCriterion("dfLotSizeMeasureId", "=", measureid);
        
        logger.debug("----------Anu------ doGetLotSizeMeasure:: " + doGetLotSizeMeasure.toString());
        
        doGetLotSizeMeasure.executeSelect(null);
        if (doGetLotSizeMeasure.getSize() > 0)
        {
          Object val = doGetLotSizeMeasure.getDfLotSizeUnitMeasureDesc();

          //logger.debug("--- Toni ---> dfLotSizeUnitMeasureDesc == " + val.toString());
          if (val != null)
          {
            return (val.toString());
          }
        }
        else return "";
      }
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.setLotSizeUnitOfMeasure :: " + e.toString());
    }
		return "";
	}


	/////////////////////////////////////////////////////////////////////////
	public String setZoningDesc(String currVal)
	{
    try
    {
      //logger.debug("--- Toni ---> zoneid == " + zoneid.toString());
      if (currVal != null && currVal.toString().trim().length() != 0)
      {
        doGetZonningDescModelImpl dogetzonningdesc =(doGetZonningDescModelImpl)
            (RequestManager.getRequestContext().getModelManager().getModel(doGetZonningDescModel.class));
        dogetzonningdesc.clearUserWhereCriteria();
        dogetzonningdesc.addUserWhereCriterion("dfZoningId", "=", currVal);
        
        logger.debug("----------Anu------ dogetzonningdesc:: " + dogetzonningdesc.toString());
        
        
        dogetzonningdesc.executeSelect(null);
        if (dogetzonningdesc.getSize() > 0)
        {
          Object val = dogetzonningdesc.getDfZoningDesc();

          //logger.debug("--- Toni ---> dfLotSizeUnitMeasureDesc == " + val.toString());
          if (val != null)
          {
            return (val.toString());
          }
        }
        else return "";
      }
     }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.setZoningDesc :: " + e.toString());
    }
		return "";
	}

	////////////////////////////////////////////////////////////////////////////
	public void handleSubmitStandard()
	{
		PageEntry pg = theSessionState.getCurrentPage();
		SessionResourceKit srk = getSessionResourceKit();

		try
		{
			SysLogger logger = getSessionResourceKit().getSysLogger();

			//logger.debug("****** DealSummary - handleSubmitStandard :: IN");
			//getSavedPages().show(logger);
			srk.beginTransaction();
			standardAdoptTxCopy(pg, true);

			//logger.debug("****** DealSummary - handleSubmitStandard :: BEFORE NAVIGATE");
			//getSavedPages().show(logger);
			navigateToNextPage();

			//logger.debug("****** DealSummary - handleSubmitStandard :: AFTER NAVIGATE");
			//getSavedPages().show(logger);
			srk.commitTransaction();
		}
		catch(Exception e)
		{
			//logger.debug("****** DealSummary - handleSubmitStandard :: ON EXCEPTION");
			//getSavedPages().show(logger);
			srk.cleanTransaction();
			logger.error("Exception @DealSummaryHandler.handleSubmitStandard()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}
	}

	// =================================================================================
	// Added Credit Bureau Liability section -- Billy 13Nov2001
	//////////////////////////////////////////////////////////////////
	public String generateOrHideBeginCBLiabSect()
	{
		doDetailViewCBLiabilitiesModelImpl listofliabilities =(doDetailViewCBLiabilitiesModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewCBLiabilitiesModel.class));

    try
    {
      //logger.debug("--- Toni --- liab1 No of ROws == " + listofliabilities.getNumRows());
      if (listofliabilities.getSize() <= 0)
        return supresTagStart;
      else
        return "";
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.generateOrHideBeginCBLiabSect :: " + e.toString());
      return "";
    }
	}

	//////////////////////////////////////////////////////////////////
	public String generateOrHideEndCBLiabSect()
	{
		doDetailViewCBLiabilitiesModelImpl listofliabilities =(doDetailViewCBLiabilitiesModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doDetailViewCBLiabilitiesModel.class));

    try
    {
      //logger.debug("--- Toni --- liab2 No of ROws == " + listofliabilities.getNumRows());
      if (listofliabilities.getSize() <= 0)
        return supresTagEnd;
      else
        return "";
    }
    catch(Exception e)
    {
      logger.error("Exception @DealSummary.generateOrHideEndCBLiabSect :: " + e.toString());
      return "";
    }
	}

	////////////////////////////////////////////////////////////////////
	public boolean setupNumOfRepeatedCBLiability(TiledView repeated1)
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			Integer noOfRows = null;
			Object obj = pst.get(NUMOFCBLIABILITY);
			if (obj instanceof Integer) noOfRows =(Integer) obj;
			else noOfRows = new Integer(0);

			//logger.error("BILLY ==> setupNumOfRepeatedCBLiability  noOfRows == " + noOfRows);
			if(noOfRows.intValue() == 0) return false;
      repeated1.setMaxDisplayTiles(noOfRows.intValue());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupNumOfRepeatedCBLiability");
			logger.error(e);
      return false;
		}
		return true;
	}

	//Method to set display or not display the FinancingProgram filed based on the property definition
	private void setDisplayFinancingProgram(PageEntry pg, ViewBean thePage)
	{
		if ((PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),COM_BASIS100_DEAL_UW_DISPLAYFINANCINGPROGRAM, "Y")).equals("Y"))
		{
			// Unset mask to display the field
			thePage.setDisplayFieldValue(pgDealSummaryViewBean.CHILD_STINCLUDEFINANCINGPROGRAMSTART, new String(""));
			thePage.setDisplayFieldValue(pgDealSummaryViewBean.CHILD_STINCLUDEFINANCINGPROGRAMEND, new String(""));
			return;
		}

		// Set Mask to make the filed not dispalyed
		//#DG702 String suppressStart = "<!--";
		//#DG702 String suppressEnd = "-->";

		// suppress
		thePage.setDisplayFieldValue(pgDealSummaryViewBean.CHILD_STINCLUDEFINANCINGPROGRAMSTART, new String(supresTagStart));
		thePage.setDisplayFieldValue(pgDealSummaryViewBean.CHILD_STINCLUDEFINANCINGPROGRAMEND, new String(supresTagEnd));

		return;
	}


	/////////////////////////////////////////////////////////////////////////////
	public String generateOrHideBeginRefiSect(String val)
	{
		if (!val.equals("Y"))
			return supresTagStart;
		else
      return "";
	}

	/////////////////////////////////////////////////////////////////////////////
	public String generateOrHideEndRefiSect(String val)
	{
		if (!val.equals("Y"))
			return supresTagEnd;
		else
      return "";
	}

  //--> New requirement : added new button for reverse funding
  //--> By Billy 11June2003
  public boolean isReverseFunding()
  {
    // check System Property
    if ((PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),COM_BASIS100_DEAL_ALLOWREVERSEFUNDING, "N")).equals("N"))
    {
      return false;
    }

    // check if deal status = 20 or 21
    SessionResourceKit srk = getSessionResourceKit();
		PageEntry pg = getTheSessionState().getCurrentPage();

		try
		{
      int statusId = new Deal(srk, null).getStatusId(pg.getPageDealId(), pg.getPageDealCID());
			if(statusId == Sc.DEAL_POST_DATED_FUNDING || statusId == Sc.DEAL_FUNDED)
        return true;
      else
        return false;
		}
		catch(Exception e)
		{
			logger.error("Exception occurred @DealSummaryHandler.isReverseFunding() : ");
			logger.error(e);
			return false;
		}
  }

  public void handleReverseFunding()
	{
		SessionResourceKit srk = getSessionResourceKit();
		PageEntry pg = getTheSessionState().getCurrentPage();

		try
		{
      srk.beginTransaction();

			Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
      deal.setStatusId(Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_SATISFIED);
      deal.setStatusDate(new java.util.Date());
      //--> Modified to reset ActualClosingDate
      //--> By Billy 03July2003
      deal.setActualClosingDate(null);
      //=======================================

      // Catherine, #1918 --------------- begin ---------------
      if (PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),COM_BASIS100_SYSTEM_RESET_UPLOAD_FLAG_ON_REV_FUNDING, "N").equalsIgnoreCase("Y"))
      {
        logger.debug("Reverse funding: resetting funding upload flag for deal # " + deal.getDealId());
        deal.updateFundingUploadDone(true, false);
      }
      // Catherine, #1918 --------------- end ---------------

      deal.ejbStore();
      CCM ccm = new CCM(srk);
      ccm.sendServicingUpload(srk, deal, Mc.SERVICING_UPLOAD_TYPE_FUNDING, LANGUAGE_PREFERENCE_ENGLISH);
//commented out because we are not merging this new feature at this time.
      //      CCM.requestCloseFundNotif(srk, deal, DEAL_CLOSING_INSTRUCTED_CONDITIONS_SATISFIED);          //FXP31941 send notification          

      //log to deal history
      DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);
      String msg = hLog.statusChange(deal.getStatusId(), "Reverse Funding");
      hLog.log(pg.getPageDealId(), pg.getPageDealCID(), msg, Mc.DEAL_TX_TYPE_EVENT,
        srk.getExpressState().getUserProfileId(), deal.getStatusId());

      workflowTrigger(2, srk, pg, null);
      srk.commitTransaction();
		}
		catch(Exception e)
		{
      srk.cleanTransaction();
			logger.error("Exception occurred @DealSummaryHandler.handleReverseFunding() : ");
			logger.error(e);
			setStandardFailMessage();
		}
	}

  	//***** Change by NBC/PP Implementation Team - GCD - Start *****//	
    //GCD Summary data retrieval Begin
  /**
	 *
	 *
	 */
	private void executeCretarioForAdjudicationResponseBNCModel(Integer dealId, Integer cspDealCPId)
	{
		doAdjudicationResponseBNCModel doARBNCDo =
                          (doAdjudicationResponseBNCModel) RequestManager.getRequestContext().getModelManager().getModel(doAdjudicationResponseBNCModel.class);

		doARBNCDo.clearUserWhereCriteria();
		doARBNCDo.addUserWhereCriterion("dfDealId", "=", dealId);
		doARBNCDo.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);
		logger.debug("----------Anu------ doARBNCDo:: " + doARBNCDo);
    this.executeModel(doARBNCDo, "UWH@executeCretarioForUWApplicantGDSTDS", false);
    logger.debug("----------Anu------ after executing doARBNCDo:: " + doARBNCDo);
	}
	
	//GCD Summary Data Retrieval End
	//***** Change by NBC/PP Implementation Team - GCD - End *****//

  //=========================================================
  //--DJ_LDI_CR--start--//
	/**
	 *
	 *
	 */
	private void executeCretarioForUWApplicantGDSTDS(Integer dealId, Integer cspDealCPId)
	{
		doUWGDSTDSApplicantDetailsModel doUWappGDSTDS =
                          (doUWGDSTDSApplicantDetailsModel) RequestManager.getRequestContext().getModelManager().getModel(doUWGDSTDSApplicantDetailsModel.class);

		doUWappGDSTDS.clearUserWhereCriteria();
		doUWappGDSTDS.addUserWhereCriterion("dfDealId", "=", dealId);
		doUWappGDSTDS.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);
		logger.debug("----------Anu------ doUWappGDSTDS:: " + doUWappGDSTDS.toString());
    this.executeModel(doUWappGDSTDS, "UWH@executeCretarioForUWApplicantGDSTDS", false);
	}

	/**
	 *
	 *
	 */
	private void startSupressSection(String stName, ViewBean thePage)
	{
		//#DG702 String supressStart = "<!--";
		// suppress
		thePage.setDisplayFieldValue(stName, new String(supresTagStart));

		return;
	}

	/**
	 *
	 *
	 */
	private void endSupressSection(String stName, ViewBean thePage)
	{
		String supressEnd = "//-->";
		// suppress
		thePage.setDisplayFieldValue(stName, new String(supressEnd));

		return;
	}
  //--DJ_LDI_CR--end--//

  //--DJ_CR134--start--27May2004--//
	/**
	 *
	 *
	 */
  private void executeCretarioForDealNotesHomeBASEInfo(Integer dealId, Integer notesCategoryId)
	{
		doDealNotesInfoModel theDealNotes =
                          (doDealNotesInfoModel) RequestManager.getRequestContext().getModelManager().getModel(doDealNotesInfoModel.class);

		theDealNotes.clearUserWhereCriteria();
		theDealNotes.addUserWhereCriterion("dfDealID", "=", dealId);
		theDealNotes.addUserWhereCriterion("dfDealNotesCategoryID", "=", notesCategoryId);
    /****MCM Sep 23 2008 :Bug Fix for : FXP22544  To support bilanguage deal notes Starts*/
     final String langStr = "("+ theSessionState.getLanguageId()+ ", "+ LANGUAGE_UNKNOW + ")";
     theDealNotes.addUserWhereCriterion("dfLanguagePreferenceID", "in", langStr);
     /****MCM Sep 23 2008 :Bug Fix for : FXP22544 FXP Ends*/
		logger.debug("----------Anu------ theDealNotes:: " + theDealNotes.toString());
    this.executeModel(theDealNotes, "DSH@executeCretarioForDealNotes" , true);
	}

	/**
	 *
	 *
	 */
  protected String getHomeBASEProductRatePmnt()
	{
    doDealNotesInfoModel theDO = (doDealNotesInfoModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doDealNotesInfoModel.class);

		String notestext = "";

    Object mNotesText = theDO.getDfDealNotesText();
    if(mNotesText != null)
    {
      notestext = mNotesText.toString();
    }
////logger.debug("DSH@getHomeBASEProductRatePmnt:: " + notestext);

    return notestext;
  }
  //--DJ_CR134--end--//

  // #1676, Catherine, 2-Sep-05 ---- start ----------------------------
  private void displayReverseFundingBtn(PageEntry pg, ViewBean thePage) {

    String start = supresTagStart;
    String end = supresTagEnd;

    // check user's rights
    PageEntry pe = (PageEntry) theSessionState.getCurrentPage();
    int accessTypeUser;
    try {
      accessTypeUser = accessByPage(pe, theSessionState, this.getSavedPages(), srk, true);
    } catch (Exception e) {
      accessTypeUser = 1;
      logger.debug("DealSummaryHandler@displayReverseFundingBtn():exception when accessing user rights:" + e);		//#DG702
    }

    if (accessTypeUser == Sc.PAGE_ACCESS_EDIT
        && !isDataObfuscated()) //#DG406 if obfuscated button is hidden
    {
      start = "";
      end = "";
    }

    thePage.setDisplayFieldValue(pgDealSummaryViewBean.CHILD_STHIDEREVERSEBTNSTART, start);
    thePage.setDisplayFieldValue(pgDealSummaryViewBean.CHILD_STHIDEREVERSEBTNEND, end);
  }
  // #1676, Catherine, 2-Sep-05 ---- end   ----------------------------

  //#DG406 to decide if this data is obfuscated let's check how some fields look like
  
  /**
   * return true if data obfuscation is on AND the content of each field is
   * considered "obfuscated". Obfuscation itself is performed by an external
   * procedure produced by Scot Jia
   * @return boolean
   */
  private boolean isDataObfuscated() {
	boolean isObfuscateOff = PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),COM_FILOGIX_IS_DATA_OBFUSCATED, "N").equalsIgnoreCase("N");
    if (isObfuscateOff)
      return false;

    PageEntry pg = getTheSessionState().getCurrentPage();

    try {
      WorkflowTrigger workflowTrigger = new WorkflowTrigger(srk);
      workflowTrigger.setSilentMode(true);		//#DG702 unclutter log file
      workflowTrigger = workflowTrigger.findByCompositeKey(pg.getPageDealId(), WORKFLOWTRIGGER_KEY_OBFUSCATED);
      return true;
    }
    catch (Exception exc) {
      return false;
    }
}
  /*******************************MCM Impl Team XS_16.13 changes starts*****************************/
  /**
   * <p>setMTGCompDisplayFields</p>
   * <p>Description: Setup the Actual Payment Term dispay fields. </p>
   * @param rowNum
   * @version 1.0 25-June-2008 XS_16.13 Intial Version.
   */
  protected void setLoanCompDisplayFields(int rowNum){
      PageEntry pg = theSessionState.getCurrentPage();
      int dealId = pg.getPageDealId();
      int copyId = theSessionState.getCurrentPage().getPageDealCID();
      doComponentLoanModelImpl doComponentLoanModel = (doComponentLoanModelImpl)
      RequestManager.getRequestContext().getModelManager().getModel(doComponentLoanModel.class);
          
      //Tiled view Bean Names
      String tileViewPrefixLoan = "RepeatedLoanComponents" + "/";
      
      //Set Actual Payment Term with years and months seperated for loan component.
      setTermsDisplayField(doComponentLoanModel, tileViewPrefixLoan + pgDealSummaryComponentLoanTiledView.CHILD_STACTUALPAYMENTTERMYEAR,
              tileViewPrefixLoan + pgDealSummaryComponentLoanTiledView.CHILD_STACTUALPAYMENTTERMMONTH, 
              doComponentLoanModel.FIELD_DFACTUALPAYMENTTERM, rowNum);
   
  }
  
  /**
   * <p>setMTGCompDisplayFields</p>
   * <p>Description: Setup the Actual Payment,Amortization Period,Effective Emortization Term dispay fields. </p>
   * @param rowNum
   * @version 1.0 25-June-2008 XS_16.13 Intial Version.
   */
  protected void setMrtgCompDisplayFields(int rowNum){
  //get the componentmortgagemodel object
  doComponentMortgageModelImpl doComponentMortgageModel = (doComponentMortgageModelImpl)
  RequestManager.getRequestContext().getModelManager().getModel(doComponentMortgageModel.class);
  //Tiled view Bean Names
  String tileViewPrefixLoan = "RepeatedMortgageComponents" + "/";
  
  //Set Actual Payment Term with years and months seperated for mortgae component. 
  setTermsDisplayField(doComponentMortgageModel, tileViewPrefixLoan + pgDealSummaryComponentMortgageTiledView.CHILD_STACTUALPAYMENTTERMYEARS,
          tileViewPrefixLoan + pgDealSummaryComponentMortgageTiledView.CHILD_STACTUALPAYMENTTERMMONTHS, 
          doComponentMortgageModel.FIELD_DFACTUALPAYMENTTERM, rowNum);

  //Set Amortization period Term with years and months seperated for mortgae component.
  
  setTermsDisplayField(doComponentMortgageModel, tileViewPrefixLoan + pgDealSummaryComponentMortgageTiledView.CHILD_STAMORTIZATIONPERIODYEARS,
          tileViewPrefixLoan + pgDealSummaryComponentMortgageTiledView.CHILD_STAMORTIZATIONPERIODMONTHS, 
          doComponentMortgageModel.FIELD_DFAMORTIZATIONTERM, rowNum);
  
 //Set Effective Amortization period Term with years and months seperated for mortgae component. 
  setTermsDisplayField(doComponentMortgageModel, tileViewPrefixLoan + pgDealSummaryComponentMortgageTiledView.CHILD_STEFFECTIVEAMORTIZATIONYEARS,
          tileViewPrefixLoan + pgDealSummaryComponentMortgageTiledView.CHILD_STEFFECTIVEAMORTIZATIONMONTHS, 
          doComponentMortgageModel.FIELD_DFEFFECTIVEAMORTIZATIONINMONTHS, rowNum);
    }
  /*******************************MCM Impl Team XS_16.13 changes ends*****************************/
  
  /**
   * Qualify Rate 
   * @param thePage
   */
  private void setHiddenQualifyRateSection(ViewBean thePage) {

      String prop = PropertiesCache.getInstance().getProperty(
              theSessionState.getDealInstitutionId(),
              "com.filogix.qualoverridechk", "Y");
      if (prop.equals("N")) {
         // thePage.setDisplayFieldValue("stQualifyRateHidden", "visible");
          thePage.setDisplayFieldValue("stQualifyRateEdit", "none");

      } else {
          // Set Mask to make the filed not dispalyed
         // thePage.setDisplayFieldValue("stQualifyRateHidden", "none");
          thePage.setDisplayFieldValue("stQualifyRateEdit", "visible");
      }
  }
  }

