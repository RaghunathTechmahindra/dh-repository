package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doDealEntryPropertySelectModelImpl extends QueryModelBase
	implements doDealEntryPropertySelectModel
{
	/**
	 *
	 *
	 */
	public doDealEntryPropertySelectModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPrimaryPropertyFlag()
	{
		return (String)getValue(FIELD_DFPRIMARYPROPERTYFLAG);
	}


	/**
	 *
	 *
	 */
	public void setDfPrimaryPropertyFlag(String value)
	{
		setValue(FIELD_DFPRIMARYPROPERTYFLAG,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStreetNumber()
	{
		return (String)getValue(FIELD_DFSTREETNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfStreetNumber(String value)
	{
		setValue(FIELD_DFSTREETNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStreetName()
	{
		return (String)getValue(FIELD_DFSTREETNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfStreetName(String value)
	{
		setValue(FIELD_DFSTREETNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStreetType()
	{
		return (String)getValue(FIELD_DFSTREETTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfStreetType(String value)
	{
		setValue(FIELD_DFSTREETTYPE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStreetDirection()
	{
		return (String)getValue(FIELD_DFSTREETDIRECTION);
	}


	/**
	 *
	 *
	 */
	public void setDfStreetDirection(String value)
	{
		setValue(FIELD_DFSTREETDIRECTION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfProvince()
	{
		return (String)getValue(FIELD_DFPROVINCE);
	}


	/**
	 *
	 *
	 */
	public void setDfProvince(String value)
	{
		setValue(FIELD_DFPROVINCE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPurchasePrice()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public void setDfPurchasePrice(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPURCHASEPRICE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLTV()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLTV);
	}


	/**
	 *
	 *
	 */
	public void setDfLTV(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLTV,value);
	}

	/**
	 * MetaData object test method to verify the SQL_TEMPLATE query.
	 *
	 */
   /**
        public void setResultSet(ResultSet value)
        {
            logger = SysLog.getSysLogger("METADATA");

            try
            {
                super.setResultSet(value);
                if( value != null)
                {
                    ResultSetMetaData metaData = value.getMetaData();
                    int columnCount = metaData.getColumnCount();
                    logger.debug("===============================================");
                    logger.debug("Testing MetaData Object for new synthetic fields for" +
	                 	            " doDealEntryPropertySelectModel");
                    logger.debug("NumberColumns: " + columnCount);
                    for(int i = 0; i < columnCount ; i++)
                    {
                        logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
                    }
                    logger.debug("================================================");
                  }
              }
              catch (SQLException e)
              {
                  logger.debug("Class: " + getClass().getName());
                  logger.debug("SQLException: " + e);
              }
        }
     **/

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL PROPERTY.DEALID, PROPERTY.PROPERTYID, PROPERTY.COPYID, PROPERTY.PRIMARYPROPERTYFLAG, PROPERTY.PROPERTYSTREETNUMBER, PROPERTY.PROPERTYSTREETNAME, STREETTYPE.STDESCRIPTION, STREETDIRECTION.SDDESCRIPTION, PROVINCE.PROVINCENAME, PROPERTY.PURCHASEPRICE, PROPERTY.LOANTOVALUE FROM PROPERTY, PROVINCE, STREETDIRECTION, STREETTYPE  __WHERE__  ORDER BY PROPERTY.PROPERTYID  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME="PROPERTY, PROVINCE, STREETDIRECTION, STREETTYPE";
	public static final String STATIC_WHERE_CRITERIA=" (PROPERTY.PROVINCEID  =  PROVINCE.PROVINCEID) AND (PROPERTY.STREETDIRECTIONID  =  STREETDIRECTION.STREETDIRECTIONID) AND (PROPERTY.STREETTYPEID  =  STREETTYPE.STREETTYPEID) ";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="PROPERTY.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYID="PROPERTY.PROPERTYID";
	public static final String COLUMN_DFPROPERTYID="PROPERTYID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="PROPERTY.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFPRIMARYPROPERTYFLAG="PROPERTY.PRIMARYPROPERTYFLAG";
	public static final String COLUMN_DFPRIMARYPROPERTYFLAG="PRIMARYPROPERTYFLAG";
	public static final String QUALIFIED_COLUMN_DFSTREETNUMBER="PROPERTY.PROPERTYSTREETNUMBER";
	public static final String COLUMN_DFSTREETNUMBER="PROPERTYSTREETNUMBER";
	public static final String QUALIFIED_COLUMN_DFSTREETNAME="PROPERTY.PROPERTYSTREETNAME";
	public static final String COLUMN_DFSTREETNAME="PROPERTYSTREETNAME";
	public static final String QUALIFIED_COLUMN_DFSTREETTYPE="STREETTYPE.STDESCRIPTION";
	public static final String COLUMN_DFSTREETTYPE="STDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFSTREETDIRECTION="STREETDIRECTION.SDDESCRIPTION";
	public static final String COLUMN_DFSTREETDIRECTION="SDDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFPROVINCE="PROVINCE.PROVINCENAME";
	public static final String COLUMN_DFPROVINCE="PROVINCENAME";
	public static final String QUALIFIED_COLUMN_DFPURCHASEPRICE="PROPERTY.PURCHASEPRICE";
	public static final String COLUMN_DFPURCHASEPRICE="PURCHASEPRICE";
	public static final String QUALIFIED_COLUMN_DFLTV="PROPERTY.LOANTOVALUE";
	public static final String COLUMN_DFLTV="LOANTOVALUE";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYID,
				COLUMN_DFPROPERTYID,
				QUALIFIED_COLUMN_DFPROPERTYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIMARYPROPERTYFLAG,
				COLUMN_DFPRIMARYPROPERTYFLAG,
				QUALIFIED_COLUMN_DFPRIMARYPROPERTYFLAG,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTREETNUMBER,
				COLUMN_DFSTREETNUMBER,
				QUALIFIED_COLUMN_DFSTREETNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTREETNAME,
				COLUMN_DFSTREETNAME,
				QUALIFIED_COLUMN_DFSTREETNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTREETTYPE,
				COLUMN_DFSTREETTYPE,
				QUALIFIED_COLUMN_DFSTREETTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTREETDIRECTION,
				COLUMN_DFSTREETDIRECTION,
				QUALIFIED_COLUMN_DFSTREETDIRECTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROVINCE,
				COLUMN_DFPROVINCE,
				QUALIFIED_COLUMN_DFPROVINCE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPURCHASEPRICE,
				COLUMN_DFPURCHASEPRICE,
				QUALIFIED_COLUMN_DFPURCHASEPRICE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLTV,
				COLUMN_DFLTV,
				QUALIFIED_COLUMN_DFLTV,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

