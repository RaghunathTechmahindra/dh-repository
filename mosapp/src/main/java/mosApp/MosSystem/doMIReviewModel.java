package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doMIReviewModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public String getDfMIStatus();


	/**
	 *
	 *
	 */
	public void setDfMIStatus(String value);


	/**
	 *
	 *
	 */
	public String getDfMIComments();


	/**
	 *
	 *
	 */
	public void setDfMIComments(String value);


	/**
	 *
	 *
	 */
	public String getDfMIUpfront();


	/**
	 *
	 *
	 */
	public void setDfMIUpfront(String value);


	/**
	 *
	 *
	 */
	public String getDfMIPolicyNumber();

  //===============================================================
  // New fields to handle MIPolicyNum problem :
  //  the number lost if user cancel MI and re-send again later.
  // -- Modified by Billy 14July2003
	public String getDfMIPolicyNumberCMHC();
  public String getDfMIPolicyNumberGE();
  public String getDfMIPolicyNumberAIGUG();
  public String getDfMIPolicyNumberPMI();
  //===============================================================
  
  // Modified by Serghei 17 Nov 2006
  public String getDfDealStatusId();

	/**
	 *
	 *
	 */
	public void setDfMIPolicyNumber(String value);

  //===============================================================
  // New fields to handle MIPolicyNum problem :
  //  the number lost if user cancel MI and re-send again later.
  // -- Modified by Billy 14July2003
  public void setDfMIPolicyNumberCMHC(String value);
  public void setDfMIPolicyNumberGE(String value);
  public void setDfMIPolicyNumberAIGUG(String value);
  public void setDfMIPolicyNumberPMI(String value);
  //===============================================================
  
  // Modified by Serghei 17 Nov 2006
  public void setDfDealStatusId(String value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIPremium();


	/**
	 *
	 *
	 */
	public void setDfMIPremium(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfMIResponse();


	/**
	 *
	 *
	 */
	public void setDfMIResponse(String value);


	/**
	 *
	 *
	 */
	public String getDfMIExistingPolicyNumber();


	/**
	 *
	 *
	 */
	public void setDfMIExistingPolicyNumber(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIIndicatorId();


	/**
	 *
	 *
	 */
	public void setDfMIIndicatorId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIStatusId();


	/**
	 *
	 *
	 */
	public void setDfMIStatusId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMITypeId();


	/**
	 *
	 *
	 */
	public void setDfMITypeId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIInsurerId();


	/**
	 *
	 *
	 */
	public void setDfMIInsurerId(java.math.BigDecimal value);

    // SEAN DJ SPEC-Progress Advance Type July 22, 2005: the getter and setter
    // methods.
    public java.math.BigDecimal getDfProgressAdvanceTypeId();
    public void setDfProgressAdvanceTypeId(java.math.BigDecimal value);
    // SEAN DJ SPEC-PAT END

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIPayorId();


	/**
	 *
	 *
	 */
	public void setDfMIPayorId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfMIStatusUpdateFlag();


	/**
	 *
	 *
	 */
	public void setDfMIStatusUpdateFlag(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLienPosition();


	/**
	 *
	 *
	 */
	public void setDfLienPosition(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfMIRUIntervention();


	/**
	 *
	 *
	 */
	public void setDfMIRUIntervention(String value);


	/**
	 *
	 *
	 */
	public String getDfPreQualificationMICertNum();


	/**
	 *
	 *
	 */
	public void setDfPreQualificationMICertNum(String value);

  //// SYNCADD.
 	/**
 	 *
 	 *
 	 */
 	public String getDfSourceFirmName();


 	/**
 	 *
 	 *
 	 */
 	public void setDfSourceFirmName(String value);


 	/**
 	 *
 	 *
 	 */
 	public String getDfSOBFirstName();


 	/**
 	 *
 	 *
 	 */
 	public void setDfSOBFirstName(String value);


 	/**
 	 *
 	 *
 	 */
 	public String getDfSOBLastName();


 	/**
 	 *
 	 *
 	 */
 	public void setDfSOBLastName(String value);


 	/**
 	 *
 	 *
 	 */
 	public String getDfSOBPhoneNum();


 	/**
 	 *
 	 *
 	 */
 	public void setDfSOBPhoneNum(String value);


 	/**
 	 *
 	 *
 	 */
 	public String getDfSOBPhoneExt();


 	/**
 	 *
 	 *
 	 */
 	public void setDfSOBPhoneExt(String value);


 	/**
 	 *
 	 *
 	 */
 	public String getDfSOBFaxNum();


 	/**
 	 *
 	 *
 	 */
 	public void setDfSOBFaxNum(String value);


 	/**
 	 *
 	 *
 	 */
 	public String getDfSOBReferenceNum();


 	/**
 	 *
 	 *
 	 */
 	public void setDfSOBReferenceNum(String value);


 	/**
 	 *
 	 *
 	 */
 	public String getDfSOBAddressLine1();


 	/**
 	 *
 	 *
 	 */
 	public void setDfSOBAddressLine1(String value);


 	/**
 	 *
 	 *
 	 */
 	public String getDfSOBAddressLine2();


 	/**
 	 *
 	 *
 	 */
 	public void setDfSOBAddressLine2(String value);


 	/**
 	 *
 	 *
 	 */
 	public String getDfSOBCity();


 	/**
 	 *
 	 *
 	 */
 	public void setDfSOBCity(String value);

 	/**
 	 *
 	 *
 	 */
 	public String getDfSOBProvince();


 	/**
 	 *
 	 *
 	 */
 	public void setDfSOBProvince(String value);


 	/**
 	 *
 	 *
 	 */
 	public String getDfSourceAppId();


 	/**
 	 *
 	 *
 	 */
 	public void setDfSourceAppId(String value);


//	MI additions
 	public java.math.BigDecimal getDfLOCRepaymentType();
    public void setDfLOCRepaymentType(java.math.BigDecimal value);
    
    public String getDfSelfDirectedRRSP();
    public void setDfSelfDirectedRRSP(String value);
    
    public String getDfRequestStandardService();
    public void setDfRequestStandardService(String value);

    public String getDfCMHCProductTrackerIdentifier();
    public void setDfCMHCProductTrackerIdentifier(String value);

    public java.math.BigDecimal getDfLOCAmortizationMonths();
    public void setDfLOCAmortizationMonths(java.math.BigDecimal value);
    
    public java.sql.Timestamp getDfLOCInterestOnlyMaturityDate();
    public void setDfLOCInterestOnlyMaturityDate(java.sql.Timestamp value);
    
    public String getDfProgressAdvance();
    public void setDfProgressAdvance(String value);
    
    public String getDfProgressAdvanceInspectionBy();
    public void setDfProgressAdvanceInspectionBy(String value);
    
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFMISTATUS="dfMIStatus";
	public static final String FIELD_DFMICOMMENTS="dfMIComments";
	public static final String FIELD_DFMIUPFRONT="dfMIUpfront";
	public static final String FIELD_DFMIPOLICYNUMBER="dfMIPolicyNumber";
	public static final String FIELD_DFMIPREMIUM="dfMIPremium";
	public static final String FIELD_DFMIRESPONSE="dfMIResponse";
	public static final String FIELD_DFMIEXISTINGPOLICYNUMBER="dfMIExistingPolicyNumber";
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFMIINDICATORID="dfMIIndicatorId";
	public static final String FIELD_DFMISTATUSID="dfMIStatusId";
	public static final String FIELD_DFMITYPEID="dfMITypeId";
	public static final String FIELD_DFMIINSURERID="dfMIInsurerId";
	public static final String FIELD_DFMIPAYORID="dfMIPayorId";
	public static final String FIELD_DFMISTATUSUPDATEFLAG="dfMIStatusUpdateFlag";
	public static final String FIELD_DFLIENPOSITION="dfLienPosition";
	public static final String FIELD_DFMIRUINTERVENTION="dfMIRUIntervention";
	public static final String FIELD_DFPREQUALIFICATIONMICERTNUM="dfPreQualificationMICertNum";
    // SEAN DJ SPEC-Progress Advance Type July 22, 2005: Define the field.
    public static final String FIELD_DFPROGRESSADVANCETYPEID="dfProgressAdvanceTypeId";
    // SEAN DJ SPEC-PAT END
  //// SYNCADD.
 	public static final String FIELD_DFSOURCEFIRMNAME="dfSourceFirmName";
 	public static final String FIELD_DFSOBFIRSTNAME="dfSOBFirstName";
 	public static final String FIELD_DFSOBLASTNAME="dfSOBLastName";
 	public static final String FIELD_DFSOBPHONENUM="dfSOBPhoneNum";
 	public static final String FIELD_DFSOBPHONEEXT="dfSOBPhoneExt";
 	public static final String FIELD_DFSOBFAXNUM="dfSOBFaxNum";
 	public static final String FIELD_DFSOBREFERENCENUM="dfSOBReferenceNum";
 	public static final String FIELD_DFSOBADDRESSLINE1="dfSOBAddressLine1";
 	public static final String FIELD_DFSOBADDRESSLINE2="dfSOBAddressLine2";
 	public static final String FIELD_DFSOBCITY="dfSOBCity";
 	public static final String FIELD_DFSOBPROVINCE="dfSOBProvince";
 	public static final String FIELD_DFSOURCEAPPID="dfSourceAppId";

  //===============================================================
  // New fields to handle MIPolicyNum problem :
  //  the number lost if user cancel MI and re-send again later.
  // -- Modified by Billy 14July2003
  public static final String FIELD_DFMIPOLICYNUMBERCMHC="dfMIPolicyNumberCMHC";
  public static final String FIELD_DFMIPOLICYNUMBERGE="dfMIPolicyNumberGE";
  public static final String FIELD_DFMIPOLICYNUMBERAIGUG="dfMIPolicyNumberAIGUG";
  public static final String FIELD_DFMIPOLICYNUMBERPMI="dfMIPolicyNumberPMI";
  //===============================================================
  
  // Modified by Serghei 17 Nov 2006
  public static final String FIELD_DFDEALSTATUSID = "dfDealStatusId";

  //MI additions
  public static final String FIELD_DFLOCREPAYMENTTYPEID = "dfLOCRepaymentType";
  public static final String FIELD_DFSELFDIRECTEDRRSP = "dfSelfDirectedRRSP";
  public static final String FIELD_DFREQUESTSTANDARDSERVICE = "dfRequestStandardService";
  public static final String FIELD_DFCMHCPRODUCTTRACKERIDENTIFIER = "dfCMHCProductTrackerIdentifier";
  public static final String FIELD_DFLOCAMORTIZATIONMONTHS = "dfLOCAmortizationMonths";
  public static final String FIELD_DFLOCINTERESTONLYMATURITYDATE = "dfLOCInterestOnlyMaturityDate";
  public static final String FIELD_DFPROGRESSADVANCE = "dfProgressAdvance";
  public static final String FIELD_DFPROGRESSADVANCEINSPECTIONBY = "dfProgressAdvanceInspectionBy";
  //5.0
  public static final String FIELD_DFPREVIOUSMIINDICATORID = "dfPreviousMIIndicatorId";
  
  //QC-Ticket-268- Start
  public static final String FIELD_DFSPECIALFEATUREID = "dfSpecialFeatureId";
  //QC-Ticket-268- Start
  
  
}

