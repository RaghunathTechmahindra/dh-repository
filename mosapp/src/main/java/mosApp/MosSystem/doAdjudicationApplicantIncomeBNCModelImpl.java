package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import MosSystem.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;
import com.basis100.picklist.BXResources;

/**
 * <p>Title: doAdjudicationApplicantIncomeBNCModelImpl</p>
 *
 * <p>Description: Implementation class for doAdjudicationApplicantIncomeBNCModel, used on Credit Decision screen</p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006 </p
 * 
 * <p>Company: Filogix Inc. </p>
 * 
 * @author NBC/PP Implementation Team
 * @version 1.0
 * @version 1.1 <br>
 * Date: 10/18/2006
 * Author: GCD Implementation Team <br>
 * Change: Added deal.copyid=income.copyid and deal.copyid=employmenthistory.copyid  to where clause of SQL statement
 * @version 1.2 <br>
 * Date: 10/3/2006 
 * Change: Added BORROWER.COPYID = INCOME.COPYID to SQL statement &  I1.COPYID = DEAL.COPYID
 * 	to the WHERE clause
 * 
 */
public class doAdjudicationApplicantIncomeBNCModelImpl extends QueryModelBase
	implements doAdjudicationApplicantIncomeBNCModel
{
	/**
	 *
	 *
	 */
	public doAdjudicationApplicantIncomeBNCModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

//   logger = SysLog.getSysLogger("AdjudicationApplicantIncomeBNCModel");
//   logger.debug("AdjudicationApplicantIncomeBNCModel@afterExecute::Size: " + sql);

		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
//	    logger = SysLog.getSysLogger("AdjudicationApplicantIncomeBNCModel");
//	    logger.debug("AdjudicationApplicantIncomeBNCModel@afterExecute::Size: " + this.getSize());
//	    logger.debug("AdjudicationApplicantIncomeBNCModel@afterExecute::SQLTemplate: " + this.getSelectSQL());
	
	    //--Release2.1--//
	    //To override all the Description fields by populating from BXResource
	    //Get the Language ID from the SessionStateModel
	    String defaultInstanceStateName =
				getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
	    SessionStateModelImpl theSessionState =
	        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
	                        SessionStateModel.class,
	                        defaultInstanceStateName,
	                        true);
	    int languageId = theSessionState.getLanguageId();
		int institutionId = theSessionState.getDealInstitutionId();

	    while(this.next())
	    {
	      //  Income Type
    	  this.setDfIncomeTypeDesc(BXResources.getPickListDescription(
    			  institutionId, "INCOMETYPE", this.getDfIncomeTypeDesc(), languageId));
	    	  
    	   
	      // Translate Months of Service into ## Years, ## Months for Time At Employer
	      java.math.BigDecimal numberYears = this.getDfMonthsOfService();
	      if (numberYears != null) 
	      {
	      numberYears = numberYears.divide(new java.math.BigDecimal("12"), java.math.BigDecimal.ROUND_DOWN);
	      String fieldValue = numberYears.toString();
	      fieldValue = fieldValue.concat(" Yr(s) ");
	      fieldValue = fieldValue.concat(String.valueOf(this.getDfMonthsAtEmployer()));
	      fieldValue = fieldValue.concat(" Mth(s)");
	      this.setDfTimeAtEmployer(fieldValue);
	    }

	}

	    // reset location
	    this.beforeFirst();

	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}

	/**
	 * 
	 *
	 */
	public java.math.BigDecimal getDfMonthsOfService()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMONTHSOFSERVICE);
	}

	/**
	 *
	 *
	 */
	public void setDfMonthsOfService( java.math.BigDecimal value)
	{
		setValue(FIELD_DFMONTHSOFSERVICE,value);
	}


	/**
	 * 
	 *
	 */
	public String getDfMonthsAtEmployer()
	{
		return (String)getValue(FIELD_DFMONTHSATEMPLOYER);
	}

	/**
	 *
	 *
	 */
	public void setDfMonthsAtEmployer(String value)
	{
		setValue(FIELD_DFMONTHSATEMPLOYER,value);
	}


	/**
	 * 
	 *
	 */
	public String getDfTimeAtEmployer()
	{
		return (String)getValue(FIELD_DFTIMEATEMPLOYER);
	}

	/**
	 *
	 *
	 */
	public void setDfTimeAtEmployer(String value)
	{
		setValue(FIELD_DFTIMEATEMPLOYER,value);
	}


	/**
	 * 
	 *
	 */
	public java.math.BigDecimal getDfAnnualIncomeAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFANNUALINCOMEAMOUNT);
	}

	/**
	 *
	 *
	 */
	public void setDfAnnualIncomeAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFANNUALINCOMEAMOUNT,value);
	}


	/**
	 * 
	 *
	 */
	public java.math.BigDecimal getDfIncomeTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINCOMETYPEID);
	}

	/**
	 *
	 *
	 */
	public void setDfIncomeTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINCOMETYPEID,value);
	}


	/**
	 * 
	 *
	 */
	public String getDfIncomeTypeDesc()
	{
		return (String)getValue(FIELD_DFINCOMETYPEDESC);
	}

	/**
	 *
	 *
	 */
	public void setDfIncomeTypeDesc(String value)
	{
		setValue(FIELD_DFINCOMETYPEDESC,value);
	}


	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}

	/**
	 * 
	 *
	 */
	public String getDfPrimaryBorrowerFlag()
	{
		return (String)getValue(FIELD_DFPRIMARYBORROWERFLAG);
	}

	/**
	 *
	 *
	 */
	public void setDfPrimaryBorrowerFlag(String value)
	{
		setValue(FIELD_DFPRIMARYBORROWERFLAG,value);
	}

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.
  public static final String DATA_SOURCE_NAME="jdbc/orcl";
  public static final String SELECT_SQL_TEMPLATE=
	  "SELECT DISTINCT DEAL.DEALID, DEAL.COPYID, INCOME.ANNUALINCOMEAMOUNT, BORROWER.BORROWERID, " +   
	  "EMPLOYMENTHISTORY.MONTHSOFSERVICE, to_char(MOD(EMPLOYMENTHISTORY.MONTHSOFSERVICE, 12)) MONTHSATEMPLOYER, " + 
	  "TO_CHAR(EMPLOYMENTHISTORY.MONTHSOFSERVICE) TIMEATEMPLOYER, INCOME.INCOMETYPEID, " + 
	  "TO_CHAR(INCOME.INCOMETYPEID) INCOMETYPEDESC, BORROWER.PRIMARYBORROWERFLAG " +
	  "FROM DEAL " +
	  "INNER JOIN BORROWER ON DEAL.DEALID = BORROWER.DEALID " + 
	  "LEFT OUTER JOIN INCOME INCOME ON BORROWER.BORROWERID = INCOME.BORROWERID AND BORROWER.COPYID = INCOME.COPYID " + 
	  "LEFT OUTER JOIN EMPLOYMENTHISTORY  ON INCOME.INCOMEID = EMPLOYMENTHISTORY.INCOMEID " +
	  "__WHERE__ ORDER BY BORROWER.PRIMARYBORROWERFLAG DESC ";	  

  public static final String MODIFYING_QUERY_TABLE_NAME="DEAL";

  public static final String STATIC_WHERE_CRITERIA="DEAL.COPYID = BORROWER.COPYID " +
  	  "AND DEAL.COPYID = INCOME.COPYID AND DEAL.COPYID = EMPLOYMENTHISTORY.COPYID " +
  	  "AND INCOME.ANNUALINCOMEAMOUNT = " + 
	  "(SELECT MAX( I1.ANNUALINCOMEAMOUNT ) FROM INCOME I1 WHERE I1.BORROWERID = BORROWER.BORROWERID AND I1.COPYID = DEAL.COPYID)";

	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();

	public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	
	public static final String QUALIFIED_COLUMN_DFMONTHSOFSERVICE = "EMPLOYMENTHISTORY.MONTHSOFSERVICE";
	public static final String COLUMN_DFMONTHSOFSERVICE = "MONTHSOFSERVICE";

	public static final String QUALIFIED_COLUMN_DFMONTHSATEMPLOYER = "EMPLOYMENTHISTORY.MONTHSATEMPLOYER";
	public static final String COLUMN_DFMONTHSATEMPLOYER = "MONTHSATEMPLOYER";

	public static final String QUALIFIED_COLUMN_DFTIMEATEMPLOYER = "EMPLOYMENTHISTORY.TIMEATEMPLOYER";
	public static final String COLUMN_DFTIMEATEMPLOYER = "TIMEATEMPLOYER";

	public static final String QUALIFIED_COLUMN_DFANNUALINCOMEAMOUNT = "INCOME.ANNUALINCOMEAMOUNT";
	public static final String COLUMN_DFANNUALINCOMEAMOUNT = "ANNUALINCOMEAMOUNT";

	public static final String QUALIFIED_COLUMN_DFINCOMETYPEID = "INCOME.INCOMETYPEID";
	public static final String COLUMN_DFINCOMETYPEID = "INCOMETYPEID";

	public static final String QUALIFIED_COLUMN_DFINCOMETYPEDESC = "INCOME.INCOMETYPEDESC";
	public static final String COLUMN_DFINCOMETYPEDESC = "INCOMETYPEDESC";

	public static final String QUALIFIED_COLUMN_DFPRIMARYBORROWERFLAG = "BORROWER.PRIMARYBORROWERFLAG";
	public static final String COLUMN_DFPRIMARYBORROWERFLAG = "PRIMARYBORROWERFLAG";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{


		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		
			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFDEALID,
					COLUMN_DFDEALID,
					QUALIFIED_COLUMN_DFDEALID,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
			
			FIELD_SCHEMA.addFieldDescriptor(
					new QueryFieldDescriptor(
					FIELD_DFMONTHSOFSERVICE,
					COLUMN_DFMONTHSOFSERVICE,
					QUALIFIED_COLUMN_DFMONTHSOFSERVICE,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
				FIELD_SCHEMA.addFieldDescriptor(
					new QueryFieldDescriptor(
					FIELD_DFMONTHSATEMPLOYER,
					COLUMN_DFMONTHSATEMPLOYER,
					QUALIFIED_COLUMN_DFMONTHSATEMPLOYER,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
				FIELD_SCHEMA.addFieldDescriptor(
					new QueryFieldDescriptor(
					FIELD_DFTIMEATEMPLOYER,
					COLUMN_DFTIMEATEMPLOYER,
					QUALIFIED_COLUMN_DFTIMEATEMPLOYER,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
								
				FIELD_SCHEMA.addFieldDescriptor(
					new QueryFieldDescriptor(
					FIELD_DFANNUALINCOMEAMOUNT,
					COLUMN_DFANNUALINCOMEAMOUNT,
					QUALIFIED_COLUMN_DFANNUALINCOMEAMOUNT,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
				
				FIELD_SCHEMA.addFieldDescriptor(
					new QueryFieldDescriptor(
					FIELD_DFINCOMETYPEID,
					COLUMN_DFINCOMETYPEID,
					QUALIFIED_COLUMN_DFINCOMETYPEID,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
				FIELD_SCHEMA.addFieldDescriptor(
					new QueryFieldDescriptor(
					FIELD_DFINCOMETYPEDESC,
					COLUMN_DFINCOMETYPEDESC,
					QUALIFIED_COLUMN_DFINCOMETYPEDESC,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
				FIELD_SCHEMA.addFieldDescriptor(
					new QueryFieldDescriptor(
					FIELD_DFPRIMARYBORROWERFLAG,
					COLUMN_DFPRIMARYBORROWERFLAG,
					QUALIFIED_COLUMN_DFPRIMARYBORROWERFLAG,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));				

	}

}

