package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 * @author MCM Impl Team
 * Change Log:
 * @version 1.1 01-July-2008 XS_2.25 added new fields dfProductName,dfCombinedLTV,dfTaxEscrow 
 * to display Deal Summary on Component Details screen.
 *
 */
public interface doUWDealSummarySnapShotModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public String getDfApplicationID();

	
	/**
	 * 
	 * 
	 */
	public void setDfApplicationID(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStatusDescr();

	
	/**
	 * 
	 * 
	 */
	public void setDfStatusDescr(String value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfStatusDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfStatusDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSFShortName();

	
	/**
	 * 
	 * 
	 */
	public void setDfSFShortName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSOBPShortName();

	
	/**
	 * 
	 * 
	 */
	public void setDfSOBPShortName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfDealTypeDescr();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealTypeDescr(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLoanPurposeDescr();

	
	/**
	 * 
	 * 
	 */
	public void setDfLoanPurposeDescr(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalPurchasePrice();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalPurchasePrice(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalLoanAmt();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalLoanAmt(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPaymentTermDescr();

	
	/**
	 * 
	 * 
	 */
	public void setDfPaymentTermDescr(String value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfEstClosingDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfEstClosingDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealID();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealID(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSpecialFeature();

	
	/**
	 * 
	 * 
	 */
	public void setDfSpecialFeature(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfUnderwriterFirstName();

	
	/**
	 * 
	 * 
	 */
	public void setDfUnderwriterFirstName(String value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfApplicationDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfApplicationDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public String getDfUnderwriterMiddleInitial();

	
	/**
	 * 
	 * 
	 */
	public void setDfUnderwriterMiddleInitial(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfUnderwriterLastName();

	
	/**
	 * 
	 * 
	 */
	public void setDfUnderwriterLastName(String value);
	
	
	//-- ========== SCR#859 begins ========== --//
	//-- by Neil on Feb 11, 2005
	public String getDfServicingMortgageNumber();
	public void setDfServicingMortgageNumber(String value);		
	//-- ========== SCR#859 ends ========== --//
  
  /****************************MCM Impl Team XS_2.25 changes starts**************************/
  /**
   * This method declaration returns the value of field the Prodcut Name.
   *
   * @return the ProductName
   */
  public String getDfProductName();
  /**
   * This method declaration sets the the Product Name.
   *
   * @param value
   */
  public void setDfProductName(String value);
  /**
   * This method declaration returns the value of field the MI Premium.
   *
   * @return the MIPremium
   */
  public java.math.BigDecimal getDfMIPremium();
  /**
   * This method declaration sets the the MIPremium.
   *
   * @param value new MI Premium
   */
  public void setDfMIPremium(java.math.BigDecimal value);
  /**
   * This method declaration returns the value of field the TaxEscrow.
   *
   * @return the TaxEscrow
   */
  public java.math.BigDecimal getDfTaxEscrow();
  /**
   * This method declaration sets the the TaxEscrow.
   *
   * @param value new TaxEscrow
   */
  public void setDfTaxEscrow(java.math.BigDecimal value);
  
  /**
   * This method declaration returns the value of field the CombinedLTV.
   *
   * @return the CombinedLTV
   */
  public java.math.BigDecimal getDfCombinedLTV();
  /**
   * This method declaration sets the the CombinedLTV.
   *
   * @param value new CombinedLTV
   */
  public void setDfCombinedLTV(java.math.BigDecimal value);
  /**
   * This method declaration returns the value of field the LOB.
   *
   * @return the LOB
   */
  public String getDfLOB();
  /**
   * This method declaration sets the the LOB.
   *
   * @param value new LOB
   */
  public void setDfLOB(String value);
 
  /****************************MCM Impl Team XS_2.25 changes ends**************************/
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFAPPLICATIONID="dfApplicationID";
	public static final String FIELD_DFSTATUSDESCR="dfStatusDescr";
	public static final String FIELD_DFSTATUSDATE="dfStatusDate";
	public static final String FIELD_DFSFSHORTNAME="dfSFShortName";
	public static final String FIELD_DFSOBPSHORTNAME="dfSOBPShortName";
	public static final String FIELD_DFDEALTYPEDESCR="dfDealTypeDescr";
	public static final String FIELD_DFLOANPURPOSEDESCR="dfLoanPurposeDescr";
	public static final String FIELD_DFTOTALPURCHASEPRICE="dfTotalPurchasePrice";
	public static final String FIELD_DFTOTALLOANAMT="dfTotalLoanAmt";
	public static final String FIELD_DFPAYMENTTERMDESCR="dfPaymentTermDescr";
	public static final String FIELD_DFESTCLOSINGDATE="dfEstClosingDate";
	public static final String FIELD_DFDEALID="dfDealID";
	public static final String FIELD_DFSPECIALFEATURE="dfSpecialFeature";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFUNDERWRITERFIRSTNAME="dfUnderwriterFirstName";
	public static final String FIELD_DFAPPLICATIONDATE="dfApplicationDate";
	public static final String FIELD_DFUNDERWRITERMIDDLEINITIAL="dfUnderwriterMiddleInitial";
	public static final String FIELD_DFUNDERWRITERLASTNAME="dfUnderwriterLastName";
	//-- ========== SCR#859 begins ========== --//
	//-- by Neil on Feb 11, 2005
	public static final String FIELD_DFSERVICINGMORTGAGENUMBER = "dfServicingMortgageNumber";
	//-- ========== SCR#859 ends ========== --//
	
  /****************************MCM Impl Team XS_2.25 Changes starts**********************/
  public static final String FIELD_DFPRODUCTNAME = "dfProductName";
  public static final String FIELD_DFMIPREMIUM = "dfMiPremium";
  public static final String FIELD_DFTAXESCROW = "dfTaxEscrow";
  public static final String FIELD_DFCOMBINEDLTV = "dfCombinedLTV";
  public static final String FIELD_DFLOB = "dfLOB";
  /****************************MCM Impl Team XS_2.25 Changes ends**********************/
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

