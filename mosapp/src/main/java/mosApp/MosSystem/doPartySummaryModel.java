package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doPartySummaryModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public String getDfPartyType();


	/**
	 *
	 *
	 */
	public void setDfPartyType(String value);


	/**
	 *
	 *
	 */
	public String getDfPartyName();


	/**
	 *
	 *
	 */
	public void setDfPartyName(String value);


	/**
	 *
	 *
	 */
	public String getDfPartyCompany();


	/**
	 *
	 *
	 */
	public void setDfPartyCompany(String value);


	/**
	 *
	 *
	 */
	public String getDfPartyPhone();


	/**
	 *
	 *
	 */
	public void setDfPartyPhone(String value);


	/**
	 *
	 *
	 */
	public String getDfPartyFax();


	/**
	 *
	 *
	 */
	public void setDfPartyFax(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPartyProfileId();


	/**
	 *
	 *
	 */
	public void setDfPartyProfileId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfPartyShortName();


	/**
	 *
	 *
	 */
	public void setDfPartyShortName(String value);


	/**
	 *
	 *
	 */
	public String getDfPartyPhoneExt();


	/**
	 *
	 *
	 */
	public void setDfPartyPhoneExt(String value);


  //--DJ_PT_CR--start//
	/**
	 *
	 *
	 */
	public String getDfTransitNumber();


	/**
	 *
	 *
	 */
	public void setDfTransitNumber(String value);

	/**
	 *
	 *
	 */
	public String getDfAddressLine1();


	/**
	 *
	 *
	 */
	public void setDfAddressLine1(String value);

	/**
	 *
	 *
	 */
	public String getDfCity();


	/**
	 *
	 *
	 */
	public void setDfCity(String value);
  //--DJ_PT_CR--end//
	  /**
	   * 
	   * 
	   */
	  public java.math.BigDecimal getDfInstitutionID();

	  
	  /**
	   * 
	   * 
	   */
	  public void setDfInstitutionID(java.math.BigDecimal value);
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFPARTYTYPE="dfPartyType";
	public static final String FIELD_DFPARTYNAME="dfPartyName";
	public static final String FIELD_DFPARTYCOMPANY="dfPartyCompany";
	public static final String FIELD_DFPARTYPHONE="dfPartyPhone";
	public static final String FIELD_DFPARTYFAX="dfPartyFax";
	public static final String FIELD_DFPARTYPROFILEID="dfPartyProfileId";
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFPARTYSHORTNAME="dfPartyShortName";
	public static final String FIELD_DFPARTYPHONEEXT="dfPartyPhoneExt";

  //--Release2.1--//
  public static final String FIELD_DFPARTYTYPEID = "dfPartyTypeId";

  //--DJ_PT_CR--start//
  public static final String FIELD_DFTRANSITNUMBER = "dfTransitNum";
	public static final String FIELD_DFADDRESSLINE1="dfAddressLine1";
	public static final String FIELD_DFCITY="dfCity";
  //--DJ_PT_CR--end//
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

