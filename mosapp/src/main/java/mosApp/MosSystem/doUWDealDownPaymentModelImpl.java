package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doUWDealDownPaymentModelImpl extends QueryModelBase
	implements doUWDealDownPaymentModel
{
	/**
	 *
	 *
	 */
	public doUWDealDownPaymentModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDownPaymentSourceId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDOWNPAYMENTSOURCEID);
	}


	/**
	 *
	 *
	 */
	public void setDfDownPaymentSourceId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDOWNPAYMENTSOURCEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDownPaymentSourceTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDOWNPAYMENTSOURCETYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfDownPaymentSourceTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDOWNPAYMENTSOURCETYPEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDPSDescription()
	{
		return (String)getValue(FIELD_DFDPSDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfDPSDescription(String value)
	{
		setValue(FIELD_DFDPSDESCRIPTION,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL DOWNPAYMENTSOURCE.DEALID, " +
            "DOWNPAYMENTSOURCE.COPYID, DOWNPAYMENTSOURCE.DOWNPAYMENTSOURCEID, " +
            "DOWNPAYMENTSOURCE.DOWNPAYMENTSOURCETYPEID, DOWNPAYMENTSOURCE.AMOUNT, " +
            "DOWNPAYMENTSOURCE.DPSDESCRIPTION FROM DOWNPAYMENTSOURCE  __WHERE__  " +
            "ORDER BY DOWNPAYMENTSOURCE.DOWNPAYMENTSOURCEID  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME="DOWNPAYMENTSOURCE";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="DOWNPAYMENTSOURCE.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DOWNPAYMENTSOURCE.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFDOWNPAYMENTSOURCEID="DOWNPAYMENTSOURCE.DOWNPAYMENTSOURCEID";
	public static final String COLUMN_DFDOWNPAYMENTSOURCEID="DOWNPAYMENTSOURCEID";
	public static final String QUALIFIED_COLUMN_DFDOWNPAYMENTSOURCETYPEID="DOWNPAYMENTSOURCE.DOWNPAYMENTSOURCETYPEID";
	public static final String COLUMN_DFDOWNPAYMENTSOURCETYPEID="DOWNPAYMENTSOURCETYPEID";
	public static final String QUALIFIED_COLUMN_DFAMOUNT="DOWNPAYMENTSOURCE.AMOUNT";
	public static final String COLUMN_DFAMOUNT="AMOUNT";
	public static final String QUALIFIED_COLUMN_DFDPSDESCRIPTION="DOWNPAYMENTSOURCE.DPSDESCRIPTION";
	public static final String COLUMN_DFDPSDESCRIPTION="DPSDESCRIPTION";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOWNPAYMENTSOURCEID,
				COLUMN_DFDOWNPAYMENTSOURCEID,
				QUALIFIED_COLUMN_DFDOWNPAYMENTSOURCEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOWNPAYMENTSOURCETYPEID,
				COLUMN_DFDOWNPAYMENTSOURCETYPEID,
				QUALIFIED_COLUMN_DFDOWNPAYMENTSOURCETYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAMOUNT,
				COLUMN_DFAMOUNT,
				QUALIFIED_COLUMN_DFAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDPSDESCRIPTION,
				COLUMN_DFDPSDESCRIPTION,
				QUALIFIED_COLUMN_DFDPSDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

