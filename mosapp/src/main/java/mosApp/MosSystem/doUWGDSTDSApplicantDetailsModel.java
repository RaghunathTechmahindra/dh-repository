package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doUWGDSTDSApplicantDetailsModel
  extends QueryModel, SelectQueryModel
{
  /**
   *
   *
   */
  public java.math.BigDecimal getDfDealId();

  /**
   *
   *
   */
  public void setDfDealId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfBorrowerId();

  /**
   *
   *
   */
  public void setDfBorrowerId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfCopyId();

  /**
   *
   *
   */
  public void setDfCopyId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfFirstName();

  /**
   *
   *
   */
  public void setDfFirstName(String value);

  /**
   *
   *
   */
  public String getDfMiddleInitial();

  /**
   *
   *
   */
  public void setDfMiddleInitial(String value);

  /**
   *
   *
   */
  public String getDfLastName();

  /**
   *
   *
   */
  public void setDfLastName(String value);
  
  
  /**
   *
   *
   */
  public String getDfSuffix();

  /**
   *
   *
   */
  public void setDfSuffix(String value);

  /**
   *
   *
   */
  public String getDfBorrowerType();

  /**
   *
   *
   */
  public void setDfBorrowerType(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfBorrowerGDS();

  /**
   *
   *
   */
  public void setDfBorrowerGDS(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfBorrowerGDS3Yr();

  /**
   *
   *
   */
  public void setDfBorrowerGDS3Yr(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfBorrowerTDS();

  /**
   *
   *
   */
  public void setDfBorrowerTDS(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfBorrowerTDS3Yr();

  /**
   *
   *
   */
  public void setDfBorrowerTDS3Yr(java.math.BigDecimal value);

  //--DJ_LDI_CR--start--//
  //--> Bug fix to support Bilingual : By Billy 19May2004
  //--> NOTE : VLAD : pls stick with the Naming Convensions !!!
  /**
   *
   *
   */
  public String getDfInsuranceProportionsId();

  /**
   *
   *
   */
  public void setDfInsuranceProportionsId(String value);

  /**
   *
   *
   */
  public String getDfLifeStatusId();

  /**
   *
   *
   */
  public void setDfLifeStatusId(String value);

  /**
   *
   *
   */
  public String getDfDisabilityStatusId();

  /**
   *
   *
   */
  public void setDfDisabilityStatusId(String value);

  //======================= End Fix =========================

  /**
   *
   *
   */
  public java.math.BigDecimal getLifePremium();

  /**
   *
   *
   */
  public void setLifePremium(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDisabilityPremium();

  /**
   *
   *
   */
  public void setDisabilityPremium(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getCombinedLDPremium();

  /**
   *
   *
   */
  public void setCombinedLDPremium(java.math.BigDecimal value);

  //--DJ_LDI_CR--end--//

  //--DJ_CR201.2--start//
  /**
   *
   *
   */
  public String getStGuarantorOtherLoans();

  /**
   *
   *
   */
  public void setStGuarantorOtherLoans(String value);
  
  //--DJ_CR201.2--end//
  public java.math.BigDecimal getDfInstitutionId();
  public void setDfInstitutionId(java.math.BigDecimal id);  
  
  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  public static final String FIELD_DFDEALID = "dfDealId";
  public static final String FIELD_DFBORROWERID = "dfBorrowerId";
  public static final String FIELD_DFCOPYID = "dfCopyId";
  public static final String FIELD_DFFIRSTNAME = "dfFirstName";
  public static final String FIELD_DFMIDDLEINITIAL = "dfMiddleInitial";
  public static final String FIELD_DFLASTNAME = "dfLastName";
  public static final String FIELD_DFSUFFIX = "dfSuffix";
  public static final String FIELD_DFBORROWERTYPE = "dfBorrowerType";
  public static final String FIELD_DFBORROWERGDS = "dfBorrowerGDS";
  public static final String FIELD_DFBORROWERGDS3YR = "dfBorrowerGDS3Yr";
  public static final String FIELD_DFBORROWERTDS = "dfBorrowerTDS";
  public static final String FIELD_DFBORROWERTDS3YR = "dfBorrowerTDS3Yr";

  //--DJ_LDI_CR--start--//
  //--> Bug fix to support Bilingual : By Billy 19May2004
  public static final String FIELD_DFINSURANCEPROPORTIONSID = "dfInsuranceProportionsId";
  //public static final String FIELD_DFIPDESC = "dfInsProportionsDesc";
  //public static final String FIELD_DFLIFESTATUSDESC = "dfLifeStatusDesc";
  //public static final String FIELD_DFDISABILITYSTATUSDESC = "dfLDisabilityStatusDesc";
  public static final String FIELD_DFLIFESTATUSID = "dfLifeStatusId";
  public static final String FIELD_DFDISABILITYSTATUSID = "dfDisabilityStatusId";
  //======================= End Fix =========================
  public static final String FIELD_DFLIFEPREMIUM = "dfLifePremium";
  public static final String FIELD_DFDISABILITYPREMIUM = "dfDisabilityPremium";
  public static final String FIELD_DFCOMBINEDLDPREMIUM = "dfCombinedLDPremium";

  //--DJ_LDI_CR--end--//

  //--DJ_CR201.2--start//
  public static final String FIELD_DFGUARANTOROTHERLOANS = "dfGuarantorOtherLoans";

  //--DJ_CR201.2--end//
  public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////

}
