package mosApp.MosSystem;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.*;
import java.util.*;
// MigrationToDo : Still Needed? import spider.session.*;
// MigrationToDo : Still Needed? import spider.util.*;
// MigrationToDo : Still Needed? import spider.visual.*;
// MigrationToDo : Still Needed? import spider.*;
// MigrationToDo : Still Needed? import spider.database.*;
// MigrationToDo : Still Needed? import spider.html.*;
// MigrationToDo : Still Needed? import spider.event.*;
import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.util.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.security.*;
import com.basis100.workflow.entity.*;
import com.basis100.workflow.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.jdbcservices.jdbcexecutor.*;
import com.basis100.entity.*;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mosApp.*;
import MosSystem.*;

import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.html.*;



public class TiledHandlerCommonImpl extends RequestHandlingTiledViewBase
	implements Sc, Cloneable, TiledView
{

        public TiledHandlerCommonImpl(View parent, String name)
        {
    			super(parent,name);
        }

        protected View createChild(String name)
        {
            //// VERY TEMP RETURN TO COMPILE THE BX TILEDVIEW HIERARCHY.
            //// MUST BE REWRITTEN.

            return getChild(name);
        }

  public HttpSession theSession;

	public String defaultInstanceStateName =
			RequestManager.getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
	public SessionStateModelImpl theSessionState =
			(SessionStateModelImpl)RequestManager.getRequestContext().getModelManager().getModel(SessionStateModel.class,
					                                                                             defaultInstanceStateName,
					                                                                             true,
					                                                                             true);

	protected SessionResourceKit srk;
	public SysLogger logger;

	/**
	 *
	 *
	 */
	public Object cloneSafeShallow()
	{
		try
		{
			return clone();
		}
		catch(Exception e)
		{
			// won't fail ... but
			return null;
		}

	}


	/**
	 *
	 *
	 */
	protected Object clone()
		throws CloneNotSupportedException
	{
		return super.clone();

	}


	/**
	 *
	 *
	 */
	public SysLogger getSysLogger()
	{
		return logger;

	}

	/**
	 *
	 *
	 */
	public SessionResourceKit getSessionResourceKit()
	{
		return srk;

	}


	/**
	 *
	 *
	 */
	public void setSessionResourceKit(SessionResourceKit aSrk)
	{
		srk = aSrk;

	}



	/**
	 *
	 *
	 */
	public void unsetSessionResourceKit()
	{
		srk.freeResources();

		srk = null;

	}


	/**
	 * This method should be synchronized with the JATO framework
	 * architecture. SessionState is converted to the SessionStateModel/
	 * SessionStateModelImpl pair.
	 *
	 */
	////public SessionState getTheSession()
	////{
	////	return theSession;
	////}

	////public HttpSession getTheSession()
	////{
	////	return theSession;
	////}

	/**
	 * This method should be synchronized with the JATO framework
	 * architecture. SessionState is converted to the SessionStateModel/
	 * SessionStateModelImpl pair.
	 *
	 */
	////public void setTheSession(SessionState theSession)
	////{
	////	this.theSession = theSession;
	////}

	////public void setTheSession(HttpSession theSession)
	////{
	////	this.theSession = theSession;
	////}

	private static final String rowNumTag="^CSpCommand.currRowNumber=";
	private int rowNumTagLen=rowNumTag.length();

	/**
	 *  The next a couple of methods redesigned and implemented by JATO itself.
	 *  They should be replaced an appropriate substitution from the JATO framework.
	 *
	 */

	////public int getRowNdxFromWebEventMethod(CSpWebEvent event)
	public int getRowNdxFromWebEventMethod(com.iplanet.jato.view.event.ViewRequestInvocationEvent event)
	{
		////String methodStr = event.getMethod().toString();
                String methodStr = event.toString();

		if (methodStr == null) return 0;

		int ndx = methodStr.indexOf(rowNumTag);

		if (ndx == - 1) return 0;

		methodStr = methodStr.substring(ndx + rowNumTagLen);

		ndx = methodStr.indexOf(")");

		if (ndx == - 1) return 0;

		methodStr = methodStr.substring(0, ndx);

		try
		{
			return (new Integer(methodStr)).intValue();
			//// ocasional conversion by JATO.
			////return(newTypeConverter.asInt(Integer(methodStr)));
		}
		catch(Exception e)
		{
			;
		}

		return 0;

	}

	/**
	 *  The next a couple of methods redesigned and implemented by JATO itself.
	 *  They should be replaced an appropriate substitution from the JATO framework.
         *  These methods currently in the place for the compilation purposes only.
	 *
	 */

	public String getRepeatedField(Object fldValue, int rowNdx)
	{
		try
		{
			////if (fldValue.isNull())
			if (fldValue.equals(null))
			{
				return "";
			}
			String temp = null;
			if (fldValue instanceof Vector)
			{
				temp =((String)((Vector) fldValue).get(rowNdx));
			}
			else
			{
				temp =(String) fldValue;
			}
			return temp.toString();
		}
		catch(Exception e)
		{
			logger.debug("Exception @PHC.getRepeatedField: getting string from filed value, row = " + rowNdx + " - return empty string");
		}

		return "";

	}

	/**
	 *  The next a couple of methods redesigned and implemented by JATO itself.
	 *  They should be replaced an appropriate substitution from the JATO framework.
	 *
	 */

	public int getCursorAbsoluteNdx(PageCursorInfo tds, int relativeSelection)
	{
		int restoreSelectionNdx = tds.getRelSelectedRowNdx();

		tds.setRelSelectedRowNdx(relativeSelection);

		int absNdx = tds.getAbsSelectedRowNdx();

		tds.setRelSelectedRowNdx(restoreSelectionNdx);

		return absNdx;

	}

	/**
	 *
	 *
	 */
	public String replaceString(String token, String str, String replaceStr)
	{
		StringTokenizer parser = new StringTokenizer(str, token);

		String [ ] elements = null;

		String retStr = "";

		try
		{
			elements = new String [ parser.countTokens() ];
			int indx = 0;
			while(parser.hasMoreElements())
			{
				elements [ indx ] =(String) parser.nextElement();
				indx ++;
			}
			if (elements.length < 2) return str;
			for(int i = 0;
			i < elements.length;
			i ++)
			{
				if (i == 0)
				{
					retStr = elements [ 0 ];
					continue;
				}
				retStr = retStr + replaceStr + elements [ i ];
			}
		}
		catch(Exception ex)
		{
			logger.error("Exception occured @replaceString ");
			logger.error(ex);
			retStr = str;
		}

		return retStr;

	}


	/**
	 *
	 *
	 */
	protected Page getPage(String pageLabel)
	{
		try
		{
			return(PDC.getInstance()).getPageByLabel(pageLabel);
		}
		catch(Exception e)
		{
			;
		}

		return null;

	}


	/**
	 *
	 *
	 */
	protected Page getPage(int pageId)
	{
		try
		{
			return(PDC.getInstance()).getPage(pageId);
		}
		catch(Exception e)
		{
			;
		}

		return null;

	}
	private static String datePattern="yyyy-MM-dd HH:mm:ss.SSS";



	/**
	 *
	 *
	 */
	public int[] parseTerms(int termsInMonth)
	{
		int [ ] retval = new int [ 2 ];

		java.math.BigInteger bi1 = java.math.BigInteger.valueOf(termsInMonth);

		java.math.BigInteger div = java.math.BigInteger.valueOf(12);

		java.math.BigInteger [ ] biret = bi1.divideAndRemainder(div);

		////retval [ 0 ] = biret [ 0 ]TypeConverter.asInt();

		////retval [ 1 ] = biret [ 1 ]TypeConverter.asInt();

		return retval;

	}


	/**
	 *
	 *
	 */
	protected static String[] getMonthNames()
	{

		// ndx=0 : used to support case when 'no date entry' permitted
		String [ ] months =
		{
			" ", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"		}
		;

		return months;

	}


	/**
	 *
	 *
	 */
	protected String[] parseDateToTokens(java.util.Date dateVal)
	{
		if (dateVal == null) return null;

		String [ ] retVal =
		{
			"", "", "", ""		}
		;

		String [ ] months = getMonthNames();

		if (dateVal == null) return retVal;

		retVal [ 0 ] = String.valueOf(dateVal.getMonth() + 1);

		retVal [ 1 ] = String.valueOf(months [ dateVal.getMonth() + 1 ]);

		retVal [ 2 ] = String.valueOf(dateVal.getDay());

		retVal [ 3 ] = String.valueOf(dateVal.getYear());

		return retVal;

	}


	/**
	 *
	 *
	 */
	public String[] getParsedPhoneNo(String ano, int rowindx, String dfName)
	{
		String [ ] retval =
		{
			"", "", ""		}
		;

		try
		{
			// detect not present
			if (ano == null || ano.trim().length() == 0) return retval;
			retval [ 0 ] = ano.substring(0, 3).trim();
			retval [ 1 ] = ano.substring(3, 6).trim();
			retval [ 2 ] = ano.substring(6).trim();
		}
		catch(Exception e)
		{
			logger.trace("--T--> @getParsedPhoneNo: in=<" + ano + ">, df=" + dfName + "(rowNdx=" + rowindx + ") - use empty phone number");
		}

		return retval;

	}


	/**
	 *
	 *
	 */
	protected int parseStringToInt(String val)
	{
		try
		{
			return((int) Double.parseDouble(val));
		}
		catch(Exception e)
		{
			logger.warning("Exception @PHC.parseStringToInt: parse error in=<" + val + ">, returning 0");
		}

		return 0;

	}


	/**
	 *
	 *
	 */
	public String convertToHtmString(String input)
	{
		String retStr = "<font face=\"Courier New\" size=\"2\"> ";

		int x = 0;

		for(x = 0;
		x < input.length();
		x ++)
		{
			char c = input.charAt(x);

			// Convert Return Chars
			if ((c == '�') ||(c == 10))
			{
				retStr += "<br>";
			}
			else if ((c == '\'') ||(c == '\"'))
			{
				retStr += "`";
			}
			else if ((c == ' '))
			{
				retStr += "&nbsp;";
			}
			else if (! Character.isISOControl(c))
			{
				retStr += c;
			}
		}

		return retStr + "</font>";

	}

}

