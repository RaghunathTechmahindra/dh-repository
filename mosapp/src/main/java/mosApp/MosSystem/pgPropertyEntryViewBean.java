package mosApp.MosSystem;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;

import mosApp.SQLConnectionManagerImpl;
import MosSystem.Mc;

import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.view.CommandFieldDescriptor;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.ViewBeanBase;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HREF;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.RadioButtonGroup;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

import com.filogix.express.web.jato.ExpressViewBeanBase;

/**
 *
 *
 *
 */
public class pgPropertyEntryViewBean extends ExpressViewBeanBase
////public class pgPropertyEntryViewBean extends PropertyEntryHandler
{
	/**
	 *
	 *
	 */
	public pgPropertyEntryViewBean()
	{
		super(PAGE_NAME);
		setDefaultDisplayURL(DEFAULT_DISPLAY_URL);

		registerChildren();

		initialize();

	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		View superReturn = super.createChild(name);
		if (superReturn != null) {
			return superReturn;
		} else if (name.equals(CHILD_TBDEALID))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBDEALID,
				CHILD_TBDEALID,
				CHILD_TBDEALID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPAGENAMES))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES_RESET_VALUE,
				null);
      //--Release2.1--//
      ////Modified to set NonSelected Label as a CHILD_CBPAGENAMES_NONSELECTED_LABEL
			////child.setLabelForNoneSelected("Choose a Page");
      child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
			child.setOptions(cbPageNamesOptions);
			return child;
		}
		else
		if (name.equals(CHILD_BTPROCEED))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPROCEED,
				CHILD_BTPROCEED,
				CHILD_BTPROCEED_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF1))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF1,
				CHILD_HREF1,
				CHILD_HREF1_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF2))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF2,
				CHILD_HREF2,
				CHILD_HREF2_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF3))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF3,
				CHILD_HREF3,
				CHILD_HREF3_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF4))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF4,
				CHILD_HREF4,
				CHILD_HREF4_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF5))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF5,
				CHILD_HREF5,
				CHILD_HREF5_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF6))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF6,
				CHILD_HREF6,
				CHILD_HREF6_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF7))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF7,
				CHILD_HREF7,
				CHILD_HREF7_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF8))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF8,
				CHILD_HREF8,
				CHILD_HREF8_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF9))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF9,
				CHILD_HREF9,
				CHILD_HREF9_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF10))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF10,
				CHILD_HREF10,
				CHILD_HREF10_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCOMPANYNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTODAYDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUSERNAMETITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTWORKQUEUELINK))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_CHANGEPASSWORDHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLHISTORY))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOONOTES))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLSEARCH))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLLOG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STERRORFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_DETECTALERTTASKS))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTPREVTASKPAGE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPREVTASKPAGE,
				CHILD_BTPREVTASKPAGE,
				CHILD_BTPREVTASKPAGE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPREVTASKPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPREVTASKPAGELABEL,
				CHILD_STPREVTASKPAGELABEL,
				CHILD_STPREVTASKPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTNEXTTASKPAGE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTNEXTTASKPAGE,
				CHILD_BTNEXTTASKPAGE,
				CHILD_BTNEXTTASKPAGE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STNEXTTASKPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STNEXTTASKPAGELABEL,
				CHILD_STNEXTTASKPAGELABEL,
				CHILD_STNEXTTASKPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTASKNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTASKNAME,
				CHILD_STTASKNAME,
				CHILD_STTASKNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDPROPERTYID))
		{
			HiddenField child = new HiddenField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_HDPROPERTYID,
				doPropertyEntryPropertySelectModel.FIELD_DFPROPERTYID,
				CHILD_HDPROPERTYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDPROPERTYCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_HDPROPERTYCOPYID,
				doPropertyEntryPropertySelectModel.FIELD_DFCOPYID,
				CHILD_HDPROPERTYCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXPROPERTYSTREETNUMBER))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXPROPERTYSTREETNUMBER,
				doPropertyEntryPropertySelectModel.FIELD_DFSTREETNUMBER,
				CHILD_TXPROPERTYSTREETNUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXPROPERTYSTREETNAME))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXPROPERTYSTREETNAME,
				doPropertyEntryPropertySelectModel.FIELD_DFSTREETNAME,
				CHILD_TXPROPERTYSTREETNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPROPERTYSTREETTYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBPROPERTYSTREETTYPE,
				doPropertyEntryPropertySelectModel.FIELD_DFSTREETTYPEID,
				CHILD_CBPROPERTYSTREETTYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("   ");
			child.setOptions(cbPropertyStreetTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBPROPERTYSTREETDIRECTION))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBPROPERTYSTREETDIRECTION,
				doPropertyEntryPropertySelectModel.FIELD_DFPROPERTYSTREETDIRECTIONID,
				CHILD_CBPROPERTYSTREETDIRECTION_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbPropertyStreetDirectionOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXPROPERTYUNITNUMBER))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXPROPERTYUNITNUMBER,
				doPropertyEntryPropertySelectModel.FIELD_DFUNITNUMBER,
				CHILD_TXPROPERTYUNITNUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXPROPERTYADDRESSLINE2))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXPROPERTYADDRESSLINE2,
				doPropertyEntryPropertySelectModel.FIELD_DFADDRESSLINE2,
				CHILD_TXPROPERTYADDRESSLINE2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXPROPERTYCITY))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXPROPERTYCITY,
				doPropertyEntryPropertySelectModel.FIELD_DFCITY,
				CHILD_TXPROPERTYCITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPROPERTYPROVINCE))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBPROPERTYPROVINCE,
				doPropertyEntryPropertySelectModel.FIELD_DFPROPERTYPROVINCEID,
				CHILD_CBPROPERTYPROVINCE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbPropertyProvinceOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXPROPERTYPOSTALFSA))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXPROPERTYPOSTALFSA,
				doPropertyEntryPropertySelectModel.FIELD_DFPOSTALFSA,
				CHILD_TXPROPERTYPOSTALFSA_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXPROPERTYPOSTALLDU))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXPROPERTYPOSTALLDU,
				doPropertyEntryPropertySelectModel.FIELD_DFPOSTALLDU,
				CHILD_TXPROPERTYPOSTALLDU_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXLEGALDESC1))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXLEGALDESC1,
				doPropertyEntryPropertySelectModel.FIELD_DFLEGALLINE1,
				CHILD_TXLEGALDESC1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXLEGALDESC2))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXLEGALDESC2,
				doPropertyEntryPropertySelectModel.FIELD_DFLEGALLINE2,
				CHILD_TXLEGALDESC2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXLEGALDESC3))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXLEGALDESC3,
				doPropertyEntryPropertySelectModel.FIELD_DFLEGALLINE3,
				CHILD_TXLEGALDESC3_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBDWELLINGTYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBDWELLINGTYPE,
				doPropertyEntryPropertySelectModel.FIELD_DFPROPERTYDWELLINGTYPEID,
				CHILD_CBDWELLINGTYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbDwellingTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBDWELLINGSTYLE))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBDWELLINGSTYLE,
				doPropertyEntryPropertySelectModel.FIELD_DFDWELLILNGSTYLEID,
				CHILD_CBDWELLINGSTYLE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbDwellingStyleOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBNEWCONSTRUCTION))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBNEWCONSTRUCTION,
				doPropertyEntryPropertySelectModel.FIELD_DFNEWCONSTRUCTIONID,
				CHILD_CBNEWCONSTRUCTION_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbNewConstructionOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXSTRUCTUREAGE))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXSTRUCTUREAGE,
				doPropertyEntryPropertySelectModel.FIELD_DFSTRUCTUREAGE,
				CHILD_TXSTRUCTUREAGE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXBUILDERNAME))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXBUILDERNAME,
				doPropertyEntryPropertySelectModel.FIELD_DFBUILDERNAME,
				CHILD_TXBUILDERNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBGARAGESIZE))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBGARAGESIZE,
				doPropertyEntryPropertySelectModel.FIELD_DFGARAGESIZEID,
				CHILD_CBGARAGESIZE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbGarageSizeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBGARAGETYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBGARAGETYPE,
				doPropertyEntryPropertySelectModel.FIELD_DFGARAGETYPEID,
				CHILD_CBGARAGETYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbGarageTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXNOOFUNITS))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXNOOFUNITS,
				doPropertyEntryPropertySelectModel.FIELD_DFNOOFUNITS,
				CHILD_TXNOOFUNITS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXNOOFBEDROOMS))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXNOOFBEDROOMS,
				doPropertyEntryPropertySelectModel.FIELD_DFNUMBEROFBEDROOMS,
				CHILD_TXNOOFBEDROOMS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXLIVINGSPACE))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXLIVINGSPACE,
				doPropertyEntryPropertySelectModel.FIELD_DFLIVINGSPACE,
				CHILD_TXLIVINGSPACE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBUNITS))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBUNITS,
				doPropertyEntryPropertySelectModel.FIELD_DFPROPERTYLIVINGSPACEMEASUREID,
				CHILD_CBUNITS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbUnitsOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXLOTSIZE))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXLOTSIZE,
				doPropertyEntryPropertySelectModel.FIELD_DFLOTSIZEDEPTH,
				CHILD_TXLOTSIZE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXLOTSIZE2))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXLOTSIZE2,
				doPropertyEntryPropertySelectModel.FIELD_DFLOTSIZEFRONTPAGE,
				CHILD_TXLOTSIZE2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBLOTSIZEUNITS))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBLOTSIZEUNITS,
				doPropertyEntryPropertySelectModel.FIELD_DFLOTSIZEUNITMEASUREDID,
				CHILD_CBLOTSIZEUNITS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbLotSizeUnitsOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBHEATTYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBHEATTYPE,
				doPropertyEntryPropertySelectModel.FIELD_DFPROPERTYHEATTYPEID,
				CHILD_CBHEATTYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbHeatTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBUFFIINSULATION))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBUFFIINSULATION,
				doPropertyEntryPropertySelectModel.FIELD_DFUFFIINSULATION,
				CHILD_CBUFFIINSULATION_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbUFFIInsulationOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBWATERTYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBWATERTYPE,
				doPropertyEntryPropertySelectModel.FIELD_DFPROPERTYWATERTYPEID,
				CHILD_CBWATERTYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbWaterTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBSEWAGETYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBSEWAGETYPE,
				doPropertyEntryPropertySelectModel.FIELD_DFPROPERTYSWEGETYPEID,
				CHILD_CBSEWAGETYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbSewageTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBPROPERTYUSAGETYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBPROPERTYUSAGETYPE,
				doPropertyEntryPropertySelectModel.FIELD_DFPROPERTYUSAGEID,
				CHILD_CBPROPERTYUSAGETYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbPropertyUsageTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBOCCUPANCY))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBOCCUPANCY,
				doPropertyEntryPropertySelectModel.FIELD_DFPROPERTYOCCUPANCYTYPEID,
				CHILD_CBOCCUPANCY_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbOccupancyOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBPROPERTTYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBPROPERTTYPE,
				doPropertyEntryPropertySelectModel.FIELD_DFPROPERTYTYPEID,
				CHILD_CBPROPERTTYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbPropertTypeOptions);
			return child;
		}
//		***** Change by NBC Impl. Team - Version 1.2 - Start *****//
		if (name.equals(CHILD_CBREQUESTAPPRAISAL))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBREQUESTAPPRAISAL,
				doPropertyEntryPropertySelectModel.FIELD_DFREQUESTAPPRAISALID,
				CHILD_CBREQUESTAPPRAISAL_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbRequestAppraisalOptions);
			return child;
		}
//		***** Change by NBC Impl. Team - Version 1.2 - End *****//		
		else
		if (name.equals(CHILD_CBPROPERTYLOCATION))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBPROPERTYLOCATION,
				doPropertyEntryPropertySelectModel.FIELD_DFPROPERTYLOCATIONID,
				CHILD_CBPROPERTYLOCATION_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbPropertyLocationOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBZONING))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBZONING,
				doPropertyEntryPropertySelectModel.FIELD_DFZONING,
				CHILD_CBZONING_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbZoningOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXPURCHASEPRICE))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXPURCHASEPRICE,
				doPropertyEntryPropertySelectModel.FIELD_DFPURCHASEPRICE,
				CHILD_TXPURCHASEPRICE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXLANDVALUE))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXLANDVALUE,
				doPropertyEntryPropertySelectModel.FIELD_DFLANDVALUE,
				CHILD_TXLANDVALUE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXESTIMATEDVALUE))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXESTIMATEDVALUE,
				doPropertyEntryPropertySelectModel.FIELD_DFESTIMATEVALUE,
				CHILD_TXESTIMATEDVALUE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_RBMLSLISTING))
		{
			RadioButtonGroup child = new RadioButtonGroup( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_RBMLSLISTING,
				doPropertyEntryPropertySelectModel.FIELD_DFMLSLISTINGFLAG,
				CHILD_RBMLSLISTING_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(rbMLSListingOptions);
			return child;
		}
		else
		if (name.equals(CHILD_STLTV))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_STLTV,
				doPropertyEntryPropertySelectModel.FIELD_DFLTV,
				CHILD_STLTV_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_REPEATEDPROPERTYEXPENSES))
		{
			pgPropertyEntryRepeatedPropertyExpensesTiledView child = new pgPropertyEntryRepeatedPropertyExpensesTiledView(this,
				CHILD_REPEATEDPROPERTYEXPENSES);
			return child;
		}
		else
		if (name.equals(CHILD_BTADDEXPENSE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTADDEXPENSE,
				CHILD_BTADDEXPENSE,
				CHILD_BTADDEXPENSE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_SESSIONUSERID))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTPROPERTYSUBMIT))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPROPERTYSUBMIT,
				CHILD_BTPROPERTYSUBMIT,
				CHILD_BTPROPERTYSUBMIT_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTPROPERTYCANCEL))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPROPERTYCANCEL,
				CHILD_BTPROPERTYCANCEL,
				CHILD_BTPROPERTYCANCEL_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTPROPERTYCANCELCURRENTCHANGES))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPROPERTYCANCELCURRENTCHANGES,
				CHILD_BTPROPERTYCANCELCURRENTCHANGES,
				CHILD_BTPROPERTYCANCELCURRENTCHANGES_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASOK,
				CHILD_STPMHASOK,
				CHILD_STPMHASOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMTITLE,
				CHILD_STPMTITLE,
				CHILD_STPMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMONOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMONOK,
				CHILD_STPMONOK,
				CHILD_STPMONOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGS,
				CHILD_STPMMSGS,
				CHILD_STPMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMTITLE,
				CHILD_STAMTITLE,
				CHILD_STAMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGS,
				CHILD_STAMMSGS,
				CHILD_STAMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMDIALOGMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMBUTTONSHTML))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTARGETPE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTARGETPE,
				CHILD_STTARGETPE,
				CHILD_STTARGETPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXTOTALPROPERTYEXP))
		{
			TextField child = new TextField(this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_TXTOTALPROPERTYEXP,
				doPropertyEntryPropertySelectModel.FIELD_DFTOTALPROPERTYEXP,
				CHILD_TXTOTALPROPERTYEXP_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STVALSDATA))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STVALSDATA,
				CHILD_STVALSDATA,
				CHILD_STVALSDATA_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTOK))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTOK,
				CHILD_BTOK,
				CHILD_BTOK_RESET_VALUE,
				null);
				return child;

		}
//		Bebin MI additions. createChild(Str)
		else
		if (name.equals(CHILD_RBSUBDIVISIONDISCOUNT))
		{
			RadioButtonGroup child = new RadioButtonGroup( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_RBSUBDIVISIONDISCOUNT,
				doPropertyEntryPropertySelectModel.FIELD_DFSUBDIVISIONDISCOUNT,
				CHILD_RBSUBDIVISIONDISCOUNT_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(rbSubdivisionDiscountOptions);
			return child;
		}
		else
		if (name.equals(CHILD_RBUFFIINSULATION))
		{
			RadioButtonGroup child = new RadioButtonGroup( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_RBUFFIINSULATION,
				doPropertyEntryPropertySelectModel.FIELD_DFUFFIINSULATION,
				CHILD_RBUFFIINSULATION_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(rbUFFIInsulationOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBTENURETYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyEntryPropertySelectModel(),
				CHILD_CBTENURETYPE,
				doPropertyEntryPropertySelectModel.FIELD_DFTENURETYPEID,
				CHILD_CBTENURETYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbTenureTypeOptions);
			return child;
		}
		else
        if(name.equals(CHILD_RBMIENERGYEFFICIENCY))
        {
            RadioButtonGroup child = new RadioButtonGroup(this,
                getdoPropertyEntryPropertySelectModel(),
                CHILD_RBMIENERGYEFFICIENCY,
                doPropertyEntryPropertySelectModel.FIELD_DFMIENERGYEFFICIENCY,
                CHILD_RBMIENERGYEFFICIENCY_RESET_VALUE,
                null);
            child.setLabelForNoneSelected("");
            child.setOptions(rbMIEnergyEfficiencyOptions);
            return child;
        }
		else
        if(name.equals(CHILD_TXONRESERVETRUSTAGREEMENTNUMBER))
        {
            TextField child = new TextField(this,
                getdoPropertyEntryPropertySelectModel(),
                CHILD_TXONRESERVETRUSTAGREEMENTNUMBER,
                doPropertyEntryPropertySelectModel.FIELD_DFONRESERVETRUSTAGREEMENTNUMBER,
                CHILD_TXONRESERVETRUSTAGREEMENTNUMBER_RESET_VALUE,
                null);
            return child;
        }
        else
        if(name.equals(CHILD_HDMORTGAGEINSURERID))
        {
            HiddenField child = new HiddenField(this,
                getdoPropertyEntryPropertySelectModel(),
                CHILD_HDMORTGAGEINSURERID,
                doPropertyEntryPropertySelectModel.FIELD_DFMORTGAGEINSURERID,
                CHILD_HDMORTGAGEINSURERID_RESET_VALUE,
                null);
            return child;
        }
		//End MI additions. createChild(Str)
		else
		if (name.equals(CHILD_STVIEWONLYTAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STVIEWONLYTAG,
				CHILD_STVIEWONLYTAG,
				CHILD_STVIEWONLYTAG_RESET_VALUE,
				null);
			return child;
		}
    //// Hidden button to trigger the ActiveMessage 'fake' submit button
    //// via the new BX ActMessageCommand class
    else
    if (name.equals(CHILD_BTACTMSG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTACTMSG,
				CHILD_BTACTMSG,
				CHILD_BTACTMSG_RESET_VALUE,
				new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));
				return child;
    }
    //--Release2.1--//
    //// New link to toggle language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new session
    //// language id throughout the all modulus.
		else
		if (name.equals(CHILD_TOGGLELANGUAGEHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF_RESET_VALUE,
				null);
				return child;
    }
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{

		super.resetChildren();
		getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
		getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
		getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
		getHref1().setValue(CHILD_HREF1_RESET_VALUE);
		getHref2().setValue(CHILD_HREF2_RESET_VALUE);
		getHref3().setValue(CHILD_HREF3_RESET_VALUE);
		getHref4().setValue(CHILD_HREF4_RESET_VALUE);
		getHref5().setValue(CHILD_HREF5_RESET_VALUE);
		getHref6().setValue(CHILD_HREF6_RESET_VALUE);
		getHref7().setValue(CHILD_HREF7_RESET_VALUE);
		getHref8().setValue(CHILD_HREF8_RESET_VALUE);
		getHref9().setValue(CHILD_HREF9_RESET_VALUE);
		getHref10().setValue(CHILD_HREF10_RESET_VALUE);
		getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
		getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
		getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
		getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
		getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
		getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
		getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
		getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
		getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
		getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
		getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
		getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
		getBtPrevTaskPage().setValue(CHILD_BTPREVTASKPAGE_RESET_VALUE);
		getStPrevTaskPageLabel().setValue(CHILD_STPREVTASKPAGELABEL_RESET_VALUE);
		getBtNextTaskPage().setValue(CHILD_BTNEXTTASKPAGE_RESET_VALUE);
		getStNextTaskPageLabel().setValue(CHILD_STNEXTTASKPAGELABEL_RESET_VALUE);
		getStTaskName().setValue(CHILD_STTASKNAME_RESET_VALUE);
		getHdPropertyId().setValue(CHILD_HDPROPERTYID_RESET_VALUE);
		getHdPropertyCopyId().setValue(CHILD_HDPROPERTYCOPYID_RESET_VALUE);
		getTxPropertyStreetNumber().setValue(CHILD_TXPROPERTYSTREETNUMBER_RESET_VALUE);
		getTxPropertyStreetName().setValue(CHILD_TXPROPERTYSTREETNAME_RESET_VALUE);
		getCbPropertyStreetType().setValue(CHILD_CBPROPERTYSTREETTYPE_RESET_VALUE);
		getCbPropertyStreetDirection().setValue(CHILD_CBPROPERTYSTREETDIRECTION_RESET_VALUE);
		getTxPropertyUnitNumber().setValue(CHILD_TXPROPERTYUNITNUMBER_RESET_VALUE);
		getTxPropertyAddressLine2().setValue(CHILD_TXPROPERTYADDRESSLINE2_RESET_VALUE);
		getTxPropertyCity().setValue(CHILD_TXPROPERTYCITY_RESET_VALUE);
		getCbPropertyProvince().setValue(CHILD_CBPROPERTYPROVINCE_RESET_VALUE);
		getTxPropertyPostalFSA().setValue(CHILD_TXPROPERTYPOSTALFSA_RESET_VALUE);
		getTxPropertyPostalLDU().setValue(CHILD_TXPROPERTYPOSTALLDU_RESET_VALUE);
		getTxLegalDesc1().setValue(CHILD_TXLEGALDESC1_RESET_VALUE);
		getTxLegalDesc2().setValue(CHILD_TXLEGALDESC2_RESET_VALUE);
		getTxLegalDesc3().setValue(CHILD_TXLEGALDESC3_RESET_VALUE);
		getCbDwellingType().setValue(CHILD_CBDWELLINGTYPE_RESET_VALUE);
		getCbDwellingStyle().setValue(CHILD_CBDWELLINGSTYLE_RESET_VALUE);
		getCbNewConstruction().setValue(CHILD_CBNEWCONSTRUCTION_RESET_VALUE);
		getTxStructureAge().setValue(CHILD_TXSTRUCTUREAGE_RESET_VALUE);
		getTxBuilderName().setValue(CHILD_TXBUILDERNAME_RESET_VALUE);
		getCbGarageSize().setValue(CHILD_CBGARAGESIZE_RESET_VALUE);
		getCbGarageType().setValue(CHILD_CBGARAGETYPE_RESET_VALUE);
		getTxNoOfUnits().setValue(CHILD_TXNOOFUNITS_RESET_VALUE);
		getTxNoOfBedRooms().setValue(CHILD_TXNOOFBEDROOMS_RESET_VALUE);
		getTxLivingSpace().setValue(CHILD_TXLIVINGSPACE_RESET_VALUE);
		getCbUnits().setValue(CHILD_CBUNITS_RESET_VALUE);
		getTxLotSize().setValue(CHILD_TXLOTSIZE_RESET_VALUE);
		getTxLotSize2().setValue(CHILD_TXLOTSIZE2_RESET_VALUE);
		getCbLotSizeUnits().setValue(CHILD_CBLOTSIZEUNITS_RESET_VALUE);
		getCbHeatType().setValue(CHILD_CBHEATTYPE_RESET_VALUE);
		getCbUFFIInsulation().setValue(CHILD_CBUFFIINSULATION_RESET_VALUE);
		getCbWaterType().setValue(CHILD_CBWATERTYPE_RESET_VALUE);
		getCbSewageType().setValue(CHILD_CBSEWAGETYPE_RESET_VALUE);
		getCbPropertyUsageType().setValue(CHILD_CBPROPERTYUSAGETYPE_RESET_VALUE);
		getCbOccupancy().setValue(CHILD_CBOCCUPANCY_RESET_VALUE);
		getCbPropertType().setValue(CHILD_CBPROPERTTYPE_RESET_VALUE);
//		***** Change by NBC Impl. Team - Version 1.2 - Start *****//
		getCbRequestAppraisal().setValue(CHILD_CBREQUESTAPPRAISAL_RESET_VALUE);
//		***** Change by NBC Impl. Team - Version 1.2 - End *****//		
		getCbPropertyLocation().setValue(CHILD_CBPROPERTYLOCATION_RESET_VALUE);
		getCbZoning().setValue(CHILD_CBZONING_RESET_VALUE);
		getTxPurchasePrice().setValue(CHILD_TXPURCHASEPRICE_RESET_VALUE);
		getTxLandValue().setValue(CHILD_TXLANDVALUE_RESET_VALUE);
		getTxEstimatedValue().setValue(CHILD_TXESTIMATEDVALUE_RESET_VALUE);
		getRbMLSListing().setValue(CHILD_RBMLSLISTING_RESET_VALUE);
		getStLTV().setValue(CHILD_STLTV_RESET_VALUE);
		getRepeatedPropertyExpenses().resetChildren();
		getBtAddExpense().setValue(CHILD_BTADDEXPENSE_RESET_VALUE);
		getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
		getBtPropertySubmit().setValue(CHILD_BTPROPERTYSUBMIT_RESET_VALUE);
		getBtPropertyCancel().setValue(CHILD_BTPROPERTYCANCEL_RESET_VALUE);
		getBtPropertyCancelCurrentChanges().setValue(CHILD_BTPROPERTYCANCELCURRENTCHANGES_RESET_VALUE);
		getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
		getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
		getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
		getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
		getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
		getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
		getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
		getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
		getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
		getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
		getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
		getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
		getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
		getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
		getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
		getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
		getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
		getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
		getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
		getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
		getStTargetPE().setValue(CHILD_STTARGETPE_RESET_VALUE);
		getTxTotalPropertyExp().setValue(CHILD_TXTOTALPROPERTYEXP_RESET_VALUE);
		getStVALSData().setValue(CHILD_STVALSDATA_RESET_VALUE);
		getBtOK().setValue(CHILD_BTOK_RESET_VALUE);
		getStViewOnlyTag().setValue(CHILD_STVIEWONLYTAG_RESET_VALUE);
    //// new hidden button to activate 'fake' submit button from the ActiveMessage class.
    getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
		getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);
		
		//MI additions
		getRbSubdivisionDiscount().setValue(CHILD_RBSUBDIVISIONDISCOUNT_RESET_VALUE);
		getRbUFFIInsulation().setValue(CHILD_RBUFFIINSULATION_RESET_VALUE);
		getCbTenureType().setValue(CHILD_CBTENURETYPE_RESET_VALUE);
		getTxOnReserveTrustAgreementNumber().setValue(CHILD_TXONRESERVETRUSTAGREEMENTNUMBER_RESET_VALUE);
		getRbMIEnergyEfficiency().setValue(CHILD_RBMIENERGYEFFICIENCY_RESET_VALUE);
		getHdMortgageInsurerId().setValue(CHILD_HDMORTGAGEINSURERID_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		super.registerChildren();
		registerChild(CHILD_TBDEALID,TextField.class);
		registerChild(CHILD_CBPAGENAMES,ComboBox.class);
		registerChild(CHILD_BTPROCEED,Button.class);
		registerChild(CHILD_HREF1,HREF.class);
		registerChild(CHILD_HREF2,HREF.class);
		registerChild(CHILD_HREF3,HREF.class);
		registerChild(CHILD_HREF4,HREF.class);
		registerChild(CHILD_HREF5,HREF.class);
		registerChild(CHILD_HREF6,HREF.class);
		registerChild(CHILD_HREF7,HREF.class);
		registerChild(CHILD_HREF8,HREF.class);
		registerChild(CHILD_HREF9,HREF.class);
		registerChild(CHILD_HREF10,HREF.class);
		registerChild(CHILD_STPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STCOMPANYNAME,StaticTextField.class);
		registerChild(CHILD_STTODAYDATE,StaticTextField.class);
		registerChild(CHILD_STUSERNAMETITLE,StaticTextField.class);
		registerChild(CHILD_BTWORKQUEUELINK,Button.class);
		registerChild(CHILD_CHANGEPASSWORDHREF,HREF.class);
		registerChild(CHILD_BTTOOLHISTORY,Button.class);
		registerChild(CHILD_BTTOONOTES,Button.class);
		registerChild(CHILD_BTTOOLSEARCH,Button.class);
		registerChild(CHILD_BTTOOLLOG,Button.class);
		registerChild(CHILD_STERRORFLAG,StaticTextField.class);
		registerChild(CHILD_DETECTALERTTASKS,HiddenField.class);
		registerChild(CHILD_BTPREVTASKPAGE,Button.class);
		registerChild(CHILD_STPREVTASKPAGELABEL,StaticTextField.class);
		registerChild(CHILD_BTNEXTTASKPAGE,Button.class);
		registerChild(CHILD_STNEXTTASKPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STTASKNAME,StaticTextField.class);
		registerChild(CHILD_HDPROPERTYID,HiddenField.class);
		registerChild(CHILD_HDPROPERTYCOPYID,HiddenField.class);
		registerChild(CHILD_TXPROPERTYSTREETNUMBER,TextField.class);
		registerChild(CHILD_TXPROPERTYSTREETNAME,TextField.class);
		registerChild(CHILD_CBPROPERTYSTREETTYPE,ComboBox.class);
		registerChild(CHILD_CBPROPERTYSTREETDIRECTION,ComboBox.class);
		registerChild(CHILD_TXPROPERTYUNITNUMBER,TextField.class);
		registerChild(CHILD_TXPROPERTYADDRESSLINE2,TextField.class);
		registerChild(CHILD_TXPROPERTYCITY,TextField.class);
		registerChild(CHILD_CBPROPERTYPROVINCE,ComboBox.class);
		registerChild(CHILD_TXPROPERTYPOSTALFSA,TextField.class);
		registerChild(CHILD_TXPROPERTYPOSTALLDU,TextField.class);
		registerChild(CHILD_TXLEGALDESC1,TextField.class);
		registerChild(CHILD_TXLEGALDESC2,TextField.class);
		registerChild(CHILD_TXLEGALDESC3,TextField.class);
		registerChild(CHILD_CBDWELLINGTYPE,ComboBox.class);
		registerChild(CHILD_CBDWELLINGSTYLE,ComboBox.class);
		registerChild(CHILD_CBNEWCONSTRUCTION,ComboBox.class);
		registerChild(CHILD_TXSTRUCTUREAGE,TextField.class);
		registerChild(CHILD_TXBUILDERNAME,TextField.class);
		registerChild(CHILD_CBGARAGESIZE,ComboBox.class);
		registerChild(CHILD_CBGARAGETYPE,ComboBox.class);
		registerChild(CHILD_TXNOOFUNITS,TextField.class);
		registerChild(CHILD_TXNOOFBEDROOMS,TextField.class);
		registerChild(CHILD_TXLIVINGSPACE,TextField.class);
		registerChild(CHILD_CBUNITS,ComboBox.class);
		registerChild(CHILD_TXLOTSIZE,TextField.class);
		registerChild(CHILD_TXLOTSIZE2,TextField.class);
		registerChild(CHILD_CBLOTSIZEUNITS,ComboBox.class);
		registerChild(CHILD_CBHEATTYPE,ComboBox.class);
		registerChild(CHILD_CBUFFIINSULATION,ComboBox.class);
		registerChild(CHILD_CBWATERTYPE,ComboBox.class);
		registerChild(CHILD_CBSEWAGETYPE,ComboBox.class);
		registerChild(CHILD_CBPROPERTYUSAGETYPE,ComboBox.class);
		registerChild(CHILD_CBOCCUPANCY,ComboBox.class);
		registerChild(CHILD_CBPROPERTTYPE,ComboBox.class);
//		***** Change by NBC Impl. Team - Version 1.2 - Start *****//
		registerChild(CHILD_CBREQUESTAPPRAISAL,ComboBox.class);
//		***** Change by NBC Impl. Team - Version 1.2 - End *****//		
		registerChild(CHILD_CBPROPERTYLOCATION,ComboBox.class);
		registerChild(CHILD_CBZONING,ComboBox.class);
		registerChild(CHILD_TXPURCHASEPRICE,TextField.class);
		registerChild(CHILD_TXLANDVALUE,TextField.class);
		registerChild(CHILD_TXESTIMATEDVALUE,TextField.class);
		registerChild(CHILD_RBMLSLISTING,RadioButtonGroup.class);
		registerChild(CHILD_STLTV,StaticTextField.class);
		registerChild(CHILD_REPEATEDPROPERTYEXPENSES,pgPropertyEntryRepeatedPropertyExpensesTiledView.class);
		registerChild(CHILD_BTADDEXPENSE,Button.class);
		registerChild(CHILD_SESSIONUSERID,HiddenField.class);
		registerChild(CHILD_BTPROPERTYSUBMIT,Button.class);
		registerChild(CHILD_BTPROPERTYCANCEL,Button.class);
		registerChild(CHILD_BTPROPERTYCANCELCURRENTCHANGES,Button.class);
		registerChild(CHILD_STPMGENERATE,StaticTextField.class);
		registerChild(CHILD_STPMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STPMHASINFO,StaticTextField.class);
		registerChild(CHILD_STPMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STPMHASOK,StaticTextField.class);
		registerChild(CHILD_STPMTITLE,StaticTextField.class);
		registerChild(CHILD_STPMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STPMONOK,StaticTextField.class);
		registerChild(CHILD_STPMMSGS,StaticTextField.class);
		registerChild(CHILD_STPMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMGENERATE,StaticTextField.class);
		registerChild(CHILD_STAMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STAMHASINFO,StaticTextField.class);
		registerChild(CHILD_STAMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STAMTITLE,StaticTextField.class);
		registerChild(CHILD_STAMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STAMMSGS,StaticTextField.class);
		registerChild(CHILD_STAMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMDIALOGMSG,StaticTextField.class);
		registerChild(CHILD_STAMBUTTONSHTML,StaticTextField.class);
		registerChild(CHILD_STTARGETPE,StaticTextField.class);
		registerChild(CHILD_TXTOTALPROPERTYEXP,TextField.class);
		registerChild(CHILD_STVALSDATA,StaticTextField.class);
		registerChild(CHILD_BTOK,Button.class);
		registerChild(CHILD_STVIEWONLYTAG,StaticTextField.class);
    //// New hidden button implementation.
    registerChild(CHILD_BTACTMSG,Button.class);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
		registerChild(CHILD_TOGGLELANGUAGEHREF,HREF.class);
		
		
		//MI additions
		registerChild(CHILD_RBSUBDIVISIONDISCOUNT, RadioButtonGroup.class);
		registerChild(CHILD_RBUFFIINSULATION, RadioButtonGroup.class);
		registerChild(CHILD_CBTENURETYPE,ComboBox.class);
		registerChild(CHILD_TXONRESERVETRUSTAGREEMENTNUMBER, TextField.class);
		registerChild(CHILD_RBMIENERGYEFFICIENCY, RadioButtonGroup.class);
		registerChild(CHILD_HDMORTGAGEINSURERID, HiddenField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
        ;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this);

		handler.setupBeforePageGeneration();

    //--Release2.1--start//
    ////Populate all ComboBoxes manually here
    //// 1. Setup Options for cbPageNames
    //cbPageNamesOptions..populate(getRequestContext());
	int langId = handler.getTheSessionState().getLanguageId();
    cbPageNamesOptions.populate(getRequestContext(), langId);

    ////Setup NoneSelected Label for cbPageNames
    CHILD_CBPAGENAMES_NONSELECTED_LABEL =
        BXResources.getGenericMsg("CBPAGENAMES_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    ////Check if the cbPageNames is already created
    if(getCbPageNames() != null)
      getCbPageNames().setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);

    //// 2-... Repopulate all Comboboxes on the page.
    cbPropertyStreetTypeOptions.populate(getRequestContext());
    cbPropertyStreetDirectionOptions.populate(getRequestContext());
    cbPropertyProvinceOptions.populate(getRequestContext());
    cbDwellingTypeOptions.populate(getRequestContext());
    cbDwellingStyleOptions.populate(getRequestContext());
    cbNewConstructionOptions.populate(getRequestContext());
    cbGarageSizeOptions.populate(getRequestContext());
    cbGarageTypeOptions.populate(getRequestContext());
    cbUnitsOptions.populate(getRequestContext());
    cbLotSizeUnitsOptions.populate(getRequestContext());
    cbHeatTypeOptions.populate(getRequestContext());
    cbWaterTypeOptions.populate(getRequestContext());
    cbSewageTypeOptions.populate(getRequestContext());
    cbPropertyUsageTypeOptions.populate(getRequestContext());
    cbOccupancyOptions.populate(getRequestContext());
    cbPropertTypeOptions.populate(getRequestContext());
//  ***** Change by NBC Impl. Team - Version 1.2 - Start *****//
    cbRequestAppraisalOptions.populate(getRequestContext());
//  ***** Change by NBC Impl. Team - Version 1.2 - End *****//    
    cbPropertyLocationOptions.populate(getRequestContext());
    cbZoningOptions.populate(getRequestContext());

    // 3-... Setup the Yes/No ComboBox and Radio Options
    String yesStr = BXResources.getGenericMsg("YES_LABEL", handler.getTheSessionState().getLanguageId());
    String noStr = BXResources.getGenericMsg("NO_LABEL", handler.getTheSessionState().getLanguageId());

    cbUFFIInsulationOptions.setOptions(new String[]{yesStr, noStr},new String[]{"Y", "N"});

    rbMLSListingOptions.setOptions(new String[]{yesStr, noStr},new String[]{"Y", "N"});
    
    //MI addition
    rbSubdivisionDiscountOptions.setOptions(new String[]{yesStr, noStr},new String[]{"Y", "N"});
    rbMIEnergyEfficiencyOptions.setOptions(new String[]{yesStr, noStr},new String[]{"Y", "N"});
    rbUFFIInsulationOptions.setOptions(new String[]{yesStr, noStr},new String[]{"Y", "N"});
    cbTenureTypeOptions.populate(getRequestContext());

    handler.pageSaveState();
		super.beginDisplay(event);
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		handler.populatePageDisplayFields();

		handler.pageSaveState();

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics this_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	/**
	 *
	 *
	 */
	public TextField getTbDealId()
	{
		return (TextField)getChild(CHILD_TBDEALID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPageNames()
	{
		return (ComboBox)getChild(CHILD_CBPAGENAMES);
	}

	/**
	 *
	 *
	 */
  //--DJ_Ticket661--start--//
  //Modified to sort by PageLabel based on the selected language
  static class CbPageNamesOptionList extends GotoOptionList
 {
    /**
     *
     *
     */
    CbPageNamesOptionList() {

    }


    public void populate(RequestContext rc) {
      Connection c = null;
      try {
        clear();
        SelectQueryModel m = null;

        SelectQueryExecutionContext eContext = new
          SelectQueryExecutionContext(
          (Connection)null,

          DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,

          DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null) {
          m = new doPageNameLabelModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else {
          m = (SelectQueryModel)rc.getModelManager().getModel(doPageNameLabelModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        // Sort the results from DataObject
        TreeMap sorted = new TreeMap();
        while (m.next()) {
          Object dfPageLabel =
            m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
          String label = (dfPageLabel == null ? "" : dfPageLabel.toString());
          Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
          String value = (dfPageId == null ? "" : dfPageId.toString());
          String[] theVal = new String[2];
          theVal[0] = label;
          theVal[1] = value;
          sorted.put(label, theVal);
        }

        // Set the sorted list to the optionlist
        Iterator theList = sorted.values().iterator();
        String[] theVal = new String[2];
        while (theList.hasNext()) {
          theVal = (String[]) (theList.next());
          add(theVal[0], theVal[1]);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      finally {
        try {
          if (c != null)
            c.close();
        }
        catch (SQLException ex) {
              // ignore
        }
      }
    }
 }
 //--DJ_Ticket661--end--//

	/**
	 *
	 *
	 */
	public void handleBtProceedRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleGoPage();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtProceed()
	{
		return (Button)getChild(CHILD_BTPROCEED);
	}


	/**
	 *
	 *
	 */
	public void handleHref1Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(0);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref1()
	{
		return (HREF)getChild(CHILD_HREF1);
	}


	/**
	 *
	 *
	 */
	public void handleHref2Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(1);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref2()
	{
		return (HREF)getChild(CHILD_HREF2);
	}


	/**
	 *
	 *
	 */
	public void handleHref3Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(2);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref3()
	{
		return (HREF)getChild(CHILD_HREF3);
	}


	/**
	 *
	 *
	 */
	public void handleHref4Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(3);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref4()
	{
		return (HREF)getChild(CHILD_HREF4);
	}


	/**
	 *
	 *
	 */
	public void handleHref5Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(4);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref5()
	{
		return (HREF)getChild(CHILD_HREF5);
	}


	/**
	 *
	 *
	 */
	public void handleHref6Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(5);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref6()
	{
		return (HREF)getChild(CHILD_HREF6);
	}


	/**
	 *
	 *
	 */
	public void handleHref7Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(6);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref7()
	{
		return (HREF)getChild(CHILD_HREF7);
	}


	/**
	 *
	 *
	 */
	public void handleHref8Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(7);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref8()
	{
		return (HREF)getChild(CHILD_HREF8);
	}


	/**
	 *
	 *
	 */
	public void handleHref9Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(8);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref9()
	{
		return (HREF)getChild(CHILD_HREF9);
	}


	/**
	 *
	 *
	 */
	public void handleHref10Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(9);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref10()
	{
		return (HREF)getChild(CHILD_HREF10);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCompanyName()
	{
		return (StaticTextField)getChild(CHILD_STCOMPANYNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTodayDate()
	{
		return (StaticTextField)getChild(CHILD_STTODAYDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUserNameTitle()
	{
		return (StaticTextField)getChild(CHILD_STUSERNAMETITLE);
	}


	/**
	 *
	 *
	 */
	public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayWorkQueue();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtWorkQueueLink()
	{
		return (Button)getChild(CHILD_BTWORKQUEUELINK);
	}


	/**
	 *
	 *
	 */
	public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleChangePassword();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getChangePasswordHref()
	{
		return (HREF)getChild(CHILD_CHANGEPASSWORDHREF);
	}

  //--Release2.1--start//
  //// Two new methods providing the business logic for the new link to toggle language.
  //// When touched this link should reload the page in opposite language (french versus english)
  //// and set this new session value.
	/**
	 *
	 *
	 */
	public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleToggleLanguage();

		handler.postHandlerProtocol();
	}

	/**
	 *
	 *
	 */
	public HREF getToggleLanguageHref()
	{
		return (HREF)getChild(CHILD_TOGGLELANGUAGEHREF);
	}

  //--Release2.1--end//
	/**
	 *
	 *
	 */
	public void handleBtToolHistoryRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayDealHistory();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtToolHistory()
	{
		return (Button)getChild(CHILD_BTTOOLHISTORY);
	}


	/**
	 *
	 *
	 */
	public void handleBtTooNotesRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayDealNotes(true);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtTooNotes()
	{
		return (Button)getChild(CHILD_BTTOONOTES);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolSearchRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDealSearch();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtToolSearch()
	{
		return (Button)getChild(CHILD_BTTOOLSEARCH);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolLogRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleSignOff();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtToolLog()
	{
		return (Button)getChild(CHILD_BTTOOLLOG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStErrorFlag()
	{
		return (StaticTextField)getChild(CHILD_STERRORFLAG);
	}


	/**
	 *
	 *
	 */
	public HiddenField getDetectAlertTasks()
	{
		return (HiddenField)getChild(CHILD_DETECTALERTTASKS);
	}


	/**
	 *
	 *
	 */
	public void handleBtPrevTaskPageRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handlePrevTaskPage();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtPrevTaskPage()
	{
		return (Button)getChild(CHILD_BTPREVTASKPAGE);
	}


	/**
	 *
	 *
	 */
	public String endBtPrevTaskPageDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btPrevTaskPage_onBeforeHtmlOutputEvent method
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generatePrevTaskPage();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}

	/**
	 *
	 *
	 */
	public StaticTextField getStPrevTaskPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPREVTASKPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public String endStPrevTaskPageLabelDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stPrevTaskPageLabel_onBeforeHtmlOutputEvent method
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generatePrevTaskPage();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public void handleBtNextTaskPageRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleNextTaskPage();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtNextTaskPage()
	{
		return (Button)getChild(CHILD_BTNEXTTASKPAGE);
	}


	/**
	 *
	 *
	 */
	public String endBtNextTaskPageDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btNextTaskPage_onBeforeHtmlOutputEvent method
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generateNextTaskPage();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public StaticTextField getStNextTaskPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STNEXTTASKPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public String endStNextTaskPageLabelDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stNextTaskPageLabel_onBeforeHtmlOutputEvent method
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generateNextTaskPage();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTaskName()
	{
		return (StaticTextField)getChild(CHILD_STTASKNAME);
	}


	/**
	 *
	 *
	 */
	public String endStTaskNameDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stTaskName_onBeforeHtmlOutputEvent method
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generateTaskName();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public HiddenField getHdPropertyId()
	{
		return (HiddenField)getChild(CHILD_HDPROPERTYID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdPropertyCopyId()
	{
		return (HiddenField)getChild(CHILD_HDPROPERTYCOPYID);
	}


	/**
	 *
	 *
	 */
	public TextField getTxPropertyStreetNumber()
	{
		return (TextField)getChild(CHILD_TXPROPERTYSTREETNUMBER);
	}


	/**
	 *
	 *
	 */
	public TextField getTxPropertyStreetName()
	{
		return (TextField)getChild(CHILD_TXPROPERTYSTREETNAME);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPropertyStreetType()
	{
		return (ComboBox)getChild(CHILD_CBPROPERTYSTREETTYPE);
	}

  //--Release2.1--//
	/**
	 *  Since the population of comboboxes on the pages should be done from BXResource
   *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
   *  element if place or it could be done directly from the BXResource. Each approach
   *  has its pros and cons. The most important is: for English and French versions
   *  could be different default values. It forces to use the second approach.
   *
   *  It this case to escape annoying code clone and follow the object oriented
   *  design the following abstact base class should encapsulate the combobox
   *  OptionList population. Each ComboBoxOptionList inner class should extend
   *  this base class and implement the BXResources table name.
   *
	 *
	 */
	abstract static class BaseComboBoxOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		BaseComboBoxOptionList()
		{

		}

		protected final void populate(RequestContext rc, int langId, String tablename)
		{
          try
          {
              String defaultInstanceStateName =
                  rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
              SessionStateModelImpl theSessionState =
                  (SessionStateModelImpl)rc.getModelManager().getModel(
                  SessionStateModel.class,
                  defaultInstanceStateName,
                  true);
            //Get IDs and Labels from BXResources
            Collection c 
                = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                       tablename, langId);
            Iterator l = c.iterator();
            String[] theVal = new String[2];
            while(l.hasNext())
            {
              theVal = (String[])(l.next());
              add(theVal[1], theVal[0]);
            }
    			}
    			catch (Exception ex) {
    				ex.printStackTrace();
    			}
        }
      }

	////static class CbPropertyStreetTypeOptionList extends OptionList
  class CbPropertyStreetTypeOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbPropertyStreetTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "STREETTYPE");
      }
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPropertyStreetDirection()
	{
		return (ComboBox)getChild(CHILD_CBPROPERTYSTREETDIRECTION);
	}

	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbPropertyStreetDirectionOptionList extends OptionList
  class CbPropertyStreetDirectionOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbPropertyStreetDirectionOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "STREETDIRECTION");
      }
	}

  //MI addition
  class CbTenureTypeOptionList extends BaseComboBoxOptionList {

		CbTenureTypeOptionList() {
		}

		public void populate(RequestContext rc) {
			// Get the Language ID from the SessionStateModel
			String defaultInstanceStateName = rc.getModelManager()
					.getDefaultModelInstanceName(SessionStateModel.class);
			SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
					.getModelManager().getModel(SessionStateModel.class,
							defaultInstanceStateName, true);

			int languageId = theSessionState.getLanguageId();

			super.populate(rc, languageId, "TENURETYPE");
		}
	}
  
	/**
	 * 
	 * 
	 */
	public TextField getTxPropertyUnitNumber()
	{
		return (TextField)getChild(CHILD_TXPROPERTYUNITNUMBER);
	}


	/**
	 * 
	 * 
	 */
	public TextField getTxPropertyAddressLine2()
	{
		return (TextField)getChild(CHILD_TXPROPERTYADDRESSLINE2);
	}


	/**
	 *
	 *
	 */
	public TextField getTxPropertyCity()
	{
		return (TextField)getChild(CHILD_TXPROPERTYCITY);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPropertyProvince()
	{
		return (ComboBox)getChild(CHILD_CBPROPERTYPROVINCE);
	}

	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbPropertyProvinceOptionList extends OptionList
  class CbPropertyProvinceOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbPropertyProvinceOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "PROVINCE");
      }
	}

	/**
	 *
	 *
	 */
	public TextField getTxPropertyPostalFSA()
	{
		return (TextField)getChild(CHILD_TXPROPERTYPOSTALFSA);
	}


	/**
	 *
	 *
	 */
	public TextField getTxPropertyPostalLDU()
	{
		return (TextField)getChild(CHILD_TXPROPERTYPOSTALLDU);
	}


	/**
	 *
	 *
	 */
	public TextField getTxLegalDesc1()
	{
		return (TextField)getChild(CHILD_TXLEGALDESC1);
	}


	/**
	 *
	 *
	 */
	public TextField getTxLegalDesc2()
	{
		return (TextField)getChild(CHILD_TXLEGALDESC2);
	}


	/**
	 *
	 *
	 */
	public TextField getTxLegalDesc3()
	{
		return (TextField)getChild(CHILD_TXLEGALDESC3);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbDwellingType()
	{
		return (ComboBox)getChild(CHILD_CBDWELLINGTYPE);
	}

	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbDwellingTypeOptionList extends OptionList
  class CbDwellingTypeOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbDwellingTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "DWELLINGTYPE");
      }
	}

	/**
	 *
	 *
	 */
	public ComboBox getCbDwellingStyle()
	{
		return (ComboBox)getChild(CHILD_CBDWELLINGSTYLE);
	}

	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbDwellingStyleOptionList extends OptionList
  class CbDwellingStyleOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbDwellingStyleOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "DWELLINGSTYLE");
      }
	}

	/**
	 *
	 *
	 */
	public ComboBox getCbNewConstruction()
	{
		return (ComboBox)getChild(CHILD_CBNEWCONSTRUCTION);
	}

	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbNewConstructionOptionList extends OptionList
  class CbNewConstructionOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbNewConstructionOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "NEWCONSTRUCTION");
      }
	}


	/**
	 *
	 *
	 */
	public TextField getTxStructureAge()
	{
		return (TextField)getChild(CHILD_TXSTRUCTUREAGE);
	}


	/**
	 *
	 *
	 */
	public TextField getTxBuilderName()
	{
		return (TextField)getChild(CHILD_TXBUILDERNAME);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbGarageSize()
	{
		return (ComboBox)getChild(CHILD_CBGARAGESIZE);
	}

	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbGarageSizeOptionList extends OptionList
  class CbGarageSizeOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbGarageSizeOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "GARAGESIZE");
      }
	}

	/**
	 *
	 *
	 */
	public ComboBox getCbGarageType()
	{
		return (ComboBox)getChild(CHILD_CBGARAGETYPE);
	}

	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbGarageTypeOptionList extends OptionList
  class CbGarageTypeOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbGarageTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "GARAGETYPE");
      }
	}

	/**
	 *
	 *
	 */
	public TextField getTxNoOfUnits()
	{
		return (TextField)getChild(CHILD_TXNOOFUNITS);
	}


	/**
	 *
	 *
	 */
	public TextField getTxNoOfBedRooms()
	{
		return (TextField)getChild(CHILD_TXNOOFBEDROOMS);
	}


	/**
	 *
	 *
	 */
	public TextField getTxLivingSpace()
	{
		return (TextField)getChild(CHILD_TXLIVINGSPACE);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbUnits()
	{
		return (ComboBox)getChild(CHILD_CBUNITS);
	}

	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbUnitsOptionList extends OptionList
  class CbUnitsOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbUnitsOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "LIVINGSPACEUNITOFMEASURE");
      }
	}

	/**
	 *
	 *
	 */
	public TextField getTxLotSize()
	{
		return (TextField)getChild(CHILD_TXLOTSIZE);
	}


	/**
	 *
	 *
	 */
	public TextField getTxLotSize2()
	{
		return (TextField)getChild(CHILD_TXLOTSIZE2);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbLotSizeUnits()
	{
		return (ComboBox)getChild(CHILD_CBLOTSIZEUNITS);
	}

	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbLotSizeUnitsOptionList extends OptionList
  class CbLotSizeUnitsOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbLotSizeUnitsOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "LOTSIZEUNITOFMEASURE");
      }
	}

	/**
	 *
	 *
	 */
	public ComboBox getCbHeatType()
	{
		return (ComboBox)getChild(CHILD_CBHEATTYPE);
	}

	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbHeatTypeOptionList extends OptionList
  class CbHeatTypeOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbHeatTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "HEATTYPE");
      }
	}

	/**
	 *
	 *
	 */
	public ComboBox getCbUFFIInsulation()
	{
		return (ComboBox)getChild(CHILD_CBUFFIINSULATION);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbWaterType()
	{
		return (ComboBox)getChild(CHILD_CBWATERTYPE);
	}

	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbWaterTypeOptionList extends OptionList
  class CbWaterTypeOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbWaterTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "WATERTYPE");
      }
	}

	/**
	 *
	 *
	 */
	public ComboBox getCbSewageType()
	{
		return (ComboBox)getChild(CHILD_CBSEWAGETYPE);
	}

	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbSewageTypeOptionList extends OptionList
  class CbSewageTypeOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbSewageTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "SEWAGETYPE");
      }
	}

	/**
	 *
	 *
	 */
	public ComboBox getCbPropertyUsageType()
	{
		return (ComboBox)getChild(CHILD_CBPROPERTYUSAGETYPE);
	}

	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbPropertyUsageTypeOptionList extends OptionList
  class CbPropertyUsageTypeOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbPropertyUsageTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "PROPERTYUSAGE");
      }
	}

	/**
	 *
	 *
	 */
	public ComboBox getCbOccupancy()
	{
		return (ComboBox)getChild(CHILD_CBOCCUPANCY);
	}

	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbOccupancyOptionList extends OptionList
  class CbOccupancyOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbOccupancyOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "OCCUPANCYTYPE");
      }
	}

	/**
	 *
	 *
	 */
	public ComboBox getCbPropertType()
	{
		return (ComboBox)getChild(CHILD_CBPROPERTTYPE);
	}

//	***** Change by NBC Impl. Team - Version 1.2 - Start *****//
	
	/**
	 *
	 *
	 */
	public ComboBox getCbRequestAppraisal()
	{
		return (ComboBox)getChild(CHILD_CBREQUESTAPPRAISAL);
	}
//	***** Change by NBC Impl. Team - Version 1.2 - End *****//
	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbPropertTypeOptionList extends OptionList
  class CbPropertTypeOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbPropertTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "PROPERTYTYPE");
      }
	}

//***** Change by NBC Impl. Team - Version 1.2 - Start *****//
  
	//static class CbRequestAppraisalOptionList extends OptionList
  class CbRequestAppraisalOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
	  CbRequestAppraisalOptionList()
		{

		}

		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "REQUESTAPPRAISAL");
      }
	}
//***** Change by NBC Impl. Team - Version 1.2 - End *****//
	/**
	 *
	 *
	 */
	public ComboBox getCbPropertyLocation()
	{
		return (ComboBox)getChild(CHILD_CBPROPERTYLOCATION);
	}

	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbPropertyLocationOptionList extends OptionList
  class CbPropertyLocationOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbPropertyLocationOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "PROPERTYLOCATION");
      }
	}

	/**
	 *
	 *
	 */
	public ComboBox getCbZoning()
	{
		return (ComboBox)getChild(CHILD_CBZONING);
	}

	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbZoningOptionList extends OptionList
  class CbZoningOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbZoningOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "PROPERTYZONING");
      }
	}

	/**
	 *
	 *
	 */
	public TextField getTxPurchasePrice()
	{
		return (TextField)getChild(CHILD_TXPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public TextField getTxLandValue()
	{
		return (TextField)getChild(CHILD_TXLANDVALUE);
	}


	/**
	 *
	 *
	 */
	public TextField getTxEstimatedValue()
	{
		return (TextField)getChild(CHILD_TXESTIMATEDVALUE);
	}


	/**
	 *
	 *
	 */
	public RadioButtonGroup getRbMLSListing()
	{
		return (RadioButtonGroup)getChild(CHILD_RBMLSLISTING);
	}

	//MI additions
	public RadioButtonGroup getRbSubdivisionDiscount()
	{
		return (RadioButtonGroup)getChild(CHILD_RBSUBDIVISIONDISCOUNT);
	}

	public RadioButtonGroup getRbUFFIInsulation()
	{
		return (RadioButtonGroup)getChild(CHILD_RBUFFIINSULATION);
	}
	
	public ComboBox getCbTenureType()
	{
		return (ComboBox)getChild(CHILD_CBTENURETYPE);
	}
	
	public TextField getTxOnReserveTrustAgreementNumber()
    {
        return (TextField)getChild(CHILD_TXONRESERVETRUSTAGREEMENTNUMBER);
    }
	
	public RadioButtonGroup getRbMIEnergyEfficiency()
    {
        return (RadioButtonGroup)getChild(CHILD_RBMIENERGYEFFICIENCY);
    }
	
	public HiddenField getHdMortgageInsurerId()
    {
        return (HiddenField)getChild(CHILD_HDMORTGAGEINSURERID);
    }
	//End MI additions
	
	/**
	 *
	 *
	 */
	public StaticTextField getStLTV()
	{
		return (StaticTextField)getChild(CHILD_STLTV);
	}


	/**
	 *
	 *
	 */
	public pgPropertyEntryRepeatedPropertyExpensesTiledView getRepeatedPropertyExpenses()
	{
		return (pgPropertyEntryRepeatedPropertyExpensesTiledView)getChild(CHILD_REPEATEDPROPERTYEXPENSES);
	}


	/**
	 *
	 *
	 */
	public void handleBtAddExpenseRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleAddPropertyExpense();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtAddExpense()
	{
		return (Button)getChild(CHILD_BTADDEXPENSE);
	}


	/**
	 *
	 *
	 */
	public String endBtAddExpenseDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btAddExpense_onBeforeHtmlOutputEvent method
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.displayEditButton();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public HiddenField getSessionUserId()
	{
		return (HiddenField)getChild(CHILD_SESSIONUSERID);
	}


	/**
	 *
	 *
	 */
	public void handleBtPropertySubmitRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDEPSubmit();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtPropertySubmit()
	{
		return (Button)getChild(CHILD_BTPROPERTYSUBMIT);
	}


	/**
	 *
	 *
	 */
	public String endBtPropertySubmitDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btPropertySubmit_onBeforeHtmlOutputEvent method
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.displaySubmitButton();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public void handleBtPropertyCancelRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		// Changed to not saveing data (For better performance)-- By Billy 02Mat2002
		handler.preHandlerProtocol(this, true);

		// Changed to No confirm message (For better performance)-- By Billy 02Mat2002
		handler.handleCancelStandard(false);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtPropertyCancel()
	{
		return (Button)getChild(CHILD_BTPROPERTYCANCEL);
	}


	/**
	 *
	 *
	 */
	public String endBtPropertyCancelDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btPropertyCancel_onBeforeHtmlOutputEvent method
		////PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.displayCancelButton(1);

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public void handleBtPropertyCancelCurrentChangesRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		// Changed to not saveing data (For better performance)-- By Billy 02Mat2002
		handler.preHandlerProtocol(this, true);

		// Changed to No confirm message (For better performance)-- By Billy 02Mat2002
		handler.handleCancelStandard(false);

		handler.postHandlerProtocol();

		// The following code block was migrated from the btPropertyCancelCurrentChanges_onWebEvent method

	}


	/**
	 *
	 *
	 */
	public Button getBtPropertyCancelCurrentChanges()
	{
		return (Button)getChild(CHILD_BTPROPERTYCANCELCURRENTCHANGES);
	}


	/**
	 *
	 *
	 */
	public String endBtPropertyCancelCurrentChangesDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btPropertyCancelCurrentChanges_onBeforeHtmlOutputEvent method
		////PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.displayCancelButton(2);

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STPMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STPMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasOk()
	{
		return (StaticTextField)getChild(CHILD_STPMHASOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STPMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmOnOk()
	{
		return (StaticTextField)getChild(CHILD_STPMONOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STAMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STAMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmDialogMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMDIALOGMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmButtonsHtml()
	{
		return (StaticTextField)getChild(CHILD_STAMBUTTONSHTML);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTargetPE()
	{
		return (StaticTextField)getChild(CHILD_STTARGETPE);
	}


	/**
	 *
	 *
	 */
	public String endStTargetPEDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stTargetPE_onBeforeHtmlOutputEvent method
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generateLoadTarget(Mc.TARGET_PRO_PROPERTY_EXPENSE);

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}

	/**
	 *
	 *
	 */
	public TextField getTxTotalPropertyExp()
	{
		return (TextField)getChild(CHILD_TXTOTALPROPERTYEXP);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStVALSData()
	{
		return (StaticTextField)getChild(CHILD_STVALSDATA);
	}


	/**
	 *
	 *
	 */
	public void handleBtOKRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleCancelStandard();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtOK()
	{
		return (Button)getChild(CHILD_BTOK);
	}


	/**
	 *
	 *
	 */
	public String endBtOKDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btOK_onBeforeHtmlOutputEvent method
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.displayOKButton();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}

	/**
	 *
	 *
	 */
	public StaticTextField getStViewOnlyTag()
	{
		return (StaticTextField)getChild(CHILD_STVIEWONLYTAG);
	}


	/**
	 *
	 *
	 */
	public String endStViewOnlyTagDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stViewOnlyTag_onBeforeHtmlOutputEvent method
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		String rc = handler.displayViewOnlyTag();

		handler.pageSaveState();

    return rc;

  }



	/**
	 *
	 *
	 */
	public doPropertyEntryPropertySelectModel getdoPropertyEntryPropertySelectModel()
	{
		if (doPropertyEntryPropertySelect == null)
			doPropertyEntryPropertySelect = (doPropertyEntryPropertySelectModel) getModel(doPropertyEntryPropertySelectModel.class);
		return doPropertyEntryPropertySelect;
	}


	/**
	 *
	 *
	 */
	public void setdoPropertyEntryPropertySelectModel(doPropertyEntryPropertySelectModel model)
	{
			doPropertyEntryPropertySelect = model;
	}


  //// New hidden button for the ActiveMessage 'fake' submit button.
  public Button getBtActMsg()
	{
		return (Button)getChild(CHILD_BTACTMSG);
	}

 //--Release2.1--//
 //// This method overrides the getDefaultURL() framework JATO method and it is located
 //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
 //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
 //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
 //// The full method is still in PHC base class. It should care the
 //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
 public String getDisplayURL()
 {
       PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();
       handler.pageGetState(this);

       ////logger = SysLog.getSysLogger("PEVB");

       String url=getDefaultDisplayURL();
       ////logger.debug("PEVB@getDisplayURL::DefaultURL: " + url);

       int languageId = handler.theSessionState.getLanguageId();
       ////logger.debug("PEVB@getDisplayURL::LanguageFromUSOSession: " + languageId);

       //// Call the language specific URL (business delegation done in the BXResource).
       if(url != null && !url.trim().equals("") && !url.trim().equals("/")) {
          url = BXResources.getBXUrl(url, languageId);
       }
       else {
          url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
       }

      return url;
  }

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////



	//]]SPIDER_EVENT<btOK_onWebEvent>

	public void handleActMessageOK(String[] args)
	{
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this, true);

		handler.handleActMessageOK(args);

		handler.postHandlerProtocol();

	}



	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String PAGE_NAME="pgPropertyEntry";
  //// It is a variable now (see explanation in the getDisplayURL() method above.
	////public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgPropertyEntry.jsp";
	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_TBDEALID="tbDealId";
	public static final String CHILD_TBDEALID_RESET_VALUE="";
	public static final String CHILD_CBPAGENAMES="cbPageNames";
	public static final String CHILD_CBPAGENAMES_RESET_VALUE="";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	private CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  ////private CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  ////Added the Variable for NonSelected Label
  private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";

  public static final String CHILD_BTPROCEED="btProceed";
	public static final String CHILD_BTPROCEED_RESET_VALUE="Proceed";
	public static final String CHILD_HREF1="Href1";
	public static final String CHILD_HREF1_RESET_VALUE="";
	public static final String CHILD_HREF2="Href2";
	public static final String CHILD_HREF2_RESET_VALUE="";
	public static final String CHILD_HREF3="Href3";
	public static final String CHILD_HREF3_RESET_VALUE="";
	public static final String CHILD_HREF4="Href4";
	public static final String CHILD_HREF4_RESET_VALUE="";
	public static final String CHILD_HREF5="Href5";
	public static final String CHILD_HREF5_RESET_VALUE="";
	public static final String CHILD_HREF6="Href6";
	public static final String CHILD_HREF6_RESET_VALUE="";
	public static final String CHILD_HREF7="Href7";
	public static final String CHILD_HREF7_RESET_VALUE="";
	public static final String CHILD_HREF8="Href8";
	public static final String CHILD_HREF8_RESET_VALUE="";
	public static final String CHILD_HREF9="Href9";
	public static final String CHILD_HREF9_RESET_VALUE="";
	public static final String CHILD_HREF10="Href10";
	public static final String CHILD_HREF10_RESET_VALUE="";
	public static final String CHILD_STPAGELABEL="stPageLabel";
	public static final String CHILD_STPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STCOMPANYNAME="stCompanyName";
	public static final String CHILD_STCOMPANYNAME_RESET_VALUE="";
	public static final String CHILD_STTODAYDATE="stTodayDate";
	public static final String CHILD_STTODAYDATE_RESET_VALUE="";
	public static final String CHILD_STUSERNAMETITLE="stUserNameTitle";
	public static final String CHILD_STUSERNAMETITLE_RESET_VALUE="";
	public static final String CHILD_BTWORKQUEUELINK="btWorkQueueLink";
	public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE=" ";
	public static final String CHILD_CHANGEPASSWORDHREF="changePasswordHref";
	public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE="";
	public static final String CHILD_BTTOOLHISTORY="btToolHistory";
	public static final String CHILD_BTTOOLHISTORY_RESET_VALUE=" ";
	public static final String CHILD_BTTOONOTES="btTooNotes";
	public static final String CHILD_BTTOONOTES_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLSEARCH="btToolSearch";
	public static final String CHILD_BTTOOLSEARCH_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLLOG="btToolLog";
	public static final String CHILD_BTTOOLLOG_RESET_VALUE=" ";
	public static final String CHILD_STERRORFLAG="stErrorFlag";
	public static final String CHILD_STERRORFLAG_RESET_VALUE="N";
	public static final String CHILD_DETECTALERTTASKS="detectAlertTasks";
	public static final String CHILD_DETECTALERTTASKS_RESET_VALUE="";
	public static final String CHILD_BTPREVTASKPAGE="btPrevTaskPage";
	public static final String CHILD_BTPREVTASKPAGE_RESET_VALUE=" ";
	public static final String CHILD_STPREVTASKPAGELABEL="stPrevTaskPageLabel";
	public static final String CHILD_STPREVTASKPAGELABEL_RESET_VALUE="";
	public static final String CHILD_BTNEXTTASKPAGE="btNextTaskPage";
	public static final String CHILD_BTNEXTTASKPAGE_RESET_VALUE=" ";
	public static final String CHILD_STNEXTTASKPAGELABEL="stNextTaskPageLabel";
	public static final String CHILD_STNEXTTASKPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STTASKNAME="stTaskName";
	public static final String CHILD_STTASKNAME_RESET_VALUE="";
	public static final String CHILD_HDPROPERTYID="hdPropertyId";
	public static final String CHILD_HDPROPERTYID_RESET_VALUE="";
	public static final String CHILD_HDPROPERTYCOPYID="hdPropertyCopyId";
	public static final String CHILD_HDPROPERTYCOPYID_RESET_VALUE="";
	public static final String CHILD_TXPROPERTYSTREETNUMBER="txPropertyStreetNumber";
	public static final String CHILD_TXPROPERTYSTREETNUMBER_RESET_VALUE="";
	public static final String CHILD_TXPROPERTYSTREETNAME="txPropertyStreetName";
	public static final String CHILD_TXPROPERTYSTREETNAME_RESET_VALUE="";
	public static final String CHILD_CBPROPERTYSTREETTYPE="cbPropertyStreetType";
	public static final String CHILD_CBPROPERTYSTREETTYPE_RESET_VALUE="0";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	////private static CbPropertyStreetTypeOptionList cbPropertyStreetTypeOptions=new CbPropertyStreetTypeOptionList();
	private CbPropertyStreetTypeOptionList cbPropertyStreetTypeOptions=new CbPropertyStreetTypeOptionList();

	public static final String CHILD_CBPROPERTYSTREETDIRECTION="cbPropertyStreetDirection";
	public static final String CHILD_CBPROPERTYSTREETDIRECTION_RESET_VALUE="";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	////private static CbPropertyStreetDirectionOptionList cbPropertyStreetDirectionOptions=new CbPropertyStreetDirectionOptionList();
	private CbPropertyStreetDirectionOptionList cbPropertyStreetDirectionOptions=new CbPropertyStreetDirectionOptionList();

	public static final String CHILD_TXPROPERTYUNITNUMBER="txPropertyUnitNumber";
	public static final String CHILD_TXPROPERTYUNITNUMBER_RESET_VALUE="";
	public static final String CHILD_TXPROPERTYADDRESSLINE2="txPropertyAddressLine2";
	public static final String CHILD_TXPROPERTYADDRESSLINE2_RESET_VALUE="";
	public static final String CHILD_TXPROPERTYCITY="txPropertyCity";
	public static final String CHILD_TXPROPERTYCITY_RESET_VALUE="";
	public static final String CHILD_CBPROPERTYPROVINCE="cbPropertyProvince";
	public static final String CHILD_CBPROPERTYPROVINCE_RESET_VALUE="";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	////private static CbPropertyProvinceOptionList cbPropertyProvinceOptions=new CbPropertyProvinceOptionList();
	private CbPropertyProvinceOptionList cbPropertyProvinceOptions=new CbPropertyProvinceOptionList();

	public static final String CHILD_TXPROPERTYPOSTALFSA="txPropertyPostalFSA";
	public static final String CHILD_TXPROPERTYPOSTALFSA_RESET_VALUE="";
	public static final String CHILD_TXPROPERTYPOSTALLDU="txPropertyPostalLDU";
	public static final String CHILD_TXPROPERTYPOSTALLDU_RESET_VALUE="";
	public static final String CHILD_TXLEGALDESC1="txLegalDesc1";
	public static final String CHILD_TXLEGALDESC1_RESET_VALUE="";
	public static final String CHILD_TXLEGALDESC2="txLegalDesc2";
	public static final String CHILD_TXLEGALDESC2_RESET_VALUE="";
	public static final String CHILD_TXLEGALDESC3="txLegalDesc3";
	public static final String CHILD_TXLEGALDESC3_RESET_VALUE="";
	public static final String CHILD_CBDWELLINGTYPE="cbDwellingType";
	public static final String CHILD_CBDWELLINGTYPE_RESET_VALUE="";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	////private static CbDwellingTypeOptionList cbDwellingTypeOptions=new CbDwellingTypeOptionList();
	private CbDwellingTypeOptionList cbDwellingTypeOptions=new CbDwellingTypeOptionList();

	public static final String CHILD_CBDWELLINGSTYLE="cbDwellingStyle";
	public static final String CHILD_CBDWELLINGSTYLE_RESET_VALUE="";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	////private static CbDwellingStyleOptionList cbDwellingStyleOptions=new CbDwellingStyleOptionList();
	private CbDwellingStyleOptionList cbDwellingStyleOptions=new CbDwellingStyleOptionList();

	public static final String CHILD_CBNEWCONSTRUCTION="cbNewConstruction";
	public static final String CHILD_CBNEWCONSTRUCTION_RESET_VALUE="";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	////private static CbNewConstructionOptionList cbNewConstructionOptions=new CbNewConstructionOptionList();
	private CbNewConstructionOptionList cbNewConstructionOptions=new CbNewConstructionOptionList();

	public static final String CHILD_TXSTRUCTUREAGE="txStructureAge";
	public static final String CHILD_TXSTRUCTUREAGE_RESET_VALUE="";
	public static final String CHILD_TXBUILDERNAME="txBuilderName";
	public static final String CHILD_TXBUILDERNAME_RESET_VALUE="";
	public static final String CHILD_CBGARAGESIZE="cbGarageSize";
	public static final String CHILD_CBGARAGESIZE_RESET_VALUE="";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	////private static CbGarageSizeOptionList cbGarageSizeOptions=new CbGarageSizeOptionList();
	private CbGarageSizeOptionList cbGarageSizeOptions=new CbGarageSizeOptionList();

	public static final String CHILD_CBGARAGETYPE="cbGarageType";
	public static final String CHILD_CBGARAGETYPE_RESET_VALUE="";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	////private static CbGarageTypeOptionList cbGarageTypeOptions=new CbGarageTypeOptionList();
	private CbGarageTypeOptionList cbGarageTypeOptions=new CbGarageTypeOptionList();

	public static final String CHILD_TXNOOFUNITS="txNoOfUnits";
	public static final String CHILD_TXNOOFUNITS_RESET_VALUE="";
	public static final String CHILD_TXNOOFBEDROOMS="txNoOfBedRooms";
	public static final String CHILD_TXNOOFBEDROOMS_RESET_VALUE="";
	public static final String CHILD_TXLIVINGSPACE="txLivingSpace";
	public static final String CHILD_TXLIVINGSPACE_RESET_VALUE="";
	public static final String CHILD_CBUNITS="cbUnits";
	public static final String CHILD_CBUNITS_RESET_VALUE="";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	////private static CbUnitsOptionList cbUnitsOptions=new CbUnitsOptionList();
	private CbUnitsOptionList cbUnitsOptions=new CbUnitsOptionList();

	public static final String CHILD_TXLOTSIZE="txLotSize";
	public static final String CHILD_TXLOTSIZE_RESET_VALUE="";
	public static final String CHILD_TXLOTSIZE2="txLotSize2";
	public static final String CHILD_TXLOTSIZE2_RESET_VALUE="";
	public static final String CHILD_CBLOTSIZEUNITS="cbLotSizeUnits";
	public static final String CHILD_CBLOTSIZEUNITS_RESET_VALUE="";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	////private static CbLotSizeUnitsOptionList cbLotSizeUnitsOptions=new CbLotSizeUnitsOptionList();
	private CbLotSizeUnitsOptionList cbLotSizeUnitsOptions=new CbLotSizeUnitsOptionList();

	public static final String CHILD_CBHEATTYPE="cbHeatType";
	public static final String CHILD_CBHEATTYPE_RESET_VALUE="";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	////private static CbHeatTypeOptionList cbHeatTypeOptions=new CbHeatTypeOptionList();
	private CbHeatTypeOptionList cbHeatTypeOptions=new CbHeatTypeOptionList();

	public static final String CHILD_CBUFFIINSULATION="cbUFFIInsulation";
	public static final String CHILD_CBUFFIINSULATION_RESET_VALUE="N";

  //--Release2.1--//
  //// This OptionList population is done now in the BeginDisplay via the BXResources
	////private static OptionList cbUFFIInsulationOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
	private OptionList cbUFFIInsulationOptions=new OptionList();

	public static final String CHILD_CBWATERTYPE="cbWaterType";
	public static final String CHILD_CBWATERTYPE_RESET_VALUE="";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	////private static CbWaterTypeOptionList cbWaterTypeOptions=new CbWaterTypeOptionList();
	private CbWaterTypeOptionList cbWaterTypeOptions=new CbWaterTypeOptionList();

	public static final String CHILD_CBSEWAGETYPE="cbSewageType";
	public static final String CHILD_CBSEWAGETYPE_RESET_VALUE="";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	////private static CbSewageTypeOptionList cbSewageTypeOptions=new CbSewageTypeOptionList();
	private CbSewageTypeOptionList cbSewageTypeOptions=new CbSewageTypeOptionList();

	public static final String CHILD_CBPROPERTYUSAGETYPE="cbPropertyUsageType";
	public static final String CHILD_CBPROPERTYUSAGETYPE_RESET_VALUE="";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	////private static CbPropertyUsageTypeOptionList cbPropertyUsageTypeOptions=new CbPropertyUsageTypeOptionList();
	private CbPropertyUsageTypeOptionList cbPropertyUsageTypeOptions=new CbPropertyUsageTypeOptionList();

	public static final String CHILD_CBOCCUPANCY="cbOccupancy";
	public static final String CHILD_CBOCCUPANCY_RESET_VALUE="";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	////private static CbOccupancyOptionList cbOccupancyOptions=new CbOccupancyOptionList();
	private CbOccupancyOptionList cbOccupancyOptions=new CbOccupancyOptionList();

	public static final String CHILD_CBPROPERTTYPE="cbPropertType";
	public static final String CHILD_CBPROPERTTYPE_RESET_VALUE="";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	////private static CbPropertTypeOptionList cbPropertTypeOptions=new CbPropertTypeOptionList();
	private CbPropertTypeOptionList cbPropertTypeOptions=new CbPropertTypeOptionList();

	public static final String CHILD_CBPROPERTYLOCATION="cbPropertyLocation";
	public static final String CHILD_CBPROPERTYLOCATION_RESET_VALUE="";

//	***** Change by NBC Impl. Team - Version 1.2 - Start *****//
	private CbRequestAppraisalOptionList cbRequestAppraisalOptions=new CbRequestAppraisalOptionList();	
//	***** Change by NBC Impl. Team - Version 1.2 - End *****//

	public static final String CHILD_CBREQUESTAPPRAISAL="cbRequestAppraisal";
	public static final String CHILD_CBREQUESTAPPRAISAL_RESET_VALUE="";
	
	
  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	////private static CbPropertyLocationOptionList cbPropertyLocationOptions=new CbPropertyLocationOptionList();
	private CbPropertyLocationOptionList cbPropertyLocationOptions=new CbPropertyLocationOptionList();

	public static final String CHILD_CBZONING="cbZoning";
	public static final String CHILD_CBZONING_RESET_VALUE="";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	////private static CbZoningOptionList cbZoningOptions=new CbZoningOptionList();
	private CbZoningOptionList cbZoningOptions=new CbZoningOptionList();

	public static final String CHILD_TXPURCHASEPRICE="txPurchasePrice";
	public static final String CHILD_TXPURCHASEPRICE_RESET_VALUE="";
	public static final String CHILD_TXLANDVALUE="txLandValue";
	public static final String CHILD_TXLANDVALUE_RESET_VALUE="";
	public static final String CHILD_TXESTIMATEDVALUE="txEstimatedValue";
	public static final String CHILD_TXESTIMATEDVALUE_RESET_VALUE="";
	public static final String CHILD_RBMLSLISTING="rbMLSListing";
	public static final String CHILD_RBMLSLISTING_RESET_VALUE="Y";

  //--Release2.1--//
  //// This OptionList population is done now in the BeginDisplay via the BXResources
	////private static OptionList rbMLSListingOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
	private OptionList rbMLSListingOptions=new OptionList();

	public static final String CHILD_STLTV="stLTV";
	public static final String CHILD_STLTV_RESET_VALUE="";
	public static final String CHILD_REPEATEDPROPERTYEXPENSES="RepeatedPropertyExpenses";
	public static final String CHILD_BTADDEXPENSE="btAddExpense";
	public static final String CHILD_BTADDEXPENSE_RESET_VALUE=" ";
	public static final String CHILD_SESSIONUSERID="sessionUserId";
	public static final String CHILD_SESSIONUSERID_RESET_VALUE="";
	public static final String CHILD_BTPROPERTYSUBMIT="btPropertySubmit";
	public static final String CHILD_BTPROPERTYSUBMIT_RESET_VALUE=" ";
	public static final String CHILD_BTPROPERTYCANCEL="btPropertyCancel";
	public static final String CHILD_BTPROPERTYCANCEL_RESET_VALUE=" ";
	public static final String CHILD_BTPROPERTYCANCELCURRENTCHANGES="btPropertyCancelCurrentChanges";
	public static final String CHILD_BTPROPERTYCANCELCURRENTCHANGES_RESET_VALUE=" ";
	public static final String CHILD_STPMGENERATE="stPmGenerate";
	public static final String CHILD_STPMGENERATE_RESET_VALUE="";
	public static final String CHILD_STPMHASTITLE="stPmHasTitle";
	public static final String CHILD_STPMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STPMHASINFO="stPmHasInfo";
	public static final String CHILD_STPMHASINFO_RESET_VALUE="";
	public static final String CHILD_STPMHASTABLE="stPmHasTable";
	public static final String CHILD_STPMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STPMHASOK="stPmHasOk";
	public static final String CHILD_STPMHASOK_RESET_VALUE="";
	public static final String CHILD_STPMTITLE="stPmTitle";
	public static final String CHILD_STPMTITLE_RESET_VALUE="";
	public static final String CHILD_STPMINFOMSG="stPmInfoMsg";
	public static final String CHILD_STPMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STPMONOK="stPmOnOk";
	public static final String CHILD_STPMONOK_RESET_VALUE="";
	public static final String CHILD_STPMMSGS="stPmMsgs";
	public static final String CHILD_STPMMSGS_RESET_VALUE="";
	public static final String CHILD_STPMMSGTYPES="stPmMsgTypes";
	public static final String CHILD_STPMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMGENERATE="stAmGenerate";
	public static final String CHILD_STAMGENERATE_RESET_VALUE="";
	public static final String CHILD_STAMHASTITLE="stAmHasTitle";
	public static final String CHILD_STAMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STAMHASINFO="stAmHasInfo";
	public static final String CHILD_STAMHASINFO_RESET_VALUE="";
	public static final String CHILD_STAMHASTABLE="stAmHasTable";
	public static final String CHILD_STAMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STAMTITLE="stAmTitle";
	public static final String CHILD_STAMTITLE_RESET_VALUE="";
	public static final String CHILD_STAMINFOMSG="stAmInfoMsg";
	public static final String CHILD_STAMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STAMMSGS="stAmMsgs";
	public static final String CHILD_STAMMSGS_RESET_VALUE="";
	public static final String CHILD_STAMMSGTYPES="stAmMsgTypes";
	public static final String CHILD_STAMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMDIALOGMSG="stAmDialogMsg";
	public static final String CHILD_STAMDIALOGMSG_RESET_VALUE="";
	public static final String CHILD_STAMBUTTONSHTML="stAmButtonsHtml";
	public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE="";
	public static final String CHILD_STTARGETPE="stTargetPE";
	public static final String CHILD_STTARGETPE_RESET_VALUE="";
	public static final String CHILD_TXTOTALPROPERTYEXP="txTotalPropertyExp";
	public static final String CHILD_TXTOTALPROPERTYEXP_RESET_VALUE="";
	public static final String CHILD_STVALSDATA="stVALSData";
	public static final String CHILD_STVALSDATA_RESET_VALUE="";
	public static final String CHILD_BTOK="btOK";
	public static final String CHILD_BTOK_RESET_VALUE="OK";
	public static final String CHILD_STVIEWONLYTAG="stViewOnlyTag";
	public static final String CHILD_STVIEWONLYTAG_RESET_VALUE="";
  //// New hidden button implementing the 'fake' one from the ActiveMessage class.
  public static final String CHILD_BTACTMSG="btActMsg";
	public static final String CHILD_BTACTMSG_RESET_VALUE="ActMsg";
  //--Release2.1--//
  //// New link to toggle the language. When touched this link should reload the
  //// page in opposite language (french versus english) and set this new language
  //// session id throughout all modulus.
	public static final String CHILD_TOGGLELANGUAGEHREF="toggleLanguageHref";
	public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE="";
	
	// Begin MI Additions
	public static final String CHILD_RBSUBDIVISIONDISCOUNT="rbSubdivisionDiscount";
	public static final String CHILD_RBSUBDIVISIONDISCOUNT_RESET_VALUE="";
	private OptionList rbSubdivisionDiscountOptions=new OptionList();
	
	public static final String CHILD_RBUFFIINSULATION="rbUFFIInsulation";
	public static final String CHILD_RBUFFIINSULATION_RESET_VALUE="";
	private OptionList rbUFFIInsulationOptions=new OptionList();
	
	public static final String CHILD_CBTENURETYPE="cbTenureType";
	public static final String CHILD_CBTENURETYPE_RESET_VALUE="";
	private CbTenureTypeOptionList cbTenureTypeOptions=new CbTenureTypeOptionList();

	public static final String CHILD_TXONRESERVETRUSTAGREEMENTNUMBER = "txOnReserveTrustAgreementNumber";
    public static final String CHILD_TXONRESERVETRUSTAGREEMENTNUMBER_RESET_VALUE = "";
    
    public static final String CHILD_RBMIENERGYEFFICIENCY = "rbMIEnergyEfficiency";
    public static final String CHILD_RBMIENERGYEFFICIENCY_RESET_VALUE = "";
    private OptionList rbMIEnergyEfficiencyOptions = new OptionList();
    
    public static final String CHILD_HDMORTGAGEINSURERID = "hdMortgageInsurerId";
    public static final String CHILD_HDMORTGAGEINSURERID_RESET_VALUE = "";
//	 End MI Additions
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doPropertyEntryPropertySelectModel doPropertyEntryPropertySelect=null;
	private String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgPropertyEntry.jsp";


	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	private PropertyEntryHandler handler=new PropertyEntryHandler();
        public SysLogger logger;

}

