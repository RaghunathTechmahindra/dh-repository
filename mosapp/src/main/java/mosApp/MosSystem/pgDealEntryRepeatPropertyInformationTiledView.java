package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

import com.basis100.log.*;
import com.basis100.resources.*;
import com.basis100.picklist.*;
import MosSystem.*;

/**
 *
 *
 *
 */
public class pgDealEntryRepeatPropertyInformationTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealEntryRepeatPropertyInformationTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doDealEntryPropertySelectModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getHdPropertyId().setValue(CHILD_HDPROPERTYID_RESET_VALUE);
		getHdPropertyCopyId().setValue(CHILD_HDPROPERTYCOPYID_RESET_VALUE);
		getCbPrimaryProperty().setValue(CHILD_CBPRIMARYPROPERTY_RESET_VALUE);
		getStPropertyStreetNumber().setValue(CHILD_STPROPERTYSTREETNUMBER_RESET_VALUE);
		getStPropertyStreetName().setValue(CHILD_STPROPERTYSTREETNAME_RESET_VALUE);
		getStPropertyStreetType().setValue(CHILD_STPROPERTYSTREETTYPE_RESET_VALUE);
		getStPropertyStreetDirection().setValue(CHILD_STPROPERTYSTREETDIRECTION_RESET_VALUE);
		getStProvince().setValue(CHILD_STPROVINCE_RESET_VALUE);
		getStPurchasePricePropertyPart().setValue(CHILD_STPURCHASEPRICEPROPERTYPART_RESET_VALUE);
		getStLTV().setValue(CHILD_STLTV_RESET_VALUE);
		getBtDeleteProperty().setValue(CHILD_BTDELETEPROPERTY_RESET_VALUE);
		getBtPropertyDetails().setValue(CHILD_BTPROPERTYDETAILS_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_HDPROPERTYID,HiddenField.class);
		registerChild(CHILD_HDPROPERTYCOPYID,HiddenField.class);
		registerChild(CHILD_CBPRIMARYPROPERTY,ComboBox.class);
		registerChild(CHILD_STPROPERTYSTREETNUMBER,StaticTextField.class);
		registerChild(CHILD_STPROPERTYSTREETNAME,StaticTextField.class);
		registerChild(CHILD_STPROPERTYSTREETTYPE,StaticTextField.class);
		registerChild(CHILD_STPROPERTYSTREETDIRECTION,StaticTextField.class);
		registerChild(CHILD_STPROVINCE,StaticTextField.class);
		registerChild(CHILD_STPURCHASEPRICEPROPERTYPART,StaticTextField.class);
		registerChild(CHILD_STLTV,StaticTextField.class);
		registerChild(CHILD_BTDELETEPROPERTY,Button.class);
		registerChild(CHILD_BTPROPERTYDETAILS,Button.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDealEntryPropertySelectModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--start//
    DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

    // 1. Setup the Yes/No/Co-App Options
    String yesStr = BXResources.getGenericMsg("YES_LABEL", handler.getTheSessionState().getLanguageId());
    String noStr = BXResources.getGenericMsg("NO_LABEL", handler.getTheSessionState().getLanguageId());

    cbPrimaryPropertyOptions.setOptions(new String[]{noStr, yesStr},new String[]{"N", "Y"});
    //--Release2.1--end//

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
      validJSFromTiledView();
		}

		return movedToRow;

	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_HDPROPERTYID))
		{
			HiddenField child = new HiddenField(this,
				getdoDealEntryPropertySelectModel(),
				CHILD_HDPROPERTYID,
				doDealEntryPropertySelectModel.FIELD_DFPROPERTYID,
				CHILD_HDPROPERTYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDPROPERTYCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoDealEntryPropertySelectModel(),
				CHILD_HDPROPERTYCOPYID,
				doDealEntryPropertySelectModel.FIELD_DFCOPYID,
				CHILD_HDPROPERTYCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPRIMARYPROPERTY))
		{
			ComboBox child = new ComboBox( this,
				getdoDealEntryPropertySelectModel(),
				CHILD_CBPRIMARYPROPERTY,
				doDealEntryPropertySelectModel.FIELD_DFPRIMARYPROPERTYFLAG,
				CHILD_CBPRIMARYPROPERTY_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbPrimaryPropertyOptions);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYSTREETNUMBER))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealEntryPropertySelectModel(),
				CHILD_STPROPERTYSTREETNUMBER,
				doDealEntryPropertySelectModel.FIELD_DFSTREETNUMBER,
				CHILD_STPROPERTYSTREETNUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYSTREETNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealEntryPropertySelectModel(),
				CHILD_STPROPERTYSTREETNAME,
				doDealEntryPropertySelectModel.FIELD_DFSTREETNAME,
				CHILD_STPROPERTYSTREETNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYSTREETTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealEntryPropertySelectModel(),
				CHILD_STPROPERTYSTREETTYPE,
				doDealEntryPropertySelectModel.FIELD_DFSTREETTYPE,
				CHILD_STPROPERTYSTREETTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYSTREETDIRECTION))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealEntryPropertySelectModel(),
				CHILD_STPROPERTYSTREETDIRECTION,
				doDealEntryPropertySelectModel.FIELD_DFSTREETDIRECTION,
				CHILD_STPROPERTYSTREETDIRECTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROVINCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealEntryPropertySelectModel(),
				CHILD_STPROVINCE,
				doDealEntryPropertySelectModel.FIELD_DFPROVINCE,
				CHILD_STPROVINCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPURCHASEPRICEPROPERTYPART))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealEntryPropertySelectModel(),
				CHILD_STPURCHASEPRICEPROPERTYPART,
				doDealEntryPropertySelectModel.FIELD_DFPURCHASEPRICE,
				CHILD_STPURCHASEPRICEPROPERTYPART_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLTV))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealEntryPropertySelectModel(),
				CHILD_STLTV,
				doDealEntryPropertySelectModel.FIELD_DFLTV,
				CHILD_STLTV_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETEPROPERTY))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETEPROPERTY,
				CHILD_BTDELETEPROPERTY,
				CHILD_BTDELETEPROPERTY_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTPROPERTYDETAILS))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPROPERTYDETAILS,
				CHILD_BTPROPERTYDETAILS,
				CHILD_BTPROPERTYDETAILS_RESET_VALUE,
				null);
				return child;

		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdPropertyId()
	{
		return (HiddenField)getChild(CHILD_HDPROPERTYID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdPropertyCopyId()
	{
		return (HiddenField)getChild(CHILD_HDPROPERTYCOPYID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPrimaryProperty()
	{
		return (ComboBox)getChild(CHILD_CBPRIMARYPROPERTY);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyStreetNumber()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYSTREETNUMBER);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyStreetName()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYSTREETNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyStreetType()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYSTREETTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyStreetDirection()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYSTREETDIRECTION);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStProvince()
	{
		return (StaticTextField)getChild(CHILD_STPROVINCE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPurchasePricePropertyPart()
	{
		return (StaticTextField)getChild(CHILD_STPURCHASEPRICEPROPERTYPART);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLTV()
	{
		return (StaticTextField)getChild(CHILD_STLTV);
	}


	/**
	 *
	 *
	 */
	public void handleBtDeletePropertyRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

    //logger = SysLog.getSysLogger("PGDERPITV");

		DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol((ViewBean) this.getParent());
    //logger.debug("pgDealEntry@handleBtDeletePropertyRequest::Parent: " + (pgDealEntryViewBean) this.getParent());

    int tileIndx = ((TiledViewRequestInvocationEvent)(event)).getTileNumber();
    //logger.debug("pgDealEntry@handleBtDeletePropertyRequest::TileIndexNew: " + tileIndx);

		handler.handleDelete(false,
                        2,
                        tileIndx,
                        "RepeatPropertyInformation/hdPropertyId",
                        "propertyId",
                        "RepeatPropertyInformation/hdPropertyCopyId",
                        "Property");

    handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtDeleteProperty()
	{
		return (Button)getChild(CHILD_BTDELETEPROPERTY);
	}


	/**
	 *
	 *
	 */
	public String endBtDeletePropertyDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btDeleteProperty_onBeforeHtmlOutputEvent method

		DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public void handleBtPropertyDetailsRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

    //logger = SysLog.getSysLogger("PGDERPITV");

		DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol((ViewBean) this.getParent());
    //logger.debug("pgDealEntry@handleBtPropertyDetailsRequest::Parent: " + (pgDealEntryViewBean) this.getParent());

    int tileIndx = ((TiledViewRequestInvocationEvent)(event)).getTileNumber();
    //logger.debug("pgDealEntry@handleBtPropertyDetailsRequest::TileIndexNew: " + tileIndx);

		handler.handlePropertyView(tileIndx,
                              "RepeatPropertyInformation/hdPropertyId",
                              "RepeatPropertyInformation/hdPropertyCopyId");

    handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtPropertyDetails()
	{
		return (Button)getChild(CHILD_BTPROPERTYDETAILS);
	}


	/**
	 *
	 *
	 */
	public doDealEntryPropertySelectModel getdoDealEntryPropertySelectModel()
	{
		if (doDealEntryPropertySelect == null)
			doDealEntryPropertySelect = (doDealEntryPropertySelectModel) getModel(doDealEntryPropertySelectModel.class);
		return doDealEntryPropertySelect;
	}


	/**
	 *
	 *
	 */
	public void setdoDealEntryPropertySelectModel(doDealEntryPropertySelectModel model)
	{
			doDealEntryPropertySelect = model;
	}

    public void validJSFromTiledView()
    {
            logger = SysLog.getSysLogger("PGDERPITV");

            //// Currently it is called from the page. Should be moved to the
            //// TiledView utility classes to generalize the parent (ViewBean name/casting).
            //// ValidationPath is temporarily hardcoded as well. Should be replaced with
            //// the picklist mechanism along with the move to the utility class.

            pgDealEntryViewBean viewBean = (pgDealEntryViewBean) getParentViewBean();

            //logger.debug("PGDERPITV@validJSFromTiledView@ParentBean: " + viewBean);

            try
            {
 
                  String propertyValidPath = Sc.DVALID_DEAL_TILE_PROPERTY_PROPS_FILE_NAME;

                  //logger.debug("PGDERPITV@RatesPath: " + propertyValidPath);

                  JSValidationRules downValidObj = new JSValidationRules(propertyValidPath, logger, viewBean);
                  downValidObj.attachJSValidationRules();
            }
            catch( Exception e )
            {
                logger.warning("PGDERPITV@DataValidJSException: " + e);
                e.printStackTrace();
            }
    }



	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_HDPROPERTYID="hdPropertyId";
	public static final String CHILD_HDPROPERTYID_RESET_VALUE="";
	public static final String CHILD_HDPROPERTYCOPYID="hdPropertyCopyId";
	public static final String CHILD_HDPROPERTYCOPYID_RESET_VALUE="";
	public static final String CHILD_CBPRIMARYPROPERTY="cbPrimaryProperty";
	public static final String CHILD_CBPRIMARYPROPERTY_RESET_VALUE="N";

  //--Release2.1--//
  //// This OptionList population is done now in the BeginDisplay via the BXResources
	////private static OptionList cbPrimaryPropertyOptions=new OptionList(new String[]{"No", "Yes"},new String[]{"N", "Y"});
	private OptionList cbPrimaryPropertyOptions=new OptionList();

	public static final String CHILD_STPROPERTYSTREETNUMBER="stPropertyStreetNumber";
	public static final String CHILD_STPROPERTYSTREETNUMBER_RESET_VALUE="";
	public static final String CHILD_STPROPERTYSTREETNAME="stPropertyStreetName";
	public static final String CHILD_STPROPERTYSTREETNAME_RESET_VALUE="";
	public static final String CHILD_STPROPERTYSTREETTYPE="stPropertyStreetType";
	public static final String CHILD_STPROPERTYSTREETTYPE_RESET_VALUE="";
	public static final String CHILD_STPROPERTYSTREETDIRECTION="stPropertyStreetDirection";
	public static final String CHILD_STPROPERTYSTREETDIRECTION_RESET_VALUE="";
	public static final String CHILD_STPROVINCE="stProvince";
	public static final String CHILD_STPROVINCE_RESET_VALUE="";
	public static final String CHILD_STPURCHASEPRICEPROPERTYPART="stPurchasePricePropertyPart";
	public static final String CHILD_STPURCHASEPRICEPROPERTYPART_RESET_VALUE="";
	public static final String CHILD_STLTV="stLTV";
	public static final String CHILD_STLTV_RESET_VALUE="";
	public static final String CHILD_BTDELETEPROPERTY="btDeleteProperty";
	public static final String CHILD_BTDELETEPROPERTY_RESET_VALUE=" ";
	public static final String CHILD_BTPROPERTYDETAILS="btPropertyDetails";
	public static final String CHILD_BTPROPERTYDETAILS_RESET_VALUE=" ";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

  private DealEntryHandler handler=new DealEntryHandler();
  private doDealEntryPropertySelectModel doDealEntryPropertySelect=null;
  public SysLogger logger;

}

