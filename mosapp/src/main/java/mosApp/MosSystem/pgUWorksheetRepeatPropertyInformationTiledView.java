package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.event.TiledViewRequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;


/**
 *
 *
 *
 */
public class pgUWorksheetRepeatPropertyInformationTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgUWorksheetRepeatPropertyInformationTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doUWPropertyInfosModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getHdPropertyId().setValue(CHILD_HDPROPERTYID_RESET_VALUE);
		getHdPropertyCopyId().setValue(CHILD_HDPROPERTYCOPYID_RESET_VALUE);
		getCbUWPrimaryProperty().setValue(CHILD_CBUWPRIMARYPROPERTY_RESET_VALUE);
		getStPropertyAddressStreetNumber().setValue(CHILD_STPROPERTYADDRESSSTREETNUMBER_RESET_VALUE);
		getStPropertyAddressStreetName().setValue(CHILD_STPROPERTYADDRESSSTREETNAME_RESET_VALUE);
		getStPropertyAddressStreetType().setValue(CHILD_STPROPERTYADDRESSSTREETTYPE_RESET_VALUE);
		getStPropertyAddressStreetDirection().setValue(CHILD_STPROPERTYADDRESSSTREETDIRECTION_RESET_VALUE);
		getStPropertyAddressUnitNumber().setValue(CHILD_STPROPERTYADDRESSUNITNUMBER_RESET_VALUE);
		getStPropertyAddressCity().setValue(CHILD_STPROPERTYADDRESSCITY_RESET_VALUE);
		getStPropertyAddressProvince().setValue(CHILD_STPROPERTYADDRESSPROVINCE_RESET_VALUE);
		getStPropertyAddressPostalFSA().setValue(CHILD_STPROPERTYADDRESSPOSTALFSA_RESET_VALUE);
		getStPropertyAddressPostalLDU().setValue(CHILD_STPROPERTYADDRESSPOSTALLDU_RESET_VALUE);
		getStPropertyPurchasePrice().setValue(CHILD_STPROPERTYPURCHASEPRICE_RESET_VALUE);
		getStPropertyTotalEstimatedValue().setValue(CHILD_STPROPERTYTOTALESTIMATEDVALUE_RESET_VALUE);
		getStPropertyActualAppraisedValue().setValue(CHILD_STPROPERTYACTUALAPPRAISEDVALUE_RESET_VALUE);
		getStPropertyType().setValue(CHILD_STPROPERTYTYPE_RESET_VALUE);
		getStPropertyOccupancyType().setValue(CHILD_STPROPERTYOCCUPANCYTYPE_RESET_VALUE);
		getBtDeleteProperty().setValue(CHILD_BTDELETEPROPERTY_RESET_VALUE);
		getBtPropertyDetails().setValue(CHILD_BTPROPERTYDETAILS_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_HDPROPERTYID,HiddenField.class);
		registerChild(CHILD_HDPROPERTYCOPYID,HiddenField.class);
		registerChild(CHILD_CBUWPRIMARYPROPERTY,ComboBox.class);
		registerChild(CHILD_STPROPERTYADDRESSSTREETNUMBER,StaticTextField.class);
		registerChild(CHILD_STPROPERTYADDRESSSTREETNAME,StaticTextField.class);
		registerChild(CHILD_STPROPERTYADDRESSSTREETTYPE,StaticTextField.class);
		registerChild(CHILD_STPROPERTYADDRESSSTREETDIRECTION,StaticTextField.class);
		registerChild(CHILD_STPROPERTYADDRESSUNITNUMBER,StaticTextField.class);
		registerChild(CHILD_STPROPERTYADDRESSCITY,StaticTextField.class);
		registerChild(CHILD_STPROPERTYADDRESSPROVINCE,StaticTextField.class);
		registerChild(CHILD_STPROPERTYADDRESSPOSTALFSA,StaticTextField.class);
		registerChild(CHILD_STPROPERTYADDRESSPOSTALLDU,StaticTextField.class);
		registerChild(CHILD_STPROPERTYPURCHASEPRICE,StaticTextField.class);
		registerChild(CHILD_STPROPERTYTOTALESTIMATEDVALUE,StaticTextField.class);
		registerChild(CHILD_STPROPERTYACTUALAPPRAISEDVALUE,StaticTextField.class);
		registerChild(CHILD_STPROPERTYTYPE,StaticTextField.class);
		registerChild(CHILD_STPROPERTYOCCUPANCYTYPE,StaticTextField.class);
		registerChild(CHILD_BTDELETEPROPERTY,Button.class);
		registerChild(CHILD_BTPROPERTYDETAILS,Button.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--start//
    UWorksheetHandler handler =(UWorksheetHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

    // 1. Setup the Yes/No/Co-App Options
    String yesStr = BXResources.getGenericMsg("YES_LABEL", handler.getTheSessionState().getLanguageId());
    String noStr = BXResources.getGenericMsg("NO_LABEL", handler.getTheSessionState().getLanguageId());

    cbUWPrimaryPropertyOptions.setOptions(new String[]{noStr, yesStr},new String[]{"N", "Y"});
    //--Release2.1--end//

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}

		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_HDPROPERTYID))
		{
			HiddenField child = new HiddenField(this,
				getdoUWPropertyInfosModel(),
				CHILD_HDPROPERTYID,
				doUWPropertyInfosModel.FIELD_DFPROPERTYID,
				CHILD_HDPROPERTYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDPROPERTYCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoUWPropertyInfosModel(),
				CHILD_HDPROPERTYCOPYID,
				doUWPropertyInfosModel.FIELD_DFCOPYID,
				CHILD_HDPROPERTYCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBUWPRIMARYPROPERTY))
		{
			ComboBox child = new ComboBox( this,
				getdoUWPropertyInfosModel(),
				CHILD_CBUWPRIMARYPROPERTY,
				doUWPropertyInfosModel.FIELD_DFPRIMARYPROPERTYFLAG,
				CHILD_CBUWPRIMARYPROPERTY_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbUWPrimaryPropertyOptions);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYADDRESSSTREETNUMBER))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWPropertyInfosModel(),
				CHILD_STPROPERTYADDRESSSTREETNUMBER,
				doUWPropertyInfosModel.FIELD_DFPROPERTYSTREETNUMBER,
				CHILD_STPROPERTYADDRESSSTREETNUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYADDRESSSTREETNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWPropertyInfosModel(),
				CHILD_STPROPERTYADDRESSSTREETNAME,
				doUWPropertyInfosModel.FIELD_DFPROPERTYSTREETNAME,
				CHILD_STPROPERTYADDRESSSTREETNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYADDRESSSTREETTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWPropertyInfosModel(),
				CHILD_STPROPERTYADDRESSSTREETTYPE,
				doUWPropertyInfosModel.FIELD_DFPROPERTYSTREETTYPE,
				CHILD_STPROPERTYADDRESSSTREETTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYADDRESSSTREETDIRECTION))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWPropertyInfosModel(),
				CHILD_STPROPERTYADDRESSSTREETDIRECTION,
				doUWPropertyInfosModel.FIELD_DFPROPERTYSTREETDIRECTION,
				CHILD_STPROPERTYADDRESSSTREETDIRECTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYADDRESSUNITNUMBER))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWPropertyInfosModel(),
				CHILD_STPROPERTYADDRESSUNITNUMBER,
				doUWPropertyInfosModel.FIELD_DFPROPERTYUNITNUMBER,
				CHILD_STPROPERTYADDRESSUNITNUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYADDRESSCITY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWPropertyInfosModel(),
				CHILD_STPROPERTYADDRESSCITY,
				doUWPropertyInfosModel.FIELD_DFPROPERTYCITY,
				CHILD_STPROPERTYADDRESSCITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYADDRESSPROVINCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWPropertyInfosModel(),
				CHILD_STPROPERTYADDRESSPROVINCE,
				doUWPropertyInfosModel.FIELD_DFPROPERTYPROVINCE,
				CHILD_STPROPERTYADDRESSPROVINCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYADDRESSPOSTALFSA))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWPropertyInfosModel(),
				CHILD_STPROPERTYADDRESSPOSTALFSA,
				doUWPropertyInfosModel.FIELD_DFPROPERTYPOSTALFSA,
				CHILD_STPROPERTYADDRESSPOSTALFSA_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYADDRESSPOSTALLDU))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWPropertyInfosModel(),
				CHILD_STPROPERTYADDRESSPOSTALLDU,
				doUWPropertyInfosModel.FIELD_DFPROPERTYPOSTALLDU,
				CHILD_STPROPERTYADDRESSPOSTALLDU_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYPURCHASEPRICE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWPropertyInfosModel(),
				CHILD_STPROPERTYPURCHASEPRICE,
				doUWPropertyInfosModel.FIELD_DFPROPERTYPURCHASEPRICE,
				CHILD_STPROPERTYPURCHASEPRICE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYTOTALESTIMATEDVALUE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWPropertyInfosModel(),
				CHILD_STPROPERTYTOTALESTIMATEDVALUE,
				doUWPropertyInfosModel.FIELD_DFPROPERTYTOTALESTIMATEDVALUE,
				CHILD_STPROPERTYTOTALESTIMATEDVALUE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYACTUALAPPRAISEDVALUE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWPropertyInfosModel(),
				CHILD_STPROPERTYACTUALAPPRAISEDVALUE,
				doUWPropertyInfosModel.FIELD_DFPROPERTYACTUALAPPRAISALVALUE,
				CHILD_STPROPERTYACTUALAPPRAISEDVALUE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWPropertyInfosModel(),
				CHILD_STPROPERTYTYPE,
				doUWPropertyInfosModel.FIELD_DFPROPERTYTYPEDESC,
				CHILD_STPROPERTYTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYOCCUPANCYTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWPropertyInfosModel(),
				CHILD_STPROPERTYOCCUPANCYTYPE,
				doUWPropertyInfosModel.FIELD_DFPROPERTYOCCUPANCYTYPEDESC,
				CHILD_STPROPERTYOCCUPANCYTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETEPROPERTY))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETEPROPERTY,
				CHILD_BTDELETEPROPERTY,
				CHILD_BTDELETEPROPERTY_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTPROPERTYDETAILS))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPROPERTYDETAILS,
				CHILD_BTPROPERTYDETAILS,
				CHILD_BTPROPERTYDETAILS_RESET_VALUE,
				null);
				return child;

		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdPropertyId()
	{
		return (HiddenField)getChild(CHILD_HDPROPERTYID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdPropertyCopyId()
	{
		return (HiddenField)getChild(CHILD_HDPROPERTYCOPYID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbUWPrimaryProperty()
	{
		return (ComboBox)getChild(CHILD_CBUWPRIMARYPROPERTY);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyAddressStreetNumber()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYADDRESSSTREETNUMBER);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyAddressStreetName()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYADDRESSSTREETNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyAddressStreetType()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYADDRESSSTREETTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyAddressStreetDirection()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYADDRESSSTREETDIRECTION);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyAddressUnitNumber()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYADDRESSUNITNUMBER);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyAddressCity()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYADDRESSCITY);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyAddressProvince()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYADDRESSPROVINCE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyAddressPostalFSA()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYADDRESSPOSTALFSA);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyAddressPostalLDU()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYADDRESSPOSTALLDU);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyPurchasePrice()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyTotalEstimatedValue()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYTOTALESTIMATEDVALUE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyActualAppraisedValue()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYACTUALAPPRAISEDVALUE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyType()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyOccupancyType()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYOCCUPANCYTYPE);
	}


	/**
	 *
	 *
	 */
	public void handleBtDeletePropertyRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
    //logger = SysLog.getSysLogger("PGUWRPITV");

    UWorksheetHandler handler =(UWorksheetHandler) this.handler.cloneSS();

    //logger.debug("pgUWRPITV@handleBtDeletePropertyRequest::Parent: " + (pgUWorksheetViewBean) this.getParent());

    handler.preHandlerProtocol((ViewBean) this.getParent());

    int tileIndx = ((TiledViewRequestInvocationEvent)(event)).getTileNumber();
    //logger.debug("pgUWRPITV@handleBtDeletePropertyRequestTileIndex: " + tileIndx);

		handler.handleDelete(false,
                         2,
                          tileIndx,
                         "RepeatPropertyInformation/hdPropertyId",
                         "propertyId",
                         "RepeatPropertyInformation/hdPropertyCopyId",
                         "Property");

		//logger.debug("pgUWRPITV@handleBtDeletePropertyRequest::After handleDelete)");

    handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtDeleteProperty()
	{
		return (Button)getChild(CHILD_BTDELETEPROPERTY);
	}


	/**
	 *
	 *
	 */
	public String endBtDeletePropertyDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btDeleteProperty_onBeforeHtmlOutputEvent method
		UWorksheetHandler handler =(UWorksheetHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public void handleBtPropertyDetailsRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
    logger = SysLog.getSysLogger("PGUWRPITV");

		UWorksheetHandler handler =(UWorksheetHandler) this.handler.cloneSS();

		handler.preHandlerProtocol((ViewBean) this.getParent());
    //logger.debug("pgUWRPITV@handleBtPropertyDetailsRequest::Parent: " + (pgUWorksheetViewBean) this.getParent());

    int tileIndx = ((TiledViewRequestInvocationEvent)(event)).getTileNumber();
    //logger.debug("pgUWRPITV@handleBtPropertyDetailsRequest::TileIndex: " + tileIndx);

		handler.handlePropertyView(tileIndx,
                              "RepeatPropertyInformation/hdPropertyId",
                              "RepeatPropertyInformation/hdPropertyCopyId");

		//logger.debug("pgUWRPITV@handleBtPropertyDetailsRequest::After handlePropertyView)");

    handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtPropertyDetails()
	{
		return (Button)getChild(CHILD_BTPROPERTYDETAILS);
	}


	/**
	 *
	 *
	 */
	public doUWPropertyInfosModel getdoUWPropertyInfosModel()
	{
		if (doUWPropertyInfos == null)
			doUWPropertyInfos = (doUWPropertyInfosModel) getModel(doUWPropertyInfosModel.class);
		return doUWPropertyInfos;
	}


	/**
	 *
	 *
	 */
	public void setdoUWPropertyInfosModel(doUWPropertyInfosModel model)
	{
			doUWPropertyInfos = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_HDPROPERTYID="hdPropertyId";
	public static final String CHILD_HDPROPERTYID_RESET_VALUE="";
	public static final String CHILD_HDPROPERTYCOPYID="hdPropertyCopyId";
	public static final String CHILD_HDPROPERTYCOPYID_RESET_VALUE="";
	public static final String CHILD_CBUWPRIMARYPROPERTY="cbUWPrimaryProperty";
	public static final String CHILD_CBUWPRIMARYPROPERTY_RESET_VALUE="N";

  //--Release2.1--//
  //// This OptionList population is done now in the BeginDisplay via the BXResources
	////private static OptionList cbUWPrimaryPropertyOptions=new OptionList(new String[]{"No", "Yes"},new String[]{"N", "Y"});
	private OptionList cbUWPrimaryPropertyOptions=new OptionList();

	public static final String CHILD_STPROPERTYADDRESSSTREETNUMBER="stPropertyAddressStreetNumber";
	public static final String CHILD_STPROPERTYADDRESSSTREETNUMBER_RESET_VALUE="";
	public static final String CHILD_STPROPERTYADDRESSSTREETNAME="stPropertyAddressStreetName";
	public static final String CHILD_STPROPERTYADDRESSSTREETNAME_RESET_VALUE="";
	public static final String CHILD_STPROPERTYADDRESSSTREETTYPE="stPropertyAddressStreetType";
	public static final String CHILD_STPROPERTYADDRESSSTREETTYPE_RESET_VALUE="";
	public static final String CHILD_STPROPERTYADDRESSSTREETDIRECTION="stPropertyAddressStreetDirection";
	public static final String CHILD_STPROPERTYADDRESSSTREETDIRECTION_RESET_VALUE="";
	public static final String CHILD_STPROPERTYADDRESSUNITNUMBER="stPropertyAddressUnitNumber";
	public static final String CHILD_STPROPERTYADDRESSUNITNUMBER_RESET_VALUE="";
	public static final String CHILD_STPROPERTYADDRESSCITY="stPropertyAddressCity";
	public static final String CHILD_STPROPERTYADDRESSCITY_RESET_VALUE="";
	public static final String CHILD_STPROPERTYADDRESSPROVINCE="stPropertyAddressProvince";
	public static final String CHILD_STPROPERTYADDRESSPROVINCE_RESET_VALUE="";
	public static final String CHILD_STPROPERTYADDRESSPOSTALFSA="stPropertyAddressPostalFSA";
	public static final String CHILD_STPROPERTYADDRESSPOSTALFSA_RESET_VALUE="";
	public static final String CHILD_STPROPERTYADDRESSPOSTALLDU="stPropertyAddressPostalLDU";
	public static final String CHILD_STPROPERTYADDRESSPOSTALLDU_RESET_VALUE="";
	public static final String CHILD_STPROPERTYPURCHASEPRICE="stPropertyPurchasePrice";
	public static final String CHILD_STPROPERTYPURCHASEPRICE_RESET_VALUE="";
	public static final String CHILD_STPROPERTYTOTALESTIMATEDVALUE="stPropertyTotalEstimatedValue";
	public static final String CHILD_STPROPERTYTOTALESTIMATEDVALUE_RESET_VALUE="";
	public static final String CHILD_STPROPERTYACTUALAPPRAISEDVALUE="stPropertyActualAppraisedValue";
	public static final String CHILD_STPROPERTYACTUALAPPRAISEDVALUE_RESET_VALUE="";
	public static final String CHILD_STPROPERTYTYPE="stPropertyType";
	public static final String CHILD_STPROPERTYTYPE_RESET_VALUE="";
	public static final String CHILD_STPROPERTYOCCUPANCYTYPE="stPropertyOccupancyType";
	public static final String CHILD_STPROPERTYOCCUPANCYTYPE_RESET_VALUE="";
	public static final String CHILD_BTDELETEPROPERTY="btDeleteProperty";
	public static final String CHILD_BTDELETEPROPERTY_RESET_VALUE=" ";
	public static final String CHILD_BTPROPERTYDETAILS="btPropertyDetails";
	public static final String CHILD_BTPROPERTYDETAILS_RESET_VALUE=" ";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doUWPropertyInfosModel doUWPropertyInfos=null;
  public SysLogger logger;
	private UWorksheetHandler handler=new UWorksheetHandler();

}

