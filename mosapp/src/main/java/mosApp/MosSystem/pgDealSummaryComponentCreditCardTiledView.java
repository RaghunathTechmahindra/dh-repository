package mosApp.MosSystem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.html.StaticTextField;

/**
 * Title: pgDealSummaryComponentCreditCardTiledView
 * <p>
 * Description:This view bean relates the pgDealSummary.jsp.
 * @author MCM Impl Team.
 * @version 1.0 10-June-2008 XS_16.7 Initial version
 * @version 1.1 25-June-2008 XS_16.16 Added new field to display Additional Details.
 * @version 1.2 17-July-2008 XS_2.8 Added nextTile(boolean) to avoid handler and fields confliction
 */
public class pgDealSummaryComponentCreditCardTiledView extends
        RequestHandlingTiledViewBase implements TiledView, RequestHandler
{

    public static Map FIELD_DESCRIPTORS;

    public static final String CHILD_STCOMPONENTTYPE = "stComponentType";

    public static final String CHILD_STCOMPONENTTYPE_RESET_VALUE = "";

    public static final String CHILD_STPRODUCTNAME = "stProductName";

    public static final String CHILD_STPRODUCTNAME_RESET_VALUE = "";

    public static final String CHILD_STAMOUNT = "stAmount";

    public static final String CHILD_STAMOUNT_RESET_VALUE = "";

    public static final String CHILD_STNETRATE = "stNetRate";

    public static final String CHILD_STNETRATE_RESET_VALUE = "";
    
    /*****************MCM Impl Team XS_16.16 changes starts****************************/
    public static final String CHILD_STADDITIONALDETAILS = "stAdditionalInfo";
    public static final String CHILD_STADDITIONALDETAILS_RESET_VALUE = "";
    /*****************MCM Impl Team XS_16.16 changes starts****************************/

    /**
     * Models used by this class to display information.
     */
    private doComponentCreditCardModel doComponentCreditCardModel = null;

    /**
     * Handler class used by this class to display information.
     */
    private DealSummaryHandler handler = new DealSummaryHandler();

    /**
     * This Constructor registers the children and sets the default URL.
     * @param View
     *            object in which this tile is a part of
     * @param name
     *            of this tiled view
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    public pgDealSummaryComponentCreditCardTiledView(View parent, String name)
    {
        super(parent, name);
        setMaxDisplayTiles(100);
        setPrimaryModelClass(doComponentCreditCardModel.class);
        registerChildren();
        initialize();

    }

    /**
     * Initialize.
     */
    protected void initialize()
    {

    }

    /**
     * Description: This method resets the children which are fields on the
     * corresponding JSP
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     * @version 1.1 25-June-2008 XS_16.16 Added necessary changes required to the new field AdditionalDetails.
     */
    public void resetChildren()
    {

        getStComponentType().setValue(CHILD_STCOMPONENTTYPE_RESET_VALUE);
        getStProductName().setValue(CHILD_STPRODUCTNAME_RESET_VALUE);
        getStAmount().setValue(CHILD_STAMOUNT_RESET_VALUE);
        getStNetRate().setValue(CHILD_STNETRATE_RESET_VALUE);
        getStNetRate().setValue(CHILD_STNETRATE_RESET_VALUE);
        /******************MCM Impl Team XS_16.16 changes starts**********************/
        getStAdditionalDetails().setValue(CHILD_STADDITIONALDETAILS_RESET_VALUE);
        /******************MCM Impl Team XS_16.16 changes ends ***************/
        getStNetRate().setValue(CHILD_STNETRATE_RESET_VALUE);

    }

    /**
     * Description: This method registers the children which are fields on the
     * corresponding JSP.
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     * @version 1.1 25-June-2008 XS_16.16 Added necessary changes required to the new field AdditionalDetails.
     */
    protected void registerChildren()
    {

        registerChild(CHILD_STCOMPONENTTYPE, StaticTextField.class);
        registerChild(CHILD_STPRODUCTNAME, StaticTextField.class);
        registerChild(CHILD_STAMOUNT, StaticTextField.class);
        registerChild(CHILD_STNETRATE, StaticTextField.class);
        /******************MCM Impl Team XS_16.16 changes starts**********************/
        registerChild(CHILD_STADDITIONALDETAILS, StaticTextField.class);
        /******************MCM Impl Team XS_16.16 changes ends**********************/

    }

    /**
     * Description: Adds all models to the model list
     * <p>.
     * @param executionType
     *            the execution type
     * @return model[]- List of models used by this viewbean
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    public Model[] getWebActionModels(int executionType)
    {

        List modelList = new ArrayList();

        switch (executionType)
        {

        case MODEL_TYPE_RETRIEVE:
            modelList.add(getDoComponentCreditCardModel());
            ;

            break;

        case MODEL_TYPE_UPDATE:
            ;

            break;

        case MODEL_TYPE_DELETE:
            ;

            break;

        case MODEL_TYPE_INSERT:
            ;

            break;

        case MODEL_TYPE_EXECUTE:
            ;

            break;

        }

        return (Model[]) modelList.toArray(new Model[0]);

    }

    /**
     * Description:This method creates the handler objects, saves the page state
     * and also populates the combo box fields. Also resets the tile index.
     * <p>
     * @param event -
     *            DisplayEvent which triggered this call.
     * @return void
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    public void beginDisplay(DisplayEvent event) throws ModelControlException
    {

        if (getPrimaryModel() == null)
        {

            throw new ModelControlException("Primary model is null");

        }

        super.beginDisplay(event);
        resetTileIndex();

    }

    /**
     * Description: This method moves the control from current tile to next
     * tile.
     * @return boolean - true, if the tile has moved. False otherwise.
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    public boolean nextTile() throws ModelControlException
    {

        boolean movedToRow = super.nextTile();

        if (movedToRow)
        {

        }

        return movedToRow;

    }

    /**
     * <p>nextTile</p>
     * <p> to be calles from child to skip this tiled nextTile to avoid confliction
     * @param skipThisTile
     * @return
     * @throws ModelControlException
     * 
     * @version 1.0 July 17, 2008 MCM Team for XS 2.8
     */
    protected boolean nextTile (boolean skipThisTile) throws ModelControlException {
        
        if (skipThisTile) {
            return super.nextTile();
        } else return nextTile();
    }

    
    
    /**
     * Description: Creates the children represented by fields on the JSP.
     * @param name -
     *            String name of the child
     * @return View - JATO field representing the child
     * @exception IllegalArgumentException
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     * @version 1.1 25-June-2008 XS_16.16 Added necessary changes required to the new field AdditionalDetails.
     */
    protected View createChild(String name)
    {

        if (name.equals(CHILD_STCOMPONENTTYPE))
        {

            StaticTextField child = new StaticTextField(this,
                    getDoComponentCreditCardModel(), CHILD_STCOMPONENTTYPE,
                    doComponentCreditCardModel.FIELD_DFCOMPONENTTYPE,
                    CHILD_STCOMPONENTTYPE_RESET_VALUE, null);

            return child;

        }
        else
            if (name.equals(CHILD_STPRODUCTNAME))
            {

                StaticTextField child = new StaticTextField(this,
                        getDoComponentCreditCardModel(), CHILD_STPRODUCTNAME,
                        doComponentCreditCardModel.FIELD_DFPRODUCTNAME,
                        CHILD_STPRODUCTNAME_RESET_VALUE, null);

                return child;

            }
            else
                if (name.equals(CHILD_STAMOUNT))
                {

                    StaticTextField child = new StaticTextField(
                            this,
                            getDoComponentCreditCardModel(),
                            CHILD_STAMOUNT,
                            doComponentCreditCardModel.FIELD_DFCREDITCARDAMOUNT,
                            CHILD_STAMOUNT_RESET_VALUE, null);

                    return child;

                }
                else
                    if (name.equals(CHILD_STNETRATE))
                    {

                        StaticTextField child = new StaticTextField(this,
                                getDoComponentCreditCardModel(),
                                CHILD_STNETRATE,
                                doComponentCreditCardModel.FIELD_DFPOSTEDRATE,
                                CHILD_STNETRATE_RESET_VALUE, null);

                        return child;

                    }
        /**********************MCM Impl Team XS_16.16 changes starts***********************/
                    else
                        if (name.equals(CHILD_STADDITIONALDETAILS))
                        {

                            StaticTextField child = new StaticTextField(this,
                                    getDoComponentCreditCardModel(),
                                    CHILD_STADDITIONALDETAILS,
                                    doComponentCreditCardModel.FIELD_DFADDITIONALINFORMATION,
                                    CHILD_STADDITIONALDETAILS_RESET_VALUE, null);

                            return child;

                        }
        /**********************MCM Impl Team XS_16.16 changes ends***********************/
                    else
                    {

                        throw new IllegalArgumentException(
                                "Invalid child name [" + name + "]");

                    }

    }

    /**
     * Getters and setters for the display fields starts
     */
    public StaticTextField getStComponentType()
    {

        return (StaticTextField) getChild(CHILD_STCOMPONENTTYPE);

    }

    public StaticTextField getStProductName()
    {

        return (StaticTextField) getChild(CHILD_STPRODUCTNAME);

    }

    public StaticTextField getStNetRate()
    {

        return (StaticTextField) getChild(CHILD_STNETRATE);

    }

    public StaticTextField getStAmount()
    {

        return (StaticTextField) getChild(CHILD_STAMOUNT);

    }
 /****************MCM Impl Team XS_16.16 changes starts********************************/
    public StaticTextField getStAdditionalDetails()
    {

        return (StaticTextField) getChild(CHILD_STADDITIONALDETAILS);

    }
 /****************MCM Impl Team XS_16.16 changes ends********************************/   
    /**
     * Getters and setters for the display fields ends.
     */

    /**
     * <p>
     * Description: This method returns the model of type
     * doComponentCreditCardModel used by this class. If the model has not been
     * set, an instance is created.
     * </p>
     * @returns doComponentCreditCardModel - the model used by this view
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    public doComponentCreditCardModel getDoComponentCreditCardModel()
    {

        if (doComponentCreditCardModel == null)
        {

            doComponentCreditCardModel = (doComponentCreditCardModel) getModel(doComponentCreditCardModel.class);

        }

        return doComponentCreditCardModel;

    }

}
