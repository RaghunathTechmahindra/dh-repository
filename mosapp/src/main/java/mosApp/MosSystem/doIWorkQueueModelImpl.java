package mosApp.MosSystem;

import java.math.BigDecimal;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doIWorkQueueModelImpl
	extends QueryModelBase
	implements doIWorkQueueModel {
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  public static final Log _log = LogFactory.getLog(doIWorkQueueModelImpl.class);
  
	public static final String DATA_SOURCE_NAME = "jdbc/orcl";

	public static final String SELECT_SQL_TEMPLATE =
		"SELECT ALL "
			+ "CONTACT.CONTACTLASTNAME, "
			+ "SOURCEFIRMPROFILE.SFSHORTNAME, "
			+ "ASSIGNEDTASKSWORKQUEUE.ASSIGNEDTASKSWORKQUEUEID, "
			+ "ASSIGNEDTASKSWORKQUEUE.DEALID, "
			+ "ASSIGNEDTASKSWORKQUEUE.APPLICATIONID, "
			+ "ASSIGNEDTASKSWORKQUEUE.TASKID, "
			+ "ASSIGNEDTASKSWORKQUEUE.TASKLABEL, "
			+ "ASSIGNEDTASKSWORKQUEUE.ENDTIMESTAMP, "
			+ "ASSIGNEDTASKSWORKQUEUE.DUETIMESTAMP, "
			+ "ASSIGNEDTASKSWORKQUEUE.USERPROFILEID, "
			+ "to_char(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID) TASKSTATUSID_STR, "
			+ "to_char(ASSIGNEDTASKSWORKQUEUE.PRIORITYID) PRIORITYID_STR, "
			+ "to_char(DEAL.HOLDREASONID) HOLDREASONID_STR, "
			+ "DEAL.HOLDREASONID, "
			+ "to_char(ASSIGNEDTASKSWORKQUEUE.TIMEMILESTONEID) TIMEMILESTONEID_STR, "
			+ "DEAL.COPYTYPE, "
			+ "DEAL.COPYID, "
      + "DEAL.INSTITUTIONPROFILEID, "
      + "to_char(DEAL.INSTITUTIONPROFILEID) INSTITUTIONPROFILEID_STR, "
      + "INSTITUTIONPROFILE.INSTITUTIONPROFILEID, "
      + "INSTITUTIONPROFILE.INSTITUTIONNAME, "
			+ "to_char(DEAL.STATUSID) DEALSTATUSID_STR "
			+ "FROM "
			+ "ASSIGNEDTASKSWORKQUEUE, "
			+ "DEAL, "
			+ "SOURCEOFBUSINESSPROFILE, "
			+ "CONTACT, "
			+ "SOURCEFIRMPROFILE, "
			+ "BORROWER, "
      + "INSTITUTIONPROFILE "
			+ " __WHERE__  ";

	public static final String MODIFYING_QUERY_TABLE_NAME =
		"ASSIGNEDTASKSWORKQUEUE, DEAL, SOURCEOFBUSINESSPROFILE, CONTACT, "
			+ "SOURCEFIRMPROFILE, BORROWER,INSTITUTIONPROFILE  ";

	public static final String STATIC_WHERE_CRITERIA =
		" ((ASSIGNEDTASKSWORKQUEUE.DEALID = DEAL.DEALID AND "
      + "DEAL.INSTITUTIONPROFILEID = INSTITUTIONPROFILE.INSTITUTIONPROFILEID AND "
      + "DEAL.INSTITUTIONPROFILEID = SOURCEOFBUSINESSPROFILE.INSTITUTIONPROFILEID AND "
      + "DEAL.INSTITUTIONPROFILEID = SOURCEFIRMPROFILE.INSTITUTIONPROFILEID AND "
      + "DEAL.INSTITUTIONPROFILEID = BORROWER.INSTITUTIONPROFILEID AND "
      + "SOURCEOFBUSINESSPROFILE.INSTITUTIONPROFILEID = CONTACT.INSTITUTIONPROFILEID AND "
      + "ASSIGNEDTASKSWORKQUEUE.INSTITUTIONPROFILEID = DEAL.INSTITUTIONPROFILEID AND "
			+ "DEAL.SOURCEOFBUSINESSPROFILEID = SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPROFILEID AND "
			+ "DEAL.SOURCEFIRMPROFILEID = SOURCEFIRMPROFILE.SOURCEFIRMPROFILEID AND "
			+ "SOURCEOFBUSINESSPROFILE.CONTACTID = CONTACT.CONTACTID AND "
			+ "DEAL.COPYID = BORROWER.COPYID (+)  AND "
			+ "DEAL.DEALID = BORROWER.DEALID(+) AND "
			+ "BORROWER.primaryborrowerflag (+)='Y') AND "
			+ "DEAL.SCENARIORECOMMENDED='Y' AND "
			+ "DEAL.COPYTYPE <> 'T' AND "
			+ "ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG <> 0 ) ";
	//-- ========== SCR#895   ends ========== --//

	//-- ========== SCR#750 ends ============================================== --//
	public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFCONTACTLASTNAME =
		"CONTACT.CONTACTLASTNAME";
	public static final String COLUMN_DFCONTACTLASTNAME = "CONTACTLASTNAME";
	public static final String QUALIFIED_COLUMN_DFSFSHORTNAME =
		"SOURCEFIRMPROFILE.SFSHORTNAME";
	public static final String COLUMN_DFSFSHORTNAME = "SFSHORTNAME";
	public static final String QUALIFIED_COLUMN_DFASSIGNEDTASKSWORKQUEUEID =
		"ASSIGNEDTASKSWORKQUEUE.ASSIGNEDTASKSWORKQUEUEID";
	public static final String COLUMN_DFASSIGNEDTASKSWORKQUEUEID =
		"ASSIGNEDTASKSWORKQUEUEID";
	public static final String QUALIFIED_COLUMN_DFDEALID =
		"ASSIGNEDTASKSWORKQUEUE.DEALID";
	public static final String COLUMN_DFDEALID = "DEALID";
	public static final String QUALIFIED_COLUMN_DFAPPLICATIONID =
		"ASSIGNEDTASKSWORKQUEUE.APPLICATIONID";
	public static final String COLUMN_DFAPPLICATIONID = "APPLICATIONID";
	public static final String QUALIFIED_COLUMN_DFTASKID =
		"ASSIGNEDTASKSWORKQUEUE.TASKID";
	public static final String COLUMN_DFTASKID = "TASKID";
	public static final String QUALIFIED_COLUMN_DFTASK_TASKLABEL =
		"ASSIGNEDTASKSWORKQUEUE.TASKLABEL";
	public static final String COLUMN_DFTASK_TASKLABEL = "TASKLABEL";
	public static final String QUALIFIED_COLUMN_DFDUETIMESTAMP =
		"ASSIGNEDTASKSWORKQUEUE.DUETIMESTAMP";
	public static final String COLUMN_DFDUETIMESTAMP = "DUETIMESTAMP";
	public static final String QUALIFIED_COLUMN_DFUSERPROFILEID =
		"ASSIGNEDTASKSWORKQUEUE.USERPROFILEID";
	public static final String COLUMN_DFUSERPROFILEID = "USERPROFILEID";
	public static final String QUALIFIED_COLUMN_DFCOPYTYPE =
		"DEAL.COPYTYPE";
	public static final String COLUMN_DFCOPYTYPE = "COPYTYPE";
	public static final String QUALIFIED_COLUMN_DFCOPYID = "DEAL.COPYID";
	public static final String COLUMN_DFCOPYID = "COPYID";

	//-- ========== SCR#750 begins ============================================ --//
	//-- by Neil on Dec/15/2004
	public static final String QUALIFIED_COLUMN_DFHOLDREASONID =
		"DEAL.HOLDREASONID";
	public static final String COLUMN_DFHOLDREASONID = "HOLDREASONID";
	public static final String QUALIFIED_COLUMN_DFHOLDREASONDESCRIPTION =
		"DEAL.HOLDREASONID_STR";
	public static final String COLUMN_DFHOLDREASONDESCRIPTION =
		"HOLDREASONID_STR";
	//-- ========== SCR#895 begins ========== --//
	//-- by Neil on Jan/25/2005
	//  public static final String QUALIFIED_COLUMN_DFDEALSTATUS = "STATUS.STATUSDESCRIPTION";
	//  public static final String COLUMN_DFDEALSTATUS = "STATUSDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFDEALSTATUS =
		"DEAL.DEALSTATUSID_STR";
	public static final String COLUMN_DFDEALSTATUS = "DEALSTATUSID_STR";
	//-- ========== SCR#895   ends ========== --//
	//-- ========== SCR#750 ends ============================================== --//

	//--Release2.1--//
	//--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFTASKSTATUS_TSDESCRIPTION="TASKSTATUS.TSDESCRIPTION";
	//public static final String COLUMN_DFTASKSTATUS_TSDESCRIPTION="TSDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFTASKSTATUS_TSDESCRIPTION =
		"ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID_STR";
	public static final String COLUMN_DFTASKSTATUS_TSDESCRIPTION =
		"TASKSTATUSID_STR";

	//public static final String QUALIFIED_COLUMN_DFPRIORITY_PDESCRIPTION="PRIORITY.PDESCRIPTION";
	//public static final String COLUMN_DFPRIORITY_PDESCRIPTION="PDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFPRIORITY_PDESCRIPTION =
		"ASSIGNEDTASKSWORKQUEUE.PRIORITYID_STR";
	public static final String COLUMN_DFPRIORITY_PDESCRIPTION =
		"PRIORITYID_STR";

	//public static final String QUALIFIED_COLUMN_DFTMDESCRIPTION="TIMEMILESTONE.TMDESCRIPTION";
	//public static final String COLUMN_DFTMDESCRIPTION="TMDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFTMDESCRIPTION =
		"ASSIGNEDTASKSWORKQUEUE.TIMEMILESTONEID_STR";
	public static final String COLUMN_DFTMDESCRIPTION = "TIMEMILESTONEID_STR";

  
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "DEAL.INSTITUTIONPROFILEID";
    public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
  
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONNAME =
        "DEAL.INSTITUTIONPROFILEID_STR";
    public static final String COLUMN_DFINSTITUTIONNAME = "INSTITUTIONPROFILEID_STR";
	//=======================================================================================================
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////
	static {
		//-- ========== SCR#750 begins ========== --//
		//-- by Neil on Dec/15/2004
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALSTATUS,
				COLUMN_DFDEALSTATUS,
				QUALIFIED_COLUMN_DFDEALSTATUS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFHOLDREASONID,
				COLUMN_DFHOLDREASONID,
				QUALIFIED_COLUMN_DFHOLDREASONID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFHOLDREASONDESCRIPTION,
				COLUMN_DFHOLDREASONDESCRIPTION,
				QUALIFIED_COLUMN_DFHOLDREASONDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		//-- ========== SCR#750 ends ========== --//
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTACTLASTNAME,
				COLUMN_DFCONTACTLASTNAME,
				QUALIFIED_COLUMN_DFCONTACTLASTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSFSHORTNAME,
				COLUMN_DFSFSHORTNAME,
				QUALIFIED_COLUMN_DFSFSHORTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFASSIGNEDTASKSWORKQUEUEID,
				COLUMN_DFASSIGNEDTASKSWORKQUEUEID,
				QUALIFIED_COLUMN_DFASSIGNEDTASKSWORKQUEUEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPLICATIONID,
				COLUMN_DFAPPLICATIONID,
				QUALIFIED_COLUMN_DFAPPLICATIONID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTASKID,
				COLUMN_DFTASKID,
				QUALIFIED_COLUMN_DFTASKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTASK_TASKLABEL,
				COLUMN_DFTASK_TASKLABEL,
				QUALIFIED_COLUMN_DFTASK_TASKLABEL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTASKSTATUS_TSDESCRIPTION,
				COLUMN_DFTASKSTATUS_TSDESCRIPTION,
				QUALIFIED_COLUMN_DFTASKSTATUS_TSDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIORITY_PDESCRIPTION,
				COLUMN_DFPRIORITY_PDESCRIPTION,
				QUALIFIED_COLUMN_DFPRIORITY_PDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDUETIMESTAMP,
				COLUMN_DFDUETIMESTAMP,
				QUALIFIED_COLUMN_DFDUETIMESTAMP,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTMDESCRIPTION,
				COLUMN_DFTMDESCRIPTION,
				QUALIFIED_COLUMN_DFTMDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERPROFILEID,
				COLUMN_DFUSERPROFILEID,
				QUALIFIED_COLUMN_DFUSERPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYTYPE,
				COLUMN_DFCOPYTYPE,
				QUALIFIED_COLUMN_DFCOPYTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    
    
    FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
          FIELD_DFINSTITUTIONID,
          COLUMN_DFINSTITUTIONID,
          QUALIFIED_COLUMN_DFINSTITUTIONID,
          String.class,
          false,
          false,
          QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
          "",
          QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
          ""));
    
    FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
          FIELD_DFINSTITUTIONNAME,
          COLUMN_DFINSTITUTIONNAME,
          QUALIFIED_COLUMN_DFINSTITUTIONNAME,
          String.class,
          false,
          false,
          QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
          "",
          QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
          ""));

	}

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	/**
	 *
	 *
	 */
	public doIWorkQueueModelImpl() {
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();
	}

	/**
	 *
	 *
	 */
	public void initialize() {
	}

	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////

	/**
	 *
	 *
	 */
	protected String beforeExecute(
		ModelExecutionContext context,
		int queryType,
		String sql)
		throws ModelControlException {
		_log.debug("VLAD ===> DOIWQMI@SQLBeforeExecute: " + sql);
    _log.debug("VLAd ===> DOIWQMI@SQL WHERE: " + STATIC_WHERE_CRITERIA);
		return sql;
	}

	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException {
		//--Release2.1--//
		//To override all the Description fields by populating from BXResource
		//--> By Billy 05Nov2002
		//Get the Language ID from the SessionStateModel
		String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(
				SessionStateModel.class);
		SessionStateModelImpl theSessionState =
			(SessionStateModelImpl) getRequestContext()
				.getModelManager()
				.getModel(
				SessionStateModel.class,
				defaultInstanceStateName,
				true);
		int languageId = theSessionState.getLanguageId();

		while (this.next()) {
			//--> todo : Populate the TaskLabel also based on the WorkFlow ID : By Billy 07Nov2002
			//Convert the Task Labels
			//Check if this is a Sentinal Task i.e. TaskId > 50000
			if (this.getDfTASKID().intValue() > 50000) {
				//It's a Sentinal Task ==> Added Sentinal Tasl Prefix
				this.setDfTASK_TASKLABEL(
					BXResources.getGenericMsg(
						"SENTINEL_TASK_PREFIX",
						languageId)
						+ " "
						+ BXResources.getPickListDescription(((this.getDfInstitutionId()).intValue()),
							"TASK741",
							this.getDfTASKID().intValue() - 50000,
							languageId));
			} else {
				//It's a normal Task
				this.setDfTASK_TASKLABEL(
					BXResources.getPickListDescription(((this.getDfInstitutionId()).intValue()),
						"TASK741",
						this.getDfTASKID().intValue(),
						languageId));
			}

			//Convert Task Status Descriptions
			this.setDfTASKSTATUS_TSDESCRIPTION(
				BXResources.getPickListDescription(((this.getDfInstitutionId()).intValue()),
					"TASKSTATUS",
					this.getDfTASKSTATUS_TSDESCRIPTION(),
					languageId));

			//-- ========== SCR#895 begins ========== --//
			// -- by Neil on Jan/25/2005 --//
			//Convert Deal Status Descriptions
			this.setDfDealStatus(
				BXResources.getPickListDescription(((this.getDfInstitutionId()).intValue()),
					"STATUS",
					this.getDfDealStatus(),
					languageId));
			//-- ========== SCR#895   ends ========== --//

			//Convert Priority Descriptions
			this.setDfPRIORITY_PDESCRIPTION(
				BXResources.getPickListDescription(((this.getDfInstitutionId()).intValue()),
					"PRIORITY",
					this.getDfPRIORITY_PDESCRIPTION(),
					languageId));

			//Convert TimeMilestone Descriptions
			this.setDfTMDESCRIPTION(
				BXResources.getPickListDescription(((this.getDfInstitutionId()).intValue()),
					"TIMEMILESTONE",
					this.getDfTMDESCRIPTION(),
					languageId));

			//-- ========== SCR#750 begins ========== --//
			//-- by Neil on Dec/23/2004
			//-- Override Hold Reason with ResourceBundle.
			this.setDfHoldReasonDescription(
				BXResources.getPickListDescription(((this.getDfInstitutionId()).intValue()),
					"HOLDREASON",
					this.getDfHoldReasonDescription(),
					languageId));

			//-- ========== SCR#750   ends ========== --//
            this.setDfInstitutionName(
                    BXResources.getInstitutionName((this.getDfInstitutionId()).intValue()));
		}
        //Convert Priority Descriptions
        

		
        //Reset Location
		this.beforeFirst();
	}

	/**
	 *
	 *
	 */
	protected void onDatabaseError(
		ModelExecutionContext context,
		int queryType,
		SQLException exception) {
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId() {
		return (java.math.BigDecimal) getValue(FIELD_DFDEALID);
	}

	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value) {
		setValue(FIELD_DFDEALID, value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId() {
		return (java.math.BigDecimal) getValue(FIELD_DFCOPYID);
	}

	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value) {
		setValue(FIELD_DFCOPYID, value);
	}

	/**
	 *
	 *
	 */
	public String getDfCONTACTLASTNAME() {
		return (String) getValue(FIELD_DFCONTACTLASTNAME);
	}

	/**
	 *
	 *
	 */
	public void setDfCONTACTLASTNAME(String value) {
		setValue(FIELD_DFCONTACTLASTNAME, value);
	}

	/**
	 *
	 *
	 */
	public String getDfSFSHORTNAME() {
		return (String) getValue(FIELD_DFSFSHORTNAME);
	}

	/**
	 *
	 *
	 */
	public void setDfSFSHORTNAME(String value) {
		setValue(FIELD_DFSFSHORTNAME, value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfASSIGNEDTASKSWORKQUEUEID() {
		return (java.math.BigDecimal) getValue(
			FIELD_DFASSIGNEDTASKSWORKQUEUEID);
	}

	/**
	 *
	 *
	 */
	public void setDfASSIGNEDTASKSWORKQUEUEID(java.math.BigDecimal value) {
		setValue(FIELD_DFASSIGNEDTASKSWORKQUEUEID, value);
	}

	//-- ========== SCR#750 begins ============================================ --//
	//-- by Neil on Dec/15/2004

	/**
	 *
	 */
	public String getDfDealStatus() {
		return (String) getValue(FIELD_DFDEALSTATUS);
	}

	/**
	 *
	 */
	public void setDfDealStatus(String value) {
		setValue(FIELD_DFDEALSTATUS, value);
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @return String
	 */
	public BigDecimal getDfHoldReasonId() {
		return (BigDecimal) getValue(FIELD_DFHOLDREASONID);
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param value String
	 */
	public void setDfHoldReasonId(BigDecimal value) {
		setValue(FIELD_DFHOLDREASONID, value);
	}

	/**
	 *
	 */
	public String getDfHoldReasonDescription() {
		return (String) getValue(FIELD_DFHOLDREASONDESCRIPTION);
	}

	/**
	 *
	 */
	public void setDfHoldReasonDescription(String value) {
		setValue(FIELD_DFHOLDREASONDESCRIPTION, value);
	}

	//-- ========== SCR#750 ends ============================================== --//

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDEALID() {
		return (java.math.BigDecimal) getValue(FIELD_DFDEALID);
	}

	/**
	 *
	 *
	 */
	public void setDfDEALID(java.math.BigDecimal value) {
		setValue(FIELD_DFDEALID, value);
	}

	/**
	 *
	 *
	 */
	public String getDfAPPLICATIONID() {
		return (String) getValue(FIELD_DFAPPLICATIONID);
	}

	/**
	 *
	 *
	 */
	public void setDfAPPLICATIONID(String value) {
		setValue(FIELD_DFAPPLICATIONID, value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTASKID() {
		return (java.math.BigDecimal) getValue(FIELD_DFTASKID);
	}

	/**
	 *
	 *
	 */
	public void setDfTASKID(java.math.BigDecimal value) {
		setValue(FIELD_DFTASKID, value);
	}

	/**
	 *
	 *
	 */
	public String getDfTASK_TASKLABEL() {
		return (String) getValue(FIELD_DFTASK_TASKLABEL);
	}

	/**
	 *
	 *
	 */
	public void setDfTASK_TASKLABEL(String value) {
		setValue(FIELD_DFTASK_TASKLABEL, value);
	}

	/**
	 *
	 *
	 */
	public String getDfTASKSTATUS_TSDESCRIPTION() {
		return (String) getValue(FIELD_DFTASKSTATUS_TSDESCRIPTION);
	}

	/**
	 *
	 *
	 */
	public void setDfTASKSTATUS_TSDESCRIPTION(String value) {
		setValue(FIELD_DFTASKSTATUS_TSDESCRIPTION, value);
	}

	/**
	 *
	 *
	 */
	public String getDfPRIORITY_PDESCRIPTION() {
		return (String) getValue(FIELD_DFPRIORITY_PDESCRIPTION);
	}

	/**
	 *
	 *
	 */
	public void setDfPRIORITY_PDESCRIPTION(String value) {
		setValue(FIELD_DFPRIORITY_PDESCRIPTION, value);
	}

	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfDUETIMESTAMP() {
		return (java.sql.Timestamp) getValue(FIELD_DFDUETIMESTAMP);
	}

	/**
	 *
	 *
	 */
	public void setDfDUETIMESTAMP(java.sql.Timestamp value) {
		setValue(FIELD_DFDUETIMESTAMP, value);
	}

	/**
	 *
	 *
	 */
	public String getDfTMDESCRIPTION() {
		return (String) getValue(FIELD_DFTMDESCRIPTION);
	}

	/**
	 *
	 *
	 */
	public void setDfTMDESCRIPTION(String value) {
		setValue(FIELD_DFTMDESCRIPTION, value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfUSERPROFILEID() {
		return (java.math.BigDecimal) getValue(FIELD_DFUSERPROFILEID);
	}

	/**
	 *
	 *
	 */
	public void setDfUSERPROFILEID(java.math.BigDecimal value) {
		setValue(FIELD_DFUSERPROFILEID, value);
	}

	/**
	 *
	 *
	 */
	public String getDfCOPYTYPE() {
		return (String) getValue(FIELD_DFCOPYTYPE);
	}

	/**
	 *
	 *
	 */
	public void setDfCOPYTYPE(String value) {
		setValue(FIELD_DFCOPYTYPE, value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCOPYID() {
		return (java.math.BigDecimal) getValue(FIELD_DFCOPYID);
	}

	/**
	 *
	 *
	 */
	public void setDfCOPYID(java.math.BigDecimal value) {
		setValue(FIELD_DFCOPYID, value);
	}

  
  public BigDecimal getDfInstitutionId() {
    return (BigDecimal) getValue(FIELD_DFINSTITUTIONID);
  }

  public void setDfInstitutionId(BigDecimal value) {
    setValue(FIELD_DFINSTITUTIONID, value);
  }
  
  public void setDfInstitutionName(String value) {
    setValue(FIELD_DFINSTITUTIONNAME, value);
  }

  public String getDfInstitutionName() {
    return (String) getValue(FIELD_DFINSTITUTIONNAME);

  }
}
