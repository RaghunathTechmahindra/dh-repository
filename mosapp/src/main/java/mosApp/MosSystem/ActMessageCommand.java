package mosApp.MosSystem;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import com.iplanet.jato.*;
import com.iplanet.jato.command.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;

import com.basis100.log.*;
import com.basis100.deal.security.*;

import MosSystem.*;

public class ActMessageCommand extends Object
     implements Command
{
         /**
          *
          *
          */
     public ActMessageCommand()
     {
         super();
     }


     /**
      *
      *
      */
    public void execute(CommandEvent event)
       throws CommandException
    {
      logger = SysLog.getSysLogger("AMCOMND");
      String operationName=event.getOperationName();

      logger.debug("ActMessageCommand@Command invoked.");
      logger.debug("ActMessageCommand@OperationName: " + operationName);

      RequestContext requestContext=event.getRequestContext();

      ViewInvocation invocation=((ViewCommandEvent)event).getInvocation();

      logger.debug("ActMessageCommand@ViewInvocation : " + invocation.toString());

      //--> Not necessary to check this !! -- By BILLY 16July2002
      //if (!event.getOperationName().equals(Command.DEFAULT_OPERATION_NAME))
      //     throw new CommandException("Unkown operation name \""+
      //        event.getOperationName()+"\"");

      //Get the Parameters string
      String parameters = requestContext.getRequest().getParameter(invocation.getQualifiedChildName());

//--> The folowing for Debug purpose only -- By BILLY 15July2002
logger.debug("BILLY ==> The Request Parameter = " + parameters);
logger.debug("BILLY ==> The btActMsg Field value (By getDisplayFieldValue()) = " + ((ViewBean)(invocation.getTargetRequestHandler())).getDisplayFieldValue(invocation.getChildName()));
logger.debug("BILLY ==> The Request Parameter (" + invocation.getQualifiedChildName() + "):: " + requestContext.getRequest().getParameter(invocation.getQualifiedChildName()));
logger.debug("BILLY ==> The Request Parameter (" + invocation.getChildName() + "):: " + requestContext.getRequest().getParameter(invocation.getChildName()));
Enumeration tmpEm = requestContext.getRequest().getParameterNames();
for(int iPara = 0 ; tmpEm.hasMoreElements(); iPara++)
{
  String currPara = tmpEm.nextElement().toString();
  //logger.debug("BILLY ==> The Request ParameterName[" + iPara + "]::" + currPara);
  //logger.debug("BILLY ==> The Request ParameterValue[" + iPara + "]::" + requestContext.getRequest().getParameter(currPara));
}
//==================================================================

    // Try to get the handleActMessageOK(String[] args) metdhod from the Page ViewBean
    Method method=null;

		try
		{
			method=invocation.getTargetRequestHandler().getClass().getMethod(
				ACT_MSG_METHOD_NAME,new Class[] {String[].class});

		}
		catch (NoSuchMethodException e)
		{
			// Method not implemented !!
      String errorMsg = "@ActMessageCommand.execute() :: Method \"handleActMessageOK(String[] args)\" not found @ " + invocation.getTargetRequestHandler();
      logger.error(errorMsg);
      throw new CommandException(errorMsg);
		}

    String [] args = new String [2];
    if(parameters != null)
    {
      // Convert the Parameters String to String array with delimeter = ','
      args = new String [2];
      StringTokenizer tmpStrToken = new StringTokenizer(parameters, ",");

      try{
        args[0] = tmpStrToken.nextToken();
      }catch(Exception e){
        args[0] = "";
      }
      try{
        args[1] = tmpStrToken.nextToken();
      }catch(Exception e){
        args[1] = "";
      }
  logger.debug("ActMessageCommand@execute::args[0]: " + args[0]);
  logger.debug("ActMessageCommand@execute::args[1]: " + args[1]);
     }

      ////Case of handleActMessageOK(@) or handleActMessageOK(^). The latter is more hypothetical
      //// possibility. There is no implementation in the project.
      //Try to invoke the Method
      try{
        method.invoke(invocation.getTargetRequestHandler(),	new Object[] {args});
      }
      //--> Test by Billy 19Dec2002
      catch(InvocationTargetException ie)
      {
        if(ie.getTargetException() instanceof CompleteRequestException)
        {
          logger.warning("ActMessageCommand@execute:: got CompleteRequestException : Most likely user logging out !!");
          throw ((CompleteRequestException)(ie.getTargetException()));
        }
        else
        {
          logger.error("ActMessageCommand@execute:: Handler method \""+ACT_MSG_METHOD_NAME+ "\" not accessible : " + ie.getTargetException().toString());
          throw new CommandException("Handler method \""+ACT_MSG_METHOD_NAME+
          "\" not accessible",ie.getTargetException());
        }
      }
      catch(Exception e)
      {
        logger.error("ActMessageCommand@execute:: Handler method \""+ACT_MSG_METHOD_NAME+ "\" not accessible : " + e.toString());
        throw new CommandException("Handler method \""+ACT_MSG_METHOD_NAME+
          "\" not accessible",e);
      }
  }


   ////////////////////////////////////////////////////////////////////////////////
   // Class variables
   ////////////////////////////////////////////////////////////////////////////////

  public SysLogger logger;

  public static final String ACT_MSG_METHOD_NAME = "handleActMessageOK";
  public static final CommandDescriptor COMMAND_DESCRIPTOR=
    new CommandDescriptor(ActMessageCommand.class, Command.DEFAULT_OPERATION_NAME,null);

   ////////////////////////////////////////////////////////////////////////////////
   // Instance variables
   ////////////////////////////////////////////////////////////////////////////////
}


