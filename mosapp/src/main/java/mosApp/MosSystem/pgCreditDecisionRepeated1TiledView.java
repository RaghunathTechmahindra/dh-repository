package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;

/**
 * <p>
 * Title: pgCreditDecisionRepeated1TiledView
 * </p>
 *
 * <p>
 * Description: ViewBean for pgCreditDecision.jsp & pgCreditDecision_fr.jsp
 * </p>
 *
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 *
 * <p>
 * Company: Filogix Inc.
 * </p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.0
 */
public class pgCreditDecisionRepeated1TiledView
	extends
	RequestHandlingTiledViewBase
	implements TiledView, RequestHandler {

	/**
	 *
	 *
	 */
	public pgCreditDecisionRepeated1TiledView(View parent, String name) {
		super(parent, name);
		setMaxDisplayTiles(4);
		setPrimaryModelClass(doAdjudicationApplicantMainBNCModel.class);
		registerChildren();
		initialize();
	}

	/**
	 *
	 *
	 */
	protected void initialize() {
	}

	// //////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	// //////////////////////////////////////////////////////////////////////////////
	/**
	 * createChild Creates child for JATO framework
	 *
	 * @param name
	 *            String<br>
	 * @return View
	 *
	 */
	protected View createChild(String name) {
		// ***** Change by NBC/PP Implementation Team - Version 1.1 - START
		// *****//
	   if (name.equals(CHILD_HDAPPLICANTID)) {
		   HiddenField child = new HiddenField(this,
											   getdoAdjudicationApplicantMainBNCModel(),
											   CHILD_HDAPPLICANTID,
											   doAdjudicationApplicantMainBNCModel.
											   FIELD_DFBORROWERID,
											   CHILD_HDAPPLICANTID_RESET_VALUE, null);
		   return child;
	   }
	   else if (name.equals(CHILD_HDAPPLICANTCOPYID)) {
		   HiddenField child = new HiddenField(this,
											   getdoAdjudicationApplicantMainBNCModel(),
											   CHILD_HDAPPLICANTCOPYID,
											   doAdjudicationApplicantMainBNCModel.
											   FIELD_DFCOPYID,
											   CHILD_HDAPPLICANTCOPYID_RESET_VALUE, null);
		   return child;
	   }
	   else if (name.equals(CHILD_HDAPPLICANTNUMBER)) {
		   HiddenField child = new HiddenField(this, getDefaultModel(),
											   CHILD_HDAPPLICANTNUMBER,
											   CHILD_HDAPPLICANTNUMBER,
											   CHILD_HDAPPLICANTNUMBER_RESET_VALUE, null);
		   return child;
	   }
	   else if (name.equals(CHILD_HDAPPLICANTRESPONSEID)) {
		   HiddenField child = new HiddenField(this,
			   getdoAdjudicationApplicantResponseBNCModel(),
											   CHILD_HDAPPLICANTRESPONSEID,
			   doAdjudicationApplicantResponseBNCModel.FIELD_DFRESPONSEID,
											   CHILD_HDAPPLICANTRESPONSEID_RESET_VALUE, null);
		   return child;
	   }
	   else if (name.equals(CHILD_STBORROWERNUMBER)) {
		   HiddenField child = new HiddenField(this, getDefaultModel(),
											   CHILD_STBORROWERNUMBER,
											   CHILD_STBORROWERNUMBER,
											   CHILD_STBORROWERNUMBER_RESET_VALUE, null);
		   return child;
	   }
	   else if (name.equals(CHILD_BTAPPLICANTDETAILS)) {
		   Button child = new Button(this, getDefaultModel(),
									 CHILD_BTAPPLICANTDETAILS,
									 CHILD_BTAPPLICANTDETAILS,
									 CHILD_BTAPPLICANTDETAILS_RESET_VALUE, null);
		   return child;
	   }
	   else if (name.equals(CHILD_TXAPPLICANTTYPE)) {
		   TextField child = new TextField(this,
										   getdoAdjudicationApplicantMainBNCModel(),
										   CHILD_TXAPPLICANTTYPE,
										   doAdjudicationApplicantMainBNCModel.
										   FIELD_DFBORROWERTYPE,
										   CHILD_TXAPPLICANTTYPE_RESET_VALUE, null);
		   return child;
	   }
	   else if (name.equals(CHILD_TXAPPLICANTCREDITBUREAU)) {
		   TextField child = new TextField(
			   this,
			   getdoAdjudicationApplicantResponseBNCModel(),
			   CHILD_TXAPPLICANTCREDITBUREAU,
			   doAdjudicationApplicantResponseBNCModel.
			   FIELD_DFUSEDCREDITBUREAUNAME,
			   CHILD_TXAPPLICANTCREDITBUREAU_RESET_VALUE, null);
		   return child;
	   }
	   else if (name.equals(CHILD_TXAPPLICANTCREDITBUREAUSCORE)) {
		   TextField child = new TextField(
			   this,
			   getdoAdjudicationApplicantResponseBNCModel(),
			   CHILD_TXAPPLICANTCREDITBUREAUSCORE,
			   doAdjudicationApplicantResponseBNCModel.
			   FIELD_DFCREDITBUREAUSCORE,
			   CHILD_TXAPPLICANTCREDITBUREAUSCORE_RESET_VALUE, null);
		   return child;
	   }
	   else if (name.equals(CHILD_TXAPPLICANTRISKRATING)) {
		   TextField child = new TextField(this,
										   getdoAdjudicationApplicantResponseBNCModel(),
										   CHILD_TXAPPLICANTRISKRATING,
										   doAdjudicationApplicantResponseBNCModel.
										   FIELD_DFRISKRATING,
										   CHILD_TXAPPLICANTRISKRATING_RESET_VALUE, null);

		   return child;

	   }
	   else if (name.equals(CHILD_TXAPPLICANTINTERNALSCORE)) {
		   TextField child = new TextField(
			   this,
			   getdoAdjudicationApplicantResponseBNCModel(),
			   CHILD_TXAPPLICANTINTERNALSCORE,
			   doAdjudicationApplicantResponseBNCModel.FIELD_DFINTERNALSCORE,
			   CHILD_TXAPPLICANTINTERNALSCORE_RESET_VALUE, null);
		   return child;
	   }
	   else if (name.equals(CHILD_TXAPPLICANTGDS)) {
		   TextField child = new TextField(this,
										   getdoAdjudicationApplicantMainBNCModel(),
										   CHILD_TXAPPLICANTGDS,
										   doAdjudicationApplicantMainBNCModel.
										   FIELD_DFBORROWERGDS,
										   CHILD_TXAPPLICANTGDS_RESET_VALUE, null);
		   return child;
	   }
	   else if (name.equals(CHILD_TXAPPLICANTTDS)) {
		   TextField child = new TextField(this,
										   getdoAdjudicationApplicantMainBNCModel(),
										   CHILD_TXAPPLICANTTDS,
										   doAdjudicationApplicantMainBNCModel.
										   FIELD_DFBORROWERTDS,
										   CHILD_TXAPPLICANTTDS_RESET_VALUE, null);
		   return child;
	   }
	   else if (name.equals(CHILD_TXAPPLICANTNETWORTH)) {
		   TextField child = new TextField(this,
										   getdoAdjudicationApplicantResponseBNCModel(),
										   CHILD_TXAPPLICANTNETWORTH,
										   doAdjudicationApplicantResponseBNCModel.
										   FIELD_DFNETWORTH,
										   CHILD_TXAPPLICANTNETWORTH_RESET_VALUE, null);
		   return child;
	   }
	   else if (name.equals(CHILD_TXAPPLICANTINCOME)) {
		   TextField child = new TextField(this,
			   getdoAdjudicationApplicantMainBNCModel(),
										   CHILD_TXAPPLICANTINCOME,
										   doAdjudicationApplicantMainBNCModel.
										   FIELD_DFANNUALINCOMEAMOUNT,
										   CHILD_TXAPPLICANTINCOME_RESET_VALUE, null);
		   return child;
	   }
	   else if (name.equals(CHILD_TXAPPLICANTOTHERINCOME)) {
		   TextField child = new TextField(this,
			   getdoAdjudicationApplicantMainBNCModel(),
										   CHILD_TXAPPLICANTOTHERINCOME,
										   doAdjudicationApplicantMainBNCModel.
										   FIELD_DFOTHERANNUALINCOME,
										   CHILD_TXAPPLICANTOTHERINCOME_RESET_VALUE, null);
		   return child;
	   }
	   else if (name.equals(CHILD_TXAPPLICANTTOTALINCOME)) {
		   TextField child =
			   new TextField(this, getdoAdjudicationApplicantMainBNCModel(),
							 CHILD_TXAPPLICANTTOTALINCOME,
							 doAdjudicationApplicantMainBNCModel.
							 FIELD_DFTOTALANNUALINCOME,
							 CHILD_TXAPPLICANTTOTALINCOME_RESET_VALUE, null);
		   return child;
	   }
	   else if (name.equals(CHILD_TXAPPLICANTINCOMETYPE)) {
		   TextField child = new TextField(this,
			   getdoAdjudicationApplicantIncomeBNCModel(),
										   CHILD_TXAPPLICANTINCOMETYPE,
			   doAdjudicationApplicantIncomeBNCModel.FIELD_DFINCOMETYPEDESC,
										   CHILD_TXAPPLICANTINCOMETYPE_RESET_VALUE, null);
		   return child;
	   }
	   else if (name.equals(CHILD_TXAPPLICANTMONTHSOFSERVICE)) {
		   TextField child = new TextField(this,
										   getdoAdjudicationApplicantIncomeBNCModel(),
										   CHILD_TXAPPLICANTMONTHSOFSERVICE,
										   doAdjudicationApplicantIncomeBNCModel.
										   FIELD_DFTIMEATEMPLOYER,
										   CHILD_TXAPPLICANTMONTHSOFSERVICE_RESET_VALUE, null);
		   return child;
	   }
	   else if (name.equals(CHILD_CBAPPLICANTREQUESTEDCREDITBUREAU)) {
		   ComboBox child = new ComboBox(this, getDefaultModel(),
										 CHILD_CBAPPLICANTREQUESTEDCREDITBUREAU,
										 CHILD_CBAPPLICANTREQUESTEDCREDITBUREAU,
										 CHILD_CBAPPLICANTREQUESTEDCREDITBUREAU_RESET_VALUE, null);
		   child.setLabelForNoneSelected("");
		   child.setOptions(cbApplicantRequestedCreditBureauOptions);
		   return child;
	   }
	   // ***** Change by NBC/PP Implementation Team - Version 1.1 - END
	   // *****//
	  else {
		  throw new IllegalArgumentException("Invalid child name [" + name
											 + "]");
	  }
	}

	/**
	 * resetChildren Reset children values for the JATO framework
	 *
	 * @param none
	 *            <br>
	 * @return none<br>
	 *
	 */
	public void resetChildren() {
		getHdApplicantId().setValue(CHILD_HDAPPLICANTID_RESET_VALUE);
		getHdApplicantCopyId().setValue(CHILD_HDAPPLICANTCOPYID_RESET_VALUE);
		getHdApplicantNumber().setValue(CHILD_HDAPPLICANTNUMBER_RESET_VALUE);
		getHdApplicantResponseId().setValue(
			CHILD_HDAPPLICANTRESPONSEID_RESET_VALUE);
		getStBorrowerNumber().setValue(CHILD_STBORROWERNUMBER_RESET_VALUE);
		getBtApplicantDetails().setValue(CHILD_BTAPPLICANTDETAILS_RESET_VALUE);
		getTxApplicantType().setValue(CHILD_TXAPPLICANTTYPE_RESET_VALUE);
		getTxApplicantCreditBureau().setValue(
			CHILD_TXAPPLICANTCREDITBUREAU_RESET_VALUE);
		getTxApplicantCreditBureauScore().setValue(
			CHILD_TXAPPLICANTCREDITBUREAUSCORE_RESET_VALUE);
		getTxApplicantRiskRating().setValue(
			CHILD_TXAPPLICANTRISKRATING_RESET_VALUE);
		getTxApplicantInternalScore().setValue(
			CHILD_TXAPPLICANTINTERNALSCORE_RESET_VALUE);
		getTxApplicantGDS().setValue(CHILD_TXAPPLICANTGDS_RESET_VALUE);
		getTxApplicantTDS().setValue(CHILD_TXAPPLICANTTDS_RESET_VALUE);
		getTxApplicantNetWorth()
			.setValue(CHILD_TXAPPLICANTNETWORTH_RESET_VALUE);
		getTxApplicantIncome().setValue(CHILD_TXAPPLICANTINCOME_RESET_VALUE);
		getTxApplicantOtherIncome().setValue(
			CHILD_TXAPPLICANTOTHERINCOME_RESET_VALUE);
		getTxApplicantTotalIncome().setValue(
			CHILD_TXAPPLICANTTOTALINCOME_RESET_VALUE);
		getTxApplicantIncomeType().setValue(
			CHILD_TXAPPLICANTINCOMETYPE_RESET_VALUE);
		getTxApplicantMonthsOfService().setValue(
			CHILD_TXAPPLICANTMONTHSOFSERVICE_RESET_VALUE);
		getCbApplicantRequestedCreditBureau().setValue(
			CHILD_CBAPPLICANTREQUESTEDCREDITBUREAU_RESET_VALUE);
		// -- end Credit Decision fields --//

	}

	/**
	 * registerChildren Register children with the JATO framework
	 *
	 * @param none
	 *            <br>
	 * @return none<br>
	 *
	 */
	protected void registerChildren() {
		registerChild(CHILD_HDAPPLICANTID, HiddenField.class);
		registerChild(CHILD_HDAPPLICANTCOPYID, HiddenField.class);
		registerChild(CHILD_HDAPPLICANTNUMBER, HiddenField.class);
		registerChild(CHILD_HDAPPLICANTRESPONSEID, HiddenField.class);
		registerChild(CHILD_STBORROWERNUMBER, StaticTextField.class);
		registerChild(CHILD_BTAPPLICANTDETAILS, Button.class);
		registerChild(CHILD_TXAPPLICANTTYPE, TextField.class);
		registerChild(CHILD_TXAPPLICANTCREDITBUREAU, TextField.class);
		registerChild(CHILD_TXAPPLICANTCREDITBUREAUSCORE, TextField.class);
		registerChild(CHILD_TXAPPLICANTRISKRATING, TextField.class);
		registerChild(CHILD_TXAPPLICANTINTERNALSCORE, TextField.class);
		registerChild(CHILD_TXAPPLICANTGDS, TextField.class);
		registerChild(CHILD_TXAPPLICANTTDS, TextField.class);
		registerChild(CHILD_TXAPPLICANTNETWORTH, TextField.class);
		registerChild(CHILD_TXAPPLICANTINCOME, TextField.class);
		registerChild(CHILD_TXAPPLICANTOTHERINCOME, TextField.class);
		registerChild(CHILD_TXAPPLICANTINCOMETYPE, TextField.class);
		registerChild(CHILD_TXAPPLICANTMONTHSOFSERVICE, TextField.class);
		registerChild(CHILD_CBAPPLICANTREQUESTEDCREDITBUREAU, ComboBox.class);
		// -- end Credit Decision specific fields --//

	}

	// //////////////////////////////////////////////////////////////////////////////
	// Model management methods
	// //////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType) {
		List modelList = new ArrayList();
		switch (executionType) {
			case MODEL_TYPE_RETRIEVE:

				// // Put your own additional Models if necessary.
				modelList.add(getdoAdjudicationApplicantMainBNCModel());
				;
				
				// ****** NBC/PP Implementation Team - Fix for defect 1626 - Start ****** //
				modelList.add(getdoAdjudicationApplicantResponseBNCModel());
				modelList.add(getdoAdjudicationApplicantIncomeBNCModel());;
				// ****** NBC/PP Implementation Team - Fix for defect 1626 - End ****** //
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[]) modelList.toArray(new Model[0]);
	}

	// //////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	// //////////////////////////////////////////////////////////////////////////////
	/**
	 * beginDisplay
	 *
	 * @param event
	 *            Display Event <br>
	 * @return none
	 *
	 * @version 1.1 <br>
	 *          Date: 07/26/2006<br>
	 *          Author: NBC/PP Implementation Team <br>
	 *          Change: <br>
	 *          Added population of credit bureau option lists
	 *
	 */
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event) throws ModelControlException {

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) {
			throw new ModelControlException("Primary model is null");
		}

		cbApplicantRequestedCreditBureauOptions.populate(getRequestContext());

		super.beginDisplay(event);
		resetTileIndex();
	}

	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext) {

		// This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}

	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext) {

		// This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}

	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext) {
		// This is the analog of NetDynamics
		// this_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}

	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext,
							 ModelControlException exception) throws
		ModelControlException {

		// This is the analog of NetDynamics this_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}

	/**
	 *
	 *
	 */
	public void setdoAdjudicationApplicantResponseBNCModel(
		doAdjudicationApplicantResponseBNCModel model) {
		doAdjudicationApplicantResponseBNC = model;
	}

	/**
	 *
	 *
	 */
	public doAdjudicationApplicantResponseBNCModel
		getdoAdjudicationApplicantResponseBNCModel() {
		if (doAdjudicationApplicantResponseBNC == null) {
			doAdjudicationApplicantResponseBNC = (
				doAdjudicationApplicantResponseBNCModel) getModel(
				doAdjudicationApplicantResponseBNCModel.class);
		}
		return doAdjudicationApplicantResponseBNC;
	}

	/**
	 *
	 *
	 */
	public void setdoAdjudicationApplicantMainBNCModel(
		doAdjudicationApplicantMainBNCModel model) {
		doAdjudicationApplicantMainBNC = model;
	}

	/**
	 *
	 *
	 */
	public doAdjudicationApplicantMainBNCModel
		getdoAdjudicationApplicantMainBNCModel() {
		if (doAdjudicationApplicantMainBNC == null) {
			doAdjudicationApplicantMainBNC = (
				doAdjudicationApplicantMainBNCModel) getModel(
				doAdjudicationApplicantMainBNCModel.class);
		}
		return doAdjudicationApplicantMainBNC;
	}

	/**
	 *
	 *
	 */
	public void setdoAdjudicationApplicantIncomeBNCModel(
		doAdjudicationApplicantIncomeBNCModel model) {
		doAdjudicationApplicantBNCIncome = model;
	}

	/**
	 *
	 *
	 */
	public doAdjudicationApplicantIncomeBNCModel
		getdoAdjudicationApplicantIncomeBNCModel() {
		if (doAdjudicationApplicantBNCIncome == null) {
			doAdjudicationApplicantBNCIncome = (
				doAdjudicationApplicantIncomeBNCModel) getModel(
				doAdjudicationApplicantIncomeBNCModel.class);
		}
		return doAdjudicationApplicantBNCIncome;
	}

	/**
	 *
	 *
	 */
	public boolean nextTile() throws ModelControlException {

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		// set applicant number
		int rowNum = this.getTileIndex();

		setDisplayFieldValue("hdApplicantNumber", String.valueOf(rowNum));

		// SCR #1518 - start
		CreditDecisionHandler handler = (CreditDecisionHandler)this.handler
			.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		int languageId = handler.getTheSessionState().getLanguageId();

		switch (rowNum) {
			case 0:
				setDisplayFieldValue("stBorrowerNumber",
									 BXResources.
									 getGenericMsg("GCD_BORROWER_LABEL_1",
					languageId));
				break;
			case 1:
				setDisplayFieldValue("stBorrowerNumber",
									 BXResources.
									 getGenericMsg("GCD_BORROWER_LABEL_2",
					languageId));
				break;
			case 2:
				setDisplayFieldValue("stBorrowerNumber",
									 BXResources.
									 getGenericMsg("GCD_BORROWER_LABEL_3",
					languageId));
				break;
			case 3:
				setDisplayFieldValue("stBorrowerNumber",
									 BXResources.
									 getGenericMsg("GCD_BORROWER_LABEL_4",
					languageId));
				break;
		}
		// SCR #1518 - end

		if (movedToRow) {
			// Get Data Object
			doAdjudicationApplicantResponseBNCModelImpl theObj1 = (
				doAdjudicationApplicantResponseBNCModelImpl)
				getdoAdjudicationApplicantResponseBNCModel();

			if (theObj1.getNumRows() > 0) {
				// Increment the location of the model
				theObj1.next();
			}
			// Get Data Object
			doAdjudicationApplicantIncomeBNCModelImpl theObj2 = (
				doAdjudicationApplicantIncomeBNCModelImpl)
				getdoAdjudicationApplicantIncomeBNCModel();

			if (theObj2.getNumRows() > 0) {
				// Increment the location of the model
				theObj2.next();
			}
		}

		return movedToRow;
	}

	/**
	 *
	 *
	 */
	public TextField getTbDealId() {
		return (TextField) getChild(CHILD_TBDEALID);
	}

	/**
	 * <p>
	 * Title: CbRequestedCreditBureauOptionsList
	 * </p>
	 *
	 * <p>
	 * Description: Options list for requested credit bureau
	 * </p>
	 *
	 * @author NBC/PP Implementation Team
	 * @version 1.0
	 */
	class CbRequestedCreditBureauOptionsList
		extends OptionList {
		CbRequestedCreditBureauOptionsList() {
		}

		public void populate(RequestContext rc) {
			try {
				// Get the Language ID from the SessionStateModel
				String defaultInstanceStateName = rc.getModelManager()
					.getDefaultModelInstanceName(SessionStateModel.class);
				SessionStateModelImpl theSessionState = (SessionStateModelImpl)
					rc
					.getModelManager().getModel(SessionStateModel.class,
												defaultInstanceStateName, true);

				int languageId = theSessionState.getLanguageId();

				// Get IDs and Labels from BXResources
		        Collection c = BXResources.getPickListValuesAndDesc(
		                theSessionState.getDealInstitutionId(),"CREDITBUREAUNAME", languageId);

				Iterator l = c.iterator();
				String[] theVal = new String[2];

				while (l.hasNext()) {
					theVal = (String[]) (l.next());
					add(theVal[1], theVal[0]);
				}

			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public HiddenField getHdApplicantId() {
		return (HiddenField) getChild(CHILD_HDAPPLICANTID);
	}

	/**
	 * getHdApplicantCopyId Returns the <code>hdApplicantCopyId</code> child
	 * View component
	 *
	 * @version 1.0 (Initial Version - 1 Aug 2006)<br>
	 */

	public HiddenField getHdApplicantCopyId() {
		return (HiddenField) getChild(CHILD_HDAPPLICANTCOPYID);
	}

	/**
	 * getHdApplicantNumber Returns the <code>hdApplicantNumber</code> child
	 * View component
	 *
	 * @version 1.0 (Initial Version - 1 Aug 2006)<br>
	 */

	public HiddenField getHdApplicantNumber() {
		return (HiddenField) getChild(CHILD_HDAPPLICANTNUMBER);
	}

	/**
	 * getHdApplicantResponseId Returns the <code>hdApplicantNumber</code> child
	 * View component
	 *
	 * @version 1.0 (Initial Version - 1 Aug 2006)<br>
	 */

	public HiddenField getHdApplicantResponseId() {
		return (HiddenField) getChild(CHILD_HDAPPLICANTRESPONSEID);
	}

	/**
	 * getStBorrowerNumber Returns the <code>StBorrowerNumber</code> child
	 * View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */

	public StaticTextField getStBorrowerNumber() {
		return (StaticTextField) getChild(CHILD_STBORROWERNUMBER);
	}

	/**
	 * getBtApplicantDetails Returns the <code>btApplicantDetails</code> child
	 * View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */

	public Button getBtApplicantDetails() {
		return (Button) getChild(CHILD_BTAPPLICANTDETAILS);
	}

	/**
	 * endBtApplicantDetailsDisplay Hides Details button for first applicant
	 *
	 * @param event
	 * @return
	 */
	public String endBtApplicantDetailsDisplay(ChildContentDisplayEvent event) {
		CreditDecisionHandler handler = (CreditDecisionHandler)this.handler
			.cloneSS(); //
		handler.pageGetState(this.getParentViewBean());

		boolean rc = true; // default: generate.

		rc = handler.handleViewRemoveDetailsButton();
		handler.pageSaveState();

		if (rc == true) {
			return event.getContent();
		}
		else {
			return "";
		}
	}

	/**
	 * handleBtApplicantDetailsRequest Handles the
	 * <code>btApplicantDetails</code> request
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public void handleBtApplicantDetailsRequest(RequestInvocationEvent event) throws
		ServletException, IOException {

		CreditDecisionHandler handler = (CreditDecisionHandler)this.handler
			.cloneSS();

		handler.preHandlerProtocol(this.getParentViewBean());
		handler.handleApplicantDetails( ( (TiledViewRequestInvocationEvent) (
			event)).getTileNumber());
		handler.postHandlerProtocol();

	}

	/**
	 * getTxApplicantTypeChild Returns the <code>txApplicantType</code> child
	 * View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxApplicantType() {
		return (TextField) getChild(CHILD_TXAPPLICANTTYPE);
	}

	/**
	 * getTxApplicantCreditBureauChild Returns the
	 * <code>txApplicantCreditBureau</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxApplicantCreditBureau() {
		return (TextField) getChild(CHILD_TXAPPLICANTCREDITBUREAU);
	}

	/**
	 * getTxApplicantCreditBureauScoreChild Returns the
	 * <code>txApplicantCreditBureauScore</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxApplicantCreditBureauScore() {
		return (TextField) getChild(CHILD_TXAPPLICANTCREDITBUREAUSCORE);
	}

	/**
	 * getTxApplicantRiskRatingChild Returns the
	 * <code>txApplicantRiskRating</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxApplicantRiskRating() {
		return (TextField) getChild(CHILD_TXAPPLICANTRISKRATING);
	}

	/**
	 * getTxApplicantInternalScoreChild Returns the
	 * <code>txApplicantInternalScore</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxApplicantInternalScore() {
		return (TextField) getChild(CHILD_TXAPPLICANTINTERNALSCORE);
	}

	/**
	 * getTxApplicantGDSChild Returns the <code>txApplicantGDS</code> child
	 * View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxApplicantGDS() {
		return (TextField) getChild(CHILD_TXAPPLICANTGDS);
	}

	/**
	 * getTxApplicantTDSChild Returns the <code>txApplicantTDS</code> child
	 * View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxApplicantTDS() {
		return (TextField) getChild(CHILD_TXAPPLICANTTDS);
	}

	/**
	 * getTxApplicantNetWorthChild Returns the <code>txApplicantNetWorth</code>
	 * child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxApplicantNetWorth() {
		return (TextField) getChild(CHILD_TXAPPLICANTNETWORTH);
	}

	/**
	 * getTxApplicantIncomeChild Returns the <code>txApplicantIncome</code>
	 * child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxApplicantIncome() {
		return (TextField) getChild(CHILD_TXAPPLICANTINCOME);
	}

	/**
	 * getTxApplicantOtherIncomeChild Returns the
	 * <code>txApplicantOtherIncome</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxApplicantOtherIncome() {
		return (TextField) getChild(CHILD_TXAPPLICANTOTHERINCOME);
	}

	/**
	 * getTxApplicantTotalIncomeChild Returns the
	 * <code>txApplicantTotalIncome</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxApplicantTotalIncome() {
		return (TextField) getChild(CHILD_TXAPPLICANTTOTALINCOME);
	}

	/**
	 * getTxApplicantIncomeTypeChild Returns the
	 * <code>txApplicantIncomeType</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxApplicantIncomeType() {
		return (TextField) getChild(CHILD_TXAPPLICANTINCOMETYPE);
	}

	/**
	 * getTxApplicantMonthsOfServiceChild Returns the
	 * <code>txApplicantMonthsOfService</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxApplicantMonthsOfService() {
		return (TextField) getChild(CHILD_TXAPPLICANTMONTHSOFSERVICE);
	}

	/**
	 * getCbApplicantRequestedCreditBureauChild Returns the
	 * <code>cbApplicantRequestedCreditBureau</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public ComboBox getCbApplicantRequestedCreditBureau() {
		return (ComboBox) getChild(CHILD_CBAPPLICANTREQUESTEDCREDITBUREAU);
	}

	// //////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	// //////////////////////////////////////////////////////////////////////////

	// //////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	// //////////////////////////////////////////////////////////////////////////

	// ]]SPIDER_EVENT<btNextTaskPage_onWebEvent>

	// //////////////////////////////////////////////////////////////////////////
	// Class variables
	// //////////////////////////////////////////////////////////////////////////

	// //////////////////////////////////////////////////////////////////////////////
	// Class variables
	// //////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;

	public static final String CHILD_TBDEALID = "tbDealId";

	public static final String CHILD_TBDEALID_RESET_VALUE = "";

	// -- Credit Decision specific fields --//
	public static final String CHILD_HDAPPLICANTID = "hdApplicantId";

	public static final String CHILD_HDAPPLICANTID_RESET_VALUE = "";

	public static final String CHILD_HDAPPLICANTCOPYID = "hdApplicantCopyId";

	public static final String CHILD_HDAPPLICANTCOPYID_RESET_VALUE = "";

	public static final String CHILD_HDAPPLICANTNUMBER = "hdApplicantNumber";

	public static final String CHILD_HDAPPLICANTNUMBER_RESET_VALUE = "";

	public static final String CHILD_HDAPPLICANTRESPONSEID =
		"hdApplicantResponseId";

	public static final String CHILD_HDAPPLICANTRESPONSEID_RESET_VALUE = "";

	public static final String CHILD_STBORROWERNUMBER = "stBorrowerNumber";

	public static final String CHILD_STBORROWERNUMBER_RESET_VALUE = "";

	public static final String CHILD_BTAPPLICANTDETAILS = "btApplicantDetails";

	public static final String CHILD_BTAPPLICANTDETAILS_RESET_VALUE = " ";

	public static final String CHILD_TXAPPLICANTTYPE = "txApplicantType";

	public static final String CHILD_TXAPPLICANTTYPE_RESET_VALUE = "";

	public static final String CHILD_TXAPPLICANTCREDITBUREAU =
		"txApplicantCreditBureau";

	public static final String CHILD_TXAPPLICANTCREDITBUREAU_RESET_VALUE = "";

	public static final String CHILD_TXAPPLICANTCREDITBUREAUSCORE =
		"txApplicantCreditBureauScore";

	public static final String CHILD_TXAPPLICANTCREDITBUREAUSCORE_RESET_VALUE =
		"";

	public static final String CHILD_TXAPPLICANTRISKRATING =
		"txApplicantRiskRating";

	public static final String CHILD_TXAPPLICANTRISKRATING_RESET_VALUE = "";

	public static final String CHILD_TXAPPLICANTINTERNALSCORE =
		"txApplicantInternalScore";

	public static final String CHILD_TXAPPLICANTINTERNALSCORE_RESET_VALUE = "";

	public static final String CHILD_TXAPPLICANTGDS = "txApplicantGDS";

	public static final String CHILD_TXAPPLICANTGDS_RESET_VALUE = "";

	public static final String CHILD_TXAPPLICANTTDS = "txApplicantTDS";

	public static final String CHILD_TXAPPLICANTTDS_RESET_VALUE = "";

	public static final String CHILD_TXAPPLICANTNETWORTH =
		"txApplicantNetWorth";

	public static final String CHILD_TXAPPLICANTNETWORTH_RESET_VALUE = "";

	public static final String CHILD_TXAPPLICANTINCOME = "txApplicantIncome";

	public static final String CHILD_TXAPPLICANTINCOME_RESET_VALUE = "";

	public static final String CHILD_TXAPPLICANTOTHERINCOME =
		"txApplicantOtherIncome";

	public static final String CHILD_TXAPPLICANTOTHERINCOME_RESET_VALUE = "";

	public static final String CHILD_TXAPPLICANTTOTALINCOME =
		"txApplicantTotalIncome";

	public static final String CHILD_TXAPPLICANTTOTALINCOME_RESET_VALUE = "";

	public static final String CHILD_TXAPPLICANTINCOMETYPE =
		"txApplicantIncomeType";

	public static final String CHILD_TXAPPLICANTINCOMETYPE_RESET_VALUE = "";

	public static final String CHILD_TXAPPLICANTMONTHSOFSERVICE =
		"txApplicantMonthsOfService";

	public static final String CHILD_TXAPPLICANTMONTHSOFSERVICE_RESET_VALUE =
		"";

	public static final String CHILD_CBAPPLICANTREQUESTEDCREDITBUREAU =
		"cbApplicantRequestedCreditBureau";

	public static final String
		CHILD_CBAPPLICANTREQUESTEDCREDITBUREAU_RESET_VALUE = "";

	private CbRequestedCreditBureauOptionsList
		cbApplicantRequestedCreditBureauOptions = new
		CbRequestedCreditBureauOptionsList();

	// -- end Credit Decision specific fields --//

	// //////////////////////////////////////////////////////////////////////////
	// Instance variables
	// //////////////////////////////////////////////////////////////////////////

	private doAdjudicationApplicantResponseBNCModel
		doAdjudicationApplicantResponseBNC = null;

	private doAdjudicationApplicantMainBNCModel doAdjudicationApplicantMainBNC = null;

	private doAdjudicationApplicantIncomeBNCModel
		doAdjudicationApplicantBNCIncome = null;

	// //////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	// //////////////////////////////////////////////////////////////////////////

	private CreditDecisionHandler handler = new CreditDecisionHandler();

	public SysLogger logger;

}
