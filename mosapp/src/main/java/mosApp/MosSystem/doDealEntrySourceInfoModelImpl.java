package mosApp.MosSystem;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

import java.sql.SQLException;


/**
 *
 *
 *
 */
public class doDealEntrySourceInfoModelImpl extends QueryModelBase
  implements doDealEntrySourceInfoModel
{
  /**
   * MetaData object test method to verify the SYNTHETIC new
   * field for the reogranized SQL_TEMPLATE query.
   *
   */
  /**
     public void setResultSet(ResultSet value)
     {
       logger = SysLog.getSysLogger("METADATA");
       try
       {
          super.setResultSet(value);
          if( value != null)
          {
              ResultSetMetaData metaData = value.getMetaData();
              int columnCount = metaData.getColumnCount();
              logger.debug("===============================================");
                         logger.debug("Testing MetaData Object for new synthetic fields for" +
                                " doDealEntrySourceInfoModel");
              logger.debug("NumberColumns: " + columnCount);
              for(int i = 0; i < columnCount ; i++)
               {
                    logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
               }
               logger.debug("================================================");
           }
       }
       catch (SQLException e)
       {
             logger.debug("Class: " + getClass().getName());
                     logger.debug("SQLException: " + e);
       }
     }
   **/

  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////
  // Custom Methods - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////
  //public SysLogger logger;
  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////
  public static final String DATA_SOURCE_NAME = "jdbc/orcl";

  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.
  ////public static final String SELECT_SQL_TEMPLATE="SELECT ALL SOURCEOFBUSINESSPROFILE.SOBPSHORTNAME, SOURCEFIRMPROFILE.SOURCEFIRMNAME, ADDR.ADDRESSLINE1, ADDR.CITY, CONTACT.CONTACTFAXNUMBER, CONTACT.CONTACTPHONENUMBER, PROVINCE.PROVINCEABBREVIATION, DEAL.DEALID, DEAL.COPYID, ADDR.ADDRESSLINE1 || ' ' || ADDR.CITY || ' ' || PROVINCE.PROVINCEABBREVIATION , CONTACT.CONTACTPHONENUMBEREXTENSION, CONTACT.CONTACTFIRSTNAME, CONTACT.CONTACTLASTNAME FROM DEAL, SOURCEFIRMPROFILE, SOURCEOFBUSINESSPROFILE, SOURCETOFIRMASSOC, PROVINCE, CONTACT, ADDR  __WHERE__  ";
  //	//---------------- FXLink Phase II ------------------//
  //	//--> Modified to use SOB.sourceFirmProfileId instead of SOURCETOFIRMASSOC table
  //	//--> By Billy 17Nov2003
  //	public static final String SELECT_SQL_TEMPLATE =
  //		"SELECT ALL SOURCEOFBUSINESSPROFILE.SOBPSHORTNAME, SOURCEFIRMPROFILE.SOURCEFIRMNAME, "
  //			+ "ADDR.ADDRESSLINE1, ADDR.CITY, CONTACT.CONTACTFAXNUMBER, CONTACT.CONTACTPHONENUMBER, "
  //			+ "PROVINCE.PROVINCEABBREVIATION, DEAL.DEALID, DEAL.COPYID, "
  //			+ "ADDR.ADDRESSLINE1 || ' ' || ADDR.CITY || ' ' || PROVINCE.PROVINCEABBREVIATION SYNTETIC_ADDRESS, "
  //			+ "CONTACT.CONTACTPHONENUMBEREXTENSION, CONTACT.CONTACTFIRSTNAME, "
  //			+ "CONTACT.CONTACTLASTNAME "
  //			+ "FROM DEAL, SOURCEFIRMPROFILE, SOURCEOFBUSINESSPROFILE, "
  //			+ "PROVINCE, CONTACT, ADDR  "
  //			+ "__WHERE__  ";
  //	public static final String MODIFYING_QUERY_TABLE_NAME =
  //		"DEAL, SOURCEFIRMPROFILE, SOURCEOFBUSINESSPROFILE, "
  //			+ "PROVINCE, CONTACT, ADDR";
  //	public static final String STATIC_WHERE_CRITERIA =
  //		" (DEAL.SOURCEFIRMPROFILEID  =  SOURCEFIRMPROFILE.SOURCEFIRMPROFILEID) AND "
  //			+ "(DEAL.SOURCEOFBUSINESSPROFILEID  =  SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPROFILEID) AND "
  //			+ 
  //		//--> Confirm if the Frim is associated with SOB
  //	"(DEAL.SOURCEFIRMPROFILEID  =  SOURCEOFBUSINESSPROFILE.SOURCEFIRMPROFILEID) AND "
  //		+ "(PROVINCE.PROVINCEID  =  ADDR.PROVINCEID) AND "
  //		+ "(CONTACT.ADDRID  =  ADDR.ADDRID) AND "
  //		+ "(CONTACT.CONTACTID  =  SOURCEOFBUSINESSPROFILE.CONTACTID)";
  //	//====================================================
  //-- ========== DJ#725 begins ========== --//
  //-- By Neil : Nov/30/2004
  //-- Modified to add new fields : 
  //-- contactEmailAddress, psDescription, systemTypeDescription 
  public static final String SELECT_SQL_TEMPLATE =
    "SELECT distinct" + " SOURCEOFBUSINESSPROFILE.SOBPSHORTNAME,"
    + " SOURCEFIRMPROFILE.SOURCEFIRMNAME,"
    + " ADDR.ADDRESSLINE1,"
    + " ADDR.CITY," + " CONTACT.CONTACTFAXNUMBER,"
    + " CONTACT.CONTACTPHONENUMBER,"
    + " CONTACT.CONTACTEMAILADDRESS,"  // new field
    + " CONTACT.CONTACTPHONENUMBEREXTENSION,"
    + " CONTACT.CONTACTFIRSTNAME," + " CONTACT.CONTACTLASTNAME,"
    + " PROVINCE.PROVINCEABBREVIATION," + " DEAL.DEALID,"
    + " DEAL.COPYID,"
    + " ADDR.ADDRESSLINE1 || ' ' || ADDR.CITY || ' ' || PROVINCE.PROVINCEABBREVIATION SYNTETIC_ADDRESS,"
    + " PROFILESTATUS.PSDESCRIPTION,"  // new field
    /***** FXP23815 change add profilestatusid *****/
    + " to_char(PROFILESTATUS.PROFILESTATUSID) PROFILESTATUSID_STR," 
    /***** FXP23815 change *****/
    + " SYSTEMTYPE.SYSTEMTYPEDESCRIPTION"  // new field
    + " FROM" + " DEAL," + " SOURCEFIRMPROFILE,"
    + " SOURCEOFBUSINESSPROFILE," + " PROVINCE," + " CONTACT,"
    + " ADDR," + " PROFILESTATUS," + " SYSTEMTYPE" + " __WHERE__  ";
  public static final String MODIFYING_QUERY_TABLE_NAME =
    "DEAL," + " SOURCEFIRMPROFILE," + " SOURCEOFBUSINESSPROFILE,"
    + " PROVINCE," + " CONTACT," + " ADDR," + " PROFILESTATUS,"
    + " SYSTEMTYPE";
  public static final String STATIC_WHERE_CRITERIA =
  " deal.institutionprofileid = sourcefirmprofile.institutionprofileid " +
  " AND deal.sourcefirmprofileid=sourcefirmprofile.sourcefirmprofileid " +
  " AND deal.institutionprofileid=sourceofbusinessprofile.institutionprofileid " +
  " AND deal.sourceofbusinessprofileid = sourceofbusinessprofile.sourceofbusinessprofileid " +
  " AND deal.sourcefirmprofileid=sourceofbusinessprofile.sourcefirmprofileid " +
  " AND sourceofbusinessprofile.institutionprofileid=contact.institutionprofileid " +
  " AND sourceofbusinessprofile.contactid = contact.contactid " +
  " AND contact.institutionprofileid = addr.institutionprofileid " +
  " AND contact.addrid = addr.addrid " +
  " AND province.provinceid = addr.provinceid " +
  " AND profilestatus.profilestatusid=sourceofbusinessprofile.profilestatusid " +
  " AND deal.institutionprofileid = systemtype.institutionprofileid " +
  " AND systemtype.systemtypeid = deal.systemtypeid " ;
  
  //-- ========== DJ#725 ends ========== --//
  public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();
  public static final String QUALIFIED_COLUMN_DFSOBPSHORTNAME =
    "SOURCEOFBUSINESSPROFILE.SOBPSHORTNAME";
  public static final String COLUMN_DFSOBPSHORTNAME = "SOBPSHORTNAME";
  public static final String QUALIFIED_COLUMN_DFSOURCEFIRMNAME =
    "SOURCEFIRMPROFILE.SOURCEFIRMNAME";
  public static final String COLUMN_DFSOURCEFIRMNAME = "SOURCEFIRMNAME";
  public static final String QUALIFIED_COLUMN_DFADDRESSLINE1 =
    "ADDR.ADDRESSLINE1";
  public static final String COLUMN_DFADDRESSLINE1 = "ADDRESSLINE1";
  public static final String QUALIFIED_COLUMN_DFCITY = "ADDR.CITY";
  public static final String COLUMN_DFCITY = "CITY";
  public static final String QUALIFIED_COLUMN_DFCONTACTFAXNUMBER =
    "CONTACT.CONTACTFAXNUMBER";
  public static final String COLUMN_DFCONTACTFAXNUMBER = "CONTACTFAXNUMBER";
  public static final String QUALIFIED_COLUMN_DFCONTACTPHONENUMBER =
    "CONTACT.CONTACTPHONENUMBER";
  public static final String COLUMN_DFCONTACTPHONENUMBER = "CONTACTPHONENUMBER";
  public static final String QUALIFIED_COLUMN_DFPROVINCEABBR =
    "PROVINCE.PROVINCEABBREVIATION";
  public static final String COLUMN_DFPROVINCEABBR = "PROVINCEABBREVIATION";
  public static final String QUALIFIED_COLUMN_DFDEALID = "DEAL.DEALID";
  public static final String COLUMN_DFDEALID = "DEALID";
  public static final String QUALIFIED_COLUMN_DFCOPYID = "DEAL.COPYID";
  public static final String COLUMN_DFCOPYID = "COPYID";

  ////public static final String QUALIFIED_COLUMN_DFFULLADDRESS=".";
  ////public static final String COLUMN_DFFULLADDRESS="";
  public static final String QUALIFIED_COLUMN_DFFULLADDRESS =
    "ADDR.SYNTETIC_ADDRESS";
  public static final String COLUMN_DFFULLADDRESS = "SYNTETIC_ADDRESS";
  public static final String QUALIFIED_COLUMN_DFCONTACTPHONEEXTENSION =
    "CONTACT.CONTACTPHONENUMBEREXTENSION";
  public static final String COLUMN_DFCONTACTPHONEEXTENSION =
    "CONTACTPHONENUMBEREXTENSION";
  public static final String QUALIFIED_COLUMN_DFSOURCEFIRSTNAME =
    "CONTACT.CONTACTFIRSTNAME";
  public static final String COLUMN_DFSOURCEFIRSTNAME = "CONTACTFIRSTNAME";
  public static final String QUALIFIED_COLUMN_DFSOURCELASTNAME =
    "CONTACT.CONTACTLASTNAME";
  public static final String COLUMN_DFSOURCELASTNAME = "CONTACTLASTNAME";
  
  //-- ========== DJ#725: Begins ========== --//
  //-- By Neil : Nov/30/2004
  // E-mail
  public static final String COLUMN_DFCONTACTEMAILADDRESS =
    "CONTACTEMAILADDRESS";
  public static final String QUALIFIED_COLUMN_DFCONTACTEMAILADDRESS =
    "CONTACT.CONTACTEMAILADDRESS";

  // Status
  public static final String COLUMN_DFPSDESCRIPTION = "PSDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFPSDESCRIPTION =
    "PROFILESTATUS.PSDESCRIPTION";

  /***** FXP23815 change add profilestatusid *****/
  public static final String COLUMN_DFPROFILESTATUSID = "PROFILESTATUSID_STR";
  public static final String QUALIFIED_COLUMN_DFPROFILESTATUSID = "PROFILESTATUS.PROFILESTATUSID_STR";
  /***** FXP23815 change add profilestatusid *****/
  
  // System Type
  public static final String COLUMN_DFSYSTEMTYPEDESCRIPTION =
    "SYSTEMTYPEDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFSYSTEMTYPEDESCRIPTION =
    "SYSTEMTYPE.SYSTEMTYPEDESCRIPTION";

  //-- ========== DJ#725: Ends ========== --//
  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////
  // Custom Members - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////
  static
  {
    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(FIELD_DFSOBPSHORTNAME,
                                                             COLUMN_DFSOBPSHORTNAME,
                                                             QUALIFIED_COLUMN_DFSOBPSHORTNAME,
                                                             String.class,
                                                             false, false,
                                                             QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                                                             "",
                                                             QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                                                             ""));

    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(FIELD_DFSOURCEFIRMNAME,
                                                             COLUMN_DFSOURCEFIRMNAME,
                                                             QUALIFIED_COLUMN_DFSOURCEFIRMNAME,
                                                             String.class,
                                                             false, false,
                                                             QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                                                             "",
                                                             QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                                                             ""));

    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(FIELD_DFADDRESSLINE1,
                                                             COLUMN_DFADDRESSLINE1,
                                                             QUALIFIED_COLUMN_DFADDRESSLINE1,
                                                             String.class,
                                                             false, false,
                                                             QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                                                             "",
                                                             QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                                                             ""));

    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(FIELD_DFCITY,
                                                             COLUMN_DFCITY,
                                                             QUALIFIED_COLUMN_DFCITY,
                                                             String.class,
                                                             false, false,
                                                             QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                                                             "",
                                                             QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                                                             ""));

    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(FIELD_DFCONTACTFAXNUMBER,
                                                             COLUMN_DFCONTACTFAXNUMBER,
                                                             QUALIFIED_COLUMN_DFCONTACTFAXNUMBER,
                                                             String.class,
                                                             false, false,
                                                             QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                                                             "",
                                                             QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                                                             ""));

    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(FIELD_DFCONTACTPHONENUMBER,
                                                             COLUMN_DFCONTACTPHONENUMBER,
                                                             QUALIFIED_COLUMN_DFCONTACTPHONENUMBER,
                                                             String.class,
                                                             false, false,
                                                             QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                                                             "",
                                                             QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                                                             ""));

    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(FIELD_DFPROVINCEABBR,
                                                             COLUMN_DFPROVINCEABBR,
                                                             QUALIFIED_COLUMN_DFPROVINCEABBR,
                                                             String.class,
                                                             false, false,
                                                             QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                                                             "",
                                                             QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                                                             ""));

    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(FIELD_DFDEALID,
                                                             COLUMN_DFDEALID,
                                                             QUALIFIED_COLUMN_DFDEALID,
                                                             java.math.BigDecimal.class,
                                                             false, false,
                                                             QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                                                             "",
                                                             QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                                                             ""));

    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(FIELD_DFCOPYID,
                                                             COLUMN_DFCOPYID,
                                                             QUALIFIED_COLUMN_DFCOPYID,
                                                             java.math.BigDecimal.class,
                                                             false, false,
                                                             QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                                                             "",
                                                             QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                                                             ""));

    //// Improper converted fragment by iMT.
    ////FIELD_SCHEMA.addFieldDescriptor(
    ////	new QueryFieldDescriptor(
    ////		FIELD_DFFULLADDRESS,
    ////		COLUMN_DFFULLADDRESS,
    ////		QUALIFIED_COLUMN_DFFULLADDRESS,
    ////		String.class,
    ////		false,
    ////		false,
    ////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
    ////		"",
    ////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
    ////		""));
    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(FIELD_DFFULLADDRESS,
                                                             COLUMN_DFFULLADDRESS,
                                                             QUALIFIED_COLUMN_DFFULLADDRESS,
                                                             String.class,
                                                             false, true,
                                                             QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
                                                             "ADDR.ADDRESSLINE1 || ' ' || ADDR.CITY || ' ' || PROVINCE.PROVINCEABBREVIATION",
                                                             QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
                                                             "ADDR.ADDRESSLINE1 || ' ' || ADDR.CITY || ' ' || PROVINCE.PROVINCEABBREVIATION"));

    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(FIELD_DFCONTACTPHONEEXTENSION,
                                                             COLUMN_DFCONTACTPHONEEXTENSION,
                                                             QUALIFIED_COLUMN_DFCONTACTPHONEEXTENSION,
                                                             String.class,
                                                             false, false,
                                                             QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                                                             "",
                                                             QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                                                             ""));

    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(FIELD_DFSOURCEFIRSTNAME,
                                                             COLUMN_DFSOURCEFIRSTNAME,
                                                             QUALIFIED_COLUMN_DFSOURCEFIRSTNAME,
                                                             String.class,
                                                             false, false,
                                                             QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                                                             "",
                                                             QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                                                             ""));

    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(FIELD_DFSOURCELASTNAME,
                                                             COLUMN_DFSOURCELASTNAME,
                                                             QUALIFIED_COLUMN_DFSOURCELASTNAME,
                                                             String.class,
                                                             false, false,
                                                             QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                                                             "",
                                                             QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                                                             ""));
    
    //-- ========== DJ#725 Begins ========== --//
    //-- By Neil at Nov/30/2004
    // contactEmailAddress
    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(FIELD_DFCONTACTEMAILADDRESS,
                                                             COLUMN_DFCONTACTEMAILADDRESS,
                                                             QUALIFIED_COLUMN_DFCONTACTEMAILADDRESS,
                                                             String.class,
                                                             false, false,
                                                             QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                                                             "",
                                                             QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                                                             ""));

    // psDescription
    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(FIELD_DFPSDESCRIPTION,
                                                             COLUMN_DFPSDESCRIPTION,
                                                             QUALIFIED_COLUMN_DFPSDESCRIPTION,
                                                             String.class,
                                                             false, false,
                                                             QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                                                             "",
                                                             QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                                                             ""));

    // systemTypeDescription
    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(FIELD_DFSYSTEMTYPEDESCRIPTION,
                                                             COLUMN_DFSYSTEMTYPEDESCRIPTION,
                                                             QUALIFIED_COLUMN_DFSYSTEMTYPEDESCRIPTION,
                                                             String.class,
                                                             false, false,
                                                             QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                                                             "",
                                                             QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                                                             ""));

    //-- ========== DJ#725 Ends ========== --//
    /***** FXP23815 add profilestatusid *****/
    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
            FIELD_DFPROFILESTATUSID, COLUMN_DFPROFILESTATUSID,
            QUALIFIED_COLUMN_DFPROFILESTATUSID, String.class, false, false,
            QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
            QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
    /***** FXP23815 add profilestatusid *****/
  }

  /**
   *
   *
   */
  public doDealEntrySourceInfoModelImpl()
  {
    super();
    setDataSourceName(DATA_SOURCE_NAME);

    setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

    setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

    setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

    setFieldSchema(FIELD_SCHEMA);

    initialize();
  }

  /**
   *
   *
   */
  public void initialize()
  {
  }

  ////////////////////////////////////////////////////////////////////////////////
  // NetDynamics-migrated events
  ////////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   */
  protected String beforeExecute(ModelExecutionContext context, int queryType,
                                 String sql) throws ModelControlException
  {
    // TODO: Migrate specific onBeforeExecute code
    return sql;
  }

  /**
   *
   *
   */
  protected void afterExecute(ModelExecutionContext context, int queryType)
                       throws ModelControlException
  {
      /***** FXP 23815 start *****/
      String defaultInstanceStateName = getRequestContext().getModelManager()
              .getDefaultModelInstanceName(SessionStateModel.class);
      SessionStateModelImpl theSessionState = (SessionStateModelImpl) getRequestContext()
              .getModelManager().getModel(SessionStateModel.class,
                      defaultInstanceStateName, true);
      int languageId = theSessionState.getLanguageId();
      int institutionId = theSessionState.getDealInstitutionId();

      String profileStatusId = getDfProfileStatusId();
      if (profileStatusId == null) {
          setDfPsDescription("");
      } else {
          setDfPsDescription(BXResources.getPickListDescription(
                  institutionId, "PROFILESTATUS",
                  profileStatusId, languageId));
      }
      /***** FXP 23815 end *****/
  }

  /**
   *
   *
   */
  protected void onDatabaseError(ModelExecutionContext context, int queryType,
                                 SQLException exception)
  {
  }

  /**
   *
   *
   */
  public String getDfSobpShortName()
  {
    return (String) getValue(FIELD_DFSOBPSHORTNAME);
  }

  /**
   *
   *
   */
  public void setDfSobpShortName(String value)
  {
    setValue(FIELD_DFSOBPSHORTNAME, value);
  }

  /**
   *
   *
   */
  public String getDfSourceFirmName()
  {
    return (String) getValue(FIELD_DFSOURCEFIRMNAME);
  }

  /**
   *
   *
   */
  public void setDfSourceFirmName(String value)
  {
    setValue(FIELD_DFSOURCEFIRMNAME, value);
  }

  /**
   *
   *
   */
  public String getDfAddressLine1()
  {
    return (String) getValue(FIELD_DFADDRESSLINE1);
  }

  /**
   *
   *
   */
  public void setDfAddressLine1(String value)
  {
    setValue(FIELD_DFADDRESSLINE1, value);
  }

  /**
   *
   *
   */
  public String getDfCity()
  {
    return (String) getValue(FIELD_DFCITY);
  }

  /**
   *
   *
   */
  public void setDfCity(String value)
  {
    setValue(FIELD_DFCITY, value);
  }

  /**
   *
   *
   */
  public String getDfContactFaxNumber()
  {
    return (String) getValue(FIELD_DFCONTACTFAXNUMBER);
  }

  /**
   *
   *
   */
  public void setDfContactFaxNumber(String value)
  {
    setValue(FIELD_DFCONTACTFAXNUMBER, value);
  }

  /**
   *
   *
   */
  public String getDfContactPhoneNumber()
  {
    return (String) getValue(FIELD_DFCONTACTPHONENUMBER);
  }

  /**
   *
   *
   */
  public void setDfContactPhoneNumber(String value)
  {
    setValue(FIELD_DFCONTACTPHONENUMBER, value);
  }

  /**
   *
   *
   */
  public String getDfProvinceAbbr()
  {
    return (String) getValue(FIELD_DFPROVINCEABBR);
  }

  /**
   *
   *
   */
  public void setDfProvinceAbbr(String value)
  {
    setValue(FIELD_DFPROVINCEABBR, value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfDealId()
  {
    return (java.math.BigDecimal) getValue(FIELD_DFDEALID);
  }

  /**
   *
   *
   */
  public void setDfDealId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFDEALID, value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfCopyId()
  {
    return (java.math.BigDecimal) getValue(FIELD_DFCOPYID);
  }

  /**
   *
   *
   */
  public void setDfCopyId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFCOPYID, value);
  }

  /**
   *
   *
   */
  public String getDfFullAddress()
  {
    return (String) getValue(FIELD_DFFULLADDRESS);
  }

  /**
   *
   *
   */
  public void setDfFullAddress(String value)
  {
    setValue(FIELD_DFFULLADDRESS, value);
  }

  /**
   *
   *
   */
  public String getDfContactPhoneExtension()
  {
    return (String) getValue(FIELD_DFCONTACTPHONEEXTENSION);
  }

  /**
   *
   *
   */
  public void setDfContactPhoneExtension(String value)
  {
    setValue(FIELD_DFCONTACTPHONEEXTENSION, value);
  }

  /**
   *
   *
   */
  public String getDfSourceFirstName()
  {
    return (String) getValue(FIELD_DFSOURCEFIRSTNAME);
  }

  /**
   *
   *
   */
  public void setDfSourceFirstName(String value)
  {
    setValue(FIELD_DFSOURCEFIRSTNAME, value);
  }

  /**
   *
   *
   */
  public String getDfSourceLastName()
  {
    return (String) getValue(FIELD_DFSOURCELASTNAME);
  }

  /**
   *
   *
   */
  public void setDfSourceLastName(String value)
  {
    setValue(FIELD_DFSOURCELASTNAME, value);
  }

  //-- ========== DJ#725 Begins ========== --//
  //-- By Neil at Nov/30/2004

  /**
   *
   */
  public String getDfContactEmailAddress()
  {
    return (String) getValue(FIELD_DFCONTACTEMAILADDRESS);
  }

  /**
   *
   */
  public void setDfContactEmailAddress(String value)
  {
    setValue(FIELD_DFCONTACTEMAILADDRESS, value);
  }

  /**
   *
   */
  public String getDfPsDescription()
  {
    return (String) getValue(FIELD_DFPSDESCRIPTION);
  }

  /**
   *
   */
  public void setDfPsDescription(String value)
  {
    setValue(FIELD_DFPSDESCRIPTION, value);
  }

  /**
   *
   */
  public String getDfSystemTypeDescription()
  {
    return (String) getValue(FIELD_DFSYSTEMTYPEDESCRIPTION);
  }

  /**
   *
   */
  public void setDfSystemTypeDescription(String value)
  {
    setValue(FIELD_DFSYSTEMTYPEDESCRIPTION, value);
  }

  //-- ========== DJ#725 Ends ========== --//
  
  /**
   *  FXP23815 profileStatusID
   */
  public String getDfProfileStatusId()
  {
      return (String) getValue(FIELD_DFPROFILESTATUSID);
  }

  /**
   *  FXP23815 profileStatusID
   */
  public void setDfProfileStatusId(String value)
  {
      setValue(FIELD_DFPROFILESTATUSID, value);
  }
}
