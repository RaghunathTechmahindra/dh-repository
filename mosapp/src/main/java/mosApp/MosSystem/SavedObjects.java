package mosApp.MosSystem;

import java.util.*;
import java.io.*;
import com.basis100.log.*;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mosApp.*;
import mosApp.MosSystem.*;
import MosSystem.*;

import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 */
public class SavedObjects extends Object
       implements Serializable
{
  //--> Modified theSession to SessionState -- By BILLY 11July2002
	//private HttpSession theSession;
  //private SessionStateModelImpl theSessionState;
	private SavedPagesModelImpl savedPages;
	public SysLogger logger;


	/**
	 *
	 *
	 */
	////SavedObjects(CSpSession session)
	SavedObjects(HttpSession session)
	{
		logger = SysLog.getSysLogger("SavedObjects");

    //// test
    HttpSession test = RequestManager.getRequestContext().getRequest().getSession();

    //logger.debug("The session from SavedObjects: " + test);

		////savedPages =(SavedPages) session.get(Sc.US_SAVED_PAGES);
		////theSession =(SessionState) session.get(Sc.US_THE_SESSION);
		String defaultInstanceStateName =
			       RequestManager.getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
		SessionStateModelImpl theSessionState =
			       (SessionStateModelImpl)RequestManager.getRequestContext().getModelManager().getModel(SessionStateModel.class,
			                                                                                            defaultInstanceStateName,
			                                                                                            true,
			                                                                                            true);
    //--> Here shpuld simply get the Saved Page from HTTPSession
    //--> By BILLY 11July2002
    savedPages = (SavedPagesModelImpl) session.getAttribute(Sc.US_SAVED_PAGES);

    if (notEmpty())
		{
			// !!IMPORTANT!! One of the main idiom of the BasisXpress project. Variations:
			// getTheSession.getCurrentPage(), etc. will be encouraged by the
			// app2JATO regular expressions.
			////PageEntry pe = theSession.getCurrentPage();

			////PageEntry pea = (PageEntry) RequestManager.getRequestContext().getViewBeanManager().
			////			getViewBeanBase().getPageSessionAttribute("NAME");
			////logger.debug("PageEntryFromPageAttribute: " + pea.getPageId());

      PageEntry pe = theSessionState.getCurrentPage();
      logger.debug("PageEntryFromSavedObjects: " + pe.getPageId());

      //--> Why ?
      /*
			if (pe.getPageId() == Mc.PGNM_INDIV_WORK_QUEUE_ID) savedPages.setWorkQueue(pe);
			else if (pe.getPageId() == Mc.PGNM_DEAL_SEARCH_ID) savedPages.setSearchPage(pe);
      */
      //--> Clean up all cached PEs -- Billy 22April2003
      savedPages.clear();
		}
	}


	/**
	 *
	 *
	 */
	boolean notEmpty()
	{
    //--> Changed by Billy 22April2003
		//return(theSessionState != null && savedPages != null);
    return(savedPages != null);
	}


	/**
	 *  JATO framework method getSession().getAttribute("String_name") replaces.
	 *  Should be reconsidered along with session redesign.
	 *
	 *
	 */
   //--> Commented out by Billy -- 22April2003
   /*
	SessionStateModelImpl getTheSessionState()
	{
		return theSessionState;
	}
  */

	/**
	 * Reimplement.
	 *
	 *
	 */
	SavedPagesModelImpl getSavedPages()
	{
		return savedPages;
	}

}

