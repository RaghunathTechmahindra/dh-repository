package mosApp.MosSystem;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.docprep.DocPrepException;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealPropagator;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.util.StringUtil;

import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;

import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;

import com.basis100.picklist.BXResources;

import com.basis100.resources.SessionResourceKit;

import com.basis100.workflow.UserProfileQuery;
import com.basis100.workflow.WorkflowAssistBean;
import com.basis100.workflow.entity.AssignedTask;
import com.basis100.workflow.pk.AssignedTaskBeanPK;
import com.filogix.util.Xc;
import com.filogix.express.web.rc.RequestProcessException;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.view.DisplayField;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.Option;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;

import java.sql.ResultSet;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 
 */
public class TaskReassignmentHandler extends PageHandlerCommon implements
        Cloneable, Xc {
	
	private final static Logger logger = 
        LoggerFactory.getLogger(TaskReassignmentHandler.class);
	
    public static final String CB_INFO = "cbInfo";
    final static int REASSIGN_CURRENT_TASK = 0;
    final static int REASSIGN_CURRENT_DEAL = 1;

    // For User Hunting
    static UserProfileQuery userQuery = new UserProfileQuery();

    /**
     * 
     * 
     */
    public TaskReassignmentHandler() {
        super();
    }

    /**
     * 
     * 
     */
    public TaskReassignmentHandler(PageHandlerCommon other) {
        super();
        initFromOther(other);
    }

    /**
     * 
     * 
     */
    public TaskReassignmentHandler cloneSS() {
        return (TaskReassignmentHandler) super.cloneSafeShallow();
    }

    // This method is used to populate the fields in the header
    // with the appropriate information
    public void populatePageDisplayFields() {
        PageEntry pg = theSessionState.getCurrentPage();
        populatePageShellDisplayFields();
        populateTaskNavigator(pg);
        populatePageDealSummarySnapShot();
        populatePreviousPagesLinks();

        populateTaskReassignmentOptions();

         DisplayField filterBox = (DisplayField) (getCurrNDPage()
                .getDisplayField(pgTaskReassignmentViewBean.CHILD_CBREASSIGNTO));
        doAutoFill(filterBox);
        filterBox = (DisplayField) (getCurrNDPage()
                .getDisplayField(pgTaskReassignmentViewBean.CHILD_CBMULTYDEALFROM));
        doAutoFill(filterBox);
        filterBox = (DisplayField) (getCurrNDPage()
                .getDisplayField(pgTaskReassignmentViewBean.CHILD_CBMULTYDEALTO));
        doAutoFill(filterBox);

        // //====================End=================================================
        try {
            //int taskId = pg.getPageCriteriaId1();
            doMasterWorkQueueModelImpl theDO = (doMasterWorkQueueModelImpl) (RequestManager
                    .getRequestContext().getModelManager()
                    .getModel(doMasterWorkQueueModel.class));
            theDO.clearUserWhereCriteria();
            theDO.addUserWhereCriterion(doMasterWorkQueueModel.FIELD_DFASSIGNTASKID, "=", new Integer(pg
                    .getPageCriteriaId1()));

            String assignedUserName = null;

            try {
                ResultSet rs = theDO.executeSelect(null);

                if ((rs != null) && (theDO.getSize() > 0)) {
                    assignedUserName = theDO
                            .getValue(
                                    doMasterWorkQueueModelImpl.FIELD_DFASSIGNEDUSERNAME)
                            .toString();
                    logger.debug("TRH@PPDFMasterWQDataObject::LastName: "
                            + assignedUserName.toString());
                } else {
                    logger
                            .error("TRH@PPDFMasterWQDataObject::Problem when getting the assignedUserName !!");
                }
            } catch (Exception e) {
                logger.error("TRH@PPDFMasterWQDataObject:: " + e);
            }

            getCurrNDPage().setDisplayFieldValue(pgTaskReassignmentViewBean.CHILD_STASSIGNEDTO,
                    assignedUserName);

            doMWQPickListModelImpl taskMo = (doMWQPickListModelImpl) (RequestManager
                    .getRequestContext().getModelManager()
                    .getModel(doMWQPickListModel.class));
            taskMo.clearUserWhereCriteria();
            taskMo.addUserWhereCriterion(doMWQPickListModel.FIELD_DFASSIGNTASKID, "=", new Integer(pg
                    .getPageCriteriaId1()));

            String taskDesc = null;

            try {
                ResultSet rs = taskMo.executeSelect(null);

                if ((rs != null) && (taskMo.getSize() > 0)) {
                    taskDesc = taskMo.getValue(
                            doMWQPickListModelImpl.FIELD_DFTMDESCRIPTION)
                            .toString();
                    logger
                            .debug("TRH@PPDFMWQPickListDataObject::TaskDescritpion: "
                                    + taskDesc.toString());
                } else {
                    logger
                            .error("TRH@PPDFMWQPickListDataObject::Problem when getting the assignedUserName !!");
                    taskDesc = "";
                }
            } catch (Exception e) {
                logger.error("TRH@PPDFMWQPickListDataObject:: " + e);
            }

            getCurrNDPage().setDisplayFieldValue(pgTaskReassignmentViewBean.CHILD_STTASKDESCRIPTION, taskDesc);
        } catch (Exception e) {
            logger
                    .error("Exception TaskReassignmentHandler@populatePageDisplayFields()");
            //logger.error(e);
            setStandardFailMessage();
        }
    }

    //
    // Setup data objects associated with page - e.g. set criteria for deal snap
    // shot, etc.
    //
    // If there is no any assinged task on the main workqueue screen, only
    // multytask
    // rerouting allowed in the Reassignment screen. Reassignment of the current
    // task
    // and Deal are disabled: if user picks from Reassign Task/Deal combobox he
    // gets message
    // and navigates to the main screen
    public void setupBeforePageGeneration() {
        PageEntry pg = theSessionState.getCurrentPage();
        SessionResourceKit srk = getSessionResourceKit();

        if (pg.getSetupBeforeGenerationCalled() == true) {
            return;
        }

        pg.setSetupBeforeGenerationCalled(true);

        if (pg.getPageCondition3() == false) {
            return;
        }
        
        int taskId = pg.getPageCriteriaId1();
        int copyId = pg.getPageCriteriaId2();

        try {
            AssignedTask at = new AssignedTask(srk);
            at.findByPrimaryKey(new AssignedTaskBeanPK(taskId));

            // int dealId = at.getDealId();
            int dealId = pg.getPageDealId();
            // int copyId = getGoldCopy(srk, dealId);
            Deal deal = new Deal(srk, null, dealId, copyId);
            // pg.setPageDealId(dealId);
            pg.setPageDealCID(copyId);
            pg.setPageDealApplicationId(deal.getApplicationId());
            setupDealSummarySnapShotDO(pg);

            try {
                setDataObject(doUnderwriter1Model.class, deal
                        .getUnderwriterUserId());
                setDataObject(doAdministratorModel.class, deal
                        .getAdministratorId());
                setDataObject(doFunderModel.class, deal.getFunderProfileId());
            } catch (Exception ex) {
                logger.debug("TRH@setupBeforePageGeneration:: " + ex);
            }

            // applyCriteriaToAutoFillDataObject("cbReassignTo", userId);
        } catch (Exception e) {
            logger
                    .error("Exception TaskReassignmentHandler@setupBeforePageGeneration()");
            //logger.error(e);
            setStandardFailMessage();
        }
    }

    /**
     * 
     * 
     */

    // //private void setDataObject(String doName, int userProfileId)
    private void setDataObject(Class modelClass, int userProfileId) {
        SelectQueryModel theDo = null;

        try {
            theDo = (SelectQueryModel) RequestManager.getRequestContext()
                    .getModelManager().getModel(modelClass);
            logger.debug("TRH@setDataObject::TheDO: " + theDo.getName());

            if (theDo != null) {
                theDo.clearUserWhereCriteria();
                theDo.addUserWhereCriterion(doUnderwriter1Model.FIELD_DFUSERPROFILEID, "=",
                        new Integer(userProfileId));
            } else {
                throw new ClassNotFoundException(
                        "TRH@setDataObject::The Model: " + modelClass
                                + " for the userProfile: " + userProfileId
                                + " has not been found");
            }
        } catch (ClassNotFoundException cnfe) {
            logger.debug("TRH@setDataObject:: " + cnfe);
        }
    }

    /**
     * 
     * 
     */
    public void doAutoFill(DisplayField field) {
        // logger.trace("Reassign@doAutofill: ComboBox Name is "+
        // field.getName());
        releaseData(field.getName());

        // logger.debug("TRH@doAutoFill::FieldName: " + field.getName());
    }

    /**
     * 
     * 
     */
    private int getGoldCopy(SessionResourceKit srk, int dealId)
            throws Exception {
        MasterDeal md = new MasterDeal(srk, null, dealId);

        return md.getGoldCopyId();
    }

    /**
     * 
     * 
     */
    public void handleSubmit() {
        // ML:
        //srk.getExpressState().cleanAllIds();

        PageEntry pg = theSessionState.getCurrentPage();
        SessionResourceKit srk = getSessionResourceKit();
        String cbReassignValue = "";
        String cbReassignToValue = "";
        String cbMultyDealFromValue = "";
        String cbMultyDealToValue = "";

        try {
            //int copyId = 0;
            Deal deal = null;

            ComboBox cbReassign = (ComboBox) getCurrNDPage().getDisplayField(
                    pgTaskReassignmentViewBean.CHILD_CBREASSIGN);
            ComboBox cbReassignTo = (ComboBox) getCurrNDPage().getDisplayField(
                    pgTaskReassignmentViewBean.CHILD_CBREASSIGNTO);
            ComboBox cbMultyDealFrom = (ComboBox) getCurrNDPage()
                    .getDisplayField(pgTaskReassignmentViewBean.CHILD_CBMULTYDEALFROM);
            ComboBox cbMultyDealTo = (ComboBox) getCurrNDPage()
                    .getDisplayField(pgTaskReassignmentViewBean.CHILD_CBMULTYDEALTO);
            cbReassignValue = (String) cbReassign.getValue();
            cbReassignToValue = (String) cbReassignTo.getValue();
            cbMultyDealFromValue = (String) cbMultyDealFrom.getValue();
            cbMultyDealToValue = (String) cbMultyDealTo.getValue();
            
            if (!StringUtil.isEmpty(cbReassignValue)){
                if (pg.getPageCondition3() == false) {
                    setActiveMessageToAlert(BXResources.getSysMsg(
                            REASSIGN_TASK_NO_CURRENT_TASK, theSessionState
                                    .getLanguageId()),
                            ActiveMsgFactory.ISCUSTOMCONFIRM);
                    navigateToNextPage();

                    return;
                }

                if (!StringUtil.isEmpty(cbReassignToValue)){
                    int selectedUserId = Integer.parseInt(cbReassignToValue);
                    //add for user deal vpd for Individual Task/Deal Reassignment
                    theSessionState.getExpressState().setDealInstitutionId(
                            theSessionState.getDealInstitutionId());
                    
                    if (Integer.parseInt(cbReassignValue) == REASSIGN_CURRENT_TASK) {
                        // re-assign this task only
                        int atId = pg.getPageCriteriaId1();

                        AssignedTask at = new AssignedTask(srk);
                        at.findByPrimaryKey(new AssignedTaskBeanPK(atId));
                        srk.beginTransaction();

                        if (reassignTaskOnly(at, selectedUserId, srk, at
                                .getInstitutionProfileId()) == false) {
                            // Re-assign failed
                            srk.cleanTransaction();

                            return;
                        }
                    } else {
                        // Re-assign the Deal
                        deal = new Deal(srk, null, pg.getPageDealId(), pg
                                .getPageDealCID());

                        // logger.debug("TRH@ReassignDeal:DealId: " +
                        // deal.getDealId());
                        // logger.debug("TRH@ReassignDeal:UnderwriterId: " +
                        // deal.getUnderwriterUserId());
                        // logger.debug("TRH@ReassignDeal:FunderProfileId: " +
                        // deal.getFunderProfileId());
                        // logger.debug("TRH@ReassignDeal:AdministratorId: " +
                        // deal.getAdministratorId());
                        srk.beginTransaction();

                        if (reassignDeal(deal, selectedUserId, srk) == false) {
                            // Re-assign failed
                            srk.cleanTransaction();

                            return;
                        }
                    }
                } else {
                    // "Please select the user you want to assign this task to."
                    setActiveMessageToAlert(BXResources.getSysMsg(
                            REASSIGN_TASK_NO_USER_TO, theSessionState
                                    .getLanguageId()),
                            ActiveMsgFactory.ISCUSTOMCONFIRM);

                    return;
                }
            } else {
                // Multiple re-assign of all deals
                if (!StringUtil.isEmpty(cbMultyDealFromValue)){
                    if (!StringUtil.isEmpty(cbMultyDealToValue)){
                        
                        //OptionList option = cbMultyDealFrom.getOptions();
                        //4.4 MWQ search start:  adding populate method call since this field was changed to non static filed.
                        //option.populate(getCurrNDPage().getRequestContext());
                        //4.4 MWQ search end;
                        //String label = option.getValueLabel(cbMultyDealFromValue);
                        //String[] labelArray = label.split(" ");
                        //String userFrom = labelArray[labelArray.length-1];

                        //label = option.getValueLabel(cbMultyDealToValue);
                        //labelArray = label.split(" ");
                        //String userTo = labelArray[labelArray.length-1];
                        
	                        if (cbMultyDealFromValue.equalsIgnoreCase(cbMultyDealToValue)) {
	                            setActiveMessageToAlert(BXResources.getSysMsg(
	                                    REASSIGN_TASK_DIFFERENT_USER_TO,
	                                    theSessionState.getLanguageId()),
	                                    ActiveMsgFactory.ISCUSTOMCONFIRM);
	
	                            return;
	                        }
                        DealHistoryLogger dhl = DealHistoryLogger
                                .getInstance(srk);

                        srk.beginTransaction();
                        long startTime = Calendar.getInstance().getTimeInMillis();
                                               
                        if (rerouteTasks(srk, cbMultyDealFromValue, cbMultyDealToValue) == false) {
                            // Failed to re-route
                            srk.cleanTransaction();

                            return;
                        }
                        long endTime = Calendar.getInstance().getTimeInMillis();
                        logger.info("Time taken to assign tasks from user: to toUser:" 
                        		+ " is " + (endTime - startTime) + " milliseconds");
                        
                    } else {
                        // "Please select the user you want to assign this task
                        // to."
                        setActiveMessageToAlert(BXResources.getSysMsg(
                                REASSIGN_TASK_NO_USER_TO, theSessionState
                                        .getLanguageId()),
                                ActiveMsgFactory.ISCUSTOMCONFIRM);

                        return;
                    }
                } else {
                    // "Please select the user whose task you want to assign."
                    setActiveMessageToAlert(BXResources.getSysMsg(
                            REASSIGN_TASK_NO_USER_FROM, theSessionState
                                    .getLanguageId()),
                            ActiveMsgFactory.ISCUSTOMCONFIRM);

                    return;
                }
            }

            navigateToNextPage();
            srk.commitTransaction();

        } catch (Exception e) {
            srk.cleanTransaction();
            logger.error("TaskReassignmentHandler: @handleSubmit() Exception" + e.getMessage());
            setStandardFailMessage();
        } finally {
            srk.freeResources();
            logger.info("TaskReassignmentHandler: @handleSubmit() free srk");
        }
    }

    private void reassignAllDealTask(SessionResourceKit srk,
            Deal deal, int currentUserId,
            int currentAdminId, int currentFunderId, int newUserId,
            int newAdminId, int newFunderId, int copyId) throws Exception {
        int dealId = deal.getDealId();
        AssignedTask at = new AssignedTask(srk);
        Vector<AssignedTask> atVec = at.findByDealbyStatus(dealId, Sc.TASK_OPEN);
        String sText = "";

        DealHistoryLogger dhl = new DealHistoryLogger(srk);
        UserProfile currAdmin = new UserProfile(srk);
        currAdmin.findByPrimaryKey(new UserProfileBeanPK(currentAdminId, deal
                .getInstitutionProfileId()));

        boolean isReassignAdmin = userQuery.getInternalUserTypeId(currAdmin.getUserTypeId()) == UserProfileQuery.ADMIN_USER_TYPE;

        UserProfile currFunder = new UserProfile(srk);
        currFunder.findByPrimaryKey(new UserProfileBeanPK(currentFunderId, deal
                .getInstitutionProfileId()));
        boolean isReassignFunder = userQuery.getInternalUserTypeId(currFunder.getUserTypeId()) == UserProfileQuery.FUNDER_USER_TYPE;

        // Mass update to check the performance
        
        
        // =================================================================================================
        //final int goldCopy = getGoldCopy(srk, dealId);
        //final int goldCopy = new Deal(srk, null).findByRecommendedScenario(new DealPK(dealId, 0), true).getCopyId();
        
        for (int i = 0; i < atVec.size(); ++i) {
            AssignedTask at1 = atVec.get(i);
            //if (at1.getTaskStatusId() != Sc.TASK_OPEN)
            //  continue;
            logger.info("reassign AssignedTask id: " + at1.getAssignedTasksWorkQueueId());
            logger.info("reassign AssignedTask dealiId: " + dealId);
            if ((currentUserId != 0) && (currentUserId == at1.getUserProfileId())) {
                // UW reassignment
                at1.setUserProfileId(newUserId);
                logger.info("reassign AssignedTask id: " + at1.getAssignedTasksWorkQueueId()
                            + " newUserid " + newUserId);
                // Create Deal History Log
                if (at1.getTaskId() < 50000) {
                    sText = dhl.taskAndDealReassign(at1,UserProfileQuery.UNDERWRITER_USER_TYPE, currentUserId, newUserId);
                    dhl.log(dealId, copyId, sText, Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId());
                }
                at1.ejbStore();

                // added here after ejbStore() to make sure the changes have been
                // submitted successfully.
                // 1. reassign to new user
                UserProfile up = new UserProfile(srk);
                up.findByPrimaryKey(new UserProfileBeanPK(newUserId, deal.getInstitutionProfileId()));
                logger.debug("TRH:" + up.getTaskAlert());

                if (up.getTaskAlert().trim().equalsIgnoreCase("Y")) {
                    sendNotificationEmail(deal, newUserId);
                }

            } else if ((isReassignAdmin == true) && (currentAdminId != 0)
                        && (currentAdminId == at1.getUserProfileId())
                        && (newAdminId != currentAdminId)) {
                // Admin reassignment
                at1.setUserProfileId(newAdminId);
                logger.info("reassign admin AssignedTask id: " + at1.getAssignedTasksWorkQueueId()
                            + " newAdminId: " + newAdminId);
                // Create Deal History Log
                if (at1.getTaskId() < 50000){
                    sText = dhl.taskAndDealReassign(at1, UserProfileQuery.ADMIN_USER_TYPE, currentAdminId, newAdminId);
                    dhl.log(dealId, copyId, sText, Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId());
                }

                at1.ejbStore();

                // added here after ejbStore() to make sure the changes have been
                // submitted successfully.
                // 2. reassign to new admin
                UserProfile up = new UserProfile(srk);
                up.findByPrimaryKey(new UserProfileBeanPK(newAdminId, deal.getInstitutionProfileId()));
                logger.debug("up.getTaskAlert:" + up.getTaskAlert());

                if (up.getTaskAlert().trim().equalsIgnoreCase("Y")) {
                    sendNotificationEmail(deal, newAdminId);
                }

            } else if ((isReassignFunder == true) && (currentFunderId != 0)
                        && (currentFunderId == at1.getUserProfileId())
                        && (newFunderId != currentFunderId)) {
                // Funder reassignment
                at1.setUserProfileId(newFunderId);
                logger.info("reassign funder AssignedTask id: " + at1.getAssignedTasksWorkQueueId()
                            + " newFunderId " + newFunderId);
                // Create Deal History Log
                if (at1.getTaskId() < 50000) {
                    sText = dhl.taskAndDealReassign(at1, UserProfileQuery.FUNDER_USER_TYPE, currentFunderId, newFunderId);
                    dhl.log(dealId, copyId, sText, Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId());
                }

                at1.ejbStore();

                // added here after ejbStore() to make sure the changes have been
                // submitted successfully.
                // 3. reassign to new funder
                UserProfile up = new UserProfile(srk);
                up.findByPrimaryKey(new UserProfileBeanPK(newFunderId, deal.getInstitutionProfileId()));
                logger.debug("TRH: " + up.getTaskAlert());

                if (up.getTaskAlert().trim().equalsIgnoreCase("Y")) {
                    sendNotificationEmail(deal, newFunderId);
                }

            } else {
                continue;
            }
        }
        
    }

    /**
     * @param deal
     * @param newId
     * 
     * @throws Exception
     */
    protected void sendNotificationEmail(Deal deal, int newId) throws Exception {
        try {
            Contact contact = new Contact(srk);
            contact.findByUserProfileId(newId);

            int languageId = contact.getLanguagePreferenceId();

            String emailSubj = BXResources.getGenericMsg(
                    NEW_TASK_ALERT_EMAIL_SUBJECT, languageId);
            String emailText = BXResources.getGenericMsg(
                    NEW_TASK_ALERT_EMAIL_MESSAGE, languageId);

            // sending alert message
            DocumentRequest.requestAlertEmail(srk, deal, contact
                    .getContactEmailAddress(), emailSubj, emailText);
        } catch (Exception e) {
          logger.error("TRH@sendNotificationEmail:"+e);
          throw e;
        }
    }

    private void setAdminAndFunder(SessionResourceKit srk, Deal currDeal, UserProfile userToAssign)
            throws Exception {
        currDeal.setUnderwriterUserId(userToAssign.getUserProfileId());
        WorkflowAssistBean wab = new WorkflowAssistBean();
        wab.assignAdminForNewUnderwriter(currDeal, srk);

        // Added Funder assignment handle -- by BILLY 14Jan2002
        wab.assignFunderForNewUnderwriter(currDeal, srk);
    }

    /*private int getPartnerId(SessionResourceKit srk, int userId,
            int institutionId) throws Exception {
        UserProfile user = new UserProfile(srk);
        user.findByPrimaryKey(new UserProfileBeanPK(userId, institutionId));
        return user.getPartnerUserId();
    }*/

    private void populateTaskReassignmentOptions() {
        //PageEntry pg = theSessionState.getCurrentPage();
        //ComboBox statFilter = (ComboBox) getCurrNDPage().getDisplayField(
        //pgTaskReassignmentViewBean.CHILD_CBREASSIGN);

        // // Manual population of the Reassign Combobox.
        int lim = Mc.TASK_REASSIGN_OPTIONS_LENGTH;
        //OptionList options = new OptionList();
        String[] labels = new String[lim];
        String[] values = new String[lim];

        // //Get the Option Labels from the GenericMessage.properties file
        String currentTaskLbl = BXResources.getGenericMsg(
                TASK_REASSIGN_CURRENT_TASK_LABEL, theSessionState
                        .getLanguageId());
        String currentDealLbl = BXResources.getGenericMsg(
                TASK_REASSIGN_CURRENT_DEAL_LABEL, theSessionState
                        .getLanguageId());
        String[] TASK_REASSING_OPTIONS = { currentTaskLbl, currentDealLbl };

        for (int i = 0; i < lim; i++) {
            labels[i] = TASK_REASSING_OPTIONS[i];
            values[i] = Integer.toString(i);
        }
        OptionList reassignOption = pgTaskReassignmentViewBean.cbReassignOptions;
        reassignOption.setOptions(labels, values);
        
       
    }

    /**
     * 
     * 
     */
    private void releaseData(String stringName) {
        try {
            PageEntry pg = theSessionState.getCurrentPage();
            Hashtable pst = pg.getPageStateTable();

            if (pst == null) {
                pst = new Hashtable();
                pg.setPageStateTable(pst);

                return;
            }

            CbInfo cbInfo = (CbInfo) (pst.get(CB_INFO));

            if (cbInfo != null) {
                if (stringName.equals(pgTaskReassignmentViewBean.CHILD_CBREASSIGN)) {
                    logger.debug("TRH@releaseData::cbReassignValue: "
                            + (String) cbInfo.getCbReassign().getValue());
                    getCurrNDPage().setDisplayFieldValue(pgTaskReassignmentViewBean.CHILD_CBREASSIGN,
                            (String) cbInfo.getCbReassign().getValue());
                }

                if (stringName.equals(pgTaskReassignmentViewBean.CHILD_CBREASSIGNTO)) {
                    logger.debug("TRH@releaseData::cbReassignValueTo: "
                            + (String) cbInfo.getCbReassignTo().getValue());
                    getCurrNDPage().setDisplayFieldValue(pgTaskReassignmentViewBean.CHILD_CBREASSIGNTO,
                            (String) cbInfo.getCbReassignTo().getValue());
                }

                if (stringName.equals(pgTaskReassignmentViewBean.CHILD_CBMULTYDEALFROM)) {
                    logger.debug("TRH@releaseData::cbMultyDealFrom: "
                            + (String) cbInfo.getCbMultyDealFrom().getValue());
                    getCurrNDPage().setDisplayFieldValue(pgTaskReassignmentViewBean.CHILD_CBMULTYDEALFROM,
                            (String) cbInfo.getCbMultyDealFrom().getValue());
                }

                if (stringName.equals(pgTaskReassignmentViewBean.CHILD_CBMULTYDEALTO)) {
                    logger.debug("TRH@releaseData::cbMultyDealTo: "
                            + (String) cbInfo.getCbMultyDealTo().getValue());
                    getCurrNDPage().setDisplayFieldValue(pgTaskReassignmentViewBean.CHILD_CBMULTYDEALTO,
                            (String) cbInfo.getCbMultyDealTo().getValue());
                }
            }
        } catch (Exception e) {
            logger.error("TaskReassignmentHandler: @releaseData() Exception");
            //logger.error(e);
            setStandardFailMessage();
        }
    }

    /**
     * 
     * 
     */
    public void saveData(PageEntry pg, boolean calledInTransaction) {
        try {
            //SessionResourceKit srk = getSessionResourceKit();
            CbInfo cbInfo = new CbInfo(
            (ComboBox) getCurrNDPage().getDisplayField(pgTaskReassignmentViewBean.CHILD_CBREASSIGN),
            (ComboBox) getCurrNDPage().getDisplayField(pgTaskReassignmentViewBean.CHILD_CBREASSIGNTO),
            (ComboBox) getCurrNDPage().getDisplayField(pgTaskReassignmentViewBean.CHILD_CBMULTYDEALFROM),
            (ComboBox) getCurrNDPage().getDisplayField(pgTaskReassignmentViewBean.CHILD_CBMULTYDEALTO));
            Hashtable pst = pg.getPageStateTable();
            pst.put(CB_INFO, cbInfo);
            pg.setModified(true);
        } catch (Exception e) {
            logger.trace("Task Reassignment.Exception=" + e);
            setActiveMessageToAlert(BXResources.getSysMsg(
                    RELATED_UNEXPECTED_FAILURE, theSessionState
                            .getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);

            return;
        }
    }

    // ==========================================================================
    // Method to hunt for a Active user
    // - if selected user id in-active then hunt by the Stand-in
    // - if all stand-in in-active then hunt by group and branch
    // ==========================================================================
    private UserProfile findActiveUser(UserProfile selectedUser,
            SessionResourceKit srk, int institutionId) {
        int iUserType = userQuery.getInternalUserTypeId(selectedUser
                .getUserTypeId());
        UserProfile userToAssign = userQuery.getUserProfile(selectedUser
                .getUserProfileId(), institutionId, true, true, iUserType, srk);

        if (userToAssign == null) {
            // No active stand-in user found.
            // Then try get user from Group or Branch
            Vector matchedUsers = null;
            WorkflowAssistBean wab = new WorkflowAssistBean();
            
            switch (iUserType) {
      	      case UserProfileQuery.UNDERWRITER_USER_TYPE:
      	      default:
                  // perform underwriter assignment - load balanced group (branch
                  // if necessary) underwriters
                  matchedUsers = userQuery.getUnderwriters(selectedUser.getGroupProfileId(), -1, true, srk, institutionId);
                  userToAssign = wab.getLeastLoadedProfile(matchedUsers, srk, institutionId);
      	      	break;
      	      case UserProfileQuery.ADMIN_USER_TYPE:
                  // perform admin assignment - load balanced group (branch if necessary)
                  matchedUsers = userQuery.getAdministrators(selectedUser.getGroupProfileId(), -1, true, srk, institutionId);
                  userToAssign = wab.getLeastLoadedProfile(matchedUsers, srk, institutionId);
      		      break;
            	case UserProfileQuery.FUNDER_USER_TYPE:		
                  // perform funder assignment - load balanced group (branch if necessary)
                  matchedUsers = userQuery.getFunders(selectedUser.getGroupProfileId(), true, srk, institutionId);
                  userToAssign = wab.getLeastLoadedProfile(matchedUsers, srk, institutionId);
      		      break;
            }
      
        }

        return userToAssign;
    }

    /**
     * 
     * 
     */
    private boolean reassignTaskOnly(AssignedTask at, int selectedUserId,
            SessionResourceKit srk, int institutionId) throws Exception {
        int dealId = at.getDealId();
        int copyId = theSessionState.getCurrentPage().getPageDealCID();
        int currentUserId = at.getUserProfileId();
        UserProfile currUser = new UserProfile(srk);
        currUser.findByPrimaryKey(new UserProfileBeanPK(currentUserId,institutionId));

        UserProfile selectedUser = new UserProfile(srk);
        selectedUser.findByPrimaryKey(new UserProfileBeanPK(selectedUserId,institutionId));

        //change srk userprofile
        srk = changeUserProfile(institutionId, srk);

        // As requested by Products that we don't need to check this -- Billy
        // 21Jan2002
        // Check if user type matched
        // if(userQuery.getInternalUserTypeId(selectedUser.getUserTypeId()) !=
        // userQuery.getInternalUserTypeId(currUser.getUserTypeId()))
        // {
        // // The user you assigned this task to is an invalid user type.
        // setActiveMessageToAlert(Sc.REASSIGN_TASK_INVALID_USER_TO,
        // ActiveMsgFactory.ISCUSTOMCONFIRM);
        // return false;
        // }
        // Need to hunt for the user if InActive -- By BILLY 01Nov2001
        UserProfile userToAssign = findActiveUser(selectedUser, srk,institutionId);
        if (userToAssign == null) {
            setActiveMessageToAlert(BXResources.getSysMsg(
                    REASSIGN_TASK_NOACTIVE_USER_FOUND, theSessionState
                            .getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);

            return false;
        }

        at.setUserProfileId(userToAssign.getUserProfileId());
        at.ejbStore();

        // -- ========== SCR#537 begins ========== --//
        // -- by Neil on Jan/24/2005
        // after successfully ejbStore(), send email
        Deal deal = new Deal(srk, null);
        deal.findByPrimaryKey(new DealPK(dealId, copyId));

        logger.debug("TRH: " + selectedUser.getTaskAlert());

        if (selectedUser.getTaskAlert().trim().equalsIgnoreCase("Y")) {
            sendNotificationEmail(deal, selectedUserId);
        }

        DealHistoryLogger dhl = new DealHistoryLogger(srk);
        String sText = dhl.taskAndDealReassign(at, UserProfileQuery.ANY_USER_TYPE, currentUserId, userToAssign
                .getUserProfileId());
        dhl.log(dealId, copyId, sText, Mc.DEAL_TX_TYPE_EVENT, 
                srk.getExpressState().getUserProfileId());

        return true;
    }

    /*
     * reassign the deal to another user and uses 'reassignAllDealTask' to reassign its tasks 
     */
    private boolean reassignDeal(Deal deal, int selectedUserId,
            SessionResourceKit srk) throws Exception {
        int currentUserId = deal.getUnderwriterUserId();
        UserProfile currUser = new UserProfile(srk);
        currUser.findByPrimaryKey(new UserProfileBeanPK(currentUserId, deal
                .getInstitutionProfileId()));

        UserProfile selectedUser = new UserProfile(srk);
        selectedUser.findByPrimaryKey(new UserProfileBeanPK(selectedUserId,
                deal.getInstitutionProfileId()));
        
        int toIUserType = userQuery.getInternalUserTypeId(selectedUser
                .getUserTypeId());

        // logger.debug("===> NEIL : AssignType = " + assignType);
        // Check if user type matched
        switch (toIUserType) {
    	    case UserProfileQuery.UNDERWRITER_USER_TYPE:
    	    case UserProfileQuery.ADMIN_USER_TYPE:
    	    case UserProfileQuery.FUNDER_USER_TYPE:		
    	    	break;
    	    default:
	          //	The user you assigned this task to is an invalid user type.
             logger.trace("TRH@unknown user type: " + toIUserType);
	         setActiveMessageToAlert(BXResources.getSysMsg(REASSIGN_TASK_INVALID_USER_TO, 
	         theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
	          return false;
        }

        // Need to hunt for the user if InActive -- By BILLY 01Nov2001
        UserProfile userToAssign = findActiveUser(selectedUser, srk, deal
                .getInstitutionProfileId());

        if (userToAssign == null) {
            setActiveMessageToAlert(BXResources.getSysMsg(
                    REASSIGN_TASK_NOACTIVE_USER_FOUND, theSessionState
                            .getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);

            return false;
        }
        
        reassignAllCopies(srk, deal, currentUserId, userToAssign, toIUserType);
        DealHistoryLogger dhl = new DealHistoryLogger(srk); 
        int dealId = deal.getDealId();
        String sText = dhl.taskAndDealReassign(null, toIUserType, currentUserId, userToAssign.getUserProfileId());
        dhl.log(dealId, getGoldCopy(srk, dealId), sText, Mc.DEAL_TX_TYPE_EVENT, srk.getUserProfileId());

        return true;
    }

    /**
     * Reassigns all deals from one user to another.
     * 
     */
    private boolean rerouteTasks(SessionResourceKit srk,
            String fromUserTaskId, String toUserTaskId) throws Exception {
        
        ComboBox cbInstitution = (ComboBox) getCurrNDPage().getDisplayField(pgTaskReassignmentViewBean.CHILD_CBINSTITUTION);
        String selectedInstitution = (String) cbInstitution.getValue();
       
        int fromInstId;
        
        if(selectedInstitution != null && selectedInstitution.length() == 0) 
        	fromInstId =  srk.getExpressState().getDealInstitutionId();
        else 
        	fromInstId = Integer.parseInt(selectedInstitution);
        
        srk.getExpressState().setDealInstitutionId(fromInstId);
        
        UserProfile fromUser = new UserProfile(srk);
        UserProfile toUser = new UserProfile(srk);
        
        fromUser.findByPrimaryKey(new UserProfileBeanPK(Integer.parseInt(fromUserTaskId), fromInstId));
        /***** FXP24382 fix start *****/
        toUser.findByPrimaryKey(new UserProfileBeanPK(Integer.parseInt(toUserTaskId), fromInstId));
        /***** FXP24382 fix end  *****/
      
        srk.getExpressState().cleanAllIds(); 
       
        int fromIUserType = userQuery.getInternalUserTypeId(fromUser
                .getUserTypeId());
        int toIUserType = userQuery.getInternalUserTypeId(toUser
                .getUserTypeId());

        // Check if user type matched
        if (fromIUserType != toIUserType) {
            // The user you assigned this task to is an invalid user type.
            setActiveMessageToAlert(BXResources.getSysMsg(
                    REASSIGN_TASK_USERS_TYPE_NOT_MATCHED, theSessionState
                            .getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);

            return false;
        }

        if ((toIUserType != UserProfileQuery.UNDERWRITER_USER_TYPE)
                && (toIUserType != UserProfileQuery.ADMIN_USER_TYPE)
                && (toIUserType != UserProfileQuery.FUNDER_USER_TYPE)) {
            // The user you assigned this task to is an invalid user type.
            setActiveMessageToAlert(BXResources.getSysMsg(
                    REASSIGN_TASK_USERS_TYPE_NOT_MATCHED, theSessionState
                            .getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);

            return false;
        }

        JdbcExecutor jExec = srk.getJdbcExecutor();
        String sql = "";
        
        int fromUserId = fromUser.getUserProfileId(fromInstId);
             //why are we only looking at gold copies?
        if (toIUserType == UserProfileQuery.UNDERWRITER_USER_TYPE) {
            // Setup SQL for UW routing
            sql = "Select dealId, copyId from Deal where UnderwriterUserId="
                    + fromUserId
                    + " AND copyType='G' AND INSTITUTIONPROFILEID = "
                    + fromInstId;
        } else if (toIUserType == UserProfileQuery.ADMIN_USER_TYPE) {
            // Setup SQL for Admin routing
            sql = "Select dealId, copyId from Deal where administratorid="
                    + fromUserId
                    + " AND copyType='G' AND INSTITUTIONPROFILEID = "
                    + fromInstId;
        } else if (toIUserType == UserProfileQuery.FUNDER_USER_TYPE) {
            // Setup SQL for Funder Routing
            sql = "Select dealId, copyId from Deal where funderProfileId="
                    + fromUserId
                    + " AND copyType='G'AND INSTITUTIONPROFILEID = "
                    + fromInstId;
        }

        int key = jExec.execute(sql);
        Deal currDeal = null;
        MasterDeal md = null;
        Collection copyIds = null;
        Iterator iCopy = null;
        int copyId = 0;
        while (jExec.next(key)) 
        {
            int dealId = jExec.getInt(key, 1);
            copyId = jExec.getInt(key, 2);
            logger.info("Task Reassignment dealId: " + dealId
                    + " copyId: " + copyId);

            srk = changeUserProfile(fromInstId, srk);

            srk.getExpressState().setDealInstitutionId(fromInstId);
            currDeal = new Deal(srk, null, dealId, copyId);

            int currentAdminId = currDeal.getAdministratorId();
            int currentFunderId = currDeal.getFunderProfileId();

            //set the new user value in the appropriate field of the deal 
            //will invoke load-balanced admin/funder finder on underwriter change 
            if (toIUserType == UserProfileQuery.UNDERWRITER_USER_TYPE) {
                currDeal.setUnderwriterUserId(toUser
                        .getUserProfileId());
                setAdminAndFunder(srk, currDeal, toUser);
            } else if (toIUserType == UserProfileQuery.ADMIN_USER_TYPE) {
                currDeal.setAdministratorId(toUser
                        .getUserProfileId());
            } else if (toIUserType == UserProfileQuery.FUNDER_USER_TYPE) {
                currDeal.setFunderProfileId(toUser
                        .getUserProfileId());
            }
            //currDeal.ejbStore();

            //prepare to propagate the change to all copies of the current Deal
            DealPropagator dProp = new DealPropagator(currDeal, null);
            dProp.setUnderwriterUserId(currDeal.getUnderwriterUserId());
            dProp.setAdministratorId(currDeal.getAdministratorId());
            dProp.setFunderProfileId(currDeal.getFunderProfileId());
            dProp.ejbStore();


            if (toIUserType == UserProfileQuery.UNDERWRITER_USER_TYPE) {
                reassignAllDealTask(srk, currDeal, fromUserId,
                        currentAdminId, currentFunderId, currDeal
                                .getUnderwriterUserId(), currDeal
                                .getAdministratorId(), currDeal
                                .getFunderProfileId(),copyId);
            } else if (toIUserType == UserProfileQuery.ADMIN_USER_TYPE) {
                reassignAllDealTask(srk, currDeal, 0,
                        currentAdminId, 0, 0, currDeal
                                .getAdministratorId(), 0,copyId);
            } else if (toIUserType == UserProfileQuery.FUNDER_USER_TYPE) {
                reassignAllDealTask(srk, currDeal, 0, 0,
                        currentFunderId, 0, 0, currDeal
                                .getFunderProfileId(),copyId);
            }
       }
	   srk.getExpressState().cleanAllIds();
	   jExec.closeData(key);

        return true;
    }

    // New method for deal resurrect -- By BILLY 11Dec2001
    public boolean reassignDealForResurrect(Deal deal, int selectedUserId,
            SessionResourceKit srk) throws Exception {
        int currentUserId = deal.getUnderwriterUserId();
        UserProfile currUser = new UserProfile(srk);
        currUser.findByPrimaryKey(new UserProfileBeanPK(currentUserId, deal
                .getInstitutionProfileId()));

        UserProfile selectedUser = new UserProfile(srk);
        selectedUser.findByPrimaryKey(new UserProfileBeanPK(selectedUserId,
                deal.getInstitutionProfileId()));

        boolean isAssignUW = false;

        final int toIUserType = userQuery.getInternalUserTypeId(selectedUser.getUserTypeId());
        switch (toIUserType) {
    	    case UserProfileQuery.UNDERWRITER_USER_TYPE:
             isAssignUW = true;
    		    break;
    	    case UserProfileQuery.ADMIN_USER_TYPE:
    	    	break;
    	    case UserProfileQuery.FUNDER_USER_TYPE:		
    	    default:
             //	The user you assigned this task to is an invalid user type.
             logger.warn("@reassignDealForResurrect -- Invalid UserType, fallback to use the UW and AD of the original Deal.");
    		    return false;
        }

        // Need to hunt for the user if InActive -- By BILLY 01Nov2001
        UserProfile userToAssign = findActiveUser(selectedUser, srk, deal
                .getInstitutionProfileId());

        if (userToAssign == null) {
            logger.warn("@reassignDealForResurrect -- No Active User Found, fallback to use the UW and AD of the original Deal.");

            return false;
        }

        MasterDeal md = new MasterDeal(srk, null, deal.getDealId());
        Collection copyIds = md.getCopyIds();
        Iterator iCopy = copyIds.iterator();
        //int currentAdminId = deal.getAdministratorId();
        Deal currDeal = null;
        int dealId = deal.getDealId();
        int copyId = 0;

        for (; iCopy.hasNext();) {
            copyId = ((Integer) iCopy.next()).intValue();
            currDeal = new Deal(srk, null, dealId, copyId);

            if (isAssignUW) {
                // logger.debug("BILLY ===> @reassignDealForResurrect the
                // deal.UW " + dealId + " to user " +
                // userToAssign.getUserProfileId());
                //currDeal.setUnderwriterUserId(userToAssign.getUserProfileId());
                setAdminAndFunder(srk, currDeal, userToAssign);

                // logger.debug("BILLY ===> @reassignDealForResurrect the
                // deal.AD After setAdmin " + dealId + " to user " +
                // currDeal.getAdministratorId());
            } else {
                // logger.debug("BILLY ===> @reassignDealForResurrect the
                // deal.AD " + dealId + " to user " +
                // userToAssign.getUserProfileId());
                currDeal.setAdministratorId(userToAssign.getUserProfileId());
            }

            currDeal.ejbStore();
        }

        return true;
    }

	private void reassignAllCopies(SessionResourceKit srk, Deal currDeal,
			int currentUserId, UserProfile userToAssign, int toIUserType)
			throws Exception {

		int currentAdminId = currDeal.getAdministratorId();
		logger.debug("TRH@CurrentAdminId: " + currentAdminId);

		int currentFunderId = currDeal.getFunderProfileId();
		logger.debug("TRH@CurrentFunderId: " + currentFunderId);

		int dealId = currDeal.getDealId();
		logger.debug("TRH@DealId:" + dealId);

		MasterDeal md = new MasterDeal(srk, null, currDeal.getDealId());
		Collection copyIds = md.getCopyIds();
		Iterator iCopy = copyIds.iterator();
		int copyId = 0;
		for (; iCopy.hasNext();) {
			copyId = ((Integer) iCopy.next()).intValue();
			logger.debug("TRH@CopyId: " + copyId);
			currDeal = new Deal(srk, null, dealId, copyId);

			switch (toIUserType) {
			case UserProfileQuery.UNDERWRITER_USER_TYPE:
				// logger.debug("BILLY ===> Assign the deal.UW " + dealId +" to user " + userToAssign.getUserProfileId());
				setAdminAndFunder(srk, currDeal, userToAssign);

				// logger.debug("BILLY ===> Assign the deal.AD After setAdminAndFunder " + dealId + " to user " +currDeal.getAdministratorId());
				break;
			case UserProfileQuery.ADMIN_USER_TYPE:
				// logger.debug("BILLY ===> Assign the deal.AD " + dealId +" to user " + userToAssign.getUserProfileId());
				currDeal.setAdministratorId(userToAssign.getUserProfileId());
				break;
			case UserProfileQuery.FUNDER_USER_TYPE:
				// logger.debug("BILLY ===> Assign the deal.FUNDER " + dealId+" to user " + userToAssign.getUserProfileId());
				currDeal.setFunderProfileId(userToAssign.getUserProfileId());
				break;
			}
			currDeal.ejbStore();
		}

		// Resign all task and create Deal History records
		switch (toIUserType) {
		case UserProfileQuery.UNDERWRITER_USER_TYPE:
			reassignAllDealTask(srk, currDeal, currentUserId, currentAdminId,
					currentFunderId, currDeal.getUnderwriterUserId(), 
					currDeal.getAdministratorId(), currDeal.getFunderProfileId(), copyId);
			break;
		case UserProfileQuery.ADMIN_USER_TYPE:
			reassignAllDealTask(srk, currDeal, 0, currentAdminId, 0, 0,
					currDeal.getAdministratorId(), 0,copyId);
			break;
		case UserProfileQuery.FUNDER_USER_TYPE:
			reassignAllDealTask(srk, currDeal, 0, 0, currentFunderId, 0, 0,
					currDeal.getFunderProfileId(),copyId);
			break;
		}
	}

    public class CbInfo {
		private ComboBox cbReassign = null;
		private ComboBox cbReassignTo = null;
		private ComboBox cbMultyDealFrom = null;
		private ComboBox cbMultyDealTo = null;

		public CbInfo(ComboBox cbReassigna, ComboBox cbReassignToa,
				ComboBox cbMultyDealFroma, ComboBox cbMultyDealToa) {
			cbReassign = cbReassigna;
			cbReassignTo = cbReassignToa;
			cbMultyDealFrom = cbMultyDealFroma;
			cbMultyDealTo = cbMultyDealToa;
		}

		public ComboBox getCbReassign() {
			return cbReassign;
		}

		public ComboBox getCbReassignTo() {
			return cbReassignTo;
		}

		public ComboBox getCbMultyDealFrom() {
			return cbMultyDealFrom;
		}

		public ComboBox getCbMultyDealTo() {
			return cbMultyDealTo;
		}
	}
    
    public SessionResourceKit changeUserProfile(int fromInstId,
            SessionResourceKit srk)  {

        try {
            UserProfile currentUserProfile = new UserProfile(srk);
            currentUserProfile = currentUserProfile
                    .findByPrimaryKey(new UserProfileBeanPK(srk.getExpressState().getUserProfileId(),
                            fromInstId));
        } catch (RemoteException e) {
            logger.debug("change userprofile ERROR: ");
            e.printStackTrace();
        } catch (FinderException e) {
            String loginID = theSessionState.getUserLogin();
            try {
                UserProfile newUserProfile = new UserProfile(srk);
                newUserProfile.findByChangeUserProfile(loginID, fromInstId);
                srk.getExpressState().setUserIds(newUserProfile.getUserProfileId(),
                        newUserProfile.getInstitutionId());
            } catch (RemoteException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            } catch (FinderException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }

        return srk;
    }
}
