package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 * <p>Title: doAdjudicationResponseBNCModel</p>
 * 
 * <p>Description: Interface class for doAdjudicationResponseBNCModelImpl</p>
 * 
 * <p> Copyright: Filogix Inc. (c) 2006 </p>
 *
 * <p> Company: Filogix Inc.</p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.0
 */
public interface doAdjudicationResponseBNCModel extends QueryModel, SelectQueryModel
{

    /**
     *
     *
     */
    public java.math.BigDecimal getDfCopyId();


    /**
     *
     *
     */
    public void setDfCopyId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfResponseId();
	/**
	 *
	 *
	 */
	public void setDfResponseId(java.math.BigDecimal value);
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRequestId();
	/**
	 *
	 *
	 */
	public void setDfRequestId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public String getDfResponseDate();
	/**
	 *
	 *
	 */
	public void setDfResponseDate(String value);

	/**
	 *
	 *
	 */
	public String getDfRequestDate();
	/**
	 *
	 *
	 */
	public void setDfRequestDate(String value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRequestStatusId();
	/**
	 *
	 *
	 */
	public void setDfRequestStatusId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public String getDfRequestStatusDesc();
	/**
	 *
	 *
	 */
	public void setDfRequestStatusDesc(String value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfGlobalUsedCreditBureauNameId();
	/**
	 *
	 *
	 */
	public void setDfGlobalUsedCreditBureauNameId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public String getDfGlobalUsedCreditBureauName();
	/**
	 *
	 *
	 */
	public void setDfGlobalUsedCreditBureauName(String value);

	/**
	 *
	 *
	 */
	public String getDfGlobalCreditBureauScore();
	/**
	 *
	 *
	 */
	public void setDfGlobalCreditBureauScore(String value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfGlobalRiskRating();
	/**
	 *
	 *
	 */
	public void setDfGlobalRiskRating(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public String getDfGlobalInternalScore();
	/**
	 *
	 *
	 */
	public void setDfGlobalInternalScore(String value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAdjudicationStatusId();
	/**
	 *
	 *
	 */
	public void setDfAdjudicationStatusId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public String getDfAdjudicationDesc();
	/**
	 *
	 *
	 */
	public void setDfAdjudicationDesc(String value);


	/**
	 *
	 *
	 */
	public String getDfProductCode();
	/**
	 *
	 *
	 */
	public void setDfProductCode(String value);

	/**
	 *
	 *
	 */
	public String getDfMIDecision();
	/**
	 *
	 *
	 */
	public void setDfMIDecision(String value);

	/**
	 *
	 *
	 */
	public String getDfAmountInsured();
	/**
	 *
	 *
	 */
	public void setDfAmountInsured(String value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPrimaryApplicantRiskId();
	/**
	 *
	 *
	 */
	public void setDfPrimaryApplicantRiskId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public String getDfPrimaryApplicantRisk();
	/**
	 *
	 *
	 */
	public void setDfPrimaryApplicantRisk(String value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSecondaryApplicantRiskId();
	/**
	 *
	 *
	 */
	public void setDfSecondaryApplicantRiskId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public String getDfSecondaryApplicantRisk();
	/**
	 *
	 *
	 */
	public void setDfSecondaryApplicantRisk(String value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMarketRiskId();
	/**
	 *
	 *
	 */
	public void setDfMarketRiskId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public String getDfMarketRisk();
	/**
	 *
	 *
	 */
	public void setDfMarketRisk(String value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyRiskId();
	/**
	 *
	 *
	 */
	public void setDfPropertyRiskId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public String getDfPropertyRisk();
	/**
	 *
	 *
	 */
	public void setDfPropertyRisk(String value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNeighbourhoodRiskId();
	/**
	 *
	 *
	 */
	public void setDfNeighbourhoodRiskId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public String getDfNeighbourhoodRisk();
	/**
	 *
	 *
	 */
	public void setDfNeighbourhoodRisk(String value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfGlobalNetWorth();
	/**
	 *
	 *
	 */
	public void setDfGlobalNetWorth(java.math.BigDecimal value);
    
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFRESPONSEID="dfResponseId";
	public static final String FIELD_DFREQUESTID="dfRequestId";
	public static final String FIELD_DFRESPONSEDATE="dfResponseDate";
	public static final String FIELD_DFREQUESTDATE="dfRequestDate";
	public static final String FIELD_DFREQUESTSTATUSID="dfRequestStatusId";
	public static final String FIELD_DFREQUESTSTATUSDESC="dfRequestStatusDesc";
	
	public static final String FIELD_DFGLOBALUSEDCREDITBUREAUNAMEID="dfGlobalUsedCreditBureauNameId";
	public static final String FIELD_DFGLOBALUSEDCREDITBUREAUNAME="dfGlobalUsedCreditBureauName";
	public static final String FIELD_DFGLOBALCREDITBUREAUSCORE="dGlobalCreditBureauScore";
	public static final String FIELD_DFGLOBALRISKRATING="dfGlobalRiskRating";
	public static final String FIELD_DFGLOBALINTERNALSCORE="dfGlobalInternalScore";
	public static final String FIELD_DFADJUDICATIONSTATUSID="dfAdjudicationStatusId";
	public static final String FIELD_DFADJUDICATIONDESC="dfAdjudicationStatus";

	public static final String FIELD_DFPRODUCTCODE="dfProductCode";
	public static final String FIELD_DFMIDECISION="dfMIDecision";
	public static final String FIELD_DFAMOUNTINSURED="dfAmountInsured";
	public static final String FIELD_DFPRIMARYAPPLICANTRISKID="dfPrimaryApplicantRiskId";
	public static final String FIELD_DFPRIMARYAPPLICANTRISK="dfPrimaryApplicantRisk";
	public static final String FIELD_DFSECONDARYAPPLICANTRISKID="dfSecondaryApplicantRiskId";
	public static final String FIELD_DFSECONDARYAPPLICANTRISK="dfSecondaryApplicantRisk";
	public static final String FIELD_DFMARKETRISKID="dfMarketRiskId";
	public static final String FIELD_DFMARKETRISK="dfMarketRisk";
	public static final String FIELD_DFPROPERTYRISKID="dfPropertyRiskId";
	public static final String FIELD_DFPROPERTYRISK="dfPropertyRisk";
	public static final String FIELD_DFNEIGHBOURHOODRISKID="dfNeighbourhoodRiskId";
	public static final String FIELD_DFNEIGHBOURHOODRISK="dfNeighbourhoodRisk";
	public static final String FIELD_DFGLOBALNETWORTH="dfGlobalNetWorth";
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

