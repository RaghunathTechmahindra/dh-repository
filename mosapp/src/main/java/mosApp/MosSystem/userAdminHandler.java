package mosApp.MosSystem;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.GroupProfile;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.ContactPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.security.PasswordEncoder;

import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;

import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;

import com.basis100.log.SysLogger;

import com.basis100.picklist.BXResources;

import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;

import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.util.StringTokenizer2;
import com.iplanet.jato.util.TypeConverter;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HtmlDisplayFieldBase;
import com.iplanet.jato.view.html.ListBox;
import com.iplanet.jato.view.html.OptionList;

import config.UserAdminConfigManager;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;

import java.text.SimpleDateFormat;

import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;


/**
 * Page Generation Details  PageEntry.pageCondition1 contains indication of User Pressed Submit
 * Button:  true - Submit pressed. false - Submit button is not pressed.  PageEntry.pageCondition2
 * contains indication of selecting a user and Go button is pressed:  true - Go button is pressed.
 * false - Go button is not pressed.   PageEntry.pageCondition3 contains indication of adding new
 * user button is pressed:  true - ADD NEW USER button is pressed. false - ADD NEW USER button is
 * not pressed.  This property is used during conditional generation as follows:  select user and
 * go button - generated if (pageCondition3 == true) select user and go button - generate if
 * (pageCondition3 == false)  PageEntry.pageCondition4 contains indication of displaying screen
 * access for the user:  true - Screen Access button is pressed. false - Screen Access button is
 * not pressed.
 */
public class userAdminHandler
  extends PageHandlerCommon
  implements Cloneable, Sc
{
  // key for storing fields edit buffer in page state table
  private static final String EDIT_BUFFER = "EDIT_BUFFER";
  private static final String CONFIGMGR = "CONFIGMGR";

  /**
   *
   *
   */
  public userAdminHandler cloneSS()
  {
    return (userAdminHandler) super.cloneSafeShallow();
  }

  /////////////////////////////////////////////////////////////////////
  // This method is used to populate the fields in the header
  // with the appropriate information
  public void populatePageDisplayFields()
  {
    PageEntry pg = getTheSessionState().getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    populatePageShellDisplayFields();

    /// new
    populateTaskNavigator(pg);

    //populatePageDealSummarySnapShot();
    /// end new
    populatePreviousPagesLinks();

    EditFieldsBuffer efb = (EditFieldsBuffer) pst.get(EDIT_BUFFER);
    setCbxDisplayFields();
    setCbxDisplayFieldsForUsers();
    efb.populateFields(getCurrNDPage());

    //--Ticket #239: Display or not display Submit Button--start--//.
    populateDefaultValues(getCurrNDPage());

    //--Ticket #239--end--//.
    setJSValidation();
    prepareJScriptData();
    prepareJScriptForHighJointApp();

    boolean pgcondition4 = pg.getPageCondition4();

    if (pgcondition4)
    {
      Vector results = null;
      Object obj = pst.get("RESULTS");

      if (obj instanceof Vector)
      {
        results = (Vector) pst.get("RESULTS");
      }
      else
      {
        results = new Vector();
      }

      //logger.debug("---- @userAdminHandler ---- Sending this number to
      // JScript = " + results.size() );
      prepareJScriptForPageAccess(results.size());
    }

    //--> Setup all disabled fields -- By Billy 04March2003
    setupDisabledFields(getCurrNDPage());

    //=====================================================
  }

  ///////////////////////////////////////////////////////////////////////
  public void setJSValidation()
  {
    // Validate # of Dependants
    HtmlDisplayFieldBase df = null;
    String theExtraHtml = null;
    ViewBean thePage = getCurrNDPage();
    df = (HtmlDisplayFieldBase) thePage.getDisplayField("tbLineOfBusiness1");
    theExtraHtml = "onBlur=\"isFieldDecimal(2);\"";
    df.setExtraHtml(theExtraHtml);
    df = (HtmlDisplayFieldBase) thePage.getDisplayField("tbLineOfBusiness2");
    theExtraHtml = "onBlur=\"isFieldDecimal(2);\"";
    df.setExtraHtml(theExtraHtml);
    df = (HtmlDisplayFieldBase) thePage.getDisplayField("tbLineOfBusiness3");
    theExtraHtml = "onBlur=\"isFieldDecimal(2);\"";
    df.setExtraHtml(theExtraHtml);
    df = (HtmlDisplayFieldBase) thePage.getDisplayField("tbWorkPhoneNumberAreaCode");
    theExtraHtml = "onBlur=\"isValidAreaCode();\"";
    df.setExtraHtml(theExtraHtml);
    df = (HtmlDisplayFieldBase) thePage.getDisplayField("tbWorkPhoneNumFirstThreeDigits");
    theExtraHtml = "onBlur=\"isValidPhoneNumber();\"";
    df.setExtraHtml(theExtraHtml);
    df = (HtmlDisplayFieldBase) thePage.getDisplayField("tbWorkPhoneNumLastFourDigits");
    theExtraHtml = "onBlur=\"isValidPhoneNumber1();\"";
    df.setExtraHtml(theExtraHtml);
    df = (HtmlDisplayFieldBase) thePage.getDisplayField("tbWorkPhoneNumExtension");
    theExtraHtml = "onBlur=\"isValidExtensionPhoneNum();\"";
    df.setExtraHtml(theExtraHtml);
    df = (HtmlDisplayFieldBase) thePage.getDisplayField("tbFaxNumberAreaCode");
    theExtraHtml = "onBlur=\"isValidAreaCode();\"";
    df.setExtraHtml(theExtraHtml);
    df = (HtmlDisplayFieldBase) thePage.getDisplayField("tbFaxNumFirstThreeDigits");
    theExtraHtml = "onBlur=\"isValidPhoneNumber();\"";
    df.setExtraHtml(theExtraHtml);
    df = (HtmlDisplayFieldBase) thePage.getDisplayField("tbFaxNumLastFourDigits");
    theExtraHtml = "onBlur=\"isValidPhoneNumber1();\"";
    df.setExtraHtml(theExtraHtml);
    df = (HtmlDisplayFieldBase) thePage.getDisplayField("tbUserTitle");
    theExtraHtml = "onBlur=\"isAlphabet();\"";
    df.setExtraHtml(theExtraHtml);
    df = (HtmlDisplayFieldBase) thePage.getDisplayField("tbUserFirstName");
    theExtraHtml = "onBlur=\"isAlphabet();\"";
    df.setExtraHtml(theExtraHtml);
    df = (HtmlDisplayFieldBase) thePage.getDisplayField("tbUserLastName");
    theExtraHtml = "onBlur=\"isAlphabet();\"";
    df.setExtraHtml(theExtraHtml);
    df = (HtmlDisplayFieldBase) thePage.getDisplayField("tbUserInitialName");
    theExtraHtml = "onBlur=\"isAlphabet();\"";
    df.setExtraHtml(theExtraHtml);
    df = (HtmlDisplayFieldBase) thePage.getDisplayField("tbEmailAddress");
    theExtraHtml = "onBlur=\"isEmail();\"";
    df.setExtraHtml(theExtraHtml);

    // Reset Onchange on for cbUserTypes -- bug fix by BILLY 1Nov2001
    df = (HtmlDisplayFieldBase) thePage.getDisplayField("cbUserTypes");
    theExtraHtml = "";
    df.setExtraHtml(theExtraHtml);
  }

  //////////////////////////////////////////////////////////////////////////
  public String removeJScriptFunction(String origHtmlStr)
  {
    PageEntry pg = getTheSessionState().getCurrentPage();
    //Hashtable pst = pg.getPageStateTable();
    boolean pgcondition4 = pg.getPageCondition4();
    logger.debug("---- userAdminHandler ---> pgcondition4 = " + pgcondition4);

    String resultStr = origHtmlStr;

    if (!pgcondition4)
    {
      String strToReplace = pg.getPageState1();

      //logger.debug("BILLY ===> the Original HTML is = " + origHtmlStr);
      //logger.debug("BILLY ===> the strToReplace is = " + strToReplace);
      //logger.debug("---- Toni ---> the replacement String = " +
      // strToReplace);
      if ((strToReplace != null) && !strToReplace.trim().equals(""))
      {
        resultStr = StringTokenizer2.replace(resultStr, strToReplace, " ");
      }
    }

    //logger.debug("BILLY ===> the Result HTML is = " + resultStr);
    return resultStr;
  }

  //////////////////////////////////////////////////////////////////////////
  //=================================================
  //--> CRF#37 :: Source/Firm to Group routing for TD
  //=================================================
  //--> Modified to support Multiple Groups assignment
  //--> By Billy 12Aug2003
  public void setCbxDisplayFields()
  {
    ViewBean currNDPage = getCurrNDPage();
    PageEntry pg = getTheSessionState().getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    ComboBox selectgroup = null;
    ComboBox cbbranches = null;
    ComboBox cbregions = null;

    //ComboBox cbGroups = null;
    ListBox lbGroups = null;

    try
    {
      boolean condition2 = pg.getPageCondition2();
      //boolean condition3 = pg.getPageCondition3();
      //boolean condition4 = pg.getPageCondition4();
      QueryModelBase thdo1 =
        (QueryModelBase) (RequestManager.getRequestContext().getModelManager().getModel(doUserAdminGetRegionsModel.class));
      thdo1.clearUserWhereCriteria();

      QueryModelBase thdo2 =
        (QueryModelBase) (RequestManager.getRequestContext().getModelManager().getModel(doUserAdminGetBranchesModel.class));
      thdo2.clearUserWhereCriteria();

      QueryModelBase thdo3 =
        (QueryModelBase) (RequestManager.getRequestContext().getModelManager().getModel(doUserAdminGetGroupsModel.class));
      thdo3.clearUserWhereCriteria();
      cbregions = (ComboBox) currNDPage.getChild("cbRegionNames");
      cbbranches = (ComboBox) currNDPage.getChild("cbBranchNames");

      //cbGroups =(ComboBox) currNDPage.getChild("cbGroupProfileName");
      lbGroups = (ListBox) currNDPage.getChild("lbGroups");

      UserAdminEditBuffer efb = (UserAdminEditBuffer) pst.get(EDIT_BUFFER);
      String institutionprofileid = efb.getFieldValue("cbInstitutionNames");
      String regionprofileid = efb.getFieldValue("cbRegionNames");
      String branchprofileid = efb.getFieldValue("cbBranchNames");

      //--> Get the first assigned group
      String groupprofileid = "";

      if ((efb.getFieldValues("lbGroups") != null) && (efb.getFieldValues("lbGroups").length > 0))
      {
        groupprofileid = efb.getFieldValues("lbGroups")[0].toString();
      }

      if (!condition2)
      {
        //logger.debug("@userAdminHandler ---- setCbxDisplayFields ----
        // efinstitutin = " + efb.getFieldValue("cbInstitutionNames"));
        //logger.debug("@userAdminHandler ---- setCbxDisplayFields ----
        // efbgroup = " + efb.getFieldValue("cbGroupProfileName"));
        //logger.debug("@userAdminHandler ---- setCbxDisplayFields ----
        // efbbranch = " + efb.getFieldValue("cbBranchNames"));
        //logger.debug("@userAdminHandler ---- setCbxDisplayFields ----
        // efbregion = " + efb.getFieldValue("cbRegionNames"));
        if (regionprofileid.length() == 0)
        {
          thdo1.addUserWhereCriterion("dfInstitutionProfileId", "=", new Integer(-1));
        }
        else
        {
          thdo1.addUserWhereCriterion("dfInstitutionProfileId", "=",
            new Integer(getIntValue(institutionprofileid)));
        }

        if (branchprofileid.length() == 0)
        {
          thdo2.addUserWhereCriterion("dfRegionProfileId", "=", new Integer(-1));
        }
        else
        {
          thdo2.addUserWhereCriterion("dfRegionProfileId", "=",
            new Integer(getIntValue(regionprofileid)));
        }

        if (groupprofileid.length() == 0)
        {
          thdo3.addUserWhereCriterion("dfBranchProfileId", "=", new Integer(-1));
        }
        else
        {
          thdo3.addUserWhereCriterion("dfBranchProfileId", "=",
            new Integer(getIntValue(branchprofileid)));
        }

        cbregions.getOptions().populate(RequestManager.getRequestContext());
        cbbranches.getOptions().populate(RequestManager.getRequestContext());
        lbGroups.getOptions().populate(RequestManager.getRequestContext());
      }

      if ((pst != null) && condition2)
      {
        //EditFieldsBuffer efb =
        // (EditFieldsBuffer)pst.get(EDIT_BUFFER);
        //logger.debug("---- Mikhael ---- condition2 = true");
        //logger.debug("@userAdminHandler ---- setCbxDisplayFields ----
        // institutionprofileid = " + institutionprofileid);
        //logger.debug("@userAdminHandler ---- setCbxDisplayFields ----
        // regionprofileid = " + regionprofileid);
        //logger.debug("@userAdminHandler ---- setCbxDisplayFields ----
        // branchprofileid = " + branchprofileid);
        //logger.debug("@userAdminHandler ---- setCbxDisplayFields ----
        // groupprofileid = " + groupprofileid);
        thdo1.addUserWhereCriterion("dfInstitutionProfileId", "=",
          new Integer(getIntValue(institutionprofileid)));
        thdo2.addUserWhereCriterion("dfRegionProfileId", "=",
          new Integer(getIntValue(regionprofileid)));
        thdo3.addUserWhereCriterion("dfBranchProfileId", "=",
          new Integer(getIntValue(branchprofileid)));
        cbregions.getOptions().populate(RequestManager.getRequestContext());
        cbbranches.getOptions().populate(RequestManager.getRequestContext());
        lbGroups.getOptions().populate(RequestManager.getRequestContext());
      }

      // get the values from the data object
      ////////
      selectgroup = (ComboBox) currNDPage.getChild("cbUserNames");

      //selectgroup.doAutoFill();
      selectgroup.getOptions().populate(RequestManager.getRequestContext());
    }
    catch (Exception e)
    {
      logger.error("Error @UserAdminHandler.setCbxDisplayFields. ");
      logger.error(e);
      setStandardFailMessage();
    }
  }

  //////////////////////////////////////////////////////////////////////////
  public void setCbxDisplayFieldsForUsers()
  {
    ViewBean currNDPage = getCurrNDPage();
    ComboBox cbpartner = null;
    ComboBox cbmanager = null;
    ComboBox cbjointapp = null;
    ComboBox cbhighapp = null;
    ComboBox cbstandin = null;
    cbpartner = (ComboBox) currNDPage.getDisplayField("cbPartnerProfileNames");
    cbmanager = (ComboBox) currNDPage.getDisplayField("cbManagers");
    cbjointapp = (ComboBox) currNDPage.getDisplayField("cbJointApprovers");
    cbhighapp = (ComboBox) currNDPage.getDisplayField("cbHigherApprovers");
    cbstandin = (ComboBox) currNDPage.getDisplayField("cbStandIn");

    //cbpartner.doAutoFill();
    cbpartner.getOptions().populate(RequestManager.getRequestContext());

    //cbmanager.doAutoFill();
    cbmanager.getOptions().populate(RequestManager.getRequestContext());

    //cbjointapp.doAutoFill();
    cbjointapp.getOptions().populate(RequestManager.getRequestContext());

    //cbhighapp.doAutoFill();
    cbhighapp.getOptions().populate(RequestManager.getRequestContext());

    //cbstandin.doAutoFill();
    cbstandin.getOptions().populate(RequestManager.getRequestContext());
  }

  //////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////
  //
  // Setup data objects associated with page - e.g. set criteria for deal snap
  // shot, etc.
  //
  public void setupBeforePageGeneration()
  {
    PageEntry pg = getTheSessionState().getCurrentPage();
    int instId = pg.getDealInstitutionId();
    srk.getExpressState().setDealInstitutionId(instId);
    
    if (pg.getSetupBeforeGenerationCalled() == true)
    {
      return;
    }

    pg.setSetupBeforeGenerationCalled(true);

    //// CUTOM STUFF GOES HERE (if any)
    Hashtable pst = pg.getPageStateTable();
    UserAdminEditBuffer efb = null;
    setRowGenerator2DOForNRows(1);

    UserProfile up = null;

    if (pst == null)
    {
      pst = new Hashtable();
      pg.setPageStateTable(pst);
      efb = new UserAdminEditBuffer("UserAdminParms", logger);
      pst.put(EDIT_BUFFER, efb);
      pst.put("RESULT", new Vector());

      // save the user admin page as saved page
      getSavedPages().setNextPage(pg);
    }

    //--> Set up the Config Manager
    //--> By Billy 04March2003
    pst.put(CONFIGMGR,
      UserAdminConfigManager.getInstance(logger, theSessionState.getSessionUserType()));

    //==============================
    //  get the edit buffer
    efb = (UserAdminEditBuffer) pst.get(EDIT_BUFFER);

    String contid = null;

    // check if the accessScreen Button is pressed
    boolean condition1 = pg.getPageCondition1();
    boolean condition2 = pg.getPageCondition2();
    boolean condition3 = pg.getPageCondition3();
    boolean condition4 = pg.getPageCondition4();
    String update = pg.getPageState1();

    //--> Added protection checking here -- By Billy 19Aug2002
    if (update == null)
    {
      update = "";
    }

    //========================================================
    logger.debug(" @userAdminHandler --> getSelectedContactId() == " + getSelectedContactId(efb));

    //--> Clear Display Buffers if user pick NO User from and it is not
    // adding user
    //--> Modified by Billy 09Dec2002
    //--> Modified again by Billy to add checking if New User
    contid = getSelectedContactId(efb);

    if ((contid == null) && (condition3 == false))
    {
      efb = new UserAdminEditBuffer("UserAdminParms", logger);
      pst.put(EDIT_BUFFER, efb);
    }

    //--> Re-Set the Contact ID to Page Session in order to prevent Screen
    // Out of sync. problem
    //--> By Billy 18Aug2003
    this.getCurrNDPage().setPageSessionAttribute("CURRENT_CONTACTID", new Integer(-1));

    //======================================================================================
    //Check if GO button pressed and no Error ==> Get display data from DB
    if (((condition2 == true) && (contid != null) && !update.equals("ERROR")))
    {
      // get the values.
      int contactid = getIntValue(contid);
      int copyid = getContactCopyId(contactid);
      logger.debug(" @userAdminHandler --> selected contact id == " + contactid);

      SessionResourceKit srk = getSessionResourceKit();

      try
      {
        Contact contact = new Contact(srk);
        contact.findByPrimaryKey(new ContactPK(contactid, copyid));
        up = new UserProfile(srk);
        up = up.findByContactId(contactid, pg.getDealInstitutionId());
        efb.setFieldValue("cbUserNames", "" + contactid);
        efb.setFieldValue("cbProfileStatus", "" + up.getProfileStatusId());

        //-- ========== SCR#311 begins ========== --//
        //-- by Neil on Dec/07/2004
        int userTypeId = up.getUserTypeId();
        int languageId = getTheSessionState().getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(
                theSessionState.getDealInstitutionId(),"USERTYPE", languageId);

        Object[] o = c.toArray();
        String[] theVal = new String[2];
        OptionList optionList =
          ((pgUserAdministrationViewBean) getCurrNDPage()).getCbUserTypes().getOptions();
        optionList.clear();

        //-- Passive user can changed to any type. --//
        if (userTypeId == 1)
        {
          for (int i = 1; i <= 13; i++)
          {
            theVal = (String[]) (o[i]);
            optionList.add(theVal[1], theVal[0]);
          }
        }

        //-- Funder Group --//
        if (userTypeId == 2)
        {
          theVal = (String[]) (o[2]);
          optionList.add(theVal[1], theVal[0]);
        }

        //-- Administrator Group --//
        if ((userTypeId >= 3) && (userTypeId <= 5))
        {
          for (int i = 3; i <= 5; i++)
          {
            theVal = (String[]) (o[i]);
            optionList.add(theVal[1], theVal[0]);
          }
        }

        //-- Underwriter Group --//
        if ((userTypeId >= 6) && (userTypeId <= 8))
        {
          for (int i = 6; i <= 8; i++)
          {
            theVal = (String[]) (o[i]);
            optionList.add(theVal[1], theVal[0]);
          }
        }

        //-- Sys. Administrator Group --//
        if ((userTypeId >= 9) && (userTypeId <= 13))
        {
          for (int i = 9; i <= 13; i++)
          {
            theVal = (String[]) (o[i]);
            optionList.add(theVal[1], theVal[0]);
          }
        }

        //-- ========== SCR#311 ends ========== --//
        //                efb.setFieldValue("cbUserTypes", "" + up.getUserTypeId());
        efb.setFieldValue("cbUserTypes", "" + userTypeId);
        efb.setFieldValue("cbLanguages", up.getBilingual());

        //-- ========== SCR#537 begins ========== --//
        //-- by Neil on Dec/02/2004
        efb.setFieldValue("cbTaskAlert", up.getTaskAlert());

        //-- ========== SCR#537 ends ========== --//
        efb.setFieldValue("cbPartnerProfileNames", "" + up.getPartnerUserId());
        efb.setFieldValue("cbStandIn", "" + up.getStandInUserId());
        efb.setFieldValue("cbHigherApprovers", "" + up.getSecondApproverUserId());
        efb.setFieldValue("cbJointApprovers", "" + up.getJointApproverUserId());
        efb.setFieldValue("cbManagers", "" + up.getManagerId());

        //=================================================
        //--> CRF#37 :: Source/Firm to Group routing for TD
        //=================================================
        //--> Modified to support Multiple Groups assignment
        //--> By Billy 12Aug2003
        //efb.setFieldValue("cbGroupProfileName", "" +
        // up.getGroupProfileId());
        efb.setFieldValues("lbGroups", up.getGroupIdsInObjArray());
        logger.debug("@userAdminHandler ---> up.getGroupIdsInObjArray() === "
          + up.getGroupIdsInObjArray());

        //--> Assume all the assigned groups should be from the same
        // Branch
        if ((up.getGroupIdsInObjArray() != null) && (up.getGroupIdsInObjArray().length > 0))
        {
          getUserAdminInfo(up.getGroupIdsInObjArray()[0].toString(), efb);
        }

        //=================================================================
        //			logger.debug("@userAdminHandler ---> regionid === " +
        // up.getRegionId() );
        efb.setFieldValue("cbSalutations", "" + contact.getSalutationId());
        efb.setFieldValue("tbUserTitle", contact.getContactJobTitle());
        efb.setFieldValue("tbUserFirstName", contact.getContactFirstName());
        efb.setFieldValue("tbUserInitialName", contact.getContactMiddleInitial());
        efb.setFieldValue("tbUserLastName", contact.getContactLastName());

        String[] phoneparts = phoneSplit(contact.getContactPhoneNumber());
        efb.setFieldValue("tbWorkPhoneNumberAreaCode", phoneparts[0]);
        efb.setFieldValue("tbWorkPhoneNumFirstThreeDigits", phoneparts[1]);
        efb.setFieldValue("tbWorkPhoneNumLastFourDigits", phoneparts[2]);
        efb.setFieldValue("tbWorkPhoneNumExtension", contact.getContactPhoneNumberExtension());

        String[] faxparts = phoneSplit(contact.getContactFaxNumber());
        efb.setFieldValue("tbFaxNumberAreaCode", faxparts[0]);
        efb.setFieldValue("tbFaxNumFirstThreeDigits", faxparts[1]);
        efb.setFieldValue("tbFaxNumLastFourDigits", faxparts[2]);
        efb.setFieldValue("tbEmailAddress", contact.getContactEmailAddress());
        efb.setFieldValue("tbLoginId", up.getUserLogin());
        efb.setFieldValue("hdLoginId", up.getUserLogin());
        efb.setFieldValue("tbBusinessId", up.getUpBusinessId());
        efb.setFieldValue("tbPassword", "");
        efb.setFieldValue("tbVerifyPassword", "");

        //=====================================================================
        setBusinessLimit(efb, up.getUserProfileId());
        pst.put("USERPROFILEID", "" + up.getUserProfileId());

        //Added new fields -- By BILLY 12April2002
        if (up.isPasswordReset() == true)
        {
          efb.setFieldValue("chForceToChangePW", "Y");
        }
        else
        {
          efb.setFieldValue("chForceToChangePW", "N");
        }

        if (up.isPasswordLocked() == true)
        {
          efb.setFieldValue("chLockPW", "Y");
        }
        else
        {
          efb.setFieldValue("chLockPW", "N");
        }

        if (up.isNewPassword() == true)
        {
          efb.setFieldValue("chNewPW", "Y");
        }
        else
        {
          efb.setFieldValue("chNewPW", "N");
        }

        //--Release2.1--//
        //--> Added Prefered Language ComboBox - By Billy 09Dec2002
        efb.setFieldValue("cbDefaultLanguage", "" + contact.getLanguagePreferenceId());

        //====================================
        //--> Clear the New Group field By Billy 28Aug2003
        efb.setFieldValue("tbNewGroupName", "");

        //================================================
        efb.setOriginalAsCurrent();

        //--> Set the Contact ID to Page Session in order to prevent
        // Screen Out of sync. problem
        //--> By Billy 18Aug2003
        this.getCurrNDPage().setPageSessionAttribute("CURRENT_CONTACTID",
          new Integer(up.getContactId()));

        //======================================================================================
      }
      catch (Exception e)
      {
        setStandardFailMessage();
        logger.error("Exception @UserAdminHandler.setupBeforePageGeneration:");
        logger.error(e.getMessage());
      }
    }

    //--Release2.1--//
    //--> Populate the Static fields in case tloggling language
    //--> by Billy 06Dec2002
    if ((efb != null) && ((contid = getSelectedContactId(efb)) != null))
    {
      try
      {
        //Get UserProfile Object if it's null
        if (up == null)
        {
          up = new UserProfile(srk);
          up = up.findByContactId(getIntValue(contid), pg.getDealInstitutionId());
        }

        Locale theLocale =
          new Locale(BXResources.getLocaleCode(theSessionState.getLanguageId()), "");
        SimpleDateFormat df = new SimpleDateFormat("MMM dd yyyy HH:mm:ss", theLocale);

        if (up.getLastLoginTime() != null)
        {
          efb.setFieldValue("stLastLoginTime", df.format(up.getLastLoginTime()));
        }
        else
        {
          efb.setFieldValue("stLastLoginTime", "--");
        }

        if (up.getLastLogoutTime() != null)
        {
          efb.setFieldValue("stLastLogoutTime", df.format(up.getLastLogoutTime()));
        }
        else
        {
          efb.setFieldValue("stLastLogoutTime", "--");
        }

        if (up.isUserLogOn())
        {
          efb.setFieldValue("stLoginFlag",
            BXResources.getGenericMsg("YES_LABEL", theSessionState.getLanguageId()));
        }
        else
        {
          efb.setFieldValue("stLoginFlag",
            BXResources.getGenericMsg("NO_LABEL", theSessionState.getLanguageId()));
        }
      }
      catch (Exception e)
      {
        setStandardFailMessage();
        logger.error("Exception @UserAdminHandler.setupBeforePageGeneration:");
        logger.error(e.getMessage());
      }
    }

    //=====================================================================================
    //				boolean condition4 = pg.getPageCondition4();
    Vector v = null;
    Object obj = pst.get("RESULTS");

    if (obj instanceof Vector)
    {
      v = (Vector) obj;
    }
    else
    {
      v = new Vector();
    }


    //if(v.size() == 0 && condition4 || condition1)
    //--> Bug Fix by Billy 20Nov2003
    if (condition4 && (!condition1 || (condition1 && update.equals("UPDATE"))))
    {
      //int numResults = 0;

      //logger.debug("BILLY ==> @userAdminHandler ---Inside condition4
      // ----");
      String usertypeid = efb.getFieldValue("cbUserTypes");
      logger.debug("@userAdminHandler (setupbeforepagegener)--- usertypeid == " + usertypeid);

      int userprofileid = getIntValue((String) pst.get("USERPROFILEID"));

      //						int userprofileid = up.getUserProfileId();
      logger.debug("@userAdminHandler (setupbeforepagegener)--- userProfileid == " + userprofileid);

      Vector vresult = getAccessScreens(getIntValue(usertypeid), userprofileid);

      if ((vresult == null) || (vresult.size() == 0))
      {
        //numResults = 0;
        pst.put("RESULTS", new Vector());
        pg.setPageCondition4(false);

        if (!condition3)
        {
          setActiveMessageToAlert(BXResources.getSysMsg("USER_ADMIN_USER_TYPE_IS_REQUIRED",
              theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
        }
      }
      else
      {
        pst.put("RESULTS", vresult);
      }
    }
  }

  ////////////////////////////////////////////////////////////////////////
  public Vector getAccessScreens(int usertypeid, int userprofileid)
  {
    Vector vresult = new Vector();
    screenAccessRow temp = null;

    try
    {
      StringBuilder  theSQL = new StringBuilder();
      theSQL.append(" SELECT ACCESSTYPE.ACCESSTYPEID, ACCESSTYPE.ATDESCRIPTION, ")
       		.append( " USERTYPEPAGEACCESS.USERTYPEID, USERTYPEPAGEACCESS.PAGEID, ")
       		.append( " PAGE.PAGELABEL FROM ACCESSTYPE, USERTYPEPAGEACCESS,PAGE ")
       		.append( " WHERE ACCESSTYPE.ACCESSTYPEID  =  USERTYPEPAGEACCESS.ACCESSTYPEID ")
       		.append( " AND USERTYPEPAGEACCESS.PAGEID  =  PAGE.PAGEID ")
       		.append( " AND USERTYPEPAGEACCESS.USERTYPEID = " )
       		.append(  usertypeid );
      // Include / exclude display of quick link page based on flag - by default N	
      PropertiesCache cache = PropertiesCache.getInstance();
      	if("N".equalsIgnoreCase(cache.getProperty(srk.getExpressState().getUserInstitutionId(),"com.filogix.ingestion.quicklinks.enabled", "N"))) {
      		theSQL.append( " AND PAGE.PAGEID <> ")
      			  .append(Mc.PGNM_QUICKLINK_ADMININISTRATION);
		}
      	
      	theSQL.append( " ORDER BY USERTYPEPAGEACCESS.PAGEID  ASC ");

      logger.debug("Modified for Quick Links ==> theSQL = " + theSQL.toString());
      //--> Converted to use Framework JdbcExecutor
      //--> Modified by Billy 15Aug2002
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(theSQL.toString());
      int institutionProfileId = theSessionState.getDealInstitutionId();

      while (jExec.next(key))
      {
        temp = new screenAccessRow();

        //--Release2.1--//
        //Get MultiLingual PageName by ResourceBundle
        //temp.setPageLabel(jExec.getString(key, 5));
        temp.setPageLabel(BXResources.getPickListDescription(institutionProfileId,"PAGELABEL", jExec.getInt(key, 4),
            theSessionState.getLanguageId()));

        //==========================================
        temp.setPageId(jExec.getInt(key, 4));

        if (isRecordInPgAccessException(userprofileid, jExec.getInt(key, 4)))
        {
          temp.setAccessTypeId(getNewAccessTypeId(userprofileid, jExec.getInt(key, 4)));
          temp.setOverideIndicator("*");
          temp.setBlankSpace("");
        }
        else
        {
          temp.setAccessTypeId(jExec.getInt(key, 1));
          temp.setOverideIndicator("");
          temp.setBlankSpace("&nbsp;&nbsp;");
        }

        //logger.debug("BILLY ===> @userAdminHandler ---- page Label ==
        // " + temp.getPageLabel());
        //logger.debug("BILLY ===> @userAdminHandler ---- page
        // accesstypeid == " + temp.getAccessTypeId());
        //logger.debug(" @userAdminHandler ---- overideindicator == " +
        // temp.getOverideIndicator());
        //logger.debug(" @userAdminHandler ---- blank space == " +
        // temp.getBlankSpace());
        //logger.debug(" @userAdminHandler ---- page ID == " +
        // result.getResultTable().getValue(i,3).toString());
        //logger.debug(" @userAdminHandler ---- USERTYPEID == " +
        // result.getResultTable().getValue(i,2).toString());
        vresult.add(temp);
      }

      jExec.closeData(key);
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.getAccessScreens: Problem encountered in getting the records of the screenAccess.");
      logger.error(e.getMessage());
    }

    return vresult;
  }

  ////////////////////////////////////////////////////////////////////////
  public void setBusinessLimit(EditFieldsBuffer efb, int userprofileid)
  {
    ViewBean currNDPage = getCurrNDPage();
    String theLimit;

    // Set BusimessLimitA
    if ((theLimit = getBusinessLimitWithAuthorityValue(userprofileid, Mc.APPROV_AUTH_TYPE_VALUE_A)) != null)
    {
      efb.setFieldValue("tbLineOfBusiness1", theLimit);
    }
    else
    {
      efb.setFieldValue("tbLineOfBusiness1", "");
    }

    // Set BusimessLimitB
    if ((theLimit = getBusinessLimitWithAuthorityValue(userprofileid, Mc.APPROV_AUTH_TYPE_VALUE_B)) != null)
    {
      efb.setFieldValue("tbLineOfBusiness2", theLimit);
    }
    else
    {
      efb.setFieldValue("tbLineOfBusiness2", "");
    }

    // Set BusimessLimitC
    if ((theLimit = getBusinessLimitWithAuthorityValue(userprofileid, Mc.APPROV_AUTH_TYPE_VALUE_C)) != null)
    {
      efb.setFieldValue("tbLineOfBusiness3", theLimit);
    }
    else
    {
      efb.setFieldValue("tbLineOfBusiness3", "");
    }
  }

  ////////////////////////////////////////////////////////////////////////////
  //	public String getUserPassword(int userprofileid)
  //	{
  //		try
  //		{
  //
  //			CSpDBResult result = CSpDataObject.executeImmediate("orcl",
  //
  //		  		"SELECT USERSECURITYPOLICY.PASSWORD FROM USERSECURITYPOLICY "
  //		  		+ " WHERE USERSECURITYPOLICY.USERID = "+ userprofileid );
  //
  //		  	if(result.getNumRows() > 0)
  //				return result.getResultTable().getValue(0,0).toString();
  //
  //		}
  //		catch (Exception e)
  //		{
  //			setStandardFailMessage();
  //			logger.error("Exception @UserAdminHandler.getUserPassword: Problem
  // encountered in getting user password.");
  //			logger.error(e.getMessage());
  //		}
  //
  //		return "";
  //	}
  /////////////////////////////////////////////////////////////////////////////
  public int getContactCopyId(int contactid)
  {
    try
    {
      //--> Converted to use Framework JdbcExecutor
      //--> Modified by Billy 15Aug2002
      String theSQL =
        "SELECT MAX(CONTACT.COPYID) FROM CONTACT " + " WHERE CONTACT.CONTACTID = " + contactid;
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(theSQL);
      int result = -1;

      while (jExec.next(key))
      {
        result = jExec.getInt(key, 1);

        break;
      }

      jExec.closeData(key);

      return result;
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.getContactCopyId: Problem encountered in getting contact copy id.");
      logger.error(e.getMessage());
    }

    return -1;
  }

  /////////////////////////////////////////////////////////////////////////////
  //--> This method was not used anymore
  //--> Commented out by Billy 15Aug2002

  /*
   * public CSpDBResult getBusinessLimit(int userprofileid) { try {
   * CSpDBResult result = CSpDataObject.executeImmediate("orcl", "SELECT
   * APPROVALAUTHORITY.APPROVALAUTHORITYTYPEVALUE," + "
   * APPROVALAUTHORITY.APPROVALLIMIT FROM APPROVALAUTHORITY " + " WHERE
   * APPROVALAUTHORITY.USERPROFILEID = " + userprofileid); if
   * (result.getNumRows() > 0) return result; } catch(Exception e) {
   * setStandardFailMessage(); logger.error("Exception
   * @UserAdminHandler.getBusinessLimit: Problem encountered in getting
   * business Limit."); logger.error(e.getMessage()); }
   *
   * return null; }
   */

  /////////////////////////////////////////////////////////////////////////////
  public String getBusinessLimitWithAuthorityValue(int userprofileid, int authorityValue)
  {
    try
    {
      //--> Converted to use Framework JdbcExecutor
      //--> Modified by Billy 15Aug2002
      String theSQL =
        "SELECT APPROVALAUTHORITY.APPROVALAUTHORITYTYPEVALUE,"
        + " APPROVALAUTHORITY.APPROVALLIMIT FROM APPROVALAUTHORITY "
        + " WHERE APPROVALAUTHORITY.USERPROFILEID = " + userprofileid
        + " AND APPROVALAUTHORITYTYPEVALUE = " + authorityValue;
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(theSQL);
      String result = null;

      while (jExec.next(key))
      {
        result = jExec.getString(key, 2);

        break;
      }

      jExec.closeData(key);

      //logger.debug("BILLY ===> @getBusinessLimitWithAuthorityValue
      // authorityValue = " + authorityValue + "; result = " + result);
      return result;
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.getBusinessLimitWithAuthorityValue: Problem encountered in getting business Limit.");
      logger.error(e.getMessage());
    }

    return null;
  }

  //////////////////////////////////////////////////////////////////////////
  public void getUserAdminInfo(String groupprofileid, EditFieldsBuffer efb)
  {
    try
    {
      //--> Converted to use Framework JdbcExecutor
      //--> Modified by Billy 15Aug2002
      String theSQL =
        "SELECT GROUPPROFILE.GROUPPROFILEID, BRANCHPROFILE.BRANCHPROFILEID, "
        + " BRANCHPROFILE.REGIONPROFILEID, REGIONPROFILE.INSTITUTIONPROFILEID "
        + " FROM GROUPPROFILE, BRANCHPROFILE, REGIONPROFILE "
        + " WHERE GROUPPROFILE.BRANCHPROFILEID  =  BRANCHPROFILE.BRANCHPROFILEID "
        + " AND BRANCHPROFILE.REGIONPROFILEID  =  REGIONPROFILE.REGIONPROFILEID "
        + " AND GROUPPROFILE.GROUPPROFILEID =  " + groupprofileid;
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(theSQL);

      while (jExec.next(key))
      {
        efb.setFieldValue("cbBranchNames", jExec.getString(key, 2));
        efb.setFieldValue("cbRegionNames", jExec.getString(key, 3));
        efb.setFieldValue("cbInstitutionNames", jExec.getString(key, 4));

        break;
      }

      jExec.closeData(key);
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.getUserAdminInfo: Problem encountered in getting user group branch region institution.");
      logger.error(e.getMessage());
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  protected int getIntValue(String sNum)
  {
    if (sNum == null)
    {
      return 0;
    }

    sNum = sNum.trim();

    if (sNum.length() == 0)
    {
      return 0;
    }

    try
    {
      // handle decimial (e.g. float type) representation
      int ndx = sNum.indexOf(".");

      if (ndx != -1)
      {
        sNum = sNum.substring(0, ndx);
      }

      return Integer.parseInt(sNum.trim());
    }
    catch (Exception e)
    {
      ;
    }

    return 0;
  }

  public String getSelectedContactId(EditFieldsBuffer efb)
  {
    String s = efb.getFieldValue("cbUserNames");

    if ((s != null) && (s.length() > 0))
    {
      return s;
    }

    return null;
  }

  /////////////////////////////////////////////////////////////////////////////
  //--> Get the Contact ID to Page Session in order to prevent Screen Out of
  // sync. problem
  //--> By Billy 18Aug2003
  public String getSelectedContactIdAndCheckIfSync()
  {
    int theContactIdFromPage = -1;
    Integer theId = ((Integer) getCurrNDPage().getPageSessionAttribute("CURRENT_CONTACTID"));

    if (theId != null)
    {
      theContactIdFromPage = theId.intValue();
    }

    String s = (String) (getCurrNDPage().getDisplayFieldValue("cbUserNames"));

    //--> Check if the Screen in Sync.
    if ((theContactIdFromPage != -1) && !s.trim().equals("" + theContactIdFromPage))
    {
      // It is out of sync. ==> Should use the one from Page Session
      logger.warning("@userAdminHandler.getSelectedContactId :: Page Out of Sync. :: "
        + "The ContactId from Page session = " + theContactIdFromPage + ", But user selected = "
        + s + " ::: Fallback to use the one from PageSession !!");

      return ("" + theContactIdFromPage);
    }

    if ((s != null) && (s.length() > 0))
    {
      return s;
    }

    return null;
  }

  //======================================================================================
  ////////////////////////////////////////////////////////////////////////////
  public void handleAddNewUser()
  {
    PageEntry pg = getTheSessionState().getCurrentPage();

    //--> Bug fix :: By Billy 20Nov2003
    pg.setPageCondition1(false);

    //=================================
    pg.setPageCondition2(false);
    pg.setPageCondition3(true);
    pg.setPageCondition4(false);
    pg.setPageState1("");

    Hashtable pst = pg.getPageStateTable();
    UserAdminEditBuffer efb = new UserAdminEditBuffer("UserAdminParms", logger);

    // Set Default values if for new user-- By BILLY 17April2002
    efb.setFieldValue("chForceToChangePW", "Y");
    efb.setFieldValue("chLockPW", "N");
    efb.setFieldValue("chNewPW", "Y");
    pst.put(EDIT_BUFFER, efb);

    int userprofileid = getNewUserProfileId();
    logger.debug("@userAdminHandler.handleAddNewUser-- userprofileid == " + userprofileid);
    pst.put("USERPROFILEID", "" + userprofileid);
    pst.put("RESULTS", new Vector());
    pst.put("NEWUSER", new String("YES"));

    ComboBox cb = (ComboBox) getCurrNDPage().getDisplayField("cbUserTypes");
    String str = cb.getExtraHtml();
    pg.setPageState1(str);
    getSavedPages().setNextPage(pg);

    return;
  }

  ////////////////////////////////////////////////////////////////////
  public void handleDisplayUserDetails()
  {
    PageEntry pg = getTheSessionState().getCurrentPage();
    //Hashtable pst = pg.getPageStateTable();
    EditFieldsBuffer efb = new UserAdminEditBuffer("UserAdminParms", logger);

    //--> Bug fix :: By Billy 20Nov2003
    pg.setPageCondition1(false);

    //=================================
    pg.setPageCondition2(true);

    // are new
    pg.setPageCondition3(false);
    pg.setPageCondition4(false);
    pg.setPageState1("");

    return;
  }

  ////////////////////////////////////////////////////////////////////
  public void handleScreenAccess()
  {
    try
    {
      PageEntry pg = getTheSessionState().getCurrentPage();
      Hashtable pst = pg.getPageStateTable();
      boolean condition2 = pg.getPageCondition2();
      boolean condition3 = pg.getPageCondition3();
      EditFieldsBuffer efb = (EditFieldsBuffer) pst.get(EDIT_BUFFER);

      if (!condition2 && !condition3)
      {
        //setActiveMessageToAlert(Sc.MUST_SELECT_OR_ADD_USER,ActiveMsgFactory.ISCUSTOMCONFIRM);
        return;
      }

      //logger.debug("@userAdminHandler ---- Those values are from the
      // saveData method. ");
      //logger.debug("@userAdminHandler ---- group = " +
      // efb.getFieldValue("cbGroupProfileName"));
      //logger.debug("@userAdminHandler ---- branch = " +
      // efb.getFieldValue("cbBranchNames"));
      //logger.debug("@userAdminHandler ---- region = " +
      // efb.getFieldValue("cbRegionNames"));
      pg.setPageCondition4(true);
      getSavedPages().setNextPage(pg);
    }
    catch (Exception ex)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.handleScreenAccess: while navigating to the same page.");
      logger.error(ex);
    }
  }

  ////////////////////////////////////////////////////////////////////
  public boolean setupNumOfRepeatedRows(RequestHandlingTiledViewBase repeated1)
  {
    try
    {
      PageEntry pg = getTheSessionState().getCurrentPage();
      Hashtable pst = pg.getPageStateTable();
      Vector results = null;
      Object obj = pst.get("RESULTS");

      if (obj instanceof Vector)
      {
        results = (Vector) pst.get("RESULTS");
      }
      else
      {
        results = new Vector();
      }

      //logger.debug("BILLY ==> The # of Row = " + results.size());
      repeated1.setMaxDisplayTiles(results.size());

      //repeated1.setCurrNumOfRows(results.size());
      repeated1.getPrimaryModel().setSize(results.size());
      repeated1.resetTileIndex();

      if (results.size() == 0)
      {
        return false;
      }
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error("Exception @userAdminHandler.setNumOfRepeatedRows");
      logger.error(e);
    }

    return true;
  }

  ////////////////////////////////////////////////////////////////////
  public boolean populateRepeatedDisplayFields(int rowindex)
  {
    try
    {
      PageEntry pg = getTheSessionState().getCurrentPage();
      Hashtable pst = pg.getPageStateTable();
      Vector results = (Vector) pst.get("RESULTS");

      //logger.debug("BILLY ==> The # of Row = " + results.size() + "
      // rowid = " + rowindex);
      if (results.size() == 0)
      {
        return false;
      }

      UserAdminResultRow uarr =
        new UserAdminResultRow("RROW" + rowindex, logger,
          (screenAccessRow) results.elementAt(rowindex));
      uarr.populateFields(getCurrNDPage());

      return true;
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @userAdminHandler.populateRepeatedFields: populating results row (ndx="
        + rowindex + ")");
      logger.error(e);
    }

    return false;
  }

  ///////////////////////////////////////////////////////////////
  public String generateUserLabel()
  {
    PageEntry pg = getTheSessionState().getCurrentPage();

    if (pg.getPageCondition3() == true)
    {
      return "";
    }
    else
    {
      //--Release2.1--start//
      //// The UserLabel must be generated via the BXGeneric message
      // facility.
      String userLabel = "";

      if (theSessionState.getLanguageId() == 0)
      {  //// English
        userLabel = "User:";
      }

      if (theSessionState.getLanguageId() == 1)
      {  //// French
        userLabel = "Usager:";
      }

      return userLabel;

      //--Release2.1--end//
    }
  }

  //////////////////////////////////////////////////////////////
  public String generateBeginingOfSelectUserComment()
  {
    PageEntry pg = getTheSessionState().getCurrentPage();

    if (pg.getPageCondition3() == true)
    {
      return "<!--";
    }
    else
    {
      return "";
    }
  }

  /////////////////////////////////////////////////////////////////////
  public String generateEndingOfSelectUserComment()
  {
    PageEntry pg = getTheSessionState().getCurrentPage();

    if (pg.getPageCondition3() == true)
    {
      return "-->";
    }
    else
    {
      return "";
    }
  }

  //////////////////////////////////////////////////////////////////
  public String generateBeginingOfScreenAccComment()
  {
    PageEntry pg = getTheSessionState().getCurrentPage();

    if (!pg.getPageCondition4())
    {
      return "<!--";
    }
    else
    {
      return "";
    }
  }

  ///////////////////////////////////////////////////////////////////
  public String generateEndingOfScreenAccComment()
  {
    PageEntry pg = getTheSessionState().getCurrentPage();

    if (!pg.getPageCondition4())
    {
      return "-->";
    }
    else
    {
      return "";
    }
  }

  //////////////////////////////////////////////////////////////////////
  public void saveData(PageEntry currPage, boolean calledInTransaction)
  {
    Hashtable pst = currPage.getPageStateTable();
    UserAdminEditBuffer efb = (UserAdminEditBuffer) pst.get(EDIT_BUFFER);

    // EditFieldsBuffer efbresult = (EditFieldsBuffer)pst.get(EDIT_BUFFER);
    efb.readFields(getCurrNDPage(), ((UserAdminConfigManager) pst.get(CONFIGMGR)));

    boolean pgcondition1 = currPage.getPageCondition1();
    boolean pgcondition2 = currPage.getPageCondition2();
    boolean pgcondition3 = currPage.getPageCondition3();
    boolean pgcondition4 = currPage.getPageCondition4();

    //if(pgcondition4)
    if ((pgcondition2 || pgcondition3 || pgcondition1) && pgcondition4)
    {
      updateRepeatedFields(currPage, pst);
    }

    // we never mark as modified - allow user to walk away without fuss!
    currPage.setModified(currPage.isModified());
  }

  //////////////////////////////////////////////////////////////////////
  public void updateRepeatedFields(PageEntry currPage, Hashtable pst)
  {
    try
    {
      Object obj = pst.get("RESULTS");
      Vector v = null;

      if (obj instanceof Vector)
      {
        v = (Vector) obj;
      }
      else
      {
        v = new Vector();
      }

      Vector vresult = new Vector();
      Vector vAccessTypes = this.getRepeatedFieldValues("Repeated1/cbPageAccessTypes");

      if (v.size() != 0)
      {
        for (int i = 0; i < vAccessTypes.size(); i++)
        {
          screenAccessRow temp = (screenAccessRow) v.elementAt(i);
          temp.setAccessTypeId(TypeConverter.asInt(vAccessTypes.get(i)));
          vresult.add(temp);
        }
      }

      pst.put("RESULTS", vresult);
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.updateRepeatedFields: Problem encountered updating Page Screen Access ");
      logger.error(e);

      return;
    }
  }

  //////////////////////////////////////////////////////////////////////
  public void handleSubmitStandard()
  {
    SessionResourceKit srk = getSessionResourceKit();
    PageEntry pg = getTheSessionState().getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    Vector vmessages = new Vector();

    try
    {
      srk.beginTransaction();

      boolean pgcondition3 = pg.getPageCondition3();
      pg.setPageCondition1(true);

      Object obj = pst.get("NEWUSER");
      String newuserstr = null;
      boolean newuser = true;

      if (obj instanceof String)
      {
        newuserstr = (String) obj;
      }
      else
      {
        newuserstr = "";
      }

      if (newuserstr.equals("NO"))
      {
        newuser = false;
      }

      runAllUserAdminBusinessRules(pg, vmessages, pgcondition3 && newuser);

      if (vmessages.size() > 0)
      {
        srk.cleanTransaction();
        constructStringBufferOfMessages(vmessages);
        pg.setPageState1("ERROR");
        getSavedPages().setNextPage(pg);
        navigateToNextPage();

        return;
      }

      int userprofileid = updateUserTables(pg, srk, pgcondition3 && newuser);
      logger.debug(" @userAdminHandler --- userprofileid == " + userprofileid);

      //--> Cervus II begins
      //--> For modifying AUTOUSER, Admin must input password. (AME spec. Section 3.2.2 - 2)
      //--> By Neil : 09/27/2004
      boolean isModificationAllowed = true;
      //--> Cervus II ends
      
      if (userprofileid != -1)
      {
        //--> Cervus II : For modifying AUTOUSER, Admin must input password. (AME spec. Section 3.2.2 - 2)
        //--> By Neil : 09/27/2004
        // userprofile found
        UserProfile up = new UserProfile(srk);

        up.findByPrimaryKey(new UserProfileBeanPK(userprofileid, pg.getDealInstitutionId()));  // up now points an entity with that userprofileid

        int userType = up.getUserTypeId();
        logger.debug("===> @userAdminHandler -----> handleSubmitStandard() : userType = "
          + userType);

        if (userType == 99)
        {  // AutoUser
          logger.debug("===> @userAdminHandler -----> handleSubmitStandard() : newUser ? "
            + newuser);

          // retrieve password for this autouser
          // get the edit buffer
          EditFieldsBuffer efb = (UserAdminEditBuffer) pst.get(EDIT_BUFFER);

          String storedPassword = up.getPassword();

          if (PropertiesCache.getInstance()
                .getInstanceProperty("com.basis100.useradmin.passwordencrypted", "Y").equals("Y"))
//        	  .getProperty("com.basis100.useradmin.passwordencrypted", "Y").equals("Y"))
          {  // encrypted
            storedPassword = PasswordEncoder.getEncodedPassword(storedPassword);
          }

          logger.debug("===> @userAdminHandler -----> handleSubmitStandard() : StoredPassword = "
            + storedPassword);

          String newPassword = efb.getFieldValue(pgUserAdministrationViewBean.CHILD_TBPASSWORD);
          logger.debug("===> @userAdminHandler -----> handleSubmitStandard() : NewPassword = "
            + newPassword);

          if (!storedPassword.equals(newPassword))
          {
            // modification denied
            isModificationAllowed = false;
          }
        }
        //--------------------------------------------------------------
        // Non-Auto User : Do updating
        //--------------------------------------------------------------
        //--> Cervus II ends
        
        if (isModificationAllowed)
        {
          //--> Cervus II begins
          //--> by Neil : 09/29/2004
          userType = new Integer(this.getCurrNDPage()
              .getDisplayFieldValue(pgUserAdministrationViewBean.CHILD_CBUSERTYPES).toString())
            .intValue();
          //--> Cervus II ends
          
          updateScreenAccessTables(pg, srk, userprofileid);
          setActiveMessageToAlert(BXResources.getSysMsg("USER_PROFILE_SUCCESSFULLY_UPDATED",
              theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
          pg.setPageState1("UPDATE");
          pst.put("NEWUSER", new String("NO"));
          pg.setPageCondition3(false);
          srk.commitTransaction();

          // Set to display the new user info if added
          if (newuser)
          {
            pg.setPageCondition2(true);

            int theContactId = getUserContactId(userprofileid);
            getCurrNDPage().setDisplayFieldValue("cbUserNames", "" + theContactId);

            //  get the edit buffer
            EditFieldsBuffer efb = (EditFieldsBuffer) pst.get(EDIT_BUFFER);
            efb.setFieldValue("cbUserNames", "" + theContactId);
          }
        }
      }
      else
      {
        //Roll back if error exists
        pg.setPageState1("ERROR");

        if (newuser)
        {
          pst.put("NEWUSER", new String("YES"));
        }

        srk.rollbackTransaction();
      }

      getSavedPages().setNextPage(pg);
      navigateToNextPage();
    }
    catch (Exception e)
    {
      srk.cleanTransaction();
      logger.error("Exception occurred @handleSubmitStandard()");
      logger.error(e);
      setStandardFailMessage();

      return;
    }
  }

  /////////////////////////////////////////////////////////////////////
  public void constructStringBufferOfMessages(Vector v)
  {
    PassiveMessage pm = new PassiveMessage();
    pm.setGenerate(true);

    //--Release2.1--//
    //--> Modified to support Multilingual
    //--> By Billy 06Dec2002
    //pm.setTitle("User Administration.");
    pm.setTitle(BXResources.getSysMsg("USERADMIN_ERROR_MSG_TITLE", theSessionState.getLanguageId()));

    //pm.setInfoMsg(" Please fix the errors and resubmit.");
    pm.setInfoMsg(BXResources.getSysMsg("USERADMIN_ERROR_MSG_INFOMSG",
        theSessionState.getLanguageId()));

    for (int i = 0; i < v.size(); i++)
    {
      pm.addMsg(v.get(i).toString(), PassiveMessage.CRITICAL);
    }

    getTheSessionState().setPasMessage(pm);

    return;
  }

  ////////////////////////////////////////////////////////////////////
  public void handleCancel()
  {
    PageEntry pg = getTheSessionState().getCurrentPage();
    pg.setPageCondition1(false);
    pg.setPageCondition2(false);
    pg.setPageCondition3(false);
    pg.setPageCondition4(false);
    pg.setPageState1("");
    handleCancelStandard();
  }

  /////////////////////////////////////////////////////////////////////
  public void updateScreenAccessTables(PageEntry pg, SessionResourceKit srk, int userprofileid)
  {
    boolean pgcondition4 = pg.getPageCondition4();
    //boolean pgcondition1 = pg.getPageCondition1();
    String update = pg.getPageState1();

    //--> Added protection checking here -- By Billy 19Aug2002
    if (update == null)
    {
      update = "";
    }

    //========================================================
    //Hashtable pst = pg.getPageStateTable();

    try
    {
      // Toni
      if ((pgcondition4 && update.equals("UPDATE")) || pgcondition4)
      {
        logger.debug("@userAdminHandler --- 000000000000000000000000000000 ");

        Vector vaccesstypes = getRepeatedFieldValues("Repeated1/cbPageAccessTypes");
        logger.debug("@userAdminHandler --- vaccesstypes values ==  " + vaccesstypes.toString());

        Vector vrow = new Vector();
        Object usertypeid = getCurrNDPage().getDisplayFieldValue("cbUserTypes");
        logger.debug("@userAdminHandler --- usertypeid ==  " + usertypeid.toString());

        screenAccessRow temp = null;
        logger.debug("@userAdminHandler --- 111111111111111111111111111 ");

        //String usertypeid = efb.getFieldValue("cbUserTypes");
        //int userprofileid = getIntValue((String)
        // pst.get("USERPROFILEID"));
        vrow = getAccessScreens(TypeConverter.asInt(usertypeid), userprofileid);

        for (int i = 0; i < vaccesstypes.size(); i++)
        {
          temp = (screenAccessRow) vrow.elementAt(i);

          int pageid = temp.getPageId();
          Object accesstypeid = vaccesstypes.get(i);
          logger.debug("@userAdminHandler --- pageid == " + pageid + " i = " + i);
          logger.debug("@userAdminHandler --- accesstypeid == " + TypeConverter.asInt(accesstypeid)
            + " i = " + i);

          //logger.debug("@userAdminHandler --- userprofileid == " +
          // userprofileid);
          // if default then if record is in pageExc --> yes -->
          // delete it.
          // if not default then if record is pageExc --> no -->
          // insert new record.
          //												yes --> update existed record.
          if (IsItDefaultPageAccessType(usertypeid, accesstypeid, pageid))
          {
            if (isRecordInPgAccessException(userprofileid, pageid))
            {
              deleteFromPgAccessException(userprofileid, pageid, srk);
            }
          }
          else
          {
            if (!isRecordInPgAccessException(userprofileid, pageid))
            {
              updatePgAccessException(userprofileid, TypeConverter.asInt(accesstypeid), pageid,
                false, srk);
            }
            else
            {
              updatePgAccessException(userprofileid, TypeConverter.asInt(accesstypeid), pageid,
                true, srk);
            }
          }
        }

        logger.debug("@userAdminHandler --- 222222222222222222222222222 ");
      }
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.updateScreenAccessTables: Problem encountered updating userAccessPageException ");
      logger.error(e);

      return;
    }
  }

  //////////////////////////////////////////////////////////////////////
  public boolean isRecordInPgAccessException(int userprofileid, int pageid)
  {
    try
    {
      //--> Converted to use Framework JdbcExecutor
      //--> Modified by Billy 15Aug2002
      String theSQL =
        "SELECT * FROM USERPAGEACCESSEXCEPTION WHERE PAGEID = " + pageid + " AND USERPROFILEID = "
        + userprofileid;
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(theSQL);
      boolean result = false;

      while (jExec.next(key))
      {
        result = true;
      }

      jExec.closeData(key);

      return result;
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.isRecordInPgAccessException: Problem encountered in checking record in pageException table ");
      logger.error(e.getMessage());
    }

    return false;
  }

  ///////////////////////////////////////////////////////////////////////
  public int getNewAccessTypeId(int userprofileid, int pageid)
  {
    try
    {
      //--> Converted to use Framework JdbcExecutor
      //--> Modified by Billy 15Aug2002
      String theSQL =
        "SELECT USERPAGEACCESSEXCEPTION.ACCESSTYPEID FROM "
        + " USERPAGEACCESSEXCEPTION WHERE PAGEID = " + pageid + " AND USERPROFILEID = "
        + userprofileid;
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(theSQL);
      int result = -1;

      while (jExec.next(key))
      {
        result = jExec.getInt(key, 1);

        break;
      }

      jExec.closeData(key);

      return result;
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.getNewAccessTypeId: Problem encountered in getting accessTypeId ");
      logger.error(e.getMessage());
    }

    return -1;
  }

  /////////////////////////////////////////////////////////////////////
  public boolean IsItDefaultPageAccessType(Object usertypeid, Object accesstypeid, int pageid)
  {
    try
    {
      //--> Converted to use Framework JdbcExecutor
      //--> Modified by Billy 15Aug2002
      String theSQL =
        "SELECT USERTYPEPAGEACCESS.ACCESSTYPEID FROM USERTYPEPAGEACCESS WHERE "
        + " USERTYPEPAGEACCESS.PAGEID  =  " + pageid + " AND USERTYPEPAGEACCESS.USERTYPEID  =  "
        + TypeConverter.asInt(usertypeid);
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(theSQL);
      boolean result = false;

      while (jExec.next(key))
      {
        if (TypeConverter.asInt(accesstypeid) == jExec.getInt(key, 1))
        {
          result = true;
        }
      }

      jExec.closeData(key);

      return result;
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.IsItDefaultPageAccessType: Problem encountered in checking if accesType is default");
      logger.error(e.getMessage());
    }

    return false;
  }

  /////////////////////////////////////////////////////////////////////
  public void deleteFromPgAccessException(int userprofileid, int pageid, SessionResourceKit srk)
  {
    String sql =
      " DELETE FROM USERPAGEACCESSEXCEPTION WHERE PAGEID = " + pageid + " AND USERPROFILEID = "
      + userprofileid;

    try
    {
      JdbcExecutor jExec = srk.getJdbcExecutor();
      jExec.executeUpdate(sql);
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.deleteFromPgAccessException: Problem encountered in deleting record from pageException table ");
      logger.error(e.getMessage());
    }

    srk.setModified(true);

    return;
  }

  /////////////////////////////////////////////////////////////////////
  public void updatePgAccessException(int userprofileid, int accesstypeid, int pageid,
    boolean updateRec, SessionResourceKit srk)
  {
    String sql = "";

    if (updateRec)
    {
      sql =
        " UPDATE USERPAGEACCESSEXCEPTION SET ACCESSTYPEID = " + accesstypeid + " WHERE PAGEID = "
        + pageid + " AND USERPROFILEID = " + userprofileid + " AND INSTITUTIONPROFILEID = " 
        + srk.getExpressState().getDealInstitutionId();
    }
    else
    {
      sql =
        " INSERT INTO USERPAGEACCESSEXCEPTION (PAGEID,ACCESSTYPEID," + " USERPROFILEID, INSTITUTIONPROFILEID) VALUES("
        + pageid + ", " + accesstypeid + ", " + userprofileid + ", " 
        + srk.getExpressState().getDealInstitutionId() + ")";
    }

    try
    {
      JdbcExecutor jExec = srk.getJdbcExecutor();
      jExec.executeUpdate(sql);
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.updatePgAccessException: Problem encountered in updating pageException table ");
      logger.error(e.getMessage());
    }

    srk.setModified(true);

    return;
  }

  /////////////////////////////////////////////////////////////////////
  //=================================================
  //--> CRF#37 :: Source/Firm to Group routing for TD
  //=================================================
  //--> Modified to support Multiple Groups assignment
  //--> By Billy 12Aug2003
  public int updateUserTables(PageEntry pg, SessionResourceKit srk, boolean newUser)
  {
    return updateUserTables(pg, srk, newUser, false);
  }

  public int updateUserTables(PageEntry pg, SessionResourceKit srk, boolean newUser,
    boolean isNewGroup)
  {
    Hashtable pst = pg.getPageStateTable();

    try
    {
      ViewBean currNDPage = getCurrNDPage();
      Object val = null;
      int userprofileid = 0;
      int usertypeid = 0;

      //-- ========== SCR#537 begins ========== --//
      //-- by Neil on Dec/02/2004
      String taskAlert = "N";
      //-- ========== SCR#537 ends ========== --//
      
      //--> Modified to support Multiple Groups assignment
      //--> By Billy 12Aug2003
      //int groupprofileid = 0;
      Object[] groupIds = null;

      //==================================================
      int higherapproveruserid = 0;
      int jointapproveruserid = 0;
      int contactid = 0;
      int profilestatusid = 0;
      int partneruserid = 0;
      int standinuserid = 0;
      int managerid = 0;
      int regionprofileid = 0;
      int branchprofileid = 0;
      int institutionprofileid = 0;
      double businesslimitA = -1;
      double businesslimitB = -1;
      double businesslimitC = -1;
      String upbusinessid = "";
      String userlogin = "";
      String standardaccess = "Y";
      String bilingual = "";
      String password = "";
      String verifypassword = "";

      // New fields for modified Password Security Handling -- By BILLY
      // 17April2002
      boolean isPWReset = false;
      boolean isPWLocked = false;
      boolean isNewPW = false;

      //--Release2.1--//
      //--> Added Prefered Language ComboBox - By Billy 09Dec2002
      int defaultLanguageId = 0;  // Default to English

      String encrypted_password = "";

      //===========================================================================
      UserProfile up = new UserProfile(srk);
      Contact contact = new Contact(srk);

      // Should continue to save the data even if error exists
      //  -- Modified by BILLY 01May2001
      int result = 0;

      /////////////////////// User Profile /////////////////////
      //--> Modified to support Multiple Groups assignment
      //--> By Billy 12Aug2003
      groupIds = ((ListBox) (currNDPage.getDisplayField("lbGroups"))).getValues();

      //--> Skip check if Creating New Group
      if (!isNewGroup)
      {
        //        if ((groupIds == null) || (groupIds.length <= 0)
        //              || ((groupIds.length == 1)
        //              && ((groupIds[0].toString().trim().length() == 0)
        //              || groupIds[0].toString().trim().equals("-1"))))
        //        {
        //          logger.debug("@userAdminHandler ----  display error message to enter groupid");
        //          setActiveMessageToAlert(BXResources.getSysMsg("USER_PROFILE_ASSIGNED_GROUP",
        //              theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
        //          result = -1;
        //        }
        
        //--> Cervus II : Modified to allow AUTOUSER omit group selection
        //--> By Neil : 09/29/2004
        //--> For user types other than AUTOUSER, it's a must.
        //--> For AUTOUSER, user may choose some value or default 0 will be assigned.
        userprofileid = getIntValue((String) pst.get("USERPROFILEID"));
        logger.debug("===> @userAdminHandler -----> updateUserTables() : userprofileid = "
          + userprofileid);

//        UserProfile up2 = new UserProfile(srk);
//        up2.findByPrimaryKey(new UserProfileBeanPK(userprofileid));  // upGot now points an entity with that userprofileid
//        int userType = up2.getUserTypeId();
		val = currNDPage.getDisplayFieldValue("cbUserTypes");
		try
		{
			if (val != null)
			{
				usertypeid = TypeConverter.asInt(val);
			}
		}
		catch (NumberFormatException e)
		{
			usertypeid = 0;
		}
        logger.debug("===> @userAdminHandler -----> updateUserTables() : userType = " + usertypeid);

        for (int i = 0; i < groupIds.length; i++)
        {
          logger.debug("===> @userAdminHandler -----> updateUserTables() : groupIds[" + i + "] = "
            + groupIds[i]);
        }

        boolean isGroupSelected =
          !((groupIds == null) || (groupIds.length <= 0)
          || ((groupIds.length == 1)
          && ((groupIds[0].toString().trim().length() == 0)
          || groupIds[0].toString().trim().equals("-1"))));
        logger.debug("===> @userAdminHandler -----> updateUserTables() : isGroupSelected = "
          + isGroupSelected);

        if (!isGroupSelected && (usertypeid != 99))
        {
          // For normal user type, group selection is a must.
          logger.debug("@userAdminHandler ----  display error message to enter groupid");
          setActiveMessageToAlert(BXResources.getSysMsg("USER_PROFILE_ASSIGNED_GROUP",
              theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
          result = -1;
        }
        else if ((usertypeid == 99) && (!isGroupSelected))
        {
          // For AUTOUSER, group selection may be omitted.
          // put 0 as default value.
          groupIds = new Object[1];
          groupIds[0] = new String("0");

          //		logger.debug("===> @userAdminHandler -----> updateUserTables() : groupIds[0] = " + groupIds[0].toString() );
        }
      }

      logger.debug("@userAdminHandler ---- groupIds == " + groupIds);

      //========================================================================================
      val = currNDPage.getDisplayFieldValue("tbPassword");

      if (val != null)
      {
        password = val.toString();
      }

      val = currNDPage.getDisplayFieldValue("tbVerifyPassword");

      if (val != null)
      {
        verifypassword = val.toString();
      }

      //logger.debug("@userAdminHandler ---- password == " + password);
      //logger.debug("@userAdminHandler ---- verifypassword == " +
      // verifypassword);
      //--Merge--EncriptedPassword--//
      //Handle Password encription -- By Billy 28Oct2002
      if (PropertiesCache.getInstance().getProperty(-1, "com.basis100.useradmin.passwordencrypted", "Y")
            .equals("Y"))
      {
        encrypted_password = PasswordEncoder.getEncodedPassword(password);
      }
      else
      {
        encrypted_password = password;
      }

      //================================================
      val = currNDPage.getDisplayFieldValue("tbBusinessId");

      if (val != null)
      {
        upbusinessid = val.toString();
      }

      //logger.debug("@userAdminHandler ---- upbusinessid == " +
      // upbusinessid);
      val = currNDPage.getDisplayFieldValue("tbLoginId");

      if (val != null)
      {
        userlogin = val.toString();
      }

      //logger.debug("@userAdminHandler ---- userlogin == " + userlogin);
      val = currNDPage.getDisplayFieldValue("cbUserTypes");

      try
      {
        if (val != null)
        {
          usertypeid = TypeConverter.asInt(val);
        }
      }
      catch (NumberFormatException e)
      {
        usertypeid = 0;
      }

      //logger.debug("@userAdminHandler ---- usertypeid == " +
      // usertypeid);
      //--> To Get the selected Contactid and check if it's out of sync.
      //--> By Billy 18Aug2003
      val = getSelectedContactIdAndCheckIfSync();

      //================================================================
      try
      {
        if (val != null)
        {
          contactid = TypeConverter.asInt(val);
        }
      }
      catch (NumberFormatException e)
      {
        contactid = 0;
      }

      //logger.debug("@userAdminHandler ---- contactid == " + contactid);
      val = currNDPage.getDisplayFieldValue("cbProfileStatus");

      try
      {
        if (val != null)
        {
          profilestatusid = TypeConverter.asInt(val);
        }
      }
      catch (NumberFormatException e)
      {
        profilestatusid = 0;
      }

      //logger.debug("@userAdminHandler ---- profilestatusid == " +
      // profilestatusid);
      val = currNDPage.getDisplayFieldValue("cbRegionNames");

      try
      {
        if (val != null)
        {
          regionprofileid = TypeConverter.asInt(val);
        }
      }
      catch (NumberFormatException e)
      {
        regionprofileid = 0;
      }

      //logger.debug("@userAdminHandler ---- regionprofileid == " +
      // regionprofileid);
      val = currNDPage.getDisplayFieldValue("cbBranchNames");

      try
      {
        if (val != null)
        {
          branchprofileid = TypeConverter.asInt(val);
        }
      }
      catch (NumberFormatException e)
      {
        branchprofileid = 0;
      }

      //logger.debug("@userAdminHandler ---- branchprofileid == " +
      // branchprofileid);
      val = currNDPage.getDisplayFieldValue("cbInstitutionNames");

      try
      {
        if (val != null)
        {
          institutionprofileid = TypeConverter.asInt(val);
        }
      }
      catch (NumberFormatException e)
      {
        institutionprofileid = 0;
      }

      //logger.debug("@userAdminHandler ---- institutionprofileid == " +
      // institutionprofileid);
      val = currNDPage.getDisplayFieldValue("cbLanguages");

      if (val != null)
      {
        bilingual = val.toString();
      }

      //-- ========== SCR#537 begins ========== --//
      //-- by Neil on Dec/02/2004
      val = currNDPage.getDisplayFieldValue("cbTaskAlert");

      if (val != null)
      {
        taskAlert = val.toString();
      }

      //-- ========== SCR#537 ends ========== --//
      //logger.debug("@userAdminHandler ---- bilingual == " + bilingual);
      val = currNDPage.getDisplayFieldValue("cbHigherApprovers");

      try
      {
        if (val != null)
        {
          higherapproveruserid = TypeConverter.asInt(val);
        }
      }
      catch (NumberFormatException e)
      {
        higherapproveruserid = 0;
      }

      //logger.debug("@userAdminHandler ---- higherapproveruserid == " +
      // higherapproveruserid);
      val = currNDPage.getDisplayFieldValue("cbJointApprovers");

      try
      {
        if (val != null)
        {
          jointapproveruserid = TypeConverter.asInt(val);
        }
      }
      catch (NumberFormatException e)
      {
        jointapproveruserid = 0;
      }

      //logger.debug("@userAdminHandler ---- jointapproveruserid == " +
      // jointapproveruserid);
      val = currNDPage.getDisplayFieldValue("cbManagers");

      try
      {
        if (val != null)
        {
          managerid = TypeConverter.asInt(val);
        }
      }
      catch (NumberFormatException e)
      {
        managerid = 0;
      }

      //logger.debug("@userAdminHandler ---- managerid == " + managerid);
      val = currNDPage.getDisplayFieldValue("cbPartnerProfileNames");

      try
      {
        if (val != null)
        {
          partneruserid = TypeConverter.asInt(val);
        }
      }
      catch (NumberFormatException e)
      {
        partneruserid = 0;
      }

      //logger.debug("@userAdminHandler ---- partneruserid == " +
      // partneruserid);
      val = currNDPage.getDisplayFieldValue("cbStandIn");

      try
      {
        if (val != null)
        {
          standinuserid = TypeConverter.asInt(val);
        }
      }
      catch (NumberFormatException e)
      {
        standinuserid = 0;
      }

      //logger.debug("@userAdminHandler ---- standinuserid == " +
      // standinuserid);
      val = currNDPage.getDisplayFieldValue("tbLineOfBusiness1");

      try
      {
        if (val != null)
        {
          businesslimitA = TypeConverter.asDouble(val);
        }
      }
      catch (NumberFormatException e)
      {
        businesslimitA = -1;
      }

      //logger.debug("@userAdminHandler ---- businesslimitA == " +
      // businesslimitA);
      val = currNDPage.getDisplayFieldValue("tbLineOfBusiness2");

      try
      {
        if (val != null)
        {
          businesslimitB = TypeConverter.asDouble(val);
        }
      }
      catch (NumberFormatException e)
      {
        businesslimitB = -1;
      }

      //logger.debug("@userAdminHandler ---- businesslimitB == " +
      // businesslimitB);
      val = currNDPage.getDisplayFieldValue("tbLineOfBusiness3");

      try
      {
        if (val != null)
        {
          businesslimitC = TypeConverter.asDouble(val);
        }
      }
      catch (NumberFormatException e)
      {
        businesslimitC = -1;
      }

      //logger.debug("@userAdminHandler ---- businesslimitC == " +
      // businesslimitC);
      // New fields for modified Password Security Handling -- By BILLY
      // 17April2002
      val = currNDPage.getDisplayFieldValue("chForceToChangePW");

      if ((val != null) && val.toString().equals("Y"))
      {
        isPWReset = true;
      }
      else
      {
        isPWReset = false;
      }

      val = currNDPage.getDisplayFieldValue("chLockPW");

      if ((val != null) && val.toString().equals("Y"))
      {
        isPWLocked = true;
      }
      else
      {
        isPWLocked = false;
      }

      val = currNDPage.getDisplayFieldValue("chNewPW");

      if ((val != null) && val.toString().equals("Y"))
      {
        isNewPW = true;
      }
      else
      {
        isNewPW = false;
      }

      //===========================================================================
      //--Release2.1--//
      //--> Added Prefered Language ComboBox - By Billy 09Dec2002
      val = currNDPage.getDisplayFieldValue("cbDefaultLanguage");

      try
      {
        if (val != null)
        {
          defaultLanguageId = TypeConverter.asInt(val);
        }
      }
      catch (NumberFormatException e)
      {
        defaultLanguageId = 0;
      }

      //=============================================================
      if (newUser)
      {
        // create new contact record.
        // creates a new user with a copyid = 1
        //					Addr address = create();
        //					contact = create(address.getPk());
        logger.debug("@userAdminHandler -- New User to be inserted.");
        userprofileid = getIntValue((String) pst.get("USERPROFILEID"));

        //userprofileid = getNewUserProfileId();
        //logger.debug("@userAdminHandler ---- userprofileid == " +
        // userprofileid);
        contact = contact.create(1);

        //new user
        pst.put("SAVEDCONTACTID", "" + contact.getContactId());

        // create a new userprofile
        //--> Modified to support Multiple Groups assignment
        //--> By Billy 12Aug2003
        //up = up.create(userprofileid, usertypeid, groupprofileid,
        // userlogin.toUpperCase(), standardaccess, bilingual,
        // higherapproveruserid, jointapproveruserid,
        // contact.getContactId(), profilestatusid, partneruserid,
        // standinuserid);
        //--> the GroupProfileId is Obseleted
        //                up = up.create(userprofileid, usertypeid, 0, userlogin
        //                        .toUpperCase(), standardaccess, bilingual,
        //                        higherapproveruserid, jointapproveruserid, contact
        //                                .getContactId(), profilestatusid,
        //                        partneruserid, standinuserid);
        //-- ========== SCR#537 begins ========== --//
        //-- by Neil on Dec/02/2004
        up =
          up.create(userprofileid, usertypeid, 0, userlogin.toUpperCase(), standardaccess,
            bilingual, higherapproveruserid, jointapproveruserid, contact.getContactId(),
            profilestatusid, partneruserid, standinuserid, taskAlert, pg.getDealInstitutionId());

        //-- ========== SCR#537 ends ========== --//
        up.setGroupIds(groupIds);

        //===================================================
        up.setManagerId(managerid);
        up.setUpBusinessId(upbusinessid.toUpperCase());

        // Changed for new Password Handling -- By BILLY 17April2002
        //--Merge--EncriptedPassword--//
        //Handle Password encription -- By Billy 28Oct2002
        
        //ML:FXP21281 start
        if(encrypted_password.length() > 0)
        //ML:FXP21281 end
            up.makePasswordChange(encrypted_password, isNewPW, isPWLocked, isPWReset);

        //================================================
        // create new Approval authority record.
        int approvalAuthorityId = 0;

        if (businesslimitA != -1)
        {
          approvalAuthorityId = getNewAuthorityId();
          insertIntoApprovalAuthority(userprofileid, approvalAuthorityId, businesslimitA,
            Mc.APPROV_AUTH_TYPE_VALUE_A, srk);
        }

        if (businesslimitB != -1)
        {
          approvalAuthorityId = getNewAuthorityId();
          insertIntoApprovalAuthority(userprofileid, approvalAuthorityId, businesslimitB,
            Mc.APPROV_AUTH_TYPE_VALUE_B, srk);
        }

        if (businesslimitC != -1)
        {
          approvalAuthorityId = getNewAuthorityId();
          insertIntoApprovalAuthority(userprofileid, approvalAuthorityId, businesslimitC,
            Mc.APPROV_AUTH_TYPE_VALUE_C, srk);
        }
      }
      else
      {
        // update a user by find by primary key for contact,userprofile
        if (!pg.getPageCondition3())
        {
          up = new UserProfile(srk);
          up = up.findByContactId(contactid, pg.getDealInstitutionId());

          int copyid = getContactCopyId(contactid);
          contact = contact.findByPrimaryKey(new ContactPK(contactid, copyid));
          logger.debug("--- Toni ---> @userAdminHandler -- update a user inserted.");
          logger.debug("--- Toni ---> @userAdminHandler -- contactid == " + contactid);
          logger.debug("--- Toni ---> @userAdminHandler -- userProfileId == "
            + up.getUserProfileId());
        }
        else
        {
          contactid = getIntValue((String) pst.get("SAVEDCONTACTID"));
          up = new UserProfile(srk);
          up = up.findByContactId(contactid, pg.getDealInstitutionId());

          int copyid = getContactCopyId(contactid);
          contact = contact.findByPrimaryKey(new ContactPK(contactid, copyid));
          logger.debug("--- Toni ---> @userAdminHandler -- update a user inserted.");
          logger.debug("--- Toni ---> @userAdminHandler -- contactid == " + contactid);
          logger.debug("--- Toni ---> @userAdminHandler -- userProfileId == "
            + up.getUserProfileId());
        }

        userprofileid = up.getUserProfileId();
        up.setUserTypeId(usertypeid);

        //--> Modified to support Multiple Groups assignment
        //--> By Billy 12Aug2003
        //up.setGroupProfileId(groupprofileid);
        up.setGroupIds(groupIds);

        //==================================================
        up.setUserLogin(userlogin.toUpperCase());
        up.setBilingual(bilingual);

        //-- ========== SCR#537 begins ========== --//
        //-- by Neil on Dec/02/2004
        up.setTaskAlert(taskAlert);

        //-- ========== SCR#537 ends ========== --//
        up.setSecondApproverUserId(higherapproveruserid);
        up.setJointApproverUserId(jointapproveruserid);
        up.setContactId(contactid);
        up.setProfileStatusId(profilestatusid);
        up.setPartnerUserId(partneruserid);
        up.setStandInUserId(standinuserid);
        up.setManagerId(managerid);
        up.setUpBusinessId(upbusinessid.toUpperCase());

        // Changed for new Password Handling -- By BILLY 17April2002
        //--Merge--EncriptedPassword--//
        //Handle Password encription -- By Billy 28Oct2002
        // - If password field = null ==> No Password change
        if ((password.length() > 0) && !up.getPassword().equals(encrypted_password))
        {
          up.makePasswordChange(encrypted_password, isNewPW, isPWLocked, isPWReset);
        }

        //==========================================================
        else
        {
          up.setPasswordAttributes(isNewPW, isPWLocked, isPWReset);

          //reset Password change date if NewPassword
          if (isNewPW == true)
          {
            up.setPasswordChangeTime(new Date());
          }

          //reset Fail Attempts counter if UNLock
          if (isPWLocked == false)
          {
            up.setPasswordFailedAttempts(0);
          }
        }

        int authorityId = 0;
        authorityId = getAuthorithyId(up.getUserProfileId(), Mc.APPROV_AUTH_TYPE_VALUE_A);

        if (businesslimitA != -1)
        {
          if (authorityId > 0)
          {
            // update if record exist
            updateApprovalAuthority(up.getUserProfileId(), businesslimitA, authorityId, srk);
          }
          else
          {
            // Create if record not exist
            insertIntoApprovalAuthority(up.getUserProfileId(), getNewAuthorityId(), businesslimitA,
              Mc.APPROV_AUTH_TYPE_VALUE_A, srk);
          }
        }
        else
        {
          // Remove if record exist
          if (authorityId > 0)
          {
            deleteApprovalAuthority(authorityId, srk);
          }
        }

        authorityId = getAuthorithyId(up.getUserProfileId(), Mc.APPROV_AUTH_TYPE_VALUE_B);

        if (businesslimitB != -1)
        {
          if (authorityId > 0)
          {
            // update if record exist
            updateApprovalAuthority(up.getUserProfileId(), businesslimitB, authorityId, srk);
          }
          else
          {
            // Create if record not exist
            insertIntoApprovalAuthority(up.getUserProfileId(), getNewAuthorityId(), businesslimitA,
              Mc.APPROV_AUTH_TYPE_VALUE_B, srk);
          }
        }
        else
        {
          // Remove if record exist
          if (authorityId > 0)
          {
            deleteApprovalAuthority(authorityId, srk);
          }
        }

        authorityId = getAuthorithyId(up.getUserProfileId(), Mc.APPROV_AUTH_TYPE_VALUE_C);

        if (businesslimitC != -1)
        {
          if (authorityId > 0)
          {
            // update if record exist
            updateApprovalAuthority(up.getUserProfileId(), businesslimitC, authorityId, srk);
          }
          else
          {
            // Create if record not exist
            insertIntoApprovalAuthority(up.getUserProfileId(), getNewAuthorityId(), businesslimitA,
              Mc.APPROV_AUTH_TYPE_VALUE_C, srk);
          }
        }
        else
        {
          // Remove if record exist
          if (authorityId > 0)
          {
            deleteApprovalAuthority(authorityId, srk);
          }
        }
      }

      // setting the contact addressid
      contact.setAddrId(getBranchContactAddrId(branchprofileid));
      val = currNDPage.getDisplayFieldValue("cbSalutations");

      try
      {
        if (val != null)
        {
          contact.setSalutationId(TypeConverter.asInt(val));
        }
      }
      catch (NumberFormatException e)
      {
        contact.setSalutationId(0);
      }

      //logger.debug("@userAdminHandler ---- cbSalutations == " + val);
      val = currNDPage.getDisplayFieldValue("tbUserTitle");

      if (val != null)
      {
        contact.setContactJobTitle(val.toString());
      }

      //logger.debug("@userAdminHandler ---- tbUserTitle == " + val);
      val = currNDPage.getDisplayFieldValue("tbUserFirstName");

      if (val != null)
      {
        contact.setContactFirstName(val.toString());
      }

      //logger.debug("@userAdminHandler ---- tbUserFirstName == " + val);
      val = currNDPage.getDisplayFieldValue("tbUserLastName");

      if (val != null)
      {
        contact.setContactLastName(val.toString());
      }

      //logger.debug("@userAdminHandler ---- tbUserLastName == " + val);
      val = currNDPage.getDisplayFieldValue("tbUserInitialName");

      if (val != null)
      {
        contact.setContactMiddleInitial(val.toString());
      }

      //logger.debug("@userAdminHandler ---- tbUserInitialName == " +
      // val);
      val = currNDPage.getDisplayFieldValue("tbWorkPhoneNumExtension");

      if (val != null)
      {
        contact.setContactPhoneNumberExtension(val.toString());
      }

      //logger.debug("@userAdminHandler ---- tbWorkPhoneNumExtension == "
      // + val);
      //if(textbox.getValue().isNumeric() == false )
      //{
      //  	setActiveMessageToAlert(Sc.USER_ADMIN_INVALID_EXT_NUMBER,
      // ActiveMsgFactory.ISCUSTOMCONFIRM);
      //  getSavedPages().setNextPage(pg);
      //	return -1;
      //}
      val = currNDPage.getDisplayFieldValue("tbEmailAddress");

      if (val != null)
      {
        contact.setContactEmailAddress(val.toString());
      }

      //logger.debug("@userAdminHandler ---- tbEmailAddress == " + val);
      String phonenumareacode =
        currNDPage.getDisplayFieldValue("tbWorkPhoneNumberAreaCode").toString();

      //logger.debug("@userAdminHandler ---- phonenumareacode == " +
      // phonenumareacode);
      if (TypeConverter.asInt(phonenumareacode, -1) == -1)
      {
        if (result != -1)
        {
          setActiveMessageToAlert(BXResources.getSysMsg("USER_ADMIN_INVALID_PHONE_NUMBER",
              theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
          result = -1;
        }

        //getSavedPages().setNextPage(pg);
        //return -1;
      }

      String phonenumfirstthree =
        currNDPage.getDisplayFieldValue("tbWorkPhoneNumFirstThreeDigits").toString();

      //logger.debug("@userAdminHandler ---- phonenumfirstthree == " +
      // phonenumfirstthree);
      if (TypeConverter.asInt(phonenumfirstthree, -1) == -1)
      {
        if (result != -1)
        {
          setActiveMessageToAlert(BXResources.getSysMsg("USER_ADMIN_INVALID_PHONE_NUMBER",
              theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
          result = -1;
        }

        //getSavedPages().setNextPage(pg);
        //return -1;
      }

      String phonenumlastfour =
        currNDPage.getDisplayFieldValue("tbWorkPhoneNumLastFourDigits").toString();

      //logger.debug("@userAdminHandler ---- phonenumlastfour == " +
      // phonenumlastfour);
      if (TypeConverter.asInt(phonenumlastfour, -1) == -1)
      {
        if (result != -1)
        {
          setActiveMessageToAlert(BXResources.getSysMsg("USER_ADMIN_INVALID_PHONE_NUMBER",
              theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
          result = -1;
        }

        //getSavedPages().setNextPage(pg);
        //return -1;
      }

      String userphonenum =
        getPhoneSearchString(phonenumareacode, phonenumfirstthree, phonenumlastfour);
      contact.setContactPhoneNumber(userphonenum);

      //logger.debug("@userAdminHandler ---- userphonenum == " +
      // userphonenum);
      String faxnumareacode = currNDPage.getDisplayFieldValue("tbFaxNumberAreaCode").toString();

      //logger.debug("@userAdminHandler ---- faxnumareacode == " +
      // faxnumareacode);
      if (TypeConverter.asInt(faxnumareacode, -1) == -1)
      {
        if (result != -1)
        {
          setActiveMessageToAlert(BXResources.getSysMsg("USER_ADMIN_INVALID_FAX_NUMBER",
              theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
          result = -1;
        }

        //getSavedPages().setNextPage(pg);
        //return -1;
      }

      String faxnumfirstthree =
        currNDPage.getDisplayFieldValue("tbFaxNumFirstThreeDigits").toString();

      //logger.debug("@userAdminHandler ---- faxnumfirstthree == " +
      // faxnumfirstthree);
      if (TypeConverter.asInt(faxnumfirstthree, -1) == -1)
      {
        if (result != -1)
        {
          setActiveMessageToAlert(BXResources.getSysMsg("USER_ADMIN_INVALID_FAX_NUMBER",
              theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
          result = -1;
        }

        //getSavedPages().setNextPage(pg);
        //return -1;
      }

      String faxnumlastfour = currNDPage.getDisplayFieldValue("tbFaxNumLastFourDigits").toString();

      //logger.debug("@userAdminHandler ---- faxnumlastfour == " +
      // faxnumlastfour);
      if (TypeConverter.asInt(faxnumlastfour, -1) == -1)
      {
        if (result != -1)
        {
          setActiveMessageToAlert(BXResources.getSysMsg("USER_ADMIN_INVALID_FAX_NUMBER",
              theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
          result = -1;
        }

        //getSavedPages().setNextPage(pg);
        //return -1;
      }

      String userfaxnum = getPhoneSearchString(faxnumareacode, faxnumfirstthree, faxnumlastfour);

      //logger.debug("@userAdminHandler ---- userfaxnum == " +
      // userfaxnum);
      contact.setContactFaxNumber(userfaxnum);

      //--Release2.1--//
      //--> Added Prefered Language ComboBox - By Billy 09Dec2002
      contact.setLanguagePreferenceId(defaultLanguageId);

      //=========================================================
      contact.ejbStore();
      up.ejbStore();

      if (result != -1)
      {
        //--> New requirement to write User Admin History records to DB
        //--> By Billy 17June2003
        // Create and setup the UserAdmin History Object
        UserProfile.UserAdminHistory theUAHistory = up.getUserAdminHistory();

        if (newUser)
        {
          theUAHistory.setGenericInfo(up.getUserProfileId(),
            UserAdminConfigManager.UA_EVENT_TYPE_CREATE);
        }
        else
        {
          theUAHistory.setGenericInfo(up.getUserProfileId(),
            UserAdminConfigManager.UA_EVENT_TYPE_UPDATE);
        }

        //Get the Screen Edit Buffer to populate the changes
        UserAdminEditBuffer efb = (UserAdminEditBuffer) pst.get(EDIT_BUFFER);
        efb.trackFieldChanges((UserAdminConfigManager) pst.get(CONFIGMGR), theUAHistory);

        //=============================================================
        return userprofileid;
      }
      else
      {
        return result;
      }
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error("Exception @UserAdminHandler.updateUserTables ");
      logger.error(e.getMessage() + "::" + e);
      logger.error(com.basis100.deal.util.StringUtil.stack2string(e));

      //			return -1;
    }

    return -1;
  }

  //////////////////////////////////////////////////////////////////////
  public int getNewUserProfileId()
  {
    try
    {
      //--> Converted to use Framework JdbcExecutor
      //--> Modified by Billy 15Aug2002
      String theSQL = "SELECT USERPROFILESEQ.NEXTVAL FROM DUAL";
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(theSQL);
      int result = -1;

      while (jExec.next(key))
      {
        result = jExec.getInt(key, 1);
      }

      jExec.closeData(key);

      return result;
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.getNewUserProfileId: Problem encountered in creating a new userprofileid.");
      logger.error(e.getMessage());
    }

    return -1;
  }

  /////////////////////////////////////////////////////////////////////
  public int getBranchContactAddrId(int branchprofileid)
  {
    try
    {
      //--> Converted to use Framework JdbcExecutor
      //--> Modified by Billy 15Aug2002
      String theSQL =
        "SELECT CONTACT.ADDRID FROM BRANCHPROFILE, CONTACT"
        + " WHERE BRANCHPROFILE.CONTACTID  =  CONTACT.CONTACTID"
        + " AND BRANCHPROFILE.BRANCHPROFILEID = " + branchprofileid;
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(theSQL);
      int result = -1;

      while (jExec.next(key))
      {
        result = jExec.getInt(key, 1);
      }

      jExec.closeData(key);

      return result;
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.getBranchContactAddrId: Problem encountered in getting branch contactId address.");
      logger.error(e.getMessage());
    }

    return -1;
  }

  //////////////////////////////////////////////////////////////////////
  public int getNewAuthorityId()
  {
    try
    {
      //--> Converted to use Framework JdbcExecutor
      //--> Modified by Billy 15Aug2002
      String theSQL = "SELECT APPROVALAUTHORITYSEQ.NEXTVAL FROM DUAL";
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(theSQL);
      int result = -1;

      while (jExec.next(key))
      {
        result = jExec.getInt(key, 1);
      }

      jExec.closeData(key);

      return result;
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.getNewAuthorityId: Problem encountered in creating a new authorityId.");
      logger.error(e.getMessage());
    }

    return -1;
  }

  /////////////////////////////////////////////////////////////////////////
  public void insertIntoApprovalAuthority(int userprofileid, int approvalauthorityid,
    double businesslimit, int authtypeid, SessionResourceKit srk)
  {
    String sql =
      "INSERT INTO APPROVALAUTHORITY (USERPROFILEID,APPROVALAUTHORITYTYPEID, "
      + " APPROVALLIMIT,APPROVALAUTHORITYID,APPROVALAUTHORITYTYPEVALUE, INSTITUTIONPROFILEID ) " + " VALUES("
      + userprofileid + " , " + " 0 , " + businesslimit + " , " + approvalauthorityid + " , "
      + authtypeid + " ," + srk.getExpressState().getDealInstitutionId() + ")";

    try
    {
      JdbcExecutor jExec = srk.getJdbcExecutor();
      jExec.executeUpdate(sql);
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.insertIntoApprovalAuthority: Problem encountered in updating ApprovalAuthority table.");
      logger.error(e.getMessage());
    }

    srk.setModified(true);

    return;
  }

  ///////////////////////////////////////////////////////////////////////
  public int getAuthorithyId(int userprofileid, int appAuthorityTypeValueId)
  {
    try
    {
      //--> Converted to use Framework JdbcExecutor
      //--> Modified by Billy 15Aug2002
      String theSQL =
        "SELECT APPROVALAUTHORITYID FROM APPROVALAUTHORITY" + " WHERE USERPROFILEID = "
        + userprofileid + " AND APPROVALAUTHORITYTYPEVALUE = " + appAuthorityTypeValueId;
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(theSQL);
      int result = 0;

      while (jExec.next(key))
      {
        result = jExec.getInt(key, 1);
      }

      jExec.closeData(key);

      return result;
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.getAuthorithyId: Problem encountered in getting AuthorityId.");
      logger.error(e.getMessage());
    }

    return 0;
  }

  //////////////////////////////////////////////////////////////////////
  public void updateApprovalAuthority(int userprofileid, double businesslimit, int approvalauthid,
    SessionResourceKit srk)
  {
    String sql =
      "UPDATE APPROVALAUTHORITY SET APPROVALLIMIT = " + businesslimit + " WHERE USERPROFILEID = "
      + userprofileid + " AND  APPROVALAUTHORITYID = " + approvalauthid;

    try
    {
      JdbcExecutor jExec = srk.getJdbcExecutor();
      jExec.executeUpdate(sql);
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.updateApprovalAuthority: Problem encountered in updating ApprovalAuthority.");
      logger.error(e.getMessage());
    }

    srk.setModified(true);

    return;
  }

  /////////////////////////////////////////////////////////////////////

  /**
   * BUSINESS RULES METHODS FOR USER ADMIN SCREEN.
   *
   * @param pg DOCUMENT ME!
   * @param vmessages DOCUMENT ME!
   * @param isNewUser DOCUMENT ME!
   */
  public void runAllUserAdminBusinessRules(PageEntry pg, Vector vmessages, boolean isNewUser)
  {
    try
    {
      // run business rule 1,2,3
      checkUserLoginBusinessId(pg, vmessages);

      // run business rule 4,5
      checkContactInfo(vmessages);

      // run business rule 6
      checkPassword(vmessages, isNewUser);

      // run business rule 7
      checkHigherJointApprover(vmessages);

      // run business rule 8
      /***** 3.2.2GR LOB allow 0 start *****/
      checkLineOfBusiness(vmessages);
      /***** 3.2.2GR LOB allow 0 end *****/
      
      // run business rule 9,10
      checkUserTypeAndPartner(vmessages);
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.runAllUserAdminBusinessRules: Problem encountered in running business rules.");
      logger.error(e.getMessage());
    }

    return;
  }

  //////////////////////////////////////////////////////////////////////
  public void checkUserLoginBusinessId(PageEntry pg, Vector vmessages)
  {
    boolean pgcondition1 = pg.getPageCondition1();
    //boolean pgcondition2 = pg.getPageCondition2();
    boolean pgcondition3 = pg.getPageCondition3();
    ViewBean currNDPage = getCurrNDPage();
    Object userlogin = null;
    Object businessid = null;

    try
    {
      userlogin = currNDPage.getDisplayFieldValue("tbLoginId");

      if ((userlogin == null) || (userlogin.toString().trim().length() == 0))
      {
        vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_EMPTY_LOGIN_FIELD",
              theSessionState.getLanguageId())));
      }

      businessid = currNDPage.getDisplayFieldValue("tbBusinessId");

      if ((businessid == null) || (businessid.toString().trim().length() == 0))
      {
        if (vmessages.size() == 0)
        {
          vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_EMPTY_LOGIN_FIELD",
                theSessionState.getLanguageId())));
        }
      }

      logger.debug(" --- userAdminHandler ---> checkUserLoginBusinessId( " + userlogin.toString()
        + ") return " + isUserLoginExistInSystem(userlogin));

      //if(goto is pressed)
      //		do not check existense
      //else
      //	check existence.
      // Submit button is pressed,
      //  condition1 == true
      // do not check
      // condition1 == false
      // check
      //logger.debug(" --- Toni ---> condition1 = " + pgcondition1 + "
      // and condition2 = " + pgcondition2);
      // if(goto is pressed) do not check for existense
      //if(addnewuser is pressed) do check for existense
      String update = pg.getPageState1();

      //--> Added protection checking here -- By Billy 19Aug2002
      if (update == null)
      {
        update = "";
      }

      //========================================================
      if (pgcondition3 && pgcondition1 && !update.equals("UPDATE"))
      //if(!(pgcondition2 && pgcondition1) || (pgcondition3 &&
      // pgcondition1))
      //if(!pgcondition2 && !pgcondition1)
      {
        if (isUserLoginExistInSystem(userlogin))
        {
          vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_LOGIN_ALREADY_EXIST",
                theSessionState.getLanguageId())));
        }

        //logger.debug(" --- userAdminHandler --->
        // isUPBusinessIdExistInSystem( "+ businessid.toString() + ")
        // return " + isUPBusinessIdExistInSystem(businessid));
        if (isUPBusinessIdExistInSystem(businessid))
        {
          vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_BUSINESSID_ALREADY_EXIST",
                theSessionState.getLanguageId())));
        }
      }
      else
      {
        Hashtable pst = pg.getPageStateTable();
        EditFieldsBuffer efb = (EditFieldsBuffer) pst.get(EDIT_BUFFER);
        String saveduserlogin = efb.getFieldValue("tbLoginId");
        String savedbusinessid = efb.getFieldValue("tbBusinessId");

        //logger.debug(" --- Toni ---> saveduserlogin == " +
        // saveduserlogin);
        //logger.debug(" --- Toni ---> userlogin == " +
        // userlogin.toString());
        //logger.debug(" --- Toni ---> savedbusinessid == " +
        // savedbusinessid);
        //logger.debug(" --- Toni ---> businessid == " +
        // businessid.toString());
        if (!saveduserlogin.equals(userlogin.toString()))
        {
          if (isUserLoginExistInSystem(userlogin))
          {
            vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_LOGIN_ALREADY_EXIST",
                  theSessionState.getLanguageId())));
          }
        }

        logger.debug(" --- userAdminHandler ---> isUPBusinessIdExistInSystem( "
          + businessid.toString() + ") return " + isUPBusinessIdExistInSystem(businessid));

        if (!savedbusinessid.equals(businessid.toString()))
        {
          if (isUPBusinessIdExistInSystem(businessid))
          {
            vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_BUSINESSID_ALREADY_EXIST",
                  theSessionState.getLanguageId())));
          }
        }
      }
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.checkUserLoginBusinessId: Problem encountered Checking if user login is in UserProfile.");
      logger.error(e.getMessage());
    }

    return;
  }

  //////////////////////////////////////////////////////////////////////
  public boolean isUserLoginExistInSystem(Object userlogin)
  {
    try
    {
      //--> Converted to use Framework JdbcExecutor
      //--> Modified by Billy 15Aug2002
      String theSQL =
        "SELECT * FROM USERPROFILE WHERE USERLOGIN = '" + userlogin.toString().toUpperCase() + "'";
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(theSQL);
      boolean result = false;

      while (jExec.next(key))
      {
        result = true;
      }

      jExec.closeData(key);

      return result;
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.isUserLoginExistInSystem: Problem encountered Checking if user login is in UserProfile.");
      logger.error(e.getMessage());
    }

    return false;
  }

  //////////////////////////////////////////////////////////////////////
  public boolean isUPBusinessIdExistInSystem(Object businessid)
  {
    try
    {
      //--> Converted to use Framework JdbcExecutor
      //--> Modified by Billy 15Aug2002
      String theSQL =
        "SELECT * FROM USERPROFILE WHERE UPBUSINESSID = '" + businessid.toString().toUpperCase()
        + "'";
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(theSQL);
      boolean result = false;

      while (jExec.next(key))
      {
        result = true;
      }

      jExec.closeData(key);

      return result;
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.isUPBusinessIdExistInSystem: Problem encountered Checking if businessId is in UserProfile.");
      logger.error(e.getMessage());
    }

    return false;
  }

  //////////////////////////////////////////////////////////////////////
  public void checkContactInfo(Vector vmessages)
  {
    ViewBean currNDPage = getCurrNDPage();
    Object val = null;
    val = currNDPage.getDisplayFieldValue("tbEmailAddress");

    if ((val == null) || (val.toString().trim().length() == 0))
    {
      vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_EMAIL_ADDR_MISSING",
            theSessionState.getLanguageId())));
    }

    val = currNDPage.getDisplayFieldValue("tbUserTitle");

    if ((val == null) || (val.toString().trim().length() == 0))
    {
      vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_EMPTY_CONTACT_INFO",
            theSessionState.getLanguageId())));

      return;
    }

    val = currNDPage.getDisplayFieldValue("tbUserFirstName");

    if ((val == null) || (val.toString().trim().length() == 0))
    {
      vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_EMPTY_CONTACT_INFO",
            theSessionState.getLanguageId())));

      return;
    }

    val = currNDPage.getDisplayFieldValue("tbUserLastName");

    if ((val == null) || (val.toString().trim().length() == 0))
    {
      vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_EMPTY_CONTACT_INFO",
            theSessionState.getLanguageId())));

      return;
    }

    //textbox =
    // (CSpTextBox)currNDPage.getDisplayField("tbUserInitialName");
    //val = textbox.getValue();
    //if (val == null || val.toString().trim().length() == 0)
    //{
    //	vmessages.put( new String( Sc.USER_ADMIN_EMPTY_CONTACT_INFO));
    //	return;
    //}
    // Phone Number
    String phonenumareacode =
      currNDPage.getDisplayFieldValue("tbWorkPhoneNumberAreaCode").toString();
    String phonenumfirstthree =
      currNDPage.getDisplayFieldValue("tbWorkPhoneNumFirstThreeDigits").toString();
    String phonenumlastfour =
      currNDPage.getDisplayFieldValue("tbWorkPhoneNumLastFourDigits").toString();
    String userphonenum =
      getPhoneSearchString(phonenumareacode, phonenumfirstthree, phonenumlastfour);

    if (userphonenum.equals(""))
    {
      vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_EMPTY_CONTACT_INFO",
            theSessionState.getLanguageId())));

      return;
    }

    String faxnumareacode = currNDPage.getDisplayFieldValue("tbFaxNumberAreaCode").toString();
    String faxnumfirstthree =
      currNDPage.getDisplayFieldValue("tbFaxNumFirstThreeDigits").toString();
    String faxnumlastfour = currNDPage.getDisplayFieldValue("tbFaxNumLastFourDigits").toString();
    String userfaxnum = getPhoneSearchString(faxnumareacode, faxnumfirstthree, faxnumlastfour);

    if (userfaxnum.equals(""))
    {
      vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_EMPTY_CONTACT_INFO",
            theSessionState.getLanguageId())));

      return;
    }

    return;
  }

  /////////////////////////////////////////////////////////////////////
  public void checkPassword(Vector vmessages, boolean isNewUser)
  {
    //-- comment out for Cervus II --//
    //-- by Neil on 09/28/2004
    //    String password = "";
    //    String verifypassword = "";
    //    ViewBean currNDPage = getCurrNDPage();
    //    Object val = null;
    //    val = currNDPage.getDisplayFieldValue("tbPassword");
    //
    //    //--Merge--EncriptedPassword--//
    //    //Handle Password encription -- By Billy 28Oct2002
    //    // - Assume NO password change if Password Fields are null
    //    // - For new user, must input password
    //    if (((val == null) || (val.toString().trim().length() == 0)) && isNewUser)
    //    {
    //      vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_PASSWORD_REQUIRED",
    //            theSessionState.getLanguageId())));
    //    }
    //    else
    //    {
    //      if (val != null)
    //      {
    //        password = val.toString().trim();
    //      }
    //      else
    //      {
    //        password = "";
    //      }
    //    }
    //
    //    //====================================================================
    //    val = currNDPage.getDisplayFieldValue("tbVerifyPassword");
    //
    //    if (val != null)
    //    {
    //      verifypassword = val.toString();
    //    }
    //
    //    if (!password.equals(verifypassword))
    //    {
    //      vmessages.add(new String(BXResources.getSysMsg("USER_PASSWORD_NOT_MATCH_VERIFICATION",
    //            theSessionState.getLanguageId())));
    //    }
    //    else
    //    {
    //      //New Password check handling -- By BILLY 17April2002
    //      Hashtable pst = getTheSessionState().getCurrentPage().getPageStateTable();
    //      String tmpResult = null;
    //
    //      try
    //      {
    //        UserProfile up = new UserProfile(srk);
    //
    //        if (isNewUser)
    //        {
    //          // For new user we don't have UserProfileId
    //          tmpResult = up.validateNewPassword(password, theSessionState.getLanguageId());
    //        }
    //        else
    //        {
    //          // For Existing User ==> Skip checking if Password not
    //          // changed
    //          int userprofileid = getIntValue((String) pst.get("USERPROFILEID"));
    //          up.findByPrimaryKey(new UserProfileBeanPK(userprofileid));
    //
    //          //--Merge--EncriptedPassword--//
    //          //Handle Password encription -- By Billy 28Oct2002
    //          // - Assume NO password change if Password Fields are null
    //          //if(!up.getPassword().equals(password))
    //          if (password.length() > 0)
    //          {
    //            tmpResult = up.validateNewPassword(password, theSessionState.getLanguageId());
    //          }
    //
    //          //=======================================================
    //        }
    //      }
    //      catch (Exception e)
    //      {
    //        logger.error("Exception @checkPassword : " + e.getMessage());
    //      }
    //
    //      if ((tmpResult != null) && (tmpResult.trim().length() > 0))
    //      {
    //        vmessages.add(new String(tmpResult));
    //      }
    //    }
    //
    //    return;
    ViewBean currNDPage = getCurrNDPage();

    int institutionId = theSessionState.getDealInstitutionId();
    // retrieve new password
    String newPassword = currNDPage.getDisplayFieldValue("tbPassword").toString();

    // retrieve verify password
    String newVPassword =
      currNDPage.getDisplayFieldValue(pgUserAdministrationViewBean.CHILD_TBVERIFYPASSWORD).toString();

    // retrieve usertype
    int userType = -1;

    //--> Cervus II : Added AUTOUSER edit. (AME Spec. Section 3.2.2-3)
    //--> By Neil : 09/28/2004
    if (newPassword == null)
    {
      newPassword = "";
    }

    if (newVPassword == null)
    {
      newVPassword = "";
    }

    if (!newPassword.equals(newVPassword))
    {
      // not equals
      vmessages.add(new String(BXResources.getSysMsg("USER_PASSWORD_NOT_MATCH_VERIFICATION",
            theSessionState.getLanguageId())));
    }
    else
    {
      Hashtable pst = getTheSessionState().getCurrentPage().getPageStateTable();
      UserProfile up = null;

      try
      {
        int userprofileid = getIntValue((String) pst.get("USERPROFILEID"));

        // Get handler points to the current entity-userprofile object.
        up = new UserProfile(srk);

        // For existing user, up now points an entity with that userprofileid
        if (!isNewUser)
        {
          up.findByPrimaryKey(new UserProfileBeanPK(userprofileid, institutionId));
          userType = up.getUserTypeId();
        }
      }
      catch (FinderException fe)
      {
        logger.error("===> Exception : @userAdminHandler -----> checkPassword() : "
          + fe.getMessage());
      }
      catch (RemoteException re)
      {
        logger.error("===> Exception : @userAdminHandler -----> checkPassword() : "
          + re.getMessage());
      }

      //------------------------------------------------------------------
      String tmpResult = null;
      
      //password for create duplicate user ID for ticket FXP21191 
      String userLogin = getCurrNDPage()
                    .getDisplayFieldValue("tbLoginId").toString();

      srk.getExpressState().cleanAllIds();
      boolean supressMsg = true;
      supressMsg = checkCurrentLogin(userLogin, institutionId);
      srk.getExpressState().setDealInstitutionId(institutionId);
      //password for create duplicate user ID  for ticket FXP21191
      
      if ((newPassword.length() == 0) && isNewUser && supressMsg)
      {
        vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_PASSWORD_REQUIRED",
              theSessionState.getLanguageId())));
      }
      else if (!isNewUser)
      {
        // For existing user
        if (userType == 99)
        {
          // for AUTOUSER(UserType=99), critical is : newPassword == passwordFromDB
          // no need to validateNewPassword
          String storedPassword = up.getPassword();

          // for encrypted password, restore password to unencrypted.
          if (PropertiesCache.getInstance()
                .getProperty(-1, "com.basis100.useradmin.passwordencrypted", "Y").equals("Y"))
          {  // encrypted
            storedPassword = PasswordEncoder.getEncodedPassword(storedPassword);
          }

          if (!newPassword.equals(storedPassword))
          {
            vmessages.add(new String(BXResources.getSysMsg("USER_AUTO_PASSWORD_REQUIRED",
                  theSessionState.getLanguageId())));
          }
        }
        else
        {
          // for other users, if password is empty, it means the password will not be changed and skip checking
          if (newPassword.length() > 0)
          {
            tmpResult = up.validateNewPassword(newPassword, theSessionState.getLanguageId());
          }

          if ((tmpResult != null) && (tmpResult.trim().length() > 0))
          {
            vmessages.add(new String(tmpResult));
          }
        }
      }
      else
      {
        // new user with newPassword.length > 0
        if (newPassword.length() > 0)
        {
          tmpResult = up.validateNewPassword(newPassword, theSessionState.getLanguageId());
        }

        if ((tmpResult != null) && (tmpResult.trim().length() > 0))
        {
          vmessages.add(new String(tmpResult));
        }
      }
    }
  }


   /**
     * GR 3.2.2 generate error message on line of business neagtive values
     * 
     * @param vmessages
     */
    public void checkLineOfBusiness(Vector vmessages) {
        ViewBean currNDPage = getCurrNDPage();
        Object linebusinessA = null;
        Object linebusinessB = null;
        Object linebusinessC = null;
        int usertypeid = -1;
        Object val = null;
        val = currNDPage.getDisplayFieldValue("cbUserTypes");

        if ((val != null) && (val.toString().length() != 0)) {
            usertypeid = TypeConverter.asInt(val);
        }

        linebusinessA = currNDPage.getDisplayFieldValue("tbLineOfBusiness1");
        linebusinessB = currNDPage.getDisplayFieldValue("tbLineOfBusiness2");
        linebusinessC = currNDPage.getDisplayFieldValue("tbLineOfBusiness3");

        if (usertypeid != -1) {
            if (((usertypeid >= Mc.USER_TYPE_PASSIVE) && (usertypeid <= Mc.USER_TYPE_SR_ADMIN))
                    || (usertypeid == Mc.USER_TYPE_SYS_ADMINISTRATOR)) {
                if ((convertLobValue(linebusinessA) < 0)
                        || (convertLobValue(linebusinessB) < 0)
                        || ((convertLobValue(linebusinessC) < 0))) {

                    vmessages.add(new String(BXResources.getSysMsg(
                            "USER_ADMIN_LINE_OF_BUSINESS_NOT_REQUIRED",
                            theSessionState.getLanguageId())));
                }
            } else if ((usertypeid >= Mc.USER_TYPE_JR_UNDERWRITER)
                    && (usertypeid <= Mc.USER_TYPE_SENIOR_MGR)) {
                if (((linebusinessA != null) && (convertLobValue(linebusinessA) < 0 || linebusinessA
                        .toString().trim().length() == 0))
                        || ((linebusinessB != null) && (convertLobValue(linebusinessB) < 0 || linebusinessB
                                .toString().trim().length() == 0))
                        || ((linebusinessC != null) && (convertLobValue(linebusinessC) < 0 || linebusinessC
                                .toString().trim().length() == 0))) {

                    vmessages.add(new String(BXResources.getSysMsg(
                            "USER_ADMIN_LINE_OF_BUSINESS_REQUIRED",
                            theSessionState.getLanguageId())));
                }
            }
        }
        return;
    }


  /////////////////////////////////////////////////////////////////////
  public void checkHigherJointApprover(Vector vmessages)
  {
    ComboBox comboBox = null;
    int usertypeid = 0;
    int higherapproveruserid = 0;
    int jointapproveruserid = 0;
    ViewBean currNDPage = getCurrNDPage();
    Object val = null;
    comboBox = (ComboBox) currNDPage.getDisplayField("cbUserTypes");
    val = comboBox.getValue();

    if ((val != null) && (val.toString().length() != 0))
    {
      usertypeid = TypeConverter.asInt(val);
    }

    comboBox = (ComboBox) currNDPage.getDisplayField("cbHigherApprovers");
    val = comboBox.getValue();

    if ((val != null) && (val.toString().length() != 0))
    {
      higherapproveruserid = TypeConverter.asInt(val);
    }

    comboBox = (ComboBox) currNDPage.getDisplayField("cbJointApprovers");
    val = comboBox.getValue();

    if ((val != null) && (val.toString().length() != 0))
    {
      jointapproveruserid = TypeConverter.asInt(val);
    }

    //logger.debug("------ Toni checkHigherJointApprover---> usertypeid = "
    // + usertypeid);
    //logger.debug("------ Toni checkHigherJointApprover--->
    // higherapproveruserid = "+ higherapproveruserid);
    //logger.debug("------ Toni checkHigherJointApprover--->
    // jointapproveruserid = " + jointapproveruserid);
    if (usertypeid != 0)
    {
      // Taken in out in the last BR changed --> March 15, 2001.
      // usertypeid != Mc.USER_TYPE_SENIOR_MGR &&
      if ((usertypeid != Mc.USER_TYPE_JR_ADMIN) && (usertypeid != Mc.USER_TYPE_ADMIN)
            && (usertypeid != Mc.USER_TYPE_SR_ADMIN)
            && (usertypeid != Mc.USER_TYPE_SYS_ADMINISTRATOR)
            && (usertypeid != Mc.USER_TYPE_INV_ADMIN) && (usertypeid != Mc.USER_TYPE_PASSIVE))
      {
        if ((higherapproveruserid == 0) && (jointapproveruserid == 0))
        {
          vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_HIGHER_JOINT_APPROVER",
                theSessionState.getLanguageId())));
        }
        else if ((higherapproveruserid != 0) && (jointapproveruserid != 0))
        {
          vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_HIGHER_JOINT_APPROVER",
                theSessionState.getLanguageId())));
        }
      }
      else
      {
        if ((higherapproveruserid != 0) || (jointapproveruserid != 0))
        {
          vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_HIGHER_JOINT_APP_NOT_REQ",
                theSessionState.getLanguageId())));
        }
      }
    }
    else
    {
      vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_USER_TYPE_IS_REQUIRED",
            theSessionState.getLanguageId())));
    }

    return;
  }

  /////////////////////////////////////////////////////////////////////
  public void checkUserTypeAndPartner(Vector vmessages)
  {
    ComboBox comboBox = null;
    int usertypeid = -1;
    int partneruserid = -1;
    ViewBean currNDPage = getCurrNDPage();
    Object val = null;
    comboBox = (ComboBox) currNDPage.getDisplayField("cbUserTypes");
    val = comboBox.getValue();

    if ((val != null) && (val.toString().length() != 0))
    {
      usertypeid = TypeConverter.asInt(val);
    }

    comboBox = (ComboBox) currNDPage.getDisplayField("cbPartnerProfileNames");
    val = comboBox.getValue();

    if ((val != null) && (val.toString().length() != 0))
    {
      partneruserid = TypeConverter.asInt(val);
    }

    //logger.debug(" --- userAdminHandler ---> usertypeid == " +
    // usertypeid);
    //logger.debug(" --- userAdminHandler ---> partneruserid == " +
    // partneruserid );
    if (partneruserid == -1)
    {
      return;
    }

    //logger.debug(" --- userAdminHandler ---> isUserIsUnderWriter( " +
    // usertypeid + ") return " + isUserIsUnderWriter(usertypeid));
    if (isUserIsUnderWriter(usertypeid))
    {
      int partnertypeid = getUserTypeId(partneruserid);

      //logger.debug(" --- userAdminHandler ---> user is an
      // underwriter!!!");
      //logger.debug(" --- userAdminHandler ---> his partner is
      // partnertypeid == " +TypeConverter.asInt(partnertypeid));
      //logger.debug(" --- userAdminHandler ---> !!!
      // isUserIsAdministrator( " +TypeConverter.asInt(partnertypeid) + ")
      // return ! "
      // +TypeConverter.asInt(isUserIsAdministrator(partnertypeid)));
      if (partnertypeid != -1)
      {
        if (!(isUserIsAdministrator(partnertypeid)))
        {
          vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_UD_PARTNER_MUST_BE_ADMIN",
                theSessionState.getLanguageId())));
        }
      }
    }

    logger.debug(" --- userAdminHandler ---> isUserIsAdministrator( " + usertypeid + ") return "
      + isUserIsAdministrator(usertypeid));

    if (isUserIsAdministrator(usertypeid))
    {
      int partnertypeid = getUserTypeId(partneruserid);

      //logger.debug(" --- userAdminHandler ---> user is an
      // administrator!!!");
      //logger.debug(" --- userAdminHandler ---> his partner is
      // partnertypeid == " +TypeConverter.asInt(partnertypeid));
      //logger.debug(" --- userAdminHandler ---> !!! isUserIsUnderWriter(
      // " +TypeConverter.asInt(partnertypeid) + ") return ! "
      // +TypeConverter.asInt(isUserIsUnderWriter(partnertypeid)));
      if (partnertypeid != -1)
      {
        if (!(isUserIsUnderWriter(partnertypeid)))
        {
          vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_ADMIN_PARTNER_MUST_BE_UD",
                theSessionState.getLanguageId())));
        }
      }
    }

    //--> Cervus II : Added to disable a user to AUTOUSER type
    //--> By Neil : 09/30/2004
    if (usertypeid == 99)
    {
      //	logger.debug("USER_AUTO_INVALID_NEW_USER_TYPE");
      logger.debug(
        "=====> @userAdminHandler : checkUserTypeAndPartner : Can not assign a user to AUTOUSER type.");
      vmessages.add(new String(BXResources.getSysMsg("USER_AUTO_INVALID_NEW_USER_TYPE",
            theSessionState.getLanguageId())));
    }

    return;
  }

  /////////////////////////////////////////////////////////////////////
  public boolean isUserIsUnderWriter(int usertypeid)
  {
    if (usertypeid == Mc.USER_TYPE_JR_UNDERWRITER)
    {
      return true;
    }

    if (usertypeid == Mc.USER_TYPE_UNDERWRITER)
    {
      return true;
    }

    if (usertypeid == Mc.USER_TYPE_SR_UNDERWRITER)
    {
      return true;
    }

    return false;
  }

  /////////////////////////////////////////////////////////////////////
  public boolean isUserIsAdministrator(int usertypeid)
  {
    if (usertypeid == Mc.USER_TYPE_JR_ADMIN)
    {
      return true;
    }

    if (usertypeid == Mc.USER_TYPE_ADMIN)
    {
      return true;
    }

    if (usertypeid == Mc.USER_TYPE_SR_ADMIN)
    {
      return true;
    }

    return false;
  }

  /////////////////////////////////////////////////////////////////////
  public int getUserTypeId(int userprofileid)
  {
    try
    {
      //--> Converted to use Framework JdbcExecutor
      //--> Modified by Billy 15Aug2002
      String theSQL = "SELECT USERTYPEID FROM USERPROFILE WHERE USERPROFILEID = " + userprofileid;
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(theSQL);
      int result = -1;

      while (jExec.next(key))
      {
        result = jExec.getInt(key, 1);
      }

      jExec.closeData(key);

      return result;
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.getUserTypeId: Problem encountered in getting usertypeid.");
      logger.error(e.getMessage());
    }

    return -1;
  }

  /////////////////////////////////////////////////////////////////////
  public int getUserContactId(int userprofileid)
  {
    try
    {
      //--> Converted to use Framework JdbcExecutor
      //--> Modified by Billy 15Aug2002
      String theSQL = "SELECT CONTACTID FROM USERPROFILE WHERE USERPROFILEID = " + userprofileid;
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(theSQL);
      int result = -1;

      while (jExec.next(key))
      {
        result = jExec.getInt(key, 1);
      }

      jExec.closeData(key);

      return result;
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.getUserContactId: Problem encountered in getting usertypeid.");
      logger.error(e.getMessage());
    }

    return -1;
  }

  /////////////////////////////////////////////////////////////////////

  /**
   * DYNAMIC POPULATIONS OF COMBOBOXES USING JAVASCRIPT
   */
  public void prepareJScriptData()
  {
    StringBuffer institutionArr = new StringBuffer();
    StringBuffer regionArr = new StringBuffer();
    StringBuffer branchArr = new StringBuffer();
    StringBuffer groupArr = new StringBuffer();
    Vector vInstitutions = new Vector();
    Vector vRegions = new Vector();
    Vector vBranches = new Vector();
    institutionArr = constructInstitutionArr(vInstitutions);
    regionArr = constructRegionArr(vInstitutions, vRegions);
    branchArr = constructBranchArr(vRegions, vBranches);
    groupArr = constructGroupArr(vBranches);

    try
    {
      ComboBox cbinstitution = (ComboBox) getCurrNDPage().getDisplayField("cbInstitutionNames");
      ComboBox cbregion = (ComboBox) getCurrNDPage().getDisplayField("cbRegionNames");
      ComboBox cbbranch = (ComboBox) getCurrNDPage().getDisplayField("cbBranchNames");
      getCurrNDPage().setDisplayFieldValue("stInstitutionArray",
        new String(institutionArr.toString()));
      getCurrNDPage().setDisplayFieldValue("stRegionArray", new String(regionArr.toString()));
      getCurrNDPage().setDisplayFieldValue("stBranchArray", new String(branchArr.toString()));
      getCurrNDPage().setDisplayFieldValue("stGroupArray", new String(groupArr.toString()));

      String onChangeAttrib1 = new String("onChange=\"populateRegions() \"");
      String onChangeAttrib2 = new String("onChange=\"populateBranches()\"");
      String onChangeAttrib3 = new String("onChange=\"populateGroups()\"");

      //-->For testing by BILLY 25Feb2003
      //HtmlDisplayFieldBase df =
      // (HtmlDisplayFieldBase)getCurrNDPage().getDisplayField("tbBusinessId");
      //df.setExtraHtml(df.getExtraHtml() + " style=\"display:inline\"
      // disabled");
      //===============================
      //logger.debug("@userAdminHandler ---- prepareJScriptData.");
      cbinstitution.setExtraHtml(onChangeAttrib1);
      cbregion.setExtraHtml(onChangeAttrib2);
      cbbranch.setExtraHtml(onChangeAttrib3);
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.prepareJScriptData: Problem encountered in creating array For javaScript.");
      logger.error(e.getMessage());
    }
  }

  ////////////////////////////////////////////////////////////////////////////////
  public StringBuffer constructInstitutionArr(Vector vinstitutions)
  {
    String definstitutionprofilename = null;
    int definstitutionprofileid = 0;
    StringBuffer institutionArr = new StringBuffer();
    Vector v = new Vector();
    String str = "";

    try
    {
      String sqlStr =
        " SELECT INSTITUTIONPROFILE.INSTITUTIONPROFILEID, "
        + " INSTITUTIONPROFILE.INSTITUTIONNAME FROM INSTITUTIONPROFILE ";

      //--> Converted to use Framework JdbcExecutor
      //--> Modified by Billy 15Aug2002
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(sqlStr);

      while (jExec.next(key))
      {
        // Get institutions
        vinstitutions.add(new Integer(jExec.getInt(key, 1)));
        definstitutionprofileid = jExec.getInt(key, 1);
        definstitutionprofilename = jExec.getString(key, 2);
        str =
          "\"\\\"" + definstitutionprofilename + "\\\",\\\"" + definstitutionprofileid + "\\\"\"";
        v.add(new String(str));
      }

      jExec.closeData(key);

      //--Release2.1--//
      //--> Modified to get NONSelect label from ResourceBundle
      //--> By Billy 09Dec2002
      institutionArr.append("i0 = new Array(\"'"
        + BXResources.getGenericMsg("CBINSTITUTIONNAMES_NONSELECTED_LABEL",
          theSessionState.getLanguageId()) + "','-1'\"");

      String delim = ",";

      for (int i = 0; i < v.size(); i++)
      {
        institutionArr.append(delim + v.get(i).toString());
      }

      institutionArr.append(");\n");
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.constructInstitutionArr: Problem encountered in creating array of institutions.");
      logger.error(e.getMessage());
    }

    return institutionArr;
  }

  /////////////////////////////////////////////////////////////////////////////
  //--Release2.1--//
  //-->Modified to support Multilingual -- By Billy 06Dec2002
  public StringBuffer constructRegionArr(Vector vinstitutions, Vector vregions)
  {
    int defregionprofileid = 0;
    String defregionprofilename = null;
    StringBuffer regionArr = new StringBuffer();
    Vector v = new Vector();
    String str = "";

    try
    {
      for (int i = 0; i < vinstitutions.size(); i++)
      {
        //--> Converted to use Framework JdbcExecutor
        //--> Modified by Billy 15Aug2002
        // Should not elimate the 0 record -- changed by BILLY 26May2001
        String sqlStr =
          " SELECT REGIONPROFILE.REGIONPROFILEID " + " FROM REGIONPROFILE "
          + " where REGIONPROFILE.INSTITUTIONPROFILEID = " + vinstitutions.get(i);

        JdbcExecutor jExec = srk.getJdbcExecutor();
        int key = jExec.execute(sqlStr);

        while (jExec.next(key))
        {
          // Get regions
          vregions.add(new Integer(jExec.getInt(key, 1)));
          defregionprofileid = jExec.getInt(key, 1);
          defregionprofilename =
              BXResources.getPickListDescription(theSessionState.getDealInstitutionId(), 
                      "REGIONPROFILE", jExec.getInt(key, 1),
                       theSessionState.getLanguageId());
          str = "\"\\\"" + defregionprofilename + "\\\",\\\"" + defregionprofileid + "\\\"\"";
          v.add(new String(str));
        }

        jExec.closeData(key);

        // Change to Populate the Array name with institutionid -- BILLY
        // 19June2001
        //regionArr.append("r"+i+" = new Array(\"'Not Applicable
        // ','-1'\"");
        regionArr.append("r" + TypeConverter.asInt(vinstitutions.get(i)) + " = new Array(\"'"
          + BXResources.getGenericMsg("CBREGIONNAMES_NONSELECTED_LABEL",
            theSessionState.getLanguageId()) + "','-1'\"");

        String delim = ",";

        for (int k = 0; k < v.size(); k++)
        {
          regionArr.append(delim + v.get(k).toString());
        }

        regionArr.append(");\n");
        v = new Vector();
      }

      // for
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.constructRegionArr: Problem encountered in creating array of Region.");
      logger.error(e.getMessage());
    }

    return regionArr;
  }

  ////////////////////////////////////////////////////////////////////////////
  //--Release2.1--//
  //-->Modified to support Multilingual -- By Billy 06Dec2002
  public StringBuffer constructBranchArr(Vector vregions, Vector vbranches)
  {
    int defbranchprofileid = 0;
    String defbranchprofilename = null;
    StringBuffer branchArr = new StringBuffer();
    Vector v = new Vector();
    String str = "";

    try
    {
      for (int i = 0; i < vregions.size(); i++)
      {
        String sqlStr =
          " SELECT BRANCHPROFILE.BRANCHPROFILEID " + " FROM BRANCHPROFILE "
          + " WHERE BRANCHPROFILE.REGIONPROFILEID = " + vregions.get(i);

        JdbcExecutor jExec = srk.getJdbcExecutor();
        int key = jExec.execute(sqlStr);

        while (jExec.next(key))
        {
          // Get branches
          defbranchprofileid = jExec.getInt(key, 1);
          defbranchprofilename =
              BXResources.getPickListDescription(theSessionState.getDealInstitutionId(), 
                      "BRANCHPROFILE", jExec.getInt(key, 1), theSessionState.getLanguageId());
          str = "\"\\\"" + defbranchprofilename + "\\\",\\\"" + defbranchprofileid + "\\\"\"";
          v.add(new String(str));
          vbranches.add(new Integer(jExec.getInt(key, 1)));
        }

        jExec.closeData(key);

        // Change to Populate the Array name with regionid -- BILLY
        // 19June2001
        //branchArr.append("b"+i+" = new Array(\"'Not Applicable
        // ','-1'\"");
        branchArr.append("b" + TypeConverter.asInt(vregions.get(i)) + " = new Array(\"'"
          + BXResources.getGenericMsg("CBBRANCHNAMES_NONSELECTED_LABEL",
            theSessionState.getLanguageId()) + "','-1'\"");

        String delim = ",";

        for (int k = 0; k < v.size(); k++)
        {
          branchArr.append(delim + v.get(k).toString());
        }

        branchArr.append(");\n");
        v = new Vector();
      }

      // for
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.constructBranchArr: Problem encountered in creating array of branches.");
      logger.error(e.getMessage());
    }

    return branchArr;
  }

  ////////////////////////////////////////////////////////////////////////////
  //--Release2.1--//
  //-->Modified to support Multilingual -- By Billy 06Dec2002
  public StringBuffer constructGroupArr(Vector vbranches)
  {
    int defgroupprofileid = 0;
    String defgroupprofilename = null;
    StringBuffer groupArr = new StringBuffer();
    Vector v = new Vector();
    String str = "";

    try
    {
      for (int i = 0; i < vbranches.size(); i++)
      {
        //=================================================
        //--> CRF#37 :: Source/Firm to Group routing for TD
        //=================================================
        //--> Modified to display the Group Name from DB if key not
        // found in ResourceBundle
        //--> By Billy 18Aug2003
        String sqlStr =
          " SELECT GROUPPROFILE.GROUPPROFILEID, GROUPPROFILE.GROUPNAME " + " FROM GROUPPROFILE "
          + " WHERE GROUPPROFILE.BRANCHPROFILEID = " + vbranches.get(i);

        //--> Converted to use Framework JdbcExecutor
        //--> Modified by Billy 15Aug2002
        JdbcExecutor jExec = srk.getJdbcExecutor();
        int key = jExec.execute(sqlStr);

        while (jExec.next(key))
        {
          // Get groups
          defgroupprofileid = jExec.getInt(key, 1);
          defgroupprofilename = 
              BXResources.getPickListDescription(theSessionState.getDealInstitutionId(), 
                      "GROUPPROFILE", jExec.getInt(key, 1), theSessionState.getLanguageId());

          //Key Not found if Return String starts with '?' ==> then
          // keep the original value
          if (defgroupprofilename.startsWith("?") && defgroupprofilename.endsWith("?"))
          {
            defgroupprofilename = jExec.getString(key, 2);
          }

          str = "\"\\\"" + defgroupprofilename + "\\\",\\\"" + defgroupprofileid + "\\\"\"";
          v.add(new String(str));
        }

        //==================================================
        jExec.closeData(key);

        // Change to Populate the Array name with brancheid -- BILLY
        // 19June2001
        //groupArr.append("g"+i+" = new Array(\"'Not Applicable
        // ','-1'\"");
        groupArr.append("g" + TypeConverter.asInt(vbranches.get(i)) + " = new Array(\"'"
          + BXResources.getGenericMsg("CBGROUPPROFILENAME_NONSELECTED_LABEL",
            theSessionState.getLanguageId()) + "','-1'\"");

        String delim = ",";

        for (int k = 0; k < v.size(); k++)
        {
          groupArr.append(delim + v.get(k).toString());
        }

        groupArr.append(");\n");
        v = new Vector();
      }

      // for
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.constructGroupArr: Problem encountered in creating array of Groups.");
      logger.error(e.getMessage());
    }

    return groupArr;
  }

  /////////////////////////////////////////////////////////////////////

  /**
   * JAVASCRIPT For Dynamic population of Screen Access pages
   *
   * @param numofrows DOCUMENT ME!
   */
  public void prepareJScriptForPageAccess(int numofrows)
  {
    StringBuffer screenAccessArr = new StringBuffer();
    Vector vUserTypes = new Vector();

    try
    {
      getMeUserTypes(vUserTypes);
      screenAccessArr = constructScreenAccessArr(vUserTypes);

      ComboBox cbusertype = (ComboBox) getCurrNDPage().getDisplayField("cbUserTypes");
      getCurrNDPage().setDisplayFieldValue("stPageAccessArray",
        new String(screenAccessArr.toString()));

      String onChangeAttrib1 =
        new String("onChange=\"populateRepeatedScreenAccess('" + numofrows + "'); \"");

      //logger.debug("@userAdminHandler ----
      // prepareJScriptForPageAccess.");
      //logger.debug("@userAdminHandler ---- getMeUserTypes.size() == " +
      // vUserTypes.size() );
      //logger.debug("@userAdminHandler ---- screenAccessArr === " +
      // screenAccessArr.toString() );
      //logger.debug("@userAdminHandler ---- onChangeAttrib1 === " +
      // onChangeAttrib1 );
      cbusertype.setExtraHtml(onChangeAttrib1);
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.prepareJScriptForPageAccess: Problem encountered in creating array For javaScript.");
      logger.error(e.getMessage());
    }

    return;
  }

  ////////////////////////////////////////////////////////////////////////////////
  public void getMeUserTypes(Vector v)
  {
    try
    {
      //--> Converted to use Framework JdbcExecutor
      //--> Modified by Billy 15Aug2002
      String theSQL =
        "SELECT USERTYPE.USERTYPEID FROM USERTYPE WHERE " + " USERTYPE.USERTYPEID <> 0 ";
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(theSQL);

      while (jExec.next(key))
      {
        v.add(new Integer(jExec.getInt(key, 1)));
      }

      jExec.closeData(key);
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.getUserTypeId: Problem encountered in getting usertypeid.");
      logger.error(e.getMessage());
    }

    return;
  }

  ////////////////////////////////////////////////////////////////////////////////
  public StringBuffer constructScreenAccessArr(Vector vusertypes)
  {
    int accesstypeid = 0;
    StringBuffer screenAccessArr = new StringBuffer();
    Vector v = new Vector();
    String str = "";

    try
    {
      for (int i = 0; i < vusertypes.size(); i++)
      {
        String sqlStr =
          " SELECT USERTYPEPAGEACCESS.ACCESSTYPEID FROM USERTYPEPAGEACCESS "
          + " WHERE USERTYPEPAGEACCESS.USERTYPEID = " + TypeConverter.asInt(vusertypes.get(i))
          + " ORDER BY USERTYPEPAGEACCESS.PAGEID  ASC ";

        //--> Converted to use Framework JdbcExecutor
        //--> Modified by Billy 15Aug2002
        JdbcExecutor jExec = srk.getJdbcExecutor();
        int key = jExec.execute(sqlStr);
        boolean hasData = false;

        while (jExec.next(key))
        {
          accesstypeid = jExec.getInt(key, 1);
          str = "'" + accesstypeid + "'";
          v.add(new String(str));
          hasData = true;
        }

        jExec.closeData(key);

        if (hasData == true)
        {
          String delim = ",";
          screenAccessArr.append("sa" + i + " = new Array(");

          for (int k = 0; k < v.size(); k++)
          {
            if (k != (v.size() - 1))
            {
              screenAccessArr.append(v.get(k).toString() + delim);
            }
            else
            {
              screenAccessArr.append(v.get(k).toString() + ");");
            }
          }
        }

        v = new Vector();
      }
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.constructGroupArr: Problem encountered in creating array of ScreenAccess.");
      logger.error(e.getMessage());
    }

    logger.debug("BILLY ==> in constructScreenAccessArr - screenAccessArr: " + screenAccessArr);

    return screenAccessArr;
  }

  ///////////////////////////////////////////////////////////////////////////
  public void setDefHighJointApprover(ComboBox thefield)
  {
    Object val = thefield.getValue();

    if ((val != null) && (val.toString().trim().length() != 0))
    {
      if (TypeConverter.asInt(val) == 0)
      {
        thefield.getOptions().populate(RequestManager.getRequestContext());
      }
    }
  }

  ///////////////////////////////////////////////////////////////////////////
  public String sendSelectedHighJointApprover(String fieldName)
  {
    Object val = null;

    if (fieldName.equals("stOldSelectedJointApp"))
    {
      val = getCurrNDPage().getDisplayFieldValue("cbHigherApprovers");
    }
    else
    {
      val = getCurrNDPage().getDisplayFieldValue("cbJointApprovers");
    }

    return val.toString();
  }

  ////////////////////////////////////////////////////////////////////////////////

  /**
   * JAVA SCRIPT FOR BUSINESS RULES
   */
  public void prepareJScriptForHighJointApp()
  {
    try
    {
      ComboBox cbhighapp = (ComboBox) getCurrNDPage().getDisplayField("cbHigherApprovers");
      ComboBox cbjointapp = (ComboBox) getCurrNDPage().getDisplayField("cbJointApprovers");
      String onChangeAttrib1 =
        new String("onChange=\"checkHighJointUserType('cbHigherApprovers') \"");
      String onChangeAttrib2 =
        new String("onChange=\"checkHighJointUserType('cbJointApprovers')\"");

      //logger.debug("@userAdminHandler ----
      // prepareJScriptForHighJointApp.");
      cbhighapp.setExtraHtml(onChangeAttrib1);
      cbjointapp.setExtraHtml(onChangeAttrib2);
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception @UserAdminHandler.prepareJScriptForHighJointApp: Problem encountered in creating array For javaScript.");
      logger.error(e.getMessage());
    }
  }

  //////////////////////////////////////////////////////////////////////
  public void deleteApprovalAuthority(int approvalauthid, SessionResourceKit srk)
  {
    String sql = "DELETE FROM APPROVALAUTHORITY WHERE " + "APPROVALAUTHORITYID = " + approvalauthid;

    try
    {
      JdbcExecutor jExec = srk.getJdbcExecutor();
      jExec.executeUpdate(sql);
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error("Exception @UserAdminHandler.deleteApprovalAuthority: ");
      logger.error(e.getMessage());
    }

    srk.setModified(true);

    return;
  }

  //--> Method to set fields disable
  //--> By Billy 04Mar2003
  private void setupDisabledFields(ViewBean thePage)
  {
    HtmlDisplayFieldBase df;
    Hashtable pst = getTheSessionState().getCurrentPage().getPageStateTable();
    Iterator it = ((UserAdminConfigManager) pst.get(CONFIGMGR)).getAllDisableFields();

    if (it != null)
    {
      String fieldName = "";

      for (; it.hasNext();)
      {
        fieldName = (String) (it.next());
        df = (HtmlDisplayFieldBase) thePage.getDisplayField(fieldName);
        df.setExtraHtml(df.getExtraHtml() + " disabled");
      }
    }
  }

  //--> New Method to check if file is hidden
  //--> By Billy 05Mar2003
  protected boolean isFieldHidden(String fieldName)
  {
    UserAdminConfigManager theConfigMgr =
      (UserAdminConfigManager) (getTheSessionState().getCurrentPage().getPageStateTable().get(CONFIGMGR));

    if (theConfigMgr != null)
    {
      return theConfigMgr.isFieldHidden(fieldName);
    }
    else
    {
      return false;
    }
  }

  //=================================================
  //--> CRF#37 :: Source/Firm to Group routing for TD
  //=================================================
  //--> Method to create new Group
  //--> By Billy 13Aug2003
  public void handleCreateGroup()
  {
    SessionResourceKit srk = getSessionResourceKit();
    PageEntry pg = getTheSessionState().getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    Vector vmessages = new Vector();
    EditFieldsBuffer efb = (EditFieldsBuffer) pst.get(EDIT_BUFFER);

    try
    {
      srk.beginTransaction();

      boolean pgcondition3 = pg.getPageCondition3();
      pg.setPageCondition1(true);

      Object obj = pst.get("NEWUSER");
      String newuserstr = null;
      boolean newuser = true;

      if (obj instanceof String)
      {
        newuserstr = (String) obj;
      }
      else
      {
        newuserstr = "";
      }

      if (newuserstr.equals("NO"))
      {
        newuser = false;
      }

      // Check if it is OK to create Group
      runBusinessRulesToCreateGroup(pg, vmessages, pgcondition3 && newuser);

      if (vmessages.size() == 0)
      {
        runAllUserAdminBusinessRules(pg, vmessages, pgcondition3 && newuser);
      }

      if (vmessages.size() > 0)
      {
        srk.cleanTransaction();
        constructStringBufferOfMessages(vmessages);
        pg.setPageState1("ERROR");
        getSavedPages().setNextPage(pg);
        navigateToNextPage();

        return;
      }

      // Save the all Data first
      int userprofileid = updateUserTables(pg, srk, pgcondition3 && newuser, true);

      if (userprofileid != -1)
      {
        // Create the New Group if user data successfully updated
        // get the BranchId selected -- assume it was already checked if
        // it's OK
        String branchprofileid = efb.getFieldValue("cbBranchNames");
        int branchId = TypeConverter.asInt(branchprofileid);
        String newGpName = efb.getFieldValue("tbNewGroupName");
        /**
         * BEGIN FIX FOR A problem occured message is shown when creating New User Group[FXP-21051]
         * Mohan V Premkumar
         * 04/01/2008
         */
        GroupProfile theGp = new GroupProfile(srk).create(branchId, newGpName, Integer.valueOf(efb.getFieldValue("cbInstitutionNames")).intValue());
        /**
         * END FIX FOR A problem occured message is shown when creating New User Group[FXP-21051]
         */
        // Add the Group to the User
        UserProfile up =
          new UserProfile(srk).findByPrimaryKey(new UserProfileBeanPK(userprofileid, pg.getDealInstitutionId()));
        up.getGroupIds().add(new Integer(theGp.getGroupProfileId()));
        up.ejbStore();
        srk.commitTransaction();
        pg.setPageState1("UPDATE");
        pst.put("NEWUSER", new String("NO"));
        pg.setPageCondition3(false);
      }
      else
      {
        // Update failed
        //Roll back if error exists
        pg.setPageState1("ERROR");

        if (newuser)
        {
          pst.put("NEWUSER", new String("YES"));
        }

        srk.rollbackTransaction();
      }

      getSavedPages().setNextPage(pg);
      navigateToNextPage();
    }
    catch (Exception e)
    {
      srk.cleanTransaction();
      logger.error("Exception occurred @handleSubmitStandard()");
      logger.error(e);
      setStandardFailMessage();

      return;
    }
  }

  //Method to check if OK to create New Group
  //--> By Billy 13Aug2003
  public void runBusinessRulesToCreateGroup(PageEntry pg, Vector vmessages, boolean isNewUser)
  {
    ViewBean currNDPage = getCurrNDPage();

    // Rule 1 : Cannot create New Group if new user
    if (isNewUser)
    {
      vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_NEW_USER_CANNOT_CREATE_GROUP",
            theSessionState.getLanguageId())));

      return;
    }

    String newGroupName = currNDPage.getDisplayFieldValue("tbNewGroupName").toString().trim();

    // Rule 2 : New Group Name filed must input
    if (newGroupName.length() <= 0)
    {
      vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_MISSING_NEW_GROUP_NAME",
            theSessionState.getLanguageId())));

      return;
    }

    // Rule 5 : Check if valid Branch profileId selected
    String branchprofileid = currNDPage.getDisplayFieldValue("cbBranchNames").toString().trim();
    int branchId = TypeConverter.asInt(branchprofileid, -1);

    if (branchId < 0)
    {
      vmessages.add(new String(BXResources.getSysMsg("USER_ADMIN_MISSING_BRANCHID",
            theSessionState.getLanguageId())));

      return;
    }

    // Rule 4 : Check if GroupName is already used
    boolean isDupGpName = false;

    try
    {
      isDupGpName = new GroupProfile(srk).isDupGroupName(newGroupName, branchId);
    }
    catch (Exception e)
    {
      logger.warning(
        "Exception @userAdminHandler.runBusinessRulesToCreateGroup :: when checking for Duplicated Group Name :: "
        + e);
    }

    if (isDupGpName)
    {
      String tmpStr =
        new String(BXResources.getSysMsg("USER_ADMIN_DUP_GROUP_NAME",
            theSessionState.getLanguageId()));

      try
      {
        tmpStr =
          com.basis100.deal.util.Format.printf(tmpStr,
            new com.basis100.deal.util.Parameters(newGroupName));
      }
      catch (Exception e)
      {
        logger.warning(
          "@userAdminHandler.runBusinessRulesToCreateGroup:: Exception when formating string:: "
          + e);
      }

      vmessages.add(tmpStr);

      return;
    }
  }

  public String generateBeginingOfNewGroupComment()
  {
    PageEntry pg = getTheSessionState().getCurrentPage();

    if (!pg.getPageCondition2())
    {
      return "<!--";
    }
    else
    {
      return "";
    }
  }

  public String generateEndingOfNewGroupComment()
  {
    PageEntry pg = getTheSessionState().getCurrentPage();

    if (!pg.getPageCondition2())
    {
      return "-->";
    }
    else
    {
      return "";
    }
  }

  //=====================================================================
  //--Ticket #239: Display or not Display Submit button--start--//

  /**
   * Populate default values (hiddens, etc.). Eliminates hardcoded call from JSP.
   *
   * @param thePage DOCUMENT ME!
   */
  protected void populateDefaultValues(ViewBean thePage)
  {
    SessionStateModelImpl theSessionState = getTheSessionState();
    PageEntry pg = theSessionState.getCurrentPage();
    String theExtraHtml = "";

    // the OnChange string to be set on the fields
    HtmlDisplayFieldBase df;
    df = (HtmlDisplayFieldBase) thePage.getDisplayField("cbUserNames");
    theExtraHtml =
      "onChange='if(isUserMeunDown) performGo();'" + " onclick='toggleUserMenuDownFlag();'"
      + " onkeypress='return handleUserNameKeyPressed()'" + " onBlur='isUserMeunDown=false;'";
    df.setExtraHtml(theExtraHtml);
  }

  //--Ticket #239--end--//.
  
  /*
     * method to check if the user is try to create duplicate login on other
     * institution
     */
    public boolean checkCurrentLogin(String userLogin, int institutionId) {

        UserProfile up = null;
        try {
            up = new UserProfile(srk);
            up.findByLoginId(userLogin);
        } catch (Exception e) {
            logger.error("@userAdminHandler checkCurrentLogin() : "
                    + e.getMessage());
            return true;
        }
        
        // good usersecutitypolicy
        boolean goodSecurityPolicy = false;
        if(!up.isPasswordLocked() && !up.isPasswordReset()&&
                (up.getProfileStatusId() == 0)&&(up.getPasswordGraceCount()<4))
            goodSecurityPolicy = true;
        
        Map upMap = up.getInstIdToUPId();
        
        if (goodSecurityPolicy && !upMap.containsKey(institutionId))
            return false;
        else
            return true;
    }
    
    /**
     * GR 3.2.2 convert line of business value
     * @param val
     * @return
     */
    public double convertLobValue(Object val) {
        double businesslimit = 0;
        try {
            if (val != null) {
                businesslimit = TypeConverter.asDouble(val);
            }
        } catch (NumberFormatException e) {
            businesslimit = -1;
        }
        return businesslimit;
    }
}


/////////////////////////////////////////////////////////////////////////
class UserAdminResultRow
  extends EditFieldsBuffer
  implements Serializable
{
  private static String[][] buf =
  {
    { "StaticText", "Repeated1/stScreenName" },
    { "StaticText", "Repeated1/stOverideIndicator" },
    { "ComboBox", "Repeated1/cbPageAccessTypes" },
    { "StaticText", "Repeated1/stBlankSpace" },
    { "StaticText", "Repeated1/stScreenName" }
  };
  private String[][] dataBuffer;

  /**
   *
   *
   */
  public UserAdminResultRow(String name, SysLogger logger, screenAccessRow row)
  {
    super(name, logger);

    int len = buf.length;
    setNumberOfFields(len);
    dataBuffer = new String[len][2];

    for (int i = 0; i < len; ++i)
    {
      for (int j = 0; j < 2; j++)
      {
        dataBuffer[i][j] = "";
      }
    }

    setFieldValue(0, "" + row.getPageLabel());
    setFieldValue(1, row.getOverideIndicator());
    setFieldValue(2, "" + row.getAccessTypeId());
    setFieldValue(3, row.getBlankSpace());
    setFieldValue(4, row.getPageLabel());
  }

  /**
   *
   *
   */
  public void setFieldValue(int fldNdx, String value)
  {
    dataBuffer[fldNdx][0] = value;
  }

  /**
   *
   *
   */
  public String getFieldName(int fldNdx)
  {
    return buf[fldNdx][1];
  }

  /**
   *
   *
   */
  public String getFieldType(int fldNdx)
  {
    return buf[fldNdx][0];
  }

  /**
   *
   *
   */
  public String getFieldValue(int fldNdx)
  {
    return dataBuffer[fldNdx][0];
  }

  // --- //
  // satisfy interface for limited implementation (used for screen population
  // only!) ...
  public String getOriginalValue(int fldNdx)
  {
    return "";
  }

  /**
   *
   *
   */
  public String getDOFieldName(int fldNdx)
  {
    return "";
  }

  /**
   *
   *
   */
  public void setFieldName(int fldNdx, String value)
  {
    ;
  }

  /**
   *
   *
   */
  public void setDOFieldName(int fldNdx, String value)
  {
    ;
  }

  /**
   *
   *
   */
  public void setFieldType(int fldNdx, String value)
  {
    ;
  }

  /**
   *
   *
   */
  public void setOriginalValue(int fldNdx, String value)
  {
    ;
  }
}


/////////////////////////////////////////////////////////////////////////
class screenAccessRow
  extends Object
{
  private int accesstypeid = 0;
  private int pageid = 0;
  private String pagelabel = "";
  private String overideindicator = "";
  private String blankspace = "";

  /**
   *
   *
   */
  public screenAccessRow()
  {
  }

  /**
   *
   *
   */
  public screenAccessRow(String pagelbl, int acctpid, int pgid, String overrideind, String blkspace)
  {
    pagelabel = pagelbl;
    accesstypeid = acctpid;
    overideindicator = overrideind;
    blankspace = blkspace;
    pageid = pgid;
  }

  // getters
  public String getPageLabel()
  {
    return pagelabel;
  }

  /**
   *
   *
   */
  public int getAccessTypeId()
  {
    return accesstypeid;
  }

  /**
   *
   *
   */
  public int getPageId()
  {
    return pageid;
  }

  /**
   *
   *
   */
  public String getOverideIndicator()
  {
    return overideindicator;
  }

  /**
   *
   *
   */
  public String getBlankSpace()
  {
    return blankspace;
  }

  // setters
  public void setPageLabel(String pagelbl)
  {
    pagelabel = pagelbl;
  }

  /**
   *
   *
   */
  public void setAccessTypeId(int acctpid)
  {
    accesstypeid = acctpid;
  }

  /**
   *
   *
   */
  public void setPageId(int pgid)
  {
    pageid = pgid;
  }

  /**
   *
   *
   */
  public void setOverideIndicator(String overidind)
  {
    overideindicator = overidind;
  }

  /**
   *
   *
   */
  public void setBlankSpace(String blkspace)
  {
    blankspace = blkspace;
  }

  ///////////////////////////////////////////////////////
  public void deserialize(DataInput inputStream)
    throws IOException
  {
  }

  /**
   *
   *
   */

  //public int getType()
  //{
  //	return CSpValue.USER_TYPE;
  //}

  /**
   *
   *
   */
  public void serialize(DataOutput outputStream)
    throws IOException
  {
  }
}


////////////////////////////////////////////////////////////////////////////////
//=================================================
//--> CRF#37 :: Source/Firm to Group routing for TD
//=================================================
//--> Special handle for ListBox fields to store more than
//--> one Value for each control.
//--> By Billy 12Aug2003
class UserAdminEditBuffer
  extends EditFieldsBuffer
  implements Serializable
{
  private static String[][] buf =
  {
    { "TextBox", "tbUserTitle" },
    { "TextBox", "tbUserFirstName" },
    { "TextBox", "tbUserInitialName" },
    { "TextBox", "tbUserLastName" },
    { "TextBox", "tbLoginId" },
    { "TextBox", "hdLoginId" },
    { "TextBox", "tbBusinessId" },
    { "TextBox", "tbWorkPhoneNumberAreaCode" },
    { "TextBox", "tbWorkPhoneNumFirstThreeDigits" },
    { "TextBox", "tbWorkPhoneNumLastFourDigits" },
    { "TextBox", "tbWorkPhoneNumExtension" },
    { "TextBox", "tbFaxNumberAreaCode" },
    { "TextBox", "tbFaxNumFirstThreeDigits" },
    { "TextBox", "tbFaxNumLastFourDigits" },
    { "TextBox", "tbEmailAddress" },
    { "TextBox", "tbPassword" },
    { "TextBox", "tbVerifyPassword" },
    { "TextBox", "tbLineOfBusiness1" },
    { "TextBox", "tbLineOfBusiness2" },
    { "TextBox", "tbLineOfBusiness3" },
    { "ComboBox", "cbUserNames" },
    { "ComboBox", "cbProfileStatus" },
    { "ComboBox", "cbUserTypes" },
    { "ComboBox", "cbLanguages" },
    
    { "ComboBox", "cbTaskAlert" },
    { "ComboBox", "cbInstitutionNames" },
    { "ComboBox", "cbRegionNames" },
    { "ComboBox", "cbBranchNames" },
    { "ComboBox", "cbManagers" },
    { "ComboBox", "cbSalutations" },
    { "ComboBox", "cbJointApprovers" },
    { "ComboBox", "cbHigherApprovers" },
    { "ComboBox", "cbStandIn" },
    { "ComboBox", "cbPartnerProfileNames" },
    { "TextBox", "chForceToChangePW" },
    { "TextBox", "chLockPW" },
    { "TextBox", "chNewPW" },
    { "StaticText", "stLastLoginTime" },
    { "StaticText", "stLastLogoutTime" },
    { "StaticText", "stLoginFlag" },
    
    { "ComboBox", "cbDefaultLanguage" },
    
    { "TextBox", "tbNewGroupName" },
    { "ListBox", "lbGroups" }
  };

  //===============================================================================
  //--> Change it to Object Array in order to store any Data Type
  //--> Assume : for regular filed type e.g. TextBox, ComboBox ... it is a
  // String
  //--> Special filed like ListBox will store a Object[].
  //--> By Billy 12Aug2003
  //private String[][] dataBuffer;
  private Object[][] dataBuffer;

  /**
   *
   *
   */
  public UserAdminEditBuffer(String name, SysLogger logger)
  {
    super(name, logger);

    int len = buf.length;
    setNumberOfFields(len);

    //--> Changed to Object array -- By Billy 12Aug2003
    //dataBuffer = new String [ len ] [ 2 ];
    dataBuffer = new Object[len][2];

    for (int i = 0; i < len; ++i)
    {
      for (int j = 0; j < 2; j++)
      {
        dataBuffer[i][j] = "";
      }
    }

    //--> need to set the default value of the checkbox here
    //--> Otherwise will cause exceptions
    //--> By Billy 16Aug2002
    setFieldValue("chForceToChangePW", "N");
    setOriginalValue("chForceToChangePW", "N");
    setFieldValue("chLockPW", "N");
    setOriginalValue("chLockPW", "N");
    setFieldValue("chNewPW", "N");
    setOriginalValue("chNewPW", "N");

    //======================================================
  }

  //////////////////////////////////////////////////////////////////////////
  public String getFieldName(int fldNdx)
  {
    return buf[fldNdx][1];
  }

  //////////////////////////////////////////////////////////////////////////
  public String getDOFieldName(int fldNdx)
  {
    return "";
  }

  //////////////////////////////////////////////////////////////////////////
  public String getFieldType(int fldNdx)
  {
    return buf[fldNdx][0];
  }

  //////////////////////////////////////////////////////////////////////////
  public String getFieldValue(int fldNdx)
  {
    return (String) (dataBuffer[fldNdx][0]);
  }

  //////////////////////////////////////////////////////////////////////////
  public String getOriginalValue(int fldNdx)
  {
    return (String) (dataBuffer[fldNdx][1]);
  }

  //////////////////////////////////////////////////////////////////////////
  //--> New Methods to support multiple field values -- By Billy 12Aug2003
  public Object[] getFieldValues(int fldNdx)
  {
    if ((dataBuffer[fldNdx][0] instanceof Object[]))
    {
      return (Object[]) (dataBuffer[fldNdx][0]);
    }
    else
    {
      return null;
    }
  }

  public Object[] getFieldValues(String fldName)
  {
    int ndx = getFieldNdx(fldName);

    if (ndx == -1)
    {
      return null;
    }

    return getFieldValues(ndx);
  }

  //////////////////////////////////////////////////////////////////////////
  public Object[] getOriginalValues(int fldNdx)
  {
    if ((dataBuffer[fldNdx][1] instanceof Object[]))
    {
      return (Object[]) (dataBuffer[fldNdx][1]);
    }
    else
    {
      return null;
    }
  }

  public Object[] getOriginalValues(String fldName)
  {
    int ndx = getFieldNdx(fldName);

    if (ndx == -1)
    {
      return null;
    }

    return getOriginalValues(ndx);
  }

  //========================================================================
  //////////////////////////////////////////////////////////////////////////
  public void setFieldName(int fldNdx, String value)
  {
    buf[fldNdx][1] = value;
  }

  //////////////////////////////////////////////////////////////////////////
  public void setDOFieldName(int fldNdx, String value)
  {
    ;
  }

  //////////////////////////////////////////////////////////////////////////
  public void setFieldType(int fldNdx, String value)
  {
    buf[fldNdx][0] = value;
  }

  //////////////////////////////////////////////////////////////////////////
  public void setFieldValue(int fldNdx, String value)
  {
    dataBuffer[fldNdx][0] = value;
  }

  //////////////////////////////////////////////////////////////////////////
  public void setOriginalValue(int fldNdx, String value)
  {
    dataBuffer[fldNdx][1] = value;
  }

  //--> New Methods to support multiple field values -- By Billy 12Aug2003
  //////////////////////////////////////////////////////////////////////////
  public void setFieldValues(int fldNdx, Object[] value)
  {
    dataBuffer[fldNdx][0] = value;
  }

  public void setFieldValues(String fldName, Object[] value)
  {
    int ndx = getFieldNdx(fldName);

    if (ndx == -1)
    {
      return;
    }

    dataBuffer[ndx][0] = value;
  }

  //////////////////////////////////////////////////////////////////////////
  public void setOriginalValues(int fldNdx, Object[] value)
  {
    dataBuffer[fldNdx][1] = value;
  }

  public void setOriginalValues(String fldName, Object[] value)
  {
    int ndx = getFieldNdx(fldName);

    if (ndx == -1)
    {
      return;
    }

    dataBuffer[ndx][1] = value;
  }

  //========================================================================
  //--> Special handling for Disable or Hide display fields
  //--> By Billy 25Feb2003
  //--> Added handling for ListBox -- By Billy 12Aug2003
  public void readFields(ViewBean thePage, UserAdminConfigManager theConfigMgr)
  {
    int lim = getNumberOfFields();

    // working vars
    String fldName = null;

    for (int i = 0; i < lim; ++i)
    {
      // first check for a mapping to a display field
      fldName = getFieldName(i);

      if ((fldName == null) || (fldName.length() == 0))
      {
        continue;
      }

      switch (fldTypeNumeric(getFieldType(i)))
      {
      case FT_TEXTBOX:
      case FT_HIDDEN:
      case FT_COMBOBOX:

        //--> Check if the field is Disabled or Hidden then set the
        // value from Original value
        if (theConfigMgr.isFieldDisableOrHidden(fldName))
        {
          setFieldValue(i, this.getOriginalValue(i));
          thePage.setDisplayFieldValue(fldName, this.getOriginalValue(i));
        }
        else
        {
          setFieldValue(i, thePage.getDisplayFieldValue(fldName).toString().trim());
        }

        if (fldName.equals("cbTaskAlert"))
        {
          logger.debug("===> NEIL : @userAdminHandler:readFields()\t" + "TaskAlert = "
            + getOriginalValue(i));
          logger.debug("===> NEIL : @userAdminHandler:readFields()\t" + "value = "
            + thePage.getDisplayFieldValue(fldName).toString().trim());
        }

        break;

      case FT_LISTBOX:

        ListBox theLB = (ListBox) thePage.getDisplayField(fldName);

        //--> Check if the field is Disabled or Hidden then set the
        // value from Original value
        if (theConfigMgr.isFieldDisableOrHidden(fldName))
        {
          setFieldValues(i, this.getOriginalValues(i));
          theLB.setValues(this.getOriginalValues(i));
        }
        else
        {
          setFieldValues(i, theLB.getValues());
        }

        break;

      case FT_STATIC:

        //We cannot get the field value from a StaticText ==> preserve
        // the previous value
        //  -- By BILLY 12April2002
        break;

      default:
        logger.error("Warning @EditFieldsBuffer(name=" + name
          + ").readFields: no support for for field type = " + getFieldType(i) + ", filedName="
          + getFieldName(i) + " - input ignored");

        break;
      }
    }
  }

  //--> New requirement (for COOP) to track the chnages of UserAdmin fields
  //--> By Billy 15June2003
  // New Method to track any Screen Field changes
  //--> Added handle for ListBox fileds
  public void trackFieldChanges(UserAdminConfigManager theConfigMgr,
    UserProfile.UserAdminHistory uaHistory)
    throws Exception
  {
    int lim = getNumberOfFields();
    String s = null;
    String fldName = null;
    int fldType = 0;

    for (int i = 0; i < lim; ++i)
    {
      // first check for a mapping to a display field
      fldName = getFieldName(i);
      fldType = fldTypeNumeric(getFieldType(i));

      if ((fldName == null) || (fldName.length() == 0))
      {
        continue;
      }

      //--> Check if the field is Trackable
      if (theConfigMgr.isFieldAuditable(fldName, uaHistory.getEventType()))
      {
        // No need to track the changes if Create
        if (uaHistory.getEventType() == UserAdminConfigManager.UA_EVENT_TYPE_UPDATE)
        {
          if (fldType == EditFieldsBuffer.FT_LISTBOX)
          {
            // Compare the Delimited Values String
            s = getFieldValuesIfModified(i);
          }
          else
          {
            // Check if field changed in all format
            s = getFieldValueIfModified(i);
          }
        }

        if ((s != null)
              || (uaHistory.getEventType() == UserAdminConfigManager.UA_EVENT_TYPE_CREATE))
        {
          if (fldType == EditFieldsBuffer.FT_LISTBOX)
          {
            if (s == null)
            {
              s = getFieldValuesInString(i);
            }

            uaHistory.setAuditFieldId(fldName);
            uaHistory.setUaPreviousValueText(getOriginalValuesInString(i));
            uaHistory.setUaCurrentValueText(s);
            uaHistory.store();
          }
          else
          {
            if (s == null)
            {
              s = getFieldValue(i);
            }

            uaHistory.setAuditFieldId(fldName);
            uaHistory.setUaPreviousValueText(getOriginalValue(i));
            uaHistory.setUaCurrentValueText(s);
            uaHistory.setUaPreviousValue(getIntValue(getOriginalValue(i)));
            uaHistory.setUaCurrentValue(getIntValue(s));
            uaHistory.store();
          }
        }
      }

      s = null;
    }

    //for
  }

  //=======================================================================
  //--> Added handle for Multiple values fields i.e. ListBox
  //--> By Billy 12Aug2003
  public void populateFields(ViewBean thePage)
  {
    Object[] o = null;
    String fldName = null;
    int lim = getNumberOfFields();

    for (int i = 0; i < lim; ++i)
    {
      // first check for a mapping to a display field
      fldName = getFieldName(i);

      if ((fldName == null) || (fldName.length() == 0))
      {
        continue;
      }

      switch (fldTypeNumeric(getFieldType(i)))
      {
      case FT_TEXTBOX:
      case FT_HIDDEN:
      case FT_STATIC:
      case FT_COMBOBOX:

        try
        {
          // must be rewritten as the following
          // getDisplayFieldValue(int i).getValue() method could be
          // called
          // only after this
          thePage.setDisplayFieldValue(fldName, new String(getDFStr(getFieldValue(i))));

          if (fldName.equals("cbTaskAlert"))
          {
            logger.debug("===> NEIL : " + "@userAdminHandler.populateFields()\t" + "TaskAlert = "
              + new String(getDFStr(getFieldValue(i))));
          }
        }
        catch (Exception e)
        {
          logger.error("Exception @EditFieldsBuffer(name=" + name
            + ").populateFields: populating field, fileName=" + getFieldName(i) + ", value=<"
            + getFieldValue(i) + ">, CSpPage=" + thePage + " - ignored");
          logger.error(e);
        }

        break;

      case FT_LISTBOX:

        try
        {
          o = getFieldValues(i);

          ListBox theLB = (ListBox) thePage.getDisplayField(fldName);
          theLB.setValues(o);

          //--> Billy try : also set the value to the first choice
          //if(o != null && o.length > 0)
          //  theLB.setValue(o[0]);
        }
        catch (Exception e)
        {
          logger.error("Exception @EditFieldsBuffer(name=" + name
            + ").populateFields: populating field, fileName=" + getFieldName(i) + ", values=<" + o
            + ">, CSpPage=" + thePage + " - ignored");
          logger.error(e);
        }

        break;

      default:
        logger.error("Warning @EditFieldsBuffer(name=" + name
          + ").populateFields: no support for for field type = " + getFieldType(i) + ", filedName="
          + getFieldName(i) + " - don't know how to populate, input ignored");

        break;
      }
    }
  }

  //--> Method to get all values and formated in a delimited string
  //--> By Billy 12Aug2003
  private String getFieldValuesInString(int fldNdx)
  {
    if (!(getFieldValues(fldNdx) instanceof Object[]))
    {
      return "";
    }

    Object[] o = getFieldValues(fldNdx);

    if ((o == null) || (o.length <= 0))
    {
      return "";
    }

    StringBuffer buf = new StringBuffer();

    for (int i = 0; i < o.length; i++)
    {
      if (i > 0)
      {
        buf.append("|" + (String) (o[i]));
      }
      else
      {
        buf.append((String) (o[i]));
      }
    }

    return buf.toString();
  }

  private String getOriginalValuesInString(int fldNdx)
  {
    if (!(getOriginalValues(fldNdx) instanceof Object[]))
    {
      return "";
    }

    Object[] o = getOriginalValues(fldNdx);

    if ((o == null) || (o.length <= 0))
    {
      return "";
    }

    StringBuffer buf = new StringBuffer();

    for (int i = 0; i < o.length; i++)
    {
      if (i > 0)
      {
        buf.append("|" + (String) (o[i]));
      }
      else
      {
        buf.append((String) (o[i]));
      }
    }

    return buf.toString();
  }

  // Method to check if field values changed
  public String getFieldValuesIfModified(int fldNdx)
  {
    if (fldNdx == -1)
    {
      return null;
    }

    String original = getOriginalValuesInString(fldNdx);
    String current = getFieldValuesInString(fldNdx);
    logger.debug("BILLY ===> @getFieldValuesIfModified : " + getFieldName(fldNdx)
      + " Changed : original = " + original + " current = " + current);

    if (original.equals(current))
    {
      return null;
    }

    return current;
  }

  public void setOriginalAsCurrent()
  {
    int lim = getNumberOfFields();

    for (int i = 0; i < lim; ++i)
    {
      if (fldTypeNumeric(getFieldType(i)) == FT_LISTBOX)
      {
        setOriginalValues(i, getFieldValues(i));
      }
      else
      {
        setOriginalValue(i, getFieldValue(i));
      }
    }
  }

  //==========================================
}
