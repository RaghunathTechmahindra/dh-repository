package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doPageNameLabelModelImpl extends QueryModelBase
    implements doPageNameLabelModel
{
    /**
     *
     *
     */
    public doPageNameLabelModelImpl()
    {
        super();
        setDataSourceName(DATA_SOURCE_NAME);

        setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

        setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

        setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

        setFieldSchema(FIELD_SCHEMA);

        initialize();

    }


    /**
     *
     *
     */
    public void initialize()
    {
    }


    ////////////////////////////////////////////////////////////////////////////////
    // NetDynamics-migrated events
    ////////////////////////////////////////////////////////////////////////////////
    /**
     *
     *
     */
    protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
        throws ModelControlException
    {

        // TODO: Migrate specific onBeforeExecute code
        return sql;

    }


    /**
     *
     *
     */
    protected void afterExecute(ModelExecutionContext context, int queryType)
        throws ModelControlException
    {
    //--Release2.1--//
    //Test by Billy 05Nov2002 -- to override all the Description fields to be populated
    //by BXResource PickList

    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
            getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();

    //Convert all Descriptions (all results) to the appropriate language
    while(this.next())
    {
      // Convert PageLabel Desc.
      ////this.setDfPageLabel(BXResources.getPickListDescription("PAGELABEL", getDfPageID().intValue(), languageId));
      //--DJ_LDI_CR--start--//
      //// No Life&Disability Insurance screen.
      if (PropertiesCache.getInstance().getProperty(-1, "com.basis100.ldinsurance.displaylifedisabilitymodule", "N").equals("N"))
      {
          //this.setDfPageLabel(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),"PAGELABEL", getDfPageID().intValue(), languageId));
          this.setDfPageLabel(getDfPageID().toString());
      }
      //// Give an access to the Life&Disability Insurance screen.
      else if (PropertiesCache.getInstance().getProperty(-1, "com.basis100.ldinsurance.displaylifedisabilitymodule", "N").equals("Y"))
      {
          //this.setDfPageLabel(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),"DJPAGELABEL", getDfPageID().intValue(), languageId));
          this.setDfPageLabel(getDfPageID().toString());
      }
      //--DJ_LDI_CR--end--//

    }

    //Reset the Result Cursor
    this.beforeFirst();
    }


    /**
     *
     *
     */
    protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
    {
    }


    /**
     *
     *
     */
    public java.math.BigDecimal getDfPageID()
    {
        return (java.math.BigDecimal)getValue(FIELD_DFPAGEID);
    }


    /**
     *
     *
     */
    public void setDfPageID(java.math.BigDecimal value)
    {
        setValue(FIELD_DFPAGEID,value);
    }


    /**
     *
     *
     */
    public String getDfPageName()
    {
        return (String)getValue(FIELD_DFPAGENAME);
    }


    /**
     *
     *
     */
    public void setDfPageName(String value)
    {
        setValue(FIELD_DFPAGENAME,value);
    }


    /**
     *
     *
     */
    public String getDfPageLabel()
    {
        return (String)getValue(FIELD_DFPAGELABEL);
    }


    /**
     *
     *
     */
    public void setDfPageLabel(String value)
    {
        setValue(FIELD_DFPAGELABEL,value);
    }


      public java.math.BigDecimal getDfInstitutionId() {
          return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
        }

        public void setDfInstitutionId(java.math.BigDecimal value) {
          setValue(FIELD_DFINSTITUTIONID, value);
        }

    ////////////////////////////////////////////////////////////////////////////
    // Obsolete Netdynamics Events - Require Manual Migration
    ////////////////////////////////////////////////////////////////////////////





    ////////////////////////////////////////////////////////////////////////////
    // Custom Methods - Require Manual Migration
    ////////////////////////////////////////////////////////////////////////////





    ////////////////////////////////////////////////////////////////////////////
    // Class variables
    ////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////
    // Class variables
    ////////////////////////////////////////////////////////////////////////////////

    public static final String DATA_SOURCE_NAME="jdbc/orcl";
    
    public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT PAGE.PAGEID, PAGE.PAGENAME, PAGE.PAGELABEL,PAGE.INSTITUTIONPROFILEID FROM PAGE  __WHERE__  ORDER BY PAGE.PAGENAME  ASC";

    public static final String MODIFYING_QUERY_TABLE_NAME="PAGE";
    
    public static final String STATIC_WHERE_CRITERIA=" PAGE.INCLUDEINGOTO = 'Y' ";
    
    public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
    public static final String QUALIFIED_COLUMN_DFPAGEID="PAGE.PAGEID";
    public static final String COLUMN_DFPAGEID="PAGEID";
    public static final String QUALIFIED_COLUMN_DFPAGENAME="PAGE.PAGENAME";
    public static final String COLUMN_DFPAGENAME="PAGENAME";
    public static final String QUALIFIED_COLUMN_DFPAGELABEL="PAGE.PAGELABEL";
    public static final String COLUMN_DFPAGELABEL="PAGELABEL";
      public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
          "PAGE.INSTITUTIONPROFILEID";
    public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";

    ////////////////////////////////////////////////////////////////////////////
    // Instance variables
    ////////////////////////////////////////////////////////////////////////////





    ////////////////////////////////////////////////////////////////////////////
    // Custom Members - Require Manual Migration
    ////////////////////////////////////////////////////////////////////////////

    static
    {

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFPAGEID,
                COLUMN_DFPAGEID,
                QUALIFIED_COLUMN_DFPAGEID,
                java.math.BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFPAGENAME,
                COLUMN_DFPAGENAME,
                QUALIFIED_COLUMN_DFPAGENAME,
                String.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFPAGELABEL,
                COLUMN_DFPAGELABEL,
                QUALIFIED_COLUMN_DFPAGELABEL,
                String.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));
        
        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                  FIELD_DFINSTITUTIONID,
                  COLUMN_DFINSTITUTIONID,
                  QUALIFIED_COLUMN_DFINSTITUTIONID,
                  java.math.BigDecimal.class,
                  false,
                  false,
                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                  "",
                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                  ""));
    }

}

