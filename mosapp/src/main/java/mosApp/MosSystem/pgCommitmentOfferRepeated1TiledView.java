package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;

/**
 *
 *
 *
 */
public class pgCommitmentOfferRepeated1TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgCommitmentOfferRepeated1TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(200);
		setPrimaryModelClass( doDefConditionModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getCbDefAction().setValue(CHILD_CBDEFACTION_RESET_VALUE);
		getStDefConditionLabel().setValue(CHILD_STDEFCONDITIONLABEL_RESET_VALUE);
		getHdDefDocTrackId().setValue(CHILD_HDDEFDOCTRACKID_RESET_VALUE);
		getBtDefDetail().setValue(CHILD_BTDEFDETAIL_RESET_VALUE);
    //--TD_CONDR_CR--start//
		getHdDefDocTrackId().setValue(CHILD_HDDEFDOCTRACKID_RESET_VALUE);
		getHdDefCopyId().setValue(CHILD_HDDEFCOPYID_RESET_VALUE);
		getHdDefRoleId().setValue(CHILD_HDDEFROLEID_RESET_VALUE);
		getHdDefRequestDate().setValue(CHILD_HDDEFREQUESTDATE_RESET_VALUE);
		getHdDefStatusDate().setValue(CHILD_HDDEFSTATUSDATE_RESET_VALUE);
		getHdDefDocStatus().setValue(CHILD_HDDEFDOCSTATUS_RESET_VALUE);
		getHdDefDocText().setValue(CHILD_HDDEFDOCTEXT_RESET_VALUE);
		getHdDefResponsibility().setValue(CHILD_HDDEFRESPONSIBILITY_RESET_VALUE);
		getHdDefDocumentLabel().setValue(CHILD_HDDEFDOCUMENTLABEL_RESET_VALUE);
    //--TD_CONDR_CR--end//
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_CBDEFACTION,ComboBox.class);
		registerChild(CHILD_STDEFCONDITIONLABEL,StaticTextField.class);
		registerChild(CHILD_HDDEFDOCTRACKID,HiddenField.class);
		registerChild(CHILD_BTDEFDETAIL,Button.class);
    //--TD_CONDR_CR--start//
		registerChild(CHILD_HDDEFDOCTRACKID,HiddenField.class);
		registerChild(CHILD_HDDEFCOPYID,HiddenField.class);
		registerChild(CHILD_HDDEFROLEID,HiddenField.class);
		registerChild(CHILD_HDDEFREQUESTDATE,HiddenField.class);
		registerChild(CHILD_HDDEFSTATUSDATE,HiddenField.class);
		registerChild(CHILD_HDDEFDOCSTATUS,HiddenField.class);
		registerChild(CHILD_HDDEFDOCTEXT,HiddenField.class);
		registerChild(CHILD_HDDEFRESPONSIBILITY,HiddenField.class);
		registerChild(CHILD_HDDEFDOCUMENTLABEL,HiddenField.class);
    //--TD_CONDR_CR--end//
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
//				modelList.add(getdoDefConditionModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By Billy 26Nov2002
    cbDefActionOptions.populate(getRequestContext());

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// The following code block was migrated from the Repeated1_onBeforeRowDisplayEvent method
      ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
      handler.pageGetState(this.getParentViewBean());
      boolean retval = handler.checkDisplayThisRow("Repeated1");
      handler.pageSaveState();
      return retval;
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_CBDEFACTION))
		{
			ComboBox child = new ComboBox( this,
				getdoDefConditionModel(),
				CHILD_CBDEFACTION,
				doDefConditionModel.FIELD_DFACTION,
				CHILD_CBDEFACTION_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbDefActionOptions);
			return child;
		}
		else
		if (name.equals(CHILD_STDEFCONDITIONLABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDefConditionModel(),
				CHILD_STDEFCONDITIONLABEL,
				doDefConditionModel.FIELD_DFDOCUMENTLABEL,
				CHILD_STDEFCONDITIONLABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDDEFDOCTRACKID))
		{
			HiddenField child = new HiddenField(this,
				getdoDefConditionModel(),
				CHILD_HDDEFDOCTRACKID,
				doDefConditionModel.FIELD_DFDEFDOCTRACKID,
				CHILD_HDDEFDOCTRACKID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDEFDETAIL))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDEFDETAIL,
				CHILD_BTDEFDETAIL,
				CHILD_BTDEFDETAIL_RESET_VALUE,
				null);
				return child;

		}
  //--TD_CONDR_CR--start//
		else
		if (name.equals(CHILD_HDDEFDOCTRACKID))
		{
			HiddenField child = new HiddenField(this,
				getdoDefConditionModel(),
				CHILD_HDDEFDOCTRACKID,
				doDefConditionModel.FIELD_DFDOCTRACKID,
				CHILD_HDDEFDOCTRACKID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDDEFCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoDefConditionModel(),
				CHILD_HDDEFCOPYID,
				doDefConditionModel.FIELD_DFCOPYID,
				CHILD_HDDEFCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDDEFROLEID))
		{
			HiddenField child = new HiddenField(this,
				getdoDefConditionModel(),
				CHILD_HDDEFROLEID,
				doDefConditionModel.FIELD_DFROLEID,
				CHILD_HDDEFROLEID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDDEFREQUESTDATE))
		{
			HiddenField child = new HiddenField(this,
				getdoDefConditionModel(),
				CHILD_HDDEFREQUESTDATE,
				doDefConditionModel.FIELD_DFREQUESTDATE,
				CHILD_HDDEFREQUESTDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDDEFSTATUSDATE))
		{
			HiddenField child = new HiddenField(this,
				getdoDefConditionModel(),
				CHILD_HDDEFSTATUSDATE,
				doDefConditionModel.FIELD_DFSTATUSCHANGEDATE,
				CHILD_HDDEFSTATUSDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDDEFDOCSTATUS))
		{
			HiddenField child = new HiddenField(this,
				getdoDefConditionModel(),
				CHILD_HDDEFDOCSTATUS,
				doDefConditionModel.FIELD_DFSTATUS,
				CHILD_HDDEFDOCSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDDEFDOCTEXT))
		{
			HiddenField child = new HiddenField(this,
				getdoDefConditionModel(),
				CHILD_HDDEFDOCTEXT,
				doDefConditionModel.FIELD_DFDOCUMENTTEXT,
				CHILD_HDDEFDOCTEXT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDDEFRESPONSIBILITY))
		{
			HiddenField child = new HiddenField(this,
				getdoDefConditionModel(),
				CHILD_HDDEFRESPONSIBILITY,
				doDefConditionModel.FIELD_DFRESPONSIBILITY,
				CHILD_HDDEFRESPONSIBILITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDDEFDOCUMENTLABEL))
		{
			HiddenField child = new HiddenField(this,
				getdoDefConditionModel(),
				CHILD_HDDEFDOCUMENTLABEL,
				doDefConditionModel.FIELD_DFDOCUMENTLABEL,
				CHILD_HDDEFDOCUMENTLABEL_RESET_VALUE,
				null);
			return child;
		}
  //--TD_CONDR_CR--end//
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbDefAction()
	{
		return (ComboBox)getChild(CHILD_CBDEFACTION);
	}

	/**
	 *
	 *
	 */
	static class CbDefActionOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbDefActionOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 26Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection<String[]> c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(), 
                                                            "DOCUMENTSTATUS", languageId);
        Iterator<String[]> l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public StaticTextField getStDefConditionLabel()
	{
		return (StaticTextField)getChild(CHILD_STDEFCONDITIONLABEL);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdDefDocTrackId()
	{
		return (HiddenField)getChild(CHILD_HDDEFDOCTRACKID);
	}


	/**
	 *
	 *
	 */
	public void handleBtDefDetailRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btDefDetail_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();

		//duplicate conditions
		handler.setDDCUpdate(false);
		
		handler.preHandlerProtocol(this.getParentViewBean());

    //--TD_CONDR_CR--start--//
    //// PageCondition6(true) forces to go to the DocTracking Detail Screen style
    //// with a possibility to edit both Default and Standard conditions.
    //// Default is "N": original Condition Details style (read only).
    if (handler.getTheSessionState().getCurrentPage().getPageCondition6() == false) {
		  handler.navigateToDetail(1, handler.getRowNdxFromWebEventMethod(event));
    } else if (handler.getTheSessionState().getCurrentPage().getPageCondition6() == true) {
		    handler.navigateToDetailDocTrackStyle(1, handler.getRowNdxFromWebEventMethod(event));
    }
    //--TD_CONDR_CR--end--//

		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtDefDetail()
	{
		return (Button)getChild(CHILD_BTDEFDETAIL);
	}

  //--TD_CONDR_CR--start//
	/**
	 *
	 *
	 */
	public HiddenField getHdDefCopyId()
	{
		return (HiddenField)getChild(CHILD_HDDEFCOPYID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdDefRoleId()
	{
		return (HiddenField)getChild(CHILD_HDDEFROLEID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdDefRequestDate()
	{
		return (HiddenField)getChild(CHILD_HDDEFREQUESTDATE);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdDefStatusDate()
	{
		return (HiddenField)getChild(CHILD_HDDEFSTATUSDATE);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdDefDocStatus()
	{
		return (HiddenField)getChild(CHILD_HDDEFDOCSTATUS);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdDefDocText()
	{
		return (HiddenField)getChild(CHILD_HDDEFDOCTEXT);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdDefResponsibility()
	{
		return (HiddenField)getChild(CHILD_HDDEFRESPONSIBILITY);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdDefDocumentLabel()
	{
		return (HiddenField)getChild(CHILD_HDDEFDOCUMENTLABEL);
	}

  //--TD_CONDR_CR--end//
	/**
	 *
	 *
	 */
	public doDefConditionModel getdoDefConditionModel()
	{
		if (doDefCondition == null)
			doDefCondition = (doDefConditionModel) getModel(doDefConditionModel.class);
		return doDefCondition;
	}


	/**
	 *
	 *
	 */
	public void setdoDefConditionModel(doDefConditionModel model)
	{
			doDefCondition = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_CBDEFACTION="cbDefAction";
	public static final String CHILD_CBDEFACTION_RESET_VALUE="";
  //--Release2.1--//
	//private static CbDefActionOptionList cbDefActionOptions=new CbDefActionOptionList();
  private CbDefActionOptionList cbDefActionOptions=new CbDefActionOptionList();
	public static final String CHILD_STDEFCONDITIONLABEL="stDefConditionLabel";
	public static final String CHILD_STDEFCONDITIONLABEL_RESET_VALUE="";
	public static final String CHILD_HDDEFDOCTRACKID="hdDefDocTrackId";
	public static final String CHILD_HDDEFDOCTRACKID_RESET_VALUE="";
	public static final String CHILD_BTDEFDETAIL="btDefDetail";
	public static final String CHILD_BTDEFDETAIL_RESET_VALUE=" ";
  //--TD_CONDR_CR--start--//
	public static final String CHILD_HDDEFCOPYID="hdDefCopyId";
	public static final String CHILD_HDDEFCOPYID_RESET_VALUE="";
	public static final String CHILD_HDDEFROLEID="hdDefRoleId";
	public static final String CHILD_HDDEFROLEID_RESET_VALUE="";
	public static final String CHILD_HDDEFREQUESTDATE="hdDefRequestDate";
	public static final String CHILD_HDDEFREQUESTDATE_RESET_VALUE="";
	public static final String CHILD_HDDEFSTATUSDATE="hdDefStatusDate";
	public static final String CHILD_HDDEFSTATUSDATE_RESET_VALUE="";
	public static final String CHILD_HDDEFDOCSTATUS="hdDefDocStatus";
	public static final String CHILD_HDDEFDOCSTATUS_RESET_VALUE="";
	public static final String CHILD_HDDEFDOCTEXT="hdDefDocText";
	public static final String CHILD_HDDEFDOCTEXT_RESET_VALUE="";
	public static final String CHILD_HDDEFRESPONSIBILITY="hdDefResponsibility";
	public static final String CHILD_HDDEFRESPONSIBILITY_RESET_VALUE="";
	public static final String CHILD_HDDEFDOCUMENTLABEL="hdDefDocumentLabel";
	public static final String CHILD_HDDEFDOCUMENTLABEL_RESET_VALUE="";
  //--TD_CONDR_CR--end--//


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDefConditionModel doDefCondition=null;
  private ConditionsReviewHandler handler=new ConditionsReviewHandler();
}

