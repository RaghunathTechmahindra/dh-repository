package mosApp.MosSystem;

import java.sql.SQLException;

import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doDealInformationModelImpl extends QueryModelBase
	implements doDealInformationModel
{
	/**
	 *
	 *
	 */
	public doDealInformationModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEffectiveAmortization()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFEFFECTIVEAMORTIZATION);
	}


	/**
	 *
	 *
	 */
	public void setDfEffectiveAmortization(java.math.BigDecimal value)
	{
		setValue(FIELD_DFEFFECTIVEAMORTIZATION,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfRateDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFRATEDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfRateDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFRATEDATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPostedInterestRate()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPOSTEDINTERESTRATE);
	}


	/**
	 *
	 *
	 */
	public void setDfPostedInterestRate(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPOSTEDINTERESTRATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNetRate()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFNETRATE);
	}


	/**
	 *
	 *
	 */
	public void setDfNetRate(java.math.BigDecimal value)
	{
		setValue(FIELD_DFNETRATE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfRateLock()
	{
		return (String)getValue(FIELD_DFRATELOCK);
	}


	/**
	 *
	 *
	 */
	public void setDfRateLock(String value)
	{
		setValue(FIELD_DFRATELOCK,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDiscount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDISCOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfDiscount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDISCOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPremium()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPREMIUM);
	}


	/**
	 *
	 *
	 */
	public void setDfPremium(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPREMIUM,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBuydownRate()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBUYDOWNRATE);
	}


	/**
	 *
	 *
	 */
	public void setDfBuydownRate(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBUYDOWNRATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPIPayment()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPIPAYMENT);
	}


	/**
	 *
	 *
	 */
	public void setDfPIPayment(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPIPAYMENT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalPayment()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALPAYMENT);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalPayment(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALPAYMENT,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfFirstPaymentDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFFIRSTPAYMENTDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfFirstPaymentDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFFIRSTPAYMENTDATE,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfMaturityDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFMATURITYDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfMaturityDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFMATURITYDATE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfRefSourceAppNo()
	{
		return (String)getValue(FIELD_DFREFSOURCEAPPNO);
	}


	/**
	 *
	 *
	 */
	public void setDfRefSourceAppNo(String value)
	{
		setValue(FIELD_DFREFSOURCEAPPNO,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCrossSell()
	{
		return (String)getValue(FIELD_DFCROSSSELL);
	}


	/**
	 *
	 *
	 */
	public void setDfCrossSell(String value)
	{
		setValue(FIELD_DFCROSSSELL,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalPurchasePrice()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalPurchasePrice(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALPURCHASEPRICE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalEstimatedValue()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALESTIMATEDVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalEstimatedValue(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALESTIMATEDVALUE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfActualAppraisedValue()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFACTUALAPPRAISEDVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfActualAppraisedValue(java.math.BigDecimal value)
	{
		setValue(FIELD_DFACTUALAPPRAISEDVALUE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLender()
	{
		return (String)getValue(FIELD_DFLENDER);
	}


	/**
	 *
	 *
	 */
	public void setDfLender(String value)
	{
		setValue(FIELD_DFLENDER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfServicingMortgageNumber()
	{
		return (String)getValue(FIELD_DFSERVICINGMORTGAGENUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfServicingMortgageNumber(String value)
	{
		setValue(FIELD_DFSERVICINGMORTGAGENUMBER,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPreAppEstPurchasePrice()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPREAPPESTPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public void setDfPreAppEstPurchasePrice(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPREAPPESTPURCHASEPRICE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfRateCode()
	{
		return (String)getValue(FIELD_DFRATECODE);
	}


	/**
	 *
	 *
	 */
	public void setDfRateCode(String value)
	{
		setValue(FIELD_DFRATECODE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfProductName()
	{
		return (String)getValue(FIELD_DFPRODUCTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfProductName(String value)
	{
		setValue(FIELD_DFPRODUCTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfInvestor()
	{
		return (String)getValue(FIELD_DFINVESTOR);
	}


	/**
	 *
	 *
	 */
	public void setDfInvestor(String value)
	{
		setValue(FIELD_DFINVESTOR,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE
	    ="SELECT DISTINCT DEAL.DEALID, DEAL.EFFECTIVEAMORTIZATIONMONTHS, DEAL.RATEDATE, " +
	    "DEAL.POSTEDINTERESTRATE, DEAL.NETINTERESTRATE, DEAL.RATELOCK, DEAL.DISCOUNT, " +
	    "DEAL.PREMIUM, DEAL.BUYDOWNRATE, DEAL.PANDIPAYMENTAMOUNT, DEAL.TOTALPAYMENTAMOUNT, " +
	    "DEAL.FIRSTPAYMENTDATE, DEAL.MATURITYDATE, DEAL.SOURCEAPPLICATIONID, " +
	    "CROSSSELLPROFILE.CSDESCRIPTION, DEAL.TOTALPURCHASEPRICE, DEAL.TOTALESTAPPRAISEDVALUE, " +
	    "DEAL.TOTALACTAPPRAISEDVALUE, LENDERPROFILE.LPSHORTNAME, DEAL.SERVICINGMORTGAGENUMBER, " +
	    "DEAL.PAPURCHASEPRICE, DEAL.COPYID, PRICINGPROFILE.RATECODE, MTGPROD.MTGPRODNAME, " +
	    "INVESTORPROFILE.INVESTORPROFILENAME " +
	    "FROM DEAL, PAYMENTTERM, CROSSSELLPROFILE, LENDERPROFILE, PRICINGPROFILE, MTGPROD, " +
	    "INVESTORPROFILE, PRICINGRATEINVENTORY  " +
	    "__WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="DEAL, PAYMENTTERM, CROSSSELLPROFILE, " +
			"LENDERPROFILE, PRICINGPROFILE, MTGPROD, INVESTORPROFILE, PRICINGRATEINVENTORY";
	public static final String STATIC_WHERE_CRITERIA
        =" INVESTORPROFILE.INVESTORPROFILEID (+)  =  DEAL.INVESTORPROFILEID " +
        "AND DEAL.PAYMENTTERMID = PAYMENTTERM.PAYMENTTERMID (+) " +
        "AND DEAL.CROSSSELLPROFILEID  =  CROSSSELLPROFILE.CROSSSELLPROFILEID (+)  " +
        "AND DEAL.LENDERPROFILEID  =  LENDERPROFILE.LENDERPROFILEID (+)  " +
        "AND DEAL.MTGPRODID  =  MTGPROD.MTGPRODID (+) " +
        "AND DEAL.PRICINGPROFILEID  =  PRICINGRATEINVENTORY.PRICINGRATEINVENTORYID (+)  " +
        "AND PRICINGRATEINVENTORY.PRICINGPROFILEID  =  PRICINGPROFILE.PRICINGPROFILEID (+)  " +
        "AND DEAL.INSTITUTIONPROFILEID = INVESTORPROFILE.INSTITUTIONPROFILEID (+)" +
        "AND DEAL.INSTITUTIONPROFILEID = PAYMENTTERM.INSTITUTIONPROFILEID (+)" +
        "AND DEAL.INSTITUTIONPROFILEID = CROSSSELLPROFILE.INSTITUTIONPROFILEID (+) " +
        "AND DEAL.INSTITUTIONPROFILEID = LENDERPROFILE.INSTITUTIONPROFILEID (+) " +
        "AND DEAL.INSTITUTIONPROFILEID = MTGPROD.INSTITUTIONPROFILEID (+) " +
        "AND DEAL.INSTITUTIONPROFILEID = PRICINGRATEINVENTORY.INSTITUTIONPROFILEID (+) " +
        "AND PRICINGRATEINVENTORY.INSTITUTIONPROFILEID = PRICINGPROFILE.INSTITUTIONPROFILEID (+) ";
	
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFEFFECTIVEAMORTIZATION="DEAL.EFFECTIVEAMORTIZATIONMONTHS";
	public static final String COLUMN_DFEFFECTIVEAMORTIZATION="EFFECTIVEAMORTIZATIONMONTHS";
	public static final String QUALIFIED_COLUMN_DFRATEDATE="DEAL.RATEDATE";
	public static final String COLUMN_DFRATEDATE="RATEDATE";
	public static final String QUALIFIED_COLUMN_DFPOSTEDINTERESTRATE="DEAL.POSTEDINTERESTRATE";
	public static final String COLUMN_DFPOSTEDINTERESTRATE="POSTEDINTERESTRATE";
	public static final String QUALIFIED_COLUMN_DFNETRATE="DEAL.NETINTERESTRATE";
	public static final String COLUMN_DFNETRATE="NETINTERESTRATE";
	public static final String QUALIFIED_COLUMN_DFRATELOCK="DEAL.RATELOCK";
	public static final String COLUMN_DFRATELOCK="RATELOCK";
	public static final String QUALIFIED_COLUMN_DFDISCOUNT="DEAL.DISCOUNT";
	public static final String COLUMN_DFDISCOUNT="DISCOUNT";
	public static final String QUALIFIED_COLUMN_DFPREMIUM="DEAL.PREMIUM";
	public static final String COLUMN_DFPREMIUM="PREMIUM";
	public static final String QUALIFIED_COLUMN_DFBUYDOWNRATE="DEAL.BUYDOWNRATE";
	public static final String COLUMN_DFBUYDOWNRATE="BUYDOWNRATE";
	public static final String QUALIFIED_COLUMN_DFPIPAYMENT="DEAL.PANDIPAYMENTAMOUNT";
	public static final String COLUMN_DFPIPAYMENT="PANDIPAYMENTAMOUNT";
	public static final String QUALIFIED_COLUMN_DFTOTALPAYMENT="DEAL.TOTALPAYMENTAMOUNT";
	public static final String COLUMN_DFTOTALPAYMENT="TOTALPAYMENTAMOUNT";
	public static final String QUALIFIED_COLUMN_DFFIRSTPAYMENTDATE="DEAL.FIRSTPAYMENTDATE";
	public static final String COLUMN_DFFIRSTPAYMENTDATE="FIRSTPAYMENTDATE";
	public static final String QUALIFIED_COLUMN_DFMATURITYDATE="DEAL.MATURITYDATE";
	public static final String COLUMN_DFMATURITYDATE="MATURITYDATE";
	public static final String QUALIFIED_COLUMN_DFREFSOURCEAPPNO="DEAL.SOURCEAPPLICATIONID";
	public static final String COLUMN_DFREFSOURCEAPPNO="SOURCEAPPLICATIONID";
	public static final String QUALIFIED_COLUMN_DFCROSSSELL="CROSSSELLPROFILE.CSDESCRIPTION";
	public static final String COLUMN_DFCROSSSELL="CSDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFTOTALPURCHASEPRICE="DEAL.TOTALPURCHASEPRICE";
	public static final String COLUMN_DFTOTALPURCHASEPRICE="TOTALPURCHASEPRICE";
	public static final String QUALIFIED_COLUMN_DFTOTALESTIMATEDVALUE="DEAL.TOTALESTAPPRAISEDVALUE";
	public static final String COLUMN_DFTOTALESTIMATEDVALUE="TOTALESTAPPRAISEDVALUE";
	public static final String QUALIFIED_COLUMN_DFACTUALAPPRAISEDVALUE="DEAL.TOTALACTAPPRAISEDVALUE";
	public static final String COLUMN_DFACTUALAPPRAISEDVALUE="TOTALACTAPPRAISEDVALUE";
	public static final String QUALIFIED_COLUMN_DFLENDER="LENDERPROFILE.LPSHORTNAME";
	public static final String COLUMN_DFLENDER="LPSHORTNAME";
	public static final String QUALIFIED_COLUMN_DFSERVICINGMORTGAGENUMBER="DEAL.SERVICINGMORTGAGENUMBER";
	public static final String COLUMN_DFSERVICINGMORTGAGENUMBER="SERVICINGMORTGAGENUMBER";
	public static final String QUALIFIED_COLUMN_DFPREAPPESTPURCHASEPRICE="DEAL.PAPURCHASEPRICE";
	public static final String COLUMN_DFPREAPPESTPURCHASEPRICE="PAPURCHASEPRICE";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFRATECODE="PRICINGPROFILE.RATECODE";
	public static final String COLUMN_DFRATECODE="RATECODE";
	public static final String QUALIFIED_COLUMN_DFPRODUCTNAME="MTGPROD.MTGPRODNAME";
	public static final String COLUMN_DFPRODUCTNAME="MTGPRODNAME";
	public static final String QUALIFIED_COLUMN_DFINVESTOR="INVESTORPROFILE.INVESTORPROFILENAME";
	public static final String COLUMN_DFINVESTOR="INVESTORPROFILENAME";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEFFECTIVEAMORTIZATION,
				COLUMN_DFEFFECTIVEAMORTIZATION,
				QUALIFIED_COLUMN_DFEFFECTIVEAMORTIZATION,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFRATEDATE,
				COLUMN_DFRATEDATE,
				QUALIFIED_COLUMN_DFRATEDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPOSTEDINTERESTRATE,
				COLUMN_DFPOSTEDINTERESTRATE,
				QUALIFIED_COLUMN_DFPOSTEDINTERESTRATE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNETRATE,
				COLUMN_DFNETRATE,
				QUALIFIED_COLUMN_DFNETRATE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFRATELOCK,
				COLUMN_DFRATELOCK,
				QUALIFIED_COLUMN_DFRATELOCK,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDISCOUNT,
				COLUMN_DFDISCOUNT,
				QUALIFIED_COLUMN_DFDISCOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPREMIUM,
				COLUMN_DFPREMIUM,
				QUALIFIED_COLUMN_DFPREMIUM,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBUYDOWNRATE,
				COLUMN_DFBUYDOWNRATE,
				QUALIFIED_COLUMN_DFBUYDOWNRATE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPIPAYMENT,
				COLUMN_DFPIPAYMENT,
				QUALIFIED_COLUMN_DFPIPAYMENT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALPAYMENT,
				COLUMN_DFTOTALPAYMENT,
				QUALIFIED_COLUMN_DFTOTALPAYMENT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFIRSTPAYMENTDATE,
				COLUMN_DFFIRSTPAYMENTDATE,
				QUALIFIED_COLUMN_DFFIRSTPAYMENTDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMATURITYDATE,
				COLUMN_DFMATURITYDATE,
				QUALIFIED_COLUMN_DFMATURITYDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFSOURCEAPPNO,
				COLUMN_DFREFSOURCEAPPNO,
				QUALIFIED_COLUMN_DFREFSOURCEAPPNO,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCROSSSELL,
				COLUMN_DFCROSSSELL,
				QUALIFIED_COLUMN_DFCROSSSELL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALPURCHASEPRICE,
				COLUMN_DFTOTALPURCHASEPRICE,
				QUALIFIED_COLUMN_DFTOTALPURCHASEPRICE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALESTIMATEDVALUE,
				COLUMN_DFTOTALESTIMATEDVALUE,
				QUALIFIED_COLUMN_DFTOTALESTIMATEDVALUE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFACTUALAPPRAISEDVALUE,
				COLUMN_DFACTUALAPPRAISEDVALUE,
				QUALIFIED_COLUMN_DFACTUALAPPRAISEDVALUE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLENDER,
				COLUMN_DFLENDER,
				QUALIFIED_COLUMN_DFLENDER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSERVICINGMORTGAGENUMBER,
				COLUMN_DFSERVICINGMORTGAGENUMBER,
				QUALIFIED_COLUMN_DFSERVICINGMORTGAGENUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPREAPPESTPURCHASEPRICE,
				COLUMN_DFPREAPPESTPURCHASEPRICE,
				QUALIFIED_COLUMN_DFPREAPPESTPURCHASEPRICE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFRATECODE,
				COLUMN_DFRATECODE,
				QUALIFIED_COLUMN_DFRATECODE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRODUCTNAME,
				COLUMN_DFPRODUCTNAME,
				QUALIFIED_COLUMN_DFPRODUCTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINVESTOR,
				COLUMN_DFINVESTOR,
				QUALIFIED_COLUMN_DFINVESTOR,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

