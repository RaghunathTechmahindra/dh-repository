package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

import com.basis100.log.*;

/**
 *
 *
 *
 */
public class pgApplicantEntryrptCreditBureauReportsTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgApplicantEntryrptCreditBureauReportsTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(100);
		setPrimaryModelClass( doRowGeneratorModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getHdRowNdx().setValue(CHILD_HDROWNDX_RESET_VALUE);
		getStCreditBureauReport().setValue(CHILD_STCREDITBUREAUREPORT_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_HDROWNDX,StaticTextField.class);
		registerChild(CHILD_STCREDITBUREAUREPORT,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoRowGeneratorModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated rptCreditBureauReports_onBeforeRowDisplayEvent code here
      ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
		  handler.pageGetState(this.getParentViewBean());
      boolean res = true;
      res = handler.populateBureauReport(this.getTileIndex());
      handler.pageSaveState();

      return res;
		}

		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_HDROWNDX))
		{
			StaticTextField child = new StaticTextField(this,
				getdoRowGeneratorModel(),
				CHILD_HDROWNDX,
				doRowGeneratorModel.FIELD_DFROWNDX,
				CHILD_HDROWNDX_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCREDITBUREAUREPORT))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCREDITBUREAUREPORT,
				CHILD_STCREDITBUREAUREPORT,
				CHILD_STCREDITBUREAUREPORT_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getHdRowNdx()
	{
		return (StaticTextField)getChild(CHILD_HDROWNDX);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCreditBureauReport()
	{
		return (StaticTextField)getChild(CHILD_STCREDITBUREAUREPORT);
	}


	/**
	 *
	 *
	 */
	public doRowGeneratorModel getdoRowGeneratorModel()
	{
		if (doRowGenerator == null)
			doRowGenerator = (doRowGeneratorModel) getModel(doRowGeneratorModel.class);
		return doRowGenerator;
	}


	/**
	 *
	 *
	 */
	public void setdoRowGeneratorModel(doRowGeneratorModel model)
	{
			doRowGenerator = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_HDROWNDX="hdRowNdx";
	public static final String CHILD_HDROWNDX_RESET_VALUE="";
	public static final String CHILD_STCREDITBUREAUREPORT="stCreditBureauReport";
	public static final String CHILD_STCREDITBUREAUREPORT_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doRowGeneratorModel doRowGenerator=null;

  private ApplicantEntryHandler handler=new ApplicantEntryHandler();

  public SysLogger logger;

}

