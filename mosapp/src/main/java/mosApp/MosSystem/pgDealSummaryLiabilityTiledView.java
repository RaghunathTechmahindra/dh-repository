package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgDealSummaryLiabilityTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealSummaryLiabilityTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(2);
		setPrimaryModelClass( doDetailViewLiabilitiesModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStLDType().setValue(CHILD_STLDTYPE_RESET_VALUE);
		getStLDLiabilityDesc().setValue(CHILD_STLDLIABILITYDESC_RESET_VALUE);
		getStLDTotalAmount().setValue(CHILD_STLDTOTALAMOUNT_RESET_VALUE);
		getStLDMonthlyPayment().setValue(CHILD_STLDMONTHLYPAYMENT_RESET_VALUE);
		getStLDLiabilityGDS().setValue(CHILD_STLDLIABILITYGDS_RESET_VALUE);
		getStLDLiabilityTDS().setValue(CHILD_STLDLIABILITYTDS_RESET_VALUE);
		getStLDLiabilityPayOffDesc().setValue(CHILD_STLDLIABILITYPAYOFFDESC_RESET_VALUE);
		getStCreditBureauIndicator().setValue(CHILD_STCREDITBUREAUINDICATOR_RESET_VALUE);
	    getStCreditBureauLiabilityLimit().setValue(CHILD_STCREDITBUREAULIABILITYLIMIT_RESET_VALUE);
        getStMaturityDate().setValue(CHILD_STMATURITYDATE_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STLDTYPE,StaticTextField.class);
		registerChild(CHILD_STLDLIABILITYDESC,StaticTextField.class);
		registerChild(CHILD_STLDTOTALAMOUNT,StaticTextField.class);
		registerChild(CHILD_STLDMONTHLYPAYMENT,StaticTextField.class);
		registerChild(CHILD_STLDLIABILITYGDS,StaticTextField.class);
		registerChild(CHILD_STLDLIABILITYTDS,StaticTextField.class);
		registerChild(CHILD_STLDLIABILITYPAYOFFDESC,StaticTextField.class);
        registerChild(CHILD_STCREDITBUREAUINDICATOR,StaticTextField.class);
        registerChild(CHILD_STCREDITBUREAULIABILITYLIMIT,StaticTextField.class);
        registerChild(CHILD_STMATURITYDATE, StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDetailViewLiabilitiesModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		// The following code block was migrated from the Liability_onBeforeDisplayEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.setupNumOfRepeatedLiability(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}

		return movedToRow;

	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STLDTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewLiabilitiesModel(),
				CHILD_STLDTYPE,
				doDetailViewLiabilitiesModel.FIELD_DFLIABILITYTYPE,
				CHILD_STLDTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLDLIABILITYDESC))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewLiabilitiesModel(),
				CHILD_STLDLIABILITYDESC,
				doDetailViewLiabilitiesModel.FIELD_DFLIABILITYDESC,
				CHILD_STLDLIABILITYDESC_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLDTOTALAMOUNT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewLiabilitiesModel(),
				CHILD_STLDTOTALAMOUNT,
				doDetailViewLiabilitiesModel.FIELD_DFLIABILITYAMOUNT,
				CHILD_STLDTOTALAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLDMONTHLYPAYMENT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewLiabilitiesModel(),
				CHILD_STLDMONTHLYPAYMENT,
				doDetailViewLiabilitiesModel.FIELD_DFLIABILITYMONTHLYPAYMENT,
				CHILD_STLDMONTHLYPAYMENT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLDLIABILITYGDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewLiabilitiesModel(),
				CHILD_STLDLIABILITYGDS,
				doDetailViewLiabilitiesModel.FIELD_DFPERCENTINCLUDEDINGDS,
				CHILD_STLDLIABILITYGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLDLIABILITYTDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewLiabilitiesModel(),
				CHILD_STLDLIABILITYTDS,
				doDetailViewLiabilitiesModel.FIELD_DFPERCENTINCLUDEDINTDS,
				CHILD_STLDLIABILITYTDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLDLIABILITYPAYOFFDESC))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewLiabilitiesModel(),
				CHILD_STLDLIABILITYPAYOFFDESC,
				doDetailViewLiabilitiesModel.FIELD_DFLIABILITYPAYOFFDESC,
				CHILD_STLDLIABILITYPAYOFFDESC_RESET_VALUE,
				null);
			return child;
		}
		if (name.equals(CHILD_STCREDITBUREAUINDICATOR))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewLiabilitiesModel(),
				CHILD_STCREDITBUREAUINDICATOR,
				doDetailViewLiabilitiesModel.FIELD_DFCREDITBUREAUINDICATOR,
				CHILD_STCREDITBUREAUINDICATOR_RESET_VALUE,
				null);
			return child;
		}
        else if (name.equals(CHILD_STCREDITBUREAULIABILITYLIMIT)) {
            TextField child = new TextField(this,
            		getdoDetailViewLiabilitiesModel(),
                CHILD_STCREDITBUREAULIABILITYLIMIT,
                doDetailViewLiabilitiesModel.FIELD_DFCREDITBUREAULIABILITYLIMIT,
                CHILD_STCREDITBUREAULIABILITYLIMIT_RESET_VALUE,
                null);
            return child;
        }
        else if (name.equals(CHILD_STMATURITYDATE)) {
			StaticTextField child = new StaticTextField(this,
					getdoDetailViewLiabilitiesModel(), CHILD_STMATURITYDATE,
					doDetailViewLiabilitiesModel.FIELD_DFMATURITYDATE,
					CHILD_STMATURITYDATE_RESET_VALUE, null);

			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLDType()
	{
		return (StaticTextField)getChild(CHILD_STLDTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLDLiabilityDesc()
	{
		return (StaticTextField)getChild(CHILD_STLDLIABILITYDESC);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLDTotalAmount()
	{
		return (StaticTextField)getChild(CHILD_STLDTOTALAMOUNT);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLDMonthlyPayment()
	{
		return (StaticTextField)getChild(CHILD_STLDMONTHLYPAYMENT);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLDLiabilityGDS()
	{
		return (StaticTextField)getChild(CHILD_STLDLIABILITYGDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLDLiabilityTDS()
	{
		return (StaticTextField)getChild(CHILD_STLDLIABILITYTDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLDLiabilityPayOffDesc()
	{
		return (StaticTextField)getChild(CHILD_STLDLIABILITYPAYOFFDESC);
	}

    /**
     * 
     * @return
     */
    public StaticTextField getStCreditBureauIndicator() {

        return (StaticTextField) getChild(CHILD_STCREDITBUREAUINDICATOR);
    }
    
    /**
     * 
     * 
     */
    public TextField getStCreditBureauLiabilityLimit() {
        return (TextField) getChild(CHILD_STCREDITBUREAULIABILITYLIMIT);
    }
    
    /**
    *
    *
    */
    public StaticTextField getStMaturityDate()
    {
        return (StaticTextField) getChild(CHILD_STMATURITYDATE);
    }
    
    /**
    *
    *
    */
	public String endStCreditBureauIndicatorDisplay(ChildContentDisplayEvent event)
	{
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setYesOrNo(event.getContent());
		handler.pageSaveState();

		return rc;
	}
    
	/**
	 *
	 *
	 */
	public doDetailViewLiabilitiesModel getdoDetailViewLiabilitiesModel()
	{
		if (doDetailViewLiabilities == null)
			doDetailViewLiabilities = (doDetailViewLiabilitiesModel) getModel(doDetailViewLiabilitiesModel.class);
		return doDetailViewLiabilities;
	}


	/**
	 *
	 *
	 */
	public void setdoDetailViewLiabilitiesModel(doDetailViewLiabilitiesModel model)
	{
			doDetailViewLiabilities = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STLDTYPE="stLDType";
	public static final String CHILD_STLDTYPE_RESET_VALUE="";
	public static final String CHILD_STLDLIABILITYDESC="stLDLiabilityDesc";
	public static final String CHILD_STLDLIABILITYDESC_RESET_VALUE="";
	public static final String CHILD_STLDTOTALAMOUNT="stLDTotalAmount";
	public static final String CHILD_STLDTOTALAMOUNT_RESET_VALUE="";
	public static final String CHILD_STLDMONTHLYPAYMENT="stLDMonthlyPayment";
	public static final String CHILD_STLDMONTHLYPAYMENT_RESET_VALUE="";
	public static final String CHILD_STLDLIABILITYGDS="stLDLiabilityGDS";
	public static final String CHILD_STLDLIABILITYGDS_RESET_VALUE="";
	public static final String CHILD_STLDLIABILITYTDS="stLDLiabilityTDS";
	public static final String CHILD_STLDLIABILITYTDS_RESET_VALUE="";
	public static final String CHILD_STLDLIABILITYPAYOFFDESC="stLDLiabilityPayOffDesc";
	public static final String CHILD_STLDLIABILITYPAYOFFDESC_RESET_VALUE="";
    public static final String CHILD_STCREDITBUREAUINDICATOR="stCreditBureauIndicator";
    public static final String CHILD_STCREDITBUREAUINDICATOR_RESET_VALUE="";
    public static final String CHILD_STCREDITBUREAULIABILITYLIMIT="stCreditBureauLiabilityLimit";
    public static final String CHILD_STCREDITBUREAULIABILITYLIMIT_RESET_VALUE="";
    public static final String CHILD_STMATURITYDATE = "stMaturityDate";
    public static final String CHILD_STMATURITYDATE_RESET_VALUE = "";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDetailViewLiabilitiesModel doDetailViewLiabilities=null;
  private DealSummaryHandler handler=new DealSummaryHandler();
}

