package mosApp.MosSystem;
/**
 * 
 * @version 1.1 <br>
 * Date: 08/08/006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change: <br>
 *   Made changes to updateForFunding() method <br>
 *     	- Added logic check for the NBC specific lender and call DocumentRequest.NBCrequestSolicitorsPackage
 *     		to generate the NBCSolicitorpackage PDF Document <br>
 * 
 */
import java.text.*;
import java.util.*;


import com.basis100.deal.entity.*;
import com.basis100.resources.*;
import com.basis100.deal.history.*;
import com.basis100.deal.security.*;
import com.basis100.deal.util.DBA;
import com.basis100.deal.validation.*;
import com.basis100.deal.commitment.*;
import com.basis100.deal.calc.*;
import com.basis100.picklist.*;

import MosSystem.*;

import com.iplanet.jato.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.html.*;

//***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//
import com.basis100.deal.docprep.xsl.XSLMerger;
import com.basis100.deal.docrequest.DocumentRequest;
//***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//
/**
 *
 *
 */
public class FundingActivityHandler extends PageHandlerCommon
	implements Cloneable, Sc
{
	///////////////////////////////////////////////////////////////////

	public FundingActivityHandler cloneSS()
	{
		return(FundingActivityHandler) super.cloneSafeShallow();
	}


	///////////////////////////////////////////////////////////////////
	//This method is used to populate the fields in the header
	//with the appropriate information

	public void populatePageDisplayFields()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		populatePageShellDisplayFields();
		populateTaskNavigator(pg);
		populatePageDealSummarySnapShot();
		populatePreviousPagesLinks();

		this.fillupMonths("cbMonths");

		populateDisplayFieldValues(pg);
		setJSValidation();

    //--XC_BNAME_CR--start--//
        getCurrNDPage().setDisplayFieldValue("stVALSData", VALS_DATA);
    //--XC_BNAME_CR--end--//

	}


	/////////////////////////////////////////////////////////////////////

	public void setJSValidation()
	{
		// Validate # of Dependants
		HtmlDisplayFieldBase df = null;
		String theExtraHtml = null;
		ViewBean thePage = getCurrNDPage();

		// Set the compare date to today
		//  Validate Month
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("cbMonths");
		theExtraHtml = " onBlur=\"isFieldValidDate('tbYear', 'cbMonths', 'tbDay');\"";
		df.setExtraHtml(theExtraHtml);

		//  Validate Day
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbDay");
		theExtraHtml = " onBlur=\"if(isFieldInIntRange(1, 31)) " + "isFieldValidDate('tbYear', 'cbMonths', 'tbDay');\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Year
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbYear");
		theExtraHtml = " onBlur=\"if(isFieldInIntRange(1800, 2100)) " + "isFieldValidDate('tbYear', 'cbMonths', 'tbDay');\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Bank Number
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbBankABABankNum");

        // Catherine, GE to MCAP link --- begin -------------
        String onChange = " onChange=\"if(!setBooleanDefaults('tbBankABABankNum', ['hdBankName','tbBankName'] )) " +
        "setValueIfNotMatched(['tbBankName'], '0', ''); " +
        "populateValueTo(['hdBankABABankNum']);\"";
        
        String size = "";
        String onBlur = "";
        boolean isRestrictLength = PropertiesCache.getInstance().getProperty(this.theSessionState.getDealInstitutionId(),"com.basis100.screen.funding.bankaccount.restrict.length", "N").equals("Y"); 
        boolean isNumericOnly = PropertiesCache.getInstance().getProperty(this.theSessionState.getDealInstitutionId(),"com.basis100.screens.funding.bankaccount.numeric.only", "N").equals("Y"); 
        
        if (isNumericOnly) {
          onBlur = " onBlur=\"isFieldInteger(3);\"";
        }
        
        theExtraHtml = onChange + size + onBlur;
        // Catherine, GE to MCAP link --- end -------------
        
		df.setExtraHtml(theExtraHtml);
        
        // Catherine, GE to MCAP link --- begin -------------
        
        // Validate Bank ABA Transit Number 
        df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbBankABATransitNum");
        
        if (isRestrictLength) {
          size = "size=\"7\" maxLength=\"7\" ";
        }  else {
          size = " size=\"16\" maxLength=\"16\" ";
        }

        if (isNumericOnly) {
          onBlur = " onBlur=\"isFieldInteger(0);\""; // any length
        } else {
          onBlur = "";
        }
        
        theExtraHtml = size + onBlur;
        df.setExtraHtml(theExtraHtml);

        // Validate Bank Account Number
        df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbBankAccountNum");

        if (isRestrictLength) {
          size = "size=\"12\" maxLength=\"12\" ";
        }  else {
          size = "size=\"20\" maxLength=\"20\" ";  // this was moved out from JSP
        }

        if (isNumericOnly) {
      		onBlur = " onBlur=\"isFieldInteger(0);\""; // any length
        } else {
          onBlur = "";
        }

        theExtraHtml = size + onBlur;
        df.setExtraHtml(theExtraHtml);

        // Catherine, GE to MCAP link --- end -------------
	}

	/////////////////////////////////////////////////////////////////////////
	public void populateDisplayFieldValues(PageEntry pg)
	{
		Hashtable pst = pg.getPageStateTable();
		ComboBox comboBox =(ComboBox)(getCurrNDPage().getDisplayField("cbMonths"));

		try
		{
			if (pst != null)
			{
				Object month =(Object) pst.get("FAMONTH");
				Object year =(Object) pst.get("FAYEAR");
				Object day =(Object) pst.get("FADAY");
				comboBox.setValue(month);
				getCurrNDPage().setDisplayFieldValue("tbYear", year);
				getCurrNDPage().setDisplayFieldValue("tbDay", day);

				// populate the Servicing Mortgage Number
				Object tmpValue =(Object) pst.get("SERVMORTGNUM");
				getCurrNDPage().setDisplayFieldValue("tbServMortgNum", tmpValue);

				// Added new fields -- By BILLY 13March2002
				// populate the BankABA Bank Number
				tmpValue =(Object) pst.get("BANKABABANKNUM");
				getCurrNDPage().setDisplayFieldValue("tbBankABABankNum", tmpValue);

				// populate the BankABA Transit Number
				tmpValue =(Object) pst.get("BANKABATRANSITNUM");
				getCurrNDPage().setDisplayFieldValue("tbBankABATransitNum", tmpValue);

				// populate the Bank Account Number
				tmpValue =(Object) pst.get("BANKACCOUNTNUM");
				getCurrNDPage().setDisplayFieldValue("tbBankAccountNum", tmpValue);

				// populate the Bank Name
				tmpValue =(Object) pst.get("BANKNAME");
				getCurrNDPage().setDisplayFieldValue("tbBankName", tmpValue);
				//====================================================================

        //--> Ticket#127 : Change request for BMO
        //--> Add check box to indicate if the Broker Commission paid or not
        //--> By Billy 25Nov2003
        // Check if Tracking Broker Commission enabled
        if(isDisplayBrokerCommPaid())
        {
          tmpValue =(Object) pst.get("BROKERCOMMPAID");
				  getCurrNDPage().setDisplayFieldValue("chBrokerCommPaid", tmpValue);
        }
        //==================================================================

				return;
			}
			if (pst == null)
			{
				pst = new Hashtable();
				pg.setPageStateTable(pst);

				QueryModelBase doactualclosingdate = (QueryModelBase)
          (RequestManager.getRequestContext().getModelManager().getModel(doGetActualClosingDateModel.class));
				doactualclosingdate.clearUserWhereCriteria();
				doactualclosingdate.addUserWhereCriterion("dfDealId", "=", new Integer(pg.getPageDealId()));
				doactualclosingdate.addUserWhereCriterion("dfCopyId", "=", new Integer(pg.getPageDealCID()));
				doactualclosingdate.executeSelect(null);

				java.util.Date actualFundedDate = (java.util.Date)(doactualclosingdate.getValue("dfActualClosingDate"));
				if (actualFundedDate == null || actualFundedDate.toString().trim().length() == 0)
				{
					QueryModelBase thDo =(QueryModelBase)
            (RequestManager.getRequestContext().getModelManager().getModel(doDealSummarySnapShotModel.class));
					actualFundedDate = (java.util.Date)(thDo.getValue("dfEstClosingDate"));
				}
				logger.debug("--- FundingActivityHandler ---> actualFundedDate = " + actualFundedDate);

				java.util.Date estimatedFundedDate = null;
				if (actualFundedDate != null && ! actualFundedDate.toString().equals(""))
				{
					estimatedFundedDate = actualFundedDate;

					comboBox.setValue(new Integer(estimatedFundedDate.getMonth() + 1));
					int year = estimatedFundedDate.getYear() + 1900;
					getCurrNDPage().setDisplayFieldValue("tbYear", new String("" + year));
					getCurrNDPage().setDisplayFieldValue("tbDay", new String("" + estimatedFundedDate.getDate()));

					//getCurrNDPage().setDisplayFieldValue("stFundDate",	actualFundedDate );
					// Set display value to Servicing Mortgage Number
					getCurrNDPage().setDisplayFieldValue("tbServMortgNum", doactualclosingdate.getValue("dfServicingMortgNum"));

					// Added new fields -- By BILLY 13March2002
					// Set BankABANumber
					// SEAN Ticket #1708 Augest 05, 2005: change to handle the null value.
					String tmpNum = doactualclosingdate.getValue("dfBankABANumber") == null ?
						null : doactualclosingdate.getValue("dfBankABANumber").toString();
					// SEAN Ticket #1708 END
					String bankNum = "";
					String transitNum = "";

					// Separate the BankNum and Transit Number ==> Format "???-????????"
					// SEAN Ticket #1708 Augest 05, 2005: change to handle the null value.
					if (tmpNum != null && tmpNum.length() > 0)
					// SEAN Ticket #1708 END
					{
						int pos = tmpNum.indexOf('-');

            //--XC_BNAME_CR--start--//
						if (pos == 1 || pos == 2 || pos == 3)
            ////if (pos != 3)
						{
							//invalid format ==> get the first 3, 2, or 1 chars as BankNum and the rest as Transit number
              //// System should handle the "??-????????" and "?-????????" formats as well.
							if (tmpNum.length() >= 3)
							{
								////bankNum = tmpNum.substring(0, 3);
								////transitNum = tmpNum.substring(3);
                bankNum = tmpNum.substring(0, pos);
                transitNum = tmpNum.substring(pos + 1);
							}
							else
							{
								bankNum = tmpNum.substring(0);
							}
						}
						else
						{
							//In Correct format ==> skip the "-" char
							if (tmpNum.length() >= 4)
							{
								bankNum = tmpNum.substring(0, pos);
								transitNum = tmpNum.substring(pos + 1);
							}
            //--XC_BNAME_CR--end--//
							else
							{
								bankNum = tmpNum.substring(0, 3);
							}
						}
					}
					getCurrNDPage().setDisplayFieldValue("tbBankABABankNum", new String(bankNum));
					getCurrNDPage().setDisplayFieldValue("tbBankABATransitNum", new String(transitNum));

					// Set Bank Account Number
					getCurrNDPage().setDisplayFieldValue("tbBankAccountNum", doactualclosingdate.getValue("dfBankAccountNumber"));

					// populate the Bank Name
					getCurrNDPage().setDisplayFieldValue("tbBankName", doactualclosingdate.getValue("dfBankName"));

					//==========================================================================================

          //--> Ticket#127 : Change request for BMO
          //--> Add check box to indicate if the Broker Commission paid or not
          //--> By Billy 25Nov2003
          // Check if Tracking Broker Commission enabled
          if(isDisplayBrokerCommPaid())
          {
            getCurrNDPage().setDisplayFieldValue("chBrokerCommPaid", doactualclosingdate.getValue("dfBrokerCommPaid"));
          }
          //==================================================================

				}
				else
				{
					comboBox.setValue(new Integer(0));
					getCurrNDPage().setDisplayFieldValue("tbYear", new String(""));
					getCurrNDPage().setDisplayFieldValue("tbDay", new String(""));
				}
			}
		}
		catch(Exception e)
		{
			logger.error("@fundingActivity.populateDateDisplayFields Problem encountered during funding. :: " + e);
      setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
		}
	}


	///////////////////////////////////////////////////////////////////
	//
	// Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
	//
	public void setupBeforePageGeneration()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		if (pg.getSetupBeforeGenerationCalled() == true) return;
		pg.setSetupBeforeGenerationCalled(true);

		// -- //
		setupDealSummarySnapShotDO(pg);
	}

	///////////////////////////////////////////////////////////////////
	public void handleFundingDeal()
	{
		int dealStatusId = 0;
		PageEntry pg = getTheSessionState().getCurrentPage();
		java.util.Date fundedDate = saveScreenFieldsAndGetFundedDate(pg);

		logger.debug(" ---  Actual Funding Date == " + fundedDate);

		if (fundedDate == null)
		{
			setActiveMessageToAlert(BXResources.getSysMsg("FUND_DATE_INVALID", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			return;
		}

		PassiveMessage pm = runAllActiveDealFundingBusinessRules(pg, srk);

		if (pm != null)
		{
			pm.setGenerate(true);
			getTheSessionState().setPasMessage(pm);

 			//// SYNCADD
 			//Check if allowed th continue if softstop errors -- By BILLY 13June2002
 			if (pm.getCritical() == true || PropertiesCache.getInstance().getProperty(this.theSessionState.getDealInstitutionId(),"com.basis100.deal.funding.allowsoftstops", "N").equals("N"))
 			{
 				setActiveMessageToAlert(BXResources.getSysMsg("FUNDING_ACTIVITY_ERROR", theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMCONFIRM);
 				return;
 			}
 			////======================================================================

		}

 		//// SYNCADD.
 		//Check if allowed th continue if softstop errors -- By BILLY 13June2002
 		////else
 		////{
 		if (fundedDate.compareTo(new java.util.Date()) <= 0) //Actual Funding Date > Current Date
 		{
 			logger.debug(" ---  Actual Funding Date <= Current Date then StatusId == " + Mc.DEAL_FUNDED);
 			dealStatusId = Mc.DEAL_FUNDED;
    }
 		else
 		{
 			logger.debug(" ---  Actual Funding Date > Current Date then StatusId == " + Mc.DEAL_POST_DATED_FUNDING);
 			dealStatusId = Mc.DEAL_POST_DATED_FUNDING;
 		}

 		if (updateForFunding(pg, dealStatusId, fundedDate) != false)
          handleOkStandard();

 		////}
 		////==========================================================================================

		return;
	}

	  /**
	    * requestFundingDocs:
	    *
	    * -- Wire Transfer Doc + Data Entry Doc + NBC Solicitor package
	    *
	    * Args: PageEntry, dealStatusId, fundedDate
	    * 
	    * @version 1.1 <br>
		*  Date: 08/08/006 <br>
		*  Author: NBC/PP Implementation Team <br>
		*  Change: <br>
		*   - Added logic check for the NBC specific lender and call DocumentRequest.NBCrequestSolicitorsPackage
	    *     to generate the NBCSolicitorpackage PDF Document <br>
	    **/
	///////////////////////////////////////////////////////////////////
	private boolean updateForFunding(PageEntry pg, int dealStatusId, java.util.Date fundedDate)
	{
		SessionResourceKit srk = getSessionResourceKit();

		try
		{
			srk.beginTransaction();

			// always adopt transactional copy on submit
			standardAdoptTxCopy(pg, true);
			Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());

			//Add log to deal history -- Billy 16May2001
			// Create History Record if Status Changed
			int oldStatus = deal.getStatusId();
			deal.setStatusId((short) dealStatusId);
			deal.setStatusDate(new Date());
			if (oldStatus != dealStatusId)
			{
				DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);
				String msg = hLog.statusChange(deal.getStatusId(), "Funding Activity");
				hLog.log(deal.getDealId(), deal.getCopyId(), msg, Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), deal.getStatusId());
			}
			deal.setActualClosingDate(fundedDate);

			// Update Servicing Mortgage Number
			deal.setServicingMortgageNumber(((String)(pg.getPageStateTable().get("SERVMORTGNUM"))).toString());

			// Check and send Servicing Upload -- Modified by BILLY 09Nov2001
			CCM ccm = new CCM(srk);
			ccm.sendServicingUpload(srk, deal, Mc.SERVICING_UPLOAD_TYPE_FUNDING, theSessionState.getLanguageId());
			//	  	***** Change by NBC Impl. Team - Version 1.1 - Start *****//
	        String _lenderId = PropertiesCache.getInstance().getProperty(this.theSessionState.getDealInstitutionId(),"NBC_LENDER","N");
			if (_lenderId.equalsIgnoreCase("y") )
			{
			  DocumentRequest.requestNBCSolicitorsPackage(srk, deal, XSLMerger.TYPE_PDF);
			}
			else
			{
			  //String logMsg = hisLog.sourceSystemReponse(deal.getSourceOfBusinessProfileId());
			// New requirement from Product -- By BILLY 03Jan2002
			ccm.requestDisbursmentLetter(srk, deal);

			// New Requirement to send 2 new Docs for funding -- By Billy 22April2002
			ccm.requestFundingDocs(srk, deal);
	        }
			//	  	***** Change by NBC Impl. Team - Version 1.1 - End *****//
			deal.ejbStore();
			workflowTrigger(0, srk, pg, null);
			srk.commitTransaction();
			navigateToNextPage();
		}
		catch(Exception e)
		{
			// rollback the transaction
			srk.cleanTransaction();
			logger.error("Problem encountered during funding, deal Id = " +((pg.getPageDealId() == - 1) ? "{unknown}" : "" + pg.getPageDealId()));
			logger.error(e);
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			return false;
		}
		return true;
	}


	/**
	 *
	 *
	 */
	public PassiveMessage runAllActiveDealFundingBusinessRules(PageEntry pg, SessionResourceKit srk)
	{
		PassiveMessage pm = new PassiveMessage();
		BusinessRuleExecutor brExec = new BusinessRuleExecutor();

		try
		{
			brExec.BREValidator(getTheSessionState(), pg, srk, pm, "DF-%");
			logger.trace("--T--> @FundingActivityHandler.runAllActiveDealFundingBusinessRules: DF rules");
			if (pm.getNumMessages() > 0) return pm;
		}
		catch(Exception e)
		{
			logger.error("Problem encountered during funding, @FundingActivityHandler.runAllActiveDealFundingBusinessRules");
			logger.error(e);
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
		}
		return null;
	}

	///////////////////////////////////////////////////////////////////
	public java.util.Date saveScreenFieldsAndGetFundedDate(PageEntry pg)
	{
		ViewBean currNDPage = getCurrNDPage();
		ComboBox months =(ComboBox) currNDPage.getDisplayField("cbMonths");

		Object cspmonth = months.getValue();
		Object cspday = currNDPage.getDisplayFieldValue("tbDay");
		Object cspyear = currNDPage.getDisplayFieldValue("tbYear");

		Hashtable pst = pg.getPageStateTable();

		if (cspmonth.toString().length() != 0) pst.put("FAMONTH", cspmonth);
		else pst.put("FAMONTH", new String(""));

		pst.put("FAYEAR", cspyear);
		pst.put("FADAY", cspday);

		// Save Servicing Mortgage Num as well
		pst.put("SERVMORTGNUM", currNDPage.getDisplayFieldValue("tbServMortgNum"));

		// Added new fields -- By BILLY 13March2002
		// Save BankABA Bank Number
    //--XC_BNAME_CR--start--//
    pst.put("BANKABABANKNUM", currNDPage.getDisplayFieldValue("hdBankABABankNum"));
logger.debug("FAH@saveScreenFieldsAndGetFundedDate::hdBankABABankNumHd: " + currNDPage.getDisplayFieldValue("hdBankABABankNum"));
logger.debug("FAH@saveScreenFieldsAndGetFundedDate::tbBankABATransitNum: " + currNDPage.getDisplayFieldValue("tbBankABATransitNum"));
    //--XC_BNAME_CR--end--//

		// Save BankABA Transit Number
		pst.put("BANKABATRANSITNUM", currNDPage.getDisplayFieldValue("tbBankABATransitNum"));

		// Save Bank Account Number
		pst.put("BANKACCOUNTNUM", currNDPage.getDisplayFieldValue("tbBankAccountNum"));

		// Save Bank Name
    //--XC_BNAME_CR--start--//
    pst.put("BANKNAME", currNDPage.getDisplayFieldValue("hdBankName"));
logger.debug("FAH@saveScreenFieldsAndGetFundedDate::hdBankNameHd: " + currNDPage.getDisplayFieldValue("hdBankName"));
logger.debug("FAH@saveScreenFieldsAndGetFundedDate::tbBankName: " + currNDPage.getDisplayFieldValue("tbBankName"));
    //--XC_BNAME_CR--end--//
		//====================================================================

    //--> Ticket#127 : Change request for BMO
    //--> Add check box to indicate if the Broker Commission paid or not
    //--> By Billy 25Nov2003
    // Check if Tracking Broker Commission enabled
    if(isDisplayBrokerCommPaid())
    {
      pst.put("BROKERCOMMPAID", currNDPage.getDisplayFieldValue("chBrokerCommPaid"));
    }
    //==================================================================

		java.util.Date fundedDate = null;

		try
		{
			if (cspyear.toString().length() < 4) return null;
			String day = "";
			if (cspday.toString().length() == 1) day = "0" + cspday.toString();
			else day = cspday.toString();
      String month = "";
			if (cspmonth.toString().length() == 1) month = "0" + cspmonth.toString();
			else month = cspmonth.toString();
			String dateIn =(month + "/" + day + "/" + cspyear.toString() + " ").trim();
			logger.debug("---FundingActivityHandler---> dateIn == " + dateIn);
			fundedDate = parseInputDate(dateIn);
		}
		catch(Exception e)
		{
			logger.error("--- @FundingActivityHandler.getFundedDate Problem encountered parsing funded date.");
			return null;
		}
		return fundedDate;
	}

	/////////////////////////////////////////////////////////////////////////////
	private java.util.Date parseInputDate(String dateStr)
	{
		try
		{
			dateStr = dateStr.trim();
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			ParsePosition pos = new ParsePosition(0);
			Date fundedDate = df.parse(dateStr, pos);

			// validate by converting to formatted date and comparing
			String valStr = df.format(fundedDate);
			if (valStr.equals(dateStr)) return fundedDate;
		}
		catch(Exception e)
		{
			logger.error("--- @FundingActivityHandler.parseInputDate Problem encountered parsing funded date. :: " + e);
			return null;
		}
		return null;
	}

	// Added new fields an make the Submit button just for saving data -- BILLY 13March2002
	public void handleSubmit()
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
        // ***** Change by NBC Impl. Team - Version 1.9 - Start *****//
		    SessionResourceKit srk  = getSessionResourceKit();
			CalcMonitor dcm         = CalcMonitor.getMonitor(srk);
			try
			{
				Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID(), dcm);
				// Force to trigger calc by totalCostOfBorrowing
			   deal.forceChange("totalCostOfBorrowing");
			   deal.forceChange("interestOverTerm");
			   deal.forceChange("balanceRemainingAtEndOfTerm");
			   deal.forceChange("teaserRateInterestSaving");
			   
		       dcm.inputEntity(deal);
			   doCalculation(dcm);
			}
		    catch (Exception ex)
			{
			    logger.error("Exception @handleRecalculate :: " + ex.getMessage());	
			       ex.printStackTrace();
			}
      // ***** Change by NBC Impl. Team - Version 1.9 - End *****//
			saveScreenFieldsAndGetFundedDate(pg);
			srk.beginTransaction();
			standardAdoptTxCopy(pg, true);

      //--> Ticket#127 : Change request for BMO
      //--> Add check box to indicate if the Broker Commission paid or not
      //--> By Billy 25Nov2003
      // Check if Tracking Broker Commission enabled then run workflow to check if
      // "Track Broker Commission task" completed
      if(isDisplayBrokerCommPaid())
      {
        workflowTrigger(0, srk, pg, null);
      }
      //=========================================================================

			srk.commitTransaction();
			navigateToNextPage(true);
			return;
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Exception @FundingActivityHandler.handleSubmit:");
			logger.error(e);
		}
		//  exception was encountered ... navigate away from the page (including away from parent page if sub)
		navigateAwayFromFromPage(true);
	}

	/////////////////////////////////////////////////////////////////////
	public void saveData(PageEntry pg, boolean calledInTransaction)
	{
		logger.debug("@FundingActivityHandler.saveData():: saveData IN");
		int i = 0;
		SessionResourceKit srk = getSessionResourceKit();
    Hashtable pst = pg.getPageStateTable();

		try
		{
			ViewBean currNDPage = getCurrNDPage();
			CalcMonitor dcm = CalcMonitor.getMonitor(srk);
			srk.beginTransaction();
			Deal deal = new Deal(srk, dcm, pg.getPageDealId(), pg.getPageDealCID());

			// Update Servicing Mortgage Number
			deal.setServicingMortgageNumber(currNDPage.getDisplayFieldValue("tbServMortgNum").toString());
			// Update BankABANumber
      //--> Bug fix :: to check if no input then clear the filed :: By Billy 26Aug2003

      //--XC_BNAME_CR--start--//
      ////String tmpBankNum = currNDPage.getDisplayFieldValue("hdBankABABankNum").toString().trim();
      String tmpBankNum = currNDPage.getDisplayFieldValue("tbBankABABankNum").toString().trim();


//temp
logger.debug("FAH@saveData::hdBankABABankNumHd: " + currNDPage.getDisplayFieldValue("hdBankABABankNum"));
logger.debug("FAH@saveData::tbBankABABankNumTBox: " + currNDPage.getDisplayFieldValue("tbBankABABankNum"));

logger.debug("FAH@saveData::tbBankABATransitNum: " + currNDPage.getDisplayFieldValue("tbBankABATransitNum"));

      //--XC_BNAME_CR--end--//

      String tmpTranNum = currNDPage.getDisplayFieldValue("tbBankABATransitNum").toString().trim();
      if(tmpBankNum.length() == 0 && tmpTranNum.length() == 0)
        deal.setBankABANumber("");
      else
			  deal.setBankABANumber(tmpBankNum + "-" + tmpTranNum);
      //============================================================
			// Update Bank Account Number
      deal.setBankAccountNumber(currNDPage.getDisplayFieldValue("tbBankAccountNum").toString());
			// Update Bank Name
//temp
logger.debug("FAH@saveData::hdBankName: " + currNDPage.getDisplayFieldValue("hdBankName"));
logger.debug("FAH@saveData::tbBankNameTBox: " + currNDPage.getDisplayFieldValue("tbBankName"));

			deal.setBankName(currNDPage.getDisplayFieldValue("tbBankName").toString());

      //--> Ticket#127 : Change request for BMO
      //--> Add check box to indicate if the Broker Commission paid or not
      //--> By Billy 25Nov2003
      // Check if Tracking Broker Commission enabled
      if(isDisplayBrokerCommPaid())
      {
        deal.setBrokerCommPaid(currNDPage.getDisplayFieldValue("chBrokerCommPaid").toString());
      }
      //==================================================================

			deal.ejbStore();
			dcm.calc();
			srk.commitTransaction();
			if (srk.getModified()) pg.setModified(true);
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @FundingActivityHandler.SaveData, deal Id = " + pg.getPageDealId() + ", copy Id = " + pg.getPageDealCID() + ", line number = " + i);
			logger.error(e);
			return;
		}
	}

  //--> Ticket#127 : Change request for BMO
  //--> Add check box to indicate if the Broker Commission paid or not
  //--> By Billy 25Nov2003
  public boolean isDisplayBrokerCommPaid()
  {
    // Display if trackbrokercomm = Y
    if((PropertiesCache.getInstance().getProperty(this.theSessionState.getDealInstitutionId(),"com.basis100.funding.trackbrokercomm", "N")).equals("N"))
      return false;
    else
      return true;
  }
  //--> Method to get the Broker Commission paid Label
  //--> By Billy 25Nov2003
  public String getBrokerCommPaidLabel()
  {
    return BXResources.getGenericMsg("BROKERCOMMPAID_LABEL", theSessionState.getLanguageId());
  }
  //==================================================================

  //--> Ticket#371 :: Change for Cervus :: Added Upload Button
  //--> By Billy 27May2004
  public void handleUpload()
  {
    SessionResourceKit srk = getSessionResourceKit();
    PageEntry pg = getTheSessionState().getCurrentPage();
    saveScreenFieldsAndGetFundedDate(pg);
    try
    {
      srk.beginTransaction();
      Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
      CCM ccm = new CCM(srk);
      ccm.sendServicingUpload(srk, deal, Mc.SERVICING_UPLOAD_TYPE_FUNDING, theSessionState.getLanguageId());
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      // rollback the transaction
      srk.cleanTransaction();
      logger.error("Problem encountered during sending upload, deal Id = " +((pg.getPageDealId() == - 1) ? "{unknown}" : "" + pg.getPageDealId()));
      logger.error(e);
    }
  }

  public boolean isDisplayUploadBtn()
  {
    // Check if Editable
    if(displaySubmitButton())
    {
      // check min usertype parameter
      int theMinUserType = getIntValue(
        (PropertiesCache.getInstance().getProperty(this.theSessionState.getDealInstitutionId(),"com.basis100.funding.minusertypeforuploadbutton", "0")));
      if(theMinUserType > 0 && theSessionState.getSessionUserType() >= theMinUserType)
        return true;
    }
    return false;
  }

  //--XC_BNAME_CR--start--//
  // Define the static variable to store the JavaScript Array values in order
  // to avoid getting PickList everytime displaying the screen.
  private static String VALS_DATA = "";
  // To initial the JavaScript Array values when the Class loaded up the first time user access the screen
  // It is done via static since the table's content is huge and it will allow escaping of JVM problem
  // on pages load.
  static
  {
    // Get values TRANSITBANKASSOC table
    VALS_DATA = "VALStbBankABABankNum = new Array(" + PicklistData.getColumnValuesCDV("TRANSITTOBANKASSOC", "TRANSITID") + ");" + "\n";
    VALS_DATA += "VALStbBankName = new Array(" + PicklistData.getColumnValuesCDS("TRANSITTOBANKASSOC", "NAMEOFINSTITUTION") + ");" + "\n";

    VALS_DATA += "VALShdBankName = VALStbBankName;" + "\n";
    VALS_DATA += "VALShdBankABABankNum = VALStbBankABABankNum;" + "\n";
  }

  private void populateDefaultsVALSData(ViewBean thePage)
  {
    // Catherine, GE to MCAP link --- begin -------------
//    String valsData = "";
//    valsData = populateDefaultsVALSDataBankTransit(thePage, valsData);
    // Catherine, GE to MCAP link --- end -------------

  }

  private String populateDefaultsVALSDataBankTransit(ViewBean thePage, String valsData)
  {
      String onChange = "";

      HtmlDisplayFieldBase df;

      // Setup OnChange events handling to populate default fields
      df = (HtmlDisplayFieldBase) thePage.getDisplayField("tbBankABABankNum");
      onChange = "onChange=\"if(!setBooleanDefaults('tbBankABABankNum', ['hdBankName','tbBankName'] )) " +
        "setValueIfNotMatched(['tbBankName'], '0', ''); " +
        "populateValueTo(['hdBankABABankNum']);\"";
      df.setExtraHtml(onChange);

      //// Currently Xceed wants to see 'one' way defaulting only: from BankNum
      //// to BankName population.
      /**
      df = (HtmlDisplayFieldBase) thePage.getDisplayField("tbBankName");
      onChange = "onChange=\"if(!setBooleanDefaults('tbBankName', ['hdBankABABankNum','tbBankABABankNum'] )) " +
        "setValueIfNotMatched(['tbBankABABankNum'], '0', ''); " +
        "populateValueTo(['hdBankName']);\"";
      df.setExtraHtml(onChange);
      **/

      return valsData;
  }
  //--XC_BNAME_CR--end--//

  //--CervusPhaseII--start--//
  public void handleCustomActMessageOk(String[] args)
  {
    logger.trace("--T--> FAH@handleCustomActMessageOk: " +((args != null && args [ 0 ] != null) ? args [ 0 ] : "No args"));

    PageEntry pg = theSessionState.getCurrentPage();
    SessionResourceKit srk = getSessionResourceKit();
    ViewBean thePage = getCurrNDPage();

    if (args [ 0 ].equals("OVERRIDE_DEAL_LOCK_YES"))
    {
      logger.trace("FAH@handleCustomActMessageOk_REJECT_COND_YES: Override deal lock");
      provideOverrideDealLockActivity(pg, srk, thePage);
      return;
    }

    if (args [ 0 ].equals("OVERRIDE_DEAL_LOCK_NO"))
    {
      logger.trace("FAH@handleCustomActMessageOk_REJECT_COND_NO: Return to previous screen");

      theSessionState.setActMessage(null);
      theSessionState.overrideDealLock(true);
      handleCancelStandard(false);

      return;
    }
  }
  //--CervusPhaseII--end--//

}


