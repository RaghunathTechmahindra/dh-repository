package mosApp.MosSystem;

import java.math.BigDecimal;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 * <p>Title: doDealSummarySnapShotModelImpl</p>
 *
 * <p>Description: implementation class - model- for deal summary snap shot</p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.2
 *
 */
public class doDealSummarySnapShotModelImpl extends QueryModelBase
	implements doDealSummarySnapShotModel
{
  
  private final static Log _log = LogFactory
    .getLog(doDealSummarySnapShotModelImpl.class);
	/**
	 *
	 *
	 */
	public doDealSummarySnapShotModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
    _log.debug("WorkQueue Deal Summary Snap sql: " + sql);
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //By Billy 04Nov2002

    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);

    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();

    //Browser all returned results and convert all Description by Resource Bundle
    while(this.next())
    {
      // Convert DealStatus Desc.
      this.setDfStatusDescr(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),"STATUS", this.getDfStatusDescr(), languageId));
      // Convert LineOfBusiness Desc.
      this.setDfLOBDescr(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),"LINEOFBUSINESS", this.getDfLOBDescr(), languageId));
      // Convert DealType Desc.
      this.setDfDealTypeDescr(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),"DEALTYPE", this.getDfDealTypeDescr(), languageId));
      // Convert DealPurpose Desc.
      this.setDfLoanPurposeDescr(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),"DEALPURPOSE", this.getDfLoanPurposeDescr(), languageId));
      // Convert PaymentTerm Desc.
      this.setDfPaymentTermDescr(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),"PAYMENTTERM", this.getDfPaymentTermDescr(), languageId));
      // Convert SpecialFeature Desc.
      this.setDfSpecialFeature(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),"SPECIALFEATURE", this.getDfSpecialFeature(), languageId));
    }

    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public String getDfApplicationID()
	{
		return (String)getValue(FIELD_DFAPPLICATIONID);
	}


	/**
	 *
	 *
	 */
	public void setDfApplicationID(String value)
	{
		setValue(FIELD_DFAPPLICATIONID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStatusDescr()
	{
		return (String)getValue(FIELD_DFSTATUSDESCR);
	}


	/**
	 *
	 *
	 */
	public void setDfStatusDescr(String value)
	{
		setValue(FIELD_DFSTATUSDESCR,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfStatusDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFSTATUSDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfStatusDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFSTATUSDATE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSFShortName()
	{
		return (String)getValue(FIELD_DFSFSHORTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfSFShortName(String value)
	{
		setValue(FIELD_DFSFSHORTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSOBPShortName()
	{
		return (String)getValue(FIELD_DFSOBPSHORTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfSOBPShortName(String value)
	{
		setValue(FIELD_DFSOBPSHORTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLOBDescr()
	{
		return (String)getValue(FIELD_DFLOBDESCR);
	}


	/**
	 *
	 *
	 */
	public void setDfLOBDescr(String value)
	{
		setValue(FIELD_DFLOBDESCR,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDealTypeDescr()
	{
		return (String)getValue(FIELD_DFDEALTYPEDESCR);
	}


	/**
	 *
	 *
	 */
	public void setDfDealTypeDescr(String value)
	{
		setValue(FIELD_DFDEALTYPEDESCR,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLoanPurposeDescr()
	{
		return (String)getValue(FIELD_DFLOANPURPOSEDESCR);
	}


	/**
	 *
	 *
	 */
	public void setDfLoanPurposeDescr(String value)
	{
		setValue(FIELD_DFLOANPURPOSEDESCR,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalPurchasePrice()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalPurchasePrice(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALPURCHASEPRICE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalLoanAmt()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALLOANAMT);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalLoanAmt(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALLOANAMT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPaymentTermDescr()
	{
		return (String)getValue(FIELD_DFPAYMENTTERMDESCR);
	}


	/**
	 *
	 *
	 */
	public void setDfPaymentTermDescr(String value)
	{
		setValue(FIELD_DFPAYMENTTERMDESCR,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfEstClosingDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFESTCLOSINGDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfEstClosingDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFESTCLOSINGDATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSpecialFeature()
	{
		return (String)getValue(FIELD_DFSPECIALFEATURE);
	}


	/**
	 *
	 *
	 */
	public void setDfSpecialFeature(String value)
	{
		setValue(FIELD_DFSPECIALFEATURE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}

	//-- ========== SCR#859 begins ========== --//
	//-- by Neil on Feb 15, 2005
	public String getDfServicingMortgageNumber() {
		return (String)getValue(FIELD_DFSERVICINGMORTGAGENUMBER);
	}
	
	public void setDfServicingMortgageNumber(String value) {
		setValue(FIELD_DFSERVICINGMORTGAGENUMBER, value);
	}
	//-- ========== SCR#859 ends ========== --//

//	 ***** Change by NBC Impl. Team - Version 1.2 - Start *****//
	/**
	 * getDfCashBackOverrideInDollars
	 * 
	 * @param None
	 *            <br>
	 * 
	 * @return String : the result of getDfCashBackOverrideInDollars <br>
	 */
	public String getDfCashBackOverrideInDollars() {
		return (String) getValue(FIELD_DFCASHBACKOVERRIDEINDOLLARS);
	}

	/**
	 * setDfCashBackOverrideInDollars
	 * 
	 * @param String
	 *            <br>
	 * 
	 * @return void : the result of setDfCashBackOverrideInDollars <br>
	 */
	public void setDfCashBackOverrideInDollars(String value) {
		setValue(FIELD_DFCASHBACKOVERRIDEINDOLLARS, value);

	}

	/**
	 * getDfCashBackInDollars
	 * 
	 * @param None
	 *            <br>
	 * 
	 * @return BigDecimal : the result of getDfCashBackInDollars <br>
	 */

	public java.math.BigDecimal getDfCashBackInDollars() {
		return (java.math.BigDecimal) getValue(FIELD_DFCASHBACKINDOLLARS);
	}

	/**
	 * setDfCashBackInDollars
	 * 
	 * @param BigDecimal
	 *            <br>
	 * 
	 * @return Void : the result of setDfCashBackInDollars <br>
	 */
	public void setDfCashBackInDollars(java.math.BigDecimal value) {
		setValue(FIELD_DFCASHBACKINDOLLARS, value);
	}

	/**
	 * getDfCashBackInPercentage
	 * 
	 * @param None
	 *            <br>
	 * 
	 * @return BigDecimal : the result of getDfCashBackInPercentage <br>
	 */

	public java.math.BigDecimal getDfCashBackInPercentage() {
		return (java.math.BigDecimal) getValue(FIELD_DFCASHBACKINPERCENTAGE);
	}

	/**
	 * setDfCashBackInPercentage
	 * 
	 * @param BigDecimal
	 *            <br>
	 * 
	 * @return void : the result of setDfCashBackInPercentage <br>
	 */
	public void setDfCashBackInPercentage(java.math.BigDecimal value) {
		setValue(FIELD_DFCASHBACKINPERCENTAGE, value);
	}

	public String getDfDealStatusId()
    {
        return (String)getValue(FIELD_DFSTATUSID_STR);
    } 

    public void setDfDealStatusId(String value)
    {
        setValue(FIELD_DFSTATUSID_STR, value);
    }
    
	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";

  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 04Nov2002

  //--Release2.1--//
  //public static final String SELECT_SQL_TEMPLATE="SELECT ALL DEAL.APPLICATIONID,
  //STATUS.STATUSDESCRIPTION, DEAL.STATUSDATE, SOURCEFIRMPROFILE.SFSHORTNAME,
  //SOURCEOFBUSINESSPROFILE.SOBPSHORTNAME, LINEOFBUSINESS.LOBDESCRIPTION,
  //DEALTYPE.DTDESCRIPTION, dealpurpose.DPDESCRIPTION, DEAL.TOTALPURCHASEPRICE,
  //DEAL.TOTALLOANAMOUNT, PAYMENTTERM.PTDESCRIPTION, DEAL.ESTIMATEDCLOSINGDATE,
  //DEAL.DEALID, SPECIALFEATURE.SFDESCRIPTION, DEAL.COPYID FROM DEAL,
  //STATUS, SOURCEFIRMPROFILE, SOURCEOFBUSINESSPROFILE, LINEOFBUSINESS,
  //PAYMENTTERM, DEALTYPE, dealpurpose, SPECIALFEATURE  __WHERE__  ";

	//--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
  public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL DEAL.APPLICATIONID, to_char(DEAL.STATUSID) STATUSID_STR, DEAL.STATUSDATE, "+
    "SOURCEFIRMPROFILE.SFSHORTNAME, SOURCEOFBUSINESSPROFILE.SOBPSHORTNAME, to_char(DEAL.LINEOFBUSINESSID) LINEOFBUSINESSID_STR, "+
    "to_char(DEAL.DEALTYPEID) DEALTYPEID_STR, to_char(DEAL.DEALPURPOSEID) DEALPURPOSEID_STR, DEAL.TOTALPURCHASEPRICE, DEAL.TOTALLOANAMOUNT, "+
    "to_char(DEAL.PAYMENTTERMID) PAYMENTTERMID_STR, DEAL.ESTIMATEDCLOSINGDATE, DEAL.DEALID, to_char(DEAL.SPECIALFEATUREID) SPECIALFEATUREID_STR, "+
	//-- ========== SCR#859 begins ========== --//
	//-- by Neil on Feb 15, 2005
	"DEAL.SERVICINGMORTGAGENUMBER, " +
	//-- ========== SCR#859 ends ========== --//
    "DEAL.COPYID ,"+
    
//  ***** Change by NBC Impl. Team - Version 1.2 - Start *****//
	"DEAL.CASHBACKAMOUNT , DEAL.CASHBACKPERCENT , DEAL.CASHBACKAMOUNTOVERRIDE, " +

	// ***** Change by NBC Impl. Team - Version 1.2 - End*****//
  "DEAL.INSTITUTIONPROFILEID " +
  
    "FROM DEAL, SOURCEFIRMPROFILE, SOURCEOFBUSINESSPROFILE "+
    " __WHERE__  ";

  //--Release2.1--//
  //public static final String MODIFYING_QUERY_TABLE_NAME="DEAL, STATUS, SOURCEFIRMPROFILE,
  //SOURCEOFBUSINESSPROFILE, LINEOFBUSINESS, PAYMENTTERM, DEALTYPE,
  //dealpurpose, SPECIALFEATURE";
	//--> Take out all table joins will PickList tables
  public static final String MODIFYING_QUERY_TABLE_NAME=
    "DEAL, SOURCEFIRMPROFILE, SOURCEOFBUSINESSPROFILE";

  //--Release2.1--//
  //public static final String STATIC_WHERE_CRITERIA=" (DEAL.STATUSID  =  STATUS.STATUSID) AND (DEAL.SOURCEFIRMPROFILEID  =  SOURCEFIRMPROFILE.SOURCEFIRMPROFILEID) AND (DEAL.SOURCEOFBUSINESSPROFILEID  =  SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPROFILEID) AND (DEAL.LINEOFBUSINESSID  =  LINEOFBUSINESS.LINEOFBUSINESSID) AND (DEAL.PAYMENTTERMID  =  PAYMENTTERM.PAYMENTTERMID) AND (DEAL.DEALTYPEID  =  DEALTYPE.DEALTYPEID) AND (DEAL.dealpurposeID  =  dealpurpose.dealpurposeID) AND (DEAL.SPECIALFEATUREID  =  SPECIALFEATURE.SPECIALFEATUREID)";
	//--> Take out all table joins will PickList tables
  public static final String STATIC_WHERE_CRITERIA=
    " (DEAL.INSTITUTIONPROFILEID  =  SOURCEFIRMPROFILE.INSTITUTIONPROFILEID) AND "+  
    " (DEAL.INSTITUTIONPROFILEID  =  SOURCEOFBUSINESSPROFILE.INSTITUTIONPROFILEID) AND "+
    " (DEAL.SOURCEFIRMPROFILEID  =  SOURCEFIRMPROFILE.SOURCEFIRMPROFILEID) AND "+
    "(DEAL.SOURCEOFBUSINESSPROFILEID  =  SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPROFILEID) ";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFAPPLICATIONID="DEAL.APPLICATIONID";
	public static final String COLUMN_DFAPPLICATIONID="APPLICATIONID";

  //--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
  //public static final String QUALIFIED_COLUMN_DFSTATUSDESCR="STATUS.STATUSDESCRIPTION";
  //public static final String COLUMN_DFSTATUSDESCR="STATUSDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFSTATUSDESCR="DEAL.STATUSID_STR";
  public static final String COLUMN_DFSTATUSDESCR="STATUSID_STR";

	public static final String QUALIFIED_COLUMN_DFSTATUSDATE="DEAL.STATUSDATE";
	public static final String COLUMN_DFSTATUSDATE="STATUSDATE";
	public static final String QUALIFIED_COLUMN_DFSFSHORTNAME="SOURCEFIRMPROFILE.SFSHORTNAME";
	public static final String COLUMN_DFSFSHORTNAME="SFSHORTNAME";
	public static final String QUALIFIED_COLUMN_DFSOBPSHORTNAME="SOURCEOFBUSINESSPROFILE.SOBPSHORTNAME";
	public static final String COLUMN_DFSOBPSHORTNAME="SOBPSHORTNAME";

  //--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFLOBDESCR="LINEOFBUSINESS.LOBDESCRIPTION";
	//public static final String COLUMN_DFLOBDESCR="LOBDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFLOBDESCR="DEAL.LINEOFBUSINESSID_STR";
	public static final String COLUMN_DFLOBDESCR="LINEOFBUSINESSID_STR";

  //--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFDEALTYPEDESCR="DEALTYPE.DTDESCRIPTION";
	//public static final String COLUMN_DFDEALTYPEDESCR="DTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFDEALTYPEDESCR="DEAL.DEALTYPEID_STR";
	public static final String COLUMN_DFDEALTYPEDESCR="DEALTYPEID_STR";

  //--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFLOANPURPOSEDESCR="dealpurpose.DPDESCRIPTION";
	//public static final String COLUMN_DFLOANPURPOSEDESCR="DPDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFLOANPURPOSEDESCR="DEAL.DEALPURPOSEID_STR";
	public static final String COLUMN_DFLOANPURPOSEDESCR="DEALPURPOSEID_STR";

	public static final String QUALIFIED_COLUMN_DFTOTALPURCHASEPRICE="DEAL.TOTALPURCHASEPRICE";
	public static final String COLUMN_DFTOTALPURCHASEPRICE="TOTALPURCHASEPRICE";
	public static final String QUALIFIED_COLUMN_DFTOTALLOANAMT="DEAL.TOTALLOANAMOUNT";
	public static final String COLUMN_DFTOTALLOANAMT="TOTALLOANAMOUNT";

  //--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFPAYMENTTERMDESCR="PAYMENTTERM.PTDESCRIPTION";
	//public static final String COLUMN_DFPAYMENTTERMDESCR="PTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFPAYMENTTERMDESCR="DEAL.PAYMENTTERMID_STR";
	public static final String COLUMN_DFPAYMENTTERMDESCR="PAYMENTTERMID_STR";

	public static final String QUALIFIED_COLUMN_DFESTCLOSINGDATE="DEAL.ESTIMATEDCLOSINGDATE";
	public static final String COLUMN_DFESTCLOSINGDATE="ESTIMATEDCLOSINGDATE";
	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";

  //--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFSPECIALFEATURE="SPECIALFEATURE.SFDESCRIPTION";
	//public static final String COLUMN_DFSPECIALFEATURE="SFDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFSPECIALFEATURE="DEAL.SPECIALFEATUREID_STR";
	public static final String COLUMN_DFSPECIALFEATURE="SPECIALFEATUREID_STR";

	public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";

	//-- ========== SCR#859 begins ========== --//
	//-- by Neil on Feb 15, 2005
	public static final String COLUMN_DFSERVICINGMORTGAGENUMBER = "SERVICINGMORTGAGENUMBER";
	public static final String QUALIFIED_COLUMN_DFSERVICINGMORTGAGENUMBER = "DEAL.SERVICINGMORTGAGENUMBER";
	//-- ========== SCR#859 ends ========== --//
	
//	 ***** Change by NBC Impl. Team - Version 1.2 - Start *****//
		public static final String QUALIFIED_COLUMN_DFCASHBACKINPERCENTAGE = "DEAL.CASHBACKPERCENT";

		public static final String COLUMN_DFCASHBACKINPERCENTAGE = "CASHBACKPERCENT";

		public static final String QUALIFIED_COLUMN_DFCASHBACKINDOLLARS = "DEAL.CASHBACKAMOUNT";

		public static final String COLUMN_DFCASHBACKINDOLLARS = "CASHBACKAMOUNT";

		public static final String QUALIFIED_COLUMN_DFCASHBACKOVERRIDEINDOLLARS = "DEAL.CASHBACKAMOUNTOVERRIDE";

		public static final String COLUMN_DFCASHBACKOVERRIDEINDOLLARS = "CASHBACKAMOUNTOVERRIDE";

		// ***** Change by NBC Impl. Team - Version 1.2 - End*****//
    public static final String QUALIFIED_COLUMN_DFSTATUSID_STR = "DEAL.STATUSID_STR";
    public static final String COLUMN_DFSTATUSID_STR = "STATUSID_STR";

    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID="DEAL.INSTITUTIONPROFILEID";
    public static final String COLUMN_DFINSTITUTIONID="INSTITUTIONPROFILEID";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPLICATIONID,
				COLUMN_DFAPPLICATIONID,
				QUALIFIED_COLUMN_DFAPPLICATIONID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTATUSDESCR,
				COLUMN_DFSTATUSDESCR,
				QUALIFIED_COLUMN_DFSTATUSDESCR,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTATUSDATE,
				COLUMN_DFSTATUSDATE,
				QUALIFIED_COLUMN_DFSTATUSDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSFSHORTNAME,
				COLUMN_DFSFSHORTNAME,
				QUALIFIED_COLUMN_DFSFSHORTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOBPSHORTNAME,
				COLUMN_DFSOBPSHORTNAME,
				QUALIFIED_COLUMN_DFSOBPSHORTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLOBDESCR,
				COLUMN_DFLOBDESCR,
				QUALIFIED_COLUMN_DFLOBDESCR,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALTYPEDESCR,
				COLUMN_DFDEALTYPEDESCR,
				QUALIFIED_COLUMN_DFDEALTYPEDESCR,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLOANPURPOSEDESCR,
				COLUMN_DFLOANPURPOSEDESCR,
				QUALIFIED_COLUMN_DFLOANPURPOSEDESCR,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALPURCHASEPRICE,
				COLUMN_DFTOTALPURCHASEPRICE,
				QUALIFIED_COLUMN_DFTOTALPURCHASEPRICE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALLOANAMT,
				COLUMN_DFTOTALLOANAMT,
				QUALIFIED_COLUMN_DFTOTALLOANAMT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPAYMENTTERMDESCR,
				COLUMN_DFPAYMENTTERMDESCR,
				QUALIFIED_COLUMN_DFPAYMENTTERMDESCR,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFESTCLOSINGDATE,
				COLUMN_DFESTCLOSINGDATE,
				QUALIFIED_COLUMN_DFESTCLOSINGDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSPECIALFEATURE,
				COLUMN_DFSPECIALFEATURE,
				QUALIFIED_COLUMN_DFSPECIALFEATURE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		//-- ========== SCR#859 begins ========== --//
		//-- by Neil on Feb 15, 2005
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSERVICINGMORTGAGENUMBER,
				COLUMN_DFSERVICINGMORTGAGENUMBER,
				QUALIFIED_COLUMN_DFSERVICINGMORTGAGENUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		//-- ========== SCR#859 ends ========== --//
//		 ***** Change by NBC Impl. Team - Version 1.2 - Start *****//
		FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
				FIELD_DFCASHBACKINDOLLARS, COLUMN_DFCASHBACKINDOLLARS,
				QUALIFIED_COLUMN_DFCASHBACKINDOLLARS,
				java.math.BigDecimal.class, false, false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

		FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
				FIELD_DFCASHBACKINPERCENTAGE,// logicalName
				COLUMN_DFCASHBACKINPERCENTAGE,// columnName
				QUALIFIED_COLUMN_DFCASHBACKINPERCENTAGE,// qualifiedColumnName
				java.math.BigDecimal.class,// fieldClass
				false,// isKey
				false,// isComputedField
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, // insertValueSource
																		// The
																		// application
																		// provides
																		// the
																		// inserted
																		// value
																		// (default)
				"",// insertFormula
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, // onEmptyValuePolicy
																// If no value
																// is provided,
																// don't supply
																// a value
																// (default)
				"")); // emptyFormula

		FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
				FIELD_DFCASHBACKOVERRIDEINDOLLARS,
				COLUMN_DFCASHBACKOVERRIDEINDOLLARS,
				QUALIFIED_COLUMN_DFCASHBACKOVERRIDEINDOLLARS, String.class,
				false, false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

		// ***** Change by NBC Impl. Team - Version 1.2 - End*****//
    FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
          FIELD_DFINSTITUTIONID,
          COLUMN_DFINSTITUTIONID,
          QUALIFIED_COLUMN_DFINSTITUTIONID,
          java.math.BigDecimal.class,
          false,
          false,
          QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
          "",
          QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
          ""));
	}

  public BigDecimal getDfInstitutionID() {

    return (java.math.BigDecimal)getValue(FIELD_DFINSTITUTIONID);
  }


  public void setDfInstitutionID(BigDecimal value) {

    setValue(FIELD_DFINSTITUTIONID,value);
  }

}

