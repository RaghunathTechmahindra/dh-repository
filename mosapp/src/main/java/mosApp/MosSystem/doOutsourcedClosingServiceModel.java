/*
 * @(#)doOutsourcedClosingServiceModel.java    2006-1-19
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package mosApp.MosSystem;

import java.math.BigDecimal;
import java.sql.Timestamp;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 * doOutsourcedClosingServiceModel - is the definition of model for outsourced
 * closing service.
 *
 * @version   1.0 2006-1-19
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface doOutsourcedClosingServiceModel
    extends QueryModel, SelectQueryModel {

    /**
     * fields bound name definition.
     */
    public final static String FIELD_DFOCSDEALID =
        "dfOCSDealId";
    public final static String FIELD_DFOCSCOPYID =
        "dfOCSCopyId";
    
    public final static String FIELD_DFOCSPROVIDERID =
        "dfOCSProviderId";
    public final static String FIELD_DFOCSPRODUCTID =
        "dfOCSProductId";

    public final static String FIELD_DFOCSREQUESTREFNUM =
        "dfOCSRequestRefNum";

    public final static String FIELD_DFOCSREQUESTSTATUS =
        "dfOCSRequestStatus";
	public final static String FIELD_DFOCSREQUESTSTATUSID =
        "dfOCSRequestStatusId";

    public final static String FIELD_DFOCSREQUESTSTATUSMSG =
        "dfOCSRequestStatusMsg";
    public final static String FIELD_DFOCSREQUESTSTATUSDATE =
        "dfOCSRequestStatusDate";

    public final static String FIELD_DFOCSREQUESTID =
        "dfOCSRequestId";

    /**
     * getter and setter definition.
     */
    
    
    
    public BigDecimal getDfOCSProviderId();
    public void setDfOCSProviderId(BigDecimal id);
    
    public BigDecimal getDfOCSProductId();
    public void setDfOCSProductId(BigDecimal id);

    public String getDfOCSRequestRefNum();
    public void setDfOCSRequestRefNum(String num);
    

    public String getDfOCSRequestStatus();
    public void setDfOCSRequestStatus(String status);

    public BigDecimal getDfOCSRequestStatusId();
    public void setDfOCSRequestStatusId(BigDecimal statusId);

    public String getDfOCSRequestStatusMsg();
    public void setDfOCSRequestStatusMsg(String msg);

    public Timestamp getDfOCSRequestStatusDate();
    public void setDfOCSRequestStatusDate(Timestamp date);
    
    public BigDecimal getDfOCSRequestId();
    public void setDfOCSRequestId(BigDecimal id);
    
    public BigDecimal getDfOCSCopyId();
    public void setDfOCSCopyId(BigDecimal id);
}
