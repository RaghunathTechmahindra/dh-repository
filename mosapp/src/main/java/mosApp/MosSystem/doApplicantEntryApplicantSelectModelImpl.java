package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 * <p>
 * Title: doApplicantEntryApplicantSelectModelImpl
 * </p>
 *
 * <p>
 * Description: class implmenting model for Applicant Entry screen
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * 
 * <p>
 * Company: Filogix Inc.
 * </p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.1
 * Change: added getters & setters for following fields <br>
 * 		<p> smoke status, solicitation, employee number, preferred contact method
 *	Added methods for <br>
 *	- Added following methods <br>
 *		getDfSmokeStatusId()
 *		setDfSmokeStatusId(java.math.BigDecimal value)
 *		getDfSolicitationId()
 *		setDfSolicitationId(java.math.BigDecimal value)
 *		getDfEmployeeNumber ()
 *		setDfEmployeeNumber(String value)
 *		getDfPrefContactMethodId ()
 *		setDfPrefContactMethodId(java.math.BigDecimal value)
 *
 */
public class doApplicantEntryApplicantSelectModelImpl extends QueryModelBase
	implements doApplicantEntryApplicantSelectModel
{
	/**
	 *
	 *
	 */
	public doApplicantEntryApplicantSelectModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSalutationId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSALUTATIONID);
	}


	/**
	 *
	 *
	 */
	public void setDfSalutationId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSALUTATIONID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfFirstName()
	{
		return (String)getValue(FIELD_DFFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfFirstName(String value)
	{
		setValue(FIELD_DFFIRSTNAME,value);
	}

  //--DJ_CR201.2--start//
	/**
	 *
	 *
	 */
	public String getDfGuarantorOtherLoans()
	{
		return (String)getValue(FIELD_DFGUARANTOROTHERLOANS);
	}

	/**
	 *
	 *
	 */
	public void setDfGuarantorOtherLoans(String value)
	{
		setValue(FIELD_DFGUARANTOROTHERLOANS,value);
	}
  //--DJ_CR201.2--end//

	/**
	 *
	 *
	 */
	public String getDfMiddleName()
	{
		return (String)getValue(FIELD_DFMIDDLENAME);
	}


	/**
	 *
	 *
	 */
	public void setDfMiddleName(String value)
	{
		setValue(FIELD_DFMIDDLENAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLastName()
	{
		return (String)getValue(FIELD_DFLASTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfLastName(String value)
	{
		setValue(FIELD_DFLASTNAME,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfBirthDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFBIRTHDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfBirthDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFBIRTHDATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMaritalstatusId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMARITALSTATUSID);
	}


	/**
	 *
	 *
	 */
	public void setDfMaritalstatusId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMARITALSTATUSID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSIN()
	{
		return (String)getValue(FIELD_DFSIN);
	}


	/**
	 *
	 *
	 */
	public void setDfSIN(String value)
	{
		setValue(FIELD_DFSIN,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCitizenshipId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCITIZENSHIPID);
	}


	/**
	 *
	 *
	 */
	public void setDfCitizenshipId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCITIZENSHIPID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNoOfDependants()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFNOOFDEPENDANTS);
	}


	/**
	 *
	 *
	 */
	public void setDfNoOfDependants(java.math.BigDecimal value)
	{
		setValue(FIELD_DFNOOFDEPENDANTS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfExistingClient()
	{
		return (String)getValue(FIELD_DFEXISTINGCLIENT);
	}


	/**
	 *
	 *
	 */
	public void setDfExistingClient(String value)
	{
		setValue(FIELD_DFEXISTINGCLIENT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfClientReferenceNumber()
	{
		return (String)getValue(FIELD_DFCLIENTREFERENCENUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfClientReferenceNumber(String value)
	{
		setValue(FIELD_DFCLIENTREFERENCENUMBER,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNoOfBankCrupts()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFNOOFBANKCRUPTS);
	}


	/**
	 *
	 *
	 */
	public void setDfNoOfBankCrupts(java.math.BigDecimal value)
	{
		setValue(FIELD_DFNOOFBANKCRUPTS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBankruptcyStatusId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBANKRUPTCYSTATUSID);
	}


	/**
	 *
	 *
	 */
	public void setDfBankruptcyStatusId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBANKRUPTCYSTATUSID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStaffOfLender()
	{
		return (String)getValue(FIELD_DFSTAFFOFLENDER);
	}


	/**
	 *
	 *
	 */
	public void setDfStaffOfLender(String value)
	{
		setValue(FIELD_DFSTAFFOFLENDER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfHomePhone()
	{
		return (String)getValue(FIELD_DFHOMEPHONE);
	}


	/**
	 *
	 *
	 */
	public void setDfHomePhone(String value)
	{
		setValue(FIELD_DFHOMEPHONE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfWorkPhone()
	{
		return (String)getValue(FIELD_DFWORKPHONE);
	}


	/**
	 *
	 *
	 */
	public void setDfWorkPhone(String value)
	{
		setValue(FIELD_DFWORKPHONE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfWorkPhoneExtension()
	{
		return (String)getValue(FIELD_DFWORKPHONEEXTENSION);
	}


	/**
	 *
	 *
	 */
	public void setDfWorkPhoneExtension(String value)
	{
		setValue(FIELD_DFWORKPHONEEXTENSION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfFaxNumber()
	{
		return (String)getValue(FIELD_DFFAXNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfFaxNumber(String value)
	{
		setValue(FIELD_DFFAXNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmailAddress()
	{
		return (String)getValue(FIELD_DFEMAILADDRESS);
	}


	/**
	 *
	 *
	 */
	public void setDfEmailAddress(String value)
	{
		setValue(FIELD_DFEMAILADDRESS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLanguagePrefId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLANGUAGEPREFID);
	}


	/**
	 *
	 *
	 */
	public void setDfLanguagePrefId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLANGUAGEPREFID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalAsset()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALASSET);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalAsset(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALASSET,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalIncome()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALINCOME);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalIncome(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALINCOME,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalLiab()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALLIAB);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalLiab(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALLIAB,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalLiabPayment()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALLIABPAYMENT);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalLiabPayment(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALLIABPAYMENT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfFirstTimeBuyer()
	{
		return (String)getValue(FIELD_DFFIRSTTIMEBUYER);
	}


	/**
	 *
	 *
	 */
	public void setDfFirstTimeBuyer(String value)
	{
		setValue(FIELD_DFFIRSTTIMEBUYER,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCreditScore()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCREDITSCORE);
	}


	/**
	 *
	 *
	 */
	public void setDfCreditScore(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCREDITSCORE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCreditBureauSummary()
	{
		return (String)getValue(FIELD_DFCREDITBUREAUSUMMARY);
	}


	/**
	 *
	 *
	 */
	public void setDfCreditBureauSummary(String value)
	{
		setValue(FIELD_DFCREDITBUREAUSUMMARY,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCreditBureauNameId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCREDITBUREAUNAMEID);
	}

	/**
	 *
	 *
	 */
	public void setDfCreditBureauNameId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCREDITBUREAUNAMEID,value);
	}

  //--DJ_LDI_CR--start--//
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerGenderId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERGENDERID);
	}

	/**
	 *
	 *
	 */
	public void setDfBorrowerGenderId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERGENDERID, value);
	}
  //--DJ_LDI_CR--end--//

    //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
	/**
	 * <p>returns smoke status id</p>
	 * @return - returns smoke status id
	 */
	public java.math.BigDecimal getDfSmokeStatusId(){
		return (java.math.BigDecimal)getValue(FIELD_DFSMOKESTATUSID);
	}

	/**
	 * <p>sets smoke status id</p>
	 * @param - sets smoke status id
	 */
	public void setDfSmokeStatusId(java.math.BigDecimal value)  {
		  setValue(FIELD_DFSMOKESTATUSID, value);
	  }
	
	/**
	 * <p>returns solicitation id</p>
	 * @return - returns solicitation id
	 */
	public java.math.BigDecimal getDfSolicitationId(){
		return (java.math.BigDecimal)getValue(FIELD_DFSOLICITATIONID);
	}

	/**
	 * <p>sets smoke status id</p>
	 * @param - sets smoke status id
	 */
	public void setDfSolicitationId(java.math.BigDecimal value) {
		  setValue(FIELD_DFSOLICITATIONID, value);
	  }
	 
	/**
	 * <p>returns Employee Number</p>
	 * @return - returns Employee Number
	 */
	public String getDfEmployeeNumber (){
		return (String)getValue(FIELD_DFEMPLOYEENUMBER);
	}

	/**
	 * <p>sets Employee Number</p>
	 * @param - sets Employee Number
	 */
	public void setDfEmployeeNumber(String value) {
		  setValue(FIELD_DFEMPLOYEENUMBER, value);
	  } 

	/**
	 * <p>returns preferred method contact id</p>
	 * @return - returns preferred method contact id
	 */
	public java.math.BigDecimal getDfPrefContactMethodId(){
		return (java.math.BigDecimal)getValue(FIELD_DFPREFCONTACTMETHODID);
	}

	/**
	 * <p>sets preferred method contact id</p>
	 * @param - sets preferred method contact id
	 */
	public void setDfPrefContactMethodId(java.math.BigDecimal value) {
		  setValue(FIELD_DFPREFCONTACTMETHODID, value);
	  }

    
    /**
     * <p>returns suffix id</p>
     * @return - returns suffix id
     */
    public java.math.BigDecimal getDfSuffixId(){
        return (java.math.BigDecimal)getValue(FIELD_DFSUFFIXID);
    }

    /**
     * <p>sets suffix id</p>
     * @param - sets suffix id
     */
    public void setDfSuffixId(java.math.BigDecimal value) {
          setValue(FIELD_DFSUFFIXID, value);
    }
	
    /**
     *
     *
     */
    public String getDfCellNumber()
    {
        return (String)getValue(FIELD_DFCELLNUMBER);
    }


    /**
     *
     *
     */
    public void setDfCellNumber(String value)
    {
        setValue(FIELD_DFCELLNUMBER,value);
    }
    
    //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////



	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";

  //--DJ_LDI_CR--start--//
  // A new combobox Applicant Gender is added.
	public static final String SELECT_SQL_TEMPLATE
        ="SELECT DISTINCT BORROWER.DEALID, BORROWER.BORROWERID, " +
        "BORROWER.COPYID, BORROWER.SALUTATIONID, " +
        "BORROWER.BORROWERFIRSTNAME, BORROWER.BORROWERMIDDLEINITIAL, " +
        "BORROWER.BORROWERLASTNAME, BORROWER.BORROWERTYPEID, " +
        "BORROWER.BORROWERBIRTHDATE, BORROWER.MARITALSTATUSID, " +
        "BORROWER.SOCIALINSURANCENUMBER, BORROWER.CITIZENSHIPTYPEID, " +
        "BORROWER.NUMBEROFDEPENDENTS, BORROWER.EXISTINGCLIENT, " +
        "BORROWER.CLIENTREFERENCENUMBER, BORROWER.NUMBEROFTIMESBANKRUPT, " +
        "BORROWER.BANKRUPTCYSTATUSID, BORROWER.STAFFOFLENDER, " +
        "BORROWER.BORROWERHOMEPHONENUMBER, BORROWER.BORROWERWORKPHONENUMBER, " +
        "BORROWER.BORROWERWORKPHONEEXTENSION, BORROWER.BORROWERFAXNUMBER, " +
        "BORROWER.BORROWEREMAILADDRESS, BORROWER.LANGUAGEPREFERENCEID, " +
        "BORROWER.TOTALASSETAMOUNT, BORROWER.TOTALINCOMEAMOUNT, " +
        "BORROWER.TOTALLIABILITYAMOUNT, BORROWER.TOTALLIABILITYPAYMENTS, " +
        "BORROWER.FIRSTTIMEBUYER, BORROWER.CREDITSCORE, " +
        "BORROWER.CREDITBUREAUSUMMARY, " +
        "BORROWER.CREDITBUREAUNAMEID, BORROWER.BORROWERGENDERID, " +
        //--DJ_CR201.2--start//
        "BORROWER.GUARANTOROTHERLOANS, " +
        //--DJ_CR201.2--end//
        //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
        "BORROWER.SMOKESTATUSID, BORROWER.SOLICITATIONID, " +
        "BORROWER.EMPLOYEENUMBER, BORROWER.PREFCONTACTMETHODID, " +
        //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
        "BORROWER.SUFFIXID, BORROWER.BORROWERCELLPHONENUMBER " +
        "FROM BORROWER  __WHERE__  ";
  //--DJ_LDI_CR--end--//

	public static final String MODIFYING_QUERY_TABLE_NAME="BORROWER";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="BORROWER.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFBORROWERID="BORROWER.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="BORROWER.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFSALUTATIONID="BORROWER.SALUTATIONID";
	public static final String COLUMN_DFSALUTATIONID="SALUTATIONID";
	public static final String QUALIFIED_COLUMN_DFFIRSTNAME="BORROWER.BORROWERFIRSTNAME";
	public static final String COLUMN_DFFIRSTNAME="BORROWERFIRSTNAME";
	public static final String QUALIFIED_COLUMN_DFMIDDLENAME="BORROWER.BORROWERMIDDLEINITIAL";
	public static final String COLUMN_DFMIDDLENAME="BORROWERMIDDLEINITIAL";
	public static final String QUALIFIED_COLUMN_DFLASTNAME="BORROWER.BORROWERLASTNAME";
	public static final String COLUMN_DFLASTNAME="BORROWERLASTNAME";
	public static final String QUALIFIED_COLUMN_DFBORROWERTYPEID="BORROWER.BORROWERTYPEID";
	public static final String COLUMN_DFBORROWERTYPEID="BORROWERTYPEID";
	public static final String QUALIFIED_COLUMN_DFBIRTHDATE="BORROWER.BORROWERBIRTHDATE";
	public static final String COLUMN_DFBIRTHDATE="BORROWERBIRTHDATE";
	public static final String QUALIFIED_COLUMN_DFMARITALSTATUSID="BORROWER.MARITALSTATUSID";
	public static final String COLUMN_DFMARITALSTATUSID="MARITALSTATUSID";
	public static final String QUALIFIED_COLUMN_DFSIN="BORROWER.SOCIALINSURANCENUMBER";
	public static final String COLUMN_DFSIN="SOCIALINSURANCENUMBER";
	public static final String QUALIFIED_COLUMN_DFCITIZENSHIPID="BORROWER.CITIZENSHIPTYPEID";
	public static final String COLUMN_DFCITIZENSHIPID="CITIZENSHIPTYPEID";
	public static final String QUALIFIED_COLUMN_DFNOOFDEPENDANTS="BORROWER.NUMBEROFDEPENDENTS";
	public static final String COLUMN_DFNOOFDEPENDANTS="NUMBEROFDEPENDENTS";
	public static final String QUALIFIED_COLUMN_DFEXISTINGCLIENT="BORROWER.EXISTINGCLIENT";
	public static final String COLUMN_DFEXISTINGCLIENT="EXISTINGCLIENT";
	public static final String QUALIFIED_COLUMN_DFCLIENTREFERENCENUMBER="BORROWER.CLIENTREFERENCENUMBER";
	public static final String COLUMN_DFCLIENTREFERENCENUMBER="CLIENTREFERENCENUMBER";
	public static final String QUALIFIED_COLUMN_DFNOOFBANKCRUPTS="BORROWER.NUMBEROFTIMESBANKRUPT";
	public static final String COLUMN_DFNOOFBANKCRUPTS="NUMBEROFTIMESBANKRUPT";
	public static final String QUALIFIED_COLUMN_DFBANKRUPTCYSTATUSID="BORROWER.BANKRUPTCYSTATUSID";
	public static final String COLUMN_DFBANKRUPTCYSTATUSID="BANKRUPTCYSTATUSID";
	public static final String QUALIFIED_COLUMN_DFSTAFFOFLENDER="BORROWER.STAFFOFLENDER";
	public static final String COLUMN_DFSTAFFOFLENDER="STAFFOFLENDER";
	public static final String QUALIFIED_COLUMN_DFHOMEPHONE="BORROWER.BORROWERHOMEPHONENUMBER";
	public static final String COLUMN_DFHOMEPHONE="BORROWERHOMEPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFWORKPHONE="BORROWER.BORROWERWORKPHONENUMBER";
	public static final String COLUMN_DFWORKPHONE="BORROWERWORKPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFWORKPHONEEXTENSION="BORROWER.BORROWERWORKPHONEEXTENSION";
	public static final String COLUMN_DFWORKPHONEEXTENSION="BORROWERWORKPHONEEXTENSION";
	public static final String QUALIFIED_COLUMN_DFFAXNUMBER="BORROWER.BORROWERFAXNUMBER";
	public static final String COLUMN_DFFAXNUMBER="BORROWERFAXNUMBER";
	public static final String QUALIFIED_COLUMN_DFEMAILADDRESS="BORROWER.BORROWEREMAILADDRESS";
	public static final String COLUMN_DFEMAILADDRESS="BORROWEREMAILADDRESS";
	public static final String QUALIFIED_COLUMN_DFLANGUAGEPREFID="BORROWER.LANGUAGEPREFERENCEID";
	public static final String COLUMN_DFLANGUAGEPREFID="LANGUAGEPREFERENCEID";
	public static final String QUALIFIED_COLUMN_DFTOTALASSET="BORROWER.TOTALASSETAMOUNT";
	public static final String COLUMN_DFTOTALASSET="TOTALASSETAMOUNT";
	public static final String QUALIFIED_COLUMN_DFTOTALINCOME="BORROWER.TOTALINCOMEAMOUNT";
	public static final String COLUMN_DFTOTALINCOME="TOTALINCOMEAMOUNT";
	public static final String QUALIFIED_COLUMN_DFTOTALLIAB="BORROWER.TOTALLIABILITYAMOUNT";
	public static final String COLUMN_DFTOTALLIAB="TOTALLIABILITYAMOUNT";
	public static final String QUALIFIED_COLUMN_DFTOTALLIABPAYMENT="BORROWER.TOTALLIABILITYPAYMENTS";
	public static final String COLUMN_DFTOTALLIABPAYMENT="TOTALLIABILITYPAYMENTS";
	public static final String QUALIFIED_COLUMN_DFFIRSTTIMEBUYER="BORROWER.FIRSTTIMEBUYER";
	public static final String COLUMN_DFFIRSTTIMEBUYER="FIRSTTIMEBUYER";
	public static final String QUALIFIED_COLUMN_DFCREDITSCORE="BORROWER.CREDITSCORE";
	public static final String COLUMN_DFCREDITSCORE="CREDITSCORE";
	public static final String QUALIFIED_COLUMN_DFCREDITBUREAUSUMMARY="BORROWER.CREDITBUREAUSUMMARY";
	public static final String COLUMN_DFCREDITBUREAUSUMMARY="CREDITBUREAUSUMMARY";
	public static final String QUALIFIED_COLUMN_DFCREDITBUREAUNAMEID="BORROWER.CREDITBUREAUNAMEID";
	public static final String COLUMN_DFCREDITBUREAUNAMEID="CREDITBUREAUNAMEID";

  //--DJ_LDI_CR--start--//
	public static final String QUALIFIED_COLUMN_DFBORROWERGENDERID="BORROWER.BORROWERGENDERID";
	public static final String COLUMN_DFBORROWERGENDERID="BORROWERGENDERID";
  //--DJ_LDI_CR--end--//

  //--DJ_CR201.2--start//
  public static final String QUALIFIED_COLUMN_DFGUARANTOROTHERLOANS="BORROWER.BORROWER.GUARANTOROTHERLOANS";
	public static final String COLUMN_DFGUARANTOROTHERLOANS="GUARANTOROTHERLOANS";
  //--DJ_CR201.2--end//

    //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
	public static final String QUALIFIED_COLUMN_DFSMOKESTATUSID = "BORROWER.SMOKESTATUSID";
	public static final String COLUMN_DFSMOKESTATUSID = "SMOKESTATUSID";

	public static final String QUALIFIED_COLUMN_DFSOLICITATIONID = "BORROWER.SOLICITATIONID";
	public static final String COLUMN_DFSOLICITATIONID = "SOLICITATIONID";

	public static final String QUALIFIED_COLUMN_DFEMPLOYEENUMBER = "BORROWER.EMPLOYEENUMBER";
	public static final String COLUMN_DFEMPLOYEENUMBER = "EMPLOYEENUMBER";

	public static final String QUALIFIED_COLUMN_DFPREFCONTACTMETHODID = "BORROWER.PREFCONTACTMETHODID";
	public static final String COLUMN_DFPREFCONTACTMETHODID = "PREFCONTACTMETHODID";

    //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
    
    public static final String QUALIFIED_COLUMN_DFSUFFIXID="BORROWER.SUFFIXID";
    public static final String COLUMN_DFSUFFIXID="SUFFIXID";
    
    public static final String QUALIFIED_COLUMN_DFCELLNUMBER="BORROWER.BORROWERCELLPHONENUMBER";
    public static final String COLUMN_DFCELLNUMBER="BORROWERCELLPHONENUMBER";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				true,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSALUTATIONID,
				COLUMN_DFSALUTATIONID,
				QUALIFIED_COLUMN_DFSALUTATIONID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFIRSTNAME,
				COLUMN_DFFIRSTNAME,
				QUALIFIED_COLUMN_DFFIRSTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIDDLENAME,
				COLUMN_DFMIDDLENAME,
				QUALIFIED_COLUMN_DFMIDDLENAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLASTNAME,
				COLUMN_DFLASTNAME,
				QUALIFIED_COLUMN_DFLASTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERTYPEID,
				COLUMN_DFBORROWERTYPEID,
				QUALIFIED_COLUMN_DFBORROWERTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBIRTHDATE,
				COLUMN_DFBIRTHDATE,
				QUALIFIED_COLUMN_DFBIRTHDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMARITALSTATUSID,
				COLUMN_DFMARITALSTATUSID,
				QUALIFIED_COLUMN_DFMARITALSTATUSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSIN,
				COLUMN_DFSIN,
				QUALIFIED_COLUMN_DFSIN,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCITIZENSHIPID,
				COLUMN_DFCITIZENSHIPID,
				QUALIFIED_COLUMN_DFCITIZENSHIPID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNOOFDEPENDANTS,
				COLUMN_DFNOOFDEPENDANTS,
				QUALIFIED_COLUMN_DFNOOFDEPENDANTS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEXISTINGCLIENT,
				COLUMN_DFEXISTINGCLIENT,
				QUALIFIED_COLUMN_DFEXISTINGCLIENT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCLIENTREFERENCENUMBER,
				COLUMN_DFCLIENTREFERENCENUMBER,
				QUALIFIED_COLUMN_DFCLIENTREFERENCENUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNOOFBANKCRUPTS,
				COLUMN_DFNOOFBANKCRUPTS,
				QUALIFIED_COLUMN_DFNOOFBANKCRUPTS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBANKRUPTCYSTATUSID,
				COLUMN_DFBANKRUPTCYSTATUSID,
				QUALIFIED_COLUMN_DFBANKRUPTCYSTATUSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTAFFOFLENDER,
				COLUMN_DFSTAFFOFLENDER,
				QUALIFIED_COLUMN_DFSTAFFOFLENDER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFHOMEPHONE,
				COLUMN_DFHOMEPHONE,
				QUALIFIED_COLUMN_DFHOMEPHONE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFWORKPHONE,
				COLUMN_DFWORKPHONE,
				QUALIFIED_COLUMN_DFWORKPHONE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFWORKPHONEEXTENSION,
				COLUMN_DFWORKPHONEEXTENSION,
				QUALIFIED_COLUMN_DFWORKPHONEEXTENSION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFAXNUMBER,
				COLUMN_DFFAXNUMBER,
				QUALIFIED_COLUMN_DFFAXNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMAILADDRESS,
				COLUMN_DFEMAILADDRESS,
				QUALIFIED_COLUMN_DFEMAILADDRESS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLANGUAGEPREFID,
				COLUMN_DFLANGUAGEPREFID,
				QUALIFIED_COLUMN_DFLANGUAGEPREFID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALASSET,
				COLUMN_DFTOTALASSET,
				QUALIFIED_COLUMN_DFTOTALASSET,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALINCOME,
				COLUMN_DFTOTALINCOME,
				QUALIFIED_COLUMN_DFTOTALINCOME,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALLIAB,
				COLUMN_DFTOTALLIAB,
				QUALIFIED_COLUMN_DFTOTALLIAB,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALLIABPAYMENT,
				COLUMN_DFTOTALLIABPAYMENT,
				QUALIFIED_COLUMN_DFTOTALLIABPAYMENT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFIRSTTIMEBUYER,
				COLUMN_DFFIRSTTIMEBUYER,
				QUALIFIED_COLUMN_DFFIRSTTIMEBUYER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCREDITSCORE,
				COLUMN_DFCREDITSCORE,
				QUALIFIED_COLUMN_DFCREDITSCORE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCREDITBUREAUSUMMARY,
				COLUMN_DFCREDITBUREAUSUMMARY,
				QUALIFIED_COLUMN_DFCREDITBUREAUSUMMARY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCREDITBUREAUNAMEID,
				COLUMN_DFCREDITBUREAUNAMEID,
				QUALIFIED_COLUMN_DFCREDITBUREAUNAMEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    //--DJ_LDI_CR--start--//
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERGENDERID,
				COLUMN_DFBORROWERGENDERID,
				QUALIFIED_COLUMN_DFBORROWERGENDERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
     //--DJ_LDI_CR--end--//

     //--DJ_CR201.2--start//
  		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFGUARANTOROTHERLOANS,
				COLUMN_DFGUARANTOROTHERLOANS,
				QUALIFIED_COLUMN_DFGUARANTOROTHERLOANS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
     //--DJ_CR201.2--end//
	
  	    //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFSMOKESTATUSID,
					COLUMN_DFSMOKESTATUSID,
					QUALIFIED_COLUMN_DFSMOKESTATUSID,
					java.math.BigDecimal.class,
					true, // Foreign key
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
	
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFSOLICITATIONID,
					COLUMN_DFSOLICITATIONID,
					QUALIFIED_COLUMN_DFSOLICITATIONID,
					java.math.BigDecimal.class,
					true, // Foreign key
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
	
		
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFEMPLOYEENUMBER,
					COLUMN_DFEMPLOYEENUMBER,
					QUALIFIED_COLUMN_DFEMPLOYEENUMBER,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
	
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFPREFCONTACTMETHODID,						
					COLUMN_DFPREFCONTACTMETHODID,
					QUALIFIED_COLUMN_DFPREFCONTACTMETHODID,
					java.math.BigDecimal.class,
					true, // Foreign key
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
	
	    //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFSUFFIXID,
                    COLUMN_DFSUFFIXID,
                    QUALIFIED_COLUMN_DFSUFFIXID,
                    java.math.BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""));
        
        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFCELLNUMBER,
                    COLUMN_DFCELLNUMBER,
                    QUALIFIED_COLUMN_DFCELLNUMBER,
                    String.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""));
	}
}

