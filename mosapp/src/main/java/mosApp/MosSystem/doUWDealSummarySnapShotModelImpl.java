package mosApp.MosSystem;


import java.math.BigDecimal;
import java.sql.SQLException;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 * @author MCM Impl Team
 * Change Log:
 * @version 1.1 01-July-2008 XS_2.25 Added Mipremium,CombinedLTV,TaxEscrow,LOB fields.
 * @version 1.2 22-Aug-2008 SQL Changed as part of artf761745 
 */
public class doUWDealSummarySnapShotModelImpl extends QueryModelBase
	implements doUWDealSummarySnapShotModel
{
	/**
	 *
	 *
	 */
	public doUWDealSummarySnapShotModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *@version 1.1 03-July-2008 XS_2.25 Added picklist description for the Product name and Line of Business (LOB).
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    ////To override all the Description fields by populating from BXResource.
    ////Get the Language ID from the SessionStateModel.
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);

    SessionStateModelImpl theSessionState =
                                          (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                           SessionStateModel.class,
                                           defaultInstanceStateName,
                                           true);

    int languageId = theSessionState.getLanguageId();
    int institutionId = theSessionState.getDealInstitutionId();
    if (institutionId == 0)
    {
        institutionId = theSessionState.getUserInstitutionId();
    }

    //Browser all returned results and convert all Description by Resource Bundle
    while(this.next())
    {
      // Convert DealStatus Desc.
      this.setDfStatusDescr(BXResources.getPickListDescription(institutionId,"STATUS", this.getDfStatusDescr(), languageId));
      // Convert DealType Desc.
      this.setDfDealTypeDescr(BXResources.getPickListDescription(institutionId,"DEALTYPE", this.getDfDealTypeDescr(), languageId));
      // Convert DealPurpose Desc.
      this.setDfLoanPurposeDescr(BXResources.getPickListDescription(institutionId,"DEALPURPOSE", this.getDfLoanPurposeDescr(), languageId));
      // Convert PaymentTerm Desc.
      this.setDfPaymentTermDescr(BXResources.getPickListDescription(institutionId,"PAYMENTTERM", this.getDfPaymentTermDescr(), languageId));
      // Convert SpecialFeature Desc.
      this.setDfSpecialFeature(BXResources.getPickListDescription(institutionId,"SPECIALFEATURE", this.getDfSpecialFeature(), languageId));
      /************************MCM Impl Team XS_2.25 changes starts**************************************************************/
      this.setDfProductName(BXResources.getPickListDescription(institutionId,"MTGPROD", this.getDfProductName(), languageId));
      this.setDfLOB(BXResources.getPickListDescription(institutionId,"LINEOFBUSINESS", this.getDfLOB(), languageId));
      /************************MCM Impl Team XS_2.25 changes ends*****************************************************************/
    }

    //Reset Location
    this.beforeFirst();

	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public String getDfApplicationID()
	{
		return (String)getValue(FIELD_DFAPPLICATIONID);
	}


	/**
	 *
	 *
	 */
	public void setDfApplicationID(String value)
	{
		setValue(FIELD_DFAPPLICATIONID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStatusDescr()
	{
		return (String)getValue(FIELD_DFSTATUSDESCR);
	}


	/**
	 *
	 *
	 */
	public void setDfStatusDescr(String value)
	{
		setValue(FIELD_DFSTATUSDESCR,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfStatusDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFSTATUSDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfStatusDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFSTATUSDATE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSFShortName()
	{
		return (String)getValue(FIELD_DFSFSHORTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfSFShortName(String value)
	{
		setValue(FIELD_DFSFSHORTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSOBPShortName()
	{
		return (String)getValue(FIELD_DFSOBPSHORTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfSOBPShortName(String value)
	{
		setValue(FIELD_DFSOBPSHORTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDealTypeDescr()
	{
		return (String)getValue(FIELD_DFDEALTYPEDESCR);
	}


	/**
	 *
	 *
	 */
	public void setDfDealTypeDescr(String value)
	{
		setValue(FIELD_DFDEALTYPEDESCR,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLoanPurposeDescr()
	{
		return (String)getValue(FIELD_DFLOANPURPOSEDESCR);
	}


	/**
	 *
	 *
	 */
	public void setDfLoanPurposeDescr(String value)
	{
		setValue(FIELD_DFLOANPURPOSEDESCR,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalPurchasePrice()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalPurchasePrice(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALPURCHASEPRICE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalLoanAmt()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALLOANAMT);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalLoanAmt(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALLOANAMT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPaymentTermDescr()
	{
		return (String)getValue(FIELD_DFPAYMENTTERMDESCR);
	}


	/**
	 *
	 *
	 */
	public void setDfPaymentTermDescr(String value)
	{
		setValue(FIELD_DFPAYMENTTERMDESCR,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfEstClosingDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFESTCLOSINGDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfEstClosingDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFESTCLOSINGDATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSpecialFeature()
	{
		return (String)getValue(FIELD_DFSPECIALFEATURE);
	}


	/**
	 *
	 *
	 */
	public void setDfSpecialFeature(String value)
	{
		setValue(FIELD_DFSPECIALFEATURE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfUnderwriterFirstName()
	{
		return (String)getValue(FIELD_DFUNDERWRITERFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfUnderwriterFirstName(String value)
	{
		setValue(FIELD_DFUNDERWRITERFIRSTNAME,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfApplicationDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFAPPLICATIONDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfApplicationDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFAPPLICATIONDATE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfUnderwriterMiddleInitial()
	{
		return (String)getValue(FIELD_DFUNDERWRITERMIDDLEINITIAL);
	}


	/**
	 *
	 *
	 */
	public void setDfUnderwriterMiddleInitial(String value)
	{
		setValue(FIELD_DFUNDERWRITERMIDDLEINITIAL,value);
	}


	/**
	 *
	 *
	 */
	public String getDfUnderwriterLastName()
	{
		return (String)getValue(FIELD_DFUNDERWRITERLASTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfUnderwriterLastName(String value)
	{
		setValue(FIELD_DFUNDERWRITERLASTNAME,value);
	}

	//-- ========== SCR#859 begins ========== --//
	//-- by Neil on Feb 14, 2005
	public String getDfServicingMortgageNumber() {
		return (String)getValue(FIELD_DFSERVICINGMORTGAGENUMBER);	
	}
	
	public void setDfServicingMortgageNumber(String value) {
		setValue(FIELD_DFSERVICINGMORTGAGENUMBER, value);
	}
	//-- ========== SCR#859 ends ========== --//

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //// 1. Replace all Descriptions from the tables included into the PickList with
  //// their joins' counterparts in the SQL_TEMPLATE.
  //// 2. Convert all these new fragments (IDs) into the string format.
  //// 3. Remove all joins which include the tables from the PickList.
  //// 4. Remove all PickList tables from the SQL_TEMPLATE.
  //// 5. Adjust Field names definitions to string format.

  public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //// 1. Replace all Descriptions from the tables included into the PickList with
  //// their joins' counterparts in the SQL_TEMPLATE.
  //// 2. Convert all these new fragments (IDs) into the string format.
	////public static final String SELECT_SQL_TEMPLATE="SELECT ALL DEAL.APPLICATIONID, STATUS.STATUSDESCRIPTION,
  ////DEAL.STATUSDATE, SOURCEFIRMPROFILE.SFSHORTNAME, SOURCEOFBUSINESSPROFILE.SOBPSHORTNAME,
  ////DEALTYPE.DTDESCRIPTION, dealpurpose.DPDESCRIPTION, DEAL.TOTALPURCHASEPRICE,
  ////DEAL.TOTALLOANAMOUNT, PAYMENTTERM.PTDESCRIPTION, DEAL.ESTIMATEDCLOSINGDATE,
  ////DEAL.DEALID, SPECIALFEATURE.SFDESCRIPTION, DEAL.COPYID, CONTACT.CONTACTFIRSTNAME,
  ////DEAL.APPLICATIONDATE, CONTACT.CONTACTMIDDLEINITIAL, CONTACT.CONTACTLASTNAME
  ////FROM DEAL, STATUS, SOURCEFIRMPROFILE, SOURCEOFBUSINESSPROFILE, LINEOFBUSINESS,
  ////PAYMENTTERM, DEALTYPE, dealpurpose, SPECIALFEATURE, CONTACT, USERPROFILE  __WHERE__  ";

	public static final String SELECT_SQL_TEMPLATE="SELECT distinct DEAL.APPLICATIONID, " +
  "to_char(DEAL.STATUSID) STATUSID_STR, DEAL.STATUSDATE, " +
  "SOURCEFIRMPROFILE.SFSHORTNAME, " +
  "SOURCEOFBUSINESSPROFILE.SOBPSHORTNAME, " +
  "to_char(DEAL.DEALTYPEID) DEALTYPEID_STR, to_char(DEAL.DEALPURPOSEID) DEALPURPOSEID_STR, " +
  "DEAL.TOTALPURCHASEPRICE, " +
  "DEAL.TOTALLOANAMOUNT, to_char(DEAL.PAYMENTTERMID) PAYMENTTERMID_STR, " +
  "DEAL.ESTIMATEDCLOSINGDATE, DEAL.DEALID, " +
  "to_char(DEAL.SPECIALFEATUREID) SPECIALFEATUREID_STR, DEAL.COPYID, CONTACT.CONTACTFIRSTNAME, " +
  "DEAL.APPLICATIONDATE, CONTACT.CONTACTMIDDLEINITIAL, " +
  //-- ========== SCR#859 begins ========== --//
  //-- by Neil on Feb 11, 2005
  "DEAL.SERVICINGMORTGAGENUMBER, " +
  //-- ========== SCR#859 ends ========== --//
  "CONTACT.CONTACTLASTNAME, " +
  /**************MCM Impl Team XS_2.25 changes starts *************/
  " DEAL.COMBINEDLTV, " +
  " to_char(DEAL.LINEOFBUSINESSID) LINEOFBUSINESSID_STR, " +
  " to_char(DEAL.MTGPRODID) MTGPRODID_STR, " +
  " DEAL.MIPREMIUMAMOUNT, " +
  " (SELECT sum(ESCROWPAYMENTAMOUNT) FROM ESCROWPAYMENT " +
  " WHERE ESCROWPAYMENT.INSTITUTIONPROFILEID=DEAL.INSTITUTIONPROFILEID " +
  " AND ESCROWPAYMENT.DEALID=DEAL.DEALID " +
  " AND ESCROWPAYMENT.COPYID=DEAL.COPYID " +
  " AND ESCROWPAYMENT.ESCROWTYPEID=0 ) ESCROWPAYMENTAMOUNT_VALUE  "  +
  /***************MCM Impl Team XS_2.25 changes ends**************/
  " FROM DEAL, " +
  "SOURCEFIRMPROFILE, SOURCEOFBUSINESSPROFILE, " +
  "CONTACT, USERPROFILE  __WHERE__  ";

  //--Release2.1--//
  //// 4. Remove all PickList tables from the SQL_TEMPLATE.
  ////public static final String MODIFYING_QUERY_TABLE_NAME="DEAL, STATUS, SOURCEFIRMPROFILE, SOURCEOFBUSINESSPROFILE, LINEOFBUSINESS, PAYMENTTERM, DEALTYPE, dealpurpose, SPECIALFEATURE, CONTACT, USERPROFILE";
	public static final String MODIFYING_QUERY_TABLE_NAME="DEAL, SOURCEFIRMPROFILE, " +
  "SOURCEOFBUSINESSPROFILE, CONTACT, USERPROFILE";

  //--Release2.1--//
  //// 3. Remove all joins which include the tables from the PickList.
	////public static final String STATIC_WHERE_CRITERIA=" (DEAL.STATUSID  =  STATUS.STATUSID) AND (DEAL.SOURCEFIRMPROFILEID  =  SOURCEFIRMPROFILE.SOURCEFIRMPROFILEID) AND (DEAL.SOURCEOFBUSINESSPROFILEID  =  SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPROFILEID) AND (DEAL.LINEOFBUSINESSID  =  LINEOFBUSINESS.LINEOFBUSINESSID) AND (DEAL.PAYMENTTERMID  =  PAYMENTTERM.PAYMENTTERMID) AND (DEAL.DEALTYPEID  =  DEALTYPE.DEALTYPEID) AND (DEAL.dealpurposeID  =  dealpurpose.dealpurposeID) AND (DEAL.SPECIALFEATUREID  =  SPECIALFEATURE.SPECIALFEATUREID) AND (DEAL.UNDERWRITERUSERID  =  USERPROFILE.USERPROFILEID) AND (CONTACT.CONTACTID  =  USERPROFILE.CONTACTID)";

	public static final String STATIC_WHERE_CRITERIA = 
	    "deal.institutionprofileid = sourcefirmprofile.institutionprofileid(+) " +       
        "AND deal.sourcefirmprofileid = sourcefirmprofile.sourcefirmprofileid(+) " +  
        "AND deal.institutionprofileid = sourceofbusinessprofile.institutionprofileid(+) " +             
        "AND deal.sourceofbusinessprofileid = sourceofbusinessprofile.sourceofbusinessprofileid(+) " +
        "AND deal.sourcefirmprofileid = sourceofbusinessprofile.sourcefirmprofileid(+) " +
        "AND deal.institutionprofileid = userprofile.institutionprofileid " +                
        "AND deal.underwriteruserid = userprofile.userprofileid " +
        "AND contact.institutionprofileid = userprofile.institutionprofileid " +
        "AND contact.contactid = userprofile.contactid ";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFAPPLICATIONID="DEAL.APPLICATIONID";
	public static final String COLUMN_DFAPPLICATIONID="APPLICATIONID";

  //--Release2.1--//
  //// 5. Adjust Field names definitions to string format.
	////public static final String QUALIFIED_COLUMN_DFSTATUSDESCR="STATUS.STATUSDESCRIPTION";
	////public static final String COLUMN_DFSTATUSDESCR="STATUSDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFSTATUSDESCR="DEAL.STATUSID_STR";
  public static final String COLUMN_DFSTATUSDESCR="STATUSID_STR";

	public static final String QUALIFIED_COLUMN_DFSTATUSDATE="DEAL.STATUSDATE";
	public static final String COLUMN_DFSTATUSDATE="STATUSDATE";
	public static final String QUALIFIED_COLUMN_DFSFSHORTNAME="SOURCEFIRMPROFILE.SFSHORTNAME";
	public static final String COLUMN_DFSFSHORTNAME="SFSHORTNAME";
	public static final String QUALIFIED_COLUMN_DFSOBPSHORTNAME="SOURCEOFBUSINESSPROFILE.SOBPSHORTNAME";
	public static final String COLUMN_DFSOBPSHORTNAME="SOBPSHORTNAME";

  //--Release2.1--//
  //// 5. Adjust Field names definitions to string format.
	////public static final String QUALIFIED_COLUMN_DFDEALTYPEDESCR="DEALTYPE.DTDESCRIPTION";
	////public static final String COLUMN_DFDEALTYPEDESCR="DTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFDEALTYPEDESCR="DEAL.DEALTYPEID_STR";
	public static final String COLUMN_DFDEALTYPEDESCR="DEALTYPEID_STR";

  //--Release2.1--//
  //// 5. Adjust Field names definitions to string format.
  ////public static final String QUALIFIED_COLUMN_DFLOANPURPOSEDESCR="dealpurpose.DPDESCRIPTION";
	////public static final String COLUMN_DFLOANPURPOSEDESCR="DPDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFLOANPURPOSEDESCR="DEAL.DEALPURPOSEID_STR";
	public static final String COLUMN_DFLOANPURPOSEDESCR="DEALPURPOSEID_STR";

	public static final String QUALIFIED_COLUMN_DFTOTALPURCHASEPRICE="DEAL.TOTALPURCHASEPRICE";
	public static final String COLUMN_DFTOTALPURCHASEPRICE="TOTALPURCHASEPRICE";
	public static final String QUALIFIED_COLUMN_DFTOTALLOANAMT="DEAL.TOTALLOANAMOUNT";
	public static final String COLUMN_DFTOTALLOANAMT="TOTALLOANAMOUNT";

  //--Release2.1--//
  //// 5. Adjust Field names definitions to string format.
	////public static final String QUALIFIED_COLUMN_DFPAYMENTTERMDESCR="PAYMENTTERM.PTDESCRIPTION";
	////public static final String COLUMN_DFPAYMENTTERMDESCR="PTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFPAYMENTTERMDESCR="DEAL.PAYMENTTERMID_STR";
	public static final String COLUMN_DFPAYMENTTERMDESCR="PAYMENTTERMID_STR";

	public static final String QUALIFIED_COLUMN_DFESTCLOSINGDATE="DEAL.ESTIMATEDCLOSINGDATE";
	public static final String COLUMN_DFESTCLOSINGDATE="ESTIMATEDCLOSINGDATE";
	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";

  //--Release2.1--//
  //// 5. Adjust Field names definitions to string format.
	////public static final String QUALIFIED_COLUMN_DFSPECIALFEATURE="SPECIALFEATURE.SFDESCRIPTION";
	////public static final String COLUMN_DFSPECIALFEATURE="SFDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFSPECIALFEATURE="DEAL.SPECIALFEATUREID_STR";
	public static final String COLUMN_DFSPECIALFEATURE="SPECIALFEATUREID_STR";

	public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFUNDERWRITERFIRSTNAME="CONTACT.CONTACTFIRSTNAME";
	public static final String COLUMN_DFUNDERWRITERFIRSTNAME="CONTACTFIRSTNAME";
	public static final String QUALIFIED_COLUMN_DFAPPLICATIONDATE="DEAL.APPLICATIONDATE";
	public static final String COLUMN_DFAPPLICATIONDATE="APPLICATIONDATE";
	public static final String QUALIFIED_COLUMN_DFUNDERWRITERMIDDLEINITIAL="CONTACT.CONTACTMIDDLEINITIAL";
	public static final String COLUMN_DFUNDERWRITERMIDDLEINITIAL="CONTACTMIDDLEINITIAL";
	public static final String QUALIFIED_COLUMN_DFUNDERWRITERLASTNAME="CONTACT.CONTACTLASTNAME";
	public static final String COLUMN_DFUNDERWRITERLASTNAME="CONTACTLASTNAME";

	//-- ========== SCR#859 begins ========== --//
	//-- by Neil on Feb 14, 2005
	public static final String COLUMN_DFSERVICINGMORTGAGENUMBER = "SERVICINGMORTGAGENUMBER";
	public static final String QUALIFIED_COLUMN_DFSERVICINGMORTGAGENUMBER = "DEAL.SERVICINGMORTGAGENUMBER"; 
	//-- ========== SCR#859 ends ========== --//

/*********************************MCM Impl Team XS_2.25 changes starts ***************************/
  public static final String COLUMN_DFMTGPRODIDSTR = "MTGPRODID_STR";
  public static final String QUALIFIED_COLUMN_DFMTGPRODIDSTR = "DEAL.MTGPRODID_STR";
  
  public static final String COLUMN_DFMIPREMIUMAMOUNT = "MIPREMIUMAMOUNT";
  public static final String QUALIFIED_COLUMN_DFMIPREMIUMAMOUNT = "DEAL.MIPREMIUMAMOUNT";
  
  public static final String COLUMN_DFESCROWPAYMENTAMOUNT = "ESCROWPAYMENTAMOUNT_VALUE";
  public static final String QUALIFIED_COLUMN_DFESCROWPAYMENTAMOUNT = "DEAL.ESCROWPAYMENTAMOUNT_VALUE";
  
  public static final String COLUMN_DFCOMBINEDLTV = "COMBINEDLTV";
  public static final String QUALIFIED_COLUMN_DFCOMBINEDLTV = "DEAL.COMBINEDLTV";
  
  public static final String COLUMN_DFLOB = "LINEOFBUSINESSID_STR";
  public static final String QUALIFIED_COLUMN_DFLOB = "DEAL.LINEOFBUSINESSID_STR";
/*********************************MCM Impl Team XS_2.25 changes ends ***************************/  
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{
		//-- ========== SCR#859 begins ========== --//
		//-- by Neil on Feb 14, 2005
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSERVICINGMORTGAGENUMBER,
				COLUMN_DFSERVICINGMORTGAGENUMBER,
				QUALIFIED_COLUMN_DFSERVICINGMORTGAGENUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		//-- ========== SCR#859 ends ========== --//

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPLICATIONID,
				COLUMN_DFAPPLICATIONID,
				QUALIFIED_COLUMN_DFAPPLICATIONID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTATUSDESCR,
				COLUMN_DFSTATUSDESCR,
				QUALIFIED_COLUMN_DFSTATUSDESCR,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTATUSDATE,
				COLUMN_DFSTATUSDATE,
				QUALIFIED_COLUMN_DFSTATUSDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSFSHORTNAME,
				COLUMN_DFSFSHORTNAME,
				QUALIFIED_COLUMN_DFSFSHORTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOBPSHORTNAME,
				COLUMN_DFSOBPSHORTNAME,
				QUALIFIED_COLUMN_DFSOBPSHORTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALTYPEDESCR,
				COLUMN_DFDEALTYPEDESCR,
				QUALIFIED_COLUMN_DFDEALTYPEDESCR,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLOANPURPOSEDESCR,
				COLUMN_DFLOANPURPOSEDESCR,
				QUALIFIED_COLUMN_DFLOANPURPOSEDESCR,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALPURCHASEPRICE,
				COLUMN_DFTOTALPURCHASEPRICE,
				QUALIFIED_COLUMN_DFTOTALPURCHASEPRICE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALLOANAMT,
				COLUMN_DFTOTALLOANAMT,
				QUALIFIED_COLUMN_DFTOTALLOANAMT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPAYMENTTERMDESCR,
				COLUMN_DFPAYMENTTERMDESCR,
				QUALIFIED_COLUMN_DFPAYMENTTERMDESCR,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFESTCLOSINGDATE,
				COLUMN_DFESTCLOSINGDATE,
				QUALIFIED_COLUMN_DFESTCLOSINGDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSPECIALFEATURE,
				COLUMN_DFSPECIALFEATURE,
				QUALIFIED_COLUMN_DFSPECIALFEATURE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUNDERWRITERFIRSTNAME,
				COLUMN_DFUNDERWRITERFIRSTNAME,
				QUALIFIED_COLUMN_DFUNDERWRITERFIRSTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPLICATIONDATE,
				COLUMN_DFAPPLICATIONDATE,
				QUALIFIED_COLUMN_DFAPPLICATIONDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUNDERWRITERMIDDLEINITIAL,
				COLUMN_DFUNDERWRITERMIDDLEINITIAL,
				QUALIFIED_COLUMN_DFUNDERWRITERMIDDLEINITIAL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUNDERWRITERLASTNAME,
				COLUMN_DFUNDERWRITERLASTNAME,
				QUALIFIED_COLUMN_DFUNDERWRITERLASTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    /***************************MCM Impl Team XS_2.25 changes starts**************************/
    FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
              FIELD_DFPRODUCTNAME,
              COLUMN_DFMTGPRODIDSTR,
              QUALIFIED_COLUMN_DFMTGPRODIDSTR,
              String.class,
              false,
              false,
              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
              "",
              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
              ""));
    
    FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
              FIELD_DFCOMBINEDLTV,
              COLUMN_DFCOMBINEDLTV,
              QUALIFIED_COLUMN_DFCOMBINEDLTV,
              java.math.BigDecimal.class,
              false,
              false,
              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
              "",
              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
              ""));
    FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
              FIELD_DFMIPREMIUM,
              COLUMN_DFMIPREMIUMAMOUNT,
              QUALIFIED_COLUMN_DFMIPREMIUMAMOUNT,
              java.math.BigDecimal.class,
              false,
              false,
              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
              "",
              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
              ""));
    FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
              FIELD_DFTAXESCROW,
              COLUMN_DFESCROWPAYMENTAMOUNT,
              QUALIFIED_COLUMN_DFESCROWPAYMENTAMOUNT,
              java.math.BigDecimal.class,
              false,
              false,
              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
              "",
              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
              ""));
    FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
              FIELD_DFLOB,
              COLUMN_DFLOB,
              QUALIFIED_COLUMN_DFLOB,
              String.class,
              false,
              false,
              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
              "",
              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
              ""));
    /***************************MCM Impl Team XS_2.25 changes ends****************************/
	}
  
  
  
  /*****************************MCM Impl Team XS_2.25 changes starts***************************/
  
    public BigDecimal getDfCombinedLTV()
    {
        return (BigDecimal)getValue(FIELD_DFCOMBINEDLTV);
    }


    public BigDecimal getDfMIPremium()
    {
        return (BigDecimal)getValue(FIELD_DFMIPREMIUM);
    }


    public String getDfProductName()
    {
        return (String)getValue(FIELD_DFPRODUCTNAME);
    }


    public BigDecimal getDfTaxEscrow()
    {
        return (BigDecimal)getValue(FIELD_DFTAXESCROW);
    }


    public void setDfCombinedLTV(BigDecimal value)
    {
        setValue(FIELD_DFCOMBINEDLTV,value);
        
    }


    public void setDfMIPremium(BigDecimal value)
    {
        setValue(FIELD_DFMIPREMIUM,value);
        
    }


    public void setDfProductName(String value)
    {
        setValue(FIELD_DFPRODUCTNAME,value);
        
    }


    public void setDfTaxEscrow(BigDecimal value)
    {
        setValue(FIELD_DFTAXESCROW,value);
        
    }
    public String getDfLOB()
    {
       
        return (String)getValue(FIELD_DFLOB);
    }

    public void setDfLOB(String value)
    {
        setValue(FIELD_DFLOB,value);
        
    }
 /*****************************MCM Impl Team XS_2.25 changes ends***************************/



}

