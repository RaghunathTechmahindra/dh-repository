package mosApp.MosSystem;

/**
 * 28/Jun/2006 DVG #DG448 #3343  Life/Disability Insurance Screen - the total amount is not calculated correctly
 */

import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import MosSystem.Mc;
import MosSystem.Sc;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.InsureOnlyApplicant;
import com.basis100.deal.entity.LifeDisPremiumsIOnlyA;
import com.basis100.deal.entity.LifeDisabilityPremiums;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.InsureOnlyApplicantPK;
import com.basis100.deal.security.ActiveMessage;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.util.DBA;
import com.basis100.deal.util.MathUtil;
import com.basis100.deal.util.Parameters;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;

import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.event.ChildDisplayEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HtmlDisplayFieldBase;
import com.iplanet.jato.view.html.TextField;

/**
 *
 *
 */
public class LifeDisabilityInsuranceHandler
  extends PageHandlerCommon
  implements Cloneable, Sc
{

  public LifeDisabilityInsuranceHandler cloneSS()
  {
    return(LifeDisabilityInsuranceHandler)super.cloneSafeShallow();
  }

  ///////////////////////////////////////////////////////////////////////////
  //This method is used to populate the fields in the header
  //with the appropriate information

  public void populatePageDisplayFields()
  {
    SessionStateModelImpl theSessionState = getTheSessionState();

    PageEntry pg = getTheSessionState().getCurrentPage();

    Hashtable pst = pg.getPageStateTable();

    PageCursorInfo tds = (PageCursorInfo)pst.get("Repeated1");

    //--DJ_CR136--start--//
    PageCursorInfo tds2 = (PageCursorInfo)pst.get("Repeated2");
    //--DJ_CR136--start--//

    populatePageShellDisplayFields();

    populateTaskNavigator(pg);

    populatePageDealSummarySnapShot();

    populatePreviousPagesLinks();

    //--DJ_CR136--start--//
    populateRowsDisplayedBorrower(getCurrNDPage(), tds);
    populateRowsDisplayedInsureOnlyAppl(getCurrNDPage(), tds2);
    //--DJ_CR136--start--//

    populateDefaultValues(getCurrNDPage());
  }

  /////////////////////////////////////////////////////////////////////////

  private void populateRowsDisplayedBorrower(ViewBean page, PageCursorInfo tds)
  {
    String rowsRpt = "";

    if(tds.getTotalRows() > 0)
    {
      rowsRpt = BXResources.getSysMsg("LIFE_DISABILITY_BORROWERS_ENROLLED", theSessionState.getLanguageId());

      try
      {
        rowsRpt = com.basis100.deal.util.Format.printf(rowsRpt,
          new Parameters(tds.getFirstDisplayRowNumber())
          .add(tds.getLastDisplayRowNumber())
          .add(tds.getTotalRows()));
      }
      catch(Exception e)
      {
        logger.warning("String Format error @LifeDisabilityInsuranceHandler.populateRowsDisplayed: " + e);
      }
    }

    page.setDisplayFieldValue("stRowsDisplayed", new String(rowsRpt));
  }

  //--DJ_CR136--start--//
  private void populateRowsDisplayedInsureOnlyAppl(ViewBean page, PageCursorInfo tds2)
  {
    String rowsRpt = "";

    // Product wants to display this message allways even with no spouses created.
    rowsRpt = BXResources.getSysMsg("INSURE_ONLY_APPLICANTS_ENROLLED", theSessionState.getLanguageId());

    try
    {
      if (tds2.getTotalRows() > 0) {
        rowsRpt = com.basis100.deal.util.Format.printf(rowsRpt,
          new Parameters(tds2.getFirstDisplayRowNumber())
          .add(tds2.getLastDisplayRowNumber())
          .add(tds2.getTotalRows()));
      }
      else if (tds2.getTotalRows() == 0) {
        rowsRpt = com.basis100.deal.util.Format.printf(rowsRpt,
          new Parameters(0)
          .add(tds2.getLastDisplayRowNumber())
          .add(tds2.getTotalRows()));
      }
    }
    catch(Exception e)
    {
      logger.warning("String Format error @LifeDisabilityInsuranceHandler.populateRowsDisplayed: " + e);
    }

    logger.debug("LDH@populateRowDisplay:: " + new String(rowsRpt));
logger.debug("LDH@FirstDisplayRowNumber:: " + tds2.getFirstDisplayRowNumber());

    page.setDisplayFieldValue("stRowsDisplayedInsureOnlyAppl", new String(rowsRpt));
  }
  //--DJ_CR136--start--//

  ///////////////////////////////////////////////////////////////////////////
  //
  // Setup models, cursor, etc. associated with page - e.g. set criteria for deal snap shot, etc.
  ///////////////////////////////////////////////////////////////////////////

  public void setupBeforePageGeneration()
  {
    PageEntry pg = getTheSessionState().getCurrentPage();

    if(pg.getSetupBeforeGenerationCalled() == true)
    {
      return;
    }
    pg.setSetupBeforeGenerationCalled(true);

    TiledView vo = null;
    TiledView vo2 = null;

    int numRows = 0;
    int numRows2 = 0;

    setupDealSummarySnapShotDO(pg);

    // set the criteria for the populating model
    int dealId = pg.getPageDealId();
    int copyId = pg.getPageDealCID();

    try
    {
      doLifeDisabilityInsuranceInfoModelImpl theDO = (doLifeDisabilityInsuranceInfoModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doLifeDisabilityInsuranceInfoModel.class));

      theDO.clearUserWhereCriteria();

      theDO.addUserWhereCriterion("dfDealId", "=", new String("" + dealId));
      theDO.addUserWhereCriterion("dfCopyId", "=", new String("" + copyId));

      PageCursorInfo tds = getPageCursorInfo(pg, "Repeated1");

      Hashtable pst = pg.getPageStateTable();

      theDO.executeSelect(null);
      numRows = theDO.getSize();
      logger.trace("@LDInsuranceHandler.setupBeforePageGeneration: Number of LifeInsurance = " + numRows);
      if(tds == null)
      {
        String voName = "Repeated1";
        tds = new PageCursorInfo("Repeated1", voName, numRows, numRows);
        tds.setRefresh(true);
        pst.put(tds.getName(), tds);
      }
      else
      {
        tds.setTotalRows(numRows);
        tds.setRowsPerPage(numRows);
      }
      vo = (TiledView)(getCurrNDPage().getChild(tds.getVoName()));
      vo.setMaxDisplayTiles(numRows);
      if(tds.isTotalRowsChanged())
      {
        // number of rows has changed - unconditional display of first page
        vo = (TiledView)(getCurrNDPage().getChild(tds.getVoName()));
        tds.setCurrPageNdx(0);
        vo.resetTileIndex();
        tds.setTotalRowsChanged(false);
      }
    }
    catch(Exception ex)
    {
      setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
      logger.error("Problem encountered getting LifeInsuranceInfo Model");
      logger.error(ex);
    }

    //--DJ_CR136--start--//
    try
    {
      // set the criteria for the populating model
      doInsureOnlyApplicantModel theDO2 = (doInsureOnlyApplicantModel)
        (RequestManager.getRequestContext().getModelManager().getModel(doInsureOnlyApplicantModel.class));

      theDO2.clearUserWhereCriteria();

      theDO2.addUserWhereCriterion("dfDealId", "=", new String("" + dealId));
      theDO2.addUserWhereCriterion("dfCopyId", "=", new String("" + copyId));

      Hashtable pst = pg.getPageStateTable();

      theDO2.executeSelect(null);
      numRows2 = theDO2.getSize();
      logger.trace("@LDInsuranceHandler.setupBeforePageGeneration: Number of InsureOnlyApplicants = " + numRows2);

      PageCursorInfo tds2 = getPageCursorInfo(pg, "Repeated2");

      if(tds2 == null)
      {
        String voName2 = "Repeated2";
        tds2 = new PageCursorInfo("Repeated2", voName2, numRows2, numRows2);
        tds2.setRefresh(true);
        pst.put(tds2.getName(), tds2);
      }
      else
      {
        tds2.setTotalRows(numRows2);
        tds2.setRowsPerPage(numRows2);
      }
      vo2 = (TiledView)(getCurrNDPage().getChild(tds2.getVoName()));
      vo2.setMaxDisplayTiles(numRows2);
      if(tds2.isTotalRowsChanged())
      {
        // number of rows has changed - unconditional display of first page
        vo2 = (TiledView)(getCurrNDPage().getChild(tds2.getVoName()));
        tds2.setCurrPageNdx(0);
        vo2.resetTileIndex();
        tds2.setTotalRowsChanged(false);
      }
    }
    catch(Exception ex)
    {
      setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
      logger.error("Problem encountered getting InsureOnlyApplicant Model");
      logger.error(ex);
    }

    //--DJ_CR136--end--//

    Integer cspDealId = new Integer(pg.getPageDealId());
    int copyid = theSessionState.getCurrentPage().getPageDealCID();
    Integer cspDealCPId = new Integer(copyid);

    executeCretarioForLifeDisabilityInfo(cspDealId, cspDealCPId);
    //--DJ_CR136--start--//
    executeCretarioForLifeDisInfoIOnlyA(cspDealId, cspDealCPId);
    //--DJ_CR136--end--//
    executeCretarioForLifeDisabilityInsProportions(cspDealId, cspDealCPId);
  }

  /**
   *
   * getDfCombinedLDPremiums()
   */
  public double countTotalPremiumFromDo()
  {
    SessionResourceKit srk = getSessionResourceKit();

    // determine Total Premiums (for all borrowers) by using (already executed) Life/Disability
    // financial details from the appropriate model.
    doLifeDisabilityInsuranceInfoModel mTotalPremium =
      (doLifeDisabilityInsuranceInfoModel)RequestManager.getRequestContext().getModelManager().getModel(
      doLifeDisabilityInsuranceInfoModel.class);

    double totPremium = 0.0;

    try
    {
      mTotalPremium.beforeFirst();

      while(mTotalPremium.next())
      {
        Object oTotPremium = mTotalPremium.getValue(mTotalPremium.FIELD_DFCOMBINEDLDPREMIUM);
        if(oTotPremium != null)
        {
          totPremium += new Double(oTotPremium.toString()).doubleValue();
        }
      }

      // restore model to before first state - in case no already used in presentation generation
      mTotalPremium.beforeFirst();
    }
    catch(ModelControlException mce)
    {
      logger.error("LDIH@.countTotalPremiumFromDo:: Unexpected model control exception, mce = " + mce);
      totPremium = 0.0;
    }

    ////logger.debug("LDIH@countTotalPremiumFromDo::TotalPremium: " + totPremium);
    return totPremium;
  }

  /**
   *
   * BXTODO: Finish this method. and create new one to set up the value on the screen.
   */
  public double countInsuredAmount(double lifePercentage)
  {
    SessionResourceKit srk = getSessionResourceKit();

    // determine totalLoanAmount (for primary borrower) by using (already executed) Life/Disability
    // financial details from the appropriate model.
    doLifeDisabilityInsProportionsModel mTotalLoanAmount =
      (doLifeDisabilityInsProportionsModel)RequestManager.getRequestContext().getModelManager().getModel(doLifeDisabilityInsProportionsModel.class);

    double totLoanAmount = 0.0;
    double insuredAmount = 0.0;

    try
    {
      mTotalLoanAmount.beforeFirst();

      while(mTotalLoanAmount.next())
      {
        Object oTotalLoanAmount = mTotalLoanAmount.getValue(mTotalLoanAmount.FIELD_DFTOTALLOANAMOUNT);
        if(oTotalLoanAmount != null)
        {
          totLoanAmount = new Double(oTotalLoanAmount.toString()).doubleValue();
          logger.debug("LDIH@.countInsuredAmount::TotalLoanAmount: " + totLoanAmount);
        }
      }

      // restore model to before first state - in case no already used in presentation generation
      mTotalLoanAmount.beforeFirst();
    }
    catch(ModelControlException mce)
    {
      logger.error("LDIH@.countInsuredAmount:: Unexpected model control exception, mce = " + mce);
      totLoanAmount = 0.0;
    }
    logger.debug("LDIH@.countInsuredAmount::TotalLoanAmount: " + totLoanAmount);

    insuredAmount = totLoanAmount * lifePercentage;
    logger.debug("LDIH@.countInsuredAmount::InsuredAmount: " + insuredAmount);

    return insuredAmount;
  }

  /**
   *
   *
   */
  //// New CR update from DJ: they want to see not totalLoanAmount here, but
  //// the insured amount (totalLoanAmount * lifePercentage).
  protected void setInsuredAmount(ViewBean pg, ChildDisplayEvent event)
  {
    PageEntry currPage = (PageEntry)theSessionState.getCurrentPage();
    Hashtable pst = currPage .getPageStateTable();
    //logger.debug("---D---->LDIH@setInsuredAmount::CurrentPage: " + currPage);

    //// Take the insuredAmount and display on the page.
    double insuredAmount = new Double(pst.get("INSURED_AMOUNT").toString()).doubleValue();
    logger.debug("---D---->LDIH@setInsuredAmount::InsuredAmount: " + insuredAmount);

    pg.setDisplayFieldValue("stMortgageInsuranceAmount", (new Double(insuredAmount)).toString());
  }

  private int getLifeAcceptance()
  {
    SessionResourceKit srk = getSessionResourceKit();

    // determine Life/Disability statuses from the appropriate model.
    doLifeDisabilityInsuranceInfoModel mLifeStatus =
      (doLifeDisabilityInsuranceInfoModel)RequestManager.getRequestContext().getModelManager().getModel(
      doLifeDisabilityInsuranceInfoModel.class);

    int lifeStatusId = 0; // default is 'blank'

    try
    {
      mLifeStatus.beforeFirst();

      while(mLifeStatus.next())
      {
        Object oLifeStatus = mLifeStatus.getValue(mLifeStatus.FIELD_DFLIFESTATUSID);
        if(oLifeStatus != null)
        {
          lifeStatusId = new Integer(oLifeStatus.toString()).intValue();
        }
      }

      // restore model to before first state - in case no already used in presentation generation
      mLifeStatus.beforeFirst();
    }
    catch(ModelControlException mce)
    {
      logger.error("LDIH@.getLifeAcceptance:: Unexpected model control exception, mce = " + mce);
      lifeStatusId = 0; // default is 'blank'
    }

    ////logger.debug("LDIH@getLifeAcceptance::LifeStatusId: " + lifeStatusId);
    return lifeStatusId;
  }

  /**
   *  Populate default values (hiddens, etc.).
   *
   */
  private void populateDefaultValues(ViewBean thePage)
  {

    PageEntry pg = theSessionState.getCurrentPage();
    String theExtraHtml = "";

    // the OnChange string to be set on the fields
    HtmlDisplayFieldBase df;

    df = (HtmlDisplayFieldBase)thePage.getDisplayField("cbInsProportions");
    theExtraHtml = "onChange=\"populateValueTo(['hdInsuranceProportionsId']);"
      + " SetLifeDisabilityDisplay(); SetLifeDisDisplayInsureOnlyApplicant();\"";
    df.setExtraHtml(theExtraHtml);
  }

  //--DJ_CR136--start--//
  /**
   *  Check to display row or not.
   *
   */
  public boolean checkDisplayThisRow2(String cursorName)
  {
    try
    {
      PageEntry pg = getTheSessionState().getCurrentPage();
      Hashtable pst = pg.getPageStateTable();

      PageCursorInfo tds2 = (PageCursorInfo)pst.get(cursorName);

logger.debug("LDIH@checkDisplayThisRow2::cursorName: " + cursorName +
                   "tds_name: " + tds2.getName() +
                   ":TotalRows: " + tds2.getTotalRows());

      if(tds2.getTotalRows() <= 0)
      {
        return false;
      }

      return true;
    }
    catch(Exception e)
    {
      logger.error("LDIH@checkDisplayThisRow");
      logger.error(e);
      return false;
    }
  }

  public boolean checkDisplayThisRow(String cursorName)
  {
    try
    {
      PageEntry pg = getTheSessionState().getCurrentPage();
      Hashtable pst = pg.getPageStateTable();

      PageCursorInfo tds = (PageCursorInfo)pst.get(cursorName);

logger.debug("LDIH@checkDisplayThisRow::cursorName: " + cursorName +
                   "tds_name: " + tds.getName() +
                   ":TotalRows: " + tds.getTotalRows());

      if(tds.getTotalRows() <= 0)
      {
        return false;
      }

      return true;
    }
    catch(Exception e)
    {
      logger.error("LDIH@checkDisplayThisRow");
      logger.error(e);
      return false;
    }
  }

  public void setupNumOfRepeatedIOnlyAppl(TiledView repeated2)
  {
    try
    {
      doInsureOnlyApplicantModelImpl numOfAppl =(doInsureOnlyApplicantModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doInsureOnlyApplicantModel.class));

      repeated2.setMaxDisplayTiles(numOfAppl.getSize());
    }
    catch(Exception e)
    {
      setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
      logger.error("Exception @LifeDisabilityHandler.setupNumOfRepeatedIOnlyAppl");
      logger.error(e);
    }
  }
  //--DJ_CR136--end--//

  public void handleSubmitStandard(boolean warn)
  {
    PageEntry pg = theSessionState.getCurrentPage();

    SessionResourceKit srk = getSessionResourceKit();
    CalcMonitor dcm = CalcMonitor.getMonitor(srk);

    // BXTODO: Validate rates, etc. Think about this when it is necessary
    // to add this full functionality of Life/Disability Insurance (wiht internal
    // calc engine) for any other client (BMO, etc.).
    ////if (validateData(pe, srk) == false) return;

    //// This is temporarily here to isolate and test the new set of Life/Disability
    try
    {
      Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

      PassiveMessage pm = null;
      if(deal.getScenarioLocked() != null && deal.getScenarioLocked().equals("N"))
      {
        logger.trace("--T--> @LDIH:handleSubmitStandard(): validate Life/Disability Insuranse");

        // run validation (verify sets of Rules should be running).
        pm = validate(deal, pg, srk);
        if(pm != null && pm.getCritical() == true)
        {
          setActiveMessageToAlert(BXResources.getSysMsg("LDQ_CANNOT_SUBMIT_VALIDATION_CRITICAL", theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMCONFIRM);
          pm.setGenerate(true);
          theSessionState.setPasMessage(pm);
          return;
        }
      }

      logger.debug("--T--> @LDIH:handleSubmitStandard::isModified? " + pg.isModified());

      // Set message set: Undiscussible return to LDI page to 'Recalc' DLLs first.
      if(pg.isModified() == true)
      {
        logger.trace("--T--> @LDIH:handleSubmitStandard(): validate data changes on the screen");
        setActiveMessageToAlert(BXResources.getSysMsg("LDQ_CANNOT_SUBMIT_RPT_INFOCAL_FIRST", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);

        // not to display by default for the next visit from DE/UW.
        theSessionState.setIsDisplayLifeDisabilitySubmit(false);

        return;
      }

      // active and/or passive message set in this case
      try
      {
        srk.beginTransaction();
        dcm.inputEntity(deal);
        doCalculation(dcm);
        saveData(pg, true);
        standardAdoptTxCopy(pg, true);

        // not to display by default for the next visit from DE/UW.
        theSessionState.setIsDisplayLifeDisabilitySubmit(false);

        srk.commitTransaction();
        navigateToNextPage(true);
      }
      catch(Exception e)
      {
        srk.cleanTransaction();
        logger.error("Exception occurred LDIH@handleSubmit()");
        logger.error(e);
        setStandardFailMessage();
        return;
      }
    }
    catch(Exception e)
    {
      logger.error("LDIH@Deal not found: " + e);
    }
  }

  /**
   *
   *
   */
  public PassiveMessage validate(Deal deal, PageEntry pe, SessionResourceKit srk) throws Exception
  {
    PassiveMessage pm = new PassiveMessage();
    BusinessRuleExecutor brExec = new BusinessRuleExecutor();
    Vector v = new Vector();

    logger.debug("VLAD =================== << BusinessRules Validate Begin >> ================");

    ////BXTODO: Validate with the Product Team which set(s) should be run for
    ////        the Life/Disability Quatation submit. Most Probably the "DE-%"
    ////        family should go, but not necessarily, may be only new "LDI-%" set.

    //// BXTODO: Temp commented out.
    ////v.add("DE-%");

    logger.debug("VLAD ==> before Running BusinessRules (-- BATCH --)");

    brExec.BREValidator(theSessionState, pe, srk, pm, v, true);

    logger.debug("VLAD ==> After Running BusinessRules (-- BATCH --)");

    // local scope rules ...
    int lim = 0;
    Object[] objs = null;

    // 1. borrower rules
    try
    {
      Collection borrowers = deal.getBorrowers();
      lim = borrowers.size();
      objs = borrowers.toArray();
    }
    catch(Exception e)
    {
      lim = 0;
    }

    //// Insurance Business Rules with the prefix "LDI-%".
    for(int i = 0; i < lim; ++i)
    {
      Borrower borr = (Borrower)objs[i];
      brExec.setCurrentProperty( -1);
      brExec.setCurrentBorrower(borr.getBorrowerId());
      logger.trace("--T--> @Life&DisabilityIns.validate: LDI rules, borrower num=" + i);
      PassiveMessage pmBorr = brExec.BREValidator(theSessionState, pe, srk, null, "LDI-%", true);
      if(pmBorr != null)
      {
        pm.addSeparator();
        String applicantLabel = (BXResources.getGenericMsg("PASSIVE_MSG_APPLIED_TO_APPLICANT_LABEL",
          theSessionState.getLanguageId())
          + "<br>");

        pm.addMsg(applicantLabel + borr.formFullName(), PassiveMessage.INFO);
////logger.debug("--D--> @Life&DisabilityIns.add: " + PassiveMessage.INFO);

        pm.addAllMessages(pmBorr);
        pm.addSeparator();
      }
////logger.debug("--D--> @Life&DisabilityIns.addMessagesNum: " + pm.getNumMessages());
    }

   //--DJ_CR136--start--//
   // 1. Insure Only Applicants rules
   try
   {
     Collection insureOnlyApplicants = deal.getInsureOnlyApplicants();
     lim = insureOnlyApplicants.size();
logger.debug("--D-->LDI@validate:NumOfInsureOnlyApps: " + lim);

     objs = insureOnlyApplicants.toArray();
   }
   catch(Exception e)
   {
     lim = 0;
   }

   //// Insurance Business Rules with the prefix "LDI-%".
   for(int i = 0; i < lim; ++i)
   {
     InsureOnlyApplicant ioa = (InsureOnlyApplicant)objs[i];
     brExec.setCurrentProperty( -1);
     brExec.setCurrentBorrower(ioa.getInsureOnlyApplicantId());
     logger.trace("--T--> @Life&DisabilityIns.validate: LDI rules, InsureOnlyApplicant num=" + i);
     PassiveMessage pmInsureOnlyAppl = brExec.BREValidator(theSessionState, pe, srk, null, "LDI-%", true);
     if(pmInsureOnlyAppl != null)
     {
       pm.addSeparator();
       String applicantLabel = (BXResources.getGenericMsg("PASSIVE_MSG_APPLIED_TO_APPLICANT_LABEL",
         theSessionState.getLanguageId())
         + "<br>");

       pm.addMsg(applicantLabel + ioa.getInsureOnlyApplicantLastName(), PassiveMessage.INFO);
logger.debug("--D--> @Life&DisabilityIns.add: " + PassiveMessage.INFO);

       pm.addAllMessages(pmInsureOnlyAppl);
       pm.addSeparator();
     }
////logger.debug("--D--> @Life&DisabilityIns.addMessagesNum: " + pm.getNumMessages());
   }

   //--DJ-CR136--end--//
    brExec.close();

    logger.debug("VLAD =================== << BusinessRules Validate End >> ================");

    if(pm.getNumMessages() > 0)
    {
      String tmpMsg = BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_DEAL_PROBLEM", theSessionState.getLanguageId())
        + "<br>" + "(Deal #: " + deal.getDealId() + ", " + "Scenario #: " + deal.getScenarioNumber() + ")";

      pm.setInfoMsg(tmpMsg);

      return pm;
    }
    return null;
  }

  public boolean validateData(PageEntry pe, SessionResourceKit srk)
  {
    try
    {
      validateLifeDisabilityRates(pe, srk);
      return true;
    }
    catch(Exception e)
    {
      logger.error("LDIH@Rate validation encountered: " + "deal Id = " + pe.getPageDealId() + ", copy Id = " +
        pe.getPageDealCID());
      logger.error(e.getMessage());
      return false;
    }
  }

  private void validateLifeDisabilityRates(PageEntry pg, SessionResourceKit srk) throws Exception
  {
    //// BXTODO: Add validation logic here for any other implementation for
    //// other client with life&disability rate management and internal audit!!!
  }

  public void handleRecalcInfocal(String[] args)
  {
    PageEntry pg = (PageEntry)theSessionState.getCurrentPage();
    SessionResourceKit srk = getSessionResourceKit();
    CalcMonitor dcm = CalcMonitor.getMonitor(srk);
    SysLogger logger = srk.getSysLogger();

    logger.trace("--T--> @LDH.handleRecalcInfocal: " + ((args != null && args[0] != null) ? args[0] : "No args"));

    if(args[0].equals("PARSE_OUTPUT_FROM_DLL"))
    {
      try
      {
        submitToDLL(pg, srk, dcm);
      }
      catch(Exception e)
      {
        logger.debug("LDH@handleRecalcInfocal:Exception: " + e);
      }

      return;
    }
  }

  //// The communication with ActiveX DLL is implemented as follows:
  //// 1. Xpress forms input Strings to RTP&Infocal DLL via new framework layer based on the internal Xpress/HomeBASE protocol.
  ////    2. And via JATO/JavaScript passes them to ActiveX Control.
  ////      3. ActiveX Control calls HomeBASE project.
  ////        4. HomeBASE parses the Strings based on the same internal Xpress/HomeBASE protocol.
  ////          5. HomeBASE calculates all values.
  ////        6. HomeBASE forms output String based on the same internal Xpress/HomeBASE protocol.
  ////      7. JavaScript extracts the output String from ActiveX Control (formed by DLLs calc engine).
  ////    8. Xpress gets the output DLL string via JATO and parses it.
  //// 9. Xpress framework updates db via entities and dynamically displays obtained (calculated) values on the screen.

  private int submitToDLL(PageEntry pg, SessionResourceKit srk, CalcMonitor dcm) throws Exception
  {
    Hashtable pst = pg.getPageStateTable();

    //int statusId = 0;
    int borrowerId = 0;
    int insureOnlyApplicantId = 0;
    int copyId = pg.getPageDealCID();

    //int theDealId = pg.getPageDealId();
    int theCopyId = pg.getPageDealCID();
    ViewBean thePage = getCurrNDPage();

    int theLDInsProportions = 0;
    int countApplicantAcceptLifeIns = 0;

    String inputToRTP = "";
    String inputToInfocal = "";
    String inputToRTPDealPart = "";
    String inputToRTPRepeatedPart = "";

    Vector hdBorrowerId = getRepeatedFieldValues("Repeated1/hdBorrowerId");
    Vector rbDisabilityInsurance = getRepeatedFieldValues("Repeated1/rbDisabilityInsurance");
    Vector rbLifeInsurance = getRepeatedFieldValues("Repeated1/rbLifeInsurance");
    Vector cbBorrowerGender = getRepeatedFieldValues("Repeated1/cbBorrowerGender");
    Vector cbBorrowerSmoker = getRepeatedFieldValues("Repeated1/cbBorrowerSmoker");

    //--DJ_CR136--start--//
    Vector hdInsureOnlyApplicantId = getRepeatedFieldValues("Repeated2/hdInsureOnlyApplicantId");
    Vector rbInsureOnlyApplDisIns = getRepeatedFieldValues("Repeated2/rbDisabilityInsurance");
    Vector rbInsureOnlyApplLifeIns = getRepeatedFieldValues("Repeated2/rbLifeInsurance");
    Vector cbInsureOnlyApplGender = getRepeatedFieldValues("Repeated2/cbInsureOnlyApplicantGender");
    Vector cbInsureOnlyApplSmoker = getRepeatedFieldValues("Repeated2/cbInsureOnlyApplicantSmoker");
    Vector txInsureOnlyApplFirstName = getRepeatedFieldValues("Repeated2/txInsureOnlyApplicantFirstName");
    Vector txInsureOnlyApplLastName = getRepeatedFieldValues("Repeated2/txInsureOnlyApplicantLastName");
    Vector cbInsureOnlyApplDOBMonth = getRepeatedFieldValues("Repeated2/cbIOApplicantDOBMonth");
    Vector txInsureOnlyApplDOBDay = getRepeatedFieldValues("Repeated2/txIOApplicantDOBDay");
    Vector txInsureOnlyApplDOBYear = getRepeatedFieldValues("Repeated2/txIOApplicantDOBYear");
    //--DJ_CR136--end--//

    //// Create/store the vectors with the potentially updated values (to propagate
    //// to the final db update after getting the results from the RTP/Infocal.
    //// First invocation!
    //// It is still in place, but should be moved to the ApplicantPage only. Here should
    //// be static(display) only. Finalize with Product.
    Vector borrGenderIds = new Vector();
    Vector borrSmokerIds = new Vector();
    Vector lifeInsuranceIds = new Vector();
    Vector disInsuranceIds = new Vector();
    Vector lifePercCoverage = new Vector();
    //--Ticket#1294--13May2005--start--//
    Vector disPercentCoverage = new Vector();
    //--Ticket#1294--13May2005--end--//

    //--DJ_CR136--start--//
    Vector insureOnlyGenderIds = new Vector();
    Vector insureOnlySmokerIds = new Vector();
    Vector insureOnlyLifeInsuranceIds = new Vector();
    Vector insureOnlyDisInsuranceIds = new Vector();
    Vector insureOnlyLifePercCoverage = new Vector();
    Vector insureOnlyFirstNames = new Vector();
    Vector insureOnlyLastNames = new Vector();
    Vector insureOnlyDOBMonths = new Vector();
    Vector insureOnlyDOBDays = new Vector();
    Vector insureOnlyDOBYears = new Vector();
    Vector insureOnlyDateOfBirths = new Vector();
    Vector insureOnlyAges = new Vector();
    //--DJ_CR136--end--//

    //--Ticket#1294--13May2005--start--//
    Vector insureOnlyDisPercCoverage = new Vector();
    //--Ticket#1294--13May2005--end--//

    Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

    ////I Do all internal calculations/formatting of data passing to DLL (#1-7 below).
    ////1. Get InsuranceOptionsId.
    try
    {
      ComboBox cbLDInsProportions = (ComboBox)thePage.getDisplayField("cbInsProportions");
      String ldInsPropoprtionsValue = cbLDInsProportions.getValue().toString();
      pst.put("INSURANCE_OPTION", ldInsPropoprtionsValue);

      logger.debug("LDIH@submitToDLL::LDInsProportionsValue: " + ldInsPropoprtionsValue);

      theLDInsProportions = (new Integer(ldInsPropoprtionsValue)).intValue();
    }
    catch(Exception e)
    {
      logger.debug("LDIH@submitToDLL::Exception getting LDInsProportions value: " + e);
    }
    logger.debug("LDIH@submitToDLL::LDInsProportionsId: " + theLDInsProportions);
    logger.debug("LSIH@submitToDLL::BorrowerIdSize: " + hdBorrowerId.size());

    //// 2. Get LifePercentCoverage Requested. IMPORTANT!
    // LifePercCoverageReq is moved to the logical Deal level (Header on the html page). On the other
    // hand, it is still a field in the Borrower table/entity. It is displayed for the 'Primary'
    // borrower now. Since the final specs have not signed by client yet it could be
    // 'return' to the original appoach (displaying for each borrower in the repeatable part).
    // Thus, the repeatable implementation is not removed, just commented out.

    TextField txLifePercCoverage = null;

    try
    {
      txLifePercCoverage = (TextField)thePage.getDisplayField("txLifePercCoverageReqReq");
    }
    catch(Exception e)
    {
      logger.debug("LDIH@submitToDLL::Exception getting LDInsProportions value: " + e);
    }

    // Force to populate db with 100 per cent if insurance proportions is 'Home Owner Plus' or 'Total'.
    String lifePercentInsurance = "";

    if(theLDInsProportions == Mc.DJ_HOME_OWNER_PLUS_OPTION ||
      theLDInsProportions == Mc.DJ_TOTAL_OPTION ||
      theLDInsProportions == Mc.DJ_OPTION_NOT_DEFINED)
    {
      lifePercentInsurance = Mc.FULL_PERCENT_COVERAGE_STRING;
    }
    else if(theLDInsProportions == Mc.DJ_DECLINED_OPTION)
    {
      lifePercentInsurance = Mc.ZERO_PERCENT_COVERAGE_STRING;
    }
    else if(theLDInsProportions == Mc.DJ_REGULAR_OPTION)
    {
      lifePercentInsurance = txLifePercCoverage.getValue().toString();
    }

    logger.debug("LSIH@submitToDLL::LifePercCoverage: " + lifePercentInsurance);

    //// 3. Calculate Life and Disability Percent Coverage passing to the DLL calc engine.
    String lifePercentInsuranceCalc = calcLifePercentInsurance(theLDInsProportions, lifePercentInsurance);
    logger.debug("LSIH@submitToDLL::lifePercentInsuranceCalc: " + lifePercentInsuranceCalc);

    ////4. Count ALL Applicants accepted Life Insurance.
    int borrowerAcceptedDisIns = 0;
    int borrowerAcceptedLifeIns = 0;

    //--DJ_CR136--start--//
    int insureOnlyApplAcceptedDisIns = 0;
    int insureOnlyApplAcceptedLifeIns = 0;
    //--DJ_CR136--end--//

    ////4. Count ALL Borrowers and Insure Only Applicants accepted Life Insurance.
    for(int i = 0; i < rbLifeInsurance.size(); i++)
    {
      logger.debug("LDIH@submitToDLL::rbLifeInsuranceValue: " + rbLifeInsurance.elementAt(i).toString());
      if(rbLifeInsurance.elementAt(i).toString().equals(Mc.LIFE_DISABILITY_STATUS_ACCEPTED))
      {
        borrowerAcceptedLifeIns += 1;
      }
    }
    logger.debug("LDIH@submitToDLL::NumOfBorrowersAcceptedLifeIns: " + borrowerAcceptedLifeIns);

    //--DJ_CR136--start--//
    for(int i = 0; i < rbInsureOnlyApplLifeIns.size(); i++)
    {
     logger.debug("LDIH@submitToDLL::rbInsureOnlyApplLifeIns: " + rbInsureOnlyApplLifeIns.elementAt(i).toString());
     if(rbInsureOnlyApplLifeIns.elementAt(i).toString().equals(Mc.LIFE_DISABILITY_STATUS_ACCEPTED))
     {
       insureOnlyApplAcceptedLifeIns += 1;
      }
    }
    logger.debug("LDIH@submitToDLL::NumOfSpousesAccptedLifeIns: " + insureOnlyApplAcceptedLifeIns);
    //--DJ_CR136--end--//

    ////5. Count ALL Borrowers and Insure Only Applicants accepted Disability Insurance.
    for(int i = 0; i < rbDisabilityInsurance.size(); i++)
    {
      logger.debug("LDIH@submitToDLL::rbDisabilityInsuranceValue: " + rbDisabilityInsurance.elementAt(i).toString());

      if(rbDisabilityInsurance.elementAt(i).toString().equals(Mc.LIFE_DISABILITY_STATUS_ACCEPTED))
      {
        borrowerAcceptedDisIns += 1;
      }
    }
    logger.debug("LDIH@submitToDLL::NumOfBorrowersAcceptedLifeIns: " + borrowerAcceptedDisIns);

    //--DJ_CR136--start--//
    for(int i = 0; i < rbInsureOnlyApplDisIns.size(); i++)
    {
      logger.debug("LDIH@submitToDLL::rbInsureOnlyApplDisInsValue: " + rbInsureOnlyApplDisIns.elementAt(i).toString());

      if(rbInsureOnlyApplDisIns.elementAt(i).toString().equals(Mc.LIFE_DISABILITY_STATUS_ACCEPTED))
      {
        insureOnlyApplAcceptedDisIns += 1;
      }
    }
    logger.debug("LDIH@submitToDLL::NumOfSpousesAcceptedDisIns: " + insureOnlyApplAcceptedDisIns);
    //--DJ_CR136--end--//

    ////6. Form the Strings for Borrowers that should be passed to the DLLs with Deal Related parameters only.
    int generalNumOfInsurersAcceptedLife = borrowerAcceptedLifeIns + insureOnlyApplAcceptedLifeIns;
    int generalNumOfInsurersAcceptedDis = borrowerAcceptedDisIns + insureOnlyApplAcceptedDisIns;

logger.debug("LDIH@submitToDLL::borrowerAcceptedDisIns: " + borrowerAcceptedDisIns);
logger.debug("LDIH@submitToDLL::insureOnlyApplAcceptedDisIns: " + insureOnlyApplAcceptedDisIns);

    //--DJ_CR136--start--//
    //// Borrower and Insure Only Applicant business entities are equivalent in terms of
    //// RTP/Infocal calcs thus the sum of both types accepted life insurance should be
    //// passed to the deal part.
    if(generalNumOfInsurersAcceptedLife >= 0)
    {
      inputToRTPDealPart = formDealPartOfRTPString(deal, generalNumOfInsurersAcceptedLife);
      inputToInfocal = formToInfocalDLLString(deal);

      logger.debug("LSIH@submitToDLL::DealPartOfRTP: " + inputToRTPDealPart);
      logger.debug("LSIH@submitToDLL::InfocalString: " + inputToInfocal);
    }

    //// 7. Finish formation of all Strings for Classical express Borrowers
    //// to RTP/Infocal DLLs and pass them/get response from Infocal.
    //--DJ_CR136--end--//
    if(hdBorrowerId != null && hdBorrowerId.size() > 0)
    {
      // There are multiple row values in this display field
      int vectorSize = hdBorrowerId.size();
      for(int i = 0; i < vectorSize; i++)
      {
        borrowerId = com.iplanet.jato.util.TypeConverter.asInt(hdBorrowerId.get(i), -1);

        if(borrowerId >= 0)
        {
          Borrower borrower = new Borrower(srk, dcm);
          borrower = borrower.findByPrimaryKey(new BorrowerPK(borrowerId, theCopyId));

//// Temp debug. Should stay until the legacy connection done.
logger.debug("LSIH@submitToDLL::BorrowerIdHidden[" + i + "]: " + borrowerId);
logger.debug("LSIH@submitToDLL::CopyId[" + i + "]: " + copyId);
logger.debug("LSIH@submitToDLL::RbDisabilityInsuranceBorrower[" + i + "]: " + rbDisabilityInsurance.get(i).toString());
logger.debug("LSIH@submitToDLL::RbLifeInsuranceBorrower[" + i + "]: " + rbLifeInsurance.get(i).toString());
logger.debug("LSIH@submitToDLL::BorrowerGenderCombo[" + i + "]: " + cbBorrowerGender.get(i).toString());
logger.debug("LSIH@submitToDLL::BorrowerSmokerCombo[" + i + "]: " + cbBorrowerSmoker.get(i).toString());

          String disabilityPercentInsurance = "";

          if(rbDisabilityInsurance.get(i).toString().equals(Mc.LIFE_DISABILITY_STATUS_ACCEPTED))
          {
            disabilityPercentInsurance = calcDisabilityPercentInsurance(theLDInsProportions,
              lifePercentInsurance,
              generalNumOfInsurersAcceptedDis);
              ////borrowerAcceptedDisIns);
          }
          else
          {
            disabilityPercentInsurance = Mc.DISABILITY_INS_ZERO_STRING;
          }
logger.debug("LSIH@submitToDLL::disabilityPercentInsurance: " + disabilityPercentInsurance);

          BorrowerScreenInfoWrapper borrScreenInfo = new BorrowerScreenInfoWrapper();
          borrScreenInfo.setSmokerStatus(cbBorrowerSmoker.get(i).toString());
          borrScreenInfo.setGenderId(cbBorrowerGender.get(i).toString());
          borrScreenInfo.setLifeInsStatus(rbLifeInsurance.get(i).toString());
          borrScreenInfo.setDisabilityInsStatus(rbDisabilityInsurance.get(i).toString());
          borrScreenInfo.setLifePercentInsurance(lifePercentInsuranceCalc);
          borrScreenInfo.setDisabilityPercentInsurance(disabilityPercentInsurance);

          borrGenderIds.addElement(cbBorrowerGender.get(i));
          borrSmokerIds.addElement(cbBorrowerSmoker.get(i));
          lifeInsuranceIds.addElement(rbLifeInsurance.get(i).toString());
          disInsuranceIds.addElement(rbDisabilityInsurance.get(i).toString());
          lifePercCoverage.addElement(lifePercentInsurance);
          //--Ticket#1294--13May2005--start--//
          disPercentCoverage.addElement(disabilityPercentInsurance);
          //--Ticket#1294--13May2005--end--//

          if(rbLifeInsurance.get(i).toString().equals(Mc.LIFE_DISABILITY_STATUS_ACCEPTED))
          {
            inputToRTPRepeatedPart += formBorrowerPartOfRTPString(borrower, borrScreenInfo);
            countApplicantAcceptLifeIns += 1;

logger.debug("LSIH@submitToDLL::inputToRTPRepeatedPartBorrowers: " + inputToRTPRepeatedPart);
logger.debug("LSIH@submitToDLL::countBorrowersAcceptedLifeIns: " + countApplicantAcceptLifeIns);
          }
        } //if
      } //for

      //// Save updated values and pass them for the final update after getting info from
      //// RTP/Infocal calc engine.
      pst.put("BORR_GENDERS", borrGenderIds);
      pst.put("BORR_SMOKER_STAT", borrSmokerIds);
      pst.put("LIFE_INS_IDS", lifeInsuranceIds);
      pst.put("DISABILITY_INS_IDS", disInsuranceIds);
      pst.put("LIFE_PERC_COVERAGE", lifePercCoverage);
      //--Ticket#1294--13May2005--start--//
      pst.put("DISABILITY_PERC_COVERAGE", disPercentCoverage);
      //--Ticket#1294--13May2005--end--//
    } //if

    //--DJ_CR136--start--//
    //// 8. Form the Strings for Insure Only Applicants that should be passed
    //// to the DLLs with Deal Related parameters only.
    //// IMPORTANT!! Should stay here. Despite HomeBASE analyst confirmed the
    //// four plus four capability (four borrowers and four insure only applicants)
    //// together is should be tested first. If it doesnt work the logic should
    //// be slightly changed in order to pass them separately.
    /**
    if(insureOnlyApplAcceptedLifeIns >= 0)
    {
      inputToRTPDealPart = formDealPartOfRTPString(deal, insureOnlyApplAcceptedLifeIns);
      inputToInfocal = formToInfocalDLLString(deal);

      logger.debug("LSIH@submitToDLL::DealPartOfRTP: " + inputToRTPDealPart);
      logger.debug("LSIH@submitToDLL::InfocalString: " + inputToInfocal);
    }
    **/
    //// 9. Finish formation of all Strings with Insure Only Applicants to RTP/Infocal DLLs and pass them/get response
    //// from Infocal.
    if(hdInsureOnlyApplicantId != null && hdInsureOnlyApplicantId.size() > 0)
    {
      // There are multiple row values in this display field
      int vectorSize = hdInsureOnlyApplicantId.size();
      for(int i = 0; i < vectorSize; i++)
      {
        insureOnlyApplicantId = com.iplanet.jato.util.TypeConverter.asInt(hdInsureOnlyApplicantId.get(i), -1);

        if(insureOnlyApplicantId >= 0)
        {
          InsureOnlyApplicant ioa = new InsureOnlyApplicant(srk, dcm);
          ioa = ioa.findByPrimaryKey(new InsureOnlyApplicantPK(insureOnlyApplicantId, theCopyId));

//// Temp debug. Should stay until the legacy connection done.
logger.debug("LSIH@submitToDLL::InsureOnlyApplicantIdHidden[" + i + "]: " + insureOnlyApplicantId);
logger.debug("LSIH@submitToDLL::CopyId[" + i + "]: " + copyId);
logger.debug("LSIH@submitToDLL::RbDisInsInsureOnlyApplicant[" + i + "]: " + rbInsureOnlyApplDisIns.get(i).toString());
logger.debug("LSIH@submitToDLL::RbLifeInsInsureOnlyApplicant[" + i + "]: " + rbInsureOnlyApplLifeIns.get(i).toString());
logger.debug("LSIH@submitToDLL::InsureOnlyApplicantGenderCombo[" + i + "]: " + cbInsureOnlyApplGender.get(i).toString());
logger.debug("LSIH@submitToDLL::InsureOnlyApplicantSmokerCombo[" + i + "]: " + cbInsureOnlyApplSmoker.get(i).toString());
logger.debug("LSIH@submitToDLL::InsureOnlyApplicantFirstName[" + i + "]: " + txInsureOnlyApplFirstName.get(i).toString());
logger.debug("LSIH@submitToDLL::InsureOnlyApplicantLastName[" + i + "]: " + txInsureOnlyApplLastName.get(i).toString());
logger.debug("LSIH@submitToDLL::InsureOnlyApplicantDOBMonth[" + i + "]: " + cbInsureOnlyApplDOBMonth.get(i).toString());
logger.debug("LSIH@submitToDLL::InsureOnlyApplicantDOBDay[" + i + "]: " + txInsureOnlyApplDOBDay.get(i).toString());
logger.debug("LSIH@submitToDLL::InsureOnlyApplicantDOBYear[" + i + "]: " + txInsureOnlyApplDOBYear.get(i).toString());

          String disPercentInsuranceInsureOnlyAppl = "";

          if(rbInsureOnlyApplDisIns.get(i).toString().equals(Mc.LIFE_DISABILITY_STATUS_ACCEPTED))
          {
            disPercentInsuranceInsureOnlyAppl = calcDisabilityPercentInsurance(theLDInsProportions,
                                                                        lifePercentInsurance,
                                                                        generalNumOfInsurersAcceptedDis);
                                                                        //insureOnlyApplAcceptedDisIns);
          }
          else
          {
            disPercentInsuranceInsureOnlyAppl = Mc.DISABILITY_INS_ZERO_STRING;
          }

logger.debug("LSIH@submitToDLL::disPercentInsuranceInsureOnlyApplicant: " + disPercentInsuranceInsureOnlyAppl);

          //--DJ_CR136--start--//
          InsureOnlyApplicantScreenInfoWrapper insureOnlyScreenInfo = new InsureOnlyApplicantScreenInfoWrapper();

          // a. repeatable
          insureOnlyScreenInfo.setSmokerStatus(cbInsureOnlyApplSmoker.get(i).toString());
          insureOnlyScreenInfo.setGenderId(cbInsureOnlyApplGender.get(i).toString());
          insureOnlyScreenInfo.setLifeInsStatus(rbInsureOnlyApplLifeIns.get(i).toString());
          insureOnlyScreenInfo.setDisabilityInsStatus(rbInsureOnlyApplDisIns.get(i).toString());
          insureOnlyScreenInfo.setInsureOnlyFirstName(txInsureOnlyApplFirstName.get(i).toString());
          insureOnlyScreenInfo.setInsureOnlyLastName(txInsureOnlyApplLastName.get(i).toString());
          insureOnlyScreenInfo.setInsureOnlyDOBMonth(cbInsureOnlyApplDOBMonth.get(i).toString());
          insureOnlyScreenInfo.setInsureOnlyDOBDay(txInsureOnlyApplDOBDay.get(i).toString());
          insureOnlyScreenInfo.setInsureOnlyDOBYear(txInsureOnlyApplDOBYear.get(i).toString());

          int numOfMonths = new Integer(cbInsureOnlyApplDOBMonth.get(i).toString()).intValue();
          int numOfDays = new Integer(txInsureOnlyApplDOBDay.get(i).toString()).intValue();
          int numOfYears = new Integer(txInsureOnlyApplDOBYear.get(i).toString()).intValue();

          java.util.Date insureOnlyDateOfBirth = new java.util.Date( numOfYears - 1900,
                                                                     numOfMonths - 1,
                                                                     numOfDays);

logger.debug("LSIH@submitToDLL::IOADateOfBirth[" + i + "]: " + insureOnlyDateOfBirth.toString());

          int ioaAge = getInsureOnlyAge( insureOnlyDateOfBirth );

logger.debug("LSIH@submitToDLL::IOAAge[" + i + "]: " + ioaAge);

          insureOnlyScreenInfo.setInsureOnlyDateOfBirth( insureOnlyDateOfBirth.toString() );
          insureOnlyScreenInfo.setInsureOnlyApplicantAge(new Integer(ioaAge).toString());

          // b. from the page header
          insureOnlyScreenInfo.setLifePercentInsurance(lifePercentInsuranceCalc);
          insureOnlyScreenInfo.setDisabilityPercentInsurance(disPercentInsuranceInsureOnlyAppl);

          // form vectors.
          insureOnlyGenderIds.addElement(cbInsureOnlyApplGender.get(i));
          insureOnlySmokerIds.addElement(cbInsureOnlyApplSmoker.get(i));
          insureOnlyLifeInsuranceIds.addElement(rbInsureOnlyApplLifeIns.get(i).toString());
          insureOnlyDisInsuranceIds.addElement(rbInsureOnlyApplDisIns.get(i).toString());
          insureOnlyFirstNames.addElement(txInsureOnlyApplFirstName.get(i).toString());
          insureOnlyLastNames.addElement(txInsureOnlyApplLastName.get(i).toString());
          insureOnlyDOBMonths.addElement(cbInsureOnlyApplDOBMonth.get(i).toString());
          insureOnlyDOBDays.addElement(txInsureOnlyApplDOBDay.get(i).toString());
          insureOnlyDOBYears.addElement(txInsureOnlyApplDOBYear.get(i).toString());
          insureOnlyDateOfBirths.addElement(insureOnlyDateOfBirth);
          insureOnlyAges.addElement(new Integer(ioaAge).toString());

          insureOnlyLifePercCoverage.addElement(lifePercentInsurance);
          //--DJ_CR136--end--//

          //--Ticket#1294--13May2005--start--//
          insureOnlyDisPercCoverage.addElement(disPercentInsuranceInsureOnlyAppl);
          //--Ticket#1294--13May2005--end--//

          if(rbInsureOnlyApplLifeIns.get(i).toString().equals(Mc.LIFE_DISABILITY_STATUS_ACCEPTED))
          {
            inputToRTPRepeatedPart += formInsureOnlyPartOfRTPString(ioa, insureOnlyScreenInfo);
            countApplicantAcceptLifeIns += 1;

logger.debug("LSIH@submitToDLL::inputToRTPRepeatedPartInsureApplicantOnly: " + inputToRTPRepeatedPart);
logger.debug("LSIH@submitToDLL::countInsureApplicantOnlyAcceptedLifeIns: " + countApplicantAcceptLifeIns);
          }
        } //if
      } //for

      //// Save updated values and pass them for the final update after getting info from
      //// RTP/Infocal calc engine.
      pst.put("INSURE_ONLY_GENDERS", insureOnlyGenderIds);
      pst.put("INSURE_ONLY_SMOKER_STAT", insureOnlySmokerIds);
      pst.put("INSURE_ONLY_LIFE_INS_IDS", insureOnlyLifeInsuranceIds);
      pst.put("INSURE_ONLY_DISABILITY_INS_IDS", insureOnlyDisInsuranceIds);
      pst.put("INSURE_ONLY_LIFE_PERC_COVERAGE", insureOnlyLifePercCoverage);
      pst.put("INSURE_ONLY_FIRST_NAMES", insureOnlyFirstNames);
      pst.put("INSURE_ONLY_LAST_NAMES", insureOnlyLastNames);
      pst.put("INSURE_ONLY_DOB_MONTHS", insureOnlyDOBMonths);
      pst.put("INSURE_ONLY_DOB_DAYS", insureOnlyDOBDays);
      pst.put("INSURE_ONLY_DOB_YEARS", insureOnlyDOBYears);
      pst.put("INSURE_ONLY_DATE_OF_BIRTH", insureOnlyDateOfBirths);
      pst.put("INSURE_ONLY_AGE", insureOnlyAges);

      //--Ticket#1294--13May2005--start--//
      pst.put("INSURE_ONLY_DISABILITY_PERC_COVERAGE", insureOnlyDisPercCoverage);
      //--Ticket#1294--13May2005--end--//
    } //if
    //--DJ_CR136--end--//

    //// II. Pass the formatted Strings with Deal/Borrowers/Screen into to RTP/Infocal.
    inputToRTP = inputToRTPDealPart + inputToRTPRepeatedPart;
logger.debug("LSIH@submitToDLL::RTP_String: " + inputToRTP);

    //// III. Submit formatted Strings to RTP/Infocal calc engine and get response from Infocal
    theSessionState.setInputToRTPCalc(inputToRTP);
    theSessionState.setInputToInfocalCalc(inputToInfocal);

    logger.debug("LSIH@submitToDLL::RTPFromSession: " + theSessionState.getInputToRTPCalc());
    logger.debug("LSIH@submitToDLL:InfocalFromSession: " + theSessionState.getInputToInfocalCalc());

    //// Display the 'NoCustomDialog' type message in order to hide the page reload
    //// and eliminate the classical active dialog type of express with 'OK' and 'Cancel'
    //// buttons in order to push data to the server via JS server hit simulator.
    //// Without this hit the data from obtained from the DLL are not in sync.
    //// IMPORTANT! Be carefull to change sequence of events here. Visually it can look like
    //// everything is okay, but for the first invocation hiddin output from Infocal is empty
    //// and each 'Recalc' hit brings the digits one step behind. Even JS alert debug messages
    //// breaks this.
    //// It is MUCH more convenient and easily to do it with the ActiveMessage layer, but
    //// Product forced to remove this extra communication layer with client.
    ActiveMessage am = null;
    am = setActiveMessageToAlert(ActiveMsgFactory.ISNODIALOG);

    am.setDialogMsg(BXResources.getSysMsg("SUBMIT_DATA_TO_RTP_INFOCAL_ENGINE", theSessionState.getLanguageId()));
    am.setGenerate(true);

    hitServer(thePage);

    return 0;
  }

  private void hitServer(ViewBean thePage)
  {

    PageEntry pg = theSessionState.getCurrentPage();
    String theExtraHtml = "";

    // the OnChange string to be set on the fields
    Button bt;

    bt = (Button)thePage.getDisplayField("btRecalculate");
    theExtraHtml = "onLoad=\"javascript:performBtRecalcSubmit(\'OK,61\');\"";
    bt.setExtraHtml(theExtraHtml);

    logger.debug("hitServer::ExtraHtml: " + bt.getExtraHtml());
  }

  public void handleUpdateBorrowers(boolean decide, String[] args)
  {
    logger.debug("@LDH.handleUpdateBorrowers: simulate the db population");

    PageEntry pg = (PageEntry)theSessionState.getCurrentPage();
    SessionResourceKit srk = getSessionResourceKit();
    CalcMonitor dcm = CalcMonitor.getMonitor(srk);
    SysLogger logger = srk.getSysLogger();

    logger.trace("--T--> @LDH.handleUpdateBorrowers: " + ((args != null && args[0] != null) ? args[0] : "No args"));

    if(args[0].equals("OK"))
    {
      try
      {
        updateBorrowersLDInsurance(pg, srk, dcm);
      }
      catch(Exception e)
      {
        logger.debug("LDH@handleUpdateBorrowers:Exception: " + e);
      }
      return;
    }
  }

  private int updateBorrowersLDInsurance(PageEntry pg, SessionResourceKit srk, CalcMonitor dcm) throws Exception
  {
    Hashtable pst = pg.getPageStateTable();

    //int statusId = 0;
    int borrowerId = 0;
    int insureOnlyApplicantId = 0;
    int copyId = pg.getPageDealCID();

    //int theDealId = pg.getPageDealId();
    int theCopyId = pg.getPageDealCID();
    ViewBean thePage = getCurrNDPage();

    int theLDInsProportions = 0;
    //String inputToRTPRepeatedPart = "";

    //// Number of Borrowers can not be changed on this page!!
    Vector hdBorrowerId = getRepeatedFieldValues("Repeated1/hdBorrowerId");

    //--DJ_CR136--start--//
    Vector hdInsureOnlyApplicantId = getRepeatedFieldValues("Repeated2/hdInsureOnlyApplicantId");
    //--DJ_CR136--end--//

    //// Get all Vectors with possibly updated values (comparing with hiddens)
    Vector borrGender = null;
    Object obj = pst.get("BORR_GENDERS");
    if(obj instanceof Vector)
    {
      borrGender = (Vector)obj;
    }
    else
    {
      borrGender = new Vector();
    }
    Vector borrSmoker = null;
    obj = pst.get("BORR_SMOKER_STAT");
    if(obj instanceof Vector)
    {
      borrSmoker = (Vector)obj;
    }
    else
    {
      borrSmoker = new Vector();
    }
    Vector borrLifeIns = null;
    obj = pst.get("LIFE_INS_IDS");
    if(obj instanceof Vector)
    {
      borrLifeIns = (Vector)obj;
    }
    else
    {
      borrLifeIns = new Vector();
    }
    Vector borrDisIns = null;
    obj = pst.get("DISABILITY_INS_IDS");
    if(obj instanceof Vector)
    {
      borrDisIns = (Vector)obj;
    }
    else
    {
      borrDisIns = new Vector();
    }
    Vector lifePercCov = null;
    obj = pst.get("LIFE_PERC_COVERAGE");
    if(obj instanceof Vector)
    {
      lifePercCov = (Vector)obj;
    }
    else
    {
      lifePercCov = new Vector();
    }

    //--Ticket#1294--13May2005--start--//
    Vector disPercentCov = null;
    obj = pst.get("DISABILITY_PERC_COVERAGE");
    if(obj instanceof Vector)
    {
      disPercentCov = (Vector)obj;
    }
    else
    {
      disPercentCov = new Vector();
    }
    //--Ticket#1294--13May2005--end--//


    //--DJ_CR136--start--//
    //// Insure Only Apllicant type part.
    Vector insureOnlyGenderIds = null;
    obj = pst.get("INSURE_ONLY_GENDERS");
    if(obj instanceof Vector){ insureOnlyGenderIds = (Vector)obj; }
    else{ insureOnlyGenderIds = new Vector(); }

    Vector insureOnlySmokerIds = null;
    obj = pst.get("INSURE_ONLY_SMOKER_STAT");
    if(obj instanceof Vector){ insureOnlySmokerIds = (Vector)obj; }
    else{ insureOnlySmokerIds = new Vector(); }

    Vector insureOnlyLifeInsuranceIds = null;
    obj = pst.get("INSURE_ONLY_LIFE_INS_IDS");
    if(obj instanceof Vector){ insureOnlyLifeInsuranceIds = (Vector)obj; }
    else{ insureOnlyLifeInsuranceIds = new Vector(); }

    Vector insureOnlyDisInsuranceIds = null;
    obj = pst.get("INSURE_ONLY_DISABILITY_INS_IDS");
    if(obj instanceof Vector){ insureOnlyDisInsuranceIds = (Vector)obj; }
    else{ insureOnlyDisInsuranceIds = new Vector(); }

    Vector insureOnlyLifePercCoverage = null;
    obj = pst.get("INSURE_ONLY_LIFE_PERC_COVERAGE");
    if(obj instanceof Vector){ insureOnlyLifePercCoverage = (Vector)obj; }
    else{ insureOnlyLifePercCoverage = new Vector(); }

    Vector insureOnlyFirstNames = null;
    obj = pst.get("INSURE_ONLY_FIRST_NAMES");
    if(obj instanceof Vector){
      insureOnlyFirstNames = (Vector)obj;
    }
    else{ insureOnlyFirstNames = new Vector(); }

    Vector insureOnlyLastNames = null;
    obj = pst.get("INSURE_ONLY_LAST_NAMES");
    if(obj instanceof Vector){ insureOnlyLastNames = (Vector)obj; }
    else{ insureOnlyLastNames = new Vector(); }

    Vector insureOnlyDOBMonths = null;
    obj = pst.get("INSURE_ONLY_DOB_MONTHS");
    if(obj instanceof Vector){ insureOnlyDOBMonths = (Vector)obj; }
    else{ insureOnlyDOBMonths = new Vector(); }

    Vector insureOnlyDOBDays = null;
    obj = pst.get("INSURE_ONLY_DOB_DAYS");
    if(obj instanceof Vector){ insureOnlyDOBDays = (Vector)obj; }
    else{ insureOnlyDOBDays = new Vector(); }

    Vector insureOnlyDOBYears = null;
    obj = pst.get("INSURE_ONLY_DOB_YEARS");
    if(obj instanceof Vector){ insureOnlyDOBYears = (Vector)obj; }
    else{ insureOnlyDOBYears = new Vector(); }

    Vector insureOnlyDateOfBirths = null;
    obj = pst.get("INSURE_ONLY_DATE_OF_BIRTH");
    if(obj instanceof Vector){ insureOnlyDateOfBirths = (Vector)obj; }
    else{ insureOnlyDateOfBirths = new Vector(); }

    Vector insureOnlyAges = null;
    obj = pst.get("INSURE_ONLY_AGE");
    if(obj instanceof Vector){ insureOnlyAges = (Vector)obj; }
    else{ insureOnlyAges = new Vector(); }
    //--DJ_CR136--end--//

    //--Ticket#1294--13May2005--start--//
    Vector insureOnlyDisPercCoverage = null;
    obj = pst.get("INSURE_ONLY_DISABILITY_PERC_COVERAGE");
    if(obj instanceof Vector){ insureOnlyDisPercCoverage = (Vector)obj; }
    else{ insureOnlyDisPercCoverage = new Vector(); }
    //--Ticket#1294--13May2005--end--//

    ////I. Get InsuranceOptionsId.
    theLDInsProportions = new Integer((String)pst.get("INSURANCE_OPTION")).intValue();

    Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

    String hdOutputFromInfocal = thePage.getDisplayFieldValue("hdOutputFromRTPInfocalCalc").toString();
    logger.error("LSIH@updateBorrowersLDInsurance::OutputFromInfocalHidden: " + hdOutputFromInfocal);

    //// IV. Parse and populate the page parameters with obtained values.
    parseInfocalResult(hdOutputFromInfocal);

    //// V. Display Error Message if problem occured.
    ////Product Team doesn't want to see the page without the error.
    if(new Integer(pst.get(Mc.DJLD_DLL_CALC_ERROR_CODE).toString()).intValue() == 0)
    {
      ////setActiveMessageToAlert(BXResources.getSysMsg("RTP_ERROR_CODE_ZERO", theSessionState.getLanguageId()),
      ////      ActiveMsgFactory.ISCUSTOMCONFIRM);

      // Set to display 'Submit' button.
      if(disableEditButton(pg) == true)
      {
        theSessionState.setIsDisplayLifeDisabilitySubmit(false);
      }
      else
      {
        theSessionState.setIsDisplayLifeDisabilitySubmit(true);
      }
    }
    else if(new Integer(pst.get(Mc.DJLD_DLL_CALC_ERROR_CODE).toString()).intValue() != 0)
    {
      String msgLabel = convertRPTErrorCodeToErrorLabel(new Integer(pst.get(Mc.DJLD_DLL_CALC_ERROR_CODE).toString()).intValue());
      setActiveMessageToAlert(BXResources.getSysMsg(msgLabel, theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);

      // Set not to display 'Submit' button
      ////pg.setPageCondition1(false);
      theSessionState.setIsDisplayLifeDisabilitySubmit(false);
    }

    //// V'. Update new deal entites here (move from borrower level).
    //// The logic is implemented in HomeBase and it is:
    //// LifeInsurance = RRG - RVS if Life and Disability is Accepted for at least one applicant.
    //// LifeInsurance = RRG - RSA if Life only (no Disability accepted for at least one applicant). Only Rejected or Ineligible.
    //// DisabilityInsurance = RVS - RSA if Life and Disability is Accepted for at least one applicant.
    //// DisabilityInsurance = 0 if Life only (no Disability accepted for at least one applicant). Only Rejected or Ineligible.
    double lifeInsPremium = 0.0;
    double disabilityInsPremium = 0.0;

    //--DJ_Ticket#499--start//
    //double creditCosts = 0.0;
    //double addCreditCostsByIns = 0.0;
    //double totalCreditCosts = 0.0;
    //--DJ_Ticket#499--end//

    //--> If RVS > 0 : LifePremium = RVS - RSA ; DisabilityPremium = RRG - RVS
    //--> if RVS = 0 and RSA > 0 : LifePremium = RRG - RSA ; DisabilityPremium = 0
    //--> if RVS = 0 and RSA = 0 : LifePremium = 0 ; DisabilityPremium = 0
    if(!pst.get(Mc.DJLD_PARAMETR_TO_FIND_LIFE_PREMIUM).toString().equals(Mc.RVS_DOUBLE_ZERO_STRING))
    {
      // RVS > 0
      lifeInsPremium = new Double(pst.get(Mc.DJLD_PARAMETR_TO_FIND_LIFE_PREMIUM).toString()).doubleValue() -
        new Double(pst.get(Mc.DJLD_DISABILITY_INSURANCE_PREMIUM).toString()).doubleValue();

      disabilityInsPremium = new Double(pst.get(Mc.DJLD_PAYMENT_PLUS_LIFE).toString()).doubleValue() -
        new Double(pst.get(Mc.DJLD_PARAMETR_TO_FIND_LIFE_PREMIUM).toString()).doubleValue();
    }
    else
    {
      //--> RVS = 0
      //-- Check if RSA = 0
      if(!pst.get(Mc.DJLD_DISABILITY_INSURANCE_PREMIUM).toString().equals(Mc.RVS_DOUBLE_ZERO_STRING))
      {
        // RSA > 0
        lifeInsPremium = new Double(pst.get(Mc.DJLD_PAYMENT_PLUS_LIFE).toString()).doubleValue() -
          new Double(pst.get(Mc.DJLD_DISABILITY_INSURANCE_PREMIUM).toString()).doubleValue();
      }
      else
      {
        // RSA = 0
        lifeInsPremium = 0.0;
      }

      disabilityInsPremium = 0.0;
    }
    //============================================================================================================

    /*#DG448 guarantee no cent discrepancies
    double combinedPremium = lifeInsPremium + disabilityInsPremium;

    deal.setDisabilityPremium(MathUtil.round(disabilityInsPremium, 2));
    deal.setLifePremium(MathUtil.round(lifeInsPremium, 2));
    deal.setCombinedPremium(MathUtil.round(combinedPremium, 2)); */
    lifeInsPremium = MathUtil.round(lifeInsPremium, 2);
    disabilityInsPremium = MathUtil.round(disabilityInsPremium, 2);
    double combinedPremium = lifeInsPremium + disabilityInsPremium;
    deal.setDisabilityPremium(disabilityInsPremium);
    deal.setLifePremium(lifeInsPremium);
    deal.setCombinedPremium(combinedPremium);
    //#DG448 end

    deal.setInterestRateIncrement(new Double(pst.get(Mc.DJLD_INTEREST_RATE_INCREMENT_FOR_LD).toString()).doubleValue());
    deal.setInterestRateIncLifeDisability(deal.getNetInterestRate() +
      new Double(pst.get(Mc.DJLD_INTEREST_RATE_INCREMENT_FOR_LD).toString()).doubleValue());

//temp
logger.error("LSIH@updateBorrowersLDInsurance::DisabilityInsurancePremium_RSA: " +
  pst.get(Mc.DJLD_DISABILITY_INSURANCE_PREMIUM));
logger.error("LSIH@updateBorrowersLDInsurance::DJLD_PARAMETR_TO_FIND_LIFE_PREMIUM_RVS: " +
  pst.get(Mc.DJLD_PARAMETR_TO_FIND_LIFE_PREMIUM));
logger.error("LSIH@updateBorrowersLDInsurance::PaymentPlusLife_RRG: " + pst.get(Mc.DJLD_PAYMENT_PLUS_LIFE));

    //--DJ_Ticket#499--start//
logger.debug("LSIH@updateBorrowersLDInsurance::CreditCosts_FCI: " + pst.get(Mc.DJLD_CREDIT_COSTS));
logger.debug("LSIH@updateBorrowersLDInsurance::AddCreditCostsIncurByIns_FCA: " + pst.get(Mc.DJLD_ADD_CREDIT_COSTS_INCURRED_BY_INS));
logger.debug("LSIH@updateBorrowersLDInsurance::TotalCreditCosts_FCT: " + pst.get(Mc.DJLD_TOTAL_CREDIT_COSTS));

    deal.setIntWithoutInsCreditCost(new Double(pst.get(Mc.DJLD_CREDIT_COSTS).toString()).doubleValue());
    deal.setAddCreditcostInsIncurred(new Double(pst.get(Mc.DJLD_ADD_CREDIT_COSTS_INCURRED_BY_INS).toString()).doubleValue());
    deal.setTotalCreditCost(new Double(pst.get(Mc.DJLD_TOTAL_CREDIT_COSTS).toString()).doubleValue());
    //--DJ_Ticket#499--end//

    //#DG448 deal.setPmntPlusLifeDisability(new Double(pst.get(Mc.DJLD_PAYMENT_PLUS_LIFE).toString()).doubleValue());
    deal.setPmntPlusLifeDisability(deal.getPandiPaymentAmount()+ combinedPremium);

    //// VI. Update all entities with screen/RTP&Infocal&Xpress calc results.
    //--DJ_CR136--start--//
    if((hdBorrowerId != null && hdBorrowerId.size() > 0) ||
       (hdInsureOnlyApplicantId != null && hdInsureOnlyApplicantId.size() > 0))
    //--DJ_CR136--end--//
    {
      // There are multiple row values in this display field
      // 1. First update borrower entities.
      int vectorSize = hdBorrowerId.size();
      for(int i = 0; i < vectorSize; i++)
      {
        borrowerId = com.iplanet.jato.util.TypeConverter.asInt(hdBorrowerId.get(i), -1);

        if(borrowerId >= 0)
        {
          Borrower borrower = new Borrower(srk, dcm);
          borrower = borrower.findByPrimaryKey(new BorrowerPK(borrowerId, theCopyId));

          LifeDisabilityPremiums ldp = new LifeDisabilityPremiums(srk, dcm);
          Collection lifeDisPremiums = ldp.findByBorrower(new BorrowerPK(borrowerId, theCopyId));

          // Update all LifeDisabilityPremiums entities.
          Iterator it = lifeDisPremiums.iterator();
          int lId = 0;
          while(it.hasNext())
          {
            ldp = (LifeDisabilityPremiums)it.next();
            lId = ldp.getLifeDisabilityPremiumsId();

//temp
logger.debug("LSIH@updateBorrowersLDInsurance::BorrowerStatus: " + borrower.isPrimaryBorrower());
logger.debug("LSIH@updateBorrowersLDInsurance::TestEntityBorId: " + ldp.getBorrowerId());
logger.debug("LSIH@updateBorrowersLDInsurance::TestEntityCopyID: " + ldp.getCopyId());
logger.debug("LSIH@updateBorrowersLDInsurance::TestEntityLDPID: " + ldp.getLifeDisabilityPremiumsId());

            /// 1. Update non-RTP/Infocal part first.
////temp
logger.debug("LSIH@updateBorrowersLDInsurance::BorrowerIdHidden[" + i + "]: " + borrowerId);
logger.debug("LSIH@updateBorrowersLDInsurance::CopyId[" + i + "]: " + copyId);
logger.debug("LSIH@updateBorrowersLDInsurance::LifePercCoverage[" + i + "]: " +
  new Double(lifePercCov.elementAt(i).toString()).doubleValue());
logger.debug("LSIH@updateBorrowersLDInsurance::RbDisabilityInsurance[" + i + "]: " +
  getLifeDisabilityId(borrDisIns.elementAt(i).toString()));
logger.debug("LSIH@updateBorrowersLDInsurance::RbLifeInsurance[" + i + "]: " +
  getLifeDisabilityId(borrLifeIns.elementAt(i).toString()));
logger.debug("LSIH@updateBorrowersLDInsurance::BorrowerGenderCombo[" + i + "]: " +
  (new Integer(borrGender.elementAt(i).toString())).intValue());
logger.debug("LSIH@updateBorrowersLDInsurance::BorrowerSmokerCombo[" + i + "]: " +
  (new Integer(borrSmoker.elementAt(i).toString())).intValue());
//--Ticket#1294--13May2005--start--//
logger.debug("LSIH@updateBorrowersLDInsurance::DisPercentCoverage[" + i + "]: " +
  new Double(disPercentCov.elementAt(i).toString()).doubleValue());
//--Ticket#1294--13May2005--end--//

            borrower.setLifeStatusId(getLifeDisabilityId(borrLifeIns.elementAt(i).toString()));
            borrower.setDisabilityStatusId(getLifeDisabilityId(borrDisIns.elementAt(i).toString()));
            borrower.setBorrowerGenderId((new Integer(borrGender.elementAt(i).toString())).intValue());
            borrower.setSmokeStatusId((new Integer(borrSmoker.elementAt(i).toString())).intValue());
            ldp.setLifeStatusId(getLifeDisabilityId(borrLifeIns.elementAt(i).toString()));
            ldp.setDisabilityStatusId(getLifeDisabilityId(borrDisIns.elementAt(i).toString()));
            borrower.setInsuranceProportionsId(theLDInsProportions);

////temp
logger.debug("LSIH@updateBorrowersLDInsurance::ErrorCode: " + pst.get(Mc.DJLD_DLL_CALC_ERROR_CODE));
logger.debug("LSIH@updateBorrowersLDInsurance::ResudialAmount: " + pst.get(Mc.DJLD_RESIDUAL_AMOUNT));
logger.debug("LSIH@updateBorrowersLDInsurance::EquivalentInterestRate: " + pst.get(Mc.DJLD_EQUIVALENT_INTEREST_RATE));
logger.debug("LSIH@updateBorrowersLDInsurance::InterestRateIncrement: " +
  pst.get(Mc.DJLD_INTEREST_RATE_INCREMENT_FOR_LD));
logger.debug("LSIH@updateBorrowersLDInsurance::LifePremium: " + pst.get(Mc.DJLD_LIFE_PREMIUM));
logger.debug("LSIH@updateBorrowersLDInsurance::DisabilityPremium: " + pst.get(Mc.DJLD_DISABILITY_PREMIUM));
logger.debug("LSIH@updateBorrowersLDInsurance::EquivalentRateInterestForSecondM: " +
  pst.get(Mc.DJLD_EQUIVALENT_INTEREST_RATE_FOR_SECOND_MORTGAGE));

logger.debug("LSIH@updateBorrowersLDInsurance::DisabilityInsurancePremium_RSA: " +
  pst.get(Mc.DJLD_DISABILITY_INSURANCE_PREMIUM));
logger.debug("LSIH@updateBorrowersLDInsurance::DJLD_PARAMETR_TO_FIND_LIFE_PREMIUM_RVS: " +
  pst.get(Mc.DJLD_PARAMETR_TO_FIND_LIFE_PREMIUM));
logger.debug("LSIH@updateBorrowersLDInsurance::PaymentPlusLife_RRG: " + pst.get(Mc.DJLD_PAYMENT_PLUS_LIFE));

            //--Ticket#1294--13May2005--start--//
            // Force to populate db with:
            //I. For LifePercentCoverage:
            // 1. 100 per cent if insurance proportions is 'Home Owner Plus' or 'Total'.
            // 2. 0 per cent if insurance proportions is 'Declined' or not defined.
            // 3. Calculated if insurance proportions is 'Regular' option.
            // II. For DisabilityPercentCoverage:
            // 1. 100 per cent if insurance proportions is 'Total'.
            // 2. 150 per cent if insurance proportions is 'Home Owner Plus'.
            // 3. 0 per cent if insurance proportions is 'Declined' or not defined.
            // 4. Calculated if insurance proportions is 'Regular' option.
            if(theLDInsProportions == Mc.DJ_TOTAL_OPTION )
            {
              borrower.setLifePercentCoverageReq(new Double(Mc.FULL_PERCENT_COVERAGE).doubleValue());
              borrower.setDisPercentCoverageReq(new Double(Mc.FULL_PERCENT_COVERAGE).doubleValue());
            }
            else if(theLDInsProportions == Mc.DJ_HOME_OWNER_PLUS_OPTION)
            {
              borrower.setLifePercentCoverageReq(new Double(Mc.FULL_PERCENT_COVERAGE).doubleValue());
              borrower.setDisPercentCoverageReq(new Double(Mc.HOME_OWNER_PLUS_DISABILITY_COVERAGE).doubleValue());
            }
            //--Ticket#1294--13May2005--end--//
            else if(theLDInsProportions == Mc.DJ_DECLINED_OPTION ||
                    theLDInsProportions == Mc.DJ_OPTION_NOT_DEFINED)
            {
              borrower.setLifePercentCoverageReq(new Double(Mc.ZERO_PERCENT_COVERAGE).doubleValue());
              //--Ticket#1294--13May2005--start--//
              borrower.setDisPercentCoverageReq(new Double(Mc.ZERO_PERCENT_COVERAGE).doubleValue());
              //--Ticket#1294--13May2005--end--//
            }
            else if(theLDInsProportions == Mc.DJ_REGULAR_OPTION)
            {
              borrower.setLifePercentCoverageReq(new Double(lifePercCov.elementAt(i).toString()).doubleValue());
              //--Ticket#1294--13May2005--start--//
              borrower.setDisPercentCoverageReq(new Double(disPercentCov.elementAt(i).toString()).doubleValue() * 100.00);
              //--Ticket#1294--13May2005--end--//
            }

            //deal.setPmntPlusLifeDisability(new Double(pst.get(Mc.DJLD_PAYMENT_PLUS_LIFE).toString()).doubleValue());
          } //end while
          //// When RTP/Infocal calc engine successfully envoked/completed 'Submit' should act as non-modified.
          ldp.ejbStore();
          borrower.ejbStore();
          //--DJ_CR136--start--//
          //deal.ejbStore();
          //--DJ_CR136--end--//
        } // end inner if
      } // end for

logger.debug("LSIH@updateLDInsuranceIOA::insureOnlyFirstNamesSize: " + insureOnlyFirstNames.size());
logger.debug("LSIH@updateLDInsuranceIOA::hdInsureOnlyApplicantIdSize: " + hdInsureOnlyApplicantId.size());

      // 2. First update insure only applicants entities.
      vectorSize = hdInsureOnlyApplicantId.size();
      for(int i = 0; i < vectorSize; i++)
      {
        insureOnlyApplicantId = com.iplanet.jato.util.TypeConverter.asInt(hdInsureOnlyApplicantId.get(i), -1);
logger.debug("LSIH@updateLDInsuranceIOA::InsureOnlyApplicantId: " + insureOnlyApplicantId);

        if(insureOnlyApplicantId >= 0)
        {
          InsureOnlyApplicant ioa = new InsureOnlyApplicant(srk, dcm);
          ioa = ioa.findByPrimaryKey(new InsureOnlyApplicantPK(insureOnlyApplicantId, theCopyId));

logger.debug("LSIH@updateLDInsuranceIOA::NewPK_Id: " + ioa.getPk().getId());
logger.debug("LSIH@updateLDInsuranceIOA::NewPK_Name: " + ioa.getPk().getName());
logger.debug("LSIH@updateLDInsuranceIOA::NewPK_WhereClause: " + ioa.getPk().getWhereClause());

          LifeDisPremiumsIOnlyA ldpioa = new LifeDisPremiumsIOnlyA(srk, dcm);
          Collection lifeDisPremiumsIOA = ldpioa.findByInsureOnlyApplicant(new InsureOnlyApplicantPK(insureOnlyApplicantId, theCopyId));

          // Update all LifeDisabilityPremiums entities.
          Iterator itioa = lifeDisPremiumsIOA.iterator();
          int lId = 0;
          while(itioa.hasNext())
          {
            ldpioa = (LifeDisPremiumsIOnlyA)itioa.next();
            lId = ldpioa.getLifeDisPremiumsIOnlyAId();

//temp
logger.debug("LSIH@updateLDInsuranceIOA::EntityIOAId: " + ldpioa.getInsureOnlyApplicantId());
logger.debug("LSIH@updateLDInsuranceIOA::EntityCopyID: " + ldpioa.getCopyId());
logger.debug("LSIH@updateLDInsuranceIOA::EntityLDPID: " + ldpioa.getLifeDisPremiumsIOnlyAId());

            /// 1. Update non-RTP/Infocal part first.
////temp
logger.debug("LSIH@updateLDInsuranceIOA::IdHidden[" + i + "]: " + insureOnlyApplicantId);
logger.debug("LSIH@updateLDInsuranceIOA::CopyId[" + i + "]: " + copyId);
logger.debug("LSIH@updateLDInsuranceIOA::LifePercCoverage[" + i + "]: " +
  new Double(insureOnlyLifePercCoverage.elementAt(i).toString()).doubleValue());
logger.debug("LSIH@updateLDInsuranceIOA::RbDisabilityInsurance[" + i + "]: " +
  getLifeDisabilityId(insureOnlyDisInsuranceIds.elementAt(i).toString()));
logger.debug("LSIH@updateLDInsuranceIOA::RbLifeInsurance[" + i + "]: " +
  getLifeDisabilityId(insureOnlyLifeInsuranceIds.elementAt(i).toString()));
logger.debug("LSIH@updateLDInsuranceIOA::IOAGenderCombo[" + i + "]: " +
  (new Integer(insureOnlyGenderIds.elementAt(i).toString())).intValue());
logger.debug("LSIH@updateLDInsuranceIOA::IOASmokerCombo[" + i + "]: " +
  (new Integer(insureOnlySmokerIds.elementAt(i).toString())).intValue());
logger.debug("LSIH@updateLDInsuranceIOA::IOAFirstName[" + i + "]: " +
  insureOnlyFirstNames.elementAt(i));
logger.debug("LSIH@updateLDInsuranceIOA::IOALastName[" + i + "]: " +
  insureOnlyLastNames.elementAt(i));
logger.debug("LSIH@updateLDInsuranceIOA::IOADOBMonth[" + i + "]: " +
  insureOnlyDOBMonths.elementAt(i).toString());
logger.debug("LSIH@updateLDInsuranceIOA::IOADOBDay[" + i + "]: " +
  insureOnlyDOBDays.elementAt(i).toString());
logger.debug("LSIH@updateLDInsuranceIOA::IOADOBYear[" + i + "]: " +
  insureOnlyDOBYears.elementAt(i).toString());
logger.debug("LSIH@updateLDInsuranceIOA::IOAAge[" + i + "]: " +
  (new Integer(insureOnlyAges.elementAt(i).toString())).intValue());
logger.debug("LSIH@updateLDInsuranceIOA::DateOfBirth[" + i + "]: " +
  ((java.util.Date)insureOnlyDateOfBirths.elementAt(i)).toString());

//--Ticket#1294--13May2005--start--//
logger.debug("LSIH@updateLDInsuranceIOA::DisabilityPercCoverage[" + i + "]: " +
  new Double(insureOnlyDisPercCoverage.elementAt(i).toString()).doubleValue());
//--Ticket#1294--13May2005--end--//

            ioa.setLifeStatusId(getLifeDisabilityId(insureOnlyLifeInsuranceIds.elementAt(i).toString()));
            ioa.setDisabilityStatusId(getLifeDisabilityId(insureOnlyDisInsuranceIds.elementAt(i).toString()));
            ioa.setInsureOnlyApplicantGenderId((new Integer(insureOnlyGenderIds.elementAt(i).toString())).intValue());

            ioa.setSmokeStatusId((new Integer(insureOnlySmokerIds.elementAt(i).toString())).intValue());
            ioa.setInsureOnlyApplicantFirstName(insureOnlyFirstNames.elementAt(i).toString());
            ioa.setInsureOnlyApplicantLastName(insureOnlyLastNames.elementAt(i).toString());
            ioa.setInsureOnlyApplicantBirthDate((java.util.Date)insureOnlyDateOfBirths.elementAt(i));
            ioa.setInsureOnlyApplicantAge( (new Integer(insureOnlyAges.elementAt(i).toString())).intValue());
            ldpioa.setLifeStatusId(getLifeDisabilityId(insureOnlyLifeInsuranceIds.elementAt(i).toString()));
            ldpioa.setDisabilityStatusId(getLifeDisabilityId(insureOnlyDisInsuranceIds.elementAt(i).toString()));

            ioa.setInsuranceProportionsId(theLDInsProportions);
            //--Ticket#1294--13May2005--start--//
            ioa.setDisPercentCoverageReq(new Double(insureOnlyDisPercCoverage.elementAt(i).toString()).doubleValue() * 100.00);
            //--Ticket#1294--13May2005--end--//

          } //end while
          //// When RTP/Infocal calc engine successfully envoked/completed 'Submit' should act as non-modified.
          ldpioa.ejbStore();
          ioa.ejbStore();
        } // end inner if
      } // end for

      ////deal.setPmntPlusLifeDisability(new Double(pst.get(Mc.DJLD_PAYMENT_PLUS_LIFE).toString()).doubleValue());
      deal.ejbStore();
    }  // end outer if

    pg.setModified(false);
    logger.debug("LSIH@updateBorrowersLDInsurance::Set pageModified false");

    pg.setPageCondition3(true);
    logger.debug("LSIH@updateBorrowersLDInsurance::PageCondition3: " + pg.getPageCondition3());

    return 0;
  }

  private String formBorrowerPartOfRTPString(Borrower borrower, BorrowerScreenInfoWrapper screenInfo)
  {
logger.trace("formBorrowerPartOfRTPString(borrowerId=" + borrower.getBorrowerId() + ")");

    StringBuffer borrowerPartOfRTP = new StringBuffer();

//temp
logger.debug("LSIH@formBorrowerPartOfRTPString::BorrowerAge: " + borrower.getAge());
logger.debug("LSIH@formBorrowerPartOfRTPString::BorrowerGender: " + screenInfo.getGenderId());
logger.debug("LSIH@formBorrowerPartOfRTPString::BorrowerSmokerStatus: " + screenInfo.getSmokerStatus());
logger.debug("LSIH@formBorrowerPartOfRTPString::LifePercInsurance: " + screenInfo.getLifePercentInsurance());
logger.debug("LSIH@formBorrowerPartOfRTPString::DisabilityPercInsurance: " + screenInfo.getDisabilityPercentInsurance());
logger.debug("LSIH@formBorrowerPartOfRTPString::DisabilityStatus: " + screenInfo.getDisabilityInsStatus());

    borrowerPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_APPLICANT_AGE + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    borrowerPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + borrower.getAge() + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);

    borrowerPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_APPLICANT_GENDER + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    borrowerPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + screenInfo.getGenderId() +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);

    borrowerPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_APPLICANT_SMOKER_STATUS +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    borrowerPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + screenInfo.getSmokerStatus() +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);

    borrowerPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_REDUCED_LIFE_INDICATOR +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    borrowerPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + (new Integer(0)).toString() +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP); //// 0 is default value to RTP

    borrowerPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_APPLICANT_LIFE_PERCENT_INSURANCE +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    borrowerPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + screenInfo.getLifePercentInsurance() +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);

    borrowerPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_APPLICANT_DISABILITY_PERCENT_INSURANCE +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    borrowerPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + screenInfo.getDisabilityPercentInsurance() +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);

    borrowerPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_APPLICANT_DISABILITY_STATUS +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    borrowerPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + convertStatus(screenInfo.getDisabilityInsStatus()) +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_APPLICANT_SEP);

    logger.debug("LSIH@formBorrowerPartOfRTPString::BorrowerPart: " + borrowerPartOfRTP.toString());

    return borrowerPartOfRTP.toString();
  }

  //--DJ_CR136--start--//
  private int getInsureOnlyAge(java.util.Date date)
  {
    try
    {
      Calendar then = Calendar.getInstance();
      then.setTime(date);

      Calendar now =  Calendar.getInstance(); //time zone
      int age = now.get(Calendar.YEAR) - then.get(Calendar.YEAR);

//logger.debug("Before Convert:: Now = " + now + " Then = " + then);
      now.set(Calendar.HOUR, 0);
      now.set(Calendar.MINUTE, 0);
      now.set(Calendar.SECOND, 0);
      now.set(Calendar.MILLISECOND, 0);
      then.set(Calendar.HOUR, 0);
      then.set(Calendar.MINUTE, 0);
      then.set(Calendar.SECOND, 0);
      then.set(Calendar.MILLISECOND, 0);
      then.set(Calendar.YEAR, now.get(Calendar.YEAR));
//logger.debug("After Convert:: Now = " + now + " Then = " + then);
      if(!then.before(now) == true)
      {
logger.debug("Birthday not passed for this year ==> subtract by 1 !!");
       age--;
      }
logger.debug("InsureOnlyApplicant Age = " + age);
      return age;
    }
    catch(Exception e)
    {
       String msg = e.getMessage();
       logger.debug ("LDH@getInsureOnlyAge:: " + msg );
       return 0;
    }
  }

  private String formInsureOnlyPartOfRTPString(InsureOnlyApplicant ioa, InsureOnlyApplicantScreenInfoWrapper screenInfo)
  {
  logger.trace("formInsureOnlyPartOfRTPString(InsureOnlyApplicantId=" + ioa.getInsureOnlyApplicantId() + ")");

      StringBuffer insureOnlyPartOfRTP = new StringBuffer();

//temp
  logger.debug("LSIH@formInsureOnlyPartOfRTPString::Gender: " + screenInfo.getGenderId());
  logger.debug("LSIH@formInsureOnlyPartOfRTPString::SmokerStatus: " + screenInfo.getSmokerStatus());
  logger.debug("LSIH@formInsureOnlyPartOfRTPString::LifePercInsurance: " + screenInfo.getLifePercentInsurance());
  logger.debug("LSIH@formInsureOnlyPartOfRTPString::DisabilityPercInsurance: " + screenInfo.getDisabilityPercentInsurance());
  logger.debug("LSIH@formInsureOnlyPartOfRTPString::DisabilityStatus: " + screenInfo.getDisabilityInsStatus());
  logger.debug("LSIH@formInsureOnlyPartOfRTPString::IOAAge: " + screenInfo.getInsureOnlyApplicantAge());

      insureOnlyPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_APPLICANT_AGE + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
      insureOnlyPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + screenInfo.getInsureOnlyApplicantAge() + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);

    insureOnlyPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_APPLICANT_GENDER + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    insureOnlyPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + screenInfo.getGenderId() +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);

    insureOnlyPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_APPLICANT_SMOKER_STATUS +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    insureOnlyPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + screenInfo.getSmokerStatus() +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);

    insureOnlyPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_REDUCED_LIFE_INDICATOR +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    insureOnlyPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + (new Integer(0)).toString() +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP); //// 0 is default value to RTP

    insureOnlyPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_APPLICANT_LIFE_PERCENT_INSURANCE +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    insureOnlyPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + screenInfo.getLifePercentInsurance() +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);

    insureOnlyPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_APPLICANT_DISABILITY_PERCENT_INSURANCE +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    insureOnlyPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + screenInfo.getDisabilityPercentInsurance() +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);

    insureOnlyPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_APPLICANT_DISABILITY_STATUS +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    insureOnlyPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + convertStatus(screenInfo.getDisabilityInsStatus()) +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_APPLICANT_SEP);

    logger.debug("LSIH@formInsureOnlyPartOfRTPString::InsureOnlyPart: " + insureOnlyPartOfRTP.toString());

    return insureOnlyPartOfRTP.toString();
  }
  //--DJ_CR136--end--//

  private String formDealPartOfRTPString(Deal deal, int numOfApplAcceptedLifeInsurance)
  {
    logger.trace("formDealPartOfRTPString(dealId=" + deal.getDealId() + ")");

    StringBuffer dealPartOfRTP = new StringBuffer();

//temp
logger.debug("LSIH@formDealPartOfRTPString::DealId: " + deal.getDealId());
logger.debug("LSIH@formDealPartOfRTPString::NetLoanRate: " + deal.getNetInterestRate());
logger.debug("LSIH@formDealPartOfRTPString::TotalLoanAmount: " + deal.getTotalLoanAmount());
logger.debug("LSIH@formDealPartOfRTPString::ApplicationDateYear: " + deal.getApplicationDate().getYear());
logger.debug("LSIH@formDealPartOfRTPString::ApplicationDateMonth: " + deal.getApplicationDate().getMonth());
logger.debug("LSIH@formDealPartOfRTPString::ApplicationDateDay: " + deal.getApplicationDate().getDay());

    java.util.Date tmpApplicationDate = deal.getApplicationDate();
    logger.debug("LSIH@formDealPartOfRTPString::ApplicationDate: " + deal.getApplicationDate());

    //// Extract year, month, day first from java.util.Date
    GregorianCalendar grApplicationDate = new GregorianCalendar();
    grApplicationDate.setTime(tmpApplicationDate);

    int year = grApplicationDate.get(Calendar.YEAR);
    int month = grApplicationDate.get(Calendar.MONTH);
    int day = grApplicationDate.get(Calendar.DAY_OF_MONTH);


//temp
logger.debug("LSIH@formDealPartOfRTPString::GregorianTime: " + grApplicationDate.toString());
logger.debug("LSIH@formDealPartOfRTPString::GregorianTimeYear: " + year);
logger.debug("LSIH@formDealPartOfRTPString::GregorianTimeMonth: " + (month + 1));
logger.debug("LSIH@formDealPartOfRTPString::GregorianTimeDay: " + day);

    dealPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_TYPE_OF_LOAN + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    dealPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + (new Integer(1)).toString() +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP); //// 1 is default value to RTP

    dealPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_AMOUNT_OF_LOAN + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    dealPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + deal.getTotalLoanAmount() + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);

    dealPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_NUMBER_OF_APPLICANTS_ACCEPTED_LIFE +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    dealPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + numOfApplAcceptedLifeInsurance +
      Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);

    dealPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_APPLICATION_DATE_YEAR + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    dealPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + year + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);

    dealPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_APPLICATION_DATE_MONTH + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    dealPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + (month + 1) + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);

    dealPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_APPLICATION_DATE_DAY + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    dealPartOfRTP.append(Mc.DJLD_JS_ESCAPE_CHARACTER + day + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_REPEATPART_SEP);

    logger.debug("LSIH@formDealPartOfRTPString::DealPart: " + dealPartOfRTP.toString());

    return dealPartOfRTP.toString();

  }

  private String formToInfocalDLLString(Deal deal)
  {
    logger.trace("formToInfocalDLLString(dealId=" + deal.getDealId() + ")");

    StringBuffer infocal = new StringBuffer();
//temp
    logger.debug("LSIH@formToInfocalDLLString::DealId: " + deal.getDealId());
    logger.debug("LSIH@formToInfocalDLLString::NetLoanRate: " + deal.getNetInterestRate());
    logger.debug("LSIH@formToInfocalDLLString::TotalLoanAmount: " + deal.getTotalLoanAmount());
    logger.debug("LSIH@formToInfocalDLLString::AmortizationPeriod: " + deal.getAmortizationTerm());
    logger.debug("LSIH@formToInfocalDLLString::ActualPaymentTerm: " + deal.getActualPaymentTerm());
    logger.debug("LSIH@formToInfocalDLLString::InterestCompoundId: " + deal.getInterestCompoundId());
    logger.debug("LSIH@formToInfocalDLLString::PaymentFrequencyId: " + deal.getPaymentFrequencyId());

    infocal.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_COMPOUND_PERIOD + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);

    //// DJ wants to calculate compound based on the mtgRate interestCompoundId, not
    //// on deal interestCompoundId which is by default is '0' and defined in the InstitutionProfile
    //// table.
    int intCompoundId = 0; //default
    try
    {
      intCompoundId = deal.getMtgProd().getInterestCompoundingId();
    }
    catch(Exception e)
    {
       logger.error("LSIH@formToInfocalDLL:InterestCompoundId not found: " + e);
    }

    //if(deal.getInterestCompoundId() == 0)
    if(intCompoundId == 0)
    { // Semi-Annually
      infocal.append(Mc.DJLD_JS_ESCAPE_CHARACTER + "S" + DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);
    }
    //else if(deal.getInterestCompoundId() == 1)
    else if(intCompoundId == 1)
    { // Monthly
      infocal.append(Mc.DJLD_JS_ESCAPE_CHARACTER + "M" + DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);
    }

    infocal.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_NET_INTEREST_RATE + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    infocal.append(Mc.DJLD_JS_ESCAPE_CHARACTER + deal.getNetInterestRate() + DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);

    infocal.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_TOTAL_LOAN_AMOUNT + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    infocal.append(Mc.DJLD_JS_ESCAPE_CHARACTER + deal.getTotalLoanAmount() + DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);

    infocal.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_FCP + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    infocal.append(Mc.DJLD_JS_ESCAPE_CHARACTER + (new Integer(0)).toString() + DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP); //defaulted to 0.

    infocal.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_PAYMENT_FREQUENCY + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    String pmntFrequencyValue = getPmntFrequencyAbbr(deal.getPaymentFrequencyId());
    infocal.append(Mc.DJLD_JS_ESCAPE_CHARACTER + pmntFrequencyValue + DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);

    infocal.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_AMORTIZATION_PERIOD + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    String amortizationTermForInfocal = countTermForInfocal(deal.getAmortizationTerm(), deal.getPaymentFrequencyId());
    infocal.append(Mc.DJLD_JS_ESCAPE_CHARACTER + amortizationTermForInfocal + DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);

    infocal.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_ACTUAL_PMNT_TERM + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    String actualPmntTermForInfocal = countTermForInfocal(deal.getActualPaymentTerm(), deal.getPaymentFrequencyId());
    infocal.append(Mc.DJLD_JS_ESCAPE_CHARACTER + actualPmntTermForInfocal + DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP);

    infocal.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_REM + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    infocal.append(Mc.DJLD_JS_ESCAPE_CHARACTER + "R" + DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP); //defaulted to R.

    infocal.append(Mc.DJLD_JS_ESCAPE_CHARACTER + Mc.DJLD_FPV + Mc.DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA);
    infocal.append(Mc.DJLD_JS_ESCAPE_CHARACTER + "N" + DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP); //defaulted to N.

    logger.debug("LSIH@formToInfocalDLLString::Infocal: " + infocal.toString());

    return infocal.toString();
  }

  //// DJ doesn't distinguish accelerated and non-accelerated payments!!
  //// RTP/Infocal DLL calc engine calculates both Weekly and Accelerated Weekly
  //// frequences identically.
  //// IMPORTANT! DJ doesn't support semi-monthly payment frequency, thus there is
  //// no case Mc.PAY_FREQ_SEMIMONTHLY.
  private String getPmntFrequencyAbbr(int pmntFrequencyId)
  {
    String frequencyAbbr = "M"; //default;

    switch(pmntFrequencyId)
    {
      case Mc.PAY_FREQ_MONTHLY:
        frequencyAbbr = "M";
        break;
      case Mc.PAY_FREQ_BIWEEKLY:
        frequencyAbbr = "D";
        break;
      case Mc.PAY_FREQ_ACCELERATED_BIWEEKLY:
        frequencyAbbr = "D";
        break;
      case Mc.PAY_FREQ_WEEKLY:
        frequencyAbbr = "H";
        break;
      case Mc.PAY_FREQ_ACCELERATED_WEEKLY:
        frequencyAbbr = "H";
        break;
      default:
        frequencyAbbr = "";
        break;
    }

    return frequencyAbbr;
  }

  private int convertStatus(String disStatusSelected)
  {
    int disStatusId = 0; //default;
    logger.debug("LSIH@convertStatus::StatusIn: " + disStatusSelected);

    if(disStatusSelected.equals(Mc.LIFE_DISABILITY_STATUS_ACCEPTED))
    {
      disStatusId = 1;
    }
    else if(disStatusSelected.equals(Mc.LIFE_DISABILITY_STATUS_REFUSED))
    {
      disStatusId = 2;
    }
    else if(disStatusSelected.equals(Mc.LIFE_DISABILITY_STATUS_INELIGIBLE))
    {
      disStatusId = 3;
    }
    else
    {
      disStatusId = 0;
    }

    return disStatusId;
  }

  //// DJ doesn't distinguish accelerated and non-accelerated payments!!
  //// All terms' values (actual pmnt and amortization pmnt) should be
  //// recalculated and transformed into the RTP/Infocal format (with the
  //// paymentFrequency letter appended at the end, see above method).

  //// IMPORTANT!! Ticket#609--start--31Jan2005--//
  //// Bug fix for DJ. Weekly and Biweekly payment frequencies should be
  //// passed rounded to months.
  //// HomeBASE passes them to RTP/Infocal engine for L&D calculations with
  //// no rounding. Thus, for Amortization Period 12y and 5m , e.g.
  //// it brings run-time problem to HomeBase.
  ////
  private String countTermForInfocal(int termsInMonth, int pmntFrequencyId)
  {
    String termInInfocalFormat = ""; //default;
    //int precision = 0;
    // SEAN Ticket #1648 Sept 13, 2005: temp variable.
    double tmpTerm;
    // SEAN Ticket #1648 END

    switch(pmntFrequencyId)
    {
      case Mc.PAY_FREQ_MONTHLY:
        termInInfocalFormat = (new Integer(termsInMonth)).toString() + "M";
        break;
        // SEAN Ticket #1648 Sept 13, 2005: round up to integer value as express regular calc.
      case Mc.PAY_FREQ_BIWEEKLY:
          tmpTerm = (termsInMonth / 12.00) * 26.00;
          if (tmpTerm > Math.floor(tmpTerm)) tmpTerm += 1;
          termInInfocalFormat = (new Double(tmpTerm)).intValue() + "D";
        break;
      case Mc.PAY_FREQ_ACCELERATED_BIWEEKLY:
          tmpTerm = (termsInMonth / 12.00) * 26.00;
          if (tmpTerm > Math.floor(tmpTerm)) tmpTerm += 1;
          termInInfocalFormat = (new Double(tmpTerm)).intValue() + "D";
        break;
      case Mc.PAY_FREQ_WEEKLY:
          tmpTerm = (termsInMonth / 12.00) * 52.00;
          if (tmpTerm > Math.floor(tmpTerm)) tmpTerm += 1;
          termInInfocalFormat = (new Double(tmpTerm)).intValue() + "H";
        break;
      case Mc.PAY_FREQ_ACCELERATED_WEEKLY:
          tmpTerm = (termsInMonth / 12.00) * 52.00;
          if (tmpTerm > Math.floor(tmpTerm)) tmpTerm += 1;
          termInInfocalFormat = (new Double(tmpTerm)).intValue() + "H";
        break;
        // SEAN Ticket #1648 END
      default:
        termInInfocalFormat = "";
        break;
    }

logger.debug("LSIH@countTermForInfocal::TermInInfocalFormat: " + termInInfocalFormat);

    return termInInfocalFormat;
  }
  //// Ticket#609--end--31Jan2005--//

  private String calcLifePercentInsurance(int insuranceOptions, String lifePefcentCoverageReq)
  {
    double lifePercentInsurance = 0.0; //default;

    switch(insuranceOptions)
    {
      case Mc.DJ_OPTION_NOT_DEFINED:
        lifePercentInsurance = 0.0;
        break;
      case Mc.DJ_HOME_OWNER_PLUS_OPTION:
        lifePercentInsurance = (new Double(1).doubleValue()); // decimal value of 100%
        break;
      case Mc.DJ_TOTAL_OPTION:
        lifePercentInsurance = (new Double(1)).doubleValue(); // decimal value of 100%
        break;
      case Mc.DJ_REGULAR_OPTION:
        lifePercentInsurance = (new Double(lifePefcentCoverageReq)).doubleValue() / 100.00; // decimal value of entered %s.
        break;
      case Mc.DJ_DECLINED_OPTION:
        lifePercentInsurance = 0.0;
        break;
      default:
        lifePercentInsurance = 0.0;
        break;
    }

    return(new Double(lifePercentInsurance)).toString();
  }

  private String calcDisabilityPercentInsurance(int insuranceOptions, String disabilityPefcentCoverageReq,
    int numbOfApplAcceptedDisIns)
  {
    double disabilityPercentInsurance = 0.0; //default;

    switch(insuranceOptions)
    {
      case Mc.DJ_OPTION_NOT_DEFINED:
        disabilityPercentInsurance = 0.0;
        break;
      case Mc.DJ_HOME_OWNER_PLUS_OPTION:
        disabilityPercentInsurance = (new Double(1.5)).doubleValue(); // decimal value of 150%
        break;
      case Mc.DJ_TOTAL_OPTION:
        disabilityPercentInsurance = (new Double(1)).doubleValue(); // decimal value of 100%
        break;
      case Mc.DJ_REGULAR_OPTION:

        // decimal value of entered %s.
        double temp = (new Double(disabilityPefcentCoverageReq)).doubleValue() / 100.00;
        //--Ticket#906--start--//
        // To simulate the DJ calc results the much precise double should be passed to
        // RTP/Infocal calc engine.
        //temp = Math.round(temp * 100.00) / 100.00;
        temp = MathUtil.round(temp, 7);
        //--Ticket#906--end--//

        // decimal value of 1 devided by # of appl accepted life insurance
        double temp1 = (new Double(1)).doubleValue() / (new Double(numbOfApplAcceptedDisIns)).doubleValue();
        //--Ticket#906--start--//
        // To simulate the DJ calc results the much precise double should be passed to
        // RTP/Infocal calc engine.
        //temp1 = Math.round(temp * 100.00) / 100.00;
        temp1 = MathUtil.round(temp1, 7);
        //--Ticket#906--end--//

logger.debug("LSIH@calcDisabilityPercentInsurance::NUM_OF_ACCEPTED: " + numbOfApplAcceptedDisIns);
logger.debug("LSIH@calcDisabilityPercentInsurance::temp: " + temp);
logger.debug("LSIH@calcDisabilityPercentInsurance::temp1: " + temp1);

        if(temp1 > temp)
        {
          disabilityPercentInsurance = temp;
        }
        else
        {
          disabilityPercentInsurance = temp1;
        }

logger.debug("LSIH@calcDisabilityPercentInsurance::disabilityPercentInsurance: " + disabilityPercentInsurance);
        break;
      case Mc.DJ_DECLINED_OPTION:
        disabilityPercentInsurance = 0.0;
        break;
      default:
        disabilityPercentInsurance = 0.0;
        break;
    }

    return(new Double(disabilityPercentInsurance)).toString();
  }

  public void saveData(PageEntry pg, boolean calledInTransaction)
  {
    try
    {
      SessionResourceKit srk = getSessionResourceKit();
      CalcMonitor dcm = CalcMonitor.getMonitor(srk);

      logger.debug("LSIH@saveData::saveData:isModified_Before?: " + pg.isModified());

      if(calledInTransaction == false)
      {
        srk.beginTransaction();
      }

      if(submitBorrowersLDInsurance(pg, srk, dcm) < 0)
      {
        logger.error("LSIH@saveData::saveData:isModified_Submit?: " + pg.isModified());

        if(calledInTransaction == false)
        {
          srk.cleanTransaction();
        }

        if(pg.getPageCondition3() == true)
        {
          pg.setModified(false);
        }

        if(srk.getModified())
        {
          pg.setModified(true);
        }

        return;
      }

      logger.debug("LSIH@saveData::saveData_Stamp5:calledInTransaction: " + calledInTransaction);
      dcm.calc();

      if(calledInTransaction == false)
      {
        srk.commitTransaction();
      }

      if(pg.getPageCondition3() == true)
      {
        pg.setModified(false);
      }

      if(srk.getModified())
      {
        pg.setModified(true);
      }

      logger.debug("LDIH@saveData::saveData:isModified_NoSubmit?: " + pg.isModified());
    }
    catch(Exception e)
    {
      if(calledInTransaction == false)
      {
        srk.cleanTransaction();
      }

      logger.error("LSIH@saveData::Error in saveData()");
      logger.error(e);
      setStandardFailMessage();
      return;
    }
  }

  private int submitBorrowersLDInsurance(PageEntry pg, SessionResourceKit srk, CalcMonitor dcm) throws Exception
  {
    //int statusId = 0;
    int borrowerId = 0;
    int insureOnlyApplicantId = 0;
    int copyId = pg.getPageDealCID();

    Vector hdBorrowerId = getRepeatedFieldValues("Repeated1/hdBorrowerId");
    Vector rbDisabilityInsurance = getRepeatedFieldValues("Repeated1/rbDisabilityInsurance");
    Vector rbLifeInsurance = getRepeatedFieldValues("Repeated1/rbLifeInsurance");
    Vector cbBorrowerGender = getRepeatedFieldValues("Repeated1/cbBorrowerGender");
    Vector cbBorrowerSmoker = getRepeatedFieldValues("Repeated1/cbBorrowerSmoker");

    //--DJ_CR136--start--//
    Vector hdInsureOnlyApplicantId = getRepeatedFieldValues("Repeated2/hdInsureOnlyApplicantId");
    Vector rbInsureOnlyApplDisIns = getRepeatedFieldValues("Repeated2/rbDisabilityInsurance");
    Vector rbInsureOnlyApplLifeIns = getRepeatedFieldValues("Repeated2/rbLifeInsurance");
    Vector cbInsureOnlyApplGender = getRepeatedFieldValues("Repeated2/cbInsureOnlyApplicantGender");
    Vector cbInsureOnlyApplSmoker = getRepeatedFieldValues("Repeated2/cbInsureOnlyApplicantSmoker");
    Vector txInsureOnlyApplFirstName = getRepeatedFieldValues("Repeated2/txInsureOnlyApplicantFirstName");
    Vector txInsureOnlyApplLastName = getRepeatedFieldValues("Repeated2/txInsureOnlyApplicantLastName");
    Vector cbInsureOnlyApplDOBMonth = getRepeatedFieldValues("Repeated2/cbIOApplicantDOBMonth");
    Vector txInsureOnlyApplDOBDay = getRepeatedFieldValues("Repeated2/txIOApplicantDOBDay");
    Vector txInsureOnlyApplDOBYear = getRepeatedFieldValues("Repeated2/txIOApplicantDOBYear");
    //--DJ_CR136--end--//

    //int theDealId = pg.getPageDealId();
    int theCopyId = pg.getPageDealCID();
    ViewBean thePage = getCurrNDPage();

    int theLDInsProportions = 0;

    try
    {
      ComboBox cbLDInsProportions = (ComboBox)thePage.getDisplayField("cbInsProportions");
      String ldInsPropoprtionsValue = cbLDInsProportions.getValue().toString();
      logger.debug("LDIH@submitBorrowersLDInsurance::LDInsProportionsValue: " + ldInsPropoprtionsValue);

      theLDInsProportions = (new Integer(ldInsPropoprtionsValue)).intValue();
    }
    catch(Exception e)
    {
      logger.debug("LDIH@submitBorrowersLDInsurance::Exception getting LDInsProportions value: " + e);
    }

    //// 1. Get LifePercentCoverage Requested. IMPORTANT!
    // LifePercCoverageReq is moved to the logical Deal level (Header on the html page). On the other
    // hand, it is still a field in the Borrower table/entity. It is displayed for the 'Primary'
    // borrower now. Since the final specs have not signed by client yet it could be
    // 'return' to the original appoach (displaying for each borrower in the repeatable part).
    // Thus, the repeatable implementation is not removed, just commented out.
    TextField txLifePercCoverage = null;

    try
    {
      txLifePercCoverage = (TextField)thePage.getDisplayField("txLifePercCoverageReqReq");
      if(txLifePercCoverage.getValue() != null)
      {
        logger.debug("LDIH@submitBorrowersLDInsurance::LifePercCoverage: " +
                     txLifePercCoverage.getValue().toString());
      }
    }
    catch(Exception e)
    {
      logger.debug("LDIH@submitBorrowersLDInsurance::Exception getting LifePercCoverage value: " + e);
    }

    //--Ticket#1294--13May2005--start--//
    // Force to populate db with:
    //I. For LifePercentCoverage:
    // 1. 100 per cent if insurance proportions is 'Home Owner Plus' or 'Total'.
    // 2. 0 per cent if insurance proportions is 'Declined' or not defined.
    // 3. Calculated if insurance proportions is 'Regular' option.
    // II. For DisabilityPercentCoverage:
    // 1. 100 per cent if insurance proportions is 'Total'.
    // 2. 150 per cent if insurance proportions is 'Home Owner Plus'.
    // 3. 0 per cent if insurance proportions is 'Declined' or not defined.
    // 4. Calculated if insurance proportions is 'Regular' option.
    String lifePercentInsurance = "";
    String disPercentCoverage = "";

	// SEAN Ticket #1666&1675 Aug 26, 2005: Change typo error from
	// DJ_HOME_OWNER_PLUS_OPTION to DJ_TOTAL_OPTION
    if(theLDInsProportions == Mc.DJ_TOTAL_OPTION)
    {
      lifePercentInsurance = Mc.FULL_PERCENT_COVERAGE_STRING;
      disPercentCoverage = Mc.FULL_PERCENT_COVERAGE_STRING;
    }
    else if(theLDInsProportions == Mc.DJ_HOME_OWNER_PLUS_OPTION)
    {
      lifePercentInsurance = Mc.FULL_PERCENT_COVERAGE_STRING;
      disPercentCoverage = Mc.HOME_OWNER_PLUS_DISABILITY_COVERAGE_STRING;
    }
    else if(theLDInsProportions == Mc.DJ_DECLINED_OPTION ||
            theLDInsProportions == Mc.DJ_OPTION_NOT_DEFINED)
    {
      lifePercentInsurance = Mc.ZERO_PERCENT_COVERAGE_STRING;
      disPercentCoverage = Mc.ZERO_PERCENT_COVERAGE_STRING;
    }
    else if(theLDInsProportions == Mc.DJ_REGULAR_OPTION)
    {
      lifePercentInsurance = txLifePercCoverage.getValue().toString();
      disPercentCoverage = txLifePercCoverage.getValue().toString();
    }
    //--Ticket#1294--13May2005--end--//

    //--DJ_CR136--start--//
    if((hdBorrowerId != null && hdBorrowerId.size() > 0) ||
       (hdInsureOnlyApplicantId != null && hdInsureOnlyApplicantId.size() > 0))
    //--DJ_CR136--end--//
    {
      // There are multiple row values in this display field
      int vectorSize = hdBorrowerId.size();
      for(int i = 0; i < vectorSize; i++)
      {
        borrowerId = com.iplanet.jato.util.TypeConverter.asInt(hdBorrowerId.get(i), -1);

        if(borrowerId >= 0)
        {
          Borrower borrower = new Borrower(srk, dcm);
          borrower = borrower.findByPrimaryKey(new BorrowerPK(borrowerId, theCopyId));

          LifeDisabilityPremiums ldp = new LifeDisabilityPremiums(srk, dcm);
          Collection lifeDisPremiums = ldp.findByBorrower(new BorrowerPK(borrowerId, theCopyId));

          Iterator it = lifeDisPremiums.iterator();
          int lId = 0;
          while(it.hasNext())
          {
            ldp = (LifeDisabilityPremiums)it.next();
            lId = ldp.getLifeDisabilityPremiumsId();

logger.debug("LSIH@submitBorrowersLDInsurance::BorrowerIdHidden[" + i + "]: " + borrowerId);
logger.debug("LSIH@submitBorrowersLDInsurance::CopyId[" + i + "]: " + copyId);
////logger.debug("LSIH@submitBorrowersLDInsurance::RbDisabilityInsurance[" + i + "]: " + rbDisabilityInsurance.get(i).toString());
////logger.debug("LSIH@submitBorrowersLDInsurance::RbLifeInsurance[" + i + "]: " + rbLifeInsurance.get(i).toString());
////logger.debug("LSIH@submitBorrowersLDInsurance::BorrowerGenderCombo[" + i + "]: " + cbBorrowerGender.get(i).toString());
////logger.debug("LSIH@submitBorrowersLDInsurance::BorrowerSmokerCombo[" + i + "]: " + cbBorrowerSmoker.get(i).toString());

            if (rbLifeInsurance.get(i) != null) {
              borrower.setLifeStatusId(getLifeDisabilityId(rbLifeInsurance.get(i).toString()));
            }
            if (rbDisabilityInsurance.get(i) != null) {
              borrower.setDisabilityStatusId(getLifeDisabilityId(rbDisabilityInsurance.get(i).toString()));
            }
            borrower.setBorrowerGenderId(com.iplanet.jato.util.TypeConverter.asInt(cbBorrowerGender.get(i), 0));
            borrower.setSmokeStatusId(com.iplanet.jato.util.TypeConverter.asInt(cbBorrowerSmoker.get(i), 0));
            if (rbLifeInsurance.get(i) != null) {
              ldp.setLifeStatusId(getLifeDisabilityId(rbLifeInsurance.get(i).toString()));
            }
            if (rbDisabilityInsurance.get(i) != null) {
              ldp.setDisabilityStatusId(getLifeDisabilityId(rbDisabilityInsurance.get(i).toString()));
            }
            borrower.setInsuranceProportionsId(theLDInsProportions);
            borrower.setLifePercentCoverageReq(new Double(lifePercentInsurance).doubleValue());

          } //end while
          ldp.ejbStore();
          borrower.ejbStore();
        } // end inner if
      } // end for

      //--DJ_CR136--start--//
      // There are multiple row values in this display field
      vectorSize = hdInsureOnlyApplicantId.size();
logger.debug("LSIH@submitLDInsurance::HdAIOVectorSize: " + vectorSize);

      for(int i = 0; i < vectorSize; i++)
      {
        insureOnlyApplicantId = com.iplanet.jato.util.TypeConverter.asInt(hdInsureOnlyApplicantId.get(i), -1);

        if(insureOnlyApplicantId >= 0)
        {
          InsureOnlyApplicant ioa = new InsureOnlyApplicant(srk, dcm);
          ioa = ioa.findByPrimaryKey(new InsureOnlyApplicantPK(insureOnlyApplicantId, theCopyId));

          LifeDisPremiumsIOnlyA ldpioa = new LifeDisPremiumsIOnlyA(srk, dcm);
          Collection lifeDisPremiumsIOA = ldpioa.findByInsureOnlyApplicant(new InsureOnlyApplicantPK(insureOnlyApplicantId, theCopyId));

          Iterator itioa = lifeDisPremiumsIOA.iterator();
          int lioaId = 0;
          while(itioa.hasNext())
          {
            ldpioa = (LifeDisPremiumsIOnlyA)itioa.next();
            lioaId = ldpioa.getLifeDisPremiumsIOnlyAId();

logger.debug("LSIH@submitLDInsurance::IOAIdHidden[" + i + "]: " + insureOnlyApplicantId);
logger.debug("LSIH@submitLDInsurance::CopyId[" + i + "]: " + copyId);
//logger.debug("LSIH@submitLDInsurance::IOARbDisabilityInsurance[" + i + "]: " + rbInsureOnlyApplDisIns.get(i).toString());
//logger.debug("LSIH@submitLDInsurance::IOARbLifeInsurance[" + i + "]: " + rbInsureOnlyApplLifeIns.get(i).toString());
//logger.debug("LSIH@submitLDInsurance::IOAInsureOnlyApplGenderCombo[" + i + "]: " + cbInsureOnlyApplGender.get(i).toString());
//logger.debug("LSIH@submitLDInsurance::IOAInsureOnlyApplSmokerCombo[" + i + "]: " + cbInsureOnlyApplSmoker.get(i).toString());
//logger.debug("LSIH@submitLDInsurance::IOAInsureOnlyApplFirstName[" + i + "]: " + txInsureOnlyApplFirstName.get(i).toString());
//logger.debug("LSIH@submitLDInsurance::IOAInsureOnlyApplLastName[" + i + "]: " + txInsureOnlyApplLastName.get(i).toString());

            if (rbInsureOnlyApplLifeIns.get(i) != null) {
              ioa.setLifeStatusId(getLifeDisabilityId(rbInsureOnlyApplLifeIns.
                get(i).toString()));
            }
            if (rbInsureOnlyApplDisIns.get(i) != null) {
              ioa.setDisabilityStatusId(getLifeDisabilityId(
                rbInsureOnlyApplDisIns.get(i).toString()));
            }

            ioa.setInsureOnlyApplicantGenderId(com.iplanet.jato.util.TypeConverter.asInt(cbInsureOnlyApplGender.get(i), 0));
            ioa.setSmokeStatusId(com.iplanet.jato.util.TypeConverter.asInt(cbInsureOnlyApplSmoker.get(i), 0));

            ioa.setInsureOnlyApplicantFirstName(txInsureOnlyApplFirstName.elementAt(i).toString());
            ioa.setInsureOnlyApplicantLastName(txInsureOnlyApplLastName.elementAt(i).toString());

            if( cbInsureOnlyApplDOBMonth.elementAt(i) != null &&
                txInsureOnlyApplDOBDay.elementAt(i) != null && !txInsureOnlyApplDOBYear.get(i).toString().equalsIgnoreCase("") &&
                txInsureOnlyApplDOBYear.elementAt(i) != null && !txInsureOnlyApplDOBDay.get(i).toString().equalsIgnoreCase(""))
            {

              int numOfMonths = new Integer(cbInsureOnlyApplDOBMonth.get(i).toString()).intValue();
              int numOfDays = new Integer(txInsureOnlyApplDOBDay.get(i).toString()).intValue();
              int numOfYears = new Integer(txInsureOnlyApplDOBYear.get(i).toString()).intValue();

              java.util.Date insureOnlyDateOfBirth = new java.util.Date(numOfYears - 1900, numOfMonths - 1, numOfDays);

logger.debug("LSIH@submitLDInsurance::::IOADateOfBirth[" + i + "]: " + insureOnlyDateOfBirth.toString());

             int ioaAge = getInsureOnlyAge( insureOnlyDateOfBirth );

             ioa.setInsureOnlyApplicantBirthDate(insureOnlyDateOfBirth);
             ioa.setInsureOnlyApplicantAge( ioaAge );
            }

            if (rbInsureOnlyApplLifeIns.get(i) != null) {
               ldpioa.setLifeStatusId(getLifeDisabilityId(rbInsureOnlyApplLifeIns.get(i).toString()));
            }

            if (rbInsureOnlyApplDisIns.get(i) != null) {
              ldpioa.setDisabilityStatusId(getLifeDisabilityId(rbInsureOnlyApplDisIns.get(i).toString()));
            }

            // non-repeatable part of Insure Only Applicant entity.
            ioa.setInsuranceProportionsId(theLDInsProportions);
            ioa.setLifePercentCoverageReq(new Double(lifePercentInsurance).doubleValue());

          } //end while
          ldpioa.ejbStore();
          ioa.ejbStore();
        } // end inner if
      } // end for
    //--DJ_CR136--end--//
    } // end outer if
    return 0;
  }

  /**
   *
   *
   */
  private void executeCretarioForLifeDisabilityInfo(Integer dealId, Integer cspDealCPId)
  {
    doLifeDisabilityInsuranceInfoModel doLifeDisabilityInfo =
      (doLifeDisabilityInsuranceInfoModel)RequestManager.getRequestContext().getModelManager().getModel(
      doLifeDisabilityInsuranceInfoModel.class);

    doLifeDisabilityInfo.clearUserWhereCriteria();
    doLifeDisabilityInfo.addUserWhereCriterion("dfDealId", "=", dealId);
    doLifeDisabilityInfo.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);

    this.executeModel(doLifeDisabilityInfo, "LDIH@executeCretarioForLifeDisabilityInfo", false);
  }

  protected void executeCretarioForLifeDisInfoIOnlyA(Integer cspDealId, Integer cspDealCPId)
  {
    doInsureOnlyApplicantModelImpl ioam =
      (doInsureOnlyApplicantModelImpl)RequestManager.getRequestContext().getModelManager().getModel(
      doInsureOnlyApplicantModel.class);

    ioam.clearUserWhereCriteria();
    ioam.addUserWhereCriterion("dfDealId", "=", cspDealId);
    ioam.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);

    this.executeModel(ioam, "LDIH@executeCretarioForLifeDisInfoIOnlyA", false);
  }

  private void executeCretarioForLifeDisabilityInsProportions(Integer dealId, Integer cspDealCPId)
  {
    doLifeDisabilityInsProportionsModel doLifeDisabilityInsProportions =
      (doLifeDisabilityInsProportionsModel)RequestManager.getRequestContext().getModelManager().getModel(
      doLifeDisabilityInsProportionsModel.class);

    doLifeDisabilityInsProportions.clearUserWhereCriteria();
    doLifeDisabilityInsProportions.addUserWhereCriterion("dfDealId", "=", dealId);
    doLifeDisabilityInsProportions.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);

    this.executeModel(doLifeDisabilityInsProportions, "LDIH@executeCretarioForLifeDisabilityInsProportions", false);
  }

  //--DJ_CR136--start--//
  public void checkAndSyncDOWithCursorInfo(RequestHandlingTiledViewBase theRepeat, String pageCursorInof)
  {
    PageEntry pg = (PageEntry)theSessionState.getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    ////PageCursorInfo tds = (PageCursorInfo)pst.get("Repeated1");
    PageCursorInfo tds = (PageCursorInfo)pst.get(pageCursorInof);

    try
    {
      //// So far we cannot find a way to manually set the Diaplay Offset of the TiledView here.
      //// The reason is some necessary method is protected (e.g. setWebActionModelOffset()). The
      //// work around is to create our own method (setCurrentDisplayOffset) in the TiledView class.
      ((pgLifeDisabilityInsuranceRepeated1TiledView)theRepeat).setCurrentDisplayOffset(tds.getFirstDisplayRowNumber() - 1);
    }
    catch(Exception e)
    {
      logger.warning("LDIH@checkAndSyncDOWithCursorInfo::Exception: " + e);
    }
  }
  //--DJ_CR136--end--//

  public void handleAddInsureOnlyApplicant()
{
  PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

logger.debug("LDH@handleAddInsureOnlyApplicant:: Start");
  if (disableEditButton(pg) == true) return;

  CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());
  SessionResourceKit srk = getSessionResourceKit();

  try
  {
    srk.beginTransaction();
    noAuditIfIncomplete(null, null);
    //int dealid = pg.getPageDealId();
    //int copyid = pg.getPageDealCID();

    //1. Create InsuranceOnlyApplicant entity first.
    InsureOnlyApplicant applicant = new InsureOnlyApplicant(srk, dcm);
    applicant = applicant.create(new DealPK( pg.getPageDealId(),
                                             pg.getPageDealCID()));

    applicant.ejbStore();

    InsureOnlyApplicantPK ioapk = (InsureOnlyApplicantPK)applicant.getPk();
logger.debug("LDH@handleAddInsureOnlyApplicant::@InsureOnlyApplicantPK: " + ioapk.getId());

    handleAddLDPremiumsInsureOnly(applicant, dcm);

    pg.setLoadTarget(TARGET_LD_INSUREONLYAPPLICANT);
    doCalculation(dcm);
    srk.commitTransaction();
    logger.debug("LDH@handleAddInsureOnlyApplicant::After Commit Transaction: ");
  }
  catch(Exception ex)
  {
    srk.cleanTransaction();
    logger.error("Exception @handleAddInsureOnlyApplicant");
    logger.error(ex);
    setStandardFailMessage();
  }
}

public void handleLifeLisPremiumsCreate()
{
  PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
  SessionResourceKit srk = getSessionResourceKit();
  CalcMonitor dcm = CalcMonitor.getMonitor(srk);

  try
  {
    srk.beginTransaction();

    //1. Create LifeDisabilityPremiums entities for the created InsuranceOnlyApplicant.
    InsureOnlyApplicant applicant = new InsureOnlyApplicant(srk, dcm);

    Collection applicants = applicant
                   .findByDeal(new DealPK( pg.getPageDealId(), 
                                           pg.getPageDealCID()));

logger.debug("LDH@handleLifeLisPremiumsCreate::InsureOnlyApplicantNumber: " + applicants.size());

    Iterator ita = applicants.iterator();
    int applId = 0;

    while(ita.hasNext())
    {
      applicant =(InsureOnlyApplicant) ita.next();
      applId = applicant.getInsureOnlyApplicantId();

logger.debug("LDH@handleLifeLisPremiumsCreate::InsureOnlyApplicantId: " + applId);
logger.debug("LDH@handleLifeLisPremiumsCreate::InsureOnlyApplicantId_fromPK: " + applicant.getPk().getId());
        handleAddLDPremiumsInsureOnly(applicant, dcm);
    }

    PageEntry ldiPg = setupSubPagePageEntry(pg, Mc.PGMN_LIFE_DISABILITY_INSURANCE, true);

    getSavedPages().setNextPage(ldiPg);
    navigateToNextPage();
    srk.commitTransaction();
    return;
  }
  catch(Exception e)
  {
    srk.cleanTransaction();
    logger.error("handleLifeLisPremiumsCreate().");
    logger.error(e);
    setStandardFailMessage();
  }
}

//--DJ_CR136--23Nov2004--start--//
/**
 *
 *
 */
/*
protected void handleAddLDPremiumsInsureOnly(InsureOnlyApplicant applicant, CalcMonitor dcm)
  throws Exception
{
logger.debug("LDH@handleLifeLisPremiumsCreate::handleAddLDPremiumsInsureOnly: " + applicant.getPk().getId());

  try
  {
    DBA.createEntity(this.getSessionResourceKit(), "LifeDisPremiumsIOnlyA", applicant, dcm, true);
  }
  catch(Exception ex)
  {
    logger.error("Exception @LifeDisabilityPremiums");
    logger.error(ex);
    throw new Exception("Add entity handler exception");
  }
}
**/

/**
   *
   *
   */
  protected void setIOApplicantMainDisplayFields(QueryModelBase dobject, int rowIndx)
  {
    PageEntry pg = getTheSessionState().getCurrentPage();
    Hashtable pst = pg.getPageStateTable();

logger.debug("LDH@setIOApplicantMainDisplayFields::RowNum: " + rowIndx +
                 " , ModelRowIndx: " + dobject.getRowIndex());

    fillupMonths("Repeated2/cbIOApplicantDOBMonth");

    setDateDisplayField(dobject, "Repeated2/cbIOApplicantDOBMonth",
                                 "Repeated2/txIOApplicantDOBDay",
                                 "Repeated2/txIOApplicantDOBYear",
                                 "dfInsureOnlyApplicantBirthDate",
                                 rowIndx);


    pst.put("DATEFIELD_INDEX", new Integer(rowIndx));

  }
//--DJ_CR136--23Nov2004--end--//

/**
 *
 *
 */
private void handleAddInsureOnlyApplicant(Deal deal, CalcMonitor dcm) throws
  Exception
{
  try
  {
    DBA.createEntity(this.getSessionResourceKit(),
                     "InsureOnlyApplicant",
                     deal,
                     dcm);
  }
  catch (Exception ex)
  {
    logger.error("Exception @handleAddInsureOnlyApplicant");
    logger.error(ex);
    throw new Exception("Add entity exception.");
  }
 }

 public Deal getDeal(int dealId, int copyId, CalcMonitor dcm)
   throws Exception
 {
   Deal deal = null;

   try
   {
     deal = DBA.getDeal(getSessionResourceKit(), dealId, copyId, dcm);
   }
   catch(Exception ex)
   {
     logger.error("Error @getDeal: dealId = " + dealId + ", copyId = " + copyId);
     logger.error(ex);
     throw new Exception("Locate deal entity exception");
   }
   return deal;
 }

 protected void handleDelete(boolean confirmed,
                             int type,
                             Vector rowindexes,
                             String primaryIdVar,
                             String primaryColName,
                             String copyIdVar,
                             String entity)
 {
   SysLogger logger = getSessionResourceKit().getSysLogger();
logger.debug("LDH@handleDelete_1:Start: ");
   PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

   if (disableEditButton(currPg) == true) return;

   String tmp = "";

   for(int i = 0;i < rowindexes.size();i ++)
   {
       tmp += ((Integer)rowindexes.elementAt(i)).intValue() + ", ";
   }
   logger.debug("LDH@handleDelete: Rowindexes = " + tmp);
   logger.debug("LDH@handleDelete: unconfirmed delete entity = " + entity + ", pId = " + primaryIdVar + ", cId = " + copyIdVar + ", primColName = " + primaryColName + " - dialog for confirmation");

   DeleteEntityProperties dep = new DeleteEntityProperties(type,
                                                           rowindexes,
                                                           primaryIdVar,
                                                           primaryColName,
                                                           copyIdVar,
                                                           entity,
                                                           theSessionState.getLanguageId());

   // Check if any row selected
   if (rowindexes == null || rowindexes.size() <= 0)
   {
     setActiveMessageToAlert(BXResources.getSysMsg("DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_NOT_SELECTED", theSessionState.getLanguageId()),
       ActiveMsgFactory.ISCUSTOMCONFIRM);
   }
   else
   {
     setActiveMessageToAlert(dep.message, ActiveMsgFactory.ISCUSTOMDIALOG, "CONFIRMDELETE");
     (theSessionState.getActMessage()).setResponseObject(dep);
   }

   // Set Target to reload the page on the position before left
   if (dep.targetTag != TARGET_NONE) currPg.setLoadTarget(dep.targetTag);

   //// IMPORTANT!! Set here the condition not to display 'Submit' button in order
   //// to force RTP/Infocal recalculation. In is necessary to updated all deal values
   //// encompassing now both types of borrower: regular express borrower and
   //// insure only applicant.
   currPg.setPageCondition3(true);

   return;
 }

 // Method to delete only one row at a time

 protected void handleDelete(boolean confirmed,
                             int type,
                             int rowindex,
                             String primaryIdVar,
                             String primaryColName,
                             String copyIdVar,
                             String entity)
 {
   Vector rowindexes = new Vector();
logger.debug("LDH@handleDelete_2:Start");

   rowindexes.add(new Integer(rowindex));

   handleDelete(confirmed, type, rowindexes, primaryIdVar, primaryColName, copyIdVar, entity);

   return;
 }

 /**
  *
  *
  */
 public void handleCustomActMessageOk(String[] args)
 {
   logger.debug("LDH@handleCustomActMessageOk::ArgsSize: " + args.length);
   logger.debug("LDH@handleCustomActMessageOk::Args[0]: " + args[0]);

   PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
   SessionResourceKit srk = getSessionResourceKit();
   SysLogger logger = srk.getSysLogger();

   if (args == null) return;

   if (args [ 0 ].equals("CONFIRMDELETE"))
   {
     // continue after a delete confirmed (e.g. delete property)
     logger.debug("LDH@handleCustomActMessageOk::I am before handleDelete");
     handleDelete((theSessionState.getActMessage()).getResponseObject());

     return;
   }
 }

 /**
  *
  *
  */
 protected void handleDelete(Object obj)
 {
   DeleteEntityProperties dep =(DeleteEntityProperties) obj;
   CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());
   SessionResourceKit srk = getSessionResourceKit();

   try
   {
     srk.beginTransaction();
     noAuditIfIncomplete(null, null);
     int primaryid = 0;
     int copyid = 0;
     int currRow = - 1;
     for(int i = 0; i < dep.rowindexes.size(); i ++)
     {
       currRow = ((Integer)(dep.rowindexes.elementAt(i))).intValue();
       primaryid =(int) Double.parseDouble(readFromPage(dep.primaryIdVar, currRow));
       copyid =(int) Double.parseDouble(readFromPage(dep.copyIdVar, currRow));

       DealEntity de = getEntity(dep.entityName, primaryid, copyid, dcm);

       // check for employment history being deleted - if so remove linked income record (if one)
       if (dep.entityName.equals("InsureOnlyApplicant"))
       {
         InsureOnlyApplicant ioapp =(InsureOnlyApplicant) de;
logger.debug("LDI@handleDelete:InsureOnlyApplicantId: " + ioapp.getInsureOnlyApplicantId());
logger.debug("LDI@handleDelete:CopyId: " + ioapp.getCopyId());

         ////ioapp = ioapp.findByPrimaryKey(new InsureOnlyApplicantPK(ioapp.getInsureOnlyApplicantId(), ioapp.getCopyId()));
         de.ejbRemove(true);
       }
     }

     srk.commitTransaction();

     // Set Target to reload the page on the position before left
     PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

     if (dep.targetTag != TARGET_NONE) {
       currPg.setLoadTarget(dep.targetTag);
     }

     //// IMPORTANT!! Set here the condition not to display 'Submit' button in order
     //// to force RTP/Infocal recalculation. In is necessary to updated all deal values
     //// encompassing now both types of borrower: regular express borrower and
     //// insure only applicant.
     currPg.setPageCondition3(true);
   }
   catch(Exception ex)
   {
     srk.cleanTransaction();
     logger.error("Exception @handleDeleteInsureApplicantOnly");
     logger.error(ex);
     setStandardFailMessage();
   }
 }
 //--DJ_CR136--23Nov2004--end--//
}

class BorrowerScreenInfoWrapper
{
  private String smokerStatus;
  private String gender;
  private String lifeInsStatus;
  private String disabilityInsStatus;
  private String lifePercentInsurance;
  private String disabilityPercentInsurance;

  // "getters" methods.
  public String getSmokerStatus()
  {
    return smokerStatus;
  }

  public String getGenderId()
  {
    return gender;
  }

  public String getLifeInsStatus()
  {
    return lifeInsStatus;
  }

  public String getDisabilityInsStatus()
  {
    return disabilityInsStatus;
  }

  public String getLifePercentInsurance()
  {
    return lifePercentInsurance;
  }

  public String getDisabilityPercentInsurance()
  {
    return disabilityPercentInsurance;
  }

  // "setters" methods.
  public void setSmokerStatus(String aSmokerStatus)
  {
    smokerStatus = aSmokerStatus;
  }

  public void setGenderId(String aGender)
  {
    gender = aGender;
  }

  public void setLifeInsStatus(String aLifeInsStatus)
  {
    lifeInsStatus = aLifeInsStatus;
  }

  public void setDisabilityInsStatus(String aDisabilityInsStatus)
  {
    disabilityInsStatus = aDisabilityInsStatus;
  }

  public void setLifePercentInsurance(String aLifePercentInsurance)
  {
    lifePercentInsurance = aLifePercentInsurance;
  }

  public void setDisabilityPercentInsurance(String aDisabilityPercentInsurance)
  {
    disabilityPercentInsurance = aDisabilityPercentInsurance;
  }
}

//--DJ_CR136--start--//
class InsureOnlyApplicantScreenInfoWrapper
{
  private String smokerStatus;
  private String gender;
  private String lifeInsStatus;
  private String disabilityInsStatus;
  private String lifePercentInsurance;
  private String disabilityPercentInsurance;

  private String insureOnlyFirstName;
  private String insureOnlyLastName;
  private String insureOnlyDOBMonth;
  private String insureOnlyDOBDay;
  private String insureOnlyDOBYear;
  private String insureOnlyDateOfBirth;
  private String insureOnlyAge;

  // "getters" methods.
  public String getSmokerStatus()
  {
    return smokerStatus;
  }

  public String getGenderId()
  {
    return gender;
  }

  public String getLifeInsStatus()
  {
    return lifeInsStatus;
  }

  public String getDisabilityInsStatus()
  {
    return disabilityInsStatus;
  }

  public String getLifePercentInsurance()
  {
    return lifePercentInsurance;
  }

  public String getDisabilityPercentInsurance()
  {
    return disabilityPercentInsurance;
  }

  public String getInsureOnlyFirstName()
  {
    return insureOnlyFirstName;
  }

  public String getInsureOnlyLastName()
  {
    return insureOnlyLastName;
  }

  public String getInsureOnlyDOBMonth()
  {
    return insureOnlyDOBMonth;
  }

  public String getInsureOnlyDOBDay()
  {
    return insureOnlyDOBDay;
  }

  public String getInsureOnlyDOBYear()
  {
    return insureOnlyDOBYear;
  }
  public String getInsureOnlyDateOfBirth()
  {
    return insureOnlyDateOfBirth;
  }

  public String getInsureOnlyApplicantAge()
  {
    return insureOnlyAge;
  }

  // "setters" methods.
  public void setSmokerStatus(String aSmokerStatus)
  {
    smokerStatus = aSmokerStatus;
  }

  public void setGenderId(String aGender)
  {
    gender = aGender;
  }

  public void setLifeInsStatus(String aLifeInsStatus)
  {
    lifeInsStatus = aLifeInsStatus;
  }

  public void setDisabilityInsStatus(String aDisabilityInsStatus)
  {
    disabilityInsStatus = aDisabilityInsStatus;
  }

  public void setLifePercentInsurance(String aLifePercentInsurance)
  {
    lifePercentInsurance = aLifePercentInsurance;
  }

  public void setDisabilityPercentInsurance(String aDisabilityPercentInsurance)
  {
    disabilityPercentInsurance = aDisabilityPercentInsurance;
  }

  public void setInsureOnlyFirstName(String aInsureOnlyFirstName)
  {
    insureOnlyFirstName = aInsureOnlyFirstName;
  }

  public void setInsureOnlyLastName(String aInsureOnlyLastName)
  {
    insureOnlyLastName = aInsureOnlyLastName;
  }

  public void setInsureOnlyDOBMonth(String aInsureOnlyDOBMonth)
  {
    insureOnlyDOBMonth = aInsureOnlyDOBMonth;
  }

  public void setInsureOnlyDOBDay(String aInsureOnlyDOBDay)
  {
    insureOnlyDOBDay = aInsureOnlyDOBDay;
  }

  public void setInsureOnlyDOBYear(String aInsureOnlyDOBYear)
  {
    insureOnlyDOBYear = aInsureOnlyDOBYear;
  }
  public void setInsureOnlyDateOfBirth(String aInsureOnlyDateOfBirth)
  {
    insureOnlyDateOfBirth = aInsureOnlyDateOfBirth;
  }

  public void setInsureOnlyApplicantAge(String aInsureOnlyAge)
  {
    insureOnlyAge = aInsureOnlyAge;
  }

}
//--DJ_CR136--end--//
