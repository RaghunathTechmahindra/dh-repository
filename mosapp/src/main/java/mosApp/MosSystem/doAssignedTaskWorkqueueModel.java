package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doAssignedTaskWorkqueueModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public String getDfTaskDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfTaskDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfTaskStatusDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfTaskStatusDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPriorityDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfPriorityDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfDueDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfDueDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public String getDfMilestone();

	
	/**
	 * 
	 * 
	 */
	public void setDfMilestone(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAssignedUser();

	
	/**
	 * 
	 * 
	 */
	public void setDfAssignedUser(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAssignedTaskWorkqueueId();

	
	/**
	 * 
	 * 
	 */
	public void setDfAssignedTaskWorkqueueId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfUnderwriterId();

	
	/**
	 * 
	 * 
	 */
	public void setDfUnderwriterId(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFTASKDESC="dfTaskDesc";
	public static final String FIELD_DFTASKSTATUSDESC="dfTaskStatusDesc";
	public static final String FIELD_DFPRIORITYDESC="dfPriorityDesc";
	public static final String FIELD_DFDUEDATE="dfDueDate";
	public static final String FIELD_DFMILESTONE="dfMilestone";
	public static final String FIELD_DFASSIGNEDUSER="dfAssignedUser";
	public static final String FIELD_DFASSIGNEDTASKWORKQUEUEID="dfAssignedTaskWorkqueueId";
	public static final String FIELD_DFUNDERWRITERID="dfUnderwriterId";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

