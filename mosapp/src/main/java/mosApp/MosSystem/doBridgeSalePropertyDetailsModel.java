package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doBridgeSalePropertyDetailsModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPrimaryBorrowerFlag();

	
	/**
	 * 
	 * 
	 */
	public void setDfPrimaryBorrowerFlag(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerAddressId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerAddressId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBorrowerTypeDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerTypeDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBorrowerAddressTypeDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerAddressTypeDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAddressLine1();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddressLine1(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAddressLine2();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddressLine2(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPostalCodeFSA();

	
	/**
	 * 
	 * 
	 */
	public void setDfPostalCodeFSA(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfCity();

	
	/**
	 * 
	 * 
	 */
	public void setDfCity(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfProvinceName();

	
	/**
	 * 
	 * 
	 */
	public void setDfProvinceName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPostalCodeLDU();

	
	/**
	 * 
	 * 
	 */
	public void setDfPostalCodeLDU(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFPRIMARYBORROWERFLAG="dfPrimaryBorrowerFlag";
	public static final String FIELD_DFBORROWERADDRESSID="dfBorrowerAddressId";
	public static final String FIELD_DFBORROWERTYPEDESC="dfBorrowerTypeDesc";
	public static final String FIELD_DFBORROWERADDRESSTYPEDESC="dfBorrowerAddressTypeDesc";
	public static final String FIELD_DFADDRESSLINE1="dfAddressLine1";
	public static final String FIELD_DFADDRESSLINE2="dfAddressLine2";
	public static final String FIELD_DFPOSTALCODEFSA="dfPostalCodeFSA";
	public static final String FIELD_DFCITY="dfCity";
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFPROVINCENAME="dfProvinceName";
	public static final String FIELD_DFPOSTALCODELDU="dfPostalCodeLDU";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

