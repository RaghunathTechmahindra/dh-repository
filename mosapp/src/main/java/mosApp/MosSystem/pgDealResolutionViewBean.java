package mosApp.MosSystem;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;

import mosApp.SQLConnectionManagerImpl;

import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.filogix.express.web.jato.ExpressViewBeanBase;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.view.CommandFieldDescriptor;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HREF;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.RadioButtonGroup;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

/**
 *
 *
 *
 */
public class pgDealResolutionViewBean extends ExpressViewBeanBase {
  /**
   *
   *
   */
  public pgDealResolutionViewBean()
  {
    super(PAGE_NAME);
    setDefaultDisplayURL(DEFAULT_DISPLAY_URL);
    registerChildren();
    initialize();
  }


  /**
   *
   *
   */
  protected void initialize()
  {
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Child manipulation methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  protected View createChild(String name)
  {
      View superReturn = super.createChild(name);  
      if (superReturn != null) {  
          return superReturn;  
      } else if (name.equals(CHILD_TBDEALID)) {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBDEALID,
        CHILD_TBDEALID,
        CHILD_TBDEALID_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_CBPAGENAMES))
    {
      ComboBox child = new ComboBox( this,
        getDefaultModel(),
        CHILD_CBPAGENAMES,
        CHILD_CBPAGENAMES,
        CHILD_CBPAGENAMES_RESET_VALUE,
        null);

      //--Release2.1--//
      //Modified to set NonSelected Label from CHILD_CBPAGENAMES_NONSELECTED_LABEL
      //--> By John 08Nov2002
      child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
      //==========================================================================

      child.setOptions(cbPageNamesOptions);
      return child;
    }
    else
    if (name.equals(CHILD_BTPROCEED))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTPROCEED,
        CHILD_BTPROCEED,
        CHILD_BTPROCEED_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_HREF1))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF1,
        CHILD_HREF1,
        CHILD_HREF1_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_HREF2))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF2,
        CHILD_HREF2,
        CHILD_HREF2_RESET_VALUE,
        null);
        return child;

    }
    else		if (name.equals(CHILD_HREF3))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF3,
        CHILD_HREF3,
        CHILD_HREF3_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_HREF4))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF4,
        CHILD_HREF4,
        CHILD_HREF4_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_HREF5))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF5,
        CHILD_HREF5,
        CHILD_HREF5_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_HREF6))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF6,
        CHILD_HREF6,
        CHILD_HREF6_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_HREF7))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF7,
        CHILD_HREF7,
        CHILD_HREF7_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_HREF8))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF8,
        CHILD_HREF8,
        CHILD_HREF8_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_STAPPROVALTYPELABEL))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAPPROVALTYPELABEL,
        CHILD_STAPPROVALTYPELABEL,
        CHILD_STAPPROVALTYPELABEL_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_HREF9))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF9,
        CHILD_HREF9,
        CHILD_HREF9_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_HREF10))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF10,
        CHILD_HREF10,
        CHILD_HREF10_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_STPAGELABEL))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPAGELABEL,
        CHILD_STPAGELABEL,
        CHILD_STPAGELABEL_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STCOMPANYNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STCOMPANYNAME,
        CHILD_STCOMPANYNAME,
        CHILD_STCOMPANYNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STTODAYDATE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STTODAYDATE,
        CHILD_STTODAYDATE,
        CHILD_STTODAYDATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STUSERNAMETITLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STUSERNAMETITLE,
        CHILD_STUSERNAMETITLE,
        CHILD_STUSERNAMETITLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STDEALID))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STDEALID,
        doDealSummarySnapShotModel.FIELD_DFAPPLICATIONID,
        CHILD_STDEALID_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STBORRFIRSTNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STBORRFIRSTNAME,
        CHILD_STBORRFIRSTNAME,
        CHILD_STBORRFIRSTNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STDEALSTATUS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STDEALSTATUS,
        doDealSummarySnapShotModel.FIELD_DFSTATUSDESCR,
        CHILD_STDEALSTATUS_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STDEALSTATUSDATE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STDEALSTATUSDATE,
        doDealSummarySnapShotModel.FIELD_DFSTATUSDATE,
        CHILD_STDEALSTATUSDATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STSOURCEFIRM))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STSOURCEFIRM,
        doDealSummarySnapShotModel.FIELD_DFSFSHORTNAME,
        CHILD_STSOURCEFIRM_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STSOURCE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STSOURCE,
        doDealSummarySnapShotModel.FIELD_DFSOBPSHORTNAME,
        CHILD_STSOURCE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STLOB))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STLOB,
        doDealSummarySnapShotModel.FIELD_DFLOBDESCR,
        CHILD_STLOB_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STDEALTYPE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STDEALTYPE,
        doDealSummarySnapShotModel.FIELD_DFDEALTYPEDESCR,
        CHILD_STDEALTYPE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STDEALPURPOSE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STDEALPURPOSE,
        doDealSummarySnapShotModel.FIELD_DFLOANPURPOSEDESCR,
        CHILD_STDEALPURPOSE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPURCHASEPRICE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STPURCHASEPRICE,
        doDealSummarySnapShotModel.FIELD_DFTOTALPURCHASEPRICE,
        CHILD_STPURCHASEPRICE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMTTERM))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STPMTTERM,
        doDealSummarySnapShotModel.FIELD_DFPAYMENTTERMDESCR,
        CHILD_STPMTTERM_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STESTCLOSINGDATE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STESTCLOSINGDATE,
        doDealSummarySnapShotModel.FIELD_DFESTCLOSINGDATE,
        CHILD_STESTCLOSINGDATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STSPECIALFEATURE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STSPECIALFEATURE,
        doDealSummarySnapShotModel.FIELD_DFSPECIALFEATURE,
        CHILD_STSPECIALFEATURE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_BTSUBMIT))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTSUBMIT,
        CHILD_BTSUBMIT,
        CHILD_BTSUBMIT_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_BTWORKQUEUELINK))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTWORKQUEUELINK,
        CHILD_BTWORKQUEUELINK,
        CHILD_BTWORKQUEUELINK_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_CHANGEPASSWORDHREF))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_CHANGEPASSWORDHREF,
        CHILD_CHANGEPASSWORDHREF,
        CHILD_CHANGEPASSWORDHREF_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_BTTOOLHISTORY))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTTOOLHISTORY,
        CHILD_BTTOOLHISTORY,
        CHILD_BTTOOLHISTORY_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_BTTOONOTES))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTTOONOTES,
        CHILD_BTTOONOTES,
        CHILD_BTTOONOTES_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_BTTOOLSEARCH))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTTOOLSEARCH,
        CHILD_BTTOOLSEARCH,
        CHILD_BTTOOLSEARCH_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_BTTOOLLOG))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTTOOLLOG,
        CHILD_BTTOOLLOG,
        CHILD_BTTOOLLOG_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_STERRORFLAG))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STERRORFLAG,
        CHILD_STERRORFLAG,
        CHILD_STERRORFLAG_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_DETECTALERTTASKS))
    {
      HiddenField child = new HiddenField(this,
        getDefaultModel(),
        CHILD_DETECTALERTTASKS,
        CHILD_DETECTALERTTASKS,
        CHILD_DETECTALERTTASKS_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STTOTALLOANAMOUNT))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STTOTALLOANAMOUNT,
        doDealSummarySnapShotModel.FIELD_DFTOTALLOANAMT,
        CHILD_STTOTALLOANAMOUNT_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_BTCANCEL))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTCANCEL,
        CHILD_BTCANCEL,
        CHILD_BTCANCEL_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_RBAPPROVEDEAL))
    {
      RadioButtonGroup child = new RadioButtonGroup( this,
        getDefaultModel(),
        CHILD_RBAPPROVEDEAL,
        CHILD_RBAPPROVEDEAL,
        CHILD_RBAPPROVEDEAL_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(rbApproveDealOptions);
      return child;
    }
    else
    if (name.equals(CHILD_RBHOLDPENDINGINFO))
    {
      RadioButtonGroup child = new RadioButtonGroup( this,
        getDefaultModel(),
        CHILD_RBHOLDPENDINGINFO,
        CHILD_RBHOLDPENDINGINFO,
        CHILD_RBHOLDPENDINGINFO_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(rbHoldPendingInfoOptions);
      return child;
    }
    else
    if (name.equals(CHILD_RBDENY))
    {
      RadioButtonGroup child = new RadioButtonGroup( this,
        getDefaultModel(),
        CHILD_RBDENY,
        CHILD_RBDENY,
        CHILD_RBDENY_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(rbDenyOptions);
      return child;
    }
    else
    if (name.equals(CHILD_RBCOLLAPSEDEAL))
    {
      RadioButtonGroup child = new RadioButtonGroup( this,
        getDefaultModel(),
        CHILD_RBCOLLAPSEDEAL,
        CHILD_RBCOLLAPSEDEAL,
        CHILD_RBCOLLAPSEDEAL_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(rbCollapseDealOptions);
      return child;
    }
    else
    if (name.equals(CHILD_CBDENIALOPTIONS))
    {
      ComboBox child = new ComboBox( this,
        getDefaultModel(),
        CHILD_CBDENIALOPTIONS,
        CHILD_CBDENIALOPTIONS,
        CHILD_CBDENIALOPTIONS_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbDenialOptionsOptions);
      return child;
    }
    else
    if (name.equals(CHILD_BTPREVTASKPAGE))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTPREVTASKPAGE,
        CHILD_BTPREVTASKPAGE,
        CHILD_BTPREVTASKPAGE_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_STPREVTASKPAGELABEL))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPREVTASKPAGELABEL,
        CHILD_STPREVTASKPAGELABEL,
        CHILD_STPREVTASKPAGELABEL_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_BTNEXTTASKPAGE))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTNEXTTASKPAGE,
        CHILD_BTNEXTTASKPAGE,
        CHILD_BTNEXTTASKPAGE_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_STNEXTTASKPAGELABEL))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STNEXTTASKPAGELABEL,
        CHILD_STNEXTTASKPAGELABEL,
        CHILD_STNEXTTASKPAGELABEL_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STTASKNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STTASKNAME,
        CHILD_STTASKNAME,
        CHILD_STTASKNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_SESSIONUSERID))
    {
      HiddenField child = new HiddenField(this,
        getDefaultModel(),
        CHILD_SESSIONUSERID,
        CHILD_SESSIONUSERID,
        CHILD_SESSIONUSERID_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_CBAPPROVALTYPE))
    {
      ComboBox child = new ComboBox( this,
        getDefaultModel(),
        CHILD_CBAPPROVALTYPE,
        CHILD_CBAPPROVALTYPE,
        CHILD_CBAPPROVALTYPE_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbApprovalTypeOptions);
      return child;
    }
    else
    if (name.equals(CHILD_TBHOLDFORDAYS))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBHOLDFORDAYS,
        CHILD_TBHOLDFORDAYS,
        CHILD_TBHOLDFORDAYS_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_CBHOLDUNTILMONTH))
    {
      ComboBox child = new ComboBox( this,
        getDefaultModel(),
        CHILD_CBHOLDUNTILMONTH,
        CHILD_CBHOLDUNTILMONTH,
        CHILD_CBHOLDUNTILMONTH_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbHoldUntilMonthOptions);
      return child;
    }
    else
    if (name.equals(CHILD_TBHOLDUNTILDAY))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBHOLDUNTILDAY,
        CHILD_TBHOLDUNTILDAY,
        CHILD_TBHOLDUNTILDAY_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_TBHOLDUNTILYEAR))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBHOLDUNTILYEAR,
        CHILD_TBHOLDUNTILYEAR,
        CHILD_TBHOLDUNTILYEAR_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_RBHOLDFOR))
    {
      RadioButtonGroup child = new RadioButtonGroup( this,
        getDefaultModel(),
        CHILD_RBHOLDFOR,
        CHILD_RBHOLDFOR,
        CHILD_RBHOLDFOR_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(rbHoldForOptions);
      return child;
    }
    else
    if (name.equals(CHILD_RBHOLDUNTIL))
    {
      RadioButtonGroup child = new RadioButtonGroup( this,
        getDefaultModel(),
        CHILD_RBHOLDUNTIL,
        CHILD_RBHOLDUNTIL,
        CHILD_RBHOLDUNTIL_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(rbHoldUntilOptions);
      return child;
    }
    else
    if (name.equals(CHILD_TBBROKERNOTETEXT))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBBROKERNOTETEXT,
        CHILD_TBBROKERNOTETEXT,
        CHILD_TBBROKERNOTETEXT_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMGENERATE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMGENERATE,
        CHILD_STPMGENERATE,
        CHILD_STPMGENERATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMHASTITLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMHASTITLE,
        CHILD_STPMHASTITLE,
        CHILD_STPMHASTITLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMHASINFO))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMHASINFO,
        CHILD_STPMHASINFO,
        CHILD_STPMHASINFO_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMHASTABLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMHASTABLE,
        CHILD_STPMHASTABLE,
        CHILD_STPMHASTABLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMHASOK))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMHASOK,
        CHILD_STPMHASOK,
        CHILD_STPMHASOK_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMTITLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMTITLE,
        CHILD_STPMTITLE,
        CHILD_STPMTITLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMINFOMSG))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMINFOMSG,
        CHILD_STPMINFOMSG,
        CHILD_STPMINFOMSG_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMONOK))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMONOK,
        CHILD_STPMONOK,
        CHILD_STPMONOK_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMMSGS))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMMSGS,
        CHILD_STPMMSGS,
        CHILD_STPMMSGS_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMMSGTYPES))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMMSGTYPES,
        CHILD_STPMMSGTYPES,
        CHILD_STPMMSGTYPES_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMGENERATE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMGENERATE,
        CHILD_STAMGENERATE,
        CHILD_STAMGENERATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMHASTITLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMHASTITLE,
        CHILD_STAMHASTITLE,
        CHILD_STAMHASTITLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMHASINFO))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMHASINFO,
        CHILD_STAMHASINFO,
        CHILD_STAMHASINFO_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMHASTABLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMHASTABLE,
        CHILD_STAMHASTABLE,
        CHILD_STAMHASTABLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMTITLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMTITLE,
        CHILD_STAMTITLE,
        CHILD_STAMTITLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMINFOMSG))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMINFOMSG,
        CHILD_STAMINFOMSG,
        CHILD_STAMINFOMSG_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMMSGS))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMMSGS,
        CHILD_STAMMSGS,
        CHILD_STAMMSGS_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMMSGTYPES))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMMSGTYPES,
        CHILD_STAMMSGTYPES,
        CHILD_STAMMSGTYPES_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMDIALOGMSG))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMDIALOGMSG,
        CHILD_STAMDIALOGMSG,
        CHILD_STAMDIALOGMSG_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMBUTTONSHTML))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMBUTTONSHTML,
        CHILD_STAMBUTTONSHTML,
        CHILD_STAMBUTTONSHTML_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STVIEWONLYTAG))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STVIEWONLYTAG,
        CHILD_STVIEWONLYTAG,
        CHILD_STVIEWONLYTAG_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_BTOK))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTOK,
        CHILD_BTOK,
        CHILD_BTOK_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_BTTOUWSHEET))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTTOUWSHEET,
        CHILD_BTTOUWSHEET,
        CHILD_BTTOUWSHEET_RESET_VALUE,
        null);
        return child;
    }
    //// Hidden button to trigger the ActiveMessage 'fake' submit button
    //// via the new BX ActMessageCommand class
    else
    if (name.equals(CHILD_BTACTMSG))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTACTMSG,
        CHILD_BTACTMSG,
        CHILD_BTACTMSG_RESET_VALUE,
        new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));
        return child;
    }
    ////SYNCADD.
    else
     if (name.equals(CHILD_STAPPROVALSERVICINGNUMLABEL))
     {
       StaticTextField child = new StaticTextField(this,
         getDefaultModel(),
         CHILD_STAPPROVALSERVICINGNUMLABEL,
         CHILD_STAPPROVALSERVICINGNUMLABEL,
         CHILD_STAPPROVALSERVICINGNUMLABEL_RESET_VALUE,
         null);
       return child;
     }
     else
     if (name.equals(CHILD_TBSERVMORTGNUM))
     {
       TextField child = new TextField(this,
         getDefaultModel(),
         CHILD_TBSERVMORTGNUM,
         CHILD_TBSERVMORTGNUM,
         CHILD_TBSERVMORTGNUM_RESET_VALUE,
         null);
       return child;
     }
    ////Suppress or not suppress Approve Option and Mortgage Servicing Number
    else
     if (name.equals(CHILD_STINCLUDEAPPROVEOPTIONSTART))
     {
       StaticTextField child = new StaticTextField(this,
         getDefaultModel(),
         CHILD_STINCLUDEAPPROVEOPTIONSTART,
         CHILD_STINCLUDEAPPROVEOPTIONSTART,
         CHILD_STINCLUDEAPPROVEOPTIONSTART_RESET_VALUE,
         null);
       return child;
     }
    else
     if (name.equals(CHILD_STINCLUDEAPPROVEOPTIONEND))
     {
       StaticTextField child = new StaticTextField(this,
         getDefaultModel(),
         CHILD_STINCLUDEAPPROVEOPTIONEND,
         CHILD_STINCLUDEAPPROVEOPTIONEND,
         CHILD_STINCLUDEAPPROVEOPTIONEND_RESET_VALUE,
         null);
       return child;
     }
    else
     if (name.equals(CHILD_STINCLUDESRVCNUMBERSTART))
     {
       StaticTextField child = new StaticTextField(this,
         getDefaultModel(),
         CHILD_STINCLUDESRVCNUMBERSTART,
         CHILD_STINCLUDESRVCNUMBERSTART,
         CHILD_STINCLUDESRVCNUMBERSTART_RESET_VALUE,
         null);
       return child;
     }
    else
     if (name.equals(CHILD_STINCLUDESRVCNUMBEREND))
     {
       StaticTextField child = new StaticTextField(this,
         getDefaultModel(),
         CHILD_STINCLUDESRVCNUMBEREND,
         CHILD_STINCLUDESRVCNUMBEREND,
         CHILD_STINCLUDESRVCNUMBEREND_RESET_VALUE,
         null);
       return child;
     }
    //--Release2.1--//
    //// New link to toggle language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new session
    //// language id throughout the all modulus.
    else
    if (name.equals(CHILD_TOGGLELANGUAGEHREF))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_TOGGLELANGUAGEHREF,
        CHILD_TOGGLELANGUAGEHREF,
        CHILD_TOGGLELANGUAGEHREF_RESET_VALUE,
        null);
        return child;
    }
    //==============================================================
    //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
    //--> By Billy 06Jan2004
    else
    if(name.equals(CHILD_TBHOLDFORHRS))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBHOLDFORHRS,
        CHILD_TBHOLDFORHRS,
        CHILD_TBHOLDFORHRS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBHOLDFORMINS))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBHOLDFORMINS,
        CHILD_TBHOLDFORMINS,
        CHILD_TBHOLDFORMINS_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_CBHOLDREASON))
    {
      ComboBox child = new ComboBox( this,
        getDefaultModel(),
        CHILD_CBHOLDREASON,
        CHILD_CBHOLDREASON,
        CHILD_CBHOLDREASON_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbHoldReasonOptions);
      return child;
    }
    //========================================================
    else
      throw new IllegalArgumentException("Invalid child name [" + name + "]");
  }

  /**
   *
   *
   */
  public void resetChildren()
  {
      super.resetChildren(); 
    getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
    getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
    getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
    getHref1().setValue(CHILD_HREF1_RESET_VALUE);
    getHref2().setValue(CHILD_HREF2_RESET_VALUE);
    getHref3().setValue(CHILD_HREF3_RESET_VALUE);
    getHref4().setValue(CHILD_HREF4_RESET_VALUE);
    getHref5().setValue(CHILD_HREF5_RESET_VALUE);
    getHref6().setValue(CHILD_HREF6_RESET_VALUE);
    getHref7().setValue(CHILD_HREF7_RESET_VALUE);
    getHref8().setValue(CHILD_HREF8_RESET_VALUE);
    getStApprovalTypeLabel().setValue(CHILD_STAPPROVALTYPELABEL_RESET_VALUE);
    getHref9().setValue(CHILD_HREF9_RESET_VALUE);
    getHref10().setValue(CHILD_HREF10_RESET_VALUE);
    getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
    getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
    getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
    getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
    getStDealId().setValue(CHILD_STDEALID_RESET_VALUE);
    getStBorrFirstName().setValue(CHILD_STBORRFIRSTNAME_RESET_VALUE);
    getStDealStatus().setValue(CHILD_STDEALSTATUS_RESET_VALUE);
    getStDealStatusDate().setValue(CHILD_STDEALSTATUSDATE_RESET_VALUE);
    getStSourceFirm().setValue(CHILD_STSOURCEFIRM_RESET_VALUE);
    getStSource().setValue(CHILD_STSOURCE_RESET_VALUE);
    getStLOB().setValue(CHILD_STLOB_RESET_VALUE);
    getStDealType().setValue(CHILD_STDEALTYPE_RESET_VALUE);
    getStDealPurpose().setValue(CHILD_STDEALPURPOSE_RESET_VALUE);
    getStPurchasePrice().setValue(CHILD_STPURCHASEPRICE_RESET_VALUE);
    getStPmtTerm().setValue(CHILD_STPMTTERM_RESET_VALUE);
    getStEstClosingDate().setValue(CHILD_STESTCLOSINGDATE_RESET_VALUE);
    getStSpecialFeature().setValue(CHILD_STSPECIALFEATURE_RESET_VALUE);
    getBtSubmit().setValue(CHILD_BTSUBMIT_RESET_VALUE);
    getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
    getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
    getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
    getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
    getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
    getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
    getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
    getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
    getStTotalLoanAmount().setValue(CHILD_STTOTALLOANAMOUNT_RESET_VALUE);
    getBtCancel().setValue(CHILD_BTCANCEL_RESET_VALUE);
    getRbApproveDeal().setValue(CHILD_RBAPPROVEDEAL_RESET_VALUE);
    getRbHoldPendingInfo().setValue(CHILD_RBHOLDPENDINGINFO_RESET_VALUE);
    getRbDeny().setValue(CHILD_RBDENY_RESET_VALUE);
    getRbCollapseDeal().setValue(CHILD_RBCOLLAPSEDEAL_RESET_VALUE);
    getCbDenialOptions().setValue(CHILD_CBDENIALOPTIONS_RESET_VALUE);
    getBtPrevTaskPage().setValue(CHILD_BTPREVTASKPAGE_RESET_VALUE);
    getStPrevTaskPageLabel().setValue(CHILD_STPREVTASKPAGELABEL_RESET_VALUE);
    getBtNextTaskPage().setValue(CHILD_BTNEXTTASKPAGE_RESET_VALUE);
    getStNextTaskPageLabel().setValue(CHILD_STNEXTTASKPAGELABEL_RESET_VALUE);
    getStTaskName().setValue(CHILD_STTASKNAME_RESET_VALUE);
    getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
    getCbApprovalType().setValue(CHILD_CBAPPROVALTYPE_RESET_VALUE);
    getTbHoldForDays().setValue(CHILD_TBHOLDFORDAYS_RESET_VALUE);
    getCbHoldUntilMonth().setValue(CHILD_CBHOLDUNTILMONTH_RESET_VALUE);
    getTbHoldUntilDay().setValue(CHILD_TBHOLDUNTILDAY_RESET_VALUE);
    getTbHoldUntilYear().setValue(CHILD_TBHOLDUNTILYEAR_RESET_VALUE);
    getRbHoldFor().setValue(CHILD_RBHOLDFOR_RESET_VALUE);
    getRbHoldUntil().setValue(CHILD_RBHOLDUNTIL_RESET_VALUE);
    getTbBrokerNoteText().setValue(CHILD_TBBROKERNOTETEXT_RESET_VALUE);
    getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
    getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
    getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
    getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
    getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
    getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
    getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
    getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
    getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
    getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
    getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
    getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
    getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
    getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
    getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
    getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
    getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
    getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
    getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
    getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
    getStViewOnlyTag().setValue(CHILD_STVIEWONLYTAG_RESET_VALUE);
    getBtOk().setValue(CHILD_BTOK_RESET_VALUE);
    getBtToUWSheet().setValue(CHILD_BTTOUWSHEET_RESET_VALUE);
    //// new hidden button to activate 'fake' submit button from the ActiveMessage class.
    getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);
    ////SYNCADD.
     getStApprovalServicingNumLabel().setValue(CHILD_STAPPROVALSERVICINGNUMLABEL_RESET_VALUE);
     getTbServMortgNum().setValue(CHILD_TBSERVMORTGNUM_RESET_VALUE);
    ////Suppress or not suppress Approve Option and Mortgage Servicing Number.
    getStIncludeApproveOptonStart().setValue(CHILD_STINCLUDEAPPROVEOPTIONSTART_RESET_VALUE);
    getStIncludeApproveOptonEnd().setValue(CHILD_STINCLUDEAPPROVEOPTIONEND_RESET_VALUE);
    getStIncludeSrvcNumberStart().setValue(CHILD_STINCLUDESRVCNUMBERSTART_RESET_VALUE);
    getStIncludeSrvcNumberEnd().setValue(CHILD_STINCLUDESRVCNUMBEREND_RESET_VALUE);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
    getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);
    //==============================================================
    //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
    //--> By Billy 06Jan2004
    getTbHoldForHrs().setValue(CHILD_TBHOLDFORHRS_RESET_VALUE);
    getTbHoldForMins().setValue(CHILD_TBHOLDFORMINS_RESET_VALUE);
    getCbHoldReason().setValue(CHILD_CBHOLDREASON_RESET_VALUE);
    //==============================================================
  }


  /**
   *
   *
   */
  protected void registerChildren()
  {
    super.registerChildren(); 
    registerChild(CHILD_TBDEALID,TextField.class);
    registerChild(CHILD_CBPAGENAMES,ComboBox.class);
    registerChild(CHILD_BTPROCEED,Button.class);
    registerChild(CHILD_HREF1,HREF.class);
    registerChild(CHILD_HREF2,HREF.class);
    registerChild(CHILD_HREF3,HREF.class);
    registerChild(CHILD_HREF4,HREF.class);
    registerChild(CHILD_HREF5,HREF.class);
    registerChild(CHILD_HREF6,HREF.class);
    registerChild(CHILD_HREF7,HREF.class);
    registerChild(CHILD_HREF8,HREF.class);
    registerChild(CHILD_STAPPROVALTYPELABEL,StaticTextField.class);
    registerChild(CHILD_HREF9,HREF.class);
    registerChild(CHILD_HREF10,HREF.class);
    registerChild(CHILD_STPAGELABEL,StaticTextField.class);
    registerChild(CHILD_STCOMPANYNAME,StaticTextField.class);
    registerChild(CHILD_STTODAYDATE,StaticTextField.class);
    registerChild(CHILD_STUSERNAMETITLE,StaticTextField.class);
    registerChild(CHILD_STDEALID,StaticTextField.class);
    registerChild(CHILD_STBORRFIRSTNAME,StaticTextField.class);
    registerChild(CHILD_STDEALSTATUS,StaticTextField.class);
    registerChild(CHILD_STDEALSTATUSDATE,StaticTextField.class);
    registerChild(CHILD_STSOURCEFIRM,StaticTextField.class);
    registerChild(CHILD_STSOURCE,StaticTextField.class);
    registerChild(CHILD_STLOB,StaticTextField.class);
    registerChild(CHILD_STDEALTYPE,StaticTextField.class);
    registerChild(CHILD_STDEALPURPOSE,StaticTextField.class);
    registerChild(CHILD_STPURCHASEPRICE,StaticTextField.class);
    registerChild(CHILD_STPMTTERM,StaticTextField.class);
    registerChild(CHILD_STESTCLOSINGDATE,StaticTextField.class);
    registerChild(CHILD_STSPECIALFEATURE,StaticTextField.class);
    registerChild(CHILD_BTSUBMIT,Button.class);
    registerChild(CHILD_BTWORKQUEUELINK,Button.class);
    registerChild(CHILD_CHANGEPASSWORDHREF,HREF.class);
    registerChild(CHILD_BTTOOLHISTORY,Button.class);
    registerChild(CHILD_BTTOONOTES,Button.class);
    registerChild(CHILD_BTTOOLSEARCH,Button.class);
    registerChild(CHILD_BTTOOLLOG,Button.class);
    registerChild(CHILD_STERRORFLAG,StaticTextField.class);
    registerChild(CHILD_DETECTALERTTASKS,HiddenField.class);
    registerChild(CHILD_STTOTALLOANAMOUNT,StaticTextField.class);
    registerChild(CHILD_BTCANCEL,Button.class);
    registerChild(CHILD_RBAPPROVEDEAL,RadioButtonGroup.class);
    registerChild(CHILD_RBHOLDPENDINGINFO,RadioButtonGroup.class);
    registerChild(CHILD_RBDENY,RadioButtonGroup.class);
    registerChild(CHILD_RBCOLLAPSEDEAL,RadioButtonGroup.class);
    registerChild(CHILD_CBDENIALOPTIONS,ComboBox.class);
    registerChild(CHILD_BTPREVTASKPAGE,Button.class);
    registerChild(CHILD_STPREVTASKPAGELABEL,StaticTextField.class);
    registerChild(CHILD_BTNEXTTASKPAGE,Button.class);
    registerChild(CHILD_STNEXTTASKPAGELABEL,StaticTextField.class);
    registerChild(CHILD_STTASKNAME,StaticTextField.class);
    registerChild(CHILD_SESSIONUSERID,HiddenField.class);
    registerChild(CHILD_CBAPPROVALTYPE,ComboBox.class);
    registerChild(CHILD_TBHOLDFORDAYS,TextField.class);
    registerChild(CHILD_CBHOLDUNTILMONTH,ComboBox.class);
    registerChild(CHILD_TBHOLDUNTILDAY,TextField.class);
    registerChild(CHILD_TBHOLDUNTILYEAR,TextField.class);
    registerChild(CHILD_RBHOLDFOR,RadioButtonGroup.class);
    registerChild(CHILD_RBHOLDUNTIL,RadioButtonGroup.class);
    registerChild(CHILD_TBBROKERNOTETEXT,TextField.class);
    registerChild(CHILD_STPMGENERATE,StaticTextField.class);
    registerChild(CHILD_STPMHASTITLE,StaticTextField.class);
    registerChild(CHILD_STPMHASINFO,StaticTextField.class);
    registerChild(CHILD_STPMHASTABLE,StaticTextField.class);
    registerChild(CHILD_STPMHASOK,StaticTextField.class);
    registerChild(CHILD_STPMTITLE,StaticTextField.class);
    registerChild(CHILD_STPMINFOMSG,StaticTextField.class);
    registerChild(CHILD_STPMONOK,StaticTextField.class);
    registerChild(CHILD_STPMMSGS,StaticTextField.class);
    registerChild(CHILD_STPMMSGTYPES,StaticTextField.class);
    registerChild(CHILD_STAMGENERATE,StaticTextField.class);
    registerChild(CHILD_STAMHASTITLE,StaticTextField.class);
    registerChild(CHILD_STAMHASINFO,StaticTextField.class);
    registerChild(CHILD_STAMHASTABLE,StaticTextField.class);
    registerChild(CHILD_STAMTITLE,StaticTextField.class);
    registerChild(CHILD_STAMINFOMSG,StaticTextField.class);
    registerChild(CHILD_STAMMSGS,StaticTextField.class);
    registerChild(CHILD_STAMMSGTYPES,StaticTextField.class);
    registerChild(CHILD_STAMDIALOGMSG,StaticTextField.class);
    registerChild(CHILD_STAMBUTTONSHTML,StaticTextField.class);
    registerChild(CHILD_STVIEWONLYTAG,StaticTextField.class);
    registerChild(CHILD_BTOK,Button.class);
    registerChild(CHILD_BTTOUWSHEET,Button.class);
    //// New hidden button implementation.
    registerChild(CHILD_BTACTMSG,Button.class);
    //// SYNCADD.
     registerChild(CHILD_STAPPROVALSERVICINGNUMLABEL,StaticTextField.class);
     registerChild(CHILD_TBSERVMORTGNUM,TextField.class);
    ////Suppress or not suppress Approve Option and Mortgage Servicing Number.
     registerChild(CHILD_STINCLUDEAPPROVEOPTIONSTART,StaticTextField.class);
     registerChild(CHILD_STINCLUDEAPPROVEOPTIONEND,StaticTextField.class);
     registerChild(CHILD_STINCLUDESRVCNUMBERSTART,StaticTextField.class);
     registerChild(CHILD_STINCLUDESRVCNUMBEREND,StaticTextField.class);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
    registerChild(CHILD_TOGGLELANGUAGEHREF,HREF.class);
    //==============================================================
    //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
    //--> By Billy 06Jan2004
    registerChild(CHILD_TBHOLDFORHRS,TextField.class);
    registerChild(CHILD_TBHOLDFORMINS,TextField.class);
    registerChild(CHILD_CBHOLDREASON,ComboBox.class);
    //==============================================================
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Model management methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  public Model[] getWebActionModels(int executionType)
  {
    List modelList=new ArrayList();
    switch(executionType)
    {
      case MODEL_TYPE_RETRIEVE:
        modelList.add(getdoDealSummarySnapShotModel());;
        break;

      case MODEL_TYPE_UPDATE:
        ;
        break;

      case MODEL_TYPE_DELETE:
        ;
        break;

      case MODEL_TYPE_INSERT:
        ;
        break;

      case MODEL_TYPE_EXECUTE:
        ;
        break;
    }
    return (Model[])modelList.toArray(new Model[0]);
  }


  ////////////////////////////////////////////////////////////////////////////////
  // View flow control methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  public void beginDisplay(DisplayEvent event)
    throws ModelControlException
  {
    DealResolutionHandler handler = (DealResolutionHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By John 08Nov2002
    //Setup Options for cbPageNames
    //cbPageNamesOptions..populate(getRequestContext());
	int langId = handler.getTheSessionState().getLanguageId();
    cbPageNamesOptions.populate(getRequestContext(), langId);

    //Setup NoneSelected Label for cbPageNames
    CHILD_CBPAGENAMES_NONSELECTED_LABEL =
      BXResources.getGenericMsg("CBPAGENAMES_NONSELECTED_LABEL",
      handler.getTheSessionState().getLanguageId());

    //Check if the cbPageNames is already created
    if(getCbPageNames() != null)
      getCbPageNames().setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);

    cbDenialOptionsOptions.populate(getRequestContext());
    //// Repopulate CBAPPROVALTYPE Comboboxe as well.
    cbApprovalTypeOptions.populate(getRequestContext());

    CHILD_STAPPROVALSERVICINGNUMLABEL_RESET_VALUE=
      BXResources.getGenericMsg("SERVICINGNUMLABEL_RESET_VALUE",
      handler.getTheSessionState().getLanguageId());
    //=====================================

    //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
    //--> By Billy 06Jan2004
    // populate options for HoldReason
    cbHoldReasonOptions.populate(getRequestContext());
    //=============================================================

    handler.pageSaveState();
    super.beginDisplay(event);
  }


  /**
   *
   *
   */
  public boolean beforeModelExecutes(Model model, int executionContext)
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    handler.setupBeforePageGeneration();

    handler.pageSaveState();

    // This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
    return super.beforeModelExecutes(model, executionContext);


    // The following code block was migrated from the this_onBeforeDataObjectExecuteEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    handler.setupBeforePageGeneration();

    handler.pageSaveState();

    return;
    */
  }


  /**
   *
   *
   */
  public void afterModelExecutes(Model model, int executionContext)
  {

    // This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
    super.afterModelExecutes(model, executionContext);

  }


  /**
   *
   *
   */
  public void afterAllModelsExecute(int executionContext)
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    handler.populatePageDisplayFields();

    handler.pageSaveState();

    // This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
    super.afterAllModelsExecute(executionContext);


    // The following code block was migrated from the this_onBeforeRowDisplayEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    int rc = PROCEED;

    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    handler.populatePageDisplayFields();

    handler.pageSaveState();

    return(rc);
    */
  }


  /**
   *
   *
   */
  public void onModelError(Model model, int executionContext, ModelControlException exception)
    throws ModelControlException
  {

    // This is the analog of NetDynamics this_onDataObjectErrorEvent
    super.onModelError(model, executionContext, exception);

  }


  /**
   *
   *
   */
  public TextField getTbDealId()
  {
    return (TextField)getChild(CHILD_TBDEALID);
  }


  /**
   *
   *
   */
  public ComboBox getCbPageNames()
  {
    return (ComboBox)getChild(CHILD_CBPAGENAMES);
  }

  /**
   *
   *
   */
  //--DJ_Ticket661--start--//
  //Modified to sort by PageLabel based on the selected language
  static class CbPageNamesOptionList extends GotoOptionList
 {
    /**
     *
     *
     */
    CbPageNamesOptionList() {

    }


    public void populate(RequestContext rc) {
      Connection c = null;
      try {
        clear();
        SelectQueryModel m = null;

        SelectQueryExecutionContext eContext = new
          SelectQueryExecutionContext(
          (Connection)null,

          DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,

          DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null) {
          m = new doPageNameLabelModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else {
          m = (SelectQueryModel)rc.getModelManager().getModel(doPageNameLabelModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        // Sort the results from DataObject
        TreeMap sorted = new TreeMap();
        while (m.next()) {
          Object dfPageLabel =
            m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
          String label = (dfPageLabel == null ? "" : dfPageLabel.toString());
          Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
          String value = (dfPageId == null ? "" : dfPageId.toString());
          String[] theVal = new String[2];
          theVal[0] = label;
          theVal[1] = value;
          sorted.put(label, theVal);
        }

        // Set the sorted list to the optionlist
        Iterator theList = sorted.values().iterator();
        String[] theVal = new String[2];
        while (theList.hasNext()) {
          theVal = (String[]) (theList.next());
          add(theVal[0], theVal[1]);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      finally {
        try {
          if (c != null)
            c.close();
        }
        catch (SQLException ex) {
              // ignore
        }
      }
    }
 }
 //--DJ_Ticket661--end--//

  //--Release2.1--start//
  /**
   *
   *
   */
  static class CbDenialOptionsOptionList extends OptionList
  {
    /**
     *
     *
     */
    CbDenialOptionsOptionList()
    {

    }


    /**
     * Modification : New requirement from Product to eliminate the reason with
     *    catageory not equals 0 or 1 :: Ticket # 329
     *    -- By Billy 06April2004
     *
     */
    public void populate(RequestContext rc)
    {
      Connection c = null;
      Statement query = null;

      try
      {
        clear();
        if(rc == null) {
          c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
        }
        else {
          c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
        }

        query = c.createStatement();
        ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);

        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
            (SessionStateModelImpl)rc.getModelManager().getModel(
                            SessionStateModel.class,
                            defaultInstanceStateName,
                            true);

        int languageId = theSessionState.getLanguageId();

        while(results.next())
        {
          String denialReasonid = results.getObject("denialreasonid").toString();
          String denialReasonDesc 
              = BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
                                                   "DENIALREASON", denialReasonid, languageId);

          add(denialReasonDesc, denialReasonid);
        }
        results.close();
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
      finally {
        try {
          if(c != null) c.close();
          if(query != null) query.close();
        }
        catch (SQLException  ex) {
          // ignore
        }
      }
    }
    private static String FETCH_DATA_STATEMENT="select denialreasonid from denialreason where catageory = 0 or catageory = 1";
  }

  //// The content of the combobox should be translated.
  static class CbApprovalTypeOptions extends OptionList
  {
    /**
     *
     *
     */
    CbApprovalTypeOptions()
    {

    }


    /**
     *
     *
     */
    public void populate(RequestContext rc)
    {
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
            (SessionStateModelImpl)rc.getModelManager().getModel(
                            SessionStateModel.class,
                            defaultInstanceStateName,
                            true);

        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                            "APPROVALTYPE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];

        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

  //--Release2.1--end//
  /**
   *
   *
   */
  public void handleBtProceedRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleGoPage();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btProceed_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleGoPage();

    handler.postHandlerProtocol();

    return PROCEED;
    */
  }


  /**
   *
   *
   */
  public Button getBtProceed()
  {
    return (Button)getChild(CHILD_BTPROCEED);
  }


  /**
   *
   *
   */
  public void handleHref1Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(0);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href1_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(0);

    return handler.postHandlerProtocol();
    */
  }


  /**
   *
   *
   */
  public HREF getHref1()
  {
    return (HREF)getChild(CHILD_HREF1);
  }


  /**
   *
   *
   */
  public void handleHref2Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(1);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href2_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(1);

    return handler.postHandlerProtocol();
    */
  }


  /**
   *
   *
   */
  public HREF getHref2()
  {
    return (HREF)getChild(CHILD_HREF2);
  }


  /**
   *
   *
   */
  public void handleHref3Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(2);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href3_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(2);

    return handler.postHandlerProtocol();
    */
  }


  /**
   *
   *
   */
  public HREF getHref3()
  {
    return (HREF)getChild(CHILD_HREF3);
  }


  /**
   *
   *
   */
  public void handleHref4Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(3);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href4_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(3);

    return handler.postHandlerProtocol();
    */
  }


  /**
   *
   *
   */
  public HREF getHref4()
  {
    return (HREF)getChild(CHILD_HREF4);
  }


  /**
   *
   *
   */
  public void handleHref5Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(4);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href5_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(4);

    return handler.postHandlerProtocol();
    */
  }


  /**
   *
   *
   */
  public HREF getHref5()
  {
    return (HREF)getChild(CHILD_HREF5);
  }


  /**
   *
   *
   */
  public void handleHref6Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(5);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href6_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(5);

    return handler.postHandlerProtocol();
    */
  }


  /**
   *
   *
   */
  public HREF getHref6()
  {
    return (HREF)getChild(CHILD_HREF6);
  }


  /**
   *
   *
   */
  public void handleHref7Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(6);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href7_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(6);

    return handler.postHandlerProtocol();
    */
  }


  /**
   *
   *
   */
  public HREF getHref7()
  {
    return (HREF)getChild(CHILD_HREF7);
  }


  /**
   *
   *
   */
  public void handleHref8Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(7);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href8_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(7);

    return handler.postHandlerProtocol();
    */
  }


  /**
   *
   *
   */
  public HREF getHref8()
  {
    return (HREF)getChild(CHILD_HREF8);
  }


  /**
   *
   *
   */
  public StaticTextField getStApprovalTypeLabel()
  {
    return (StaticTextField)getChild(CHILD_STAPPROVALTYPELABEL);
  }


  /**
   *
   *
   */
  public void handleHref9Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(8);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href9_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(8);

    return handler.postHandlerProtocol();
    */
  }


  /**
   *
   *
   */
  public HREF getHref9()
  {
    return (HREF)getChild(CHILD_HREF9);
  }


  /**
   *
   *
   */
  public void handleHref10Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(9);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href10_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(9);

    return handler.postHandlerProtocol();
    */
  }


  /**
   *
   *
   */
  public HREF getHref10()
  {
    return (HREF)getChild(CHILD_HREF10);
  }


  /**
   *
   *
   */
  public StaticTextField getStPageLabel()
  {
    return (StaticTextField)getChild(CHILD_STPAGELABEL);
  }


  /**
   *
   *
   */
  public StaticTextField getStCompanyName()
  {
    return (StaticTextField)getChild(CHILD_STCOMPANYNAME);
  }


  /**
   *
   *
   */
  public StaticTextField getStTodayDate()
  {
    return (StaticTextField)getChild(CHILD_STTODAYDATE);
  }


  /**
   *
   *
   */
  public StaticTextField getStUserNameTitle()
  {
    return (StaticTextField)getChild(CHILD_STUSERNAMETITLE);
  }


  /**
   *
   *
   */
  public StaticTextField getStDealId()
  {
    return (StaticTextField)getChild(CHILD_STDEALID);
  }


  /**
   *
   *
   */
  public StaticTextField getStBorrFirstName()
  {
    return (StaticTextField)getChild(CHILD_STBORRFIRSTNAME);
  }


  /**
   *
   *
   */
  public StaticTextField getStDealStatus()
  {
    return (StaticTextField)getChild(CHILD_STDEALSTATUS);
  }


  /**
   *
   *
   */
  public StaticTextField getStDealStatusDate()
  {
    return (StaticTextField)getChild(CHILD_STDEALSTATUSDATE);
  }


  /**
   *
   *
   */
  public StaticTextField getStSourceFirm()
  {
    return (StaticTextField)getChild(CHILD_STSOURCEFIRM);
  }


  /**
   *
   *
   */
  public StaticTextField getStSource()
  {
    return (StaticTextField)getChild(CHILD_STSOURCE);
  }


  /**
   *
   *
   */
  public StaticTextField getStLOB()
  {
    return (StaticTextField)getChild(CHILD_STLOB);
  }


  /**
   *
   *
   */
  public StaticTextField getStDealType()
  {
    return (StaticTextField)getChild(CHILD_STDEALTYPE);
  }


  /**
   *
   *
   */
  public StaticTextField getStDealPurpose()
  {
    return (StaticTextField)getChild(CHILD_STDEALPURPOSE);
  }


  /**
   *
   *
   */
  public StaticTextField getStPurchasePrice()
  {
    return (StaticTextField)getChild(CHILD_STPURCHASEPRICE);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmtTerm()
  {
    return (StaticTextField)getChild(CHILD_STPMTTERM);
  }


  /**
   *
   *
   */
  public StaticTextField getStEstClosingDate()
  {
    return (StaticTextField)getChild(CHILD_STESTCLOSINGDATE);
  }


  /**
   *
   *
   */
  public StaticTextField getStSpecialFeature()
  {
    return (StaticTextField)getChild(CHILD_STSPECIALFEATURE);
  }


  /**
   *
   *
   */
  public void handleBtSubmitRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleDealResolutionSubmit();

    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtSubmit()
  {
    return (Button)getChild(CHILD_BTSUBMIT);
  }


  /**
   *
   *
   */
  public String endBtSubmitDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btSubmit_onBeforeHtmlOutputEvent method
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(getParentViewBean());

    boolean rc = handler.displaySubmitButton();

    handler.pageSaveState();

    if(rc == true)
      return event.getContent();
    else
      return "";

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    int rc = handler.displaySubmitButton();

    handler.pageSaveState();

    return rc;
    */
  }

  //// Set of the methods to hide the Approval option under certain circumstances.
  //// In ND it was implemented on the Page level (via the setUnvisible, ND proprietary
  //// methods), in JATO it should better do accessing each HTML element separately.


  /**
   *
   *
   */
  public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleDisplayWorkQueue();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btWorkQueueLink_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleDisplayWorkQueue();

    handler.postHandlerProtocol();

    return PROCEED;
    */
  }


  /**
   *
   *
   */
  public Button getBtWorkQueueLink()
  {
    return (Button)getChild(CHILD_BTWORKQUEUELINK);
  }


  /**
   *
   *
   */
  public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleChangePassword();

    handler.postHandlerProtocol();

    // The following code block was migrated from the changePasswordHref_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleChangePassword();

    handler.postHandlerProtocol();

    return PROCEED;
    */
  }


  /**
   *
   *
   */
  public HREF getChangePasswordHref()
  {
    return (HREF)getChild(CHILD_CHANGEPASSWORDHREF);
  }

  //--Release2.1--start//
  //// Two new methods providing the business logic for the new link to toggle language.
  //// When touched this link should reload the page in opposite language (french versus english)
  //// and set this new session value.
  /**
   *
   *
   */
  public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this, true);

    handler.handleToggleLanguage();

    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getToggleLanguageHref()
  {
    return (HREF)getChild(CHILD_TOGGLELANGUAGEHREF);
  }

  //--Release2.1--end//
  /**
   *
   *
   */
  public void handleBtToolHistoryRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleDisplayDealHistory();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btToolHistory_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleDisplayDealHistory();

    handler.postHandlerProtocol();

    return PROCEED;
    */
  }


  /**
   *
   *
   */
  public Button getBtToolHistory()
  {
    return (Button)getChild(CHILD_BTTOOLHISTORY);
  }


  /**
   *
   *
   */
  public void handleBtTooNotesRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleDisplayDealNotes(true);

    handler.postHandlerProtocol();

    // The following code block was migrated from the btTooNotes_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleDisplayDealNotes(true);

    handler.postHandlerProtocol();

    return PROCEED;
    */
  }


  /**
   *
   *
   */
  public Button getBtTooNotes()
  {
    return (Button)getChild(CHILD_BTTOONOTES);
  }


  /**
   *
   *
   */
  public void handleBtToolSearchRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleDealSearch();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btToolSearch_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleDealSearch();

    handler.postHandlerProtocol();

    return PROCEED;
    */
  }


  /**
   *
   *
   */
  public Button getBtToolSearch()
  {
    return (Button)getChild(CHILD_BTTOOLSEARCH);
  }


  /**
   *
   *
   */
  public void handleBtToolLogRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleSignOff();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btToolLog_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleSignOff();

    handler.postHandlerProtocol();

    return;
    */
  }


  /**
   *
   *
   */
  public Button getBtToolLog()
  {
    return (Button)getChild(CHILD_BTTOOLLOG);
  }


  /**
   *
   *
   */
  public StaticTextField getStErrorFlag()
  {
    return (StaticTextField)getChild(CHILD_STERRORFLAG);
  }


  /**
   *
   *
   */
  public HiddenField getDetectAlertTasks()
  {
    return (HiddenField)getChild(CHILD_DETECTALERTTASKS);
  }


  /**
   *
   *
   */
  public StaticTextField getStTotalLoanAmount()
  {
    return (StaticTextField)getChild(CHILD_STTOTALLOANAMOUNT);
  }

////////
  /**
   *
   * SYNCADD method.
   */
   public StaticTextField getStApprovalServicingNumLabel()
   {
     return (StaticTextField)getChild(CHILD_STAPPROVALSERVICINGNUMLABEL);
  }

  /**
   *
   * SYNCADD method.
   */
   public TextField getTbServMortgNum()
   {
     return (TextField)getChild(CHILD_TBSERVMORTGNUM);
   }

  ////Suppress or not suppress Approve Option and Mortgage Servicing Number set of methods.
  /**
   *
   *
   */
   public StaticTextField getStIncludeApproveOptonStart()
   {
     return (StaticTextField)getChild(CHILD_STINCLUDEAPPROVEOPTIONSTART);
  }

  /**
   *
   *
   */
   public StaticTextField getStIncludeApproveOptonEnd()
   {
     return (StaticTextField)getChild(CHILD_STINCLUDEAPPROVEOPTIONEND);
  }

  /**
   *
   *
   */
   public StaticTextField getStIncludeSrvcNumberStart()
   {
     return (StaticTextField)getChild(CHILD_STINCLUDESRVCNUMBERSTART);
  }

  /**
   *
   *
   */
   public StaticTextField getStIncludeSrvcNumberEnd()
   {
     return (StaticTextField)getChild(CHILD_STINCLUDESRVCNUMBEREND);
  }

  /**
   *
   *
   */
  public void handleBtCancelRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleCancelStandard();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btCancel_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleCancelStandard();

    handler.postHandlerProtocol();

    return PROCEED;
    */
  }


  /**
   *
   *
   */
  public Button getBtCancel()
  {
    return (Button)getChild(CHILD_BTCANCEL);
  }


  /**
   *
   *
   */
  public String endBtCancelDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btCancel_onBeforeHtmlOutputEvent method
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(getParentViewBean());

    boolean rc = handler.displayCancelButton();

    handler.pageSaveState();

    if(rc == true)
      return event.getContent();
    else
      return "";

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    int rc = handler.displayCancelButton();

    handler.pageSaveState();

    return rc;
    */
  }


  /**
   *
   *
   */
  public RadioButtonGroup getRbApproveDeal()
  {
    return (RadioButtonGroup)getChild(CHILD_RBAPPROVEDEAL);
  }


  /**
   *
   *
   */
  public RadioButtonGroup getRbHoldPendingInfo()
  {

    return (RadioButtonGroup)getChild(CHILD_RBHOLDPENDINGINFO);
  }


  /**
   *
   *
   */
  public RadioButtonGroup getRbDeny()
  {
    return (RadioButtonGroup)getChild(CHILD_RBDENY);
  }


  /**
   *
   *
   */
  public RadioButtonGroup getRbCollapseDeal()
  {
    return (RadioButtonGroup)getChild(CHILD_RBCOLLAPSEDEAL);
  }


  /**
   *
   *
   */
  public ComboBox getCbDenialOptions()
  {
    return (ComboBox)getChild(CHILD_CBDENIALOPTIONS);
  }


  /**
   *
   *
   */
  public String endCbDenialOptionsDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the cbDenialOptions_onBeforeHtmlOutputEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    return;
    */


    return event.getContent();
  }


  /**
   *
   *
   */
  public void handleBtPrevTaskPageRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handlePrevTaskPage();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btPrevTaskPage_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handlePrevTaskPage();

    handler.postHandlerProtocol();

    return PROCEED;
    */
  }


  /**
   *
   *
   */
  public Button getBtPrevTaskPage()
  {
    return (Button)getChild(CHILD_BTPREVTASKPAGE);
  }


  /**
   *
   *
   */
  public String endBtPrevTaskPageDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btPrevTaskPage_onBeforeHtmlOutputEvent method
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(getParentViewBean());

    boolean rc = handler.generatePrevTaskPage();

    handler.pageSaveState();

    if(rc == true)
      return event.getContent();
    else
      return "";


    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    int rc = handler.generatePrevTaskPage();

    handler.pageSaveState();

    return rc;
    */
  }


  /**
   *
   *
   */
  public StaticTextField getStPrevTaskPageLabel()
  {
    return (StaticTextField)getChild(CHILD_STPREVTASKPAGELABEL);
  }


  /**
   *
   *
   */
  public String endStPrevTaskPageLabelDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the stPrevTaskPageLabel_onBeforeHtmlOutputEvent method
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(getParentViewBean());

    boolean rc = handler.generatePrevTaskPage();

    handler.pageSaveState();

    if(rc == true)
      return event.getContent();
    else
      return "";

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    int rc = handler.generatePrevTaskPage();

    handler.pageSaveState();

    return rc;
    */
  }


  /**
   *
   *
   */
  public void handleBtNextTaskPageRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleNextTaskPage();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btNextTaskPage_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleNextTaskPage();

    handler.postHandlerProtocol();

    return PROCEED;
    */
  }


  /**
   *
   *
   */
  public Button getBtNextTaskPage()
  {
    return (Button)getChild(CHILD_BTNEXTTASKPAGE);
  }


  /**
   *
   *
   */
  public String endBtNextTaskPageDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btNextTaskPage_onBeforeHtmlOutputEvent method
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(getParentViewBean());

    boolean rc = handler.generateNextTaskPage();

    handler.pageSaveState();

    if(rc == true)
      return event.getContent();
    else
      return "";

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    int rc = handler.generateNextTaskPage();

    handler.pageSaveState();

    return rc;
    */
  }


  /**
   *
   *
   */
  public StaticTextField getStNextTaskPageLabel()
  {
    return (StaticTextField)getChild(CHILD_STNEXTTASKPAGELABEL);
  }


  /**
   *
   *
   */
  public String endStNextTaskPageLabelDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the stNextTaskPageLabel_onBeforeHtmlOutputEvent method
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(getParentViewBean());

    boolean rc = handler.generateNextTaskPage();

    handler.pageSaveState();

    if(rc == true)
      return event.getContent();
    else
      return "";

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    int rc = handler.generateNextTaskPage();

    handler.pageSaveState();

    return rc;
    */
  }


  /**
   *
   *
   */
  public StaticTextField getStTaskName()
  {
    return (StaticTextField)getChild(CHILD_STTASKNAME);
  }


  /**
   *
   *
   */
  public String endStTaskNameDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the stTaskName_onBeforeHtmlOutputEvent method
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(getParentViewBean());

    boolean rc = handler.generateTaskName();

    handler.pageSaveState();

    if(rc == true)
      return event.getContent();
    else
      return "";

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    int rc = handler.generateTaskName();

    handler.pageSaveState();

    return rc;
    */
  }


  /**
   *
   *
   */
  public HiddenField getSessionUserId()
  {
    return (HiddenField)getChild(CHILD_SESSIONUSERID);
  }


  /**
   *
   *
   */
  public ComboBox getCbApprovalType()
  {
    return (ComboBox)getChild(CHILD_CBAPPROVALTYPE);
  }


  /**
   *
   *
   */
  public TextField getTbHoldForDays()
  {
    return (TextField)getChild(CHILD_TBHOLDFORDAYS);
  }


  /**
   *
   *
   */
  public ComboBox getCbHoldUntilMonth()
  {
    return (ComboBox)getChild(CHILD_CBHOLDUNTILMONTH);
  }


  /**
   *
   *
   */
  public TextField getTbHoldUntilDay()
  {
    return (TextField)getChild(CHILD_TBHOLDUNTILDAY);
  }


  /**
   *
   *
   */
  public TextField getTbHoldUntilYear()
  {
    return (TextField)getChild(CHILD_TBHOLDUNTILYEAR);
  }


  /**
   *
   *
   */
  public RadioButtonGroup getRbHoldFor()
  {
    return (RadioButtonGroup)getChild(CHILD_RBHOLDFOR);
  }


  /**
   *
   *
   */
  public RadioButtonGroup getRbHoldUntil()
  {
    return (RadioButtonGroup)getChild(CHILD_RBHOLDUNTIL);
  }


  /**
   *
   *
   */
  public TextField getTbBrokerNoteText()
  {
    return (TextField)getChild(CHILD_TBBROKERNOTETEXT);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmGenerate()
  {
    return (StaticTextField)getChild(CHILD_STPMGENERATE);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmHasTitle()
  {
    return (StaticTextField)getChild(CHILD_STPMHASTITLE);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmHasInfo()
  {
    return (StaticTextField)getChild(CHILD_STPMHASINFO);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmHasTable()
  {
    return (StaticTextField)getChild(CHILD_STPMHASTABLE);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmHasOk()
  {
    return (StaticTextField)getChild(CHILD_STPMHASOK);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmTitle()
  {
    return (StaticTextField)getChild(CHILD_STPMTITLE);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmInfoMsg()
  {
    return (StaticTextField)getChild(CHILD_STPMINFOMSG);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmOnOk()
  {
    return (StaticTextField)getChild(CHILD_STPMONOK);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmMsgs()
  {
    return (StaticTextField)getChild(CHILD_STPMMSGS);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmMsgTypes()
  {
    return (StaticTextField)getChild(CHILD_STPMMSGTYPES);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmGenerate()
  {
    return (StaticTextField)getChild(CHILD_STAMGENERATE);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmHasTitle()
  {
    return (StaticTextField)getChild(CHILD_STAMHASTITLE);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmHasInfo()
  {
    return (StaticTextField)getChild(CHILD_STAMHASINFO);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmHasTable()
  {
    return (StaticTextField)getChild(CHILD_STAMHASTABLE);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmTitle()
  {
    return (StaticTextField)getChild(CHILD_STAMTITLE);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmInfoMsg()
  {
    return (StaticTextField)getChild(CHILD_STAMINFOMSG);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmMsgs()
  {
    return (StaticTextField)getChild(CHILD_STAMMSGS);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmMsgTypes()
  {
    return (StaticTextField)getChild(CHILD_STAMMSGTYPES);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmDialogMsg()
  {
    return (StaticTextField)getChild(CHILD_STAMDIALOGMSG);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmButtonsHtml()
  {
    return (StaticTextField)getChild(CHILD_STAMBUTTONSHTML);
  }


  /**
   *
   *
   */
  public StaticTextField getStViewOnlyTag()
  {
    return (StaticTextField)getChild(CHILD_STVIEWONLYTAG);
  }


  /**
   *
   *
   */
  public String endStViewOnlyTagDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the stViewOnlyTag_onBeforeHtmlOutputEvent method
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    String rc = handler.displayViewOnlyTag();

    handler.pageSaveState();

    return rc;

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    int rc = handler.displayViewOnlyTag((DisplayField) event.getSource());

    handler.pageSaveState();

    return rc;
    */
  }


  public void handleBtOkRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

	  if (handler.isSpecialWorkflowTrigerEnable()) {
		  handler.handleOKSpecial();
	  } else {
		  handler.handleCancelStandard();
	  }

    handler.postHandlerProtocol();

  }


  /**
   *
   *
   */
  public Button getBtOk()
  {
    return (Button)getChild(CHILD_BTOK);
  }


  /**
   *
   *
   */
  public String endBtOkDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btOk_onBeforeHtmlOutputEvent method
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(getParentViewBean());

    boolean rc = handler.displayOKButton();

    handler.pageSaveState();

    if(rc == true)
      return event.getContent();
    else
      return "";

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    int rc = handler.displayOKButton();

    handler.pageSaveState();

    return rc;
    */
  }


  /**
   *
   *
   */
  public void handleBtToUWSheetRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleToUWSheet();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btToUWSheet_onWebEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleToUWSheet();

    handler.postHandlerProtocol();

    return;
    */
  }


  /**
   *
   *
   */
  public Button getBtToUWSheet()
  {
    return (Button)getChild(CHILD_BTTOUWSHEET);
  }


  /**
   *
   *
   */
  public doDealSummarySnapShotModel getdoDealSummarySnapShotModel()
  {
    if (doDealSummarySnapShot == null)
      doDealSummarySnapShot = (doDealSummarySnapShotModel) getModel(doDealSummarySnapShotModel.class);
    return doDealSummarySnapShot;
  }


  /**
   *
   *
   */
  public void setdoDealSummarySnapShotModel(doDealSummarySnapShotModel model)
  {
      doDealSummarySnapShot = model;
  }


  //// New hidden button for the ActiveMessage 'fake' submit button.
  public Button getBtActMsg()
  {
    return (Button)getChild(CHILD_BTACTMSG);
  }

  //// Populate previous links new set of methods to populate Href display Strings.
  public String endHref1Display(ChildContentDisplayEvent event)
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 0);
    handler.pageSaveState();

    return rc;
  }
  public String endHref2Display(ChildContentDisplayEvent event)
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 1);
    handler.pageSaveState();

    return rc;
  }
  public String endHref3Display(ChildContentDisplayEvent event)
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 2);
    handler.pageSaveState();

    return rc;
  }
  public String endHref4Display(ChildContentDisplayEvent event)
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 3);
    handler.pageSaveState();

    return rc;
  }
  public String endHref5Display(ChildContentDisplayEvent event)
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 4);
    handler.pageSaveState();

    return rc;
  }
  public String endHref6Display(ChildContentDisplayEvent event)
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 5);
    handler.pageSaveState();

    return rc;
  }
  public String endHref7Display(ChildContentDisplayEvent event)
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 6);
    handler.pageSaveState();

    return rc;
  }
  public String endHref8Display(ChildContentDisplayEvent event)
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 7);
    handler.pageSaveState();

    return rc;
  }
  public String endHref9Display(ChildContentDisplayEvent event)
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 8);
    handler.pageSaveState();

    return rc;
  }
  public String endHref10Display(ChildContentDisplayEvent event)
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 9);
    handler.pageSaveState();

    return rc;
  }

  //// This method overrides the getDefaultURL() framework JATO method and it is located
  //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
  //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
  //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
  //// The full method is still in PHC base class. It should care the
  //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
  public String getDisplayURL()
  {
       DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();
       handler.pageGetState(this);

       String url=getDefaultDisplayURL();

       int languageId = handler.theSessionState.getLanguageId();

       //// Call the language specific URL (business delegation done in the BXResource).
       if(url != null && !url.trim().equals("") && !url.trim().equals("/")) {
          url = BXResources.getBXUrl(url, languageId);
       }
       else {
          url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
       }

      return url;
  }

  //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
  //--> By Billy 06Jan2004
  public TextField getTbHoldForHrs()
  {
    return (TextField)getChild(CHILD_TBHOLDFORHRS);
  }

  public TextField getTbHoldForMins()
  {
    return (TextField)getChild(CHILD_TBHOLDFORMINS);
  }

  static class CbHoldReasonOptionList extends OptionList
  {
    CbHoldReasonOptionList()
    {
    }

    public void populate(RequestContext rc)
    {
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
            (SessionStateModelImpl)rc.getModelManager().getModel(
                            SessionStateModel.class,
                            defaultInstanceStateName,
                            true);

        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                            "HOLDREASON", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];

        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

  public ComboBox getCbHoldReason()
  {
    return (ComboBox)getChild(CHILD_CBHOLDREASON);
  }

  //=============================================================

  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Custom Methods - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////



  //]]SPIDER_EVENT<btNextTaskPage_onWebEvent>

  /* MigrationToDo : Migrate custom method
  public int handleActMessageOK(String[] args)
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleActMessageOK(args);

    handler.postHandlerProtocol();

    return;
  }
   */

  public void handleActMessageOK(String[] args)
  {
    DealResolutionHandler handler =(DealResolutionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleActMessageOK(args);

    handler.postHandlerProtocol();
  }

  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////

  public static final String PAGE_NAME="pgDealResolution";
  //// It is a variable now (see explanation in the getDisplayURL() method above.
  ////public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgDealResolution.jsp";
  public static Map FIELD_DESCRIPTORS;
  public static final String CHILD_TBDEALID="tbDealId";
  public static final String CHILD_TBDEALID_RESET_VALUE="";
  public static final String CHILD_CBPAGENAMES="cbPageNames";
  public static final String CHILD_CBPAGENAMES_RESET_VALUE="";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 08Nov2002
  //private static CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  private CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  //Added the Variable for NonSelected Label
  private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";
  //==================================================================

  public static final String CHILD_BTPROCEED="btProceed";
  public static final String CHILD_BTPROCEED_RESET_VALUE="Proceed";
  public static final String CHILD_HREF1="Href1";
  public static final String CHILD_HREF1_RESET_VALUE="";
  public static final String CHILD_HREF2="Href2";
  public static final String CHILD_HREF2_RESET_VALUE="";
  public static final String CHILD_HREF3="Href3";
  public static final String CHILD_HREF3_RESET_VALUE="";
  public static final String CHILD_HREF4="Href4";
  public static final String CHILD_HREF4_RESET_VALUE="";
  public static final String CHILD_HREF5="Href5";
  public static final String CHILD_HREF5_RESET_VALUE="";
  public static final String CHILD_HREF6="Href6";
  public static final String CHILD_HREF6_RESET_VALUE="";
  public static final String CHILD_HREF7="Href7";
  public static final String CHILD_HREF7_RESET_VALUE="";
  public static final String CHILD_HREF8="Href8";
  public static final String CHILD_HREF8_RESET_VALUE="";
  public static final String CHILD_STAPPROVALTYPELABEL="stApprovalTypeLabel";
  public static final String CHILD_STAPPROVALTYPELABEL_RESET_VALUE="";
  public static final String CHILD_HREF9="Href9";
  public static final String CHILD_HREF9_RESET_VALUE="";
  public static final String CHILD_HREF10="Href10";
  public static final String CHILD_HREF10_RESET_VALUE="";
  public static final String CHILD_STPAGELABEL="stPageLabel";
  public static final String CHILD_STPAGELABEL_RESET_VALUE="";
  public static final String CHILD_STCOMPANYNAME="stCompanyName";
  public static final String CHILD_STCOMPANYNAME_RESET_VALUE="";
  public static final String CHILD_STTODAYDATE="stTodayDate";
  public static final String CHILD_STTODAYDATE_RESET_VALUE="";
  public static final String CHILD_STUSERNAMETITLE="stUserNameTitle";
  public static final String CHILD_STUSERNAMETITLE_RESET_VALUE="";
  public static final String CHILD_STDEALID="stDealId";
  public static final String CHILD_STDEALID_RESET_VALUE="";
  public static final String CHILD_STBORRFIRSTNAME="stBorrFirstName";
  public static final String CHILD_STBORRFIRSTNAME_RESET_VALUE="";
  public static final String CHILD_STDEALSTATUS="stDealStatus";
  public static final String CHILD_STDEALSTATUS_RESET_VALUE="";
  public static final String CHILD_STDEALSTATUSDATE="stDealStatusDate";
  public static final String CHILD_STDEALSTATUSDATE_RESET_VALUE="";
  public static final String CHILD_STSOURCEFIRM="stSourceFirm";
  public static final String CHILD_STSOURCEFIRM_RESET_VALUE="";
  public static final String CHILD_STSOURCE="stSource";
  public static final String CHILD_STSOURCE_RESET_VALUE="";
  public static final String CHILD_STLOB="stLOB";
  public static final String CHILD_STLOB_RESET_VALUE="";
  public static final String CHILD_STDEALTYPE="stDealType";
  public static final String CHILD_STDEALTYPE_RESET_VALUE="";
  public static final String CHILD_STDEALPURPOSE="stDealPurpose";
  public static final String CHILD_STDEALPURPOSE_RESET_VALUE="";
  public static final String CHILD_STPURCHASEPRICE="stPurchasePrice";
  public static final String CHILD_STPURCHASEPRICE_RESET_VALUE="";
  public static final String CHILD_STPMTTERM="stPmtTerm";
  public static final String CHILD_STPMTTERM_RESET_VALUE="";
  public static final String CHILD_STESTCLOSINGDATE="stEstClosingDate";
  public static final String CHILD_STESTCLOSINGDATE_RESET_VALUE="";
  public static final String CHILD_STSPECIALFEATURE="stSpecialFeature";
  public static final String CHILD_STSPECIALFEATURE_RESET_VALUE="";
  public static final String CHILD_BTSUBMIT="btSubmit";
  public static final String CHILD_BTSUBMIT_RESET_VALUE=" ";
  public static final String CHILD_BTWORKQUEUELINK="btWorkQueueLink";
  public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE=" ";
  public static final String CHILD_CHANGEPASSWORDHREF="changePasswordHref";
  public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE="";
  public static final String CHILD_BTTOOLHISTORY="btToolHistory";
  public static final String CHILD_BTTOOLHISTORY_RESET_VALUE=" ";
  public static final String CHILD_BTTOONOTES="btTooNotes";
  public static final String CHILD_BTTOONOTES_RESET_VALUE=" ";
  public static final String CHILD_BTTOOLSEARCH="btToolSearch";
  public static final String CHILD_BTTOOLSEARCH_RESET_VALUE=" ";
  public static final String CHILD_BTTOOLLOG="btToolLog";
  public static final String CHILD_BTTOOLLOG_RESET_VALUE=" ";
  public static final String CHILD_STERRORFLAG="stErrorFlag";
  public static final String CHILD_STERRORFLAG_RESET_VALUE="N";
  public static final String CHILD_DETECTALERTTASKS="detectAlertTasks";
  public static final String CHILD_DETECTALERTTASKS_RESET_VALUE="";
  public static final String CHILD_STTOTALLOANAMOUNT="stTotalLoanAmount";
  public static final String CHILD_STTOTALLOANAMOUNT_RESET_VALUE="";
  public static final String CHILD_BTCANCEL="btCancel";
  public static final String CHILD_BTCANCEL_RESET_VALUE=" ";
  public static final String CHILD_RBAPPROVEDEAL="rbApproveDeal";
  public static final String CHILD_RBAPPROVEDEAL_RESET_VALUE="";
  //// Changed to access from the Handler
  ////private static OptionList rbApproveDealOptions=new OptionList(new String[]{},new String[]{});
  protected OptionList rbApproveDealOptions=new OptionList(new String[]{},new String[]{});
  public static final String CHILD_RBHOLDPENDINGINFO="rbHoldPendingInfo";
  public static final String CHILD_RBHOLDPENDINGINFO_RESET_VALUE="";
  //// Changed to access from the Handler
  ////private static OptionList rbHoldPendingInfoOptions=new OptionList(new String[]{},new String[]{});
  protected OptionList rbHoldPendingInfoOptions=new OptionList(new String[]{},new String[]{});
  public static final String CHILD_RBDENY="rbDeny";
  public static final String CHILD_RBDENY_RESET_VALUE="";
  //// Changed to access from the Handler
  ////private static OptionList rbDenyOptions=new OptionList(new String[]{},new String[]{});
  protected OptionList rbDenyOptions=new OptionList(new String[]{},new String[]{});
  public static final String CHILD_RBCOLLAPSEDEAL="rbCollapseDeal";
  public static final String CHILD_RBCOLLAPSEDEAL_RESET_VALUE="";
  //// Changed to access from the Handler
  ////private static OptionList rbCollapseDealOptions=new OptionList(new String[]{},new String[]{});
  protected OptionList rbCollapseDealOptions=new OptionList(new String[]{},new String[]{});
  public static final String CHILD_CBDENIALOPTIONS="cbDenialOptions";
  public static final String CHILD_CBDENIALOPTIONS_RESET_VALUE="";

  //// Changed to access from the Handler
  ////private static OptionList cbDenialOptionsOptions=new OptionList(new String[]{},new String[]{});
  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //  Also made an inner class to handle multilingual population better
  //--> By John 26Nov2002
  //protected static OptionList cbDenialOptionsOptions=new OptionList(new String[]{},new String[]{});
  protected CbDenialOptionsOptionList cbDenialOptionsOptions=new CbDenialOptionsOptionList();
  //==================================================================

  public static final String CHILD_BTPREVTASKPAGE="btPrevTaskPage";
  public static final String CHILD_BTPREVTASKPAGE_RESET_VALUE=" ";
  public static final String CHILD_STPREVTASKPAGELABEL="stPrevTaskPageLabel";
  public static final String CHILD_STPREVTASKPAGELABEL_RESET_VALUE="";
  public static final String CHILD_BTNEXTTASKPAGE="btNextTaskPage";
  public static final String CHILD_BTNEXTTASKPAGE_RESET_VALUE=" ";
  public static final String CHILD_STNEXTTASKPAGELABEL="stNextTaskPageLabel";
  public static final String CHILD_STNEXTTASKPAGELABEL_RESET_VALUE="";
  public static final String CHILD_STTASKNAME="stTaskName";
  public static final String CHILD_STTASKNAME_RESET_VALUE="";
  public static final String CHILD_SESSIONUSERID="sessionUserId";
  public static final String CHILD_SESSIONUSERID_RESET_VALUE="";
  public static final String CHILD_CBAPPROVALTYPE="cbApprovalType";
  public static final String CHILD_CBAPPROVALTYPE_RESET_VALUE="";
  //// Changed to access from the Handler
  ////The Option list should not be Static, otherwise this will screw up
  ////private static OptionList cbApprovalTypeOptions=new OptionList(new String[]{},new String[]{});
  protected CbApprovalTypeOptions cbApprovalTypeOptions=new CbApprovalTypeOptions();

  public static final String CHILD_TBHOLDFORDAYS="tbHoldForDays";
  public static final String CHILD_TBHOLDFORDAYS_RESET_VALUE="";
  public static final String CHILD_CBHOLDUNTILMONTH="cbHoldUntilMonth";
  public static final String CHILD_CBHOLDUNTILMONTH_RESET_VALUE="";
  //// Changed to access from the Handler
  ////private static OptionList cbHoldUntilMonthOptions=new OptionList(new String[]{},new String[]{});
  protected OptionList cbHoldUntilMonthOptions=new OptionList(new String[]{},new String[]{});
  public static final String CHILD_TBHOLDUNTILDAY="tbHoldUntilDay";
  public static final String CHILD_TBHOLDUNTILDAY_RESET_VALUE="";
  public static final String CHILD_TBHOLDUNTILYEAR="tbHoldUntilYear";
  public static final String CHILD_TBHOLDUNTILYEAR_RESET_VALUE="";
  public static final String CHILD_RBHOLDFOR="rbHoldFor";
  public static final String CHILD_RBHOLDFOR_RESET_VALUE="";
  //// Changed to access from the Handler
  ////private static OptionList rbHoldForOptions=new OptionList(new String[]{},new String[]{});
  protected OptionList rbHoldForOptions=new OptionList(new String[]{},new String[]{});
  public static final String CHILD_RBHOLDUNTIL="rbHoldUntil";
  public static final String CHILD_RBHOLDUNTIL_RESET_VALUE="";
  //// Changed to access from the Handler
  ////private static OptionList rbHoldUntilOptions=new OptionList(new String[]{},new String[]{});
  protected OptionList rbHoldUntilOptions=new OptionList(new String[]{},new String[]{});
  public static final String CHILD_TBBROKERNOTETEXT="tbBrokerNoteText";
  public static final String CHILD_TBBROKERNOTETEXT_RESET_VALUE="";
  public static final String CHILD_STPMGENERATE="stPmGenerate";
  public static final String CHILD_STPMGENERATE_RESET_VALUE="";
  public static final String CHILD_STPMHASTITLE="stPmHasTitle";
  public static final String CHILD_STPMHASTITLE_RESET_VALUE="";
  public static final String CHILD_STPMHASINFO="stPmHasInfo";
  public static final String CHILD_STPMHASINFO_RESET_VALUE="";
  public static final String CHILD_STPMHASTABLE="stPmHasTable";
  public static final String CHILD_STPMHASTABLE_RESET_VALUE="";
  public static final String CHILD_STPMHASOK="stPmHasOk";
  public static final String CHILD_STPMHASOK_RESET_VALUE="";
  public static final String CHILD_STPMTITLE="stPmTitle";
  public static final String CHILD_STPMTITLE_RESET_VALUE="";
  public static final String CHILD_STPMINFOMSG="stPmInfoMsg";
  public static final String CHILD_STPMINFOMSG_RESET_VALUE="";
  public static final String CHILD_STPMONOK="stPmOnOk";
  public static final String CHILD_STPMONOK_RESET_VALUE="";
  public static final String CHILD_STPMMSGS="stPmMsgs";
  public static final String CHILD_STPMMSGS_RESET_VALUE="";
  public static final String CHILD_STPMMSGTYPES="stPmMsgTypes";
  public static final String CHILD_STPMMSGTYPES_RESET_VALUE="";
  public static final String CHILD_STAMGENERATE="stAmGenerate";
  public static final String CHILD_STAMGENERATE_RESET_VALUE="";
  public static final String CHILD_STAMHASTITLE="stAmHasTitle";
  public static final String CHILD_STAMHASTITLE_RESET_VALUE="";
  public static final String CHILD_STAMHASINFO="stAmHasInfo";
  public static final String CHILD_STAMHASINFO_RESET_VALUE="";
  public static final String CHILD_STAMHASTABLE="stAmHasTable";
  public static final String CHILD_STAMHASTABLE_RESET_VALUE="";
  public static final String CHILD_STAMTITLE="stAmTitle";
  public static final String CHILD_STAMTITLE_RESET_VALUE="";
  public static final String CHILD_STAMINFOMSG="stAmInfoMsg";
  public static final String CHILD_STAMINFOMSG_RESET_VALUE="";
  public static final String CHILD_STAMMSGS="stAmMsgs";
  public static final String CHILD_STAMMSGS_RESET_VALUE="";
  public static final String CHILD_STAMMSGTYPES="stAmMsgTypes";
  public static final String CHILD_STAMMSGTYPES_RESET_VALUE="";
  public static final String CHILD_STAMDIALOGMSG="stAmDialogMsg";
  public static final String CHILD_STAMDIALOGMSG_RESET_VALUE="";
  public static final String CHILD_STAMBUTTONSHTML="stAmButtonsHtml";
  public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE="";
  public static final String CHILD_STVIEWONLYTAG="stViewOnlyTag";
  public static final String CHILD_STVIEWONLYTAG_RESET_VALUE="";
  public static final String CHILD_BTOK="btOk";
  public static final String CHILD_BTOK_RESET_VALUE=" ";
  public static final String CHILD_BTTOUWSHEET="btToUWSheet";
  public static final String CHILD_BTTOUWSHEET_RESET_VALUE=" ";
  //// New hidden button implementing the 'fake' one from the ActiveMessage class.
  public static final String CHILD_BTACTMSG="btActMsg";
  public static final String CHILD_BTACTMSG_RESET_VALUE="ActMsg";
  ////SYNCADD.
   public static final String CHILD_STAPPROVALSERVICINGNUMLABEL="stApprovalServicingNumLabel";
   public static String CHILD_STAPPROVALSERVICINGNUMLABEL_RESET_VALUE="";
   public static final String CHILD_TBSERVMORTGNUM="tbServMortgNum";
   public static final String CHILD_TBSERVMORTGNUM_RESET_VALUE="";
  ////Suppress or not suppress Approve Option and Mortgage Servicing Number

  public static final String CHILD_STINCLUDEAPPROVEOPTIONSTART="stIncludeApproveOptionStart";
  public static final String CHILD_STINCLUDEAPPROVEOPTIONSTART_RESET_VALUE="";
  public static final String CHILD_STINCLUDEAPPROVEOPTIONEND="stIncludeApproveOptionEnd";
  public static final String CHILD_STINCLUDEAPPROVEOPTIONEND_RESET_VALUE="";
  public static final String CHILD_STINCLUDESRVCNUMBERSTART="stIncludeSrvcNumberStart";
  public static final String CHILD_STINCLUDESRVCNUMBERSTART_RESET_VALUE="";
  public static final String CHILD_STINCLUDESRVCNUMBEREND="stIncludeSrvcNumberEnd";
  public static final String CHILD_STINCLUDESRVCNUMBEREND_RESET_VALUE="";
  //--Release2.1--//
  //// New link to toggle the language. When touched this link should reload the
  //// page in opposite language (french versus english) and set this new language
  //// session id throughout all modulus.
  public static final String CHILD_TOGGLELANGUAGEHREF = "toggleLanguageHref";
  public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE = "";

  //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
  //--> By Billy 06Jan2004
  public static final String CHILD_TBHOLDFORHRS = "tbHoldForHrs";
  public static final String CHILD_TBHOLDFORHRS_RESET_VALUE = "";
  public static final String CHILD_TBHOLDFORMINS = "tbHoldForMins";
  public static final String CHILD_TBHOLDFORMINS_RESET_VALUE = "";
  public static final String CHILD_CBHOLDREASON = "cbHoldReason";
  public static final String CHILD_CBHOLDREASON_RESET_VALUE = "";
  protected CbHoldReasonOptionList cbHoldReasonOptions = new CbHoldReasonOptionList();


  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////

  private doDealSummarySnapShotModel doDealSummarySnapShot=null;
  protected String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgDealResolution.jsp";


  ////////////////////////////////////////////////////////////////////////////
  // Custom Members - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////

  // MigrationToDo : Migrate custom member
  private DealResolutionHandler handler=new DealResolutionHandler();
  public SysLogger logger;
}

