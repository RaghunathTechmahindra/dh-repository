package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doDetailViewPropertiesModelImpl extends QueryModelBase
	implements doDetailViewPropertiesModel
{
	/**
	 *
	 *
	 */
	public doDetailViewPropertiesModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 18Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert STREETTYPE Desc.
      this.setDfStreetType(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "STREETTYPE", this.getDfStreetType(), languageId));
      //Convert STREETDIRECTION Desc.
      this.setDfStreetDirection(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "STREETDIRECTION", this.getDfStreetDirection(), languageId));
      //Convert PROVINCE Name
      this.setDfProvince(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "PROVINCE", this.getDfProvince(), languageId));
      //Convert DWELLINGTYPE Desc.
      this.setDfDwellingType(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "DWELLINGTYPE", this.getDfDwellingType(), languageId));
      //Convert DWELLINGSTYLE Desc.
      this.setDfDwellingStyle(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "DWELLINGSTYLE", this.getDfDwellingStyle(), languageId));
      //Convert HEATTYPE Desc.
      this.setDfHeatType(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "HEATTYPE", this.getDfHeatType(), languageId));
      //Convert WATERTYPE Desc.
      this.setDfWater(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "WATERTYPE", this.getDfWater(), languageId));
      //Convert SEWAGETYPE Desc.
      this.setDfSewage(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "SEWAGETYPE", this.getDfSewage(), languageId));
      //Convert PROPERTYUSAGE Desc.
      this.setDfPropertyUsage(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "PROPERTYUSAGE", this.getDfPropertyUsage(), languageId));
      //Convert OCCUPANCYTYPE Desc.
      this.setDfOccupancy(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "OCCUPANCYTYPE", this.getDfOccupancy(), languageId));
      //Convert PROPERTYTYPE Desc.
      this.setDfPropertyType(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "PROPERTYTYPE", this.getDfPropertyType(), languageId));
      //Convert PROPERTYLOCATION Desc.
      this.setDfPropertyLocation(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "PROPERTYLOCATION", this.getDfPropertyLocation(), languageId));
      //Convert APPRAISALSOURCE Desc.
      this.setDfAppraisalSource(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "APPRAISALSOURCE", this.getDfAppraisalSource(), languageId));
      //Convert NEWCONSTRUCTION Desc.
      this.setDfNewConstructionDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "NEWCONSTRUCTION", this.getDfNewConstructionDesc(), languageId));
      //Convert GARAGESIZE Desc.
      this.setDfGarageSize(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "GARAGESIZE", this.getDfGarageSize(), languageId));
      //Convert GARAGETYPE Desc.
      this.setDfGarageType(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "GARAGETYPE", this.getDfGarageType(), languageId));
      
      //--Release3.1--begins
      //--by Hiro Apr 11, 2006
      //Convert TENURETYPE Desc.
      this.setDfTenure(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "TENURETYPE", this.getDfTenure(), languageId));
      //--Release3.1--ends
      
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPrimaryProperty()
	{
		return (String)getValue(FIELD_DFPRIMARYPROPERTY);
	}


	/**
	 *
	 *
	 */
	public void setDfPrimaryProperty(String value)
	{
		setValue(FIELD_DFPRIMARYPROPERTY,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStreetNumber()
	{
		return (String)getValue(FIELD_DFSTREETNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfStreetNumber(String value)
	{
		setValue(FIELD_DFSTREETNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStreetName()
	{
		return (String)getValue(FIELD_DFSTREETNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfStreetName(String value)
	{
		setValue(FIELD_DFSTREETNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStreetType()
	{
		return (String)getValue(FIELD_DFSTREETTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfStreetType(String value)
	{
		setValue(FIELD_DFSTREETTYPE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStreetDirection()
	{
		return (String)getValue(FIELD_DFSTREETDIRECTION);
	}


	/**
	 *
	 *
	 */
	public void setDfStreetDirection(String value)
	{
		setValue(FIELD_DFSTREETDIRECTION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAddressLine()
	{
		return (String)getValue(FIELD_DFADDRESSLINE);
	}


	/**
	 *
	 *
	 */
	public void setDfAddressLine(String value)
	{
		setValue(FIELD_DFADDRESSLINE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCity()
	{
		return (String)getValue(FIELD_DFCITY);
	}


	/**
	 *
	 *
	 */
	public void setDfCity(String value)
	{
		setValue(FIELD_DFCITY,value);
	}


	/**
	 *
	 *
	 */
	public String getDfProvince()
	{
		return (String)getValue(FIELD_DFPROVINCE);
	}


	/**
	 *
	 *
	 */
	public void setDfProvince(String value)
	{
		setValue(FIELD_DFPROVINCE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPostalFSA()
	{
		return (String)getValue(FIELD_DFPOSTALFSA);
	}


	/**
	 *
	 *
	 */
	public void setDfPostalFSA(String value)
	{
		setValue(FIELD_DFPOSTALFSA,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPostalLDU()
	{
		return (String)getValue(FIELD_DFPOSTALLDU);
	}


	/**
	 *
	 *
	 */
	public void setDfPostalLDU(String value)
	{
		setValue(FIELD_DFPOSTALLDU,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLegalLine1()
	{
		return (String)getValue(FIELD_DFLEGALLINE1);
	}


	/**
	 *
	 *
	 */
	public void setDfLegalLine1(String value)
	{
		setValue(FIELD_DFLEGALLINE1,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLegalLine2()
	{
		return (String)getValue(FIELD_DFLEGALLINE2);
	}


	/**
	 *
	 *
	 */
	public void setDfLegalLine2(String value)
	{
		setValue(FIELD_DFLEGALLINE2,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLegalLine3()
	{
		return (String)getValue(FIELD_DFLEGALLINE3);
	}


	/**
	 *
	 *
	 */
	public void setDfLegalLine3(String value)
	{
		setValue(FIELD_DFLEGALLINE3,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDwellingType()
	{
		return (String)getValue(FIELD_DFDWELLINGTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfDwellingType(String value)
	{
		setValue(FIELD_DFDWELLINGTYPE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDwellingStyle()
	{
		return (String)getValue(FIELD_DFDWELLINGSTYLE);
	}


	/**
	 *
	 *
	 */
	public void setDfDwellingStyle(String value)
	{
		setValue(FIELD_DFDWELLINGSTYLE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfStructureAge()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSTRUCTUREAGE);
	}


	/**
	 *
	 *
	 */
	public void setDfStructureAge(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSTRUCTUREAGE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNoOfUnits()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFNOOFUNITS);
	}


	/**
	 *
	 *
	 */
	public void setDfNoOfUnits(java.math.BigDecimal value)
	{
		setValue(FIELD_DFNOOFUNITS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNumberOfBedRooms()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFNUMBEROFBEDROOMS);
	}


	/**
	 *
	 *
	 */
	public void setDfNumberOfBedRooms(java.math.BigDecimal value)
	{
		setValue(FIELD_DFNUMBEROFBEDROOMS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLivingSpace()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIVINGSPACE);
	}


	/**
	 *
	 *
	 */
	public void setDfLivingSpace(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIVINGSPACE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLotSize()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLOTSIZE);
	}


	/**
	 *
	 *
	 */
	public void setDfLotSize(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLOTSIZE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfHeatType()
	{
		return (String)getValue(FIELD_DFHEATTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfHeatType(String value)
	{
		setValue(FIELD_DFHEATTYPE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfUFFIInsulation()
	{
		return (String)getValue(FIELD_DFUFFIINSULATION);
	}


	/**
	 *
	 *
	 */
	public void setDfUFFIInsulation(String value)
	{
		setValue(FIELD_DFUFFIINSULATION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfWater()
	{
		return (String)getValue(FIELD_DFWATER);
	}


	/**
	 *
	 *
	 */
	public void setDfWater(String value)
	{
		setValue(FIELD_DFWATER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSewage()
	{
		return (String)getValue(FIELD_DFSEWAGE);
	}


	/**
	 *
	 *
	 */
	public void setDfSewage(String value)
	{
		setValue(FIELD_DFSEWAGE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyUsage()
	{
		return (String)getValue(FIELD_DFPROPERTYUSAGE);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyUsage(String value)
	{
		setValue(FIELD_DFPROPERTYUSAGE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfOccupancy()
	{
		return (String)getValue(FIELD_DFOCCUPANCY);
	}


	/**
	 *
	 *
	 */
	public void setDfOccupancy(String value)
	{
		setValue(FIELD_DFOCCUPANCY,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyType()
	{
		return (String)getValue(FIELD_DFPROPERTYTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyType(String value)
	{
		setValue(FIELD_DFPROPERTYTYPE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyLocation()
	{
		return (String)getValue(FIELD_DFPROPERTYLOCATION);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyLocation(String value)
	{
		setValue(FIELD_DFPROPERTYLOCATION,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfZoning()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFZONING);
	}


	/**
	 *
	 *
	 */
	public void setDfZoning(java.math.BigDecimal value)
	{
		setValue(FIELD_DFZONING,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPurchasePrice()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public void setDfPurchasePrice(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPURCHASEPRICE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLandValue()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLANDVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfLandValue(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLANDVALUE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEquityAvailable()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFEQUITYAVAILABLE);
	}


	/**
	 *
	 *
	 */
	public void setDfEquityAvailable(java.math.BigDecimal value)
	{
		setValue(FIELD_DFEQUITYAVAILABLE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEstimateValue()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFESTIMATEVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfEstimateValue(java.math.BigDecimal value)
	{
		setValue(FIELD_DFESTIMATEVALUE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfActualAppraisedValue()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFACTUALAPPRAISEDVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfActualAppraisedValue(java.math.BigDecimal value)
	{
		setValue(FIELD_DFACTUALAPPRAISEDVALUE,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfAppraisalDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFAPPRAISALDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraisalDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFAPPRAISALDATE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAppraisalSource()
	{
		return (String)getValue(FIELD_DFAPPRAISALSOURCE);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraisalSource(String value)
	{
		setValue(FIELD_DFAPPRAISALSOURCE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLotSizeUnitOfMeasureId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLOTSIZEUNITOFMEASUREID);
	}


	/**
	 *
	 *
	 */
	public void setDfLotSizeUnitOfMeasureId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLOTSIZEUNITOFMEASUREID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLivingSpaceUnitOfMeasureId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIVINGSPACEUNITOFMEASUREID);
	}


	/**
	 *
	 *
	 */
	public void setDfLivingSpaceUnitOfMeasureId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIVINGSPACEUNITOFMEASUREID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNewConstructionId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFNEWCONSTRUCTIONID);
	}


	/**
	 *
	 *
	 */
	public void setDfNewConstructionId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFNEWCONSTRUCTIONID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfNewConstructionDesc()
	{
		return (String)getValue(FIELD_DFNEWCONSTRUCTIONDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfNewConstructionDesc(String value)
	{
		setValue(FIELD_DFNEWCONSTRUCTIONDESC,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBuilderName()
	{
		return (String)getValue(FIELD_DFBUILDERNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfBuilderName(String value)
	{
		setValue(FIELD_DFBUILDERNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfGarageSize()
	{
		return (String)getValue(FIELD_DFGARAGESIZE);
	}


	/**
	 *
	 *
	 */
	public void setDfGarageSize(String value)
	{
		setValue(FIELD_DFGARAGESIZE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfGarageType()
	{
		return (String)getValue(FIELD_DFGARAGETYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfGarageType(String value)
	{
		setValue(FIELD_DFGARAGETYPE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMLSFlag()
	{
		return (String)getValue(FIELD_DFMLSFLAG);
	}


	/**
	 *
	 *
	 */
	public void setDfMLSFlag(String value)
	{
		setValue(FIELD_DFMLSFLAG,value);
	}

  //--Release3.1--begins
  //--by Hiro Apr 10, 2006
  public String getDfTenure()
  {
    return (String) getValue(FIELD_DFTENURE);
  }

  public void setDfTenure(String value)
  {
    setValue(FIELD_DFTENURE, value);
  }

  public String getDfMiEnergyEfficiency()
  {
    return (String) getValue(FIELD_DFMIENERGYEFFICIENCY);
  }

  public void setDfMiEnergyEfficiency(String value)
  {
    setValue(FIELD_DFMIENERGYEFFICIENCY, value);
  }

  public String getDfSubdivisionDiscount()
  {
    return (String) getValue(FIELD_DFSUBDIVISIONDISCOUNT);
  }

  public void setDfSubdivisionDiscount(String value)
  {
    setValue(FIELD_DFSUBDIVISIONDISCOUNT, value);
  }

  public java.math.BigDecimal getDfOnReserveTrustAgreement()
  {
    return (java.math.BigDecimal) getValue(FIELD_DFONRESERVETRUSTAGREEMENT);
  }

  public void setDfOnReserveTrustAgreement(java.math.BigDecimal value)
  {
    setValue(FIELD_DFONRESERVETRUSTAGREEMENT, value);
  }
  
  public java.math.BigDecimal getDfInstitutionId() {
	    return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
	  }

	  public void setDfInstitutionId(java.math.BigDecimal value) {
	    setValue(FIELD_DFINSTITUTIONID, value);
	  }  
  //--Release3.1--ends

	/**
	 *
	 *
	 */
	public String getDfUnitNumber()
	{
		return (String)getValue(FIELD_DFPROPERTYUNITNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfUnitNumber(String value)
	{
		setValue(FIELD_DFPROPERTYUNITNUMBER,value);
	}
	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 18Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
	//public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT PROPERTY.DEALID, PROPERTY.PRIMARYPROPERTYFLAG, PROPERTY.PROPERTYSTREETNUMBER, PROPERTY.PROPERTYSTREETNAME, STREETTYPE.STDESCRIPTION, STREETDIRECTION.SDDESCRIPTION, PROPERTY.PROPERTYADDRESSLINE2, PROPERTY.PROPERTYCITY, PROVINCE.PROVINCENAME, PROPERTY.PROPERTYPOSTALFSA, PROPERTY.PROPERTYPOSTALLDU, PROPERTY.LEGALLINE1, PROPERTY.LEGALLINE2, PROPERTY.LEGALLINE3, DWELLINGTYPE.DTDESCRIPTION, DWELLINGSTYLE.DSDESCRIPTION, PROPERTY.STRUCTUREAGE, PROPERTY.NUMBEROFUNITS, PROPERTY.NUMBEROFBEDROOMS, PROPERTY.LIVINGSPACE, PROPERTY.LOTSIZE, HEATTYPE.HTDESCRIPTION, PROPERTY.INSULATEDWITHUFFI, WATERTYPE.WTDESCRIPTION, SEWAGETYPE.STDESCRIPTION, PROPERTYUSAGE.PUDESCRIPTION, OCCUPANCYTYPE.OTDESCRIPTION, PROPERTYTYPE.PTDESCRIPTION, PROPERTYLOCATION.PROPERTYLOCATIONDESCRIPTION, PROPERTY.ZONING, PROPERTY.PURCHASEPRICE, PROPERTY.LANDVALUE, PROPERTY.EQUITYAVAILABLE, PROPERTY.ESTIMATEDAPPRAISALVALUE, PROPERTY.ACTUALAPPRAISALVALUE, PROPERTY.APPRAISALDATEACT, APPRAISALSOURCE.ASDESCRIPTION, PROPERTY.COPYID, PROPERTY.PROPERTYID, PROPERTY.LOTSIZEUNITOFMEASUREID, PROPERTY.LIVINGSPACEUNITOFMEASUREID, PROPERTY.NEWCONSTRUCTIONID, NEWCONSTRUCTION.NEWCONSTRUCTIONDESCRIPTION, PROPERTY.BUILDERNAME, GARAGESIZE.GARAGESIZEDESCRIPTION, GARAGETYPE.GARAGETYPEDESCRIPTION, PROPERTY.MLSLISTINGFLAG FROM PROPERTY, STREETTYPE, PROVINCE, PROPERTYTYPE, PROPERTYUSAGE, DWELLINGSTYLE, OCCUPANCYTYPE, HEATTYPE, DWELLINGTYPE, SEWAGETYPE, WATERTYPE, PROPERTYLOCATION, APPRAISALSOURCE, STREETDIRECTION, NEWCONSTRUCTION, GARAGESIZE, GARAGETYPE  __WHERE__  ";
  //--Release3.1--//
  //added sevral columns to the SQL
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT DISTINCT PROPERTY.DEALID, PROPERTY.PRIMARYPROPERTYFLAG, "+
    "PROPERTY.PROPERTYSTREETNUMBER, PROPERTY.PROPERTYSTREETNAME, "+
    "to_char(PROPERTY.STREETTYPEID) STREETTYPEID_STR, to_char(PROPERTY.STREETDIRECTIONID) STREETDIRECTIONID_STR, "+
    "PROPERTY.PROPERTYADDRESSLINE2, PROPERTY.PROPERTYCITY, "+
    "to_char(PROPERTY.PROVINCEID) PROVINCEID_STR, PROPERTY.PROPERTYPOSTALFSA, "+
    "PROPERTY.PROPERTYPOSTALLDU, PROPERTY.LEGALLINE1, PROPERTY.LEGALLINE2, "+
    "PROPERTY.LEGALLINE3, to_char(PROPERTY.DWELLINGTYPEID) DWELLINGTYPEID_STR, "+
    "to_char(PROPERTY.DWELLINGSTYLEID) DWELLINGSTYLEID_STR, PROPERTY.STRUCTUREAGE, "+
    "PROPERTY.NUMBEROFUNITS, PROPERTY.NUMBEROFBEDROOMS, "+
    "PROPERTY.LIVINGSPACE, PROPERTY.LOTSIZE, to_char(PROPERTY.HEATTYPEID) HEATTYPEID_STR, "+
    "PROPERTY.INSULATEDWITHUFFI, to_char(PROPERTY.WATERTYPEID) WATERTYPEID_STR, "+
    "to_char(PROPERTY.SEWAGETYPEID) SEWAGETYPEID_STR, to_char(PROPERTY.PROPERTYUSAGEID) PROPERTYUSAGEID_STR, "+
    "to_char(PROPERTY.OCCUPANCYTYPEID) OCCUPANCYTYPEID_STR, to_char(PROPERTY.PROPERTYTYPEID) PROPERTYTYPEID_STR, "+
    "to_char(PROPERTY.PROPERTYLOCATIONID) PROPERTYLOCATIONID_STR, PROPERTY.ZONING, "+
    "PROPERTY.PURCHASEPRICE, PROPERTY.LANDVALUE, PROPERTY.EQUITYAVAILABLE, "+
    "PROPERTY.ESTIMATEDAPPRAISALVALUE, PROPERTY.ACTUALAPPRAISALVALUE, "+
    "PROPERTY.APPRAISALDATEACT, to_char(PROPERTY.APPRAISALSOURCEID) APPRAISALSOURCEID_STR, PROPERTY.COPYID, "+
    "PROPERTY.PROPERTYID, PROPERTY.LOTSIZEUNITOFMEASUREID, PROPERTY.LIVINGSPACEUNITOFMEASUREID, "+
    "PROPERTY.NEWCONSTRUCTIONID, to_char(PROPERTY.NEWCONSTRUCTIONID) NEWCONSTRUCTIONID_STR, "+
    "PROPERTY.BUILDERNAME, to_char(PROPERTY.GARAGESIZEID) GARAGESIZEID_STR, "+
    "to_char(PROPERTY.GARAGETYPEID) GARAGETYPEID_STR, PROPERTY.MLSLISTINGFLAG "+
    // Release3.1 begins
    ",to_char(PROPERTY.TENURETYPEID) TENURETYPEID_STR ," +
    "PROPERTY.MIENERGYEFFICIENCY, " + 
    "PROPERTY.SUBDIVISIONDISCOUNT, " +
    "PROPERTY.ONRESERVETRUSTAGREEMENTNUMBER ONRESERVETRUSTAGREEMENT, " + 
    "PROPERTY.INSTITUTIONPROFILEID, "+
    "PROPERTY.UNITNUMBER "+
    // Release3.1 ends
    " FROM PROPERTY  "+
    "__WHERE__  ";
	//--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //public static final String MODIFYING_QUERY_TABLE_NAME="PROPERTY, STREETTYPE, PROVINCE, PROPERTYTYPE, PROPERTYUSAGE, DWELLINGSTYLE, OCCUPANCYTYPE, HEATTYPE, DWELLINGTYPE, SEWAGETYPE, WATERTYPE, PROPERTYLOCATION, APPRAISALSOURCE, STREETDIRECTION, NEWCONSTRUCTION, GARAGESIZE, GARAGETYPE";
	public static final String MODIFYING_QUERY_TABLE_NAME=
    "PROPERTY";
	//--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
  //public static final String STATIC_WHERE_CRITERIA=" (PROPERTY.STREETTYPEID  =  STREETTYPE.STREETTYPEID) AND (PROPERTY.PROVINCEID  =  PROVINCE.PROVINCEID) AND (PROPERTY.PROPERTYTYPEID  =  PROPERTYTYPE.PROPERTYTYPEID) AND (PROPERTY.PROPERTYUSAGEID  =  PROPERTYUSAGE.PROPERTYUSAGEID) AND (PROPERTY.DWELLINGSTYLEID  =  DWELLINGSTYLE.DWELLINGSTYLEID) AND (PROPERTY.OCCUPANCYTYPEID  =  OCCUPANCYTYPE.OCCUPANCYTYPEID) AND (PROPERTY.HEATTYPEID  =  HEATTYPE.HEATTYPEID) AND (PROPERTY.DWELLINGTYPEID  =  DWELLINGTYPE.DWELLINGTYPEID) AND (PROPERTY.SEWAGETYPEID  =  SEWAGETYPE.SEWAGETYPEID) AND (PROPERTY.WATERTYPEID  =  WATERTYPE.WATERTYPEID) AND (PROPERTY.PROPERTYLOCATIONID  =  PROPERTYLOCATION.PROPERTYLOCATIONID) AND (PROPERTY.APPRAISALSOURCEID  =  APPRAISALSOURCE.APPRAISALSOURCEID) AND (PROPERTY.STREETDIRECTIONID  =  STREETDIRECTION.STREETDIRECTIONID) AND (PROPERTY.NEWCONSTRUCTIONID  =  NEWCONSTRUCTION.NEWCONSTRUCTIONID) AND (PROPERTY.GARAGETYPEID  =  GARAGETYPE.GARAGETYPEID) AND (PROPERTY.GARAGESIZEID  =  GARAGESIZE.GARAGESIZEID)";
	public static final String STATIC_WHERE_CRITERIA=
    "";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="PROPERTY.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFPRIMARYPROPERTY="PROPERTY.PRIMARYPROPERTYFLAG";
	public static final String COLUMN_DFPRIMARYPROPERTY="PRIMARYPROPERTYFLAG";
	public static final String QUALIFIED_COLUMN_DFSTREETNUMBER="PROPERTY.PROPERTYSTREETNUMBER";
	public static final String COLUMN_DFSTREETNUMBER="PROPERTYSTREETNUMBER";
	public static final String QUALIFIED_COLUMN_DFSTREETNAME="PROPERTY.PROPERTYSTREETNAME";
	public static final String COLUMN_DFSTREETNAME="PROPERTYSTREETNAME";
	public static final String QUALIFIED_COLUMN_DFADDRESSLINE="PROPERTY.PROPERTYADDRESSLINE2";
	public static final String COLUMN_DFADDRESSLINE="PROPERTYADDRESSLINE2";
	public static final String QUALIFIED_COLUMN_DFCITY="PROPERTY.PROPERTYCITY";
	public static final String COLUMN_DFCITY="PROPERTYCITY";
	public static final String QUALIFIED_COLUMN_DFPOSTALFSA="PROPERTY.PROPERTYPOSTALFSA";
	public static final String COLUMN_DFPOSTALFSA="PROPERTYPOSTALFSA";
	public static final String QUALIFIED_COLUMN_DFPOSTALLDU="PROPERTY.PROPERTYPOSTALLDU";
	public static final String COLUMN_DFPOSTALLDU="PROPERTYPOSTALLDU";
	public static final String QUALIFIED_COLUMN_DFLEGALLINE1="PROPERTY.LEGALLINE1";
	public static final String COLUMN_DFLEGALLINE1="LEGALLINE1";
	public static final String QUALIFIED_COLUMN_DFLEGALLINE2="PROPERTY.LEGALLINE2";
	public static final String COLUMN_DFLEGALLINE2="LEGALLINE2";
	public static final String QUALIFIED_COLUMN_DFLEGALLINE3="PROPERTY.LEGALLINE3";
	public static final String COLUMN_DFLEGALLINE3="LEGALLINE3";
	public static final String QUALIFIED_COLUMN_DFSTRUCTUREAGE="PROPERTY.STRUCTUREAGE";
	public static final String COLUMN_DFSTRUCTUREAGE="STRUCTUREAGE";
	public static final String QUALIFIED_COLUMN_DFNOOFUNITS="PROPERTY.NUMBEROFUNITS";
	public static final String COLUMN_DFNOOFUNITS="NUMBEROFUNITS";
	public static final String QUALIFIED_COLUMN_DFNUMBEROFBEDROOMS="PROPERTY.NUMBEROFBEDROOMS";
	public static final String COLUMN_DFNUMBEROFBEDROOMS="NUMBEROFBEDROOMS";
	public static final String QUALIFIED_COLUMN_DFLIVINGSPACE="PROPERTY.LIVINGSPACE";
	public static final String COLUMN_DFLIVINGSPACE="LIVINGSPACE";
	public static final String QUALIFIED_COLUMN_DFLOTSIZE="PROPERTY.LOTSIZE";
	public static final String COLUMN_DFLOTSIZE="LOTSIZE";
	public static final String QUALIFIED_COLUMN_DFUFFIINSULATION="PROPERTY.INSULATEDWITHUFFI";
	public static final String COLUMN_DFUFFIINSULATION="INSULATEDWITHUFFI";
	public static final String QUALIFIED_COLUMN_DFZONING="PROPERTY.ZONING";
	public static final String COLUMN_DFZONING="ZONING";
	public static final String QUALIFIED_COLUMN_DFPURCHASEPRICE="PROPERTY.PURCHASEPRICE";
	public static final String COLUMN_DFPURCHASEPRICE="PURCHASEPRICE";
	public static final String QUALIFIED_COLUMN_DFLANDVALUE="PROPERTY.LANDVALUE";
	public static final String COLUMN_DFLANDVALUE="LANDVALUE";
	public static final String QUALIFIED_COLUMN_DFEQUITYAVAILABLE="PROPERTY.EQUITYAVAILABLE";
	public static final String COLUMN_DFEQUITYAVAILABLE="EQUITYAVAILABLE";
	public static final String QUALIFIED_COLUMN_DFESTIMATEVALUE="PROPERTY.ESTIMATEDAPPRAISALVALUE";
	public static final String COLUMN_DFESTIMATEVALUE="ESTIMATEDAPPRAISALVALUE";
	public static final String QUALIFIED_COLUMN_DFACTUALAPPRAISEDVALUE="PROPERTY.ACTUALAPPRAISALVALUE";
	public static final String COLUMN_DFACTUALAPPRAISEDVALUE="ACTUALAPPRAISALVALUE";
	public static final String QUALIFIED_COLUMN_DFAPPRAISALDATE="PROPERTY.APPRAISALDATEACT";
	public static final String COLUMN_DFAPPRAISALDATE="APPRAISALDATEACT";
	public static final String QUALIFIED_COLUMN_DFCOPYID="PROPERTY.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYID="PROPERTY.PROPERTYID";
	public static final String COLUMN_DFPROPERTYID="PROPERTYID";
	public static final String QUALIFIED_COLUMN_DFLOTSIZEUNITOFMEASUREID="PROPERTY.LOTSIZEUNITOFMEASUREID";
	public static final String COLUMN_DFLOTSIZEUNITOFMEASUREID="LOTSIZEUNITOFMEASUREID";
	public static final String QUALIFIED_COLUMN_DFLIVINGSPACEUNITOFMEASUREID="PROPERTY.LIVINGSPACEUNITOFMEASUREID";
	public static final String COLUMN_DFLIVINGSPACEUNITOFMEASUREID="LIVINGSPACEUNITOFMEASUREID";
	public static final String QUALIFIED_COLUMN_DFNEWCONSTRUCTIONID="PROPERTY.NEWCONSTRUCTIONID";
	public static final String COLUMN_DFNEWCONSTRUCTIONID="NEWCONSTRUCTIONID";
	public static final String QUALIFIED_COLUMN_DFBUILDERNAME="PROPERTY.BUILDERNAME";
	public static final String COLUMN_DFBUILDERNAME="BUILDERNAME";
	public static final String QUALIFIED_COLUMN_DFMLSFLAG="PROPERTY.MLSLISTINGFLAG";
	public static final String COLUMN_DFMLSFLAG="MLSLISTINGFLAG";

	//--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFSTREETTYPE="STREETTYPE.STDESCRIPTION";
	//public static final String COLUMN_DFSTREETTYPE="STDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFSTREETTYPE="PROPERTY.STREETTYPEID_STR";
	public static final String COLUMN_DFSTREETTYPE="STREETTYPEID_STR";
	//public static final String QUALIFIED_COLUMN_DFSTREETDIRECTION="STREETDIRECTION.SDDESCRIPTION";
	//public static final String COLUMN_DFSTREETDIRECTION="SDDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFSTREETDIRECTION="PROPERTY.STREETDIRECTIONID_STR";
	public static final String COLUMN_DFSTREETDIRECTION="STREETDIRECTIONID_STR";
  //public static final String QUALIFIED_COLUMN_DFPROVINCE="PROVINCE.PROVINCENAME";
	//public static final String COLUMN_DFPROVINCE="PROVINCENAME";
  public static final String QUALIFIED_COLUMN_DFPROVINCE="PROPERTY.PROVINCEID_STR";
	public static final String COLUMN_DFPROVINCE="PROVINCEID_STR";
  //public static final String QUALIFIED_COLUMN_DFDWELLINGTYPE="DWELLINGTYPE.DTDESCRIPTION";
	//public static final String COLUMN_DFDWELLINGTYPE="DTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFDWELLINGTYPE="PROPERTY.DWELLINGTYPEID_STR";
	public static final String COLUMN_DFDWELLINGTYPE="DWELLINGTYPEID_STR";
	//public static final String QUALIFIED_COLUMN_DFDWELLINGSTYLE="DWELLINGSTYLE.DSDESCRIPTION";
	//public static final String COLUMN_DFDWELLINGSTYLE="DSDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFDWELLINGSTYLE="PROPERTY.DWELLINGSTYLEID_STR";
	public static final String COLUMN_DFDWELLINGSTYLE="DWELLINGSTYLEID_STR";
  //public static final String QUALIFIED_COLUMN_DFHEATTYPE="HEATTYPE.HTDESCRIPTION";
	//public static final String COLUMN_DFHEATTYPE="HTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFHEATTYPE="PROPERTY.HEATTYPEID_STR";
	public static final String COLUMN_DFHEATTYPE="HEATTYPEID_STR";
  //public static final String QUALIFIED_COLUMN_DFWATER="WATERTYPE.WTDESCRIPTION";
	//public static final String COLUMN_DFWATER="WTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFWATER="PROPERTY.WATERTYPEID_STR";
	public static final String COLUMN_DFWATER="WATERTYPEID_STR";
	//public static final String QUALIFIED_COLUMN_DFSEWAGE="SEWAGETYPE.STDESCRIPTION";
	//public static final String COLUMN_DFSEWAGE="STDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFSEWAGE="PROPERTY.SEWAGETYPEID_STR";
	public static final String COLUMN_DFSEWAGE="SEWAGETYPEID_STR";
  //public static final String QUALIFIED_COLUMN_DFPROPERTYUSAGE="PROPERTYUSAGE.PUDESCRIPTION";
	//public static final String COLUMN_DFPROPERTYUSAGE="PUDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFPROPERTYUSAGE="PROPERTY.PROPERTYUSAGEID_STR";
	public static final String COLUMN_DFPROPERTYUSAGE="PROPERTYUSAGEID_STR";
	//public static final String QUALIFIED_COLUMN_DFOCCUPANCY="OCCUPANCYTYPE.OTDESCRIPTION";
	//public static final String COLUMN_DFOCCUPANCY="OTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFOCCUPANCY="PROPERTY.OCCUPANCYTYPEID_STR";
	public static final String COLUMN_DFOCCUPANCY="OCCUPANCYTYPEID_STR";
	//public static final String QUALIFIED_COLUMN_DFPROPERTYTYPE="PROPERTYTYPE.PTDESCRIPTION";
	//public static final String COLUMN_DFPROPERTYTYPE="PTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFPROPERTYTYPE="PROPERTY.PROPERTYTYPEID_STR";
	public static final String COLUMN_DFPROPERTYTYPE="PROPERTYTYPEID_STR";
	//public static final String QUALIFIED_COLUMN_DFPROPERTYLOCATION="PROPERTYLOCATION.PROPERTYLOCATIONDESCRIPTION";
	//public static final String COLUMN_DFPROPERTYLOCATION="PROPERTYLOCATIONDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFPROPERTYLOCATION="PROPERTY.PROPERTYLOCATIONID_STR";
	public static final String COLUMN_DFPROPERTYLOCATION="PROPERTYLOCATIONID_STR";
  //public static final String QUALIFIED_COLUMN_DFAPPRAISALSOURCE="APPRAISALSOURCE.ASDESCRIPTION";
	//public static final String COLUMN_DFAPPRAISALSOURCE="ASDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFAPPRAISALSOURCE="PROPERTY.APPRAISALSOURCEID_STR";
	public static final String COLUMN_DFAPPRAISALSOURCE="APPRAISALSOURCEID_STR";
  //public static final String QUALIFIED_COLUMN_DFNEWCONSTRUCTIONDESC="NEWCONSTRUCTION.NEWCONSTRUCTIONDESCRIPTION";
	//public static final String COLUMN_DFNEWCONSTRUCTIONDESC="NEWCONSTRUCTIONDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFNEWCONSTRUCTIONDESC="PROPERTY.NEWCONSTRUCTIONID_STR";
	public static final String COLUMN_DFNEWCONSTRUCTIONDESC="NEWCONSTRUCTIONID_STR";
  //public static final String QUALIFIED_COLUMN_DFGARAGESIZE="GARAGESIZE.GARAGESIZEDESCRIPTION";
	//public static final String COLUMN_DFGARAGESIZE="GARAGESIZEDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFGARAGESIZE="PROPERTY.GARAGESIZEID_STR";
	public static final String COLUMN_DFGARAGESIZE="GARAGESIZEID_STR";
	//public static final String QUALIFIED_COLUMN_DFGARAGETYPE="GARAGETYPE.GARAGETYPEDESCRIPTION";
	//public static final String COLUMN_DFGARAGETYPE="GARAGETYPEDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFGARAGETYPE="PROPERTY.GARAGETYPEID_STR";
	public static final String COLUMN_DFGARAGETYPE="GARAGETYPEID_STR";

  //--Release3.1--begins
  //--by Hiro Apr 10, 2006
  
  public static final String QUALIFIED_COLUMN_DFTENURETYPEID = "PROPERTY.TENURETYPEID_STR";
  public static final String COLUMN_DFTENURETYPEID = "TENURETYPEID_STR";

  public static final String QUALIFIED_COLUMN_DFMIENERGYEFFICIENCY = "PROPERTY.MIENERGYEFFICIENCY";
  public static final String COLUMN_DFMIENERGYEFFICIENCY = "MIENERGYEFFICIENCY";

  public static final String QUALIFIED_COLUMN_DFSUBDIVISIONDISCOUNT = "PROPERTY.SUBDIVISIONDISCOUNT";
  public static final String COLUMN_DFSUBDIVISIONDISCOUNT = "SUBDIVISIONDISCOUNT";

//  public static final String QUALIFIED_COLUMN_DFONRESERVETRUSTAGREEMENTNUMBER = "PROPERTY.ONRESERVETRUSTAGREEMENTNUMBER";
  public static final String QUALIFIED_COLUMN_DFONRESERVETRUSTAGREEMENTNUMBER = "PROPERTY.ONRESERVETRUSTAGREEMENT";
  public static final String COLUMN_DFONRESERVETRUSTAGREEMENTNUMBER = "ONRESERVETRUSTAGREEMENT";

  //--Release3.1--ends


  public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
	    "PROPERTY.INSTITUTIONPROFILEID";
	  public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
  
  public static final String QUALIFIED_COLUMN_DFPROPERTYUNITNUMBER="PROPERTY.UNITNUMBER";
  public static final String COLUMN_DFPROPERTYUNITNUMBER="UNITNUMBER";
		  
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIMARYPROPERTY,
				COLUMN_DFPRIMARYPROPERTY,
				QUALIFIED_COLUMN_DFPRIMARYPROPERTY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTREETNUMBER,
				COLUMN_DFSTREETNUMBER,
				QUALIFIED_COLUMN_DFSTREETNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTREETNAME,
				COLUMN_DFSTREETNAME,
				QUALIFIED_COLUMN_DFSTREETNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTREETTYPE,
				COLUMN_DFSTREETTYPE,
				QUALIFIED_COLUMN_DFSTREETTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTREETDIRECTION,
				COLUMN_DFSTREETDIRECTION,
				QUALIFIED_COLUMN_DFSTREETDIRECTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRESSLINE,
				COLUMN_DFADDRESSLINE,
				QUALIFIED_COLUMN_DFADDRESSLINE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCITY,
				COLUMN_DFCITY,
				QUALIFIED_COLUMN_DFCITY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROVINCE,
				COLUMN_DFPROVINCE,
				QUALIFIED_COLUMN_DFPROVINCE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPOSTALFSA,
				COLUMN_DFPOSTALFSA,
				QUALIFIED_COLUMN_DFPOSTALFSA,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPOSTALLDU,
				COLUMN_DFPOSTALLDU,
				QUALIFIED_COLUMN_DFPOSTALLDU,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLEGALLINE1,
				COLUMN_DFLEGALLINE1,
				QUALIFIED_COLUMN_DFLEGALLINE1,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLEGALLINE2,
				COLUMN_DFLEGALLINE2,
				QUALIFIED_COLUMN_DFLEGALLINE2,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLEGALLINE3,
				COLUMN_DFLEGALLINE3,
				QUALIFIED_COLUMN_DFLEGALLINE3,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDWELLINGTYPE,
				COLUMN_DFDWELLINGTYPE,
				QUALIFIED_COLUMN_DFDWELLINGTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDWELLINGSTYLE,
				COLUMN_DFDWELLINGSTYLE,
				QUALIFIED_COLUMN_DFDWELLINGSTYLE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTRUCTUREAGE,
				COLUMN_DFSTRUCTUREAGE,
				QUALIFIED_COLUMN_DFSTRUCTUREAGE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNOOFUNITS,
				COLUMN_DFNOOFUNITS,
				QUALIFIED_COLUMN_DFNOOFUNITS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNUMBEROFBEDROOMS,
				COLUMN_DFNUMBEROFBEDROOMS,
				QUALIFIED_COLUMN_DFNUMBEROFBEDROOMS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIVINGSPACE,
				COLUMN_DFLIVINGSPACE,
				QUALIFIED_COLUMN_DFLIVINGSPACE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLOTSIZE,
				COLUMN_DFLOTSIZE,
				QUALIFIED_COLUMN_DFLOTSIZE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFHEATTYPE,
				COLUMN_DFHEATTYPE,
				QUALIFIED_COLUMN_DFHEATTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUFFIINSULATION,
				COLUMN_DFUFFIINSULATION,
				QUALIFIED_COLUMN_DFUFFIINSULATION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFWATER,
				COLUMN_DFWATER,
				QUALIFIED_COLUMN_DFWATER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSEWAGE,
				COLUMN_DFSEWAGE,
				QUALIFIED_COLUMN_DFSEWAGE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYUSAGE,
				COLUMN_DFPROPERTYUSAGE,
				QUALIFIED_COLUMN_DFPROPERTYUSAGE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFOCCUPANCY,
				COLUMN_DFOCCUPANCY,
				QUALIFIED_COLUMN_DFOCCUPANCY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYTYPE,
				COLUMN_DFPROPERTYTYPE,
				QUALIFIED_COLUMN_DFPROPERTYTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYLOCATION,
				COLUMN_DFPROPERTYLOCATION,
				QUALIFIED_COLUMN_DFPROPERTYLOCATION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFZONING,
				COLUMN_DFZONING,
				QUALIFIED_COLUMN_DFZONING,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPURCHASEPRICE,
				COLUMN_DFPURCHASEPRICE,
				QUALIFIED_COLUMN_DFPURCHASEPRICE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLANDVALUE,
				COLUMN_DFLANDVALUE,
				QUALIFIED_COLUMN_DFLANDVALUE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEQUITYAVAILABLE,
				COLUMN_DFEQUITYAVAILABLE,
				QUALIFIED_COLUMN_DFEQUITYAVAILABLE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFESTIMATEVALUE,
				COLUMN_DFESTIMATEVALUE,
				QUALIFIED_COLUMN_DFESTIMATEVALUE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFACTUALAPPRAISEDVALUE,
				COLUMN_DFACTUALAPPRAISEDVALUE,
				QUALIFIED_COLUMN_DFACTUALAPPRAISEDVALUE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISALDATE,
				COLUMN_DFAPPRAISALDATE,
				QUALIFIED_COLUMN_DFAPPRAISALDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISALSOURCE,
				COLUMN_DFAPPRAISALSOURCE,
				QUALIFIED_COLUMN_DFAPPRAISALSOURCE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYID,
				COLUMN_DFPROPERTYID,
				QUALIFIED_COLUMN_DFPROPERTYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLOTSIZEUNITOFMEASUREID,
				COLUMN_DFLOTSIZEUNITOFMEASUREID,
				QUALIFIED_COLUMN_DFLOTSIZEUNITOFMEASUREID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIVINGSPACEUNITOFMEASUREID,
				COLUMN_DFLIVINGSPACEUNITOFMEASUREID,
				QUALIFIED_COLUMN_DFLIVINGSPACEUNITOFMEASUREID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNEWCONSTRUCTIONID,
				COLUMN_DFNEWCONSTRUCTIONID,
				QUALIFIED_COLUMN_DFNEWCONSTRUCTIONID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNEWCONSTRUCTIONDESC,
				COLUMN_DFNEWCONSTRUCTIONDESC,
				QUALIFIED_COLUMN_DFNEWCONSTRUCTIONDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBUILDERNAME,
				COLUMN_DFBUILDERNAME,
				QUALIFIED_COLUMN_DFBUILDERNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFGARAGESIZE,
				COLUMN_DFGARAGESIZE,
				QUALIFIED_COLUMN_DFGARAGESIZE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFGARAGETYPE,
				COLUMN_DFGARAGETYPE,
				QUALIFIED_COLUMN_DFGARAGETYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMLSFLAG,
				COLUMN_DFMLSFLAG,
				QUALIFIED_COLUMN_DFMLSFLAG,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    //--Release3.1--begins
    //--by Hiro Apr 10, 2006

    FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
          FIELD_DFTENURE,
          COLUMN_DFTENURETYPEID,
          QUALIFIED_COLUMN_DFTENURETYPEID,
          String.class,
          false,
          false,
          QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
          "",
          QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
          ""));
    
    FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
          FIELD_DFMIENERGYEFFICIENCY,
          COLUMN_DFMIENERGYEFFICIENCY,
          QUALIFIED_COLUMN_DFMIENERGYEFFICIENCY,
          String.class,
          false,
          false,
          QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
          "",
          QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
          ""));
    
    FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
          FIELD_DFSUBDIVISIONDISCOUNT,
          COLUMN_DFSUBDIVISIONDISCOUNT,
          QUALIFIED_COLUMN_DFSUBDIVISIONDISCOUNT,
          String.class,
          false,
          false,
          QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
          "",
          QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
          ""));
    
    FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
          FIELD_DFONRESERVETRUSTAGREEMENT,
          COLUMN_DFONRESERVETRUSTAGREEMENTNUMBER,
          QUALIFIED_COLUMN_DFONRESERVETRUSTAGREEMENTNUMBER,
          String.class,
          false,
          false,
          QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
          "",
          QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
          ""));
    
    //--Release3.1--ends
    FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
              FIELD_DFINSTITUTIONID,
              COLUMN_DFINSTITUTIONID,
              QUALIFIED_COLUMN_DFINSTITUTIONID,
              java.math.BigDecimal.class,
              false,
              false,
              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
              "",
              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
              ""));
		
	  FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFPROPERTYUNITNUMBER,
					COLUMN_DFPROPERTYUNITNUMBER,
					QUALIFIED_COLUMN_DFPROPERTYUNITNUMBER,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
	        
	}

}

