package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doMainViewBorrowerModelImpl extends QueryModelBase
	implements doMainViewBorrowerModel
{
	/**
	 *
	 *
	 */
	public doMainViewBorrowerModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 19Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert Language Desc.
      this.setDfLanguagePreference(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "LANGUAGEPREFERENCE", this.getDfLanguagePreference(), languageId));
      this.setDfSuffix(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(), "SUFFIX", this.getDfSuffix(), languageId));    

    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPrimaryBorrowerFlag()
	{
		return (String)getValue(FIELD_DFPRIMARYBORROWERFLAG);
	}


	/**
	 *
	 *
	 */
	public void setDfPrimaryBorrowerFlag(String value)
	{
		setValue(FIELD_DFPRIMARYBORROWERFLAG,value);
	}


	/**
	 *
	 *
	 */
	public String getDfFirstName()
	{
		return (String)getValue(FIELD_DFFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfFirstName(String value)
	{
		setValue(FIELD_DFFIRSTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLastName()
	{
		return (String)getValue(FIELD_DFLASTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfLastName(String value)
	{
		setValue(FIELD_DFLASTNAME,value);
	}

	/**
	 *
	 *
	 */
	public String getDfSuffix()
	{
		return (String)getValue(FIELD_DFSUFFIX);
	}


	/**
	 *
	 *
	 */
	public void setDfSuffix(String value)
	{
		setValue(FIELD_DFSUFFIX,value);
	}
	
	/**
	 *
	 *
	 */
	public String getDfMiddleInitial()
	{
		return (String)getValue(FIELD_DFMIDDLEINITIAL);
	}


	/**
	 *
	 *
	 */
	public void setDfMiddleInitial(String value)
	{
		setValue(FIELD_DFMIDDLEINITIAL,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLanguagePreference()
	{
		return (String)getValue(FIELD_DFLANGUAGEPREFERENCE);
	}


	/**
	 *
	 *
	 */
	public void setDfLanguagePreference(String value)
	{
		setValue(FIELD_DFLANGUAGEPREFERENCE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfExisitingCleint()
	{
		return (String)getValue(FIELD_DFEXISITINGCLEINT);
	}


	/**
	 *
	 *
	 */
	public void setDfExisitingCleint(String value)
	{
		setValue(FIELD_DFEXISITINGCLEINT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfRefCleintNo()
	{
		return (String)getValue(FIELD_DFREFCLEINTNO);
	}


	/**
	 *
	 *
	 */
	public void setDfRefCleintNo(String value)
	{
		setValue(FIELD_DFREFCLEINTNO,value);
	}


	/**
	 *
	 *
	 */
	public String getDfHomePhoneNumber()
	{
		return (String)getValue(FIELD_DFHOMEPHONENUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfHomePhoneNumber(String value)
	{
		setValue(FIELD_DFHOMEPHONENUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfWorkPhoneNumber()
	{
		return (String)getValue(FIELD_DFWORKPHONENUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfWorkPhoneNumber(String value)
	{
		setValue(FIELD_DFWORKPHONENUMBER,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCreditScore()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCREDITSCORE);
	}


	/**
	 *
	 *
	 */
	public void setDfCreditScore(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCREDITSCORE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfWorkPhoneExt()
	{
		return (String)getValue(FIELD_DFWORKPHONEEXT);
	}


	/**
	 *
	 *
	 */
	public void setDfWorkPhoneExt(String value)
	{
		setValue(FIELD_DFWORKPHONEEXT,value);
	}


	  public java.math.BigDecimal getDfInstitutionId() {
		    return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
		  }

		  public void setDfInstitutionId(java.math.BigDecimal value) {
		    setValue(FIELD_DFINSTITUTIONID, value);
		  }
		  


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 19Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL BORROWER.BORROWERID, BORROWER.DEALID, BORROWER.BORROWERTYPEID, BORROWER.PRIMARYBORROWERFLAG, BORROWER.BORROWERFIRSTNAME, BORROWER.BORROWERLASTNAME, BORROWER.BORROWERMIDDLEINITIAL, LANGUAGEPREFERENCE.LPDESCRIPTION, BORROWER.EXISTINGCLIENT, BORROWER.CLIENTREFERENCENUMBER, BORROWER.BORROWERHOMEPHONENUMBER, BORROWER.BORROWERWORKPHONENUMBER, BORROWER.CREDITSCORE, BORROWER.COPYID, BORROWER.BORROWERWORKPHONEEXTENSION FROM BORROWER, LANGUAGEPREFERENCE  __WHERE__  ORDER BY BORROWER.PRIMARYBORROWERFLAG  DESC";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL BORROWER.BORROWERID, BORROWER.DEALID, BORROWER.BORROWERTYPEID, "+
    "BORROWER.PRIMARYBORROWERFLAG, BORROWER.BORROWERFIRSTNAME, BORROWER.BORROWERLASTNAME, to_char(BORROWER.SUFFIXID) BORROWERSUFFIXID_STR, "+
    "BORROWER.BORROWERMIDDLEINITIAL, to_char(BORROWER.LANGUAGEPREFERENCEID) LANGUAGEPREFERENCEID_STR, "+
    "BORROWER.EXISTINGCLIENT, BORROWER.CLIENTREFERENCENUMBER, BORROWER.BORROWERHOMEPHONENUMBER, "+
    "BORROWER.BORROWERWORKPHONENUMBER, BORROWER.CREDITSCORE, BORROWER.COPYID, "+
    "BORROWER.BORROWERWORKPHONEEXTENSION, "+
    "BORROWER.INSTITUTIONPROFILEID " +
    " FROM BORROWER "+
    "__WHERE__  "+
    " ORDER BY BORROWER.PRIMARYBORROWERFLAG  DESC";
	//--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //public static final String MODIFYING_QUERY_TABLE_NAME="BORROWER, LANGUAGEPREFERENCE";
  public static final String MODIFYING_QUERY_TABLE_NAME="BORROWER";
  //--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
	//public static final String STATIC_WHERE_CRITERIA=" (((BORROWER.LANGUAGEPREFERENCEID  =  LANGUAGEPREFERENCE.LANGUAGEPREFERENCEID)) AND BORROWER.PRIMARYBORROWERFLAG = 'Y') ";
	public static final String STATIC_WHERE_CRITERIA=
    " BORROWER.PRIMARYBORROWERFLAG = 'Y' ";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="BORROWER.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFDEALID="BORROWER.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFBORROWERTYPEID="BORROWER.BORROWERTYPEID";
	public static final String COLUMN_DFBORROWERTYPEID="BORROWERTYPEID";
	public static final String QUALIFIED_COLUMN_DFPRIMARYBORROWERFLAG="BORROWER.PRIMARYBORROWERFLAG";
	public static final String COLUMN_DFPRIMARYBORROWERFLAG="PRIMARYBORROWERFLAG";
	public static final String QUALIFIED_COLUMN_DFFIRSTNAME="BORROWER.BORROWERFIRSTNAME";
	public static final String COLUMN_DFFIRSTNAME="BORROWERFIRSTNAME";
	public static final String QUALIFIED_COLUMN_DFLASTNAME="BORROWER.BORROWERLASTNAME";
	public static final String COLUMN_DFLASTNAME="BORROWERLASTNAME";
    public static final String QUALIFIED_COLUMN_DFSUFFIX="BORROWER.BORROWERSUFFIXID_STR";
    public static final String COLUMN_DFSUFFIX="BORROWERSUFFIXID_STR";
	public static final String QUALIFIED_COLUMN_DFMIDDLEINITIAL="BORROWER.BORROWERMIDDLEINITIAL";
	public static final String COLUMN_DFMIDDLEINITIAL="BORROWERMIDDLEINITIAL";
	public static final String QUALIFIED_COLUMN_DFEXISITINGCLEINT="BORROWER.EXISTINGCLIENT";
	public static final String COLUMN_DFEXISITINGCLEINT="EXISTINGCLIENT";
	public static final String QUALIFIED_COLUMN_DFREFCLEINTNO="BORROWER.CLIENTREFERENCENUMBER";
	public static final String COLUMN_DFREFCLEINTNO="CLIENTREFERENCENUMBER";
	public static final String QUALIFIED_COLUMN_DFHOMEPHONENUMBER="BORROWER.BORROWERHOMEPHONENUMBER";
	public static final String COLUMN_DFHOMEPHONENUMBER="BORROWERHOMEPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFWORKPHONENUMBER="BORROWER.BORROWERWORKPHONENUMBER";
	public static final String COLUMN_DFWORKPHONENUMBER="BORROWERWORKPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFCREDITSCORE="BORROWER.CREDITSCORE";
	public static final String COLUMN_DFCREDITSCORE="CREDITSCORE";
	public static final String QUALIFIED_COLUMN_DFCOPYID="BORROWER.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFWORKPHONEEXT="BORROWER.BORROWERWORKPHONEEXTENSION";
	public static final String COLUMN_DFWORKPHONEEXT="BORROWERWORKPHONEEXTENSION";
      public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
    	    "BORROWER.INSTITUTIONPROFILEID";
      public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
		  

  //--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFLANGUAGEPREFERENCE="LANGUAGEPREFERENCE.LPDESCRIPTION";
	//public static final String COLUMN_DFLANGUAGEPREFERENCE="LPDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFLANGUAGEPREFERENCE="BORROWER.LANGUAGEPREFERENCEID_STR";
	public static final String COLUMN_DFLANGUAGEPREFERENCE="LANGUAGEPREFERENCEID_STR";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERTYPEID,
				COLUMN_DFBORROWERTYPEID,
				QUALIFIED_COLUMN_DFBORROWERTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIMARYBORROWERFLAG,
				COLUMN_DFPRIMARYBORROWERFLAG,
				QUALIFIED_COLUMN_DFPRIMARYBORROWERFLAG,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFIRSTNAME,
				COLUMN_DFFIRSTNAME,
				QUALIFIED_COLUMN_DFFIRSTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLASTNAME,
				COLUMN_DFLASTNAME,
				QUALIFIED_COLUMN_DFLASTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFSUFFIX,
                COLUMN_DFSUFFIX,
                QUALIFIED_COLUMN_DFSUFFIX,
                String.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));
		
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIDDLEINITIAL,
				COLUMN_DFMIDDLEINITIAL,
				QUALIFIED_COLUMN_DFMIDDLEINITIAL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLANGUAGEPREFERENCE,
				COLUMN_DFLANGUAGEPREFERENCE,
				QUALIFIED_COLUMN_DFLANGUAGEPREFERENCE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEXISITINGCLEINT,
				COLUMN_DFEXISITINGCLEINT,
				QUALIFIED_COLUMN_DFEXISITINGCLEINT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFCLEINTNO,
				COLUMN_DFREFCLEINTNO,
				QUALIFIED_COLUMN_DFREFCLEINTNO,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFHOMEPHONENUMBER,
				COLUMN_DFHOMEPHONENUMBER,
				QUALIFIED_COLUMN_DFHOMEPHONENUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFWORKPHONENUMBER,
				COLUMN_DFWORKPHONENUMBER,
				QUALIFIED_COLUMN_DFWORKPHONENUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCREDITSCORE,
				COLUMN_DFCREDITSCORE,
				QUALIFIED_COLUMN_DFCREDITSCORE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFWORKPHONEEXT,
				COLUMN_DFWORKPHONEEXT,
				QUALIFIED_COLUMN_DFWORKPHONEEXT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	    FIELD_SCHEMA.addFieldDescriptor(
	            new QueryFieldDescriptor(
	              FIELD_DFINSTITUTIONID,
	              COLUMN_DFINSTITUTIONID,
	              QUALIFIED_COLUMN_DFINSTITUTIONID,
	              java.math.BigDecimal.class,
	              false,
	              false,
	              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	              "",
	              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	              ""));
	        

	}

}

