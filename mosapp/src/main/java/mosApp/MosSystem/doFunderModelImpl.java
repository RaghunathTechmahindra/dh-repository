package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;


/**
 *
 *
 *
 */
public class doFunderModelImpl extends QueryModelBase
	implements doFunderModel
{
	/**
	 *
	 *
	 */
	public doFunderModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfUserProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFUSERPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfUserProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFUSERPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfUsername()
	{
		return (String)getValue(FIELD_DFUSERNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfUsername(String value)
	{
		setValue(FIELD_DFUSERNAME,value);
	}

	/**
	 * MetaData object test method to verify the SYNTHETIC new
	 * field for the reogranized SQL_TEMPLATE query.
	 *
	 */
  /**
	public void setResultSet(ResultSet value)
	{
  		logger = SysLog.getSysLogger("METADATA");

  		try
  		{
        super.setResultSet(value);
   			if( value != null)
   			{
       			ResultSetMetaData metaData = value.getMetaData();
       			int columnCount = metaData.getColumnCount();
       			logger.debug("===============================================");
            logger.debug("Testing MetaData Object for new synthetic fields for" +
                          " doFunderModel");
       			logger.debug("NumberColumns: " + columnCount);
         		for(int i = 0; i < columnCount ; i++)
          		{
                 	logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
          		}
          		logger.debug("================================================");
      		}
  		}
  		catch (SQLException e)
  		{
    		    logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
  		}
 	}
  **/


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.

  //--Release2.1--//
  //// 1. Replace all Descriptions from the tables included into the PickList with
  //// their joins' counterparts in the SQL_TEMPLATE.
  //// 2. Convert all these new fragments (IDs) into the string format.
  //// 3. Remove all joins which include the tables from the PickList.
  //// 4. Remove all PickList tables from the SQL_TEMPLATE.
  //// 5. Repopulate the PickList Description fields manually in the afterExecute method (see above).

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //// 1. Replace all Descriptions from the tables included into the PickList with
  //// their joins' counterparts in the SQL_TEMPLATE.
  //// 2. Convert all these new fragments (IDs) into the string format.
	////public static final String SELECT_SQL_TEMPLATE="SELECT ALL USERPROFILE.USERPROFILEID, CONTACT.CONTACTLASTNAME || ', ' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1) USERNAME FROM CONTACT, USERPROFILE, USERTYPE  __WHERE__  ";
	////public static final String SELECT_SQL_TEMPLATE="SELECT ALL USERPROFILE.USERPROFILEID, " +
  ////"CONTACT.CONTACTLASTNAME || ', ' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1) SYNTHETICUSERNAME " +
  ////"FROM CONTACT, USERPROFILE, USERTYPE  __WHERE__  ";

	public static final String SELECT_SQL_TEMPLATE="SELECT ALL USERPROFILE.USERPROFILEID, " +
  "CONTACT.CONTACTLASTNAME || ', ' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1) SYNTHETICUSERNAME " +
  "FROM CONTACT, USERPROFILE  __WHERE__  ";

	////public static final String MODIFYING_QUERY_TABLE_NAME="CONTACT, USERPROFILE, USERTYPE";
	public static final String MODIFYING_QUERY_TABLE_NAME="CONTACT, USERPROFILE";

  //--Release2.1--//
  //// 3. Remove all joins which include the tables from the PickList.
	////public static final String STATIC_WHERE_CRITERIA=" (((CONTACT.CONTACTID  =  USERPROFILE.CONTACTID) AND (USERPROFILE.USERTYPEID  =  USERTYPE.USERTYPEID)) AND CONTACT.COPYID = 1)";
	public static final String STATIC_WHERE_CRITERIA=" (((CONTACT.CONTACTID  =  USERPROFILE.CONTACTID) " +
            "AND (CONTACT.INSTITUTIONPROFILEID  =  USERPROFILE.INSTITUTIONPROFILEID)) " +
            "AND CONTACT.COPYID = 1)";

	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFUSERPROFILEID="USERPROFILE.USERPROFILEID";
	public static final String COLUMN_DFUSERPROFILEID="USERPROFILEID";
	////public static final String QUALIFIED_COLUMN_DFUSERNAME=".";
	////public static final String COLUMN_DFUSERNAME="";
	public static final String QUALIFIED_COLUMN_DFUSERNAME="CONTACT.SYNTHETICUSERNAME";
	public static final String COLUMN_DFUSERNAME="SYNTHETICUSERNAME";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERPROFILEID,
				COLUMN_DFUSERPROFILEID,
				QUALIFIED_COLUMN_DFUSERPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		////FIELD_SCHEMA.addFieldDescriptor(
		////	new QueryFieldDescriptor(
		////		FIELD_DFUSERNAME,
		////		COLUMN_DFUSERNAME,
		////		QUALIFIED_COLUMN_DFUSERNAME,
		////		String.class,
		////		false,
		////		false,
		////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		////		"",
		////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		////		""));

		//// Adjustemnt for the FieldDescriptor improperly converted by the iMT tool
    //// for the ComputedColumn.
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERNAME,
				COLUMN_DFUSERNAME,
				QUALIFIED_COLUMN_DFUSERNAME,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"CONTACT.CONTACTLASTNAME || ', ' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1)",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"CONTACT.CONTACTLASTNAME || ', ' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1)"));

	}

}

