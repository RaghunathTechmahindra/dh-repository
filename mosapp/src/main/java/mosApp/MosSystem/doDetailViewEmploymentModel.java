package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doDetailViewEmploymentModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmployerName();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmployerName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmploymentStatus();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmploymentStatus(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmploymentType();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmploymentType(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmployerPhoneNo();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmployerPhoneNo(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmployerFaxNo();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmployerFaxNo(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmployerEmailAddress();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmployerEmailAddress(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmployerAddressLine1();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmployerAddressLine1(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmployerAddressLine2();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmployerAddressLine2(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmployerCity();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmployerCity(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmployerProvince();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmployerProvince(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmployerPostalFSA();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmployerPostalFSA(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmployerPostalLDU();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmployerPostalLDU(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfIndustrySector();

	
	/**
	 * 
	 * 
	 */
	public void setDfIndustrySector(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfOccupation();

	
	/**
	 * 
	 * 
	 */
	public void setDfOccupation(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfJobTittle();

	
	/**
	 * 
	 * 
	 */
	public void setDfJobTittle(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTimeAtJob();

	
	/**
	 * 
	 * 
	 */
	public void setDfTimeAtJob(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfEmpHistIncomeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmpHistIncomeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfJobTitleId();

	
	/**
	 * 
	 * 
	 */
	public void setDfJobTitleId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmpWorkPhoneExt();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmpWorkPhoneExt(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfJobTitle1();

	
	/**
	 * 
	 * 
	 */
	public void setDfJobTitle1(String value);
	
    public java.math.BigDecimal getDfInstitutionId();
    public void setDfInstitutionId(java.math.BigDecimal id);  	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFEMPLOYERNAME="dfEmployerName";
	public static final String FIELD_DFEMPLOYMENTSTATUS="dfEmploymentStatus";
	public static final String FIELD_DFEMPLOYMENTTYPE="dfEmploymentType";
	public static final String FIELD_DFEMPLOYERPHONENO="dfEmployerPhoneNo";
	public static final String FIELD_DFEMPLOYERFAXNO="dfEmployerFaxNo";
	public static final String FIELD_DFEMPLOYEREMAILADDRESS="dfEmployerEmailAddress";
	public static final String FIELD_DFEMPLOYERADDRESSLINE1="dfEmployerAddressLine1";
	public static final String FIELD_DFEMPLOYERADDRESSLINE2="dfEmployerAddressLine2";
	public static final String FIELD_DFEMPLOYERCITY="dfEmployerCity";
	public static final String FIELD_DFEMPLOYERPROVINCE="dfEmployerProvince";
	public static final String FIELD_DFEMPLOYERPOSTALFSA="dfEmployerPostalFSA";
	public static final String FIELD_DFEMPLOYERPOSTALLDU="dfEmployerPostalLDU";
	public static final String FIELD_DFINDUSTRYSECTOR="dfIndustrySector";
	public static final String FIELD_DFOCCUPATION="dfOccupation";
	public static final String FIELD_DFJOBTITTLE="dfJobTittle";
	public static final String FIELD_DFTIMEATJOB="dfTimeAtJob";
	public static final String FIELD_DFEMPHISTINCOMEID="dfEmpHistIncomeId";
	public static final String FIELD_DFJOBTITLEID="dfJobTitleId";
	public static final String FIELD_DFEMPWORKPHONEEXT="dfEmpWorkPhoneExt";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFJOBTITLE1="dfJobTitle1";
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

