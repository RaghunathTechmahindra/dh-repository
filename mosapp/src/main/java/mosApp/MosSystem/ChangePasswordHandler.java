package mosApp.MosSystem;

import java.text.*;
import java.util.*;

import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.jdbcservices.jdbcexecutor.*;
import com.basis100.deal.security.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.picklist.BXResources;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mosApp.*;
import MosSystem.*;

import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.html.*;

//// Original (iMT) inheritance.
//// public class ChangePasswordHandler extends PageHandlerCommon
////	implements Sc, Cloneable

//// Highly discussable implementation with the XXXXHandler extension of the
//// ViewBeanBase JATO class. The sequence diagrams must be verified. On the other hand,
//// This implementation allows to use the basic JATO framework elements such as
//// RequestContext() class. Theoretically, it could allow to override the JATO/iMT
//// auto-migration. Temporarily these fragments are commented out now for the compability
//// with the stand alone MWHC<--PHC<--XXXHandlerName Utility hierarchy.
//// getModel(ClassName.class), etc., could be also rewritten in the Utility hierarchy
//// using this inheritance.

public class ChangePasswordHandler extends PageHandlerCommon
	implements Sc, Cloneable, View, ViewBean
{
	public ChangePasswordHandler()
	{
		super();
	}

	public ChangePasswordHandler(String name)
	{
		this(null,name); // No parent
	}

	protected ChangePasswordHandler(View parent, String name)
	{
		super(parent,name);
	}

	/**
	 *
	 *
	 */
	public ChangePasswordHandler cloneSS()
	{
		return(ChangePasswordHandler) super.cloneSafeShallow();

	}

	/**
	 *
	 *
	 */
	public void populatePageDisplayFields()
	{
    getCurrNDPage().setDisplayFieldValue("stChangeMessage", new String(getTheSessionState().getCurrentPage().getPageState1()));
		showDialogMessage();
	}

	/**
	 *  Important method to allow reload the page. Should be reconsidered after
	 *  the BasisXpress page navigation design.
	 *
	 */

	////public int allowRetry()
  //--> Changed to return boolean ==> true (allowed) :: false (disallowed)
  //--> By BILLY 10July2002
	public boolean allowRetry()
	{
		if (getTheSessionState().getIsFatal() == true) return false;
		return true;

  /* The following is commented out by BILLY 10July2002
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    logger.debug("@CPH.allowRetry: IN :: page = <" + pg.getPageName() + ">");

    pgChangePasswordViewBean pswdPage = null;

    try
    {
       pswdPage = (pgChangePasswordViewBean) RequestManager.getRequestContext().
       getViewBeanManager().getLocalViewBean("pgChangePasswordViewBean");

       logger.debug("CPH@handleBtOKonWebEvent::currNDPage: " + pswdPage.getName());
    }
    catch(ClassNotFoundException cnfe)
    {
          ////treat it.
    }

		if (theSessionState.getIsFatal() == true)
    {
		      pswdPage.forwardTo(getRequestContext());
					     	throw new CompleteRequestException();
    }
    else
    {
		      pswdPage.forwardTo(getRequestContext());
    }
    */
	}

	/**
	 *
	 *
	 */
	////public int allowCancel()
  //--> Changed to return boolean ==> true (allowed) :: false (disallowed)
  //--> By BILLY 10July2002
	public boolean allowCancel()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();


		// attempts exceeded
		if (getTheSessionState().getIsFatal() == true) return false;

		// check user forced to change
		if (pg.getPageCondition1() == true) return false;

		return true;

    /* The following is commented out by BILLY 10July2002
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    logger.debug("@CPH.allowCancel: IN :: page = <" + pg.getPageName() + ">");
    logger.debug("@CPH.allowCancel::PageCondition1: " + pg.getPageCondition1());

    pgChangePasswordViewBean pswdPage = null;

    try
    {
      pswdPage = (pgChangePasswordViewBean) RequestManager.getRequestContext().
                                                        getViewBeanManager().getLocalViewBean("pgChangePasswordViewBean");

		  logger.debug("CPH@handleBtOKonWebEvent::currNDPage: " + pswdPage.getName());
    }
    catch(ClassNotFoundException cnfe)
    {
        ////treat it.
    }

		if ((theSessionState.getIsFatal() == true) || (pg.getPageCondition1() == false))
    {
		      pswdPage.forwardTo(getRequestContext());
					     	throw new CompleteRequestException();
    }
    else
    {
		      pswdPage.forwardTo(getRequestContext());
    }
    */
	}

	/**
	 *
	 *
	 */
  //--> Modified by BILLY to sync with the new Password handling
  //--> 10July2002
	public void handleBtOKonWebEvent()
	{
		SessionResourceKit srk = getSessionResourceKit();

		ViewBean currNDPage = getCurrNDPage();

		int userprofileid = getTheSessionState().getSessionUserId();

		UserProfile up = null;

		try
		{
			up = new UserProfile(srk);
			up.findByPrimaryKey(new UserProfileBeanPK(userprofileid, 
                                                      theSessionState.getUserInstitutionId()));
		}
		catch(Exception e)
		{
			logger.error("Exception @ChangePasswordHandler.updateUserSecurityPolicy:" + e.getMessage());
			setMessage(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()));
			return;
		}


		//reset the display of the cancel button.
		String currpassword = currNDPage.getDisplayFieldValue("tbCurrent").toString().trim();

		String newpassword = currNDPage.getDisplayFieldValue("tbNew").toString().trim();

		String verifypassword = currNDPage.getDisplayFieldValue("tbVerify").toString().trim();

    //--Merge--EncriptedPassword--//
    //Handle Password encription -- By Billy 28Oct2002
    String encrypted_currpassword = currpassword;
		String encrypted_newpassword = newpassword;
		String encrypted_verifypassword = verifypassword;
    if(PropertiesCache.getInstance().getInstanceProperty("com.basis100.useradmin.passwordencrypted", "Y").equals("Y"))
//    	if(PropertiesCache.getInstance().getProperty(up.getInstitutionId(), "com.basis100.useradmin.passwordencrypted", "Y").equals("Y"))
    {
      encrypted_currpassword = PasswordEncoder.getEncodedPassword(currpassword);
      encrypted_newpassword = PasswordEncoder.getEncodedPassword(newpassword);
      encrypted_verifypassword = PasswordEncoder.getEncodedPassword(verifypassword);
    }
    //==================================================

		//1. Check if New Password , Verify password, Current password are empty
		if (currpassword.equals("") || newpassword.equals("") || verifypassword.equals(""))
		{
			setMessage(BXResources.getSysMsg("CHG_PSWD_ALL_FIELDS_MUST_BE_ENTERED", theSessionState.getLanguageId()));
			return;
		}


		//1. Check if New Password = Verify password
		if (! newpassword.equals(verifypassword))
		{
			setMessage(BXResources.getSysMsg("CHG_PSWD_NEW_NOT_MATCH_VERIFICATION", theSessionState.getLanguageId()));
			return;
		}


		//2. Check if New Password = Old password
		if (currpassword.equals(newpassword))
		{
			setMessage(BXResources.getSysMsg("CHG_PSWD_NEW_PSWD_MUST_BE_DIF_OLD_PSWD", theSessionState.getLanguageId()));
			return;
		}

    //--Merge--EncriptedPassword--//
    //Handle Password encription -- By Billy 28Oct2002
    if(!up.getPassword().equals(encrypted_currpassword))
    //================================================

		//3. Check if Current Password entered is correct.
		if (! up.getPassword().equals(currpassword))
		{
			// determine if number of tries exceeded (if so lock out user)
			int tries = 1 + getTheSessionState().getAuthenticationAttempts();
			getTheSessionState().setAuthenticationAttempts(tries);
			int allowed = getTheSessionState().getAllowedAuthenticationAttempts();
			if (tries >= allowed && allowed > 0) getTheSessionState().setIsFatal(true);
			setMessage(BXResources.getSysMsg("CHG_PSWD_INCORRECT_PASSWORD", theSessionState.getLanguageId()));
			return;
		}


		//4. Validate New Password format -- based on the Password Strength defined
    //--Release2.1--//
    //--> Added LanguageId to pass for Password Validation
    //--> BY Billy 10Dec2002
		String msg = up.validateNewPassword(newpassword, theSessionState.getLanguageId());
    //====================================================

		if (msg != null && ! msg.equals(""))
		{
			// Validate failed
			setMessage(BXResources.getSysMsg("INVALID_NEW_PASSWORD", theSessionState.getLanguageId()) + msg);
			return;
		}


		// Update password
		try
		{
			srk.beginTransaction();

			// Turn PasswordReset Flag Off
      //--Merge--EncriptedPassword--//
      //Handle Password encription -- By Billy 28Oct2002
			up.makePasswordChange(encrypted_newpassword, up.isNewPassword(), up.isPasswordLocked(), false);
      //================================================
			srk.commitTransaction();
		}
		catch(Exception ex)
		{
			srk.cleanTransaction();
			setActiveMessageToAlert(BXResources.getSysMsg("CHG_PSWD_MSG_TITLE", theSessionState.getLanguageId()),
          BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM, "A");
			logger.error("@ChangePasswordHandler.handleBtOKonWebEvent Problem executing update password");
			logger.error(ex);
			return;
		}

		setActiveMessageToAlert(BXResources.getSysMsg("CHG_PSWD_PASSWORD_SUCCESSFULLY_CHANGED", theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);


		// navigate ...
		handleCancel();

		return;

	}


	/**
	 *
	 *
	 */
	public void handleCancel()
	{
    //logger.debug("CPH@handleCancel::I am handleCancel");

		// to 'back' page if set, else standard navigation
		////PageEntry backPage = getSavedPages().getBackPage();

    PageEntry backPage = savedPages.getBackPage();
    //logger.debug("CPH@handleCancel::BackPageName: " + backPage.getPageName());
		//logger.debug("CPH@handleCancel::BackPageId: " + backPage.getPageId());

		if (backPage == null || backPage.getPageId() == 0) backPage = null;

		////getSavedPages().setNextPage(backPage);
    savedPages.setNextPage(backPage);


		//logger.debug("CPH@handleCancel::I am before navigateToNextPage");
    navigateToNextPage(true);
		//logger.debug("CPH@handleCancel::I am after navigateToNextPage");

		////return;

  }


	/**
	 *
	 *
	 */
	private void setMessage(String message)
	{
    ////if (getTheSession().getIsFatal() == true)
    if (theSessionState.getIsFatal() == true)
		{
			setActiveMessageToAlert(
          BXResources.getSysMsg("CHG_PSWD_MSG_TITLE", theSessionState.getLanguageId()),
          BXResources.getSysMsg("CHG_PSWD_ATTEMPTS_EXCEEDED", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMFATAL, "A");
			return;
		}

		setActiveMessageToAlert(BXResources.getSysMsg("CHG_PSWD_MSG_TITLE", theSessionState.getLanguageId()), message,
        ActiveMsgFactory.ISCUSTOMCONFIRM, "A");

		return;

	}

  //--> Added this to handle invalid Password input ==> Force Logoff
  /**
	 *
	 *
	 */
	public void handleCustomActMessageOk(String[] args)
	{
		if (args [ 0 ].equals("A"))
		{
			try
			{
//logger.debug("BILLY==> @handleCustomActMessageOk('A') Just entered !!");
        // User failed to enter Old Password ==> Force to Logoff
        handleSignOff(false, false);
//logger.debug("BILLY==> @handleCustomActMessageOk(CONFIRM_NEW_SESSION) After finishLogin(true, up) !!");
			}
			catch(Exception e)
			{
				// ... re-load sign-on page --- bad!!!!
				logger.error("Unexpected exception at ChangePassword");
				logger.error(e);
				setMessage(BXResources.getSysMsg("TECH_FAILURE_LOGIN", theSessionState.getLanguageId()));
			}
			return;
		}

	}

}

