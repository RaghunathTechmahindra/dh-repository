package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doPirorityCountModelImpl extends QueryModelBase
	implements doPirorityCountModel
{
	/**
	 *
	 *
	 */
	public doPirorityCountModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

          return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 05Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    int institutionId = theSessionState.getDealInstitutionId();
    if (institutionId == 0)
    {
        institutionId = theSessionState.getUserInstitutionId();
    }
    while(this.next())
    {
      this.setDfPriorityDesc(BXResources.getPickListDescription(institutionId,
        "PRIORITY", this.getMOS_PRIORITY_PRIORITYID().intValue(), languageId));
    }

   //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public String getDfPriorityDesc()
	{
		return (String)getValue(FIELD_DFPRIORITYDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfPriorityDesc(String value)
	{
		setValue(FIELD_DFPRIORITYDESC,value);
	}


	/**
	 *
	 *
	 */
	public Integer getDfPriorityCount()
	{
		return (Integer)getValue(FIELD_DFPRIORITYCOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfPriorityCount(Integer value)
	{
		setValue(FIELD_DFPRIORITYCOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getMOS_PRIORITY_PRIORITYID()
	{
		return (java.math.BigDecimal)getValue(FIELD_MOS_PRIORITY_PRIORITYID);
	}


	/**
	 *
	 *
	 */
	public void setMOS_PRIORITY_PRIORITYID(java.math.BigDecimal value)
	{
		setValue(FIELD_MOS_PRIORITY_PRIORITYID,value);
	}


	/**
	 * MetaData object test method to verify the SYNTHETIC new
	 * field for the reogranized SQL_TEMPLATE query.
	 *
	 */
  /**
	public void setResultSet(ResultSet value)
	{
  		logger = SysLog.getSysLogger("METADATA");

  		try
  		{
        super.setResultSet(value);
   			if( value != null)
   			{
       			ResultSetMetaData metaData = value.getMetaData();
       			int columnCount = metaData.getColumnCount();
       			logger.debug("===============================================");
            logger.debug("Testing MetaData Object for new synthetic fields for" +
                          " doPirorityCountModel");
       			logger.debug("NumberColumns: " + columnCount);
         		for(int i = 0; i < columnCount ; i++)
          		{
                 	logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
          		}
          		logger.debug("================================================");
      		}
  		}
  		catch (SQLException e)
  		{
    		    logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
  		}
 	}
  **/

	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////
       //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

       //// Original improper converted fragment by iMT
       /***
       public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL MIN(PRIORITY.PDESCRIPTION), COUNT(ASSIGNEDTASKSWORKQUEUE.PRIORITYID), PRIORITY.PRIORITYID FROM ASSIGNEDTASKSWORKQUEUE, PRIORITY, DEAL  __WHERE__  GROUP BY PRIORITY.PRIORITYID ORDER BY PRIORITY.PRIORITYID  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME="ASSIGNEDTASKSWORKQUEUE, PRIORITY, DEAL";
	public static final String STATIC_WHERE_CRITERIA=" ((ASSIGNEDTASKSWORKQUEUE.DEALID  =  DEAL.DEALID AND PRIORITY.PRIORITYID  <> 0 AND  PRIORITY.PRIORITYID  =  ASSIGNEDTASKSWORKQUEUE.PRIORITYID(+)  AND ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID(+) = 1 ) AND DEAL.SCENARIORECOMMENDED = 'Y' AND DEAL.COPYTYPE <> 'T') ";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFPRIORITYDESC=".";
	public static final String COLUMN_DFPRIORITYDESC="";
	public static final String QUALIFIED_COLUMN_DFPRIORITYCOUNT=".";
	public static final String COLUMN_DFPRIORITYCOUNT="";
	public static final String QUALIFIED_COLUMN_MOS_PRIORITY_PRIORITYID="PRIORITY.PRIORITYID";
	public static final String COLUMN_MOS_PRIORITY_PRIORITYID="PRIORITYID";
	***/


	//// workaround to solve the bug for the do with a ComputedColumn (see detailed description
       //// in the doc as well.
       //// In case of ComputedColumn presence a "fake", synthetic definition of this
       //// field must be included into the TEMPLATE.

       //--Release2.1--//
       //Not necessary to modify the SQL because the PriorityId is already included.
       //But we still need to populate the Priority.Description with ResourceBundle in afterExecute handler.
       //For More details please refer to afterExecute method.
       //--> NOTE : Billy 05Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";

       //----SCR#1147--start--23Mar2005--//
       // Vlad: Counter model should be enhanced with the filter criteria as well.
       // For this the STATIC_WHERE_DYNAMIC should become virtuall dynamic from the
       // IWH setup.
       public static final String SELECT_SQL_TEMPLATE="SELECT ALL MIN(PRIORITY.PDESCRIPTION) MIN_PDESCRIPTION, " +
       "COUNT(ASSIGNEDTASKSWORKQUEUE.PRIORITYID) COUNT_PRIORITYID, PRIORITY.PRIORITYID " +

       "FROM ASSIGNEDTASKSWORKQUEUE, PRIORITY, DEAL  " +
       "__WHERE__  GROUP BY PRIORITY.PRIORITYID ORDER BY PRIORITY.PRIORITYID  ASC";

        public static final String MODIFYING_QUERY_TABLE_NAME="ASSIGNEDTASKSWORKQUEUE, PRIORITY, DEAL";
        public static final String STATIC_WHERE_CRITERIA=" (ASSIGNEDTASKSWORKQUEUE.DEALID  =  DEAL.DEALID " +
        "AND DEAL.INSTITUTIONPROFILEID = ASSIGNEDTASKSWORKQUEUE.INSTITUTIONPROFILEID " +
        "AND PRIORITY.PRIORITYID  <> 0 " +
        "AND  PRIORITY.PRIORITYID  =  ASSIGNEDTASKSWORKQUEUE.PRIORITYID(+)  " +
        //----SCR#1147--start--23Mar2005--//
        //"AND ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID(+) = 1  " +
        //----SCR#1147--end--23Mar2005--//
        "AND DEAL.SCENARIORECOMMENDED = 'Y' " +
        "AND DEAL.COPYTYPE <> 'T') ";

        public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
        public static final String COLUMN_DFPRIORITYDESC="MIN_PDESCRIPTION";
        public static final String QUALIFIED_COLUMN_DFPRIORITYDESC="PRIORITY.MIN_PDESCRIPTION";
        public static final String COLUMN_DFPRIORITYCOUNT="COUNT_PRIORITYID";
        public static final String QUALIFIED_COLUMN_DFPRIORITYCOUNT="PRIORITY.COUNT_PRIORITYID";
        public static final String COLUMN_MOS_PRIORITY_PRIORITYID="PRIORITYID";
        public static final String QUALIFIED_COLUMN_MOS_PRIORITY_PRIORITYID="PRIORITY.PRIORITYID";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////
	//// Original improper conversion
	/**
	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIORITYDESC,
				COLUMN_DFPRIORITYDESC,
				QUALIFIED_COLUMN_DFPRIORITYDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIORITYCOUNT,
				COLUMN_DFPRIORITYCOUNT,
				QUALIFIED_COLUMN_DFPRIORITYCOUNT,
				Integer.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_MOS_PRIORITY_PRIORITYID,
				COLUMN_MOS_PRIORITY_PRIORITYID,
				QUALIFIED_COLUMN_MOS_PRIORITY_PRIORITYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}
        **/

        //// Adjusted field descriptors that include the computed columns definitions.
	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIORITYDESC,
				COLUMN_DFPRIORITYDESC,
				QUALIFIED_COLUMN_DFPRIORITYDESC,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"MIN(PRIORITY.PDESCRIPTION)",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"MIN(PRIORITY.PDESCRIPTION)"));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIORITYCOUNT,
				COLUMN_DFPRIORITYCOUNT,
				QUALIFIED_COLUMN_DFPRIORITYCOUNT,
				Integer.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"COUNT(ASSIGNEDTASKSWORKQUEUE.PRIORITYID)",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"COUNT(ASSIGNEDTASKSWORKQUEUE.PRIORITYID)"));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_MOS_PRIORITY_PRIORITYID,
				COLUMN_MOS_PRIORITY_PRIORITYID,
				QUALIFIED_COLUMN_MOS_PRIORITY_PRIORITYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}
/**
 public QueryFieldDescriptor(
 		String logicalName,
 		String columnName,
 		String qualifiedColumnName,
 		Class  fieldClass,
 		boolean isKey,
 		boolean isComputedField,
 		int insertValueSource,
 		String insertFormula,
 		int onEmptyValuePolicy,
 		String emptyFormula)
 	{
 		super();
 		this.logicalName = logicalName;
 		this.columnName = columnName;
 		this.qualifiedColumnName = qualifiedColumnName;
 		this.fieldClass = fieldClass;
 		this.isKey = isKey;
 		this.isComputedField = isComputedField;
 		this.insertValueSource = insertValueSource;
 		this.onEmptyValuePolicy = onEmptyValuePolicy;
 		this.emptyFormula = emptyFormula;
 		this.insertFormula = insertFormula;
	}
**/

}

