package mosApp.MosSystem;

import com.basis100.picklist.BXResources;

import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import java.math.BigDecimal;
import java.sql.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 *
 *
 */
public class doMasterWorkQueueModelImpl
	extends QueryModelBase
	implements doMasterWorkQueueModel {
  
  private static final Log _log = LogFactory.getLog(doMasterWorkQueueModelImpl.class);
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
	//// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
	//// (see detailed description in the desing doc as well).
	//// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
	//// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
	//// accordingly.
	//--Release2.1--//
	//// 1. Replace all Descriptions from the tables included into the PickList with
	//// their joins' counterparts in the SQL_TEMPLATE.
	//// 2. Convert all these new fragments (IDs) into the string format.
	//// 3. Remove all joins which include the tables from the PickList.
	//// 4. Remove all PickList tables from the SQL_TEMPLATE.
	//// 5. Since this is computed field redefine the fields follow the #1,2 in the
	//// FIELD_SCHEMA.addFieldDescriptor() methods.
	//// 6. Repopulate the PickList Description fields manually in the afterExecute method (see above).
	//// 7. Create new FIELDS to pass the UserTypeDescriptionId and translate it via
	//// the BXResourceBundle.
	public static final String DATA_SOURCE_NAME = "jdbc/orcl";

	//--Release2.1--//
	//// 1. Replace all Descriptions from the tables included into the PickList with
	//// their joins' counterparts in the SQL_TEMPLATE.
	//// 2. Convert all these new fragments (IDs) into the string format.


	public static final String SELECT_SQL_TEMPLATE;
	static{
	    StringBuffer sql = new StringBuffer();
	    sql.append(" SELECT ASSIGNEDTASKSWORKQUEUE.ASSIGNEDTASKSWORKQUEUEID,");
	    sql.append("     ASSIGNEDTASKSWORKQUEUE.DEALID                     ,");
	    sql.append("     ASSIGNEDTASKSWORKQUEUE.APPLICATIONID              ,");
	    sql.append("     ASSIGNEDTASKSWORKQUEUE.ENDTIMESTAMP               ,");
	    sql.append("     ASSIGNEDTASKSWORKQUEUE.USERPROFILEID              ,");
	    sql.append("     C1.CONTACTLASTNAME || ', ' || SUBSTR(C1.CONTACTFIRSTNAME,1,1) SYNTHETICASSIGNEDUSERNAME,");
	    sql.append("     BORROWER.BORROWERLASTNAME || ', ' || SUBSTR(BORROWER.BORROWERFIRSTNAME,1,1) SYNTHETICBORRNAME,");
	    sql.append("     USERPROFILE.USERLOGIN                                              ,");
	    sql.append("     GROUPPROFILE.GROUPPROFILEID                                        ,");
	    sql.append("     GROUPPROFILE.BRANCHPROFILEID                                       ,");
	    sql.append("     BRANCHPROFILE.REGIONPROFILEID                                      ,");
	    sql.append("     DEAL.COPYID                                                        ,");
	    sql.append("     DEAL.INSTITUTIONPROFILEID                                          ,");
	    sql.append("     ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID                                ,");
	    sql.append("     TO_CHAR(USERPROFILE.USERTYPEID) USERTYPEID_STR                     ,");
	    sql.append("     USERPROFILE.USERTYPEID                                             ,");
	    sql.append("     TO_CHAR(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID) TASKSTATUSID_STR      ,");
	    sql.append("     ASSIGNEDTASKSWORKQUEUE.TASKID                                      ,");
	    sql.append("     TO_CHAR(ASSIGNEDTASKSWORKQUEUE.TIMEMILESTONEID) TIMEMILESTONEID_STR,");
	    sql.append("     WORKFLOWTASK.TASKNAME                                              ,");
	    sql.append("     TO_CHAR(ASSIGNEDTASKSWORKQUEUE.PRIORITYID) PRIORITYID_STR          ,");
	    sql.append("     TO_CHAR(DEAL.HOLDREASONID) HOLDREASONID_STR                        ,");
	    sql.append("     TO_CHAR(DEAL.INSTITUTIONPROFILEID) INSTITUTIONPROFILEID_STR        ,");
	    sql.append("     DEAL.HOLDREASONID                                                  ,");
	    sql.append("     C2.CONTACTLASTNAME                                                 ,");
	    sql.append("     SOURCEFIRMPROFILE.SFSHORTNAME                                      ,");
	    sql.append("     TO_CHAR(DEAL.STATUSID) DEALSTATUSID_STR");
	    sql.append(" FROM ASSIGNEDTASKSWORKQUEUE ,");
	    sql.append("     CONTACT C1              ,");
	    sql.append("     CONTACT C2              ,");
	    sql.append("     USERPROFILE             ,");
	    sql.append("     BORROWER                ,");
	    sql.append("     BRANCHPROFILE           ,");
	    sql.append("     GROUPPROFILE            ,");
	    sql.append("     DEAL                    ,");
	    sql.append("     WORKFLOWTASK            ,");
	    sql.append("     SOURCEOFBUSINESSPROFILE ,");
	    sql.append("     SOURCEFIRMPROFILE");
	    sql.append(" __WHERE__ ");

	    SELECT_SQL_TEMPLATE = sql.toString();
	}

	public static final String MODIFYING_QUERY_TABLE_NAME =
		"ASSIGNEDTASKSWORKQUEUE, CONTACT C1, CONTACT C2, USERPROFILE, "
			+ "BORROWER, BRANCHPROFILE, GROUPPROFILE, DEAL, WORKFLOWTASK, "
			+ "SOURCEOFBUSINESSPROFILE, SOURCEFIRMPROFILE ";


	public static final String STATIC_WHERE_CRITERIA;
	/**
  	 * BEGIN FIX FOR Performance Issue on Master Work queue [FXP21615]
     */
	static {
	    StringBuffer sql = new StringBuffer();	    
	    sql.append("deal.institutionprofileid = assignedtasksworkqueue.institutionprofileid ");
	    sql.append(" AND deal.dealid = assignedtasksworkqueue.dealid "); 
	    sql.append("     AND deal.institutionprofileid = sourcefirmprofile.institutionprofileid "); 
	    sql.append("     AND deal.sourcefirmprofileid = sourcefirmprofile.sourcefirmprofileid      "); 
	    sql.append("     AND deal.institutionprofileid = borrower.institutionprofileid "); 
	    sql.append("     AND deal.dealid = borrower.dealid "); 
	    sql.append("     AND deal.copyid = borrower.copyid           "); 
	    sql.append("     AND assignedtasksworkqueue.institutionprofileid = workflowtask.institutionprofileid "); 
	    sql.append("     AND assignedtasksworkqueue.workflowid = workflowtask.workflowid "); 
	    sql.append("     AND assignedtasksworkqueue.taskid = workflowtask.taskid "); 
	    sql.append("     AND assignedtasksworkqueue.institutionprofileid = userprofile.institutionprofileid "); 
	    sql.append("     AND assignedtasksworkqueue.userprofileid = userprofile.userprofileid      "); 
	    sql.append("     AND userprofile.institutionprofileid = c1.institutionprofileid "); 
	    sql.append("     AND userprofile.contactid = c1.contactid      "); 
	    sql.append("     AND userprofile.institutionprofileid = groupprofile.institutionprofileid "); 
	    sql.append("     AND userprofile.groupprofileid = groupprofile.groupprofileid      "); 
	    sql.append("     AND branchprofile.institutionprofileid = groupprofile.institutionprofileid "); 
	    sql.append("     AND branchprofile.branchprofileid = groupprofile.branchprofileid "); 
	    sql.append("     AND deal.institutionprofileid = sourceofbusinessprofile.institutionprofileid "); 
	    sql.append("     AND deal.sourceofbusinessprofileid = sourceofbusinessprofile.sourceofbusinessprofileid "); 
	    sql.append("     AND sourceofbusinessprofile.institutionprofileid = c2.institutionprofileid "); 
	    sql.append("     AND sourceofbusinessprofile.contactid = c2.contactid "); 
	    sql.append(" AND BORROWER.PRIMARYBORROWERFLAG                 = 'Y'");
	    sql.append(" AND DEAL.SCENARIORECOMMENDED                     = 'Y'");
	    sql.append(" AND ASSIGNEDTASKSWORKQUEUE.TASKID                < 50000");
	    sql.append(" AND C1.COPYID                                    = 1");
	    sql.append(" AND C2.COPYID                                    = 1");
	    sql.append(" AND DEAL.COPYTYPE                               <> 'T' ");
	    STATIC_WHERE_CRITERIA = sql.toString();
	}
	/**
  	 * END FIX FOR Performance Issue on Master Work queue [FXP21615]
     */
	public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFASSIGNTASKID =
		"ASSIGNEDTASKSWORKQUEUE.ASSIGNEDTASKSWORKQUEUEID";
	public static final String COLUMN_DFASSIGNTASKID =
		"ASSIGNEDTASKSWORKQUEUEID";
	public static final String QUALIFIED_COLUMN_DFDEALID =
		"ASSIGNEDTASKSWORKQUEUE.DEALID";
	public static final String COLUMN_DFDEALID = "DEALID";
	public static final String QUALIFIED_COLUMN_DFAPPLICATIONID =
		"ASSIGNEDTASKSWORKQUEUE.APPLICATIONID";
	public static final String COLUMN_DFAPPLICATIONID = "APPLICATIONID";
	public static final String QUALIFIED_COLUMN_DFDUETIMESTAMP =
		"ASSIGNEDTASKSWORKQUEUE.ENDTIMESTAMP";
	public static final String COLUMN_DFDUETIMESTAMP = "ENDTIMESTAMP";
	public static final String QUALIFIED_COLUMN_DFUSERPROFILEID =
		"ASSIGNEDTASKSWORKQUEUE.USERPROFILEID";
	public static final String COLUMN_DFUSERPROFILEID = "USERPROFILEID";

	////public static final String QUALIFIED_COLUMN_DFASSIGNEDUSERNAME=".";
	////public static final String COLUMN_DFASSIGNEDUSERNAME="";
	////public static final String QUALIFIED_COLUMN_DFBORROWER=".";
	////public static final String COLUMN_DFBORROWER="";
	public static final String QUALIFIED_COLUMN_DFASSIGNEDUSERNAME =
		"C1.SYNTHETICASSIGNEDUSERNAME";
	public static final String COLUMN_DFASSIGNEDUSERNAME =
		"SYNTHETICASSIGNEDUSERNAME";
	public static final String QUALIFIED_COLUMN_DFBORROWER =
		"BORROWER.SYNTHETICBORRNAME";
	public static final String COLUMN_DFBORROWER = "SYNTHETICBORRNAME";
	public static final String QUALIFIED_COLUMN_DFLOGIN =
		"USERPROFILE.USERLOGIN";
	public static final String COLUMN_DFLOGIN = "USERLOGIN";
	public static final String QUALIFIED_COLUMN_DFGROUPPROFILEID =
		"USERPROFILE.GROUPPROFILEID";
	public static final String COLUMN_DFGROUPPROFILEID = "GROUPPROFILEID";
	public static final String QUALIFIED_COLUMN_DFBRANCHPROFILEID =
		"GROUPPROFILE.BRANCHPROFILEID";
	public static final String COLUMN_DFBRANCHPROFILEID = "BRANCHPROFILEID";
	public static final String QUALIFIED_COLUMN_DFREGIONPROFILEID =
		"BRANCHPROFILE.REGIONPROFILEID";
	public static final String COLUMN_DFREGIONPROFILEID = "REGIONPROFILEID";
	public static final String QUALIFIED_COLUMN_DFCOPYID = "DEAL.COPYID";
	public static final String COLUMN_DFCOPYID = "COPYID";
	public static final String QUALIFIED_COLUMN_DFTASKSTATUSID =
		"ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID";
	public static final String COLUMN_DFTASKSTATUSID = "TASKSTATUSID";

	//--Release2.1--//
	//// 7. In addition to the regular 6 steps to adjust the computed column of fly
	//// another step should be done as well: the 'fake' hidden fields should be created
	//// to override the UserTypeDescripton in the afterExecute() method.
	public static final String QUALIFIED_COLUMN_UTDESCRIPTION =
		"USERPROFILE.USERTYPEID_STR";
	public static final String COLUMN_UTDESCRIPTION = "USERTYPEID_STR";
	public static final String QUALIFIED_COLUMN_USERTYPEID =
		"USERPROFILE.USERTYPEID";
	public static final String COLUMN_USERTYPEID = "USERTYPEID";

	//--> Combined with the fileds from doMWQPickListModel for performance optimization
	//--> By Billy 17July2003
	public static final String QUALIFIED_COLUMN_DFSTATUSDESC =
		"ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID_STR";
	public static final String COLUMN_DFSTATUSDESC = "TASKSTATUSID_STR";
	public static final String QUALIFIED_COLUMN_DFTASKID =
		"ASSIGNEDTASKSWORKQUEUE.TASKID";
	public static final String COLUMN_DFTASKID = "TASKID";
	public static final String QUALIFIED_COLUMN_DFTMDESCRIPTION =
		"ASSIGNEDTASKSWORKQUEUE.TIMEMILESTONEID_STR";
	public static final String COLUMN_DFTMDESCRIPTION = "TIMEMILESTONEID_STR";
	public static final String QUALIFIED_COLUMN_DFTASKNAME =
		"WORKFLOWTASK.TASKNAME";
	public static final String COLUMN_DFTASKNAME = "TASKNAME";
	public static final String QUALIFIED_COLUMN_DFPRIORITYDESCRIPTION =
		"ASSIGNEDTASKSWORKQUEUE.PRIORITYID_STR";
	public static final String COLUMN_DFPRIORITYDESCRIPTION = "PRIORITYID_STR";

	//=================================================================================
	//--TD_MWQ_CR--start--//
	//// Added Source Firm and Source to the Borrower info.
	public static final String QUALIFIED_COLUMN_DFSOURCE =
		"C2.CONTACTLASTNAME";
	public static final String COLUMN_DFSOURCE = "CONTACTLASTNAME";
	public static final String QUALIFIED_COLUMN_SFSHORTNAME =
		"SOURCEFIRMPROFILE.SFSHORTNAME";
	public static final String COLUMN_SFSHORTNAME = "SFSHORTNAME";

	//-- ========== SCR#750 begins ============================================ --//
	//-- by Neil on Dec/20/2004
	public static final String QUALIFIED_COLUMN_DFHOLDREASONID =
		"DEAL.HOLDREASONID";
	public static final String COLUMN_DFHOLDREASONID = "HOLDREASONID";
	public static final String QUALIFIED_COLUMN_DFHOLDREASONDESCRIPTION =
		"DEAL.HOLDREASONID_STR";
	public static final String COLUMN_DFHOLDREASONDESCRIPTION =
		"HOLDREASONID_STR";
	//-- ========== SCR#895 begins ========== --//
	//-- by Neil on Jan/25/2005
	public static final String QUALIFIED_COLUMN_DFDEALSTATUS =
		"DEAL.DEALSTATUSID_STR";
	public static final String COLUMN_DFDEALSTATUS = "DEALSTATUSID_STR";
	//-- ========== SCR#895   ends ========== --//

	//-- ========== SCR#750 ends ============================================== --//
  
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "DEAL.INSTITUTIONPROFILEID";
    public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
    
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONNAME =
        "DEAL.INSTITUTIONPROFILEID_STR";
    public static final String COLUMN_DFINSTITUTIONNAME = "INSTITUTIONPROFILEID_STR";
	////=================================================================================
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////
	static {
		//-- ========== SCR#750 begins ========== --//
		//-- by Neil on Dec/20/2004
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALSTATUS,
				COLUMN_DFDEALSTATUS,
				QUALIFIED_COLUMN_DFDEALSTATUS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFHOLDREASONID,
				COLUMN_DFHOLDREASONID,
				QUALIFIED_COLUMN_DFHOLDREASONID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFHOLDREASONDESCRIPTION,
				COLUMN_DFHOLDREASONDESCRIPTION,
				QUALIFIED_COLUMN_DFHOLDREASONDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		//-- ========== SCR#750 ends ========== --//
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFASSIGNTASKID,
				COLUMN_DFASSIGNTASKID,
				QUALIFIED_COLUMN_DFASSIGNTASKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPLICATIONID,
				COLUMN_DFAPPLICATIONID,
				QUALIFIED_COLUMN_DFAPPLICATIONID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDUETIMESTAMP,
				COLUMN_DFDUETIMESTAMP,
				QUALIFIED_COLUMN_DFDUETIMESTAMP,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERPROFILEID,
				COLUMN_DFUSERPROFILEID,
				QUALIFIED_COLUMN_DFUSERPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


		//// Adjustment to fix the improper conversion of the iMT tool for the Computed
		//// Columns.
		//--Release2.1--//
		//// 1. Replace all Descriptions from the tables included into the PickList with
		//// their joins' counterparts in the SQL_TEMPLATE.
		//// 2. Convert all these new fragments (IDs) into the string format.
		////!!!!|| ' ('  || to_char(USERPROFILE.USERTYPEID) USERTYPEID_STR || ')'
		FIELD_SCHEMA
			.addFieldDescriptor(new QueryFieldDescriptor(
				FIELD_DFASSIGNEDUSERNAME,
				COLUMN_DFASSIGNEDUSERNAME,
				QUALIFIED_COLUMN_DFASSIGNEDUSERNAME,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,

		////"CONTACT.CONTACTLASTNAME || ', ' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1) || ' ('  || USERTYPE.UTDESCRIPTION || ')'",
		"C1.CONTACTLASTNAME || ', ' ||  SUBSTR(C1.CONTACTFIRSTNAME,1,1) || ' (' || to_char(USERPROFILE.USERTYPEID) USERTYPEID_STR || ')'",
			QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
			"C1.CONTACTLASTNAME || ', ' ||  SUBSTR(C1.CONTACTFIRSTNAME,1,1) || ' (' || to_char(USERPROFILE.USERTYPEID) USERTYPEID_STR || ')'"));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWER,
				COLUMN_DFBORROWER,
				QUALIFIED_COLUMN_DFBORROWER,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"BORROWER.BORROWERLASTNAME || ', ' ||  SUBSTR(BORROWER.BORROWERFIRSTNAME,1,1)",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"BORROWER.BORROWERLASTNAME || ', ' ||  SUBSTR(BORROWER.BORROWERFIRSTNAME,1,1)"));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLOGIN,
				COLUMN_DFLOGIN,
				QUALIFIED_COLUMN_DFLOGIN,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFGROUPPROFILEID,
				COLUMN_DFGROUPPROFILEID,
				QUALIFIED_COLUMN_DFGROUPPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRANCHPROFILEID,
				COLUMN_DFBRANCHPROFILEID,
				QUALIFIED_COLUMN_DFBRANCHPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREGIONPROFILEID,
				COLUMN_DFREGIONPROFILEID,
				QUALIFIED_COLUMN_DFREGIONPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTASKSTATUSID,
				COLUMN_DFTASKSTATUSID,
				QUALIFIED_COLUMN_DFTASKSTATUSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		//--Release2.1--//
		//// 7. In addition to the regular 6 steps to adjust the computed column of fly
		//// another step should be done as well: the 'fake' hidden fields should be created
		//// to override the UserTypeDescripton in the afterExecute() method.
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUTDESCRIPTION,
				COLUMN_UTDESCRIPTION,
				QUALIFIED_COLUMN_UTDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERTYPEID,
				COLUMN_USERTYPEID,
				QUALIFIED_COLUMN_USERTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		//--> Combined with the fileds from doMWQPickListModel for performance optimization
		//--> By Billy 17July2003
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTATUSDESC,
				COLUMN_DFSTATUSDESC,
				QUALIFIED_COLUMN_DFSTATUSDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTMDESCRIPTION,
				COLUMN_DFTMDESCRIPTION,
				QUALIFIED_COLUMN_DFTMDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTASKNAME,
				COLUMN_DFTASKNAME,
				QUALIFIED_COLUMN_DFTASKNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIORITYDESCRIPTION,
				COLUMN_DFPRIORITYDESCRIPTION,
				QUALIFIED_COLUMN_DFPRIORITYDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		//--Release2.1--//
		//// 6. Add new fiels taskId to maintain the translation via the BXResoure.
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTASKID,
				COLUMN_DFTASKID,
				QUALIFIED_COLUMN_DFTASKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		//--TD_MWQ_CR--start--//
		//// Added Source Firm and Source to the Borrower info.
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOURCE,
				COLUMN_DFSOURCE,
				QUALIFIED_COLUMN_DFSOURCE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_SFSHORTNAME,
				COLUMN_SFSHORTNAME,
				QUALIFIED_COLUMN_SFSHORTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    
        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFINSTITUTIONID,
                COLUMN_DFINSTITUTIONID,
                QUALIFIED_COLUMN_DFINSTITUTIONID,
                String.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));
        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                  FIELD_DFINSTITUTIONNAME,
                  COLUMN_DFINSTITUTIONNAME,
                  QUALIFIED_COLUMN_DFINSTITUTIONNAME,
                  String.class,
                  false,
                  false,
                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                  "",
                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                  ""));
		////=================================================================================
	}

	////===========================================================================
	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	/**
	 *
	 *
	 */
	public doMasterWorkQueueModelImpl() {
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();
	}

	/**
	 *
	 *
	 */
	public void initialize() {
	}

	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////

	/**
	 *
	 *
	 */
	protected String beforeExecute(
		ModelExecutionContext context,
		int queryType,
		String sql)
		throws ModelControlException {

	  _log.debug("DOMWQPA@afterExecute::Size: " + this.getSize());
		_log.debug("DOMWQPA@beforeExecute::SQL: " + sql);

		return sql;
	}

	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException {
		//--Release2.1--//
		//// 6. Override all the Description fields (see #1-5 below in SQL_TEMPLATE section
		//// and populate them from the BXResource based on the Language ID from the SessionStateModel.

		String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(
				SessionStateModel.class);
		SessionStateModelImpl theSessionState =
			(SessionStateModelImpl) getRequestContext()
				.getModelManager()
				.getModel(
				SessionStateModel.class,
				defaultInstanceStateName,
				true);

		int languageId = theSessionState.getLanguageId();

		while (this.next()) {
			//Convert Task Status Descriptions
			this.setDfUserDescription(
				BXResources.getPickListDescription(this.getDfInstitutionId().intValue(),
					"USERTYPE",
					this.getDfUserTypeId().intValue(),
					languageId));

			//--> Combined with the fileds from doMWQPickListModel for performance optimization
			//--> By Billy 17July2003
			// Convert Task Labels (using the new field DFTASKID).
			this.setDfTASKNAME(
				BXResources.getPickListDescription(this.getDfInstitutionId().intValue(),
					"TASK741",
					this.getDfTASKID().intValue(),
					languageId));

			//Convert Task Status Descriptions
			this.setDfStatusDesc(
				BXResources.getPickListDescription(this.getDfInstitutionId().intValue(),
					"TASKSTATUS",
					this.getDfStatusDesc(),
					languageId));

			//Convert Priority Descriptions
			this.setDfPriorityDescription(
				BXResources.getPickListDescription(this.getDfInstitutionId().intValue(),
					"PRIORITY",
					this.getDfPriorityDescription(),
					languageId));

			//Convert TimeMilestone Descriptions
			this.setDfTMDescription(
				BXResources.getPickListDescription(this.getDfInstitutionId().intValue(),
					"TIMEMILESTONE",
					this.getDfTMDescription(),
					languageId));

			//=================================================================================
			//-- ========== SCR#750 begins ========== --//
			//-- by Neil on Dec/23/2004
			//-- Override Hold Reason with ResourceBundle.
			this.setDfHoldReasonDescription(
				BXResources.getPickListDescription(this.getDfInstitutionId().intValue(),
					"HOLDREASON",
					this.getDfHoldReasonDescription(),
					languageId));
			//-- ========== SCR#750   ends ========== --//

			//-- ========== SCR#895 begins ========== --//
			// -- by Neil on Jan/25/2005 --//
			//Convert Deal Status Descriptions
			this.setDfDealStatus(
				BXResources.getPickListDescription(this.getDfInstitutionId().intValue(),
					"STATUS",
					this.getDfDealStatus(),
					languageId));
			//-- ========== SCR#895   ends ========== --//
            this.setDfInstitutionName(
                    BXResources.getInstitutionName(this.getDfInstitutionId().intValue()));
		}

		//Reset Location
		this.beforeFirst();
	}

	/**
	 *
	 *
	 */
	protected void onDatabaseError(
		ModelExecutionContext context,
		int queryType,
		SQLException exception) {
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAssignTaskId() {
		return (java.math.BigDecimal) getValue(FIELD_DFASSIGNTASKID);
	}

	/**
	 *
	 *
	 */
	public void setDfAssignTaskId(java.math.BigDecimal value) {
		setValue(FIELD_DFASSIGNTASKID, value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDEALID() {
		return (java.math.BigDecimal) getValue(FIELD_DFDEALID);
	}

	/**
	 *
	 *
	 */
	public void setDfDEALID(java.math.BigDecimal value) {
		setValue(FIELD_DFDEALID, value);
	}

	/**
	 *
	 *
	 */
	public String getDfAPPLICATIONID() {
		return (String) getValue(FIELD_DFAPPLICATIONID);
	}

	/**
	 *
	 *
	 */
	public void setDfAPPLICATIONID(String value) {
		setValue(FIELD_DFAPPLICATIONID, value);
	}

	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfDUETIMESTAMP() {
		return (java.sql.Timestamp) getValue(FIELD_DFDUETIMESTAMP);
	}

	/**
	 *
	 *
	 */
	public void setDfDUETIMESTAMP(java.sql.Timestamp value) {
		setValue(FIELD_DFDUETIMESTAMP, value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfUserProfileId() {
		return (java.math.BigDecimal) getValue(FIELD_DFUSERPROFILEID);
	}

	/**
	 *
	 *
	 */
	public void setDfUserProfileId(java.math.BigDecimal value) {
		setValue(FIELD_DFUSERPROFILEID, value);
	}

	/**
	 *
	 *
	 */
	public String getDfAssignedUserName() {
		return (String) getValue(FIELD_DFASSIGNEDUSERNAME);
	}

	/**
	 *
	 *
	 */
	public void setDfAssignedUserName(String value) {
		setValue(FIELD_DFASSIGNEDUSERNAME, value);
	}

	/**
	 *
	 *
	 */
	public String getDfBorrower() {
		return (String) getValue(FIELD_DFBORROWER);
	}

	/**
	 *
	 *
	 */
	public void setDfBorrower(String value) {
		setValue(FIELD_DFBORROWER, value);
	}

	/**
	 *
	 *
	 */
	public String getDfLogin() {
		return (String) getValue(FIELD_DFLOGIN);
	}

	/**
	 *
	 *
	 */
	public void setDfLogin(String value) {
		setValue(FIELD_DFLOGIN, value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfGroupProfileId() {
		return (java.math.BigDecimal) getValue(FIELD_DFGROUPPROFILEID);
	}

	/**
	 *
	 *
	 */
	public void setDfGroupProfileId(java.math.BigDecimal value) {
		setValue(FIELD_DFGROUPPROFILEID, value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBranchProfileId() {
		return (java.math.BigDecimal) getValue(FIELD_DFBRANCHPROFILEID);
	}

	/**
	 *
	 *
	 */
	public void setDfBranchProfileId(java.math.BigDecimal value) {
		setValue(FIELD_DFBRANCHPROFILEID, value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRegionProfileId() {
		return (java.math.BigDecimal) getValue(FIELD_DFREGIONPROFILEID);
	}

	/**
	 *
	 *
	 */
	public void setDfRegionProfileId(java.math.BigDecimal value) {
		setValue(FIELD_DFREGIONPROFILEID, value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId() {
		return (java.math.BigDecimal) getValue(FIELD_DFCOPYID);
	}

	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value) {
		setValue(FIELD_DFCOPYID, value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTaskStatusId() {
		return (java.math.BigDecimal) getValue(FIELD_DFTASKSTATUSID);
	}

	//--Release2.1--//
	//// 7. In addition to the regular 6 steps to adjust the computed column of fly
	//// another step should be done as well: the 'fake' hidden fields should be created
	//// to override the UserTypeDescripton in the afterExecute() method.

	/**
	 *
	 *
	 */
	public String getDfUserDescription() {
		return (String) getValue(FIELD_DFUTDESCRIPTION);
	}

	/**
	 *
	 *
	 */
	public void setDfUserDescription(String value) {
		setValue(FIELD_DFUTDESCRIPTION, value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfUserTypeId() {
		return (java.math.BigDecimal) getValue(FIELD_DFUSERTYPEID);
	}

	/**
	 *
	 *
	 */
	public void setDfUserTypeId(java.math.BigDecimal value) {
		setValue(FIELD_DFUSERTYPEID, value);
	}

	/**
	 *
	 *
	 */
	public void setDfTaskStatusId(java.math.BigDecimal value) {
		setValue(FIELD_DFTASKSTATUSID, value);
	}

	//--> Combined with the fileds from doMWQPickListModel for performance optimization
	//--> By Billy 17July2003
	public String getDfStatusDesc() {
		return (String) getValue(FIELD_DFSTATUSDESC);
	}

	public void setDfStatusDesc(String value) {
		setValue(FIELD_DFSTATUSDESC, value);
	}

	public String getDfTMDescription() {
		return (String) getValue(FIELD_DFTMDESCRIPTION);
	}

	public void setDfTMDescription(String value) {
		setValue(FIELD_DFTMDESCRIPTION, value);
	}

	public String getDfTASKNAME() {
		return (String) getValue(FIELD_DFTASKNAME);
	}

	public void setDfTASKNAME(String value) {
		setValue(FIELD_DFTASKNAME, value);
	}

	public String getDfPriorityDescription() {
		return (String) getValue(FIELD_DFPRIORITYDESCRIPTION);
	}

	public void setDfPriorityDescription(String value) {
		setValue(FIELD_DFPRIORITYDESCRIPTION, value);
	}

	public java.math.BigDecimal getDfTASKID() {
		return (java.math.BigDecimal) getValue(FIELD_DFTASKID);
	}

	public void setDfTASKID(java.math.BigDecimal value) {
		setValue(FIELD_DFTASKID, value);
	}

	//===========================================================================
	//--TD_MWQ_CR--start--//
	//// Added Source Firm and Source to the Borrower info.
	public String getDfSFShortName() {
		return (String) getValue(FIELD_SFSHORTNAME);
	}

	public void setDfSFShortName(String value) {
		setValue(FIELD_SFSHORTNAME, value);
	}

	public String getDfSource() {
		return (String) getValue(FIELD_DFSOURCE);
	}

	public void setDfSource(String value) {
		setValue(FIELD_DFSOURCE, value);
	}

	//-- ========== SCR#750 begins ============================================ --//
	//-- by Neil on Dec/20/2004

	/**
	 *
	 */
	public String getDfDealStatus() {
		return (String) getValue(FIELD_DFDEALSTATUS);
	}

	/**
	 *
	 */
	public void setDfDealStatus(String value) {
		setValue(FIELD_DFDEALSTATUS, value);
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @return String
	 */
	public BigDecimal getDfHoldReasonId() {
		return (BigDecimal) getValue(FIELD_DFHOLDREASONID);
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param value String
	 */
	public void setDfHoldReasonId(BigDecimal value) {
		setValue(FIELD_DFHOLDREASONID, value);
	}

	public String getDfHoldReasonDescription() {
		return (String) getValue(FIELD_DFHOLDREASONDESCRIPTION);
	}

	public void setDfHoldReasonDescription(String value) {
		setValue(FIELD_DFHOLDREASONDESCRIPTION, value);
	}

    public BigDecimal getDfInstitutionId() {
        return (BigDecimal) getValue(FIELD_DFINSTITUTIONID);
    }

    public void setDfInstitutionId(BigDecimal value) {
      setValue(FIELD_DFINSTITUTIONID, value);
    }

    public String getDfInstitutionName() {
        return (String) getValue(FIELD_DFINSTITUTIONNAME);
    }

    public void setDfInstitutionName(String value) {
      setValue(FIELD_DFINSTITUTIONNAME, value);
    }
}
