package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *  TD_MWQ_CR.
 *  TD wants to see the new summary section counted by TaskName
 *  (currently only two tasks: all open Decision Deal
 *                         and Source Resubmition.
 *
 */
public interface doCountTasksByTaskNameModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public Integer getDfTaskNameCount();


	/**
	 *
	 *
	 */
	public void setDfTaskNameCount(Integer value);




	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFTASKNAMECOUNT="dfTaskNameCount";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

