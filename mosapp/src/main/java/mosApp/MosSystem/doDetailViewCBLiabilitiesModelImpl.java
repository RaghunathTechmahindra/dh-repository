package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doDetailViewCBLiabilitiesModelImpl extends QueryModelBase
	implements doDetailViewCBLiabilitiesModel
{
	/**
	 *
	 *
	 */
	public doDetailViewCBLiabilitiesModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 05Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert LIABILITYTYPE Desc.
      this.setDfLiabilityType(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "LIABILITYTYPE", this.getDfLiabilityType(), languageId));
      //Convert LIABILITYPAYOFFTYPE Desc.
      this.setDfLiabilityPayOffDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "LIABILITYPAYOFFTYPE", this.getDfLiabilityPayOffDesc(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLiabilityType()
	{
		return (String)getValue(FIELD_DFLIABILITYTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityType(String value)
	{
		setValue(FIELD_DFLIABILITYTYPE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLiabilityDesc()
	{
		return (String)getValue(FIELD_DFLIABILITYDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityDesc(String value)
	{
		setValue(FIELD_DFLIABILITYDESC,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIABILITYAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIABILITYAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityMonthlyPayment()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIABILITYMONTHLYPAYMENT);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityMonthlyPayment(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIABILITYMONTHLYPAYMENT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPercentIncludedInGDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPERCENTINCLUDEDINGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPercentIncludedInGDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPERCENTINCLUDEDINGDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPercentIncludedInTDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPERCENTINCLUDEDINTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPercentIncludedInTDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPERCENTINCLUDEDINTDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLiabilityPayOffDesc()
	{
		return (String)getValue(FIELD_DFLIABILITYPAYOFFDESC);
	}

    /**
     * This method declaration returns the value of field the CreditBureauLiabilityLimit.
     * 
     * @return dfCreditBureauLiabilityLimit
     */
    public java.math.BigDecimal getDfCreditBureauLiabilityLimit() {
        return (java.math.BigDecimal) getValue(FIELD_DFCREDITBUREAULIABILITYLIMIT);
    }

    /**
     * This method declaration sets the CreditBureauLiabilityLimit.
     * 
     * @param value: the new CreditBureauLiabilityLimit
     */
    public void setDfCreditBureauLiabilityLimit(java.math.BigDecimal value) {
        setValue(FIELD_DFCREDITBUREAULIABILITYLIMIT, value);
    }
	
    /**
     * This method declaration returns the value of field the CreditBureauIndicator.
     * 
     * @return dfCreditBureauIndicator
     */
    public String getDfCreditBureauIndicator() {
        return (String) getValue(FIELD_DFCREDITBUREAUINDICATOR);
    }

    /**
     * This method declaration sets the CreditBureauIndicator.
     * 
     * @param value: the new CreditBureauIndicator
     */
    public void setDfCreditBureauIndicator(String value) {
        setValue(FIELD_DFCREDITBUREAUINDICATOR, value);
    }

    /**
     * This method declaration returns the value of field the MaturityDate.
     * 
     * @return dfMaturityDate
     */
    public java.sql.Timestamp getDfMaturityDate() {
        return (java.sql.Timestamp) getValue(FIELD_DFMATURITYDATE);
    }

    /**
     * This method declaration sets the MaturityDate.
     * 
     * @param value: the new dfMaturityDate
     */
    public void setDfMaturityDate(java.sql.Timestamp value) {
        setValue(FIELD_DFMATURITYDATE, value);
    }
    
	/**
	 *
	 *
	 */
	public void setDfLiabilityPayOffDesc(String value)
	{
		setValue(FIELD_DFLIABILITYPAYOFFDESC,value);
	}


	  public java.math.BigDecimal getDfInstitutionId() {
	        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
	      }

	      public void setDfInstitutionId(java.math.BigDecimal value) {
	        setValue(FIELD_DFINSTITUTIONID, value);
	      }

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 15Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL LIABILITY.BORROWERID, LIABILITYTYPE.LDESCRIPTION, LIABILITY.LIABILITYDESCRIPTION, LIABILITY.LIABILITYAMOUNT, LIABILITY.LIABILITYMONTHLYPAYMENT, LIABILITY.PERCENTINGDS, LIABILITY.PERCENTINTDS, LIABILITY.COPYID, LIABILITYPAYOFFTYPE.LPTDESCRIPTION FROM LIABILITY, LIABILITYTYPE, LIABILITYPAYOFFTYPE  __WHERE__  ORDER BY LIABILITY.LIABILITYTYPEID  ASC, LIABILITY.LIABILITYDESCRIPTION  ASC";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL LIABILITY.BORROWERID, to_char(LIABILITY.LIABILITYTYPEID) LIABILITYTYPEID_STR, "+
    "LIABILITY.LIABILITYDESCRIPTION, LIABILITY.LIABILITYAMOUNT, "+
    "LIABILITY.LIABILITYMONTHLYPAYMENT, LIABILITY.PERCENTINGDS, "+
    "LIABILITY.PERCENTINTDS, LIABILITY.COPYID, to_char(LIABILITY.LIABILITYPAYOFFTYPEID) LIABILITYPAYOFFTYPEID_STR, "+
    "LIABILITY.CREDITLIMIT, LIABILITY.CREDITBUREAURECORDINDICATOR, LIABILITY.MATURITYDATE, " +
    "LIABILITY.INSTITUTIONPROFILEID "+
    " FROM LIABILITY "+
    "__WHERE__  "+
    "ORDER BY LIABILITY.LIABILITYTYPEID  ASC, LIABILITY.LIABILITYDESCRIPTION  ASC";
	//--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //public static final String MODIFYING_QUERY_TABLE_NAME="LIABILITY, LIABILITYTYPE, LIABILITYPAYOFFTYPE";
	public static final String MODIFYING_QUERY_TABLE_NAME=
    "LIABILITY";
	//--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
  //public static final String STATIC_WHERE_CRITERIA=" (((LIABILITY.LIABILITYTYPEID  =  LIABILITYTYPE.LIABILITYTYPEID) AND (LIABILITY.LIABILITYPAYOFFTYPEID  =  LIABILITYPAYOFFTYPE.LIABILITYPAYOFFTYPEID)) AND (LIABILITY.PERCENTOUTGDS = 1 OR LIABILITY.PERCENTOUTGDS = 2 OR LIABILITY.PERCENTOUTGDS = 3)) ";
	public static final String STATIC_WHERE_CRITERIA=
    " (LIABILITY.PERCENTOUTGDS = 1 OR LIABILITY.PERCENTOUTGDS = 2 OR LIABILITY.PERCENTOUTGDS = 3) ";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="LIABILITY.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFLIABILITYDESC="LIABILITY.LIABILITYDESCRIPTION";
	public static final String COLUMN_DFLIABILITYDESC="LIABILITYDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFLIABILITYAMOUNT="LIABILITY.LIABILITYAMOUNT";
	public static final String COLUMN_DFLIABILITYAMOUNT="LIABILITYAMOUNT";
	public static final String QUALIFIED_COLUMN_DFLIABILITYMONTHLYPAYMENT="LIABILITY.LIABILITYMONTHLYPAYMENT";
	public static final String COLUMN_DFLIABILITYMONTHLYPAYMENT="LIABILITYMONTHLYPAYMENT";
	public static final String QUALIFIED_COLUMN_DFPERCENTINCLUDEDINGDS="LIABILITY.PERCENTINGDS";
	public static final String COLUMN_DFPERCENTINCLUDEDINGDS="PERCENTINGDS";
	public static final String QUALIFIED_COLUMN_DFPERCENTINCLUDEDINTDS="LIABILITY.PERCENTINTDS";
	public static final String COLUMN_DFPERCENTINCLUDEDINTDS="PERCENTINTDS";
	public static final String QUALIFIED_COLUMN_DFCOPYID="LIABILITY.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";

	//--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFLIABILITYTYPE="LIABILITYTYPE.LDESCRIPTION";
	//public static final String COLUMN_DFLIABILITYTYPE="LDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFLIABILITYTYPE="LIABILITY.LIABILITYTYPEID_STR";
	public static final String COLUMN_DFLIABILITYTYPE="LIABILITYTYPEID_STR";
  //public static final String QUALIFIED_COLUMN_DFLIABILITYPAYOFFDESC="LIABILITYPAYOFFTYPE.LPTDESCRIPTION";
	//public static final String COLUMN_DFLIABILITYPAYOFFDESC="LPTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFLIABILITYPAYOFFDESC="LIABILITY.LIABILITYPAYOFFTYPEID_STR";
	public static final String COLUMN_DFLIABILITYPAYOFFDESC="LIABILITYPAYOFFTYPEID_STR";
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "LIABILITY.INSTITUTIONPROFILEID";
      public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
      
    public static final String QUALIFIED_COLUMN_DFCREDITBUREAULIABILITYLIMIT="LIABILITY.CREDITLIMIT";
    public static final String COLUMN_DFCREDITBUREAULIABILITYLIMIT="CREDITLIMIT";
    public static final String QUALIFIED_COLUMN_DFCREDITBUREAUINDICATOR="LIABILITY.CREDITBUREAURECORDINDICATOR";
    public static final String COLUMN_DFCREDITBUREAUINDICATOR="CREDITBUREAURECORDINDICATOR";
    public static final String QUALIFIED_COLUMN_DFMATURITYDATE="LIABILITY.MATURITYDATE";
    public static final String COLUMN_DFMATURITYDATE="MATURITYDATE";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	//[[SPIDER_EVENTS BEGIN

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYTYPE,
				COLUMN_DFLIABILITYTYPE,
				QUALIFIED_COLUMN_DFLIABILITYTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYDESC,
				COLUMN_DFLIABILITYDESC,
				QUALIFIED_COLUMN_DFLIABILITYDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYAMOUNT,
				COLUMN_DFLIABILITYAMOUNT,
				QUALIFIED_COLUMN_DFLIABILITYAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYMONTHLYPAYMENT,
				COLUMN_DFLIABILITYMONTHLYPAYMENT,
				QUALIFIED_COLUMN_DFLIABILITYMONTHLYPAYMENT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPERCENTINCLUDEDINGDS,
				COLUMN_DFPERCENTINCLUDEDINGDS,
				QUALIFIED_COLUMN_DFPERCENTINCLUDEDINGDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPERCENTINCLUDEDINTDS,
				COLUMN_DFPERCENTINCLUDEDINTDS,
				QUALIFIED_COLUMN_DFPERCENTINCLUDEDINTDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYPAYOFFDESC,
				COLUMN_DFLIABILITYPAYOFFDESC,
				QUALIFIED_COLUMN_DFLIABILITYPAYOFFDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	      FIELD_SCHEMA.addFieldDescriptor(
	                new QueryFieldDescriptor(
	                  FIELD_DFINSTITUTIONID,
	                  COLUMN_DFINSTITUTIONID,
	                  QUALIFIED_COLUMN_DFINSTITUTIONID,
	                  java.math.BigDecimal.class,
	                  false,
	                  false,
	                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	                  "",
	                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	                  ""));
	      
          FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFCREDITBUREAULIABILITYLIMIT,
                    COLUMN_DFCREDITBUREAULIABILITYLIMIT,
                    QUALIFIED_COLUMN_DFCREDITBUREAULIABILITYLIMIT,
                    java.math.BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""));

          FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                    FIELD_DFCREDITBUREAUINDICATOR,
                    COLUMN_DFCREDITBUREAUINDICATOR,
                    QUALIFIED_COLUMN_DFCREDITBUREAUINDICATOR,
                    String.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""));
            
          FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                    FIELD_DFMATURITYDATE,
                    COLUMN_DFMATURITYDATE,
                    QUALIFIED_COLUMN_DFMATURITYDATE,
                    String.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""));
	}

}

