package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;
import com.basis100.picklist.BXResources;

/**
 *
 *
 *
 */
public class doPAWorkQueueModelImpl extends QueryModelBase
	implements doPAWorkQueueModel
{
	/**
	 *
	 *
	 */
	public doPAWorkQueueModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code

		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{

    //logger = SysLog.getSysLogger("DOPAWQ");
    //logger.debug("DOMWQPA@afterExecute::Size: " + this.getSize());
    //logger.debug("DOMWQPA@afterExecute::SQLTemplate: " + this.getSelectSQL());

    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 05Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();

    while(this.next())
    {
      //--> TODO : Populate the TaskLabel also based on the WorkFlow ID : By Billy 07Nov2002
      //Convert the Task Labels
      //Check if this is a Sentinal Task i.e. TaskId > 50000
      if(this.getDfTASKID().intValue() > 50000)
      {
        //It's a Sentinal Task ==> Added Sentinal Tasl Prefix
        this.setDfTASK_TASKLABEL(
          BXResources.getGenericMsg("SENTINEL_TASK_PREFIX", languageId) + " " +
          BXResources.getPickListDescription(theSessionState.getUserInstitutionId(),
            "TASK741", this.getDfTASKID().intValue() - 50000, languageId));
      }
      else
      {
        //It's a normal Task
          /******  MCM team ML Ticket merge FXP22063 ******/
          this.setDfTASK_TASKLABEL(BXResources.getPickListDescription(this.getDfINSTITUTIONPROFILEID().intValue(),
          "TASK741", this.getDfTASKID().intValue(), languageId));
      }

      //Convert Task Status Descriptions
      this.setDfTASKSTATUS_TSDESCRIPTION(BXResources.getPickListDescription(theSessionState.getUserInstitutionId(),
          "TASKSTATUS", this.getDfTASKSTATUS_TSDESCRIPTION(), languageId));

      //Convert Priority Descriptions
      this.setDfPRIORITY_PDESCRIPTION(BXResources.getPickListDescription(theSessionState.getUserInstitutionId(),
          "PRIORITY", this.getDfPRIORITY_PDESCRIPTION(), languageId));

      //Convert TimeMilestone Descriptions
      this.setDfTMDESCRIPTION(BXResources.getPickListDescription(theSessionState.getUserInstitutionId(),
          "TIMEMILESTONE", this.getDfTMDESCRIPTION(), languageId));
    }

   //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public String getDfCONTACTLASTNAME()
	{
		return (String)getValue(FIELD_DFCONTACTLASTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfCONTACTLASTNAME(String value)
	{
		setValue(FIELD_DFCONTACTLASTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSFSHORTNAME()
	{
		return (String)getValue(FIELD_DFSFSHORTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfSFSHORTNAME(String value)
	{
		setValue(FIELD_DFSFSHORTNAME,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfASSIGNEDTASKSWORKQUEUEID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFASSIGNEDTASKSWORKQUEUEID);
	}


	/**
	 *
	 *
	 */
	public void setDfASSIGNEDTASKSWORKQUEUEID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFASSIGNEDTASKSWORKQUEUEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDEALID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}
	

    /**
     * 
     * MCM team ML Ticket merge FXP22063
     */
    public java.math.BigDecimal getDfINSTITUTIONPROFILEID() {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONPROFILEID);
    }    
    
	/**
	 *
	 *
	 */
	public void setDfDEALID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}
	

    /**
     * 
     * MCM team ML Ticket merge FXP22063
     */
    public void setDfINSTITUTIONPROFILEID(java.math.BigDecimal value)
    {
        setValue(FIELD_DFINSTITUTIONPROFILEID,value);
    }
    
	/**
	 *
	 *
	 */
	public String getDfAPPLICATIONID()
	{
		return (String)getValue(FIELD_DFAPPLICATIONID);
	}


	/**
	 *
	 *
	 */
	public void setDfAPPLICATIONID(String value)
	{
		setValue(FIELD_DFAPPLICATIONID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTASKID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTASKID);
	}


	/**
	 *
	 *
	 */
	public void setDfTASKID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTASKID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfTASK_TASKLABEL()
	{
		return (String)getValue(FIELD_DFTASK_TASKLABEL);
	}


	/**
	 *
	 *
	 */
	public void setDfTASK_TASKLABEL(String value)
	{
		setValue(FIELD_DFTASK_TASKLABEL,value);
	}


	/**
	 *
	 *
	 */
	public String getDfTASKSTATUS_TSDESCRIPTION()
	{
		return (String)getValue(FIELD_DFTASKSTATUS_TSDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfTASKSTATUS_TSDESCRIPTION(String value)
	{
		setValue(FIELD_DFTASKSTATUS_TSDESCRIPTION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPRIORITY_PDESCRIPTION()
	{
		return (String)getValue(FIELD_DFPRIORITY_PDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfPRIORITY_PDESCRIPTION(String value)
	{
		setValue(FIELD_DFPRIORITY_PDESCRIPTION,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfDUETIMESTAMP()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFDUETIMESTAMP);
	}


	/**
	 *
	 *
	 */
	public void setDfDUETIMESTAMP(java.sql.Timestamp value)
	{
		setValue(FIELD_DFDUETIMESTAMP,value);
	}


	/**
	 *
	 *
	 */
	public String getDfTMDESCRIPTION()
	{
		return (String)getValue(FIELD_DFTMDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfTMDESCRIPTION(String value)
	{
		setValue(FIELD_DFTMDESCRIPTION,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCOPYID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCOPYID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	public SysLogger logger;
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 05Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
  /**
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL CONTACT.CONTACTLASTNAME,
  SOURCEFIRMPROFILE.SFSHORTNAME, ASSIGNEDTASKSWORKQUEUE.ASSIGNEDTASKSWORKQUEUEID,
  ASSIGNEDTASKSWORKQUEUE.DEALID, ASSIGNEDTASKSWORKQUEUE.APPLICATIONID,
  ASSIGNEDTASKSWORKQUEUE.TASKID, ASSIGNEDTASKSWORKQUEUE.TASKLABEL,
  TASKSTATUS.TSDESCRIPTION, PRIORITY.PDESCRIPTION,
  ASSIGNEDTASKSWORKQUEUE.DUETIMESTAMP, TIMEMILESTONE.TMDESCRIPTION,
  DEAL.COPYID FROM ASSIGNEDTASKSWORKQUEUE, DEAL, SOURCEOFBUSINESSPROFILE,
  CONTACT, SOURCEFIRMPROFILE, TASKSTATUS, PRIORITY, TIMEMILESTONE
   __WHERE__  ORDER BY ASSIGNEDTASKSWORKQUEUE.PRIORITYID  DESC,
   ASSIGNEDTASKSWORKQUEUE.TIMEMILESTONEID  ASC";
   **/

	public static final String SELECT_SQL_TEMPLATE=
  "SELECT ALL CONTACT.CONTACTLASTNAME, SOURCEFIRMPROFILE.SFSHORTNAME, "+
  "ASSIGNEDTASKSWORKQUEUE.ASSIGNEDTASKSWORKQUEUEID, ASSIGNEDTASKSWORKQUEUE.DEALID, "+
  /*****MCM team ML Ticket merge FXP22063 ****/
  "ASSIGNEDTASKSWORKQUEUE.APPLICATIONID, ASSIGNEDTASKSWORKQUEUE.TASKID, ASSIGNEDTASKSWORKQUEUE.INSTITUTIONPROFILEID, "+
  /*****MCM team ML Ticket merge FXP22063 ****/
  "ASSIGNEDTASKSWORKQUEUE.TASKLABEL, to_char(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID) TASKSTATUSID_STR, "+
  "to_char(ASSIGNEDTASKSWORKQUEUE.PRIORITYID) PRIORITYID_STR, ASSIGNEDTASKSWORKQUEUE.DUETIMESTAMP, "+
  "to_char(ASSIGNEDTASKSWORKQUEUE.TIMEMILESTONEID) TIMEMILESTONEID_STR, DEAL.COPYID "+
  "FROM ASSIGNEDTASKSWORKQUEUE, DEAL, SOURCEOFBUSINESSPROFILE, "+
  "CONTACT, SOURCEFIRMPROFILE  "+
  "__WHERE__  "+
  "ORDER BY ASSIGNEDTASKSWORKQUEUE.PRIORITYID  DESC, ASSIGNEDTASKSWORKQUEUE.TIMEMILESTONEID  ASC";

  //--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  /**
  //public static final String MODIFYING_QUERY_TABLE_NAME="ASSIGNEDTASKSWORKQUEUE, DEAL,
  SOURCEOFBUSINESSPROFILE, CONTACT, SOURCEFIRMPROFILE,
  TASKSTATUS, PRIORITY, TIMEMILESTONE";
  **/

	public static final String MODIFYING_QUERY_TABLE_NAME=
    "ASSIGNEDTASKSWORKQUEUE, DEAL, SOURCEOFBUSINESSPROFILE, CONTACT, SOURCEFIRMPROFILE";

   //--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
  /**
  //public static final String STATIC_WHERE_CRITERIA=" (((ASSIGNEDTASKSWORKQUEUE.DEALID  =  DEAL.DEALID) AND
  (DEAL.SOURCEOFBUSINESSPROFILEID  =  SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPROFILEID) AND
  (SOURCEOFBUSINESSPROFILE.CONTACTID  =  CONTACT.CONTACTID) AND
  (DEAL.SOURCEFIRMPROFILEID  =  SOURCEFIRMPROFILE.SOURCEFIRMPROFILEID) AND
  (ASSIGNEDTASKSWORKQUEUE.PRIORITYID  =  PRIORITY.PRIORITYID) AND
  (ASSIGNEDTASKSWORKQUEUE.TIMEMILESTONEID  =  TIMEMILESTONE.TIMEMILESTONEID) AND
  (TASKSTATUS.TASKSTATUSID  =  ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID)) AND
   ASSIGNEDTASKSWORKQUEUE.ACKNOWLEDGED = 'N' AND
   DEAL.COPYTYPE <> 'T' AND
   DEAL.SCENARIORECOMMENDED = 'Y' AND
   ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG <> 0) ";
  **/

    /******* MCM team ML Ticket merge FXP22063 ********/
    public static final String STATIC_WHERE_CRITERIA = 
        "((assignedtasksworkqueue.dealid = deal.dealid) " +
        "AND (deal.institutionprofileid = assignedtasksworkqueue.institutionprofileid) " +
        "AND (deal.sourceofbusinessprofileid = sourceofbusinessprofile.sourceofbusinessprofileid) " +
        "AND (deal.institutionprofileid = sourceofbusinessprofile.institutionprofileid) " +
        "AND (sourceofbusinessprofile.contactid = contact.contactid) " +
        "AND (sourceofbusinessprofile.institutionprofileid = contact.institutionprofileid) " +
        "AND (deal.sourcefirmprofileid = sourcefirmprofile.sourcefirmprofileid) " +
        "AND (deal.institutionprofileid =sourcefirmprofile.institutionprofileid) " +
        "AND ASSIGNEDTASKSWORKQUEUE.ACKNOWLEDGED = 'N' AND "+
        "DEAL.COPYTYPE <> 'T' AND "+
        "DEAL.SCENARIORECOMMENDED = 'Y' AND "+
        "ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG <> 0 ) ";
    /******* MCM team ML Ticket merge FXP22063 ********/

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFCONTACTLASTNAME="CONTACT.CONTACTLASTNAME";
	public static final String COLUMN_DFCONTACTLASTNAME="CONTACTLASTNAME";
	public static final String QUALIFIED_COLUMN_DFSFSHORTNAME="SOURCEFIRMPROFILE.SFSHORTNAME";
	public static final String COLUMN_DFSFSHORTNAME="SFSHORTNAME";
	public static final String QUALIFIED_COLUMN_DFASSIGNEDTASKSWORKQUEUEID="ASSIGNEDTASKSWORKQUEUE.ASSIGNEDTASKSWORKQUEUEID";
	public static final String COLUMN_DFASSIGNEDTASKSWORKQUEUEID="ASSIGNEDTASKSWORKQUEUEID";
	public static final String QUALIFIED_COLUMN_DFDEALID="ASSIGNEDTASKSWORKQUEUE.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFAPPLICATIONID="ASSIGNEDTASKSWORKQUEUE.APPLICATIONID";
	public static final String COLUMN_DFAPPLICATIONID="APPLICATIONID";
	public static final String QUALIFIED_COLUMN_DFTASKID="ASSIGNEDTASKSWORKQUEUE.TASKID";
	public static final String COLUMN_DFTASKID="TASKID";
	public static final String QUALIFIED_COLUMN_DFTASK_TASKLABEL="ASSIGNEDTASKSWORKQUEUE.TASKLABEL";
	public static final String COLUMN_DFTASK_TASKLABEL="TASKLABEL";
	public static final String QUALIFIED_COLUMN_DFDUETIMESTAMP="ASSIGNEDTASKSWORKQUEUE.DUETIMESTAMP";
	public static final String COLUMN_DFDUETIMESTAMP="DUETIMESTAMP";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
    public static final String COLUMN_DFCOPYID="COPYID";

    /****** MCM team ML Ticket merge FXP22063 ******/
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONPROFILEID="ASSIGNEDTASKSWORKQUEUE.INSTITUTIONPROFILEID";
    public static final String COLUMN_DFINSTITUTIONPROFILEID="INSTITUTIONPROFILEID";
    /****** MCM team ML Ticket merge FXP22063 ******/
  //--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
  //public static final String QUALIFIED_COLUMN_DFTASKSTATUS_TSDESCRIPTION="TASKSTATUS.TSDESCRIPTION";
	//public static final String COLUMN_DFTASKSTATUS_TSDESCRIPTION="TSDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFTASKSTATUS_TSDESCRIPTION="ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID_STR";
	public static final String COLUMN_DFTASKSTATUS_TSDESCRIPTION="TASKSTATUSID_STR";
	//public static final String QUALIFIED_COLUMN_DFPRIORITY_PDESCRIPTION="PRIORITY.PDESCRIPTION";
	//public static final String COLUMN_DFPRIORITY_PDESCRIPTION="PDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFPRIORITY_PDESCRIPTION="ASSIGNEDTASKSWORKQUEUE.PRIORITYID_STR";
	public static final String COLUMN_DFPRIORITY_PDESCRIPTION="PRIORITYID_STR";
  //public static final String QUALIFIED_COLUMN_DFTMDESCRIPTION="TIMEMILESTONE.TMDESCRIPTION";
	//public static final String COLUMN_DFTMDESCRIPTION="TMDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFTMDESCRIPTION="ASSIGNEDTASKSWORKQUEUE.TIMEMILESTONEID_STR";
	public static final String COLUMN_DFTMDESCRIPTION="TIMEMILESTONEID_STR";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTACTLASTNAME,
				COLUMN_DFCONTACTLASTNAME,
				QUALIFIED_COLUMN_DFCONTACTLASTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSFSHORTNAME,
				COLUMN_DFSFSHORTNAME,
				QUALIFIED_COLUMN_DFSFSHORTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFASSIGNEDTASKSWORKQUEUEID,
				COLUMN_DFASSIGNEDTASKSWORKQUEUEID,
				QUALIFIED_COLUMN_DFASSIGNEDTASKSWORKQUEUEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPLICATIONID,
				COLUMN_DFAPPLICATIONID,
				QUALIFIED_COLUMN_DFAPPLICATIONID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTASKID,
				COLUMN_DFTASKID,
				QUALIFIED_COLUMN_DFTASKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTASK_TASKLABEL,
				COLUMN_DFTASK_TASKLABEL,
				QUALIFIED_COLUMN_DFTASK_TASKLABEL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTASKSTATUS_TSDESCRIPTION,
				COLUMN_DFTASKSTATUS_TSDESCRIPTION,
				QUALIFIED_COLUMN_DFTASKSTATUS_TSDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIORITY_PDESCRIPTION,
				COLUMN_DFPRIORITY_PDESCRIPTION,
				QUALIFIED_COLUMN_DFPRIORITY_PDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDUETIMESTAMP,
				COLUMN_DFDUETIMESTAMP,
				QUALIFIED_COLUMN_DFDUETIMESTAMP,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTMDESCRIPTION,
				COLUMN_DFTMDESCRIPTION,
				QUALIFIED_COLUMN_DFTMDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
		FIELD_SCHEMA.addFieldDescriptor(
	            new QueryFieldDescriptor(
	                FIELD_DFINSTITUTIONPROFILEID,
	                COLUMN_DFINSTITUTIONPROFILEID,
	                QUALIFIED_COLUMN_DFINSTITUTIONPROFILEID,
	                java.math.BigDecimal.class,
	                false,
	                false,
	                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	                "",
	                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	                ""));

        /****** MCM team ML Ticket merge FXP22063 ******/
        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFINSTITUTIONPROFILEID,
                    COLUMN_DFINSTITUTIONPROFILEID,
                    QUALIFIED_COLUMN_DFINSTITUTIONPROFILEID,
                    java.math.BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""));
        /****** MCM team ML Ticket merge FXP22063 ******/
	}

}

