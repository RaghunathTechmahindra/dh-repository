package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgClosingrptDealFeesTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgClosingrptDealFeesTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(100);
		setPrimaryModelClass( doClosingDealFeesModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStFeeType().setValue(CHILD_STFEETYPE_RESET_VALUE);
		getStFeeAmount().setValue(CHILD_STFEEAMOUNT_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STFEETYPE,StaticTextField.class);
		registerChild(CHILD_STFEEAMOUNT,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoClosingDealFeesModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// The following code block was migrated from the rptDealFees_onBeforeRowDisplayEvent method
      //--> Not necessary for Jato
      //--> Commented out by Billy 11Sept2002
      /*
		  // Get Data Object
      CSpDataObject theObj =(CSpDataObject) getModel(doClosingDealFeesModel.class);

      if (theObj.getNumRows() > 0) return PROCEED;
      else return SKIP;
      */
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// The following code block was migrated from the rptDealFees_onBeforeDataObjectExecuteEvent method
		ClosingActivityHandler handler =(ClosingActivityHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

		boolean retval = true;

		retval = handler.filterDealFeeReptDo((QueryModelBase)model);

		handler.pageSaveState();

		if(retval == true)
      return super.beforeModelExecutes(model, executionContext);
    else
      return false;
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STFEETYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoClosingDealFeesModel(),
				CHILD_STFEETYPE,
				doClosingDealFeesModel.FIELD_DFFEETYPEDESC,
				CHILD_STFEETYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STFEEAMOUNT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoClosingDealFeesModel(),
				CHILD_STFEEAMOUNT,
				doClosingDealFeesModel.FIELD_DFFEEAMOUNT,
				CHILD_STFEEAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStFeeType()
	{
		return (StaticTextField)getChild(CHILD_STFEETYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStFeeAmount()
	{
		return (StaticTextField)getChild(CHILD_STFEEAMOUNT);
	}


	/**
	 *
	 *
	 */
	public doClosingDealFeesModel getdoClosingDealFeesModel()
	{
		if (doClosingDealFees == null)
			doClosingDealFees = (doClosingDealFeesModel) getModel(doClosingDealFeesModel.class);
		return doClosingDealFees;
	}


	/**
	 *
	 *
	 */
	public void setdoClosingDealFeesModel(doClosingDealFeesModel model)
	{
			doClosingDealFees = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STFEETYPE="stFeeType";
	public static final String CHILD_STFEETYPE_RESET_VALUE="";
	public static final String CHILD_STFEEAMOUNT="stFeeAmount";
	public static final String CHILD_STFEEAMOUNT_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doClosingDealFeesModel doClosingDealFees=null;
  private ClosingActivityHandler handler=new ClosingActivityHandler();

}

