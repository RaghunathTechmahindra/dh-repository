package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doDetailViewCreditRefsModelImpl extends QueryModelBase
	implements doDetailViewCreditRefsModel
{
	/**
	 *
	 *
	 */
	public doDetailViewCreditRefsModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 18Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert CREDITREFTYPE Desc.
      this.setDfCreditReferenceType(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "CREDITREFTYPE", this.getDfCreditReferenceType(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCreditReferenceType()
	{
		return (String)getValue(FIELD_DFCREDITREFERENCETYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfCreditReferenceType(String value)
	{
		setValue(FIELD_DFCREDITREFERENCETYPE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfReferenceDesc()
	{
		return (String)getValue(FIELD_DFREFERENCEDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfReferenceDesc(String value)
	{
		setValue(FIELD_DFREFERENCEDESC,value);
	}


	/**
	 *
	 *
	 */
	public String getDfInstitutionName()
	{
		return (String)getValue(FIELD_DFINSTITUTIONNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfInstitutionName(String value)
	{
		setValue(FIELD_DFINSTITUTIONNAME,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTimeWithReference()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTIMEWITHREFERENCE);
	}


	/**
	 *
	 *
	 */
	public void setDfTimeWithReference(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTIMEWITHREFERENCE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAccountNumber()
	{
		return (String)getValue(FIELD_DFACCOUNTNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfAccountNumber(String value)
	{
		setValue(FIELD_DFACCOUNTNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCurrentBalance()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCURRENTBALANCE);
	}


	/**
	 *
	 *
	 */
	public void setDfCurrentBalance(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCURRENTBALANCE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}

    public java.math.BigDecimal getDfInstitutionId() {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
      }

      public void setDfInstitutionId(java.math.BigDecimal value) {
        setValue(FIELD_DFINSTITUTIONID, value);
      }


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 18Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL CREDITREFERENCE.BORROWERID, CREDITREFTYPE.CRTDESCRIPTION, CREDITREFERENCE.CREDITREFERENCEDESCRIPTION, CREDITREFERENCE.INSTITUTIONNAME, CREDITREFERENCE.TIMEWITHREFERENCE, CREDITREFERENCE.ACCOUNTNUMBER, CREDITREFERENCE.CURRENTBALANCE, CREDITREFERENCE.COPYID FROM CREDITREFERENCE, CREDITREFTYPE  __WHERE__  ";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL CREDITREFERENCE.BORROWERID, to_char(CREDITREFERENCE.CREDITREFTYPEID) CREDITREFTYPEID_STR, "+
    "CREDITREFERENCE.CREDITREFERENCEDESCRIPTION, CREDITREFERENCE.INSTITUTIONNAME, "+
    "CREDITREFERENCE.TIMEWITHREFERENCE, CREDITREFERENCE.ACCOUNTNUMBER, "+
    "CREDITREFERENCE.CURRENTBALANCE, CREDITREFERENCE.COPYID, "+
    "CREDITREFERENCE.INSTITUTIONPROFILEID "+
    " FROM CREDITREFERENCE "+
    "__WHERE__  ";
	//--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //public static final String MODIFYING_QUERY_TABLE_NAME="CREDITREFERENCE, CREDITREFTYPE";
	public static final String MODIFYING_QUERY_TABLE_NAME="CREDITREFERENCE";
	//--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
  //public static final String STATIC_WHERE_CRITERIA=" (CREDITREFERENCE.CREDITREFTYPEID  =  CREDITREFTYPE.CREDITREFTYPEID)";
	public static final String STATIC_WHERE_CRITERIA=
    "";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="CREDITREFERENCE.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFREFERENCEDESC="CREDITREFERENCE.CREDITREFERENCEDESCRIPTION";
	public static final String COLUMN_DFREFERENCEDESC="CREDITREFERENCEDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFINSTITUTIONNAME="CREDITREFERENCE.INSTITUTIONNAME";
	public static final String COLUMN_DFINSTITUTIONNAME="INSTITUTIONNAME";
	public static final String QUALIFIED_COLUMN_DFTIMEWITHREFERENCE="CREDITREFERENCE.TIMEWITHREFERENCE";
	public static final String COLUMN_DFTIMEWITHREFERENCE="TIMEWITHREFERENCE";
	public static final String QUALIFIED_COLUMN_DFACCOUNTNUMBER="CREDITREFERENCE.ACCOUNTNUMBER";
	public static final String COLUMN_DFACCOUNTNUMBER="ACCOUNTNUMBER";
	public static final String QUALIFIED_COLUMN_DFCURRENTBALANCE="CREDITREFERENCE.CURRENTBALANCE";
	public static final String COLUMN_DFCURRENTBALANCE="CURRENTBALANCE";
	public static final String QUALIFIED_COLUMN_DFCOPYID="CREDITREFERENCE.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";

	//--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFCREDITREFERENCETYPE="CREDITREFTYPE.CRTDESCRIPTION";
	//public static final String COLUMN_DFCREDITREFERENCETYPE="CRTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFCREDITREFERENCETYPE="CREDITREFERENCE.CREDITREFTYPEID_STR";
	public static final String COLUMN_DFCREDITREFERENCETYPE="CREDITREFTYPEID_STR";
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "CREDITREFERENCE.INSTITUTIONPROFILEID";
      public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	//[[SPIDER_EVENTS BEGIN

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCREDITREFERENCETYPE,
				COLUMN_DFCREDITREFERENCETYPE,
				QUALIFIED_COLUMN_DFCREDITREFERENCETYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFERENCEDESC,
				COLUMN_DFREFERENCEDESC,
				QUALIFIED_COLUMN_DFREFERENCEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINSTITUTIONNAME,
				COLUMN_DFINSTITUTIONNAME,
				QUALIFIED_COLUMN_DFINSTITUTIONNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTIMEWITHREFERENCE,
				COLUMN_DFTIMEWITHREFERENCE,
				QUALIFIED_COLUMN_DFTIMEWITHREFERENCE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFACCOUNTNUMBER,
				COLUMN_DFACCOUNTNUMBER,
				QUALIFIED_COLUMN_DFACCOUNTNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCURRENTBALANCE,
				COLUMN_DFCURRENTBALANCE,
				QUALIFIED_COLUMN_DFCURRENTBALANCE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
	      FIELD_SCHEMA.addFieldDescriptor(
	                new QueryFieldDescriptor(
	                  FIELD_DFINSTITUTIONID,
	                  COLUMN_DFINSTITUTIONID,
	                  QUALIFIED_COLUMN_DFINSTITUTIONID,
	                  java.math.BigDecimal.class,
	                  false,
	                  false,
	                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	                  "",
	                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	                  ""));

	}

}

