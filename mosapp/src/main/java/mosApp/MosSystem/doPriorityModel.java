package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doPriorityModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public String getDfPriorityLabel();

	
	/**
	 * 
	 * 
	 */
	public void setDfPriorityLabel(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPriorityId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPriorityId(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFPRIORITYLABEL="dfPriorityLabel";
	public static final String FIELD_DFPRIORITYID="dfPriorityId";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

