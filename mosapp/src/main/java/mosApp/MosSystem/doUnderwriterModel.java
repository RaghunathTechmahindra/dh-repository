package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doUnderwriterModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public String getDfUsername();

	
	/**
	 * 
	 * 
	 */
	public void setDfUsername(String value);

	
	/**
	 * 
	 * 
	 */
	public String getMOS_CONTACT_CONTACTFIRSTNAME();

	
	/**
	 * 
	 * 
	 */
	public void setMOS_CONTACT_CONTACTFIRSTNAME(String value);

	
	/**
	 * 
	 * 
	 */
	public String getMOS_CONTACT_CONTACTLASTNAME();

	
	/**
	 * 
	 * 
	 */
	public void setMOS_CONTACT_CONTACTLASTNAME(String value);

	
	/**
	 * 
	 * 
	 */
	public Integer getDfUserProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfUserProfileId(Integer value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFUSERNAME="dfUsername";
	public static final String FIELD_MOS_CONTACT_CONTACTFIRSTNAME="MOS_CONTACT_CONTACTFIRSTNAME";
	public static final String FIELD_MOS_CONTACT_CONTACTLASTNAME="MOS_CONTACT_CONTACTLASTNAME";
	public static final String FIELD_DFUSERPROFILEID="dfUserProfileId";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

