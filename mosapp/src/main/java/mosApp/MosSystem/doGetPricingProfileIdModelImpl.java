package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doGetPricingProfileIdModelImpl extends QueryModelBase
	implements doGetPricingProfileIdModel
{
	/**
	 *
	 *
	 */
	public doGetPricingProfileIdModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPricingProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPRICINGPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPricingProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPRICINGPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfRateCode()
	{
		return (String)getValue(FIELD_DFRATECODE);
	}


	/**
	 *
	 *
	 */
	public void setDfRateCode(String value)
	{
		setValue(FIELD_DFRATECODE,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL PRICINGPROFILE.PRICINGPROFILEID, PRICINGPROFILE.RATECODE FROM PRICINGPROFILE  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="PRICINGPROFILE";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFPRICINGPROFILEID="PRICINGPROFILE.PRICINGPROFILEID";
	public static final String COLUMN_DFPRICINGPROFILEID="PRICINGPROFILEID";
	public static final String QUALIFIED_COLUMN_DFRATECODE="PRICINGPROFILE.RATECODE";
	public static final String COLUMN_DFRATECODE="RATECODE";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRICINGPROFILEID,
				COLUMN_DFPRICINGPROFILEID,
				QUALIFIED_COLUMN_DFPRICINGPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFRATECODE,
				COLUMN_DFRATECODE,
				QUALIFIED_COLUMN_DFRATECODE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

