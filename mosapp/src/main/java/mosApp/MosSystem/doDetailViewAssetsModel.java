package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doDetailViewAssetsModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAssetType();

	
	/**
	 * 
	 * 
	 */
	public void setDfAssetType(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAssetDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfAssetDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAssetValue();

	
	/**
	 * 
	 * 
	 */
	public void setDfAssetValue(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPercentIncludedInNetworth();

	
	/**
	 * 
	 * 
	 */
	public void setDfPercentIncludedInNetworth(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerNetWorth();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerNetWorth(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);
	
	
    public java.math.BigDecimal getDfInstitutionId();
    public void setDfInstitutionId(java.math.BigDecimal id);
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFASSETTYPE="dfAssetType";
	public static final String FIELD_DFASSETDESC="dfAssetDesc";
	public static final String FIELD_DFASSETVALUE="dfAssetValue";
	public static final String FIELD_DFPERCENTINCLUDEDINNETWORTH="dfPercentIncludedInNetworth";
	public static final String FIELD_DFBORROWERNETWORTH="dfBorrowerNetWorth";
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

