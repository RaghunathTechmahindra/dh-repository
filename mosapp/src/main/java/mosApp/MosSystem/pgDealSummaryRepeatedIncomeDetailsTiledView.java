package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgDealSummaryRepeatedIncomeDetailsTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealSummaryRepeatedIncomeDetailsTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(2);
		setPrimaryModelClass( doDetailViewBorrowersModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStIDBorrType().setValue(CHILD_STIDBORRTYPE_RESET_VALUE);
		getStIDBorrFlag().setValue(CHILD_STIDBORRFLAG_RESET_VALUE);
		getStIDBorrSalutation().setValue(CHILD_STIDBORRSALUTATION_RESET_VALUE);
		getStIDborrFirstName().setValue(CHILD_STIDBORRFIRSTNAME_RESET_VALUE);
		getStIDBorrMidInitial().setValue(CHILD_STIDBORRMIDINITIAL_RESET_VALUE);
		getStIDBorrLastName().setValue(CHILD_STIDBORRLASTNAME_RESET_VALUE);
		getEmployment().resetChildren();
		getOtherIncome().resetChildren();
		getStBeginHideEmpSection().setValue(CHILD_STBEGINHIDEEMPSECTION_RESET_VALUE);
		getStEndHideEmpSection().setValue(CHILD_STENDHIDEEMPSECTION_RESET_VALUE);
		getStBeginHideOtherIncSection().setValue(CHILD_STBEGINHIDEOTHERINCSECTION_RESET_VALUE);
		getStEndHideOtherIncSection().setValue(CHILD_STENDHIDEOTHERINCSECTION_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STIDBORRTYPE,StaticTextField.class);
		registerChild(CHILD_STIDBORRFLAG,StaticTextField.class);
		registerChild(CHILD_STIDBORRSALUTATION,StaticTextField.class);
		registerChild(CHILD_STIDBORRFIRSTNAME,StaticTextField.class);
		registerChild(CHILD_STIDBORRMIDINITIAL,StaticTextField.class);
		registerChild(CHILD_STIDBORRLASTNAME,StaticTextField.class);
		registerChild(CHILD_EMPLOYMENT,pgDealSummaryEmploymentTiledView.class);
		registerChild(CHILD_OTHERINCOME,pgDealSummaryOtherIncomeTiledView.class);
		registerChild(CHILD_STBEGINHIDEEMPSECTION,StaticTextField.class);
		registerChild(CHILD_STENDHIDEEMPSECTION,StaticTextField.class);
		registerChild(CHILD_STBEGINHIDEOTHERINCSECTION,StaticTextField.class);
		registerChild(CHILD_STENDHIDEOTHERINCSECTION,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDetailViewBorrowersModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");
		// The following code block was migrated from the RepeatedIncomeDetails_onBeforeDisplayEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.setupNumOfRepeatedIncomeDetails(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// The following code block was migrated from the RepeatedIncomeDetails_onBeforeRowDisplayEvent method
      DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
      handler.pageGetState(this.getParentViewBean());
      handler.setBorrowerIncomes(this.getTileIndex());
      handler.pageSaveState();
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STIDBORRTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STIDBORRTYPE,
				doDetailViewBorrowersModel.FIELD_DFAPPLICANTTYPE,
				CHILD_STIDBORRTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDBORRFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STIDBORRFLAG,
				doDetailViewBorrowersModel.FIELD_DFPRIMARYBORROWER,
				CHILD_STIDBORRFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDBORRSALUTATION))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STIDBORRSALUTATION,
				doDetailViewBorrowersModel.FIELD_DFSALUTATION,
				CHILD_STIDBORRSALUTATION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDBORRFIRSTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STIDBORRFIRSTNAME,
				doDetailViewBorrowersModel.FIELD_DFFIRSTNAME,
				CHILD_STIDBORRFIRSTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDBORRMIDINITIAL))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STIDBORRMIDINITIAL,
				doDetailViewBorrowersModel.FIELD_DFMIDDLENAME,
				CHILD_STIDBORRMIDINITIAL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDBORRLASTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STIDBORRLASTNAME,
				doDetailViewBorrowersModel.FIELD_DFLASTNAME,
				CHILD_STIDBORRLASTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_EMPLOYMENT))
		{
			pgDealSummaryEmploymentTiledView child = new pgDealSummaryEmploymentTiledView(this,
				CHILD_EMPLOYMENT);
			return child;
		}
		else
		if (name.equals(CHILD_OTHERINCOME))
		{
			pgDealSummaryOtherIncomeTiledView child = new pgDealSummaryOtherIncomeTiledView(this,
				CHILD_OTHERINCOME);
			return child;
		}
		else
		if (name.equals(CHILD_STBEGINHIDEEMPSECTION))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBEGINHIDEEMPSECTION,
				CHILD_STBEGINHIDEEMPSECTION,
				CHILD_STBEGINHIDEEMPSECTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STENDHIDEEMPSECTION))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STENDHIDEEMPSECTION,
				CHILD_STENDHIDEEMPSECTION,
				CHILD_STENDHIDEEMPSECTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBEGINHIDEOTHERINCSECTION))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBEGINHIDEOTHERINCSECTION,
				CHILD_STBEGINHIDEOTHERINCSECTION,
				CHILD_STBEGINHIDEOTHERINCSECTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STENDHIDEOTHERINCSECTION))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STENDHIDEOTHERINCSECTION,
				CHILD_STENDHIDEOTHERINCSECTION,
				CHILD_STENDHIDEOTHERINCSECTION_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDBorrType()
	{
		return (StaticTextField)getChild(CHILD_STIDBORRTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDBorrFlag()
	{
		return (StaticTextField)getChild(CHILD_STIDBORRFLAG);
	}


	/**
	 *
	 *
	 */
	public String endStIDBorrFlagDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stIDBorrFlag_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setYesOrNo(event.getContent());
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDBorrSalutation()
	{
		return (StaticTextField)getChild(CHILD_STIDBORRSALUTATION);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDborrFirstName()
	{
		return (StaticTextField)getChild(CHILD_STIDBORRFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDBorrMidInitial()
	{
		return (StaticTextField)getChild(CHILD_STIDBORRMIDINITIAL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDBorrLastName()
	{
		return (StaticTextField)getChild(CHILD_STIDBORRLASTNAME);
	}


	/**
	 *
	 *
	 */
	public pgDealSummaryEmploymentTiledView getEmployment()
	{
		return (pgDealSummaryEmploymentTiledView)getChild(CHILD_EMPLOYMENT);
	}


	/**
	 *
	 *
	 */
	public pgDealSummaryOtherIncomeTiledView getOtherIncome()
	{
		return (pgDealSummaryOtherIncomeTiledView)getChild(CHILD_OTHERINCOME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBeginHideEmpSection()
	{
		return (StaticTextField)getChild(CHILD_STBEGINHIDEEMPSECTION);
	}


	/**
	 *
	 *
	 */
	public String endStBeginHideEmpSectionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stBeginHideEmpSection_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.generateOrHideBeginEmpSect();
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEndHideEmpSection()
	{
		return (StaticTextField)getChild(CHILD_STENDHIDEEMPSECTION);
	}


	/**
	 *
	 *
	 */
	public String endStEndHideEmpSectionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stEndHideEmpSection_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.generateOrHideEndEmpSect();
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBeginHideOtherIncSection()
	{
		return (StaticTextField)getChild(CHILD_STBEGINHIDEOTHERINCSECTION);
	}


	/**
	 *
	 *
	 */
	public String endStBeginHideOtherIncSectionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stBeginHideOtherIncSection_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.generateOrHideBeginOtherIncSect();
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEndHideOtherIncSection()
	{
		return (StaticTextField)getChild(CHILD_STENDHIDEOTHERINCSECTION);
	}


	/**
	 *
	 *
	 */
	public String endStEndHideOtherIncSectionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stEndHideOtherIncSection_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.generateOrHideEndOtherIncSect();
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public doDetailViewBorrowersModel getdoDetailViewBorrowersModel()
	{
		if (doDetailViewBorrowers == null)
			doDetailViewBorrowers = (doDetailViewBorrowersModel) getModel(doDetailViewBorrowersModel.class);
		return doDetailViewBorrowers;
	}


	/**
	 *
	 *
	 */
	public void setdoDetailViewBorrowersModel(doDetailViewBorrowersModel model)
	{
			doDetailViewBorrowers = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STIDBORRTYPE="stIDBorrType";
	public static final String CHILD_STIDBORRTYPE_RESET_VALUE="";
	public static final String CHILD_STIDBORRFLAG="stIDBorrFlag";
	public static final String CHILD_STIDBORRFLAG_RESET_VALUE="";
	public static final String CHILD_STIDBORRSALUTATION="stIDBorrSalutation";
	public static final String CHILD_STIDBORRSALUTATION_RESET_VALUE="";
	public static final String CHILD_STIDBORRFIRSTNAME="stIDborrFirstName";
	public static final String CHILD_STIDBORRFIRSTNAME_RESET_VALUE="";
	public static final String CHILD_STIDBORRMIDINITIAL="stIDBorrMidInitial";
	public static final String CHILD_STIDBORRMIDINITIAL_RESET_VALUE="";
	public static final String CHILD_STIDBORRLASTNAME="stIDBorrLastName";
	public static final String CHILD_STIDBORRLASTNAME_RESET_VALUE="";
	public static final String CHILD_EMPLOYMENT="Employment";
	public static final String CHILD_OTHERINCOME="OtherIncome";
	public static final String CHILD_STBEGINHIDEEMPSECTION="stBeginHideEmpSection";
	public static final String CHILD_STBEGINHIDEEMPSECTION_RESET_VALUE="";
	public static final String CHILD_STENDHIDEEMPSECTION="stEndHideEmpSection";
	public static final String CHILD_STENDHIDEEMPSECTION_RESET_VALUE="";
	public static final String CHILD_STBEGINHIDEOTHERINCSECTION="stBeginHideOtherIncSection";
	public static final String CHILD_STBEGINHIDEOTHERINCSECTION_RESET_VALUE="";
	public static final String CHILD_STENDHIDEOTHERINCSECTION="stEndHideOtherIncSection";
	public static final String CHILD_STENDHIDEOTHERINCSECTION_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDetailViewBorrowersModel doDetailViewBorrowers=null;
  private DealSummaryHandler handler=new DealSummaryHandler();

}

