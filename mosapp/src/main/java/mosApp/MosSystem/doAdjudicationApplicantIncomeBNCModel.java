package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 * <p>Title: doAdjudicationApplicantIncomeBNCModel</p>
 *
 * <p>Description: Interface class for doAdjudicationApplicantIncomeBNCModelImpl, used on Credit Decision screen</p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006 </p
 * 
 * <p>Company: Filogix Inc. </p>
 * 
 * @author NBC/PP Implementation Team
 * @version 1.0
 *
 */
public interface doAdjudicationApplicantIncomeBNCModel extends QueryModel, SelectQueryModel
{
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();

	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value);


	/**
	 * 
	 *
	 */
	public java.math.BigDecimal getDfMonthsOfService();

	/**
	 *
	 *
	 */
	public void setDfMonthsOfService(java.math.BigDecimal value);


	/**
	 * 
	 *
	 */
	public String getDfMonthsAtEmployer();

	/**
	 *
	 *
	 */
	public void setDfMonthsAtEmployer(String value);


	/**
	 * 
	 *
	 */
	public String getDfTimeAtEmployer();

	/**
	 *
	 *
	 */
	public void setDfTimeAtEmployer(String value);


	/**
	 * 
	 *
	 */
	public java.math.BigDecimal getDfAnnualIncomeAmount();

	/**
	 *
	 *
	 */
	public void setDfAnnualIncomeAmount(java.math.BigDecimal value);


	/**
	 * 
	 *
	 */
	public java.math.BigDecimal getDfIncomeTypeId();

	/**
	 *
	 *
	 */
	public void setDfIncomeTypeId(java.math.BigDecimal value);


	/**
	 * 
	 *
	 */
	public String getDfIncomeTypeDesc();

	/**
	 *
	 *
	 */
	public void setDfIncomeTypeDesc(String value);

	/**
	 * 
	 *
	 */
	public String getDfPrimaryBorrowerFlag();

	/**
	 *
	 *
	 */
	public void setDfPrimaryBorrowerFlag(String value);






	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFMONTHSOFSERVICE="dfMonthsOfService";
	public static final String FIELD_DFMONTHSATEMPLOYER="dfMonthsAtEmployer";
	public static final String FIELD_DFTIMEATEMPLOYER="dfTimeAtEmployer";
	public static final String FIELD_DFANNUALINCOMEAMOUNT="dfAnnualIncomeAmount";
	public static final String FIELD_DFINCOMETYPEID="dfIncomeTypeId";
	public static final String FIELD_DFINCOMETYPEDESC="dfIncomeTypeDesc";
	public static final String FIELD_DFPRIMARYBORROWERFLAG="dfPrimaryBorrowerId";
	

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

