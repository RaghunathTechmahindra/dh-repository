package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doBorrowerEntryEmploymentHistoryModelImpl extends QueryModelBase
	implements doBorrowerEntryEmploymentHistoryModel
{
	/**
	 *
	 *
	 */
	public doBorrowerEntryEmploymentHistoryModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEmploymentHistoryId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFEMPLOYMENTHISTORYID);
	}


	/**
	 *
	 *
	 */
	public void setDfEmploymentHistoryId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFEMPLOYMENTHISTORYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEmploymentHistoryOccupationId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFEMPLOYMENTHISTORYOCCUPATIONID);
	}


	/**
	 *
	 *
	 */
	public void setDfEmploymentHistoryOccupationId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFEMPLOYMENTHISTORYOCCUPATIONID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmploymentHistoryName()
	{
		return (String)getValue(FIELD_DFEMPLOYMENTHISTORYNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfEmploymentHistoryName(String value)
	{
		setValue(FIELD_DFEMPLOYMENTHISTORYNAME,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEmploymentHistoryMonthsOfService()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFEMPLOYMENTHISTORYMONTHSOFSERVICE);
	}


	/**
	 *
	 *
	 */
	public void setDfEmploymentHistoryMonthsOfService(java.math.BigDecimal value)
	{
		setValue(FIELD_DFEMPLOYMENTHISTORYMONTHSOFSERVICE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEmploymentHistoryContactId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFEMPLOYMENTHISTORYCONTACTID);
	}


	/**
	 *
	 *
	 */
	public void setDfEmploymentHistoryContactId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFEMPLOYMENTHISTORYCONTACTID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfContactId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCONTACTID);
	}


	/**
	 *
	 *
	 */
	public void setDfContactId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCONTACTID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfContactPhoneNumber()
	{
		return (String)getValue(FIELD_DFCONTACTPHONENUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfContactPhoneNumber(String value)
	{
		setValue(FIELD_DFCONTACTPHONENUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfContactFaxNumber()
	{
		return (String)getValue(FIELD_DFCONTACTFAXNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfContactFaxNumber(String value)
	{
		setValue(FIELD_DFCONTACTFAXNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfContactEmailAddress()
	{
		return (String)getValue(FIELD_DFCONTACTEMAILADDRESS);
	}


	/**
	 *
	 *
	 */
	public void setDfContactEmailAddress(String value)
	{
		setValue(FIELD_DFCONTACTEMAILADDRESS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfContactAddrId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCONTACTADDRID);
	}


	/**
	 *
	 *
	 */
	public void setDfContactAddrId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCONTACTADDRID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAddrId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFADDRID);
	}


	/**
	 *
	 *
	 */
	public void setDfAddrId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFADDRID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAddressLine2()
	{
		return (String)getValue(FIELD_DFADDRESSLINE2);
	}


	/**
	 *
	 *
	 */
	public void setDfAddressLine2(String value)
	{
		setValue(FIELD_DFADDRESSLINE2,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAddressLine1()
	{
		return (String)getValue(FIELD_DFADDRESSLINE1);
	}


	/**
	 *
	 *
	 */
	public void setDfAddressLine1(String value)
	{
		setValue(FIELD_DFADDRESSLINE1,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCity()
	{
		return (String)getValue(FIELD_DFCITY);
	}


	/**
	 *
	 *
	 */
	public void setDfCity(String value)
	{
		setValue(FIELD_DFCITY,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPostalFSA()
	{
		return (String)getValue(FIELD_DFPOSTALFSA);
	}


	/**
	 *
	 *
	 */
	public void setDfPostalFSA(String value)
	{
		setValue(FIELD_DFPOSTALFSA,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPostalLDU()
	{
		return (String)getValue(FIELD_DFPOSTALLDU);
	}


	/**
	 *
	 *
	 */
	public void setDfPostalLDU(String value)
	{
		setValue(FIELD_DFPOSTALLDU,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfProvinceId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROVINCEID);
	}


	/**
	 *
	 *
	 */
	public void setDfProvinceId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROVINCEID,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT EMPLOYMENTHISTORY.BORROWERID, " +
            "EMPLOYMENTHISTORY.EMPLOYMENTHISTORYID, EMPLOYMENTHISTORY.OCCUPATIONID, " +
            "EMPLOYMENTHISTORY.EMPLOYERNAME, EMPLOYMENTHISTORY.MONTHSOFSERVICE, " +
            "EMPLOYMENTHISTORY.CONTACTID, CONTACT.CONTACTID, CONTACT.CONTACTPHONENUMBER, " +
            "CONTACT.CONTACTFAXNUMBER, CONTACT.CONTACTEMAILADDRESS, CONTACT.ADDRID, " +
            "ADDR.ADDRID, ADDR.ADDRESSLINE2, ADDR.ADDRESSLINE1, ADDR.CITY, ADDR.POSTALFSA, " +
            "ADDR.POSTALLDU, ADDR.PROVINCEID FROM EMPLOYMENTHISTORY, CONTACT, ADDR, BORROWER  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="EMPLOYMENTHISTORY, CONTACT, ADDR";
	public static final String STATIC_WHERE_CRITERIA="(EMPLOYMENTHISTORY.INSTITUTIONPROFILEID = BORROWER.INSTITUTIONPROFILEID)" +
            "AND (CONTACT.INSTITUTIONPROFILEID = BORROWER.INSTITUTIONPROFILEID)" +
            "AND (ADDR.INSTITUTIONPROFILEID = BORROWER.INSTITUTIONPROFILEID)" +
            "AND (EMPLOYMENTHISTORY.CONTACTID  =  CONTACT.CONTACTID) AND (CONTACT.ADDRID  =  ADDR.ADDRID)";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="EMPLOYMENTHISTORY.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFEMPLOYMENTHISTORYID="EMPLOYMENTHISTORY.EMPLOYMENTHISTORYID";
	public static final String COLUMN_DFEMPLOYMENTHISTORYID="EMPLOYMENTHISTORYID";
	public static final String QUALIFIED_COLUMN_DFEMPLOYMENTHISTORYOCCUPATIONID="EMPLOYMENTHISTORY.OCCUPATIONID";
	public static final String COLUMN_DFEMPLOYMENTHISTORYOCCUPATIONID="OCCUPATIONID";
	public static final String QUALIFIED_COLUMN_DFEMPLOYMENTHISTORYNAME="EMPLOYMENTHISTORY.EMPLOYERNAME";
	public static final String COLUMN_DFEMPLOYMENTHISTORYNAME="EMPLOYERNAME";
	public static final String QUALIFIED_COLUMN_DFEMPLOYMENTHISTORYMONTHSOFSERVICE="EMPLOYMENTHISTORY.MONTHSOFSERVICE";
	public static final String COLUMN_DFEMPLOYMENTHISTORYMONTHSOFSERVICE="MONTHSOFSERVICE";
	public static final String QUALIFIED_COLUMN_DFEMPLOYMENTHISTORYCONTACTID="EMPLOYMENTHISTORY.CONTACTID";
	public static final String COLUMN_DFEMPLOYMENTHISTORYCONTACTID="CONTACTID";
	public static final String QUALIFIED_COLUMN_DFCONTACTID="CONTACT.CONTACTID";
	public static final String COLUMN_DFCONTACTID="CONTACTID";
	public static final String QUALIFIED_COLUMN_DFCONTACTPHONENUMBER="CONTACT.CONTACTPHONENUMBER";
	public static final String COLUMN_DFCONTACTPHONENUMBER="CONTACTPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFCONTACTFAXNUMBER="CONTACT.CONTACTFAXNUMBER";
	public static final String COLUMN_DFCONTACTFAXNUMBER="CONTACTFAXNUMBER";
	public static final String QUALIFIED_COLUMN_DFCONTACTEMAILADDRESS="CONTACT.CONTACTEMAILADDRESS";
	public static final String COLUMN_DFCONTACTEMAILADDRESS="CONTACTEMAILADDRESS";
	public static final String QUALIFIED_COLUMN_DFCONTACTADDRID="CONTACT.ADDRID";
	public static final String COLUMN_DFCONTACTADDRID="ADDRID";
	public static final String QUALIFIED_COLUMN_DFADDRID="ADDR.ADDRID";
	public static final String COLUMN_DFADDRID="ADDRID";
	public static final String QUALIFIED_COLUMN_DFADDRESSLINE2="ADDR.ADDRESSLINE2";
	public static final String COLUMN_DFADDRESSLINE2="ADDRESSLINE2";
	public static final String QUALIFIED_COLUMN_DFADDRESSLINE1="ADDR.ADDRESSLINE1";
	public static final String COLUMN_DFADDRESSLINE1="ADDRESSLINE1";
	public static final String QUALIFIED_COLUMN_DFCITY="ADDR.CITY";
	public static final String COLUMN_DFCITY="CITY";
	public static final String QUALIFIED_COLUMN_DFPOSTALFSA="ADDR.POSTALFSA";
	public static final String COLUMN_DFPOSTALFSA="POSTALFSA";
	public static final String QUALIFIED_COLUMN_DFPOSTALLDU="ADDR.POSTALLDU";
	public static final String COLUMN_DFPOSTALLDU="POSTALLDU";
	public static final String QUALIFIED_COLUMN_DFPROVINCEID="ADDR.PROVINCEID";
	public static final String COLUMN_DFPROVINCEID="PROVINCEID";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				true,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYMENTHISTORYID,
				COLUMN_DFEMPLOYMENTHISTORYID,
				QUALIFIED_COLUMN_DFEMPLOYMENTHISTORYID,
				java.math.BigDecimal.class,
				true,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYMENTHISTORYOCCUPATIONID,
				COLUMN_DFEMPLOYMENTHISTORYOCCUPATIONID,
				QUALIFIED_COLUMN_DFEMPLOYMENTHISTORYOCCUPATIONID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYMENTHISTORYNAME,
				COLUMN_DFEMPLOYMENTHISTORYNAME,
				QUALIFIED_COLUMN_DFEMPLOYMENTHISTORYNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYMENTHISTORYMONTHSOFSERVICE,
				COLUMN_DFEMPLOYMENTHISTORYMONTHSOFSERVICE,
				QUALIFIED_COLUMN_DFEMPLOYMENTHISTORYMONTHSOFSERVICE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYMENTHISTORYCONTACTID,
				COLUMN_DFEMPLOYMENTHISTORYCONTACTID,
				QUALIFIED_COLUMN_DFEMPLOYMENTHISTORYCONTACTID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTACTID,
				COLUMN_DFCONTACTID,
				QUALIFIED_COLUMN_DFCONTACTID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTACTPHONENUMBER,
				COLUMN_DFCONTACTPHONENUMBER,
				QUALIFIED_COLUMN_DFCONTACTPHONENUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTACTFAXNUMBER,
				COLUMN_DFCONTACTFAXNUMBER,
				QUALIFIED_COLUMN_DFCONTACTFAXNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTACTEMAILADDRESS,
				COLUMN_DFCONTACTEMAILADDRESS,
				QUALIFIED_COLUMN_DFCONTACTEMAILADDRESS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTACTADDRID,
				COLUMN_DFCONTACTADDRID,
				QUALIFIED_COLUMN_DFCONTACTADDRID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRID,
				COLUMN_DFADDRID,
				QUALIFIED_COLUMN_DFADDRID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRESSLINE2,
				COLUMN_DFADDRESSLINE2,
				QUALIFIED_COLUMN_DFADDRESSLINE2,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRESSLINE1,
				COLUMN_DFADDRESSLINE1,
				QUALIFIED_COLUMN_DFADDRESSLINE1,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCITY,
				COLUMN_DFCITY,
				QUALIFIED_COLUMN_DFCITY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPOSTALFSA,
				COLUMN_DFPOSTALFSA,
				QUALIFIED_COLUMN_DFPOSTALFSA,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPOSTALLDU,
				COLUMN_DFPOSTALLDU,
				QUALIFIED_COLUMN_DFPOSTALLDU,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROVINCEID,
				COLUMN_DFPROVINCEID,
				QUALIFIED_COLUMN_DFPROVINCEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

