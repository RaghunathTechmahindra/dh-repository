package mosApp.MosSystem;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.Collator;
import java.util.Locale;

import mosApp.SQLConnectionManagerImpl;

import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryModel;
/**
 * 
 * @author BZhang, May 25, 2006
 *
 */
public class JatoInputUtils {

	/**
	 * Locale-Independent Comparisons.
	 * @param pageLabelvalue	{label, value}
	 * @param languageId
	 * @return
	 */
	static String[][] sortOption(String[][] pageLabelvalue, int languageId)
	{
		Locale locale = languageId == 0 ? Locale.ENGLISH : Locale.FRENCH;
		Collator collator = Collator.getInstance(locale);
		
		String tmp = "";
		for (int i = 0; i < pageLabelvalue.length - 1; i++) {
			for (int j = i + 1; j < pageLabelvalue.length; j++) {
				if (collator.compare(pageLabelvalue[i][0], pageLabelvalue[j][0]) > 0) {
					tmp = pageLabelvalue[i][0];
					pageLabelvalue[i][0] = pageLabelvalue[j][0];
					pageLabelvalue[j][0] = tmp;
					
					tmp = pageLabelvalue[i][1];
					pageLabelvalue[i][1] = pageLabelvalue[j][1];
					pageLabelvalue[j][1] = tmp;
				}
			}
		}
		
		return pageLabelvalue;
	}
}
