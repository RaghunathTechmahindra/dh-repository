package mosApp.MosSystem;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.security.ACM;
import com.basis100.deal.security.ActiveMessage;
import com.basis100.deal.security.DealEditControl;
import com.basis100.deal.security.JumpState;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.picklist.BXResources;


import com.filogix.express.web.ExpressWebContext;
import com.filogix.express.web.state.InstitutionProfileDataBean;
import com.filogix.express.web.state.UserProfileDataBean;
import com.filogix.express.web.state.UserStateDataManager;
import com.filogix.express.core.ExpressRuntimeException;
import com.filogix.express.legacy.mosapp.ISessionStateModel;
import com.filogix.express.state.ExpressState;
import com.filogix.express.state.ExpressStateFactory;
import com.iplanet.jato.model.BeanAdapterModel;
import com.iplanet.jato.model.Model;

/**
 *
 *
 *
 */
//--> This may cause memory leak if we store the HTTPSession the SessionState Object
//--> As the SessionModel will holding a reference to the HTTPSession then when we invalide
//--> the session it will not go away when GC clicks off.
//--> Modified by Billy 11April2003
//public class SessionStateModelImpl extends SessionModel
public class SessionStateModelImpl extends BeanAdapterModel
implements SessionStateModel, Model, ISessionStateModel, Serializable
{

    private final static Log _log = LogFactory.getLog(SessionStateModelImpl.class);
    
    /**
     * the express state.
     *
     * @since 3.3
     */
    private ExpressState _expressState;

    /**
     * the user institution profile id.
     *
     * @since 3.3
     */
    private int _userInstitutionId = -1;

    private UserStateDataManager userStateDataManager;
    /**
     *
     *
     */
    public SessionStateModelImpl()
    {
        super();

        // create a ExpressState with void VPD status.
        ExpressStateFactory factory = (ExpressStateFactory) ExpressWebContext.
        getService(ExpressWebContext.EXPRESS_STATE_FACTORY_KEY);
        _expressState = factory.getExpressState();
    }

    //--> This may cause memory leak if we store the HTTPSession the SessionState Object
    //--> Commented out by Billy 11April2003
    /*
  public SessionStateModelImpl(HttpSession session)
  {
      super();
       this.session = session;
  }
     */

    /**
     * @since 3.3
     */
    public ExpressState getExpressState() {

        return _expressState;
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Convenience accessors & mutators
    ////////////////////////////////////////////////////////////////////////////////

    //--Release2.1--//
    //--> Modifications to support multilingual
    //--> By Billy 10Jan2003
    public java.util.Date getSessionDate()
    {
        return sessionDate;
    }

    public void setSessionDate(java.util.Date theDate)
    {
        sessionDate = theDate;
    }

    public String getSessionDateStr()
    {
        SimpleDateFormat dfs = null;

        //Format the date string based on the current language
        if (this.languageId == 0) //// English
        {
            dfs = new SimpleDateFormat("EEEEE, MMMMM d, yyyy",
                    new Locale(BXResources.getLocaleCode(this.languageId), ""));
        }
        else if (this.languageId == 1) //// French
        {
            dfs = new SimpleDateFormat("EEEEE d MMMMM yyyy",
                    new Locale(BXResources.getLocaleCode(this.languageId), ""));
        }

        return dfs.format(this.sessionDate);
    }
    //========= End Release2.1 changes ===========
    /**
     *
     *
     */
    public boolean getIsAlert()
    {
        return isAlert;

    }


    /**
     *
     *
     */
    public boolean getIsFatal()
    {
        return isFatal;

    }


    /**
     *
     *
     */
    public void setIsAlert(boolean isAlrt)
    {
        isAlert = isAlrt;

    }


    /**
     *
     *
     */
    public void setIsFatal(boolean isFatl)
    {
        isFatal = isFatl;

    }


    /**
     *
     *
     */
    public boolean isInUse()
    {
        return inUse;

    }


    /**
     *
     *
     */
    public void setInUse(boolean b)
    {
        inUse = b;

    }


    /**
     *
     *
     */
    public boolean isActMessageSet()
    {
        if (actMessage == null) return false;

        return actMessage.getGenerate();

    }


    /**
     *
     *
     */
    public boolean isPasMessageSet()
    {
        if (pasMessage == null) return false;

        return pasMessage.getGenerate();

    }


    /**
     *
     *
     */
    public String getBrandName()
    {
        return brandName;

    }


//    /**
//     *
//     *
//     */
//    public String getLanguage()
//    {
//        return language;
//
//    }

    public PageEntry getCurrentPage()
    {
        return currentPage;

    }


    /**
     *
     *
     */
    public DealEditControl getDec()
    {
        return dec;

    }


    /**
     *
     *
     */
    public ACM getAcm()
    {
        return acm;

    }


    /**
     *
     *
     */
    public ActiveMessage getActMessage()
    {
        return actMessage;

    }


    /**
     *
     *
     */
    public PassiveMessage getPasMessage()
    {
        return pasMessage;

    }


    /**
     *
     *
     */
    public JumpState getJmpState()
    {
        return jmpState;

    }


    /**
     *
     *
     */
    public String getDetectAlertTasks()
    {
        return detectAlertTasks;

    }


    /**
     *
     *
     */
    public boolean isEndTheSession()
    {
        return endTheSession;

    }


    /**
     *
     *
     */
    public String getInstitutionName()
    {
        return institutionName;

    }


    /**
     *
     *
     */
    public boolean getPageIsWorkQueue()
    {
        return pageIsWorkQueue;

    }


    /**
     *
     *
     */
    public int getSessionUserId()
    {
        return sessionUserId;

    }


    /**
     *
     *
     */
    public String getSessionUserName()
    {
        return sessionUserName;

    }

    /**
     *
     *
     */
    public String getSessionUserRole()
    {
        return sessionUserRole;

    }


    /**
     *
     *
     */
    public int getSessionUserType()
    {
        return sessionUserType;

    }


    /**
     *
     *
     */
    public int getSessionUserTimeZone()
    {
        return sessionUserTimeZone;

    }


    /**
     *
     *
     */
    public Calendar getLastPasswordChange()
    {
        return lastPasswordChange;

    }


    /**
     *
     *
     */
    public int getAllowedAuthenticationAttempts()
    {
        return allowedAuthenticationAttempts;

    }


    /**
     *
     *
     */
    public int getAuthenticationAttempts()
    {
        return authenticationAttempts;

    }

    //--DJ_LDI_CR--start--//
    /**
     *
     *
     */
    public String getInputToRTPCalc()
    {
        return inputToRTP;
    }

    /**
     *
     *
     */
    public String getInputToInfocalCalc()
    {
        return inputToInfocal;
    }

    /**
     *
     *
     */
    public String getOutputFromInfocalCalc()
    {
        return outputFromInfocal;
    }

    /**
     *
     *
     */
    public boolean getIsDisplayLifeDisabilitySubmit()
    {
        return isDisplayLifeDisabilityBt;
    }

    /**
     *
     *
     */
    public void setIsDisplayLifeDisabilitySubmit(boolean value)
    {
        isDisplayLifeDisabilityBt = value;
    }


    public boolean getSubmitSaved()
    {
        return submitSaved ;
    }

    public void setSubmitSaved(boolean value)
    {
        submitSaved = value;
    }

//  Jerry closing button ends

    /**
     *
     *
     */
    public void setInputToRTPCalc(String val)
    {
        inputToRTP = val;
    }

    /**
     *
     *
     */
    public void setInputToInfocalCalc(String val)
    {
        inputToInfocal = val;
    }

    /**
     *
     *
     */
    public void setOutputFromInfocalCalc(String val)
    {
        outputFromInfocal = val;
    }
    //--DJ_LDI_CR--end--//

    /**
     *
     *
     */
    public void setBrandName(String brndName)
    {
        brandName = brndName;

    }


    /**
     *
     *
     */
    public void setCurrentPage(PageEntry aPageEntry)
    {
        currentPage = aPageEntry;

    }


    /**
     *
     *
     */
    public void setDec(DealEditControl dec)
    {
        this.dec = dec;

    }


    /**
     *
     *
     */
    public void setAcm(ACM acm)
    {
        this.acm = acm;

    }


    /**
     *
     *
     */
    public void setActMessage(ActiveMessage am)
    {
        actMessage = am;

    }


    /**
     *
     *
     */
    public void setPasMessage(PassiveMessage pm)
    {
        pasMessage = pm;

    }


    /**
     *
     *
     */
    public void setJmpState(JumpState js)
    {
        jmpState = js;

    }


    /**
     *
     *
     */
    public void setDetectAlertTasks(String in)
    {
        if (in == null) in = "Y";


        // .. should never happen ... but ...
        in =(in.toUpperCase() + "Y").substring(0, 1);

        if (!(in.equals("Y") || in.equals("N") || in.equals("P"))) in = "Y";

        detectAlertTasks = in;

    }


    /**
     *  No need. See comments in the SessionStateModel class
     *
     */
    ////public void setEndTheSession(boolean anEndTheSession)
    ////{
    ////  endTheSession = anEndTheSession;
    ////
    ////}


    /**
     *
     *
     */
    public void setInstitutionName(String instName)
    {
        institutionName = instName;

    }


    /**
     *
     *
     */
    public void setPageIsWorkQueue(boolean pgIsWorkQueue)
    {
        pageIsWorkQueue = pgIsWorkQueue;

    }

    /**
     * 
     * @param institutionId
     */
    public void switchInstitutionUserData(int institutionId){
        
        if(containtsInstitution(institutionId) == false){
            throw new ExpressRuntimeException("invalid institutionId : " 
                    + institutionId);
        }
        _userInstitutionId = institutionId;
        InstitutionProfileDataBean iBean 
            = userStateDataManager.getInstitutionProfileDataBean(institutionId);
        UserProfileDataBean uBean
            = userStateDataManager.getUserProfileDataBean(institutionId);
        
        brandName = iBean.getBrandName();
        institutionName = iBean.getInstitutionName();
        
        sessionUserId = uBean.getUserProfileId();
        sessionUserName = uBean.getUserFullName();
        sessionUserRole = uBean.getUserTypeDescription();
        sessionUserType = uBean.getUserTypeId();
        sessionUserTimeZone = uBean.getUserTimeZoneId();
        
    }
    
    /**
     * returns the user institution profile id.
     *
     * @since 3.3
     */
    public int getUserInstitutionId() {
        return _userInstitutionId;
    }
    

    /**
     * return deal institution profile id.
     * @since 3.3
     */
    public int getDealInstitutionId() {
        return getCurrentPage().getDealInstitutionId();
    }


    /**
     *
     *
     */
    public void setSessionUserName(String sesUserName)
    {
        sessionUserName = sesUserName;
    }


    /**
     *
     *
     */
    public void setSessionUserRole(String sesUserRole)
    {
        sessionUserRole = sesUserRole;

    }


    /**
     *
     *
     */
    public void setSessionUserType(int sesUserType)
    {
        sessionUserType = sesUserType;

    }


    /**
     *
     *
     */
    public void setSessionUserTimeZone(int tzId)
    {
        sessionUserTimeZone = tzId;

    }


    /**
     *
     *
     */
    public void setLastPasswordChange(Calendar d)
    {
        lastPasswordChange = d;

    }


    /**
     *
     *
     */
    public void setAllowedAuthenticationAttempts(int count)
    {
        allowedAuthenticationAttempts = count;

    }


    /**
     *
     *
     */
    public void setAuthenticationAttempts(int count)
    {
        authenticationAttempts = count;

    }

//    /**
//     *
//     *
//     */
//    public void setLanguage(String currLanguage)
//    {
//        language = currLanguage;
//    }

    //--Release2.1--//
    //New Methods to get / set LanguageId -- By Billy 04Nov2002
    public void setLanguageId(int language)
    {
        languageId = language;
    }

    public int getLanguageId()
    {
        return languageId;
    }

  public boolean getLanguageChanged()
  {
    return languageChanged;
  }
  
  public void setLanguageChanged(boolean aLanguageChanged)
  {
	 languageChanged = aLanguageChanged;
  }

    //=========================================================

    //--Release2.1--TD_DTS_CR--start//
    //// New methods to get/set Vector of Document Ids passed from the
    //// DocTrackingDetails for incliding into the Condtion Letter.


    public void setDocTrackingIds(Vector ids)
    {
        docTrackingIds = ids;
    }

    public Vector getDocTrackingIds()
    {
        return docTrackingIds;
    }
   //=========================================================

    //// In order to overcome the retreiving bug (checkbox) for DefaultModel.
    //// The workaround of reassigning of the binding from DefaultModel to
    //// Primary Model (as for Repeatable Liability) doesn't work.
    public void setCkProduceCondUpdateDoc(String value)
    {
        ckbxValue = value;
    }

    public String getCkProduceCondUpdateDoc()
    {
        return ckbxValue;
    }
    //--Release2.1--TD_DTS_CR--end//

    //--CervusPhaseII--start--//
    public void overrideDealLock(boolean value)
    {
        overrideDealLock = value;
    }

    public boolean isOverrideDealLock()
    {
        return overrideDealLock;
    }
    //--CervusPhaseII--end--//

    ////////////////////////////////////////////////////////////////////////////////
    // Accessors & mutators
    ////////////////////////////////////////////////////////////////////////////////
    /**
     *
     *
     */
    public Model getModel()
    {
        return model;
    }

    /**
     *
     *
     */
    public void setModel(Model value)
    {
        model=value;
    }

    /**
     *
     *
     */

    ////////////////////////////////////////////////////////////////////////////////
    // Model execution methods
    ////////////////////////////////////////////////////////////////////////////////

    ////ADD EXECUTION IMPLEMENTATION.


    ////////////////////////////////////////////////////////////////////////////////
    // Class variables
    ////////////////////////////////////////////////////////////////////////////////

    protected static final String LOGIN_TIME = "USER_LOGIN_TIME";

    ////////////////////////////////////////////////////////////////////////////////
    // Member variables
    ////////////////////////////////////////////////////////////////////////////////

    private boolean inUse=false;
    //private PageEntry currentPage=new PageEntry();
    private PageEntry currentPage;
    private DealEditControl dec=null;
    private ACM acm=null;
    private ActiveMessage actMessage;
    private PassiveMessage pasMessage;
    private JumpState jmpState;
    private boolean isAlert=false;
    private boolean isFatal=false;
    private boolean endTheSession=false;
    private String detectAlertTasks="Y";
    private String institutionName=null;
    private String brandName=null;
    private boolean pageIsWorkQueue=false;
    //--Release2.1--//
    //--> Modified to store the Date instead of String for Multilingual support
    //--> By Billy 10Jan2003
    //private String sessionDateStr=null;
    private java.util.Date sessionDate=null;
    //=========================================================================
//    private UserProfile sessionUser = null;
    private int sessionUserId=0;
    private String sessionUserName=null;
    private String sessionUserRole=null;
    private int sessionUserType=0;
    private int sessionUserTimeZone=5;
    private Calendar lastPasswordChange=null;
    private int allowedAuthenticationAttempts=0;
    private int authenticationAttempts=0;

    private Model model;
//    private PageEntry pageEntry;
    //--> This may cause memory leak if we store the HTTPSession the SessionState Object
    //--> Commented out by Billy 11April2003
    //private HttpSession session;

//    private String name;

//  private String language = "english";
  //--Release2.1--//
  //Added LanguageId to store the current language User Picked
  // By Billy 04Nov2002
  //--> Test by Billy set default to -1 to indicate no set yet
  //private int languageId = 0;  // default to English
  private int languageId = -1;  // default to English
  
  //Language changed flag
  private boolean languageChanged = false;
  
  //==========================================================
  //--Release2.1--TD_DTS_CR--start//
  ////private Vector docTrackingIds = null;
  private Vector docTrackingIds = new Vector();
  private String ckbxValue = "F"; // default is empty ckbox.
  //--Release2.1--TD_DTS_CR--end//

    //--DJ_LDI_CR--start--//
    private String inputToRTP="";
    private String inputToInfocal="";
    private String outputFromInfocal="";
    //default is not display until call the DLL RPT/Infocal calc engine.
    private boolean isDisplayLifeDisabilityBt = false;
    //--DJ_LDI_CR--end--//

    //--CervusPhaseII--start--//
    private boolean overrideDealLock = false; // default
    //--CervusPhaseII--end--//

    //closing button start
//  private boolean isDisplayClosingRequestBt = true;
//  private boolean isDisplayClosingUpdateBt = false;
    private boolean submitSaved = true;
    //closing button start
    ////private Model model;
    ////private PageEntry pageEntry;
    ////private HttpSession session;
    //private HttpSession sessionOut;

    private String userLogin = "";

    /**
     * @return the userLogin
     */
    public String getUserLogin() {
        return userLogin;
    }

    /**
     * @param userLogin
     *            the userLogin to set
     */
    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public void setUserStateDataManager(
            UserStateDataManager userStateDataManager) {
        this.userStateDataManager = userStateDataManager;
    }

    public UserStateDataManager getUserStateDataManager() {
        return userStateDataManager;
    }
    
    public List<Integer> institutionList(){
        
        return userStateDataManager.institutionList();
    }
    
    public boolean containtsInstitution(int institutionId){
        return institutionList().contains(institutionId);
    }
    
    public boolean isMultiAccess(){
        return userStateDataManager.isMultiAccess();
    }
    
    public int getUserProfileId(int institutionProfileId) {
        if(containtsInstitution(institutionProfileId) == false){
            throw new ExpressRuntimeException("invalid institutionId : " 
                    + institutionProfileId);
        }
        return userStateDataManager
                .getUserProfileDataBean(institutionProfileId)
                .getUserProfileId();
    }
    
    public int getUserTypeId(int institutionProfileId){
        if(containtsInstitution(institutionProfileId) == false){
            throw new ExpressRuntimeException("invalid institutionId : " 
                    + institutionProfileId);
        }
        return userStateDataManager
                .getUserProfileDataBean(institutionProfileId)
                .getUserTypeId();
    }

    public int getGroupProfileId(int institutionProfileId){
        if(containtsInstitution(institutionProfileId) == false){
            throw new ExpressRuntimeException("invalid institutionId : " 
                    + institutionProfileId);
        }
        return userStateDataManager
                .getUserProfileDataBean(institutionProfileId)
                .getGroupProfileId();
    }
    
    
    //--STRESSTEST--start--//  
    private String previousPage = null;  
    private String buttonName = null;  

    /**
     * get previous page
     * @return
     */
    public String getPreviousPage() {
        return previousPage;
    }

    /**
     * get button name
     * @return
     */
    public String getInvokeButtonName() {
        return buttonName;
    }

    /**
     * set previous page
     * @param pageName
     */
    public void setPreviousPage(String pageName) {
        previousPage = pageName;
    }

    /**
     * set button name
     * @param bName
     */
    public void setInvokeButtonName(String bName) {
        buttonName = bName;
    }  
    // --STRESSTEST--end--//
    
    /***** 4.3 GR MI Response Alert *****/
    private String miResponseExpected;
    
    /**
     * @return the miResponseExpected
     */
    public String getMiResponseExpected() {
        return miResponseExpected;
    }

    /**
     * @param miResponseExpected the miResponseExpected to set
     */
    public void setMiResponseExpected(String miResponseExpected) {
        this.miResponseExpected = miResponseExpected;
    }
    /***** 4.3 GR MI Response Alert *****/
}
