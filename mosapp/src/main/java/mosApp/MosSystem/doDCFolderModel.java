package mosApp.MosSystem;



import java.math.BigDecimal;
import java.util.*;
import java.sql.*;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

public interface doDCFolderModel extends QueryModel,
        SelectQueryModel {

    
    /*
     * Field bound definition
     */
    public final static String FIELD_DFDEALID =
        "dfDealId";

    public final static String FIELD_DFSOURCEAPPLICATIONID =
        "dfSourceApplicationId";

    public final static String FIELD_DFDCFOLDERCODE =
        "dfDCFolderCode";
    
    public final static String FIELD_DFDATECREATED =
        "dfDateCreated";

    public final static String FIELD_DFDCFOLDERSTATUSCODE =
        "dfDCFolderStatusCode";

    public final static String FIELD_DFDCFOLDERSTATUSDESCRIPTION =
        "dfDCFolderStatusDescription";
    
    public static final String FIELD_DFINSTITUTIONPROFILEID =
        "dfInstitutionProfileId";


    
    /*
     * Getter and Setter
     */
    
    public BigDecimal getDfDealId();
    public void setDfDealId(BigDecimal id);
    
    public String getDfSourceApplicationId();
    public void setDfSourceApplicationId(String sourceApp);

    public String getDfDCFolderCode();
    public void setDfDCFolderCode(String DCFolderCode);

	public Timestamp getDfDateCreated();
    public void setDfDateCreated(Timestamp dateCreated);
    
    public String getDfDCFolderStatusCode();
    public void setDfDCFolderStatusCode(String statusCode);
    
    public String getDfDCFolderStatusDescription();
    public void setDfDCFolderStatusDescription(String statusDescription);
    
    public int getDfInstitutionProfileId();
    public void setDfInstitutionProfileId(int institutionProfileId);

}
