package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doPartySearchModelImpl
  extends QueryModelBase
  implements doPartySearchModel
{
  /**
   *
   *
   */
  public doPartySearchModelImpl()
  {
    super();
    setDataSourceName(DATA_SOURCE_NAME);

    setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

    setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

    setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

    setFieldSchema(FIELD_SCHEMA);

    initialize();

  }

  /**
   *
   *
   */
  public void initialize()
  {
  }

  ////////////////////////////////////////////////////////////////////////////////
  // NetDynamics-migrated events
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  protected String beforeExecute(ModelExecutionContext context, int queryType, String sql) throws ModelControlException
  {

    // TODO: Migrate specific onBeforeExecute code
    return sql;

  }

  /**
   *
   *
   */
  protected void afterExecute(ModelExecutionContext context, int queryType) throws ModelControlException
  {
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By John 06Dec2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
      getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
      (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
      SessionStateModel.class,
      defaultInstanceStateName,
      true);
    int languageId = theSessionState.getLanguageId();

    while(this.next())
    {
      //  Party Type
      this.setDfPartyTypeDescription(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
        "PARTYTYPE", this.getDfPartyTypeDescription(), languageId));

      //  Profile Status
      this.setDfPartyProfileStatusDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
        "PROFILESTATUS", this.getDfPartyProfileStatusDesc(), languageId));

      //  Province abbreviation
      this.setDfProvinceAbbreviation(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
        "PROVINCESHORTNAME", this.getDfProvinceAbbreviation(), languageId));

      //--DJ_PREFCOMMETHOD_CR--start--//
      this.setDfPreferredDeliveryMethod(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
        "PREFERREDDELIVERYMETHOD", this.getDfPreferredDeliveryMethod(), languageId));
      //--DJ_PREFCOMMETHOD_CR--end--//
    }
  }

  /**
   *
   *
   */
  protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
  {
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfPartyType()
  {
    return(java.math.BigDecimal)getValue(FIELD_DFPARTYTYPE);
  }

  /**
   *
   *
   */
  public void setDfPartyType(java.math.BigDecimal value)
  {
    setValue(FIELD_DFPARTYTYPE, value);
  }

  /**
   *
   *
   */
  public String getDfCompany()
  {
    return(String)getValue(FIELD_DFCOMPANY);
  }

  /**
   *
   *
   */
  public void setDfCompany(String value)
  {
    setValue(FIELD_DFCOMPANY, value);
  }

  /**
   *
   *
   */
  public String getDfCity()
  {
    return(String)getValue(FIELD_DFCITY);
  }

  /**
   *
   *
   */
  public void setDfCity(String value)
  {
    setValue(FIELD_DFCITY, value);
  }

  /**
   *
   *
   */
  public String getDfPhone()
  {
    return(String)getValue(FIELD_DFPHONE);
  }

  /**
   *
   *
   */
  public void setDfPhone(String value)
  {
    setValue(FIELD_DFPHONE, value);
  }

  /**
   *
   *
   */
  public String getDfFax()
  {
    return(String)getValue(FIELD_DFFAX);
  }

  /**
   *
   *
   */
  public void setDfFax(String value)
  {
    setValue(FIELD_DFFAX, value);
  }

  /**
   *
   *
   */
  public String getDfProvinceAbbreviation()
  {
    return(String)getValue(FIELD_DFPROVINCEABBREVIATION);
  }

  /**
   *
   *
   */
  public void setDfProvinceAbbreviation(String value)
  {
    setValue(FIELD_DFPROVINCEABBREVIATION, value);
  }

  /**
   *
   *
   */
  public String getDfPartyTypeDescription()
  {
    return(String)getValue(FIELD_DFPARTYTYPEDESCRIPTION);
  }

  /**
   *
   *
   */
  public void setDfPartyTypeDescription(String value)
  {
    setValue(FIELD_DFPARTYTYPEDESCRIPTION, value);
  }

  /**
   *
   *
   */
  public String getDfProvinceName()
  {
    return(String)getValue(FIELD_DFPROVINCENAME);
  }

  /**
   *
   *
   */
  public void setDfProvinceName(String value)
  {
    setValue(FIELD_DFPROVINCENAME, value);
  }

  /**
   *
   *
   */
  public String getDfPhoneNumberExt()
  {
    return(String)getValue(FIELD_DFPHONENUMBEREXT);
  }

  /**
   *
   *
   */
  public void setDfPhoneNumberExt(String value)
  {
    setValue(FIELD_DFPHONENUMBEREXT, value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfPartyStatus()
  {
    return(java.math.BigDecimal)getValue(FIELD_DFPARTYSTATUS);
  }

  /**
   *
   *
   */
  public void setDfPartyStatus(java.math.BigDecimal value)
  {
    setValue(FIELD_DFPARTYSTATUS, value);
  }

  /**
   *
   *
   */
  public String getDfPartyShortName()
  {
    return(String)getValue(FIELD_DFPARTYSHORTNAME);
  }

  /**
   *
   *
   */
  public void setDfPartyShortName(String value)
  {
    setValue(FIELD_DFPARTYSHORTNAME, value);
  }

  /**
   *
   *
   */
  public String getDfClientNumber()
  {
    return(String)getValue(FIELD_DFCLIENTNUMBER);
  }

  /**
   *
   *
   */
  public void setDfClientNumber(String value)
  {
    setValue(FIELD_DFCLIENTNUMBER, value);
  }

  /**
   *
   *
   */
  public String getDfContactFirstName()
  {
    return(String)getValue(FIELD_DFCONTACTFIRSTNAME);
  }

  /**
   *
   *
   */
  public void setDfContactFirstName(String value)
  {
    setValue(FIELD_DFCONTACTFIRSTNAME, value);
  }

  /**
   *
   *
   */
  public String getDfContactIntitial()
  {
    return(String)getValue(FIELD_DFCONTACTINTITIAL);
  }

  /**
   *
   *
   */
  public void setDfContactIntitial(String value)
  {
    setValue(FIELD_DFCONTACTINTITIAL, value);
  }

  /**
   *
   *
   */
  public String getDfContactLastName()
  {
    return(String)getValue(FIELD_DFCONTACTLASTNAME);
  }

  /**
   *
   *
   */
  public void setDfContactLastName(String value)
  {
    setValue(FIELD_DFCONTACTLASTNAME, value);
  }

  /**
   *
   *
   */
  public String getDfContactEmail()
  {
    return(String)getValue(FIELD_DFCONTACTEMAIL);
  }

  /**
   *
   *
   */
  public void setDfContactEmail(String value)
  {
    setValue(FIELD_DFCONTACTEMAIL, value);
  }

  /**
   *
   *
   */
  public String getDfAddressLine1()
  {
    return(String)getValue(FIELD_DFADDRESSLINE1);
  }

  /**
   *
   *
   */
  public void setDfAddressLine1(String value)
  {
    setValue(FIELD_DFADDRESSLINE1, value);
  }

  /**
   *
   *
   */
  public String getDfAddressLine2()
  {
    return(String)getValue(FIELD_DFADDRESSLINE2);
  }

  /**
   *
   *
   */
  public void setDfAddressLine2(String value)
  {
    setValue(FIELD_DFADDRESSLINE2, value);
  }

  /**
   *
   *
   */
  public String getDfPostalFSA()
  {
    return(String)getValue(FIELD_DFPOSTALFSA);
  }

  /**
   *
   *
   */
  public void setDfPostalFSA(String value)
  {
    setValue(FIELD_DFPOSTALFSA, value);
  }

  /**
   *
   *
   */
  public String getDfPostalLDU()
  {
    return(String)getValue(FIELD_DFPOSTALLDU);
  }

  /**
   *
   *
   */
  public void setDfPostalLDU(String value)
  {
    setValue(FIELD_DFPOSTALLDU, value);
  }

  /**
   *
   *
   */
  public String getDfContactPhoneExt()
  {
    return(String)getValue(FIELD_DFCONTACTPHONEEXT);
  }

  /**
   *
   *
   */
  public void setDfContactPhoneExt(String value)
  {
    setValue(FIELD_DFCONTACTPHONEEXT, value);
  }

  /**
   *
   *
   */
  public String getDfPostalFSA_LDU()
  {
    return(String)getValue(FIELD_DFPOSTALFSA_LDU);
  }

  /**
   *
   *
   */
  public void setDfPostalFSA_LDU(String value)
  {
    setValue(FIELD_DFPOSTALFSA_LDU, value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfProfileStatus()
  {
    return(java.math.BigDecimal)getValue(FIELD_DFPROFILESTATUS);
  }

  /**
   *
   *
   */
  public void setDfProfileStatus(java.math.BigDecimal value)
  {
    setValue(FIELD_DFPROFILESTATUS, value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfProvinceId()
  {
    return(java.math.BigDecimal)getValue(FIELD_DFPROVINCEID);
  }

  /**
   *
   *
   */
  public void setDfProvinceId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFPROVINCEID, value);
  }

  /**
   *
   *
   */
  public String getDfPartyProfileStatusDesc()
  {
    return(String)getValue(FIELD_DFPARTYPROFILESTATUSDESC);
  }

  /**
   *
   *
   */
  public void setDfPartyProfileStatusDesc(String value)
  {
    setValue(FIELD_DFPARTYPROFILESTATUSDESC, value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfPartyId()
  {
    return(java.math.BigDecimal)getValue(FIELD_DFPARTYID);
  }

  /**
   *
   *
   */
  public void setDfPartyId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFPARTYID, value);
  }

  /**
   *
   *
   */
  public String getDfPartyName()
  {
    return(String)getValue(FIELD_DFPARTYNAME);
  }

  /**
   *
   *
   */
  public void setDfPartyName(String value)
  {
    setValue(FIELD_DFPARTYNAME, value);
  }

  /**
   *
   *
   */
  public String getDfNotes()
  {
    return(String)getValue(FIELD_DFNOTES);
  }

  /**
   *
   *
   */
  public void setDfNotes(String value)
  {
    setValue(FIELD_DFNOTES, value);
  }

  //--DJ_PREFCOMMETHOD_CR--start--//
  /**
   *
   *
   */
  public String getDfPreferredDeliveryMethod()
  {
    return(String)getValue(FIELD_DFPREFERREDDELIVERYMETHOD);
  }

  /**
   *
   *
   */
  public void setDfPreferredDeliveryMethod(String value)
  {
    setValue(FIELD_DFPREFERREDDELIVERYMETHOD, value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfPreferredDeliveryMethodId()
  {
    return(java.math.BigDecimal)getValue(FIELD_DFPREFERREDDELIVERYMETHODID);
  }

  /**
   *
   *
   */
  public void setDfPreferredDeliveryMethodId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFPREFERREDDELIVERYMETHODID, value);
  }

  //--DJ_PREFCOMMETHOD_CR--end--//

  //--DJ_PT_CR--start//
  /**
   *
   *
   */
  public String getDfBranchTransitNumCMHC()
  {
    return(String)getValue(FIELD_DFBRANCHTRANSITNUMCMHC);
  }

  /**
   *
   *
   */
  public void setDfBranchTransitNumCMHC(String value)
  {
    setValue(FIELD_DFBRANCHTRANSITNUMCMHC, value);
  }

  /**
   *
   *
   */
  public String getDfBranchTransitNumGE()
  {
    return(String)getValue(FIELD_DFBRANCHTRANSITNUMGE);
  }

  /**
   *
   *
   */
  public void setDfBranchTransitNumGE(String value)
  {
    setValue(FIELD_DFBRANCHTRANSITNUMGE, value);
  }

  /**
   *
   *
   */
  public String getDfPartyNameFirstPart()
  {
    return(String)getValue(FIELD_DFPARTYNAMEFIRSTPART);
  }

  /**
   *
   *
   */
  public void setDfPartyNameFirstPart(String value)
  {
    setValue(FIELD_DFPARTYNAMEFIRSTPART, value);
  }

  /**
   *
   *
   */
  public String getDfPartyNameSecondPart()
  {
    return(String)getValue(FIELD_DFPARTYNAMESECONDPART);
  }

  /**
   *
   *
   */
  public void setDfPartyNameSecondPart(String value)
  {
    setValue(FIELD_DFPARTYNAMESECONDPART, value);
  }

  /**
   *
   *
   */
  public String getDfPartyNameThirdPart()
  {
    return(String)getValue(FIELD_DFPARTYNAMETHIRDPART);
  }

  /**
   *
   *
   */
  public void setDfPartyNameThirdPart(String value)
  {
    setValue(FIELD_DFPARTYNAMETHIRDPART, value);
  }

  //--DJ_PT_CR--end//

  //--> Ticket#369 :: Change for Cervus :: added New Bank info for Party
  //--> By Billy 21May2004
  public String getDfBankTransitNum()
  {
    return(String)getValue(FIELD_DFBANKTRANSITNUM);
  }

  public void setDfBankTransitNum(String value)
  {
    setValue(FIELD_DFBANKTRANSITNUM, value);
  }

  public String getDfBankAccNum()
  {
    return(String)getValue(FIELD_DFBANKACCNUM);
  }

  public void setDfBankAccNum(String value)
  {
    setValue(FIELD_DFBANKACCNUM, value);
  }

  public String getDfBankName()
  {
    return(String)getValue(FIELD_DFBANKNAME);
  }

  public void setDfBankName(String value)
  {
    setValue(FIELD_DFBANKNAME, value);
  }

  //====================================================================
  /**
   * getDfLocation<br>
   * 	This method returns the value for the constant FIELD_DFLOCATION
   * 
   * @param none<br>
   * @version 1.0 (Initial Version - 29 May 2006)
   * @author:NBC/PP Implementation Team<br>
   * 
   */
  public String getDfLocation()
  {
	  return(String)getValue(FIELD_DFLOCATION);
  }
  
  /**
   * setDfLocation<br>
   * 	This method sets the value for the constant FIELD_DFLOCATION
   * 
   * @param value String<br>
   * 	New value for location
   * @version 1.0 (Initial Version - 29 May 2006)
   * @author:NBC/PP Implementation Team<br>
   * 
   */
  public void setDfLocation(String value)
  {
	  setValue(FIELD_DFLOCATION, value);
  }  

  public java.math.BigDecimal getDfInstitutionId() {
      return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
    }

    public void setDfInstitutionId(java.math.BigDecimal value) {
      setValue(FIELD_DFINSTITUTIONID, value);
    }
    
  /**
   * MetaData object test method to verify the SYNTHETIC new
   * field for the reogranized SQL_TEMPLATE query.
   *
   */
  /**
    public void setResultSet(ResultSet value)
    {
    logger = SysLog.getSysLogger("METADATA");

    try
    {
        super.setResultSet(value);
      if( value != null)
      {
          ResultSetMetaData metaData = value.getMetaData();
          int columnCount = metaData.getColumnCount();
          logger.debug("===============================================");
            logger.debug("Testing MetaData Object for new synthetic fields for" +
                          " doPartySearchModel");
          logger.debug("NumberColumns: " + columnCount);
           for(int i = 0; i < columnCount ; i++)
            {
                  logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
            }
            logger.debug("================================================");
        }
    }
    catch (SQLException e)
    {
          logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
    }
     }
   **/

  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Custom Methods - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////

  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.
  public static final String DATA_SOURCE_NAME = "jdbc/orcl";

  ////public static final String SELECT_SQL_TEMPLATE="SELECT ALL PARTYPROFILE.PARTYTYPEID, PARTYPROFILE.PARTYCOMPANYNAME, ADDR.CITY, CONTACT.CONTACTPHONENUMBER, CONTACT.CONTACTFAXNUMBER, PROVINCE.PROVINCEABBREVIATION, PARTYTYPE.PTDESCRIPTION, PROVINCE.PROVINCENAME, CONTACT.CONTACTPHONENUMBEREXTENSION, PARTYPROFILE.PROFILESTATUSID, PARTYPROFILE.PTPSHORTNAME, PARTYPROFILE.PTPBUSINESSID, CONTACT.CONTACTFIRSTNAME, CONTACT.CONTACTMIDDLEINITIAL, CONTACT.CONTACTLASTNAME, CONTACT.CONTACTEMAILADDRESS, ADDR.ADDRESSLINE1, ADDR.ADDRESSLINE2, ADDR.POSTALFSA, ADDR.POSTALLDU, PARTYTYPE.PTDESCRIPTION, CONTACT.CONTACTPHONENUMBEREXTENSION, ADDR.POSTALFSA || ' ' || ADDR.POSTALLDU POSTAL, PROFILESTATUS.PROFILESTATUSID, PROVINCE.PROVINCEID, PROFILESTATUS.PSDESCRIPTION, PARTYPROFILE.PARTYPROFILEID, PARTYPROFILE.PARTYNAME, PARTYPROFILE.NOTES FROM PARTYPROFILE, ADDR, CONTACT, PROVINCE, PARTYTYPE, PROFILESTATUS  __WHERE__  ORDER BY PARTYPROFILE.PARTYNAME  ASC";
  //--Release2.1--//
  //  Changed to use BXResources
  //public static final String SELECT_SQL_TEMPLATE="SELECT ALL PARTYPROFILE.PARTYTYPEID, PARTYPROFILE.PARTYCOMPANYNAME, ADDR.CITY, CONTACT.CONTACTPHONENUMBER, CONTACT.CONTACTFAXNUMBER, PROVINCE.PROVINCEABBREVIATION, PARTYTYPE.PTDESCRIPTION, PROVINCE.PROVINCENAME, CONTACT.CONTACTPHONENUMBEREXTENSION, PARTYPROFILE.PROFILESTATUSID, PARTYPROFILE.PTPSHORTNAME, PARTYPROFILE.PTPBUSINESSID, CONTACT.CONTACTFIRSTNAME, CONTACT.CONTACTMIDDLEINITIAL, CONTACT.CONTACTLASTNAME, CONTACT.CONTACTEMAILADDRESS, ADDR.ADDRESSLINE1, ADDR.ADDRESSLINE2, ADDR.POSTALFSA, ADDR.POSTALLDU, PARTYTYPE.PTDESCRIPTION, CONTACT.CONTACTPHONENUMBEREXTENSION, ADDR.POSTALFSA || ' ' || ADDR.POSTALLDU SYNTHETICPOSTALCODE, PROFILESTATUS.PROFILESTATUSID, PROVINCE.PROVINCEID, PROFILESTATUS.PSDESCRIPTION, PARTYPROFILE.PARTYPROFILEID, PARTYPROFILE.PARTYNAME, PARTYPROFILE.NOTES FROM PARTYPROFILE, ADDR, CONTACT, PROVINCE, PARTYTYPE, PROFILESTATUS  __WHERE__  ORDER BY PARTYPROFILE.PARTYNAME  ASC";
  //public static final String MODIFYING_QUERY_TABLE_NAME="PARTYPROFILE, ADDR, CONTACT, PROVINCE, PARTYTYPE, PROFILESTATUS";
  //public static final String STATIC_WHERE_CRITERIA=" (PARTYPROFILE.CONTACTID  =  CONTACT.CONTACTID) AND (PARTYPROFILE.COPYID  =  CONTACT.COPYID) AND (PARTYPROFILE.PARTYTYPEID  =  PARTYTYPE.PARTYTYPEID) AND (ADDR.ADDRID  =  CONTACT.ADDRID) AND (ADDR.COPYID  =  CONTACT.COPYID) AND (PARTYPROFILE.PROFILESTATUSID  =  PROFILESTATUS.PROFILESTATUSID) AND (ADDR.PROVINCEID  =  PROVINCE.PROVINCEID) ";
  public static final String SELECT_SQL_TEMPLATE =
    "SELECT ALL PARTYPROFILE.PARTYTYPEID, PARTYPROFILE.PARTYCOMPANYNAME, " +
    "ADDR.CITY, CONTACT.CONTACTPHONENUMBER, CONTACT.CONTACTFAXNUMBER, " +
    //--> Modified to eliminate the table joint : fixed by Billy 21May2004
    //"to_char(PROVINCE.PROVINCEID) PROVINCEID_STR, to_char(PARTYTYPE.PARTYTYPEID) PARTYTYPEID_STR, " +
    "to_char(PROVINCE.PROVINCEID) PROVINCEID_STR, to_char(PARTYPROFILE.PARTYTYPEID) PARTYTYPEID_STR, " +
    //=======================================================================
    "PROVINCE.PROVINCENAME, CONTACT.CONTACTPHONENUMBEREXTENSION, " +
    "PARTYPROFILE.PROFILESTATUSID, PARTYPROFILE.PTPSHORTNAME, " +
    //--DJ_PT_CR--//
    "PARTYPROFILE.CHMCBRANCHTRANSITNUM, PARTYPROFILE.GEBRANCHTRANSITNUM, " +
    "PARTYPROFILE.PTPBUSINESSID, CONTACT.CONTACTFIRSTNAME, " +
    "CONTACT.CONTACTMIDDLEINITIAL, CONTACT.CONTACTLASTNAME, " +
    "CONTACT.CONTACTEMAILADDRESS, ADDR.ADDRESSLINE1, ADDR.ADDRESSLINE2, " +
    //--> Modified to eliminate the table joint : fixed by Billy 21May2004
    //"ADDR.POSTALFSA, ADDR.POSTALLDU, PARTYTYPE.PTDESCRIPTION, " +
    "ADDR.POSTALFSA, ADDR.POSTALLDU, " +
    //====================================================================
    "CONTACT.CONTACTPHONENUMBEREXTENSION, " +
    "ADDR.POSTALFSA || ' ' || ADDR.POSTALLDU SYNTHETICPOSTALCODE, " +
    "PARTYPROFILE.PROFILESTATUSID, PROVINCE.PROVINCEID, " +
    //--> Modified to eliminate the table joint : fixed by Billy 21May2004
    //"to_char(PROFILESTATUS.PROFILESTATUSID) PROFILESTATUSID_STR, " +
    "to_char(PARTYPROFILE.PROFILESTATUSID) PROFILESTATUSID_STR, " +
    //====================================================================
    "PARTYPROFILE.PARTYPROFILEID, PARTYPROFILE.PARTYNAME, " +
    "PARTYPROFILE.NOTES, " +
    //--DJ_PREFCOMMETHOD_CR--start--//
    //--> Modified to eliminate the table joint : fixed by Billy 21May2004
    //"to_char(PREFERREDDELIVERYMETHOD.PREFERREDDELIVERYMETHODID) PREFERREDDELIVERYMETHODID_STR, " +
    "to_char(CONTACT.PREFERREDDELIVERYMETHODID) PREFERREDDELIVERYMETHODID_STR, " +
    //=====================================================================
    "CONTACT.PREFERREDDELIVERYMETHODID, " +
    //--DJ_PREFCOMMETHOD_CR--end--//
    //--DJ_PT_CR--start--//
    // Should be just substr from partyname (it is already selected).
    "substr(PARTYPROFILE.PARTYNAME, 0, 50) SUBSTRFIRSTPART, " +
    "substr(PARTYPROFILE.PARTYNAME, 51, 50) SUBSTRSECONDPART, " +
    "substr(PARTYPROFILE.PARTYNAME, 101, 50) SUBSTRTHIRDPART, " +

    //--DJ_PT_CR--end--//
    //--> Ticket#369 :: Change for Cervus :: added New Bank info for Party
    //--> By Billy 21May2004
    //"PARTYPROFILE.BANKTRANSITNUM, PARTYPROFILE.BANKACCNUM, PARTYPROFILE.BANKNAME " +
    //====================================================================
    //***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//
    // Add Location column for Service Branch
    "PARTYPROFILE.BANKTRANSITNUM, PARTYPROFILE.BANKACCNUM, PARTYPROFILE.BANKNAME, " +
    "PARTYPROFILE.LOCATION,PARTYPROFILE.INSTITUTIONPROFILEID " + 
    //***** Change by NBC/PP Implementation Team - Version 1.1 - End *****// 

    "FROM PARTYPROFILE, ADDR, CONTACT, PROVINCE " +
    //--> Modified to eliminate the table joint : fixed by Billy 21May2004
    //"PARTYTYPE, PROFILESTATUS  " +
    //--DJ_PREFCOMMETHOD_CR--start--//
    //"PREFERREDDELIVERYMETHOD " +
    //--DJ_PREFCOMMETHOD_CR--end--//
    "  __WHERE__  ORDER BY PARTYPROFILE.PARTYNAME  ASC";

  public static final String MODIFYING_QUERY_TABLE_NAME = "PARTYPROFILE, ADDR, CONTACT, PROVINCE ";

  //--> Modified to eliminate the table joint : fixed by Billy 21May2004
  //"PARTYTYPE, PROFILESTATUS " +
  //--DJ_PREFCOMMETHOD_CR--start--//
  //"PREFERREDDELIVERYMETHOD ";
  //--DJ_PREFCOMMETHOD_CR--end--//

  public static final String STATIC_WHERE_CRITERIA = " (PARTYPROFILE.CONTACTID  =  CONTACT.CONTACTID) " +
    "AND (PARTYPROFILE.COPYID  =  CONTACT.COPYID) AND (PARTYPROFILE.INSTITUTIONPROFILEID  =  CONTACT.INSTITUTIONPROFILEID) " +
    //--> Modified to eliminate the table joint : fixed by Billy 21May2004
    //"AND (PARTYPROFILE.PARTYTYPEID  =  PARTYTYPE.PARTYTYPEID) " +
    "AND (ADDR.ADDRID  =  CONTACT.ADDRID) " +
    "AND (ADDR.COPYID  =  CONTACT.COPYID) AND (ADDR.INSTITUTIONPROFILEID  =  CONTACT.INSTITUTIONPROFILEID) " +
    //--DJ_PREFCOMMETHOD_CR--start--//
    //"AND (CONTACT.PREFERREDDELIVERYMETHODID  =  PREFERREDDELIVERYMETHOD.PREFERREDDELIVERYMETHODID) " +
    //--DJ_PREFCOMMETHOD_CR--end--//
    //"AND (PARTYPROFILE.PROFILESTATUSID  =  PROFILESTATUS.PROFILESTATUSID) " +
    "AND (ADDR.PROVINCEID  =  PROVINCE.PROVINCEID) ";

  public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();
  public static final String QUALIFIED_COLUMN_DFPARTYTYPE = "PARTYPROFILE.PARTYTYPEID";
  public static final String COLUMN_DFPARTYTYPE = "PARTYTYPEID";
  public static final String QUALIFIED_COLUMN_DFCOMPANY = "PARTYPROFILE.PARTYCOMPANYNAME";
  public static final String COLUMN_DFCOMPANY = "PARTYCOMPANYNAME";
  public static final String QUALIFIED_COLUMN_DFCITY = "ADDR.CITY";
  public static final String COLUMN_DFCITY = "CITY";
  public static final String QUALIFIED_COLUMN_DFPHONE = "CONTACT.CONTACTPHONENUMBER";
  public static final String COLUMN_DFPHONE = "CONTACTPHONENUMBER";
  public static final String QUALIFIED_COLUMN_DFFAX = "CONTACT.CONTACTFAXNUMBER";
  public static final String COLUMN_DFFAX = "CONTACTFAXNUMBER";

  //--Release2.1--//
  //  Changed to use BXResources
  //public static final String QUALIFIED_COLUMN_DFPROVINCEABBREVIATION="PROVINCE.PROVINCEABBREVIATION";
  //public static final String COLUMN_DFPROVINCEABBREVIATION="PROVINCEABBREVIATION";
  //public static final String QUALIFIED_COLUMN_DFPARTYTYPEDESCRIPTION="PARTYTYPE.PTDESCRIPTION";
  //public static final String COLUMN_DFPARTYTYPEDESCRIPTION="PTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFPROVINCEABBREVIATION = "PROVINCE.PROVINCEID_STR";
  public static final String COLUMN_DFPROVINCEABBREVIATION = "PROVINCEID_STR";
  public static final String QUALIFIED_COLUMN_DFPARTYTYPEDESCRIPTION = "PARTYPROFILE.PARTYTYPEID_STR";
  public static final String COLUMN_DFPARTYTYPEDESCRIPTION = "PARTYTYPEID_STR";

  //--------------//

  public static final String QUALIFIED_COLUMN_DFPROVINCENAME = "PROVINCE.PROVINCENAME";
  public static final String COLUMN_DFPROVINCENAME = "PROVINCENAME";
  public static final String QUALIFIED_COLUMN_DFPHONENUMBEREXT = "CONTACT.CONTACTPHONENUMBEREXTENSION";
  public static final String COLUMN_DFPHONENUMBEREXT = "CONTACTPHONENUMBEREXTENSION";
  public static final String QUALIFIED_COLUMN_DFPARTYSTATUS = "PARTYPROFILE.PROFILESTATUSID";
  public static final String COLUMN_DFPARTYSTATUS = "PROFILESTATUSID";
  public static final String QUALIFIED_COLUMN_DFPARTYSHORTNAME = "PARTYPROFILE.PTPSHORTNAME";
  public static final String COLUMN_DFPARTYSHORTNAME = "PTPSHORTNAME";
  public static final String QUALIFIED_COLUMN_DFCLIENTNUMBER = "PARTYPROFILE.PTPBUSINESSID";
  public static final String COLUMN_DFCLIENTNUMBER = "PTPBUSINESSID";
  public static final String QUALIFIED_COLUMN_DFCONTACTFIRSTNAME = "CONTACT.CONTACTFIRSTNAME";
  public static final String COLUMN_DFCONTACTFIRSTNAME = "CONTACTFIRSTNAME";
  public static final String QUALIFIED_COLUMN_DFCONTACTINTITIAL = "CONTACT.CONTACTMIDDLEINITIAL";
  public static final String COLUMN_DFCONTACTINTITIAL = "CONTACTMIDDLEINITIAL";
  public static final String QUALIFIED_COLUMN_DFCONTACTLASTNAME = "CONTACT.CONTACTLASTNAME";
  public static final String COLUMN_DFCONTACTLASTNAME = "CONTACTLASTNAME";
  public static final String QUALIFIED_COLUMN_DFCONTACTEMAIL = "CONTACT.CONTACTEMAILADDRESS";
  public static final String COLUMN_DFCONTACTEMAIL = "CONTACTEMAILADDRESS";
  public static final String QUALIFIED_COLUMN_DFADDRESSLINE1 = "ADDR.ADDRESSLINE1";
  public static final String COLUMN_DFADDRESSLINE1 = "ADDRESSLINE1";
  public static final String QUALIFIED_COLUMN_DFADDRESSLINE2 = "ADDR.ADDRESSLINE2";
  public static final String COLUMN_DFADDRESSLINE2 = "ADDRESSLINE2";
  public static final String QUALIFIED_COLUMN_DFPOSTALFSA = "ADDR.POSTALFSA";
  public static final String COLUMN_DFPOSTALFSA = "POSTALFSA";
  public static final String QUALIFIED_COLUMN_DFPOSTALLDU = "ADDR.POSTALLDU";
  public static final String COLUMN_DFPOSTALLDU = "POSTALLDU";
  public static final String QUALIFIED_COLUMN_DFCONTACTPHONEEXT = "CONTACT.CONTACTPHONENUMBEREXTENSION";
  public static final String COLUMN_DFCONTACTPHONEEXT = "CONTACTPHONENUMBEREXTENSION";

  ////public static final String QUALIFIED_COLUMN_DFPOSTALFSA_LDU=".";
  ////public static final String COLUMN_DFPOSTALFSA_LDU="";
  public static final String QUALIFIED_COLUMN_DFPOSTALFSA_LDU = "ADDR.SYNTHETICPOSTALCODE";
  public static final String COLUMN_DFPOSTALFSA_LDU = "SYNTHETICPOSTALCODE";
  public static final String QUALIFIED_COLUMN_DFPROFILESTATUS = "PARTYPROFILE.PROFILESTATUSID";
  public static final String COLUMN_DFPROFILESTATUS = "PROFILESTATUSID";
  public static final String QUALIFIED_COLUMN_DFPROVINCEID = "PROVINCE.PROVINCEID";
  public static final String COLUMN_DFPROVINCEID = "PROVINCEID";

  //--Release2.1--//
  //  Changed to use BXResources
  //public static final String QUALIFIED_COLUMN_DFPARTYPROFILESTATUSDESC="PROFILESTATUS.PSDESCRIPTION";
  //public static final String COLUMN_DFPARTYPROFILESTATUSDESC="PSDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFPARTYPROFILESTATUSDESC = "PARTYPROFILE.PROFILESTATUSID_STR";
  public static final String COLUMN_DFPARTYPROFILESTATUSDESC = "PROFILESTATUSID_STR";

  //--------------//

  public static final String QUALIFIED_COLUMN_DFPARTYID = "PARTYPROFILE.PARTYPROFILEID";
  public static final String COLUMN_DFPARTYID = "PARTYPROFILEID";
  public static final String QUALIFIED_COLUMN_DFPARTYNAME = "PARTYPROFILE.PARTYNAME";
  public static final String COLUMN_DFPARTYNAME = "PARTYNAME";

  public static final String QUALIFIED_COLUMN_DFNOTES = "PARTYPROFILE.NOTES";
  public static final String COLUMN_DFNOTES = "NOTES";

  //--DJ_PREFCOMMETHOD_CR--start--//
  public static final String QUALIFIED_COLUMN_DFPREFERREDDELIVERYMETHOD = "CONTACT.PREFERREDDELIVERYMETHODID_STR";
  public static final String COLUMN_DFPREFERREDDELIVERYMETHOD = "PREFERREDDELIVERYMETHODID_STR";

  public static final String QUALIFIED_COLUMN_DFPREFERREDDELIVERYMETHODID = "CONTACT.PREFERREDDELIVERYMETHODID";
  public static final String COLUMN_DFPREFERREDDELIVERYMETHODID = "PREFERREDDELIVERYMETHODID";

  //--DJ_PREFCOMMETHOD_CR--end--//

  //--DJ_PT_CR--start//
  public static final String QUALIFIED_COLUMN_DFBRANCHTRANSITNUMCMHC = "PARTYPROFILE.CHMCBRANCHTRANSITNUM";
  public static final String COLUMN_DFBRANCHTRANSITNUMCMHC = "CHMCBRANCHTRANSITNUM";

  public static final String QUALIFIED_COLUMN_DFBRANCHTRANSITNUMGE = "PARTYPROFILE.GEBRANCHTRANSITNUM";
  public static final String COLUMN_DFBRANCHTRANSITNUMGE = "GEBRANCHTRANSITNUM";

  public static final String QUALIFIED_COLUMN_DFPARTYNAMEFIRSTPART = "PARTYPROFILE.SUBSTRFIRSTPART";
  public static final String COLUMN_DFPARTYNAMEFIRSTPART = "SUBSTRFIRSTPART";

  public static final String QUALIFIED_COLUMN_DFPARTYNAMESECONDPART = "PARTYPROFILE.SUBSTRSECONDPART";
  public static final String COLUMN_DFPARTYNAMESECONDPART = "SUBSTRSECONDPART";

  public static final String QUALIFIED_COLUMN_DFPARTYNAMETHIRDPART = "PARTYPROFILE.SUBSTRTHIRDPART";
  public static final String COLUMN_DFPARTYNAMETHIRDPART = "SUBSTRTHIRDPART";

  //--DJ_PT_CR--end//

  //--> Ticket#369 :: Change for Cervus :: added New Bank info for Party
  //--> By Billy 21May2004
  public static final String QUALIFIED_COLUMN_DFBANKTRANSITNUM = "PARTYPROFILE.BANKTRANSITNUM";
  public static final String COLUMN_DFBANKTRANSITNUM = "BANKTRANSITNUM";
  public static final String QUALIFIED_COLUMN_DFBANKACCNUM = "PARTYPROFILE.BANKACCNUM";
  public static final String COLUMN_DFBANKACCNUM = "BANKACCNUM";
  public static final String QUALIFIED_COLUMN_DFBANKNAME = "PARTYPROFILE.BANKNAME";
  public static final String COLUMN_DFBANKNAME = "BANKNAME";

  //====================================================================
  //***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//
  // Add for new location field
  public static final String QUALIFIED_COLUMN_DFLOCATION = "PARTYPROFILE.LOCATION";
  public static final String COLUMN_DFLOCATION = "LOCATION";
  //***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//
  public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
      "PARTYPROFILE.INSTITUTIONPROFILEID";
public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////
  // Custom Members - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////

  static
  {
    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPARTYTYPE,
      COLUMN_DFPARTYTYPE,
      QUALIFIED_COLUMN_DFPARTYTYPE,
      java.math.BigDecimal.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFCOMPANY,
      COLUMN_DFCOMPANY,
      QUALIFIED_COLUMN_DFCOMPANY,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFCITY,
      COLUMN_DFCITY,
      QUALIFIED_COLUMN_DFCITY,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPHONE,
      COLUMN_DFPHONE,
      QUALIFIED_COLUMN_DFPHONE,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFFAX,
      COLUMN_DFFAX,
      QUALIFIED_COLUMN_DFFAX,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPROVINCEABBREVIATION,
      COLUMN_DFPROVINCEABBREVIATION,
      QUALIFIED_COLUMN_DFPROVINCEABBREVIATION,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPARTYTYPEDESCRIPTION,
      COLUMN_DFPARTYTYPEDESCRIPTION,
      QUALIFIED_COLUMN_DFPARTYTYPEDESCRIPTION,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPROVINCENAME,
      COLUMN_DFPROVINCENAME,
      QUALIFIED_COLUMN_DFPROVINCENAME,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPHONENUMBEREXT,
      COLUMN_DFPHONENUMBEREXT,
      QUALIFIED_COLUMN_DFPHONENUMBEREXT,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPARTYSTATUS,
      COLUMN_DFPARTYSTATUS,
      QUALIFIED_COLUMN_DFPARTYSTATUS,
      java.math.BigDecimal.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPARTYSHORTNAME,
      COLUMN_DFPARTYSHORTNAME,
      QUALIFIED_COLUMN_DFPARTYSHORTNAME,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFCLIENTNUMBER,
      COLUMN_DFCLIENTNUMBER,
      QUALIFIED_COLUMN_DFCLIENTNUMBER,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFCONTACTFIRSTNAME,
      COLUMN_DFCONTACTFIRSTNAME,
      QUALIFIED_COLUMN_DFCONTACTFIRSTNAME,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFCONTACTINTITIAL,
      COLUMN_DFCONTACTINTITIAL,
      QUALIFIED_COLUMN_DFCONTACTINTITIAL,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFCONTACTLASTNAME,
      COLUMN_DFCONTACTLASTNAME,
      QUALIFIED_COLUMN_DFCONTACTLASTNAME,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFCONTACTEMAIL,
      COLUMN_DFCONTACTEMAIL,
      QUALIFIED_COLUMN_DFCONTACTEMAIL,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFADDRESSLINE1,
      COLUMN_DFADDRESSLINE1,
      QUALIFIED_COLUMN_DFADDRESSLINE1,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFADDRESSLINE2,
      COLUMN_DFADDRESSLINE2,
      QUALIFIED_COLUMN_DFADDRESSLINE2,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPOSTALFSA,
      COLUMN_DFPOSTALFSA,
      QUALIFIED_COLUMN_DFPOSTALFSA,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPOSTALLDU,
      COLUMN_DFPOSTALLDU,
      QUALIFIED_COLUMN_DFPOSTALLDU,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFCONTACTPHONEEXT,
      COLUMN_DFCONTACTPHONEEXT,
      QUALIFIED_COLUMN_DFCONTACTPHONEEXT,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    ////FIELD_SCHEMA.addFieldDescriptor(
    ////	new QueryFieldDescriptor(
    ////		FIELD_DFPOSTALFSA_LDU,
    ////		COLUMN_DFPOSTALFSA_LDU,
    ////		QUALIFIED_COLUMN_DFPOSTALFSA_LDU,
    ////		String.class,
    ////		false,
    ////		false,
    ////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
    ////		"",
    ////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
    ////		""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPOSTALFSA_LDU,
      COLUMN_DFPOSTALFSA_LDU,
      QUALIFIED_COLUMN_DFPOSTALFSA_LDU,
      String.class,
      false,
      true,
      QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
      "ADDR.POSTALFSA || ' ' || ADDR.POSTALLDU",
      QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
      "ADDR.POSTALFSA || ' ' || ADDR.POSTALLDU"));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPROFILESTATUS,
      COLUMN_DFPROFILESTATUS,
      QUALIFIED_COLUMN_DFPROFILESTATUS,
      java.math.BigDecimal.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPROVINCEID,
      COLUMN_DFPROVINCEID,
      QUALIFIED_COLUMN_DFPROVINCEID,
      java.math.BigDecimal.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPARTYPROFILESTATUSDESC,
      COLUMN_DFPARTYPROFILESTATUSDESC,
      QUALIFIED_COLUMN_DFPARTYPROFILESTATUSDESC,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPARTYID,
      COLUMN_DFPARTYID,
      QUALIFIED_COLUMN_DFPARTYID,
      java.math.BigDecimal.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPARTYNAME,
      COLUMN_DFPARTYNAME,
      QUALIFIED_COLUMN_DFPARTYNAME,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFNOTES,
      COLUMN_DFNOTES,
      QUALIFIED_COLUMN_DFNOTES,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    //--DJ_PREFCOMMETHOD_CR--start--//
    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPREFERREDDELIVERYMETHOD,
      COLUMN_DFPREFERREDDELIVERYMETHOD,
      QUALIFIED_COLUMN_DFPREFERREDDELIVERYMETHOD,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPREFERREDDELIVERYMETHODID,
      COLUMN_DFPREFERREDDELIVERYMETHODID,
      QUALIFIED_COLUMN_DFPREFERREDDELIVERYMETHODID,
      java.math.BigDecimal.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));
    //--DJ_PREFCOMMETHOD_CR--end--//

    //--DJ_PT_CR--start//
    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFBRANCHTRANSITNUMCMHC,
      COLUMN_DFBRANCHTRANSITNUMCMHC,
      QUALIFIED_COLUMN_DFBRANCHTRANSITNUMCMHC,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFBRANCHTRANSITNUMGE,
      COLUMN_DFBRANCHTRANSITNUMGE,
      QUALIFIED_COLUMN_DFBRANCHTRANSITNUMGE,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPARTYNAMEFIRSTPART,
      COLUMN_DFPARTYNAMEFIRSTPART,
      QUALIFIED_COLUMN_DFPARTYNAMEFIRSTPART,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPARTYNAMESECONDPART,
      COLUMN_DFPARTYNAMESECONDPART,
      QUALIFIED_COLUMN_DFPARTYNAMESECONDPART,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));
    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFPARTYNAMETHIRDPART,
      COLUMN_DFPARTYNAMETHIRDPART,
      QUALIFIED_COLUMN_DFPARTYNAMETHIRDPART,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));
    //--DJ_PT_CR--end//

    //--> Ticket#369 :: Change for Cervus :: added New Bank info for Party
    //--> By Billy 21May2004
    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFBANKTRANSITNUM,
      COLUMN_DFBANKTRANSITNUM,
      QUALIFIED_COLUMN_DFBANKTRANSITNUM,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFBANKACCNUM,
      COLUMN_DFBANKACCNUM,
      QUALIFIED_COLUMN_DFBANKACCNUM,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFBANKNAME,
      COLUMN_DFBANKNAME,
      QUALIFIED_COLUMN_DFBANKNAME,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));
    //=============================================================================
    //  ***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//
    // Add for new location field
    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFLOCATION,
      COLUMN_DFLOCATION,
      QUALIFIED_COLUMN_DFLOCATION,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));
    //  ***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//
    FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
              FIELD_DFINSTITUTIONID,
              COLUMN_DFINSTITUTIONID,
              QUALIFIED_COLUMN_DFINSTITUTIONID,
              java.math.BigDecimal.class,
              false,
              false,
              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
              "",
              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
              ""));
  }

}
