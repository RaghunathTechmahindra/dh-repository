package mosApp.MosSystem;

import java.util.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;
import com.basis100.picklist.BXResources;
import java.io.IOException;

import javax.servlet.ServletException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.view.event.RequestInvocationEvent;
/**
 * <p>Title: pgDisclosurePEIViewBean
 *
 * Description: View bean for Disclosure PEI PAGE
 * Company: Filogix Inc.
 * Date: 11/28/2006 
 * @author NBC/PP Implementation Team
 * @version 1.0 
 *
 */
public class pgDisclosurePEIViewBean extends pgDisclosureViewBean {
  /**
   * pgDisclosurePEIViewBean() This constructor sets the url of the jsp and also
   * makes a call to the registerChildren() method 
   * Date: 11/28/2006
   * 
   * @param name
   *            This is the name of the child form element to create	 
   *
   **/
  public pgDisclosurePEIViewBean()
  {
	super(PAGE_NAME);          
	setDefaultDisplayURL(DISPLAY_URL);
	setDefaultDisplayUrl(DISPLAY_URL);
	registerChildren();
  }
  
  ////////////////////////////////////////////////////////////////////////////////
  // Child manipulation methods
  ////////////////////////////////////////////////////////////////////////////////
  
  /**
   * createChild() This method covers creating form elements specific to NS.
   * The method calls createChild() of the parent base view bean to load the
   * base elements common to the page<br>
   * Date: 11/28/2006
   * 
   * @param name
   *            This is the name of the child form element to instantiate.
   * @return Name of child view if there is a child associated with the container 
   * 
   */
  
  protected View createChild(String name)
  {
	View childFromParent = super.createChild(name);
	
	if (childFromParent != null) {
	  return childFromParent;
	}
	else if (name.equals(CHILD_CBDATETOAPPEARMONTH_PE))
	{
	  ComboBox child =
		new ComboBox(this, getDefaultModel(),
			CHILD_CBDATETOAPPEARMONTH_PE,
			CHILD_CBDATETOAPPEARMONTH_PE,
			CHILD_CBDATETOAPPEARMONTH_RESET_VALUE_PE, null);
	  child.setLabelForNoneSelected("");
	  child.setOptions(cbMonthOptions);
	  
	  return child;
	}
	else if (name.equals(CHILD_TXDATETOAPPEARDAY_PE))
	{
	  TextField child =
		new TextField(this, getDefaultModel(),
			CHILD_TXDATETOAPPEARDAY_PE,
			CHILD_TXDATETOAPPEARDAY_PE,
			CHILD_TXDATETOAPPEARDAY_RESET_VALUE_PE, null);
	  
	  return child;
	}
	else	if (name.equals(CHILD_TXDATETOAPPEARYEAR_PE)) {
	  TextField child =
		new TextField(this, getDefaultModel(),
			CHILD_TXDATETOAPPEARYEAR_PE,
			CHILD_TXDATETOAPPEARYEAR_PE,
			CHILD_TXDATETOAPPEARYEAR_RESET_VALUE_PE, null);
	  
	  return child;
	}
	else  if (name.equals(CHILD_TXACTPERCVAR)) {
	  TextField child =
		new TextField(this, getDefaultModel(),
			CHILD_TXACTPERCVAR,
			CHILD_TXACTPERCVAR,
			CHILD_TXACTPERCVAR_RESET_VALUE, null);
	  
	  return child;
	}
	else	if (name.equals(CHILD_TXCHARGES)) {
	  TextField child =
		new TextField(this, getDefaultModel(),
			CHILD_TXCHARGES,
			CHILD_TXCHARGES,
			CHILD_TXCHARGES_RESET_VALUE, null);
	  
	  return child;
	}
	else
	  if (name.equals(CHILD_TXTERMLOANVAR)) {
		TextField child =
		  new TextField(this, getDefaultModel(),
			  CHILD_TXTERMLOANVAR,
			  CHILD_TXTERMLOANVAR,
			  CHILD_TXTERMLOANVAR_RESET_VALUE, null);
		
		return child;
	  }
	  else	if (name.equals(CHILD_TXTERMSCONDS)) {
		TextField child =
		  new TextField(this, getDefaultModel(),
			  CHILD_TXTERMSCONDS,
			  CHILD_TXTERMSCONDS,
			  CHILD_TXTERMSCONDS_RESET_VALUE, null);
		
		return child;
	  }
	  else
		throw new IllegalArgumentException("Invalid child name [" + name + "]");
  }
  
  /**
   * resetChildren()
   * This method covers reseting form elements specific to NS
   * The method calls resetChildren() from the parent base view bean
   * to reset the base elements common to the page
   * Date: 11/28/2006
   *
   */
  public void resetChildren()
  {
	super.resetChildren();
	
	getCbDateToAppearMonthPE().setValue(CHILD_CBDATETOAPPEARMONTH_RESET_VALUE_PE);
	getTxDateToAppearDayPE().setValue(CHILD_TXDATETOAPPEARDAY_RESET_VALUE_PE);
	getTxDateToAppearYearPE().setValue(CHILD_TXDATETOAPPEARYEAR_RESET_VALUE_PE);
	getTxActPercentVar().setValue(CHILD_TXACTPERCVAR_RESET_VALUE);
	getTxCharges().setValue(CHILD_TXCHARGES_RESET_VALUE);
	getTxTermLoanVar().setValue(CHILD_TXTERMLOANVAR_RESET_VALUE);
	getTxTermsConds().setValue(CHILD_TXTERMSCONDS_RESET_VALUE);
	
  }
  
  /**
   * registerChildren()
   * This method covers regestering form elements specific to NS
   * The method calls registerChildren() from the parent base view bean
   * to register the base elements common to the page 
   * Date: 11/28/2006
   */

  protected void registerChildren()
  {
	super.registerChildren();
	
	registerChild(CHILD_CBDATETOAPPEARMONTH_PE,ComboBox.class);
	registerChild(CHILD_TXDATETOAPPEARDAY_PE,TextField.class);
	registerChild(CHILD_TXDATETOAPPEARYEAR_PE,TextField.class);
	registerChild(CHILD_TXACTPERCVAR,TextField.class);
	registerChild(CHILD_TXCHARGES,TextField.class);
	registerChild(CHILD_TXTERMLOANVAR,TextField.class);
	registerChild(CHILD_TXTERMSCONDS,TextField.class);
	
  }
  
  /**
   * beginDisplay()
   * This method renders the jsp
   * Date: 11/28/2006
   * @param event DisplayEvent
   */  
  public void beginDisplay(DisplayEvent event) throws ModelControlException
  {
	
	// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
	// Ensure the primary model is non-null; if null, it would cause havoc
	// with the various methods dependent on the primary model
	if (getdoDisclosureSelectModel() == null)throw new ModelControlException("Primary model is null");
	
	DisclosureHandler handler = (DisclosureHandler)this.handler.cloneSS();
	handler.pageGetState(this.getParentViewBean());
	
	// populate the cbYesNoOptions
	String yesStr = BXResources.getGenericMsg("YES_LABEL",
		handler.getTheSessionState().
		getLanguageId());
	String noStr = BXResources.getGenericMsg("NO_LABEL",
		handler.getTheSessionState().
		getLanguageId());
	
	cbYesNoOptions.setOptions(new String[] {noStr, yesStr},
		new String[] {"N", "Y"});
	
	// populate the months
	cbMonthOptions.populate(getRequestContext());
	
	handler.overridePageLabel(62);
	
	super.beginDisplay(event);
  }
 
  /**
   * getCbDateToAppearMonthPE() - getter for CHILD_CBDATETOAPPEARMONTH_PE
   * 
   * Date: 11/28/2006
   * @return ComboBox form field
   */
  public ComboBox getCbDateToAppearMonthPE() {
	return (ComboBox) getChild(CHILD_CBDATETOAPPEARMONTH_PE);
  }
  
  /**
   * getTxDateToAppearDayPE() - getter for CHILD_TXDATETOAPPEARDAY_PE
   * 
   * Date: 11/28/2006
   * @return TextField form field
   */  
  public TextField getTxDateToAppearDayPE()
  {
	return (TextField)getChild(CHILD_TXDATETOAPPEARDAY_PE);
  }
  
  /**
   * getTxDateToAppearYearPE() - getter for CHILD_TXDATETOAPPEARYEAR_PE
   * 
   * Date: 11/28/2006
   * @return TextField form field
   */    
  public TextField getTxDateToAppearYearPE()
  {
	return (TextField)getChild(CHILD_TXDATETOAPPEARYEAR_PE);
  }
  
  /**
   * getTxTermLoanVar() - getter for CHILD_TXTERMLOANVAR
   * 
   * Date: 11/28/2006
   * @return TextField form field
   */     
  public TextField getTxTermLoanVar()
  {
	return (TextField)getChild(CHILD_TXTERMLOANVAR);
  }
  
  /**
   * getTxActPercentVar() - getter for CHILD_TXACTPERCVAR
   * 
   * Date: 11/28/2006
   * @return TextField form field
   */   
  public TextField getTxActPercentVar()
  {
	return (TextField)getChild(CHILD_TXACTPERCVAR);
  }
  
  /**
   * getTxTermsConds() - getter for CHILD_TXTERMSCONDS
   * 
   * Date: 11/28/2006
   * @return TextField form field
   */    
  public TextField getTxTermsConds()
  {
	return (TextField)getChild(CHILD_TXTERMSCONDS);
  }
  
  /**
   * getTxCharges() - getter for CHILD_TXCHARGES
   * 
   * Date: 11/28/2006
   * @return TextField form field
   */   
  public TextField getTxCharges()
  {
	return (TextField)getChild(CHILD_TXCHARGES);
  }
  
  
  /**
   *
   * <p>Title: CbBureauPulledMonthOptionList</p>
   *
   * <p>Description: Inner class that represents the options in the month CBs on this page</p>
   *
   * <p>Copyright: </p>
   *
   * <p>Company: </p>
   *
   * @author not attributable
   * @version 1.0
   */
  static class CbMonthOptionList extends OptionList
  {
	CbMonthOptionList() {
	  
	}
	
	
	/**
	 * populate the option list
	 * @param rc RequestContext
	 */
	public void populate(RequestContext rc) {
	  try {
		//Get Language from SessionState
		//Get the Language ID from the SessionStateModel
		String defaultInstanceStateName =
		  rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
		SessionStateModelImpl theSessionState =
		  (SessionStateModelImpl) rc.getModelManager().getModel(
			  SessionStateModel.class,
			  defaultInstanceStateName,
			  true);
		
		int languageId = theSessionState.getLanguageId();
		
		//Get IDs and Labels from BXResources
		Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),"MONTHS", languageId);
		Iterator l = c.iterator();
		String[] theVal = new String[2];
		
		// remove all values first
		clear();
		
		while (l.hasNext()) {
		  theVal = (String[]) (l.next());
		  add(theVal[1], theVal[0]);
		}
	  }
	  catch (Exception ex) {
		ex.printStackTrace();
	  }
	}
  }
  
  /**
   * beforeModelExecutes <br>
   * This method executes any application logic required before rendering the jsp <br>
   * Creation Date: 11/28/2006
   * @Param model
   *            The model that is about to be auto-executed
   * @Param executionContext
   *            The context under which this model is being executed
   * @return True if the execution of the model should proceed, false to skip
   *         execution of the model
   */
  public boolean beforeModelExecutes(Model model, int executionContext)
  {
	DisclosureHandler handler = this.handler.cloneSS();
	handler.pageGetState(this);
	handler.setupBeforePageGeneration();
	handler.pageSaveState();
	return super.beforeModelExecutes(model, executionContext);
  }

  /**
   * afterModelExecutes <br>
   * This method executes any application logic required after rendering the
   * jsp <br>
   * Creation Date: 11/28/2006
   * @Param model
   *            The model that is about to be auto-executed
   * @Param executionContext
   *            The context under which this model is being executed
   */
  public void afterModelExecutes(Model model, int executionContext)
  {
	super.afterModelExecutes(model, executionContext);
  }

  /**
   * afterModelExecutes <br>
   * This method executes after all models have finished execution <br>
   * Creation Date: 11/28/2006
   * @Param executionContext
   *            The context under which this model is being executed
   */
  public void afterAllModelsExecute(int executionContext)
  {
	DisclosureHandler handler = this.handler.cloneSS();
	handler.pageGetState(this);
	handler.populatePageDisplayFields();
	handler.pageSaveState();
	super.afterAllModelsExecute(executionContext);
  }

  /**
   * onModelError <br>
   * Invoked as notification that a given model threw a <code>
   * ModelControlException</code>
   * during auto-execution.<br>
   * Creation Date: 11/28/2006
   * @Param model
   *            The model that which caused an execution error
   * @Param executionContext
   *            The context under which this model is being executed
   * @param exception
   *            The exception thrown during auto-execution
   */
  public void onModelError(Model model, int executionContext,
	  ModelControlException exception) throws ModelControlException
  {
	super.onModelError(model, executionContext, exception);
  }

  /**
   * handleBtProceedRequest <br>
   * This method handles the go button click <br>
   * Creation Date: 11/28/2006
   * @Param event
   *            The RequestInvocationEvent.
   */
  public void handleBtProceedRequest(RequestInvocationEvent event)
	  throws ServletException, IOException
  {
	DisclosureHandler handler = this.handler.cloneSS();
	handler.preHandlerProtocol(this);
	handler.handleGoPage();
	handler.postHandlerProtocol();
  }

  /**
   * getDisplayURL <br>
   * This method overrides the getDefaultURL() framework JATO method and it is
   * located in each ViewBean. This allows not to stay with the JATO ViewBean
   * extension, otherwise each ViewBean a.k.a page should extend its Handler
   * (to be called by the framework) and it is not good. DEFAULT_DISPLAY_URL
   * String is a variable now. The full method is still in PHC base class. It
   * should care the the url="/" case (non-initialized defaultURL) by the BX
   * framework methods. <br>
   * Creation Date: 11/28/2006
   * @return Url for the jsp which is displayed
   */
  public String getDisplayURL()
  {
	DisclosureHandler handler = (DisclosureHandler) this.handler.cloneSS();
	handler.pageGetState(this);
	String url = getDefaultDisplayURL();
	int languageId = handler.theSessionState.getLanguageId();
	if (url != null && !url.trim().equals("") && !url.trim().equals("/"))
	{
	  url = BXResources.getBXUrl(url, languageId);
	}
	else
	{
	  url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
	}
	return url;
  }

  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////
  
  /**
   *the value has the page tag on the top of the jsp page
   *@value "pgDisclosurePEI"
   */  
  public static final String PAGE_NAME = "pgDisclosurePEI";
  
  // populated in beginDisplay()
  /**
   *holds the options yes no
   */  

  private OptionList cbYesNoOptions = new OptionList();
  
  /**
   *cbMonthOptions - holds the months options
   */
  private OptionList cbMonthOptions = new CbMonthOptionList();
  /**
   * Variable to hold the value of the month of disclosure
   * @value "106Q010A_M"
   */    
  public static final String CHILD_CBDATETOAPPEARMONTH_PE = "106Q010A_M";
  public static final String CHILD_CBDATETOAPPEARMONTH_RESET_VALUE_PE = "";        
  /**
   * Variable to hold the value of the month of disclosure
   * @value "106Q010A_D"
   */
  public static final String CHILD_TXDATETOAPPEARDAY_PE = "106Q010A_D";
  public static final String CHILD_TXDATETOAPPEARDAY_RESET_VALUE_PE = "";
  /**
   * Variable to hold the value of the month of disclosure
   * @value "106Q010A_Y"
   */
  public static final String CHILD_TXDATETOAPPEARYEAR_PE = "106Q010A_Y";
  public static final String CHILD_TXDATETOAPPEARYEAR_RESET_VALUE_PE = "";
  /**
   * Variable to hold the value of the month of disclosure
   * @value "106Q080V"
   */
  public static final String CHILD_TXTERMLOANVAR = "106Q080V";
  public static final String CHILD_TXTERMLOANVAR_RESET_VALUE = "";
  /**
   * Variable to hold the value of the month of disclosure
   * @value "106Q10AB"
   */
  public static final String CHILD_TXACTPERCVAR = "106Q10AB";
  public static final String CHILD_TXACTPERCVAR_RESET_VALUE = "";
  /**
   * Variable to hold the value of the month of disclosure
   * @value "106Q11AC"
   */
  public static final String CHILD_TXTERMSCONDS = "106Q11AC";
  public static final String CHILD_TXTERMSCONDS_RESET_VALUE = "";
  /**
   * Variable to hold the value of the month of disclosure
   * @value "106Q12AD"
   */
  public static final String CHILD_TXCHARGES = "106Q12AD";
  public static final String CHILD_TXCHARGES_RESET_VALUE = "";
  
  
  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////
  
  private String DISPLAY_URL="/mosApp/MosSystem/pgDisclosurePEI.jsp";
}

