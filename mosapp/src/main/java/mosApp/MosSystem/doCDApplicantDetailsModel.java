/**
 * 
 */
package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 * @author NBC Implementation Team
 *
 */
public interface doCDApplicantDetailsModel extends QueryModel, SelectQueryModel {
	
	public String getDfBorrowerFirstName();
	public void setDfBorrowerFirstName(String value);
	public String getDfBorrowerLastName();
	public void setDfBorrowerLastName(String value);
	public java.math.BigDecimal getDfBorrowerId();
	public void setDfBorrowerId(java.math.BigDecimal value);
	public java.math.BigDecimal getDfCopyId();
	public void setDfCopyId(java.math.BigDecimal value);
	public String getDfCreditBureauReport();
	public void setDfCreditBureauReport(String value);
	public java.math.BigDecimal getDfResponseId();
	public void setDfResponseId(java.math.BigDecimal value);
	public java.math.BigDecimal getDfApplicantNumber();
	public void setDfApplicantNumber (java.math.BigDecimal value);
	
	public String getDfApplicantName();
	public void setDfApplicantName(String value);
	public java.sql.Clob getDfCBReport();
	public void setDfCBReport(java.sql.Clob value);

	public static final String FIELD_DFBORROWERFIRSTNAME="dfBorrowerFirstName";
	public static final String FIELD_DFBORROWERLASTNAME="dfBorrowerLastName";
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFCREDITBUREAUREPORT="dfCreditBureauReport";
	public static final String FIELD_DFRESPONSEID="dfResponseId";
	public static final String FIELD_DFAPPLICANTNUMBER="dfApplicantNumber";
	public static final String FIELD_DFAPPLICANTNAME="dfApplicantName";
	public static final String FIELD_DFCBREPORT="dfCbReport";

}
