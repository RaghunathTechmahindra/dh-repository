package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doDetailViewCBLiabilitiesModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLiabilityType();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabilityType(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLiabilityDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabilityDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLiabilityAmount();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabilityAmount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLiabilityMonthlyPayment();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabilityMonthlyPayment(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPercentIncludedInGDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfPercentIncludedInGDS(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPercentIncludedInTDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfPercentIncludedInTDS(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLiabilityPayOffDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabilityPayOffDesc(String value);
	
    public java.math.BigDecimal getDfInstitutionId();
    public void setDfInstitutionId(java.math.BigDecimal id);  
    
    /**
     * 
     */
    public java.math.BigDecimal getDfCreditBureauLiabilityLimit();

    
    /**
     * 
     */
    public void setDfCreditBureauLiabilityLimit(java.math.BigDecimal value);
    
    /**
     * 
     */
    public String getDfCreditBureauIndicator();
    

    /**
     * 
     */
    public void setDfCreditBureauIndicator(String value);
    
    /**
     * 
     */
    public  java.sql.Timestamp getDfMaturityDate();

    /**
     * 
     */
    public void setDfMaturityDate(java.sql.Timestamp value);
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFLIABILITYTYPE="dfLiabilityType";
	public static final String FIELD_DFLIABILITYDESC="dfLiabilityDesc";
	public static final String FIELD_DFLIABILITYAMOUNT="dfLiabilityAmount";
	public static final String FIELD_DFLIABILITYMONTHLYPAYMENT="dfLiabilityMonthlyPayment";
	public static final String FIELD_DFPERCENTINCLUDEDINGDS="dfPercentIncludedInGDS";
	public static final String FIELD_DFPERCENTINCLUDEDINTDS="dfPercentIncludedInTDS";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFLIABILITYPAYOFFDESC="dfLiabilityPayOffDesc";
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
    public static final String FIELD_DFCREDITBUREAULIABILITYLIMIT="dfCreditBureauLiabilityLimit";
    public static final String FIELD_DFCREDITBUREAUINDICATOR="dfCreditBureauIndicator";
    public static final String FIELD_DFMATURITYDATE = "dfMaturityDate";
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

