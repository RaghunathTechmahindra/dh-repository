package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgTransactionHistoryRepeated1TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgTransactionHistoryRepeated1TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doTransactionHistoryModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStTransactionDate().setValue(CHILD_STTRANSACTIONDATE_RESET_VALUE);
		getStField().setValue(CHILD_STFIELD_RESET_VALUE);
		getStContext().setValue(CHILD_STCONTEXT_RESET_VALUE);
		getStPreviousValue().setValue(CHILD_STPREVIOUSVALUE_RESET_VALUE);
		getStCurrentValue().setValue(CHILD_STCURRENTVALUE_RESET_VALUE);
		getStContextSource().setValue(CHILD_STCONTEXTSOURCE_RESET_VALUE);
		getStEntity().setValue(CHILD_STENTITY_RESET_VALUE);
		getStUser().setValue(CHILD_STUSER_RESET_VALUE);
		getStDivId().setValue(CHILD_STDIVID_RESET_VALUE);
		getStLineNumber().setValue(CHILD_STLINENUMBER_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STTRANSACTIONDATE,StaticTextField.class);
		registerChild(CHILD_STFIELD,StaticTextField.class);
		registerChild(CHILD_STCONTEXT,StaticTextField.class);
		registerChild(CHILD_STPREVIOUSVALUE,StaticTextField.class);
		registerChild(CHILD_STCURRENTVALUE,StaticTextField.class);
		registerChild(CHILD_STCONTEXTSOURCE,StaticTextField.class);
		registerChild(CHILD_STENTITY,StaticTextField.class);
		registerChild(CHILD_STUSER,StaticTextField.class);
		registerChild(CHILD_STDIVID,StaticTextField.class);
		registerChild(CHILD_STLINENUMBER,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoTransactionHistoryModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			TransactionHistoryHandler handler =(TransactionHistoryHandler) this.handler.cloneSS();
			handler.pageGetState(this.getParentViewBean());
			handler.populateTransactionHistoryLineNumber(this.getTileIndex());
			handler.pageSaveState();
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STTRANSACTIONDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoTransactionHistoryModel(),
				CHILD_STTRANSACTIONDATE,
				doTransactionHistoryModel.FIELD_DFTRANSACTIONDATE,
				CHILD_STTRANSACTIONDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STFIELD))
		{
			StaticTextField child = new StaticTextField(this,
				getdoTransactionHistoryModel(),
				CHILD_STFIELD,
				doTransactionHistoryModel.FIELD_DFFIELD,
				CHILD_STFIELD_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCONTEXT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoTransactionHistoryModel(),
				CHILD_STCONTEXT,
				doTransactionHistoryModel.FIELD_DFCONTEXT,
				CHILD_STCONTEXT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPREVIOUSVALUE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoTransactionHistoryModel(),
				CHILD_STPREVIOUSVALUE,
				doTransactionHistoryModel.FIELD_DFPREVIOUSVALUE,
				CHILD_STPREVIOUSVALUE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCURRENTVALUE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoTransactionHistoryModel(),
				CHILD_STCURRENTVALUE,
				doTransactionHistoryModel.FIELD_DFCURRENTVALUE,
				CHILD_STCURRENTVALUE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCONTEXTSOURCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoTransactionHistoryModel(),
				CHILD_STCONTEXTSOURCE,
				doTransactionHistoryModel.FIELD_DFCONTEXTSOURCE,
				CHILD_STCONTEXTSOURCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STENTITY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoTransactionHistoryModel(),
				CHILD_STENTITY,
				doTransactionHistoryModel.FIELD_DFENTITY,
				CHILD_STENTITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUSER))
		{
			StaticTextField child = new StaticTextField(this,
				getdoTransactionHistoryModel(),
				CHILD_STUSER,
				doTransactionHistoryModel.FIELD_DFUSER,
				CHILD_STUSER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDIVID))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STDIVID,
				CHILD_STDIVID,
				CHILD_STDIVID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLINENUMBER))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STLINENUMBER,
				CHILD_STLINENUMBER,
				CHILD_STLINENUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTransactionDate()
	{
		return (StaticTextField)getChild(CHILD_STTRANSACTIONDATE);
	}


	/**
	 *
	 *
	 */
	public boolean beginStTransactionDateDisplay(ChildDisplayEvent event)
	{
		// The following code block was migrated from the stTransactionDate_onBeforeDisplayEvent method
		TransactionHistoryHandler handler =(TransactionHistoryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.convertToUserTimeZone(this.getParentViewBean(), "Repeated1/stTransactionDate");
		handler.pageSaveState();
    return true;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStField()
	{
		return (StaticTextField)getChild(CHILD_STFIELD);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStContext()
	{
		return (StaticTextField)getChild(CHILD_STCONTEXT);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPreviousValue()
	{
		return (StaticTextField)getChild(CHILD_STPREVIOUSVALUE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCurrentValue()
	{
		return (StaticTextField)getChild(CHILD_STCURRENTVALUE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStContextSource()
	{
		return (StaticTextField)getChild(CHILD_STCONTEXTSOURCE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEntity()
	{
		return (StaticTextField)getChild(CHILD_STENTITY);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUser()
	{
		return (StaticTextField)getChild(CHILD_STUSER);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDivId()
	{
		return (StaticTextField)getChild(CHILD_STDIVID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLineNumber()
	{
		return (StaticTextField)getChild(CHILD_STLINENUMBER);
	}


	/**
	 *
	 *
	 */
	public doTransactionHistoryModel getdoTransactionHistoryModel()
	{
		if (doTransactionHistory == null)
			doTransactionHistory = (doTransactionHistoryModel) getModel(doTransactionHistoryModel.class);
		return doTransactionHistory;
	}


	/**
	 *
	 *
	 */
	public void setdoTransactionHistoryModel(doTransactionHistoryModel model)
	{
			doTransactionHistory = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STTRANSACTIONDATE="stTransactionDate";
	public static final String CHILD_STTRANSACTIONDATE_RESET_VALUE="";
	public static final String CHILD_STFIELD="stField";
	public static final String CHILD_STFIELD_RESET_VALUE="";
	public static final String CHILD_STCONTEXT="stContext";
	public static final String CHILD_STCONTEXT_RESET_VALUE="";
	public static final String CHILD_STPREVIOUSVALUE="stPreviousValue";
	public static final String CHILD_STPREVIOUSVALUE_RESET_VALUE="";
	public static final String CHILD_STCURRENTVALUE="stCurrentValue";
	public static final String CHILD_STCURRENTVALUE_RESET_VALUE="";
	public static final String CHILD_STCONTEXTSOURCE="stContextSource";
	public static final String CHILD_STCONTEXTSOURCE_RESET_VALUE="";
	public static final String CHILD_STENTITY="stEntity";
	public static final String CHILD_STENTITY_RESET_VALUE="";
	public static final String CHILD_STUSER="stUser";
	public static final String CHILD_STUSER_RESET_VALUE="";
	public static final String CHILD_STDIVID="stDivId";
	public static final String CHILD_STDIVID_RESET_VALUE="";
	public static final String CHILD_STLINENUMBER="stLineNumber";
	public static final String CHILD_STLINENUMBER_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doTransactionHistoryModel doTransactionHistory=null;
  private TransactionHistoryHandler handler=new TransactionHistoryHandler();

}

