package mosApp.MosSystem;


import java.util.Hashtable;

import com.basis100.deal.entity.DocumentTracking;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.view.ViewBean;

import MosSystem.Sc;



/**
 *
 *
 */
public class ConditionDetailHandler extends PageHandlerCommon
	implements Cloneable, Sc
{
	/**
	 *
	 *
	 */
	public ConditionDetailHandler cloneSS()
	{
		return(ConditionDetailHandler) super.cloneSafeShallow();

	}


	//This method is used to populate the fields in the header
	//with the appropriate information
	public void populatePageDisplayFields()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		populatePageShellDisplayFields();
		populateTaskNavigator(pg);
		populatePreviousPagesLinks();
	}


	//
	// Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
	//
	public void setupBeforePageGeneration()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		if (pg.getSetupBeforeGenerationCalled() == true) return;

		pg.setSetupBeforeGenerationCalled(true);
		int numRows = 0;

		try
		{
			ViewBean currNDPage = getCurrNDPage();
			PageCursorInfo tds = getPageCursorInfo(pg, "Repeated1");
			Hashtable pst = pg.getPageStateTable();
			String dfDocTrackId =(String) pst.get("dfDocTrackId");
			if (pst == null)
			{
				logger.trace("------NO PST!");
			}
			int copyId = pg.getPageDealCID();
// CIBC DTV			
//			doConditionDetailModelImpl conDo =(doConditionDetailModelImpl)
//          (RequestManager.getRequestContext().getModelManager().getModel(doConditionDetailModel.class));
			doConditionDetailModel conDo = (doConditionDetailModel)
	          (RequestManager.getRequestContext().getModelManager().getModel(doConditionDetailModel.class));
			conDo.clearUserWhereCriteria();
			conDo.addUserWhereCriterion("dfDocTrackId", "=", new String(dfDocTrackId));
			conDo.addUserWhereCriterion("dfCopyId", "=", new String("" + copyId));
			conDo.executeSelect(null);
			
			String isGenerateOnTheFly = PropertiesCache.getInstance().
						getInstanceProperty("com.basis100.conditions.dtverbiage.generateonthefly", "N");
			if(isGenerateOnTheFly != null && "Y".equals(isGenerateOnTheFly)) {
				
				DocumentTracking dt = new DocumentTracking(
						this.srk, Integer.parseInt(dfDocTrackId), copyId);
				while(conDo.next()){
					int dflanguageId = Integer.parseInt(conDo.getDfLanguage());
					if(conDo.getDfDocumentLabel() == null){
						conDo.setDfDocumentLabel(
								dt.generateDocumentLabelOnTheFly(dflanguageId));
						conDo.setDfDocumentText(
								dt.generateDocumentTextVerbiageOnTheFly(dflanguageId));
					}
					conDo.setDfLanguage(BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(),
							"LANGUAGEPREFERENCE", dflanguageId,
							theSessionState.getLanguageId()));
				}
				conDo.beforeFirst();
				
			}
			
			numRows = conDo.getSize();
			if (tds == null)
			{
				String voName = "Repeated1";
				tds = new PageCursorInfo("Repeated1", voName, numRows, numRows);
				tds.setRefresh(true);
				pst.put(tds.getName(), tds);
			}
			else
			{
				tds.setTotalRows(numRows);
				tds.setRowsPerPage(numRows);
			}
		}
		catch(Exception e)
		{
			logger.error("ConditionReview@Details@setupBeforePageGeneration()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}

	}


	/**
	 *
	 *
	 */
	public void handleSubmit()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();

		try
		{
			navigateToNextPage();
		}
		catch(Exception e)
		{
			logger.error("ConditionReview@Details@handleSubmit()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}

	}

}

