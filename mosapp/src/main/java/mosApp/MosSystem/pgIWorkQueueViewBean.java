package mosApp.MosSystem;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.ServletException;
import mosApp.SQLConnectionManagerImpl;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.view.CommandFieldDescriptor;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.ViewBeanBase;
import com.iplanet.jato.view.WebActions;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.ChildDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.JspChildDisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HREF;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

import com.filogix.express.web.jato.ExpressViewBeanBase;

//// Original (iMT) inheritance.
public class pgIWorkQueueViewBean extends ExpressViewBeanBase
// // Highly discussable implementation with the XXXXHandler extension of the
// // ViewBeanBase JATO class. The sequence diagrams must be verified. On the
// other hand,
// // This implementation allows to use the basic JATO framework elements such
// as
// // RequestContext() class. Theoretically, it could allow to override the
// JATO/iMT
// // auto-migration. Temporarily these fragments are commented out now for the
// compability
// // with the stand alone MWHC<--PHC<--XXXHandlerName Utility hierarchy.
// // getModel(ClassName.class), etc., could be also rewritten in the Utility
// hierarchy
// // using this inheritance.
// //public class pgIWorkQueueViewBean extends iWorkQueueHandler
// // implements ViewBean, Mc, Sc
{
    // //////////////////////////////////////////////////////////////////////////
    // Class variables
    // //////////////////////////////////////////////////////////////////////////
    // //////////////////////////////////////////////////////////////////////////////
    // Class variables
    // //////////////////////////////////////////////////////////////////////////////
    public static final String PAGE_NAME = "pgIWorkQueue";

    // // It is a variable now (see explanation in the getDisplayURL() method
    // above.
    // //public static final String
    // DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgIWorkQueue.jsp";
    public static Map FIELD_DESCRIPTORS;

    public static final String CHILD_REPEATED1 = "Repeated1";

    public static final String CHILD_REPEATED2 = "Repeated2";

    public static final String CHILD_ROWSDISPLAYED = "rowsDisplayed";

    public static final String CHILD_ROWSDISPLAYED_RESET_VALUE = "";

    public static final String CHILD_STUSERNAMETITLE = "stUserNameTitle";

    public static final String CHILD_STUSERNAMETITLE_RESET_VALUE = "";

    public static final String CHILD_STTODAYDATE = "stTodayDate";

    public static final String CHILD_STTODAYDATE_RESET_VALUE = "";

    public static final String CHILD_STCOMPANYNAME = "stCompanyName";

    public static final String CHILD_STCOMPANYNAME_RESET_VALUE = "";

    public static final String CHILD_STPAGELABEL = "stPageLabel";

    public static final String CHILD_STPAGELABEL_RESET_VALUE = "";

    public static final String CHILD_CBPAGENAMES = "cbPageNames";

    public static final String CHILD_CBPAGENAMES_RESET_VALUE = "";

    // ==================================================================
    public static final String CHILD_BTPROCEED = "btProceed";

    public static final String CHILD_BTPROCEED_RESET_VALUE = "Proceed";

    public static final String CHILD_STESTCLOSINGDATE = "stEstClosingDate";

    public static final String CHILD_STESTCLOSINGDATE_RESET_VALUE = "";

    public static final String CHILD_STPMTTERM = "stPmtTerm";

    public static final String CHILD_STPMTTERM_RESET_VALUE = "";

    public static final String CHILD_STPURCHASEPRICE = "stPurchasePrice";

    public static final String CHILD_STPURCHASEPRICE_RESET_VALUE = "";

    public static final String CHILD_STDEALPURPOSE = "stDealPurpose";

    public static final String CHILD_STDEALPURPOSE_RESET_VALUE = "";

    public static final String CHILD_STDEALTYPE = "stDealType";

    public static final String CHILD_STDEALTYPE_RESET_VALUE = "";

    public static final String CHILD_STLOB = "stLOB";

    public static final String CHILD_STLOB_RESET_VALUE = "";

    public static final String CHILD_STSOURCE = "stSource";

    public static final String CHILD_STSOURCE_RESET_VALUE = "";

    public static final String CHILD_STSOURCEFIRM = "stSourceFirm";

    public static final String CHILD_STSOURCEFIRM_RESET_VALUE = "";

    public static final String CHILD_STDEALSTATUSDATE = "stDealStatusDate";

    public static final String CHILD_STDEALSTATUSDATE_RESET_VALUE = "";

    public static final String CHILD_STDEALSTATUS = "stDealStatus";

    public static final String CHILD_STDEALSTATUS_RESET_VALUE = "";

    public static final String CHILD_STDEALID = "stDealId";

    public static final String CHILD_STDEALID_RESET_VALUE = "";

    public static final String CHILD_HREF1 = "Href1";

    public static final String CHILD_HREF1_RESET_VALUE = "";

    public static final String CHILD_HREF2 = "Href2";

    public static final String CHILD_HREF2_RESET_VALUE = "";

    public static final String CHILD_HREF3 = "Href3";

    public static final String CHILD_HREF3_RESET_VALUE = "";

    public static final String CHILD_HREF4 = "Href4";

    public static final String CHILD_HREF4_RESET_VALUE = "";

    public static final String CHILD_HREF5 = "Href5";

    public static final String CHILD_HREF5_RESET_VALUE = "";

    public static final String CHILD_HREF6 = "Href6";

    public static final String CHILD_HREF6_RESET_VALUE = "";

    public static final String CHILD_HREF7 = "Href7";

    public static final String CHILD_HREF7_RESET_VALUE = "";

    public static final String CHILD_HREF8 = "Href8";

    public static final String CHILD_HREF8_RESET_VALUE = "";

    public static final String CHILD_HREF9 = "Href9";

    public static final String CHILD_HREF9_RESET_VALUE = "";

    public static final String CHILD_HREF10 = "Href10";

    public static final String CHILD_HREF10_RESET_VALUE = "";

    public static final String CHILD_TBDEALID = "tbDealId";

    public static final String CHILD_TBDEALID_RESET_VALUE = "";

    public static final String CHILD_BTBACKWARDBUTTON = "btBackwardButton";

    public static final String CHILD_BTBACKWARDBUTTON_RESET_VALUE = "Previous";

    public static final String CHILD_BTFILTERSUBMITBUTTON = "btFilterSubmitButton";

    public static final String CHILD_BTFILTERSUBMITBUTTON_RESET_VALUE = "Filter Pop-up Submit";

    public static final String CHILD_BTSORTBUTTON = "btSortButton";

    public static final String CHILD_BTSORTBUTTON_RESET_VALUE = "Sort Pop-up Submit";

    public static final String CHILD_CBSORTOPTIONS = "cbSortOptions";

    public static final String CHILD_CBSORTOPTIONS_RESET_VALUE = "";

    protected static OptionList cbSortOptionsOptions = new OptionList(
            new String[] {}, new String[] {});

    public static final String CHILD_CBFILTEROPTIONS = "cbFilterOptions";

    public static final String CHILD_CBFILTEROPTIONS_RESET_VALUE = "";

    // ==================================================================
    public static final String CHILD_BTFILTERSORTRESET = "btFilterSortReset";

    public static final String CHILD_BTFILTERSORTRESET_RESET_VALUE = " Reset Sort/Filter";

    public static final String CHILD_STSORTOPTIONDESC = "stSortOptionDesc";

    public static final String CHILD_STSORTOPTIONDESC_RESET_VALUE = "";

    public static final String CHILD_STFILTEROPTIONDESC = "stFilterOptionDesc";

    public static final String CHILD_STFILTEROPTIONDESC_RESET_VALUE = "";

    public static final String CHILD_CHANGEPASSWORDHREF = "changePasswordHref";

    public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE = "";

    public static final String CHILD_BTTOOLHISTORY = "btToolHistory";

    public static final String CHILD_BTTOOLHISTORY_RESET_VALUE = " ";

    public static final String CHILD_BTTOONOTES = "btTooNotes";

    public static final String CHILD_BTTOONOTES_RESET_VALUE = " ";

    public static final String CHILD_BTTOOLSEARCH = "btToolSearch";

    public static final String CHILD_BTTOOLSEARCH_RESET_VALUE = " ";

    public static final String CHILD_BTTOOLLOG = "btToolLog";

    public static final String CHILD_BTTOOLLOG_RESET_VALUE = " ";

    public static final String CHILD_BTWORKQUEUELINK = "btWorkQueueLink";

    public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE = " ";

    public static final String CHILD_STERRORFLAG = "stErrorFlag";

    public static final String CHILD_STERRORFLAG_RESET_VALUE = "N";

    public static final String CHILD_BTFORWARDBUTTON = "btForwardButton";

    public static final String CHILD_BTFORWARDBUTTON_RESET_VALUE = "Next";

    public static final String CHILD_DETECTALERTTASKS = "detectAlertTasks";

    public static final String CHILD_DETECTALERTTASKS_RESET_VALUE = "";

    public static final String CHILD_DISPLAYPRIORITYTASKS = "displayprioritytasks";

    public static final String CHILD_DISPLAYPRIORITYTASKS_RESET_VALUE = "";

    public static final String CHILD_STTOTALLOANAMOUNT = "stTotalLoanAmount";

    public static final String CHILD_STTOTALLOANAMOUNT_RESET_VALUE = "";

    public static final String CHILD_STBORRFIRSTNAME = "stBorrFirstName";

    public static final String CHILD_STBORRFIRSTNAME_RESET_VALUE = "";

    public static final String CHILD_SESSIONUSERID = "sessionUserId";

    public static final String CHILD_SESSIONUSERID_RESET_VALUE = "";

    public static final String CHILD_STPMGENERATE = "stPmGenerate";

    public static final String CHILD_STPMGENERATE_RESET_VALUE = "";

    public static final String CHILD_STPMHASTITLE = "stPmHasTitle";

    public static final String CHILD_STPMHASTITLE_RESET_VALUE = "";

    public static final String CHILD_STPMHASINFO = "stPmHasInfo";

    public static final String CHILD_STPMHASINFO_RESET_VALUE = "";

    public static final String CHILD_STPMHASTABLE = "stPmHasTable";

    public static final String CHILD_STPMHASTABLE_RESET_VALUE = "";

    public static final String CHILD_STPMHASOK = "stPmHasOk";

    public static final String CHILD_STPMHASOK_RESET_VALUE = "";

    public static final String CHILD_STPMTITLE = "stPmTitle";

    public static final String CHILD_STPMTITLE_RESET_VALUE = "";

    public static final String CHILD_STPMINFOMSG = "stPmInfoMsg";

    public static final String CHILD_STPMINFOMSG_RESET_VALUE = "";

    public static final String CHILD_STPMONOK = "stPmOnOk";

    public static final String CHILD_STPMONOK_RESET_VALUE = "";

    public static final String CHILD_STPMMSGS = "stPmMsgs";

    public static final String CHILD_STPMMSGS_RESET_VALUE = "";

    public static final String CHILD_STPMMSGTYPES = "stPmMsgTypes";

    public static final String CHILD_STPMMSGTYPES_RESET_VALUE = "";

    public static final String CHILD_STAMGENERATE = "stAmGenerate";

    public static final String CHILD_STAMGENERATE_RESET_VALUE = "";

    public static final String CHILD_STAMHASTITLE = "stAmHasTitle";

    public static final String CHILD_STAMHASTITLE_RESET_VALUE = "";

    public static final String CHILD_STAMHASINFO = "stAmHasInfo";

    public static final String CHILD_STAMHASINFO_RESET_VALUE = "";

    public static final String CHILD_STAMHASTABLE = "stAmHasTable";

    public static final String CHILD_STAMHASTABLE_RESET_VALUE = "";

    public static final String CHILD_STAMTITLE = "stAmTitle";

    public static final String CHILD_STAMTITLE_RESET_VALUE = "";

    public static final String CHILD_STAMINFOMSG = "stAmInfoMsg";

    public static final String CHILD_STAMINFOMSG_RESET_VALUE = "";

    public static final String CHILD_STAMMSGS = "stAmMsgs";

    public static final String CHILD_STAMMSGS_RESET_VALUE = "";

    public static final String CHILD_STAMMSGTYPES = "stAmMsgTypes";

    public static final String CHILD_STAMMSGTYPES_RESET_VALUE = "";

    public static final String CHILD_STAMDIALOGMSG = "stAmDialogMsg";

    public static final String CHILD_STAMDIALOGMSG_RESET_VALUE = "";

    public static final String CHILD_STAMBUTTONSHTML = "stAmButtonsHtml";

    public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE = "";

    public static final String CHILD_STSPECIALFEATURE = "stSpecialFeature";

    public static final String CHILD_STSPECIALFEATURE_RESET_VALUE = "";

    public static final String CHILD_RPPIRORITYCOUNT = "rpPirorityCount";

    // --> Hidden ActMessageButton (FINDTHISLATER)
    // --> Test by BILLY 15July2002
    public static final String CHILD_BTACTMSG = "btActMsg";

    public static final String CHILD_BTACTMSG_RESET_VALUE = "ActMsg";

    // --Release2.1--//
    // // New link to toggle the language. When touched this link should reload
    // the
    // // page in opposite language (french versus english) and set this new
    // language
    // // session value throughout all modulus.
    public static final String CHILD_TOGGLELANGUAGEHREF = "toggleLanguageHref";

    public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE = "";

    public static final String CHILD_CBINSTITUTION = "cbInstitution";

    public static final String CHILD_CBINSTITUTION_RESET_VALUE = "";

    // --Release2.1--//
    // The Option list should not be Static, otherwise this will screw up
    // --> By Billy 05Nov2002
    // private static CbPageNamesOptionList cbPageNamesOptions=new
    // CbPageNamesOptionList();
    private CbPageNamesOptionList cbPageNamesOptions = new CbPageNamesOptionList();

    private CbInstitutionOptions cbInstitutionOptions = new CbInstitutionOptions();

    // Added the Variable for NonSelected Label
    private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";

    // --Release2.1--//
    // The Option list should not be Static, otherwise this will screw up
    // --> By Billy 05Nov2002
    // protected static CbFilterOptionsOptionList cbFilterOptionsOptions=new
    // CbFilterOptionsOptionList();
    protected CbFilterOptionsOptionList cbFilterOptionsOptions = new CbFilterOptionsOptionList();

    // //////////////////////////////////////////////////////////////////////////
    // Instance variables
    // //////////////////////////////////////////////////////////////////////////
    private doDealSummarySnapShotModel doDealSummarySnapShot = null;

    protected String DEFAULT_DISPLAY_URL = "/mosApp/MosSystem/pgIWorkQueue.jsp";

    // //////////////////////////////////////////////////////////////////////////
    // Custom Members - Require Manual Migration
    // //////////////////////////////////////////////////////////////////////////
    // MigrationToDo : Migrate custom member
    // private iWorkQueueHandler handler=new iWorkQueueHandler();
    private iWorkQueueHandler handler = new iWorkQueueHandler();

    public SysLogger logger;

    /**
     * 
     * 
     */
    public pgIWorkQueueViewBean() {
        super(PAGE_NAME);
        setDefaultDisplayURL(DEFAULT_DISPLAY_URL);
        registerChildren();

        initialize();
    }

    /**
     * 
     * 
     */
    protected void initialize() {
    }

    // //////////////////////////////////////////////////////////////////////////////
    // Child manipulation methods
    // //////////////////////////////////////////////////////////////////////////////

    /**
     * 
     * 
     */
    protected View createChild(String name) {

        View superReturn = super.createChild(name);
        if (superReturn != null) {
            return superReturn;
        } else if (name.equals(CHILD_REPEATED1)) {

            pgIWorkQueueRepeated1TiledView child = new pgIWorkQueueRepeated1TiledView(
                    this, CHILD_REPEATED1);

            return child;
        } else if (name.equals(CHILD_REPEATED2)) {
            pgIWorkQueueRepeated2TiledView child = new pgIWorkQueueRepeated2TiledView(
                    this, CHILD_REPEATED2);

            return child;
        } else if (name.equals(CHILD_ROWSDISPLAYED)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_ROWSDISPLAYED,
                    CHILD_ROWSDISPLAYED, CHILD_ROWSDISPLAYED_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STUSERNAMETITLE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STUSERNAMETITLE,
                    CHILD_STUSERNAMETITLE, CHILD_STUSERNAMETITLE_RESET_VALUE,
                    null);

            return child;
        } else if (name.equals(CHILD_STTODAYDATE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STTODAYDATE, CHILD_STTODAYDATE,
                    CHILD_STTODAYDATE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STCOMPANYNAME)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STCOMPANYNAME,
                    CHILD_STCOMPANYNAME, CHILD_STCOMPANYNAME_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPAGELABEL)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPAGELABEL, CHILD_STPAGELABEL,
                    CHILD_STPAGELABEL_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBPAGENAMES)) {
            ComboBox child = new ComboBox(this, getDefaultModel(),
                    CHILD_CBPAGENAMES, CHILD_CBPAGENAMES,
                    CHILD_CBPAGENAMES_RESET_VALUE, null);

            // --Release2.1--//
            // Modified to set NonSelected Label from
            // CHILD_CBPAGENAMES_NONSELECTED_LABEL
            // --> By Billy 05Nov2002
            child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);

            // ==========================================================================
            child.setOptions(cbPageNamesOptions);

            return child;
        } else if (name.equals(CHILD_BTPROCEED)) {
            Button child = new Button(this, getDefaultModel(), CHILD_BTPROCEED,
                    CHILD_BTPROCEED, CHILD_BTPROCEED_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STESTCLOSINGDATE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoDealSummarySnapShotModel(), CHILD_STESTCLOSINGDATE,
                    doDealSummarySnapShotModel.FIELD_DFESTCLOSINGDATE,
                    CHILD_STESTCLOSINGDATE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMTTERM)) {
            StaticTextField child = new StaticTextField(this,
                    getdoDealSummarySnapShotModel(), CHILD_STPMTTERM,
                    doDealSummarySnapShotModel.FIELD_DFPAYMENTTERMDESCR,
                    CHILD_STPMTTERM_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPURCHASEPRICE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoDealSummarySnapShotModel(), CHILD_STPURCHASEPRICE,
                    doDealSummarySnapShotModel.FIELD_DFTOTALPURCHASEPRICE,
                    CHILD_STPURCHASEPRICE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STDEALPURPOSE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoDealSummarySnapShotModel(), CHILD_STDEALPURPOSE,
                    doDealSummarySnapShotModel.FIELD_DFLOANPURPOSEDESCR,
                    CHILD_STDEALPURPOSE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STDEALTYPE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoDealSummarySnapShotModel(), CHILD_STDEALTYPE,
                    doDealSummarySnapShotModel.FIELD_DFDEALTYPEDESCR,
                    CHILD_STDEALTYPE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STLOB)) {
            StaticTextField child = new StaticTextField(this,
                    getdoDealSummarySnapShotModel(), CHILD_STLOB,
                    doDealSummarySnapShotModel.FIELD_DFLOBDESCR,
                    CHILD_STLOB_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STSOURCE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoDealSummarySnapShotModel(), CHILD_STSOURCE,
                    doDealSummarySnapShotModel.FIELD_DFSOBPSHORTNAME,
                    CHILD_STSOURCE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STSOURCEFIRM)) {
            StaticTextField child = new StaticTextField(this,
                    getdoDealSummarySnapShotModel(), CHILD_STSOURCEFIRM,
                    doDealSummarySnapShotModel.FIELD_DFSFSHORTNAME,
                    CHILD_STSOURCEFIRM_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STDEALSTATUSDATE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoDealSummarySnapShotModel(), CHILD_STDEALSTATUSDATE,
                    doDealSummarySnapShotModel.FIELD_DFSTATUSDATE,
                    CHILD_STDEALSTATUSDATE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STDEALSTATUS)) {
            StaticTextField child = new StaticTextField(this,
                    getdoDealSummarySnapShotModel(), CHILD_STDEALSTATUS,
                    doDealSummarySnapShotModel.FIELD_DFSTATUSDESCR,
                    CHILD_STDEALSTATUS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STDEALID)) {
            StaticTextField child = new StaticTextField(this,
                    getdoDealSummarySnapShotModel(), CHILD_STDEALID,
                    doDealSummarySnapShotModel.FIELD_DFAPPLICATIONID,
                    CHILD_STDEALID_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF1)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF1,
                    CHILD_HREF1, CHILD_HREF1_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF2)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF2,
                    CHILD_HREF2, CHILD_HREF2_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF3)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF3,
                    CHILD_HREF3, CHILD_HREF3_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF4)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF4,
                    CHILD_HREF4, CHILD_HREF4_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF5)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF5,
                    CHILD_HREF5, CHILD_HREF5_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF6)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF6,
                    CHILD_HREF6, CHILD_HREF6_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF7)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF7,
                    CHILD_HREF7, CHILD_HREF7_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF8)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF8,
                    CHILD_HREF8, CHILD_HREF8_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF9)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF9,
                    CHILD_HREF9, CHILD_HREF9_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF10)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF10,
                    CHILD_HREF10, CHILD_HREF10_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TBDEALID)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TBDEALID, CHILD_TBDEALID, CHILD_TBDEALID_RESET_VALUE,
                    null);

            return child;
        } else if (name.equals(CHILD_BTBACKWARDBUTTON)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTBACKWARDBUTTON, CHILD_BTBACKWARDBUTTON,
                    CHILD_BTBACKWARDBUTTON_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTFILTERSUBMITBUTTON)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTFILTERSUBMITBUTTON, CHILD_BTFILTERSUBMITBUTTON,
                    CHILD_BTFILTERSUBMITBUTTON_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTSORTBUTTON)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTSORTBUTTON, CHILD_BTSORTBUTTON,
                    CHILD_BTSORTBUTTON_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBSORTOPTIONS)) {
            ComboBox child = new ComboBox(this, getDefaultModel(),
                    CHILD_CBSORTOPTIONS, CHILD_CBSORTOPTIONS,
                    CHILD_CBSORTOPTIONS_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbSortOptionsOptions);

            return child;
        } else if (name.equals(CHILD_CBFILTEROPTIONS)) {
            ComboBox child = new ComboBox(this, getDefaultModel(),
                    CHILD_CBFILTEROPTIONS, CHILD_CBFILTEROPTIONS,
                    CHILD_CBFILTEROPTIONS_RESET_VALUE, null);
            // TODO
            // child.setLabelForNoneSelected("");
            child.setOptions(cbFilterOptionsOptions);

            return child;
        } else if (name.equals(CHILD_BTFILTERSORTRESET)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTFILTERSORTRESET, CHILD_BTFILTERSORTRESET,
                    CHILD_BTFILTERSORTRESET_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STSORTOPTIONDESC)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STSORTOPTIONDESC,
                    CHILD_STSORTOPTIONDESC, CHILD_STSORTOPTIONDESC_RESET_VALUE,
                    null);

            return child;
        } else if (name.equals(CHILD_STFILTEROPTIONDESC)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STFILTEROPTIONDESC,
                    CHILD_STFILTEROPTIONDESC,
                    CHILD_STFILTEROPTIONDESC_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CHANGEPASSWORDHREF)) {
            HREF child = new HREF(this, getDefaultModel(),
                    CHILD_CHANGEPASSWORDHREF, CHILD_CHANGEPASSWORDHREF,
                    CHILD_CHANGEPASSWORDHREF_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTTOOLHISTORY)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTTOOLHISTORY, CHILD_BTTOOLHISTORY,
                    CHILD_BTTOOLHISTORY_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTTOONOTES)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTTOONOTES, CHILD_BTTOONOTES,
                    CHILD_BTTOONOTES_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTTOOLSEARCH)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTTOOLSEARCH, CHILD_BTTOOLSEARCH,
                    CHILD_BTTOOLSEARCH_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTTOOLLOG)) {
            Button child = new Button(this, getDefaultModel(), CHILD_BTTOOLLOG,
                    CHILD_BTTOOLLOG, CHILD_BTTOOLLOG_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTWORKQUEUELINK)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTWORKQUEUELINK, CHILD_BTWORKQUEUELINK,
                    CHILD_BTWORKQUEUELINK_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STERRORFLAG)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STERRORFLAG, CHILD_STERRORFLAG,
                    CHILD_STERRORFLAG_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTFORWARDBUTTON)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTFORWARDBUTTON, CHILD_BTFORWARDBUTTON,
                    CHILD_BTFORWARDBUTTON_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_DETECTALERTTASKS)) {
            HiddenField child = new HiddenField(this, getDefaultModel(),
                    CHILD_DETECTALERTTASKS, CHILD_DETECTALERTTASKS,
                    CHILD_DETECTALERTTASKS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_DISPLAYPRIORITYTASKS)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_DISPLAYPRIORITYTASKS,
                    CHILD_DISPLAYPRIORITYTASKS,
                    CHILD_DISPLAYPRIORITYTASKS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STTOTALLOANAMOUNT)) {
            StaticTextField child = new StaticTextField(this,
                    getdoDealSummarySnapShotModel(), CHILD_STTOTALLOANAMOUNT,
                    doDealSummarySnapShotModel.FIELD_DFTOTALLOANAMT,
                    CHILD_STTOTALLOANAMOUNT_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STBORRFIRSTNAME)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STBORRFIRSTNAME,
                    CHILD_STBORRFIRSTNAME, CHILD_STBORRFIRSTNAME_RESET_VALUE,
                    null);

            return child;
        } else if (name.equals(CHILD_SESSIONUSERID)) {
            HiddenField child = new HiddenField(this, getDefaultModel(),
                    CHILD_SESSIONUSERID, CHILD_SESSIONUSERID,
                    CHILD_SESSIONUSERID_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMGENERATE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMGENERATE, CHILD_STPMGENERATE,
                    CHILD_STPMGENERATE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMHASTITLE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMHASTITLE, CHILD_STPMHASTITLE,
                    CHILD_STPMHASTITLE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMHASINFO)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMHASINFO, CHILD_STPMHASINFO,
                    CHILD_STPMHASINFO_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMHASTABLE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMHASTABLE, CHILD_STPMHASTABLE,
                    CHILD_STPMHASTABLE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMHASOK)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMHASOK, CHILD_STPMHASOK,
                    CHILD_STPMHASOK_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMTITLE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMTITLE, CHILD_STPMTITLE,
                    CHILD_STPMTITLE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMINFOMSG)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMINFOMSG, CHILD_STPMINFOMSG,
                    CHILD_STPMINFOMSG_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMONOK)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMONOK, CHILD_STPMONOK,
                    CHILD_STPMONOK_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMMSGS)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMMSGS, CHILD_STPMMSGS,
                    CHILD_STPMMSGS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMMSGTYPES)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMMSGTYPES, CHILD_STPMMSGTYPES,
                    CHILD_STPMMSGTYPES_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMGENERATE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMGENERATE, CHILD_STAMGENERATE,
                    CHILD_STAMGENERATE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMHASTITLE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMHASTITLE, CHILD_STAMHASTITLE,
                    CHILD_STAMHASTITLE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMHASINFO)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMHASINFO, CHILD_STAMHASINFO,
                    CHILD_STAMHASINFO_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMHASTABLE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMHASTABLE, CHILD_STAMHASTABLE,
                    CHILD_STAMHASTABLE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMTITLE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMTITLE, CHILD_STAMTITLE,
                    CHILD_STAMTITLE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMINFOMSG)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMINFOMSG, CHILD_STAMINFOMSG,
                    CHILD_STAMINFOMSG_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMMSGS)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMMSGS, CHILD_STAMMSGS,
                    CHILD_STAMMSGS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMMSGTYPES)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMMSGTYPES, CHILD_STAMMSGTYPES,
                    CHILD_STAMMSGTYPES_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMDIALOGMSG)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMDIALOGMSG,
                    CHILD_STAMDIALOGMSG, CHILD_STAMDIALOGMSG_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMBUTTONSHTML)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMBUTTONSHTML,
                    CHILD_STAMBUTTONSHTML, CHILD_STAMBUTTONSHTML_RESET_VALUE,
                    null);

            return child;
        } else if (name.equals(CHILD_STSPECIALFEATURE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoDealSummarySnapShotModel(), CHILD_STSPECIALFEATURE,
                    doDealSummarySnapShotModel.FIELD_DFSPECIALFEATURE,
                    CHILD_STSPECIALFEATURE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_RPPIRORITYCOUNT)) {
            pgIWorkQueuerpPirorityCountTiledView child = new pgIWorkQueuerpPirorityCountTiledView(
                    this, CHILD_RPPIRORITYCOUNT);

            return child;
        }

        // --> Hidden ActMessageButton (FINDTHISLATER)
        // --> by BILLY 15July2002
        else if (name.equals(CHILD_BTACTMSG)) {
            Button child = new Button(this, getDefaultModel(), CHILD_BTACTMSG,
                    CHILD_BTACTMSG, CHILD_BTACTMSG_RESET_VALUE,
                    new CommandFieldDescriptor(
                            ActMessageCommand.COMMAND_DESCRIPTOR));

            return child;
        }

        // --Release2.1--//
        // // Link to toggle language. When touched this link should reload the
        // // page in opposite language (french versus english) and set this new
        // session
        // // language throughout the all modulus.
        else if (name.equals(CHILD_TOGGLELANGUAGEHREF)) {
            HREF child = new HREF(this, getDefaultModel(),
                    CHILD_TOGGLELANGUAGEHREF, CHILD_TOGGLELANGUAGEHREF,
                    CHILD_TOGGLELANGUAGEHREF_RESET_VALUE, null);

            return child;
        } 
        else if(name.equals(CHILD_CBINSTITUTION))
        {
            ComboBox child =
                new ComboBox(this, getDefaultModel(), CHILD_CBINSTITUTION, CHILD_CBINSTITUTION,
                        CHILD_CBINSTITUTION_RESET_VALUE, null);
                child.setOptions(cbInstitutionOptions);
                return child;
        }
        else {
            throw new IllegalArgumentException("Invalid child name [" + name
                    + "]");
        }
    }

    /**
     * 
     * 
     */
    public void resetChildren() {

        super.resetChildren();

        getRepeated1().resetChildren();
        getRepeated2().resetChildren();
        getRowsDisplayed().setValue(CHILD_ROWSDISPLAYED_RESET_VALUE);
        getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
        getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
        getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
        getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
        getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
        getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
        getStEstClosingDate().setValue(CHILD_STESTCLOSINGDATE_RESET_VALUE);
        getStPmtTerm().setValue(CHILD_STPMTTERM_RESET_VALUE);
        getStPurchasePrice().setValue(CHILD_STPURCHASEPRICE_RESET_VALUE);
        getStDealPurpose().setValue(CHILD_STDEALPURPOSE_RESET_VALUE);
        getStDealType().setValue(CHILD_STDEALTYPE_RESET_VALUE);
        getStLOB().setValue(CHILD_STLOB_RESET_VALUE);
        getStSource().setValue(CHILD_STSOURCE_RESET_VALUE);
        getStSourceFirm().setValue(CHILD_STSOURCEFIRM_RESET_VALUE);
        getStDealStatusDate().setValue(CHILD_STDEALSTATUSDATE_RESET_VALUE);
        getStDealStatus().setValue(CHILD_STDEALSTATUS_RESET_VALUE);
        getStDealId().setValue(CHILD_STDEALID_RESET_VALUE);
        getHref1().setValue(CHILD_HREF1_RESET_VALUE);
        getHref2().setValue(CHILD_HREF2_RESET_VALUE);
        getHref3().setValue(CHILD_HREF3_RESET_VALUE);
        getHref4().setValue(CHILD_HREF4_RESET_VALUE);
        getHref5().setValue(CHILD_HREF5_RESET_VALUE);
        getHref6().setValue(CHILD_HREF6_RESET_VALUE);
        getHref7().setValue(CHILD_HREF7_RESET_VALUE);
        getHref8().setValue(CHILD_HREF8_RESET_VALUE);
        getHref9().setValue(CHILD_HREF9_RESET_VALUE);
        getHref10().setValue(CHILD_HREF10_RESET_VALUE);
        getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
        getBtBackwardButton().setValue(CHILD_BTBACKWARDBUTTON_RESET_VALUE);
        getBtFilterSubmitButton().setValue(
                CHILD_BTFILTERSUBMITBUTTON_RESET_VALUE);
        getBtSortButton().setValue(CHILD_BTSORTBUTTON_RESET_VALUE);
        getCbSortOptions().setValue(CHILD_CBSORTOPTIONS_RESET_VALUE);
        getCbFilterOptions().setValue(CHILD_CBFILTEROPTIONS_RESET_VALUE);
        getBtFilterSortReset().setValue(CHILD_BTFILTERSORTRESET_RESET_VALUE);
        getStSortOptionDesc().setValue(CHILD_STSORTOPTIONDESC_RESET_VALUE);
        getStFilterOptionDesc().setValue(CHILD_STFILTEROPTIONDESC_RESET_VALUE);
        getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
        getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
        getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
        getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
        getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
        getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
        getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
        getBtForwardButton().setValue(CHILD_BTFORWARDBUTTON_RESET_VALUE);
        getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
        getDisplayprioritytasks().setValue(
                CHILD_DISPLAYPRIORITYTASKS_RESET_VALUE);
        getStTotalLoanAmount().setValue(CHILD_STTOTALLOANAMOUNT_RESET_VALUE);
        getStBorrFirstName().setValue(CHILD_STBORRFIRSTNAME_RESET_VALUE);
        getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
        getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
        getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
        getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
        getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
        getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
        getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
        getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
        getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
        getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
        getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
        getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
        getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
        getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
        getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
        getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
        getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
        getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
        getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
        getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
        getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
        getStSpecialFeature().setValue(CHILD_STSPECIALFEATURE_RESET_VALUE);
        getRpPirorityCount().resetChildren();

        // --> Hidden ActMessageButton (FINDTHISLATER)
        // --> Test by BILLY 15July2002
        getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);

        // --Release2.1--//
        // // New link to toggle the language. When touched this link should
        // reload the
        // // page in opposite language (french versus english) and set this new
        // language
        // // session value throughout all modulus.
        getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);
        getCbInstitution().setValue(CHILD_CBINSTITUTION_RESET_VALUE);
    }

    /**
     * 
     * 
     */
    protected void registerChildren() {

        super.registerChildren();

        registerChild(CHILD_REPEATED1, pgIWorkQueueRepeated1TiledView.class);
        registerChild(CHILD_REPEATED2, pgIWorkQueueRepeated2TiledView.class);
        registerChild(CHILD_ROWSDISPLAYED, StaticTextField.class);
        registerChild(CHILD_STUSERNAMETITLE, StaticTextField.class);
        registerChild(CHILD_STTODAYDATE, StaticTextField.class);
        registerChild(CHILD_STCOMPANYNAME, StaticTextField.class);
        registerChild(CHILD_STPAGELABEL, StaticTextField.class);
        registerChild(CHILD_CBPAGENAMES, ComboBox.class);
        registerChild(CHILD_BTPROCEED, Button.class);
        registerChild(CHILD_STESTCLOSINGDATE, StaticTextField.class);
        registerChild(CHILD_STPMTTERM, StaticTextField.class);
        registerChild(CHILD_STPURCHASEPRICE, StaticTextField.class);
        registerChild(CHILD_STDEALPURPOSE, StaticTextField.class);
        registerChild(CHILD_STDEALTYPE, StaticTextField.class);
        registerChild(CHILD_STLOB, StaticTextField.class);
        registerChild(CHILD_STSOURCE, StaticTextField.class);
        registerChild(CHILD_STSOURCEFIRM, StaticTextField.class);
        registerChild(CHILD_STDEALSTATUSDATE, StaticTextField.class);
        registerChild(CHILD_STDEALSTATUS, StaticTextField.class);
        registerChild(CHILD_STDEALID, StaticTextField.class);
        registerChild(CHILD_HREF1, HREF.class);
        registerChild(CHILD_HREF2, HREF.class);
        registerChild(CHILD_HREF3, HREF.class);
        registerChild(CHILD_HREF4, HREF.class);
        registerChild(CHILD_HREF5, HREF.class);
        registerChild(CHILD_HREF6, HREF.class);
        registerChild(CHILD_HREF7, HREF.class);
        registerChild(CHILD_HREF8, HREF.class);
        registerChild(CHILD_HREF9, HREF.class);
        registerChild(CHILD_HREF10, HREF.class);
        registerChild(CHILD_TBDEALID, TextField.class);
        registerChild(CHILD_BTBACKWARDBUTTON, Button.class); // // back
                                                                // button
        registerChild(CHILD_BTFILTERSUBMITBUTTON, Button.class);
        registerChild(CHILD_BTSORTBUTTON, Button.class);
        registerChild(CHILD_CBSORTOPTIONS, ComboBox.class);
        registerChild(CHILD_CBFILTEROPTIONS, ComboBox.class);
        registerChild(CHILD_BTFILTERSORTRESET, Button.class);
        registerChild(CHILD_STSORTOPTIONDESC, StaticTextField.class);
        registerChild(CHILD_STFILTEROPTIONDESC, StaticTextField.class);
        registerChild(CHILD_CHANGEPASSWORDHREF, HREF.class);
        registerChild(CHILD_BTTOOLHISTORY, Button.class);
        registerChild(CHILD_BTTOONOTES, Button.class);
        registerChild(CHILD_BTTOOLSEARCH, Button.class);
        registerChild(CHILD_BTTOOLLOG, Button.class);
        registerChild(CHILD_BTWORKQUEUELINK, Button.class);
        registerChild(CHILD_STERRORFLAG, StaticTextField.class);
        registerChild(CHILD_BTFORWARDBUTTON, Button.class); // // forward button
        registerChild(CHILD_DETECTALERTTASKS, HiddenField.class);
        registerChild(CHILD_DISPLAYPRIORITYTASKS, StaticTextField.class);
        registerChild(CHILD_STTOTALLOANAMOUNT, StaticTextField.class);
        registerChild(CHILD_STBORRFIRSTNAME, StaticTextField.class);
        registerChild(CHILD_SESSIONUSERID, HiddenField.class);
        registerChild(CHILD_STPMGENERATE, StaticTextField.class);
        registerChild(CHILD_STPMHASTITLE, StaticTextField.class);
        registerChild(CHILD_STPMHASINFO, StaticTextField.class);
        registerChild(CHILD_STPMHASTABLE, StaticTextField.class);
        registerChild(CHILD_STPMHASOK, StaticTextField.class);
        registerChild(CHILD_STPMTITLE, StaticTextField.class);
        registerChild(CHILD_STPMINFOMSG, StaticTextField.class);
        registerChild(CHILD_STPMONOK, StaticTextField.class);
        registerChild(CHILD_STPMMSGS, StaticTextField.class);
        registerChild(CHILD_STPMMSGTYPES, StaticTextField.class);
        registerChild(CHILD_STAMGENERATE, StaticTextField.class);
        registerChild(CHILD_STAMHASTITLE, StaticTextField.class);
        registerChild(CHILD_STAMHASINFO, StaticTextField.class);
        registerChild(CHILD_STAMHASTABLE, StaticTextField.class);
        registerChild(CHILD_STAMTITLE, StaticTextField.class);
        registerChild(CHILD_STAMINFOMSG, StaticTextField.class);
        registerChild(CHILD_STAMMSGS, StaticTextField.class);
        registerChild(CHILD_STAMMSGTYPES, StaticTextField.class);
        registerChild(CHILD_STAMDIALOGMSG, StaticTextField.class);
        registerChild(CHILD_STAMBUTTONSHTML, StaticTextField.class);
        registerChild(CHILD_STSPECIALFEATURE, StaticTextField.class);
        registerChild(CHILD_RPPIRORITYCOUNT,
                pgIWorkQueuerpPirorityCountTiledView.class);

        // --> Hidden ActMessageButton (FINDTHISLATER)
        // --> Test by BILLY 15July2002
        registerChild(CHILD_BTACTMSG, Button.class);

        // --Release2.1--//
        // // New link to toggle the language. When touched this link should
        // reload the
        // // page in opposite language (french versus english) and set this new
        // language
        // // session value throughout all modulus.
        registerChild(CHILD_TOGGLELANGUAGEHREF, HREF.class);
        registerChild(CHILD_CBINSTITUTION, ComboBox.class);
    }

    // //////////////////////////////////////////////////////////////////////////////
    // Model management methods
    // //////////////////////////////////////////////////////////////////////////////

    /**
     * 
     * 
     */
    public Model[] getWebActionModels(int executionType) {
        List modelList = new ArrayList();

        switch (executionType) {
        case MODEL_TYPE_RETRIEVE:
            modelList.add(getdoDealSummarySnapShotModel());
            ;

            break;

        case MODEL_TYPE_UPDATE:
            ;
            break;

        case MODEL_TYPE_DELETE:
            ;

            break;

        case MODEL_TYPE_INSERT:
            ;

            break;

        case MODEL_TYPE_EXECUTE:
            ;

            break;
        }

        return (Model[]) modelList.toArray(new Model[0]);
    }

    // //////////////////////////////////////////////////////////////////////////////
    // View flow control methods
    // //////////////////////////////////////////////////////////////////////////////

    /**
     * 
     * 
     */
    public void beginDisplay(DisplayEvent event) throws ModelControlException {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        // --Release2.1--//
        // Populate all ComboBoxes manually here
        // --> By Billy 05Nov2002
        // Setup Options for cbPageNames
        // cbPageNamesOptions..populate(getRequestContext());
        int langId = handler.getTheSessionState().getLanguageId();
        cbPageNamesOptions.populate(getRequestContext(), langId);
        cbInstitutionOptions.populate(getRequestContext());
        // Setup NoneSelected Label for cbPageNames
        CHILD_CBPAGENAMES_NONSELECTED_LABEL = BXResources.getGenericMsg(
                "CBPAGENAMES_NONSELECTED_LABEL", handler.getTheSessionState()
                        .getLanguageId());

        // Check if the cbPageNames is already created
        if (getCbPageNames() != null) {
            getCbPageNames().setLabelForNoneSelected(
                    CHILD_CBPAGENAMES_NONSELECTED_LABEL);
        }

        // -- ========== SCR#931 begins ========== --//
        // -- by Neil on Feb 09, 2005
        // BMO: populate FilterOption combo box on-flight instead of getting
        // from resource bundle.
        // others: populate from resource bundle, need to invoke the following
        // call.
        logger = SysLog.getSysLogger("IWQPAGE");
        boolean isClientBMO = (PropertiesCache.getInstance().getInstanceProperty(
//        		boolean isClientBMO = (PropertiesCache.getInstance().getProperty(
                "com.basis100.fxp.clientid.bmo", "N")).equals("Y");
        logger.debug("IWQVB@beginDisplay::Client is BMO?" + isClientBMO);
        if (!isClientBMO) {
            // Setup Options for cbFilter
            cbFilterOptionsOptions.populate(getRequestContext());
        }
        // -- ========== SCR#931 ends ========== --//

        // STRESSTEST--start--//
        super.beginDisplay(event);
        // beginDisplay(event);
        // STRESSTEST--end--//
    }

    /**
     * 
     * 
     */
    public boolean beforeModelExecutes(Model model, int executionContext) {
        // logger = SysLog.getSysLogger("IWQPAGE");
        // logger.debug("IWQPAGE@Test setupBeforePageGeneration from IWQ page");
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        // logger.debug("IWQPAGE@Before setupBeforePageGeneration");
        handler.setupBeforePageGeneration();

        // logger.debug("IWQPAGE@After setupBeforePageGeneration");
        handler.pageSaveState();

         logger.debug("IWQPAGE@beforeModelExecutes");
         logger.debug("IWQPAGE@ModelIn: " + model);
         logger.debug("IWQPAGE@ExecutionContextIn: " + executionContext);
         logger.debug("IWQPAGE@Hanlder.DealInstituionProfileId: " + handler.theSessionState.getDealInstitutionId());
//       This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
        return super.beforeModelExecutes(model, executionContext);

        // The following code block was migrated from the
        // this_onBeforeDataObjectExecuteEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
         * iWorkQueueHandler handler =(iWorkQueueHandler)
         * this.handler.cloneSS(); handler.pageGetState(this);
         * //CSpHtml.sendMessage("DO Exec Event at deal summary " +
         * event.getDataObject()); handler.setupBeforePageGeneration();
         * handler.pageSaveState(); return;
         */
    }

    /**
     * 
     * 
     */
    public void afterModelExecutes(Model model, int executionContext) {
        // This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
        super.afterModelExecutes(model, executionContext);
    }

    // --Release2.1--//
    // // Get LanguageId from the SessionStateModel
    private String getSessionLanguageId() {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        int langId = handler.getTheSessionState().getLanguageId();
        handler.pageSaveState();

        return new Integer(langId).toString();
    }

    /**
     * 
     * 
     */
    public void afterAllModelsExecute(int executionContext) {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        handler.populatePageDisplayFields();

        handler.pageSaveState();

        // This is the analog of NetDynamics
        // this_onAfterAllDataObjectsExecuteEvent
        super.afterAllModelsExecute(executionContext);

        // The following code block was migrated from the
        // this_onBeforeRowDisplayEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
         * iWorkQueueHandler handler =(iWorkQueueHandler)
         * this.handler.cloneSS(); handler.pageGetState(this);
         * handler.populatePageDisplayFields(); handler.pageSaveState(); return
         * PROCEED;
         */
    }

    /**
     * 
     * 
     */
    public void onModelError(Model model, int executionContext,
            ModelControlException exception) throws ModelControlException {
        // This is the analog of NetDynamics this_onDataObjectErrorEvent
        super.onModelError(model, executionContext, exception);
    }

    /**
     * 
     * 
     */
    public pgIWorkQueueRepeated1TiledView getRepeated1() {
        return (pgIWorkQueueRepeated1TiledView) getChild(CHILD_REPEATED1);
    }

    /**
     * 
     * 
     */
    public pgIWorkQueueRepeated2TiledView getRepeated2() {
        return (pgIWorkQueueRepeated2TiledView) getChild(CHILD_REPEATED2);
    }

    /**
     * 
     * 
     */
    public StaticTextField getRowsDisplayed() {
        return (StaticTextField) getChild(CHILD_ROWSDISPLAYED);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStUserNameTitle() {
        return (StaticTextField) getChild(CHILD_STUSERNAMETITLE);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStTodayDate() {
        return (StaticTextField) getChild(CHILD_STTODAYDATE);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStCompanyName() {
        return (StaticTextField) getChild(CHILD_STCOMPANYNAME);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStPageLabel() {
        return (StaticTextField) getChild(CHILD_STPAGELABEL);
    }

    /**
     * 
     * 
     */
    public ComboBox getCbPageNames() {
        return (ComboBox) getChild(CHILD_CBPAGENAMES);
    }

    /**
     * 
     * 
     */

    // // This is a new fragment which done by the iMT tool. Should be tested.
    // // Temporarily the original one is used.
    public void handleBtProceedRequest(RequestInvocationEvent event)
            throws ServletException, IOException
    // //public void handleBtProceedRequest(RequestContext requestContext)
    // // throws ServletException, IOException
    {
        // logger = SysLog.getSysLogger("IWQPAGE");
        // // IMPORTANT!! Currently the system displays the image of this button
        // // to process the request. There is some problem in the
        // JavaScript/JATO "grey"
        // // area again. Direct request (no JS) works identical either invoked
        // from button
        // // or from the link as it should be (both are Command objects).
        // // Nevertheless, this button allows to test the GOTO converted to
        // JATO
        // // business functionality.
        // // This fragment is a classical example how nice the migration could
        // be finished
        // // when the BasisXpress framework migrated and tested. It is just
        // necessary to uncomment
        // // the appropriate fragment commented by the iMT during the
        // translation phase. Currently this
        // // commented out fragment is copied to show its origination by the
        // iMT.
        // logger.debug("GoPage function test");
        // Button goToBtn = getBtProceed();
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleGoPage();

        // // execution of all executing Web action Models. Test it!!!
        // this.handleWebAction(WebActionHandler.ACTION_EXECUTE);
        // goToBtn.mapSourceTargetNVPs();
        handler.postHandlerProtocol();

        // The following code block was migrated from the btProceed_onWebEvent
        // method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
         * iWorkQueueHandler handler =(iWorkQueueHandler)
         * this.handler.cloneSS(); handler.preHandlerProtocol(this);
         * handler.handleGoPage(); handler.postHandlerProtocol(); return
         * PROCEED;
         */
    }

    /**
     * 
     * 
     */
    public Button getBtProceed() {
        return (Button) getChild(CHILD_BTPROCEED);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStEstClosingDate() {
        return (StaticTextField) getChild(CHILD_STESTCLOSINGDATE);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStPmtTerm() {
        return (StaticTextField) getChild(CHILD_STPMTTERM);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStPurchasePrice() {
        return (StaticTextField) getChild(CHILD_STPURCHASEPRICE);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStDealPurpose() {
        return (StaticTextField) getChild(CHILD_STDEALPURPOSE);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStDealType() {
        return (StaticTextField) getChild(CHILD_STDEALTYPE);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStLOB() {
        return (StaticTextField) getChild(CHILD_STLOB);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStSource() {
        return (StaticTextField) getChild(CHILD_STSOURCE);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStSourceFirm() {
        return (StaticTextField) getChild(CHILD_STSOURCEFIRM);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStDealStatusDate() {
        return (StaticTextField) getChild(CHILD_STDEALSTATUSDATE);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStDealStatus() {
        return (StaticTextField) getChild(CHILD_STDEALSTATUS);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStDealId() {
        return (StaticTextField) getChild(CHILD_STDEALID);
    }

    /**
     * 
     * 
     */
    public void handleHref1Request(RequestInvocationEvent event)
            throws ServletException, IOException {
        // The following code block was migrated from the Href1_onWebEvent
        // method
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(0);
        handler.postHandlerProtocol();
    }

    /**
     * 
     * 
     */
    public HREF getHref1() {
        return (HREF) getChild(CHILD_HREF1);
    }

    /**
     * 
     * 
     */
    public void handleHref2Request(RequestInvocationEvent event)
            throws ServletException, IOException {
        // The following code block was migrated from the Href2_onWebEvent
        // method
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(1);
        handler.postHandlerProtocol();
    }

    /**
     * 
     * 
     */
    public HREF getHref2() {
        return (HREF) getChild(CHILD_HREF2);
    }

    /**
     * 
     * 
     */
    public void handleHref3Request(RequestInvocationEvent event)
            throws ServletException, IOException {
        // The following code block was migrated from the Href3_onWebEvent
        // method
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(2);
        handler.postHandlerProtocol();
    }

    /**
     * 
     * 
     */
    public HREF getHref3() {
        return (HREF) getChild(CHILD_HREF3);
    }

    /**
     * 
     * 
     */
    public void handleHref4Request(RequestInvocationEvent event)
            throws ServletException, IOException {
        // The following code block was migrated from the Href4_onWebEvent
        // method
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(3);
        handler.postHandlerProtocol();
    }

    /**
     * 
     * 
     */
    public HREF getHref4() {
        return (HREF) getChild(CHILD_HREF4);
    }

    /**
     * 
     * 
     */
    public void handleHref5Request(RequestInvocationEvent event)
            throws ServletException, IOException {
        // The following code block was migrated from the Href5_onWebEvent
        // method
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(4);
        handler.postHandlerProtocol();
    }

    /**
     * 
     * 
     */
    public HREF getHref5() {
        return (HREF) getChild(CHILD_HREF5);
    }

    /**
     * 
     * 
     */
    public void handleHref6Request(RequestInvocationEvent event)
            throws ServletException, IOException {
        // The following code block was migrated from the Href6_onWebEvent
        // method
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(5);
        handler.postHandlerProtocol();
    }

    /**
     * 
     * 
     */
    public HREF getHref6() {
        return (HREF) getChild(CHILD_HREF6);
    }

    /**
     * 
     * 
     */
    public void handleHref7Request(RequestInvocationEvent event)
            throws ServletException, IOException {
        // The following code block was migrated from the Href7_onWebEvent
        // method
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(6);
        handler.postHandlerProtocol();
    }

    /**
     * 
     * 
     */
    public HREF getHref7() {
        return (HREF) getChild(CHILD_HREF7);
    }

    /**
     * 
     * 
     */
    public void handleHref8Request(RequestInvocationEvent event)
            throws ServletException, IOException {
        // The following code block was migrated from the Href8_onWebEvent
        // method
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(7);
        handler.postHandlerProtocol();
    }

    /**
     * 
     * 
     */
    public HREF getHref8() {
        return (HREF) getChild(CHILD_HREF8);
    }

    /**
     * 
     * 
     */
    public void handleHref9Request(RequestInvocationEvent event)
            throws ServletException, IOException {
        // The following code block was migrated from the Href9_onWebEvent
        // method
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(8);
        handler.postHandlerProtocol();
    }

    /**
     * 
     * 
     */
    public HREF getHref9() {
        return (HREF) getChild(CHILD_HREF9);
    }

    /**
     * 
     * 
     */
    public void handleHref10Request(RequestInvocationEvent event)
            throws ServletException, IOException {
        // The following code block was migrated from the Href10_onWebEvent
        // method
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(9);
        handler.postHandlerProtocol();
    }

    /**
     * 
     * 
     */
    public HREF getHref10() {
        return (HREF) getChild(CHILD_HREF10);
    }

    /**
     * 
     * 
     */
    public TextField getTbDealId() {
        return (TextField) getChild(CHILD_TBDEALID);
    }

    /**
     * 
     * 
     */
    public Button getBtBackwardButton() {
        return (Button) getChild(CHILD_BTBACKWARDBUTTON);
    }

    /**
     * 
     * 
     */
    public String endBtBackwardButtonDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btBackwardButton_onBeforeHtmlOutputEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
         * iWorkQueueHandler handler =(iWorkQueueHandler)
         * this.handler.cloneSS(); handler.pageGetState(this); int rc =
         * handler.generateBackwardButtonStd(); handler.pageSaveState(); return
         * rc;
         */
        return event.getContent();
    }

    /**
     * 
     * 
     */
    public void handleBtFilterSubmitButtonRequest(RequestInvocationEvent event)
            throws ServletException, IOException {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleFilterButton();

        handler.postHandlerProtocol();

        // The following code block was migrated from the
        // btFilterSubmitButton_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
         * iWorkQueueHandler handler =(iWorkQueueHandler)
         * this.handler.cloneSS(); handler.preHandlerProtocol(this);
         * handler.handleFilterButton(); handler.postHandlerProtocol(); return
         * PROCEED;
         */
    }

    /**
     * 
     * 
     */
    public Button getBtFilterSubmitButton() {
        return (Button) getChild(CHILD_BTFILTERSUBMITBUTTON);
    }

    /**
     * 
     * 
     */
    public void handleBtSortButtonRequest(RequestInvocationEvent event)
            throws ServletException, IOException {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleSortButton();

        handler.postHandlerProtocol();

        // The following code block was migrated from the
        // btSortButton_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
         * iWorkQueueHandler handler =(iWorkQueueHandler)
         * this.handler.cloneSS(); handler.preHandlerProtocol(this);
         * handler.handleSortButton(); handler.postHandlerProtocol(); return
         * PROCEED;
         */
    }

    /**
     * 
     * 
     */
    public Button getBtSortButton() {
        return (Button) getChild(CHILD_BTSORTBUTTON);
    }

    /**
     * 
     * 
     */
    public ComboBox getCbSortOptions() {
        return (ComboBox) getChild(CHILD_CBSORTOPTIONS);
    }

    /**
     * 
     * 
     */
    public ComboBox getCbFilterOptions() {
        return (ComboBox) getChild(CHILD_CBFILTEROPTIONS);
    }

    /**
     * @param event
     * 
     * @throws ServletException
     *             DOCUMENT ME!
     * @throws IOException
     *             DOCUMENT ME!
     */
    public void handleBtFilterSortResetRequest(RequestInvocationEvent event)
            throws ServletException, IOException {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleFilterSortResetButton();

        handler.postHandlerProtocol();

        // The following code block was migrated from the
        // btFilterSortReset_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
         * iWorkQueueHandler handler =(iWorkQueueHandler)
         * this.handler.cloneSS(); handler.preHandlerProtocol(this);
         * handler.handleFilterSortResetButton(); handler.postHandlerProtocol();
         * return PROCEED;
         */
    }

    /**
     * 
     * 
     */
    public Button getBtFilterSortReset() {
        return (Button) getChild(CHILD_BTFILTERSORTRESET);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStSortOptionDesc() {
        return (StaticTextField) getChild(CHILD_STSORTOPTIONDESC);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStFilterOptionDesc() {
        return (StaticTextField) getChild(CHILD_STFILTEROPTIONDESC);
    }

    /**
     * 
     * 
     */
    public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
            throws ServletException, IOException {
        // The following code block was migrated from the
        // changePasswordHref_onWebEvent method
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);
        handler.handleChangePassword();
        handler.postHandlerProtocol();

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
         * iWorkQueueHandler handler =(iWorkQueueHandler)
         * this.handler.cloneSS(); handler.preHandlerProtocol(this);
         * handler.handleChangePassword(); handler.postHandlerProtocol(); return
         * PROCEED;
         */
    }

    /**
     * 
     * 
     */
    public HREF getChangePasswordHref() {
        return (HREF) getChild(CHILD_CHANGEPASSWORDHREF);
    }

    // --Release2.1--start//
    // // Two new methods providing the business logic for the new link to
    // toggle language.
    // // When touched this link should reload the page in opposite language
    // (french versus english)
    // // and set this new session value.

    /**
     * 
     * 
     */
    public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
            throws ServletException, IOException {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleToggleLanguage();

        handler.postHandlerProtocol();
    }

    /**
     * 
     * 
     */
    public HREF getToggleLanguageHref() {
        return (HREF) getChild(CHILD_TOGGLELANGUAGEHREF);
    }

    // --Release2.1--end//

    /**
     * 
     * 
     */
    public void handleBtToolHistoryRequest(RequestInvocationEvent event)
            throws ServletException, IOException {
        // // IMPORTANT! Currently we are using this button to test directly the
        // DealEntry page.
        // // On the other hand, the generic solution via the Pre/PostHandler
        // protocols is done
        // // as well. This a VERY TEMP TESTING solution left here after the
        // final
        // // testing/finishing the main BasisXpress framework migration.
        // // IMPORTANT! Tomcat container for some unclear reason fires
        // exception
        // // being forwarded to the DealEntry/UWorksheet page. Resin works
        // fine.
        // // It is much better in many other parameters (compairing with the
        // Tomcat), so
        // // the Resin should be used for the development.
        // // The testing with the deployment container (iPlanet) must be done
        // // as soon as possible to distinguish/resolve such and other
        // deployment
        // // issues!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // logger = SysLog.getSysLogger("IWQPAGE");
        // logger.debug("Go to DealEntry page from ToolHistory button");
        Button historyBtn = getBtToolHistory();

        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleDisplayDealHistory();

        handler.postHandlerProtocol();

        // The following code block was migrated from the
        // btToolHistory_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
         * iWorkQueueHandler handler =(iWorkQueueHandler)
         * this.handler.cloneSS(); handler.preHandlerProtocol(this);
         * handler.handleDisplayDealHistory(); handler.postHandlerProtocol();
         * return PROCEED;
         */
    }

    /**
     * 
     * 
     */
    public Button getBtToolHistory() {
        return (Button) getChild(CHILD_BTTOOLHISTORY);
    }

    /**
     * 
     * 
     */
    public void handleBtTooNotesRequest(RequestInvocationEvent event)
            throws ServletException, IOException {
        // // IMPORTANT! Currently we are using this button to test directly the
        // UWorksheet page.
        // // On the other hand, the generic solution via the Pre/PostHandler
        // protocols is done
        // // as well. This a VERY TEMP TESTING solution left here after the
        // final
        // // testing/finishing the main BasisXpress framework migration.
        // // IMPORTANT! Tomcat container for some unclear reason fires
        // exception
        // // being forwarded to the DealEntry/UWorksheet page. Resin works
        // fine.
        // // It is much better in many other parameters (compairing with the
        // Tomcat), so
        // // the Resin should be used for the development.
        // // The testing with the deployment container (iPlanet) must be done
        // // as soon as possible to distinguish/resolve such and other
        // deployment
        // // issues!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleDisplayDealNotes();

        handler.postHandlerProtocol();

        // The following code block was migrated from the btTooNotes_onWebEvent
        // method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
         * iWorkQueueHandler handler =(iWorkQueueHandler)
         * this.handler.cloneSS(); handler.preHandlerProtocol(this);
         * handler.handleDisplayDealNotes(); handler.postHandlerProtocol();
         * return PROCEED;
         */
    }

    /**
     * 
     * 
     */
    public Button getBtTooNotes() {
        return (Button) getChild(CHILD_BTTOONOTES);
    }

    /**
     * 
     * 
     */
    public void handleBtToolSearchRequest(RequestInvocationEvent event)
            throws ServletException, IOException {

        // // IMPORTANT! Currently we are using this button to test directly the
        // SourceOfBusSearch.
        // // On the other hand, the generic solution via the Pre/PostHandler
        // protocols is done
        // // as well. This a VERY TEMP TESTING solution left here after the
        // final
        // // testing/finishing the main BasisXpress framework migration.
        // // IMPORTANT! Tomcat container for some unclear reason fires
        // exception
        // // being forwarded to the DealEntry/UWorksheet page. Resin works
        // fine.
        // // It is much better in many other parameters (compairing with the
        // Tomcat), so
        // // the Resin should be used for the development.
        // // The testing with the deployment container (iPlanet) must be done
        // // as soon as possible to distinguish/resolve such and other
        // deployment
        // // issues!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleDealSearch();

        handler.postHandlerProtocol();

        // The following code block was migrated from the
        // btToolSearch_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
         * iWorkQueueHandler handler =(iWorkQueueHandler)
         * this.handler.cloneSS(); handler.preHandlerProtocol(this);
         * handler.handleDealSearch(); handler.postHandlerProtocol(); return
         * PROCEED;
         */
    }

    /**
     * 
     * 
     */
    public Button getBtToolSearch() {
        return (Button) getChild(CHILD_BTTOOLSEARCH);
    }

    /**
     * 
     * 
     */
    public void handleBtToolLogRequest(RequestInvocationEvent event)
            throws ServletException, IOException {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleSignOff();

        handler.postHandlerProtocol();

        // The following code block was migrated from the btToolLog_onWebEvent
        // method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
         * iWorkQueueHandler handler =(iWorkQueueHandler)
         * this.handler.cloneSS(); handler.preHandlerProtocol(this);
         * handler.handleSignOff(); handler.postHandlerProtocol(); return;
         */
    }

    /**
     * 
     * 
     */
    public Button getBtToolLog() {
        return (Button) getChild(CHILD_BTTOOLLOG);
    }

    /**
     * 
     * 
     */
    public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
            throws ServletException, IOException {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleDisplayWorkQueue();

        handler.postHandlerProtocol();

        // The following code block was migrated from the
        // btWorkQueueLink_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
         * iWorkQueueHandler handler =(iWorkQueueHandler)
         * this.handler.cloneSS(); handler.preHandlerProtocol(this);
         * handler.handleDisplayWorkQueue(); handler.postHandlerProtocol();
         * return PROCEED;
         */
    }

    /**
     * 
     * 
     */
    public Button getBtWorkQueueLink() {
        return (Button) getChild(CHILD_BTWORKQUEUELINK);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStErrorFlag() {
        return (StaticTextField) getChild(CHILD_STERRORFLAG);
    }

    // /////////
    // // VERY IMPORTANT BACK/FORWARD JATO MVC IMPLEMENTATION BASED OF THE
    // // (FIRST/NEST/PREVIOUS/LAST NETDYNAMICS PATTERN.
    // // SysLog class currently fires NullPointer Exception (or JATO Root
    // // Navigation Exception). It is not critical here now and currently
    // commented out.
    // /////////

    /**
     * 
     * 
     */
    public boolean beginBtForwardButtonDisplay(ChildDisplayEvent event)
            throws IOException // //BTFORWARDBUTTON
    {
        // // logger.debug("Web event invoked:
        // "+getClass().getName()+".BtForwardButton");
        boolean result = ((doIWorkQueueModelImpl) getPgIWorkQueueRepeated1TiledView()
                .getdoIWorkQueueModel()).hasMoreResults();

        // //logger.debug("IWQ@beginBtForwardButtonDisplay: " + result);
        // //if (!result)
        // // renderDisabledButton(event, CHILD_BTFORWARDBUTTON,
        // CHILD_BTFORWARDBUTTON_RESET_VALUE);
        return result;
    }

    public pgIWorkQueueRepeated1TiledView getPgIWorkQueueRepeated1TiledView() {
        return (pgIWorkQueueRepeated1TiledView) getChild(CHILD_REPEATED1);
    }

    public boolean beginBtBackwardButtonDisplay(ChildDisplayEvent event)
            throws IOException // //BTBACKWARDBUTTON
    {
        boolean result = ((doIWorkQueueModelImpl) getPgIWorkQueueRepeated1TiledView()
                .getdoIWorkQueueModel()).hasPreviousResults();

        return result;
    }

    private void renderDisabledButton(ChildDisplayEvent event, String name,
            String value) throws IOException {
        // This demonstates how to use the supplied display event to output
        // directly to the JSP output stream
        ((JspChildDisplayEvent) event).getPageContext().getOut().println(
                "<input type=\"button\" name=\""
                        + getChild(name).getQualifiedName() + "\" value=\""
                        + value + "\" disabled>");
    }

    // //////////////////////////////////////////////////////////////////////////
    // Request handling event methods
    // //////////////////////////////////////////////////////////////////////////

    /**
     * 
     * 
     */

    // --> Test and fix by BILLY 22July2002
    public void handleBtBackwardButtonRequest(RequestInvocationEvent event)
            throws ServletException, IOException {
        // // SysLog fires NullPointerException??!! (check the base class(es).
        // //SysLog.getSysLogger("IWQPAGE");
        // //logger.debug("Web event invoked:
        // "+getClass().getName()+".BtForwardButton");
        // //logger.debug("IWQ@handleBtBackwardButtonRequest: before forward
        // back");

        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        getPgIWorkQueueRepeated1TiledView().handleWebAction(
                WebActions.ACTION_PREV);

        handler.handleBackwardButton();

        handler.postHandlerProtocol();
    }

    /**
     * 
     * 
     */

    // --> Test by Billy 22July2002
    /*
     * public void handleBtForwardButtonRequest(RequestInvocationEvent event)
     * throws ServletException, IOException { //// SysLog fires
     * NullPointerException??!! (check the base class(es).
     * ////SysLog.getSysLogger("IWQPAGE"); ////logger.debug("Web event invoked:
     * "+getClass().getName()+".BtForwardButton");
     * ////logger.debug("IWQ@handleBtBackwardButtonRequest: before go forward");
     * getPgIWorkQueueRepeated1TiledView().handleWebAction(WebActions.ACTION_NEXT);
     * forwardTo(); }
     */
    /**
     * 
     * 
     */
    public void handleBtForwardButtonRequest(RequestInvocationEvent event)
            throws ServletException, IOException {

        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        getPgIWorkQueueRepeated1TiledView().handleWebAction(
                WebActions.ACTION_NEXT);

        handler.handleForwardButton();

        handler.postHandlerProtocol();

        // // Fires NullPointerException???? (check the base class(es).
        // //logger.debug("IWVB@handleBtForwardButtonRequest: start");
        // //String message="--- " + "::beginDisplay() event fired for " +
        // getChild(CHILD_BTFORWARDBUTTON).getQualifiedName();
        // //getParentViewBean().getRequestContext().getMessageWriter().println(message);
        // //logger.debug("Debug: " + message);
        // The following code block was migrated from the
        // btForwardButton_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
         * iWorkQueueHandler handler =(iWorkQueueHandler)
         * this.handler.cloneSS(); handler.preHandlerProtocol(this);
         * handler.handleForwardButton(); handler.postHandlerProtocol(); return
         * PROCEED;
         */
    }

    /**
     * 
     * 
     */
    public Button getBtForwardButton() {
        return (Button) getChild(CHILD_BTFORWARDBUTTON);
    }

    /**
     * 
     * 
     */
    public String endBtForwardButtonDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btForwardButton_onBeforeHtmlOutputEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
         * iWorkQueueHandler handler =(iWorkQueueHandler)
         * this.handler.cloneSS(); handler.pageGetState(this); int rc =
         * handler.generateForwardButtonStd(); handler.pageSaveState(); return
         * rc;
         */
        return event.getContent();
    }

    /**
     * 
     * 
     */
    public HiddenField getDetectAlertTasks() {
        return (HiddenField) getChild(CHILD_DETECTALERTTASKS);
    }

    /**
     * 
     * 
     */
    public StaticTextField getDisplayprioritytasks() {
        return (StaticTextField) getChild(CHILD_DISPLAYPRIORITYTASKS);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStTotalLoanAmount() {
        return (StaticTextField) getChild(CHILD_STTOTALLOANAMOUNT);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStBorrFirstName() {
        return (StaticTextField) getChild(CHILD_STBORRFIRSTNAME);
    }

    /**
     * 
     * 
     */
    public HiddenField getSessionUserId() {
        return (HiddenField) getChild(CHILD_SESSIONUSERID);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStPmGenerate() {
        return (StaticTextField) getChild(CHILD_STPMGENERATE);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStPmHasTitle() {
        return (StaticTextField) getChild(CHILD_STPMHASTITLE);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStPmHasInfo() {
        return (StaticTextField) getChild(CHILD_STPMHASINFO);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStPmHasTable() {
        return (StaticTextField) getChild(CHILD_STPMHASTABLE);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStPmHasOk() {
        return (StaticTextField) getChild(CHILD_STPMHASOK);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStPmTitle() {
        return (StaticTextField) getChild(CHILD_STPMTITLE);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStPmInfoMsg() {
        return (StaticTextField) getChild(CHILD_STPMINFOMSG);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStPmOnOk() {
        return (StaticTextField) getChild(CHILD_STPMONOK);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStPmMsgs() {
        return (StaticTextField) getChild(CHILD_STPMMSGS);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStPmMsgTypes() {
        return (StaticTextField) getChild(CHILD_STPMMSGTYPES);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStAmGenerate() {
        return (StaticTextField) getChild(CHILD_STAMGENERATE);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStAmHasTitle() {
        return (StaticTextField) getChild(CHILD_STAMHASTITLE);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStAmHasInfo() {
        return (StaticTextField) getChild(CHILD_STAMHASINFO);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStAmHasTable() {
        return (StaticTextField) getChild(CHILD_STAMHASTABLE);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStAmTitle() {
        return (StaticTextField) getChild(CHILD_STAMTITLE);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStAmInfoMsg() {
        return (StaticTextField) getChild(CHILD_STAMINFOMSG);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStAmMsgs() {
        return (StaticTextField) getChild(CHILD_STAMMSGS);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStAmMsgTypes() {
        return (StaticTextField) getChild(CHILD_STAMMSGTYPES);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStAmDialogMsg() {
        return (StaticTextField) getChild(CHILD_STAMDIALOGMSG);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStAmButtonsHtml() {
        return (StaticTextField) getChild(CHILD_STAMBUTTONSHTML);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStSpecialFeature() {
        return (StaticTextField) getChild(CHILD_STSPECIALFEATURE);
    }

    public ComboBox getCbInstitution() {
        return (ComboBox) getChild(CHILD_CBINSTITUTION);
    }
    
    /**
     * 
     * 
     */

    // // ADDBACK. The ComputedColumn iMT bug is fixed now!! The following
    // fragment is
    // // uncommented out (see the solution description in the doc and in the
    // // doPirorityCountModelImpl
    public pgIWorkQueuerpPirorityCountTiledView getRpPirorityCount() {
        return (pgIWorkQueuerpPirorityCountTiledView) getChild(CHILD_RPPIRORITYCOUNT);
    }

    /**
     * 
     * 
     */
    public doDealSummarySnapShotModel getdoDealSummarySnapShotModel() {
        if (doDealSummarySnapShot == null) {
            doDealSummarySnapShot = (doDealSummarySnapShotModel) getModel(doDealSummarySnapShotModel.class);
        }

        return doDealSummarySnapShot;
    }

    /**
     * 
     * 
     */
    public void setdoDealSummarySnapShotModel(doDealSummarySnapShotModel model) {
        doDealSummarySnapShot = model;
    }

    // --> Hidden ActMessageButton (FINDTHISLATER)
    // --> Test by BILLY 15July2002
    public Button getBtActMsg() {
        return (Button) getChild(CHILD_BTACTMSG);
    }

    // ===========================================
    // //////////////////////////////////////////////////////////////////////////
    // Obsolete Netdynamics Events - Require Manual Migration
    // //////////////////////////////////////////////////////////////////////////
    // //////////////////////////////////////////////////////////////////////////
    // Custom Methods - Require Manual Migration
    // //////////////////////////////////////////////////////////////////////////

    /* MigrationToDo : Migrate custom method */
    public void handleActMessageOK(String[] args) {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleActMessageOK(args);

        handler.postHandlerProtocol();

        // //return;
    }

    // --> Addition methods to propulate Href display String
    // --> Test by BILLY 07Aug2002
    public String endHref1Display(ChildContentDisplayEvent event) {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 0);

        // logger = SysLog.getSysLogger("pgIWQ");
        // logger.debug("pgIWQ@EndHrefDisplay: " + rc);
        handler.pageSaveState();

        return rc;
    }

    public String endHref2Display(ChildContentDisplayEvent event) {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 1);
        handler.pageSaveState();

        return rc;
    }

    public String endHref3Display(ChildContentDisplayEvent event) {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 2);
        handler.pageSaveState();

        return rc;
    }

    public String endHref4Display(ChildContentDisplayEvent event) {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 3);
        handler.pageSaveState();

        return rc;
    }

    public String endHref5Display(ChildContentDisplayEvent event) {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 4);
        handler.pageSaveState();

        return rc;
    }

    public String endHref6Display(ChildContentDisplayEvent event) {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 5);
        handler.pageSaveState();

        return rc;
    }

    public String endHref7Display(ChildContentDisplayEvent event) {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 6);
        handler.pageSaveState();

        return rc;
    }

    public String endHref8Display(ChildContentDisplayEvent event) {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 7);
        handler.pageSaveState();

        return rc;
    }

    public String endHref9Display(ChildContentDisplayEvent event) {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 8);
        handler.pageSaveState();

        return rc;
    }

    public String endHref10Display(ChildContentDisplayEvent event) {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 9);
        handler.pageSaveState();

        return rc;
    }
    
    public void handleBtInstSelectedRequest(RequestInvocationEvent event)
	throws ServletException, Exception {
		iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.postHandlerProtocol();
	}
    // =====================================================
    // // This method overrides the getDefaultURL() framework JATO method and it
    // is located
    // // in each ViewBean. This allows not to stay with the JATO ViewBean
    // extension,
    // // otherwise each ViewBean a.k.a page should extend its Handler (to be
    // called by the framework)
    // // and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
    // // The full method is still in PHC base class. It should care the
    // // the url="/" case (non-initialized defaultURL) by the BX framework
    // methods.
    public String getDisplayURL() {
        iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        // logger = SysLog.getSysLogger("MWQVB");
        String url = getDefaultDisplayURL();

        // logger.debug("IWQVB@getDisplayURL::DefaultURL: " + url);
        int languageId = handler.theSessionState.getLanguageId();

        // logger.debug("IWQV@getDisplayURL::LanguageFromUSOSession: " +
        // languageId);
        // // Call the language specific URL (business delegation done in the
        // BXResource).
        if ((url != null) && !url.trim().equals("") && !url.trim().equals("/")) {
            url = BXResources.getBXUrl(url, languageId);

            // logger.debug("IWQV@getDisplayURL::GoodMode: " + url);
        } else {
            url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);

            // logger.debug("IWQV@getDisplayURL::BadMode: " + url);
        }

        return url;
    }

    static class CbPageNamesOptionList extends GotoOptionList {
        /**
         * 
         * 
         */
        CbPageNamesOptionList() {
        }

        public void populate(RequestContext rc) {
            Connection c = null;

            try {
                clear();

                SelectQueryModel m = null;

                SelectQueryExecutionContext eContext = new SelectQueryExecutionContext(
                        (Connection) null,
                        DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
                        DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

                if (rc == null) {
                    m = new doPageNameLabelModelImpl();
                    c = SQLConnectionManagerImpl.obtainConnection(m
                            .getDataSourceName());
                    eContext.setConnection(c);
                } else {
                    m = (SelectQueryModel) rc.getModelManager().getModel(
                            doPageNameLabelModel.class);
                }

                m.retrieve(eContext);
                m.beforeFirst();

                // --> Modified to sort by PageLabel based on the selected
                // language
                // --> by Billy 07Oct2004
                // Sort the results from DataObject
                TreeMap sorted = new TreeMap();

                while (m.next()) {
                    Object dfPageLabel = m
                            .getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
                    String label = ((dfPageLabel == null) ? "" : dfPageLabel
                            .toString());
                    Object dfPageId = m
                            .getValue(doPageNameLabelModel.FIELD_DFPAGEID);
                    String value = ((dfPageId == null) ? "" : dfPageId
                            .toString());
                    String[] theVal = new String[2];
                    theVal[0] = label;
                    theVal[1] = value;
                    sorted.put(label, theVal);
                }

                // Set the sorted list to the optionlist
                Iterator theList = sorted.values().iterator();
                String[] theVal = new String[2];

                while (theList.hasNext()) {
                    theVal = (String[]) (theList.next());
                    add(theVal[0], theVal[1]);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (c != null) {
                        c.close();
                    }
                } catch (SQLException ex) {
                    // ignore
                }
            }
        }
    }

    
    /*
     * Combo Box for Institution
     */
    static class CbInstitutionOptions extends
            pgPortfolioStatsViewBean.CbInstitutionsOptionList {

        /**
         * constructor.
         */
        CbInstitutionOptions() {
        }

        /**
         * 
         * 
         */
        public void populate(RequestContext rc) {
            Connection c = null;
            Statement query = null;
            try {
                clear();
                if (rc == null) {
                    c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
                } else {
                    c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
                }

                query = c.createStatement();
                ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);

                add("", "");
                while (results.next()) {
                    Object INSTITUTIONPROFILEID = results
                            .getObject("INSTITUTIONPROFILEID");
                    Object INSTITUTIONPROFILE_INSTITUTIONNAME = results
                            .getObject("INSTITUTIONNAME");

                    String label = (INSTITUTIONPROFILE_INSTITUTIONNAME == null ? ""
                            : INSTITUTIONPROFILE_INSTITUTIONNAME.toString());

                    String value = (INSTITUTIONPROFILEID == null ? ""
                            : INSTITUTIONPROFILEID.toString());

                    add(label, value);
                }
                results.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (c != null)
                        c.close();
                    if (query != null)
                        query.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        private String FETCH_DATA_STATEMENT = "SELECT INSTITUTIONNAME, INSTITUTIONPROFILEID FROM INSTITUTIONPROFILE";

    }


    
    /**
     * 
     * 
     */
    static class CbFilterOptionsOptionList extends OptionList {
        /**
         * 
         * 
         */
        CbFilterOptionsOptionList() {
        }

        // --Release2.1--//
        // Convert to populate the Options from ResourceBundle
        public void populate(RequestContext rc) {
            try {
                // Get Language from SessionState
                // Get the Language ID from the SessionStateModel
                String defaultInstanceStateName = rc.getModelManager()
                        .getDefaultModelInstanceName(SessionStateModel.class);
                SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
                        .getModelManager().getModel(SessionStateModel.class,
                                defaultInstanceStateName, true);
                int languageId = theSessionState.getLanguageId();

                // Get IDs and Labels from BXResources
                Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                        "TASKSTATUS", languageId);
                Iterator l = c.iterator();
                String[] theVal = new String[2];

                while (l.hasNext()) {
                    theVal = (String[]) (l.next());
                    add(theVal[1], theVal[0]);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

}
