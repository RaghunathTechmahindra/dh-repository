package mosApp.MosSystem;

import java.sql.Clob;
import java.sql.SQLException;

import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.log.SysLog;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

public class doSignConditionModelImplNoVerbiage extends QueryModelBase
	implements doSignConditionModel
{
	/**
	 *
	 *
	 */
	public doSignConditionModelImplNoVerbiage()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
		
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public String getDfDocumentLabel()
	{
		return (String)getValue(FIELD_DFDOCUMENTLABEL);
	}


	/**
	 *
	 *
	 */
	public void setDfDocumentLabel(String value)
	{
		setValue(FIELD_DFDOCUMENTLABEL,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDocumentText()
	{
		return (String)getValue(FIELD_DFDOCUMENTTEXT);
	}

	/**
	 *
	 *
	 */
	public void setDfDocumentText(String value)
	{
		setValue(FIELD_DFDOCUMENTTEXT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAction()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFACTION);
	}


	/**
	 *
	 *
	 */
	public void setDfAction(java.math.BigDecimal value)
	{
		setValue(FIELD_DFACTION,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDefDocTrackId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEFDOCTRACKID);
	}


	/**
	 *
	 *
	 */
	public void setDfDefDocTrackId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEFDOCTRACKID,value);
	}

  //--Release2.1--//
  //--> Added LanguagePreferenceId DataField
  public java.math.BigDecimal getDfLanguagePrederenceId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLANGUAGEPREFERENCEID);
	}

	public void setDfLanguagePrederenceId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLANGUAGEPREFERENCEID,value);
	}

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //--> Added new LanguageId datafield in order to setup Language Criteria when page displayed
  //--> By Billy 26Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL DOCUMENTTRACKINGVERBIAGE.DOCUMENTLABEL, DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXT, DOCUMENTTRACKING.DEALID, DOCUMENTTRACKING.COPYID, DOCUMENTTRACKING.DOCUMENTSTATUSID, DOCUMENTTRACKINGVERBIAGE.DOCUMENTTRACKINGID FROM DOCUMENTTRACKING, DOCUMENTTRACKINGVERBIAGE, CONDITION  __WHERE__  ORDER BY DOCUMENTTRACKING.DOCUMENTTRACKINGID  ASC";
/*	CIBC DTV
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL DOCUMENTTRACKINGVERBIAGE.DOCUMENTLABEL, DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXT, "+
    "DOCUMENTTRACKING.DEALID, DOCUMENTTRACKING.COPYID, DOCUMENTTRACKING.DOCUMENTSTATUSID, "+
    //--> Added LanguagePreferenceId
    "DOCUMENTTRACKINGVERBIAGE.DOCUMENTTRACKINGID, DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID "+
    "FROM DOCUMENTTRACKING, DOCUMENTTRACKINGVERBIAGE, CONDITION  "+
    "__WHERE__  "+
    "ORDER BY DOCUMENTTRACKING.DOCUMENTTRACKINGID  ASC";
*/
	public static final String SELECT_SQL_TEMPLATE = 
		"SELECT NVL(documenttrackingverbiage.DOCUMENTLABEL ,conditionverbiage.conditionlabel)DOCUMENTLABEL, " + 
        	   "null documenttext, " + 
        	   "documenttracking.dealid, " + 
        	   "documenttracking.copyid, " + 
        	   "documenttracking.documentstatusid, " + 
        	   "documenttracking.documenttrackingid, " + 
        	   "conditionverbiage.languagepreferenceid " + 
        "FROM condition " + 
        "JOIN conditionverbiage " + 
          "ON condition.conditionid = conditionverbiage.conditionid " + 
          "AND condition.institutionprofileid = conditionverbiage.institutionprofileid " + 
        "JOIN documenttracking " + 
          "ON condition.conditionid = documenttracking.conditionid " + 
          "AND condition.institutionprofileid = documenttracking.institutionprofileid " + 
        "LEFT OUTER JOIN documenttrackingverbiage " + 
          "ON documenttracking.documenttrackingid = documenttrackingverbiage.documenttrackingid " + 
         "AND documenttracking.copyid = documenttrackingverbiage.copyid " + 
         "AND conditionverbiage.languagepreferenceid = documenttrackingverbiage.languagepreferenceid " + 
         "AND conditionverbiage.institutionprofileid = documenttrackingverbiage.institutionprofileid " + 
       " __WHERE__ " + 
       "ORDER BY documenttracking.documenttrackingid";

	
  public static final String MODIFYING_QUERY_TABLE_NAME="DOCUMENTTRACKING, DOCUMENTTRACKINGVERBIAGE, CONDITION";
	//--Release2.1--//
  //--> Take out the HardCoded Criteria, DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID = 0
  //public static final String STATIC_WHERE_CRITERIA=" (((CONDITION.CONDITIONID  =  DOCUMENTTRACKING.CONDITIONID) AND (DOCUMENTTRACKING.DOCUMENTTRACKINGID  =  DOCUMENTTRACKINGVERBIAGE.DOCUMENTTRACKINGID) AND (DOCUMENTTRACKING.COPYID  =  DOCUMENTTRACKINGVERBIAGE.COPYID)) AND DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID = 0 AND DOCUMENTTRACKING.DOCUMENTTRACKINGSOURCEID = 0 AND CONDITION.CONDITIONTYPEID = 1 AND CONDITION.CONDITIONID = 55) ";
	public static final String STATIC_WHERE_CRITERIA=
    "DOCUMENTTRACKING.DOCUMENTTRACKINGSOURCEID = 0 AND "+
    "CONDITION.CONDITIONTYPEID = 1 AND "+
    "CONDITION.CONDITIONID = 55 ";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDOCUMENTLABEL="DOCUMENTLABEL";
	public static final String COLUMN_DFDOCUMENTLABEL="DOCUMENTLABEL";
	public static final String QUALIFIED_COLUMN_DFDOCUMENTTEXT="DOCUMENTTEXT";
	public static final String COLUMN_DFDOCUMENTTEXT="DOCUMENTTEXT";
	public static final String QUALIFIED_COLUMN_DFDEALID="DOCUMENTTRACKING.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DOCUMENTTRACKING.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFACTION="DOCUMENTTRACKING.DOCUMENTSTATUSID";
	public static final String COLUMN_DFACTION="DOCUMENTSTATUSID";
	public static final String QUALIFIED_COLUMN_DFDEFDOCTRACKID="DOCUMENTTRACKING.DOCUMENTTRACKINGID";
	public static final String COLUMN_DFDEFDOCTRACKID="DOCUMENTTRACKINGID";
  //--Release2.1--//
  //--> Added LanguagePreferenceId DataField
  public static final String QUALIFIED_COLUMN_DFLANGUAGEPREFERENCEID="CONDITIONVERBIAGE.LANGUAGEPREFERENCEID";
	public static final String COLUMN_DFLANGUAGEPREFERENCEID="LANGUAGEPREFERENCEID";



	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOCUMENTLABEL,
				COLUMN_DFDOCUMENTLABEL,
				QUALIFIED_COLUMN_DFDOCUMENTLABEL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOCUMENTTEXT,
				COLUMN_DFDOCUMENTTEXT,
				QUALIFIED_COLUMN_DFDOCUMENTTEXT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFACTION,
				COLUMN_DFACTION,
				QUALIFIED_COLUMN_DFACTION,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEFDOCTRACKID,
				COLUMN_DFDEFDOCTRACKID,
				QUALIFIED_COLUMN_DFDEFDOCTRACKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    //--Release2.1--//
    //--> Added LanguagePreferenceId DataField
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLANGUAGEPREFERENCEID,
				COLUMN_DFLANGUAGEPREFERENCEID,
				QUALIFIED_COLUMN_DFLANGUAGEPREFERENCEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}
