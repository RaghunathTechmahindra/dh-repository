package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;

/**
 *
 *
 *
 */
public class doDealMITypeModelImpl extends QueryModelBase
	implements doDealMITypeModel
{
	/**
	 *
	 *
	 */
	public doDealMITypeModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


    /**
     *
     *
     */
    public java.math.BigDecimal getDfCopyId()
    {
        return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
    }


    /**
     *
     *
     */
    public void setDfCopyId(java.math.BigDecimal value)
    {
        setValue(FIELD_DFCOPYID,value);
    }


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMITypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMITYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfMITypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMITYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMIMortgageTypeDescription()
	{
		return (String)getValue(FIELD_DFMIMORTGAGETYPEDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfMIMortgageTypeDescription(String value)
	{
		setValue(FIELD_DFMIMORTGAGETYPEDESCRIPTION,value);
	}

	/**
	 * MetaData object test method to verify the SQL_TEMPLATE query.
	 *
	 */
      /**
        public void setResultSet(ResultSet value)
        {
            logger = SysLog.getSysLogger("METADATA");

            try
            {
                super.setResultSet(value);
                if( value != null)
                {
                    ResultSetMetaData metaData = value.getMetaData();
                    int columnCount = metaData.getColumnCount();
                    logger.debug("===============================================");
                    logger.debug("Testing MetaData Object for new synthetic fields for" +
	                 	            " doDealMITypeModel");
                    logger.debug("NumberColumns: " + columnCount);
                    for(int i = 0; i < columnCount ; i++)
                    {
                        logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
                    }
                    logger.debug("================================================");
                  }
              }
              catch (SQLException e)
              {
                  logger.debug("Class: " + getClass().getName());
                  logger.debug("SQLException: " + e);
              }
        }
        **/

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL DEAL.DEALID, DEAL.COPYID, MORTGAGEINSURANCETYPE.MORTGAGEINSURANCETYPEID, MORTGAGEINSURANCETYPE.MTDESCRIPTION FROM DEAL, MORTGAGEINSURANCETYPE  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="DEAL, MORTGAGEINSURANCETYPE";
	public static final String STATIC_WHERE_CRITERIA=" (DEAL.MITYPEID  =  MORTGAGEINSURANCETYPE.MORTGAGEINSURANCETYPEID)";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFMITYPEID="MORTGAGEINSURANCETYPE.MORTGAGEINSURANCETYPEID";
	public static final String COLUMN_DFMITYPEID="MORTGAGEINSURANCETYPEID";
	public static final String QUALIFIED_COLUMN_DFMIMORTGAGETYPEDESCRIPTION="MORTGAGEINSURANCETYPE.MTDESCRIPTION";
	public static final String COLUMN_DFMIMORTGAGETYPEDESCRIPTION="MTDESCRIPTION";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMITYPEID,
				COLUMN_DFMITYPEID,
				QUALIFIED_COLUMN_DFMITYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIMORTGAGETYPEDESCRIPTION,
				COLUMN_DFMIMORTGAGETYPEDESCRIPTION,
				QUALIFIED_COLUMN_DFMIMORTGAGETYPEDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

