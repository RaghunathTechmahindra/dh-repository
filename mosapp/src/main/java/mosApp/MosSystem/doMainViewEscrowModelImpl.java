package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doMainViewEscrowModelImpl extends QueryModelBase
	implements doMainViewEscrowModel
{
	/**
	 *
	 *
	 */
	public doMainViewEscrowModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 19Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert ESCROWTYPE Desc.
      this.setDfEscrowType(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "ESCROWTYPE", this.getDfEscrowType(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEscrowType()
	{
		return (String)getValue(FIELD_DFESCROWTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfEscrowType(String value)
	{
		setValue(FIELD_DFESCROWTYPE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEscrowPaymentDesc()
	{
		return (String)getValue(FIELD_DFESCROWPAYMENTDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfEscrowPaymentDesc(String value)
	{
		setValue(FIELD_DFESCROWPAYMENTDESC,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEscrowPayment()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFESCROWPAYMENT);
	}


	/**
	 *
	 *
	 */
	public void setDfEscrowPayment(java.math.BigDecimal value)
	{
		setValue(FIELD_DFESCROWPAYMENT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}

  public java.math.BigDecimal getDfInstitutionId() {
	    return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
	  }

	  public void setDfInstitutionId(java.math.BigDecimal value) {
	    setValue(FIELD_DFINSTITUTIONID, value);
	  }



	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 19Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
	//public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT ESCROWPAYMENT.DEALID, ESCROWTYPE.ESCROWTYPEDESCRIPTION, ESCROWPAYMENT.ESCROWPAYMENTDESCRIPTION, ESCROWPAYMENT.ESCROWPAYMENTAMOUNT, ESCROWPAYMENT.COPYID FROM ESCROWPAYMENT, ESCROWTYPE  __WHERE__  ";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT DISTINCT ESCROWPAYMENT.DEALID, to_char(ESCROWPAYMENT.ESCROWTYPEID) ESCROWTYPEID_STR, "+
    "ESCROWPAYMENT.ESCROWPAYMENTDESCRIPTION, ESCROWPAYMENT.ESCROWPAYMENTAMOUNT, "+
    "ESCROWPAYMENT.COPYID, "+
    "ESCROWPAYMENT.INSTITUTIONPROFILEID "+
    " FROM ESCROWPAYMENT  "+
    "__WHERE__  ";
	//--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //public static final String MODIFYING_QUERY_TABLE_NAME="ESCROWPAYMENT, ESCROWTYPE";
  public static final String MODIFYING_QUERY_TABLE_NAME="ESCROWPAYMENT";
  //--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
	//public static final String STATIC_WHERE_CRITERIA=" (ESCROWPAYMENT.ESCROWTYPEID  =  ESCROWTYPE.ESCROWTYPEID)";
	public static final String STATIC_WHERE_CRITERIA=
    "";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="ESCROWPAYMENT.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFESCROWPAYMENTDESC="ESCROWPAYMENT.ESCROWPAYMENTDESCRIPTION";
	public static final String COLUMN_DFESCROWPAYMENTDESC="ESCROWPAYMENTDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFESCROWPAYMENT="ESCROWPAYMENT.ESCROWPAYMENTAMOUNT";
	public static final String COLUMN_DFESCROWPAYMENT="ESCROWPAYMENTAMOUNT";
	public static final String QUALIFIED_COLUMN_DFCOPYID="ESCROWPAYMENT.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";

	//--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFESCROWTYPE="ESCROWTYPE.ESCROWTYPEDESCRIPTION";
	//public static final String COLUMN_DFESCROWTYPE="ESCROWTYPEDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFESCROWTYPE="ESCROWPAYMENT.ESCROWTYPEID_STR";
	public static final String COLUMN_DFESCROWTYPE="ESCROWTYPEID_STR";

	  public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
		    "ESCROWPAYMENT.INSTITUTIONPROFILEID";
		  public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFESCROWTYPE,
				COLUMN_DFESCROWTYPE,
				QUALIFIED_COLUMN_DFESCROWTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFESCROWPAYMENTDESC,
				COLUMN_DFESCROWPAYMENTDESC,
				QUALIFIED_COLUMN_DFESCROWPAYMENTDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFESCROWPAYMENT,
				COLUMN_DFESCROWPAYMENT,
				QUALIFIED_COLUMN_DFESCROWPAYMENT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	    FIELD_SCHEMA.addFieldDescriptor(
	            new QueryFieldDescriptor(
	              FIELD_DFINSTITUTIONID,
	              COLUMN_DFINSTITUTIONID,
	              QUALIFIED_COLUMN_DFINSTITUTIONID,
	              java.math.BigDecimal.class,
	              false,
	              false,
	              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	              "",
	              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	              ""));
	        

	}

}

