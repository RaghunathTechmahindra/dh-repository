package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *  TD_MWQ_CR.
 *  TD wants to see the new summary section counted by TaskName
 *  (currently only two tasks: all open Decision Deal
 *                         and Source Resubmition.
 *
 */
public interface doTasksOutstandingsModel extends QueryModel, SelectQueryModel
{


  public String getDfTaskNameLabel();

	/**
	 *
	 *
	 */
	public void setDfTaskNameLabel(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTaskId();


	/**
	 *
	 *
	 */
	public void setDfTaskId(java.math.BigDecimal value);


	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFTASKNAMELABEL="dfTaskNameLabel";
	public static final String FIELD_DFTASKID="dfTaskId";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

