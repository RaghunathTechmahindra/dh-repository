package mosApp.MosSystem;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Vector;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.query.DealQuery;
import com.basis100.deal.query.DealQueryResult;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.security.DealStatusManager;
import com.basis100.deal.util.Parameters;


import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
//==============================================================================
//  Note : PageCondition1 and PageCondition2 is already used for PageCursor handling
//  Usage of PageCondition3
//    - true  : Submit button was pressed and need to re-query and refresh display
//              even if Search Criteria changed
//    - false : Re-query only if the Search Criteria changed
//==============================================================================

public class DealSearchHandler extends PageHandlerCommon
	implements Cloneable, Sc
{

	public DealSearchHandler()
	{
    super();
    theSessionState = super.theSessionState;
    savedPages = super.savedPages;
	}

	public DealSearchHandler(String name)
	{
		this(null,name); // No parent
    theSessionState = super.theSessionState;
    savedPages = super.savedPages;
	}

	protected DealSearchHandler(View parent, String name)
	{
		super(parent,name);
    theSessionState = super.theSessionState;
    savedPages = super.savedPages;
	}


	// key for storing fields edit buffer in page state table
  private static final String EDIT_BUFFER="EDIT_BUFFER";

	/**
	 *
	 *
	 */
	public DealSearchHandler cloneSS()
	{
		return(DealSearchHandler) super.cloneSafeShallow();

	}


	///////////////////////////////////////////////////////////////////////

	public void saveData(PageEntry currPage, boolean calledInTransaction)
	{
		PageCursorInfo tds = getPageCursorInfo(currPage, "DEFAULT");

		Hashtable pst = currPage.getPageStateTable();

		EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(EDIT_BUFFER);

		efb.readFields(getCurrNDPage());

		// we never mark as modified - allow user to walk away without fuss!
		currPage.setModified(false);

	}


	///////////////////////////////////////////////////////////////////////

	public void preHandlerProtocol(ViewBean currNDPage)
	{
    //--> Not necessary to do the following in JATO
    //--> Commented out by BILLY 08Aug2002
    super.preHandlerProtocol(currNDPage);
    PageEntry pg = theSessionState.getCurrentPage();

		// sefault page load method to display()
		////pg.setPageDisplayMethod(Mc.DISPLAY_METHOD_DISP);

 		//// SYNCADD.
 		// Allow search even if not set to not Editable (Force to call savedata)
 		//  -- By Billy 10June2002
 		if (pg.isEditable() == false)
          this.saveData(pg, false);

	}

	///////////////////////////////////////////////////////////////////////
	// This method is used to populate the fields in the header
	// with the appropriate information

	public void populatePageDisplayFields()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		Hashtable pst = pg.getPageStateTable();

		populatePageShellDisplayFields();

		// Quick Link Menu display
		displayQuickLinkMenu();
		
		populatePreviousPagesLinks();
		
		revisePrevNextButtonGeneration(pg, tds);

		// populate current search criteria
		EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(EDIT_BUFFER);

		efb.populateFields(getCurrNDPage());

		// generate tasks displayed display field
		populateRowsDisplayed(getCurrNDPage(), tds);

	}


	///////////////////////////////////////////////////////////////////////

	private void populateRowsDisplayed(ViewBean NDPage, PageCursorInfo tds)
	{
		String rowsRpt = "";

		if (tds.getTotalRows() > 0)
    {
      rowsRpt = BXResources.getSysMsg("DP_DEALSEARCHROWSDISPLAYED", theSessionState.getLanguageId());

      try
      {
        rowsRpt = com.basis100.deal.util.Format.printf(rowsRpt,
                new Parameters(tds.getFirstDisplayRowNumber())
                .add(tds.getLastDisplayRowNumber())
                .add(tds.getTotalRows()));
      }
      catch (Exception e)
      {
        logger.warning("String Format error @ DealSearchHandler.populateRowsDisplayed: " + e);
      }
    }

		NDPage.setDisplayFieldValue("rowsDisplayed", new String(rowsRpt));
	}


	///////////////////////////////////////////////////////////////////////
  //--> Modified to return boolean -- By Billy 09Aug2002
	public boolean populateRepeatedFields(int ndx)
	{

		//logger.debug("@DealSearchHandler.populateRepeatedFields: relative page row ndx=" + ndx);
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");
			if (ndx < 0 || tds.getTotalRows() <= 0 || ndx >= tds.getNumberOfDisplayRowsOnPage()) return false;

			//logger.debug("tds:: total currPage  currRel currAbs lastPage currRows :: " + tds.getTotalRows()
			//          + " " + tds.getCurrPageNdx() + " " + tds.getRelSelectedRowNdx() + " "
			//          + tds.getAbsSelectedRowNdx() + " " + tds.isLastPage() + " " + tds.getNumberOfDisplayRowsOnPage());
			ndx = getCursorAbsoluteNdx(tds, ndx);

			//logger.debug("tds:: currAbs (computed) :: " +  ndx);
			Vector results =(Vector) pst.get("RESULTS");

			//			logger.debug("results:: size == " + results.size());
			DealSearchResultRow dsrr =
        new DealSearchResultRow("RROW" + ndx, logger,(DealQueryResult) results.elementAt(ndx),
        this.theSessionState.getLanguageId(), theSessionState.isMultiAccess());

      //--> Caution !! this may not work !! Need to try it out ??
      //--> Comment by Billy 09Aug2002
      dsrr.populateFields(getCurrNDPage());
      //=========================================================

			return true;
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSearchHandler.populateRepeatedFields: populating results row (ndx=" + ndx + ")");
			logger.error(e);
		}

		return false;

	}

	///////////////////////////////////////////////////////////////////////
	/*
	 * Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
	*/
	public void setupBeforePageGeneration()
	{
		logger.trace("--T--> @DealSearchHandler.setupBeforePageGeneration");

		PageEntry pg = getTheSessionState().getCurrentPage();

		if (pg.getSetupBeforeGenerationCalled() == true) return;

		pg.setSetupBeforeGenerationCalled(true);

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		Hashtable pst = pg.getPageStateTable();

		EditFieldsBuffer efb = null;


		// our (main) page has no DO bindings so row generator (2) was used to
		// create one - because we rely on binding to trigger Object Framework methods
		setRowGenerator2DOForNRows(1);


		// search results display state (corresponds to list of deals matching criteria ::
		// this object is the default (named "DEFAULT") for this page
		if (tds == null)
		{
			logger.trace("--T--> @DealSearchHandler.setupBeforePageGeneration: first invocation");

			// first invocation!
			// determine number of notes (shown) per display page
			String voName = "Repeated1";
			int perPage =((TiledView)(getCurrNDPage().getChild(voName))).getMaxDisplayTiles();

			// set up and save task display state object
			tds = new PageCursorInfo("DEFAULT", voName, perPage, - 1);
			tds.setRefresh(true);
			tds.setCriteria("");
			pst.put(tds.getName(), tds);

			// edit buffer for screen fields
			efb = new DealSearchEditBuffer("DealSearchParms", logger);
			pst.put(EDIT_BUFFER, efb);

			// save the search page as saved page
			getSavedPages().setSearchPage(pg);
		}


		//  get the edit buffer
		efb =(EditFieldsBuffer) pst.get(EDIT_BUFFER);


		// chech for search criteria present
		int numResults = 0;

		if (isSearchCriteria(efb) != false)
		{
			// check for change in search criteria
			tds.setCriteria(efb.getAsCriteria());
			logger.trace("--T--> @DealSearchHandler.setupBeforePageGeneration: have search criteria");
			numResults = - 1;

			// to keep previous value!
			logger.trace("--T--> @DealSearchHandler.setupBeforePageGeneration: criteriaChanged = " + tds.isCriteriaChanged() + " getPageCondition3 = " + pg.getPageCondition3());
			if (tds.isCriteriaChanged() == true || pg.getPageCondition3() == true)
			{
				// Reset PageCondition3 -- prevent re-query
				pg.setPageCondition3(false);
				try
				{
					// re-query and store results()
					DealQuery dq = new DealQuery(getSessionResourceKit());
					boolean flags [ ] = getSearchCriteriaFlags(efb);
					logger.trace("--T--> @DealSearchHandler.setupBeforePageGeneration: performing search:: " + flags [ 0 ] + " " + flags [ 1 ] + " " + flags [ 2 ] + " " + flags [ 3 ] + " " + flags [ 4 ]);
					Vector results = dq.query(flags,
// Modified to add search for All Borrowers -- by BILLY 01March2002
					  efb.getFieldValue("rbPrimaryBorrowerOnly").equals("Y"),
            efb.getFieldValue("tbBorrowerLastName"),
            efb.getFieldValue("tbBorrowerFirstName"),
            efb.getFieldValue("tbBorrowerMailingAddress"),
            getPhoneSearchString(efb.getFieldValue("tbBorrowerHomePhoneNumber1"), efb.getFieldValue("tbBorrowerHomePhoneNumber2"), efb.getFieldValue("tbBorrowerHomePhoneNumber3")),
            getPhoneSearchString(efb.getFieldValue("tbBorrowerWorkPhoneNumber1"), efb.getFieldValue("tbBorrowerWorkPhoneNumber2"), efb.getFieldValue("tbBorrowerWorkPhoneNumber3")),
            efb.getFieldValue("tbPropertyStreetNumber"),
            efb.getFieldValue("tbPropertyStreetName"),
            efb.getFieldValue("tbPropertyCity"),
            efb.getFieldValue("tbSourceName"),
            efb.getFieldValue("tbUnderwriter"),
            efb.getFieldValue("tbAdministrator"),
            efb.getFieldValue("tbFunder"),
            efb.getFieldValue("tbDealNumber"),
            efb.getFieldValue("tbSourceAppId"),
            //--> Ticket#129 -- Support search by Servicing Mortgage #
            //--> By Billy 26Nov2003
            efb.getFieldValue("tbServicingNum")
            //========================================================
            );
					logger.debug("--D--> query parms --> "
            + "\"" + efb.getFieldValue("rbPrimaryBorrowerOnly").equals("Y") + " \", "
            + "\"" + efb.getFieldValue("tbBorrowerLastName") + "\", "
            + "\"" + "\"" + efb.getFieldValue("tbBorrowerFirstName") + "\", "
            + "\"" + efb.getFieldValue("tbBorrowerMailingAddress") + "\", "
            + "\"" + getPhoneSearchString(efb.getFieldValue("tbBorrowerHomePhoneNumber1"), efb.getFieldValue("tbBorrowerHomePhoneNumber2"), efb.getFieldValue("tbBorrowerHomePhoneNumber3")) + "\", "
            + "\"" + getPhoneSearchString(efb.getFieldValue("tbBorrowerWorkPhoneNumber1"), efb.getFieldValue("tbBorrowerWorkPhoneNumber2"), efb.getFieldValue("tbBorrowerWorkPhoneNumber3")) + "\", "
            + "\"" + efb.getFieldValue("tbPropertyStreetNumber") + "\", "
            + "\"" + efb.getFieldValue("tbPropertyStreetName") + "\", "
            + "\"" + efb.getFieldValue("tbPropertyCity") + "\", "
            + "\"" + efb.getFieldValue("tbSourceName") + "\", "
            + "\"" + efb.getFieldValue("tbUnderwriter") + "\", "
            + "\"" + efb.getFieldValue("tbAdministrator") + "\", "
            + "\"" + efb.getFieldValue("tbFunder") + "\", "
            + "\"" + efb.getFieldValue("tbDealNumber") + "\", "
            + "\"" + efb.getFieldValue("tbSourceAppId") + "\", "
            + "\"" + efb.getFieldValue("tbServicingNum") + "\"");
					logger.debug("--D--> results: " +((results == null) ? "null" :("" + results.size() + "rows")));
					if (results == null || results.size() == 0)
					{
						numResults = 0;
						pst.put("RESULTS", new Vector());
						setActiveMessageToAlert(BXResources.getSysMsg("DEAL_SEARCH_NO_MATCH", theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
					}
					else
					{
						// store search results ...
						numResults = results.size();

						// but populate deal status description (via DSM) first
						DealStatusManager dsm = DealStatusManager.getInstance(getSessionResourceKit());
						DealQueryResult dqr = null;
						for(int i = 0; i < numResults; ++ i)
						{
							dqr =(DealQueryResult) results.elementAt(i);

 							//// SYNCADD.
 							// New required by Product to add DenialReason :
 							// if (Deal.statusid = 24) or (Deal.statusid = 23 and DenialReason.denialreasonid <> 0)
 							// -- Have to detach the "stDealStatus" from the screen to take effect !!
 							// -- By Billy 21Aug2002
 							int theStatusId = dqr.getStatusId();

 							int theDenResId = dqr.getDenialReasonId();

              //--Release2.1--//
 							//String tmpStatusDec = PicklistData.getDescription("status", theStatusId);
              int langID = getTheSessionState().getLanguageId();

              String tmpStatusDec =
                BXResources.getPickListDescription(this.theSessionState.getUserInstitutionId(),"STATUS", theStatusId, langID);
              //--------------//

 							if (theStatusId == Mc.DEAL_DENIED ||
                  (theStatusId == Mc.DEAL_COLLAPSED && theDenResId != Mc.DENIAL_REASON_OTHER))
 							{
                //--Release2.1--//
 								//tmpStatusDec = tmpStatusDec + "/" + PicklistData.getDescription("denialreason", theDenResId);
                tmpStatusDec = tmpStatusDec + "/" +
                  BXResources.getPickListDescription(this.theSessionState.getUserInstitutionId(),"DENIALREASON", theDenResId, langID);
                //--------------//
 							}
 							logger.debug("DSH@SetupBeforePageGeneration::StatusDescription: " + tmpStatusDec);

              dqr.setStatusDesc(tmpStatusDec);
 							////========================================================================================
						}
						tds.setTotalRows(numResults);
						setRowGeneratorDOForNRows(numResults);
						pst.put("RESULTS", results);

						// ensure display from first page
            tds.setCurrPageNdx(0);

						TiledView vo =(TiledView)(getCurrNDPage().getChild(tds.getVoName()));
						try
            {
              vo.resetTileIndex();
            }
            catch (ModelControlException mce)
            {
              logger.error("DealSearchHandler@setupBeforePageGeneration::Exception when reset TiledView to display the first row: " + mce);
            }
					}
				}
				catch(Exception e)
				{
					numResults = 0;
					setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
					logger.error("Exception @DealSearchHandler.setupBeforePageGeneration: searching");
					logger.error(e.getMessage());
				}
			}
			else
			{
				// Bug fix -- to set the rows to display if not re-query again -- BILLY 01March2002
				setRowGeneratorDOForNRows(tds.getTotalRows());
			}
		}

		if (numResults == 0)
		{
			tds.setTotalRows(0);
			setRowGeneratorDOForNRows(0);
			pst.put("RESULTS", new Vector());
		}

	}


	///////////////////////////////////////////////////////////////////////

	public void handleForwardButton()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

		handleForwardBackwardCommon(pg, tds, - 1, true);

		return;

	}


	///////////////////////////////////////////////////////////////////////

	public void handleBackwardButton()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

		handleForwardBackwardCommon(pg, tds, - 1, false);

		return;

	}


	///////////////////////////////////////////////////////////////////////
	// called to open the deal summary page for the deal selected (from search results)
	//  - <dealNdx> is relative to the results being displayed on the the page, not absolute.

	public void handleSelectDeal(int dealNdx)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");
		Exception selectException = null;

		boolean success = false;

		try
		{
			if (tds.getTotalRows() <= 0) return;
			tds.setRelSelectedRowNdx(dealNdx);
			int absNdx = tds.getAbsSelectedRowNdx();
			DealQueryResult dqr =(DealQueryResult)((Vector) pst.get("RESULTS")).elementAt(absNdx);
			int dealId = dqr.getDealId();
			int institutionId = dqr.getInstitutionProfileId();
			PageEntry pgEntry = setupMainPagePageEntry(Sc.PGNM_DEAL_SUMMARY_ID, dealId, -1, institutionId);
			if (pgEntry != null)
			{
				getSavedPages().setNextPage(pgEntry);
				navigateToNextPage();
				success = true;
			}
		}
		catch(Exception e)
		{
			selectException = e;
		}

		if (success == false)
		{
			logger.error("@DealSearchHandler.handleSelectDeal: Error encountered opening deal summary from deal search");
			if (selectException != null) logger.error(selectException);
			setStandardFailMessage();
		}

		return;

	}


	///////////////////////////////////////////////////////////////////////

	public void handleCancelButton()
	{
		handleCancelStandard();

	}


	///////////////////////////////////////////////////////////////////////

	public boolean checkPassiveUser()
	{
		if (getTheSessionState().getSessionUserType() == Mc.USER_TYPE_PASSIVE)
		{
			return false;
		}

		return true;

	}


	///////////////////////////////////////////////////////////////////////

	public void handleBtSearchClickEvent()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		Object obj = null;

		EditFieldsBuffer efb = new DealSearchEditBuffer("DealSearchParms", logger);

		obj = pst.get(EDIT_BUFFER);

		if (obj instanceof EditFieldsBuffer) efb =(EditFieldsBuffer) obj;

		if (! isSearchCriteria(efb))
		{
			setActiveMessageToAlert(BXResources.getSysMsg("DEAL_SEARCH_INPUT_REQUIRED", theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			return;
		}
		else
		{
			// Set PageCondition3 to force re-query
			pg.setPageCondition3(true);
		}

		return;

	}


	///////////////////////////////////////////////////////////////////////

	private boolean isSearchCriteria(EditFieldsBuffer efb)
	{
		boolean [ ] flags = getSearchCriteriaFlags(efb);

		int lim = flags.length;

		for(int i = 0;
		i < lim;
		++ i)
		{
			if (flags [ i ] == true) return true;
		}

		return false;

	}


	///////////////////////////////////////////////////////////////////////
	//
	// Returns array as follows:
	//
	//  [0] - true/false - search by borrower criteria present
	//  [1] - true/false - search by property criteria present
	//  [2] - true/false - search by source of business criteria present
	//  [3] - true/false - search by underwriter  criteria present
	//  [4] - true/false - search by deal (number) criteria present
	//  [5] - true/false - search by Administrator  criteria present
	//  [6] - true/false - search by Funder criteria present
	//

	private boolean[] getSearchCriteriaFlags(EditFieldsBuffer efb)
	{
		boolean flags [ ] = new boolean [ 7 ];

		flags [ 0 ] = isBorrowerSearchCriteria(efb);

		flags [ 1 ] = isPropertySearchCriteria(efb);

		flags [ 2 ] = isSourceSearchCriteria(efb);

		flags [ 3 ] = isUWSearchCriteria(efb);

		flags [ 4 ] = isDealSearchCriteria(efb);

		flags [ 5 ] = isAdminSearchCriteria(efb);

		flags [ 6 ] = isFunderSearchCriteria(efb);

		return flags;

	}


	///////////////////////////////////////////////////////////////////////

	private boolean isBorrowerSearchCriteria(EditFieldsBuffer efb)
	{
		String s = efb.getFieldValue("tbBorrowerFirstName");

		if (s != null && s.length() > 0) return true;

		s = efb.getFieldValue("tbBorrowerLastName");

		if (s != null && s.length() > 0) return true;

		s = efb.getFieldValue("tbBorrowerMailingAddress");

		if (s != null && s.length() > 0) return true;

		s = efb.getFieldValue("tbBorrowerHomePhoneNumber1");

		if (s != null && s.length() > 0) return true;

		s = efb.getFieldValue("tbBorrowerHomePhoneNumber2");

		if (s != null && s.length() > 0) return true;

		s = efb.getFieldValue("tbBorrowerHomePhoneNumber3");

		if (s != null && s.length() > 0) return true;

		s = efb.getFieldValue("tbBorrowerWorkPhoneNumber1");

		if (s != null && s.length() > 0) return true;

		s = efb.getFieldValue("tbBorrowerWorkPhoneNumber2");

		if (s != null && s.length() > 0) return true;

		s = efb.getFieldValue("tbBorrowerWorkPhoneNumber3");

		if (s != null && s.length() > 0) return true;

		return false;

	}


	///////////////////////////////////////////////////////////////////////

	private boolean isPropertySearchCriteria(EditFieldsBuffer efb)
	{
		String s = efb.getFieldValue("tbPropertyStreetNumber");

		if (s != null && s.length() > 0) return true;

		s = efb.getFieldValue("tbPropertyStreetName");

		if (s != null && s.length() > 0) return true;

		s = efb.getFieldValue("tbPropertyCity");

		if (s != null && s.length() > 0) return true;

		return false;

	}


	///////////////////////////////////////////////////////////////////////

	private boolean isSourceSearchCriteria(EditFieldsBuffer efb)
	{
		String s = efb.getFieldValue("tbSourceName");

		if (s != null && s.length() > 0) return true;

		return false;

	}


	///////////////////////////////////////////////////////////////////////

	private boolean isUWSearchCriteria(EditFieldsBuffer efb)
	{
		String s = efb.getFieldValue("tbUnderwriter");

		if (s != null && s.length() > 0) return true;

		return false;

	}


	///////////////////////////////////////////////////////////////////////

	private boolean isAdminSearchCriteria(EditFieldsBuffer efb)
	{
		String s = efb.getFieldValue("tbAdministrator");

		if (s != null && s.length() > 0) return true;

		return false;

	}


	///////////////////////////////////////////////////////////////////////

	private boolean isFunderSearchCriteria(EditFieldsBuffer efb)
	{
		String s = efb.getFieldValue("tbFunder");

		if (s != null && s.length() > 0) return true;

		return false;

	}


	///////////////////////////////////////////////////////////////////////

	private boolean isDealSearchCriteria(EditFieldsBuffer efb)
	{
		String s = efb.getFieldValue("tbDealNumber");
		if (s != null && s.length() > 0) return true;

		s = efb.getFieldValue("tbSourceAppId");
		if (s != null && s.length() > 0) return true;

    //--> Ticket#129 -- Support search by Servicing Mortgage #
    //--> By Billy 26Nov2003
    s = efb.getFieldValue("tbServicingNum");
		if (s != null && s.length() > 0) return true;

		return false;

	}


	/**
	 *
	 *
	 */
	public void handleSelectDealNumber(int dealNdx)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

		Exception selectException = null;

		boolean success = false;

		try
		{
			if (tds.getTotalRows() <= 0) return;
			tds.setRelSelectedRowNdx(dealNdx);
			int absNdx = tds.getAbsSelectedRowNdx();
			DealQueryResult dqr =(DealQueryResult)((Vector) pst.get("RESULTS")).elementAt(absNdx);
			int dealId = dqr.getDealId();
			int copyId = dqr.getCopyId();
			int institutionId = dqr.getInstitutionProfileId();
			String appId = dqr.getApplicationId();
			logger.debug("The Selected DealId ======= " + dealId);
			logger.debug("The Selected copyid ======= " + copyId);
            logger.debug("The Selected appId ======= " + appId);
            logger.debug("The Selected instId ======= " + institutionId);
			pg.setPageDealId(dealId);
			pg.setPageDealCID(copyId);
			pg.setPageDealApplicationId(appId);
			pg.setDealInstitutionId(institutionId);
		}
		catch(Exception e)
		{
			selectException = e;
		}

		return;

	}


	////////////////////////////////////////////////////////////////////////
  public void checkAndSyncDOWithCursorInfo(RequestHandlingTiledViewBase theRepeat)
	{
		////PageEntry pg = getTheSession().getCurrentPage();
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
 		Hashtable pst = pg.getPageStateTable();
 	  PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");
    try
    {
      //--> So far we cannot find a way to manually set the Diaplay Offset of the TiledView here.
      //--> The reason is some necessary method is protected (e.g. setWebActionModelOffset()). The
      //--> work around is to create our own method (setCurrentDisplayOffset) in the TiledView class
      //--> (e.g. pgIWorkQueueRepeated1TiledView).  However, this method have to be implemented in all
      //--> TiledView which have Forward/Backward buttons.
      //--> Need to investigate more later  !!!!
      //--> By BILLY 22July2002
      ((pgDealSearchRepeated1TiledView)theRepeat).setCurrentDisplayOffset(tds.getFirstDisplayRowNumber() - 1);
    }
    catch(Exception mce)
    {
      logger.debug("DealSearchHandler@checkAndSyncDOWithCursorInfo::Exception: " + mce);
    }
	}

}


/**
 *
 *
 */
class DealSearchEditBuffer extends EditFieldsBuffer
	implements Serializable
{
	private static String[][] buf={
    {"TextBox", "tbDealNumber"},
    {"TextBox", "tbBorrowerFirstName"},
    {"TextBox", "tbBorrowerLastName"},
    {"TextBox", "tbBorrowerMailingAddress"},
    {"TextBox", "tbBorrowerHomePhoneNumber1"},
    {"TextBox", "tbBorrowerHomePhoneNumber2"},
    {"TextBox", "tbBorrowerHomePhoneNumber3"},
    {"TextBox", "tbBorrowerWorkPhoneNumber1"},
    {"TextBox", "tbBorrowerWorkPhoneNumber2"},
    {"TextBox", "tbBorrowerWorkPhoneNumber3"},
    {"TextBox", "tbPropertyStreetNumber"},
    {"TextBox", "tbPropertyStreetName"},
    {"TextBox", "tbPropertyCity"},
    {"TextBox", "tbSourceName"},
    {"TextBox", "tbUnderwriter"},
    {"TextBox", "tbAdministrator"},
    {"TextBox", "tbFunder"},
    {"TextBox", "tbSourceAppId"},
    {"TextBox", "rbPrimaryBorrowerOnly"},
    //--> Ticket#129 -- Support search by Servicing Mortgage #
    //--> By Billy 26Nov2003
    {"TextBox", "tbServicingNum"}
  };

	private String[][] dataBuffer;


	/**
	 *
	 *
	 */
	public DealSearchEditBuffer(String name, SysLogger logger)
	{
		super(name, logger);
		int len = buf.length;

		setNumberOfFields(len);

		dataBuffer = new String [ len ] [ 2 ];

		for(int i = 0; i < len;	++ i)
		{
			for(int j = 0;	j < 2; j ++)
          dataBuffer [ i ] [ j ] = "";
		}

	}


	/**
	 *
	 *
	 */
	public String getFieldName(int fldNdx)
	{
		return buf [ fldNdx ] [ 1 ];

	}


	/**
	 *
	 *
	 */
	public String getDOFieldName(int fldNdx)
	{
		return "";

	}


	/**
	 *
	 *
	 */
	public String getFieldType(int fldNdx)
	{
		return buf [ fldNdx ] [ 0 ];

	}


	/**
	 *
	 *
	 */
	public String getFieldValue(int fldNdx)
	{
		return dataBuffer [ fldNdx ] [ 0 ];

	}


	/**
	 *
	 *
	 */
	public String getOriginalValue(int fldNdx)
	{
		return dataBuffer [ fldNdx ] [ 1 ];

	}


	// --- //

	public void setFieldName(int fldNdx, String value)
	{
		buf [ fldNdx ] [ 1 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setDOFieldName(int fldNdx, String value)
	{
		;

	}


	/**
	 *
	 *
	 */
	public void setFieldType(int fldNdx, String value)
	{
		buf [ fldNdx ] [ 0 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setFieldValue(int fldNdx, String value)
	{
		dataBuffer [ fldNdx ] [ 0 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setOriginalValue(int fldNdx, String value)
	{
		dataBuffer [ fldNdx ] [ 1 ] = value;

	}

}


/**
 *
 *
 */
class DealSearchResultRow extends EditFieldsBuffer
	implements Serializable
{
	private static String[][] buf={
    {"StaticText", "Repeated1/stDealNumber"},
    {"StaticText", "Repeated1/stApplicantName"},
    {"StaticText", "Repeated1/stApplicantHmTel"},
    {"StaticText", "Repeated1/stApplicantAddr"},
    {"StaticText", "Repeated1/stSourceName"},
    {"StaticText", "Repeated1/stDealStatus"},
    {"StaticText", "Repeated1/stApplicantWorkTel"},
    {"StaticText", "Repeated1/stPropertyAddr"},
    {"StaticText", "Repeated1/stUWName"},
    {"StaticText", "Repeated1/stAdminName"},
    {"StaticText", "Repeated1/stFunderName"},
    {"StaticText", "Repeated1/stSourceAppId"},
    {"StaticText", "Repeated1/stPrimBorrower"},
    //--> Ticket#129 -- Support search by Servicing Mortgage #
    //--> By Billy 26Nov2003
    {"StaticText", "Repeated1/stServicingNum"},
    {"StaticText", "Repeated1/stInstitutionLabel"},
    {"StaticText", "Repeated1/stInstitutionName"}
    //========================================================
  };

	private String[][] dataBuffer;


	/**
	 *
	 *
	 */
	public DealSearchResultRow(String name, SysLogger logger, DealQueryResult dqr, int lang, boolean multiAccess)
	{
		super(name, logger);
		int len = buf.length;

		setNumberOfFields(len);

		dataBuffer = new String [ len ] [ 2 ];

		for(int i = 0; i < len;	++ i)
		{
			for(int j = 0;
			j < 2;
			j ++) dataBuffer [ i ] [ j ] = "";
		}

		setFieldValue(0, "" + dqr.dealId);
		
		String suffixString = BXResources.getPickListDescription(dqr.institutionProfileId, 
                "SUFFIX", dqr.suffixId, lang);
		//setFieldValue(1, formFullName(dqr.borrowerFirstName, dqr.borrowerMiddleInitial, dqr.borrowerLastName));
		setFieldValue(1, formFullName(dqr.borrowerFirstName, dqr.borrowerMiddleInitial, dqr.borrowerLastName, suffixString));

		//dqr.borrowerFullName);
		setFieldValue(2, formatPhoneNumber(dqr.borrowerHomePhoneNumber));

		setFieldValue(3, dqr.addressLine1);

		setFieldValue(4, formSourceFullName(dqr.source_contactFirstName, dqr.source_contactLastName));
		//setFieldValue(4, dqr.source_fullname);

    //--Release2.1--//
    //  Create status from picklist
    //setFieldValue(5, dqr.getStatusDesc());
    int theStatusId = dqr.getStatusId();
    int theDenResId = dqr.getDenialReasonId();

    String tmpStatusDec =
                BXResources.getPickListDescription(dqr.getInstitutionProfileId(),"STATUS", theStatusId, lang);

    if (theStatusId == Mc.DEAL_DENIED ||
        (theStatusId == Mc.DEAL_COLLAPSED && theDenResId != Mc.DENIAL_REASON_OTHER))
    {
      tmpStatusDec = tmpStatusDec + "/" +
        BXResources.getPickListDescription(dqr.getInstitutionProfileId(),"DENIALREASON", theDenResId, lang);
    }

    setFieldValue(5, tmpStatusDec);
    //--------------//

		String s = formatPhoneNumber(dqr.borrowerWorkPhoneNumber);

		if (s == null) s = "";

		s = s.trim();


		//============================================================================
		// Modification made because of change in the Deal query result class.
		// BorrowerWorkphoneextension field there has been changed from int to String
		//============================================================================
		if (dqr.borrowerWorkPhoneExtension != null && dqr.borrowerWorkPhoneExtension.trim().length() > 0) s = s + " x " + dqr.borrowerWorkPhoneExtension;

		setFieldValue(6, s);

		setFieldValue(7, dqr.properyAddress);

		setFieldValue(8, dqr.uw_fullname);

		setFieldValue(9, dqr.ad_fullname);

		setFieldValue(10, dqr.fn_fullname);

		setFieldValue(11, dqr.sourceApplicationId);


		// Added Primary Borrower CheckBox -- Billy 01March2002
    //--Release2.1--//
    //  Get Yes and No label from picklist
		if (dqr.getPrimaryBorrowerFlag() != null && dqr.getPrimaryBorrowerFlag().equals("Y"))
      setFieldValue(12, BXResources.getGenericMsg("YES_LABEL", lang));
		else
      setFieldValue(12, BXResources.getGenericMsg("NO_LABEL", lang));
    //--------------//

    //--> Ticket#129 -- Support search by Servicing Mortgage #
    //--> By Billy 26Nov2003
	    setFieldValue(13, dqr.servicingMortgageNum);
	    
	    // for ML, only when user can access more than one institution
	    // display institution Information
	    //fix for FXP21119.
	    if (multiAccess) {
	        setFieldValue(14, BXResources.getGenericMsg("INSTITUTION_LABEL", lang));

    	    setFieldValue(15, BXResources.getInstitutionName(dqr.institutionProfileId));
	    } else { 
            setFieldValue(14, "");

            setFieldValue(15, "");	        
	    }
    //========================================================
	}


	/**
	 *
	 *
	 */
	public void setFieldValue(int fldNdx, String value)
	{
		dataBuffer [ fldNdx ] [ 0 ] = value;

	}


	/**
	 *
	 *
	 */
	public String getFieldName(int fldNdx)
	{
		return buf [ fldNdx ] [ 1 ];

	}


	/**
	 *
	 *
	 */
	public String getFieldType(int fldNdx)
	{
		return buf [ fldNdx ] [ 0 ];

	}


	/**
	 *
	 *
	 */
	public String getFieldValue(int fldNdx)
	{
		return dataBuffer [ fldNdx ] [ 0 ];

	}


	/////////////////////////////////////////////////////

	public String formatPhoneNumber(String phStr)
	{
		String fullPhoneNumber = "";

		if (phStr == null || phStr.trim().length() == 0)
		{
			if (fullPhoneNumber.length() < 7) return fullPhoneNumber;
		}

		if (phStr.length() == 7)
		{
			fullPhoneNumber = phStr.substring(0, 3) + "-" + phStr.substring(3, 7);
			return fullPhoneNumber;
		}

		if (phStr.trim().length() == 10)
		{
			fullPhoneNumber = "(" + phStr.substring(0, 3) + ") " + phStr.substring(3, 6) + "-" + phStr.substring(6, 10);
			return fullPhoneNumber;
		}

		return fullPhoneNumber;

	}


	//////////////////////////////////////////////////////////////

	public String formFullName(String f, String i, String l, String suffix)
	{
		if (f == null) f = "";

		if (l == null) l = "";

		String fullName = l + " " + suffix + " " + f;

		if (fullName.equals("  ")) fullName = "";

		if (i != null &&(! i.equals(""))) return fullName + " " + i + ".";

		return fullName;

	}


	//////////////////////////////////////////////////////////////

	public String formSourceFullName(String f, String l)
	{
		if (f == null) f = "";

		if (l == null) l = "";

		String fullName = l + ", " + f;

		if (fullName.equals(", ")) fullName = "";

		return fullName;

	}


	// --- //
	// satisfy interface for limited implementation (used for screen population only!) ...

	public String getOriginalValue(int fldNdx)
	{
		return "";

	}


	/**
	 *
	 *
	 */
	public String getDOFieldName(int fldNdx)
	{
		return "";

	}


	/**
	 *
	 *
	 */
	public void setFieldName(int fldNdx, String value)
	{
		;

	}


	/**
	 *
	 *
	 */
	public void setDOFieldName(int fldNdx, String value)
	{
		;

	}


	/**
	 *
	 *
	 */
	public void setFieldType(int fldNdx, String value)
	{
		;

	}


	/**
	 *
	 *
	 */
	public void setOriginalValue(int fldNdx, String value)
	{
		;

	}

}

