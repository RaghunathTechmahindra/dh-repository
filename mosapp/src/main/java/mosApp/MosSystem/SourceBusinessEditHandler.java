package mosApp.MosSystem;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.commitment.CCM;
import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.SourceOfBusinessProfile;
import com.basis100.deal.pk.SourceOfBusinessProfilePK;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.util.TypeConverter;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.HtmlDisplayFieldBase;
import com.iplanet.jato.view.html.TextField;

/**
 *
 *
 */
public class SourceBusinessEditHandler extends PageHandlerCommon
	implements Cloneable, Sc
{
	// Key name to pass the SOB Id
	public static final String SOB_ID_PASSED_PARM="sourceOBIdPassed";
	public static final String SFIRM_ID_PASSED_PARM="sourceFirmIdPassed";
  //-- FXLink Phase II --//
  //--> System Type Must be passed for Adding new SOB
  //--> By Billy 04Nov2003
  public static final String SYSTYPE_ID_PASSED_PARM="systemTypeIdPassed";
  //--> Source Firm Returned from Adding new Firm
  //--> By Billy 17Nov2003
  public static final String SFIRM_ID_RETURN_PARM="sourceFirmIdReturned";
  //=================================================

	// following used as key in page state table to indicate if user has edit permission
	public static final String USER_CAN_EDIT="USER_CAN_EDIT";
	// key for edit buffer (in page state table)
  public static final String SOB_EDIT_BUFFER="SOB_EDIT_BUFFER";
	// Key to indicate the action : Add or Modify
	public static final String SOB_EDIT_ACTION="SOB_EDIT_ACTION";

	// AlertMessage OK type ==> User OK to unassociate user
	public static final String SOB_OK_TO_UNASSOCIATEUSER="SOB_OK_TO_UNASSOCIATEUSER";
  // AlertMessage OK type ==> User OK to unassociate group
  public static final String SOB_OK_TO_UNASSOCIATEGROUP="SOB_OK_TO_UNASSOCIATEGROUP";


	/**
	 *
	 *
	 */
	public SourceBusinessEditHandler cloneSS()
	{
		return(SourceBusinessEditHandler) super.cloneSafeShallow();

	}

	/////////////////////////////////////////////////////////////////////////
	// This method is used to populate the fields in the header
	// with the appropriate information

	public void populatePageDisplayFields()
	{
		PageEntry pg = theSessionState.getCurrentPage();
		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");
		Hashtable pst = pg.getPageStateTable();
		ViewBean theNDPage = getCurrNDPage();

		populatePageShellDisplayFields();
		populatePreviousPagesLinks();
		populatePageDealSummarySnapShot();

    EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(SOB_EDIT_BUFFER);
    //---------------- FXLink Phase II ------------------//
    //--> Set some fields to disable based on the ChannelMedia and System Properties
    //--> By Billy 11Nov2003
    // Note : this must be called before populateFields in order to preserve the
    //    fileds which disabled.
    setupDisabledFields(theNDPage, efb);
    //=====================================================
    //-- FXLink Phase II --//
    //--> Check if SourceFirm ID returned
    // Note : this must be called before populateFields in order to preserve the
    //--> By Billy 17Nov2003
    String tmpSFId = (String)(pst.get(SFIRM_ID_RETURN_PARM));
    if(tmpSFId != null && !tmpSFId.trim().equals(""))
    {
      // Set the Firm ComboBox the display the returned ID
      efb.setFieldValue("cbSourceFirm", tmpSFId);
      // Reset the Returned Parameter
      pst.put(SFIRM_ID_RETURN_PARM, "");
    }
    //=====================================================
		efb.populateFields(getCurrNDPage());
    displayDSSConditional(pg, getCurrNDPage());

		//--Release2.1--//
    //// Since this static html label should be translated on fly toggling the
    //// language and pst content is not refreshed on toggle action the label
    //// except text should be set to pst.
    //// Lookup to translate the label.
    String sobAction = BXResources.getGenericMsg(new String((String) pst.get(SOB_EDIT_ACTION)),
                                                 theSessionState.getLanguageId());

    getCurrNDPage().setDisplayFieldValue("stAction", sobAction);

    //// Data validation API implementation.
    try
    {
      String sourceBusEditValidPath = Sc.DVALID_SOURCE_BUS_EDIT_PROPS_FILE_NAME;

      logger.debug("SBEH@sourceBusEditValidPath: " + sourceBusEditValidPath);

      JSValidationRules sourceBusEditValidObj = new JSValidationRules(sourceBusEditValidPath, logger, theNDPage);
      sourceBusEditValidObj.attachJSValidationRules();
    }
    catch( Exception e )
    {
      logger.error("Exception SBEH@PopulatePageDisplayFields: Problem encountered in JS data validation API." + e.getMessage());
    }
	}

	private void displayDSSConditional(PageEntry pg, ViewBean thePage)
	{
		String suppressStart = "<!--";

		String suppressEnd = "//-->";

		if (pg == null || pg.getPageDealId() <= 0)
		{
			// suppress
			thePage.setDisplayFieldValue("stIncludeDSSstart", new String(suppressStart));
			thePage.setDisplayFieldValue("stIncludeDSSend", new String(suppressEnd));
			return;
		}


		// display
		thePage.setDisplayFieldValue("stIncludeDSSstart", new String(""));

		thePage.setDisplayFieldValue("stIncludeDSSend", new String(""));

		return;

	}


	/////////////////////////////////////////////////////////////////////////

	public void setupBeforePageGeneration()
	{
    PageEntry pg = getTheSessionState().getCurrentPage();
		if (pg.getSetupBeforeGenerationCalled() == true) return;
		pg.setSetupBeforeGenerationCalled(true);

		ViewBean theNDPage = getCurrNDPage();
		Hashtable pst = pg.getPageStateTable();

		// determine if user can modify existing party information
		if (getUserAccessType(Mc.PGNM_SOB_ADD) == Sc.PAGE_ACCESS_EDIT) pst.put(USER_CAN_EDIT, USER_CAN_EDIT);

		setupEditBuffer();
		setupDealSummarySnapShotDO(pg);

	}


	/**
	 *
	 *
	 */
	public void setupEditBuffer()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		try
		{
			Hashtable pst = pg.getPageStateTable();

      //--> Bug fix : Need to fill the month even if state on the same page i.e. for ToggleLanguage
      //--> By Billy 21Aug2003
      // Set up Date display fields
      //-- FXLink Phase II --//
      //--> Firm Effective Date is taken out
      //--> By Billy 07Nov2003
      //fillupMonths("cbSFEffectiveMonth");
      //====================================
      fillupMonths("cbAUserEffectiveMonth");
      //=================================================
      //--> CRF#37 :: Source/Firm to Group routing for TD
      //=================================================
      //--> Added new fields to handle Source to Group association
      //--> By Billy 20Aug2003
      fillupMonths("cbAGroupEffectiveMonth");
      //=================================================

			if (pst == null || pst.get(SOB_EDIT_BUFFER) == null)
			{
				// first invocation!
				if (pst == null)
				{
					pst = new Hashtable();
					pg.setPageStateTable(pst);
				}
				EditFieldsBuffer efb = new EditSOBBuffer("EditSOB", logger);
				pst.put(SOB_EDIT_BUFFER, efb);

				//=========================================================
				// determine if this is a modify existing vs. add new
				String SOBProfileId =(String) pst.get(SOB_ID_PASSED_PARM);
				if (SOBProfileId != null && SOBProfileId.length() > 0)
				{
					//logger.trace("BILLY ==>> @SourceBusinessEditHandler.setupEditBuffer: is modify, SOB profile id = " + SOBProfileId);

					// set modify action string!
          //--Release2.1--start//
          //// Since this static html label should be translated on fly toggling the
          //// language and pst content is not refreshed on toggle action the label
          //// except text should be set to pst.

          pst.put(SOB_EDIT_ACTION, "MODIFY_EXISTING_SOURCE_LABEL");
          //--Release2.1--end//

					// user data object to populate existing data
					doSourceOfBusinessModel theDO = (doSourceOfBusinessModel)
              RequestManager.getRequestContext().getModelManager().getModel(doSourceOfBusinessModel.class);

					theDO.clearUserWhereCriteria();
					theDO.addUserWhereCriterion("dfSBProfileId", "=", new String(SOBProfileId));
					efb.readFields( (QueryModelBase) theDO);
          //Manually populate some fileds
					// Need special handle for TierLevel field -- By BILLY 11April2002
          //logger.debug("SBEH@setupEditBuffer::TierLevelFromTextBox: " + efb.getFieldValue("tbTierLevel"));

					try
					{
						////efb.setFieldValue("tbTierLevel", "" + TypeConverter.asInt(Double.valueOf(efb.getFieldValue("tbTierLevel"))));
					  efb.setFieldValue("tbTierLevel", ""+TypeConverter.asInt(efb.getFieldValue("tbTierLevel")));
					}
					catch(NumberFormatException e)
					{
						efb.setFieldValue("tbTierLevel", "");
					}

          //logger.debug("SBEH@setupEditBuffer::PhoneNoFromTextBox: " + efb.getFieldValue("tbPhoneNumber1"));
          //logger.debug("SBEH@setupEditBuffer::FaxNoFromTextBox: " + efb.getFieldValue("tbFaxNumber1"));

					// Setup Phone# display fields
          //--> Simplified by Billy : 20Aug2003
          //--> Set Phone #
          setPhoneNoDisplayField(theDO, "tbPhoneNumber1", "tbPhoneNumber2", "tbPhoneNumber3", "dfSBPPhone", 0);
          //--> Also need to populate to efb
          efb.setFieldValue("tbPhoneNumber1", getCurrNDPage().getDisplayFieldValue("tbPhoneNumber1").toString());
          efb.setFieldValue("tbPhoneNumber2", getCurrNDPage().getDisplayFieldValue("tbPhoneNumber2").toString());
          efb.setFieldValue("tbPhoneNumber3", getCurrNDPage().getDisplayFieldValue("tbPhoneNumber3").toString());
          //--> Set FAX #
					setPhoneNoDisplayField(theDO, "tbFaxNumber1", "tbFaxNumber2", "tbFaxNumber3", "dfSBPFax", 0);
          //--> Also need to populate to efb
          efb.setFieldValue("tbFaxNumber1", getCurrNDPage().getDisplayFieldValue("tbFaxNumber1").toString());
          efb.setFieldValue("tbFaxNumber2", getCurrNDPage().getDisplayFieldValue("tbFaxNumber2").toString());
          efb.setFieldValue("tbFaxNumber3", getCurrNDPage().getDisplayFieldValue("tbFaxNumber3").toString());

          //logger.debug("SBEH@setupEditBuffer::YearsFromTextBox: " + efb.getFieldValue("tbLenOfSrvYrs"));
          //logger.debug("SBEH@setupEditBuffer::MonthsFromTextBox: " + efb.getFieldValue("tbLenOfSrvMths"));

          // Setup Terms Display Fields
          setTermsDisplayField(theDO, "tbLenOfSrvYrs", "tbLenOfSrvMths", "dfSBPServLen", 0);
          //--> Also need to populate to efb
          efb.setFieldValue("tbLenOfSrvYrs", getCurrNDPage().getDisplayFieldValue("tbLenOfSrvYrs").toString());
          efb.setFieldValue("tbLenOfSrvMths", getCurrNDPage().getDisplayFieldValue("tbLenOfSrvMths").toString());

          // Set up Date display fields
          //---------------- FXLink Phase II ------------------//
          //--> Remove Firm Effective Date
          //--> By Billy 10Nov2003
          // Set Firm Effective Date
          /*
          setDateDisplayField(theDO, "cbSFEffectiveMonth", "txSFEffectiveDay",
              "txSFEffectiveYear", theDO.FIELD_DFSBPFIRMDATE, 0);
          //--> Also need to populate to efb
          efb.setFieldValue("cbSFEffectiveMonth", getCurrNDPage().getDisplayFieldValue("cbSFEffectiveMonth").toString());
          efb.setFieldValue("txSFEffectiveDay", getCurrNDPage().getDisplayFieldValue("txSFEffectiveDay").toString());
          efb.setFieldValue("txSFEffectiveYear", getCurrNDPage().getDisplayFieldValue("txSFEffectiveYear").toString());
          */
          //====================================================

          // Set User Effective Date
          setDateDisplayField(theDO, "cbAUserEffectiveMonth", "txAUserEffectiveDay",
              "txAUserEffectiveYear", theDO.FIELD_DFSBPUSERDATE, 0);
          //--> Also need to populate to efb
          efb.setFieldValue("cbAUserEffectiveMonth", getCurrNDPage().getDisplayFieldValue("cbAUserEffectiveMonth").toString());
          efb.setFieldValue("txAUserEffectiveDay", getCurrNDPage().getDisplayFieldValue("txAUserEffectiveDay").toString());
          efb.setFieldValue("txAUserEffectiveYear", getCurrNDPage().getDisplayFieldValue("txAUserEffectiveYear").toString());
          //=================================================
          //--> CRF#37 :: Source/Firm to Group routing for TD
          //=================================================
          //--> Added new fields to handle Source to Group association
          //--> By Billy 20Aug2003
				  // Set Group Effective Date
          setDateDisplayField(theDO, "cbAGroupEffectiveMonth", "txAGroupEffectiveDay",
              "txAGroupEffectiveYear", theDO.FIELD_DFSBPGROUPDATE, 0);
          //--> Also need to populate to efb
          efb.setFieldValue("cbAGroupEffectiveMonth", getCurrNDPage().getDisplayFieldValue("cbAGroupEffectiveMonth").toString());
          efb.setFieldValue("txAGroupEffectiveDay", getCurrNDPage().getDisplayFieldValue("txAGroupEffectiveDay").toString());
          efb.setFieldValue("txAGroupEffectiveYear", getCurrNDPage().getDisplayFieldValue("txAGroupEffectiveYear").toString());
          //=================================================

					efb.setOriginalAsCurrent();

          //---------------- FXLink Phase II ------------------//
          //--> By Billy 11Nov2003
          // set the SystemType value to PST as well
          pst.put(SYSTYPE_ID_PASSED_PARM, theDO.getDfSBPSystemTypeId().toString());
          //====================================================
				}
				else
				{
          // -- Add page -- //
          //--Release2.1--start//
          //// Since this static html label should be translated on fly toggling the
          //// language and pst content is not refreshed on toggle action the label
          //// except text should be set to pst.
          pst.put(SOB_EDIT_ACTION, "ADD_NEW_SOURCE_OF_BUSINESS_LABEL");
          //--Release2.1--end//

          //---------------- FXLink Phase II ------------------//
          //--> Setup System Type display from PST input
          //--> By Billy 11Nov2003
          efb.setFieldValue("stSystemType",
            BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),"SYSTEMTYPE", getPageSystemTypeId(), theSessionState.getLanguageId()));
          //====================================================
				}
			}
		}
		catch(Exception ex)
		{
			setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
			logger.error("Exception @SourceBusinessEditHandler.setupEditBuffer");
			logger.error(ex);
			return;
		}
	}


	/**
	 *
	 *
	 */
	public void handleDetailSourceFirm()
	{
		try
		{
			////SessionState theSession = getTheSession();
			PageEntry pg = theSessionState.getCurrentPage();
			ViewBean thisNDPage = this.getCurrNDPage();
      //---------------- FXLink Phase II ------------------//
      //--> Get the Firm ID from EditBuffer if Edit Disabled
      //--> By Billy 12Nov2003
			String theSorceFirmId = "";
      if(isSystemCodeEditable())
      {
        theSorceFirmId = thisNDPage.getDisplayFieldValue("cbSourceFirm").toString();
      }
      else
      {
        theSorceFirmId = ((EditFieldsBuffer)(pg.getPageStateTable().get(SOB_EDIT_BUFFER))).getOriginalValue("cbSourceFirm");
      }
			if (theSorceFirmId == null || theSorceFirmId.trim().equals("") || theSorceFirmId.equals("0"))
			{
				setActiveMessageToAlert(BXResources.getSysMsg("SOURCE_OF_BUSINESS_SOURCE_FIRM_NOT_SET", theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMCONFIRM);
				return;
			}

			// Goto review SourceFirm page
			PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_SFIRM_REVIEW, true);
			Hashtable subPst = pgEntry.getPageStateTable();
			subPst.put(SFIRM_ID_PASSED_PARM, theSorceFirmId);
			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage(true);
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setStandardFailMessage();
			logger.error("Error @SourceBusinessEditHandler.handleDetailSourceFirm: ");
			logger.error(e);
		}

	}


	/**
	 *
	 *
	 */
	////public int handleViewModifyButton()
	public boolean handleViewModifyButton()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		try
		{
			Hashtable pst = pg.getPageStateTable();
			if (pst.get(USER_CAN_EDIT) != null)
          return true;
		}
		catch(Exception e)
		{
			;
		}

		return false;

	}


	/**
	 *
	 *
	 */
	public boolean handleViewUnassociateButtons()
  {
		PageEntry pg = theSessionState.getCurrentPage();

		try
		{
			Hashtable pst = pg.getPageStateTable();
			if (pst.get(USER_CAN_EDIT) != null)
			{
				String SOBProfileId =(String) pst.get(SOB_ID_PASSED_PARM);
				if (SOBProfileId != null && SOBProfileId.length() > 0)
            return true;
			}
		}
		catch(Exception e)
		{
			;
		}

		return false;

	}


	/**
	 *
	 *
	 */
	public void saveData(PageEntry currPage, boolean calledInTransaction)
	{
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(SOB_EDIT_BUFFER);

		efb.readFields(getCurrNDPage());

		if (efb.isModified())
		{
			pg.setModified(true);
			logger.debug("BILLY===> setModified = true !!");
		}
	}


	/**
	 *
	 *
	 */
	public void handleSubmit()
	{
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();

		// Validation check ... i.e. check for minimal data inpur, DupeCheck, ...
		String errorMsg = validateSOB(getCurrNDPage());

		if (! errorMsg.equals(""))
		{
			// Validation Errors found -- set to display Alert Messages
			setActiveMessageToAlert(BXResources.getSysMsg("SOURCE_OF_BUSINESS_EDIT_VALIDATION_ERROR", theSessionState.getLanguageId()) + errorMsg,
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			return;
		}


		// check for party modify vs. add new
		SourceOfBusinessProfile theSOBP;
		String SOBProfileId =(String) pst.get(SOB_ID_PASSED_PARM);

		if (SOBProfileId == null || SOBProfileId.length() == 0)
		{
			////theSOBP = addNewSOB(theSession, pg, pst);
			theSOBP = addNewSOB(theSessionState, pg, pst);

			// Check if Parent Page is SourceBusiness Review, then set to display the new SourceBusiness -- Billy 19Nov2001
			if (theSOBP != null)
			{
				PageEntry parent = pg.getParentPage();
				if (parent.getPageId() == Mc.PGNM_SOB_REVIEW)
				{
					Hashtable pPst = parent.getPageStateTable();
					pPst.put(SOB_ID_PASSED_PARM, "" + theSOBP.getSourceOfBusinessProfileId());
				}
			}
		}
		else
		{
			////theSOBP = modifySOB(getIntValue(SOBProfileId), theSession, pg, pst);
			theSOBP = modifySOB(getIntValue(SOBProfileId), theSessionState, pg, pst);
		}

		// if no message set (no eror encountered) then away we go ...
		if (theSessionState.isActMessageSet() == false)
		{
			navigateToNextPage(true);
		}
	}


	/**
	 *
	 *
	 */
	public void handleUserUnassociate(boolean confirmed)
	{
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		SessionResourceKit srk = getSessionResourceKit();

		if (! confirmed)
		{
			// Validation Errors found -- set to display Alert Messages
			setActiveMessageToAlert(BXResources.getSysMsg("SOURCE_OF_BUSINESS_UNASSOC_USER_CONFIRM", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMDIALOG, SOB_OK_TO_UNASSOCIATEUSER);
			return;
		}

		try
		{
			srk.beginTransaction();
			SourceOfBusinessProfile theSOBP = new SourceOfBusinessProfile(srk);
			String SOBProfileId =(String) pst.get(SOB_ID_PASSED_PARM);
			theSOBP = theSOBP.findByPrimaryKey(new SourceOfBusinessProfilePK(getIntValue(SOBProfileId)));
			theSOBP.deleteSourceUserAssoc();
			srk.commitTransaction();

			// reset Associated User DisplayFields
			EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(SOB_EDIT_BUFFER);
			efb.setFieldValue("cbAssociatedUser", new String(""));
			efb.setFieldValue("cbAUserEffectiveMonth", new String("0"));
			efb.setFieldValue("txAUserEffectiveDay", new String(""));
			efb.setFieldValue("txAUserEffectiveYear", new String(""));
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Exception @PatrySearchHandler.handleUserUnassociate:");
			logger.error(e);
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
		}
	}


	//--> Miss convert to multi-lingual
  //--> Fixed by Billy 19Nov2003
	private String validateSOB(ViewBean thePage)
	{
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		SessionResourceKit srk = getSessionResourceKit();
    int languageId = theSessionState.getLanguageId();

		String retStr = "";

		int SOBProfileId = getIntValue((String) pst.get(SOB_ID_PASSED_PARM));

		// Check if ShortName input
		////if (thePage.getDisplayFieldStringValue("tbShortName").equals(""))
		if (thePage.getDisplayFieldValue("tbShortName").equals(""))
		{
			if (! retStr.equals("")) retStr += ", ";
      retStr += "<br> - " + BXResources.getSysMsg("SOB_SF_EDIT_MISS_SHORTNAME", languageId);
		}


		// Check if LastName input
		////if (thePage.getDisplayFieldStringValue("tbContactLastName").equals(""))
		if (thePage.getDisplayFieldValue("tbContactLastName").equals(""))
		{
			if (! retStr.equals("")) retStr += ", ";
      retStr += "<br> - " + BXResources.getSysMsg("SOB_SF_EDIT_MISS_LASTNAME", languageId);
		}

    //---------------- FXLink Phase II ------------------//
    //--> Check if Frim Input OK
    //--> By Billy 13Nov2003
    if(isSystemCodeEditable() && thePage.getDisplayFieldValue("cbSourceFirm").equals(""))
		{
			if (! retStr.equals("")) retStr += ", ";
      retStr += "<br> - " + BXResources.getSysMsg("SOB_SF_EDIT_MISS_FIRMNAME", languageId);
		}

		// Check if Source System Code input OK
		//---------------- FXLink Phase II ------------------//
    //--> Rename MAILBOXNUM ==> SYSTEMCODE
    //--> Logic changed for System Code Checking
    //--> by Billy 10Nov2003
    String tmpFieldValue = thePage.getDisplayFieldValue("tbSystemCode").toString();
    int theSystemTypeId = getIntValue((String) pst.get(SYSTYPE_ID_PASSED_PARM));
    //logger.debug("SBEH@validateSOB::SystemCode: " + tmpFieldValue + " ::SystemTypeId:" + theSystemTypeId);

		try
		{
      SourceOfBusinessProfile tmpSOB = new SourceOfBusinessProfile(srk);

      // Check if dup if SystemType.ChannelMedia <> 'E'
			if (isSystemCodeEditable())
			{
				if (tmpFieldValue.equals(""))
				{
					if (! retStr.equals("")) retStr += ", ";
          retStr += "<br> - " + BXResources.getSysMsg("SOB_SF_EDIT_MISS_SOURCESYSTEMCODE", languageId);
				}
				else
				{
					if (tmpSOB.isDupeSystemCode(tmpFieldValue, theSystemTypeId, SOBProfileId))
            retStr += "<br> - " + BXResources.getSysMsg("SOB_SF_EDIT_DUP_SOURCESYSTEMCODE", languageId);
				}
			}
		}
		catch(Exception e)
		{
			logger.warning("Exception @SourceBusinessEditHandler.validateSOB (SystemCode DupeCheck) :" + e.getMessage());
		}


		// Check if BusinessId input
		////tmpFieldValue = thePage.getDisplayFieldStringValue("tbClientNum");
		TextField cbxClientNum =(TextField) thePage.getDisplayField("tbClientNum");
    tmpFieldValue = (String)cbxClientNum.getValue();
    logger.debug("SBEH@validateSOB::ClientNum: " + tmpFieldValue);

		//Check if dupicated BusinessId
		try
		{
			SourceOfBusinessProfile tmpSOB = new SourceOfBusinessProfile(srk);

			// check if BusCheck Needed
			if (isBusIdEditable())
			{
				if (tmpFieldValue.equals(""))
				{
					if (! retStr.equals("")) retStr += ", ";
          retStr += "<br> - " + BXResources.getSysMsg("SOB_SF_EDIT_MISS_CLIENTNUM", languageId);
				}
				else
				{
					if (tmpSOB.isDupeBusId(tmpFieldValue, SOBProfileId))
            retStr += "<br> - " + BXResources.getSysMsg("SOB_SF_EDIT_DUP_SOURCECLIENTNUM", languageId);
				}
			}
		}
		catch(Exception e)
		{
			logger.warning("Exception @SourceBusinessEditHandler.validateSOB (BusinessId DupeCheck) :" + e.getMessage());
		}

		return(retStr);

	}


	/**
	 *
	 *
	 */
	////private SourceOfBusinessProfile addNewSOB(SessionState theSession, PageEntry pg, Hashtable pst)
	private SourceOfBusinessProfile addNewSOB(SessionStateModelImpl theSessionState, PageEntry pg, Hashtable pst)
	{
		SessionResourceKit srk = getSessionResourceKit();

		try
		{
			srk.beginTransaction();
			EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(SOB_EDIT_BUFFER);

			// Create new SOBP entity
			SourceOfBusinessProfile theSOBP = new SourceOfBusinessProfile(srk);
			theSOBP.create();

			// Perform save data from screen
			saveSOBData(theSOBP, efb, true);
			srk.commitTransaction();

			// Set modified flag to false to prevent display meaningless message to user
			pg.setModified(false);
			return theSOBP;
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Exception @SourceBusinessEditHandler.addNewSOB:");
			logger.error(e);
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			return null;
		}

	}


	/**
	 *
	 *
	 */
	////private SourceOfBusinessProfile modifySOB(int SOBPId, SessionState theSession, PageEntry pg, Hashtable pst)
	private SourceOfBusinessProfile modifySOB(int SOBPId, SessionStateModelImpl theSessionState, PageEntry pg, Hashtable pst)
	{
		SessionResourceKit srk = getSessionResourceKit();

		try
		{
			srk.beginTransaction();
			EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(SOB_EDIT_BUFFER);

			// Create new SOBP entity
			SourceOfBusinessProfile theSOBP = new SourceOfBusinessProfile(srk);
			theSOBP = theSOBP.findByPrimaryKey(new SourceOfBusinessProfilePK(SOBPId));

			// Perform save data from screen
			saveSOBData(theSOBP, efb, false);
			srk.commitTransaction();

			// Set modified flag to false to prevent display meaningless message to user
			pg.setModified(false);
			return theSOBP;
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Exception @SourceBusinessEditHandler.modifySOB:");
			logger.error(e);
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			return null;
		}

	}


	/**
	 *
	 *
	 */
  //---------------- FXLink Phase II ------------------//
  //--> Modified to pass the indicator to specify if it is a New or Modify
  //--> By Billy 11Nov2003
	private void saveSOBData(SourceOfBusinessProfile theSOBP, EditFieldsBuffer efb, boolean isNew)
		throws Exception
	{
		// Update Contact and Address Info.
		Contact contact = theSOBP.getContact();
		Addr addr = contact.getAddr();
		String [ ] val = new String [ 5 ];

		val [ 0 ] = efb.getFieldValue("cbProvince");
		if (val [ 0 ] != null) addr.setProvinceId(getIntValue(val [ 0 ]));

		val [ 0 ] = efb.getFieldValue("tbAddressLine1");
		if (val [ 0 ] != null) addr.setAddressLine1(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("tbAddressLine2");
		if (val [ 0 ] != null) addr.setAddressLine2(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("tbCity");
		if (val [ 0 ] != null) addr.setCity(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("tbPostalCodeFSA");
		if (val [ 0 ] != null) addr.setPostalFSA(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("tbPostalCodeLDU");
		if (val [ 0 ] != null) addr.setPostalLDU(val [ 0 ]);

		addr.ejbStore();

		val [ 0 ] = efb.getFieldValue("tbContactFirstName");
		if (val [ 0 ] != null) contact.setContactFirstName(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("tbContactMiddleInitial");
		if (val [ 0 ] != null) contact.setContactMiddleInitial(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("tbContactLastName");
		if (val [ 0 ] != null) contact.setContactLastName(val [ 0 ]);

		val [ 0 ] = phoneJoin(efb.getFieldValue("tbPhoneNumber1"), efb.getFieldValue("tbPhoneNumber2"), efb.getFieldValue("tbPhoneNumber3"));
		if (val [ 0 ] != null && val [ 0 ].trim().length() != 0) contact.setContactPhoneNumber(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("tbPhoneExtension");
		if (val [ 0 ] != null) contact.setContactPhoneNumberExtension(val [ 0 ]);

		val [ 0 ] = phoneJoin(efb.getFieldValue("tbFaxNumber1"), efb.getFieldValue("tbFaxNumber2"), efb.getFieldValue("tbFaxNumber3"));
		if (val [ 0 ] != null && val [ 0 ].trim().length() != 0) contact.setContactFaxNumber(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("tbEmail");
		if (val [ 0 ] != null) contact.setContactEmailAddress(val [ 0 ]);

    //--DJ_PREFCOMMETHOD_CR--start--//
    val [ 0 ] = efb.getFieldValue("cbPrefDeliveryMethod");
    if (val [ 0 ] != null) contact.setPreferredDeliveryMethodId((getIntValue(val [ 0 ])));
    //--DJ_PREFCOMMETHOD_CR--end--//

		contact.ejbStore();

		// Update SOBP info.
		val [ 0 ] = efb.getFieldValue("tbShortName");
		if (val [ 0 ] != null) theSOBP.setSOBPShortName(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("cbStatus");
		if (val [ 0 ] != null) theSOBP.setProfileStatusId(getIntValue(val [ 0 ]));

		val [ 0 ] = efb.getFieldValue("tbCompenFactor");
		if (val [ 0 ] != null) theSOBP.setCompensationFactor(getDoubleValue(val [ 0 ]));

    //---------------- FXLink Phase II ------------------//
    //--> check if ClientNum editable
    //--> By Billy 11Nov2003
    if(isBusIdEditable())
    {
      val [ 0 ] = efb.getFieldValue("tbClientNum");
      if (val [ 0 ] != null) theSOBP.setSOBPBusinessId(val [ 0 ]);
    }
    //--> Check if New ==> create Client # automatically
    else if(isNew)
    {
      // Create Client # automatically
      CCM theCCM = new CCM(srk);
      theSOBP.setSOBPBusinessId(theCCM.getIPENumber(1, theSessionState.getLanguageId(), 
                                                    theSessionState.getDealInstitutionId()));
    }
    //=====================================================

		val [ 0 ] = efb.getFieldValue("tbAlternativeId");
		if (val [ 0 ] != null) theSOBP.setAlternativeId(val [ 0 ]);

		val [ 0 ] = efb.getFieldValue("tbLenOfSrvYrs");
		val [ 1 ] = efb.getFieldValue("tbLenOfSrvMths");
		if (val [ 0 ] != null && val [ 1 ] != null) theSOBP.setLengthOfService(getTermsInMonths(val));

		val [ 0 ] = efb.getFieldValue("cbCategory");
		if (val [ 0 ] != null) theSOBP.setSourceOfBusinessCategoryId(getIntValue(val [ 0 ]));

		val [ 0 ] = efb.getFieldValue("tbTierLevel");
		if (val [ 0 ] != null) theSOBP.setTierLevel(getIntValue(val [ 0 ]));

    //---------------- FXLink Phase II ------------------//
    //--> Rename MailBox to SystemCode
    //--> Check if it is editable
    //--> By Billy 11Nov2003
    if(isSystemCodeEditable())
    {
      val [ 0 ] = efb.getFieldValue("tbSystemCode");
		  if (val [ 0 ] != null) theSOBP.setSourceOfBusinessCode(val [ 0 ]);
    }
    //======================================================

        val [ 0 ] = efb.getFieldValue("tbLicenseNumber");
	    if (val [ 0 ] != null) theSOBP.setSobLicenseRegistrationNumber(val [ 0 ]);

		// Added Notes field -- BY Billy 11April2002
		val [ 0 ] = efb.getFieldValue("tbNotes");
		if (val [ 0 ] != null) theSOBP.setNotes(val [ 0 ]);

		//=========================================
		// Added SOBRegionId field -- BY Billy 17May2002
		val [ 0 ] = efb.getFieldValue("cbSaleRegion");
		if (val [ 0 ] != null) theSOBP.setSOBRegionId(getIntValue(val [ 0 ]));

    //--> Ticket#139 : BMO - Ability to prioritize Broker (SOB)
    //--> Add new field to indicate the Priority of the SOB
    //--> By Billy 28Nov2003
    val [ 0 ] = efb.getFieldValue("cbSOBPriority");
		if (val [ 0 ] != null) theSOBP.setSourceOfBusinessPriorityId(getIntValue(val [ 0 ]));
    //========================================================

		//=========================================
		// Check for Update/Create/Delect SourceFirm Association
    //---------------- FXLink Phase II ------------------//
    //--> Modified to use SOB.sourceFirmProfileId instead of SOURCETOFIRMASSOC table
    //--> Check if editable
    //--> By Billy 11Nov2003
    if(isSystemCodeEditable())
    {
      val [ 0 ] = efb.getFieldValue("cbSourceFirm");
      // Check if SourceFirm selected
      if (val [ 0 ] != null && ! val [ 0 ].trim().equals(""))
      {
        int theSourceFirmId = getIntValue(val [ 0 ]);
        // Update/Create SourceFirm Association
        theSOBP.setSourceFirmProfileId(theSourceFirmId);
      }
      else
      {
        // Remove all Associations
        theSOBP.setSourceFirmProfileId(0);
      }
    }

    // Update SystemType if new SOB
    if(isNew)
      theSOBP.setSystemTypeId(getPageSystemTypeId());
    //=======================================================

		// Check for Update/Create/Delect SourceUser Association
		val [ 0 ] = efb.getFieldValue("cbAssociatedUser");
		// Check if SourceUser selected
		if (val [ 0 ] != null && ! val [ 0 ].trim().equals(""))
		{
			int theSourceUserId = getIntValue(val [ 0 ]);
			//get effective date
			val [ 0 ] = efb.getFieldValue("txAUserEffectiveYear");
			val [ 1 ] = efb.getFieldValue("cbAUserEffectiveMonth");
			val [ 2 ] = efb.getFieldValue("txAUserEffectiveDay");
			Date theEffDate = getDateFromFields(val);
			// Update/Create SourceFirm Association
			theSOBP.updateSourceUserAssoc(theSourceUserId, theEffDate);
		}
		else
		{
			// Remove all Associations
			theSOBP.deleteSourceUserAssoc();
		}

    //=================================================
    //--> CRF#37 :: Source/Firm to Group routing for TD
    //=================================================
    //--> Added new fields to handle Source to Group association
    //--> By Billy 19Aug2003
    // Check for Update/Create/Delect SourceGroup Association
		val [ 0 ] = efb.getFieldValue("cbAssociatedGroup");
		// Check if SourceGroup selected
		if (val [ 0 ] != null && ! val [ 0 ].trim().equals(""))
		{
			int theSourceGroupId = getIntValue(val [ 0 ]);
			//get effective date
			val [ 0 ] = efb.getFieldValue("txAGroupEffectiveYear");
			val [ 1 ] = efb.getFieldValue("cbAGroupEffectiveMonth");
			val [ 2 ] = efb.getFieldValue("txAGroupEffectiveDay");
			Date theEffDate = getDateFromFields(val);
			// Update/Create SourceFirm Association
			theSOBP.updateSourceGroupAssoc(theSourceGroupId, theEffDate);
		}
		else
		{
			// Remove all Associations
			theSOBP.deleteSourceGroupAssoc();
		}
    //===============================================

		theSOBP.ejbStore();

	}


	/**
	 *
	 *
	 */
	public void handleCustomActMessageOk(String[] args)
	{
		if (args [ 0 ].equals(SOB_OK_TO_UNASSOCIATEUSER))
		{
			handleUserUnassociate(true);
			return;
		}
    //=================================================
    //--> CRF#37 :: Source/Firm to Group routing for TD
    //=================================================
    //--> Added new fields to handle Source to Group association
    //--> By Billy 19Aug2003
    else if (args [ 0 ].equals(SOB_OK_TO_UNASSOCIATEGROUP))
		{
			handleGroupUnassociate(true);
			return;
		}
    //===============================================
	}


	//////////////// Helper functions /////////////////////

	public int getTermsInMonths(String[] terms)
	{
		int years = getIntValue(terms [ 0 ]);

		int months = getIntValue(terms [ 1 ]);

		return((years * 12) + months);

	}


	/**
	 *
	 *
	 */
	public Date getDateFromFields(String[] inDate)
	{

		// Check if date not set then return current Date
		if (inDate [ 0 ] == null || inDate [ 0 ].trim().equals("") || inDate [ 1 ] == null || inDate [ 1 ].trim().equals("") || inDate [ 1 ].trim().equals("0") || inDate [ 2 ] == null || inDate [ 2 ].trim().equals(""))
		{
			return(new Date());
		}
		else
		{
			int year = getIntValue(inDate [ 0 ]);
			int month = getIntValue(inDate [ 1 ]);
			int date = getIntValue(inDate [ 2 ]);
			Calendar cDate = new GregorianCalendar(year, month - 1, date);
      logger.debug("SBEH@getDateFromFields::cDate: " + cDate.getTime());

			return(cDate.getTime());
		}

	}

  //=================================================
  //--> CRF#37 :: Source/Firm to Group routing for TD
  //=================================================
  //--> New Methods to handle Source to Group association
  //--> By Billy 20Aug2003
  public void handleGroupUnassociate(boolean confirmed)
	{
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		SessionResourceKit srk = getSessionResourceKit();

		if (!confirmed)
		{
			// Validation Errors found -- set to display Alert Messages
			setActiveMessageToAlert(BXResources.getSysMsg("SOURCE_OF_BUSINESS_UNASSOC_GROUP_CONFIRM", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMDIALOG, SOB_OK_TO_UNASSOCIATEGROUP);
			return;
		}

		try
		{
			srk.beginTransaction();
			SourceOfBusinessProfile theSOBP = new SourceOfBusinessProfile(srk);
			String SOBProfileId =(String) pst.get(SOB_ID_PASSED_PARM);
			theSOBP = theSOBP.findByPrimaryKey(new SourceOfBusinessProfilePK(getIntValue(SOBProfileId)));
			theSOBP.deleteSourceGroupAssoc();
			srk.commitTransaction();

			// reset Associated User DisplayFields
			EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(SOB_EDIT_BUFFER);
			efb.setFieldValue("cbAssociatedGroup", new String(""));
			efb.setFieldValue("cbAGroupEffectiveMonth", new String("0"));
			efb.setFieldValue("txAGroupEffectiveDay", new String(""));
			efb.setFieldValue("txAGroupEffectiveYear", new String(""));
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Exception @SourceBusinessEditHandler.handleGroupUnassociate:");
			logger.error(e);
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
		}
	}

  //---------------- FXLink Phase II ------------------//
  //--> Add button to create new Firm
  //--> By Billy 11Nov2003
  public void handleAddSourceFirm()
	{
		try
		{
			////SessionState theSession = getTheSession();
			PageEntry pg = theSessionState.getCurrentPage();
			Hashtable pst = pg.getPageStateTable();

			PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_SFIRM_ADD, true);

      //--> Get SystemType from PST
      String systemTypeId = (String)pst.get(SYSTYPE_ID_PASSED_PARM);
      pgEntry.getPageStateTable().put(SourceFirmEditHandler.SYSTYPE_ID_PASSED_PARM, systemTypeId);

			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage(true);
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setStandardFailMessage();
			logger.error("Error @SourceOfBusinessEditHandler.handleAddSourceFirm()");
			logger.error(e);
		}
	}

  //---------------- FXLink Phase II ------------------//
  //--> New method to check if SystemCode editable
  //--> By Billy 11Nov2003
  public boolean isSystemCodeEditable()
  {
    return(!(SourceOfBusinessProfile.getChannelMedia(srk, getPageSystemTypeId()).equals("E")));
  }

  //---------------- FXLink Phase II ------------------//
  //--> New method to check if SystemCode editable
  //--> By Billy 11Nov2003
  public boolean isBusIdEditable()
  {
    // Skip if generatesobrefnum = Y
    if((PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),"com.basis100.system.generatesobrefnum", "Y")).equals("Y"))
      return false;
    else
      return true;
  }

  //---------------- FXLink Phase II ------------------//
  //--> New method to get the current System Type Id
  //--> By Billy 11Nov2003
  public int getPageSystemTypeId()
  {
    PageEntry pg = theSessionState.getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    return getIntValue((String) pst.get(SYSTYPE_ID_PASSED_PARM));
  }

  //---------------- FXLink Phase II ------------------//
  //--> Method to set fields disable
  //--> By Billy 11Nov2003
  private void setupDisabledFields(ViewBean thePage, EditFieldsBuffer efb)
	{
   	HtmlDisplayFieldBase df;

    //Set SourceSystemCode and Firm disable if not editable
    if(!isSystemCodeEditable())
    {
      df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbSystemCode");
      df.setExtraHtml(df.getExtraHtml() + " disabled");
      // Preserve the field from it's original value
      efb.setFieldValue("tbSystemCode", efb.getOriginalValue("tbSystemCode"));

      // Disable if Edit page
      //--> This check is not necessary as we don't allow to add 'E' type SOB
      //--> By Billy 12Nov2003
      //String SOBProfileId =(String) theSessionState.getCurrentPage().getPageStateTable().get(SOB_ID_PASSED_PARM);
		  //if (SOBProfileId != null && SOBProfileId.length() > 0)
		  //{
        df = (HtmlDisplayFieldBase)thePage.getDisplayField("cbSourceFirm");
        df.setExtraHtml(df.getExtraHtml() + " disabled");
        // Preserve the field from it's original value
        efb.setFieldValue("cbSourceFirm", efb.getOriginalValue("cbSourceFirm"));
      //}
    }

    // Set BusinessId filed disable if not editable
    if(!isBusIdEditable())
    {
      df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbClientNum");
      df.setExtraHtml(df.getExtraHtml() + " disabled");
      // Preserve the field from it's original value
      efb.setFieldValue("tbClientNum", efb.getOriginalValue("tbClientNum"));
    }
  }

}


// EditBuffer for SourceOfBusiness Edit

class EditSOBBuffer extends EditFieldsBuffer
	implements Serializable
{
	private static String[][] buf={{"TextBox", "tbShortName", "dfSBPShortName"},
                                  {"ComboBox", "cbStatus", "dfSBPStatusId"},
                                  {"TextBox", "tbCompenFactor", "dfSBPCompFactor"},
                                  {"TextBox", "tbClientNum", "dfSBPClientId"},
                                  {"TextBox", "tbAlternativeId", "dfSBPAlternativeId"},
                                  {"TextBox", "tbLenOfSrvYrs", "dfSBPServLen"},
                                  {"TextBox", "tbLenOfSrvMths", ""},
                                  {"ComboBox", "cbCategory", "dfSBPCategoryId"},
                                  {"TextBox", "tbTierLevel", "dfSBPTierLevel"},
                                  //---------------- FXLink Phase II ------------------//
                                  //--> Name changed from tbMailBoxNum ==> tbSystemCode
                                  //--> By Billy 07Nov2003
                                  //{"TextBox", "tbMailBoxNum", "dfSBPMailboxNum"},
                                  {"TextBox", "tbSystemCode", "dfSBPSystemCode"},
                                  {"TextBox", "tbLicenseNumber", "dfLicenseNumber"},
                                  //====================================================
                                  {"TextBox", "tbContactFirstName", "dfSBPFirstName"},
                                  {"TextBox", "tbContactMiddleInitial", "dfSBPMidInit"},
                                  {"TextBox", "tbContactLastName", "dfSBPLastName"},
                                  {"TextBox", "tbPhoneNumber1", "dfSBPPhone"},
                                  {"TextBox", "tbPhoneNumber2", ""},
                                  {"TextBox", "tbPhoneNumber3", ""},
                                  {"TextBox", "tbPhoneExtension", "dfSBPPhoneExt"},
                                  {"TextBox", "tbFaxNumber1", "dfSBPFax"},
                                  {"TextBox", "tbFaxNumber2", ""},
                                  {"TextBox", "tbFaxNumber3", ""},
                                  {"TextBox", "tbEmail", "dfSBPEmail"},
                                  {"TextBox", "tbAddressLine1", "dfSBPAddrLine1"},
                                  {"TextBox", "tbAddressLine2", "dfSBPAddrLine2"},
                                  {"TextBox", "tbCity", "dfSBPCity"},
                                  {"ComboBox", "cbProvince", "dfSBPProvinceId"},
                                  {"TextBox", "tbPostalCodeFSA", "dfSBP_FSA"},
                                  {"TextBox", "tbPostalCodeLDU", "dfSBP_LDU"},
                                  {"ComboBox", "cbSourceFirm", "dfSBPFirmId"},
                                  //---------------- FXLink Phase II ------------------//
                                  //--> Remove Firm Effective Date
                                  //--> By Billy 07Nov2003
                                  //{"ComboBox", "cbSFEffectiveMonth", ""},
                                  //{"TextBox", "txSFEffectiveDay", ""},
                                  //{"TextBox", "txSFEffectiveYear", ""},
                                  //====================================================
                                  {"ComboBox", "cbAssociatedUser", "dfSBPUserId"},
                                  {"ComboBox", "cbAUserEffectiveMonth", ""},
                                  {"TextBox", "txAUserEffectiveDay", ""},
                                  {"TextBox", "txAUserEffectiveYear", ""},
                                  {"TextBox", "tbNotes", "dfNotes"},
                                  {"ComboBox", "cbSaleRegion", "dfSOBRegionId"},
                                  //=================================================
                                  //--> CRF#37 :: Source/Firm to Group routing for TD
                                  //=================================================
                                  //--> Added new fields to handle Source to Group association
                                  //--> By Billy 20Aug2003
                                  {"ComboBox", "cbAssociatedGroup", "dfSBPGroupId"},
                                  {"ComboBox", "cbAGroupEffectiveMonth", ""},
                                  {"TextBox", "txAGroupEffectiveDay", ""},
                                  {"TextBox", "txAGroupEffectiveYear", ""},
                                  //=================================================
                                  //---------------- FXLink Phase II ------------------//
                                  //--> Add SystemType static field
                                  //--> By Billy 07Nov2003
                                  {"StaticText", "stSystemType", "dfSBPSystemTypeDesc"},
                                  //=================================================
                                  //--> Ticket#139 : BMO - Ability to prioritize Broker (SOB)
                                  //--> Add new field to indicate the Priority of the SOB
                                  //--> By Billy 28Nov2003
                                  {"ComboBox", "cbSOBPriority", "dfSBPPriorityId"},
                                  //=================================================
                                  //--DJ_PREFCOMMETHOD_CR--start--//
                                  {"ComboBox", "cbPrefDeliveryMethod", "dfPrefDeliveryMethodId"},
                                  //--DJ_PREFCOMMETHOD_CR--end--//
                                  };

	private String[][] dataBuffer;


	/**
	 *
	 *
	 */
	public EditSOBBuffer(String name, SysLogger logger)
	{
		super(name, logger);
		int len = buf.length;

		setNumberOfFields(len);

		dataBuffer = new String [ len ] [ 2 ];

		for(int i = 0;
		i < len;
		++ i)
		{
			for(int j = 0;
			j < 2;
			j ++) dataBuffer [ i ] [ j ] = "";
		}

	}


	/**
	 *
	 *
	 */
	public String getFieldName(int fldNdx)
	{
		return buf [ fldNdx ] [ 1 ];

	}


	/**
	 *
	 *
	 */
	public String getDOFieldName(int fldNdx)
	{
		return buf [ fldNdx ] [ 2 ];

	}


	/**
	 *
	 *
	 */
	public String getFieldType(int fldNdx)
	{
		return buf [ fldNdx ] [ 0 ];

	}


	/**
	 *
	 *
	 */
	public String getFieldValue(int fldNdx)
	{
		return dataBuffer [ fldNdx ] [ 0 ];

	}


	/**
	 *
	 *
	 */
	public String getOriginalValue(int fldNdx)
	{
		return dataBuffer [ fldNdx ] [ 1 ];

	}


	// --- //

	public void setFieldName(int fldNdx, String value)
	{
		buf [ fldNdx ] [ 1 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setDOFieldName(int fldNdx, String value)
	{
		buf [ fldNdx ] [ 2 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setFieldType(int fldNdx, String value)
	{
		buf [ fldNdx ] [ 0 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setFieldValue(int fldNdx, String value)
	{
		dataBuffer [ fldNdx ] [ 0 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setOriginalValue(int fldNdx, String value)
	{
		dataBuffer [ fldNdx ] [ 1 ] = value;

	}

}

