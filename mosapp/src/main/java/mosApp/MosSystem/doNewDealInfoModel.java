package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doNewDealInfoModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStatusDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfStatusDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfApplicationDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfApplicationDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfUnderwriterProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfUnderwriterProfileId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAdministratorProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfAdministratorProfileId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfFunderProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfFunderProfileId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfOrigDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfOrigDealId(String value);
	
	public java.math.BigDecimal getDfInstitutionId();
    public void setDfInstitutionId(java.math.BigDecimal id);
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFSTATUSDESC="dfStatusDesc";
	public static final String FIELD_DFAPPLICATIONDATE="dfApplicationDate";
	public static final String FIELD_DFUNDERWRITERPROFILEID="dfUnderwriterProfileId";
	public static final String FIELD_DFADMINISTRATORPROFILEID="dfAdministratorProfileId";
	public static final String FIELD_DFFUNDERPROFILEID="dfFunderProfileId";
	public static final String FIELD_DFORIGDEALID="dfOrigDealId";
	
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

