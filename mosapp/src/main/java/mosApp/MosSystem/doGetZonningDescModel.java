package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doGetZonningDescModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfZoningId();

	
	/**
	 * 
	 * 
	 */
	public void setDfZoningId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfZoningDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfZoningDesc(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFZONINGID="dfZoningId";
	public static final String FIELD_DFZONINGDESC="dfZoningDesc";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

