package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doAppraiserSelectModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public String getDfAppraiserAddress();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserAddress(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfFax();

	
	/**
	 * 
	 * 
	 */
	public void setDfFax(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAppraiserId();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmail();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmail(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAppraiserShortName();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserShortName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAppraiserPhoneNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserPhoneNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAppraiserExtension();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserExtension(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAppraiserName();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAppraiserCompanyName();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserCompanyName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAppraiserNotes();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraiserNotes(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFAPPRAISERADDRESS="dfAppraiserAddress";
	public static final String FIELD_DFFAX="dfFax";
	public static final String FIELD_DFAPPRAISERID="dfAppraiserId";
	public static final String FIELD_DFEMAIL="dfEmail";
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFPROPERTYID="dfPropertyId";
	public static final String FIELD_DFAPPRAISERSHORTNAME="dfAppraiserShortName";
	public static final String FIELD_DFAPPRAISERPHONENUMBER="dfAppraiserPhoneNumber";
	public static final String FIELD_DFAPPRAISEREXTENSION="dfAppraiserExtension";
	public static final String FIELD_DFAPPRAISERNAME="dfAppraiserName";
	public static final String FIELD_DFAPPRAISERCOMPANYNAME="dfAppraiserCompanyName";
	public static final String FIELD_DFAPPRAISERNOTES="dfAppraiserNotes";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

