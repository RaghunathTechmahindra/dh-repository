package mosApp.MosSystem;

import java.sql.SQLException;

import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *  WARNING - BJH: Nov 22, 02 : Dropped usage - no longer needed
 *
 */
public class doCountCreditBureauLiabModelImpl extends QueryModelBase
	implements doCountCreditBureauLiabModel
{
	/**
	 *
	 *
	 */
	public doCountCreditBureauLiabModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIABILITYID);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIABILITYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIABILITYAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIABILITYAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityMonthlyPayment()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIABILITYMONTHLYPAYMENT);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityMonthlyPayment(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIABILITYMONTHLYPAYMENT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPercentOutGDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPERCENTOUTGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPercentOutGDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPERCENTOUTGDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL LIABILITY.LIABILITYID, LIABILITY.BORROWERID, LIABILITY.LIABILITYAMOUNT, LIABILITY.LIABILITYMONTHLYPAYMENT, LIABILITY.PERCENTOUTGDS, LIABILITY.COPYID FROM LIABILITY  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="LIABILITY";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFLIABILITYID="LIABILITY.LIABILITYID";
	public static final String COLUMN_DFLIABILITYID="LIABILITYID";
	public static final String QUALIFIED_COLUMN_DFBORROWERID="LIABILITY.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFLIABILITYAMOUNT="LIABILITY.LIABILITYAMOUNT";
	public static final String COLUMN_DFLIABILITYAMOUNT="LIABILITYAMOUNT";
	public static final String QUALIFIED_COLUMN_DFLIABILITYMONTHLYPAYMENT="LIABILITY.LIABILITYMONTHLYPAYMENT";
	public static final String COLUMN_DFLIABILITYMONTHLYPAYMENT="LIABILITYMONTHLYPAYMENT";
	public static final String QUALIFIED_COLUMN_DFPERCENTOUTGDS="LIABILITY.PERCENTOUTGDS";
	public static final String COLUMN_DFPERCENTOUTGDS="PERCENTOUTGDS";
	public static final String QUALIFIED_COLUMN_DFCOPYID="LIABILITY.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYID,
				COLUMN_DFLIABILITYID,
				QUALIFIED_COLUMN_DFLIABILITYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYAMOUNT,
				COLUMN_DFLIABILITYAMOUNT,
				QUALIFIED_COLUMN_DFLIABILITYAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYMONTHLYPAYMENT,
				COLUMN_DFLIABILITYMONTHLYPAYMENT,
				QUALIFIED_COLUMN_DFLIABILITYMONTHLYPAYMENT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPERCENTOUTGDS,
				COLUMN_DFPERCENTOUTGDS,
				QUALIFIED_COLUMN_DFPERCENTOUTGDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

