package mosApp.MosSystem;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.TextField;

/**
 * <p>Title: pgAdjudicationRequestNBCViewBean.java </p>
 *
 * <p>Description:NBC implementation of the adjudication request screen</p>
 *
 * <p>Copyright: </p>
 *
 * <p>Company: filogix</p>
 *
 * @author Vlad Ivanov
 * @version 1.0
 */
public class pgAdjudicationRequestNBCViewBean extends pgAdjudicationRequestViewBean
{
      private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";

	/**
	 *
	 *
	 */
	public pgAdjudicationRequestNBCViewBean()
	{
		super(PAGE_NAME);
		setDefaultDisplayURL(DISPLAY_URL);
                setDefaultDisplayUrl(DISPLAY_URL);
		registerChildren();
	}

          protected void initialize() {
          }

          ////////////////////////////////////////////////////////////////////////////////
          // Child manipulation methods
          ////////////////////////////////////////////////////////////////////////////////
          protected View createChild(String name)
          {
            View childFromParent = super.createChild(name);

            if (childFromParent != null) {
              return childFromParent;
            }
            else if (name.equals(CHILD_TBCREDITBUREAU)) {
              TextField child = new TextField(this,
                                            getDefaultModel(),
                                            CHILD_TBCREDITBUREAU,
                                            CHILD_TBCREDITBUREAU,
                                            CHILD_TBCREDITBUREAU_RESET_VALUE,
                                            null);
               return child;
            }
            else
            if(name.equals(CHILD_TBCREDITBUREAUSCORE))
            {
              TextField child = new TextField(this,
                getDefaultModel(),
                CHILD_TBCREDITBUREAUSCORE,
                CHILD_TBCREDITBUREAUSCORE,
                CHILD_TBCREDITBUREAUSCORE_RESET_VALUE,
                null);
              return child;
            }
            else
            if(name.equals(CHILD_TBRISKRATING))
            {
              TextField child = new TextField(this,
                getDefaultModel(),
                CHILD_TBRISKRATING,
                CHILD_TBRISKRATING,
                CHILD_TBRISKRATING_RESET_VALUE,
                null);
              return child;
            }
            else
            if (name.equals(CHILD_TXINTERNALSCORE))
            {
              TextField child =
                new TextField(this, getDefaultModel(),
                              CHILD_TXINTERNALSCORE,
                              CHILD_TXINTERNALSCORE,
                              CHILD_TXINTERNALSCORE_RESET_VALUE,
                              null);
              return child;
            }
            else
            if (name.equals(CHILD_TXCREDITDECISIONSTATUS)) {
              TextField child =
                  new TextField(this, getDefaultModel(),
                                CHILD_TXCREDITDECISIONSTATUS,
                                CHILD_TXCREDITDECISIONSTATUS,
                                CHILD_TXCREDITDECISIONSTATUS_RESET_VALUE,
                                null);

              return child;
            }
            else
            if (name.equals(CHILD_TXCREDITDECISION)) {
              TextField child =
                  new TextField(this, getDefaultModel(),
                                CHILD_TXCREDITDECISION,
                                CHILD_TXCREDITDECISION,
                                CHILD_TXCREDITDECISION_RESET_VALUE, null);

              return child;
            }
            else
            if (name.equals(CHILD_TXDECISIONMESSAGES)) {
              TextField child =
                  new TextField(this, getDefaultModel(),
                                CHILD_TXDECISIONMESSAGES,
                                CHILD_TXDECISIONMESSAGES,
                                CHILD_TXDECISIONMESSAGES_RESET_VALUE, null);

              return child;
            }
            else
              throw new IllegalArgumentException("Invalid child name [" + name + "]");
          }

          public void resetChildren()
          {
            super.resetChildren();

            getTbCreditBureau().setValue(CHILD_TBCREDITBUREAU_RESET_VALUE);
            getTbCreditBureauScore().setValue(CHILD_TBCREDITBUREAUSCORE_RESET_VALUE);
            getTbRiskRating().setValue(CHILD_TBRISKRATING_RESET_VALUE);
            getTbInternalScore().setValue(CHILD_TXINTERNALSCORE_RESET_VALUE);
            getTbCreditScoringStatus().setValue(CHILD_TXCREDITDECISIONSTATUS_RESET_VALUE);
            getTbCreditDecision().setValue(CHILD_TXCREDITDECISION_RESET_VALUE);
            getTbDecisionMessages().setValue(CHILD_TXDECISIONMESSAGES_RESET_VALUE);
          }


          /**
           *
           *
           */
          protected void registerChildren()
          {
            super.registerChildren();

            registerChild(CHILD_TBCREDITBUREAU,TextField.class);
            registerChild(CHILD_TBCREDITBUREAUSCORE,TextField.class);
            registerChild(CHILD_TBRISKRATING,TextField.class);
            registerChild(CHILD_TXINTERNALSCORE,TextField.class);
            registerChild(CHILD_TXCREDITDECISIONSTATUS,TextField.class);
            registerChild(CHILD_TXCREDITDECISION,TextField.class);
            registerChild(CHILD_TXDECISIONMESSAGES,TextField.class);
          }

          public void beginDisplay(DisplayEvent event) throws ModelControlException
          {
            super.beginDisplay(event);
          }

          public boolean beforeModelExecutes(Model model, int executionContext) {
            AdjudicationRequestHandler handler = this.handler.cloneSS();

            handler.pageGetState(this);
            handler.setupBeforePageGeneration();
            handler.pageSaveState();

            // This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
            return super.beforeModelExecutes(model, executionContext);
          }

          public void afterModelExecutes(Model model, int executionContext)
          {
            // This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
            super.afterModelExecutes(model, executionContext);
          }

          public void afterAllModelsExecute(int executionContext) {
            AdjudicationRequestHandler handler = this.handler.cloneSS();

            handler.pageGetState(this);

            handler.populatePageDisplayFields();

            handler.pageSaveState();

            // This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
            super.afterAllModelsExecute(executionContext);
          }

          public void onModelError(Model model, int executionContext, ModelControlException exception)
                  throws ModelControlException
          {

            // This is the analog of NetDynamics this_onDataObjectErrorEvent
            super.onModelError(model, executionContext, exception);

          }

          public void handleBtProceedRequest(RequestInvocationEvent event)
                  throws ServletException, IOException
          {

            AdjudicationRequestHandler handler = this.handler.cloneSS();

            handler.preHandlerProtocol(this);

            handler.handleGoPage();

            handler.postHandlerProtocol();

          }

    //// This method overrides the getDefaultURL() framework JATO method and it is located
    //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
    //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
    //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
    //// The full method is still in PHC base class. It should care the
    //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
    public String getDisplayURL()
    {
         AdjudicationRequestHandler handler =(AdjudicationRequestHandler) this.handler.cloneSS();
         handler.pageGetState(this);

         String url=getDefaultDisplayURL();

         int languageId = handler.theSessionState.getLanguageId();

         //// Call the language specific URL (business delegation done in the BXResource).
         if(url != null && !url.trim().equals("") && !url.trim().equals("/")) {
            url = BXResources.getBXUrl(url, languageId);
         }
         else {
            url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
         }

        return url;
    }

    public TextField getTbCreditBureau() {
      return (TextField) getChild(CHILD_TBCREDITBUREAU);
    }

    public TextField getTbCreditBureauScore()
    {
      return (TextField)getChild(CHILD_TBCREDITBUREAUSCORE);
    }

    public TextField getTbRiskRating()
    {
      return (TextField)getChild(CHILD_TBRISKRATING);
    }

    public TextField getTbInternalScore()
    {
      return (TextField)getChild(CHILD_TXINTERNALSCORE);
    }

    public TextField getTbCreditScoringStatus()
    {
      return (TextField)getChild(CHILD_TXCREDITDECISIONSTATUS);
    }

    public TextField getTbCreditDecision() {
      return (TextField)getChild(CHILD_TXCREDITDECISION);
    }

    public TextField getTbDecisionMessages() {
      return (TextField)getChild(CHILD_TXDECISIONMESSAGES);
    }

          ////////////////////////////////////////////////////////////////////////////
          // Obsolete Netdynamics Events - Require Manual Migration
          ////////////////////////////////////////////////////////////////////////////


          ////////////////////////////////////////////////////////////////////////////
          // Custom Methods - Require Manual Migration
          ////////////////////////////////////////////////////////////////////////////

          public void handleActMessageOK(String[] args) {
            AdjudicationRequestHandler handler = this.handler.cloneSS();

            handler.preHandlerProtocol(this);

            handler.handleActMessageOK(args);

            handler.postHandlerProtocol();
          }


          ////////////////////////////////////////////////////////////////////////////////
          // Class variables
          ////////////////////////////////////////////////////////////////////////////////
          public static final String PAGE_NAME = "pgAdjudicationRequestNBC";
          //// It is a variable now (see explanation in the getDisplayURL() method above.
          public static Map FIELD_DESCRIPTORS;
          public static final String CHILD_TBCREDITBUREAU = "txCreditBureau";
          public static final String CHILD_TBCREDITBUREAU_RESET_VALUE = "";

          public static final String CHILD_TBCREDITBUREAUSCORE = "txCreditBureauScore";
          public static final String CHILD_TBCREDITBUREAUSCORE_RESET_VALUE = "";

          public static final String CHILD_TBRISKRATING = "txRiskRating";
          public static final String CHILD_TBRISKRATING_RESET_VALUE = "";

          public static final String CHILD_TXINTERNALSCORE = "txInternalScore";
          public static final String CHILD_TXINTERNALSCORE_RESET_VALUE = "";

          public static final String CHILD_TXCREDITDECISIONSTATUS = "txCreditDecisionStatus";
          public static final String CHILD_TXCREDITDECISIONSTATUS_RESET_VALUE = "";

          public static final String CHILD_TXCREDITDECISION = "txCreditDecision";
          public static final String CHILD_TXCREDITDECISION_RESET_VALUE = "";

          public static final String CHILD_TXDECISIONMESSAGES = "txDecisionMessages";
          public static final String CHILD_TXDECISIONMESSAGES_RESET_VALUE = "";


          ////////////////////////////////////////////////////////////////////////////
          // Instance variables
          ////////////////////////////////////////////////////////////////////////////

          private String DISPLAY_URL="/mosApp/MosSystem/pgAdjudicationRequestNBC.jsp";
}

