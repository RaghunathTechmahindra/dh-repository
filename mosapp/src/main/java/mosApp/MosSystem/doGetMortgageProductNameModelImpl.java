package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doGetMortgageProductNameModelImpl extends QueryModelBase
	implements doGetMortgageProductNameModel
{
	/**
	 *
	 *
	 */
	public doGetMortgageProductNameModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 19Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert MTG Product Name
      this.setDfMortgageProductName(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "MTGPROD", this.getDfMortgageProductId().intValue(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMortgageProductId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMORTGAGEPRODUCTID);
	}


	/**
	 *
	 *
	 */
	public void setDfMortgageProductId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMORTGAGEPRODUCTID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMortgageProductName()
	{
		return (String)getValue(FIELD_DFMORTGAGEPRODUCTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfMortgageProductName(String value)
	{
		setValue(FIELD_DFMORTGAGEPRODUCTNAME,value);
	}


    public java.math.BigDecimal getDfInstitutionId() {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
      }

      public void setDfInstitutionId(java.math.BigDecimal value) {
        setValue(FIELD_DFINSTITUTIONID, value);
      }


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL MTGPROD.MTGPRODID, MTGPROD.MTGPRODNAME,MTGPROD.INSTITUTIONPROFILEID FROM MTGPROD  __WHERE__  ";
	
	public static final String MODIFYING_QUERY_TABLE_NAME="MTGPROD";
	
	public static final String STATIC_WHERE_CRITERIA="";
	
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFMORTGAGEPRODUCTID="MTGPROD.MTGPRODID";
	public static final String COLUMN_DFMORTGAGEPRODUCTID="MTGPRODID";
	public static final String QUALIFIED_COLUMN_DFMORTGAGEPRODUCTNAME="MTGPROD.MTGPRODNAME";
	public static final String COLUMN_DFMORTGAGEPRODUCTNAME="MTGPRODNAME";
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "MTGPROD.INSTITUTIONPROFILEID";
      public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";



	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMORTGAGEPRODUCTID,
				COLUMN_DFMORTGAGEPRODUCTID,
				QUALIFIED_COLUMN_DFMORTGAGEPRODUCTID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMORTGAGEPRODUCTNAME,
				COLUMN_DFMORTGAGEPRODUCTNAME,
				QUALIFIED_COLUMN_DFMORTGAGEPRODUCTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                  FIELD_DFINSTITUTIONID,
                  COLUMN_DFINSTITUTIONID,
                  QUALIFIED_COLUMN_DFINSTITUTIONID,
                  java.math.BigDecimal.class,
                  false,
                  false,
                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                  "",
                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                  ""));
		
	}

}

