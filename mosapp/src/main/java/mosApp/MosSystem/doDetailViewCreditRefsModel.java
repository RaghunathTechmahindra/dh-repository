package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doDetailViewCreditRefsModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfCreditReferenceType();

	
	/**
	 * 
	 * 
	 */
	public void setDfCreditReferenceType(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfReferenceDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfReferenceDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfInstitutionName();

	
	/**
	 * 
	 * 
	 */
	public void setDfInstitutionName(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTimeWithReference();

	
	/**
	 * 
	 * 
	 */
	public void setDfTimeWithReference(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAccountNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfAccountNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCurrentBalance();

	
	/**
	 * 
	 * 
	 */
	public void setDfCurrentBalance(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);
	
    public java.math.BigDecimal getDfInstitutionId();
    public void setDfInstitutionId(java.math.BigDecimal id);
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFCREDITREFERENCETYPE="dfCreditReferenceType";
	public static final String FIELD_DFREFERENCEDESC="dfReferenceDesc";
	public static final String FIELD_DFINSTITUTIONNAME="dfInstitutionName";
	public static final String FIELD_DFTIMEWITHREFERENCE="dfTimeWithReference";
	public static final String FIELD_DFACCOUNTNUMBER="dfAccountNumber";
	public static final String FIELD_DFCURRENTBALANCE="dfCurrentBalance";
	public static final String FIELD_DFCOPYID="dfCopyId";
	
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

