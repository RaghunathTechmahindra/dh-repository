package mosApp.MosSystem;


import java.sql.SQLException;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doUWApplicantFinancialDetailsModelImpl extends QueryModelBase
	implements doUWApplicantFinancialDetailsModel
{
	/**
	 *
	 *
	 */
	public doUWApplicantFinancialDetailsModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{

    //--Release2.1--//
    ////To override all the Description fields by populating from BXResource.
    ////Get the Language ID from the SessionStateModel.
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);

    SessionStateModelImpl theSessionState =
                                          (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                           SessionStateModel.class,
                                           defaultInstanceStateName,
                                           true);

    int languageId = theSessionState.getLanguageId();
	int institutionId = theSessionState.getDealInstitutionId();

    while(this.next())
    {
      //// Convert BorrowerStatus Desc.
      this.setDfBorrowerTypeDesc(BXResources.getPickListDescription(institutionId, "BORROWERTYPE", this.getDfBorrowerTypeDesc(), languageId));
      this.setDfSuffix(BXResources.getPickListDescription(institutionId, "SUFFIX", this.getDfSuffix(), languageId));      
    }

    ////Reset Location
    this.beforeFirst();
  }

	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPrimaryFlag()
	{
		return (String)getValue(FIELD_DFPRIMARYFLAG);
	}


	/**
	 *
	 *
	 */
	public void setDfPrimaryFlag(String value)
	{
		setValue(FIELD_DFPRIMARYFLAG,value);
	}


	/**
	 *
	 *
	 */
	public String getDfFirstName()
	{
		return (String)getValue(FIELD_DFFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfFirstName(String value)
	{
		setValue(FIELD_DFFIRSTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBorrowerMiddleName()
	{
		return (String)getValue(FIELD_DFBORROWERMIDDLENAME);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerMiddleName(String value)
	{
		setValue(FIELD_DFBORROWERMIDDLENAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLastName()
	{
		return (String)getValue(FIELD_DFLASTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfLastName(String value)
	{
		setValue(FIELD_DFLASTNAME,value);
	}

    /**
     *
     *
     */
    public String getDfSuffix()
    {
        return (String)getValue(FIELD_DFSUFFIX);
    }


    /**
     *
     *
     */
    public void setDfSuffix(String value)
    {
        setValue(FIELD_DFSUFFIX,value);
    }
    
	/**
	 *
	 *
	 */
	public String getDfBorrowerTypeDesc()
	{
		return (String)getValue(FIELD_DFBORROWERTYPEDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerTypeDesc(String value)
	{
		setValue(FIELD_DFBORROWERTYPEDESC,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalIncomeAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALINCOMEAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalIncomeAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALINCOMEAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalLiabilityAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALLIABILITYAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalLiabilityAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALLIABILITYAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalAssetAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALASSETAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalAssetAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALASSETAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerNetWorth()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERNETWORTH);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerNetWorth(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERNETWORTH,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //// 1. Replace all Descriptions from the tables included into the PickList with
  //// their joins' counterparts in the SQL_TEMPLATE.
  //// 2. Convert all these new fragments (IDs) into the string format.
  //// 3. Adjust Field names definitions to string format.

	public static final String DATA_SOURCE_NAME="jdbc/orcl";

  //--Release2.1--//
  //// 1. Replace all Descriptions from the tables included into the PickList with
  //// their joins' counterparts in the SQL_TEMPLATE.
  //// 2. Convert all these new fragments (IDs) into the string format.
	////public static final String SELECT_SQL_TEMPLATE="SELECT ALL BORROWER.DEALID, BORROWER.BORROWERID, BORROWER.COPYID, BORROWER.PRIMARYBORROWERFLAG, BORROWER.BORROWERFIRSTNAME, BORROWER.BORROWERMIDDLEINITIAL, BORROWER.BORROWERLASTNAME, BORROWERTYPE.BTDESCRIPTION, BORROWER.TOTALINCOMEAMOUNT, BORROWER.TOTALLIABILITYAMOUNT, BORROWER.TOTALASSETAMOUNT, BORROWER.NETWORTH FROM BORROWER, BORROWERTYPE  __WHERE__  ORDER BY BORROWER.BORROWERID  ASC";

	//FXP23719 - MCM Nov 28, 2008 - start
	/*
	public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT BORROWER.DEALID, BORROWER.BORROWERID, " +
	"BORROWER.COPYID, BORROWER.PRIMARYBORROWERFLAG, BORROWER.BORROWERFIRSTNAME, " +
	"BORROWER.BORROWERMIDDLEINITIAL, BORROWER.BORROWERLASTNAME, " +
	"to_char(BORROWER.BORROWERTYPEID) BORROWERTYPEID_STR, BORROWER.TOTALINCOMEAMOUNT, BORROWER.TOTALLIABILITYAMOUNT, " +
	"BORROWER.TOTALASSETAMOUNT, BORROWER.NETWORTH " +
	"FROM BORROWER, BORROWERTYPE, DEAL  __WHERE__  ORDER BY BORROWER.BORROWERID  ASC";
	*/
	public static final String SELECT_SQL_TEMPLATE="SELECT BORROWER.DEALID, BORROWER.BORROWERID, " +
	"BORROWER.COPYID, BORROWER.PRIMARYBORROWERFLAG, BORROWER.BORROWERFIRSTNAME, " +
	"BORROWER.BORROWERMIDDLEINITIAL, BORROWER.BORROWERLASTNAME, BORROWER.BORROWERLASTNAME, to_char(BORROWER.SUFFIXID) BORROWERSUFFIXID_STR, " +
	"to_char(BORROWER.BORROWERTYPEID) BORROWERTYPEID_STR, BORROWER.TOTALINCOMEAMOUNT, BORROWER.TOTALLIABILITYAMOUNT, " +
	"BORROWER.TOTALASSETAMOUNT, BORROWER.NETWORTH " +
	"FROM BORROWER  __WHERE__  ORDER BY BORROWER.BORROWERID  ASC";
	   
    //public static final String MODIFYING_QUERY_TABLE_NAME="BORROWER, BORROWERTYPE";
    public static final String MODIFYING_QUERY_TABLE_NAME="BORROWER";

    /*
    public static final String STATIC_WHERE_CRITERIA 
    =" (BORROWER.BORROWERTYPEID  =  BORROWERTYPE.BORROWERTYPEID) "
    +"AND (BORROWER.INSTITUTIONPROFILEID  =  DEAL.INSTITUTIONPROFILEID)";
    */
    public static final String STATIC_WHERE_CRITERIA = "";
	//FXP23719 - MCM Nov 28, 2008 - end

    public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="BORROWER.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFBORROWERID="BORROWER.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="BORROWER.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFPRIMARYFLAG="BORROWER.PRIMARYBORROWERFLAG";
	public static final String COLUMN_DFPRIMARYFLAG="PRIMARYBORROWERFLAG";
	public static final String QUALIFIED_COLUMN_DFFIRSTNAME="BORROWER.BORROWERFIRSTNAME";
	public static final String COLUMN_DFFIRSTNAME="BORROWERFIRSTNAME";
	public static final String QUALIFIED_COLUMN_DFBORROWERMIDDLENAME="BORROWER.BORROWERMIDDLEINITIAL";
	public static final String COLUMN_DFBORROWERMIDDLENAME="BORROWERMIDDLEINITIAL";
	public static final String QUALIFIED_COLUMN_DFLASTNAME="BORROWER.BORROWERLASTNAME";
	public static final String COLUMN_DFLASTNAME="BORROWERLASTNAME";

    public static final String QUALIFIED_COLUMN_DFSUFFIX="BORROWER.BORROWERSUFFIXID_STR";
    public static final String COLUMN_DFSUFFIX="BORROWERSUFFIXID_STR";
  //--Release2.1--//
  //// 3. Adjust Field names definitions to string format.
	////public static final String QUALIFIED_COLUMN_DFBORROWERTYPE="BORROWERTYPE.BTDESCRIPTION";
	////public static final String COLUMN_DFBORROWERTYPE="BTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFBORROWERTYPEDESC="BORROWER.BORROWERTYPEID_STR";
	public static final String COLUMN_DFBORROWERTYPEDESC="BORROWERTYPEID_STR";

	public static final String QUALIFIED_COLUMN_DFTOTALINCOMEAMOUNT="BORROWER.TOTALINCOMEAMOUNT";
	public static final String COLUMN_DFTOTALINCOMEAMOUNT="TOTALINCOMEAMOUNT";
	public static final String QUALIFIED_COLUMN_DFTOTALLIABILITYAMOUNT="BORROWER.TOTALLIABILITYAMOUNT";
	public static final String COLUMN_DFTOTALLIABILITYAMOUNT="TOTALLIABILITYAMOUNT";
	public static final String QUALIFIED_COLUMN_DFTOTALASSETAMOUNT="BORROWER.TOTALASSETAMOUNT";
	public static final String COLUMN_DFTOTALASSETAMOUNT="TOTALASSETAMOUNT";
	public static final String QUALIFIED_COLUMN_DFBORROWERNETWORTH="BORROWER.NETWORTH";
	public static final String COLUMN_DFBORROWERNETWORTH="NETWORTH";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIMARYFLAG,
				COLUMN_DFPRIMARYFLAG,
				QUALIFIED_COLUMN_DFPRIMARYFLAG,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFIRSTNAME,
				COLUMN_DFFIRSTNAME,
				QUALIFIED_COLUMN_DFFIRSTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERMIDDLENAME,
				COLUMN_DFBORROWERMIDDLENAME,
				QUALIFIED_COLUMN_DFBORROWERMIDDLENAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLASTNAME,
				COLUMN_DFLASTNAME,
				QUALIFIED_COLUMN_DFLASTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
        
        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFSUFFIX,
                COLUMN_DFSUFFIX,
                QUALIFIED_COLUMN_DFSUFFIX,
                String.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERTYPEDESC,
				COLUMN_DFBORROWERTYPEDESC,
				QUALIFIED_COLUMN_DFBORROWERTYPEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALINCOMEAMOUNT,
				COLUMN_DFTOTALINCOMEAMOUNT,
				QUALIFIED_COLUMN_DFTOTALINCOMEAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALLIABILITYAMOUNT,
				COLUMN_DFTOTALLIABILITYAMOUNT,
				QUALIFIED_COLUMN_DFTOTALLIABILITYAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALASSETAMOUNT,
				COLUMN_DFTOTALASSETAMOUNT,
				QUALIFIED_COLUMN_DFTOTALASSETAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERNETWORTH,
				COLUMN_DFBORROWERNETWORTH,
				QUALIFIED_COLUMN_DFBORROWERNETWORTH,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

