package mosApp.MosSystem;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.CommandFieldDescriptor;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.ViewBeanBase;
import com.iplanet.jato.view.WebActionHandler;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.ChildDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.HREF;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

//// Original (iMT) inheritance.
////public class pgSignOnViewBean extends ViewBeanBase

//// Highly discussable implementation with the XXXXHandler extension of the
//// ViewBeanBase JATO class. The sequence diagrams must be verified. On the other hand,
//// This implementation allows to use the basic JATO framework elements such as
//// RequestContext() class. Theoretically, it could allow to override the JATO/iMT
//// auto-migration. Temporarily these fragments are commented out now for the compability
//// with the stand alone MWHC<--PHC<--XXXHandlerName Utility hierarchy.
//// getModel(ClassName.class), etc., could be also rewritten in the Utility hierarchy
//// using this inheritance.

//--> Bug :: Should not extends SignOnHandler
//public class pgSignOnViewBean extends SignOnHandler
public class pgSignOnViewBean extends ViewBeanBase
  implements ViewBean
{
  /**
   *
   *
   */
  public pgSignOnViewBean()
  {
    super(PAGE_NAME);
    setDefaultDisplayURL(DEFAULT_DISPLAY_URL);

    registerChildren();

    initialize();

  }


  /**
   *
   *
   */
  protected void initialize()
  {
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Child manipulation methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  protected View createChild(String name)
  {
    if (name.equals(CHILD_STMESSAGE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STMESSAGE,
        CHILD_STMESSAGE,
        CHILD_STMESSAGE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STVERSION))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STVERSION,
        CHILD_STVERSION,
        CHILD_STVERSION_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_TFUSERID))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TFUSERID,
        CHILD_TFUSERID,
        CHILD_TFUSERID_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_TFPASSWORD))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TFPASSWORD,
        CHILD_TFPASSWORD,
        CHILD_TFPASSWORD_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_BTSUBMIT))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTSUBMIT,
        CHILD_BTSUBMIT,
        CHILD_BTSUBMIT_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_BTCANCEL))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTCANCEL,
        CHILD_BTCANCEL,
        CHILD_BTCANCEL_RESET_VALUE,
        null);
        return child;

    }
    else
    if (name.equals(CHILD_STERRORFLAG))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STERRORFLAG,
        CHILD_STERRORFLAG,
        CHILD_STERRORFLAG_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMGENERATE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMGENERATE,
        CHILD_STPMGENERATE,
        CHILD_STPMGENERATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMHASTITLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMHASTITLE,
        CHILD_STPMHASTITLE,
        CHILD_STPMHASTITLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMHASINFO))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMHASINFO,
        CHILD_STPMHASINFO,
        CHILD_STPMHASINFO_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMHASTABLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMHASTABLE,
        CHILD_STPMHASTABLE,
        CHILD_STPMHASTABLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMHASOK))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMHASOK,
        CHILD_STPMHASOK,
        CHILD_STPMHASOK_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMTITLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMTITLE,
        CHILD_STPMTITLE,
        CHILD_STPMTITLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMINFOMSG))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMINFOMSG,
        CHILD_STPMINFOMSG,
        CHILD_STPMINFOMSG_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMONOK))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMONOK,
        CHILD_STPMONOK,
        CHILD_STPMONOK_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMMSGS))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMMSGS,
        CHILD_STPMMSGS,
        CHILD_STPMMSGS_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPMMSGTYPES))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMMSGTYPES,
        CHILD_STPMMSGTYPES,
        CHILD_STPMMSGTYPES_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMGENERATE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMGENERATE,
        CHILD_STAMGENERATE,
        CHILD_STAMGENERATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMHASTITLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMHASTITLE,
        CHILD_STAMHASTITLE,
        CHILD_STAMHASTITLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMHASINFO))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMHASINFO,
        CHILD_STAMHASINFO,
        CHILD_STAMHASINFO_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMHASTABLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMHASTABLE,
        CHILD_STAMHASTABLE,
        CHILD_STAMHASTABLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMTITLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMTITLE,
        CHILD_STAMTITLE,
        CHILD_STAMTITLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMINFOMSG))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMINFOMSG,
        CHILD_STAMINFOMSG,
        CHILD_STAMINFOMSG_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMMSGS))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMMSGS,
        CHILD_STAMMSGS,
        CHILD_STAMMSGS_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMMSGTYPES))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMMSGTYPES,
        CHILD_STAMMSGTYPES,
        CHILD_STAMMSGTYPES_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMDIALOGMSG))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMDIALOGMSG,
        CHILD_STAMDIALOGMSG,
        CHILD_STAMDIALOGMSG_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAMBUTTONSHTML))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMBUTTONSHTML,
        CHILD_STAMBUTTONSHTML,
        CHILD_STAMBUTTONSHTML_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_DETECTALERTTASKS))
    {
      HiddenField child = new HiddenField(this,
        getDefaultModel(),
        CHILD_DETECTALERTTASKS,
        CHILD_DETECTALERTTASKS,
        CHILD_DETECTALERTTASKS_RESET_VALUE,
        null);
      return child;
    }
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> by BILLY 15July2002
    else
    if (name.equals(CHILD_BTACTMSG))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTACTMSG,
        CHILD_BTACTMSG,
        CHILD_BTACTMSG_RESET_VALUE,
        new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));
        return child;
    }
    //===========================================
    //// Link to toggle language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new session
    //// language id throughout the all modulus.
    else
    if (name.equals(CHILD_TOGGLELANGUAGEHREF))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_TOGGLELANGUAGEHREF,
        CHILD_TOGGLELANGUAGEHREF,
        CHILD_TOGGLELANGUAGEHREF_RESET_VALUE,
        null);
        return child;
    }
    else
      throw new IllegalArgumentException("Invalid child name [" + name + "]");
  }


  /**
   *
   *
   */
  public void resetChildren()
  {
    getStMessage().setValue(CHILD_STMESSAGE_RESET_VALUE);
    getStVersion().setValue(CHILD_STVERSION_RESET_VALUE);
    getTfUserID().setValue(CHILD_TFUSERID_RESET_VALUE);
    getTfPassword().setValue(CHILD_TFPASSWORD_RESET_VALUE);
    getBtSubmit().setValue(CHILD_BTSUBMIT_RESET_VALUE);
    getBtCancel().setValue(CHILD_BTCANCEL_RESET_VALUE);
    getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
    getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
    getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
    getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
    getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
    getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
    getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
    getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
    getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
    getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
    getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
    getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
    getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
    getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
    getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
    getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
    getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
    getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
    getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
    getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
    getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
    getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> Test by BILLY 15July2002
    getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value throughout all modulus.
    getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);
  }


  /**
   *
   *
   */
  protected void registerChildren()
  {
    registerChild(CHILD_STMESSAGE,StaticTextField.class);
    registerChild(CHILD_STVERSION,StaticTextField.class);
    registerChild(CHILD_TFUSERID,TextField.class);
    registerChild(CHILD_TFPASSWORD,TextField.class);
    registerChild(CHILD_BTSUBMIT,Button.class);
    registerChild(CHILD_BTCANCEL,Button.class);
    registerChild(CHILD_STERRORFLAG,StaticTextField.class);
    registerChild(CHILD_STPMGENERATE,StaticTextField.class);
    registerChild(CHILD_STPMHASTITLE,StaticTextField.class);
    registerChild(CHILD_STPMHASINFO,StaticTextField.class);
    registerChild(CHILD_STPMHASTABLE,StaticTextField.class);
    registerChild(CHILD_STPMHASOK,StaticTextField.class);
    registerChild(CHILD_STPMTITLE,StaticTextField.class);
    registerChild(CHILD_STPMINFOMSG,StaticTextField.class);
    registerChild(CHILD_STPMONOK,StaticTextField.class);
    registerChild(CHILD_STPMMSGS,StaticTextField.class);
    registerChild(CHILD_STPMMSGTYPES,StaticTextField.class);
    registerChild(CHILD_STAMGENERATE,StaticTextField.class);
    registerChild(CHILD_STAMHASTITLE,StaticTextField.class);
    registerChild(CHILD_STAMHASINFO,StaticTextField.class);
    registerChild(CHILD_STAMHASTABLE,StaticTextField.class);
    registerChild(CHILD_STAMTITLE,StaticTextField.class);
    registerChild(CHILD_STAMINFOMSG,StaticTextField.class);
    registerChild(CHILD_STAMMSGS,StaticTextField.class);
    registerChild(CHILD_STAMMSGTYPES,StaticTextField.class);
    registerChild(CHILD_STAMDIALOGMSG,StaticTextField.class);
    registerChild(CHILD_STAMBUTTONSHTML,StaticTextField.class);
    registerChild(CHILD_DETECTALERTTASKS,HiddenField.class);
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> Test by BILLY 15July2002
    registerChild(CHILD_BTACTMSG,Button.class);
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
    registerChild(CHILD_TOGGLELANGUAGEHREF,HREF.class);
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Model management methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  public Model[] getWebActionModels(int executionType)
  {
    List modelList=new ArrayList();
    switch(executionType)
    {
      case MODEL_TYPE_RETRIEVE:
        ;
        break;

      case MODEL_TYPE_UPDATE:
        ;
        break;

      case MODEL_TYPE_DELETE:
        ;
        break;

      case MODEL_TYPE_INSERT:
        ;
        break;

      case MODEL_TYPE_EXECUTE:
        ;
        break;
    }
    return (Model[])modelList.toArray(new Model[0]);
  }

////////////////////////////////////////////////////////
//// Excerption from automatic model execution.
/******
      public Model[] getWebActionModels(int modelType)
      {
          // Mark the CustomModel instance we're bound to as auto-retrieving.
          // This means it will automatically be executed via its retrieve()
          // method when the tiled view begins display.
          switch (modelType)
          {
              case MODEL_TYPE_RETRIEVE:
                  return new Model[] {getCustomModel()};
          }

          return new Model[0];
      }


      public CustomModel getCustomModel()
      {
          return (CustomModel)getRequestContext().getModelManager().getModel(CustomModel.class);
      }

***/
/////////////////////////////////////////////////////////


  ////////////////////////////////////////////////////////////////////////////////
  // View flow control methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  public void beginDisplay(DisplayEvent event)
    throws ModelControlException
  {
    super.beginDisplay(event);
  }


  /**
   *
   *
   */
  public boolean beforeModelExecutes(Model model, int executionContext)
  {

    // This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
    return super.beforeModelExecutes(model, executionContext);

  }


  /**
   *
   *
   */
  public void afterModelExecutes(Model model, int executionContext)
  {

    // This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
    super.afterModelExecutes(model, executionContext);

  }


  /**
   *
   *
   */
  public void afterAllModelsExecute(int executionContext)
  {

    //logger = SysLog.getSysLogger("SIGNON");

    // This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
    super.afterAllModelsExecute(executionContext);

    SignOnHandler handler =(SignOnHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    handler.populatePageDisplayFields(this);

    //// VERY TEMP COMMENTED OUT. Currently brings NullPointerException.
    ////handler.pageSaveState();

    // The following code block was migrated from the this_onBeforeRowDisplayEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    int rc = PROCEED;

    SignOnHandler handler =(SignOnHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    handler.populatePageDisplayFields(this);

    handler.pageSaveState();

    return(rc);
    */
  }


  /**
   *
   *
   */
  public void onModelError(Model model, int executionContext, ModelControlException exception)
    throws ModelControlException
  {

    // This is the analog of NetDynamics this_onDataObjectErrorEvent
    super.onModelError(model, executionContext, exception);

  }


  /**
   *
   *
   */
  public StaticTextField getStMessage()
  {
    return (StaticTextField)getChild(CHILD_STMESSAGE);
  }


  /**
   *
   *
   */
  public StaticTextField getStVersion()
  {
    return (StaticTextField)getChild(CHILD_STVERSION);
  }


  /**
   *
   *
   */
  public boolean beginStVersionDisplay(ChildDisplayEvent event)
  {
    //Object value = getStVersion().getValue();
    //return true;

    // The following code block was migrated from the stVersion_onBeforeDisplayEvent method
    boolean rc = true;

    SignOnHandler handler =(SignOnHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    handler.setVersionNumber(this);

    handler.pageSaveState();

    return(rc);

  }


  /**
   *
   *
   */
  public TextField getTfUserID()
  {
    return (TextField)getChild(CHILD_TFUSERID);
  }


  /**
   *
   *
   */
  public TextField getTfPassword()
  {
    return (TextField)getChild(CHILD_TFPASSWORD);
  }


  /**
   *
   *
   */

   public void handleBtSubmitRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  ////public void handleBtSubmitRequest(RequestContext requestContext)
  ////	throws ServletException, IOException
  {

    logger = SysLog.getSysLogger("SIGNPAGE");

    Button field = getBtSubmit();

    field.mapSourceTargetNVPs();

    SignOnHandler handler =(SignOnHandler) this.handler.cloneSS();
    handler.memberInit();

    //logger.debug("SNONH@handleBtSubmitRequest::After init");
    //// execution of all executing Web action Models. Test it!!!
    this.handleWebAction(WebActionHandler.ACTION_EXECUTE);

    try
    {
      handler.handleLogin(this, true);
    }
    catch(ModelControlException mce)
    {
      logger.debug("SNONH@handleBtSubmitRequest::ClassNotFound Exception: " + mce);
    }
    catch(SQLException sqle)
    {
      logger.debug("SNONH@handleBtSubmitRequest::SQL Exception: " + sqle);
    }

    //logger.debug("SNONH@handleBtSubmitRequest::After handleLogin");

    //// Temp commented out to test the generic call from the SignOn
    //// ViewBeanHandler. After the testing of the converted BasisXpress
    //// this call will be finally moved to the handler. It is tested and
    //// works in both reductions: here to go to the IWorkQueue directly and
    //// in the PageHandlerCommmon class to obtain the target on fly.

    //// Finally commented out as the redirection is done now from the SignOnHandler.
    ////targetView.forwardTo(getRequestContext());

    //logger.debug("I am successfully forwarded to the IWorkQueue page");
  }


  /**
   *
   *
   */
  public Button getBtSubmit()
  {
    return (Button)getChild(CHILD_BTSUBMIT);
  }


  /**
   *
   *
   */
  public String endBtSubmitDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btSubmit_onBeforeHtmlOutputEvent method

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
    /*
    SignOnHandler handler =(SignOnHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    int rc = handler.generateSubmitButton();

    handler.pageSaveState();

    return rc;
    */


    return event.getContent();
  }


  /**
   *
   *
   */
  //// This is a new auto-migrated signature. Should be investigated. The bottom
  //// one is from the previous version.
  ////public void handleBtCancelRequest(RequestInvocationEvent event)
  ////	throws ServletException, IOException
  public void handleBtCancelRequest(RequestContext requestContext)
    throws ServletException, IOException
  {

    SignOnHandler handler =(SignOnHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleUnSupportedCalls();

    handler.postHandlerProtocol();

    return;

  }

  //// Two new methods providing the business logic for the new link to toggle language.
  //// When touched this link should reload the page in opposite language (french versus english)
  //// and set this new session value.
  /**
   *
   *
   */
  public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {

    SignOnHandler handler =(SignOnHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleToggleLanguage();

    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getToggleLanguageHref()
  {
    return (HREF)getChild(CHILD_TOGGLELANGUAGEHREF);
  }

  /**
   *
   *
   */
  public Button getBtCancel()
  {
    return (Button)getChild(CHILD_BTCANCEL);
  }


  /**
   *
   *
   */
  public StaticTextField getStErrorFlag()
  {
    return (StaticTextField)getChild(CHILD_STERRORFLAG);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmGenerate()
  {
    return (StaticTextField)getChild(CHILD_STPMGENERATE);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmHasTitle()
  {
    return (StaticTextField)getChild(CHILD_STPMHASTITLE);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmHasInfo()
  {
    return (StaticTextField)getChild(CHILD_STPMHASINFO);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmHasTable()
  {
    return (StaticTextField)getChild(CHILD_STPMHASTABLE);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmHasOk()
  {
    return (StaticTextField)getChild(CHILD_STPMHASOK);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmTitle()
  {
    return (StaticTextField)getChild(CHILD_STPMTITLE);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmInfoMsg()
  {
    return (StaticTextField)getChild(CHILD_STPMINFOMSG);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmOnOk()
  {
    return (StaticTextField)getChild(CHILD_STPMONOK);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmMsgs()
  {
    return (StaticTextField)getChild(CHILD_STPMMSGS);
  }


  /**
   *
   *
   */
  public StaticTextField getStPmMsgTypes()
  {
    return (StaticTextField)getChild(CHILD_STPMMSGTYPES);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmGenerate()
  {
    return (StaticTextField)getChild(CHILD_STAMGENERATE);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmHasTitle()
  {
    return (StaticTextField)getChild(CHILD_STAMHASTITLE);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmHasInfo()
  {
    return (StaticTextField)getChild(CHILD_STAMHASINFO);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmHasTable()
  {
    return (StaticTextField)getChild(CHILD_STAMHASTABLE);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmTitle()
  {
    return (StaticTextField)getChild(CHILD_STAMTITLE);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmInfoMsg()
  {
    return (StaticTextField)getChild(CHILD_STAMINFOMSG);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmMsgs()
  {
    return (StaticTextField)getChild(CHILD_STAMMSGS);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmMsgTypes()
  {
    return (StaticTextField)getChild(CHILD_STAMMSGTYPES);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmDialogMsg()
  {
    return (StaticTextField)getChild(CHILD_STAMDIALOGMSG);
  }


  /**
   *
   *
   */
  public StaticTextField getStAmButtonsHtml()
  {
    return (StaticTextField)getChild(CHILD_STAMBUTTONSHTML);
  }


  /**
   *
   *
   */
  public HiddenField getDetectAlertTasks()
  {
    return (HiddenField)getChild(CHILD_DETECTALERTTASKS);
  }


  //--> Hidden ActMessageButton (FINDTHISLATER)
  //--> Test by BILLY 15July2002
  public Button getBtActMsg()
  {
    return (Button)getChild(CHILD_BTACTMSG);
  }
  //===========================================


  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Custom Methods - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////

  public void handleActMessageOK(String[] args)
  {
    SignOnHandler handler =(SignOnHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this, true);

    handler.handleActMessageOK(args);

  }

  //// This method overrides the getDefaultURL() framework JATO method and it is located
 //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
 //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
 //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
 //// The full method is still in PHC base class. It should care the
 //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
 //--> Added handling for display SignOn Page based on the parameter passing when user
 //--> Requested.  i.e. Parameter like "?language=fr" for franch and default  = english
 //--> By Billy 27Mar2003
 public String getDisplayURL()
 {
       logger = SysLog.getSysLogger("SignOnVB");
       String url=getDefaultDisplayURL();

       //logger.debug("pgSignOnVB@getDisplayURL::DefaultURL: " + url);

       int languageId = 0;

       // Testing by Billy to set up the Session Language
       String defaultInstanceStateName =
         RequestManager.getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
       SessionStateModelImpl theSessionState =
         (SessionStateModelImpl)RequestManager.getRequestContext().getModelManager().getModel(
         SessionStateModel.class,
         defaultInstanceStateName,
         true,
         true);

       if(theSessionState.getLanguageId() < 0)
       {
          // It was not set yet !!
          //--> Check if user request for the Franch Login Page by
          //--> Passing parameter "?language=xx"
          String theLang = getRequestContext().getRequest().getParameter("language");
          if(theLang != null && theLang.trim().equals("fr"))
            languageId = 1;
          theSessionState.setLanguageId(languageId);
       }
       else
       {
         // It was already set
         languageId = theSessionState.getLanguageId();
       }
       //===========================================



       //logger.debug("pgSignOnVB@getDisplayURL::LanguageFromUSOSession: " + languageId);
       //// Call the language specific URL (business delegation done in the BXResource).
       if(url != null && !url.trim().equals("") && !url.trim().equals("/")) {
          url = BXResources.getBXUrl(url, languageId);
          //logger.debug("pgSignOnVB@getDisplayURL::GoodMode: " + url);
       }
       else {
          url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
          //logger.debug("pgSignOnVB@getDisplayURL::BadMode: " + url);
       }

      return url;
  }
  //==================================================================

  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////

  public static final String PAGE_NAME="pgSignOn";
  //public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgSignOn.jsp";
  protected String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgSignOn.jsp";
  public static Map FIELD_DESCRIPTORS;
  public static final String CHILD_STMESSAGE="stMessage";
  public static final String CHILD_STMESSAGE_RESET_VALUE="";
  public static final String CHILD_STVERSION="stVersion";
  public static final String CHILD_STVERSION_RESET_VALUE="";
  public static final String CHILD_TFUSERID="tfUserID";
  public static final String CHILD_TFUSERID_RESET_VALUE="";
  public static final String CHILD_TFPASSWORD="tfPassword";
  public static final String CHILD_TFPASSWORD_RESET_VALUE="";
  public static final String CHILD_BTSUBMIT="btSubmit";
  public static final String CHILD_BTSUBMIT_RESET_VALUE=" ";
  public static final String CHILD_BTCANCEL="btCancel";
  public static final String CHILD_BTCANCEL_RESET_VALUE="Cancel";
  public static final String CHILD_STERRORFLAG="stErrorFlag";
  public static final String CHILD_STERRORFLAG_RESET_VALUE="N";
  public static final String CHILD_STPMGENERATE="stPmGenerate";
  public static final String CHILD_STPMGENERATE_RESET_VALUE="";
  public static final String CHILD_STPMHASTITLE="stPmHasTitle";
  public static final String CHILD_STPMHASTITLE_RESET_VALUE="";
  public static final String CHILD_STPMHASINFO="stPmHasInfo";
  public static final String CHILD_STPMHASINFO_RESET_VALUE="";
  public static final String CHILD_STPMHASTABLE="stPmHasTable";
  public static final String CHILD_STPMHASTABLE_RESET_VALUE="";
  public static final String CHILD_STPMHASOK="stPmHasOk";
  public static final String CHILD_STPMHASOK_RESET_VALUE="";
  public static final String CHILD_STPMTITLE="stPmTitle";
  public static final String CHILD_STPMTITLE_RESET_VALUE="";
  public static final String CHILD_STPMINFOMSG="stPmInfoMsg";
  public static final String CHILD_STPMINFOMSG_RESET_VALUE="";
  public static final String CHILD_STPMONOK="stPmOnOk";
  public static final String CHILD_STPMONOK_RESET_VALUE="";
  public static final String CHILD_STPMMSGS="stPmMsgs";
  public static final String CHILD_STPMMSGS_RESET_VALUE="";
  public static final String CHILD_STPMMSGTYPES="stPmMsgTypes";
  public static final String CHILD_STPMMSGTYPES_RESET_VALUE="";
  public static final String CHILD_STAMGENERATE="stAmGenerate";
  public static final String CHILD_STAMGENERATE_RESET_VALUE="";
  public static final String CHILD_STAMHASTITLE="stAmHasTitle";
  public static final String CHILD_STAMHASTITLE_RESET_VALUE="";
  public static final String CHILD_STAMHASINFO="stAmHasInfo";
  public static final String CHILD_STAMHASINFO_RESET_VALUE="";
  public static final String CHILD_STAMHASTABLE="stAmHasTable";
  public static final String CHILD_STAMHASTABLE_RESET_VALUE="";
  public static final String CHILD_STAMTITLE="stAmTitle";
  public static final String CHILD_STAMTITLE_RESET_VALUE="";
  public static final String CHILD_STAMINFOMSG="stAmInfoMsg";
  public static final String CHILD_STAMINFOMSG_RESET_VALUE="";
  public static final String CHILD_STAMMSGS="stAmMsgs";
  public static final String CHILD_STAMMSGS_RESET_VALUE="";
  public static final String CHILD_STAMMSGTYPES="stAmMsgTypes";
  public static final String CHILD_STAMMSGTYPES_RESET_VALUE="";
  public static final String CHILD_STAMDIALOGMSG="stAmDialogMsg";
  public static final String CHILD_STAMDIALOGMSG_RESET_VALUE="";
  public static final String CHILD_STAMBUTTONSHTML="stAmButtonsHtml";
  public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE="";
  public static final String CHILD_DETECTALERTTASKS="detectAlertTasks";
  public static final String CHILD_DETECTALERTTASKS_RESET_VALUE="";
  //--> Hidden ActMessageButton (FINDTHISLATER)
  //--> Test by BILLY 15July2002
  public static final String CHILD_BTACTMSG="btActMsg";
  public static final String CHILD_BTACTMSG_RESET_VALUE="ActMsg";
  //// New link to toggle the language. When touched this link should reload the
  //// page in opposite language (french versus english) and set this new language
  //// session id throughout all modulus.
  public static final String CHILD_TOGGLELANGUAGEHREF="toggleLanguageHref";
  public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE="";

  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Custom Members - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////

  private SignOnHandler handler=new SignOnHandler();
  public SysLogger logger;
}

