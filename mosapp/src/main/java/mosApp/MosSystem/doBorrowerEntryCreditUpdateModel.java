package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doBorrowerEntryCreditUpdateModel extends QueryModel, UpdateQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCreditRefId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCreditRefId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCreditTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCreditTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfInstitutionName();

	
	/**
	 * 
	 * 
	 */
	public void setDfInstitutionName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAccountNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfAccountNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCurrentBalance();

	
	/**
	 * 
	 * 
	 */
	public void setDfCurrentBalance(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFCREDITREFID="dfCreditRefId";
	public static final String FIELD_DFCREDITTYPEID="dfCreditTypeId";
	public static final String FIELD_DFINSTITUTIONNAME="dfInstitutionName";
	public static final String FIELD_DFACCOUNTNUMBER="dfAccountNumber";
	public static final String FIELD_DFCURRENTBALANCE="dfCurrentBalance";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

