package mosApp.MosSystem;

import java.text.*;
import java.util.*;

/**
 * <p>Title: NBCAdjudicationRequestType.java </p>
 *
 * <p>Description: NBC adjudication request type prototype</p>
 *
 * <p>Copyright: </p>
 *
 * <p>Company: filogix</p>
 *
 * @author Vlad Ivanov
 * @version 1.0
 */

public class NBCAdjudicationRequestType extends AdjudicationRequestType{
                public NBCAdjudicationRequestType() {
                  //TODO: implement NBC specific gui logic here.
                }

                public int getDocumentTypeId() {
                  // this is the id in the DOCUMENTTYPE table if document exists.
                  return 0; //should be defined
                }
              }


