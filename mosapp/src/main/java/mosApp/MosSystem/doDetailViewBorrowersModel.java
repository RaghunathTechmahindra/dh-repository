package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 * <p>
 * Title: doDetailViewBorrowersModel
 * </p>
 *
 * <p>
 * Description: Model class for Applicant detials part of the deal summary
 * screen
 * 
 *
 * @author
 * @version 1.1 Date: 7/27/006 <br>
 *          Author: NBC/PP Implementation Team <br>
 *          Change: <br>
 *          Added 5 new fields in the applicant details section
 *
 */
public interface doDetailViewBorrowersModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	/**
	 * 
	 * 
	 */
	public String getDfSalutation();

	/**
	 * 
	 * 
	 */
	public void setDfSalutation(String value);

	/**
	 * 
	 * 
	 */
	public String getDfFirstName();

	/**
	 * 
	 * 
	 */
	public void setDfFirstName(String value);

	/**
	 * 
	 * 
	 */
	public String getDfMiddleName();

	/**
	 * 
	 * 
	 */
	public void setDfMiddleName(String value);

	/**
	 * 
	 * 
	 */
	public String getDfLastName();

	/**
	 * 
	 * 
	 */
	public void setDfLastName(String value);
	
	/**
	 * 
	 * 
	 */
	public String getDfSuffix();

	/**
	 * 
	 * 
	 */
	public void setDfSuffix(String value);

	/**
	 * 
	 * 
	 */
	public String getDfApplicantType();

	/**
	 * 
	 * 
	 */
	public void setDfApplicantType(String value);

	/**
	 * 
	 * 
	 */
	public String getDfPrimaryBorrower();

	/**
	 * 
	 * 
	 */
	public void setDfPrimaryBorrower(String value);

	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfDateOfBirth();

	/**
	 * 
	 * 
	 */
	public void setDfDateOfBirth(java.sql.Timestamp value);

	/**
	 * 
	 * 
	 */
	public String getDfMaritalStatus();

	/**
	 * 
	 * 
	 */
	public void setDfMaritalStatus(String value);

	/**
	 * 
	 * 
	 */
	public String getDfSINNo();

	/**
	 * 
	 * 
	 */
	public void setDfSINNo(String value);

	/**
	 * 
	 * 
	 */
	public String getDfCitizenshipStatus();

	/**
	 * 
	 * 
	 */
	public void setDfCitizenshipStatus(String value);

	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfNumberOfDependants();

	/**
	 * 
	 * 
	 */
	public void setDfNumberOfDependants(java.math.BigDecimal value);

	/**
	 * 
	 * 
	 */
	public String getDfExistingCleint();

	/**
	 * 
	 * 
	 */
	public void setDfExistingCleint(String value);

	/**
	 * 
	 * 
	 */
	public String getDfReferenceClientNo();

	/**
	 * 
	 * 
	 */
	public void setDfReferenceClientNo(String value);

	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfNoOfTimesBankrupt();

	/**
	 * 
	 * 
	 */
	public void setDfNoOfTimesBankrupt(java.math.BigDecimal value);

	/**
	 * 
	 * 
	 */
	public String getDfBankruptcyStatus();

	/**
	 * 
	 * 
	 */
	public void setDfBankruptcyStatus(String value);

	/**
	 * 
	 * 
	 */
	public String getDfStaffOfLender();

	/**
	 * 
	 * 
	 */
	public void setDfStaffOfLender(String value);

	/**
	 * 
	 * 
	 */
	public String getDfHomePhoneNo();

	/**
	 * 
	 * 
	 */
	public void setDfHomePhoneNo(String value);

	/**
	 * 
	 * 
	 */
	public String getDfWorkPhoneNo();

	/**
	 * 
	 * 
	 */
	public void setDfWorkPhoneNo(String value);
	
	/**
	 * 
	 * 
	 */
	public String getDfCellPhoneNo();

	/**
	 * 
	 * 
	 */
	public void setDfCellPhoneNo(String value);

	/**
	 * 
	 * 
	 */
	public String getDfFaxNumber();

	/**
	 * 
	 * 
	 */
	public void setDfFaxNumber(String value);

	/**
	 * 
	 * 
	 */
	public String getDfEmailAddress();

	/**
	 * 
	 * 
	 */
	public void setDfEmailAddress(String value);

	/**
	 * 
	 * 
	 */
	public String getDfLanguagePreference();

	/**
	 * 
	 * 
	 */
	public void setDfLanguagePreference(String value);

	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	/**
	 * 
	 * 
	 */
	public String getDfBorrowerWorkPhoneExt();

	/**
	 * 
	 * 
	 */
	public void setDfBorrowerWorkPhoneExt(String value);

	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCreditScore();

	/**
	 * 
	 * 
	 */
	public void setDfCreditScore(java.math.BigDecimal value);

	/**
	 * 
	 * 
	 */
	public String getSnapShotBorrowerName();

	/**
	 * 
	 * 
	 */
	public void setSnapShotBorrowerName(String value);
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFSALUTATION="dfSalutation";
	public static final String FIELD_DFFIRSTNAME="dfFirstName";
	public static final String FIELD_DFMIDDLENAME="dfMiddleName";
	public static final String FIELD_DFLASTNAME="dfLastName";
	public static final String FIELD_DFSUFFIX="dfSuffix";
	public static final String FIELD_DFAPPLICANTTYPE="dfApplicantType";
	public static final String FIELD_DFPRIMARYBORROWER="dfPrimaryBorrower";
	public static final String FIELD_DFDATEOFBIRTH="dfDateOfBirth";
	public static final String FIELD_DFMARITALSTATUS="dfMaritalStatus";
	public static final String FIELD_DFSINNO="dfSINNo";
	public static final String FIELD_DFCITIZENSHIPSTATUS="dfCitizenshipStatus";
	public static final String FIELD_DFNUMBEROFDEPENDANTS="dfNumberOfDependants";
	public static final String FIELD_DFEXISTINGCLEINT="dfExistingCleint";
	public static final String FIELD_DFREFERENCECLIENTNO="dfReferenceClientNo";
	public static final String FIELD_DFNOOFTIMESBANKRUPT="dfNoOfTimesBankrupt";
	public static final String FIELD_DFBANKRUPTCYSTATUS="dfBankruptcyStatus";
	public static final String FIELD_DFSTAFFOFLENDER="dfStaffOfLender";
	public static final String FIELD_DFHOMEPHONENO="dfHomePhoneNo";
	public static final String FIELD_DFWORKPHONENO="dfWorkPhoneNo";
	public static final String FIELD_DFCELLPHONENO="dfCellPhoneNo";
	public static final String FIELD_DFFAXNUMBER="dfFaxNumber";
	public static final String FIELD_DFEMAILADDRESS="dfEmailAddress";
	public static final String FIELD_DFLANGUAGEPREFERENCE="dfLanguagePreference";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFBORROWERWORKPHONEEXT="dfBorrowerWorkPhoneExt";
	public static final String FIELD_DFCREDITSCORE="dfCreditScore";
	public static final String FIELD_SNAPSHOTBORROWERNAME="SnapShotBorrowerName";
	
	// ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
	public static final String FIELD_DFGENDER = "dfBorrowerGender";
	public static final String FIELD_DFSOLICITATION = "dfSolicitation";
	public static final String FIELD_DFSMOKER = "dfSmokeStatus";
	public static final String FIELD_DFPREFERREDMETHODOFCONTACT = "dfPreferredMethodOfContact";
	public static final String FIELD_DFEMPLOYEENUMBER = "dfEmployeeNumber";
	 public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	/**
	 * getDfGender
	 *
	 * @param None <br>
	 *
	 * @return String : the result of getDfGender <br>
	 */
	public String getDfGender();

	/**
	 * setDfGender
	 *
	 * @param String <br>
	 *
	 * @return Void : the result of setDfGender <br>
	 */
	public void setDfGender(String value);

	/**
	 * getDfSolicitation
	 *
	 * @param None <br>
	 *
	 * @return String : the result of getDfSolicitation <br>
	 */
	public String getDfSolicitation();

	/**
	 * setDfSolicitation
	 *
	 * @param String <br>
	 *
	 * @return Void : the result of setDfSolicitation <br>
	 */
	public void setDfSolicitation(String value);

	/**
	 * getDfSmoker
	 *
	 * @param None <br>
	 *
	 * @return String : the result of getDfSmoker <br>
	 */
	public String getDfSmoker();

	/**
	 * setDfSmoker
	 *
	 * @param String <br>
	 *
	 * @return Void : the result of setDfSmoker <br>
	 */
	public void setDfSmoker(String value);

	/**
	 * getDfEmployeeNumber
	 *
	 * @param None <br>
	 *
	 * @return String : the result of getDfEmployeeNumber <br>
	 */
	public String getDfEmployeeNumber();
	
	/**
	 * setDfEmployeeNumber
	 *
	 * @param String <br>
	 *
	 * @return Void : the result of setDfEmployeeNumber <br>
	 */
	public void setDfEmployeeNumber(String value);
	
	/**
	 * getDfPreferredMethodOfContact
	 *
	 * @param None <br>
	 *
	 * @return String : the result of getDfPreferredMethodOfContact <br>
	 */
	public String getDfPreferredMethodOfContact();
	
	/**
	 * setDfPreferredMethodOfContact
	 *
	 * @param String <br>
	 *
	 * @return Void : the result of setDfPreferredMethodOfContact <br>
	 */
	public void setDfPreferredMethodOfContact(String value);
	// ***** Change by NBC Impl. Team - Version 1.1 - End*****//
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
	  public java.math.BigDecimal getDfInstitutionId();
	  public void setDfInstitutionId(java.math.BigDecimal id);
	  
}
