package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doMWQPickListModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public String getDfDescription();


	/**
	 *
	 *
	 */
	public void setDfDescription(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAssignTaskId();


	/**
	 *
	 *
	 */
	public void setDfAssignTaskId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfTMDescription();


	/**
	 *
	 *
	 */
	public void setDfTMDescription(String value);


	/**
	 *
	 *
	 */
	public String getDfTASKNAME();


	/**
	 *
	 *
	 */
	public void setDfTASKNAME(String value);


	/**
	 *
	 *
	 */
	public String getDfPriorityDescription();


	/**
	 *
	 *
	 */
	public void setDfPriorityDescription(String value);




	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDESCRIPTION="dfDescription";
	public static final String FIELD_DFASSIGNTASKID="dfAssignTaskId";
	public static final String FIELD_DFTMDESCRIPTION="dfTMDescription";
	public static final String FIELD_DFTASKNAME="dfTASKNAME";
	public static final String FIELD_DFPRIORITYDESCRIPTION="dfPriorityDescription";
  public static final String FIELD_DFTASKID="dfTASKID";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

