package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;
import com.basis100.picklist.BXResources;

import com.basis100.log.*;

/**
 *
 *
 *
 */
public class pgApplicantEntryRepeatedAssetsTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgApplicantEntryRepeatedAssetsTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(100);
		setPrimaryModelClass( doApplicantAssetsSelectModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getHdAssetId().setValue(CHILD_HDASSETID_RESET_VALUE);
		getHdAssetCopyId().setValue(CHILD_HDASSETCOPYID_RESET_VALUE);
		getCbAssetType().setValue(CHILD_CBASSETTYPE_RESET_VALUE);
		getTxAssetDesc().setValue(CHILD_TXASSETDESC_RESET_VALUE);
		getTxAssetValue().setValue(CHILD_TXASSETVALUE_RESET_VALUE);
		getCbAssetIncludeInNetworth().setValue(CHILD_CBASSETINCLUDEINNETWORTH_RESET_VALUE);
		getTxPercentageIncludedInNetworth().setValue(CHILD_TXPERCENTAGEINCLUDEDINNETWORTH_RESET_VALUE);
		getHdPercentageIncludedInNetworth().setValue(CHILD_HDPERCENTAGEINCLUDEDINNETWORTH_RESET_VALUE);
		getBtDeleteAsset().setValue(CHILD_BTDELETEASSET_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_HDASSETID,HiddenField.class);
		registerChild(CHILD_HDASSETCOPYID,HiddenField.class);
		registerChild(CHILD_CBASSETTYPE,ComboBox.class);
		registerChild(CHILD_TXASSETDESC,TextField.class);
		registerChild(CHILD_TXASSETVALUE,TextField.class);
		registerChild(CHILD_CBASSETINCLUDEINNETWORTH,ComboBox.class);
		registerChild(CHILD_TXPERCENTAGEINCLUDEDINNETWORTH,TextField.class);
		registerChild(CHILD_HDPERCENTAGEINCLUDEDINNETWORTH,HiddenField.class);
		registerChild(CHILD_BTDELETEASSET,Button.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By Billy 05Nov2002
    ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

    //Set up all ComboBox options
    cbAssetTypeOptions.populate(getRequestContext());

    //Setup the Yes/No Options
    String yesStr = BXResources.getGenericMsg("YES_LABEL", handler.getTheSessionState().getLanguageId());
    String noStr = BXResources.getGenericMsg("NO_LABEL", handler.getTheSessionState().getLanguageId());
    cbAssetIncludeInNetworthOptions.setOptions(new String[]{yesStr, noStr},new String[]{"Y", "N"});

    handler.pageSaveState();
		//========================================================================

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated RepeatedAssets_onBeforeRowDisplayEvent code here
      //CSpDataObject theObj =(CSpDataObject) getModel(doApplicantAssetsSelectModel.class);
		  //--> This may not necessary for JATO -- comment by BILLY 25July2002
		  if(getdoApplicantAssetsSelectModel().getSize() > 0)
      {
        //--> Important !! If the field (in TiledView) is not binded to any DataField, we must
        //--> set the defaslt value here in order to populate the value in the Model (PrimaryModel).
        //--> Otherwise, when the page submitted (via button or Herf) the user input will not populate
        //--> Comment by BILLY 02Aug2002
        setDisplayFieldValue(CHILD_HDPERCENTAGEINCLUDEDINNETWORTH,
              new String(CHILD_HDPERCENTAGEINCLUDEDINNETWORTH_RESET_VALUE));
        //============================================================================================
        return true;
      }
		  else return false;
		}

		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
    return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_HDASSETID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantAssetsSelectModel(),
				CHILD_HDASSETID,
				doApplicantAssetsSelectModel.FIELD_DFASSETID,
				CHILD_HDASSETID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDASSETCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantAssetsSelectModel(),
				CHILD_HDASSETCOPYID,
				doApplicantAssetsSelectModel.FIELD_DFCOPYID,
				CHILD_HDASSETCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBASSETTYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantAssetsSelectModel(),
				CHILD_CBASSETTYPE,
				doApplicantAssetsSelectModel.FIELD_DFASSETTYPEID,
				CHILD_CBASSETTYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbAssetTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXASSETDESC))
		{
			TextField child = new TextField(this,
				getdoApplicantAssetsSelectModel(),
				CHILD_TXASSETDESC,
				doApplicantAssetsSelectModel.FIELD_DFASSETDESC,
				CHILD_TXASSETDESC_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXASSETVALUE))
		{
			TextField child = new TextField(this,
				getdoApplicantAssetsSelectModel(),
				CHILD_TXASSETVALUE,
				doApplicantAssetsSelectModel.FIELD_DFASSETVALUE,
				CHILD_TXASSETVALUE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBASSETINCLUDEINNETWORTH))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantAssetsSelectModel(),
				CHILD_CBASSETINCLUDEINNETWORTH,
				doApplicantAssetsSelectModel.FIELD_DFINCLUDEINNETWORTH,
				CHILD_CBASSETINCLUDEINNETWORTH_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbAssetIncludeInNetworthOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXPERCENTAGEINCLUDEDINNETWORTH))
		{
			TextField child = new TextField(this,
				getdoApplicantAssetsSelectModel(),
				CHILD_TXPERCENTAGEINCLUDEDINNETWORTH,
				doApplicantAssetsSelectModel.FIELD_DFPERCENTINCLUDEDINNETWORTH,
				CHILD_TXPERCENTAGEINCLUDEDINNETWORTH_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDPERCENTAGEINCLUDEDINNETWORTH))
		{
			HiddenField child = new HiddenField(this,
				//getDefaultModel(),
        getdoApplicantAssetsSelectModel(),
				CHILD_HDPERCENTAGEINCLUDEDINNETWORTH,
				CHILD_HDPERCENTAGEINCLUDEDINNETWORTH,
				CHILD_HDPERCENTAGEINCLUDEDINNETWORTH_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETEASSET))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETEASSET,
				CHILD_BTDELETEASSET,
				CHILD_BTDELETEASSET_RESET_VALUE,
				null);
				return child;

		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdAssetId()
	{
		return (HiddenField)getChild(CHILD_HDASSETID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdAssetCopyId()
	{
		return (HiddenField)getChild(CHILD_HDASSETCOPYID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbAssetType()
	{
		return (ComboBox)getChild(CHILD_CBASSETTYPE);
	}

	/**
	 *
	 *
	 */
	static class CbAssetTypeOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbAssetTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 21Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "ASSETTYPE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public TextField getTxAssetDesc()
	{
		return (TextField)getChild(CHILD_TXASSETDESC);
	}


	/**
	 *
	 *
	 */
	public TextField getTxAssetValue()
	{
		return (TextField)getChild(CHILD_TXASSETVALUE);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbAssetIncludeInNetworth()
	{
		return (ComboBox)getChild(CHILD_CBASSETINCLUDEINNETWORTH);
	}


	/**
	 *
	 *
	 */
	public TextField getTxPercentageIncludedInNetworth()
	{
		return (TextField)getChild(CHILD_TXPERCENTAGEINCLUDEDINNETWORTH);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdPercentageIncludedInNetworth()
	{
		return (HiddenField)getChild(CHILD_HDPERCENTAGEINCLUDEDINNETWORTH);
	}


	/**
	 *
	 *
	 */
	public void handleBtDeleteAssetRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btDeleteAsset_onWebEvent method
		ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());
		handler.handleDelete(false, 10, handler.getRowNdxFromWebEventMethod(event), "RepeatedAssets/hdAssetId", "assetId", "RepeatedAssets/hdAssetCopyId", "Asset");
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtDeleteAsset()
	{
		return (Button)getChild(CHILD_BTDELETEASSET);
	}


	/**
	 *
	 *
	 */
	public String endBtDeleteAssetDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btDeleteAsset_onBeforeHtmlOutputEvent method
		ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public doApplicantAssetsSelectModel getdoApplicantAssetsSelectModel()
	{
		if (doApplicantAssetsSelect == null)
			doApplicantAssetsSelect = (doApplicantAssetsSelectModel) getModel(doApplicantAssetsSelectModel.class);
		return doApplicantAssetsSelect;
	}


	/**
	 *
	 *
	 */
	public void setdoApplicantAssetsSelectModel(doApplicantAssetsSelectModel model)
	{
			doApplicantAssetsSelect = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_HDASSETID="hdAssetId";
	public static final String CHILD_HDASSETID_RESET_VALUE="";
	public static final String CHILD_HDASSETCOPYID="hdAssetCopyId";
	public static final String CHILD_HDASSETCOPYID_RESET_VALUE="";
	public static final String CHILD_CBASSETTYPE="cbAssetType";
	public static final String CHILD_CBASSETTYPE_RESET_VALUE="";
  //--Release2.1--//
	//private static CbAssetTypeOptionList cbAssetTypeOptions=new CbAssetTypeOptionList();
  private CbAssetTypeOptionList cbAssetTypeOptions=new CbAssetTypeOptionList();
	public static final String CHILD_TXASSETDESC="txAssetDesc";
	public static final String CHILD_TXASSETDESC_RESET_VALUE="";
	public static final String CHILD_TXASSETVALUE="txAssetValue";
	public static final String CHILD_TXASSETVALUE_RESET_VALUE="";
	public static final String CHILD_CBASSETINCLUDEINNETWORTH="cbAssetIncludeInNetworth";
	public static final String CHILD_CBASSETINCLUDEINNETWORTH_RESET_VALUE="Y";
  //--Release2.1--//
	//private static OptionList cbAssetIncludeInNetworthOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
	private OptionList cbAssetIncludeInNetworthOptions=new OptionList();
	public static final String CHILD_TXPERCENTAGEINCLUDEDINNETWORTH="txPercentageIncludedInNetworth";
	public static final String CHILD_TXPERCENTAGEINCLUDEDINNETWORTH_RESET_VALUE="";
	public static final String CHILD_HDPERCENTAGEINCLUDEDINNETWORTH="hdPercentageIncludedInNetworth";
	public static final String CHILD_HDPERCENTAGEINCLUDEDINNETWORTH_RESET_VALUE="";
	public static final String CHILD_BTDELETEASSET="btDeleteAsset";
	public static final String CHILD_BTDELETEASSET_RESET_VALUE=" ";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doApplicantAssetsSelectModel doApplicantAssetsSelect=null;

  private ApplicantEntryHandler handler=new ApplicantEntryHandler();

  public SysLogger logger;

}

