package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doNoteCategoryModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public String getDfDealNotesCategory();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealNotesCategory(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealNotesCategoryID();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealNotesCategoryID(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALNOTESCATEGORY="dfDealNotesCategory";
	public static final String FIELD_DFDEALNOTESCATEGORYID="dfDealNotesCategoryID";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

