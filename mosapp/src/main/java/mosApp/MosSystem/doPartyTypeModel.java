package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doPartyTypeModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public String getDfPartyTypeDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfPartyTypeDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPartyTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPartyTypeId(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFPARTYTYPEDESC="dfPartyTypeDesc";
	public static final String FIELD_DFPARTYTYPEID="dfPartyTypeId";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

