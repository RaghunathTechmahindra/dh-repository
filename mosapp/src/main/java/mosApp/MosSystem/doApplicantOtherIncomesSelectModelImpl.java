package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


public class doApplicantOtherIncomesSelectModelImpl extends QueryModelBase
	implements doApplicantOtherIncomesSelectModel
{
	/**
	 *
	 *
	 */
	public doApplicantOtherIncomesSelectModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIncomeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINCOMEID);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINCOMEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIncomeTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINCOMETYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINCOMETYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIncomeDesc()
	{
		return (String)getValue(FIELD_DFINCOMEDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeDesc(String value)
	{
		setValue(FIELD_DFINCOMEDESC,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIncomePeriodId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINCOMEPERIODID);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomePeriodId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINCOMEPERIODID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIncomeAmt()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINCOMEAMT);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeAmt(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINCOMEAMT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIncludeInGDS()
	{
		return (String)getValue(FIELD_DFINCLUDEINGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfIncludeInGDS(String value)
	{
		setValue(FIELD_DFINCLUDEINGDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPercentIncludedInGDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPERCENTINCLUDEDINGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPercentIncludedInGDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPERCENTINCLUDEDINGDS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIncludeInTDS()
	{
		return (String)getValue(FIELD_DFINCLUDEINTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfIncludeInTDS(String value)
	{
		setValue(FIELD_DFINCLUDEINTDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPercentIncludedInTDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPERCENTINCLUDEDINTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPercentIncludedInTDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPERCENTINCLUDEDINTDS,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT INCOME.BORROWERID, " +
            "INCOME.INCOMEID, INCOME.COPYID, INCOME.INCOMETYPEID, INCOME.INCOMEDESCRIPTION, " +
            "INCOME.INCOMEPERIODID, INCOME.INCOMEAMOUNT, INCOME.INCINCLUDEINGDS, " +
            "INCOME.INCPERCENTINGDS, INCOME.INCINCLUDEINTDS, INCOME.INCPERCENTINTDS " +
            "FROM INCOME, EMPLOYMENTHISTORY  __WHERE__  ORDER BY INCOME.INCOMEID  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME="INCOME, EMPLOYMENTHISTORY";
	public static final String STATIC_WHERE_CRITERIA
            = " INCOME.INCOMEID  =  EMPLOYMENTHISTORY.INCOMEID (+) " +
            "AND INCOME.BORROWERID  =  EMPLOYMENTHISTORY.BORROWERID (+) " +
            "AND INCOME.COPYID  =  EMPLOYMENTHISTORY.COPYID (+) " +
            "AND INCOME.INSTITUTIONPROFILEID = EMPLOYMENTHISTORY.INSTITUTIONPROFILEID (+)" +
            "AND EMPLOYMENTHISTORY.EMPLOYMENTHISTORYID is null  ";
                        
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="INCOME.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFINCOMEID="INCOME.INCOMEID";
	public static final String COLUMN_DFINCOMEID="INCOMEID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="INCOME.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFINCOMETYPEID="INCOME.INCOMETYPEID";
	public static final String COLUMN_DFINCOMETYPEID="INCOMETYPEID";
	public static final String QUALIFIED_COLUMN_DFINCOMEDESC="INCOME.INCOMEDESCRIPTION";
	public static final String COLUMN_DFINCOMEDESC="INCOMEDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFINCOMEPERIODID="INCOME.INCOMEPERIODID";
	public static final String COLUMN_DFINCOMEPERIODID="INCOMEPERIODID";
	public static final String QUALIFIED_COLUMN_DFINCOMEAMT="INCOME.INCOMEAMOUNT";
	public static final String COLUMN_DFINCOMEAMT="INCOMEAMOUNT";
	public static final String QUALIFIED_COLUMN_DFINCLUDEINGDS="INCOME.INCINCLUDEINGDS";
	public static final String COLUMN_DFINCLUDEINGDS="INCINCLUDEINGDS";
	public static final String QUALIFIED_COLUMN_DFPERCENTINCLUDEDINGDS="INCOME.INCPERCENTINGDS";
	public static final String COLUMN_DFPERCENTINCLUDEDINGDS="INCPERCENTINGDS";
	public static final String QUALIFIED_COLUMN_DFINCLUDEINTDS="INCOME.INCINCLUDEINTDS";
	public static final String COLUMN_DFINCLUDEINTDS="INCINCLUDEINTDS";
	public static final String QUALIFIED_COLUMN_DFPERCENTINCLUDEDINTDS="INCOME.INCPERCENTINTDS";
	public static final String COLUMN_DFPERCENTINCLUDEDINTDS="INCPERCENTINTDS";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEID,
				COLUMN_DFINCOMEID,
				QUALIFIED_COLUMN_DFINCOMEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMETYPEID,
				COLUMN_DFINCOMETYPEID,
				QUALIFIED_COLUMN_DFINCOMETYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEDESC,
				COLUMN_DFINCOMEDESC,
				QUALIFIED_COLUMN_DFINCOMEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEPERIODID,
				COLUMN_DFINCOMEPERIODID,
				QUALIFIED_COLUMN_DFINCOMEPERIODID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEAMT,
				COLUMN_DFINCOMEAMT,
				QUALIFIED_COLUMN_DFINCOMEAMT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCLUDEINGDS,
				COLUMN_DFINCLUDEINGDS,
				QUALIFIED_COLUMN_DFINCLUDEINGDS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPERCENTINCLUDEDINGDS,
				COLUMN_DFPERCENTINCLUDEDINGDS,
				QUALIFIED_COLUMN_DFPERCENTINCLUDEDINGDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCLUDEINTDS,
				COLUMN_DFINCLUDEINTDS,
				QUALIFIED_COLUMN_DFINCLUDEINTDS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPERCENTINCLUDEDINTDS,
				COLUMN_DFPERCENTINCLUDEDINTDS,
				QUALIFIED_COLUMN_DFPERCENTINCLUDEDINTDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

