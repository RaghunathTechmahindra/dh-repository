package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 * WARNING - BJH Nov 18, 2002
 *
 * Scenario Number removed from as predicate was entirely unnecessary. Deal trees are uniqiely identified by deal Id
 * and copy Id, and hence a join on Borrowers based upon this uniqueness is entirely adequate - i.e. if scenarion
 * number was necessary this would suggest that borrower records are shared by multiple main deal records and this
 * is, of course, not the case.
 *
 * WARNING - BJH Nov 20, 2002
 *
 * Usage of model dropped completely - no necessary
 *
 */

public interface doUWCountNetWorthModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId();


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNetWorth();


	/**
	 *
	 *
	 */
	public void setDfNetWorth(java.math.BigDecimal value);



	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFNETWORTH="dfNetWorth";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

