package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.deal.util.BXStringTokenizer;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doCreditReviewModelImpl extends QueryModelBase
	implements doCreditReviewModel
{
	/**
	 *
	 *
	 */
	public doCreditReviewModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By John 25Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
	int institutionId = theSessionState.getDealInstitutionId();
    String tmpStr;
    while (this.next())
    {
      //  Transaction type
      this.setDfBorrowerType(BXResources.getPickListDescription(
          theSessionState.getDealInstitutionId(),
          "BORROWERTYPE", this.getDfBorrowerType(), languageId));
      tmpStr = this.getDfBorrowerName();
      tmpStr = BXStringTokenizer.replace(tmpStr, "BORROWER.SUFFIXID",
    	        BXResources.getPickListDescription(
    	          institutionId, "SUFFIX", this.getDfSuffixId().intValue(), languageId));
      this.setDfBorrowerName(tmpStr);
    }
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBorrowerName()
	{
		return (String)getValue(FIELD_DFBORROWERNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerName(String value)
	{
		setValue(FIELD_DFBORROWERNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBorrowerType()
	{
		return (String)getValue(FIELD_DFBORROWERTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerType(String value)
	{
		setValue(FIELD_DFBORROWERTYPE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfExistingClient()
	{
		return (String)getValue(FIELD_DFEXISTINGCLIENT);
	}


	/**
	 *
	 *
	 */
	public void setDfExistingClient(String value)
	{
		setValue(FIELD_DFEXISTINGCLIENT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfExistClientComments()
	{
		return (String)getValue(FIELD_DFEXISTCLIENTCOMMENTS);
	}


	/**
	 *
	 *
	 */
	public void setDfExistClientComments(String value)
	{
		setValue(FIELD_DFEXISTCLIENTCOMMENTS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfGTS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFGTS);
	}


	/**
	 *
	 *
	 */
	public void setDfGTS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFGTS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfGTS3Year()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFGTS3YEAR);
	}


	/**
	 *
	 *
	 */
	public void setDfGTS3Year(java.math.BigDecimal value)
	{
		setValue(FIELD_DFGTS3YEAR,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfTDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTDS3Year()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTDS3YEAR);
	}


	/**
	 *
	 *
	 */
	public void setDfTDS3Year(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTDS3YEAR,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNetWorth()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFNETWORTH);
	}


	/**
	 *
	 *
	 */
	public void setDfNetWorth(java.math.BigDecimal value)
	{
		setValue(FIELD_DFNETWORTH,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBureauNameId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBUREAUNAMEID);
	}


	/**
	 *
	 *
	 */
	public void setDfBureauNameId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBUREAUNAMEID,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfBureauOnfile()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFBUREAUONFILE);
	}


	/**
	 *
	 *
	 */
	public void setDfBureauOnfile(java.sql.Timestamp value)
	{
		setValue(FIELD_DFBUREAUONFILE,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfBureauProfile()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFBUREAUPROFILE);
	}


	/**
	 *
	 *
	 */
	public void setDfBureauProfile(java.sql.Timestamp value)
	{
		setValue(FIELD_DFBUREAUPROFILE,value);
	}

	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfAuthorization()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFAUTHORIZATION);
	}


	/**
	 *
	 *
	 */
	public void setDfAuthorization(java.sql.Timestamp value)
	{
		setValue(FIELD_DFAUTHORIZATION,value);
	}
	
	
	/**
	 *
	 *
	 */
	public String getDfAuthorizationMethod()
	{
		return (String)getValue(FIELD_DFAUTHORIZATIONMETHOD);
	}


	/**
	 *
	 *
	 */
	public void setDfAuthorizationMethod(String value)
	{
		setValue(FIELD_DFAUTHORIZATIONMETHOD,value);
	}
	
	/**
	 *
	 *
	 */
    public java.math.BigDecimal getDfSuffixId() 
    {
        return (java.math.BigDecimal) getValue(FIELD_DFSUFFIXID);
	}

	/**
	 *
	 *
	 */
	public void setDfSuffixId(java.math.BigDecimal value) 
	{
		setValue(FIELD_DFSUFFIXID, value);
	}
	  
	/**
	 * 
	 * 
	 */
	public String getDfBureauSummary()
	{
		return (String)getValue(FIELD_DFBUREAUSUMMARY);
	}


	/**
	 *
	 *
	 */
	public void setDfBureauSummary(String value)
	{
		setValue(FIELD_DFBUREAUSUMMARY,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCreditScore()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCREDITSCORE);
	}


	/**
	 *
	 *
	 */
	public void setDfCreditScore(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCREDITSCORE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTimesBankrupt()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTIMESBANKRUPT);
	}


	/**
	 *
	 *
	 */
	public void setDfTimesBankrupt(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTIMESBANKRUPT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBankruptStatusId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBANKRUPTSTATUSID);
	}


	/**
	 *
	 *
	 */
	public void setDfBankruptStatusId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBANKRUPTSTATUSID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getNSFOccurenceStatusId()
	{
		return (java.math.BigDecimal)getValue(FIELD_NSFOCCURENCESTATUSID);
	}


	/**
	 *
	 *
	 */
	public void setNSFOccurenceStatusId(java.math.BigDecimal value)
	{
		setValue(FIELD_NSFOCCURENCESTATUSID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPaymentHistoryTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPAYMENTHISTORYTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPaymentHistoryTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPAYMENTHISTORYTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerGeneralStatusId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERGENERALSTATUSID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerGeneralStatusId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERGENERALSTATUSID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPrimaryBorrowerFlag()
	{
		return (String)getValue(FIELD_DFPRIMARYBORROWERFLAG);
	}


	/**
	 *
	 *
	 */
	public void setDfPrimaryBorrowerFlag(String value)
	{
		setValue(FIELD_DFPRIMARYBORROWERFLAG,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCreditBureauSummary()
	{
		return (String)getValue(FIELD_DFCREDITBUREAUSUMMARY);
	}


	/**
	 *
	 *
	 */
	public void setDfCreditBureauSummary(String value)
	{
		setValue(FIELD_DFCREDITBUREAUSUMMARY,value);
	}

	/**
	 * MetaData object test method to verify the SYNTHETIC new
	 * field for the reogranized SQL_TEMPLATE query.
	 *
	 */
  /**
	public void setResultSet(ResultSet value)
	{
  		logger = SysLog.getSysLogger("METADATA");

  		try
  		{
   			super.setResultSet(value);
   			if( value != null)
   			{
       			ResultSetMetaData metaData = value.getMetaData();
       			int columnCount = metaData.getColumnCount();
       			logger.debug("===============================================");
              	        logger.debug("Testing MetaData Object for new synthetic fields for" +
                 	            " doCreditReviewModel");
       			logger.debug("NumberColumns: " + columnCount);
         		for(int i = 0; i < columnCount ; i++)
          		{
                 	logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
          		}
          		logger.debug("================================================");
      		}
  		}
  		catch (SQLException e)
  		{
    		    logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
  		}
 	}
  **/


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.
	////public static final String SELECT_SQL_TEMPLATE="SELECT ALL BORROWER.BORROWERID, BORROWER.COPYID, BORROWER.DEALID, BORROWER.BORROWERFIRSTNAME || ' ' || SUBSTR(BORROWER.BORROWERMIDDLEINITIAL,0,1) || '  ' || BORROWER.BORROWERLASTNAME, BORROWERTYPE.BTDESCRIPTION, BORROWER.EXISTINGCLIENT, BORROWER.EXISTINGCLIENTCOMMENTS, BORROWER.GDS, BORROWER.GDS3YEAR, BORROWER.TDS, BORROWER.TDS3YEAR, BORROWER.NETWORTH, BORROWER.CREDITBUREAUNAMEID, BORROWER.CREDITBUREAUONFILEDATE, BORROWER.DATEOFCREDITBUREAUPROFILE, BORROWER.CREDITSUMMARY, BORROWER.CREDITSCORE, BORROWER.NUMBEROFTIMESBANKRUPT, BORROWER.BANKRUPTCYSTATUSID, BORROWER.NSFOCCURENCETYPEID, BORROWER.PAYMENTHISTORYTYPEID, BORROWER.BORROWERGENERALSTATUSID, BORROWER.PRIMARYBORROWERFLAG, BORROWER.CREDITBUREAUSUMMARY FROM BORROWER, BORROWERTYPE  __WHERE__  ORDER BY BORROWER.BORROWERID  ASC";

  //--Release2.1--//
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL BORROWER.BORROWERID, BORROWER.COPYID, BORROWER.DEALID, BORROWER.BORROWERFIRSTNAME || ' ' || SUBSTR(BORROWER.BORROWERMIDDLEINITIAL,0,1) || '  ' || BORROWER.BORROWERLASTNAME SYNTHETIC_BORROWER, BORROWERTYPE.BTDESCRIPTION, BORROWER.EXISTINGCLIENT, BORROWER.EXISTINGCLIENTCOMMENTS, BORROWER.GDS, BORROWER.GDS3YEAR, BORROWER.TDS, BORROWER.TDS3YEAR, BORROWER.NETWORTH, BORROWER.CREDITBUREAUNAMEID, BORROWER.CREDITBUREAUONFILEDATE, BORROWER.DATEOFCREDITBUREAUPROFILE, BORROWER.CREDITSUMMARY, BORROWER.CREDITSCORE, BORROWER.NUMBEROFTIMESBANKRUPT, BORROWER.BANKRUPTCYSTATUSID, BORROWER.NSFOCCURENCETYPEID, BORROWER.PAYMENTHISTORYTYPEID, BORROWER.BORROWERGENERALSTATUSID, BORROWER.PRIMARYBORROWERFLAG, BORROWER.CREDITBUREAUSUMMARY FROM BORROWER, BORROWERTYPE  __WHERE__  ORDER BY BORROWER.BORROWERID  ASC";
  public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL BORROWER.BORROWERID, BORROWER.COPYID, BORROWER.DEALID, " +
    "BORROWER.BORROWERFIRSTNAME || ' ' || SUBSTR(BORROWER.BORROWERMIDDLEINITIAL,0,1) || " +
    "'  ' || BORROWER.BORROWERLASTNAME || ' ' || 'BORROWER.SUFFIXID' SYNTHETIC_BORROWER, " +
    "to_char(BORROWER.BORROWERTYPEID) BORROWERTYPEID_STR, BORROWER.EXISTINGCLIENT, " +
    "BORROWER.EXISTINGCLIENTCOMMENTS, BORROWER.GDS, BORROWER.GDS3YEAR, " +
    "BORROWER.TDS, BORROWER.TDS3YEAR, BORROWER.NETWORTH, " +
    "BORROWER.CREDITBUREAUNAMEID, BORROWER.CREDITBUREAUONFILEDATE, " +
    "BORROWER.DATEOFCREDITBUREAUPROFILE, BORROWER.CBAUTHORIZATIONDATE, BORROWER.CREDITSUMMARY, " +
    "BORROWER.CBAUTHORIZATIONMETHOD, BORROWER.SUFFIXID, " +
    "BORROWER.CREDITSCORE, BORROWER.NUMBEROFTIMESBANKRUPT, " +
    "BORROWER.BANKRUPTCYSTATUSID, BORROWER.NSFOCCURENCETYPEID, " +
    "BORROWER.PAYMENTHISTORYTYPEID, BORROWER.BORROWERGENERALSTATUSID, " +
    "BORROWER.PRIMARYBORROWERFLAG, BORROWER.CREDITBUREAUSUMMARY FROM " +
    "BORROWER  __WHERE__  ORDER BY BORROWER.BORROWERID  ASC";

	//public static final String MODIFYING_QUERY_TABLE_NAME="BORROWER, BORROWERTYPE";
  public static final String MODIFYING_QUERY_TABLE_NAME="BORROWER";

	//public static final String STATIC_WHERE_CRITERIA=" (BORROWER.BORROWERTYPEID  =  BORROWERTYPE.BORROWERTYPEID) ";
  public static final String STATIC_WHERE_CRITERIA="  ";
  //--------------//

	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="BORROWER.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="BORROWER.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFDEALID="BORROWER.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	////public static final String QUALIFIED_COLUMN_DFBORROWERNAME=".";
	////public static final String COLUMN_DFBORROWERNAME="";
	public static final String QUALIFIED_COLUMN_DFBORROWERNAME="BORROWER.SYNTHETIC_BORROWER";
	public static final String COLUMN_DFBORROWERNAME="SYNTHETIC_BORROWER";

  //--Release2.1--//
  //public static final String QUALIFIED_COLUMN_DFBORROWERTYPE="BORROWERTYPE.BTDESCRIPTION";
	//public static final String COLUMN_DFBORROWERTYPE="BTDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFBORROWERTYPE="BORROWER.BORROWERTYPEID_STR";
	public static final String COLUMN_DFBORROWERTYPE="BORROWERTYPEID_STR";
  //--------------//

	public static final String QUALIFIED_COLUMN_DFEXISTINGCLIENT="BORROWER.EXISTINGCLIENT";
	public static final String COLUMN_DFEXISTINGCLIENT="EXISTINGCLIENT";
	public static final String QUALIFIED_COLUMN_DFEXISTCLIENTCOMMENTS="BORROWER.EXISTINGCLIENTCOMMENTS";
	public static final String COLUMN_DFEXISTCLIENTCOMMENTS="EXISTINGCLIENTCOMMENTS";
	public static final String QUALIFIED_COLUMN_DFGTS="BORROWER.GDS";
	public static final String COLUMN_DFGTS="GDS";
	public static final String QUALIFIED_COLUMN_DFGTS3YEAR="BORROWER.GDS3YEAR";
	public static final String COLUMN_DFGTS3YEAR="GDS3YEAR";
	public static final String QUALIFIED_COLUMN_DFTDS="BORROWER.TDS";
	public static final String COLUMN_DFTDS="TDS";
	public static final String QUALIFIED_COLUMN_DFTDS3YEAR="BORROWER.TDS3YEAR";
	public static final String COLUMN_DFTDS3YEAR="TDS3YEAR";
	public static final String QUALIFIED_COLUMN_DFNETWORTH="BORROWER.NETWORTH";
	public static final String COLUMN_DFNETWORTH="NETWORTH";
	public static final String QUALIFIED_COLUMN_DFBUREAUNAMEID="BORROWER.CREDITBUREAUNAMEID";
	public static final String COLUMN_DFBUREAUNAMEID="CREDITBUREAUNAMEID";
	public static final String QUALIFIED_COLUMN_DFBUREAUONFILE="BORROWER.CREDITBUREAUONFILEDATE";
	public static final String COLUMN_DFBUREAUONFILE="CREDITBUREAUONFILEDATE";
	public static final String QUALIFIED_COLUMN_DFBUREAUPROFILE="BORROWER.DATEOFCREDITBUREAUPROFILE";
	public static final String COLUMN_DFBUREAUPROFILE="DATEOFCREDITBUREAUPROFILE";
	public static final String QUALIFIED_COLUMN_DFAUTHORIZATION="BORROWER.CBAUTHORIZATIONDATE";
	public static final String COLUMN_DFAUTHORIZATION="CBAUTHORIZATIONDATE";
	public static final String QUALIFIED_COLUMN_DFAUTHORIZATIONMETHOD="BORROWER.CBAUTHORIZATIONMETHOD";
	public static final String COLUMN_DFAUTHORIZATIONMETHOD="CBAUTHORIZATIONMETHOD";
    public static final String QUALIFIED_COLUMN_DFSUFFIXID="BORROWER.SUFFIXID";
    public static final String COLUMN_DFSUFFIXID="SUFFIXID";
	public static final String QUALIFIED_COLUMN_DFBUREAUSUMMARY="BORROWER.CREDITSUMMARY";
	public static final String COLUMN_DFBUREAUSUMMARY="CREDITSUMMARY";
	public static final String QUALIFIED_COLUMN_DFCREDITSCORE="BORROWER.CREDITSCORE";
	public static final String COLUMN_DFCREDITSCORE="CREDITSCORE";
	public static final String QUALIFIED_COLUMN_DFTIMESBANKRUPT="BORROWER.NUMBEROFTIMESBANKRUPT";
	public static final String COLUMN_DFTIMESBANKRUPT="NUMBEROFTIMESBANKRUPT";
	public static final String QUALIFIED_COLUMN_DFBANKRUPTSTATUSID="BORROWER.BANKRUPTCYSTATUSID";
	public static final String COLUMN_DFBANKRUPTSTATUSID="BANKRUPTCYSTATUSID";
	public static final String QUALIFIED_COLUMN_NSFOCCURENCESTATUSID="BORROWER.NSFOCCURENCETYPEID";
	public static final String COLUMN_NSFOCCURENCESTATUSID="NSFOCCURENCETYPEID";
	public static final String QUALIFIED_COLUMN_DFPAYMENTHISTORYTYPEID="BORROWER.PAYMENTHISTORYTYPEID";
	public static final String COLUMN_DFPAYMENTHISTORYTYPEID="PAYMENTHISTORYTYPEID";
	public static final String QUALIFIED_COLUMN_DFBORROWERGENERALSTATUSID="BORROWER.BORROWERGENERALSTATUSID";
	public static final String COLUMN_DFBORROWERGENERALSTATUSID="BORROWERGENERALSTATUSID";
	public static final String QUALIFIED_COLUMN_DFPRIMARYBORROWERFLAG="BORROWER.PRIMARYBORROWERFLAG";
	public static final String COLUMN_DFPRIMARYBORROWERFLAG="PRIMARYBORROWERFLAG";
	public static final String QUALIFIED_COLUMN_DFCREDITBUREAUSUMMARY="BORROWER.CREDITBUREAUSUMMARY";
	public static final String COLUMN_DFCREDITBUREAUSUMMARY="CREDITBUREAUSUMMARY";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		//// Improper converted fragment by iMT.
                ////FIELD_SCHEMA.addFieldDescriptor(
		////	new QueryFieldDescriptor(
		////		FIELD_DFBORROWERNAME,
		////		COLUMN_DFBORROWERNAME,
		////		QUALIFIED_COLUMN_DFBORROWERNAME,
		////		String.class,
		////		false,
		////		false,
		////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		////		"",
		////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		////		""));

		//// Adjusted FieldDescriptor to fix the ComputedColumn iMT bug.
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERNAME,
				COLUMN_DFBORROWERNAME,
				QUALIFIED_COLUMN_DFBORROWERNAME,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"BORROWER.BORROWERFIRSTNAME || ' ' || SUBSTR(BORROWER.BORROWERMIDDLEINITIAL,0,1) || '  ' || BORROWER.BORROWERLASTNAME",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"BORROWER.BORROWERFIRSTNAME || ' ' || SUBSTR(BORROWER.BORROWERMIDDLEINITIAL,0,1) || '  ' || BORROWER.BORROWERLASTNAME"));


		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERTYPE,
				COLUMN_DFBORROWERTYPE,
				QUALIFIED_COLUMN_DFBORROWERTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEXISTINGCLIENT,
				COLUMN_DFEXISTINGCLIENT,
				QUALIFIED_COLUMN_DFEXISTINGCLIENT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEXISTCLIENTCOMMENTS,
				COLUMN_DFEXISTCLIENTCOMMENTS,
				QUALIFIED_COLUMN_DFEXISTCLIENTCOMMENTS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFGTS,
				COLUMN_DFGTS,
				QUALIFIED_COLUMN_DFGTS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFGTS3YEAR,
				COLUMN_DFGTS3YEAR,
				QUALIFIED_COLUMN_DFGTS3YEAR,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTDS,
				COLUMN_DFTDS,
				QUALIFIED_COLUMN_DFTDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTDS3YEAR,
				COLUMN_DFTDS3YEAR,
				QUALIFIED_COLUMN_DFTDS3YEAR,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNETWORTH,
				COLUMN_DFNETWORTH,
				QUALIFIED_COLUMN_DFNETWORTH,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBUREAUNAMEID,
				COLUMN_DFBUREAUNAMEID,
				QUALIFIED_COLUMN_DFBUREAUNAMEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBUREAUONFILE,
				COLUMN_DFBUREAUONFILE,
				QUALIFIED_COLUMN_DFBUREAUONFILE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBUREAUPROFILE,
				COLUMN_DFBUREAUPROFILE,
				QUALIFIED_COLUMN_DFBUREAUPROFILE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAUTHORIZATION,
				COLUMN_DFAUTHORIZATION,
				QUALIFIED_COLUMN_DFAUTHORIZATION,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAUTHORIZATIONMETHOD,
				COLUMN_DFAUTHORIZATIONMETHOD,
				QUALIFIED_COLUMN_DFAUTHORIZATIONMETHOD,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSUFFIXID,
				COLUMN_DFSUFFIXID,
				QUALIFIED_COLUMN_DFSUFFIXID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBUREAUSUMMARY,
				COLUMN_DFBUREAUSUMMARY,
				QUALIFIED_COLUMN_DFBUREAUSUMMARY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCREDITSCORE,
				COLUMN_DFCREDITSCORE,
				QUALIFIED_COLUMN_DFCREDITSCORE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTIMESBANKRUPT,
				COLUMN_DFTIMESBANKRUPT,
				QUALIFIED_COLUMN_DFTIMESBANKRUPT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBANKRUPTSTATUSID,
				COLUMN_DFBANKRUPTSTATUSID,
				QUALIFIED_COLUMN_DFBANKRUPTSTATUSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_NSFOCCURENCESTATUSID,
				COLUMN_NSFOCCURENCESTATUSID,
				QUALIFIED_COLUMN_NSFOCCURENCESTATUSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPAYMENTHISTORYTYPEID,
				COLUMN_DFPAYMENTHISTORYTYPEID,
				QUALIFIED_COLUMN_DFPAYMENTHISTORYTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERGENERALSTATUSID,
				COLUMN_DFBORROWERGENERALSTATUSID,
				QUALIFIED_COLUMN_DFBORROWERGENERALSTATUSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIMARYBORROWERFLAG,
				COLUMN_DFPRIMARYBORROWERFLAG,
				QUALIFIED_COLUMN_DFPRIMARYBORROWERFLAG,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCREDITBUREAUSUMMARY,
				COLUMN_DFCREDITBUREAUSUMMARY,
				QUALIFIED_COLUMN_DFCREDITBUREAUSUMMARY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

