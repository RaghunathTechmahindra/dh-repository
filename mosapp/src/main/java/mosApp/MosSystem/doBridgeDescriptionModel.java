package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doBridgeDescriptionModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBridgePurposeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBridgePurposeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBPDescription();

	
	/**
	 * 
	 * 
	 */
	public void setDfBPDescription(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBRIDGEPURPOSEID="dfBridgePurposeId";
	public static final String FIELD_DFBPDESCRIPTION="dfBPDescription";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

