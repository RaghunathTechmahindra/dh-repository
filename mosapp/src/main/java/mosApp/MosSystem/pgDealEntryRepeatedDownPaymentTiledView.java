package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import mosApp.*;
import MosSystem.*;

import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

import com.basis100.log.*;
import com.basis100.resources.*;
import com.basis100.picklist.BXResources;
import MosSystem.*;

/**
 *
 *
 *
 */
public class pgDealEntryRepeatedDownPaymentTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealEntryRepeatedDownPaymentTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doDealEntryDownPaymentModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getHdDownPaymentSourceId().setValue(CHILD_HDDOWNPAYMENTSOURCEID_RESET_VALUE);
		getHdDownPaymentCopyId().setValue(CHILD_HDDOWNPAYMENTCOPYID_RESET_VALUE);
		getCbDownPaymentSource().setValue(CHILD_CBDOWNPAYMENTSOURCE_RESET_VALUE);
		getTxDownPayment().setValue(CHILD_TXDOWNPAYMENT_RESET_VALUE);
		getBtDeleteDownPayment().setValue(CHILD_BTDELETEDOWNPAYMENT_RESET_VALUE);
		getTxDPSDescription().setValue(CHILD_TXDPSDESCRIPTION_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_HDDOWNPAYMENTSOURCEID,HiddenField.class);
		registerChild(CHILD_HDDOWNPAYMENTCOPYID,HiddenField.class);
		registerChild(CHILD_CBDOWNPAYMENTSOURCE,ComboBox.class);
		registerChild(CHILD_TXDOWNPAYMENT,TextField.class);
		registerChild(CHILD_BTDELETEDOWNPAYMENT,Button.class);
		registerChild(CHILD_TXDPSDESCRIPTION,TextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDealEntryDownPaymentModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--start//
    ////Populate Downpayment ComboBoxes manually here
    DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

    //Setup Downpayment ComboBox options
    cbDownPaymentSourceOptions.populate(getRequestContext());

    handler.pageSaveState();
    //--Release2.1--end//

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
      validJSFromTiledView();
      // Put migrated repeated_onBeforeRowDisplayEvent code here
		}

		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_HDDOWNPAYMENTSOURCEID))
		{
			HiddenField child = new HiddenField(this,
				getdoDealEntryDownPaymentModel(),
				CHILD_HDDOWNPAYMENTSOURCEID,
				doDealEntryDownPaymentModel.FIELD_DFDOWNPAYMENTSOURCEID,
				CHILD_HDDOWNPAYMENTSOURCEID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDDOWNPAYMENTCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoDealEntryDownPaymentModel(),
				CHILD_HDDOWNPAYMENTCOPYID,
				doDealEntryDownPaymentModel.FIELD_DFCOPYID,
				CHILD_HDDOWNPAYMENTCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBDOWNPAYMENTSOURCE))
		{
			ComboBox child = new ComboBox( this,
				getdoDealEntryDownPaymentModel(),
				CHILD_CBDOWNPAYMENTSOURCE,
				doDealEntryDownPaymentModel.FIELD_DFDOWNPAYMENTSOURCETYPEID,
				CHILD_CBDOWNPAYMENTSOURCE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbDownPaymentSourceOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXDOWNPAYMENT))
		{
			TextField child = new TextField(this,
				getdoDealEntryDownPaymentModel(),
				CHILD_TXDOWNPAYMENT,
				doDealEntryDownPaymentModel.FIELD_DFDOWNPAYMENTAMOUNT,
				CHILD_TXDOWNPAYMENT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETEDOWNPAYMENT))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETEDOWNPAYMENT,
				CHILD_BTDELETEDOWNPAYMENT,
				CHILD_BTDELETEDOWNPAYMENT_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_TXDPSDESCRIPTION))
		{
			TextField child = new TextField(this,
				getdoDealEntryDownPaymentModel(),
				CHILD_TXDPSDESCRIPTION,
				doDealEntryDownPaymentModel.FIELD_DFDPSDESCRIPTION,
				CHILD_TXDPSDESCRIPTION_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdDownPaymentSourceId()
	{
		return (HiddenField)getChild(CHILD_HDDOWNPAYMENTSOURCEID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdDownPaymentCopyId()
	{
		return (HiddenField)getChild(CHILD_HDDOWNPAYMENTCOPYID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbDownPaymentSource()
	{
		return (ComboBox)getChild(CHILD_CBDOWNPAYMENTSOURCE);
	}

	/**
	 *
	 *
	 */
  //--Release2.1--//
	static class CbDownPaymentSourceOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbDownPaymentSourceOptionList()
		{

		}

   //// IMPORTANT!!! //--Release2.1--//
   /**
	 *  Since the population of comboboxes on the pages should be done from BXResource
   *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
   *  element or it could be done directly from the BXResource. Each approach
   *  has its pros and cons. The most important is: for English and French versions
   *  could be different default values. It forces to use the second approach.
   *
   *  It this case to escape annoying code clone and follow the object oriented
   *  design the following abstact base class should encapsulate the combobox
   *  OptionList population. Each ComboBoxOptionList inner class should extend
   *  this base class and implement the BXResources table name.
   *
   *  Based on discussion with Billy the latest approach is picked up.
	 *
	 */

   //--Release2.1--//
   //// Populate the comboBox from the ResourceBundle.
		public void populate(RequestContext rc)
		{
      try
      {
        ////Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),"DOWNPAYMENTSOURCETYPE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }

	}



	/**
	 *
	 *
	 */
	public TextField getTxDownPayment()
	{
		return (TextField)getChild(CHILD_TXDOWNPAYMENT);
	}


	/**
	 *
	 *
	 */
	public void handleBtDeleteDownPaymentRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

    //logger = SysLog.getSysLogger("PGDERDPTV");

    DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

    //logger.debug("handleBtDeleteDownPaymentRequest::Before PreHandler");
    //logger.debug("handleBtDeleteDownPaymentRequest::Parent: " + (pgDealEntryViewBean) this.getParent());

    handler.preHandlerProtocol((ViewBean) this.getParent());

    int tileIndx = ((TiledViewRequestInvocationEvent)(event)).getTileNumber();
    //logger.debug("pgDealEntry@handleBtDeleteDownPaymentRequest::TileIndexNew: " + tileIndx);

    handler.handleDelete(false,
                          3,
                          ////handler.getRowNdxFromWebEventMethod(event),
                          tileIndx,
                          "RepeatedDownPayment/hdDownPaymentSourceId",
                          "downPaymentSourceId",
                          "RepeatedDownPayment/hdDownPaymentCopyId",
                          "DownPaymentSource");

    handler.postHandlerProtocol();
	}

	/**
	 *
	 *
	 */
	public Button getBtDeleteDownPayment()
	{
		return (Button)getChild(CHILD_BTDELETEDOWNPAYMENT);
	}


	/**
	 *
	 *
	 */
	public String endBtDeleteDownPaymentDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btDeleteDownPayment_onBeforeHtmlOutputEvent method
		DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public TextField getTxDPSDescription()
	{
		return (TextField)getChild(CHILD_TXDPSDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public doDealEntryDownPaymentModel getdoDealEntryDownPaymentModel()
	{
		if (doDealEntryDownPayment == null)
			doDealEntryDownPayment = (doDealEntryDownPaymentModel) getModel(doDealEntryDownPaymentModel.class);
		return doDealEntryDownPayment;
	}


	/**
	 *
	 *
	 */
	public void setdoDealEntryDownPaymentModel(doDealEntryDownPaymentModel model)
	{
			doDealEntryDownPayment = model;
	}

  public void validJSFromTiledView()
  {
      logger = SysLog.getSysLogger("PGDERDPTV");

      //// Currently it is called from the page. Should be moved to the
      //// TiledView utility classes to generalize the parent (ViewBean name/casting).
      //// ValidationPath is temporarily hardcoded as well. Should be replaced with
      //// the picklist mechanism along with the move to the utility class.

      pgDealEntryViewBean viewBean = (pgDealEntryViewBean) getParentViewBean();

      ////logger.debug("PGDERDPTV@validJSFromTiledView@ParentBean: " + viewBean);

      try
      {


        String downValidPath = Sc.DVALID_DEAL_TILE_DOWNPAY_PROP_FILE_NAME;

        ////logger.debug("PGDERDPTV@RatesPath: " + downValidPath);

        JSValidationRules downValidObj = new JSValidationRules(downValidPath, logger, viewBean);
        downValidObj.attachJSValidationRules();
      }
      catch( Exception e )
      {
          logger.warning("DataValidJSException: " + e);
          e.printStackTrace();
      }
  }



	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_HDDOWNPAYMENTSOURCEID="hdDownPaymentSourceId";
	public static final String CHILD_HDDOWNPAYMENTSOURCEID_RESET_VALUE="";
	public static final String CHILD_HDDOWNPAYMENTCOPYID="hdDownPaymentCopyId";
	public static final String CHILD_HDDOWNPAYMENTCOPYID_RESET_VALUE="";
	public static final String CHILD_CBDOWNPAYMENTSOURCE="cbDownPaymentSource";
	public static final String CHILD_CBDOWNPAYMENTSOURCE_RESET_VALUE="";

  //--Release2.1--//
	////private static CbDownPaymentSourceOptionList cbDownPaymentSourceOptions=new CbDownPaymentSourceOptionList();
	private CbDownPaymentSourceOptionList cbDownPaymentSourceOptions=new CbDownPaymentSourceOptionList();

	public static final String CHILD_TXDOWNPAYMENT="txDownPayment";
	public static final String CHILD_TXDOWNPAYMENT_RESET_VALUE="";
	public static final String CHILD_BTDELETEDOWNPAYMENT="btDeleteDownPayment";
	public static final String CHILD_BTDELETEDOWNPAYMENT_RESET_VALUE=" ";
	public static final String CHILD_TXDPSDESCRIPTION="txDPSDescription";
	public static final String CHILD_TXDPSDESCRIPTION_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDealEntryDownPaymentModel doDealEntryDownPayment=null;
  private DealEntryHandler handler=new DealEntryHandler();
  public SysLogger logger;
}

