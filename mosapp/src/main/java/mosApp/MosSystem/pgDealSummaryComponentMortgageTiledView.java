package mosApp.MosSystem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.html.StaticTextField;


/**
 * Title: pgDealSummaryComponentMortgageTiledView
 * <p>
 * Description:This view bean relates the pgDealSummary.jsp.
 * @author MCM Impl Team.
 * @version 1.0 10-June-2008 XS_16.7 Initial version
 * @version 1.1 23-June-2008 XS_16.13 Added new fields for the Mortgage Component Section.
 * @version 1.2 25-June-2008 XS_16.13 Review comment:Updated the resetChildren() method.
 * @version 1.3 25-June-2008 XS_16.13 Updadted nextTile() method:-Added code to display ActualPaymentTerm,AmortizationPeriod.
 * @version 1.4 17-July-2008 XS_2.8 Added nextTile(boolean) to avoid handler and fileds confliction
 * @version 1.5 05-Aug-2008 XS_16.21 nextTile() - Added code for internationalization support of generic messages.
 * @version 1.6 14-Oct-2008 Bugfix FXP22958 Mapping Changed for MI Premium Amount
 */
public class pgDealSummaryComponentMortgageTiledView
    extends RequestHandlingTiledViewBase implements TiledView, RequestHandler {

    public static Map FIELD_DESCRIPTORS;
    public static final String CHILD_STCOMPONENTTYPE = "stComponentType";
    public static final String CHILD_STCOMPONENTTYPE_RESET_VALUE = "";
    public static final String CHILD_STPRODUCTNAME = "stProductName";
    public static final String CHILD_STPRODUCTNAME_RESET_VALUE = "";
    public static final String CHILD_STAMOUNT = "stAmount";
    public static final String CHILD_STAMOUNT_RESET_VALUE = "";
    public static final String CHILD_STINCLUDEMIPREMIUM = "stAllocateMIPremium";
    public static final String CHILD_STINCLUDEMIPREMIUM_RESET_VALUE = "";
    public static final String CHILD_STPAYMENTAMOUNT = "stPaymentAmount";
    public static final String CHILD_STPAYMENTAMOUNT_RESET_VALUE = "";
    public static final String CHILD_STALLOWTAXESCROW = "stAllowTaxEscrow";
    public static final String CHILD_STALLOWTAXESCROW_RESET_VALUE = "";
    public static final String CHILD_STNETRATE = "stNetRate";
    public static final String CHILD_STNETRATE_RESET_VALUE = "";
    public static final String CHILD_STDISCOUNT = "stDiscount";
    public static final String CHILD_STDISCOUNT_RESET_VALUE = "";
    public static final String CHILD_STPREMIUM = "stPremium";
    public static final String CHILD_STPREMIUM_RESET_VALUE = "";
    public static final String CHILD_STBUYDOWNRATE = "stBuyDownRate";
    public static final String CHILD_STBUYDOWNRATE_RESET_VALUE = "";

    /*******************************************MCM Impl Team XS_16.13 changes starts******************************/
    public static final String CHILD_STCASHBACKAMOUNT = "stCashbackAmount";
    public static final String CHILD_STCASHBACKAMOUNT_RESET_VALUE = "";
    public static final String CHILD_STHOLDBACKAMOUNT = "stHolbackAmount";
    public static final String CHILD_STHOLDBACKAMOUNT_RESET_VALUE = "";
    public static final String CHILD_STPAYMENTTERMDESRIPTION = "stPaymentTermDescription";
    public static final String CHILD_STPAYMENTTERMDESRIPTION_RESET_VALUE = "";
    
    public static final String CHILD_STMORTGAGEAMOUNT = "stMortgageAmount";
    public static final String CHILD_STMORTGAGEAMOUNT_RESET_VALUE = "";
    public static final String CHILD_STACTUALPAYMENTTERM = "stActualPaymentTerm";
    public static final String CHILD_STACTUALPAYMENTTERM_RESET_VALUE = "";
    public static final String CHILD_STACTUALPAYMENTTERMYEARS = "stActualPaymentTermYrs";
    public static final String CHILD_STACTUALPAYMENTTERMYEARS_RESET_VALUE = "";
    public static final String CHILD_STACTUALPAYMENTTERMMONTHS = "stActualPaymentTermMths";
    public static final String CHILD_STACTUALPAYMENTTERMMONTHS_RESET_VALUE = "";
    
   
    public static final String CHILD_STREPAYMENTTYPE = "stRepaymentType";
    public static final String CHILD_STREPAYMENTTYPE_RESET_VALUE = "";
    public static final String CHILD_STRATEGUARANTEEPERIOD = "stRateGuaranteePeriod";
    public static final String CHILD_STRATEGUARANTEEPERIOD_RESET_VALUE = "";
    
    public static final String CHILD_STMIPREMIUM = "stMiPremium";
    public static final String CHILD_STMIPREMIUM_RESET_VALUE = "";
    public static final String CHILD_STPAYMENTFREQUENCY = "stPaymentFrequency";
    public static final String CHILD_STPAYMENTFREQUENCY_RESET_VALUE = "";
    public static final String CHILD_STRATELOCKEDIN = "stRateLockedIn";
    public static final String CHILD_STRATELOCKEDIN_RESET_VALUE = "";
    public static final String CHILD_STPANDIAPAYMENT = "stPAndIPayment";
    public static final String CHILD_STPANDIAPAYMENT_RESET_VALUE = "";
    public static final String CHILD_STEXISTINGACCOUNT = "stExistingAccount";
    public static final String CHILD_STEXISTINGACCOUNT_RESET_VALUE = "";
    

    public static final String CHILD_STPREPAYMENTOPTION = "stPrePaymentOption";
    public static final String CHILD_STPREPAYMENTOPTION_RESET_VALUE = "";
    public static final String CHILD_STADDITIONALPRINCIPALPAYMENT = "stAdditionalPrincipalPayment";
    public static final String CHILD_STADDITIONALPRINCIPALPAYMENT_RESET_VALUE = "";
    public static final String CHILD_STEXISTINGACCOUNTREFERENCE = "stExistingAccountReference";
    public static final String CHILD_STEXISTINGACCOUNTREFERENCE_RESET_VALUE = "";
   
    public static final String CHILD_STPRIVILEGEPAYMENTOPTION = "stPrivilegePaymentOption";
    public static final String CHILD_STPRIVILEGEPAYMENTOPTION_RESET_VALUE = "";
    public static final String CHILD_STPOSTEDRATE = "stPostedRate";
    public static final String CHILD_STPOSTEDRATE_RESET_VALUE = "";
    public static final String CHILD_STFIRSTPAYMENTDATE = "stFirstPaymentDate";
    public static final String CHILD_STFIRSTPAYMENTDATE_RESET_VALUE = "";
    
    public static final String CHILD_STAMORTIZATIONPERIO = "stAmortizationPeriod";
    public static final String CHILD_STAMORTIZATIONPERIOD_RESET_VALUE = "";
    public static final String CHILD_STAMORTIZATIONPERIODYEARS = "stAmortizationYrs";
    public static final String CHILD_STAMORTIZATIONPERIODYEARS_RESET_VALUE = "";
    public static final String CHILD_STAMORTIZATIONPERIODMONTHS = "stAmortizationMths";
    public static final String CHILD_STAMORTIZATIONPERIODMONTHS_RESET_VALUE = "";
   
    public static final String CHILD_STCOMMISSIONCODE = "stCommissionCode";
    public static final String CHILD_STCOMMISSIONCODE_RESET_VALUE = "";
    public static final String CHILD_STTAXESCROW = "stTaxEscrow";
    public static final String CHILD_STTAXESCROW_RESET_VALUE = "";
    public static final String CHILD_STMATURITYDATE = "stMaturityDate";
    public static final String CHILD_STMATURITYDATE_RESET_VALUE = "";
    
    public static final String CHILD_STEFFECTIVEAMORTIZATION = "stEffectiveAmortization";
    public static final String CHILD_STEFFECTIVEAMORTIZATION_RESET_VALUE = "";
    public static final String CHILD_STEFFECTIVEAMORTIZATIONYEARS = "stEffectiveAmortizationYrs";
    public static final String CHILD_STEFFECTIVEAMORTIZATIONYEARS_RESET_VALUE = "";
    public static final String CHILD_STEFFECTIVEAMORTIZATIONMONTHS = "stEffectiveAmortizationMths";
    public static final String CHILD_STEFFECTIVEAMORTIZATIONMONTHS_RESET_VALUE = "";
    
   
    public static final String CHILD_STCASHBACKPERCENTAGE = "stCashbackPercetage";
    public static final String CHILD_STCASHBACKPERCENTAGE_RESET_VALUE = "";
    public static final String CHILD_STTOTALPAYMENT = "stTotalPayment";
    public static final String CHILD_STTOTALPAYMENT_RESET_VALUE = "";
    public static final String CHILD_STADDITIONALDETAILS = "stAdditionalDetails";
    public static final String CHILD_STADDITIONALDETAILS_RESET_VALUE = "";
    /**********************************************MCM Impl Team XS_16.13 changes starts***********************************/

    /**
     * Models used by this class to display information.
     */
    private doComponentMortgageModel doComponentMortgageModel = null;

    /**
     * Handler class used by this class to display information.
     */
    private DealSummaryHandler handler = new DealSummaryHandler();

    /**
     * This Constructor registers the children and sets the default URL.
     * @param View
     *            object in which this tile is a part of
     * @param name
     *            of this tiled view
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    public pgDealSummaryComponentMortgageTiledView (View parent, String name) {
        super(parent, name);
        setMaxDisplayTiles(100);
        setPrimaryModelClass(doComponentMortgageModel.class);
        registerChildren();
        initialize();

    }

    /**
     * Initialize.
     */
    protected void initialize () {

    }

    /**
     * Description: This method resets the children which are fields on the
     * corresponding JSP
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     * @version 1.1 17-June-2008 XS_16.13
     *              Added necessary changes required for the new tiled view for mortgage infomation section.
     */
    public void resetChildren () {

        getStComponentType().setValue(CHILD_STCOMPONENTTYPE_RESET_VALUE);
        getStProductName().setValue(CHILD_STPRODUCTNAME_RESET_VALUE);
        getStAmount().setValue(CHILD_STAMOUNT_RESET_VALUE);
        getStIncludeMIPremium().setValue(CHILD_STINCLUDEMIPREMIUM_RESET_VALUE);
        getStPaymentAmount().setValue(CHILD_STPAYMENTAMOUNT_RESET_VALUE);
        getStAllowTaxEscrow().setValue(CHILD_STALLOWTAXESCROW_RESET_VALUE);
        getStNetRate().setValue(CHILD_STNETRATE_RESET_VALUE);
        getStDiscount().setValue(CHILD_STDISCOUNT_RESET_VALUE);
        getStPremium().setValue(CHILD_STPREMIUM_RESET_VALUE);
        getStBuyDownRate().setValue(CHILD_STBUYDOWNRATE_RESET_VALUE);
        /***************************************MCM Impl Team XS_16.13 changes Starts ***********************/
        getStCashbackAmount().setValue(CHILD_STCASHBACKAMOUNT_RESET_VALUE);
        getStHoldbackAmount().setValue(CHILD_STHOLDBACKAMOUNT_RESET_VALUE);
        getStMortgageAmount().setValue(CHILD_STMORTGAGEAMOUNT_RESET_VALUE);
        getStActualPaymentTerm().setValue(CHILD_STACTUALPAYMENTTERM_RESET_VALUE);
        getStActualPaymentTermYears().setValue(CHILD_STACTUALPAYMENTTERMYEARS_RESET_VALUE);
        getStActualPaymentTermMonths().setValue(CHILD_STACTUALPAYMENTTERMMONTHS_RESET_VALUE);
        getStRepaymentType().setValue(CHILD_STREPAYMENTTYPE_RESET_VALUE);
        getStRateGuaranteePeriod().setValue(CHILD_STRATEGUARANTEEPERIOD_RESET_VALUE);
        getStPremium().setValue(CHILD_STMIPREMIUM_RESET_VALUE);
        getStPaymentFrequency().setValue(CHILD_STPAYMENTFREQUENCY_RESET_VALUE);
        getStRateLockedIn().setValue(CHILD_STRATELOCKEDIN_RESET_VALUE);
        getStPAndIPayment().setValue(CHILD_STPANDIAPAYMENT_RESET_VALUE);
        getStExistingAccount().setValue(CHILD_STEXISTINGACCOUNT_RESET_VALUE);
        getStPrePaymentOption().setValue(CHILD_STPREPAYMENTOPTION_RESET_VALUE);
        getStAdditionalPrincipalPayment().setValue(CHILD_STADDITIONALPRINCIPALPAYMENT);
        getStExistingAccountReference().setValue(CHILD_STEXISTINGACCOUNTREFERENCE_RESET_VALUE);
        getStPrivilegePaymentOption().setValue(CHILD_STPRIVILEGEPAYMENTOPTION_RESET_VALUE);
        getStPostedRate().setValue(CHILD_STPOSTEDRATE_RESET_VALUE);
        getStFirstPaymentDate().setValue(CHILD_STFIRSTPAYMENTDATE_RESET_VALUE);
        getStAmortizationPeriodMonths().setValue(CHILD_STAMORTIZATIONPERIODMONTHS_RESET_VALUE);
        getStAmortizationPeriodYears().setValue(CHILD_STAMORTIZATIONPERIODYEARS_RESET_VALUE);
        getStCommissionCode().setValue(CHILD_STCOMMISSIONCODE_RESET_VALUE);
        getStTaxEscrow().setValue(CHILD_STTAXESCROW_RESET_VALUE);
        getStMaturityDate().setValue(CHILD_STMATURITYDATE_RESET_VALUE);
        getStEffectiveAmortization().setValue(CHILD_STEFFECTIVEAMORTIZATION_RESET_VALUE);
        getStEffectiveAmortizationYears().setValue(CHILD_STEFFECTIVEAMORTIZATIONYEARS_RESET_VALUE);
        getStEffectiveAmortizationMonths().setValue(CHILD_STEFFECTIVEAMORTIZATIONMONTHS_RESET_VALUE);
        getStCashbackPercentage().setValue(CHILD_STCASHBACKPERCENTAGE_RESET_VALUE);
        getStTotalPayment().setValue(CHILD_STTOTALPAYMENT_RESET_VALUE);
        getStAdditionalDetails().setValue(CHILD_STADDITIONALDETAILS_RESET_VALUE);
        getStAdditionalDetails().setValue(CHILD_STPAYMENTTERMDESRIPTION_RESET_VALUE);
        /*********************************************MCM Impl Team XS_16.13 changes ends*******************************************************/
    }

    /**
     * Description: This method registers the children which are fields on the
     * corresponding JSP.
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     * @version 1.1 17-June-2008 XS_16.13
     *              Added necessary changes required for the new tiled view for mortgage infomation section.
     */
    protected void registerChildren () {

        registerChild(CHILD_STCOMPONENTTYPE, StaticTextField.class);
        registerChild(CHILD_STPRODUCTNAME, StaticTextField.class);
        registerChild(CHILD_STAMOUNT, StaticTextField.class);
        registerChild(CHILD_STINCLUDEMIPREMIUM, StaticTextField.class);
        registerChild(CHILD_STPAYMENTAMOUNT, StaticTextField.class);
        registerChild(CHILD_STALLOWTAXESCROW, StaticTextField.class);
        registerChild(CHILD_STNETRATE, StaticTextField.class);
        registerChild(CHILD_STDISCOUNT, StaticTextField.class);
        registerChild(CHILD_STPREMIUM, StaticTextField.class);
        registerChild(CHILD_STBUYDOWNRATE, StaticTextField.class);
        /***************************************MCM Impl Team XS_16.13 changes Starts ***********************/
        registerChild(CHILD_STHOLDBACKAMOUNT, StaticTextField.class);
        registerChild(CHILD_STCASHBACKAMOUNT, StaticTextField.class);
        registerChild(CHILD_STHOLDBACKAMOUNT, StaticTextField.class);
        registerChild(CHILD_STMORTGAGEAMOUNT, StaticTextField.class);
        registerChild(CHILD_STACTUALPAYMENTTERM, StaticTextField.class);
        registerChild(CHILD_STACTUALPAYMENTTERMYEARS, StaticTextField.class);
        registerChild(CHILD_STACTUALPAYMENTTERMMONTHS, StaticTextField.class);
        registerChild(CHILD_STREPAYMENTTYPE, StaticTextField.class);
        registerChild(CHILD_STRATEGUARANTEEPERIOD, StaticTextField.class);
        registerChild(CHILD_STMIPREMIUM, StaticTextField.class);
        registerChild(CHILD_STPAYMENTFREQUENCY, StaticTextField.class);
        registerChild(CHILD_STRATELOCKEDIN, StaticTextField.class);
        registerChild(CHILD_STPANDIAPAYMENT, StaticTextField.class);
        registerChild(CHILD_STEXISTINGACCOUNT, StaticTextField.class);
    
        registerChild(CHILD_STPREPAYMENTOPTION, StaticTextField.class);
        registerChild(CHILD_STADDITIONALPRINCIPALPAYMENT, StaticTextField.class);
        registerChild(CHILD_STEXISTINGACCOUNTREFERENCE, StaticTextField.class);
        registerChild(CHILD_STPRIVILEGEPAYMENTOPTION, StaticTextField.class);
        registerChild(CHILD_STPOSTEDRATE, StaticTextField.class);
        registerChild(CHILD_STFIRSTPAYMENTDATE, StaticTextField.class);
        registerChild(CHILD_STAMORTIZATIONPERIODYEARS, StaticTextField.class);
        registerChild(CHILD_STAMORTIZATIONPERIODMONTHS, StaticTextField.class);
        registerChild(CHILD_STCOMMISSIONCODE, StaticTextField.class);
        registerChild(CHILD_STTAXESCROW, StaticTextField.class);
        registerChild(CHILD_STMATURITYDATE, StaticTextField.class);
        registerChild(CHILD_STEFFECTIVEAMORTIZATION, StaticTextField.class);
        registerChild(CHILD_STEFFECTIVEAMORTIZATIONYEARS, StaticTextField.class);
        registerChild(CHILD_STEFFECTIVEAMORTIZATIONMONTHS, StaticTextField.class);
        registerChild(CHILD_STCASHBACKPERCENTAGE, StaticTextField.class);
        registerChild(CHILD_STTOTALPAYMENT, StaticTextField.class);
        registerChild(CHILD_STADDITIONALDETAILS, StaticTextField.class);
        registerChild(CHILD_STPAYMENTTERMDESRIPTION, StaticTextField.class);
        /*********************************************MCM Impl Team XS_16.13 changes ends*******************************************************/
    }

    /**
     * Description: Adds all models to the model list
     * <p>.
     * @param executionType
     *            the execution type
     * @return model[]- List of models used by this viewbean
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    public Model [] getWebActionModels (int executionType) {

        List modelList = new ArrayList();

        switch (executionType) {

        case MODEL_TYPE_RETRIEVE:
            modelList.add(getDoComponentMortgageModel());
            ;

            break;

        case MODEL_TYPE_UPDATE:
            ;

            break;

        case MODEL_TYPE_DELETE:
            ;

            break;

        case MODEL_TYPE_INSERT:
            ;

            break;

        case MODEL_TYPE_EXECUTE:
            ;

            break;

        }

        return (Model []) modelList.toArray(new Model[0]);

    }

    /**
     * Description:This method creates the handler objects, saves the page state
     * and also populates the combo box fields. Also resets the tile index.
     * <p>
     * @param event -
     *            DisplayEvent which triggered this call.
     * @return void
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    public void beginDisplay (DisplayEvent event) throws ModelControlException {

        if (getPrimaryModel() == null) {

            throw new ModelControlException("Primary model is null");

        }

        super.beginDisplay(event);
        resetTileIndex();

    }

    /**
     * Description: This method moves the control from current tile to next
     * tile.
     * @return boolean - true, if the tile has moved. False otherwise.
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     * @version 1.1 23-June-2008 XS_16.13 Update to dislay the the flag value.
     * @version 1.1 25-June-2008 XS_16.13 Added code to display ActualPaymentTerm,AmortizationPeriod.
     * @version 1.2 05-Aug-2008 XS_16.21 Added code for internationalization support of generic messages.
     */
    public boolean nextTile () throws ModelControlException {

        boolean movedToRow = super.nextTile();
        doComponentMortgageModelImpl theObj =(doComponentMortgageModelImpl) this.getDoComponentMortgageModel();
        
        //***************MCM Impl team changes starts - XS_16.21************************/
        String defaultInstanceStateName = getRequestContext().getModelManager()
        .getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl) getRequestContext()
        .getModelManager().getModel(SessionStateModel.class,
                defaultInstanceStateName, true);
        int languageId = theSessionState.getLanguageId();
        //***************MCM Impl team changes ends - XS_16.21************************/
        if (movedToRow) {
            //set Allocate property Tax String
            //***************MCM Impl team changes starts - XS_16.21 - Added BXResources for generic message for localization support*/
           if("Y".equals(theObj.getDfPropertyTaxFlag()))
               setDisplayFieldValue(CHILD_STALLOWTAXESCROW,  BXResources.getGenericMsg("YES_LABEL",languageId));
           else{
               setDisplayFieldValue(CHILD_STALLOWTAXESCROW,  BXResources.getGenericMsg("NO_LABEL",languageId));
               setDisplayFieldValue(CHILD_STTAXESCROW,  new String("00.0"));
           }
           if("Y".equals(theObj.getDfMiAllocateFlag()))
               setDisplayFieldValue(CHILD_STINCLUDEMIPREMIUM,  BXResources.getGenericMsg("YES_LABEL",languageId));
           else
               setDisplayFieldValue(CHILD_STINCLUDEMIPREMIUM,BXResources.getGenericMsg("NO_LABEL",languageId));
           //Display rate locked flag
           if("Y".equals(theObj.getDfRateLockedIn()))
               setDisplayFieldValue(CHILD_STRATELOCKEDIN,  BXResources.getGenericMsg("YES_LABEL",languageId));
           else
               setDisplayFieldValue(CHILD_STRATELOCKEDIN,BXResources.getGenericMsg("NO_LABEL",languageId));
           //Display Exisiting account flag
           if("Y".equals(theObj.getDfExsitingAccountIndicator()))
               setDisplayFieldValue(CHILD_STEXISTINGACCOUNT,  BXResources.getGenericMsg("YES_LABEL",languageId));
           else
               setDisplayFieldValue(CHILD_STEXISTINGACCOUNT,BXResources.getGenericMsg("NO_LABEL",languageId));
           //***************MCM Impl team changes ends - XS_16.21***************************/
           //Display term field 
           DealSummaryHandler handler = (DealSummaryHandler)this.handler.cloneSS();
           handler.pageGetState(this.getParentViewBean());
           //call the handler method to set the display fields.
           handler.setMrtgCompDisplayFields(getTileIndex());
           handler.pageSaveState();

        }

        return movedToRow;

    }

    /**
     * <p>nextTile</p>
     * <p> to be calles from child to skip this tiled nextTile to avoid confliction
     * @param skipThisTile
     * @return
     * @throws ModelControlException
     * 
     * @version 1.0 July 17, 2008 MCM Team for XS 2.8
     */
    protected boolean nextTile (boolean skipThisTile) throws ModelControlException {
        
        if (skipThisTile) {
            return super.nextTile();
        } else return nextTile();
    }

    /**
     * Description: Creates the children represented by fields on the JSP.
     * @param name -
     *            String name of the child
     * @return View - JATO field representing the child
     * @exception IllegalArgumentException
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     * @version 1.1 17-June-2008 XS_16.13
     *              Added necessary changes required for the new tiled view for mortgage infomation section.
     */
    protected View createChild (String name) {

        if (name.equals(CHILD_STCOMPONENTTYPE)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STCOMPONENTTYPE,
                                                        doComponentMortgageModel.FIELD_DFCOMPONENTTYPE,
                                                        CHILD_STCOMPONENTTYPE_RESET_VALUE,
                                                        null);
            return child;

        } else if (name.equals(CHILD_STPRODUCTNAME)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STPRODUCTNAME,
                                                        doComponentMortgageModel.FIELD_DFPRODUCTNAME,
                                                        CHILD_STPRODUCTNAME_RESET_VALUE,
                                                        null);

            return child;

        } else if (name.equals(CHILD_STAMOUNT)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STAMOUNT,
                                                        doComponentMortgageModel.FIELD_DFTOTALMORTGAGEAMOUNT,
                                                        CHILD_STAMOUNT_RESET_VALUE,
                                                        null);

            return child;

        } else if (name.equals(CHILD_STINCLUDEMIPREMIUM)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STINCLUDEMIPREMIUM,
                                                        doComponentMortgageModel.FIELD_DFMIALLOCAGTEFLAG,
                                                        CHILD_STINCLUDEMIPREMIUM_RESET_VALUE,
                                                        null);

            return child;

        } else if (name.equals(CHILD_STPAYMENTAMOUNT)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STPAYMENTAMOUNT,
                                                        doComponentMortgageModel.FIELD_DFTOTALPAYMENTAMOUNT,
                                                        CHILD_STPAYMENTAMOUNT_RESET_VALUE,
                                                        null);

            return child;

        } else if (name.equals(CHILD_STALLOWTAXESCROW)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STALLOWTAXESCROW,
                                                        doComponentMortgageModel.FIELD_DFPROPERTYTAXALLOCATEFLAG,
                                                        CHILD_STALLOWTAXESCROW_RESET_VALUE,
                                                        null);

            return child;

        } else if (name.equals(CHILD_STNETRATE)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STNETRATE,
                                                        doComponentMortgageModel.FIELD_DFNETINTRESTRATE,
                                                        CHILD_STNETRATE_RESET_VALUE,
                                                        null);

            return child;

        } else if (name.equals(CHILD_STDISCOUNT)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STDISCOUNT,
                                                        doComponentMortgageModel.FIELD_DFDISCOUNT,
                                                        CHILD_STDISCOUNT_RESET_VALUE,
                                                        null);

            return child;

        } else if (name.equals(CHILD_STPREMIUM)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STPREMIUM,
                                                        doComponentMortgageModel.FIELD_DFPREMIUM,
                                                        CHILD_STPREMIUM_RESET_VALUE,
                                                        null);

            return child;

        } else if (name.equals(CHILD_STBUYDOWNRATE)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STBUYDOWNRATE,
                                                        doComponentMortgageModel.FIELD_DFBUYDOWNRATE,
                                                        CHILD_STBUYDOWNRATE_RESET_VALUE,
                                                        null);

            return child;

        }
        /******************************************MCM Impl Team XS_16.13 changes starts *********************************/
        else if (name.equals(CHILD_STCASHBACKAMOUNT)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STCASHBACKAMOUNT,
                                                        doComponentMortgageModel.FIELD_DFCASHBACKAMOUNT,
                                                        CHILD_STCASHBACKAMOUNT_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STHOLDBACKAMOUNT)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STHOLDBACKAMOUNT,
                                                        doComponentMortgageModel.FIELD_DFADVANCEHOLD,
                                                        CHILD_STHOLDBACKAMOUNT_RESET_VALUE,
                                                        null);

            return child;
        }   
        else if (name.equals(CHILD_STMORTGAGEAMOUNT)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STMORTGAGEAMOUNT,
                                                        doComponentMortgageModel.FIELD_DFMORTGAGEAMOUNT,
                                                        CHILD_STMORTGAGEAMOUNT_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STACTUALPAYMENTTERM)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STACTUALPAYMENTTERM,
                                                        doComponentMortgageModel.FIELD_DFACTUALPAYMENTTERM,
                                                        CHILD_STACTUALPAYMENTTERM_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STACTUALPAYMENTTERMYEARS)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDefaultModel(),
                                                        CHILD_STACTUALPAYMENTTERMYEARS,
                                                        CHILD_STACTUALPAYMENTTERMYEARS,
                                                        CHILD_STACTUALPAYMENTTERMYEARS_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STACTUALPAYMENTTERMMONTHS)) {

            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(),
                    CHILD_STACTUALPAYMENTTERMMONTHS,
                    CHILD_STACTUALPAYMENTTERMMONTHS,
                    CHILD_STACTUALPAYMENTTERMMONTHS_RESET_VALUE,
                    null);

            return child;
        }
        else if (name.equals(CHILD_STREPAYMENTTYPE)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STREPAYMENTTYPE,
                                                        doComponentMortgageModel.FIELD_DFREPAYMENTTYPE,
                                                        CHILD_STREPAYMENTTYPE_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STRATEGUARANTEEPERIOD)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STRATEGUARANTEEPERIOD,
                                                        doComponentMortgageModel.FIELD_DFRATEGUARANTEEPERIOD,
                                                        CHILD_STRATEGUARANTEEPERIOD_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STFIRSTPAYMENTDATE)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STFIRSTPAYMENTDATE,
                                                        doComponentMortgageModel.FIELD_DFFIRSTPAYMENTDATE,
                                                        CHILD_STFIRSTPAYMENTDATE_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STMIPREMIUM)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STMIPREMIUM,
                                                        doComponentMortgageModel.FIELD_DFDEALSUMMARYMIPREMIUMAMOUNT,
                                                        CHILD_STMIPREMIUM_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STPAYMENTFREQUENCY)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STPAYMENTFREQUENCY,
                                                        doComponentMortgageModel.FIELD_DFPAYMENTFREQUENCY,
                                                        CHILD_STPAYMENTFREQUENCY_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STRATELOCKEDIN)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STRATELOCKEDIN,
                                                        doComponentMortgageModel.FIELD_DFRATELOCK,
                                                        CHILD_STRATELOCKEDIN_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STPANDIAPAYMENT)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STPANDIAPAYMENT,
                                                        doComponentMortgageModel.FIELD_DFPIPAYMENTAMOUNT,
                                                        CHILD_STPANDIAPAYMENT_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STEXISTINGACCOUNT)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STEXISTINGACCOUNT,
                                                        doComponentMortgageModel.FIELD_DFEXISTINGACOUNTINDICATOR,
                                                        CHILD_STEXISTINGACCOUNT_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STPREPAYMENTOPTION)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STPREPAYMENTOPTION,
                                                        doComponentMortgageModel.FIELD_DFPREPAYMENTOPTION,
                                                        CHILD_STPREPAYMENTOPTION_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STADDITIONALPRINCIPALPAYMENT)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STADDITIONALPRINCIPALPAYMENT,
                                                        doComponentMortgageModel.FIELD_DFADDITIONALPRINCIPAL,
                                                        CHILD_STADDITIONALPRINCIPALPAYMENT_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STEXISTINGACCOUNTREFERENCE)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STEXISTINGACCOUNTREFERENCE,
                                                        doComponentMortgageModel.FIELD_DFEXISTINGACOUNTNUMBER,
                                                        CHILD_STEXISTINGACCOUNTREFERENCE_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STPRIVILEGEPAYMENTOPTION)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STPRIVILEGEPAYMENTOPTION,
                                                        doComponentMortgageModel.FIELD_DFPRIVILEGEPAYMENTOPTION,
                                                        CHILD_STPRIVILEGEPAYMENTOPTION_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STPOSTEDRATE)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STPOSTEDRATE,
                                                        doComponentMortgageModel.FIELD_DFPOSTEDRATE,
                                                        CHILD_STPOSTEDRATE_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STFIRSTPAYMENTDATE)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STFIRSTPAYMENTDATE,
                                                        doComponentMortgageModel.FIELD_DFFIRSTPAYMENTDATE,
                                                        CHILD_STFIRSTPAYMENTDATE_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STAMORTIZATIONPERIODYEARS)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDefaultModel(),
                                                        CHILD_STAMORTIZATIONPERIODYEARS,
                                                        CHILD_STAMORTIZATIONPERIODYEARS,
                                                        CHILD_STAMORTIZATIONPERIODYEARS_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STAMORTIZATIONPERIODMONTHS)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDefaultModel(),
                                                        CHILD_STAMORTIZATIONPERIODMONTHS,
                                                        CHILD_STAMORTIZATIONPERIODMONTHS,
                                                        CHILD_STAMORTIZATIONPERIODMONTHS_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STCOMMISSIONCODE)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STCOMMISSIONCODE,
                                                        doComponentMortgageModel.FIELD_DFCOMMISSIONCODE,
                                                        CHILD_STCOMMISSIONCODE_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STTAXESCROW)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STTAXESCROW,
                                                        doComponentMortgageModel.FIELD_DFDEALSUMMARYTAXESCROWAMOUNT,
                                                        CHILD_STTAXESCROW_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STMATURITYDATE)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STMATURITYDATE,
                                                        doComponentMortgageModel.FIELD_DFMATURITYDATE,
                                                        CHILD_STMATURITYDATE_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STEFFECTIVEAMORTIZATION)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STEFFECTIVEAMORTIZATION,
                                                        doComponentMortgageModel.FIELD_DFEFFECTIVEAMORTIZATIONINMONTHS,
                                                        CHILD_STEFFECTIVEAMORTIZATION_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STEFFECTIVEAMORTIZATIONYEARS)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDefaultModel(),
                                                        CHILD_STEFFECTIVEAMORTIZATIONYEARS,
                                                        CHILD_STEFFECTIVEAMORTIZATIONYEARS,
                                                        CHILD_STEFFECTIVEAMORTIZATIONYEARS_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STEFFECTIVEAMORTIZATIONMONTHS)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDefaultModel(),
                                                        CHILD_STEFFECTIVEAMORTIZATIONMONTHS,
                                                        CHILD_STEFFECTIVEAMORTIZATIONMONTHS,
                                                        CHILD_STEFFECTIVEAMORTIZATIONMONTHS_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STCASHBACKPERCENTAGE)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STCASHBACKPERCENTAGE,
                                                        doComponentMortgageModel.FIELD_DFCASHBACKPERCENTAGE,
                                                        CHILD_STCASHBACKPERCENTAGE_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STTOTALPAYMENT)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STTOTALPAYMENT,
                                                        doComponentMortgageModel.FIELD_DFTOTALPAYMENTAMOUNT,
                                                        CHILD_STTOTALPAYMENT_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STADDITIONALDETAILS)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STADDITIONALDETAILS,
                                                        doComponentMortgageModel.FIELD_DFADDITIONALINFORMATION,
                                                        CHILD_STADDITIONALDETAILS_RESET_VALUE,
                                                        null);

            return child;
        }
        else if (name.equals(CHILD_STPAYMENTTERMDESRIPTION)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentMortgageModel(),
                                                        CHILD_STPAYMENTTERMDESRIPTION,
                                                        doComponentMortgageModel.FIELD_DFPAYMENTTERMDESC,
                                                        CHILD_STPAYMENTTERMDESRIPTION_RESET_VALUE,
                                                        null);

            return child;
        }
        /******************************************MCM Impl Team XS_16.13 changes ends*********************************/
        else {

            throw new IllegalArgumentException("Invalid child name [" + name +
                                               "]");

        }

    }

    /**
     * Getters and setters for the display fields starts
     */
    public StaticTextField getStDiscount () {

        return (StaticTextField) getChild(CHILD_STDISCOUNT);

    }

    public StaticTextField getStPremium () {

        return (StaticTextField) getChild(CHILD_STPREMIUM);

    }

    public StaticTextField getStBuyDownRate () {

        return (StaticTextField) getChild(CHILD_STBUYDOWNRATE);

    }

    public StaticTextField getStComponentType () {

        return (StaticTextField) getChild(CHILD_STCOMPONENTTYPE);

    }

    public StaticTextField getStProductName () {

        return (StaticTextField) getChild(CHILD_STPRODUCTNAME);

    }

    public StaticTextField getStIncludeMIPremium () {

        return (StaticTextField) getChild(CHILD_STINCLUDEMIPREMIUM);

    }

    public StaticTextField getStPaymentAmount () {

        return (StaticTextField) getChild(CHILD_STPAYMENTAMOUNT);

    }

    public StaticTextField getStAllowTaxEscrow () {

        return (StaticTextField) getChild(CHILD_STALLOWTAXESCROW);

    }

    public StaticTextField getStNetRate () {

        return (StaticTextField) getChild(CHILD_STNETRATE);

    }

    public StaticTextField getStAmount () {

        return (StaticTextField) getChild(CHILD_STAMOUNT);

    }

    /**************************MCM Impl Team XS_16.13 Changes Starts ********************************/
    public StaticTextField getStCashbackAmount () {

        return (StaticTextField) getChild(CHILD_STCASHBACKAMOUNT);

    }

    public StaticTextField getStHoldbackAmount () {

        return (StaticTextField) getChild(CHILD_STHOLDBACKAMOUNT);

    }

    private StaticTextField getStAdditionalDetails()
    {
        return (StaticTextField) getChild(CHILD_STADDITIONALDETAILS);
    }

    private StaticTextField getStTotalPayment()
    {
        return (StaticTextField) getChild(CHILD_STTOTALPAYMENT);
    }

    private StaticTextField getStCashbackPercentage()
    {
        return (StaticTextField) getChild(CHILD_STCASHBACKPERCENTAGE);
    }

    private StaticTextField getStEffectiveAmortization()
    {
        return (StaticTextField) getChild(CHILD_STEFFECTIVEAMORTIZATION);
    }
    private StaticTextField getStEffectiveAmortizationYears()
    {
        return (StaticTextField) getChild(CHILD_STEFFECTIVEAMORTIZATIONYEARS);
    }

    private StaticTextField getStEffectiveAmortizationMonths()
    {
        return (StaticTextField) getChild(CHILD_STEFFECTIVEAMORTIZATIONMONTHS);
    }

    private StaticTextField getStMaturityDate()
    {
        return (StaticTextField) getChild(CHILD_STMATURITYDATE);
    }

    private StaticTextField getStTaxEscrow()
    {
        return (StaticTextField) getChild(CHILD_STTAXESCROW);
    }

    private StaticTextField getStCommissionCode()
    {
        return (StaticTextField) getChild(CHILD_STCOMMISSIONCODE);
    }


    private StaticTextField getStAmortizationPeriodYears()
    {
        return (StaticTextField) getChild(CHILD_STAMORTIZATIONPERIODYEARS);
    }
    private StaticTextField getStAmortizationPeriodMonths()
    {
        return (StaticTextField) getChild(CHILD_STAMORTIZATIONPERIODMONTHS);
    }

    private StaticTextField getStFirstPaymentDate()
    {
        return (StaticTextField) getChild(CHILD_STFIRSTPAYMENTDATE);
    }

    private StaticTextField getStPostedRate()
    {
        return (StaticTextField) getChild(CHILD_STPOSTEDRATE);
    }

    private StaticTextField getStPrivilegePaymentOption()
    {
        return (StaticTextField) getChild(CHILD_STPRIVILEGEPAYMENTOPTION);
    }

    private StaticTextField getStExistingAccountReference()
    {
        return (StaticTextField) getChild(CHILD_STEXISTINGACCOUNTREFERENCE);
    }

    private StaticTextField getStAdditionalPrincipalPayment()
    {
        return (StaticTextField) getChild(CHILD_STADDITIONALPRINCIPALPAYMENT);
    }

    private StaticTextField getStPrePaymentOption()
    {
        return (StaticTextField) getChild(CHILD_STPREPAYMENTOPTION);
    }



    private StaticTextField getStExistingAccount()
    {
        return (StaticTextField) getChild(CHILD_STEXISTINGACCOUNT);
    }

    private StaticTextField getStPAndIPayment()
    {
        return (StaticTextField) getChild(CHILD_STPANDIAPAYMENT);
    }

    private StaticTextField getStRateLockedIn()
    {
        return (StaticTextField) getChild(CHILD_STRATELOCKEDIN);
    }

    private StaticTextField getStPaymentFrequency()
    {
        return (StaticTextField) getChild(CHILD_STPAYMENTFREQUENCY);
    }

    private StaticTextField getStRateGuaranteePeriod()
    {
        return (StaticTextField) getChild(CHILD_STRATEGUARANTEEPERIOD);
    }

    private StaticTextField getStRepaymentType()
    {
        return (StaticTextField) getChild(CHILD_STREPAYMENTTYPE);
    }

    private StaticTextField getStActualPaymentTerm()
    {
        return (StaticTextField) getChild(CHILD_STACTUALPAYMENTTERM);
    }

    private StaticTextField getStActualPaymentTermYears()
    {
        return (StaticTextField) getChild(CHILD_STACTUALPAYMENTTERMYEARS);
    }
    private StaticTextField getStActualPaymentTermMonths()
    {
        return (StaticTextField) getChild(CHILD_STACTUALPAYMENTTERMMONTHS);
    }
    private StaticTextField getStMortgageAmount()
    {
        return (StaticTextField) getChild(CHILD_STMORTGAGEAMOUNT);
    }

  

    /**********************************************MCM Impl Team XS_16.13 changes ends**************************************************/
    /**
     * Getters and setters for the display fields ends.
     */

    /**
     * <p>
     * Description: This method returns the model of type
     * doComponentMortgageModel used by this class. If the model has not been
     * set, an instance is created.
     * </p>
     * @returns doComponentMortgageModel - the model used by this view
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    public doComponentMortgageModel getDoComponentMortgageModel () {

        if (doComponentMortgageModel == null) {

            doComponentMortgageModel = (doComponentMortgageModel) getModel(doComponentMortgageModel.class);

        }

        return doComponentMortgageModel;

    }

}
