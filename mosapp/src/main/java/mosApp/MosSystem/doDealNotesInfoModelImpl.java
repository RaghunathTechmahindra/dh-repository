package mosApp.MosSystem;

import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.SQLException;

import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;

import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doDealNotesInfoModelImpl extends QueryModelBase
	implements doDealNotesInfoModel
{
	/**
	 *
	 *
	 */
	public doDealNotesInfoModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

   logger = SysLog.getSysLogger("DNIM");
   logger.debug("DNIM@afterExecute::Size: " + sql);

		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    logger = SysLog.getSysLogger("DNIM");
    logger.debug("DNIM@afterExecute::Size: " + this.getSize());
    logger.debug("DNIM@afterExecute::SQLTemplate: " + this.getSelectSQL());

    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By John 25Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();

    while(this.next())
    {
      //  Category
      this.setDfDealNotesCategory(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
        "DEALNOTESCATEGORY", this.getDfDealNotesCategory(), languageId));
  		//#DG702 reset clob as string
      //================================================================
      //CLOB Performance Enhancement June 2010
      BigDecimal dnTextLength_bd = (BigDecimal)getValue(FIELD_DFDEALNOTESTEXTLENGTH);
      int dnTextLength = (dnTextLength_bd == null)?0:dnTextLength_bd.intValue();
 			String dnText;
 			if(dnTextLength > 4000) {
	 			try {
					Clob dnTextObj = (Clob)getValue(FIELD_DFDEALNOTESTEXT);
					dnText = TypeConverter.stringFromClob(dnTextObj);
				}
				catch (Exception e) {
					SysLog.error(getClass(), StringUtil.stack2string(e), "DealNotesIM");
					dnText = null;
				}
 			} else {
 				dnText = (String)getValue(FIELD_DFDEALNOTESTEXTVARCHAR);
 			}
			setDfDealNotesText(dnText);
  		//#DG702 end		
    }
    //Reset Location
    beforeFirst();		//#DG702	
    }

	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**	 *
	 *
	 */
	public java.sql.Timestamp getDfDealNotesDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFDEALNOTESDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfDealNotesDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFDEALNOTESDATE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDealNotesCategory()
	{
		return (String)getValue(FIELD_DFDEALNOTESCATEGORY);
	}


	/**
	 *
	 *
	 */
	public void setDfDealNotesCategory(String value)
	{
		setValue(FIELD_DFDEALNOTESCATEGORY,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealNotesID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALNOTESID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealNotesID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALNOTESID,value);
	}


	/**
	 *
	 *
	 */
  //--DJ_CR134--start--27May2004--//
  public java.math.BigDecimal getDfDealNotesCategoryID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALNOTESCATEGORYID);
	}

	/**
	 *
	 *
	 */
	public void setDfDealNotesCategoryID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALNOTESCATEGORYID,value);
	}
  //--DJ_CR134--end--//

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealNotesUserID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALNOTESUSERID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealNotesUserID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALNOTESUSERID,value);
	}


	/**
	 * bilingual deal notes
	 *
	 */
	public String getDfDealNotesText()
	{
		return (String)getValue(FIELD_DFDEALNOTESTEXT);
	}


	/**
	 * bilingual deal notes
	 *
	 */
	public void setDfDealNotesText(String value)
	{
		setValue(FIELD_DFDEALNOTESTEXT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfUserName()
	{
		return (String)getValue(FIELD_DFUSERNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfUserName(String value)
	{
		setValue(FIELD_DFUSERNAME,value);
	}

    public java.math.BigDecimal getDfInstitutionId() {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
      }

      public void setDfInstitutionId(java.math.BigDecimal value) {
        setValue(FIELD_DFINSTITUTIONID, value);
      }


	/**
	 * MetaData object test method to verify the SYNTHETIC new
	 * field for the reogranized SQL_TEMPLATE query.
	 *
	 */
  /**
	public void setResultSet(ResultSet value)
	{
  		logger = SysLog.getSysLogger("METADATA");

  		try
  		{
   			super.setResultSet(value);
   			if( value != null)
   			{
       			ResultSetMetaData metaData = value.getMetaData();
       			int columnCount = metaData.getColumnCount();
       			logger.debug("===============================================");
              	        logger.debug("Testing MetaData Object for new synthetic fields for" +
                 	            " doDealEntrySourceInfoModel");
       			logger.debug("NumberColumns: " + columnCount);
         		for(int i = 0; i < columnCount ; i++)
          		{
                 	logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
          		}
          		logger.debug("================================================");
      		}
  		}
  		catch (SQLException e)
  		{
    		    logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
  		}
 	}
  **/

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	////public static final String SELECT_SQL_TEMPLATE="SELECT ALL DEALNOTES.DEALNOTESDATE, DEALNOTESCATEGORY.CATEGORYNAME, DEALNOTES.DEALNOTESID, DEALNOTES.DEALNOTESCATEGORYID, DEALNOTES.DEALID, DEALNOTES.DEALNOTESUSERID, DEALNOTES.DEALNOTESTEXT,  contact.contactlastname || ',' || substr(contact.CONTACTFIRSTNAME,1,1) username FROM DEALNOTES, DEALNOTESCATEGORY, USERPROFILE, CONTACT  __WHERE__  ORDER BY DEALNOTES.DEALNOTESDATE  DESC";

  //--Release2.1--//
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL DEALNOTES.DEALNOTESDATE, DEALNOTESCATEGORY.CATEGORYNAME, DEALNOTES.DEALNOTESID, DEALNOTES.DEALNOTESCATEGORYID, DEALNOTES.DEALID, DEALNOTES.DEALNOTESUSERID, DEALNOTES.DEALNOTESTEXT,  contact.contactlastname || ',' || substr(contact.CONTACTFIRSTNAME,1,1) SYNTETIC_USERNAME FROM DEALNOTES, DEALNOTESCATEGORY, USERPROFILE, CONTACT  __WHERE__  ORDER BY DEALNOTES.DEALNOTESDATE  DESC";
  public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL DEALNOTES.DEALNOTESDATE, " +
    "to_char(DEALNOTES.DEALNOTESCATEGORYID) CATEGORYID_STR, " +
    "DEALNOTES.DEALNOTESID, " +
    //--DJ_CR134--start--27May2004--//
    "DEALNOTES.DEALNOTESCATEGORYID, " +
    //--DJ_CR134--end--//
    "DEALNOTES.DEALID, DEALNOTES.DEALNOTESUSERID, " +
    //================================================================
    //CLOB Performance Enhancement June 2010
    "DEALNOTES.DEALNOTESTEXT,  " +
    "LENGTH(DEALNOTES.DEALNOTESTEXT) DEALNOTESTEXT_LENGTH,  " +
    "CAST(SUBSTR(DEALNOTES.DEALNOTESTEXT,1,4000) as VARCHAR(4000)) DEALNOTESTEXT_VARCHAR," +
    //================================================================
    "contact.contactlastname || ',' || substr(contact.CONTACTFIRSTNAME,1,1) SYNTETIC_USERNAME, " +
    "USERPROFILE.INSTITUTIONPROFILEID, "+
    "DEALNOTES.LANGUAGEPREFERENCEID " +
    "FROM DEALNOTES, USERPROFILE, CONTACT,  " +
    //--DJ_CR134--start--27May2004--//
    "DEALNOTESCATEGORY " +
    //--DJ_CR134--end--//
    "__WHERE__  ORDER BY DEALNOTES.DEALNOTESDATE  DESC";

  //public static final String MODIFYING_QUERY_TABLE_NAME="DEALNOTES, DEALNOTESCATEGORY, USERPROFILE, CONTACT";
  public static final String MODIFYING_QUERY_TABLE_NAME=
    "DEALNOTES, USERPROFILE, CONTACT, " +
    //--DJ_CR134--start--27May2004--//
    "DEALNOTESCATEGORY";
    //--DJ_CR134--end--//

  //public static final String STATIC_WHERE_CRITERIA=" (DEALNOTES.DEALNOTESCATEGORYID  =  DEALNOTESCATEGORY.DEALNOTESCATEGORYID) AND (USERPROFILE.CONTACTID  =  CONTACT.CONTACTID) AND (USERPROFILE.USERPROFILEID  =  DEALNOTES.DEALNOTESUSERID) ";
  public static final String STATIC_WHERE_CRITERIA=
      " (USERPROFILE.INSTITUTIONPROFILEID  =  CONTACT.INSTITUTIONPROFILEID) AND " +
      " (USERPROFILE.CONTACTID  =  CONTACT.CONTACTID) AND " +
      " (DEALNOTES.INSTITUTIONPROFILEID  =  USERPROFILE.INSTITUTIONPROFILEID) AND " +
      " (DEALNOTES.DEALNOTESUSERID  =  USERPROFILE.USERPROFILEID) AND" +
      //--DJ_CR134--start--27May2004--//
      " (DEALNOTES.DEALNOTESCATEGORYID  =  DEALNOTESCATEGORY.DEALNOTESCATEGORYID) ";
      //--DJ_CR134--end--//

  //--------------//

	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALNOTESDATE="DEALNOTES.DEALNOTESDATE";
	public static final String COLUMN_DFDEALNOTESDATE="DEALNOTESDATE";
	public static final String QUALIFIED_COLUMN_DFDEALNOTESCATEGORY="DEALNOTESCATEGORY.CATEGORYID_STR";
	public static final String COLUMN_DFDEALNOTESCATEGORY="CATEGORYID_STR";
	public static final String QUALIFIED_COLUMN_DFDEALNOTESID="DEALNOTES.DEALNOTESID";
	public static final String COLUMN_DFDEALNOTESID="DEALNOTESID";

  //--DJ_CR134--start--27May2004--//
  public static final String QUALIFIED_COLUMN_DFDEALNOTESCATEGORYID="DEALNOTES.DEALNOTESCATEGORYID";
  public static final String COLUMN_DFDEALNOTESCATEGORYID="DEALNOTESCATEGORYID";
  //--DJ_CR134--end--//

	public static final String QUALIFIED_COLUMN_DFDEALID="DEALNOTES.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFDEALNOTESUSERID="DEALNOTES.DEALNOTESUSERID";
	public static final String COLUMN_DFDEALNOTESUSERID="DEALNOTESUSERID";
	public static final String QUALIFIED_COLUMN_DFDEALNOTESTEXT="DEALNOTES.DEALNOTESTEXT";
	public static final String COLUMN_DFDEALNOTESTEXT="DEALNOTESTEXT";
	//================================================================
	//CLOB Performance Enhancement June 2010
	public static final String QUALIFIED_COLUMN_DFDEALNOTESTEXTVARCHAR="DEALNOTES.DEALNOTESTEXT_VARCHAR";
	public static final String COLUMN_DFDEALNOTESTEXTVARCHAR="DEALNOTESTEXT_VARCHAR";
	public static final String QUALIFIED_COLUMN_DFDEALNOTESTEXTLENGTH="DEALNOTES.DEALNOTESTEXT_LENGTH";
	public static final String COLUMN_DFDEALNOTESTEXTLENGTH="DEALNOTESTEXT_LENGTH";
	//================================================================
	////public static final String QUALIFIED_COLUMN_DFUSERNAME=".";
	////public static final String COLUMN_DFUSERNAME="";
	public static final String QUALIFIED_COLUMN_DFUSERNAME="CONTACT.SYNTETIC_USERNAME";
	public static final String COLUMN_DFUSERNAME="SYNTETIC_USERNAME";
	public static final String QUALIFIED_COLUMN_DFINSTITUTIONID = "PROPERTYEXPENSE.INSTITUTIONPROFILEID";
	public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
	public static final String QUALIFIED_COLUMN_DFLANGUAGEPREFERENCEID="DEALNOTES.LANGUAGEPREFERENCEID";
	public static final String COLUMN_DFLANGUAGEPREFERENCEID="LANGUAGEPREFERENCEID";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALNOTESDATE,
				COLUMN_DFDEALNOTESDATE,
				QUALIFIED_COLUMN_DFDEALNOTESDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALNOTESCATEGORY,
				COLUMN_DFDEALNOTESCATEGORY,
				QUALIFIED_COLUMN_DFDEALNOTESCATEGORY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALNOTESID,
				COLUMN_DFDEALNOTESID,
				QUALIFIED_COLUMN_DFDEALNOTESID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    //--DJ_CR134--start--27May2004--//
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALNOTESCATEGORYID,
				COLUMN_DFDEALNOTESCATEGORYID,
				QUALIFIED_COLUMN_DFDEALNOTESCATEGORYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    //--DJ_CR134--end--//
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALNOTESUSERID,
				COLUMN_DFDEALNOTESUSERID,
				QUALIFIED_COLUMN_DFDEALNOTESUSERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALNOTESTEXT,
				COLUMN_DFDEALNOTESTEXT,
				QUALIFIED_COLUMN_DFDEALNOTESTEXT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
		//================================================================
		//CLOB Performance Enhancement June 2010
		 FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALNOTESTEXTVARCHAR,
				COLUMN_DFDEALNOTESTEXTVARCHAR,
				QUALIFIED_COLUMN_DFDEALNOTESTEXTVARCHAR,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		 
		 FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALNOTESTEXTLENGTH,
				COLUMN_DFDEALNOTESTEXTLENGTH,
				QUALIFIED_COLUMN_DFDEALNOTESTEXTLENGTH,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
	//================================================================
	 
		//// Improper converted fragment by iMT.
    ////FIELD_SCHEMA.addFieldDescriptor(
		////	new QueryFieldDescriptor(
		////		FIELD_DFUSERNAME,
		////		COLUMN_DFUSERNAME,
		////		QUALIFIED_COLUMN_DFUSERNAME,
		////		String.class,
		////		false,
		////		false,
		////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		////		"",
		////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		////		""));

		//// Adjusted FieldDescriptor for the ComputedColumn to fix the iMT tool bug.
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERNAME,
				COLUMN_DFUSERNAME,
				QUALIFIED_COLUMN_DFUSERNAME,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"contact.contactlastname || ',' || substr(contact.CONTACTFIRSTNAME,1,1)",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"contact.contactlastname || ',' || substr(contact.CONTACTFIRSTNAME,1,1)"));

    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINSTITUTIONID,
				COLUMN_DFINSTITUTIONID,
				QUALIFIED_COLUMN_DFINSTITUTIONID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLANGUAGEPREFERENCEID,
				COLUMN_DFLANGUAGEPREFERENCEID,
				QUALIFIED_COLUMN_DFLANGUAGEPREFERENCEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
	}

}

