package mosApp.MosSystem;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mosApp.SQLConnectionManagerImpl;

import MosSystem.Mc;

import com.basis100.picklist.BXResources;
import com.filogix.express.web.jato.ExpressViewBeanBase;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.view.CommandFieldDescriptor;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.CheckBox;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HREF;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.ListBox;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

/**
 *
 *
 *
 */
public class pgCommitmentOfferViewBean extends ExpressViewBeanBase
{
    // The logger
    private final static Logger _logger = 
        LoggerFactory.getLogger(pgCommitmentOfferViewBean.class);

	/**
	 *
	 *
	 */
	public pgCommitmentOfferViewBean()
	{
		super(PAGE_NAME);
		setDefaultDisplayURL(DEFAULT_DISPLAY_URL);
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
	    View superReturn = super.createChild(name);
        if (superReturn != null) {
            return superReturn;
        } else  if (name.equals(CHILD_TBDEALID))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBDEALID,
				CHILD_TBDEALID,
				CHILD_TBDEALID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPAGENAMES))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES_RESET_VALUE,
				null);
      //--Release2.1--//
			//child.setLabelForNoneSelected("Choose a Page");
      child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
			child.setOptions(cbPageNamesOptions);
			return child;
		}
		else
		if (name.equals(CHILD_BTPROCEED))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPROCEED,
				CHILD_BTPROCEED,
				CHILD_BTPROCEED_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF1))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF1,
				CHILD_HREF1,
				CHILD_HREF1_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF2))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF2,
				CHILD_HREF2,
				CHILD_HREF2_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF3))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF3,
				CHILD_HREF3,
				CHILD_HREF3_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF4))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF4,
				CHILD_HREF4,
				CHILD_HREF4_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF5))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF5,
				CHILD_HREF5,
				CHILD_HREF5_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF6))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF6,
				CHILD_HREF6,
				CHILD_HREF6_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF7))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF7,
				CHILD_HREF7,
				CHILD_HREF7_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF8))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF8,
				CHILD_HREF8,
				CHILD_HREF8_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF9))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF9,
				CHILD_HREF9,
				CHILD_HREF9_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF10))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF10,
				CHILD_HREF10,
				CHILD_HREF10_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCOMPANYNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTODAYDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUSERNAMETITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALID))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWDealSummarySnapShotModel(),
				CHILD_STDEALID,
				doUWDealSummarySnapShotModel.FIELD_DFAPPLICATIONID,
				CHILD_STDEALID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBORRFIRSTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBORRFIRSTNAME,
				CHILD_STBORRFIRSTNAME,
				CHILD_STBORRFIRSTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALSTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWDealSummarySnapShotModel(),
				CHILD_STDEALSTATUS,
				doUWDealSummarySnapShotModel.FIELD_DFSTATUSDESCR,
				CHILD_STDEALSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALSTATUSDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWDealSummarySnapShotModel(),
				CHILD_STDEALSTATUSDATE,
				doUWDealSummarySnapShotModel.FIELD_DFSTATUSDATE,
				CHILD_STDEALSTATUSDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSOURCEFIRM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWDealSummarySnapShotModel(),
				CHILD_STSOURCEFIRM,
				doUWDealSummarySnapShotModel.FIELD_DFSFSHORTNAME,
				CHILD_STSOURCEFIRM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSOURCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWDealSummarySnapShotModel(),
				CHILD_STSOURCE,
				doUWDealSummarySnapShotModel.FIELD_DFSOBPSHORTNAME,
				CHILD_STSOURCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUNDERWRITERLASTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWDealSummarySnapShotModel(),
				CHILD_STUNDERWRITERLASTNAME,
				doUWDealSummarySnapShotModel.FIELD_DFUNDERWRITERLASTNAME,
				CHILD_STUNDERWRITERLASTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUNDERWRITERFIRSTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWDealSummarySnapShotModel(),
				CHILD_STUNDERWRITERFIRSTNAME,
				doUWDealSummarySnapShotModel.FIELD_DFUNDERWRITERFIRSTNAME,
				CHILD_STUNDERWRITERFIRSTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUNDERWRITERMIDDLEINITIAL))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWDealSummarySnapShotModel(),
				CHILD_STUNDERWRITERMIDDLEINITIAL,
				doUWDealSummarySnapShotModel.FIELD_DFUNDERWRITERMIDDLEINITIAL,
				CHILD_STUNDERWRITERMIDDLEINITIAL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWDealSummarySnapShotModel(),
				CHILD_STDEALTYPE,
				doUWDealSummarySnapShotModel.FIELD_DFDEALTYPEDESCR,
				CHILD_STDEALTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALPURPOSE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWDealSummarySnapShotModel(),
				CHILD_STDEALPURPOSE,
				doUWDealSummarySnapShotModel.FIELD_DFLOANPURPOSEDESCR,
				CHILD_STDEALPURPOSE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTOTALLOANAMOUNT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWDealSummarySnapShotModel(),
				CHILD_STTOTALLOANAMOUNT,
				doUWDealSummarySnapShotModel.FIELD_DFTOTALLOANAMT,
				CHILD_STTOTALLOANAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPLICATIONDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWDealSummarySnapShotModel(),
				CHILD_STAPPLICATIONDATE,
				doUWDealSummarySnapShotModel.FIELD_DFAPPLICATIONDATE,
				CHILD_STAPPLICATIONDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMTTERM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWDealSummarySnapShotModel(),
				CHILD_STPMTTERM,
				doUWDealSummarySnapShotModel.FIELD_DFPAYMENTTERMDESCR,
				CHILD_STPMTTERM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STESTCLOSINGDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWDealSummarySnapShotModel(),
				CHILD_STESTCLOSINGDATE,
				doUWDealSummarySnapShotModel.FIELD_DFESTCLOSINGDATE,
				CHILD_STESTCLOSINGDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSPECIALFEATURE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWDealSummarySnapShotModel(),
				CHILD_STSPECIALFEATURE,
				doUWDealSummarySnapShotModel.FIELD_DFSPECIALFEATURE,
				CHILD_STSPECIALFEATURE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTSUBMIT))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSUBMIT,
				CHILD_BTSUBMIT,
				CHILD_BTSUBMIT_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTRESOLVEDEAL))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTRESOLVEDEAL,
				CHILD_BTRESOLVEDEAL,
				CHILD_BTRESOLVEDEAL_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTWORKQUEUELINK))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_CHANGEPASSWORDHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLHISTORY))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOONOTES))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLSEARCH))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLLOG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STERRORFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_DETECTALERTTASKS))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTCANCEL))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTCANCEL,
				CHILD_BTCANCEL,
				CHILD_BTCANCEL_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTPREVTASKPAGE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPREVTASKPAGE,
				CHILD_BTPREVTASKPAGE,
				CHILD_BTPREVTASKPAGE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPREVTASKPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPREVTASKPAGELABEL,
				CHILD_STPREVTASKPAGELABEL,
				CHILD_STPREVTASKPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTNEXTTASKPAGE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTNEXTTASKPAGE,
				CHILD_BTNEXTTASKPAGE,
				CHILD_BTNEXTTASKPAGE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STNEXTTASKPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STNEXTTASKPAGELABEL,
				CHILD_STNEXTTASKPAGELABEL,
				CHILD_STNEXTTASKPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTASKNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTASKNAME,
				CHILD_STTASKNAME,
				CHILD_STTASKNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_SESSIONUSERID))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTSTDADDCONDITION))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSTDADDCONDITION,
				CHILD_BTSTDADDCONDITION,
				CHILD_BTSTDADDCONDITION_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTCUSADDCONDITION))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTCUSADDCONDITION,
				CHILD_BTCUSADDCONDITION,
				CHILD_BTCUSADDCONDITION_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_LBSTDCONDITION))
		{
			ListBox child = new ListBox(this,
				getDefaultModel(),
				CHILD_LBSTDCONDITION,
				CHILD_LBSTDCONDITION,
				CHILD_LBSTDCONDITION_RESET_VALUE,
				null);

			child.setLabelForNoneSelected("");
			child.setOptions(lbStdConditionOptions);
			child.setMultiSelect(true);
			return child;
		}
		else
		if (name.equals(CHILD_LBCUSTCONDITION))
		{
			ListBox child = new ListBox(this,
				getDefaultModel(),
				CHILD_LBCUSTCONDITION,
				CHILD_LBCUSTCONDITION,
				CHILD_LBCUSTCONDITION_RESET_VALUE,
				null);
      //--Release2.1--//
			//child.setLabelForNoneSelected("Free form");
      child.setLabelForNoneSelected(CHILD_LBCUSTCONDITION_NONSELECTED_LABEL);
			child.setOptions(lbCustConditionOptions);
			child.setMultiSelect(true);
			return child;
		}
		else
    //--> This is not necessary to bind to DataModel
    //--> Fixed by Billy 26Nov2002
		if (name.equals(CHILD_CBLANGUAGEPREFERENCE))
		{
			ComboBox child = new ComboBox( this,
				//getdoApplicantEntryApplicantSelectModel(),
        getDefaultModel(),
				CHILD_CBLANGUAGEPREFERENCE,
				//doApplicantEntryApplicantSelectModel.FIELD_DFLANGUAGEPREFID,
        CHILD_CBLANGUAGEPREFERENCE,
				CHILD_CBLANGUAGEPREFERENCE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbLanguagePreferenceOptions);
			return child;
		}
    //==============================================
		else
		if (name.equals(CHILD_REPEATED4))
		{
			pgCommitmentOfferRepeated4TiledView child = new pgCommitmentOfferRepeated4TiledView(this,
				CHILD_REPEATED4);
			return child;
		}
		else
		if (name.equals(CHILD_REPEATED1))
		{
			pgCommitmentOfferRepeated1TiledView child = new pgCommitmentOfferRepeated1TiledView(this,
				CHILD_REPEATED1);
			return child;
		}
		else
		if (name.equals(CHILD_REPEATED2))
		{
			pgCommitmentOfferRepeated2TiledView child = new pgCommitmentOfferRepeated2TiledView(this,
				CHILD_REPEATED2);
			return child;
		}
		else
		if (name.equals(CHILD_REPEATED3))
		{
			pgCommitmentOfferRepeated3TiledView child = new pgCommitmentOfferRepeated3TiledView(this,
				CHILD_REPEATED3);
			return child;
		}
    //// SYNCADD.
    else
 		if (name.equals(CHILD_REPEATED5))
 		{
 			pgCommitmentOfferRepeated5TiledView child = new pgCommitmentOfferRepeated5TiledView(this,
 				CHILD_REPEATED5);
      return child;
    }
		else
		if (name.equals(CHILD_STPMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASOK,
				CHILD_STPMHASOK,
				CHILD_STPMHASOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMTITLE,
				CHILD_STPMTITLE,
				CHILD_STPMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMONOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMONOK,
				CHILD_STPMONOK,
				CHILD_STPMONOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGS,
				CHILD_STPMMSGS,
				CHILD_STPMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMTITLE,
				CHILD_STAMTITLE,
				CHILD_STAMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGS,
				CHILD_STAMMSGS,
				CHILD_STAMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMDIALOGMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMBUTTONSHTML))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDBINDDUMMY))
		{
			HiddenField child = new HiddenField(this,
				getdoRowGeneratorModel(),
				CHILD_HDBINDDUMMY,
				doRowGeneratorModel.FIELD_DFROWNDX,
				CHILD_HDBINDDUMMY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTOK))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTOK,
				CHILD_BTOK,
				CHILD_BTOK_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STADDSTDBUTTONHTML))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STADDSTDBUTTONHTML,
				CHILD_STADDSTDBUTTONHTML,
				CHILD_STADDSTDBUTTONHTML_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STADDCUSTBUTTONHTML))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STADDCUSTBUTTONHTML,
				CHILD_STADDCUSTBUTTONHTML,
				CHILD_STADDCUSTBUTTONHTML_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STVIEWONLYTAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STVIEWONLYTAG,
				CHILD_STVIEWONLYTAG,
				CHILD_STVIEWONLYTAG_RESET_VALUE,
				null);
			return child;
		}
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> by BILLY 15July2002
    else
    if (name.equals(CHILD_BTACTMSG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTACTMSG,
				CHILD_BTACTMSG,
				CHILD_BTACTMSG_RESET_VALUE,
				new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));
				return child;
    }
    //// SYNCADD.
 		if (name.equals(CHILD_STTARGETSTDCON))
 		{
 			StaticTextField child = new StaticTextField(this,
 				getDefaultModel(),
 				CHILD_STTARGETSTDCON,
 				CHILD_STTARGETSTDCON,
 				CHILD_STTARGETSTDCON_RESET_VALUE,
 				null);
 			return child;
 		}
 		else
 		if (name.equals(CHILD_STTARGETCUSTCON))
 		{
 			StaticTextField child = new StaticTextField(this,
 				getDefaultModel(),
 				CHILD_STTARGETCUSTCON,
 				CHILD_STTARGETCUSTCON,
 				CHILD_STTARGETCUSTCON_RESET_VALUE,
 				null);
 			return child;
 		}
    //--Release2.1--//
    //// New link to toggle language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new session
    //// language id throughout the all modulus.
		else
		if (name.equals(CHILD_TOGGLELANGUAGEHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF_RESET_VALUE,
				null);
				return child;
    }
    //--DJ_CL_CR009--start--//
		else
    if (name.equals(CHILD_CKBDJPRODUCECOMMITMENT))
		{
			CheckBox child = new CheckBox(this,
				getDefaultModel(),
				CHILD_CKBDJPRODUCECOMMITMENT,
				CHILD_CKBDJPRODUCECOMMITMENT,
				"T","F",
				false,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDISPLAYDJCHECKBOXSTART))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STDISPLAYDJCHECKBOXSTART,
				CHILD_STDISPLAYDJCHECKBOXSTART,
				CHILD_STDISPLAYDJCHECKBOXSTART_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDISPLAYDJCHECKBOXEND))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STDISPLAYDJCHECKBOXEND,
				CHILD_STDISPLAYDJCHECKBOXEND,
				CHILD_STDISPLAYDJCHECKBOXEND_RESET_VALUE,
				null);
			return child;
    }
    //--DJ_CL_CR009--end--//
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
	    super.resetChildren();
		getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
		getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
		getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
		getHref1().setValue(CHILD_HREF1_RESET_VALUE);
		getHref2().setValue(CHILD_HREF2_RESET_VALUE);
		getHref3().setValue(CHILD_HREF3_RESET_VALUE);
		getHref4().setValue(CHILD_HREF4_RESET_VALUE);
		getHref5().setValue(CHILD_HREF5_RESET_VALUE);
		getHref6().setValue(CHILD_HREF6_RESET_VALUE);
		getHref7().setValue(CHILD_HREF7_RESET_VALUE);
		getHref8().setValue(CHILD_HREF8_RESET_VALUE);
		getHref9().setValue(CHILD_HREF9_RESET_VALUE);
		getHref10().setValue(CHILD_HREF10_RESET_VALUE);
		getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
		getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
		getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
		getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
		getStDealId().setValue(CHILD_STDEALID_RESET_VALUE);
		getStBorrFirstName().setValue(CHILD_STBORRFIRSTNAME_RESET_VALUE);
		getStDealStatus().setValue(CHILD_STDEALSTATUS_RESET_VALUE);
		getStDealStatusDate().setValue(CHILD_STDEALSTATUSDATE_RESET_VALUE);
		getStSourceFirm().setValue(CHILD_STSOURCEFIRM_RESET_VALUE);
		getStSource().setValue(CHILD_STSOURCE_RESET_VALUE);
		getStUnderwriterLastName().setValue(CHILD_STUNDERWRITERLASTNAME_RESET_VALUE);
		getStUnderwriterFirstName().setValue(CHILD_STUNDERWRITERFIRSTNAME_RESET_VALUE);
		getStUnderwriterMiddleInitial().setValue(CHILD_STUNDERWRITERMIDDLEINITIAL_RESET_VALUE);
		getStDealType().setValue(CHILD_STDEALTYPE_RESET_VALUE);
		getStDealPurpose().setValue(CHILD_STDEALPURPOSE_RESET_VALUE);
		getStTotalLoanAmount().setValue(CHILD_STTOTALLOANAMOUNT_RESET_VALUE);
		getStApplicationDate().setValue(CHILD_STAPPLICATIONDATE_RESET_VALUE);
		getStPmtTerm().setValue(CHILD_STPMTTERM_RESET_VALUE);
		getStEstClosingDate().setValue(CHILD_STESTCLOSINGDATE_RESET_VALUE);
		getStSpecialFeature().setValue(CHILD_STSPECIALFEATURE_RESET_VALUE);
		getBtSubmit().setValue(CHILD_BTSUBMIT_RESET_VALUE);
		getBtResolveDeal().setValue(CHILD_BTRESOLVEDEAL_RESET_VALUE);
		getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
		getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
		getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
		getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
		getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
		getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
		getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
		getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
		getBtCancel().setValue(CHILD_BTCANCEL_RESET_VALUE);
		getBtPrevTaskPage().setValue(CHILD_BTPREVTASKPAGE_RESET_VALUE);
		getStPrevTaskPageLabel().setValue(CHILD_STPREVTASKPAGELABEL_RESET_VALUE);
		getBtNextTaskPage().setValue(CHILD_BTNEXTTASKPAGE_RESET_VALUE);
		getStNextTaskPageLabel().setValue(CHILD_STNEXTTASKPAGELABEL_RESET_VALUE);
		getStTaskName().setValue(CHILD_STTASKNAME_RESET_VALUE);
		getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
		getBtStdAddCondition().setValue(CHILD_BTSTDADDCONDITION_RESET_VALUE);
		getBtCusAddCondition().setValue(CHILD_BTCUSADDCONDITION_RESET_VALUE);
		getLbStdCondition().setValue(CHILD_LBSTDCONDITION_RESET_VALUE);
		getLbCustCondition().setValue(CHILD_LBCUSTCONDITION_RESET_VALUE);
		getCbLanguagePreference().setValue(CHILD_CBLANGUAGEPREFERENCE_RESET_VALUE);
		getRepeated4().resetChildren();
		getRepeated1().resetChildren();
		getRepeated2().resetChildren();
		getRepeated3().resetChildren();
    //// SYNCADD.
 		getRepeated5().resetChildren();
		getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
		getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
		getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
		getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
		getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
		getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
		getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
		getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
		getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
		getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
		getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
		getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
		getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
		getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
		getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
		getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
		getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
		getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
		getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
		getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
		getHdBindDummy().setValue(CHILD_HDBINDDUMMY_RESET_VALUE);
		getBtOK().setValue(CHILD_BTOK_RESET_VALUE);
		getStAddStdButtonHTML().setValue(CHILD_STADDSTDBUTTONHTML_RESET_VALUE);
		getStAddCustButtonHTML().setValue(CHILD_STADDCUSTBUTTONHTML_RESET_VALUE);
		getStViewOnlyTag().setValue(CHILD_STVIEWONLYTAG_RESET_VALUE);
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> Test by BILLY 15July2002
    getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);
    //// SYNCADD.
 		getStTargetStdCon().setValue(CHILD_STTARGETSTDCON_RESET_VALUE);
 		getStTargetCustCon().setValue(CHILD_STTARGETCUSTCON_RESET_VALUE);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
		getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);
    //--DJ_CL_CR009--start--//
    getCkbDJProduceCommitment().setValue(CHILD_CKBDJPRODUCECOMMITMENT_RESET_VALUE);
    getStDisplayDJCheckBoxStart().setValue(CHILD_STDISPLAYDJCHECKBOXSTART_RESET_VALUE);
    getStDisplayDJCheckBoxEnd().setValue(CHILD_STDISPLAYDJCHECKBOXEND_RESET_VALUE);
    //--DJ_CL_CR009--end--//
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
	    super.registerChildren();
		registerChild(CHILD_TBDEALID,TextField.class);
		registerChild(CHILD_CBPAGENAMES,ComboBox.class);
		registerChild(CHILD_BTPROCEED,Button.class);
		registerChild(CHILD_HREF1,HREF.class);
		registerChild(CHILD_HREF2,HREF.class);
		registerChild(CHILD_HREF3,HREF.class);
		registerChild(CHILD_HREF4,HREF.class);
		registerChild(CHILD_HREF5,HREF.class);
		registerChild(CHILD_HREF6,HREF.class);
		registerChild(CHILD_HREF7,HREF.class);
		registerChild(CHILD_HREF8,HREF.class);
		registerChild(CHILD_HREF9,HREF.class);
		registerChild(CHILD_HREF10,HREF.class);
		registerChild(CHILD_STPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STCOMPANYNAME,StaticTextField.class);
		registerChild(CHILD_STTODAYDATE,StaticTextField.class);
		registerChild(CHILD_STUSERNAMETITLE,StaticTextField.class);
		registerChild(CHILD_STDEALID,StaticTextField.class);
		registerChild(CHILD_STBORRFIRSTNAME,StaticTextField.class);
		registerChild(CHILD_STDEALSTATUS,StaticTextField.class);
		registerChild(CHILD_STDEALSTATUSDATE,StaticTextField.class);
		registerChild(CHILD_STSOURCEFIRM,StaticTextField.class);
		registerChild(CHILD_STSOURCE,StaticTextField.class);
		registerChild(CHILD_STUNDERWRITERLASTNAME,StaticTextField.class);
		registerChild(CHILD_STUNDERWRITERFIRSTNAME,StaticTextField.class);
		registerChild(CHILD_STUNDERWRITERMIDDLEINITIAL,StaticTextField.class);
		registerChild(CHILD_STDEALTYPE,StaticTextField.class);
		registerChild(CHILD_STDEALPURPOSE,StaticTextField.class);
		registerChild(CHILD_STTOTALLOANAMOUNT,StaticTextField.class);
		registerChild(CHILD_STAPPLICATIONDATE,StaticTextField.class);
		registerChild(CHILD_STPMTTERM,StaticTextField.class);
		registerChild(CHILD_STESTCLOSINGDATE,StaticTextField.class);
		registerChild(CHILD_STSPECIALFEATURE,StaticTextField.class);
		registerChild(CHILD_BTSUBMIT,Button.class);
		registerChild(CHILD_BTRESOLVEDEAL,Button.class);
		registerChild(CHILD_BTWORKQUEUELINK,Button.class);
		registerChild(CHILD_CHANGEPASSWORDHREF,HREF.class);
		registerChild(CHILD_BTTOOLHISTORY,Button.class);
		registerChild(CHILD_BTTOONOTES,Button.class);
		registerChild(CHILD_BTTOOLSEARCH,Button.class);
		registerChild(CHILD_BTTOOLLOG,Button.class);
		registerChild(CHILD_STERRORFLAG,StaticTextField.class);
		registerChild(CHILD_DETECTALERTTASKS,HiddenField.class);
		registerChild(CHILD_BTCANCEL,Button.class);
		registerChild(CHILD_BTPREVTASKPAGE,Button.class);
		registerChild(CHILD_STPREVTASKPAGELABEL,StaticTextField.class);
		registerChild(CHILD_BTNEXTTASKPAGE,Button.class);
		registerChild(CHILD_STNEXTTASKPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STTASKNAME,StaticTextField.class);
		registerChild(CHILD_SESSIONUSERID,HiddenField.class);
		registerChild(CHILD_BTSTDADDCONDITION,Button.class);
		registerChild(CHILD_BTCUSADDCONDITION,Button.class);
		registerChild(CHILD_LBSTDCONDITION,ListBox.class);
		registerChild(CHILD_LBCUSTCONDITION,ListBox.class);
		registerChild(CHILD_CBLANGUAGEPREFERENCE,ComboBox.class);
		registerChild(CHILD_REPEATED4,pgCommitmentOfferRepeated4TiledView.class);
		registerChild(CHILD_REPEATED1,pgCommitmentOfferRepeated1TiledView.class);
		registerChild(CHILD_REPEATED2,pgCommitmentOfferRepeated2TiledView.class);
		registerChild(CHILD_REPEATED3,pgCommitmentOfferRepeated3TiledView.class);
    //// SYNCADD
 		registerChild(CHILD_REPEATED5,pgCommitmentOfferRepeated5TiledView.class);
		registerChild(CHILD_STPMGENERATE,StaticTextField.class);
		registerChild(CHILD_STPMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STPMHASINFO,StaticTextField.class);
		registerChild(CHILD_STPMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STPMHASOK,StaticTextField.class);
		registerChild(CHILD_STPMTITLE,StaticTextField.class);
		registerChild(CHILD_STPMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STPMONOK,StaticTextField.class);
		registerChild(CHILD_STPMMSGS,StaticTextField.class);
		registerChild(CHILD_STPMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMGENERATE,StaticTextField.class);
		registerChild(CHILD_STAMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STAMHASINFO,StaticTextField.class);
		registerChild(CHILD_STAMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STAMTITLE,StaticTextField.class);
		registerChild(CHILD_STAMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STAMMSGS,StaticTextField.class);
		registerChild(CHILD_STAMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMDIALOGMSG,StaticTextField.class);
		registerChild(CHILD_STAMBUTTONSHTML,StaticTextField.class);
		registerChild(CHILD_HDBINDDUMMY,HiddenField.class);
		registerChild(CHILD_BTOK,Button.class);
		registerChild(CHILD_STADDSTDBUTTONHTML,StaticTextField.class);
		registerChild(CHILD_STADDCUSTBUTTONHTML,StaticTextField.class);
		registerChild(CHILD_STVIEWONLYTAG,StaticTextField.class);
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> Test by BILLY 15July2002
    registerChild(CHILD_BTACTMSG,Button.class);
    //// SYNCADD.
 		registerChild(CHILD_STTARGETSTDCON,StaticTextField.class);
 		registerChild(CHILD_STTARGETCUSTCON,StaticTextField.class);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
		registerChild(CHILD_TOGGLELANGUAGEHREF,HREF.class);
    //--DJ_CL_CR009--start--//
    registerChild(CHILD_CKBDJPRODUCECOMMITMENT,CheckBox.class);
		registerChild(CHILD_STDISPLAYDJCHECKBOXSTART,StaticTextField.class);
		registerChild(CHILD_STDISPLAYDJCHECKBOXEND,StaticTextField.class);
    //--DJ_CL_CR009--end--//
 	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoUWDealSummarySnapShotModel());;modelList.add(getdoRowGeneratorModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
    ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By Billy 25Nov2002
    //Setup Options for cbPageNames
    //cbPageNamesOptions..populate(getRequestContext());
	int langId = handler.getTheSessionState().getLanguageId();
    cbPageNamesOptions.populate(getRequestContext(), langId);

    //Setup NoneSelected Label for cbPageNames
    CHILD_CBPAGENAMES_NONSELECTED_LABEL =
        BXResources.getGenericMsg("CBPAGENAMES_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    //Check if the cbPageNames is already created
    if(getCbPageNames() != null)
      getCbPageNames().setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);

    //Populate all other ComboBoxes
    lbStdConditionOptions.populate(getRequestContext());

    lbCustConditionOptions.populate(getRequestContext());
    //Setup NoneSelected Label for cbPageNames
    CHILD_LBCUSTCONDITION_NONSELECTED_LABEL = BXResources.getGenericMsg("LBCUSTCONDITION_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    //Check if the cbPageNames is already created
    if(getLbCustCondition() != null)
      getLbCustCondition().setLabelForNoneSelected(CHILD_LBCUSTCONDITION_NONSELECTED_LABEL);

    cbLanguagePreferenceOptions.populate(getRequestContext());

    handler.pageSaveState();

		super.beginDisplay(event);
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// The following code block was migrated from the this_onBeforeDataObjectExecuteEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		handler.setupBeforePageGeneration();
		handler.pageSaveState();

		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

		// The following code block was migrated from the this_onBeforeRowDisplayEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		handler.populatePageDisplayFields();
		handler.pageSaveState();
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics this_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);
	}


	/**
	 *
	 *
	 */
	public TextField getTbDealId()
	{
		return (TextField)getChild(CHILD_TBDEALID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPageNames()
	{
		return (ComboBox)getChild(CHILD_CBPAGENAMES);
	}

	/**
	 *
	 *
	 */
  //--DJ_Ticket661--start--//
  //Modified to sort by PageLabel based on the selected language
  static class CbPageNamesOptionList extends GotoOptionList
 {
    /**
     *
     *
     */
    CbPageNamesOptionList() {

    }


    public void populate(RequestContext rc) {
      Connection c = null;
      try {
        clear();
        SelectQueryModel m = null;

        SelectQueryExecutionContext eContext = new
          SelectQueryExecutionContext(
          (Connection)null,

          DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,

          DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null) {
          m = new doPageNameLabelModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else {
          m = (SelectQueryModel)rc.getModelManager().getModel(doPageNameLabelModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        // Sort the results from DataObject
        TreeMap sorted = new TreeMap();
        while (m.next()) {
          Object dfPageLabel =
            m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
          String label = (dfPageLabel == null ? "" : dfPageLabel.toString());
          Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
          String value = (dfPageId == null ? "" : dfPageId.toString());
          String[] theVal = new String[2];
          theVal[0] = label;
          theVal[1] = value;
          sorted.put(label, theVal);
        }

        // Set the sorted list to the optionlist
        Iterator theList = sorted.values().iterator();
        String[] theVal = new String[2];
        while (theList.hasNext()) {
          theVal = (String[]) (theList.next());
          add(theVal[0], theVal[1]);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      finally {
        try {
          if (c != null)
            c.close();
        }
        catch (SQLException ex) {
              // ignore
        }
      }
    }
 }
 //--DJ_Ticket661--end--//

	/**
	 *
	 *
	 */
	public void handleBtProceedRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btProceed_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleGoPage();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtProceed()
	{
		return (Button)getChild(CHILD_BTPROCEED);
	}


	/**
	 *
	 *
	 */
	public void handleHref1Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href1_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(0);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref1()
	{
		return (HREF)getChild(CHILD_HREF1);
	}


	/**
	 *
	 *
	 */
	public void handleHref2Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href2_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(1);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref2()
	{
		return (HREF)getChild(CHILD_HREF2);
	}


	/**
	 *
	 *
	 */
	public void handleHref3Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href3_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(2);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref3()
	{
		return (HREF)getChild(CHILD_HREF3);
	}


	/**
	 *
	 *
	 */
	public void handleHref4Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href4_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(3);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref4()
	{
		return (HREF)getChild(CHILD_HREF4);
	}


	/**
	 *
	 *
	 */
	public void handleHref5Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href5_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(4);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref5()
	{
		return (HREF)getChild(CHILD_HREF5);
	}


	/**
	 *
	 *
	 */
	public void handleHref6Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href6_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(5);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref6()
	{
		return (HREF)getChild(CHILD_HREF6);
	}


	/**
	 *
	 *
	 */
	public void handleHref7Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href7_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(6);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref7()
	{
		return (HREF)getChild(CHILD_HREF7);
	}


	/**
	 *
	 *
	 */
	public void handleHref8Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href8_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(7);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref8()
	{
		return (HREF)getChild(CHILD_HREF8);
	}


	/**
	 *
	 *
	 */
	public void handleHref9Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href9_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(8);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref9()
	{
		return (HREF)getChild(CHILD_HREF9);
	}


	/**
	 *
	 *
	 */
	public void handleHref10Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href10_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(9);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref10()
	{
		return (HREF)getChild(CHILD_HREF10);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCompanyName()
	{
		return (StaticTextField)getChild(CHILD_STCOMPANYNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTodayDate()
	{
		return (StaticTextField)getChild(CHILD_STTODAYDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUserNameTitle()
	{
		return (StaticTextField)getChild(CHILD_STUSERNAMETITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealId()
	{
		return (StaticTextField)getChild(CHILD_STDEALID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBorrFirstName()
	{
		return (StaticTextField)getChild(CHILD_STBORRFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealStatus()
	{
		return (StaticTextField)getChild(CHILD_STDEALSTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealStatusDate()
	{
		return (StaticTextField)getChild(CHILD_STDEALSTATUSDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSourceFirm()
	{
		return (StaticTextField)getChild(CHILD_STSOURCEFIRM);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSource()
	{
		return (StaticTextField)getChild(CHILD_STSOURCE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUnderwriterLastName()
	{
		return (StaticTextField)getChild(CHILD_STUNDERWRITERLASTNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUnderwriterFirstName()
	{
		return (StaticTextField)getChild(CHILD_STUNDERWRITERFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUnderwriterMiddleInitial()
	{
		return (StaticTextField)getChild(CHILD_STUNDERWRITERMIDDLEINITIAL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealType()
	{
		return (StaticTextField)getChild(CHILD_STDEALTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealPurpose()
	{
		return (StaticTextField)getChild(CHILD_STDEALPURPOSE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTotalLoanAmount()
	{
		return (StaticTextField)getChild(CHILD_STTOTALLOANAMOUNT);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApplicationDate()
	{
		return (StaticTextField)getChild(CHILD_STAPPLICATIONDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmtTerm()
	{
		return (StaticTextField)getChild(CHILD_STPMTTERM);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEstClosingDate()
	{
		return (StaticTextField)getChild(CHILD_STESTCLOSINGDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSpecialFeature()
	{
		return (StaticTextField)getChild(CHILD_STSPECIALFEATURE);
	}


	/**
	 *
	 *
	 */
	public void handleBtSubmitRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btSubmit_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		// ALERT - no warn
		handler.handleSubmit();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtSubmit()
	{
		return (Button)getChild(CHILD_BTSUBMIT);
	}


	/**
	 *
	 *
	 */
	public String endBtSubmitDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btSubmit_onBeforeHtmlOutputEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.displaySubmitButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public void handleBtResolveDealRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btResolveDeal_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		// ALERT - no warn
		handler.handleSubmit(true);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtResolveDeal()
	{
		return (Button)getChild(CHILD_BTRESOLVEDEAL);
	}


	/**
	 *
	 *
	 */
	public String endBtResolveDealDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btResolveDeal_onBeforeHtmlOutputEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.displayResolveDealButton();
		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btWorkQueueLink_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleDisplayWorkQueue();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtWorkQueueLink()
	{
		return (Button)getChild(CHILD_BTWORKQUEUELINK);
	}


	/**
	 *
	 *
	 */
	public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the changePasswordHref_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleChangePassword();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getChangePasswordHref()
	{
		return (HREF)getChild(CHILD_CHANGEPASSWORDHREF);
	}

  //--Release2.1--start//
  //// Two new methods providing the business logic for the new link to toggle language.
  //// When touched this link should reload the page in opposite language (french versus english)
  //// and set this new session value.
	/**
	 *
	 *
	 */
	public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

	    _logger.debug("Web event invoked: "+getClass().getName()+".ToggleLanguageHref");

		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this, true);

		handler.handleToggleLanguage();

		handler.postHandlerProtocol();
	}

	/**
	 *
	 *
	 */
	public HREF getToggleLanguageHref()
	{
		return (HREF)getChild(CHILD_TOGGLELANGUAGEHREF);
	}

  //--Release2.1--end//
	/**
	 *
	 *
	 */
	public void handleBtToolHistoryRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btToolHistory_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleDisplayDealHistory();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtToolHistory()
	{
		return (Button)getChild(CHILD_BTTOOLHISTORY);
	}


	/**
	 *
	 *
	 */
	public void handleBtTooNotesRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btTooNotes_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleDisplayDealNotes();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtTooNotes()
	{
		return (Button)getChild(CHILD_BTTOONOTES);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolSearchRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btToolSearch_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleDealSearch();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtToolSearch()
	{
		return (Button)getChild(CHILD_BTTOOLSEARCH);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolLogRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btToolLog_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleSignOff();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtToolLog()
	{
		return (Button)getChild(CHILD_BTTOOLLOG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStErrorFlag()
	{
		return (StaticTextField)getChild(CHILD_STERRORFLAG);
	}


	/**
	 *
	 *
	 */
	public HiddenField getDetectAlertTasks()
	{
		return (HiddenField)getChild(CHILD_DETECTALERTTASKS);
	}


	/**
	 *
	 *
	 */
	public void handleBtCancelRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btCancel_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		// ALERT - no warn: handler.handleCancelStandard(false);
		handler.handleCancelStandard();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtCancel()
	{
		return (Button)getChild(CHILD_BTCANCEL);
	}


	/**
	 *
	 *
	 */
	public String endBtCancelDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btCancel_onBeforeHtmlOutputEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.displayCancelButton();
		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public void handleBtPrevTaskPageRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btPrevTaskPage_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handlePrevTaskPage();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtPrevTaskPage()
	{
		return (Button)getChild(CHILD_BTPREVTASKPAGE);
	}


	/**
	 *
	 *
	 */
	public String endBtPrevTaskPageDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btPrevTaskPage_onBeforeHtmlOutputEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.generatePrevTaskPage();
		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPrevTaskPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPREVTASKPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public String endStPrevTaskPageLabelDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stPrevTaskPageLabel_onBeforeHtmlOutputEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.generatePrevTaskPage();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public void handleBtNextTaskPageRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btNextTaskPage_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleNextTaskPage();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtNextTaskPage()
	{
		return (Button)getChild(CHILD_BTNEXTTASKPAGE);
	}


	/**
	 *
	 *
	 */
	public String endBtNextTaskPageDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btNextTaskPage_onBeforeHtmlOutputEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.generateNextTaskPage();
		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStNextTaskPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STNEXTTASKPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public String endStNextTaskPageLabelDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stNextTaskPageLabel_onBeforeHtmlOutputEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.generateNextTaskPage();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTaskName()
	{
		return (StaticTextField)getChild(CHILD_STTASKNAME);
	}


	/**
	 *
	 *
	 */
	public String endStTaskNameDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stTaskName_onBeforeHtmlOutputEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.generateTaskName();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public HiddenField getSessionUserId()
	{
		return (HiddenField)getChild(CHILD_SESSIONUSERID);
	}


	/**
	 *
	 *
	 */
	public void handleBtStdAddConditionRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btStdAddCondition_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleAdd("btStdAddCondition");
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtStdAddCondition()
	{
		return (Button)getChild(CHILD_BTSTDADDCONDITION);
	}


	/**
	 *
	 *
	 */
	public String endBtStdAddConditionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btStdAddCondition_onBeforeHtmlOutputEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public void handleBtCusAddConditionRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btCusAddCondition_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleAdd("btCusAddCondition");
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtCusAddCondition()
	{
		return (Button)getChild(CHILD_BTCUSADDCONDITION);
	}


	/**
	 *
	 *
	 */
	public String endBtCusAddConditionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btCusAddCondition_onBeforeHtmlOutputEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public ListBox getLbStdCondition()
	{
		return (ListBox)getChild(CHILD_LBSTDCONDITION);
	}

	/**
	 *
	 *
	 */
	static class LbStdConditionOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		LbStdConditionOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
			Connection c = null;
			try {
				clear();
				SelectQueryModel m = null;

				SelectQueryExecutionContext eContext=new SelectQueryExecutionContext(
					(Connection)null,
					DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
					DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

				if(rc == null) {
					m = new doPickConditionModelImpl();
					c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
					eContext.setConnection(c);
				}
				else {
					m = (SelectQueryModel) rc.getModelManager().getModel(doPickConditionModel.class);
				}

        //--Release2.1--//
        //--> Setup LanguagePreference Criteria -- By Billy 26Nov2002
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
              SessionStateModel.class, defaultInstanceStateName, true);
        int languageId = theSessionState.getLanguageId();

        ((doPickConditionModelImpl)m).setStaticWhereCriteriaString(doPickConditionModelImpl.STATIC_WHERE_CRITERIA
              + " AND CONDITIONVERBIAGE.LANGUAGEPREFERENCEID = " + languageId);
        //==============================================================

				m.retrieve(eContext);
				m.beforeFirst();

				while (m.next()) {
					Object dfConditionId = m.getValue(doPickConditionModel.FIELD_DFCONDITIONID);
					Object dfDescription = m.getValue(doPickConditionModel.FIELD_DFDESCRIPTION);

					String label = (dfDescription == null?"":dfDescription.toString());

					String value = (dfConditionId == null?"":dfConditionId.toString());

					add(label, value);
				}
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			finally {
				try {
					if(c != null)
						c.close();
				}
				catch (SQLException  ex) {
					// ignore
				}
			}
		}

	}



	/**
	 *
	 *
	 */
	public ListBox getLbCustCondition()
	{
		return (ListBox)getChild(CHILD_LBCUSTCONDITION);
	}

	/**
	 *
	 *
	 */
	static class LbCustConditionOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		LbCustConditionOptionList()
		{

		}


		/**
		 *
		 *
		 */
    public void populate(RequestContext rc)
		{
			Connection c = null;
			try {
				clear();
				SelectQueryModel m = null;

				SelectQueryExecutionContext eContext=new SelectQueryExecutionContext(
					(Connection)null,
					DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
					DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

				if(rc == null) {
					m = new doPickConditionModelImpl();
					c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
					eContext.setConnection(c);
				}
				else {
					m = (SelectQueryModel) rc.getModelManager().getModel(doPickConditionModel.class);
				}

        //--Release2.1--//
        //--> Setup LanguagePreference Criteria -- By Billy 26Nov2002
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
              SessionStateModel.class, defaultInstanceStateName, true);
        int languageId = theSessionState.getLanguageId();

        ((doPickConditionModelImpl)m).setStaticWhereCriteriaString(doPickConditionModelImpl.STATIC_WHERE_CRITERIA
              + " AND CONDITIONVERBIAGE.LANGUAGEPREFERENCEID = " + languageId);
        //==============================================================

				m.retrieve(eContext);
				m.beforeFirst();

				while (m.next()) {
					Object dfConditionId = m.getValue(doPickConditionModel.FIELD_DFCONDITIONID);
					Object dfDescription = m.getValue(doPickConditionModel.FIELD_DFDESCRIPTION);

					String label = (dfDescription == null?"":dfDescription.toString());

					String value = (dfConditionId == null?"":dfConditionId.toString());

					add(label, value);
				}
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			finally {
				try {
					if(c != null)
						c.close();
				}
				catch (SQLException  ex) {
					// ignore
				}
			}
		}

	}



	/**
	 *
	 *
	 */
	public ComboBox getCbLanguagePreference()
	{
		return (ComboBox)getChild(CHILD_CBLANGUAGEPREFERENCE);
	}

	/**
	 *
	 *
	 */
	static class CbLanguagePreferenceOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbLanguagePreferenceOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 22Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection<String[]> c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                            "LANGUAGEPREFERENCE", languageId);
        Iterator<String[]> l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public pgCommitmentOfferRepeated4TiledView getRepeated4()
	{
		return (pgCommitmentOfferRepeated4TiledView)getChild(CHILD_REPEATED4);
	}


	/**
	 *
	 *
	 */
	public pgCommitmentOfferRepeated1TiledView getRepeated1()
	{
		return (pgCommitmentOfferRepeated1TiledView)getChild(CHILD_REPEATED1);
	}


	/**
	 *
	 *
	 */
	public pgCommitmentOfferRepeated2TiledView getRepeated2()
	{
		return (pgCommitmentOfferRepeated2TiledView)getChild(CHILD_REPEATED2);
	}


	/**
	 *
	 *
	 */
	public pgCommitmentOfferRepeated3TiledView getRepeated3()
	{
		return (pgCommitmentOfferRepeated3TiledView)getChild(CHILD_REPEATED3);
	}

	/**
	 *
	 * SYNCADD.
	 */
 	public pgCommitmentOfferRepeated5TiledView getRepeated5()
 	{
 		return (pgCommitmentOfferRepeated5TiledView)getChild(CHILD_REPEATED5);
  }

	/**
	 *
	 *
	 */
	public StaticTextField getStPmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STPMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STPMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasOk()
	{
		return (StaticTextField)getChild(CHILD_STPMHASOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STPMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmOnOk()
	{
		return (StaticTextField)getChild(CHILD_STPMONOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STAMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STAMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmDialogMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMDIALOGMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmButtonsHtml()
	{
		return (StaticTextField)getChild(CHILD_STAMBUTTONSHTML);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdBindDummy()
	{
		return (HiddenField)getChild(CHILD_HDBINDDUMMY);
	}

	/**
	 *
	 *  SYNCADD method.
	 */
 	public StaticTextField getStTargetStdCon()
 	{
 		return (StaticTextField)getChild(CHILD_STTARGETSTDCON);
  }

	/**
	 *
	 *  SYNCADD method.
	 */
 	public StaticTextField getStTargetCustCon()
 	{
 		return (StaticTextField)getChild(CHILD_STTARGETCUSTCON);
  }

  //--> These were missed from the pervious merge
  //--> By Billy 26Nov2002
  public String endStTargetCustConDisplay(ChildContentDisplayEvent event)
	{
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.generateLoadTarget(Mc.TARGET_CONREVIEW_CUST);
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}

  public String endStTargetStdConDisplay(ChildContentDisplayEvent event)
	{
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.generateLoadTarget(Mc.TARGET_CONREVIEW_STD);
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}
  //===========================================================

	/**
	 *
	 *
	 */
	public void handleBtOKRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btOK_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleCancelStandard();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtOK()
	{
		return (Button)getChild(CHILD_BTOK);
	}


	/**
	 *
	 *
	 */
	public String endBtOKDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btOK_onBeforeHtmlOutputEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.displayOKButton();
		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAddStdButtonHTML()
	{
		return (StaticTextField)getChild(CHILD_STADDSTDBUTTONHTML);
	}


	/**
	 *
	 *
	 */
	public String endStAddStdButtonHTMLDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stAddStdButtonHTML_onBeforeDisplayEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		String rc = handler.setAddStdConditionButtonHTML();
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAddCustButtonHTML()
	{
		return (StaticTextField)getChild(CHILD_STADDCUSTBUTTONHTML);
	}


	/**
	 *
	 *
	 */
	public String endStAddCustButtonHTMLDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stAddCustButtonHTML_onBeforeDisplayEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		String rc = handler.setAddCustConditionButtonHTML();
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStViewOnlyTag()
	{
		return (StaticTextField)getChild(CHILD_STVIEWONLYTAG);
	}


	/**
	 *
	 *
	 */
	public String endStViewOnlyTagDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stViewOnlyTag_onBeforeHtmlOutputEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		String rc = handler.displayViewOnlyTag();
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public doUWDealSummarySnapShotModel getdoUWDealSummarySnapShotModel()
	{
		if (doUWDealSummarySnapShot == null)
			doUWDealSummarySnapShot = (doUWDealSummarySnapShotModel) getModel(doUWDealSummarySnapShotModel.class);
		return doUWDealSummarySnapShot;
	}


	/**
	 *
	 *
	 */
	public void setdoUWDealSummarySnapShotModel(doUWDealSummarySnapShotModel model)
	{
			doUWDealSummarySnapShot = model;
	}


	/**
	 *
	 *
	 */
	public doRowGeneratorModel getdoRowGeneratorModel()
	{
		if (doRowGenerator == null)
			doRowGenerator = (doRowGeneratorModel) getModel(doRowGeneratorModel.class);
		return doRowGenerator;
	}


	/**
	 *
	 *
	 */
	public void setdoRowGeneratorModel(doRowGeneratorModel model)
	{
			doRowGenerator = model;
	}


  //--> Hidden ActMessageButton (FINDTHISLATER)
  //--> Test by BILLY 15July2002
  public Button getBtActMsg()
	{
		return (Button)getChild(CHILD_BTACTMSG);
	}
  //===========================================

  //--> Addition methods to propulate Href display String
  //--> Test by BILLY 07Aug2002
  public String endHref1Display(ChildContentDisplayEvent event)
	{
    ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 0);
		handler.pageSaveState();

    return rc;
  }
  public String endHref2Display(ChildContentDisplayEvent event)
	{
    ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 1);
		handler.pageSaveState();

    return rc;
  }
  public String endHref3Display(ChildContentDisplayEvent event)
	{
    ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 2);
		handler.pageSaveState();

    return rc;
  }
  public String endHref4Display(ChildContentDisplayEvent event)
	{
    ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 3);
		handler.pageSaveState();

    return rc;
  }
  public String endHref5Display(ChildContentDisplayEvent event)
	{
    ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 4);
		handler.pageSaveState();

    return rc;
  }
  public String endHref6Display(ChildContentDisplayEvent event)
	{
    ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 5);
		handler.pageSaveState();

    return rc;
  }
  public String endHref7Display(ChildContentDisplayEvent event)
	{
    ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 6);
		handler.pageSaveState();

    return rc;
  }
  public String endHref8Display(ChildContentDisplayEvent event)
	{
    ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 7);
		handler.pageSaveState();

    return rc;
  }
  public String endHref9Display(ChildContentDisplayEvent event)
	{
    ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 8);
		handler.pageSaveState();

    return rc;
  }
  public String endHref10Display(ChildContentDisplayEvent event)
	{
    ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 9);
		handler.pageSaveState();

    return rc;
  }
  //=====================================================

  //--DJ_CL_CR009--start--//
  public CheckBox getCkbDJProduceCommitment()
  {
    return (CheckBox)getChild(CHILD_CKBDJPRODUCECOMMITMENT);
  }

	/**
	 *
	 *
	 */
	public StaticTextField getStDisplayDJCheckBoxStart()
	{
		return (StaticTextField)getChild(CHILD_STDISPLAYDJCHECKBOXSTART);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStDisplayDJCheckBoxEnd()
	{
		return (StaticTextField)getChild(CHILD_STDISPLAYDJCHECKBOXEND);
	}
  //--DJ_CL_CR009--end--//

  //// This method overrides the getDefaultURL() framework JATO method and it is located
  //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
  //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
  //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
  //// The full method is still in PHC base class. It should care the
  //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
  public String getDisplayURL()
  {
       ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
       handler.pageGetState(this);

       String url=getDefaultDisplayURL();

       int languageId = handler.theSessionState.getLanguageId();

       //// Call the language specific URL (business delegation done in the BXResource).
       if(url != null && !url.trim().equals("") && !url.trim().equals("/")) {
          url = BXResources.getBXUrl(url, languageId);
       }
       else {
          url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
       }

      return url;
  }

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	public void handleActMessageOK(String[] args)
	{
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleActMessageOK(args);
		handler.postHandlerProtocol();
	}


	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String PAGE_NAME="pgCommitmentOffer";
        //// It is a variable now (see explanation in the getDisplayURL() method above.
	////public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgCommitmentOffer.jsp";
	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_TBDEALID="tbDealId";
	public static final String CHILD_TBDEALID_RESET_VALUE="";
	public static final String CHILD_CBPAGENAMES="cbPageNames";
	public static final String CHILD_CBPAGENAMES_RESET_VALUE="";
  //--Release2.1--//
	//private static CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  private CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  //Added the Variable for NonSelected Label
  private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";
  //==================================================================
	public static final String CHILD_BTPROCEED="btProceed";
	public static final String CHILD_BTPROCEED_RESET_VALUE="Proceed";
	public static final String CHILD_HREF1="Href1";
	public static final String CHILD_HREF1_RESET_VALUE="";
	public static final String CHILD_HREF2="Href2";
	public static final String CHILD_HREF2_RESET_VALUE="";
	public static final String CHILD_HREF3="Href3";
	public static final String CHILD_HREF3_RESET_VALUE="";
	public static final String CHILD_HREF4="Href4";
	public static final String CHILD_HREF4_RESET_VALUE="";
	public static final String CHILD_HREF5="Href5";
	public static final String CHILD_HREF5_RESET_VALUE="";
	public static final String CHILD_HREF6="Href6";
	public static final String CHILD_HREF6_RESET_VALUE="";
	public static final String CHILD_HREF7="Href7";
	public static final String CHILD_HREF7_RESET_VALUE="";
	public static final String CHILD_HREF8="Href8";
	public static final String CHILD_HREF8_RESET_VALUE="";
	public static final String CHILD_HREF9="Href9";
	public static final String CHILD_HREF9_RESET_VALUE="";
	public static final String CHILD_HREF10="Href10";
	public static final String CHILD_HREF10_RESET_VALUE="";
	public static final String CHILD_STPAGELABEL="stPageLabel";
	public static final String CHILD_STPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STCOMPANYNAME="stCompanyName";
	public static final String CHILD_STCOMPANYNAME_RESET_VALUE="";
	public static final String CHILD_STTODAYDATE="stTodayDate";
	public static final String CHILD_STTODAYDATE_RESET_VALUE="";
	public static final String CHILD_STUSERNAMETITLE="stUserNameTitle";
	public static final String CHILD_STUSERNAMETITLE_RESET_VALUE="";
	public static final String CHILD_STDEALID="stDealId";
	public static final String CHILD_STDEALID_RESET_VALUE="";
	public static final String CHILD_STBORRFIRSTNAME="stBorrFirstName";
	public static final String CHILD_STBORRFIRSTNAME_RESET_VALUE="";
	public static final String CHILD_STDEALSTATUS="stDealStatus";
	public static final String CHILD_STDEALSTATUS_RESET_VALUE="";
	public static final String CHILD_STDEALSTATUSDATE="stDealStatusDate";
	public static final String CHILD_STDEALSTATUSDATE_RESET_VALUE="";
	public static final String CHILD_STSOURCEFIRM="stSourceFirm";
	public static final String CHILD_STSOURCEFIRM_RESET_VALUE="";
	public static final String CHILD_STSOURCE="stSource";
	public static final String CHILD_STSOURCE_RESET_VALUE="";
	public static final String CHILD_STUNDERWRITERLASTNAME="stUnderwriterLastName";
	public static final String CHILD_STUNDERWRITERLASTNAME_RESET_VALUE="";
	public static final String CHILD_STUNDERWRITERFIRSTNAME="stUnderwriterFirstName";
	public static final String CHILD_STUNDERWRITERFIRSTNAME_RESET_VALUE="";
	public static final String CHILD_STUNDERWRITERMIDDLEINITIAL="stUnderwriterMiddleInitial";
	public static final String CHILD_STUNDERWRITERMIDDLEINITIAL_RESET_VALUE="";
	public static final String CHILD_STDEALTYPE="stDealType";
	public static final String CHILD_STDEALTYPE_RESET_VALUE="";
	public static final String CHILD_STDEALPURPOSE="stDealPurpose";
	public static final String CHILD_STDEALPURPOSE_RESET_VALUE="";
	public static final String CHILD_STTOTALLOANAMOUNT="stTotalLoanAmount";
	public static final String CHILD_STTOTALLOANAMOUNT_RESET_VALUE="";
	public static final String CHILD_STAPPLICATIONDATE="stApplicationDate";
	public static final String CHILD_STAPPLICATIONDATE_RESET_VALUE="";
	public static final String CHILD_STPMTTERM="stPmtTerm";
	public static final String CHILD_STPMTTERM_RESET_VALUE="";
	public static final String CHILD_STESTCLOSINGDATE="stEstClosingDate";
	public static final String CHILD_STESTCLOSINGDATE_RESET_VALUE="";
	public static final String CHILD_STSPECIALFEATURE="stSpecialFeature";
	public static final String CHILD_STSPECIALFEATURE_RESET_VALUE="";
	public static final String CHILD_BTSUBMIT="btSubmit";
	public static final String CHILD_BTSUBMIT_RESET_VALUE=" ";
	public static final String CHILD_BTRESOLVEDEAL="btResolveDeal";
	public static final String CHILD_BTRESOLVEDEAL_RESET_VALUE=" ";
	public static final String CHILD_BTWORKQUEUELINK="btWorkQueueLink";
	public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE=" ";
	public static final String CHILD_CHANGEPASSWORDHREF="changePasswordHref";
	public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE="";
	public static final String CHILD_BTTOOLHISTORY="btToolHistory";
	public static final String CHILD_BTTOOLHISTORY_RESET_VALUE=" ";
	public static final String CHILD_BTTOONOTES="btTooNotes";
	public static final String CHILD_BTTOONOTES_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLSEARCH="btToolSearch";
	public static final String CHILD_BTTOOLSEARCH_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLLOG="btToolLog";
	public static final String CHILD_BTTOOLLOG_RESET_VALUE=" ";
	public static final String CHILD_STERRORFLAG="stErrorFlag";
	public static final String CHILD_STERRORFLAG_RESET_VALUE="N";
	public static final String CHILD_DETECTALERTTASKS="detectAlertTasks";
	public static final String CHILD_DETECTALERTTASKS_RESET_VALUE="";
	public static final String CHILD_BTCANCEL="btCancel";
	public static final String CHILD_BTCANCEL_RESET_VALUE=" ";
	public static final String CHILD_BTPREVTASKPAGE="btPrevTaskPage";
	public static final String CHILD_BTPREVTASKPAGE_RESET_VALUE=" ";
	public static final String CHILD_STPREVTASKPAGELABEL="stPrevTaskPageLabel";
	public static final String CHILD_STPREVTASKPAGELABEL_RESET_VALUE="";
	public static final String CHILD_BTNEXTTASKPAGE="btNextTaskPage";
	public static final String CHILD_BTNEXTTASKPAGE_RESET_VALUE=" ";
	public static final String CHILD_STNEXTTASKPAGELABEL="stNextTaskPageLabel";
	public static final String CHILD_STNEXTTASKPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STTASKNAME="stTaskName";
	public static final String CHILD_STTASKNAME_RESET_VALUE="";
	public static final String CHILD_SESSIONUSERID="sessionUserId";
	public static final String CHILD_SESSIONUSERID_RESET_VALUE="";
	public static final String CHILD_BTSTDADDCONDITION="btStdAddCondition";
	public static final String CHILD_BTSTDADDCONDITION_RESET_VALUE=" ";
	public static final String CHILD_BTCUSADDCONDITION="btCusAddCondition";
	public static final String CHILD_BTCUSADDCONDITION_RESET_VALUE=" ";
	public static final String CHILD_LBSTDCONDITION="lbStdCondition";
	public static final String CHILD_LBSTDCONDITION_RESET_VALUE="";
  //--Release2.1--//
	//private static LbStdConditionOptionList lbStdConditionOptions=new LbStdConditionOptionList();
  private LbStdConditionOptionList lbStdConditionOptions=new LbStdConditionOptionList();
	public static final String CHILD_LBCUSTCONDITION="lbCustCondition";
	public static final String CHILD_LBCUSTCONDITION_RESET_VALUE="0";
  //--Release2.1--//
	//private static LbCustConditionOptionList lbCustConditionOptions=new LbCustConditionOptionList();
  private LbCustConditionOptionList lbCustConditionOptions=new LbCustConditionOptionList();
  //Added the Variable for NonSelected Label
  private String CHILD_LBCUSTCONDITION_NONSELECTED_LABEL = "";
  //============================================================
	public static final String CHILD_CBLANGUAGEPREFERENCE="cbLanguagePreference";
	public static final String CHILD_CBLANGUAGEPREFERENCE_RESET_VALUE="";
  //--Release2.1--//
	//private static CbLanguagePreferenceOptionList cbLanguagePreferenceOptions=new CbLanguagePreferenceOptionList();
  private CbLanguagePreferenceOptionList cbLanguagePreferenceOptions=new CbLanguagePreferenceOptionList();
	public static final String CHILD_REPEATED4="Repeated4";
	public static final String CHILD_REPEATED1="Repeated1";
	public static final String CHILD_REPEATED2="Repeated2";
	public static final String CHILD_REPEATED3="Repeated3";
	public static final String CHILD_STPMGENERATE="stPmGenerate";
	public static final String CHILD_STPMGENERATE_RESET_VALUE="";
	public static final String CHILD_STPMHASTITLE="stPmHasTitle";
	public static final String CHILD_STPMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STPMHASINFO="stPmHasInfo";
	public static final String CHILD_STPMHASINFO_RESET_VALUE="";
	public static final String CHILD_STPMHASTABLE="stPmHasTable";
	public static final String CHILD_STPMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STPMHASOK="stPmHasOk";
	public static final String CHILD_STPMHASOK_RESET_VALUE="";
	public static final String CHILD_STPMTITLE="stPmTitle";
	public static final String CHILD_STPMTITLE_RESET_VALUE="";
	public static final String CHILD_STPMINFOMSG="stPmInfoMsg";
	public static final String CHILD_STPMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STPMONOK="stPmOnOk";
	public static final String CHILD_STPMONOK_RESET_VALUE="";
	public static final String CHILD_STPMMSGS="stPmMsgs";
	public static final String CHILD_STPMMSGS_RESET_VALUE="";
	public static final String CHILD_STPMMSGTYPES="stPmMsgTypes";
	public static final String CHILD_STPMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMGENERATE="stAmGenerate";
	public static final String CHILD_STAMGENERATE_RESET_VALUE="";
	public static final String CHILD_STAMHASTITLE="stAmHasTitle";
	public static final String CHILD_STAMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STAMHASINFO="stAmHasInfo";
	public static final String CHILD_STAMHASINFO_RESET_VALUE="";
	public static final String CHILD_STAMHASTABLE="stAmHasTable";
	public static final String CHILD_STAMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STAMTITLE="stAmTitle";
	public static final String CHILD_STAMTITLE_RESET_VALUE="";
	public static final String CHILD_STAMINFOMSG="stAmInfoMsg";
	public static final String CHILD_STAMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STAMMSGS="stAmMsgs";
	public static final String CHILD_STAMMSGS_RESET_VALUE="";
	public static final String CHILD_STAMMSGTYPES="stAmMsgTypes";
	public static final String CHILD_STAMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMDIALOGMSG="stAmDialogMsg";
	public static final String CHILD_STAMDIALOGMSG_RESET_VALUE="";
	public static final String CHILD_STAMBUTTONSHTML="stAmButtonsHtml";
	public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE="";
	public static final String CHILD_HDBINDDUMMY="hdBindDummy";
	public static final String CHILD_HDBINDDUMMY_RESET_VALUE="";
	public static final String CHILD_BTOK="btOK";
	public static final String CHILD_BTOK_RESET_VALUE="OK";
	public static final String CHILD_STADDSTDBUTTONHTML="stAddStdButtonHTML";
	public static final String CHILD_STADDSTDBUTTONHTML_RESET_VALUE="";
	public static final String CHILD_STADDCUSTBUTTONHTML="stAddCustButtonHTML";
	public static final String CHILD_STADDCUSTBUTTONHTML_RESET_VALUE="";
	public static final String CHILD_STVIEWONLYTAG="stViewOnlyTag";
	public static final String CHILD_STVIEWONLYTAG_RESET_VALUE="";
  //--> Hidden ActMessageButton (FINDTHISLATER)
  //--> Test by BILLY 15July2002
  public static final String CHILD_BTACTMSG="btActMsg";
	public static final String CHILD_BTACTMSG_RESET_VALUE="ActMsg";
  //// SYNCADD
  public static final String CHILD_REPEATED5="Repeated5";
 	public static final String CHILD_STTARGETSTDCON="stTargetStdCon";
 	public static final String CHILD_STTARGETSTDCON_RESET_VALUE="";
 	public static final String CHILD_STTARGETCUSTCON="stTargetCustCon";
 	public static final String CHILD_STTARGETCUSTCON_RESET_VALUE="";
  //--Release2.1--//
  //// New link to toggle the language. When touched this link should reload the
  //// page in opposite language (french versus english) and set this new language
  //// session id throughout all modulus.
	public static final String CHILD_TOGGLELANGUAGEHREF="toggleLanguageHref";
	public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE="";

  //--DJ_CL_CR009--start--//
  public static final String CHILD_CKBDJPRODUCECOMMITMENT="ckbDJProduceCommitment";
	public static final String CHILD_CKBDJPRODUCECOMMITMENT_RESET_VALUE="F";

  public static final String CHILD_STDISPLAYDJCHECKBOXSTART="stDisplayDJCheckBoxStart";
	public static final String CHILD_STDISPLAYDJCHECKBOXSTART_RESET_VALUE="";

  public static final String CHILD_STDISPLAYDJCHECKBOXEND="stDisplayDJCheckBoxEnd";
	public static final String CHILD_STDISPLAYDJCHECKBOXEND_RESET_VALUE="";
  //--DJ_CL_CR009--end--//


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doUWDealSummarySnapShotModel doUWDealSummarySnapShot=null;
	private doRowGeneratorModel doRowGenerator=null;
	protected String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgCommitmentOffer.jsp";


	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	private ConditionsReviewHandler handler=new ConditionsReviewHandler();

}

