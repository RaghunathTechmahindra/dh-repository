package mosApp.MosSystem;


import java.math.BigDecimal;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doDetailViewPropertiesModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPrimaryProperty();

	
	/**
	 * 
	 * 
	 */
	public void setDfPrimaryProperty(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStreetNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfStreetNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStreetName();

	
	/**
	 * 
	 * 
	 */
	public void setDfStreetName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStreetType();

	
	/**
	 * 
	 * 
	 */
	public void setDfStreetType(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStreetDirection();

	
	/**
	 * 
	 * 
	 */
	public void setDfStreetDirection(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAddressLine();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddressLine(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfCity();

	
	/**
	 * 
	 * 
	 */
	public void setDfCity(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfProvince();

	
	/**
	 * 
	 * 
	 */
	public void setDfProvince(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPostalFSA();

	
	/**
	 * 
	 * 
	 */
	public void setDfPostalFSA(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPostalLDU();

	
	/**
	 * 
	 * 
	 */
	public void setDfPostalLDU(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLegalLine1();

	
	/**
	 * 
	 * 
	 */
	public void setDfLegalLine1(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLegalLine2();

	
	/**
	 * 
	 * 
	 */
	public void setDfLegalLine2(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLegalLine3();

	
	/**
	 * 
	 * 
	 */
	public void setDfLegalLine3(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfDwellingType();

	
	/**
	 * 
	 * 
	 */
	public void setDfDwellingType(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfDwellingStyle();

	
	/**
	 * 
	 * 
	 */
	public void setDfDwellingStyle(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfStructureAge();

	
	/**
	 * 
	 * 
	 */
	public void setDfStructureAge(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfNoOfUnits();

	
	/**
	 * 
	 * 
	 */
	public void setDfNoOfUnits(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfNumberOfBedRooms();

	
	/**
	 * 
	 * 
	 */
	public void setDfNumberOfBedRooms(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLivingSpace();

	
	/**
	 * 
	 * 
	 */
	public void setDfLivingSpace(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLotSize();

	
	/**
	 * 
	 * 
	 */
	public void setDfLotSize(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfHeatType();

	
	/**
	 * 
	 * 
	 */
	public void setDfHeatType(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfUFFIInsulation();

	
	/**
	 * 
	 * 
	 */
	public void setDfUFFIInsulation(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfWater();

	
	/**
	 * 
	 * 
	 */
	public void setDfWater(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSewage();

	
	/**
	 * 
	 * 
	 */
	public void setDfSewage(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyUsage();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyUsage(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfOccupancy();

	
	/**
	 * 
	 * 
	 */
	public void setDfOccupancy(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyType();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyType(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyLocation();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyLocation(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfZoning();

	
	/**
	 * 
	 * 
	 */
	public void setDfZoning(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPurchasePrice();

	
	/**
	 * 
	 * 
	 */
	public void setDfPurchasePrice(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLandValue();

	
	/**
	 * 
	 * 
	 */
	public void setDfLandValue(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfEquityAvailable();

	
	/**
	 * 
	 * 
	 */
	public void setDfEquityAvailable(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfEstimateValue();

	
	/**
	 * 
	 * 
	 */
	public void setDfEstimateValue(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfActualAppraisedValue();

	
	/**
	 * 
	 * 
	 */
	public void setDfActualAppraisedValue(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfAppraisalDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraisalDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAppraisalSource();

	
	/**
	 * 
	 * 
	 */
	public void setDfAppraisalSource(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLotSizeUnitOfMeasureId();

	
	/**
	 * 
	 * 
	 */
	public void setDfLotSizeUnitOfMeasureId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLivingSpaceUnitOfMeasureId();

	
	/**
	 * 
	 * 
	 */
	public void setDfLivingSpaceUnitOfMeasureId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfNewConstructionId();

	
	/**
	 * 
	 * 
	 */
	public void setDfNewConstructionId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfNewConstructionDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfNewConstructionDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBuilderName();

	
	/**
	 * 
	 * 
	 */
	public void setDfBuilderName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfGarageSize();

	
	/**
	 * 
	 * 
	 */
	public void setDfGarageSize(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfGarageType();

	
	/**
	 * 
	 * 
	 */
	public void setDfGarageType(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfMLSFlag();

	
	/**
	 * 
	 * 
	 */
	public void setDfMLSFlag(String value);
	
	
  //--Release3.1--begins
  //--by Hiro Apr 10, 2006
  public String getDfTenure();

  public void setDfTenure(String value);

  public String getDfMiEnergyEfficiency();

  public void setDfMiEnergyEfficiency(String value);

  public String getDfSubdivisionDiscount();

  public void setDfSubdivisionDiscount(String value);

  public BigDecimal getDfOnReserveTrustAgreement();

  public void setDfOnReserveTrustAgreement(BigDecimal value);
  //--Release3.1--ends
  public java.math.BigDecimal getDfInstitutionId();
  public void setDfInstitutionId(java.math.BigDecimal id);
  
  
  public String getDfUnitNumber();
  public void setDfUnitNumber(String value);
  ////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFPRIMARYPROPERTY="dfPrimaryProperty";
	public static final String FIELD_DFSTREETNUMBER="dfStreetNumber";
	public static final String FIELD_DFSTREETNAME="dfStreetName";
	public static final String FIELD_DFSTREETTYPE="dfStreetType";
	public static final String FIELD_DFSTREETDIRECTION="dfStreetDirection";
	public static final String FIELD_DFADDRESSLINE="dfAddressLine";
	public static final String FIELD_DFCITY="dfCity";
	public static final String FIELD_DFPROVINCE="dfProvince";
	public static final String FIELD_DFPOSTALFSA="dfPostalFSA";
	public static final String FIELD_DFPOSTALLDU="dfPostalLDU";
	public static final String FIELD_DFLEGALLINE1="dfLegalLine1";
	public static final String FIELD_DFLEGALLINE2="dfLegalLine2";
	public static final String FIELD_DFLEGALLINE3="dfLegalLine3";
	public static final String FIELD_DFDWELLINGTYPE="dfDwellingType";
	public static final String FIELD_DFDWELLINGSTYLE="dfDwellingStyle";
	public static final String FIELD_DFSTRUCTUREAGE="dfStructureAge";
	public static final String FIELD_DFNOOFUNITS="dfNoOfUnits";
	public static final String FIELD_DFNUMBEROFBEDROOMS="dfNumberOfBedRooms";
	public static final String FIELD_DFLIVINGSPACE="dfLivingSpace";
	public static final String FIELD_DFLOTSIZE="dfLotSize";
	public static final String FIELD_DFHEATTYPE="dfHeatType";
	public static final String FIELD_DFUFFIINSULATION="dfUFFIInsulation";
	public static final String FIELD_DFWATER="dfWater";
	public static final String FIELD_DFSEWAGE="dfSewage";
	public static final String FIELD_DFPROPERTYUSAGE="dfPropertyUsage";
	public static final String FIELD_DFOCCUPANCY="dfOccupancy";
	public static final String FIELD_DFPROPERTYTYPE="dfPropertyType";
	public static final String FIELD_DFPROPERTYLOCATION="dfPropertyLocation";
	public static final String FIELD_DFZONING="dfZoning";
	public static final String FIELD_DFPURCHASEPRICE="dfPurchasePrice";
	public static final String FIELD_DFLANDVALUE="dfLandValue";
	public static final String FIELD_DFEQUITYAVAILABLE="dfEquityAvailable";
	public static final String FIELD_DFESTIMATEVALUE="dfEstimateValue";
	public static final String FIELD_DFACTUALAPPRAISEDVALUE="dfActualAppraisedValue";
	public static final String FIELD_DFAPPRAISALDATE="dfAppraisalDate";
	public static final String FIELD_DFAPPRAISALSOURCE="dfAppraisalSource";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFPROPERTYID="dfPropertyId";
	public static final String FIELD_DFLOTSIZEUNITOFMEASUREID="dfLotSizeUnitOfMeasureId";
	public static final String FIELD_DFLIVINGSPACEUNITOFMEASUREID="dfLivingSpaceUnitOfMeasureId";
	public static final String FIELD_DFNEWCONSTRUCTIONID="dfNewConstructionId";
	public static final String FIELD_DFNEWCONSTRUCTIONDESC="dfNewConstructionDesc";
	public static final String FIELD_DFBUILDERNAME="dfBuilderName";
	public static final String FIELD_DFGARAGESIZE="dfGarageSize";
	public static final String FIELD_DFGARAGETYPE="dfGarageType";
	public static final String FIELD_DFMLSFLAG="dfMLSFlag";
	
  //--Release3.1--begins
  //--by Hiro Apr 10, 2006
  public static final String FIELD_DFTENURE =  "dfTenure";
  public static final String FIELD_DFMIENERGYEFFICIENCY = "dfMiEnergyEfficiency";
  public static final String FIELD_DFSUBDIVISIONDISCOUNT = "dfSubdivisionDiscount";
  public static final String FIELD_DFONRESERVETRUSTAGREEMENT = "dfOnReserveTrustAgreement";
  //--Release3.1--ends
  public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
  public static final String FIELD_DFPROPERTYUNITNUMBER="dfUnitNumber";
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

