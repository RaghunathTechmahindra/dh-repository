package mosApp.MosSystem;

import java.math.BigDecimal;
import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 * Title: doComponentCreditCardModelImpl
 * <p>
 * Description: This is the this is the model class for queringt the data from
 * the ComponentCreditCard table.
 * @author MCM Impl Team.
 * @version 1.0 09-June-2008 XS_16.7 Initial version
 * @version 1.1 23-June-2008 XS_16.7 - Modified  STATIC_WHERE_CRITERIA for ordering the join condition.
 * @version 1.2 25-June-2008 XS_16.16 - Added new Additional Information Field and modified SELECT_SQL_TEMPLATE.
 * @version 1.3 25-June-2008 XS_2.37  - Added fields for the Dynamic product feilds on the loan section in CD screen
 */
public class doComponentCreditCardModelImpl extends QueryModelBase implements
        doComponentCreditCardModel {

    public static final String DATA_SOURCE_NAME = "jdbc/orcl";

    public static final String SELECT_SQL_TEMPLATE = "SELECT ALL COMPONENT.POSTEDRATE,"
            + " COMPONENT.DEALID,COMPONENT.COMPONENTID, "
            + " COMPONENT.COPYID, COMPONENT.INSTITUTIONPROFILEID, "
            + " to_char(COMPONENT.COMPONENTTYPEID)COMPONENTTYPEID_STR,"
            + " to_char(COMPONENT.MTGPRODID) MTGPRODID_STR, "
            + " COMPONENTCREDITCARD.CREDITCARDAMOUNT, "
            + " COMPONENT.ADDITIONALINFORMATION, COMPONENT.MTGPRODID, "
            + " COMPONENT.PRICINGRATEINVENTORYID " //XS_2.36(Dynamic drop down lists)
            + " FROM COMPONENT,COMPONENTCREDITCARD "
            + " __WHERE__  ORDER BY COMPONENT.COMPONENTID ";

    public static final String MODIFYING_QUERY_TABLE_NAME = "COMPONENT, COMPONENTCREDITCARD";

    public static final String STATIC_WHERE_CRITERIA = "COMPONENT.INSTITUTIONPROFILEID= COMPONENTCREDITCARD.INSTITUTIONPROFILEID "
            + "AND COMPONENT.COPYID=COMPONENTCREDITCARD.COPYID "
            + "AND COMPONENT.COMPONENTID=COMPONENTCREDITCARD.COMPONENTID "
            + "AND COMPONENT.COMPONENTTYPEID=4";

    public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();

    public static final String QUALIFIED_COLUMN_DFCOMPONENTID = "COMPONENT.COMPONENTID";

    public static final String COLUMN_DFCOMPONENTID = "COMPONENTID";

    public static final String QUALIFIED_COLUMN_DFDEALID = "COMPONENT.DEALID";

    public static final String COLUMN_DFDEALID = "DEALID";

    public static final String QUALIFIED_COLUMN_DFISTITUTIONPROFILEID = "COMPONENT.INSTITUTIONPROFILEID";

    public static final String COLUMN_DFISTITUTIONPROFILEID = "INSTITUTIONPROFILEID";

    public static final String QUALIFIED_COLUMN_DFCOPYID = "COMPONENT.COPYID";

    public static final String COLUMN_DFCOPYID = "COPYID";

    public static final String QUALIFIED_COLUMN_DFCOMPONENTTYPE = "COMPONENT.COMPONENTTYPEID_STR";

    public static final String COLUMN_DFCOMPONENTTYPE = "COMPONENTTYPEID_STR";

    public static final String QUALIFIED_COLUMN_DFPRODUCTNAME = "COMPONENT.MTGPRODID_STR";

    public static final String COLUMN_DFPRODUCTNAME = "MTGPRODID_STR";

    public static final String QUALIFIED_COLUMN_DFCOMPONENTCREDITCARDAMOUNT = "COMPONENTCREDITCARD.CREDITCARDAMOUNT";

    public static final String COLUMN_DFCOMPONENTCREDITCARDAMOUNT = "CREDITCARDAMOUNT";

    public static final String QUALIFIED_COLUMN_DFPOSTEDRATE = "COMPONENT.POSTEDRATE";

    public static final String COLUMN_DFPOSTEDRATE = "POSTEDRATE";
    /**************************MCM Impl Team XS_16.16 changes starts*****************/
    public static final String QUALIFIED_COLUMN_DFADDITIONALINFORMATION = "COMPONENT.ADDITIONALINFORMATION";
    public static final String COLUMN_DFADDITIONALINFORMATION = "ADDITIONALINFORMATION";
    /**************************MCM Impl Team XS_16.16 changes ends*****************/
    
    /**************************MCM Impl Team XS_2.36 changes starts*****************/
    public static final String QUALIFIED_COLUMN_DFMTGPRODID = "COMPONENT.MTGPRODID";
    public static final String COLUMN_DFMTGPRODID = "MTGPRODID";
    /**************************MCM Impl Team XS_2.36 changes ends*****************/
    
    /*********************MCM Impl Team XS_2.36 (dynamic drop down lists) changes starts******************/
    public static final String QUALIFIED_COLUMN_DFPRICINGRATEINVENTORYID = "COMPONENT.PRICINGRATEINVENTORYID";
    public static final String COLUMN_DFPRICINGRATEINVENTORYID = "PRICINGRATEINVENTORYID";
    /*********************MCM Impl Team XS_2.36 (dynamic drop down lists) changes ends******************/

    static {

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCOMPONENTID, COLUMN_DFCOMPONENTID,
                QUALIFIED_COLUMN_DFCOMPONENTID, java.math.BigDecimal.class,
                false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFDEALID, COLUMN_DFDEALID, QUALIFIED_COLUMN_DFDEALID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCOPYID, COLUMN_DFCOPYID, QUALIFIED_COLUMN_DFCOPYID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFISNTITUTIONID, COLUMN_DFISTITUTIONPROFILEID,
                QUALIFIED_COLUMN_DFISTITUTIONPROFILEID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCOMPONENTTYPE, COLUMN_DFCOMPONENTTYPE,
                QUALIFIED_COLUMN_DFCOMPONENTTYPE, String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPRODUCTNAME, COLUMN_DFPRODUCTNAME,
                QUALIFIED_COLUMN_DFPRODUCTNAME, String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCREDITCARDAMOUNT, COLUMN_DFCOMPONENTCREDITCARDAMOUNT,
                QUALIFIED_COLUMN_DFCOMPONENTCREDITCARDAMOUNT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPOSTEDRATE, COLUMN_DFPOSTEDRATE,
                QUALIFIED_COLUMN_DFPOSTEDRATE, java.math.BigDecimal.class,
                false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        /**************************MCM Impl Team XS_16.16 changes starts*****************/      
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFADDITIONALINFORMATION, COLUMN_DFADDITIONALINFORMATION,
                QUALIFIED_COLUMN_DFADDITIONALINFORMATION, String.class,
                false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        /**************************MCM Impl Team XS_16.16 changes ends*****************/
        
        /**************************MCM Impl Team XS_2.36 changes starts*****************/      
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFMTGPRODID, 
                COLUMN_DFMTGPRODID, 
                QUALIFIED_COLUMN_DFMTGPRODID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        /**************************MCM Impl Team XS_2.36 changes ends*****************/      

        /*********************MCM Impl Team XS_2.36 (dynamic drop down lists) changes starts******************/
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPRICINGRATEINVENTORYID, 
                COLUMN_DFPRICINGRATEINVENTORYID,
                QUALIFIED_COLUMN_DFPRICINGRATEINVENTORYID, 
                BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        /*********************MCM Impl Team XS_2.36 (dynamic drop down lists) changes starts******************/
    }

    /**
     * Instantiates a new do component credit card model impl.
     */
    public doComponentCreditCardModelImpl() {
        super();
        setDataSourceName(DATA_SOURCE_NAME);

        setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

        setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

        setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

        setFieldSchema(FIELD_SCHEMA);

        initialize();

    }

    /**
     * Initialize.
     */
    public void initialize() {

    }

    /**
     * BeforeExecute
     * @param context
     *            the context
     * @param queryType
     *            the query type
     * @param sql
     *            the sql
     * @return list of model model[]
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    protected String beforeExecute(ModelExecutionContext context,
            int queryType, String sql) throws ModelControlException {

        return sql;

    }

    /**
     * This method is used to handle any display logic after executing the
     * model.
     * @param context
     *            the context
     * @param queryType
     *            the query type
     * @return list of model model[]
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    protected void afterExecute(ModelExecutionContext context, int queryType)
            throws ModelControlException {

        String defaultInstanceStateName = getRequestContext().getModelManager()
                .getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl) getRequestContext()
                .getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName, true);
        int languageId = theSessionState.getLanguageId();
        int institutionId = theSessionState.getDealInstitutionId();

        while (this.next()) {

            this.setDfComponentType(BXResources.getPickListDescription(
                    institutionId, "COMPONENTTYPE", this.getDfComponentType(),
                    languageId));
            this.setDfProductName(BXResources.getPickListDescription(
                    institutionId, "MTGPROD", this.getDfProductName(),
                    languageId));

        }

        // Reset Location
        this.beforeFirst();

    }

    /**
     * OnDatabaseError.
     * @param context
     *            the context
     * @param queryType
     *            the query type
     * @param exception
     *            the exception
     * @return list of model model[]
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    protected void onDatabaseError(ModelExecutionContext context,
            int queryType, SQLException exception) {

    }

    public java.math.BigDecimal getDfComponentId() {

        return (java.math.BigDecimal) getValue(FIELD_DFCOMPONENTID);

    }

    public void setDfComponentId(java.math.BigDecimal value) {

        setValue(FIELD_DFCOMPONENTID, value);

    }

    public java.math.BigDecimal getDfCopyId() {

        return (java.math.BigDecimal) getValue(FIELD_DFCOPYID);

    }

    public void setDfCopyId(java.math.BigDecimal value) {

        setValue(FIELD_DFCOPYID, value);

    }

    public BigDecimal getDfInstitutionId() {

        return (java.math.BigDecimal) getValue(FIELD_DFISNTITUTIONID);

    }

    public void setDfInstitutionId(BigDecimal value) {

        setValue(FIELD_DFISNTITUTIONID, value);

    }

    public void setDfCreditCardAmount(BigDecimal value) {

        setValue(FIELD_DFCREDITCARDAMOUNT, value);

    }

    public java.math.BigDecimal getDfCreditCardAmount() {

        return (java.math.BigDecimal) getValue(FIELD_DFCREDITCARDAMOUNT);

    }

    public String getDfComponentType() {

        return (String) getValue(FIELD_DFCOMPONENTTYPE);

    }

    public BigDecimal getDfPostedRate(){

        return (java.math.BigDecimal) getValue(FIELD_DFPOSTEDRATE);

    }

    public String getDfProductName() {

        return (String) getValue(FIELD_DFPRODUCTNAME);

    }

    public void setDfComponentType(String value) {

        setValue(FIELD_DFCOMPONENTTYPE, value);

    }

    public void setDfPostedRate(BigDecimal value) {

        setValue(FIELD_DFPOSTEDRATE, value);

    }

    public void setDfProductName(String value) {

        setValue(FIELD_DFPRODUCTNAME, value);

    }

    public BigDecimal getDfDealId() {

        return (java.math.BigDecimal) getValue(FIELD_DFDEALID);

    }

    public void setDfDealId(BigDecimal value) {

        setValue(FIELD_DFDEALID, value);

    }
/*********************MCM Impl Team XS_16.16 changes starts*********************/
    public String getDfAdditionalInformation() {
        return (String) getValue(FIELD_DFADDITIONALINFORMATION);
    }

    public void setDfAdditionalInformation(String value) {
        setValue(FIELD_DFADDITIONALINFORMATION, value);
    }
/*********************MCM Impl Team XS_16.16 changes ends*********************/
    
/*********************MCM Impl Team XS_2.36 changes starts*********************/
    /**
     * This method declaration returns the value of field the MtgProdId.
     * @return dfMtgProdId
     */
    public java.math.BigDecimal getDfMtgProdId(){
        return (BigDecimal) getValue(FIELD_DFMTGPRODID);
        
    }

    /**
     * This method declaration sets the MtgProdId.
     * @param value: the new dfMtgProdId
     */
    public void setDfMtgProdId(java.math.BigDecimal value){
        setValue(FIELD_DFMTGPRODID, value);
        
    }
    /*********************MCM Impl Team XS_2.36 changes starts*********************/

    /*********************MCM Impl Team XS_2.36 (dynamic drop down lists) changes starts******************/
    /**
     * This method declaration returns the value of field the dfPricingrateinventoryid.
     * 
     * @return the dfPricingrateinventoryid
     */
    public BigDecimal getDfPricingrateinventoryid() {
        return (BigDecimal) getValue(FIELD_DFPRICINGRATEINVENTORYID);
    }
    
    /**
     * This method declaration sets the the dfPricingrateinventoryid.
     * 
     * @param value  the new dfPricingrateinventoryid
     */
    public void setDfPricingrateinventoryid(BigDecimal value) {
        setValue(FIELD_DFPRICINGRATEINVENTORYID, value);
        
    }
    /*********************MCM Impl Team XS_2.36 (dynamic drop down lists) changes ends******************/
}
