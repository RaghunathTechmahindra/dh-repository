package mosApp.MosSystem;

import java.text.*;
import java.util.*;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.*;
import com.basis100.deal.security.*;
import com.basis100.deal.pk.*;
import com.basis100.workflow.*;
import com.basis100.workflow.entity.*;
import com.basis100.workflow.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.util.*;
import com.basis100.deal.validation.*;
import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.jdbcservices.jdbcexecutor.*;
import com.basis100.util.date.*;
import com.basis100.picklist.BXResources;
import com.basis100.deal.docrequest.DocumentRequest;

import mosApp.*;
import MosSystem.*;
import mosApp.MosSystem.models.AVMOrderModel;

import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.html.*;
import com.iplanet.jato.view.event.*;

import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.express.web.rc.RequestProcessException;
import com.filogix.express.web.util.service.ValuationAppraisalHelper;

/**
 *
 *
 */
public class AppraisalHandler
  extends PageHandlerCommon
  implements Cloneable, Sc
{
    // the logger.
    static final private Log _log = LogFactory.getLog(AppraisalHandler.class);
    
    //Appraiser flag is set
    public final String APPRAISER_FLAG_SET = "Y";
   
    //Status ID for submitted to READ
    //Moved this constant to ServiceConst - 22 Nov 2006
    //public final int  STATUS_SUBMITTED_TO_READ = 19;
    static final String voName = "Repeated1";
    static final String voNameB = "Repeated1/" ;
    

  /**
   *
   *
   */
  public AppraisalHandler cloneSS()
  {
    return(AppraisalHandler)super.cloneSafeShallow();

  }

  /////////////////////////////////////////////////////////////////////
  //This method is used to populate the fields in the header
  //with the appropriate information

  public void populatePageDisplayFields()
  {
    PageEntry pg = getTheSessionState().getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    populatePageShellDisplayFields();
    // Quick Link Menu display
    displayQuickLinkMenu();
    populateTaskNavigator(pg);
    populatePageDealSummarySnapShot();
    populatePreviousPagesLinks();
  }


  /////////////////////////////////////////////////////////////////////
  //
  // Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
  //
  public void setupBeforePageGeneration()
  {
    PageEntry pg = getTheSessionState().getCurrentPage();
    if(pg.getSetupBeforeGenerationCalled() == true)
    {
      return;
    }
    pg.setSetupBeforeGenerationCalled(true);

    // -- //
    setupDealSummarySnapShotDO(pg);

    // criteria for main properties population
    QueryModelBase theDO = (QueryModelBase)
      (RequestManager.getRequestContext().getModelManager().getModel(
      doAppraisalPropertyModel.class));

    theDO.clearUserWhereCriteria();
    theDO.addUserWhereCriterion("dfDealId", "=",
      new String("" + pg.getPageDealId()));
    theDO.addUserWhereCriterion("dfCopyId", "=",
      new String("" + pg.getPageDealCID()));

    PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");
    Hashtable pst = pg.getPageStateTable();

    if(tds == null)
    {
      // first invocation!
      Vector propIds = new Vector();
      pst.put("PROPIDS", propIds);
      Vector aDates = new Vector();
      pst.put("ADATES", aDates);

      // get and store property ids, etc
      int numRows = 0;
      try
      {
        theDO.executeSelect(null);
        numRows = theDO.getSize();
        logger.debug(
          "--- AppraiserReviewHandler.setupBeforePageGeneration ---> :: numRows = " +
          numRows);
      }
      catch(Exception ex)
      {
        logger.error("Exception @AppraisalHandler.setupBeforePageGeneration - entry to page will be disallowed.");
        logger.error(ex);
        numRows = 0;
      }

      // store state info
      logger.debug(
        "--- AppraiserReviewHandler.setupBeforePageGeneration ---> :: Store State Info. **************************** ");
      java.util.Date dt = null;
      try
      {
        while(theDO.next())
        {
          propIds.addElement(theDO.getValue(doAppraisalPropertyModel.FIELD_DFPROPERTYID).toString());
          logger.debug("--- AppraisalReviewHandler ---> :: propertyid = " +
            theDO.getValue(doAppraisalPropertyModel.FIELD_DFPROPERTYID).toString());
          dt = (java.util.Date)(theDO.getValue(doAppraisalPropertyModel.FIELD_DFAPPRAISALDATE));
          aDates.addElement(dt);
          logger.debug("--- AppraisalReviewHandler ---> :: adate = " + dt);
        }
      }
      catch(Exception e)
      {
        logger.error("@AppraisalHandler..setupBeforePageGeneration : Error when getting data from Model");
        logger.error(e.toString());
      }

      String voName = "Repeated1";
      int perPage = ((TiledView)(getCurrNDPage().getChild(voName))).
        getMaxDisplayTiles();

      // set up and save task display state object
      tds = new PageCursorInfo("DEFAULT", voName, perPage, propIds.size());
      tds.setRefresh(true);
      pst.put(tds.getName(), tds);

      // setup to generate no entry (active) message when there are no properties
      if(propIds.size() <= 0)
      {
        setActiveMessageToAlert(BXResources.getSysMsg(
          "APPRAISAL_REVIEW_CANNOT_ENTER", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMLEAVEPAGE);
      }
    }

    // --- //
    theDO = (QueryModelBase)
      (RequestManager.getRequestContext().getModelManager().getModel(
      doTotalAppraisedModel.class));
    theDO.clearUserWhereCriteria();
    theDO.addUserWhereCriterion("dfDealId", "=",
      new String("" + pg.getPageDealId()));
    theDO.addUserWhereCriterion("dfCopyId", "=",
      new String("" + pg.getPageDealCID()));
  }

  /////////////////////////////////////////////////////////////////////
  public void displayAppraiser(int rowNdx)
  {
    String propertyId = new String("");
    Date appraisalDate = null;

    try
    {
      PageEntry pg = getTheSessionState().getCurrentPage();
      ViewBean currNDPage = getCurrNDPage();
      Hashtable pst = pg.getPageStateTable();
      int dealid = pg.getPageDealId();
      int copyid = pg.getPageDealCID();
      
      Vector propIds = new Vector();
      Vector aDates = new Vector();

      //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
      //--> By Billy 18Dec2003
      //Set display for the Phone #s
      doAppraisalPropertyModelImpl theObj =(doAppraisalPropertyModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doAppraisalPropertyModel.class));
      setPhoneNoDisplayField(theObj,
        "Repeated1/tbAOContactWorkPhoneNum1",
        "Repeated1/tbAOContactWorkPhoneNum2",
        "Repeated1/tbAOContactWorkPhoneNum3",
        "dfAOContactWorkPhoneNum", rowNdx);
      setPhoneNoDisplayField(theObj,
        "Repeated1/tbAOContactCellPhoneNum1",
        "Repeated1/tbAOContactCellPhoneNum2",
        "Repeated1/tbAOContactCellPhoneNum3",
        "dfAOContactCellPhoneNum", rowNdx);
      setPhoneNoDisplayField(theObj,
        "Repeated1/tbAOContactHomePhoneNum1",
        "Repeated1/tbAOContactHomePhoneNum2",
        "Repeated1/tbAOContactHomePhoneNum3",
        "dfAOContactHomePhoneNum", rowNdx);
      //===================================================================

      /////////////////////////////////////////////////////
      Object obj = pst.get("PROPIDS");
      if(obj instanceof Vector)
      {
        propIds = (Vector)obj;

        // check for no row case
      }
      if(propIds.size() == 0)
      {
        return;
      }
      obj = pst.get("ADATES");
      if(obj instanceof Vector)
      {
        aDates = (Vector)obj;
      }
      logger.debug("--- AppraiserReviewHandler.displayAppraiser ---> :: rowIndex = " + rowNdx);

      /////////////////////////////////////////////////////
      //ComboBox comboBox =(ComboBox)(getCurrNDPage().getDisplayField("Repeated1/cbMonths"));
      final String nameCbMonths = voNameB+pgAppraiserReviewRepeated1TiledView.CHILD_CBMONTHS;
      fillupMonths(nameCbMonths);

      obj = aDates.elementAt(rowNdx);
      if(obj instanceof Date)
      {
        appraisalDate = (Date)obj;
      }
      String sYear, sMonths, sDay;
      if(appraisalDate == null) {
        //--> Fill the default values if appraisalDate = null--> By Billy 09Sept2002
    	logger.debug("--- AppraiserReviewHandler.displayAppraiser ---> appraisalDate = null :: Set all defaults for Date");
        sYear = "";
        sMonths = "0";
        sDay = "";
      } else {
      	int aYear = appraisalDate.getYear();
        final int aMonth = appraisalDate.getMonth() + 1;
	    final int aDay = appraisalDate.getDate();
	    logger.debug("--- AppraiserReviewHandler.displayAppraiser ---> :: appraisalDate.year = " +aYear
              + ";appraisalDate.month = " + aMonth + ";appraisalDate.day = " + aDay);
        //--> The following is a bug -- By Billy 09Sept2002
        aYear += 1900;
        //-->if (year < 30) year += 2000;
        //-->if (year > 100) year += 1900;
        //================================================
        sMonths = Integer.toString(aMonth);
        sYear = Integer.toString(aYear);
        sDay = Integer.toString(aDay);
      }
      currNDPage.setDisplayFieldValue(voNameB+pgAppraiserReviewRepeated1TiledView.CHILD_TBYEAR, sYear);
      currNDPage.setDisplayFieldValue(nameCbMonths, sMonths);
      currNDPage.setDisplayFieldValue(voNameB+pgAppraiserReviewRepeated1TiledView.CHILD_TBDAY, sDay);
      
      //===================================================

      obj = propIds.elementAt(rowNdx);
      if(obj instanceof String)
      {
        propertyId = (String)obj;
      }
      logger.debug("--- AppraiserReviewHandler.displayAppraiser ---> :: propertyId.toString() = " + propertyId.toString());

      // SEAN Appraisal/AVM hidden control.
      setHiddenAppraisalSection(getCurrNDPage());
      completeTaskStatus(propertyId, rowNdx);
      setHiddenAVMSection(getCurrNDPage());
      // SEAN Appraisal/AVM hidden control END.

      // START:Appraisal Contact Information hidden control
      setHiddenAppraisalContactInfoSection(getCurrNDPage());
      // END:Appraisal Contact Information hidden control

      // display the avm order section.
      displayAVMOrderSection(propertyId, copyid);
      displayAppraisalSection(propertyId, copyid);   

      // SEAN AVM March 15, 2006: Binding the stTileIndex.
      // We already checked the property existence before, and already got the property id.
      ((StaticTextField) getCurrNDPage().getDisplayField("Repeated1/stTileIndex")).setValue("" + rowNdx);
      // SEAN AVM: set the onchange event for provider combobox and other fields.
      prepareDisplayFields(rowNdx);
      // populate the existing AVM response.
      // we have the dealid, copyid, propertyid, let's go find the existing response.
      String avmReports = getAVMReportsTrs(dealid, copyid, propertyId, rowNdx);
      ((StaticTextField) getCurrNDPage().
           getDisplayField("Repeated1/stAVMReports")).setValue(avmReports);
      
      
      String appraisalSummary = getAppraisalReportsTrs(dealid, copyid, propertyId, rowNdx); 
      ((StaticTextField) getCurrNDPage(). 
              getDisplayField("Repeated1/stAppraisalSummary")).setValue(appraisalSummary); 
      
      // SEAN AVM End

      QueryModelBase appraiserSelectDO = (QueryModelBase)
        (RequestManager.getRequestContext().getModelManager().getModel(
        doAppraiserSelectModel.class));
      appraiserSelectDO.clearUserWhereCriteria();
      appraiserSelectDO.addUserWhereCriterion("dfDealId", "=", new Integer(dealid));

      appraiserSelectDO.addUserWhereCriterion("dfPropertyId", "=", new String(propertyId));
      appraiserSelectDO.executeSelect(null);

      if(appraiserSelectDO.getSize() <= 0)
      {
        pst.put("APPRAISER_THIS_ROW", "N");
        logger.debug(
          "--- AppraiserReviewHandler.displayAppraiser ---> :: No records returned. ");
        String noVal = new String("");

        // Added new fields -- Billy 13March2002
        getCurrNDPage().setDisplayFieldValue("Repeated1/stAppraiserShortName",
          noVal);
        getCurrNDPage().setDisplayFieldValue("Repeated1/stAppraiserCompanyName",
          noVal);
        getCurrNDPage().setDisplayFieldValue("Repeated1/tbAppraiserNotes",
          noVal);
        //============================================================================
        getCurrNDPage().setDisplayFieldValue("Repeated1/stAppraiserName", noVal);
        getCurrNDPage().setDisplayFieldValue("Repeated1/stAppraiserAddress",
          noVal);
        getCurrNDPage().setDisplayFieldValue("Repeated1/stPhone", noVal);
        getCurrNDPage().setDisplayFieldValue("Repeated1/stFax", noVal);
        getCurrNDPage().setDisplayFieldValue("Repeated1/stEmail", noVal);
        return;

        // no appraiser currently set
      }
      logger.debug(
        "--- AppraiserReviewHandler.displayAppraiser ---> :: records returned. ");
      pst.put("APPRAISER_THIS_ROW", "Y");

      // Added new fields -- Billy 13March2002
      getCurrNDPage().setDisplayFieldValue("Repeated1/stAppraiserShortName",
        appraiserSelectDO.getValue(
        "dfAppraiserShortName"));
      getCurrNDPage().setDisplayFieldValue("Repeated1/stAppraiserCompanyName",
        appraiserSelectDO.getValue(
        "dfAppraiserCompanyName"));
      getCurrNDPage().setDisplayFieldValue("Repeated1/tbAppraiserNotes",
        appraiserSelectDO.getValue(
        "dfAppraiserNotes"));
      //============================================================================
      getCurrNDPage().setDisplayFieldValue("Repeated1/stAppraiserAddress",
        appraiserSelectDO.getValue(
        "dfAppraiserAddress"));
      getCurrNDPage().setDisplayFieldValue("Repeated1/stFax",
        appraiserSelectDO.getValue("dfFax"));
      getCurrNDPage().setDisplayFieldValue("Repeated1/stEmail",
        appraiserSelectDO.getValue("dfEmail"));
      getCurrNDPage().setDisplayFieldValue("Repeated1/stAppraiserName",
        appraiserSelectDO.getValue(
        "dfAppraiserName"));
      getCurrNDPage().setDisplayFieldValue("Repeated1/stPhone",
        appraiserSelectDO.getValue(
        "dfAppraiserPhoneNumber"));
      Object extension = appraiserSelectDO.getValue("dfAppraiserExtension");
      if(extension != null && extension.toString().trim().length() != 0)
      {
        getCurrNDPage().setDisplayFieldValue("Repeated1/stAppraiserExtension",
          new
          String("x" + extension.toString()));
      }
    }
    catch(Exception e)
    {
      setStandardFailMessage();
      logger.error("Exception @AppraisalHandler.displayAppraiser: Problem encountered locating appraiser, propertyId=<" +
        propertyId + ">");
      logger.error(e);
      return;
    }

    return;

  }

  /////////////////////////////////////////////////////////////////////
  public boolean setAddAppraiserStatus()
  {
    // use (row scope) page state variable set by displayAppraiser() [triggered on
    // a row event] to communicate if an appraiser is selected for the current row.
    PageEntry pg = getTheSessionState().getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    boolean haveAppraiser = false;
    String flag = (String)pst.get("APPRAISER_THIS_ROW");

    if(flag != null && flag.equals("Y"))
    {
      haveAppraiser = true;

    }
    if(haveAppraiser == false)
    {
      return true;
    }
    // none - display the 'add' button
    else
    {
      return false;
    }
  }

  ///////////////////////////////////////////////////////////////////////////
  public boolean setChangeAppraiserStatus()
  {
    // use (row scope) page state variable set by displayAppraiser() [triggered on
    // a row event] to communicate if an appraiser is selected for the current row.
    PageEntry pg = getTheSessionState().getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    boolean haveAppraiser = false;
    String flag = (String)pst.get("APPRAISER_THIS_ROW");

    if(flag != null && flag.equals("Y"))
    {
      haveAppraiser = true;

    }
    if(haveAppraiser == false)
    {

// none - hide the 'change' button
      return false;
    }
    else
    {
      return true;
    }
  }

  /////////////////////////////////////////////////////////////////////
  public void saveData(PageEntry pg, boolean calledInTransaction)
  {
    logger.debug("@AppraisalHandler.saveData():: saveData IN");

    int i = 0;

    PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");
    
    // SEAN AVM for testing the cancel NullPointerException issue.
    if (tds == null) {
        setupBeforePageGeneration();
    }
    tds = getPageCursorInfo(pg, "DEFAULT");
    SessionResourceKit srk = getSessionResourceKit();

    try
    {

      CalcMonitor dcm = CalcMonitor.getMonitor(srk);
      //FXP21773
      srk.getExpressState().setDealInstitutionId(pg.getDealInstitutionId());
      // SEAN AVM EDN
      srk.beginTransaction();

      // update entities
      int propertiesOnPage = tds.getNumberOfDisplayRowsOnPage();
      for(i = 0; i < propertiesOnPage; ++i)
      {
        logger.debug(
          "@AppraisalHandler.saveData():: calling updateProperty for property " + i);
        updateProperty(i, pg, srk, dcm);
      }
      dcm.calc();
      srk.commitTransaction();
      if(srk.getModified())
      {
        pg.setModified(true);
      }
    }
    catch(Exception e)
    {
      srk.cleanTransaction();
      setActiveMessageToAlert(BXResources.getSysMsg(
        "TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
      logger.error("Exception @AppraisalHandler.SaveData, deal Id = " +
        pg.getPageDealId() + ", copy Id = " + pg.getPageDealCID() +
        ", line number = " + i);
      logger.error(e);
      return;
    }

  }

  /////////////////////////////////////////////////////////////////////
  private void updateProperty(int rowNdx, PageEntry pg, SessionResourceKit srk, CalcMonitor dcm) throws Exception
  {
    int month = 0;
    int day = 0;
    int year = 0;
    int propertyId = 0;

    try
    {
      Hashtable pst = pg.getPageStateTable();
      Vector propIds = (Vector)pst.get("PROPIDS");
      Vector aDates = (Vector)pst.get("ADATES");
      propertyId = com.iplanet.jato.util.TypeConverter.asInt(propIds.elementAt(rowNdx), 0);
      logger.debug("@AppraisalHandler.updateProperty():: propertyId = " + propertyId);
      logger.debug("@AppraisalHandler.updateProperty():: copyid = " + pg.getPageDealCID());
      Property pp = new Property(srk, dcm, propertyId, pg.getPageDealCID());

      // read and set properties
      //#DG796 rewritten
      // Appraisal Date
      Date dt;
      
      String sYear = getRepeatedField(voNameB+pgAppraiserReviewRepeated1TiledView.CHILD_TBYEAR, rowNdx);
      String sMonths = getRepeatedField(voNameB+pgAppraiserReviewRepeated1TiledView.CHILD_CBMONTHS, rowNdx);
      String sDay = getRepeatedField(voNameB+pgAppraiserReviewRepeated1TiledView.CHILD_TBDAY, rowNdx);
      // allow clearance
      if(StringUtil.isEmpty(sYear) && "0".equals(sMonths) && StringUtil.isEmpty(sDay))	
			dt = null;
      else {
      	year = com.iplanet.jato.util.TypeConverter.asInt(sYear, -1);
      	month = com.iplanet.jato.util.TypeConverter.asInt(sMonths, -1) - 1;
      	day = com.iplanet.jato.util.TypeConverter.asInt(sDay, -1);
			if (month >= 0 &&  day > 0 && year > 0) {
				Calendar calendar = GregorianCalendar.getInstance();
				calendar.set(year, month, day, 0, 0, 0);		//#DG796
				dt = calendar.getTime();
			}
			else
				dt = pp.getAppraisalDateAct();	//leave as is
		}
		pp.setAppraisalDateAct(dt);
      logger.debug("updateProperty -- the date :: year=" + year + ";month=" +month + ";day=" + day);
      aDates.set(rowNdx, dt);

      pp.setAppraisalSourceId(
        com.iplanet.jato.util.TypeConverter.asInt(getRepeatedField(
        "Repeated1/cbAppraisalSource", rowNdx), 0));

      pp.setEstimatedAppraisalValue(com.iplanet.jato.util.TypeConverter.asDouble(getRepeatedField(
        "Repeated1/tbEstimatedAppraisal", rowNdx), 0));

      pp.setActualAppraisalValue(com.iplanet.jato.util.TypeConverter.asDouble(getRepeatedField(
        "Repeated1/tbActualAppraisal", rowNdx), 0));

//    check if need to update Appraisal Order status
      if(supportSubmitToRead())
      {      	
    	  AppraisalOrder theAO = pp.getAppraisalOrder();
          if(theAO == null)
          {    	
            // Create the Appraisal Order record
            theAO = new AppraisalOrder(srk, dcm).create((PropertyPK)(pp.getPk()));
          }
      	  //set status to Submitted to READ      	
          theAO.setAppraisalStatusId(ServiceConst.REQUEST_STATUS_SUBMIT_TO_READ);       
          theAO.ejbStore();
      }      

      //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
      //--> By Billy 22Dec2003
      // Check if Appraisal Order supported
      if(isSupportAppraisalOrder())
        updateAppraisalOrder(rowNdx, pg, srk, dcm, pp);
      //=================================================================

      pp.ejbStore();
    }
    catch(Exception ex)
    {
      setActiveMessageToAlert(BXResources.getSysMsg(
        "TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
      logger.error("Exception @AppraisalHandler.updateProperty: deal Id = " +
        pg.getPageDealId() + ", copy Id = " + pg.getPageDealCID() +
        ", property Id = " + propertyId);
      logger.error(ex);
      throw new Exception("@updateProperty");
    }
  }

  ///////////////////////////////////////////////////////////////////////////
  public void changeAppraiser(int rowNdx, String buttonName)
  {
    //  Pass PartyTypeId, PropertyId into the Deal Summary Screen
    //  Navigate to the Deal Summary Screen
    try
    {
      PageEntry pg = getTheSessionState().getCurrentPage();
      Hashtable pst = pg.getPageStateTable();
      Vector propIds = (Vector)pst.get("PROPIDS");
      
      if (disableEditButton(pg) == true) 
          return;
      
      logger.trace("--T--> @AppraiserHandler.changeAppraiser:");
      PageEntry pgEntry = null;
      if(buttonName.equals("btChangeAppraiser"))
      {
        pgEntry = setupSubPagePageEntry(pg, Sc.PGNM_PARTY_SEARCH, true);
      }
      else if(buttonName.equals("btAddAppraiser"))
      {
        pgEntry = setupSubPagePageEntry(pg, Sc.PGNM_PARTY_SEARCH, true);
      }
      else
      {
        throw new Exception("");
      }

      Hashtable subPst = pgEntry.getPageStateTable();
      subPst.put(PartySearchHandler.PROPERTY_ID_FILTER_PASSED_PARM,
        (String)propIds.elementAt(rowNdx));
      subPst.put(PartySearchHandler.PARTY_TYPE_FILTER_PASSED_PARM,
        "" + Sc.PARTY_TYPE_APPRAISER);
      logger.trace(
        "--T--> @AppraiserHandler.changeAppraiser: passing: propertyId=" +
        ((String)subPst.get(PartySearchHandler.
        PROPERTY_ID_FILTER_PASSED_PARM)) +
        ", partyTypeId=" +
        ((String)subPst.get(PartySearchHandler.PARTY_TYPE_FILTER_PASSED_PARM)));
      getSavedPages().setNextPage(pgEntry);
      navigateToNextPage();
    }
    catch(Exception ex)
    {
      setActiveMessageToAlert(BXResources.getSysMsg(
        "TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
      logger.error("Exception @AppraisalHandler.changeAppraiser: while navigating to the Party Summary/Search subpage");
      logger.error(ex);
    }
  }

  /////////////////////////////////////////////////////////////////////
  public void handleAppraiserSubmit()
  {
    try
    {
      
      String clickBySubmit = "Y"; //Appraisal READ Change , This variable is used by handleAppraisalSubmitSave() to determine
	  //if it is called by on click of 'Submit' or 'Submit to READ' button - 22 Nov 2006
      PageEntry pg = getTheSessionState().getCurrentPage();
      srk.beginTransaction();
      int dealId = pg.getPageDealId();
      
      QueryModelBase theModel = (QueryModelBase)
      (RequestManager.getRequestContext().getModelManager().getModel(
      doAppraisalPropertyModel.class));

      theModel.clearUserWhereCriteria();
      theModel.addUserWhereCriterion("dfDealId", "=",
              new String("" + pg.getPageDealId()));
      theModel.addUserWhereCriterion("dfCopyId", "=",
              new String("" + pg.getPageDealCID()));
      
      theModel.executeSelect(null);
      
      // update entities
      for(int i = 0; i < theModel.getSize(); ++i)
      {
          handleAppraisalSubmitSave(i, dealId , clickBySubmit);
      }

      //method to handle save the info when submit button clicked
      standardAdoptTxCopy(pg, true);
      srk.commitTransaction();

      // #2370 - Catherine, 4-Nov-05 --- start -----------------------
      try {
        logger.debug("--> PartySearchHandler.handleAddPartySubmit() -- Calling Workflow");
        workflowTrigger(2, srk, pg, null);
      }
      catch(Exception e)
      {
        logger.error("Exception @AppraisalHandler.handleAppraiserSubmit():");
        logger.error(e);
        setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMCONFIRM);
        return;
      }
      // #2370 - Catherine, 4-Oct-05 --- end -----------------------
      //  	***** Change by NBC Impl. Team - Version 1.3 - Start *****//

      SessionResourceKit srk = getSessionResourceKit();
	  Deal deal = null;
	   dealId = pg.getPageDealId();
	  int copyId = pg.getPageDealCID();

	  Hashtable pst = pg.getPageStateTable();
	  String flag = (String) pst.get("APPRAISER_THIS_ROW");
      
	  if (flag != null && flag.equals("Y"))
	  {
		deal = new Deal(srk, null, dealId, copyId);
		DocumentRequest.requestAppraisalPackage(srk, deal);
	  }
	  //	***** Change by NBC Impl. Team - Version 1.3 - End*****//
      navigateToNextPage(true);
      return;
    }
    catch(Exception e)
    {
      srk.cleanTransaction();
      logger.error(
        "Exception @AppraisalHandler.handleAppraiserSubmit: while Submiting");
      logger.error(e);
    }
    //  exception was encountered ... navigate away from the page (including away from parent page if sub)
    navigateAwayFromFromPage(true);
  }

  /**
   * handleSubmitToRead
   * handles Submit to READ button click event
   *
   * @return void<br>
   *  
   */

  public void handleSubmitToRead()
  {
	  try
	    {
		  
		  String clickBySubmit = "N"; //Appraisal READ Change , This variable is used by handleAppraisalSubmitSave() to determine
		  //if it is called by on click of 'Submit' or 'Submit to READ' button -22 Nov 2006
	      PageEntry pg = getTheSessionState().getCurrentPage();
	      
	      //***** Added as a part of Appraisal Submit to read CR - START *****//
	      PassiveMessage pm = runAllAppraisalBusinessRules(pg, srk);

		  if (pm != null)
		  {
				pm.setGenerate(true);
				getTheSessionState().setPasMessage(pm);

	 			if (pm.getCritical() == true)
	 			{
	 				setActiveMessageToAlert(BXResources.getSysMsg("APPRAISAL_DOCUMENT_FAILURE", theSessionState.getLanguageId()),
	                ActiveMsgFactory.ISCUSTOMCONFIRM);
	 				return;
	 			}
		  }
	      //***** Added as a part of Appraisal Submit to read CR - END *****//
		  
		  srk.beginTransaction();
		  
		  //Appraisal READ Change -Start -22 Nov 2006
		  // This change is done to save the Appraisal screen data when Submit To READ button is clicked
		  int dealId = pg.getPageDealId();
	      
	      QueryModelBase theModel = (QueryModelBase)
	      (RequestManager.getRequestContext().getModelManager().getModel(
	      doAppraisalPropertyModel.class));

	      theModel.clearUserWhereCriteria();
	      theModel.addUserWhereCriterion("dfDealId", "=",
	              new String("" + pg.getPageDealId()));
	      theModel.addUserWhereCriterion("dfCopyId", "=",
	              new String("" + pg.getPageDealCID()));
	      
	      theModel.executeSelect(null);
	      
	      // update entities
	      for(int i = 0; i < theModel.getSize(); ++i)
	      {
	    	  //method to handle save the info when Submit To READ button is clicked -22 Nov 2006
	    	  
	          handleAppraisalSubmitSave(i, dealId , clickBySubmit);
	          
	      }
		  
	      //Appraisal READ Change -End
		  
	      standardAdoptTxCopy(pg, true);
	      srk.commitTransaction();
	    
	      try {
	          logger.debug("--> PartySearchHandler.handleAddPartySubmit() -- Calling Workflow");
	          workflowTrigger(2, srk, pg, null);
	          logger.debug("--> PartySearchHandler.handleAddPartySubmit() -- After Calling Workflow");
	        }
	  
	      catch(Exception e)
	      {
	          logger.error("Exception @AppraisalHandler.handleAppraiserSubmit():");
	          logger.error(e);
	          setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
	              ActiveMsgFactory.ISCUSTOMCONFIRM);
	          return;
	        }
	      	     
		  Deal deal = null;
		  
		  int copyId = pg.getPageDealCID();

		  Hashtable pst = pg.getPageStateTable();
		  String flag = (String) pst.get("APPRAISER_THIS_ROW");

		  if (flag != null && flag.equals(APPRAISER_FLAG_SET))
		  {
			deal = new Deal(srk, null, dealId, copyId);
			DocumentRequest.requestAppraisalPackage(srk, deal);
		  }
		  //	***** Change by NBC Impl. Team - Version 1.3 - End*****//
	      navigateToNextPage(true);
	      return;
	    }
	    catch(Exception e)
	    {
	      srk.cleanTransaction();
	      logger.error(
	        "Exception @AppraisalHandler.handleAppraiserSubmit: while Submiting");
	      logger.error(e);
	    }
	    //  exception was encountered ... navigate away from the page (including away from parent page if sub)
	    navigateAwayFromFromPage(true);
	  
  }
  /////////////////////////////////////////////////////////////////////
  /**
   * runAllAppraisalBusinessRules
   * @version 1.2  <br>
   * Date: 13/10/2006 <br>
   * Author: NBC/PP Implementation Team <br>
   * Change:  <br>
   *	Added functionality to trigger AR business rules before the Appraisal document generation is requested <br>
   */
  	public PassiveMessage runAllAppraisalBusinessRules(PageEntry pg, SessionResourceKit srk)
	{
		PassiveMessage pm = new PassiveMessage();
		BusinessRuleExecutor brExec = new BusinessRuleExecutor();
		
		try
		{
			brExec.BREValidator(getTheSessionState(), pg, srk, pm, "AR-%");
			logger.trace("--T--> @AppraisalHandler.runAllAppraisalBusinessRules: AR rules");
			 
			if (pm.getNumMessages() > 0) return pm;
		}
		catch(Exception e)
		{
			logger.error("Problem encountered during appraisal, @AppraisalHandler.runAppraisalBusinessRules");
			logger.error(e);
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
			ActiveMsgFactory.ISCUSTOMCONFIRM);
		}
		return null;
	}
  

  /////////////////////////////////////////////////////////////////////

  //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
  //--> By Billy 22Dec2003
  // method to check if Appraisal Order is supported or not
  public boolean isSupportAppraisalOrder()
  {
    if((PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),"com.basis100.appraisalreview.supportappraisalorder", "N")).equals("N"))
      return false;
    else
      return true;
  }


  /**
   * supportSubmitToRead
   * checks if Submit To READ functionality is supported
   *
   *
   * @return boolean : the result of supportSubmitToRead <br>    
   */

  public boolean supportSubmitToRead ()
  {
    if((PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),"com.basis100.appraisalreview.supportSubmitToRead", "N")).equals("N"))
      return false;
    else
      return true;
  }

  // method to check if Appraisal Order is supported
  public String getIsSupportAppraisalOrderStr()
  {
    if(isSupportAppraisalOrder())
      return "true";
    else
      return "false";
  }

  // Method to update or create Appraisal Order
  private void updateAppraisalOrder(int rowNdx, PageEntry pg, SessionResourceKit srk, CalcMonitor dcm, Property p)
    throws Exception
  {
    // check if Appraisal Order exist
    AppraisalOrder theAO = p.getAppraisalOrder();
    if(theAO == null)
    {
      // Create the Appraisal Order record
      theAO = new AppraisalOrder(srk, dcm).create((PropertyPK)(p.getPk()));
    }

    // Update Appraisal Order data from input fields
    theAO.setUseBorrowerInfo(getRepeatedField("Repeated1/chUseBorrowerInfo", rowNdx));
    // Update Name of Property Owner
    theAO.setPropertyOwnerName(getRepeatedField("Repeated1/tbPropertyOwnerName", rowNdx));
    // Update Contact Name
    theAO.setContactName(getRepeatedField("Repeated1/tbAOContactName", rowNdx));
    // Update Contact Work Phone Number
    theAO.setContactWorkPhoneNum(this.phoneJoin(
      getRepeatedField("Repeated1/tbAOContactWorkPhoneNum1", rowNdx),
      getRepeatedField("Repeated1/tbAOContactWorkPhoneNum2", rowNdx),
      getRepeatedField("Repeated1/tbAOContactWorkPhoneNum3", rowNdx)
      ));
    theAO.setContactWorkPhoneNumExt(getRepeatedField("Repeated1/tbAOContactWorkPhoneNumExt", rowNdx));
    // Update Contact Cell Phone Number
    theAO.setContactCellPhoneNum(this.phoneJoin(
      getRepeatedField("Repeated1/tbAOContactCellPhoneNum1", rowNdx),
      getRepeatedField("Repeated1/tbAOContactCellPhoneNum2", rowNdx),
      getRepeatedField("Repeated1/tbAOContactCellPhoneNum3", rowNdx)
      ));
    // Update Contact Home Phone Number
    theAO.setContactHomePhoneNum(this.phoneJoin(
      getRepeatedField("Repeated1/tbAOContactHomePhoneNum1", rowNdx),
      getRepeatedField("Repeated1/tbAOContactHomePhoneNum2", rowNdx),
      getRepeatedField("Repeated1/tbAOContactHomePhoneNum3", rowNdx)
      ));
    // Update Comments to Appraiser
    theAO.setCommentsForAppraiser(getRepeatedField("Repeated1/tbCommentsForAppraiser", rowNdx));

    // check if need to update Appraisal Order status
    if(isSubmitAndSendButton == true)
    {
      // set status to Submitted
      theAO.setAppraisalStatusId(1);
    }

    theAO.ejbStore();
  }

  public void handleSubmitAndSendAOReq()
  {
    try
    {
      PageEntry pg = getTheSessionState().getCurrentPage();
      
      Deal deal = null;

      srk.beginTransaction();
      standardAdoptTxCopy(pg, true);

      deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

      //--DJ_#548_Ticket--start--//
      SessionResourceKit srk = getSessionResourceKit();

      // Get the corresponding Deal Entity
      ////Deal theDeal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());

      // validation
      PassiveMessage pm = validateAppraisalRules(deal, theSessionState, pg, srk);
      //logger.debug("AH@handleSubmitAndSendAOReq::PM#ofMsgs: " + pm.getNumMessages());
      //logger.debug("AH@handleSubmitAndSendAOReq::isCritical? " + pm.getCritical());

      if (pm.getCritical() == true)
      {
        pm.setGenerate(true);
        theSessionState.setPasMessage(pm);

        setActiveMessageToAlert(BXResources.getSysMsg("MI_VALIDATION_CANCEL_ERROR_MSG", theSessionState.getLanguageId()),
              ActiveMsgFactory.ISCUSTOMCONFIRM);

        return;
      }

      // no validate problems - determine and produce document for appraiser
      //--DJ_#548_Ticket--start--//
      //--DJ_DT_CR--start--//
      logger.debug("VLAD ===> TEST_Before :: Before send the requestInstructionToAppraiser!!");
      DocumentRequest.requestInstructionToAppraiser(srk, deal);
      logger.debug("VLAD ===> TEST_After :: Sent requestInstructionToAppraiser!!");
      //--DJ_DT_CR--end--//

      //--Ticket#652--start--//
      // Trigger workflow if Appraisal Rules are OK.
      workflowTrigger(2, srk, pg, null);
      //--Ticket#652--start--//

      srk.commitTransaction();
      navigateToNextPage(true);
      return;
    }
    catch(Exception e)
    {
      srk.cleanTransaction();
      logger.error(
        "Exception @AppraisalHandler.handleAppraiserSubmit: while Submiting");
      logger.error(e);
    }
    //  exception was encountered ... navigate away from the page (including away from parent page if sub)
    navigateAwayFromFromPage(true);
  }

  // This is only used for the Submit and send button to indicate if to send AO req.
  private boolean isSubmitAndSendButton = false;
  public void preHandlerProtocolSpecial(ViewBean currNDPage)
  {
    logger = SysLog.getSysLogger("@AppraisalHandler.preHandlerProtocolSpecial");
    isSubmitAndSendButton = true;
    preHandlerProtocol(currNDPage, false);
    isSubmitAndSendButton = false;
  }

  //--DJ_#548_Ticket--start--//
  public PassiveMessage validateAppraisalRules(Deal deal, SessionStateModelImpl theSession, PageEntry pe, SessionResourceKit srk) throws Exception
  {
      PassiveMessage       pm     = new PassiveMessage();
      BusinessRuleExecutor brExec = new BusinessRuleExecutor();

      // local scope rules ...
      int lim = 0;
      Object [] objs = null;

      // property rules
      try
      {
          Collection properties = deal.getProperties();
          lim = properties.size();
          objs = properties.toArray();
      }
      catch (Exception e) { lim = 0; } // assume no properties

      for (int i = 0; i < lim; ++i)
      {
          Property prop = (Property)objs[i];
          brExec.setCurrentProperty(prop.getPropertyId());
          brExec.setCurrentBorrower(-1);

          PassiveMessage pmProp = brExec.BREValidator(theSession, pe, srk, null, "AR-%");

          if (pmProp != null)
          {
            pm.addSeparator();
            pm.addMsg(BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_PROPERTY_PROBLEM", theSessionState.getLanguageId())  +
                      prop.formPropertyAddress(), PassiveMessage.INFO);
            pm.addAllMessages(pmProp);
            pm.addSeparator();
          }
      }

      if (pm.getNumMessages() > 0)
      {
          String tmpMsg = BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_DEAL_PROBLEM", theSessionState.getLanguageId())
            + "<br>" + "(Deal #: " + deal.getDealId() + ", " + "Scenario #: " + deal.getScenarioNumber() + ")";
          pm.setInfoMsg(tmpMsg);
          return pm;
      }

      return pm;
  }
  //--DJ_#548_Ticket--end--//

    // SEAN AVM set display Appraisal section
    private void setHiddenAppraisalSection(ViewBean thePage) {

        String prop = PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),"mosApp.MosSystem.appraisalreview.suppressappraisaldisplay", "Y");
        if (prop.equals("N")) {
            // Unset mask to display the field
            thePage.setDisplayFieldValue("Repeated1/stAppraisalHiddenStart", "");
            thePage.setDisplayFieldValue("Repeated1/stAppraisalHiddenEnd", "");
            thePage.setDisplayFieldValue("Repeated1/stAppraisalTypeHidden", "hidden");
            thePage.setDisplayFieldValue("Repeated1/stAppraisalSourceStart", "<!--");
            thePage.setDisplayFieldValue("Repeated1/stAppraisalSourceEnd", "-->");
        } else {
            // Set Mask to make the filed not dispalyed
            thePage.setDisplayFieldValue("Repeated1/stAppraisalHiddenStart", "<!--");

            thePage.setDisplayFieldValue("Repeated1/stAppraisalHiddenEnd", "-->");
            thePage.setDisplayFieldValue("Repeated1/stAppraisalTypeHidden", "visible");
            thePage.setDisplayFieldValue("Repeated1/stAppraisalSourceStart", "");
            thePage.setDisplayFieldValue("Repeated1/stAppraisalSourceEnd", "");
        }
        return;
    }

    private void setHiddenAVMSection(ViewBean thePage) {
        String prop = PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),"mosApp.MosSystem.appraisalreview.suppressavmdisplay", "Y");
        if (prop.equals("N")) {
            thePage.setDisplayFieldValue("Repeated1/stAVMOrderHidden", "");
            thePage.setDisplayFieldValue("Repeated1/stAVMOrderHiddenEnd", "");
        } else {
            thePage.setDisplayFieldValue("Repeated1/stAVMOrderHidden", "<!--");
            thePage.setDisplayFieldValue("Repeated1/stAVMOrderHiddenEnd", "-->");
        }
    }
    // SEAN AVM set display Appraisal section END

    // Appraisal Contact Information section set visible/hidden
    private void setHiddenAppraisalContactInfoSection(ViewBean thePage) {

        String prop = PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),"com.basis100.appraisalreview.supportSubmitToRead", "N");
        // ***** Change by NBC/PP Implementation Team, Defect #1419 - START *****//
        boolean isSupportSubmitToRead = prop.equalsIgnoreCase("Y");   
        String readStr = "false";
        if (isSupportSubmitToRead)
          readStr = "true";
        thePage.setDisplayFieldValue("Repeated1/stRead", readStr);
        
        doAppraisalProviderModel theModel = (doAppraisalProviderModel)
        (RequestManager.getRequestContext().getModelManager().
         getModel(doAppraisalProviderModel.class));
		
	    // product id.
	    BigDecimal productId = theModel.getDfAppraisalProductId();        
	    if (isSupportSubmitToRead || (productId != null && productId.intValue() == 4)) {	      	
	   // ***** Change by NBC/PP Implementation Team, Defect #1419 - END *****//	      	
            thePage.setDisplayFieldValue("Repeated1/stAppraisalContactInfoHiddenStart", "");
            thePage.setDisplayFieldValue("Repeated1/stAppraisalContactInfoHiddenEnd", "");
        } else {
            thePage.setDisplayFieldValue("Repeated1/stAppraisalContactInfoHiddenStart", "<!--");
            thePage.setDisplayFieldValue("Repeated1/stAppraisalContactInfoHiddenEnd", "-->");
        }
    }
    
  

    // SEAN AVM: preparing display fields for AML.
    // Date: 10/25/2006 
    // Author: NBC/PP Implementation Team 
    // Change: Added prop as a parameter to JS function for cbAppraisalProvider
    protected void prepareDisplayFields(int rowIndex) {
    	String prop = PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),"com.basis100.appraisalreview.supportSubmitToRead", "N");//Added for Sumbit to Read functionality 18-Oct-2006
        boolean isSupportSubmitToRead = prop.equalsIgnoreCase("Y");

        // avm provider.
        HtmlDisplayFieldBase df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/cbAVMProvider");
        String onChange = "id='cbAVMProvider" + rowIndex + "' " +
            "onChange='avmProviderChange(" + rowIndex + ")'";
        df.setExtraHtml(onChange);

        // AVM PRODUCT.
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/cbAVMProduct");
        df.setExtraHtml("id='cbAVMProduct" + rowIndex + "'");

        df = (HtmlDisplayFieldBase)getCurrNDPage().getDisplayField("Repeated1/tbEstimatedAppraisal");
//      #4786
        String jsValid = " onChange=\"isFieldInDecRange(0, 99999999999.99);\"";
        String jsChange = " onBlur=\"isFieldDecimal(2);\"";
        df.setExtraHtml(jsValid + jsChange);

        
        // actual appraisal value.
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/tbActualAppraisal");
        String extraId = "id='tbActualAppraisal" + rowIndex + "'";
//        #4786
        jsValid = " onChange=\"isFieldInDecRange(0, 99999999999.99);\"";
        jsChange = " onBlur=\"isFieldDecimal(2);\"";
        
        String actualAppraExtraHtml = extraId + jsValid + jsChange;
        
        // Add by Anna for PMI2 -- set Actual Appraisal Field as read only
		// under the condition: 1.From GoTo button/Task/Link History
		// 2.The Deal is Mortgage Insured. 3.MI type coded as Extended Payload = 'Y'
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			SessionResourceKit srk = getSessionResourceKit();
			Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
			if(!(getTheSessionState().getCurrentPage().isSubPage())&&(deal.getMortgageInsurerId()!=0)&&
					(DealCalcUtil.checkIfMIPremiumIsExtended(deal)))			 
			{
				// 1. Customize color
			    String changeColorStyle = new String("style=\"background-color:d1ebff\"" + " style=\"border-bottom: hidden d1ebff 0\"" + " style=\"border-left: hidden d1ebff 0\"" + " style=\"border-right: hidden d1ebff 0\"" + " style=\"border-top: hidden d1ebff 0\"" + " style=\"font-weight: bold\"");

			    // 2. Disable edition
			    String disableEdition = new String(" disabled");
			    
			    actualAppraExtraHtml = actualAppraExtraHtml + changeColorStyle + disableEdition;
			}	
		}catch(Exception e)
		{
			logger.error("Exception @AppraisalReviewHandler.prepareDisplayFields()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}
		// End	
        df.setExtraHtml(actualAppraExtraHtml);

        // date for actual appraisal value

        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/cbMonths");
        extraId = "id='cbMonths" + rowIndex + "'";
        jsValid = " onBlur=\"isFieldValidDate('tbYear', 'cbMonths', 'tbDay');\"";
        df.setExtraHtml(extraId + jsValid);
        
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/tbDay");
        extraId = "id='tbDay" + rowIndex + "'";
        jsValid = " onBlur=\"if(isFieldInIntRange(1, 31)) " +
        "isFieldValidDate('tbYear', 'cbMonths', 'tbDay');\"";
        df.setExtraHtml(extraId + jsValid);
        
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/tbYear");
        extraId = "id='tbYear" + rowIndex + "'";
        jsValid = " onBlur=\"if(isFieldInIntRange(1800, 2100)) " +
        "isFieldValidDate('tbYear', 'cbMonths', 'tbDay');\"";
        df.setExtraHtml(extraId + jsValid);
               
        // Appraisal provider/product
        // ***** Change by NBC/PP Implementation Team, Defect #1419 - START *****//
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/cbAppraisalProvider");
        df.setExtraHtml("id='cbAppraisalProvider" + rowIndex + "' " +
                        "onChange='appraisalProviderChange(" + rowIndex + "," +  isSupportSubmitToRead + ");'");
        
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/cbAppraisalProduct");
        df.setExtraHtml("id='cbAppraisalProduct" + rowIndex 
        		+ "' onChange='toggleAppraisalContactInfoSection(" + rowIndex + "," +  isSupportSubmitToRead + ");'");//Modified for Sumbit to Read functionality 18-Oct-2006
        // ***** Change by NBC/PP Implementation Team, Defect #1419 - END *****//        

        // primary borrower check box.
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/chPrimaryBorrowerInfo");
        df.setExtraHtml("id='chPrimaryBorrowerInfo" + rowIndex + "'");
        
        // special instruction.
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/tbAppraisalSpecialInst");
        extraId = "id='tbAppraisalSpecialInst" + rowIndex + "'";
        jsValid = " onChange=\'return isFieldMaxLengthSel(255)'";
        df.setExtraHtml(extraId + jsValid);
        
        // status message.
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/tbAppraisalStatusMessages");
        df.setExtraHtml("id='tbAppraisalStatusMessages" + rowIndex + "'");
        // contact info.
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/stAOContactFirstName");
        df.setExtraHtml("id='stAOContactFirstName" + rowIndex + "'");
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/stAOContactLastName");
        df.setExtraHtml("id='stAOContactLastName" + rowIndex + "'");
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/stAOPropertyOwnerName");
        df.setExtraHtml("id='stAOPropertyOwnerName" + rowIndex + "'");
        // email
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/tbAOEmail");
        df.setExtraHtml("id='tbAOEmail" + rowIndex + "'");
        // work phone
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/tbAOWorkPhoneAreaCode");
        extraId = "id='tbAOWorkPhoneAreaCode" + rowIndex + "'";
        jsValid = " onBlur=\"isFieldInteger(3);\"";
        df.setExtraHtml(extraId + jsValid);
        
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/tbAOWorkPhoneFirstThreeDigits");
        extraId = "id='tbAOWorkPhoneFirstThreeDigits" + rowIndex + "'";
        jsValid = " onBlur=\"isFieldInteger(3);\"";
        df.setExtraHtml(extraId + jsValid);
        
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/tbAOWorkPhoneLastFourDigits");
        extraId = "id='tbAOWorkPhoneLastFourDigits" + rowIndex + "'";
        jsValid = " onBlur=\"isFieldInteger(4);\"";
        df.setExtraHtml(extraId + jsValid);
        
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/tbAOWorkPhoneNumExtension");
        extraId = "id='tbAOWorkPhoneNumExtension" + rowIndex + "'";
        jsValid = " onBlur=\"isFieldInteger(0);\"";
        df.setExtraHtml(extraId + jsValid);
        // cell phone
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/tbAOCellPhoneAreaCode");
        extraId = "id='tbAOCellPhoneAreaCode" + rowIndex + "'";
        jsValid = " onBlur=\"isFieldInteger(3);\"";
        df.setExtraHtml(extraId + jsValid);
        
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/tbAOCellPhoneFirstThreeDigits");
        extraId = "id='tbAOCellPhoneFirstThreeDigits" + rowIndex + "'";
        jsValid = " onBlur=\"isFieldInteger(3);\"";
        df.setExtraHtml(extraId + jsValid);
        
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/tbAOCellPhoneLastFourDigits");
        extraId = "id='tbAOCellPhoneLastFourDigits" + rowIndex + "'";
        jsValid = " onBlur=\"isFieldInteger(4);\"";
        df.setExtraHtml(extraId + jsValid);
        
        // home phone
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/tbAOHomePhoneAreaCode");
        extraId = "id='tbAOHomePhoneAreaCode" + rowIndex + "'";
        jsValid = " onBlur=\"isFieldInteger(3);\"";
        df.setExtraHtml(extraId + jsValid);
        
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/tbAOHomePhoneFirstThreeDigits");
        extraId = "id='tbAOHomePhoneFirstThreeDigits" + rowIndex + "'";
        jsValid = " onBlur=\"isFieldInteger(3);\"";
        df.setExtraHtml(extraId + jsValid);
        
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("Repeated1/tbAOHomePhoneLastFourDigits");
        extraId = "id='tbAOHomePhoneLastFourDigits" + rowIndex + "'";
        jsValid = " onBlur=\"isFieldInteger(4);\"";
        df.setExtraHtml(extraId + jsValid);
        
        
        //extrahtml javascript for old contact info
        //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
        //--> By Billy 22Dec2003
        // JS for Work Phone # fields
        ViewBean thePage = getCurrNDPage();

        df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactWorkPhoneNum1");
        String theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
        df.setExtraHtml(theExtraHtml);

        df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactWorkPhoneNum2");
        theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
        df.setExtraHtml(theExtraHtml);

        df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactWorkPhoneNum3");
        theExtraHtml = "onBlur=\"isFieldInteger(4);\"";
        df.setExtraHtml(theExtraHtml);

        // JS for Work Phone # Extention
        df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactWorkPhoneNumExt");
        theExtraHtml = "onBlur=\"isFieldInteger(0);\"";
        df.setExtraHtml(theExtraHtml);

        // JS for Cell Phone # fields
        df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactCellPhoneNum1");
        theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
        df.setExtraHtml(theExtraHtml);

        df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactCellPhoneNum2");
        theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
        df.setExtraHtml(theExtraHtml);

        df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactCellPhoneNum3");
        theExtraHtml = "onBlur=\"isFieldInteger(4);\"";
        df.setExtraHtml(theExtraHtml);

        // JS for Cell Phone # fields
        df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactHomePhoneNum1");
        theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
        df.setExtraHtml(theExtraHtml);

        df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactHomePhoneNum2");
        theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
        df.setExtraHtml(theExtraHtml);

        df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactHomePhoneNum3");
        theExtraHtml = "onBlur=\"isFieldInteger(4);\"";
        df.setExtraHtml(theExtraHtml);
        //==============================================================
        
    }
    // SEAN AVM END

    /**
     * display the avm order section for the given property id and copy id.
     *
     * TODO: exception handling!!!
     */
    public void displayAVMOrderSection(String propertyId, int copyId)
        throws Exception {

        // populate the provider's options.
        populateProviders("Repeated1/cbAVMProvider",
                          ServiceConst.SERVICE_TYPE_VALUATION,
                          ServiceConst.SERVICE_SUB_TYPE_AVM);

        // binding the fields.
        QueryModelBase avmOrderModel = (QueryModelBase)
            (RequestManager.getRequestContext().getModelManager().
             getModel(AVMOrderModel.class));
        avmOrderModel.clearUserWhereCriteria();
        avmOrderModel.addUserWhereCriterion(AVMOrderModel.FIELD_DFAVMPROPERTYID,
                                            new Integer(propertyId));
        avmOrderModel.addUserWhereCriterion(AVMOrderModel.FIELD_DFAVMCOPYID,
                                            new Integer(copyId));
        avmOrderModel.executeSelect(null);

        _log.info("AVM Order model return SIZE: " + avmOrderModel.getSize());
        if (avmOrderModel.getSize() < 1) {
            // we got nothing.
            // do nothing to those static text fields.
            // setting up the combobox fields, need clean up the product
            // combox's options.
            cleanOptions("Repeated1/cbAVMProduct");
        } else {
            // static text fields.
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/stAVMRequestStatus",
                                     avmOrderModel.
                                     getValue(AVMOrderModel.
                                              FIELD_DFAVMREQUESTSTATUS));
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/stAVMRequestDate",
                                     avmOrderModel.
                                     getValue(AVMOrderModel.
                                              FIELD_DFAVMREQUESTDATE));
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/stAVMRequestStatusMsg",
                                     avmOrderModel.
                                     getValue(AVMOrderModel.
                                              FIELD_DFAVMREQUESTSTATUSMSG));
            // the combobox fields.
            String providerId = avmOrderModel.
                getValue(AVMOrderModel.FIELD_DFAVMPROVIDERID).toString();
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/cbAVMProvider",
                                     avmOrderModel.
                                     getValue(AVMOrderModel.
                                              FIELD_DFAVMPROVIDERID));
            // let's prepare the options.
            populateProducts("Repeated1/cbAVMProduct",
                             Integer.parseInt(providerId),
                             ServiceConst.SERVICE_TYPE_VALUATION,
                             ServiceConst.SERVICE_SUB_TYPE_AVM);
            // set up the product.
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/cbAVMProduct",
                                     avmOrderModel.
                                     getValue(AVMOrderModel.
                                              FIELD_DFAVMPRODUCTID));
        }
    }

    /**
     * returns the existing AVM reports fro the given deal and property as a formated TR.
     *
     * TODO: Exception Handling!!!!!!!
     */
    protected String getAVMReportsTrs(int dealid, int copyid,
                                      String propertyId, int rowNdx)
        throws Exception {

        StringBuffer reports = new StringBuffer();

        // prepare the query SQL.
        String querySQL =
            "SELECT DISTINCT " +
            "response.RESPONSEDATE, response.RESPONSEID, " +
            "prod.SERVICEPROVIDERID, " +
            "avm.AVMVALUE, avm.CONFIDENCELEVEL, " +
            "avm.HIGHAVMRANGE, avm.LOWAVMRANGE, avm.VALUATIONDATE " +
            "FROM " +
            "RESPONSE response, REQUEST request, " +
            "SERVICEREQUEST servicereq, SERVICEPRODUCT prod, AVMSUMMARY avm " +
            "WHERE " +
            "avm.RESPONSEID = response.RESPONSEID AND " +
            "response.DEALID = " + dealid + " AND " +
            "servicereq.REQUESTID = request.REQUESTID AND " +
            "servicereq.PROPERTYID = " + propertyId + " AND " +
            "response.REQUESTID = request.REQUESTID AND " +
            "request.SERVICEPRODUCTID = prod.SERVICEPRODUCTID AND " +
            "prod.SERVICETYPEID = 6 AND prod.SERVICESUBTYPEID = 1 " +
            "ORDER BY response.RESPONSEDATE DESC";
        if (_log.isDebugEnabled()) {
            _log.debug("Query SQL: " + querySQL);
        }

        SessionResourceKit srk = getSessionResourceKit();
        try {
            JdbcExecutor jExec = srk.getJdbcExecutor();
            int key = jExec.execute(querySQL);
            for (int i = 0; jExec.next(key); i++) {
                Date responseDate = jExec.getDate(key, "VALUATIONDATE");
                if (responseDate == null) {
                    // for legacy data
                    responseDate = jExec.getDate(key, "RESPONSEDATE");
                }
                int responseId =
                    jExec.getInt(key, "RESPONSEID");
                int providerId =
                    jExec.getInt(key, "SERVICEPROVIDERID");
                double avmValue =
                    jExec.getDouble(key, "AVMVALUE");
                String confidence =
                    jExec.getString(key, "CONFIDENCELEVEL");
                double highRange =
                    jExec.getDouble(key, "HIGHAVMRANGE");
                double lowRange =
                    jExec.getDouble(key, "LOWAVMRANGE");

                // get the provider name.
                String providerName = BXResources.
                    getPickListDescription(theSessionState.getDealInstitutionId(),"SERVICEPROVIDER", providerId,
                                           getTheSessionState().getLanguageId());

                Locale locale = new Locale(BXResources.
                                           getLocaleCode(getTheSessionState().
                                                         getLanguageId()),
                                           "");
                // the datetime format.
                String strResponseDate =
                    HtmlUtil.format(responseDate,
                                    HtmlUtil.DATETIME_FORMAT_TYPE,
                                    "MMM dd yyyy", locale);
                String currencyFmt = locale.getLanguage().equals(BXResources.FRENCH_CODE)?
                    "#,##0.00$; (-#)":"$#,##0.00; (-#)";
                String strAvmValue =
                    HtmlUtil.format(new Double(avmValue),
                                    HtmlUtil.CURRENCY_FORMAT_TYPE,
                                    currencyFmt, locale);
                String strHighRange = 
                    HtmlUtil.format(new Double(highRange),
                                    HtmlUtil.CURRENCY_FORMAT_TYPE,
                                    currencyFmt, locale);
                String strLowRange = 
                    HtmlUtil.format(new Double(lowRange),
                                    HtmlUtil.CURRENCY_FORMAT_TYPE,
                                    currencyFmt, locale);

                confidence = confidence != null ?
                    confidence :
                    BXResources.
                    getRichClientMsg("SERVICE_AVM_CONFIDENCE_NOT_AVAILABLE",
                                     getTheSessionState().getLanguageId());

                // the default image is English.
                String viewReportImageFile = "view_report.gif";
                if(getTheSessionState().getLanguageId() ==
                   Mc.LANGUAGE_PREFERENCE_FRENCH) {

                    viewReportImageFile = "view_report_fr.gif";
                }

                // preparing the reports, each report is TR.
                reports.append("<tr style=\"padding: 0px 0px 5px 0px\"");
                if (i == 0) {
                    // only for the first one: the latest report.
                    reports.append(" id=\"latestAVMReport" + rowNdx + "\">");
                } else {
                    reports.append(">");
                }
                // the report button. TODO: should change to image.
                reports.append("<td valign=top align=\"center\">")
                    .append("<a href=\"JavaScript: viewAvmReport(")
                    .append(responseId)
                    .append(")\">")
                    .append("<img alt=\"View Report\" border=\"0\" ")
                    .append("src=\"/images/")
                    .append(viewReportImageFile)
                    .append("\"/>")
                    .append("</a></td>")
                    // the provider.
                    .append("<td valign=top><font size=2 >&nbsp;&nbsp;")
                    .append(providerName)
                    .append("</font></td>")
                    // the avm value.
                    .append("<td valign=top><font size=2 >&nbsp;&nbsp;")
                    .append("" + strAvmValue)
                    .append("</font></td>")
                    // the confidence level.
                    .append("<td valign=top><font size=2 >&nbsp;&nbsp;")
                    .append("" + confidence)
                    .append("</font></td>")
                    // the high/low range.
                    .append("<td valign=top><font size=2 >&nbsp;&nbsp;")
                    .append("" + strHighRange + "/" + strLowRange)
                    .append("</font></td>")
                    // the valuation date. acturally the response date.
                    .append("<td valign=top><font size=2 >&nbsp;&nbsp;")
                    .append(strResponseDate)
                    .append("</font></td>")
                    .append("</tr>");

            }
            jExec.closeData(key);
        } catch (Exception e) {
            _log.error("JDBC Executor ERROR: ", e);
            // TODO: need exception handling.
            throw e;
        } finally {
            srk.freeResources();
        }

        return reports.toString();
    }

    /**
     * display the appraisal section
     */
    protected void displayAppraisalSection(String propertyId, int copyId)
        throws Exception {
    	
    	
        // populate the provider options.
        populateProviders("Repeated1/cbAppraisalProvider",
                          ServiceConst.SERVICE_TYPE_VALUATION,
                          ServiceConst.SERVICE_SUB_TYPE_APPRAISAL);
        // binding fields.
        doAppraisalProviderModel theModel = (doAppraisalProviderModel)
            (RequestManager.getRequestContext().getModelManager().
             getModel(doAppraisalProviderModel.class)); 
        
           
        theModel.clearUserWhereCriteria();
        theModel.addUserWhereCriterion(theModel.FIELD_DFPROPERTYID,
                                       new Integer(propertyId));
        theModel.addUserWhereCriterion(theModel.FIELD_DFAPPRAISALCOPYID,
                                       new Integer(copyId));
        
        
        
        theModel.executeSelect(null);

        _log.info("Appraisal order section size: " + theModel.getSize());
        if (theModel.getSize() < 1) {
            _log.info("no request found.");
            // clean the product options.
            cleanOptions("Repeated1/cbAppraisalProduct");
            // reset the checkbox.
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/chPrimaryBorrowerInfo", "N");
        } else {
            _log.info("Found request, get the latest one.");
            // ids.
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/stAppraisalStatusId",
                                     theModel.getDfAppraisalStatusId());
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/stAppraisalRequestId",
                                     theModel.getDfAppraisalRequestId());
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/stAppraisalContactId",
                                     theModel.getDfAppraisalContactId());
            // other fields.
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/tbAppraisalSpecialInst",
                                     theModel.getDfAppraisalSpecialInst());
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/stAppraisalRequestStatus",
                                     theModel.getDfAppraisalRequestStatus());
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/stAppraisalRequestStatusDate",
                                     theModel.getDfAppraisalRequestStatusDate());
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/stAppraisalRequestRefNum",
                                     theModel.getDfAppraisalRequestRefNum());
//          provider id.
            int providerId = theModel.getDfAppraisalProviderId().intValue();
            
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/cbAppraisalProvider",
                                     new Integer(providerId));
            	
            	
            	
            	
            
            //populate products options.
           populateProducts("Repeated1/cbAppraisalProduct", providerId,
                             ServiceConst.SERVICE_TYPE_VALUATION,
                             ServiceConst.SERVICE_SUB_TYPE_APPRAISAL);
           
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/cbAppraisalProduct",
                                     theModel.getDfAppraisalProductId());
            String messages =
                ValuationAppraisalHelper.
                getAppraisalStatusMessage(getSessionResourceKit(),
                                          theModel.
                                          getDfAppraisalRequestId().intValue(),
                                          copyId,
                                          theSessionState.getLanguageId());
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/tbAppraisalStatusMessages",
                                     messages);

            // contact info.
            BigDecimal borrowerId = theModel.getDfAppraisalBorrowerId();
            if (borrowerId == null) {
                getCurrNDPage().
                    setDisplayFieldValue("Repeated1/chPrimaryBorrowerInfo",
                                         "N");
            } else {
                getCurrNDPage().
                    setDisplayFieldValue("Repeated1/chPrimaryBorrowerInfo",
                                         "Y");
                getCurrNDPage().
                    setDisplayFieldValue("Repeated1/stAppraisalBorrowerId",
                                         borrowerId);
            }
            // owner name.
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/stAOPropertyOwnerName",
                                     theModel.getDfAOPropertyOwnerName());
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/stAOContactFirstName",
                                     theModel.getDfAOContactFirstName());
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/stAOContactLastName",
                                     theModel.getDfAOContactLastName());
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/tbAOEmail",
                                     theModel.getDfAOEmail());
            // work phone.
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/tbAOWorkPhoneAreaCode",
                                     theModel.getDfAOWorkPhoneAreaCode());
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/tbAOWorkPhoneFirstThreeDigits",
                                     theModel.getDfAOWorkPhoneFirstThreeDigits());
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/tbAOWorkPhoneLastFourDigits",
                                     theModel.getDfAOWorkPhoneLastFourDigits());
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/tbAOWorkPhoneNumExtension",
                                     theModel.getDfAOWorkPhoneNumExtension());
            // home phone
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/tbAOHomePhoneAreaCode",
                                     theModel.getDfAOHomePhoneAreaCode());
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/tbAOHomePhoneFirstThreeDigits",
                                     theModel.getDfAOHomePhoneFirstThreeDigits());
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/tbAOHomePhoneLastFourDigits",
                                     theModel.getDfAOHomePhoneLastFourDigits());
            // cell phone.
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/tbAOCellPhoneAreaCode",
                                     theModel.getDfAOCellPhoneAreaCode());
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/tbAOCellPhoneFirstThreeDigits",
                                     theModel.getDfAOCellPhoneFirstThreeDigits());
            getCurrNDPage().
                setDisplayFieldValue("Repeated1/tbAOCellPhoneLastFourDigits",
                                     theModel.getDfAOCellPhoneLastFourDigits());
        }
    }

    protected String getAppraisalReportsTrs(int dealid, int copyid,
                                            String propertyId, int rowNdx)
        throws Exception { 

        StringBuffer reports = new StringBuffer(); 

        // prepare the query SQL. 
        String querySQL = 
            "SELECT DISTINCT " + 
            "response.RESPONSEDATE, response.RESPONSEID, " + 
            "prod.SERVICEPRODUCTID, " + 
            "APPRAISALSUMMARY.APPRAISALDATE, " + 
            "APPRAISALSUMMARY.ACTUALAPPRAISALVALUE " + 
            "FROM " + 
            "RESPONSE response, REQUEST request, " + 
            "SERVICEREQUEST servicereq, SERVICEPRODUCT prod, APPRAISALSUMMARY " + 
            "WHERE " + 
            "APPRAISALSUMMARY.RESPONSEID = response.RESPONSEID AND " + 
            "response.DEALID = " + dealid + " AND " + 
            "servicereq.REQUESTID = request.REQUESTID AND " + 
            "servicereq.PROPERTYID = " + propertyId + " AND " + 
            "response.REQUESTID = request.REQUESTID AND " + 
            "request.SERVICEPRODUCTID = prod.SERVICEPRODUCTID AND " + 
            "prod.SERVICETYPEID = 6 AND prod.SERVICESUBTYPEID = 2 " + 
            "ORDER BY response.RESPONSEDATE DESC"; 
        if (_log.isDebugEnabled()) {
            _log.debug("Query SQL: " + querySQL); 
        } 

        SessionResourceKit srk = getSessionResourceKit();

        try {
            JdbcExecutor jExec = srk.getJdbcExecutor();
            int key = jExec.execute(querySQL);
            for (int i = 0; jExec.next(key); i++) {
                Date responseDate =
                    jExec.getDate(key, "RESPONSEDATE");
                int responseId =
                    jExec.getInt(key, "RESPONSEID");
                int productId =
                    jExec.getInt(key, "SERVICEPRODUCTID");
                Date appraisalDate =
                    jExec.getDate(key, "APPRAISALDATE");
                double actualAppraisalValue =
                    jExec.getDouble(key, "ACTUALAPPRAISALVALUE"); 

                // get the product name.
                String productName = BXResources.
                    getPickListDescription(theSessionState.getDealInstitutionId(),"SERVICEPRODUCT", productId,
                                           getTheSessionState().
                                           getLanguageId());
                // the default image is English.
                String viewReportImageFile = "view_report.gif";
                if(getTheSessionState().getLanguageId() ==
                   Mc.LANGUAGE_PREFERENCE_FRENCH) {
                    viewReportImageFile = "view_report_fr.gif";
                }

                Locale locale = new Locale(BXResources.
                                           getLocaleCode(getTheSessionState().
                                                         getLanguageId()),
                                           "");
                String currencyFmt = locale.getLanguage().equals(BXResources.FRENCH_CODE)?
                        "#,##0.00$; (-#)":"$#,##0.00; (-#)";

                String responseDateStr =
                    HtmlUtil.format(responseDate,
                                    HtmlUtil.DATETIME_FORMAT_TYPE,
                                    "MMM dd yyyy / HH:mm", locale);
                String appraisalDateStr = 
                    HtmlUtil.format(appraisalDate,
                                    HtmlUtil.DATETIME_FORMAT_TYPE,
                                    "MMM dd yyyy", locale);

                String strAppraisalValue =
                    HtmlUtil.format(new Double(actualAppraisalValue),
                                    HtmlUtil.CURRENCY_FORMAT_TYPE,
                                    currencyFmt, locale);
                
                // preparing the reports, each report is TR.
                reports.append("<tr style=\"padding: 0px 0px 5px 0px\"");
                if (i == 0) {
                    // only for the first one: the latest report.
                    reports.append(" id=\"latestAppraisalReport" +
                                   rowNdx + "\">");
                } else {
                    reports.append(">");
                }
                // the report button
                reports.append("<td valign=top align=\"center\">")
                    .append("<a href=\"JavaScript: viewAppraisalReport(")
                    .append(responseId)
                    .append(")\">")
                    .append("<img alt=\"View Report\" border=\"0\" ")
                    .append("src=\"/images/")
                    .append(viewReportImageFile)
                    .append("\"/>")
                    .append("</a></td>")
                    // the provider.
                    .append("<td valign=top><font size=2 >&nbsp;&nbsp;")
                    .append(responseDateStr)
                    .append("</font></td>")
                    // the avm value.
                    .append("<td valign=top><font size=2 >&nbsp;&nbsp;")
                    .append("" + productName)
                    .append("</font></td>")
                    // the actua level.
                    .append("<td valign=top><font size=2 >&nbsp;&nbsp;")
                    .append(appraisalDateStr)
                    .append("</font></td>")
                    // the valuation date. acturally the response date.
                    .append("<td valign=top><font size=2 >&nbsp;&nbsp;")
                    .append("" + strAppraisalValue)
                    .append("</font></td>")
                    .append("</tr>");
            }
            jExec.closeData(key);
        } catch (Exception e) {
            _log.error("JDBC Executor ERROR: ", e);
            // TODO: need exception handling.
        }

        return reports.toString(); 
    }

    /**
     * populate provider options.
    */
    protected void populateProviders(String fieldName, int type, int subtype) {

        // populate the provider's options.
    	// Get subtypes
    	// Run a loop for sub-types. For each subtype
    	
        Map providers = ValuationAppraisalHelper.
            getValidProviders(getSessionResourceKit(),
                              theSessionState.getLanguageId(),
                              type, subtype);
        OptionList providerOptions = new OptionList();
        Set ids = providers.keySet();
        for (Iterator i = ids.iterator(); i.hasNext(); ) {
            String id = (String) i.next();
            String label = (String) providers.get(id);
            providerOptions.add(new Option(label, id));
        }
        ComboBox cbProvider = (ComboBox) getCurrNDPage().getChild(fieldName);
        cbProvider.setOptions(providerOptions);
        
    }

    /**
     * populate product options.
     */
    protected void populateProducts(String fieldName, int providerId,
                                    int type, int subtype) {

        // let's prepare the options.
        OptionList theProductOptions = new OptionList();
        Map products = ValuationAppraisalHelper.
            getValidProducts(getSessionResourceKit(),
                             theSessionState.getLanguageId(),
                             type, subtype, providerId);
        for (Iterator i = products.keySet().iterator(); i.hasNext(); ) {
            Integer id = (Integer) i.next();
            String label = (String) products.get(id);
            theProductOptions.add(new Option(label, id.toString()));
        }
        // populate the product options.
        ComboBox cbProduct =
            (ComboBox) getCurrNDPage().getChild(fieldName);
        cbProduct.setOptions(theProductOptions);
    }

    /**
     * clean up the options for the given field.
     */
    protected void cleanOptions(String fieldName) {

        ComboBox box = (ComboBox) getCurrNDPage().getChild(fieldName);
        box.setOptions(new OptionList());
    }
    

    /**
     * method the save the info when submit button is clicked
     * @param dealId
     */
	protected void handleAppraisalSubmitSave(int rowNdx, int dealId , String cbSubmit) {
		
		String pageName = getCurrNDPage().getName();
        
        int requestId = -1;
        int contactId = -1;
        int productId = -1;
        PageEntry pg = getTheSessionState().getCurrentPage();
        
        int copyId = pg.getPageDealCID();
        
        int propertyId =  Integer.parseInt((getDisplayField(rowNdx, pageName, 
                "Repeated1", "hbPropertyId")));
        /*Appraisal READ Change - Check if the request coming by clicking 'Submit' button or 
        'Submit to Read' button ,If request coming from 'Submit to Read' button , then call getReadAppraisalRequests()-22 Nov 2006 */
        Map requestResult = null;
        
        if(cbSubmit.equals("N")){
        	requestResult = ValuationAppraisalHelper.
            getReadAppraisalRequests(srk, pg.getPageDealId(), copyId, propertyId);
        	        	
        }
        else {
        requestResult = ValuationAppraisalHelper.
            getAppraisalRequests(srk, pg.getPageDealId(), copyId, propertyId);
        }
        
        Set ids = requestResult.keySet();
        
        for (Iterator i = ids.iterator(); i.hasNext(); ) {
            Integer requestInt = (Integer)i.next();
            requestId = requestInt.intValue();
            Integer contactInt = (Integer)requestResult.get(requestInt);
            contactId = contactInt.intValue();
        }
        
        try{
            Request requestEntity = new Request(srk);
            
            String productStr =  getDisplayField(rowNdx, pageName, 
                    "Repeated1", "cbAppraisalProduct");
         
            /*
            NBC Implemetation /PP - Team 6 -22 Nov 2006
            Following code change is done for READ appraisal CR.
            Check 1. When the input from cbAppraisalProduct is null,
            then check if the user has clicked Submit To Read or Submit button,If Submit to READ has clicked, the 
            productid will be set as READ product and save as READ data else the data will not be saved in request table
            
            Check 2.When there is a input through cbAppraisalProduct,  
            then check if the user has clicked Submit To Read or Submit button,If Submit to READ has clicked, the 
            productid will be set as READ product and saved as READ data else the data will be saved as non-READ Data in request table
            -------------------------------------Start----------------------------------------
            */    
               
            if (cbSubmit.equals("N")) {
        	  productId = ServiceConst.SERVICE_PRODUCT_READ_APPRAISAL; //Appraisal READ change
        	}
            
            else {
            
            if (productStr==null) {
                productId = 0;
                }
            else { 
                productId = Integer.parseInt(productStr);
                }

            }
            
             
            /* -------------------------------------End---------------------------------------- */	
             	                
            if(productId>0){
                if (requestId > 0) {
                    requestEntity = requestEntity.
                        findByPrimaryKey(new RequestPK(requestId, copyId));
    
                    // check the request's status to see what we need.
                    int reqStatus = requestEntity.getRequestStatusId();
                    
                    switch (reqStatus) {
                    case ServiceConst.REQUEST_STATUS_COMPLETED:
                    case ServiceConst.REQUEST_STATUS_COMPLETED_CONFIRMED:
                    case ServiceConst.REQUEST_STATUS_CANCELLATION_ACCEPTED:
                    case ServiceConst.REQUEST_STATUS_CANCELLATION_CONFIRMED:
                                       	
                        
                        _log.info("create request entity for status: " +
                                  requestEntity.getRequestStatusId());
                        
                        createRequestEntity(rowNdx, dealId, copyId, productId);
                        break;
                    default:
                        _log.info("update the requests!" + " For status: "
                                  + requestEntity.getRequestStatusId());
                    
                        // for following status, we need create new request.
                    	//Appraisal READ Change - Passing request status as additional parameter -22 Nov 2006
                    	
                        requestEntity = updateRequestEntity(rowNdx, requestEntity,
                                        contactId, productId , reqStatus );
                        break;
                    }
                } else {
                    _log.info(" create a " + "new request entity!");
                    
                    createRequestEntity(rowNdx, dealId, copyId, productId);
                }
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
  
    /**
     * create the request entity.
     */
    private void createRequestEntity(int rowNdx, int dealId, int copyId, int productId)
        throws Exception {

        _log.info("Trying to create a new Request entity ...");
        try {
        	String pageName = getCurrNDPage().getName();
            // creating request entity.
            _log.info("creating request for deal: [dealid=" +
                      dealId + ", copyid=" + copyId + "]...");
            Request requestEntity = new Request(srk);

            //Appraisal READ change-22 Nov 2006
            if (productId == ServiceConst.SERVICE_PRODUCT_READ_APPRAISAL){
            requestEntity.create(new DealPK(dealId, copyId),
                                 ServiceConst.REQUEST_STATUS_SUBMIT_TO_READ,
                                 new Date(), srk.getExpressState().getUserProfileId());
            }
            else 
            {
            	requestEntity.create(new DealPK(dealId, copyId),
                        ServiceConst.REQUEST_STATUS_NOT_SUBMITTED,
                        new Date(), srk.getExpressState().getUserProfileId());
            }
            if (_log.isDebugEnabled())
                _log.debug("Request Entity Created: " +
                           requestEntity.toString());

            requestEntity.setChannelId(3);  // CHANNELID_appraisal
            requestEntity.setServiceProductId(productId);

            requestEntity.
                setRequestTypeId(ServiceConst.
                                 REQUEST_TYPE_APPRAISAL_REQUEST_INT);
            requestEntity.setRequestDate(new Date());
            requestEntity.setStatusDate(new Date());
            // current user profile id.
            requestEntity.setUserProfileId(srk.getExpressState().getUserProfileId());
            requestEntity.ejbStore();

            // creating servicerequest entity.
            _log.info("creating servicerequest for request: " +
                      requestEntity.getRequestId());
            ServiceRequest serviceRequest = new ServiceRequest(srk);
            serviceRequest = serviceRequest.create(requestEntity.getRequestId(),
                                                   requestEntity.getCopyId());
            // update other info.
            // should get the primary propery's property id.
            String specialInst =  getDisplayField(rowNdx, pageName, 
                    "Repeated1", "tbAppraisalSpecialInst");
             	
            String propertyId =  getDisplayField(rowNdx, pageName, 
                    "Repeated1", "hbPropertyId");
                        
            serviceRequest.setSpecialInstructions(specialInst);
			serviceRequest.setPropertyId(Integer.parseInt(propertyId));
            serviceRequest.ejbStore();
            
            if (_log.isDebugEnabled())
                _log.debug("ServiceRequest Created: " +
                           serviceRequest.toString());

            String primaryBorrowerInfo = getDisplayField(rowNdx, pageName, 
                    "Repeated1", "chPrimaryBorrowerInfo");
            boolean needPrimary = false;
            
            if(primaryBorrowerInfo!=null)
                needPrimary = true;
                            
            // create servicerequest contact
            ServiceRequestContact contact = new ServiceRequestContact(srk);
            if (needPrimary) {
                contact = createContactByBorrower(requestEntity, contact);
            } else {
                contact = contact.create(requestEntity.getRequestId(),
                                         requestEntity.getCopyId());
                contact = updateServicRequestContact(rowNdx, contact);
            }
            contact.ejbStore();
            _log.info("Successfully created entities!");
                  
        } catch (Exception e) {
            _log.error("Any other exceptions!", e);
            e.printStackTrace();
        }
    }
    
    /**
     * create service request contact by use the borrower.
     */
    private ServiceRequestContact
        createContactByBorrower(Request requestEntity,
                                ServiceRequestContact contact)
        throws Exception {

        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey(new DealPK(requestEntity.getDealId(), 
                								requestEntity.getCopyId()));
        for (Iterator i = deal.getBorrowers().iterator(); i.hasNext(); ) {
            Borrower borrower = (Borrower) i.next();
            if (borrower.isPrimaryBorrower()) {
                contact = contact.
                    createWithBorrowerAssoc(requestEntity.getRequestId(),
                                            borrower.getBorrowerId(),
                                            requestEntity.getCopyId());
                updateServicRequestContactBorrower(contact, borrower);
                break;
            }
        }

        return contact;
    }
    
    /**
     * update the service request eneity.
     */
	private Request updateRequestEntity(int rowNdx, Request requestEntity,
                                        int contactId, int productId, int reqStatusid )
        throws Exception {
		
        String pageName = getCurrNDPage().getName();
        if (_log.isDebugEnabled())
            _log.debug("Updating request entity...");
        // generic update.
        requestEntity.setServiceProductId(productId);
        requestEntity.setRequestDate(new Date());
        requestEntity.setUserProfileId(srk.getExpressState().getUserProfileId());
        
        /* Appraisal READ Change - Check if Request Status is Submit to READ Appraisal and 
        if it is then set the payload type as READ Appraisal Request - 22 Nov 2006 */ 
        if (reqStatusid == ServiceConst.REQUEST_STATUS_SUBMIT_TO_READ)
        {
        	requestEntity.
            setPayloadTypeId(ServiceConst.
                         SERVICE_PAYLOAD_TYPE_READ_APP_REQUEST);
        	
        }
        else{
        	requestEntity.
            setPayloadTypeId(ServiceConst.
                         SERVICE_PAYLOAD_TYPE_APP_REQUEST);
        }
        requestEntity.
            setServiceTransactionTypeId(ServiceConst.
                                    SERVICE_TRANSACTION_TYPE_REQUEST);
        requestEntity.ejbStore();

        if (_log.isDebugEnabled())
            _log.debug("Updating ServiceRequest...");
        // get the existing ServiceRequest.

        ServiceRequest serviceRequest = requestEntity.getServiceRequest();
        if (serviceRequest != null) {

            String specialInst = getDisplayField(rowNdx, pageName, 
                    "Repeated1", "tbAppraisalSpecialInst");
            serviceRequest.setSpecialInstructions(specialInst);
            serviceRequest.ejbStore();
        }

        if (_log.isDebugEnabled())
            _log.debug("Updating ServiceRequestContact...");
        // get the existing contact.
        ServiceRequestContact contact =
            new ServiceRequestContact(srk, contactId,
                   requestEntity.getRequestId(), requestEntity.getCopyId());
        // the property owner name is not related to borrower.

        String ownerName = getDisplayField(rowNdx, pageName, 
                "Repeated1", "stAOPropertyOwnerName");
        contact.setPropertyOwnerName(ownerName);
        // don't touch comments for now.

        
        String primaryBorrowerInfo = getDisplayField(rowNdx, pageName, 
                "Repeated1", "chPrimaryBorrowerInfo");
        
        
        boolean needPrimary = false;
        
        if(primaryBorrowerInfo!=null)
            needPrimary = true;
            
        if (needPrimary) {
            _log.info("Updating request entity by primary borrower");
            if (contact.getBorrowerId() > 0) {
            	 
                _log.info("we already use primary borrower...");
                // do nothing here...
            } else {
            	
                _log.info("without to with primary borrower create ...");
                // find the borrower info.
                Borrower borrower = null;
                Deal deal = new Deal(srk, null);
                deal = deal.findByPrimaryKey(new DealPK(requestEntity.getDealId(), 
                        								requestEntity.getCopyId()));
                for (Iterator i = deal.getBorrowers().iterator();
                     i.hasNext(); ) {
                    borrower = (Borrower) i.next();
                    if (borrower.isPrimaryBorrower()) {
                        break;
                    }
                }
                if (borrower == null) {
                    throw new RequestProcessException("No Primary Borrower!");
                }
                
                requestEntity.createChildAssocs(borrower.getBorrowerId());
                
                contact.setBorrowerId(borrower.getBorrowerId());
                
                updateServicRequestContactBorrower(contact, borrower);
                
            }
            contact.ejbStore();
        } else {
            _log.info("Updating request entity without primary borrower");
                // update the contact info.
            
            contact = updateServicRequestContact(rowNdx, contact);
            
            contact.ejbStore();
            if (contact.getBorrowerId() > 0) {
                _log.info("with to without primary borrower. remove...");
                contact.cutBorrowerAssoc();
                requestEntity.removeChildAssocs();
            } else {
                _log.info("without to without primary borrower. do thing...");
            }
        }
        return requestEntity;
    }
    
    /**
     * update  the contact info based on the request.
     * 
     * @version<br> 
     * Date: 12/18/2006 <br>
     * Change: Added save for Property Owner name, ticket 1710
     */
    private ServiceRequestContact
        updateServicRequestContact(int rowNdx, ServiceRequestContact 
                serviceRequestContactEntity) {
        String pageName = getCurrNDPage().getName();
        
        String fieldValue = getDisplayField(rowNdx, pageName, 
                "Repeated1", "stAOContactFirstName");
        serviceRequestContactEntity.setFirstName(fieldValue);
        
        fieldValue = getDisplayField(rowNdx, pageName, 
                "Repeated1", "stAOContactLastName");
        serviceRequestContactEntity.setLastName(fieldValue);
        
        fieldValue = getDisplayField(rowNdx, pageName, 
                "Repeated1", "tbAOEmail");
        serviceRequestContactEntity.setEmailAddress(fieldValue);
        
        fieldValue = getDisplayField(rowNdx, pageName, 
                "Repeated1", "tbAOWorkPhoneAreaCode");
        serviceRequestContactEntity.setWorkAreaCode(fieldValue);
        
        String firstNum = getDisplayField(rowNdx, pageName, 
                "Repeated1", "tbAOWorkPhoneFirstThreeDigits");
        String lastNum = getDisplayField(rowNdx, pageName, 
                "Repeated1", "tbAOWorkPhoneLastFourDigits");
        serviceRequestContactEntity.setWorkPhoneNumber(firstNum+lastNum);
        
        fieldValue = getDisplayField(rowNdx, pageName, 
                "Repeated1", "tbAOWorkPhoneNumExtension");
    	serviceRequestContactEntity.setWorkExtension(fieldValue);

        fieldValue = getDisplayField(rowNdx, pageName, 
                "Repeated1", "tbAOCellPhoneAreaCode");
        serviceRequestContactEntity.setCellAreaCode(fieldValue);
        
        firstNum = getDisplayField(rowNdx, pageName, 
                "Repeated1", "tbAOCellPhoneFirstThreeDigits");
        lastNum = getDisplayField(rowNdx, pageName, 
                "Repeated1", "tbAOCellPhoneLastFourDigits");
        serviceRequestContactEntity.setCellPhoneNumber(firstNum+lastNum);
        
        fieldValue = getDisplayField(rowNdx, pageName, 
                "Repeated1", "tbAOHomePhoneAreaCode");
        serviceRequestContactEntity.setHomeAreaCode(fieldValue);

        
        firstNum = getDisplayField(rowNdx, pageName, 
                "Repeated1", "tbAOHomePhoneFirstThreeDigits");
        lastNum = getDisplayField(rowNdx, pageName, 
                "Repeated1", "tbAOHomePhoneLastFourDigits");
        serviceRequestContactEntity.setHomePhoneNumber(firstNum+lastNum);
        
        //***** Change by NBC/PP Implementation Team - Ticket #1710 - Start *****//
        fieldValue = getDisplayField(rowNdx, pageName, 
                "Repeated1", "stAOPropertyOwnerName");
        serviceRequestContactEntity.setPropertyOwnerName(fieldValue);
        //***** Change by NBC/PP Implementation Team - Ticket #1710 - End *****//
        
        return serviceRequestContactEntity;
    }

    /**
     * update the contact inf based on the primary borrower id.
     */
    private ServiceRequestContact
        updateServicRequestContactBorrower(ServiceRequestContact contact,
                                   Borrower borrower) {

        if(_log.isDebugEnabled())
            _log.debug("update contact record based on the borrower info");
        contact.setFirstName(borrower.getBorrowerFirstName());
        contact.setLastName(borrower.getBorrowerLastName());
        // email address.
        contact.setEmailAddress(borrower.getBorrowerEmailAddress());
        // work phone.
        String phoneNumber = borrower.getBorrowerWorkPhoneNumber();
        if ((phoneNumber != null) && phoneNumber.trim().length() > 0) {
            contact.setWorkAreaCode(phoneNumber.substring(0, 3));
            contact.setWorkPhoneNumber(phoneNumber.substring(3));
        } else {
            contact.setWorkAreaCode("");
            contact.setWorkPhoneNumber("");
        }
        contact.setWorkExtension(borrower.getBorrowerWorkPhoneExtension());
        // no cell phone number, make it empty.
        contact.setCellAreaCode("");
        contact.setCellPhoneNumber("");
        // home phone
        phoneNumber = borrower.getBorrowerHomePhoneNumber();
        if ((phoneNumber != null) && phoneNumber.trim().length() > 0) {
            contact.setHomeAreaCode(phoneNumber.substring(0, 3));
            contact.setHomePhoneNumber(phoneNumber.substring(3));
        } else {
            contact.setHomeAreaCode("");
            contact.setHomePhoneNumber("");
        }
        // we will not touch the coments.

        return contact;
    }
    
    /**
     * method to get display field value in a tile, 
     * @param rowNdx
     * @param pageName
     * @param tileName
     * @param fieldName
     * @return
     */
    private String getDisplayField(int rowNdx, String pageName, 
            String tileName, String fieldName) {
    	HttpServletRequest _req = RequestManager.getRequest();
    	String paramName = pageName + "_" + tileName + "[" + 
                rowNdx +"]_" + fieldName;
    	String field = _req.getParameter(paramName);
        return field;
    }

    /**
     * flip request status when status is completed
     * @param userId
     * @param dealId
     * @param copyId
     * @throws Exception
     */
    public void completeTaskStatus(String propertyIdStr, int rowNdx)
    throws Exception
    {   
        PageEntry pg = getTheSessionState().getCurrentPage();
        int copyId = pg.getPageDealCID();
        int requestId = 0;
        
        int propertyId = Integer.parseInt(propertyIdStr);
            
        Map requestResult = ValuationAppraisalHelper.getAppraisalRequests(srk,
                pg.getPageDealId(), copyId, propertyId);

        Set ids = requestResult.keySet();

        for (Iterator i = ids.iterator(); i.hasNext();) 
        {   Integer requestInt = (Integer) i.next();
            requestId = requestInt.intValue();
        }
          
        if ( requestId > 0 ) 
        {   srk.beginTransaction();
            Request requestEntity = new Request(srk);
            requestEntity = requestEntity
                .findByPrimaryKey(new RequestPK(requestId, copyId));
            if(requestEntity.getUserProfileId() == srk.getExpressState().getUserProfileId())
            {   switch (requestEntity.getRequestStatusId()) 
                {   case ServiceConst.REQUEST_STATUS_COMPLETED:
                     _log.info("change request status to completed: "
                                + requestEntity.getRequestStatusId());
                         requestEntity
                                .setRequestStatusId(ServiceConst
                                    .REQUEST_STATUS_COMPLETED_CONFIRMED);
                         requestEntity.ejbStore();
                         break;
                     default:
                         break;
                 }
            }
            srk.commitTransaction();
        } 
    }
    
    
}
