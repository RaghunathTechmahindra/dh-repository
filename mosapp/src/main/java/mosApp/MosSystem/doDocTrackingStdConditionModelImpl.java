package mosApp.MosSystem;

/**
 * 11/Oct/2007 DVG #DG638 LEN217180: Malcolm Whitton Condition Text exceeds 4000 characters 
 */

import java.sql.Clob;
import java.sql.SQLException;

import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 * *
 *
 */
//--Release2.1--TD_DTS_CR//
//// TD requests to add ConditionReview functionalities to the DocumentTracking screen.

public class doDocTrackingStdConditionModelImpl extends QueryModelBase
	implements doDocTrackingStdConditionModel
{
	/**
	 *
	 *
	 */
	public doDocTrackingStdConditionModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{

    logger = SysLog.getSysLogger("DODTSTDC");
    logger.debug("DODTSTDC@afterExecute::Size: " + this.getSize());
    logger.debug("DODTSTDC@afterExecute::SQLTemplate: " + this.getSelectSQL());

		//#DG638 reset clob as string		
		if( queryType != QUERY_TYPE_SELECT)	
			return;
		while(next())    {
			String condText;
			//================================================================
			//CLOB Performance Enhancement June 2010
			Object docTextVarchar = getValue(FIELD_DFDOCUMENTTEXTLITE);
			if( docTextVarchar != null) {
				condText = (String)docTextVarchar;
			} else {
		  		try {
		  			Clob condTextObj = (Clob)getValue(FIELD_DFDOCUMENTTEXT);
		  			condText = TypeConverter.stringFromClob(condTextObj);
		  		}
		  		catch (Exception e) {
		  			SysLog.error(getClass(), StringUtil.stack2string(e), "DTSConIM");
		  			condText = null;
		  		}
			}
			setDfDocumentText(condText);
		}
		//Reset Location
		beforeFirst();
		//#DG638 end		
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAction()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFACTION);
	}


	/**
	 *
	 *
	 */
	public void setDfAction(java.math.BigDecimal value)
	{
		setValue(FIELD_DFACTION,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDocumentLabel()
	{
		return (String)getValue(FIELD_DFDOCUMENTLABEL);
	}


	/**
	 *
	 *
	 */
	public void setDfDocumentLabel(String value)
	{
		setValue(FIELD_DFDOCUMENTLABEL,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDocumentText()
	{
		return (String)getValue(FIELD_DFDOCUMENTTEXT);
	}


	/**
	 *
	 *
	 */
	public void setDfDocumentText(String value)
	{
		setValue(FIELD_DFDOCUMENTTEXT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfStdDocTrackId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSTDDOCTRACKID);
	}


	/**
	 *
	 *
	 */
	public void setDfStdDocTrackId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSTDDOCTRACKID,value);
	}


  public java.math.BigDecimal getDfLanguagePrederenceId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLANGUAGEPREFERENCEID);
	}

	public void setDfLanguagePrederenceId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLANGUAGEPREFERENCEID,value);
	}

	/**
	 *
	 *
	 */
	public String getDfByPass()
	{
		return (String)getValue(FIELD_DFBYPASS);
	}

	/**
	 *
	 *
	 */
	public void setDfByPass(String value)
	{
		setValue(FIELD_DFBYPASS,value);
	}

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL DOCUMENTTRACKING.DOCUMENTSTATUSID, DOCUMENTTRACKING.DEALID, " +
    "DOCUMENTTRACKING.COPYID, DOCUMENTTRACKINGVERBIAGE.DOCUMENTLABEL, " +
    //================================================================
    //CLOB Performance Enhancement June 2010
    "DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXT, DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXTLITE, " +
    "DOCUMENTTRACKING.DOCUMENTTRACKINGID, " +
    //================================================================
    //// Added LanguagePreferenceId, StApprovalByPass
    "DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID, CONDITION.STAPPROVALBYPASS " +
    "FROM CONDITION, DOCUMENTTRACKING, DOCUMENTTRACKINGVERBIAGE, DOCUMENTSTATUS " +
    "__WHERE__  " +
    "ORDER BY DOCUMENTTRACKING.DOCUMENTTRACKINGID  ASC";

  public static final String MODIFYING_QUERY_TABLE_NAME="CONDITION, DOCUMENTTRACKING, DOCUMENTTRACKINGVERBIAGE";
	public static final String STATIC_WHERE_CRITERIA=
	    "DOCUMENTTRACKING.INSTITUTIONPROFILEID = DOCUMENTTRACKINGVERBIAGE.INSTITUTIONPROFILEID " + 
	    " AND DOCUMENTTRACKING.DOCUMENTTRACKINGID = DOCUMENTTRACKINGVERBIAGE.DOCUMENTTRACKINGID " +        
	    " AND DOCUMENTTRACKING.COPYID = DOCUMENTTRACKINGVERBIAGE.COPYID " + 
        " AND DOCUMENTTRACKING.INSTITUTIONPROFILEID = CONDITION.INSTITUTIONPROFILEID " + 
        " AND DOCUMENTTRACKING.CONDITIONID = CONDITION.CONDITIONID " +    
        " AND DOCUMENTSTATUS.DOCUMENTSTATUSID = DOCUMENTTRACKING.DOCUMENTSTATUSID " +
        " AND DOCUMENTTRACKING.DOCUMENTTRACKINGSOURCEID = 1 " +
        " AND DOCUMENTSTATUS.DOCUMENTSTATUSID = 0 ";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFACTION="DOCUMENTTRACKING.DOCUMENTSTATUSID";
	public static final String COLUMN_DFACTION="DOCUMENTSTATUSID";
	public static final String QUALIFIED_COLUMN_DFDEALID="DOCUMENTTRACKING.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DOCUMENTTRACKING.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFDOCUMENTLABEL="DOCUMENTTRACKINGVERBIAGE.DOCUMENTLABEL";
	public static final String COLUMN_DFDOCUMENTLABEL="DOCUMENTLABEL";
	public static final String QUALIFIED_COLUMN_DFDOCUMENTTEXT="DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXT";
	public static final String COLUMN_DFDOCUMENTTEXT="DOCUMENTTEXT";
	//================================================================
	//CLOB Performance Enhancement June 2010
	public static final String QUALIFIED_COLUMN_DFDOCUMENTTEXTLITE="DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXTLITE";
	public static final String COLUMN_DFDOCUMENTTEXTLITE="DOCUMENTTEXTLITE";
	//================================================================
	public static final String QUALIFIED_COLUMN_DFSTDDOCTRACKID="DOCUMENTTRACKING.DOCUMENTTRACKINGID";
	public static final String COLUMN_DFSTDDOCTRACKID="DOCUMENTTRACKINGID";
	//--Release2.1--//
  //// Added LanguagePreferenceId DataField
  public static final String QUALIFIED_COLUMN_DFLANGUAGEPREFERENCEID="DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID";
	public static final String COLUMN_DFLANGUAGEPREFERENCEID="LANGUAGEPREFERENCEID";
	public static final String QUALIFIED_COLUMN_DFBYPASS="CONDITION.STAPPROVALBYPASS";
	public static final String COLUMN_DFBYPASS="STAPPROVALBYPASS";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFACTION,
				COLUMN_DFACTION,
				QUALIFIED_COLUMN_DFACTION,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOCUMENTLABEL,
				COLUMN_DFDOCUMENTLABEL,
				QUALIFIED_COLUMN_DFDOCUMENTLABEL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOCUMENTTEXT,
				COLUMN_DFDOCUMENTTEXT,
				QUALIFIED_COLUMN_DFDOCUMENTTEXT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		//================================================================
		//CLOB Performance Enhancement June 2010
			FIELD_SCHEMA.addFieldDescriptor(
					new QueryFieldDescriptor(
						FIELD_DFDOCUMENTTEXTLITE,
						COLUMN_DFDOCUMENTTEXTLITE,
						QUALIFIED_COLUMN_DFDOCUMENTTEXTLITE,
						String.class,
						false,
						false,
						QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
						"",
						QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
						""));
		  //================================================================

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTDDOCTRACKID,
				COLUMN_DFSTDDOCTRACKID,
				QUALIFIED_COLUMN_DFSTDDOCTRACKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    //// Added LanguagePreferenceId DataField
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLANGUAGEPREFERENCEID,
				COLUMN_DFLANGUAGEPREFERENCEID,
				QUALIFIED_COLUMN_DFLANGUAGEPREFERENCEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    //// Added DfByPass DataField
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBYPASS,
				COLUMN_DFBYPASS,
				QUALIFIED_COLUMN_DFBYPASS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

