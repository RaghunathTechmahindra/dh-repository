package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgDealSummaryRepeatedLiabDetailsTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealSummaryRepeatedLiabDetailsTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(2);
		setPrimaryModelClass( doDetailViewBorrowersModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStLDBorrType().setValue(CHILD_STLDBORRTYPE_RESET_VALUE);
		getStLDBorrFlag().setValue(CHILD_STLDBORRFLAG_RESET_VALUE);
		getStLDBorrSalutation().setValue(CHILD_STLDBORRSALUTATION_RESET_VALUE);
		getStLDBorrFirstName().setValue(CHILD_STLDBORRFIRSTNAME_RESET_VALUE);
		getStLDBorrMidInitial().setValue(CHILD_STLDBORRMIDINITIAL_RESET_VALUE);
		getStLDBorrLastName().setValue(CHILD_STLDBORRLASTNAME_RESET_VALUE);
		getStLDBorrSuffix().setValue(CHILD_STLDBORRSUFFIX_RESET_VALUE);
		getCreditBureauLiability().resetChildren();
		getStBeginHideCBLiabSection().setValue(CHILD_STBEGINHIDECBLIABSECTION_RESET_VALUE);
		getStEndHideCBLiabSection().setValue(CHILD_STENDHIDECBLIABSECTION_RESET_VALUE);
		getLiability().resetChildren();
		getStBeginHideLiabSection().setValue(CHILD_STBEGINHIDELIABSECTION_RESET_VALUE);
		getStEndHideLiabSection().setValue(CHILD_STENDHIDELIABSECTION_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STLDBORRTYPE,StaticTextField.class);
		registerChild(CHILD_STLDBORRFLAG,StaticTextField.class);
		registerChild(CHILD_STLDBORRSALUTATION,StaticTextField.class);
		registerChild(CHILD_STLDBORRFIRSTNAME,StaticTextField.class);
		registerChild(CHILD_STLDBORRMIDINITIAL,StaticTextField.class);
		registerChild(CHILD_STLDBORRLASTNAME,StaticTextField.class);
		registerChild(CHILD_STLDBORRSUFFIX,StaticTextField.class);
		registerChild(CHILD_CREDITBUREAULIABILITY,pgDealSummaryCreditBureauLiabilityTiledView.class);
		registerChild(CHILD_STBEGINHIDECBLIABSECTION,StaticTextField.class);
		registerChild(CHILD_STENDHIDECBLIABSECTION,StaticTextField.class);
		registerChild(CHILD_LIABILITY,pgDealSummaryLiabilityTiledView.class);
		registerChild(CHILD_STBEGINHIDELIABSECTION,StaticTextField.class);
		registerChild(CHILD_STENDHIDELIABSECTION,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDetailViewBorrowersModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		// The following code block was migrated from the RepeatedLiabDetails_onBeforeDisplayEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.setupNumOfRepeatedLiabDetails(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// The following code block was migrated from the RepeatedLiabDetails_onBeforeRowDisplayEvent method
		  DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
      handler.pageGetState(this.getParentViewBean());
      handler.setBorrowerLiabilities(this.getTileIndex());
      handler.pageSaveState();
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STLDBORRTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STLDBORRTYPE,
				doDetailViewBorrowersModel.FIELD_DFAPPLICANTTYPE,
				CHILD_STLDBORRTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLDBORRFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STLDBORRFLAG,
				doDetailViewBorrowersModel.FIELD_DFPRIMARYBORROWER,
				CHILD_STLDBORRFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLDBORRSALUTATION))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STLDBORRSALUTATION,
				doDetailViewBorrowersModel.FIELD_DFSALUTATION,
				CHILD_STLDBORRSALUTATION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLDBORRFIRSTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STLDBORRFIRSTNAME,
				doDetailViewBorrowersModel.FIELD_DFFIRSTNAME,
				CHILD_STLDBORRFIRSTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLDBORRMIDINITIAL))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STLDBORRMIDINITIAL,
				doDetailViewBorrowersModel.FIELD_DFMIDDLENAME,
				CHILD_STLDBORRMIDINITIAL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLDBORRLASTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STLDBORRLASTNAME,
				doDetailViewBorrowersModel.FIELD_DFLASTNAME,
				CHILD_STLDBORRLASTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLDBORRSUFFIX))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STLDBORRSUFFIX,
				doDetailViewBorrowersModel.FIELD_DFSUFFIX,
				CHILD_STLDBORRSUFFIX_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CREDITBUREAULIABILITY))
		{
			pgDealSummaryCreditBureauLiabilityTiledView child = new pgDealSummaryCreditBureauLiabilityTiledView(this,
				CHILD_CREDITBUREAULIABILITY);
			return child;
		}
		else
		if (name.equals(CHILD_STBEGINHIDECBLIABSECTION))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBEGINHIDECBLIABSECTION,
				CHILD_STBEGINHIDECBLIABSECTION,
				CHILD_STBEGINHIDECBLIABSECTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STENDHIDECBLIABSECTION))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STENDHIDECBLIABSECTION,
				CHILD_STENDHIDECBLIABSECTION,
				CHILD_STENDHIDECBLIABSECTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_LIABILITY))
		{
			pgDealSummaryLiabilityTiledView child = new pgDealSummaryLiabilityTiledView(this,
				CHILD_LIABILITY);
			return child;
		}
		else
		if (name.equals(CHILD_STBEGINHIDELIABSECTION))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBEGINHIDELIABSECTION,
				CHILD_STBEGINHIDELIABSECTION,
				CHILD_STBEGINHIDELIABSECTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STENDHIDELIABSECTION))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STENDHIDELIABSECTION,
				CHILD_STENDHIDELIABSECTION,
				CHILD_STENDHIDELIABSECTION_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLDBorrType()
	{
		return (StaticTextField)getChild(CHILD_STLDBORRTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLDBorrFlag()
	{
		return (StaticTextField)getChild(CHILD_STLDBORRFLAG);
	}


	/**
	 *
	 *
	 */
	public String endStLDBorrFlagDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stLDBorrFlag_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setYesOrNo(event.getContent());
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLDBorrSalutation()
	{
		return (StaticTextField)getChild(CHILD_STLDBORRSALUTATION);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLDBorrFirstName()
	{
		return (StaticTextField)getChild(CHILD_STLDBORRFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLDBorrMidInitial()
	{
		return (StaticTextField)getChild(CHILD_STLDBORRMIDINITIAL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLDBorrLastName()
	{
		return (StaticTextField)getChild(CHILD_STLDBORRLASTNAME);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStLDBorrSuffix()
	{
		return (StaticTextField)getChild(CHILD_STLDBORRSUFFIX);
	}
	
	/**
	 *
	 *
	 */
	public pgDealSummaryCreditBureauLiabilityTiledView getCreditBureauLiability()
	{
		return (pgDealSummaryCreditBureauLiabilityTiledView)getChild(CHILD_CREDITBUREAULIABILITY);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBeginHideCBLiabSection()
	{
		return (StaticTextField)getChild(CHILD_STBEGINHIDECBLIABSECTION);
	}


	/**
	 *
	 *
	 */
	public String endStBeginHideCBLiabSectionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stBeginHideCBLiabSection_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.generateOrHideBeginCBLiabSect();
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEndHideCBLiabSection()
	{
		return (StaticTextField)getChild(CHILD_STENDHIDECBLIABSECTION);
	}


	/**
	 *
	 *
	 */
	public String endStEndHideCBLiabSectionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stEndHideCBLiabSection_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.generateOrHideEndCBLiabSect();
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public pgDealSummaryLiabilityTiledView getLiability()
	{
		return (pgDealSummaryLiabilityTiledView)getChild(CHILD_LIABILITY);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBeginHideLiabSection()
	{
		return (StaticTextField)getChild(CHILD_STBEGINHIDELIABSECTION);
	}


	/**
	 *
	 *
	 */
	public String endStBeginHideLiabSectionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stBeginHideLiabSection_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.generateOrHideBeginLiabSect();
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEndHideLiabSection()
	{
		return (StaticTextField)getChild(CHILD_STENDHIDELIABSECTION);
	}


	/**
	 *
	 *
	 */
	public String endStEndHideLiabSectionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stEndHideLiabSection_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.generateOrHideEndLiabSect();
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public doDetailViewBorrowersModel getdoDetailViewBorrowersModel()
	{
		if (doDetailViewBorrowers == null)
			doDetailViewBorrowers = (doDetailViewBorrowersModel) getModel(doDetailViewBorrowersModel.class);
		return doDetailViewBorrowers;
	}


	/**
	 *
	 *
	 */
	public void setdoDetailViewBorrowersModel(doDetailViewBorrowersModel model)
	{
			doDetailViewBorrowers = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STLDBORRTYPE="stLDBorrType";
	public static final String CHILD_STLDBORRTYPE_RESET_VALUE="";
	public static final String CHILD_STLDBORRFLAG="stLDBorrFlag";
	public static final String CHILD_STLDBORRFLAG_RESET_VALUE="";
	public static final String CHILD_STLDBORRSALUTATION="stLDBorrSalutation";
	public static final String CHILD_STLDBORRSALUTATION_RESET_VALUE="";
	public static final String CHILD_STLDBORRFIRSTNAME="stLDBorrFirstName";
	public static final String CHILD_STLDBORRFIRSTNAME_RESET_VALUE="";
	public static final String CHILD_STLDBORRMIDINITIAL="stLDBorrMidInitial";
	public static final String CHILD_STLDBORRMIDINITIAL_RESET_VALUE="";
	public static final String CHILD_STLDBORRLASTNAME="stLDBorrLastName";
	public static final String CHILD_STLDBORRLASTNAME_RESET_VALUE="";
	public static final String CHILD_STLDBORRSUFFIX="stLDBorrSuffix";
	public static final String CHILD_STLDBORRSUFFIX_RESET_VALUE="";
	public static final String CHILD_CREDITBUREAULIABILITY="CreditBureauLiability";
	public static final String CHILD_STBEGINHIDECBLIABSECTION="stBeginHideCBLiabSection";
	public static final String CHILD_STBEGINHIDECBLIABSECTION_RESET_VALUE="";
	public static final String CHILD_STENDHIDECBLIABSECTION="stEndHideCBLiabSection";
	public static final String CHILD_STENDHIDECBLIABSECTION_RESET_VALUE="";
	public static final String CHILD_LIABILITY="Liability";
	public static final String CHILD_STBEGINHIDELIABSECTION="stBeginHideLiabSection";
	public static final String CHILD_STBEGINHIDELIABSECTION_RESET_VALUE="";
	public static final String CHILD_STENDHIDELIABSECTION="stEndHideLiabSection";
	public static final String CHILD_STENDHIDELIABSECTION_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDetailViewBorrowersModel doDetailViewBorrowers=null;
  private DealSummaryHandler handler=new DealSummaryHandler();

}

