package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doMainViewEscrowModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEscrowType();

	
	/**
	 * 
	 * 
	 */
	public void setDfEscrowType(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEscrowPaymentDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfEscrowPaymentDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfEscrowPayment();

	
	/**
	 * 
	 * 
	 */
	public void setDfEscrowPayment(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);
	
	  public java.math.BigDecimal getDfInstitutionId();
	  public void setDfInstitutionId(java.math.BigDecimal id);
	  
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFESCROWTYPE="dfEscrowType";
	public static final String FIELD_DFESCROWPAYMENTDESC="dfEscrowPaymentDesc";
	public static final String FIELD_DFESCROWPAYMENT="dfEscrowPayment";
	public static final String FIELD_DFCOPYID="dfCopyId";
	
  public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
 
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

