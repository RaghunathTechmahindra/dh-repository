package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;


/**
 *
 *
 *
 */
public class doAppraiserSelectModelImpl extends QueryModelBase
	implements doAppraiserSelectModel
{
	/**
	 *
	 *
	 */
	public doAppraiserSelectModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public String getDfAppraiserAddress()
	{
		return (String)getValue(FIELD_DFAPPRAISERADDRESS);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserAddress(String value)
	{
		setValue(FIELD_DFAPPRAISERADDRESS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfFax()
	{
		return (String)getValue(FIELD_DFFAX);
	}


	/**
	 *
	 *
	 */
	public void setDfFax(String value)
	{
		setValue(FIELD_DFFAX,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAppraiserId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFAPPRAISERID);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFAPPRAISERID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmail()
	{
		return (String)getValue(FIELD_DFEMAIL);
	}


	/**
	 *
	 *
	 */
	public void setDfEmail(String value)
	{
		setValue(FIELD_DFEMAIL,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAppraiserShortName()
	{
		return (String)getValue(FIELD_DFAPPRAISERSHORTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserShortName(String value)
	{
		setValue(FIELD_DFAPPRAISERSHORTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAppraiserPhoneNumber()
	{
		return (String)getValue(FIELD_DFAPPRAISERPHONENUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserPhoneNumber(String value)
	{
		setValue(FIELD_DFAPPRAISERPHONENUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAppraiserExtension()
	{
		return (String)getValue(FIELD_DFAPPRAISEREXTENSION);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserExtension(String value)
	{
		setValue(FIELD_DFAPPRAISEREXTENSION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAppraiserName()
	{
		return (String)getValue(FIELD_DFAPPRAISERNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserName(String value)
	{
		setValue(FIELD_DFAPPRAISERNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAppraiserCompanyName()
	{
		return (String)getValue(FIELD_DFAPPRAISERCOMPANYNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserCompanyName(String value)
	{
		setValue(FIELD_DFAPPRAISERCOMPANYNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAppraiserNotes()
	{
		return (String)getValue(FIELD_DFAPPRAISERNOTES);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserNotes(String value)
	{
		setValue(FIELD_DFAPPRAISERNOTES,value);
	}


	/**
	 * MetaData object test method to verify the SYNTHETIC new
	 * field for the reogranized SQL_TEMPLATE query.
	 *
	 */
  /**
	public void setResultSet(ResultSet value)
	{
  		logger = SysLog.getSysLogger("METADATA");

  		try
  		{
   			super.setResultSet(value);
   			if( value != null)
   			{
       			ResultSetMetaData metaData = value.getMetaData();
       			int columnCount = metaData.getColumnCount();
       			logger.debug("===============================================");
              	        logger.debug("Testing MetaData Object for new synthetic fields for" +
                 	            " doAppraiserSelectModel");
       			logger.debug("NumberColumns: " + columnCount);
         		for(int i = 0; i < columnCount ; i++)
          		{
                 	logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
          		}
          		logger.debug("================================================");
      		}
  		}
  		catch (SQLException e)
  		{
                    logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
  		}
 	}
  **/

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	////public static final String SELECT_SQL_TEMPLATE="SELECT ALL ADDR.ADDRESSLINE1 || ' ' || ADDR.CITY ||  ' '|| PROVINCE.PROVINCEABBREVIATION , CONTACT.CONTACTFAXNUMBER, PARTYPROFILE.PARTYPROFILEID, CONTACT.CONTACTEMAILADDRESS, PARTYDEALASSOC.DEALID, PARTYDEALASSOC.PROPERTYID, PARTYPROFILE.PTPSHORTNAME, CONTACT.CONTACTPHONENUMBER, CONTACT.CONTACTPHONENUMBEREXTENSION, PARTYPROFILE.PARTYNAME, PARTYPROFILE.PARTYCOMPANYNAME, PARTYPROFILE.NOTES FROM PARTYPROFILE, CONTACT, ADDR, PROVINCE, PARTYDEALASSOC  __WHERE__  ";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL ADDR.ADDRESSLINE1 || ' ' || ADDR.CITY ||  ' '|| PROVINCE.PROVINCEABBREVIATION SYNTHETICADDRESS, CONTACT.CONTACTFAXNUMBER, PARTYPROFILE.PARTYPROFILEID, CONTACT.CONTACTEMAILADDRESS, PARTYDEALASSOC.DEALID, PARTYDEALASSOC.PROPERTYID, PARTYPROFILE.PTPSHORTNAME, CONTACT.CONTACTPHONENUMBER, CONTACT.CONTACTPHONENUMBEREXTENSION, PARTYPROFILE.PARTYNAME, PARTYPROFILE.PARTYCOMPANYNAME, PARTYPROFILE.NOTES FROM PARTYPROFILE, CONTACT, ADDR, PROVINCE, PARTYDEALASSOC  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="PARTYPROFILE, CONTACT, ADDR, PROVINCE, PARTYDEALASSOC";
	public static final String STATIC_WHERE_CRITERIA=
	    " (PARTYPROFILE.INSTITUTIONPROFILEID  =  CONTACT.INSTITUTIONPROFILEID) AND " +
	    " (PARTYPROFILE.INSTITUTIONPROFILEID  =  ADDR.INSTITUTIONPROFILEID) AND " +
	    " (PARTYPROFILE.INSTITUTIONPROFILEID  =  PARTYDEALASSOC.INSTITUTIONPROFILEID) AND " +
	    " (PARTYPROFILE.CONTACTID  =  CONTACT.CONTACTID) AND " +
	    "(ADDR.ADDRID  =  CONTACT.ADDRID) AND (ADDR.PROVINCEID  =  PROVINCE.PROVINCEID) AND " +
	    "(PARTYPROFILE.PARTYPROFILEID  =  PARTYDEALASSOC.PARTYPROFILEID)";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	////public static final String QUALIFIED_COLUMN_DFAPPRAISERADDRESS=".";
	////public static final String COLUMN_DFAPPRAISERADDRESS="";
	public static final String QUALIFIED_COLUMN_DFAPPRAISERADDRESS="ADDR.SYNTHETICADDRESS";
	public static final String COLUMN_DFAPPRAISERADDRESS="SYNTHETICADDRESS";
	public static final String QUALIFIED_COLUMN_DFFAX="CONTACT.CONTACTFAXNUMBER";
	public static final String COLUMN_DFFAX="CONTACTFAXNUMBER";
	public static final String QUALIFIED_COLUMN_DFAPPRAISERID="PARTYPROFILE.PARTYPROFILEID";
	public static final String COLUMN_DFAPPRAISERID="PARTYPROFILEID";
	public static final String QUALIFIED_COLUMN_DFEMAIL="CONTACT.CONTACTEMAILADDRESS";
	public static final String COLUMN_DFEMAIL="CONTACTEMAILADDRESS";
	public static final String QUALIFIED_COLUMN_DFDEALID="PARTYDEALASSOC.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYID="PARTYDEALASSOC.PROPERTYID";
	public static final String COLUMN_DFPROPERTYID="PROPERTYID";
	public static final String QUALIFIED_COLUMN_DFAPPRAISERSHORTNAME="PARTYPROFILE.PTPSHORTNAME";
	public static final String COLUMN_DFAPPRAISERSHORTNAME="PTPSHORTNAME";
	public static final String QUALIFIED_COLUMN_DFAPPRAISERPHONENUMBER="CONTACT.CONTACTPHONENUMBER";
	public static final String COLUMN_DFAPPRAISERPHONENUMBER="CONTACTPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFAPPRAISEREXTENSION="CONTACT.CONTACTPHONENUMBEREXTENSION";
	public static final String COLUMN_DFAPPRAISEREXTENSION="CONTACTPHONENUMBEREXTENSION";
	public static final String QUALIFIED_COLUMN_DFAPPRAISERNAME="PARTYPROFILE.PARTYNAME";
	public static final String COLUMN_DFAPPRAISERNAME="PARTYNAME";
	public static final String QUALIFIED_COLUMN_DFAPPRAISERCOMPANYNAME="PARTYPROFILE.PARTYCOMPANYNAME";
	public static final String COLUMN_DFAPPRAISERCOMPANYNAME="PARTYCOMPANYNAME";
	public static final String QUALIFIED_COLUMN_DFAPPRAISERNOTES="PARTYPROFILE.NOTES";
	public static final String COLUMN_DFAPPRAISERNOTES="NOTES";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		//// Improper converted fieldDescriptor for the ComputedColumn by iMT tool.
    ////FIELD_SCHEMA.addFieldDescriptor(
		////	new QueryFieldDescriptor(
		////		FIELD_DFAPPRAISERADDRESS,
		////		COLUMN_DFAPPRAISERADDRESS,
		////		QUALIFIED_COLUMN_DFAPPRAISERADDRESS,
		////		String.class,
		////		false,
		////		false,
		////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		////		"",
		////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		////		""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERADDRESS,
				COLUMN_DFAPPRAISERADDRESS,
				QUALIFIED_COLUMN_DFAPPRAISERADDRESS,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"ADDR.ADDRESSLINE1 || ' ' || ADDR.CITY ||  ' '|| PROVINCE.PROVINCEABBREVIATION",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"ADDR.ADDRESSLINE1 || ' ' || ADDR.CITY ||  ' '|| PROVINCE.PROVINCEABBREVIATION"));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFAX,
				COLUMN_DFFAX,
				QUALIFIED_COLUMN_DFFAX,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERID,
				COLUMN_DFAPPRAISERID,
				QUALIFIED_COLUMN_DFAPPRAISERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMAIL,
				COLUMN_DFEMAIL,
				QUALIFIED_COLUMN_DFEMAIL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYID,
				COLUMN_DFPROPERTYID,
				QUALIFIED_COLUMN_DFPROPERTYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERSHORTNAME,
				COLUMN_DFAPPRAISERSHORTNAME,
				QUALIFIED_COLUMN_DFAPPRAISERSHORTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERPHONENUMBER,
				COLUMN_DFAPPRAISERPHONENUMBER,
				QUALIFIED_COLUMN_DFAPPRAISERPHONENUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISEREXTENSION,
				COLUMN_DFAPPRAISEREXTENSION,
				QUALIFIED_COLUMN_DFAPPRAISEREXTENSION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERNAME,
				COLUMN_DFAPPRAISERNAME,
				QUALIFIED_COLUMN_DFAPPRAISERNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERCOMPANYNAME,
				COLUMN_DFAPPRAISERCOMPANYNAME,
				QUALIFIED_COLUMN_DFAPPRAISERCOMPANYNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERNOTES,
				COLUMN_DFAPPRAISERNOTES,
				QUALIFIED_COLUMN_DFAPPRAISERNOTES,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

