package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doMainViewBridgeModelImpl extends QueryModelBase
	implements doMainViewBridgeModel
{
	/**
	 *
	 *
	 */
	public doMainViewBridgeModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 19Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert BRIDGEPURPOSE Desc.
      this.setDfBridgePurpose(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "BRIDGEPURPOSE", this.getDfBridgePurpose(), languageId));
      //Convert PAYMENTTERM Desc.
      this.setDfPaymentTerm(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "PAYMENTTERM", this.getDfPaymentTerm(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBridgeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBRIDGEID);
	}


	/**
	 *
	 *
	 */
	public void setDfBridgeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBRIDGEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBridgePurpose()
	{
		return (String)getValue(FIELD_DFBRIDGEPURPOSE);
	}


	/**
	 *
	 *
	 */
	public void setDfBridgePurpose(String value)
	{
		setValue(FIELD_DFBRIDGEPURPOSE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLender()
	{
		return (String)getValue(FIELD_DFLENDER);
	}


	/**
	 *
	 *
	 */
	public void setDfLender(String value)
	{
		setValue(FIELD_DFLENDER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPaymentTerm()
	{
		return (String)getValue(FIELD_DFPAYMENTTERM);
	}


	/**
	 *
	 *
	 */
	public void setDfPaymentTerm(String value)
	{
		setValue(FIELD_DFPAYMENTTERM,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPostedInterestRate()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPOSTEDINTERESTRATE);
	}


	/**
	 *
	 *
	 */
	public void setDfPostedInterestRate(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPOSTEDINTERESTRATE,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfRateDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFRATEDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfRateDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFRATEDATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDiscount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDISCOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfDiscount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDISCOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNetRate()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFNETRATE);
	}


	/**
	 *
	 *
	 */
	public void setDfNetRate(java.math.BigDecimal value)
	{
		setValue(FIELD_DFNETRATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBridgeLoanAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBRIDGELOANAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfBridgeLoanAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBRIDGELOANAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfActualPaymentTerm()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFACTUALPAYMENTTERM);
	}


	/**
	 *
	 *
	 */
	public void setDfActualPaymentTerm(java.math.BigDecimal value)
	{
		setValue(FIELD_DFACTUALPAYMENTTERM,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfMaturityDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFMATURITYDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfMaturityDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFMATURITYDATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBridgePremium()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBRIDGEPREMIUM);
	}


	/**
	 *
	 *
	 */
	public void setDfBridgePremium(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBRIDGEPREMIUM,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBridgePricingProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBRIDGEPRICINGPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfBridgePricingProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBRIDGEPRICINGPROFILEID,value);
	}


      public java.math.BigDecimal getDfInstitutionId() {
    	    return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
    	  }
    
    
    	  public void setDfInstitutionId(java.math.BigDecimal value) {
    	    setValue(FIELD_DFINSTITUTIONID, value);
    	  }
		  

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 19Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL BRIDGE.BRIDGEID, BRIDGEPURPOSE.BPDESCRIPTION, LENDERPROFILE.LPSHORTNAME, PAYMENTTERM.PTDESCRIPTION, BRIDGE.POSTEDINTERESTRATE, BRIDGE.RATEDATE, BRIDGE.DISCOUNT, BRIDGE.NETINTERESTRATE, BRIDGE.NETLOANAMOUNT, BRIDGE.ACTUALPAYMENTTERM, BRIDGE.MATURITYDATE, BRIDGE.PREMIUM, BRIDGE.PRICINGPROFILEID FROM BRIDGE, BRIDGEPURPOSE, LENDERPROFILE, PAYMENTTERM  __WHERE__  ";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL BRIDGE.BRIDGEID, to_char(BRIDGE.BRIDGEPURPOSEID) BRIDGEPURPOSEID_STR, LENDERPROFILE.LPSHORTNAME, "+
    "to_char(BRIDGE.PAYMENTTERMID) PAYMENTTERMID_STR, BRIDGE.POSTEDINTERESTRATE, BRIDGE.RATEDATE, "+
    "BRIDGE.DISCOUNT, BRIDGE.NETINTERESTRATE, BRIDGE.NETLOANAMOUNT, "+
    "BRIDGE.ACTUALPAYMENTTERM, BRIDGE.MATURITYDATE, BRIDGE.PREMIUM, "+
    "BRIDGE.PRICINGPROFILEID, "+
    "BRIDGE.INSTITUTIONPROFILEID "+
    " FROM BRIDGE, LENDERPROFILE  "+
    "__WHERE__  ";
	//--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //public static final String MODIFYING_QUERY_TABLE_NAME="BRIDGE, BRIDGEPURPOSE, LENDERPROFILE, PAYMENTTERM";
	public static final String MODIFYING_QUERY_TABLE_NAME=
    " BRIDGE, LENDERPROFILE ";
	//--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
  //public static final String STATIC_WHERE_CRITERIA=" (BRIDGE.BRIDGEPURPOSEID  =  BRIDGEPURPOSE.BRIDGEPURPOSEID) AND (BRIDGE.PAYMENTTERMID  =  PAYMENTTERM.PAYMENTTERMID) AND (BRIDGE.LENDERPROFILEID  =  LENDERPROFILE.LENDERPROFILEID)";
	public static final String STATIC_WHERE_CRITERIA=
    " (BRIDGE.LENDERPROFILEID  =  LENDERPROFILE.LENDERPROFILEID AND " 
		+   " BRIDGE.INSTITUTIONPROFILEID = LENDERPROFILE.INSTITUTIONPROFILEID)";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBRIDGEID="BRIDGE.BRIDGEID";
	public static final String COLUMN_DFBRIDGEID="BRIDGEID";
	public static final String QUALIFIED_COLUMN_DFLENDER="LENDERPROFILE.LPSHORTNAME";
	public static final String COLUMN_DFLENDER="LPSHORTNAME";
	public static final String QUALIFIED_COLUMN_DFPOSTEDINTERESTRATE="BRIDGE.POSTEDINTERESTRATE";
	public static final String COLUMN_DFPOSTEDINTERESTRATE="POSTEDINTERESTRATE";
	public static final String QUALIFIED_COLUMN_DFRATEDATE="BRIDGE.RATEDATE";
	public static final String COLUMN_DFRATEDATE="RATEDATE";
	public static final String QUALIFIED_COLUMN_DFDISCOUNT="BRIDGE.DISCOUNT";
	public static final String COLUMN_DFDISCOUNT="DISCOUNT";
	public static final String QUALIFIED_COLUMN_DFNETRATE="BRIDGE.NETINTERESTRATE";
	public static final String COLUMN_DFNETRATE="NETINTERESTRATE";
	public static final String QUALIFIED_COLUMN_DFBRIDGELOANAMOUNT="BRIDGE.NETLOANAMOUNT";
	public static final String COLUMN_DFBRIDGELOANAMOUNT="NETLOANAMOUNT";
	public static final String QUALIFIED_COLUMN_DFACTUALPAYMENTTERM="BRIDGE.ACTUALPAYMENTTERM";
	public static final String COLUMN_DFACTUALPAYMENTTERM="ACTUALPAYMENTTERM";
	public static final String QUALIFIED_COLUMN_DFMATURITYDATE="BRIDGE.MATURITYDATE";
	public static final String COLUMN_DFMATURITYDATE="MATURITYDATE";
	public static final String QUALIFIED_COLUMN_DFBRIDGEPREMIUM="BRIDGE.PREMIUM";
	public static final String COLUMN_DFBRIDGEPREMIUM="PREMIUM";
	public static final String QUALIFIED_COLUMN_DFBRIDGEPRICINGPROFILEID="BRIDGE.PRICINGPROFILEID";
	public static final String COLUMN_DFBRIDGEPRICINGPROFILEID="PRICINGPROFILEID";

	//--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFBRIDGEPURPOSE="BRIDGEPURPOSE.BPDESCRIPTION";
	//public static final String COLUMN_DFBRIDGEPURPOSE="BPDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFBRIDGEPURPOSE="BRIDGE.BRIDGEPURPOSEID_STR";
	public static final String COLUMN_DFBRIDGEPURPOSE="BRIDGEPURPOSEID_STR";
  //public static final String QUALIFIED_COLUMN_DFPAYMENTTERM="PAYMENTTERM.PTDESCRIPTION";
	//public static final String COLUMN_DFPAYMENTTERM="PTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFPAYMENTTERM="BRIDGE.PAYMENTTERMID_STR";
	public static final String COLUMN_DFPAYMENTTERM="PAYMENTTERMID_STR";
	
	  public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
		    "BRIDGE.INSTITUTIONPROFILEID";
		  public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
		  
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRIDGEID,
				COLUMN_DFBRIDGEID,
				QUALIFIED_COLUMN_DFBRIDGEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRIDGEPURPOSE,
				COLUMN_DFBRIDGEPURPOSE,
				QUALIFIED_COLUMN_DFBRIDGEPURPOSE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLENDER,
				COLUMN_DFLENDER,
				QUALIFIED_COLUMN_DFLENDER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPAYMENTTERM,
				COLUMN_DFPAYMENTTERM,
				QUALIFIED_COLUMN_DFPAYMENTTERM,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPOSTEDINTERESTRATE,
				COLUMN_DFPOSTEDINTERESTRATE,
				QUALIFIED_COLUMN_DFPOSTEDINTERESTRATE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFRATEDATE,
				COLUMN_DFRATEDATE,
				QUALIFIED_COLUMN_DFRATEDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDISCOUNT,
				COLUMN_DFDISCOUNT,
				QUALIFIED_COLUMN_DFDISCOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNETRATE,
				COLUMN_DFNETRATE,
				QUALIFIED_COLUMN_DFNETRATE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRIDGELOANAMOUNT,
				COLUMN_DFBRIDGELOANAMOUNT,
				QUALIFIED_COLUMN_DFBRIDGELOANAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFACTUALPAYMENTTERM,
				COLUMN_DFACTUALPAYMENTTERM,
				QUALIFIED_COLUMN_DFACTUALPAYMENTTERM,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMATURITYDATE,
				COLUMN_DFMATURITYDATE,
				QUALIFIED_COLUMN_DFMATURITYDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRIDGEPREMIUM,
				COLUMN_DFBRIDGEPREMIUM,
				QUALIFIED_COLUMN_DFBRIDGEPREMIUM,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRIDGEPRICINGPROFILEID,
				COLUMN_DFBRIDGEPRICINGPROFILEID,
				QUALIFIED_COLUMN_DFBRIDGEPRICINGPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		   FIELD_SCHEMA.addFieldDescriptor(
			        new QueryFieldDescriptor(
			          FIELD_DFINSTITUTIONID,
			          COLUMN_DFINSTITUTIONID,
			          QUALIFIED_COLUMN_DFINSTITUTIONID,
			          java.math.BigDecimal.class,
			          false,
			          false,
			          QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
			          "",
			          QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
			          ""));
			    

	}

}

