package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doLifeDisabilityInsProportionsModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();

	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */

  public java.math.BigDecimal getDfBorrowerId();

	/**
	 *
	 *
	 */
	public String getDfFirstBorrowerName();

	/**
	 *
	 *
	 */
	public String getDfCopyType();

	/**
	 *
	 *
	 */
	public void setDfCopyType(String value);

	/**
	 *
	 *
	 */
	public void setDfCopyID(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public String getDfIPDesc();

	/**
	 *
	 *
	 */
	public void setDfIPDesc(String value);

	/**
	 *
	 *
	 */
	public void setDfTotalLoanAmount(java.math.BigDecimal value);

	/**
	 *
	 *
	 */

  public java.math.BigDecimal getDfTotalLoanAmount();

	/**
	 *
	 *
	 */
	public void setDfInsuranceProportionsId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */

  public java.math.BigDecimal getDfInsuranceProportionsId();

	/**
	 *
	 *
	 */
	public void setDfPaymentFrequencyId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */

  public java.math.BigDecimal getDfPaymentFrequencyId();

	/**
	 *
	 *
	 */
	public String getDfPFDescription();

	/**
	 *
	 *
	 */
	public void setDfPFDescription(String value);

	/**
	 *
	 *
	 */
	public void setDfPremiumAmount(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfPremiumAmount();

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfLifePercentCoverageReq();

	/**
	 *
	 *
	 */
	public void setDfLifePercentCoverageReq(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPIPaymentAmount();

	/**
	 *
	 *
	 */
	public void setDfPIPaymentAmount(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNetRate();

	/**
	 *
	 *
	 */
	public void setDfNetRate(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfLifePremium();

	/**
	 *
	 *
	 */
	public void setDfDisabilityPremium(java.math.BigDecimal value);

	/**
	 *
	 *
	 */

  public java.math.BigDecimal getDfDisabilityPremium();

	/**
	 *
	 *
	 */
	public void setDfCombinedLDPremium(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfCombindedLDPremium();

  /**
	 *
	 *
	 */
	public void setDfPmntInclLifeDisability(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfPmntInclLifeDisability();

	/**
	 *
	 *
	 */
	public void setDfTotalRateInclLifeDisability(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfTotalRateInclLifeDisability();

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfInterestRateIncrement();

	/**
	 *
	 *
	 */
	public void stDfInterestRateIncrement(java.math.BigDecimal value);


	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
  public static final String FIELD_DFBORROWERID="dfBorrowerId";
  public static final String FIELD_DFBORROWERFIRSTNAME="dfBorrowerFirstName";
  public static final String FIELD_DFBORROWERLASTNAME="dfBorrowerLastName";
  public static final String FIELD_DFPFDESCRIPTION="dfPFDescription";
  public static final String FIELD_DFMIPREMIUMAMOUNT="dfMIPremiumAmoung";
  public static final String FIELD_DFPAYMENTFREQUENCYID="dfPaymentFrequencyId";
  public static final String FIELD_DFINSURANCEPROPORTIONSID="dfInsuranceProportionsId";
  public static final String FIELD_DFTOTALLOANAMOUNT="dfTotalLoanAmount";
  public static final String FIELD_DFIPDESC="dfIPDesc";
  public static final String FIELD_DFCOPYTYPE="dfCopyType";
  public static final String FIELD_DFLIFEPERCENTCOVERAGEREQ="dfLifePercentCoverageReq";
  public static final String FIELD_DFPIPAYMENTAMOUNT="dfPIPaymentAmount";
  public static final String FIELD_DFNETRATE="dfNetRate";
  public static final String FIELD_DFLIFEPREMIUM="dfLifePremium";
  public static final String FIELD_DFDISABILITYPREMIUM="dfDisabilityPremium";
  public static final String FIELD_DFCOMBINEDLDPREMIUM="dfCombinedLDPremium";
  public static final String FIELD_DFPMNTINCLLIFEDISABILITY="dfPmntInclLifeDisability";
  public static final String FIELD_DFTOTALRATEINCLLIFEDISABILITY="dfTotalRateInclLifeDis";
  public static final String FIELD_DFADDRATEFORLIFEDISABILITY="dfAddRateForLifeDisability";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

