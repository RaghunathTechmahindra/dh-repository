package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doDocStatusModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDocStatusId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDocStatusId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfDocStatusDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfDocStatusDesc(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDOCSTATUSID="dfDocStatusId";
	public static final String FIELD_DFDOCSTATUSDESC="dfDocStatusDesc";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

