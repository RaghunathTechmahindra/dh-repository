package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doGetPricingRateInventoryModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPricingRateInventoryId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPricingRateInventoryId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfInternalRatePercentage();

	
	/**
	 * 
	 * 
	 */
	public void setDfInternalRatePercentage(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfIndexEffectiveDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfIndexEffectiveDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfMaximumDiscountAllowed();

	
	/**
	 * 
	 * 
	 */
	public void setDfMaximumDiscountAllowed(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFPRICINGRATEINVENTORYID="dfPricingRateInventoryId";
	public static final String FIELD_DFINTERNALRATEPERCENTAGE="dfInternalRatePercentage";
	public static final String FIELD_DFINDEXEFFECTIVEDATE="dfIndexEffectiveDate";
	public static final String FIELD_DFMAXIMUMDISCOUNTALLOWED="dfMaximumDiscountAllowed";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

