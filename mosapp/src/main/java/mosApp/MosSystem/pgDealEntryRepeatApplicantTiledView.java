package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;

import MosSystem.Sc;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;


/**
 *
 *
 *
 */
public class pgDealEntryRepeatApplicantTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealEntryRepeatApplicantTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doDealEntryApplicantSelectModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getHdApplicantId().setValue(CHILD_HDAPPLICANTID_RESET_VALUE);
		getHdApplicantCopyId().setValue(CHILD_HDAPPLICANTCOPYID_RESET_VALUE);
		getCbPrimaryApplicant().setValue(CHILD_CBPRIMARYAPPLICANT_RESET_VALUE);
		getStFirstName().setValue(CHILD_STFIRSTNAME_RESET_VALUE);
		getStMiddleInitial().setValue(CHILD_STMIDDLEINITIAL_RESET_VALUE);
		getStLastName().setValue(CHILD_STLASTNAME_RESET_VALUE);
        getStSuffix().setValue(CHILD_STSUFFIX_RESET_VALUE);
		getStLanguage().setValue(CHILD_STLANGUAGE_RESET_VALUE);
		getStHomePhoneNo().setValue(CHILD_STHOMEPHONENO_RESET_VALUE);
		getStWorkPhoneNo().setValue(CHILD_STWORKPHONENO_RESET_VALUE);
		getStWorkPhoneExt().setValue(CHILD_STWORKPHONEEXT_RESET_VALUE);
		getStApplicantType().setValue(CHILD_STAPPLICANTTYPE_RESET_VALUE);
		getBtDeleteApplicant().setValue(CHILD_BTDELETEAPPLICANT_RESET_VALUE);
		getBtApplicantDetails().setValue(CHILD_BTAPPLICANTDETAILS_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_HDAPPLICANTID,HiddenField.class);
		registerChild(CHILD_HDAPPLICANTCOPYID,HiddenField.class);
		registerChild(CHILD_CBPRIMARYAPPLICANT,ComboBox.class);
		registerChild(CHILD_STFIRSTNAME,StaticTextField.class);
		registerChild(CHILD_STMIDDLEINITIAL,StaticTextField.class);
		registerChild(CHILD_STLASTNAME,StaticTextField.class);
        registerChild(CHILD_STSUFFIX,StaticTextField.class);
		registerChild(CHILD_STLANGUAGE,StaticTextField.class);
		registerChild(CHILD_STHOMEPHONENO,StaticTextField.class);
		registerChild(CHILD_STWORKPHONENO,StaticTextField.class);
		registerChild(CHILD_STWORKPHONEEXT,StaticTextField.class);
		registerChild(CHILD_STAPPLICANTTYPE,StaticTextField.class);
		registerChild(CHILD_BTDELETEAPPLICANT,Button.class);
		registerChild(CHILD_BTAPPLICANTDETAILS,Button.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDealEntryApplicantSelectModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--start//
    DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

    // 1. Setup the Yes/No/Co-App Options
    String yesStr = BXResources.getGenericMsg("YES_LABEL", handler.getTheSessionState().getLanguageId());
    String noStr = BXResources.getGenericMsg("NO_LABEL", handler.getTheSessionState().getLanguageId());
    String coAppStr = BXResources.getGenericMsg("COAPP_LABEL", handler.getTheSessionState().getLanguageId());

    cbPrimaryApplicantOptions.setOptions(new String[]{noStr, yesStr, coAppStr},new String[]{"N", "Y", "C"});
    //--Release2.1--end//

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
          validJSFromTiledView();
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}

		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_HDAPPLICANTID))
		{
			HiddenField child = new HiddenField(this,
				getdoDealEntryApplicantSelectModel(),
				CHILD_HDAPPLICANTID,
				doDealEntryApplicantSelectModel.FIELD_DFBORROWERID,
				CHILD_HDAPPLICANTID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDAPPLICANTCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoDealEntryApplicantSelectModel(),
				CHILD_HDAPPLICANTCOPYID,
				doDealEntryApplicantSelectModel.FIELD_DFCOPYID,
				CHILD_HDAPPLICANTCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPRIMARYAPPLICANT))
		{
			ComboBox child = new ComboBox( this,
				getdoDealEntryApplicantSelectModel(),
				CHILD_CBPRIMARYAPPLICANT,
				doDealEntryApplicantSelectModel.FIELD_DFPRIMARYBORROWERFLAG,
				CHILD_CBPRIMARYAPPLICANT_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbPrimaryApplicantOptions);
			return child;
		}
		else
		if (name.equals(CHILD_STFIRSTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealEntryApplicantSelectModel(),
				CHILD_STFIRSTNAME,
				doDealEntryApplicantSelectModel.FIELD_DFFIRSTNAME,
				CHILD_STFIRSTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STMIDDLEINITIAL))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealEntryApplicantSelectModel(),
				CHILD_STMIDDLEINITIAL,
				doDealEntryApplicantSelectModel.FIELD_DFMIDDLENAME,
				CHILD_STMIDDLEINITIAL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLASTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealEntryApplicantSelectModel(),
				CHILD_STLASTNAME,
				doDealEntryApplicantSelectModel.FIELD_DFLASTNAME,
				CHILD_STLASTNAME_RESET_VALUE,
				null);
			return child;
		}
        else
        if (name.equals(CHILD_STSUFFIX))
        {
            StaticTextField child = new StaticTextField(this,
                getdoDealEntryApplicantSelectModel(),
                CHILD_STSUFFIX,
                doDealEntryApplicantSelectModel.FIELD_DFSUFFIX,
                CHILD_STSUFFIX_RESET_VALUE,
                null);
            return child;
        }
		else
		if (name.equals(CHILD_STLANGUAGE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealEntryApplicantSelectModel(),
				CHILD_STLANGUAGE,
				doDealEntryApplicantSelectModel.FIELD_DFLANGUAGEPREFERENCEDESCR,
				CHILD_STLANGUAGE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STHOMEPHONENO))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealEntryApplicantSelectModel(),
				CHILD_STHOMEPHONENO,
				doDealEntryApplicantSelectModel.FIELD_DFHOMEPHONENUMBER,
				CHILD_STHOMEPHONENO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STWORKPHONENO))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealEntryApplicantSelectModel(),
				CHILD_STWORKPHONENO,
				doDealEntryApplicantSelectModel.FIELD_DFWORKPHONENUMBER,
				CHILD_STWORKPHONENO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STWORKPHONEEXT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealEntryApplicantSelectModel(),
				CHILD_STWORKPHONEEXT,
				doDealEntryApplicantSelectModel.FIELD_DFWORKPHONEEXT,
				CHILD_STWORKPHONEEXT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPLICANTTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealEntryApplicantSelectModel(),
				CHILD_STAPPLICANTTYPE,
				doDealEntryApplicantSelectModel.FIELD_DFAPPLICANTTYPE,
				CHILD_STAPPLICANTTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETEAPPLICANT))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETEAPPLICANT,
				CHILD_BTDELETEAPPLICANT,
				CHILD_BTDELETEAPPLICANT_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTAPPLICANTDETAILS))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTAPPLICANTDETAILS,
				CHILD_BTAPPLICANTDETAILS,
				CHILD_BTAPPLICANTDETAILS_RESET_VALUE,
				null);
				return child;

		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdApplicantId()
	{
		return (HiddenField)getChild(CHILD_HDAPPLICANTID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdApplicantCopyId()
	{
		return (HiddenField)getChild(CHILD_HDAPPLICANTCOPYID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPrimaryApplicant()
	{
		return (ComboBox)getChild(CHILD_CBPRIMARYAPPLICANT);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStFirstName()
	{
		return (StaticTextField)getChild(CHILD_STFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStMiddleInitial()
	{
		return (StaticTextField)getChild(CHILD_STMIDDLEINITIAL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLastName()
	{
		return (StaticTextField)getChild(CHILD_STLASTNAME);
	}

    /**
     *
     *
     */
    public StaticTextField getStSuffix()
    {
        return (StaticTextField)getChild(CHILD_STSUFFIX);
    }

	/**
	 *
	 *
	 */
	public StaticTextField getStLanguage()
	{
		return (StaticTextField)getChild(CHILD_STLANGUAGE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStHomePhoneNo()
	{
		return (StaticTextField)getChild(CHILD_STHOMEPHONENO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStWorkPhoneNo()
	{
		return (StaticTextField)getChild(CHILD_STWORKPHONENO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStWorkPhoneExt()
	{
		return (StaticTextField)getChild(CHILD_STWORKPHONEEXT);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApplicantType()
	{
		return (StaticTextField)getChild(CHILD_STAPPLICANTTYPE);
	}


	/**
	 *
	 *
	 */
	public void handleBtDeleteApplicantRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this.getParentViewBean());

		handler.handleDelete(false,
                          1,
                          handler.getRowNdxFromWebEventMethod(event),
                          "RepeatApplicant/hdApplicantId",
                          "borrowerId",
                          "RepeatApplicant/hdApplicantCopyId",
                          "Borrower");

		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtDeleteApplicant()
	{
		return (Button)getChild(CHILD_BTDELETEAPPLICANT);
	}


	/**
	 *
	 *
	 */
	public String endBtDeleteApplicantDisplay(ChildContentDisplayEvent event)
	{

		DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public void handleBtApplicantDetailsRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		// The following code block was migrated from the btApplicantDetails_onWebEvent method
		DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());

		handler.handleApplicantsView(handler.getRowNdxFromWebEventMethod(event), "RepeatApplicant/hdApplicantId", "RepeatApplicant/hdApplicantCopyId");
		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtApplicantDetails()
	{
		return (Button)getChild(CHILD_BTAPPLICANTDETAILS);
	}


	/**
	 *
	 *
	 */
	public doDealEntryApplicantSelectModel getdoDealEntryApplicantSelectModel()
	{
		if (doDealEntryApplicantSelect == null)
			doDealEntryApplicantSelect = (doDealEntryApplicantSelectModel) getModel(doDealEntryApplicantSelectModel.class);
		return doDealEntryApplicantSelect;
	}


	/**
	 *
	 *
	 */
	public void setdoDealEntryApplicantSelectModel(doDealEntryApplicantSelectModel model)
	{
			doDealEntryApplicantSelect = model;
	}

    public void validJSFromTiledView()
    {
            logger = SysLog.getSysLogger("PGDERATV");

            //// Currently it is called from the page. Should be moved to the
            //// TiledView utility classes to generalize the parent (ViewBean name/casting).
            //// ValidationPath is temporarily hardcoded as well. Should be replaced with
            //// the picklist mechanism along with the move to the utility class.

            pgDealEntryViewBean viewBean = (pgDealEntryViewBean) getParentViewBean();

            //logger.debug("PGDERATV@validJSFromTiledView@ParentBean: " + viewBean);

            try
            {
                
                String tileAppValidPath = Sc.DVALID_DEAL_TILE_APPLICANT_PROPS_FILE_NAME;

                //logger.debug("PGDERATV@RatesPath: " + tileAppValidPath);

                JSValidationRules downValidObj = new JSValidationRules(tileAppValidPath, logger, viewBean);
                downValidObj.attachJSValidationRules();
            }
            catch( Exception e )
            {
                logger.warning("PGDERATV@DataValidJSException: " + e);
                e.printStackTrace();
            }
    }



	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_HDAPPLICANTID="hdApplicantId";
	public static final String CHILD_HDAPPLICANTID_RESET_VALUE="";
	public static final String CHILD_HDAPPLICANTCOPYID="hdApplicantCopyId";
	public static final String CHILD_HDAPPLICANTCOPYID_RESET_VALUE="";
	public static final String CHILD_CBPRIMARYAPPLICANT="cbPrimaryApplicant";
	public static final String CHILD_CBPRIMARYAPPLICANT_RESET_VALUE="N";

  //--Release2.1--//
  //// This OptionList population is done now in the BeginDisplay via the BXResources
	////private static OptionList cbPrimaryApplicantOptions=new OptionList(new String[]{"No", "Yes", "Co-App"},new String[]{"N", "Y", "C"});
	private OptionList cbPrimaryApplicantOptions=new OptionList();

	public static final String CHILD_STFIRSTNAME="stFirstName";
	public static final String CHILD_STFIRSTNAME_RESET_VALUE="";
	public static final String CHILD_STMIDDLEINITIAL="stMiddleInitial";
	public static final String CHILD_STMIDDLEINITIAL_RESET_VALUE="";
	public static final String CHILD_STLASTNAME="stLastName";
	public static final String CHILD_STLASTNAME_RESET_VALUE="";
    public static final String CHILD_STSUFFIX="stSuffix";
    public static final String CHILD_STSUFFIX_RESET_VALUE="";
	public static final String CHILD_STLANGUAGE="stLanguage";
	public static final String CHILD_STLANGUAGE_RESET_VALUE="";
	public static final String CHILD_STHOMEPHONENO="stHomePhoneNo";
	public static final String CHILD_STHOMEPHONENO_RESET_VALUE="";
	public static final String CHILD_STWORKPHONENO="stWorkPhoneNo";
	public static final String CHILD_STWORKPHONENO_RESET_VALUE="";
	public static final String CHILD_STWORKPHONEEXT="stWorkPhoneExt";
	public static final String CHILD_STWORKPHONEEXT_RESET_VALUE="";
	public static final String CHILD_STAPPLICANTTYPE="stApplicantType";
	public static final String CHILD_STAPPLICANTTYPE_RESET_VALUE="";
	public static final String CHILD_BTDELETEAPPLICANT="btDeleteApplicant";
	public static final String CHILD_BTDELETEAPPLICANT_RESET_VALUE=" ";
	public static final String CHILD_BTAPPLICANTDETAILS="btApplicantDetails";
	public static final String CHILD_BTAPPLICANTDETAILS_RESET_VALUE=" ";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDealEntryApplicantSelectModel doDealEntryApplicantSelect=null;

  private DealEntryHandler handler=new DealEntryHandler();

  public SysLogger logger;
}

