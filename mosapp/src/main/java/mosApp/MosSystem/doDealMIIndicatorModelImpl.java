package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;

/**
 *
 *
 *
 */
public class doDealMIIndicatorModelImpl extends QueryModelBase
	implements doDealMIIndicatorModel
{
	/**
	 *
	 *
	 */
	public doDealMIIndicatorModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


    /**
     *
     *
     */
    public java.math.BigDecimal getDfCopyId()
    {
        return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
    }


    /**
     *
     *
     */
    public void setDfCopyId(java.math.BigDecimal value)
    {
        setValue(FIELD_DFCOPYID,value);
    }


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIIndicatorId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMIINDICATORID);
	}


	/**
	 *
	 *
	 */
	public void setDfMIIndicatorId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMIINDICATORID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMIIndicatorDescription()
	{
		return (String)getValue(FIELD_DFMIINDICATORDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfMIIndicatorDescription(String value)
	{
		setValue(FIELD_DFMIINDICATORDESCRIPTION,value);
	}

	/**
	 * MetaData object test method to verify the SQL_TEMPLATE query.
	 *
	 */
   /**
        public void setResultSet(ResultSet value)
        {
            logger = SysLog.getSysLogger("METADATA");

            try
            {
                super.setResultSet(value);
                if( value != null)
                {
                    ResultSetMetaData metaData = value.getMetaData();
                    int columnCount = metaData.getColumnCount();
                    logger.debug("===============================================");
                    logger.debug("Testing MetaData Object for new synthetic fields for" +
	                 	            " doDealMIIndicatorModel");
                    logger.debug("NumberColumns: " + columnCount);
                    for(int i = 0; i < columnCount ; i++)
                    {
                        logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
                    }
                    logger.debug("================================================");
                  }
              }
              catch (SQLException e)
              {
                  logger.debug("Class: " + getClass().getName());
                  logger.debug("SQLException: " + e);
              }
        }
     **/
	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL DEAL.DEALID, DEAL.COPYID, MIINDICATOR.MIINDICATORID, MIINDICATOR.MIIDESCRIPTION FROM DEAL, MIINDICATOR  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="DEAL, MIINDICATOR";
	public static final String STATIC_WHERE_CRITERIA=" (DEAL.MIINDICATORID  =  MIINDICATOR.MIINDICATORID)";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFMIINDICATORID="MIINDICATOR.MIINDICATORID";
	public static final String COLUMN_DFMIINDICATORID="MIINDICATORID";
	public static final String QUALIFIED_COLUMN_DFMIINDICATORDESCRIPTION="MIINDICATOR.MIIDESCRIPTION";
	public static final String COLUMN_DFMIINDICATORDESCRIPTION="MIIDESCRIPTION";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIINDICATORID,
				COLUMN_DFMIINDICATORID,
				QUALIFIED_COLUMN_DFMIINDICATORID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIINDICATORDESCRIPTION,
				COLUMN_DFMIINDICATORDESCRIPTION,
				QUALIFIED_COLUMN_DFMIINDICATORDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

