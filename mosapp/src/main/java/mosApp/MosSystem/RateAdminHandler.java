package mosApp.MosSystem;

/**
 * 27/Mar/2007 DVG #DG594 FXP16322: NBC Pricing Rate Inventory - Express and Expert out of sync 
 * 06/Sep/2006 DVG #DG498 #4026  Product Rate Change bug
 */
import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.docprep.Dc;
import com.basis100.deal.entity.DocumentQueue;
import com.basis100.deal.entity.PricingProfile;
import com.basis100.deal.entity.PricingRateInventory;
import com.basis100.deal.entity.PrimeIndexRateInventory;
import com.basis100.deal.entity.PrimeIndexRateProfile;
import com.basis100.deal.pk.DocumentProfilePK;
import com.basis100.deal.pk.PricingProfilePK;
import com.basis100.deal.pk.PrimeIndexRateProfilePK;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.security.PDC;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.util.DBA;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.entity.Page;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HtmlDisplayFieldBase;
import com.iplanet.jato.view.html.TextField;

/**
 * @version 1.1 <br>
 *          Date: 08/04/2006 <br>
 *          Author: NBC/PP Implementation Team <br>
 *          Change: <br>
 *          Changes made to facilitate Rate Synchronization in Expert <br> -
 *          added method isRateSyncSupported <br> - added method
 *          updateExpertRates <br> - modified method insertRateRecord to add
 *          calls to the new procedures <br> - modified method
 *          retrievePricingRateInventory to comment call to
 *          getPendingPriceInventoryId <br>
 */
public class RateAdminHandler extends PageHandlerCommon
	implements Cloneable, Sc
{
	/**
	 *
	 *
	 */
	public RateAdminHandler cloneSS()
	{
		return(RateAdminHandler) super.cloneSafeShallow();

	}
	// following used since handles multiple pages (Rate Administration, Rate History,Rate Update)
	//   appropriate valus set by call to setPageFlag()

	boolean isRateAdmin;
	boolean isRateHistory;
	boolean isRateUpdate;

  //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
  private final static int REASSIGN_PRIME_RATE = 1;
  private final static int ASSIGN_NEW_PRIME_RATE = 2;
  private final static int UNASSIGN_PRIME_RATE = 3;
  private final static int UPDATE_PREVIOUSLY_ASSIGNED_PRIME_RATE = 4;
  private final static int UPDATE_RECORD_WITH_NO_ASSIGNED_PRIME_RATE = 5;

  private final static String ZERO_WITH_RATE_PRECISION = "0.000";
  private final static double ZERO_WITH_RATE_PRECISION_DOUBLE = 0.000;
  //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

	//This method is used to populate the fields in the header
	//with the appropriate information

	public void populatePageDisplayFields()
	{
		logger.debug("--- RateAdminHandler.populatePageDisplayFields ---> populatePageDisplayFields");

		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		populatePageShellDisplayFields();
		populateTaskNavigator(pg);
		populatePreviousPagesLinks();
		setPageFlag(pg);

		if (isRateHistory == true)
		{
			//PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");
			//revisePrevNextButtonGeneration(pg, tds);
			populateHistoryHeader(pst);

      //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
      ViewBean thePage = getCurrNDPage();

      if (getIsDisplayPrimeIndexSection(srk) == false)
      {
        startSupressContent("stIncludePrimeIndxSectionStart", thePage);
        endSupressContent("stIncludePrimeIndxSectionEnd", thePage);

        startSupressContent("Repeated1/stIncludePrimeIndxSectionStart", thePage);
        endSupressContent("Repeated1/stIncludePrimeIndxSectionEnd", thePage);
      }
      //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

		}
		else if (isRateUpdate == true)
		{
			populateMonthsComboBox(pg);

      restoreDeltas(pst);
			setJSValidation();

			customizeTextBox("tbBestRate");

      //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
      customizeTextBox("txBaseRate");
      customizeTextBox("txTeaserRate");

      if (getIsDisplayPrimeIndexSection(srk) == true) {
        populateDefaultValues(getCurrNDPage());
      }

      // 1. Based on the PrimeIndexId <> 0 criteria close access to the :
      // a. PostedRate textbox
      // b. BaseRate textbox
      Integer iBaseIndexId = (Integer) pst.get("chosenBaseIndexId");
      getCurrNDPage().setDisplayFieldValue("hdBaseIndexId", iBaseIndexId);

      if(iBaseIndexId != null && iBaseIndexId.intValue() != 0)
      {
logger.debug("--T--> RAH@populatePageDisplayFields:iBaseIndexId: " + iBaseIndexId.intValue());
        customizeTextBox("tbPostedRate");
        getCurrNDPage().setDisplayFieldValue("hdDummy", iBaseIndexId);

        pg.setPageCondition6(true);
      }

      ViewBean thePage = getCurrNDPage();

      if (getIsDisplayPrimeIndexSection(srk) == false)
      {
        startSupressContent("stIncludePrimeIndxSectionStart", thePage);
        endSupressContent("stIncludePrimeIndxSectionEnd", thePage);
      }
      //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//
		}
		else // RateAdmin case
		{
			if (pst == null)
			{
				logger.error("RATE ADMIN = NO PST (is null)");
				return;
			}
      //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//

      setAdminRepeatedJSValidation();
      ViewBean thePage = getCurrNDPage();

      int userProfileId = srk.getExpressState().getUserProfileId();
      int pageId = pg.getPageId();

logger.debug("--T--> RAH@populatePageDisplayFields:UserProfileId: " + srk.getExpressState().getUserProfileId());
logger.debug("--T--> RAH@populatePageDisplayFields:PageId: " + pg.getPageId());

      int userAccessTypeId = getUserAccessTypeId(userProfileId, pageId);
logger.debug("--T--> RAH@populatePageDisplayFields:UserAccessTypeId: " + userAccessTypeId);

      if (userAccessTypeId == Sc.PAGE_ACCESS_VIEW_ONLY ||
          userAccessTypeId == Sc.PAGE_ACCESS_DISALLOWED)
      {
        reduceAccessToComboBox(thePage,
                               "RepeatedPrimeIndex/cbIndexRateEffMonth",
                               1, //temp
                               theSessionState.getLanguageId(),
                               "MONTHS");


        closeAccessToTextBox("RepeatedPrimeIndex/txIndexRateEffYear");
        closeAccessToTextBox("RepeatedPrimeIndex/txIndexRateEffDay");
        closeAccessToTextBox("RepeatedPrimeIndex/txIndexRateEffHour");
        closeAccessToTextBox("RepeatedPrimeIndex/txIndexRateEffMinute");
        closeAccessToTextBox("RepeatedPrimeIndex/txPrimeIndexEffRate");
      }

      PageCursorInfo tds2 = (PageCursorInfo)pst.get("RepeatedPrimeIndex");

      // 1. Based on the PrimeIndexId <> 0 criteria close access to the :
      // a. Effective Prime Index set
      // b. BaseRate textbox
      // 2. Based on the PrimeIndexDisDate is not null criteria close access to these fields as well

      if (getIsDisplayPrimeIndexSection(srk) == false)
      {
        startSupressContent("stIncludePrimeIndxSectionStart", thePage);
        endSupressContent("stIncludePrimeIndxSectionEnd", thePage);

        startSupressContent("Repeated1/stIncludePrimeIndxSectionStart", thePage);
        endSupressContent("Repeated1/stIncludePrimeIndxSectionEnd", thePage);

        startSupressContent("RepeatedPrimeIndex/stIncludePrimeIndxSectionStart", thePage);
        endSupressContent("RepeatedPrimeIndex/stIncludePrimeIndxSectionEnd", thePage);
      }
      //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//
		}
	}

//--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
  public void setAdminRepeatedJSValidation()
  {
    HtmlDisplayFieldBase df = null;
    String theExtraHtml = null;
    ViewBean thePage = getCurrNDPage();

    df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedPrimeIndex/txIndexRateEffHour");
    theExtraHtml = "onBlur=\"isFieldInIntRange(0,23);\"";
    df.setExtraHtml(theExtraHtml);

    df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedPrimeIndex/txIndexRateEffMinute");
    theExtraHtml = "onBlur=\"isFieldInIntRange(0,59);\"";
    df.setExtraHtml(theExtraHtml);

    // Set the compare date to today
    //  Validate Month
    df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedPrimeIndex/cbIndexRateEffMonth");
    theExtraHtml = "onBlur=\"isFieldValidDate('txIndexRateEffYear', 'cbIndexRateEffMonth', 'txIndexRateEffDay');\"";
    df.setExtraHtml(theExtraHtml);

    //  Validate Day
    df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedPrimeIndex/txIndexRateEffDay");
    theExtraHtml = "onBlur=\"if(isFieldInIntRange(1, 31)) " + "isFieldValidDate('txIndexRateEffYear', 'cbIndexRateEffMonth', 'txIndexRateEffDay');\"";
    df.setExtraHtml(theExtraHtml);

    // Validate Year
    df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedPrimeIndex/txIndexRateEffYear");
    theExtraHtml = "onBlur=\"if(isFieldInIntRange(1800, 2100)) " + "isFieldValidDate('txIndexRateEffYear', 'cbIndexRateEffMonth', 'txIndexRateEffDay');\"";
    df.setExtraHtml(theExtraHtml);

    df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedPrimeIndex/txPrimeIndexEffRate");
    theExtraHtml = "onBlur=\"if(isFieldDecimal(3)) (isFieldInDecRange(1, 15))\"";
    df.setExtraHtml(theExtraHtml);
  }
//--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

	public void setJSValidation()
	{
		// Validate # of Dependants
		HtmlDisplayFieldBase df = null;
		String theExtraHtml = null;
		ViewBean thePage = getCurrNDPage();

		df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbHour");
		theExtraHtml = "onBlur=\"isFieldInIntRange(0,23);\"";
		df.setExtraHtml(theExtraHtml);

		df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbMinute");
		theExtraHtml = "onBlur=\"isFieldInIntRange(0,59);\"";
		df.setExtraHtml(theExtraHtml);

		// Set the compare date to today
		//  Validate Month
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("cbMonth");
		theExtraHtml = "onBlur=\"isFieldValidDate('tbYear', 'cbMonth', 'tbDay');\"";
		df.setExtraHtml(theExtraHtml);

		//  Validate Day
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbDay");
		theExtraHtml = "onBlur=\"if(isFieldInIntRange(1, 31)) " + "isFieldValidDate('tbYear', 'cbMonth', 'tbDay');\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Year
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbYear");
		theExtraHtml = "onBlur=\"if(isFieldInIntRange(1800, 2100)) " + "isFieldValidDate('tbYear', 'cbMonth', 'tbDay');\"";
		df.setExtraHtml(theExtraHtml);

		df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbPostedRate");
		theExtraHtml = "onBlur=\"if(isFieldDecimal(3)) {if(isFieldInDecRange(1, 100)) " + "changeBestRate('tbPostedRate');}\"";
		df.setExtraHtml(theExtraHtml);

    //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    df = (HtmlDisplayFieldBase)thePage.getDisplayField("txBaseRate");
    theExtraHtml = "onBlur=\"if(isFieldDecimal(3)) (isFieldInDecRange(0, 100))\"";
    df.setExtraHtml(theExtraHtml);

    df = (HtmlDisplayFieldBase)thePage.getDisplayField("txTeaserRate");
    theExtraHtml = "onBlur=\"if(isFieldDecimal(3)) (isFieldInDecRange(0, 100))\"";
    df.setExtraHtml(theExtraHtml);

    df = (HtmlDisplayFieldBase)thePage.getDisplayField("txBaseAdjustment");
    theExtraHtml = "onBlur=\"if(isFieldDecimal(3)) (isFieldInFloatRange(-10, 100))\"";
    df.setExtraHtml(theExtraHtml);

    df = (HtmlDisplayFieldBase)thePage.getDisplayField("txTeaserDiscount");
    theExtraHtml = "onBlur=\"if(isFieldDecimal(3)) (isFieldInDecRange(0, 100))\"";
    df.setExtraHtml(theExtraHtml);

    df = (HtmlDisplayFieldBase)thePage.getDisplayField("txTeaserTerms");
    theExtraHtml = "onBlur=\"isFieldInIntRange(0,1000);\"";
    df.setExtraHtml(theExtraHtml);
    //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

		df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbMaxDiscountRate");
    theExtraHtml = "onBlur=\"if(isFieldDecimal(3)) (isFieldInFloatRange(-10, 100))\"";
    df.setExtraHtml(theExtraHtml);
		// Get Max Discount from properties file -- By BILLY 01Nov2001
		//  Set to Posted Rate if not set = ""

    //--Ticket468--start--15July2004//
    // New property defines the possible negative range for CAP rate adjustment.
    int institutionId = srk.getExpressState().getDealInstitutionId();
    String minNegativeDis = PropertiesCache.getInstance().getProperty(
            institutionId,"com.basis100.rateadmin.capdifferential", "D");
    String maxDis = PropertiesCache.getInstance().getProperty(
            institutionId,"com.basis100.rateadmin.maxdiscount", "D");

logger.debug("--T--> RateAdminHandler: Max discount: " + maxDis +  " CAP: " + minNegativeDis);

		if ((maxDis == null || maxDis.trim().equals("D")) && (minNegativeDis == null || minNegativeDis.trim().equals("D")))
		{
			// If both properties are not set , user can type decimal value in a range (0, PostedInterestRate)
      // and the Best Rate to the posted rate
logger.debug("--T--> RateAdminHandler: Max discount and CAP are not defined");

			theExtraHtml = "onBlur=\"if(isFieldDecimal(3)) {if(isFieldInFloatRange(0, getPostRate())) " + "changeBestRate('tbMaxDiscountRate');}\"";
		}
    else if (maxDis != null && !maxDis.trim().equals("D") && (minNegativeDis == null || minNegativeDis.trim().equals("D")))
    {
      // If only maxdiscount property is set, user can type decimal value in a range (0, + maxdiscount)
      // and the Best Rate = Posted Rate - maxdiscount
logger.debug("--T--> RateAdminHandler: Max discount is defined, " + maxDis +  "CAP is not: " + minNegativeDis);
      theExtraHtml = "onBlur=\"if(isFieldDecimal(3)) {if(isFieldInFloatRange(0, " + maxDis + ")) " + "changeBestRate('tbMaxDiscountRate');}\"";
    }
    else if (minNegativeDis != null && !minNegativeDis.trim().equals("D") && (maxDis == null || maxDis.trim().equals("D")))
    {
      // If only capdifferential property is set, user can type decimal value in a range (-capdifferential, 0)
      // and the CAP Rate (Best Rate in this case is called CAP) = Posted Rate - ( - capdifferential).
logger.debug("--T--> RateAdminHandler: MaxDiscount is not defined, CAP is defined");
      theExtraHtml = "onBlur=\"if(isFieldDecimal(3)) {if(isFieldInFloatRange(" + minNegativeDis + ", getPostRate())) " + "changeBestRate('tbMaxDiscountRate');}\"";
    }
		else if (minNegativeDis != null && !minNegativeDis.trim().equals("D") && (maxDis != null && !maxDis.trim().equals("D")))
		{
      // If both properties are set, user can type decimal value in a range (-capdifferential, + maxdiscount) and
      // 1. the positive value is typed:
      // Best Rate = Posted Rate - maxdiscount.
      // 2. the positive value is typed:
      // CAP Rate (Best Rate in this case is called CAP) = Posted Rate - ( - capdifferential).
logger.debug("--T--> RateAdminHandler: Max discount and CAP are both defined");

      theExtraHtml = "onBlur=\"if(isFieldInFloatRange("+ minNegativeDis + ", " + maxDis + ")) " + "changeBestRate('tbMaxDiscountRate');\"";
		}
    //--Ticket468--end--//

		df.setExtraHtml(theExtraHtml);
	}

	/////////////////////////////////////////////////////////////////////////////
	//
	// Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
	//
	public void setupBeforePageGeneration()
	{
		logger.debug("--T--> RateAdminHandler.setupBeforePageGeneration: IN");
		PageEntry pg = getTheSessionState().getCurrentPage();

		if (pg.getSetupBeforeGenerationCalled() == true) return;
		pg.setSetupBeforeGenerationCalled(true);

		// -- //
		logger.debug("--T--> RateAdminHandler.setupBeforePageGeneration: PROCESSING");

		int numRows = 0;

		try
		{
			ViewBean currNDPage = getCurrNDPage();
			setPageFlag(pg);
			if (isRateUpdate == true)
			{
				logger.trace("--T--> RateAdminHandler.setupBeforPageGeneration: Rate Update");

				// set row generator for one row - to create a DO binding only
				setRowGeneratorDOForNRows(1);
				return;
			} // end of RateUpdate true

			PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");
			Hashtable pst = pg.getPageStateTable();
			if (tds == null)
			{
				String voName = "Repeated1";
				int perPage =((TiledView)(getCurrNDPage().getChild(voName))).getMaxDisplayTiles();
				tds = new PageCursorInfo("DEFAULT", voName, perPage, - 1);
				tds.setRefresh(true);
				pst.put(tds.getName(), tds);
			}

      //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
      // set the criteria for the populating model
      int numRows2 = 0;

      doPrimeIndexRateModel thePrimeIndxModel = (doPrimeIndexRateModel)
        (RequestManager.getRequestContext().getModelManager().getModel(doPrimeIndexRateModel.class));

      thePrimeIndxModel.clearUserWhereCriteria();

      thePrimeIndxModel.executeSelect(null);
      numRows2 = thePrimeIndxModel.getSize();
logger.trace("VLAD ==> RAH@setupBeforePageGeneration: Number of thePrimeIndxModel = " + numRows2);

      PageCursorInfo tds2 = (PageCursorInfo)pst.get("RepeatedPrimeIndex");

      if (tds2 == null)
      {
logger.trace("VLAD ==> RAH@setupBeforePageGeneration:tds2 null mode.");
        String voRepPrimeIndx = "RepeatedPrimeIndex";
        tds2 = new PageCursorInfo("RepeatedPrimeIndex", voRepPrimeIndx, numRows2 , -1);
        tds2.setRefresh(true);
        pst.put(tds2.getName(), tds2);
      }
      else
      {
        tds2.setTotalRows(numRows2);
        tds2.setRowsPerPage(numRows2);
      }
      //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

			if (isRateAdmin == true)
			{
				logger.trace("--T--> RateAdminHandler.setupBeforPageGeneration: Rate Admin");
				numRows = getNumberOfLattestRates();
				tds.setTotalRows(numRows);
logger.trace("RAH@RateAdminMode: NUMROWS_Repeated1 = " + numRows);
        if (tds.isTotalRowsChanged() == true || tds.getRefresh() == true)
				{
					getLattestRatesData(getSessionResourceKit(), pst);
          pst.put("ROWNDX", new Integer(0));

					logger.trace("RENEWAL");
				}

				setRowGeneratorDOForNRows(tds.getTotalRows());

			} // end of RateAdmin true

			if (isRateHistory == true)
			{
				logger.trace("--T--> RateAdminHandler.setupBeforPageGeneration: Rate History");
				QueryModelBase historyDO =(QueryModelBase)
          (RequestManager.getRequestContext().getModelManager().getModel(doInterestRateHistoryModel.class));
				historyDO.clearUserWhereCriteria();
				String rateCode =(String) pst.get("chosenRateCode");
				historyDO.addUserWhereCriterion("dfRateCode", "=", new String(rateCode));

				//logger.trace("SQL filter =" + historyDO.getDynamicCriteria());
				historyDO.executeSelect(null);

				//logger.trace(historyDO.getLastResults().getResultStatus().getVendorErrorMessage1());
				//logger.trace(historyDO.getLastResults().getResultStatus().getVendorErrorMessage2());
				numRows = historyDO.getSize();
logger.debug("BILLY ===> @RateAdminHandler.setupBeforPageGeneration: Total Rate History = " + numRows);

				tds.setTotalRows(numRows);
			} // end of Rate History true
      //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

		}
		catch(Exception ex)
		{
			logger.error("@RateAdminHandler.setupBeforePageGeneration:: Exception encountered");
			logger.error(ex);

			// attempt to avoid down stream failures
			getPageCursorInfo(pg, "DEFAULT");
			Hashtable pst0 = pg.getPageStateTable();
			PageCursorInfo tds0 = new PageCursorInfo("DEFAULT", "Repeated1", 5, 0);
			pst0.put(tds0.getName(), tds0);
			setStandardFailMessage();
		}

	}

	////////////////////////////////////////////////////////////////////////
	private void setPageFlag(PageEntry pg)
	{
		isRateAdmin = false;
		isRateHistory = false;
		isRateUpdate = false;

		if (pg.getPageId() == Mc.PGNM_INTEREST_RATES_ADMIN)
		{
			isRateAdmin = true;
			return;
		}

		if (pg.getPageId() == Mc.INTEREST_RATES_HISTORY)
		{
			isRateHistory = true;
			return;
		}

		isRateUpdate = true;
	}

	///////////////////////////////////////////////////////////////////////////
	private int getNumberOfLattestRates()
	{
		try
		{
			String sqlStr = "select count(*) from pricingprofile p1 where p1.ratecode in " + "(select distinct p.ratecode from PRICINGRATEINVENTORY I, pricingprofile p " + "where p.PRICINGPROFILEID=i.PRICINGPROFILEID)";
			JdbcExecutor jExec = srk.getJdbcExecutor();
			int key = jExec.execute(sqlStr);
			int count = 0;
			while(jExec.next(key))
			{
				count = jExec.getInt(key, 1);
				break;
			}
			jExec.closeData(key);
			return count;
		}
		catch(Exception e)
		{
			logger.trace("Rates Number Error!!! " + e);		//#DG594 improve loging
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
		}
		return 0;
	}

 //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
	private void getLattestRatesData(SessionResourceKit srk, Hashtable pst)
	{
		logger.debug("--- RateAdminHandler ---> getLattestRatesData");

		try
		{
       //original query with no PrimeIndexRate related business.
       /**
        sqlStr = "SELECT P.RATECODE, MIN(S.PRICINGSTATUSID), MIN(S.PRICINGSTATUSDESCRIPTION),"
        + " MIN(P.RATECODEDESCRIPTION), MIN(I.INDEXEFFECTIVEDATE), MIN(I.INTERNALRATEPERCENTAGE),"
        + " MIN(I.MAXIMUMDISCOUNTALLOWED), MIN(I.INDEXEXPIRYDATE), MIN(I.PRICINGRATEINVENTORYID),"
        + " MIN(I.PRICINGPROFILEID),  MIN(P.RATEDISABLEDDATE)"
        + " FROM PRICINGRATEINVENTORY I, PRICINGPROFILE P, PRICINGSTATUS S"
        + " WHERE P.PRICINGPROFILEID = I.PRICINGPROFILEID AND"
        + " P.PRICINGSTATUSID = S.PRICINGSTATUSID AND"
        + " I.INDEXEFFECTIVEDATE = (SELECT MAX(I1.INDEXEFFECTIVEDATE)"
        + " FROM PRICINGRATEINVENTORY I1,PRICINGPROFILE P1 WHERE"
        + " P1.PRICINGPROFILEID = I1.PRICINGPROFILEID AND"
        + " P1.RATECODE = P.RATECODE)"
        + " GROUP BY P.RATECODE "
        + " ORDER BY P.RATECODE ASC ";
      **/

       String sqlStr = "SELECT P.RATECODE, MIN(S.PRICINGSTATUSID), MIN(S.PRICINGSTATUSDESCRIPTION),"
       + " MIN(P.RATECODEDESCRIPTION), MIN(I.INDEXEFFECTIVEDATE), MIN(I.INTERNALRATEPERCENTAGE),"
       + " MIN(I.MAXIMUMDISCOUNTALLOWED), MIN(I.INDEXEXPIRYDATE), MIN(I.PRICINGRATEINVENTORYID),"
       + " MIN(I.PRICINGPROFILEID), MIN(P.RATEDISABLEDDATE),"

       //--Ticket#1736--18July2005--start--//
       //+ " MIN(I.INTERNALRATEPERCENTAGE) - MIN(M.TEASERDISCOUNT),"
       + " MIN(I.INTERNALRATEPERCENTAGE) - MIN(I.TEASERDISCOUNT),"
       //--Ticket#1736--18July2005--end--//

       + " MIN(PRII.PRIMEINDEXRATEPROFILEID), MIN(PRII.PRIMEINDEXRATE), MIN(I.PRIMEBASEADJ),"
       //--Ticket#1736--18July2005--start--//
       //+ " MIN(M.TEASERDISCOUNT), MIN(M.TEASERTERM), MIN(PRII.PRIMEINDEXEFFDATE)"
       + " MIN(I.TEASERDISCOUNT), MIN(I.TEASERTERM), MIN(PRII.PRIMEINDEXEFFDATE)"
       //--Ticket#1736--18July2005--end--//

       + " FROM PRICINGRATEINVENTORY I, PRICINGPROFILE P, PRICINGSTATUS S,"
       + " PRIMEINDEXRATEPROFILE PRIP, MTGPROD M, PRIMEINDEXRATEINVENTORY PRII"

       + " WHERE P.PRICINGPROFILEID = I.PRICINGPROFILEID AND"
       + " P.PRICINGSTATUSID = S.PRICINGSTATUSID AND"

       + " (P.PRIMEINDEXRATEPROFILEID  =  PRIP.PRIMEINDEXRATEPROFILEID) AND"
       + " (M.PRICINGPROFILEID  =  P.PRICINGPROFILEID) AND"

       + " (I.PRIMEINDEXRATEINVENTORYID  =  PRII.PRIMEINDEXRATEINVENTORYID) AND"

       + " I.INDEXEFFECTIVEDATE = (SELECT MAX(I1.INDEXEFFECTIVEDATE)"
       + " FROM PRICINGRATEINVENTORY I1,PRICINGPROFILE P1, PRIMEINDEXRATEPROFILE PRIP1,"
       + " MTGPROD M1, PRIMEINDEXRATEINVENTORY PRII1 WHERE"
       + " P1.PRICINGPROFILEID = I1.PRICINGPROFILEID AND"

       + " P1.PRIMEINDEXRATEPROFILEID  =  PRIP1.PRIMEINDEXRATEPROFILEID AND"
       + " (M1.PRICINGPROFILEID  =  P1.PRICINGPROFILEID) AND"

       + " (I1.PRIMEINDEXRATEINVENTORYID  =  PRII1.PRIMEINDEXRATEINVENTORYID) AND"

       + " P1.RATECODE = P.RATECODE)"
       + " GROUP BY P.RATECODE "
       + " ORDER BY P.RATECODE ASC ";
      //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

logger.debug("--- RateAdminHandler ---> getLattestRatesData:SQL: " + sqlStr);

			JdbcExecutor jExec = srk.getJdbcExecutor();
			int key = jExec.execute(sqlStr);
			int numRows = jExec.getRowCount(key);

			String [ ] sRateCode =(String [ ]) Array.newInstance(String.class, numRows);
			int [ ] iStatusId =(int [ ]) Array.newInstance(int.class, numRows);
			String [ ] sStatusDescription =(String [ ]) Array.newInstance(String.class, numRows);
			String [ ] sRateCodeDescription =(String [ ]) Array.newInstance(String.class, numRows);
			Date [ ] dtEffectiveDate =(Date [ ]) Array.newInstance(Date.class, numRows);
			double [ ] dbPostedRate =(double [ ]) Array.newInstance(double.class, numRows);
			double [ ] dbMaxDiscountRate =(double [ ]) Array.newInstance(double.class, numRows);
			Date [ ] dtExpiryDate =(Date [ ]) Array.newInstance(Date.class, numRows);
			double [ ] dbInventoryId =(double [ ]) Array.newInstance(double.class, numRows);
			double [ ] dbProfileId =(double [ ]) Array.newInstance(double.class, numRows);
			Date [ ] dtDisabledDate =(Date [ ]) Array.newInstance(Date.class, numRows);
			//int [ ] realStatusId =(int [ ]) Array.newInstance(int.class, numRows);

      //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
      double [ ] dbTeaserRate =(double [ ]) Array.newInstance(double.class, numRows);
      int [ ] iBaseIndexId =(int [ ]) Array.newInstance(int.class, numRows);
      double [ ] dbBaseRate =(double [ ]) Array.newInstance(double.class, numRows);
      double [ ] dbBaseAdjustment =(double [ ]) Array.newInstance(double.class, numRows);
      double [ ] dbTeaserDiscount =(double [ ]) Array.newInstance(double.class, numRows);
      int [ ] iTeaserTerms =(int [ ]) Array.newInstance(int.class, numRows);
      Date [ ] dtBaseRateEffDate =(Date [ ]) Array.newInstance(Date.class, numRows);
      //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

			int i = 0;

      int languageId = theSessionState.getLanguageId();
	  int institutionId = srk.getExpressState().getDealInstitutionId();
logger.debug("--- RateAdminHandler ---> numOfRows == " + numRows + " LanguageId == " + languageId);
			while(jExec.next(key))
			{
				Array.set(sRateCode, i, jExec.getString(key, 1));
				Array.setInt(iStatusId, i, jExec.getInt(key, 2));
        //--Release2.1--//
        //--> Get description fields from ResourceBundle
        //--> By Billy 27Nov2002
				//Array.set(sStatusDescription, i, jExec.getString(key, 3));
				//Array.set(sRateCodeDescription, i, jExec.getString(key, 4));
        Array.set(sStatusDescription, i,
                 BXResources.getPickListDescription(institutionId, "PRICINGSTATUS", jExec.getInt(key, 2), languageId));

logger.debug("VLAD ===> StatusDescription ["+i+"] = " + BXResources.getPickListDescription(institutionId, "PRICINGSTATUS", jExec.getInt(key, 2), languageId));

				Array.set(sRateCodeDescription, i,
                  BXResources.getPickListDescription(institutionId, "PRICINGPROFILE", new Double(jExec.getDouble(key, 10)).intValue(), languageId));
logger.debug("VLAD ===> RateCodeDescription ["+i+"] = " + BXResources.getPickListDescription(institutionId, "PRICINGPROFILE", new Double(jExec.getDouble(key, 10)).intValue(), languageId));

				Array.set(dtEffectiveDate, i, jExec.getDate(key, 5));
				Array.setDouble(dbPostedRate, i, jExec.getDouble(key, 6));
				Array.setDouble(dbMaxDiscountRate, i, jExec.getDouble(key, 7));
				if (jExec.getDate(key, 8) == null) Array.set(dtExpiryDate, i, new Date(0, 0, 0));
				else Array.set(dtExpiryDate, i, jExec.getDate(key, 8));

				Array.setDouble(dbInventoryId, i, jExec.getDouble(key, 9));
				Array.setDouble(dbProfileId, i, jExec.getDouble(key, 10));
        Array.set(dtDisabledDate, i, jExec.getDate(key, 11));

        //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
        Array.setDouble(dbTeaserRate, i, jExec.getDouble(key, 12));

logger.debug("VLAD ===> IndexValue ["+i+"] = " + " BaseIndexId: " + jExec.getInt(key, 13));
        Array.setInt(iBaseIndexId, i, jExec.getInt(key, 13));

logger.debug("VLAD ===> IndexValue ["+i+"] = " + " BaseRate: " + jExec.getDouble(key, 14));
       Array.setDouble(dbBaseRate, i, jExec.getDouble(key, 14));

logger.debug("VLAD ===> IndexValue ["+i+"] = " + " BaseAdjustment: " + jExec.getDouble(key, 15));

        Array.setDouble(dbBaseAdjustment, i, jExec.getDouble(key, 15));
logger.debug("VLAD ===> IndexValue ["+i+"] = " + " TeaserDiscount: " + jExec.getDouble(key, 16));

        Array.setDouble(dbTeaserDiscount, i, jExec.getDouble(key, 16));
logger.debug("VLAD ===> IndexValue ["+i+"] = " + " TeaserTerms: " + jExec.getDouble(key, 17));

			  Array.setInt(iTeaserTerms, i, jExec.getInt(key, 17));

        if (jExec.getDate(key, 18) == null) Array.set(dtBaseRateEffDate, i, new Date(0, 0, 0));
        else Array.set(dtBaseRateEffDate, i, jExec.getDate(key, 18));
logger.debug("VLAD ===> IndexValue ["+i+"] = " + " BaseRateEffDate: " + jExec.getDate(key, 18));
       //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

				i ++;
			}

			jExec.closeData(key);
			pst.put("sRateCode", sRateCode);
			pst.put("iStatusId", iStatusId);
			pst.put("sStatusDescription", sStatusDescription);
			pst.put("sRateCodeDescription", sRateCodeDescription);
			pst.put("dtEffectiveDate", dtEffectiveDate);
			pst.put("dbPostedRate", dbPostedRate);
			pst.put("dbMaxDiscountRate", dbMaxDiscountRate);
			pst.put("dtExpiryDate", dtExpiryDate);
			pst.put("dbInventoryId", dbInventoryId);
			pst.put("dbProfileId", dbProfileId);
			pst.put("dtDisabledDate", dtDisabledDate);

      //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
      pst.put("dbTeaserRate", dbTeaserRate);
      pst.put("iBaseIndexId", iBaseIndexId);
      pst.put("dbBaseRate", dbBaseRate);
      pst.put("dbBaseAdjustment", dbBaseAdjustment);
      pst.put("dbTeaserDiscount", dbTeaserDiscount);
      pst.put("iTeaserTerms", iTeaserTerms);
      pst.put("dtBaseRateEffDate", dtBaseRateEffDate);
      //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

			return;
		}
		catch(Exception e)
		{
			logger.trace("Getting Rates Error!!! " + e);		//#DG594 improve loging
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
			return;
		}

	}

	public void handleSubmit()
	{
		handleCancelStandard();

	}

  //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
  /**
   *  Populate default values (hiddens, etc.).
   *
   */
  private void populateDefaultValues(ViewBean thePage)
  {
    PageEntry pg = theSessionState.getCurrentPage();
    String theExtraHtml = "";

    // the OnChange string to be set on the fields
    HtmlDisplayFieldBase df;

    df = (HtmlDisplayFieldBase)thePage.getDisplayField("cbBaseIndex");
    theExtraHtml = "onChange=\"populateValueTo(['hdBaseIndexId']);"
      + " SetPrimeIndexHide(); \"";
    df.setExtraHtml(theExtraHtml);
  }

  /**
   *  Display or not the recalc button based on PrimeIndexRateId <> 0 condition.
   *
   */
  public boolean displayRecalcButton()
  {
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    if (pg.getPageCondition6() == true) return true;

    return false;
  }

  /**
   *
   *  6. Repopulate the text boxes related to the #2 change on 'Recalculate' button
   *  action. ==> separate function.
   *
   */
   public void handleRedisplayPrimeRate()
   {
     PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
     SessionResourceKit srk = getSessionResourceKit();
     Hashtable pst = pg.getPageStateTable();

     int newBaseIndex = com.iplanet.jato.util.TypeConverter.asInt(getCurrNDPage().getDisplayFieldValue("cbBaseIndex"));
     int origBaseIndex = com.iplanet.jato.util.TypeConverter.asInt((Object) pst.get("chosenBaseIndexId"));

     int pricingUpdateType = getMtgRatePrimeRateRelation(pg.getPageCondition6(),
                                                         newBaseIndex,
                                                         origBaseIndex);

//temp debug
     logger.debug("--- RAH@handleRedisplayPrimeRate::OrigBaseIndexIdVal: " + origBaseIndex);
     logger.debug("--- RAH@handleRedisplayPrimeRate::IndexIdFromScreen: " + newBaseIndex);
     logger.debug("--- RAH@handleRedisplayPrimeRate::PgCond6: " + pg.getPageCondition6());
     logger.debug("--- RAH@handleRedisplayPrimeRate::PricingUpdateType: " + pricingUpdateType);

     // Update all necessary doubles here: the PrimeRate, PostedRate, Teaser Rate,
     // and BestRate on the screen via "delta" values.
     //Ticket--#1688--14Jul2005--start--//
     if (pricingUpdateType == UPDATE_RECORD_WITH_NO_ASSIGNED_PRIME_RATE)
     //Ticket--#1688--14Jul2005--end--//
     {
         // do not update values in this mode
         try
         {
           getSavedPages().setNextPage(pg);
           navigateToNextPage();
         }
         catch(Exception e)
         {
           srk.cleanTransaction();
           logger.error("Error calculating Prime Rate");
           logger.error(e);
           setStandardFailMessage();
         }
        handleCancelStandard();
     }
     //Ticket--#1688--14Jul2005--start--//
     else if (pricingUpdateType == UPDATE_PREVIOUSLY_ASSIGNED_PRIME_RATE ||
              pricingUpdateType == ASSIGN_NEW_PRIME_RATE ||
              pricingUpdateType == REASSIGN_PRIME_RATE ||
              pricingUpdateType == UNASSIGN_PRIME_RATE)
     //Ticket--#1688--14Jul2005--end--//
    {

       try
       {
         PrimeIndexRateProfilePK piripk = new PrimeIndexRateProfilePK(newBaseIndex);
         PrimeIndexRateProfile primeIndexRateProfile = new PrimeIndexRateProfile(srk, newBaseIndex);

         PrimeIndexRateInventory primeInventoryLatestRate =
           getLatestPrimeIndexRateInventory(primeIndexRateProfile);

//temp
logger.debug("--- handleRedisplayPrimeRate::PrimeIndexRateInventoryId: " + primeInventoryLatestRate.getPrimeIndexRateInventoryId());
logger.debug("--- handleRedisplayPrimeRate::GetPrimeIndexRate: " + primeInventoryLatestRate.getPrimeIndexRate());

         saveDeltas(pst, pg, primeInventoryLatestRate);
         getSavedPages().setNextPage(pg);

         navigateToNextPage();
       }
       catch (Exception ex) {
         logger.error(
           "RAH@HandleRedisplayPrimeRate: Exception redisplaying the Rate: " +
           ex);
       }
     }
   }
  //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//
	public boolean populateRepeatedFields(int ndx)
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

logger.debug("BILLY ===> @populateRepeatedFields tds.getFirstDisplayRowNumber():" + tds.getFirstDisplayRowNumber());
			setPageFlag(pg);
			int absNdx = getCursorAbsoluteNdx(tds, ndx);
      //--Release2.1--//
      //--> Convert Date / Currency format with current language
      //--> By Billy 27Nov2002
      Locale theLocale = new Locale(BXResources.getLocaleCode(theSessionState.getLanguageId()), "");
			SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy HH:mm", theLocale);
			DecimalFormat floatFmt = new DecimalFormat("#0.000", new DecimalFormatSymbols(theLocale));
      //========================================================
			int num = tds.getNumberOfDisplayRowsOnPage();
			logger.debug("--- RateAdminHandler ---> num == " + num);
			logger.debug("--- RateAdminHandler ---> ndx == " + ndx);
      logger.debug("--- RateAdminHandler ---> absNdx == " + absNdx);

			if (isRateAdmin == true)
			{
				logger.debug("--- RateAdminHandler ---> populateRepeatedFields for RateAdmin");
				if (ndx >= num)
				{
					return false;
				}
        //--Release2.1--//
        //--> Get description fields from ResourceBundle
        //--> By Billy 28Nov2002
				String [ ] sRateCode =(String [ ]) pst.get("sRateCode");
                
                /***** GR 3.2.2 code change for curent index exceed ratecode array *****/
                if (absNdx >= sRateCode.length)
                    return false;
                /***** GR 3.2.2 code change for curent index exceed ratecode array *****/

				//String [ ] sStatusDescription =(String [ ]) pst.get("sStatusDescription");
				int [ ] iStatusId =(int [ ]) pst.get("iStatusId");
				//String [ ] sRateCodeDescription =(String [ ]) pst.get("sRateCodeDescription");
				Date [ ] dtEffectiveDate =(Date [ ]) pst.get("dtEffectiveDate");
				double [ ] dbPostedRate =(double [ ]) pst.get("dbPostedRate");
				double [ ] dbMaxDiscountRate =(double [ ]) pst.get("dbMaxDiscountRate");
        //--> Added ProfileId in order to get RateDescription from resourceBundle
        double [ ] dbProfileId =(double [ ]) pst.get("dbProfileId");

        //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
        //double formattedFloatZero = 0.000;
        double floatZero = 0.0;
        double [ ] dbTeaserRate =(double [ ]) pst.get("dbTeaserRate");
        int [ ] iBaseIndexId =(int [ ]) pst.get("iBaseIndexId");
        double [ ] dbBaseRate =(double [ ]) pst.get("dbBaseRate");
        double [ ] dbBaseAdjustment =(double [ ]) pst.get("dbBaseAdjustment");
        double [ ] dbTeaserDiscount =(double [ ]) pst.get("dbTeaserDiscount");
        int [ ] iTeaserTerms =(int [ ]) pst.get("iTeaserTerms");
        Date [ ] dtBaseRateEffDate =(Date [ ]) pst.get("dtBaseRateEffDate");
        //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

				getCurrNDPage().setDisplayFieldValue("Repeated1/stCode", new String((String) Array.get(sRateCode, absNdx)));

				// Pending/Disabled Status
        //--> Get Labels from ResourceBundle
				getCurrNDPage().setDisplayFieldValue("Repeated1/stStatus",
          new String(Array.getInt(iStatusId, absNdx) == Mc.PRICING_STATUS_DISABLED ? BXResources.getGenericMsg("DISABLE_LABEL", theSessionState.getLanguageId())
            :(((Date) Array.get(dtEffectiveDate, absNdx)).after(new Date()) ? BXResources.getGenericMsg("PENDING_LABEL", theSessionState.getLanguageId())
            : "-")));

        //--> Get RateCodeDescription from Bundle based on ProfileId
				//getCurrNDPage().setDisplayFieldValue("Repeated1/stDescription", new String((String) Array.get(sRateCodeDescription, absNdx)));
        getCurrNDPage().setDisplayFieldValue("Repeated1/stDescription",
            new String(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(), "PRICINGPROFILE",
              new Double(Array.getDouble(dbProfileId, absNdx)).intValue(),
              theSessionState.getLanguageId())));

				Date t =(Date) Array.get(dtEffectiveDate, absNdx);
logger.debug("---RateAdminHandler---> t == " + t.toString());
				getCurrNDPage().setDisplayFieldValue("Repeated1/stEffectiveDate", new String(dateFormat.format((Date) Array.get(dtEffectiveDate, absNdx))));

				getCurrNDPage().setDisplayFieldValue("Repeated1/stMaxDiscountRate", new String("" + floatFmt.format(Array.getDouble(dbMaxDiscountRate, absNdx))));

				getCurrNDPage().setDisplayFieldValue("Repeated1/stPostedRate", new String("" + floatFmt.format(Array.getDouble(dbPostedRate, absNdx))));
				double bestrate = Array.getDouble(dbPostedRate, absNdx) - Array.getDouble(dbMaxDiscountRate, absNdx);

				getCurrNDPage().setDisplayFieldValue("Repeated1/stBestRate", new String("" + floatFmt.format(bestrate)));
logger.debug("---RateAdminHandler---> BestRate == " + floatFmt.format(bestrate));

        //--> As the hdRowNdx is bind to the RowGenerator Model, it is not necessaary to set the value here !!
        //--> By Billy 05Sept
				//getCurrNDPage().setDisplayFieldValue("Repeated1/hdRowNdx", new Integer(ndx));

        //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
        logger.debug("---RateAdminHandler---> TeaserRate[" + absNdx + "]  == " + Array.getDouble(dbTeaserRate, absNdx));
        String tmpStr =  "";

        if( Array.getDouble(dbTeaserRate, absNdx) != floatZero &&
            Array.getDouble(dbTeaserDiscount, absNdx) != floatZero) {
         tmpStr =  floatFmt.format(Array.getDouble(dbTeaserRate, absNdx));
        } else {
          tmpStr = "";
        }
        getCurrNDPage().setDisplayFieldValue("Repeated1/stTeaserRate", tmpStr);

        if (getIsDisplayPrimeIndexSection(srk) == true) {
          getCurrNDPage().setDisplayFieldValue("Repeated1/stBaseIndex",
                                               new String(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(), "PRIMEINDEXRATEPROFILE",
                                                           new Integer(Array.getInt(iBaseIndexId,
                                                          absNdx)).intValue(),
                                                          theSessionState.getLanguageId())));
        }

logger.debug("---RateAdminHandler---> BaseRate[" + absNdx + "]  == " + floatFmt.format(Array.getDouble(dbBaseRate, absNdx)));
        if( Array.getDouble(dbBaseRate, absNdx) != floatZero){
          tmpStr = floatFmt.format(Array.getDouble(dbBaseRate, absNdx));
        } else {
          tmpStr = "";
        }

        if (getIsDisplayPrimeIndexSection(srk) == true) {
          getCurrNDPage().setDisplayFieldValue("Repeated1/stBaseRate", tmpStr);
        }

logger.debug("---RateAdminHandler---> BaseAdjustment[" + absNdx + "]  == " + floatFmt.format(Array.getDouble(dbBaseAdjustment, absNdx)));
        if( Array.getDouble(dbBaseAdjustment, absNdx) != floatZero)  {
          tmpStr = floatFmt.format(Array.getDouble(dbBaseAdjustment, absNdx));
        } else {
          tmpStr = "";
        }

        if (getIsDisplayPrimeIndexSection(srk) == true) {
          getCurrNDPage().setDisplayFieldValue("Repeated1/stBaseAdjustment",
                                               tmpStr);
        }

logger.debug("---RateAdminHandler---> TeaserDiscount[" + absNdx + "]  == " + floatFmt.format(Array.getDouble(dbTeaserDiscount, absNdx)));
        if( Array.getDouble(dbTeaserDiscount, absNdx) != floatZero)  {
          tmpStr = floatFmt.format(Array.getDouble(dbTeaserDiscount, absNdx));
        }  else {
          tmpStr = "";
        }

        getCurrNDPage().setDisplayFieldValue("Repeated1/stTeaserDiscount", tmpStr);

logger.debug("---RateAdminHandler---> TeaserTerm[" + absNdx + "]  == " + floatFmt.format(Array.getDouble(iTeaserTerms, absNdx)));
        if( Array.getInt(iTeaserTerms, absNdx) != 0) {
          tmpStr = new Integer(Array.getInt(iTeaserTerms, absNdx)).toString();
        } else {
          tmpStr = "";
        }

        getCurrNDPage().setDisplayFieldValue("Repeated1/stTeaserTerms", tmpStr);

        // it is needed to distinguish MtgProd pending and PrimeRate pending
        if( new String(dateFormat.format((Date) Array.get(dtBaseRateEffDate, absNdx))) != null &&
            !(new String(dateFormat.format((Date) Array.get(dtBaseRateEffDate, absNdx))).equals("")) &&
            ((Date) Array.get(dtBaseRateEffDate, absNdx)).after(new Date())) {

          tmpStr =  new String(dateFormat.format((Date) Array.get(dtBaseRateEffDate, absNdx)));

logger.debug("---RateAdminHandler---> PrimeRateEffDate[" + absNdx + "]  == " + new String(dateFormat.format((Date) Array.get(dtBaseRateEffDate, absNdx))));
        } else {
          tmpStr = "";
        }

        getCurrNDPage().setDisplayFieldValue("Repeated1/stPrimeRateEffDate", tmpStr);

        if( new String(dateFormat.format((Date) Array.get(dtBaseRateEffDate, absNdx))) != null &&
            !(new String(dateFormat.format((Date) Array.get(dtBaseRateEffDate, absNdx))).equals("")) &&
            ((Date) Array.get(dtBaseRateEffDate, absNdx)).after(new Date())) {

          tmpStr = BXResources.getGenericMsg("PENDING_LABEL_PRIME_RATE", theSessionState.getLanguageId());

logger.debug("---RateAdminHandler---> PrimeRateStatus[" + absNdx + "]  == " + new String(dateFormat.format((Date) Array.get(dtBaseRateEffDate, absNdx))));
        } else {
          tmpStr = "";
        }

        getCurrNDPage().setDisplayFieldValue("Repeated1/stPrimeRateStatus", tmpStr);
        //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

				return true;
			} // end of RateAdmin option.
			if (isRateHistory == true)
			{
				logger.debug("--- RateAdminHandler ---> populateRepeatedFields for RateHistory");
				doInterestRateHistoryModelImpl tab =(doInterestRateHistoryModelImpl)
          (RequestManager.getRequestContext().getModelManager().getModel(doInterestRateHistoryModel.class));

				if (tab != null)
				{
					java.util.Date effectiveDate =(java.util.Date)(tab.getValue(tab.FIELD_DFEFFECTIVEDATE));
					getCurrNDPage().setDisplayFieldValue("Repeated1/stPending", new String(effectiveDate.after(new Date()) == true ? BXResources.getGenericMsg("YES_LABEL", theSessionState.getLanguageId())
            : "-"));
					Object postedrate = tab.getValue(tab.FIELD_STPOSTEDRATE);
					Object betrate = tab.getValue(tab.FIELD_DFBESTRATE);
					Object maxdiscountrate = tab.getValue(tab.FIELD_DFMAXDISCOUNTRATE);
					if (postedrate != null || postedrate.toString().length() != 0)
            getCurrNDPage().setDisplayFieldValue("Repeated1/stPostedRate", new String("" +(floatFmt.format(com.iplanet.jato.util.TypeConverter.asFloat(postedrate)))));
					if (betrate != null || betrate.toString().length() != 0)
            getCurrNDPage().setDisplayFieldValue("Repeated1/stBestRate", new String("" +(floatFmt.format(com.iplanet.jato.util.TypeConverter.asFloat(betrate)))));
					if (maxdiscountrate != null || maxdiscountrate.toString().length() != 0)
            getCurrNDPage().setDisplayFieldValue("Repeated1/stMaxDiscountRate", new String("" +(floatFmt.format(com.iplanet.jato.util.TypeConverter.asFloat(maxdiscountrate)))));

          //--Ticket#1551--20Jul2005--start--//
          Object baseRate = tab.getValue(tab.FIELD_DFPRIMEINDEXRATE);
          String br = ZERO_WITH_RATE_PRECISION;
          if (baseRate != null)
             br = new String("" +(floatFmt.format(com.iplanet.jato.util.TypeConverter.asFloat(baseRate))));

          getCurrNDPage().setDisplayFieldValue("Repeated1/stBaseRate", br);

          Object baseAdj = tab.getValue(tab.FIELD_DFPRIMEBASEADJ);
          String ba = ZERO_WITH_RATE_PRECISION;
          if (baseAdj != null)
            ba = new String("" +(floatFmt.format(com.iplanet.jato.util.TypeConverter.asFloat(baseAdj))));

          getCurrNDPage().setDisplayFieldValue("Repeated1/stBaseAdjustment", ba);

          Object teaserDisc = tab.getValue(tab.FIELD_DFTEASERDISCOUNT);
          String tdisc = ZERO_WITH_RATE_PRECISION;
          if (teaserDisc != null)
             tdisc = new String("" +(floatFmt.format(com.iplanet.jato.util.TypeConverter.asFloat(teaserDisc))));

          getCurrNDPage().setDisplayFieldValue("Repeated1/stTeaserDiscount", tdisc);

          String trate = ZERO_WITH_RATE_PRECISION;
          if (postedrate != null && teaserDisc != null)
          {
            trate = new String( (floatFmt.format(com.iplanet.jato.util.TypeConverter.asFloat(postedrate) -
                                             com.iplanet.jato.util.TypeConverter.asFloat(teaserDisc))));
          }

          getCurrNDPage().setDisplayFieldValue("Repeated1/stTeaserRate", trate);

          //--Ticket#1880,1885--09Aug2005--start--//
          Object teaserTerm = tab.getValue(tab.FIELD_DFTEASERTERM);
          String tterm = "0";

          if (teaserTerm != null)
            tterm = new String("" + com.iplanet.jato.util.TypeConverter.asInt(teaserTerm));

          getCurrNDPage().setDisplayFieldValue("Repeated1/stTeaserTerms", tterm);
          //--Ticket#1880,1885--09Aug2005--end--//
          //--Ticket#1551--20Jul2005--end--//
					return true;
				}
				else
				{
					getCurrNDPage().setDisplayFieldValue("Repeated1/stPending", new String("DB_ERROR"));
					return false;
				}
			}
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Problem encountered populating Repeatable of Pricing Rates");
			logger.error(e);		//#DG594 improve loging
			return false;
		}
		return true;
	}

	///////////////////////////////////////////////////////////////////////////
	// Navigate to the History and Update subpages
	public void navigateToRateAdminSubpage(int pageRowNdx, String sourceName)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

		int pageId = 0;
		Page pgEntity = null;

		try
		{
			int trueSelectedRelRowNdx = tds.getRelSelectedRowNdx();
			tds.setRelSelectedRowNdx(pageRowNdx);
			int rowNdx = tds.getAbsSelectedRowNdx();
			tds.setRelSelectedRowNdx(trueSelectedRelRowNdx);
			String [ ] sRateCode =(String [ ]) pst.get("sRateCode");
			String [ ] sStatusDescription =(String [ ]) pst.get("sStatusDescription");
			int [ ] iStatusId =(int [ ]) pst.get("iStatusId");
			String [ ] sRateCodeDescription =(String [ ]) pst.get("sRateCodeDescription");
			Date [ ] dtEffectiveDate =(Date [ ]) pst.get("dtEffectiveDate");
			double [ ] dbPostedRate =(double [ ]) pst.get("dbPostedRate");
			double [ ] dbMaxDiscountRate =(double [ ]) pst.get("dbMaxDiscountRate");
			Date [ ] dtExpiryDate =(Date [ ]) pst.get("dtExpiryDate");
			double [ ] dbInventoryId =(double [ ]) pst.get("dbInventoryId");
			double [ ] dbProfileId =(double [ ]) pst.get("dbProfileId");
			Date [ ] dtDisabledDate =(Date [ ]) pst.get("dtDisabledDate");

      //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
      double[] dbTeaserRate = (double[]) pst.get("dbTeaserRate");
      int[] iBaseIndexId = (int[]) pst.get("iBaseIndexId");
      double[] dbBaseRate = (double[]) pst.get("dbBaseRate");
      double[] dbBaseAdjustment = (double[]) pst.get("dbBaseAdjustment");
      double[] dbTeaserDiscount = (double[]) pst.get("dbTeaserDiscount");
      int[] iTeaserTerms = (int[]) pst.get("iTeaserTerms");
      Date [ ] dtBaseRateEffDate =(Date [ ]) pst.get("dtBaseRateEffDate");
     //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

			PDC pdc = PDC.getInstance();

			logger.debug(" -- Toni Nehme -- 000000000000000000000000000000000");
			if (sourceName.equals("btHistory"))
			{
				//logger.debug(" -- Toni Nehme -- 11111111111111111111111111111");
				pgEntity = pdc.getPage(Sc.INTEREST_RATES_HISTORY);
			}
			else
			{
				if (sourceName.equals("btUpdate"))
				{
					pgEntity = pdc.getPage(Sc.INTEREST_RATES_UPDATE);
				}
				else throw new Exception("Unknown Button was pressed!!!");

				//logger.debug(" -- Toni Nehme -- 22222222222222222222222222222");
			}
			PageEntry pgEntry = new PageEntry();
			pgEntry.setPageMosUserId(theSessionState.getCurrentPage().getPageMosUserId());
			pgEntry.setPageMosUserTypeId(theSessionState.getCurrentPage().getPageMosUserTypeId());
			pgEntry.setPageId(pgEntity.getPageId());
			pgEntry.setPageName(pgEntity.getPageName());
			pgEntry.setPageLabel(pgEntity.getPageLabel());
			pgEntry.setSubPage(true);
			Hashtable pst1 = pgEntry.getPageStateTable();

			if (pst1 == null)
			{
				pst1 = new Hashtable();
				pgEntry.setPageStateTable(pst1);
			}
			pgEntry.setDealInstitutionId(pg.getDealInstitutionId()); //since 3.3
			pst1.put("chosenRateCode", sRateCode [ rowNdx ]);
      //--Release2.1--//
      //-->Pass IDs instead,in order to populate Descriptions by ResourceBundle
      //--> By Billy28Nov2002
			//pst1.put("chosenRateDescription", sRateCodeDescription [ rowNdx ]);
      //--> For RateDesc.
      pst1.put("chosenProfileId", new Double(dbProfileId [ rowNdx ]));
      //--> For Status Desc.
      pst1.put("chosenStatusId", new Integer(iStatusId [ rowNdx ]));
      //===================================================================
			if (sourceName.equals("btHistory"))
			{
				if (iStatusId [ rowNdx ] == Mc.PRICING_STATUS_DISABLED)
				{
					pst1.put("chosenDisabledDate", dtDisabledDate [ rowNdx ]);
				}
				else
				{
					pst1.put("chosenDisabledDate", new String("-"));
				}
			}
			else
			{
				pgEntry.setPageId(Sc.INTEREST_RATES_UPDATE);
				// original value!
				pst1.put("deltaStatusId", new Integer(iStatusId [ rowNdx ]));

				//  break out effective date into part - for individual edit fields
				GregorianCalendar cal = new GregorianCalendar();
				Date effDt = cal.getTime();
				pst1.put("chosenEffectiveDate", effDt);

				// original value
				int month = effDt.getMonth() + 1;
				pst1.put("deltaMonth", new String("" + month));
				pst1.put("deltaDay", new String("" + effDt.getDate()));
				pst1.put("deltaYear", new String("" +(effDt.getYear() + 1900)));

				int hours = effDt.getHours();
				pst1.put("deltaHour", new String("" + hours));
				pst1.put("deltaMinute", new String("" + effDt.getMinutes()));

				pst1.put("chosenPostedRate", new Double(dbPostedRate [ rowNdx ]));

				//original value
				pst1.put("deltaPostedRate", new Double(dbPostedRate [ rowNdx ]));
				pst1.put("chosenMaxDiscountRate", new Double(dbMaxDiscountRate [ rowNdx ]));

				//original value
				pst1.put("deltaMaxDiscountRate", new Double(dbMaxDiscountRate [ rowNdx ]));
				pst1.put("chosenExpiryDate", dtExpiryDate [ rowNdx ]);
				pst1.put("chosenInventoryId", new Double(dbInventoryId [ rowNdx ]));

        //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
        pst1.put("deltaTeaserRate", new Double(dbTeaserRate[ rowNdx ]));
        pst1.put("chosenTeaserRate", new Double(dbTeaserRate [ rowNdx ]));

        pst1.put("deltaBaseIndexId", new Integer(iBaseIndexId [ rowNdx ]));
        pst1.put("chosenBaseIndexId", new Integer(iBaseIndexId [ rowNdx ]));

        pst1.put("deltaBaseRate", new Double(dbBaseRate[ rowNdx ]));
        pst1.put("chosenBaseRate", new Double(dbBaseRate [ rowNdx ]));

        pst1.put("deltaBaseAdjustment", new Double(dbBaseAdjustment [ rowNdx ]));
        pst1.put("chosenBaseAdjustment", new Double(dbBaseAdjustment [ rowNdx ]));

        pst1.put("deltaTeaserDiscount", new Double(dbTeaserDiscount [ rowNdx ]));
        pst1.put("chosenTeaserDiscount", new Double(dbTeaserDiscount [ rowNdx ]));

        pst1.put("deltaTeaserTerms", new Integer(iTeaserTerms [ rowNdx ]));
        pst1.put("chosenTeaserTerms", new Integer(iTeaserTerms [ rowNdx ]));

        //-- Ticket #3533 FXP3.1 --start ; use value as Date( not as a string)
        //Locale theLocale = new Locale(BXResources.getLocaleCode(theSessionState.getLanguageId()), "");
        //SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy HH:mm", theLocale);
        //pst1.put("deltaBaseRateEffDate", new String(dateFormat.format((Date)Array.get(dtBaseRateEffDate, rowNdx))));
        pst1.put("deltaBaseRateEffDate", (Date)Array.get(dtBaseRateEffDate, rowNdx));
        //-- Ticket #3533 FXP3.1 --end
        
        pst1.put("origBaseRateEffDate", (Date) dtBaseRateEffDate [ rowNdx ]);
       //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//
			}

			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage();
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Pricing Rates:Problem encountered while navigating to the RateAdmin. subpage");
			logger.error(e);		//#DG594 improve loging
		}

	}

	public void navigateBackFromHistoryPage()
	{
		logger.debug("--- RateAdminHandler ---> navigateBackFromHistoryPage");

		try
		{
			navigateToNextPage();
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Pricing Rates:Problem encountered while navigating back from the Rate History page");
			logger.error(e);		//#DG594 improve loging
		}
	}

	public void populateHistoryHeader(Hashtable pst)
	{
		try
		{
    	logger.debug("--- RateAdminHandler ---> populateHistoryHeader");
      //--Release2.1--//
      //--> Convert Date format with current language
      //--> By Billy 27Nov2002
			SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy HH:mm",
          new Locale(BXResources.getLocaleCode(theSessionState.getLanguageId()), ""));
      //==============================================
			getCurrNDPage().setDisplayFieldValue("stCode", new String((String) pst.get("chosenRateCode")));
      //--Release2.1--//
      //--> Populate Descriptions from ResourceBundle
      //--> By Billy 28Nov2002
			//getCurrNDPage().setDisplayFieldValue("stDescription", new String((String) pst.get("chosenRateDescription")));
      getCurrNDPage().setDisplayFieldValue("stDescription",
          new String(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(), "PRICINGPROFILE",
            ((Double)pst.get("chosenProfileId")).intValue(),
            theSessionState.getLanguageId())));
			//getCurrNDPage().setDisplayFieldValue("stStatus", new String((String) pst.get("chosenStatusDescription")));
      getCurrNDPage().setDisplayFieldValue("stStatus",
          new String((BXResources.getPickListDescription(theSessionState.getDealInstitutionId(), "PRICINGSTATUS",
            ((Integer)pst.get("chosenStatusId")).intValue(),
            theSessionState.getLanguageId()))));
      //==================================================

			//logger.debug("--- Toni ---> 000000000000000000000000");
			Object obj = pst.get("chosenDisabledDate");
			if (obj instanceof String) getCurrNDPage().setDisplayFieldValue("stDisabledDate", new String("-"));
			else getCurrNDPage().setDisplayFieldValue("stDisabledDate", new String(dateFormat.format((Date) obj)));
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Problem encountered populating Header of the Pricing Rate History Screen");
			logger.error(e);		//#DG594 improve loging
		}
	}

	//////////////////////////////////////////////////////////////////////////////
	public void handleForwardButton()
	{
		logger.debug("--- RateAdminHandler ---> handleForwardButton");

		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");
		handleForwardBackwardCommon(pg, tds, - 1, true);

		return;
	}

	///////////////////////////////////////////////////////////////////////////
	public void handleBackwardButton()
	{
		logger.debug("--- RateAdminHandler ---> handleBackwardButton");

		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");
		handleForwardBackwardCommon(pg, tds, - 1, false);

		return;
	}

	//////////////////////////////////////////////////////////////////////////
	public void populateMonthsComboBox(PageEntry pg)
	{
		logger.debug("--- Toni ---> populateMonthsComboBox");
    fillupMonths("cbMonth");
	}


	// Override framework method to save current inputs on the Rate Update screen (only
	// screen in this set that is editable)
  //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
	public void saveData(PageEntry pg, boolean calledInTransaction)
	{
		logger.debug("--- RateAdminHandler ---> saveData");

		Hashtable pst = pg.getPageStateTable();

		try
		{
			saveDeltas(pst, pg, null);

			// saved to session objects (pst of page enry - no DB tx)
			///// ALERT
			////  if (srk.getModified())
			////		pg.setModified(true);
			pg.setModified(true);
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Problem encountered processing Submit Dialog Button on the Update Pricing Rate Screen");
			logger.error(e);		//#DG594 improve loging
		}
	}
  //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

	//////////////////////////////////////////////////////////////////////////
	// update rate only
	public void handleOnSubmit()
	{
		logger.debug("--- RateAdminHandler ---> handleOnSubmit");

		int rateStatusVal = 0;

		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		SessionResourceKit srk = getSessionResourceKit();

		pg.setPageCondition3(true);

		//signal for refreshing Update Page
		Object rateStatus = getCurrNDPage().getDisplayFieldValue("cbStatus");


		//Check Rate Status
		try
		{
			rateStatusVal = com.iplanet.jato.util.TypeConverter.asInt(rateStatus);
			if (rateStatusVal == Mc.PRICING_STATUS_DISABLED)
			{
				//Cond 4 == true  that asks user to confirm  rate disabling
				pg.setPageCondition4(true);

				// Raise Cond 3
				setActiveMessageToAlert(BXResources.getSysMsg("RATE_STATUS_DISABLED", theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMDIALOG);
				return;
			}
			else if(com.iplanet.jato.util.TypeConverter.asInt(pst.get("chosenStatusId")) == Mc.PRICING_STATUS_DISABLED)
			{
				enableRateUpdate();
				return;
			}
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg("RATE_STATUS_NOT_SELECTED", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error(e);		//#DG594 improve loging
			return;
		}


		//Check correct Effective Date
		GregorianCalendar calendar = null;

		try
		{
			Object month = getCurrNDPage().getDisplayFieldValue("cbMonth");
			Object day = getCurrNDPage().getDisplayFieldValue("tbDay");
			Object year = getCurrNDPage().getDisplayFieldValue("tbYear");
			Object hour = getCurrNDPage().getDisplayFieldValue("tbHour");
			Object minute = getCurrNDPage().getDisplayFieldValue("tbMinute");
			calendar = new GregorianCalendar(
        com.iplanet.jato.util.TypeConverter.asInt(year),
        com.iplanet.jato.util.TypeConverter.asInt(month) - 1,
        com.iplanet.jato.util.TypeConverter.asInt(day),
        com.iplanet.jato.util.TypeConverter.asInt(hour),
        com.iplanet.jato.util.TypeConverter.asInt(minute));
			if (calendar == null)
			{
				throw new Exception();
			}
			Date savedDate =(Date) pst.get("chosenEffectiveDate");
			// original value
			Date inputDate = calendar.getTime();
			logger.debug("--- RateAdminHandler ---> savedDateMtgProd == " + savedDate.toString());
			logger.debug("--- RateAdminHandler ---> inputDateMtgProd = " + calendar.getTime());
			savedDate.setSeconds(0);
			SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy HH:mm");
			String formatedInput = dateFormat.format(inputDate);
			String formatedSaved = dateFormat.format(savedDate);
			logger.debug("--- RateAdminHandler ---> savedDateMtgProd == " + savedDate.toString());
			logger.debug("--- RateAdminHandler ---> inputDateMtgProd = " + calendar.getTime());

			if (inputDate.before(savedDate) && ! formatedInput.equals(formatedSaved))
			{
				setActiveMessageToAlert(BXResources.getSysMsg("RATE_EFFECTIVE_DATE_ACTUAL", theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMCONFIRM);
				return;
			}

			// Save Effective Date to insert new rate
			pst.put("deltaRateEffectiveDate", calendar.getTime());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg("RATE_EFFECTIVE_DATE_INVALID", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			return;
		}


		try
		{
			Object postedRate = getCurrNDPage().getDisplayFieldValue("tbPostedRate");
			Object maxDiscountRate = getCurrNDPage().getDisplayFieldValue("tbMaxDiscountRate");
			Double lastPostedRate =(Double) pst.get("chosenPostedRate");

			if (lastPostedRate != null)
			{
				//Compare with latest rate
				if((com.iplanet.jato.util.TypeConverter.asDouble(postedRate) -
          com.iplanet.jato.util.TypeConverter.asDouble(lastPostedRate)) > 0.5)
				{
					//Raise Cond 4 == false that asks user to confirm that posted rate will be increased more than 0.5 percent
					pg.setPageCondition4(false);

					//Raise Condition4
					setActiveMessageToAlert(BXResources.getSysMsg("RATE_SHARP_RAISE", theSessionState.getLanguageId()),
              ActiveMsgFactory.ISCUSTOMDIALOG);
					return;
				}
			}
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg("RATE_POSTED_AND_DISCOUNT_INVALID", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			return;
		}

    //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    Object chosenBaseIndexId = (Object) pst.get("chosenBaseIndexId");
    int chosenBaseIndexIdVal = com.iplanet.jato.util.TypeConverter.asInt(chosenBaseIndexId);
    int baseIndexFromScreen = com.iplanet.jato.util.TypeConverter.asInt(getCurrNDPage().getDisplayFieldValue("cbBaseIndex"));

//temp
logger.debug("--- RAH@HandleOnSubmit::ChosenBaseIndexIdVal: " + chosenBaseIndexIdVal);
logger.debug("--- RAH@HandleOnSubmit::ChosenBaseIndexIdFromScreen: " + getCurrNDPage().getDisplayFieldValue("cbBaseIndex"));
logger.debug("--- RAH@HandleOnSubmit::PgCond6: " + pg.getPageCondition6());

    int pricingUpdateType = getMtgRatePrimeRateRelation(pg.getPageCondition6(),
                                                        chosenBaseIndexIdVal,
                                                        baseIndexFromScreen);
//temp
logger.debug("--- RAH@HandleOnSubmit::pricingUpdateType: " + pricingUpdateType);
logger.debug("--- RAH@HandleOnSubmit::ChosenEffectiveDate: " + (Date) pst.get("chosenEffectiveDate"));
logger.debug("--- RAH@HandleOnSubmit::DeltaRateEffectiveDate: " + (Date) pst.get("deltaRateEffectiveDate"));

    // Check Prime Rate mode: do not allow reassign of Prime Rate in 'Pending' mode!!
    if (pricingUpdateType == REASSIGN_PRIME_RATE &&
        (((Date) pst.get("chosenEffectiveDate")).after(new Date()) == true ||
         ((Date) pst.get("deltaRateEffectiveDate")).after(new Date()) == true))
    {
      setActiveMessageToAlert(BXResources.getSysMsg("PRIME_RATE_NOT_IN_REASSIGN_MODE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
      return;
    }

    //--Ticket#1693&1696--23Aug--start--//
    if (pricingUpdateType == UPDATE_PREVIOUSLY_ASSIGNED_PRIME_RATE ||
         pricingUpdateType == ASSIGN_NEW_PRIME_RATE ||
         pricingUpdateType == REASSIGN_PRIME_RATE ||
         pricingUpdateType == UNASSIGN_PRIME_RATE)
    {
      try
      {
        PrimeIndexRateProfilePK piripk = new PrimeIndexRateProfilePK(chosenBaseIndexIdVal);
        PrimeIndexRateProfile primeIndexRateProfile = new PrimeIndexRateProfile(srk,
                                                                               chosenBaseIndexIdVal);
        PrimeIndexRateInventory primeInventoryLatestRate = getLatestPrimeIndexRateInventory(primeIndexRateProfile);

logger.debug("RAH@HandleOnSubmit::primeInventoryLatestRateId: " + primeInventoryLatestRate.getPrimeIndexRateInventoryId());
logger.debug("RAH@HandleOnSubmit::primeInventoryLatestRateId: " + primeInventoryLatestRate.getPrimeIndexRate());

        saveDeltas(pst, pg, primeInventoryLatestRate);

        double deltaTeaserRate = ( (Double) pst.get("deltaTeaserRate")).doubleValue();

        if (deltaTeaserRate < ZERO_WITH_RATE_PRECISION_DOUBLE){
          setActiveMessageToAlert(BXResources.getSysMsg("TEASER_RATE_MUST_BE_POSITIVE",
                                                        theSessionState.getLanguageId()),
                                                        ActiveMsgFactory.ISCUSTOMCONFIRM);
          return;
        }

        double deltaPostedRate = ((Double)pst.get("deltaPostedRate")).doubleValue();
        if (deltaPostedRate < ZERO_WITH_RATE_PRECISION_DOUBLE){
            setActiveMessageToAlert(BXResources.getSysMsg("POSTED_RATE_MUST_BE_POSITIVE", theSessionState.getLanguageId()),
                              ActiveMsgFactory.ISCUSTOMCONFIRM);
            return;
        }

        getSavedPages().setNextPage(pg);
        navigateToNextPage();
      }
      catch (Exception ex)
      {
       logger.error("RAH@HandleRedisplayPrimeRate: Exception redisplaying the Rate: " + ex);
      }
    }
    //--Ticket#1693&1696--23Aug--end--//

    int pricingProfileId = ((Double)(pst.get("chosenProfileId"))).intValue();
logger.debug("RAH@HandleOnSubmit::ProfileId: " + pricingProfileId);
    //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

		try
		{
			PassiveMessage pm = runAllIMBusinessRules(pg, srk);
			if (pm != null)
			{
				pm.setGenerate(true);
				getTheSessionState().setPasMessage(pm);

				return;
			}

			srk.beginTransaction();

      //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
			insertRateRecord(pg,
                       pst,
                       srk,
                       calendar,
                       pricingUpdateType,
                       pricingProfileId,
                       0.00, //TEMP
                       false);
      //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

			//For main Rate Administration page: refresh data because of updating rate status
			getPageCursorInfo(pg.getParentPage(), "DEFAULT").setRefresh(true);
			resetAllConditions(pg);
			navigateToNextPage();
			srk.commitTransaction();
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("the Update Pricing Rate Screen: Problem encountered during Submit event processing");
			logger.error(e.toString());
		}

	}

	//////////////////////////////////////////////////////////////////////////
	private void saveDeltas(Hashtable pst, PageEntry pg, PrimeIndexRateInventory lastRecord)
	{
		logger.debug("--- RateAdminHandler ---> saveDeltas");

		pst.put("deltaStatusId", getCurrNDPage().getDisplayFieldValue("cbStatus"));
		pst.put("deltaMonth", getCurrNDPage().getDisplayFieldValue("cbMonth"));
		pst.put("deltaDay", getCurrNDPage().getDisplayFieldValue("tbDay"));
		pst.put("deltaYear", getCurrNDPage().getDisplayFieldValue("tbYear"));
		pst.put("deltaHour", getCurrNDPage().getDisplayFieldValue("tbHour"));
		pst.put("deltaMinute", getCurrNDPage().getDisplayFieldValue("tbMinute"));

    //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    if (pg.getPageCondition6() == false) {
      pst.put("deltaPostedRate", getCurrNDPage().getDisplayFieldValue("tbPostedRate"));
    }
		pst.put("deltaMaxDiscountRate", getCurrNDPage().getDisplayFieldValue("tbMaxDiscountRate"));

    pst.put("deltaBaseIndexId", getCurrNDPage().getDisplayFieldValue("cbBaseIndex"));

    if (pg.getPageCondition6() == false) {
      pst.put("deltaTeaserRate", getCurrNDPage().getDisplayFieldValue("txTeaserRate"));
      pst.put("deltaBaseRate", getCurrNDPage().getDisplayFieldValue("txBaseRate"));
    }
    pst.put("deltaBaseAdjustment", getCurrNDPage().getDisplayFieldValue("txBaseAdjustment"));
    pst.put("deltaTeaserDiscount", getCurrNDPage().getDisplayFieldValue("txTeaserDiscount"));
    pst.put("deltaTeaserTerms", getCurrNDPage().getDisplayFieldValue("txTeaserTerms"));


//temp
logger.debug("RAH@saveDeltas::BaseRate: " + (Object)pst.get("deltaBaseRate"));
logger.debug("RAH@saveDeltas::Posted Rate Group");
logger.debug("RAH@saveDeltas::PostedRate: " + (Object)pst.get("deltaPostedRate"));
logger.debug("RAH@saveDeltas::BaseAdjustemnt: " + getCurrNDPage().getDisplayFieldValue("txBaseAdjustment"));

logger.debug("RAH@saveDeltas::Teaser Rate Group");
logger.debug("RAH@saveDeltas::DeltaTeaserTerms: " + getCurrNDPage().getDisplayFieldValue("txTeaserTerms"));
logger.debug("RAH@saveDeltas::DeltaTeaserDiscount: " + getCurrNDPage().getDisplayFieldValue("txTeaserDiscount"));
logger.debug("RAH@saveDeltas::TeaserRate: " + (Object)pst.get("deltaTeaserRate"));

    if(lastRecord != null)
    {
      //--Ticket#1693&1696--23Aug--start--//
        double deltaTeaserRate = (new Double(lastRecord.getPrimeIndexRate() +
                                  com.iplanet.jato.util.TypeConverter.asDouble(getCurrNDPage().getDisplayFieldValue("txBaseAdjustment")) -
                                  com.iplanet.jato.util.TypeConverter.asDouble(getCurrNDPage().getDisplayFieldValue("txTeaserDiscount")))).doubleValue();

        pst.put("deltaTeaserRate", new Double(lastRecord.getPrimeIndexRate() +
                                 com.iplanet.jato.util.TypeConverter.asDouble(getCurrNDPage().getDisplayFieldValue("txBaseAdjustment")) -
                                 com.iplanet.jato.util.TypeConverter.asDouble(getCurrNDPage().getDisplayFieldValue("txTeaserDiscount"))));

        double deltaPostedRate = (new Double(lastRecord.getPrimeIndexRate() +
                                   com.iplanet.jato.util.TypeConverter.asDouble(getCurrNDPage().getDisplayFieldValue("txBaseAdjustment")))).doubleValue();

        pst.put("deltaPostedRate", new Double(lastRecord.getPrimeIndexRate() +
                                        com.iplanet.jato.util.TypeConverter.asDouble(getCurrNDPage().getDisplayFieldValue("txBaseAdjustment"))));

//temp
logger.debug("RAH@saveDeltas::lastRecord: " + lastRecord.getPrimeIndexRate());
logger.debug("RAH@saveDeltas::BaseAdjustemnt: " + getCurrNDPage().getDisplayFieldValue("txBaseAdjustment"));
logger.debug("RAH@saveDeltas::DeltaTeaserDiscount: " + getCurrNDPage().getDisplayFieldValue("txTeaserDiscount"));
logger.debug("RAH@saveDeltas::PostedRate: " + getCurrNDPage().getDisplayFieldValue("tbPostedRate"));
logger.debug("RAH@saveDeltas::DeltaPostedRateFinal: " + deltaPostedRate);
logger.debug("RAH@saveDeltas::DeltaTeaserRateFinal: " + deltaTeaserRate);

      pst.put("deltaBaseRate", new Double(lastRecord.getPrimeIndexRate()));
    }
    //--Ticket#1693&1696--23Aug--end--//
   //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//
	}


	private void restoreDeltas(Hashtable pst)
	{
		logger.debug("--- RateAdminHandler ---> restoreDeltas");

		try
		{
			String minutes = "";
			Object val =(Object) pst.get("deltaMinute");

			if (val != null && val.toString().trim().length() != 0)
			{
				minutes = val.toString();
				if (minutes.trim().length() == 1) minutes = "0" + minutes;
			}
			getCurrNDPage().setDisplayFieldValue("stCode", new String((String) pst.get("chosenRateCode")));
      //--Release2.1--//
      //--> Populate RateDesc by ResourceBundle
      //--> By Billy 28Nov2002
			//getCurrNDPage().setDisplayFieldValue("stDescription", new String((String) pst.get("chosenRateDescription")));
			getCurrNDPage().setDisplayFieldValue("stDescription",
          new String(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(), "PRICINGPROFILE",
            ((Double)pst.get("chosenProfileId")).intValue(),
            theSessionState.getLanguageId())));
			//==============================================
      getCurrNDPage().setDisplayFieldValue("cbStatus",(Object) pst.get("deltaStatusId"));
			getCurrNDPage().setDisplayFieldValue("cbMonth",(Object) pst.get("deltaMonth"));
			getCurrNDPage().setDisplayFieldValue("tbDay",(Object) pst.get("deltaDay"));
			getCurrNDPage().setDisplayFieldValue("tbYear",(Object) pst.get("deltaYear"));
			getCurrNDPage().setDisplayFieldValue("tbHour",(Object) pst.get("deltaHour"));
			getCurrNDPage().setDisplayFieldValue("tbMinute", new String(minutes));

			Object postedRate =(Object) pst.get("deltaPostedRate");

			Object maxDiscountRate =(Object) pst.get("deltaMaxDiscountRate");
			getCurrNDPage().setDisplayFieldValue("tbPostedRate", postedRate);
			getCurrNDPage().setDisplayFieldValue("tbMaxDiscountRate", maxDiscountRate);

			if (com.iplanet.jato.util.TypeConverter.asDouble(postedRate, -1) == -1)
			{
				getCurrNDPage().setDisplayFieldValue("tbBestRate", new String("0.00"));
				return;
			}
			if (com.iplanet.jato.util.TypeConverter.asDouble(maxDiscountRate, -1) == -1)
			{
				getCurrNDPage().setDisplayFieldValue("tbBestRate", maxDiscountRate);
				return;
			}
			getCurrNDPage().setDisplayFieldValue("tbBestRate",
        new Double(com.iplanet.jato.util.TypeConverter.asDouble(postedRate) -
          com.iplanet.jato.util.TypeConverter.asDouble(maxDiscountRate)));

      //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
      getCurrNDPage().setDisplayFieldValue("cbBaseIndex",(Object) pst.get("deltaBaseIndexId"));
      getCurrNDPage().setDisplayFieldValue("txTeaserRate",(Object) pst.get("deltaTeaserRate"));
      getCurrNDPage().setDisplayFieldValue("txBaseRate",(Object) pst.get("deltaBaseRate"));
      getCurrNDPage().setDisplayFieldValue("txBaseAdjustment", (Object) pst.get("deltaBaseAdjustment"));
      getCurrNDPage().setDisplayFieldValue("txTeaserDiscount", (Object) pst.get("deltaTeaserDiscount"));
      getCurrNDPage().setDisplayFieldValue("txTeaserTerms", (Object) pst.get("deltaTeaserTerms"));
      
      //-- Ticket #3533 FXP3.1 --start ; use value as Date( not as a string)
      //getCurrNDPage().setDisplayFieldValue("stBaseIndxLastUpdateTime", ((Object) pst.get("deltaBaseRateEffDate")).toString());
      getCurrNDPage().setDisplayFieldValue("stBaseIndxLastUpdateTime", ((Object) pst.get("deltaBaseRateEffDate")));
      //-- Ticket #3533 FXP3.1 --end 
      
      //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Display stored Deltas of Pricing Rate Screen: Problem encountered");
			logger.error(e);		//#DG594 improve loging
		}
	}

	/////////////////////////////////////////////////////////////////////////
  //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
  //--Ticket#1736--18July2005--start--//
  // No need for this method anymore since the Teaser related fields moved to the
  // PricingRateInventory table
  //--Ticket#1736--18July2005--end--//
  //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

  public void handleOnPrimeIndexRateUpdate(int rowIndx)
  {
    logger.debug("--- RateAdminHandler ---> handleOnPrimeIndexRateUpdate:RowIndx: " + rowIndx);
    PageEntry pg = theSessionState.getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    SessionResourceKit srk = getSessionResourceKit();
    PageCursorInfo tds2 = (PageCursorInfo)pst.get("RepeatedPrimeIndex");
    tds2.setRelSelectedRowNdx(rowIndx);


    // 1. Get the PrimeIndexRateId to update here (from new model).
    //Check correct Effective Date
    int primeIndexRateProfileId = 0; // get it from new function.
    double newIndexRateVal = 0.0; // get it from the screen.

logger.debug("RAH@handleOnPrimeIndexRateUpdate::NewIndexRateVal[ " + rowIndx +
                     "]: " + getRepeatedField("RepeatedPrimeIndex/txPrimeIndexEffRate", rowIndx));

    Object indexRate = (Object) getCurrNDPage().getDisplayFieldValue("RepeatedPrimeIndex/txPrimeIndexEffRate");
    newIndexRateVal = com.iplanet.jato.util.TypeConverter.asDouble(indexRate);
logger.debug("RAH@handleOnPrimeIndexRateUpdate::NewIndexRateVal: " + newIndexRateVal);

    // This Id works
    Object primeProfileId = (Object) getCurrNDPage().getDisplayFieldValue("RepeatedPrimeIndex/hdPrimeIndexProfileId");
    primeIndexRateProfileId = com.iplanet.jato.util.TypeConverter.asInt(primeProfileId);
logger.debug("RAH@handleOnPrimeIndexRateUpdate::PrimeIndexRateProfileId: " + primeIndexRateProfileId);

    // 2. Get the current date from the screen.
    // TODO: Separate this date massage into the sub-routine (it is called twice).
    GregorianCalendar calendar = null;

    try
    {
      Object month = getCurrNDPage().getDisplayFieldValue("RepeatedPrimeIndex/cbIndexRateEffMonth");
      Object day = getCurrNDPage().getDisplayFieldValue("RepeatedPrimeIndex/txIndexRateEffDay");
      Object year = getCurrNDPage().getDisplayFieldValue("RepeatedPrimeIndex/txIndexRateEffYear");
      Object hour = getCurrNDPage().getDisplayFieldValue("RepeatedPrimeIndex/txIndexRateEffHour");
      Object minute = getCurrNDPage().getDisplayFieldValue("RepeatedPrimeIndex/txIndexRateEffMinute");

//temp
logger.debug("RAH@handleOnPrimeIndexRateUpdate::Year: " + com.iplanet.jato.util.TypeConverter.asInt(year));
logger.debug("RAH@handleOnPrimeIndexRateUpdate::Month: " + com.iplanet.jato.util.TypeConverter.asInt(month));
logger.debug("RAH@handleOnPrimeIndexRateUpdate::Day: " + com.iplanet.jato.util.TypeConverter.asInt(day));
logger.debug("RAH@handleOnPrimeIndexRateUpdate::Hour: " + com.iplanet.jato.util.TypeConverter.asInt(hour));
logger.debug("RAH@handleOnPrimeIndexRateUpdate::Minute: " + com.iplanet.jato.util.TypeConverter.asInt(minute));

      calendar = new GregorianCalendar(
        com.iplanet.jato.util.TypeConverter.asInt(year),
        com.iplanet.jato.util.TypeConverter.asInt(month) - 1,
        com.iplanet.jato.util.TypeConverter.asInt(day),
        com.iplanet.jato.util.TypeConverter.asInt(hour),
        com.iplanet.jato.util.TypeConverter.asInt(minute));
      if (calendar == null)
      {
        throw new Exception();
      }

      //  3. Format/compare latest PrimeIndexRateEffDate (PrimeIndexRateExpDate
      //  is null) and current date from the screen.
      Date inputDate = calendar.getTime();
logger.debug("--- RateAdminHandler ---> inputDatePrimeRate = " + calendar.getTime());
      SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy HH:mm");

      String formatedInput = dateFormat.format(inputDate);
logger.debug("--- RateAdminHandler ---> inputDatePrimeRate = " + calendar.getTime());

      if(calendar.before(GregorianCalendar.getInstance()))
      {
        setActiveMessageToAlert(BXResources.getSysMsg("RATE_EFFECTIVE_DATE_ACTUAL", theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMCONFIRM);
        return;
      }

      // Save new Prime Rate Effective Date to insert new rate
      pst.put("newPrimeRateEffDate", calendar.getTime());
    }
    catch(Exception e)
    {
      setActiveMessageToAlert(BXResources.getSysMsg("RATE_EFFECTIVE_DATE_INVALID", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
      return;
    }

    // 4. Update ALL necessary PrimeIndexInventory records here.
    try
    {
      PassiveMessage pm = runAllIMBusinessRules(pg, srk);
      if (pm != null)
      {
        pm.setGenerate(true);
        getTheSessionState().setPasMessage(pm);

        return;
      }
    }
    catch(Exception e)
    {
      srk.cleanTransaction();
      setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
      logger.error("Pricing Rate Admin Screen: Problem encountered during Update Index Rate event processing");
      logger.error(e.toString());
    }

    try
    {
      boolean isPendingProducts = reportAnyPendingProducts(primeIndexRateProfileId);

      if (isPendingProducts == true)
        return;
logger.debug("Should not be here");

      srk.beginTransaction();

      updateRecordsWithPrimeIndexInventory(pg,
                                           pst,
                                           srk,
                                           calendar,
                                           newIndexRateVal,
                                           primeIndexRateProfileId,
                                           true);

      resetAllConditions(pg);
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      srk.cleanTransaction();
      setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
      logger.error("Pricing Rate Admin Screen: Problem encountered during Update Index Rate event processing");
      logger.error(e.toString());
    }
  }

  public boolean reportAnyPendingProducts(int primeIndexRateProfileId)
  {
    boolean ret = false;
    try
      {
        PricingProfile record = new PricingProfile(srk);
        List pricingRecords = record.findByPrimeIndexRateProfileId(primeIndexRateProfileId);

        Iterator it = pricingRecords.iterator();
        int pricingProfileId = 0;
        while (it.hasNext())
        {
          PricingProfile pricingRec = (PricingProfile) it.next();

          pricingProfileId = pricingRec.getPricingProfileId();

          int thePendingInventory = getPendingPriceInventoryId(srk, pricingProfileId);
          if (thePendingInventory != -1) {
          logger.trace("RAH@reportAnyPendingProducts::primeIndexRateProfileId: " + primeIndexRateProfileId +
              ", thePendingPricingProfileId: " + pricingProfileId);

            String pendingProductName = pricingRec.getRateCodeDescription();

            String tmpStr = new String(BXResources.getSysMsg("PRODUCT_NOT_IN_UPDATE_INDEX_MODE",
                                               theSessionState.getLanguageId()));

            try
            {
              tmpStr = com.basis100.deal.util.Format.printf(tmpStr,
                  new com.basis100.deal.util.Parameters(pendingProductName));
              ret = true;
            }
            catch (Exception ex)
            {
              logger.warning("@RAH@reportAnyPendingProducts::Exception when formating string:: "
                + ex);
            }

            setActiveMessageToAlert(tmpStr, ActiveMsgFactory.ISCUSTOMCONFIRM);
          }
        }
      }
      catch (Exception e)
      {
        setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMCONFIRM);
        logger.error("RAH@reportAnyPendingProducts: Problem encountered during Update Index Rate event processing");
        logger.error(e.toString());
      }
      return ret;
  }

  public void updateRecordsWithPrimeIndexInventory(PageEntry pg,
                                                   Hashtable pst,
                                                   SessionResourceKit srk,
                                                   GregorianCalendar inpDate,
                                                   double newPrimeRateVal,
                                                   int primeIndexRateProfileId,
                                                   boolean isAllPrimeRatesUpdate)
  {
    int pricingProfileId = - 1;

    try
    {
      JdbcExecutor jExec = srk.getJdbcExecutor();

      int key = jExec.execute("SELECT pi.PRICINGPROFILEID FROM primeIndexRateProfile prp, " +
                              "primeIndexRateInventory pri, PRICINGPROFILE pp, PRICINGRATEINVENTORY pi " +
                              "WHERE (prp.PRIMEINDEXRATEPROFILEID = pri.PRIMEINDEXRATEPROFILEID) " +
                              "AND (prp.PRIMEINDEXRATEPROFILEID = pp.PRIMEINDEXRATEPROFILEID) " +
                              "AND (pri.PRIMEINDEXRATEINVENTORYID  =  pi.PRIMEINDEXRATEINVENTORYID) " +
                              "AND pri.PRIMEINDEXRATEPROFILEID = " + primeIndexRateProfileId +
                              " AND pp.RATEDISABLEDDATE is null " +
                              "AND pi.INDEXEXPIRYDATE is null " +
                              "AND pi.INDEXEFFECTIVEDATE <= " + DBA.sqlStringFrom(inpDate.getTime()) +
                              " GROUP BY pi.PRICINGPROFILEID " +
                              " ORDER BY pi.PRICINGPROFILEID ASC");

      // IMPORTANT!! In should be just minutes.
      //inpDate.add(inpDate.MINUTE, - 1);
logger.trace("VLAD ==> updateRecordsWithPrimeIndexInventory::InputDate: " + DBA.sqlStringFrom(inpDate.getTime()));

      Date expiryDate = inpDate.getTime();


      while(jExec.next(key))
      {
        pricingProfileId = jExec.getInt(key, 1);
logger.trace("VLAD ==> updateRecordsWithPrimeIndexInventory:: pricingProfileId = " + pricingProfileId +
                     " NewPrimeIndexRate= " + newPrimeRateVal +  " ExpiryDate = " + expiryDate);

logger.trace("VLAD ==> updateRecordsWithPrimeIndexInventory:: InputDate = " + inpDate.getTime());

        // Update insertRateRecord through boolean is PrimeIndexRateUpdate, it includes:
        // 1. Original insertRateRecord part, but except reading from the screen
        // create entity bean instance with the values from the found one (analog
        // of insertOriginalRateRecordWithUpdates(..).
        // 2. New 'copy' create entity action is moved to the
        // retrievePricingInventory() method.
        // 3. After this point follow the logic calls in insertRateRecord()
        // enhanced with logic for PrimeIndexRate business
        // (UPDATE_PREVIOUSLY_ASSIGNED_PRIME_RATE) mode.

        insertRateRecord(pg,
                         pst,
                         srk,
                         inpDate,
                         UPDATE_PREVIOUSLY_ASSIGNED_PRIME_RATE,
                         pricingProfileId,
                         newPrimeRateVal,
                         true);

logger.trace("VLAD ==> UpdateRecordsWithPrimeIndexInventory:: Update for pricingProfileId = " + pricingProfileId + " is done.");

      }
      jExec.closeData(key);
    }
    catch(Exception e)
    {
      logger.error("RAH@UpdateRecordsWithPrimeIndexInventory Exception: " + e.toString());
    }
  }
  //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

  //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
	private void insertRateRecord(PageEntry pg, Hashtable pst, SessionResourceKit srk,
                                 GregorianCalendar inpDate, int pricingUpdateType,
                                 int pricingProfileId, double newPrimeRateVal,
                                 boolean isAllPrimeRatesUpdate)
		throws Exception
	{
		logger.debug("--- RateAdminHandler ---> insertRateRecord: It includes both PricingRateInventory AND " +
                 "PrimiIndexRateInventory records!!");

    // 1. First recognize the PricingRate profileId and accosiated with it entities.
		PricingProfilePK ppk = new PricingProfilePK(pricingProfileId);
		PricingProfile profile = new PricingProfile(srk, pricingProfileId);

		// ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
		//	Moved this function call outside method "getPendingPriceInventoryId"
		int pendingPricingRateInventoryID = getPendingPriceInventoryId(srk, pricingProfileId);

    PricingRateInventory inventory = retrievePricingRateInventory(srk,
                                                                  ppk,
                                                                  inpDate,
                                                                  pricingProfileId,
	                                                                  isAllPrimeRatesUpdate,
	                                                                  pendingPricingRateInventoryID);
		// ***** Change by NBC Impl. Team - Version 1.1 - End *****//	    

    // 2. Secondly, recognize the PrimeIndexRateId associated with the PricingRate on 'Update Rate' screen.
    //int baseIndexFromScreen = 0;

    int chosenBaseIndexIdVal = 0; //default
    int baseIndexFromScreen = 0; // default

    if (isAllPrimeRatesUpdate == false)
    {
      Object chosenBaseIndexId = (Object) pst.get("chosenBaseIndexId");
      chosenBaseIndexIdVal = com.iplanet.jato.util.TypeConverter.asInt(chosenBaseIndexId);
      baseIndexFromScreen = com.iplanet.jato.util.TypeConverter.asInt(getCurrNDPage().getDisplayFieldValue("cbBaseIndex"));
logger.debug("--- RAH@InsertRateRecord::ChosenBaseIndexIdVal: " + chosenBaseIndexIdVal);
    }
    else if(isAllPrimeRatesUpdate == true)
    {
      Object primeProfileId = (Object) getCurrNDPage().getDisplayFieldValue("RepeatedPrimeIndex/hdPrimeIndexProfileId");
      int primeIndexRateProfileId = com.iplanet.jato.util.TypeConverter.asInt(primeProfileId);
logger.debug("--- RAH@InsertRateRecord::PrimeIndexRateProfileId: " + primeIndexRateProfileId);

      chosenBaseIndexIdVal = primeIndexRateProfileId;
      baseIndexFromScreen = primeIndexRateProfileId;
    }

//temp
logger.debug("--- RAH@InsertRateRecord::PgCond6: " + pg.getPageCondition6());
logger.debug("--- RAH@InsertRateRecord::pricingUpdateType_fromFunction: " + pricingUpdateType);
logger.debug("--- RAH@InsertRateRecord::inventoryId: " + inventory.getPricingRateInventoryID());
logger.debug("--- RAH@InsertRateRecordCaseII::baseIndexFromScreen: " + baseIndexFromScreen);

    // 3. Provide original PricingRate update business with updates related
    // to new PrimeIndexRate additions for a single PricingRateRecord update.
    // For multiple PricingRate records update (isAllPrimeRatesUpdate = true mode)
    // it is already done through full entiry create method in
    // retrievePricingRateInventory(..) method.
    if (isAllPrimeRatesUpdate == false)
    {
      insertOriginalRateRecordWithUpdates(inventory,
                                          pst,
                                          chosenBaseIndexIdVal,
                                          baseIndexFromScreen,
                                          pricingUpdateType);
    }

    // Until that point it is a common business (with modifications related to
    // PrimeIndex business if necessary) to insert the new record (now PrimeRate).
    // The new logic is added form this point:
    // 1. If baseIndexId is the same as chosen just update the new PricingRecord
    // with PrimeIndexRateInventory related info.
    // 2. If baseIndexId is changed on the screen reaccociate the new PricingRecord
    // record with this new PrimeIndex from now to future (it should not affect
    // the history.
    // 3. If there was no PrimeRate associated with the MtgRate associate it the
    // on the Inventory level (no history updates).
    // 4. If baseIndexId is changed back to 0 (un-association of MtgRate and PrimeRate)
    // return common rate business (header up to this notes).
    // 5. IMPORTANT!! If PrimeIndexRateProfileId <> provide new logic to implement
    // internalRatePercentage value

    // On Submit create new PrimeIndexRateInventory record here!!
    // 4. Get PrimeIndexRateProfilePK based baseIndexFromScreen and entity.
    PrimeIndexRateProfilePK piripk = new PrimeIndexRateProfilePK(baseIndexFromScreen);
    PrimeIndexRateProfile primeIndexRateProfile = new PrimeIndexRateProfile(srk, baseIndexFromScreen);

    PrimeIndexRateInventory primeInventoryLatestRate = getLatestPrimeIndexRateInventory(primeIndexRateProfile);

//temp
logger.debug("--- RAH@primeInventoryLatestRate.primeIndexRateInventoryId: " + primeInventoryLatestRate.getPrimeIndexRateInventoryId());
logger.debug("--- RAH@primeInventoryLatestRategetPrimeIndexRate: " + primeInventoryLatestRate.getPrimeIndexRate());
logger.debug("--- RAH@primeInventoryLatestRateprimeInventory.getPrimeIndexEffDate: " + primeInventoryLatestRate.getPrimeIndexEffDate());
logger.debug("--- RAH@primeInventoryLatestRate.getPrimeIndexExpDate: " + primeInventoryLatestRate.getPrimeIndexExpDate());

    // 2. Create PrimeIndexRateInventory based on primeIndexRateProfilePK
    PrimeIndexRateInventory primeIndexRateInventory = new PrimeIndexRateInventory(srk);
    primeIndexRateInventory.create(primeIndexRateInventory.createPrimaryKey(), piripk);

logger.debug("--- RAH@InsertRateRecordCaseII::PrimeIndexRateInventoryId_newRecord: " + primeIndexRateInventory.getPrimeIndexRateInventoryId());

    // 3. Insert new PrimeIndexRateInventory record and update all related
    // values/records (PricingRate related).
    insertPrimeIndexInventoryRecord(profile,
                                    inventory,
                                    primeIndexRateProfile,
                                    primeInventoryLatestRate,
                                    primeIndexRateInventory,
                                    pst,
                                    newPrimeRateVal,
                                    isAllPrimeRatesUpdate
                                    );

    // 4. Finally, set all necessary expiry dates and
    // Create/update appropriate PrimeIndexInventory record here
    // (and update PricingInventory record with new PrimeIndex related info).
    int primeIndexRateInventoryId = getPrimeIndexInventoryId (pricingProfileId);
logger.debug("--- RAH@InsertRateRecord::PrimeIndexRateInventoryId: " + primeIndexRateInventoryId);

    PrimeIndexRateInventory primeIndexInventoryCurr = null;
    primeIndexInventoryCurr = new PrimeIndexRateInventory(srk, primeIndexRateInventoryId);

    // 5. Update the current PrimeIndexRateInventory record ==> set Dates (Effective and
    //  Expriry ones as usual in pricing business).
    if (chosenBaseIndexIdVal == baseIndexFromScreen &&
        pricingUpdateType != UPDATE_PREVIOUSLY_ASSIGNED_PRIME_RATE) {
logger.debug("--- RAH@InsertRateRecord::PricingUpdateType_I: " + pricingUpdateType);

      setExpriryDateForCurrentPrimeIndexRecord(srk,
                                               primeIndexInventoryCurr,
                                               chosenBaseIndexIdVal,
                                               baseIndexFromScreen);
    }
    else if ((chosenBaseIndexIdVal != baseIndexFromScreen) ||
             (pricingUpdateType == UPDATE_PREVIOUSLY_ASSIGNED_PRIME_RATE) &&
             (chosenBaseIndexIdVal == baseIndexFromScreen)) {

logger.debug("--- RAH@InsertRateRecord::PricingUpdateType_II: " + pricingUpdateType);

      setExpriryDateForCurrentPrimeIndexRecord(srk,
                                               primeInventoryLatestRate,
                                               chosenBaseIndexIdVal,
                                               baseIndexFromScreen);
    }
    //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

		//Close open record: put expiry date in it
		setExpiryDateForAllCurrentRates(srk, pricingProfileId, inpDate, inventory.getPricingRateInventoryID());

    //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    // Teaser related values updates if update an individual PrimeIndexInventory.
    //--Ticket#1736--18July2005--start--//
    // TeaserDiscount and TeaserTerm fields are moved to the PricingRateInventory table.
    // No need to update mtgProd anymore!!
    //--Ticket#1736--18July2005--start--//
    //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//
		
		// ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
		//#DG594 	rates are always to be synch'ed if supported
		//if (isRateSyncSupported() && isAllPrimeRatesUpdate == false){
		if (isRateSyncSupported() ){
			updateExpertRates(srk,pendingPricingRateInventoryID,inventory);
		}
		// ***** Change by NBC Impl. Team - Version 1.1 - End *****//
	}
	
	// ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
	/**
	 * isRateSyncSupported 
	 * Method used to verify if Rate Synchronization b/n Express
	 * and expert is enabled/disabled.
	 * 
	 * @return boolean : true if Rate synchronization is enabled else false <br>
	 */
	public boolean isRateSyncSupported() {
		boolean returnVal = true;

		try {
			if (PropertiesCache.getInstance().getProperty(
			        srk.getExpressState().getDealInstitutionId(),
					"mosapp.mossystem.ratesync.RateSyncEnabled", "N").equalsIgnoreCase(
					"N")) {
				returnVal = false;
			}
		} catch (Exception exc) {
			logger.debug("-- RAH@isRateSyncSupported cannot read mossys properties"
					+ exc);
		}

		return returnVal;
	}

	/**
	 * updateExpertRates 
	 * Method used to synchronize rate changes b/n
	 * Express and Expert
	 * 
	 * @param srk
	 *            Session Resource Kit used to obtain connections <br>
	 * @param pendingPricingRateInventoryID
	 *            Primary Key of a pending PricingRateinventory record<br>
	 *            Will have a value -1 if the record modified is not pending
	 * @param inventory
	 *            PricingRateInventory entity for which the values have changed<br>
	 * @return void
	 */
	public void updateExpertRates(SessionResourceKit srk,
			int pendingPricingRateInventoryID, PricingRateInventory inventory) {
		java.sql.CallableStatement callableStmt = null;
		JdbcExecutor jExec = srk.getJdbcExecutor();
		java.sql.Connection con = jExec.getCon();
		int insertFlag = 0;
		String indexEffectiveDate = null;
		int pricingProfileID = -1;
		double internalRatePercentage = 0;
		double maximumDiscountAllowed = 0;
		double bestRate = 0;
		// ***** Change by NBC Impl. Team - CR7 Fix - Start *****//
		double discountPercentage = 0;
		double baseRatePercentage = 0;
		int primeIndexRateInventoryID = 0;
		double primeBaseAdj = 0;
		double teaserDiscount = 0;
		int teaserTerm = 0;
		// ***** Change by NBC Impl. Team - CR7 Fix - End *****//
		try {
			// Fetch field values from the inventory object
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			pricingProfileID = inventory.getPricingProfileID();
			indexEffectiveDate = df.format(inventory.getIndexEffectiveDate());
			internalRatePercentage = inventory.getInternalRatePercentage();
			maximumDiscountAllowed = inventory.getMaximumDiscountAllowed();
			bestRate = inventory.getBestRate();
			// ***** Change by NBC Impl. Team - CR7 Fix - Start *****//
			discountPercentage = inventory.getDiscountPercentage();
			baseRatePercentage = inventory.getBaseRatePercentage();
			primeIndexRateInventoryID = inventory.getPrimeIndexRateInventoryId();
			primeBaseAdj = inventory.getPrimeBaseAdj();
			teaserDiscount = inventory.getTeaserDiscount();
			teaserTerm = inventory.getTeaserTerm();	
			// ***** Change by NBC Impl. Team - CR7 Fix - End *****//
			insertFlag = (pendingPricingRateInventoryID == -1) ? 1 : 0;
			
			// Prepare and Execute anonymous PL/SQL code to execute the Stored Procedure
			String sql = "begin modify_expert_rate_prc(" + pricingProfileID
					+ ",TO_DATE('" + indexEffectiveDate
					+ "','DD/MM/YYYY HH24:MI:SS')" + ","
					// ***** Change by NBC Impl. Team - CR7 Fix - Start *****//
					+ internalRatePercentage + ","
					+ ((discountPercentage==0)? "NULL" : "" + discountPercentage) + ","
					+ ((baseRatePercentage==0)? "NULL" : "" + baseRatePercentage) + ","
					+ bestRate + "," + maximumDiscountAllowed + ","
					+ primeIndexRateInventoryID + "," 
					+ ((primeBaseAdj==0)? "NULL" : "" + primeBaseAdj) + ","
					+ ((teaserDiscount==0)? "NULL" : "" + teaserDiscount) + "," 
					+ ((teaserTerm==0)? "NULL" : "" + teaserTerm) + ","
					// ***** Change by NBC Impl. Team - CR7 Fix - End *****//
					+ insertFlag + "); end;";

			callableStmt = (java.sql.CallableStatement) con.prepareCall(sql);
			callableStmt.execute();
			callableStmt.close();
		} catch (java.sql.SQLException e) {
			try {
				
				logger.error("@RAH.updateExpertRates::Expert rate sync failed\n" + e);		//#DG594 improve loging
				
				logger.debug("@RAH.updateExpertRates::Sending Failure mail to Administrator....");
				
				PropertiesCache propCache = PropertiesCache.getInstance();
				String mailText = null;
				String mailSubject = null;
				DocumentQueue queue = new DocumentQueue(srk);
				DocumentProfilePK dppk = new DocumentProfilePK(
						Dc.VERSION_1_GENX,
						Mc.LANGUAGE_PREFERENCE_ENGLISH,
						0, // Lender profile id
						Dc.DOCUMENT_GENERAL_TYPE_FAILURE_EMAIL,
						"rtf1"
				);
				
				// Read properties
                String mailTo = propCache.getProperty(
                        srk.getExpressState().getDealInstitutionId(),
                        "mosapp.mossystem.ratesync.FailureMailRecipient");
                String expressEnvName = propCache.getProperty(
                        srk.getExpressState().getDealInstitutionId(),
                        "mosapp.mossystem.ratesync.ExpressEnvironmentName",
                        "Express");
                String expertEnvName = propCache.getProperty(
                        srk.getExpressState().getDealInstitutionId(),
                        "mosapp.mossystem.ratesync.ExpertEnvironmentName",
                        "Expert");
				
				// Fetch ratecode from pricingprofile 
				String sql = "SELECT ratecode FROM pricingprofile WHERE pricingprofileid = "
						+ pricingProfileID;
				String rateCode = "???";
				int key = jExec.execute(sql);
				if (jExec.next(key)) {
					rateCode = jExec.getString(key, 1);
				}
				jExec.closeData(key);
				
				// Prepare mail Subject
				mailSubject = "Rate Synchronization failure alert for "
					+ expertEnvName;
				
				// Prepare mail content
				int errorCode = e.getErrorCode();
				String errorMsg = e.toString();		//#DG594 improve loging
				if (errorCode > 20000) {
					mailText = errorMsg.substring(errorMsg.indexOf(':') + 1,
							errorMsg.indexOf('#'));
				} else if (errorCode == 2068) {
					mailText = "Database : Connecting to Remote database failed";
				} else {
					mailText = errorMsg.substring(errorMsg.indexOf(':') + 1,
							errorMsg.indexOf('\n'));
				}
				mailText = "Rate Synchronization for "
						+ expertEnvName
						+ " initiated by "
						+ expressEnvName + " has failed due to:\n"
						+ mailText + "\n\n\n";


				mailText = mailText
						+ "Use these SQL's to update rates in Expert" +
								"\n(Replace <<Value from Step 1>> with actual values):\n\n";

				if (insertFlag == 1) {
					
					// SQL to identify PricingProfileID
					mailText = mailText
							+ "Step 1 - Fetch pricing profile id: \n\n";
					mailText = mailText
							+ "SELECT pp.pricing_profile_id FROM pricing_profile pp WHERE" +
									" pp.rate_code = '"	+ rateCode + "';\n\n";
					
					// SQL to set expirydate for current active rate record	
					mailText = mailText
							+ "Step 2 - Update Expiry Date for current active record: \n\n";
					mailText = mailText
							+ "UPDATE pricing_rate_inventory pri \n"
							+ "SET pri.index_expiry_date = TO_DATE('"
							+ indexEffectiveDate
							+ "','DD/MM/YYYY HH24:MI:SS')\n"
							+ "WHERE pri.pricing_profile_id = <<Value from Step 1>>\n"
							+ "AND pri.index_expiry_date IS NULL;\n\n";
					
					// SQL to insert new rate record
					mailText = mailText
							+ "Step 3 - Insert new active rate record: \n\n";
					mailText = mailText
							+ "INSERT INTO pricing_rate_inventory(\n"
							+ "pricing_rate_inventory_id, pricing_profile_id, discount_percentage,\n"
							+ "internal_rate_percentage, index_effective_date, index_expiry_date,\n"
							// ***** Change by NBC Impl. Team - CR7 Fix - Start *****//
							+ "base_rate_percentage, best_rate, maximum_discount_allowed,\n"
							+ "prime_index_rate_inventory_id, prime_base_adj, teaser_discount, teaser_term)\n"
							+ "VALUES(pricing_rate_inventory_seq.NEXTVAL, <<Value from Step 1>> ,\n"
							+ ((discountPercentage==0)? "NULL" : "" + discountPercentage) + ", " 
							+ internalRatePercentage + ", TO_DATE('"
							+ indexEffectiveDate
							+ "','DD/MM/YYYY HH24:MI:SS'),\n" + "NULL, "
							+ ((baseRatePercentage==0)? "NULL" : "" + baseRatePercentage) + ","
							+ bestRate + ", " + maximumDiscountAllowed + " ,"
							+ primeIndexRateInventoryID + "," 
							+ ((primeBaseAdj==0)? "NULL" : "" + primeBaseAdj) + ","
							+ ((teaserDiscount==0)? "NULL" : "" + teaserDiscount) + "," 
							+ ((teaserTerm==0)? "NULL" : "" + teaserTerm)
							// ***** Change by NBC Impl. Team - CR7 Fix - End *****//
							+ ");\n\n";

				} else if (insertFlag == 0) {

					// SQL to identify PricingProfileID
					mailText = mailText
							+ "Step 1 - Fetch pricing profile id: \n\n";
					mailText = mailText
							+ "SELECT pp.pricing_profile_id FROM pricing_profile pp WHERE pp.rate_code = '"
							+ rateCode + "';\n\n";

					// SQL to update expiry date for most recent inactive record
					mailText = mailText
							+ "Step 2 - Update Expiry Date for previous active record: \n\n";
					mailText = mailText
							+ "UPDATE pricing_rate_inventory pri \n"
							+ "SET pri.index_expiry_date = TO_DATE('"
							+ indexEffectiveDate
							+ "','DD/MM/YYYY HH24:MI:SS')\n"
							+ "WHERE pri.pricing_rate_inventory_id = (\n"
							+ "SELECT MAX(pri2.pricing_rate_inventory_id) \n"
							+ "FROM pricing_rate_inventory pri2 \n"
							+ "WHERE pri2.pricing_profile_id = <<Value from Step 1>>\n"
							+ "AND pri2.index_expiry_date IS NOT NULL);\n\n";

					// SQL to update rate values for pending active record
					mailText = mailText
							+ "Step 3 - Update pending active rate record values: \n\n";
					mailText = mailText
							+ "UPDATE pricing_rate_inventory pri\n"
							+ "SET pri.index_effective_date = TO_DATE('"
							+ indexEffectiveDate
							+ "','DD/MM/YYYY HH24:MI:SS'),\n"
							// ***** Change by NBC Impl. Team - CR7 Fix - Start *****//
							+ "pri.discount_percentage = " + ((discountPercentage==0)? "NULL" : "" + discountPercentage) + ", " 
							+ "pri.internal_rate_percentage = "
							+ internalRatePercentage
							+ ",\n"
							+ "pri.index_expiry_date = NULL,"
							+ "pri.base_rate_percentage = " + ((baseRatePercentage==0)? "NULL" : "" + baseRatePercentage) + ",\n"
							+ "pri.best_rate = " + bestRate
							+ ", pri.maximum_discount_allowed = " + maximumDiscountAllowed+ ", "
							+ "\n"
							+ "pri.prime_index_rate_inventory_id = " + primeIndexRateInventoryID + "," 
							+ "pri.prime_base_adj = " + ((primeBaseAdj==0)? "NULL" : "" + primeBaseAdj) + ","
							+ "pri.teaser_discount = " + ((teaserDiscount==0)? "NULL" : "" + teaserDiscount) + "," 
							+ "pri.teaser_term = " + ((teaserTerm==0)? "NULL" : "" + teaserTerm)
							+ "\n"
							// ***** Change by NBC Impl. Team - CR7 Fix - End *****//
							+ "WHERE pri.pricing_profile_id = <<Value from Step 1>>\n"
							+ "AND pri.index_effective_date = (\n"
							+ "SELECT MAX(pri.index_effective_date)\n"
							+ "FROM pricing_rate_inventory pri \n"
							+ "WHERE pri.pricing_profile_id = <<Value from Step 1>> );\n\n";

	}

				// Create and Insert new documentqueue record
				queue.create(
					new Date(), // Time Requested
					0, // Requestor User ID
					mailTo, // Recepient mail address
					mailSubject, // Mail subject line
					mailText, // Mail content text
					dppk // Document Profile PK
				);
				
				callableStmt.close();
			} catch (Exception ex) {
				logger.error("@RAH.updateExpertRates::Error sending failure mail\n" + ex);		//#DG594 improve loging
			}
		} catch (Exception e) {
			logger.error("@RAH.updateExpertRates::Expert rate sync failed\n" + e);		//#DG594 improve loging
		}
	}
	// ***** Change by NBC Impl. Team - Version 1.1 - End *****//

  //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
  /*
  *
  *
  */
  private void setExpriryDateForCurrentPrimeIndexRecord(SessionResourceKit srk,
                                                        PrimeIndexRateInventory primeIndexInventoryToUpdate,
                                                        int origBaseIndexId,
                                                        int baseIndexFromScreen)
  throws Exception
  {
    logger.debug("--- RAH@setExpriryDateForCurrentPrimeIndexRecord::Same PrimeIndex ==> setup the Exp date");
    logger.debug("--- RAH@setExpriryDateForCurrentPrimeIndexRecord::PrimeIndexRateInventoryId_currRecord: " + primeIndexInventoryToUpdate.getPrimeIndexRateInventoryId());
    logger.debug("--- RAH@setExpriryDateForCurrentPrimeIndexRecord::getPrimeIndexExpDate_currRecord: " + primeIndexInventoryToUpdate.getPrimeIndexExpDate());

    // Update the appropriate PrimeIndexRateInventory record ==> set Dates (Effective and
    // Expriry ones as usual in pricing business).
    primeIndexInventoryToUpdate.setPrimeIndexExpDate(new Date());
    primeIndexInventoryToUpdate.ejbStore();
  }

  /*
  *
  *
  */
  private void insertPrimeIndexInventoryRecord(PricingProfile profile,
                                               PricingRateInventory inventory,
                                               PrimeIndexRateProfile primeIndexRateProfile,
                                               PrimeIndexRateInventory primeInventoryLatestRate,
                                               PrimeIndexRateInventory newPrimeIndexEntity,
                                               Hashtable pst,
                                               double newPrimeRateVal,
                                               boolean isAllPrimeRatesUpdate)
  throws Exception
  {
    // 4. Get Latest PrimeIndex Rate:
    // a. If isAllPrimeRatesUpdate is true it is a value passed from the RateAdmin
    // screen.
    // b. If is All PrimeRatesUpdate is false this value shold be extracted from the
    // primeInventoryLatestRate entity.
    int primeIndexRateProfileId = primeIndexRateProfile.getPrimeIndexRateProfileId();

    double lastPrimeIndexRate = 0.0;
    if (isAllPrimeRatesUpdate == false)
    {
      if (primeIndexRateProfileId != 0) {
        lastPrimeIndexRate = getLatestPrimeIndexRate(srk,
                                                     primeInventoryLatestRate.getPrimeIndexRateInventoryId()
                                                     );
      }
      else if (primeIndexRateProfileId == 0) {
        lastPrimeIndexRate = 0.0;
      }
    }
    else if (isAllPrimeRatesUpdate == true)
    {
      lastPrimeIndexRate = newPrimeRateVal;
    }
logger.debug("--- RAH@InsertRateRecordCaseII::lastPrimeIndexRate: " + lastPrimeIndexRate);

    //5. IMPORTANT!! Implement new logic defining the postedRate if PrimeIndex is <> 0.
    //a. Update Rate Adjustment
    double baseAdjustmentVal = 0.00;
    double teaserDiscountVal = 0.00;
    int teaserTermVal = 0;

    if (isAllPrimeRatesUpdate == false)
    {
      Object baseAdjustment = (Object) pst.get("deltaBaseAdjustment");
      baseAdjustmentVal = com.iplanet.jato.util.TypeConverter.asDouble(baseAdjustment, 0.0);
      inventory.setPrimeBaseAdj(baseAdjustmentVal);

      //--Ticket#1736--18July2005--start--//
      Object teaserDiscount = (Object) pst.get("deltaTeaserDiscount");
      teaserDiscountVal = com.iplanet.jato.util.TypeConverter.asDouble(teaserDiscount, 0.0);
      inventory.setTeaserDiscount(teaserDiscountVal);

      Object teaserTerm = (Object) pst.get("deltaTeaserTerms");
      teaserTermVal = com.iplanet.jato.util.TypeConverter.asInt(teaserTerm, 0);
      inventory.setTeaserTerm(teaserTermVal);
      //--Ticket#1736--18July2005--end--//
    }
    else if (isAllPrimeRatesUpdate == true)
    {
      baseAdjustmentVal = inventory.getPrimeBaseAdj();
    }

//temp
logger.debug("--- RAH@InsertPrimeIndexInventoryRecord::BaseAdjustment: " + baseAdjustmentVal);
logger.debug("--- RAH@InsertPrimeIndexInventoryRecord::primeIndexRateProfileId: " + primeIndexRateProfileId);
logger.debug("--- RAH@InsertPrimeIndexInventoryRecord::IndexEffDate: " + inventory.getIndexEffectiveDate());
logger.debug("--- RAH@InsertPrimeIndexInventoryRecord::InventoryId: " + inventory.getPricingRateInventoryID());

    //b. IMPORTANT!! For the new association between the rate and PrimeIndex
    // set the appropriate indicator to 'Y' or back to 'N' (default).
    //5b. Update InternalRatePercentage (a.k.a. PostedRate) if PrimeIndex is <> 0: current case.
    if (primeIndexRateProfileId != 0) {
      inventory.setInternalRatePercentage(lastPrimeIndexRate + baseAdjustmentVal);
      profile.setPrimeIndexIndicator("Y");
    }
    else if (primeIndexRateProfileId == 0) {
      double postedRateValNew = com.iplanet.jato.util.TypeConverter.asDouble(getCurrNDPage().getDisplayFieldValue("tbPostedRate"));
      inventory.setInternalRatePercentage(postedRateValNew);
      profile.setPrimeIndexIndicator("N");
    }

    //c. Reassign PrimeIndexId in PricingProfile as well.
    profile.setPrimeIndexRateProfileId(primeIndexRateProfileId);

    // 6. Set all field in new PrimeIndexRateInventory from #3.
    // b. Get/Set the rest.
    newPrimeIndexEntity.setPrimeIndexRateProfileId(primeIndexRateProfileId);

    // Enhanced based on the MtgRateEffDate/New EffDate.
    Date dtPrimeRateEffDate = null;
    if (isAllPrimeRatesUpdate == false)
    {
      dtPrimeRateEffDate =(Date) pst.get("origBaseRateEffDate");
logger.debug("--- RAH@InsertPrimeIndexInventoryRecord_Update::PrimeRateEffDate: " + dtPrimeRateEffDate.toString());
      newPrimeIndexEntity.setPrimeIndexEffDate(dtPrimeRateEffDate);
    }
    else if (isAllPrimeRatesUpdate == true)
    {
      dtPrimeRateEffDate =(Date) pst.get("newPrimeRateEffDate");
logger.debug("--- RAH@InsertPrimeIndexInventoryRecord_newPrime::PrimeRateEffDate: " + dtPrimeRateEffDate.toString());
      newPrimeIndexEntity.setPrimeIndexEffDate(dtPrimeRateEffDate);
    }

    newPrimeIndexEntity.setPrimeIndexRate(lastPrimeIndexRate);

    // 7. Update PricingRateInventory with the new PrimeIndexRateInventoryId
    // associated with this PricingRateInventory record.
    inventory.setPrimeIndexRateInventoryId(newPrimeIndexEntity.getPrimeIndexRateInventoryId());
logger.debug("--- RAH@InsertPrimeIndexInventoryRecord::NewPrimeIndexRateInventoryIdToPricingInv: " + newPrimeIndexEntity.getPrimeIndexRateInventoryId());

    newPrimeIndexEntity.ejbStore();
    // IMPORTANT!! This trigger spoil the PricingInventory store here with following
    // overriding and moved to the
    //primeIndexInventoryCurr.ejbStore();
    inventory.ejbStore();
    profile.ejbStore();
  }

  /**
  *
  *
  */
  // TODO: Separate into this small call.
  private void setPrimeIndexFlagInPricingProfile(PricingProfile profile,
                                                 String flagVal)
  {
  }

  /**
  *
  *
  */
  private PrimeIndexRateInventory getLatestPrimeIndexRateInventory(PrimeIndexRateProfile primeIndexRateProfile)
  throws Exception
  {
    PrimeIndexRateInventory primeInventoryLatestRate = null;

    // 3. Find all appropriate values in appropriate PrimeIndexRateProfile record.
    // a. Get the latest rate from the Collection.
    Collection primeIndexRateInventories = primeIndexRateProfile.getPrimeIndexRateInventories();
logger.debug("--- RAH@getLatestPrimeIndexRateInventory::PrimeIndexRateInventoriesSize: " + primeIndexRateInventories.size());

    Iterator it = primeIndexRateInventories.iterator();

    while (it.hasNext())
    {
      primeInventoryLatestRate = (PrimeIndexRateInventory) it.next();
      primeInventoryLatestRate.getPrimeIndexRateInventoryId();

// temp debug
logger.debug("--- RAH@getLatestPrimeIndexRateInventory::PrimeIndexRateInventoryId: " + primeInventoryLatestRate.getPrimeIndexRateInventoryId());
logger.debug("--- RAH@getLatestPrimeIndexRateInventory::PrimeIndexRate: " + primeInventoryLatestRate.getPrimeIndexRate());
logger.debug("--- RAH@getLatestPrimeIndexRateInventory::PrimeInventory.getPrimeIndexEffDate: " + primeInventoryLatestRate.getPrimeIndexEffDate());
logger.debug("--- RAH@getLatestPrimeIndexRateInventory::GetPrimeIndexExpDate: " + primeInventoryLatestRate.getPrimeIndexExpDate());

      if (primeInventoryLatestRate.getPrimeIndexExpDate() == null)
      {
        primeInventoryLatestRate = primeInventoryLatestRate;
        break;
      }
    }

    return primeInventoryLatestRate;
  }

  /*
  *
  *
  */
  private void insertOriginalRateRecordWithUpdates(PricingRateInventory inventory,
                                                   Hashtable pst,
                                                   int origBaseIndexId,
                                                   int baseIndexFromScreen,
                                                   int pricingUpdateType)
  throws Exception
  {
      inventory.setIndexEffectiveDate( (Date) pst.get("deltaRateEffectiveDate"));

      Object postedRate = (Object) pst.get("deltaPostedRate");
      double postedRateVal = com.iplanet.jato.util.TypeConverter.asDouble(postedRate, 0.0);
      inventory.setInternalRatePercentage(postedRateVal);

      Object maxDiscountRate = (Object) pst.get("deltaMaxDiscountRate");
      double maxDiscountRateVal = com.iplanet.jato.util.TypeConverter.asDouble(maxDiscountRate, 0.0);
      inventory.setMaximumDiscountAllowed(maxDiscountRateVal);

      inventory.setBestRate(postedRateVal - maxDiscountRateVal);

      // IMPORTANT!! This case should be split into two sub-cases since InternalRate
      // percentage should be updated differently.
      // Pricing Rate Business as usual (without PrimeIndexRate updates):
      // 1. or it may be non-changed PrimeIndexRateProfile.
      // 2. It may be a case of PrimeIndexRateProfile = 0 (pageCondition6 false)
      // ==> no PrimeIndexRateInventory associates at all.
      Object baseIndexId = (Object) pst.get("deltaBaseIndexId");
      int baseIndexIdVal = com.iplanet.jato.util.TypeConverter.asInt(baseIndexId);
      inventory.setPrimeIndexRateInventoryId(baseIndexIdVal);
logger.debug("--- insertOriginalRateRecordWithUpdates::BaseIndexIdVal: " + baseIndexIdVal);

      Object baseAdjustment = (Object) pst.get("deltaBaseAdjustment");
      double baseAdjustmentVal = com.iplanet.jato.util.TypeConverter.asDouble(baseAdjustment, 0.0);
      inventory.setPrimeBaseAdj(baseAdjustmentVal);

      if (origBaseIndexId == baseIndexFromScreen)
      {
        inventory.setInternalRatePercentage(postedRateVal + baseAdjustmentVal);
      }

      inventory.ejbStore();
  }

  /*
  *
  *
  */
  private PricingRateInventory retrievePricingRateInventory(SessionResourceKit srk,
                                                            PricingProfilePK pppk,
                                                            GregorianCalendar inpDate,
                                                            int profileId,
                                                            boolean isAllPrimeRatesUpdate,
                                                            int thePendingInventory)
  {
    PricingRateInventory inventory = null;

    // Create new record in the PRICINGRATEINVENTORY with new PK and FK for PricingProfile.
    // Check if any Pending record exist, if YES ==> Update the Record instead of create a new one
    //  -- Fully reconsidered for PrimeIndexRate implementation.

    //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
    //  Moved this function call to method "insertRateRecord"
    //  int thePendingInventory = getPendingPriceInventoryId(srk, profileId);
    //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//

    try
    {
      if (thePendingInventory == -1)
      {
        inventory = new PricingRateInventory(srk);
logger.debug("--- RAH@GetPricingRateInventory::PricingProfileId(no Pending): " + inventory.getPricingProfileID());

        if(isAllPrimeRatesUpdate == false)
        {
          inventory.create(inventory.createPrimaryKey(), pppk);
          inventory.getPricingProfileID();
logger.debug("--- RAH@GetPricingRateInventory::isAllPrimeRatesUpdateFalse: " + inventory.getPricingRateInventoryID());
        }
        else if (isAllPrimeRatesUpdate == true)
        {
          int currentPricingRateInventoryId = getCurrentPricingRateInventoryId(srk, profileId);
logger.debug("--- RAH@GetPricingRateInventory::CurrentPricingInventoryId: " + currentPricingRateInventoryId );

          PricingRateInventory currentInventory = new PricingRateInventory(srk, currentPricingRateInventoryId);
logger.debug("--- RAH@GetPricingRateInventory::isAllPrimeRatesUpdateTrue: " + currentInventory.getPricingRateInventoryID());

          //Create new full ('copy') pricingRateInventory Entity to follow the rest
          // of the logic after (PrimeIndexRate related inserts).
          PricingRateInventory newPricingRateInventory = new PricingRateInventory(srk);
          newPricingRateInventory.create(currentInventory.getPricingProfileID(),
                                         currentInventory.getDiscountPercentage(),
                                         currentInventory.getInternalRatePercentage(),
                                         DBA.sqlStringFrom(inpDate.getTime()),
                                         null,
                                         currentInventory.getBaseRatePercentage(),
                                         currentInventory.getBestRate(),
                                         currentInventory.getMaximumDiscountAllowed(),
                                         currentInventory.getPrimeIndexRateInventoryId(),
                                         currentInventory.getPrimeBaseAdj(),
                                         //--Ticket#1736--18July2005--start--//
                                         currentInventory.getTeaserDiscount(),
                                         currentInventory.getTeaserTerm());
                                         //--Ticket#1736--18July2005--end--//

          newPricingRateInventory.ejbStore();
          inventory = newPricingRateInventory;
logger.debug("--- RAH@GetPricingRateInventory::newPricingRateInventory: " + newPricingRateInventory.getPricingRateInventoryID());

        }
      }
      else
      {
        // Override here the pending status for both cases: single Pricing rate update
        // from the screen: isAllPrimeRatesUpdate = false mode
        // and PrimeIndexRate update multiple mode from the InterestRateAdmin screen:
        // isAllPrimeRatesUpdate = true mode.
        inventory = new PricingRateInventory(srk, thePendingInventory);
logger.debug("--- RAH@GetPricingRateInventory::PendingProfileId: " + inventory.getPricingProfileID());
      }
    }
    catch (Exception ex)
    {
      logger.debug("--- RAH@GetPricingRateInventory::Exception getting the PricingInventory entity: " + ex);
    }

    return inventory;
  }

  /**
  *
  *
  */
  private int getMtgRatePrimeRateRelation(boolean isPrimeRateAlreadyAssociated,
                                          int originalBaseIndexId,
                                          int baseIndexIdFromScreen)
  {
    int ret = UPDATE_RECORD_WITH_NO_ASSIGNED_PRIME_RATE; // default

    if (isPrimeRateAlreadyAssociated == true && baseIndexIdFromScreen != 0 && originalBaseIndexId != baseIndexIdFromScreen)
    {
logger.debug("--- RAH@getMtgRatePrimeRateRelation::Case Reassign PrimeIndex to Rate");
      ret = REASSIGN_PRIME_RATE;
    }
    else if (isPrimeRateAlreadyAssociated == false && baseIndexIdFromScreen != 0)
    {
logger.debug("--- RAH@getMtgRatePrimeRateRelation::Case Assign a new PrimeIndex");
      ret = ASSIGN_NEW_PRIME_RATE;
    }
    else if (isPrimeRateAlreadyAssociated == true && baseIndexIdFromScreen == 0 && originalBaseIndexId != 0)
    {
logger.debug("--- RAH@getMtgRatePrimeRateRelation::Un-Assign a PrimeIndex from the Rate");
      ret = UNASSIGN_PRIME_RATE;
    }
    else if (isPrimeRateAlreadyAssociated == true && originalBaseIndexId == baseIndexIdFromScreen)
    {
logger.debug("--- RAH@getMtgRatePrimeRateRelation::Non-Changed PrimeIndex previously associated with the Rate");
      ret = UPDATE_PREVIOUSLY_ASSIGNED_PRIME_RATE;
    }
    else if (isPrimeRateAlreadyAssociated == false && baseIndexIdFromScreen == 0)
    {
logger.debug("--- RAH@getMtgRatePrimeRateRelation::Pricing Inventory with no accociated PrimeIndex " +
                    "==> original Rate logic");
       ret = UPDATE_RECORD_WITH_NO_ASSIGNED_PRIME_RATE;
    }
    else //fallback
    {
      logger.debug("--- RAH@getMtgRatePrimeRateRelation::Undiscovered case ==> fallback");
             ret = UPDATE_RECORD_WITH_NO_ASSIGNED_PRIME_RATE;
    }

    return ret;
  }

 /**
  *
  *
  */
 public double getLatestPrimeIndexRate(SessionResourceKit srk, int latestPrimeIndexRateInventoryId)
 {
   double ret = 0.0;

   try
   {
     JdbcExecutor jExec = srk.getJdbcExecutor();
     int key = jExec.execute("SELECT primeIndexRate FROM primeIndexRateInventory WHERE  " +
                             " PRIMEINDEXRATEINVENTORYID = " + latestPrimeIndexRateInventoryId);

     while(jExec.next(key))
     {
       ret = jExec.getDouble(key, 1);
logger.error("RAH@getLatestPrimeIndexRateCollection::Rate: " + ret);
       break; // should be only one record at this point.
     }
     jExec.closeData(key);
   }
   catch(Exception e)
   {
     logger.error("RAH@getLatestPrimeIndexRate Exception at getting inventoryId "+e);		//#DG594 improve loging
   }
   return ret;
 }

  /**
   *
   *
   */
  public boolean viewUpdateIndexRateButton()
  {
logger.trace("--T--> RAH@viewUpdateIndexRateButton--> trace");

    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
    SessionResourceKit srk = getSessionResourceKit();

    /*
    // Prepared: if Product/client decides down the road to close an individual
    // access to a particular row this is ready to go.
    Hashtable pst = pg.getPageStateTable();
    PageCursorInfo tds2 = getPageCursorInfo(pg, "RepeatedPrimeIndex");

    // trick to keep track of row being generated
    int rowNdx =(((Integer)pst.get("ROWNDX"))).intValue();
    pst.put("ROWNDX", new Integer(rowNdx + 1));

    logger.debug("**** handleViewUpdateIndexRateButton ::::: rowNdx = " + rowNdx +
                 ", absolute = " + getCursorAbsoluteNdx(tds2, rowNdx));

    rowNdx = getCursorAbsoluteNdx(tds2, rowNdx);
    */

    int userProfileId = srk.getExpressState().getUserProfileId();
    int pageId = pg.getPageId();

logger.debug("--T--> RAH@viewUpdateIndexRateButton::UserProfileId: " + srk.getExpressState().getUserProfileId());
logger.debug("--T--> RAH@viewUpdateIndexRateButton::PageId: " + pg.getPageId());

    int userAccessTypeId = getUserAccessTypeId(userProfileId, pageId);

logger.debug("--T--> RAH@viewUpdateIndexRateButton::UserAccessTypeId: " + userAccessTypeId);

    if (userAccessTypeId == Sc.PAGE_ACCESS_VIEW_ONLY ||
        userAccessTypeId == Sc.PAGE_ACCESS_DISALLOWED)
      return false;
    else
     return true;

  }

  /**
  *
  *
  */
  protected void setPrimeIndexRateDisplayFields(QueryModelBase dobject, int rowIndx)
  {
    PageEntry pg = getTheSessionState().getCurrentPage();
    Hashtable pst = pg.getPageStateTable();

    logger.debug("RAH@setPrimeIndexRateDisplayFields::RowNum: " + rowIndx +
                 " , ModelRowIndx: " + dobject.getRowIndex());

    fillupMonths("RepeatedPrimeIndex/cbIndexRateEffMonth");

    // Parse date for repeatable fields from the current date.
    // from db format/parsing:  PHC@setDateDisplayField::dfValueFromModel: 2005-01-01 00:00:00.0
    // This implementation: Apr 28 2005 15:05

     GregorianCalendar cal = new GregorianCalendar();
     Date gregRightNow = cal.getTime();

     Locale theLocale = new Locale(BXResources.getLocaleCode(theSessionState.getLanguageId()), "");
     SimpleDateFormat dateFormatLocale = new SimpleDateFormat("yyyy-MM-dd HH:mm", theLocale);

     logger.debug("VLAD ==> setPrimeIndexRateDisplayFields  ---> CurrentTimeFromGregorigan == " + gregRightNow.toString());
     String dateInSQLFormat = new String(dateFormatLocale.format(gregRightNow));
     logger.debug("VLAD ==> setPrimeIndexRateDisplayFields  ---> CurrentTimeSQLFormatted == " + dateInSQLFormat);


     // Parse date for repeatable fields defalting to current time.
     setUpCurrentDate("RepeatedPrimeIndex/cbIndexRateEffMonth",
                      "RepeatedPrimeIndex/txIndexRateEffDay",
                      "RepeatedPrimeIndex/txIndexRateEffYear",
                      "RepeatedPrimeIndex/txIndexRateEffHour",
                      "RepeatedPrimeIndex/txIndexRateEffMinute",
                      dateInSQLFormat,
                      rowIndx);

    pst.put("DATEFIELD_INDEX", new Integer(rowIndx));
  }

  protected void setUpCurrentDate(String monthOfDate,
                                  String dayOfDate,
                                  String yearOfDate,
                                  String hourOfDate,
                                  String minuteOfDate,
                                  String dateInSQLFormat,
                                  int rowindx)
  {
      if(dateInSQLFormat.equals(""))
      {
        // Clear the Date fields
        ((ComboBox)getCurrNDPage().getDisplayField(monthOfDate)).setValue(new String("0"));
        ((TextField)getCurrNDPage().getDisplayField(dayOfDate)).setValue(new String(""));
        ((TextField)getCurrNDPage().getDisplayField(yearOfDate)).setValue(new String(""));
        ((TextField)getCurrNDPage().getDisplayField(hourOfDate)).setValue(new String(""));
        ((TextField)getCurrNDPage().getDisplayField(minuteOfDate)).setValue(new String(""));

        return;
      }

      String[] dateTokens = parseDateStringToTokens(dateInSQLFormat);

      // detect null date - no field setting
      if (dateTokens == null)
      {
        // Clear the Date fields
        ((ComboBox)getCurrNDPage().getDisplayField(monthOfDate)).setValue(new String("0"));
        ((TextField)getCurrNDPage().getDisplayField(dayOfDate)).setValue(new String(""));
        ((TextField)getCurrNDPage().getDisplayField(yearOfDate)).setValue(new String(""));
        ((TextField)getCurrNDPage().getDisplayField(hourOfDate)).setValue(new String(""));
        ((TextField)getCurrNDPage().getDisplayField(minuteOfDate)).setValue(new String(""));

        return;
      }

      try
      {
        ComboBox comboOptions = (ComboBox)getCurrNDPage().getDisplayField(monthOfDate);

        int indx = Integer.parseInt(dateTokens[1]);

       //Set the selected option of the ComboBox
        comboOptions.setValue((new Integer(indx)).toString(), true);
      }
      catch (Exception e)
      {
        logger.debug("RAH@exception Setting the current date : " + e);
      }

      ((TextField)getCurrNDPage().getDisplayField(dayOfDate)).setValue(new String(dateTokens[2]));
      ((TextField)getCurrNDPage().getDisplayField(yearOfDate)).setValue(new String(dateTokens[0]));
      ((TextField)getCurrNDPage().getDisplayField(hourOfDate)).setValue(dateInSQLFormat.substring(dateInSQLFormat.length() - 5,
                                                                                                  dateInSQLFormat.length() - 3));
      ((TextField)getCurrNDPage().getDisplayField(minuteOfDate)).setValue(dateInSQLFormat.substring(dateInSQLFormat.length() - 2));
  }

  public boolean checkDisplayThisRow2(String cursorName)
  {
    try
    {
      PageEntry pg = getTheSessionState().getCurrentPage();
      Hashtable pst = pg.getPageStateTable();

      PageCursorInfo tds2 = (PageCursorInfo)pst.get(cursorName);

      logger.debug("RAH@checkDisplayThisRow2::cursorName: " + cursorName +
                   "tds_name: " + tds2.getName() +
                   ":TotalRows: " + tds2.getTotalRows());

      if(tds2.getTotalRows() <= 0)
      {
        return false;
      }

      return true;
    }
    catch(Exception e)
    {
      logger.error("RAH@checkDisplayThisRow2");
      logger.error(e);
      return false;
    }
  }

  public boolean getIsDisplayPrimeIndexSection(SessionResourceKit srk)
  {
    boolean display = false; //default
    int numOfRecords = 0; // default

    try
    {
      JdbcExecutor jExec = srk.getJdbcExecutor();

      int key = jExec.execute("select count(*) count from primeindexrateprofile WHERE  " +
                              "primeindexrateprofileId <> 0");
      while(jExec.next(key))
      {
        numOfRecords = jExec.getInt(key, 1);
        break;
      }
      jExec.closeData(key);
    }
    catch(Exception e)
    {
      logger.error("RAH@getIsDisplayPrimeIndexSection Exception at getting condition. " + e);
    }

    if (numOfRecords > 0)
      display = true;

    return display;
  }

  public int getPrimeIndexInventoryId(int pricingProfileId)
  {
    int primeIndxInvId = 0; // default

    try
    {
      JdbcExecutor jExec = srk.getJdbcExecutor();

      int key = jExec.execute("SELECT pi.primeIndexRateInventoryId " +
                              "FROM primeIndexRateProfile prp, primeIndexRateInventory pri, PRICINGPROFILE pp, PRICINGRATEINVENTORY pi " +
                              "WHERE prp.PRIMEINDEXRATEPROFILEID = pri.PRIMEINDEXRATEPROFILEID " +
                              "AND (pri.PRIMEINDEXRATEINVENTORYID  =  pi.PRIMEINDEXRATEINVENTORYID) " +
                              "AND (pp.PRIMEINDEXRATEPROFILEID  =  prp.PRIMEINDEXRATEPROFILEID) " +
                              "AND pp.PRICINGPROFILEID = " + pricingProfileId +
                              " ORDER by pri.primeIndexRateInventoryId DESC");

      while(jExec.next(key))
      {
        primeIndxInvId = jExec.getInt(key, 1);
        break; //the only one latest record should be selected.
      }
      jExec.closeData(key);
    }
    catch(Exception e)
    {
      logger.error("RAH@getPrimeIndexInventoryId Exception at getting primeIndexInventoryId. " + e);
    }

    return primeIndxInvId;
  }

  public int getCurrentPricingRateInventoryId(SessionResourceKit srk, int pricingProfileId)
  {
    int id = - 1;

    try
    {
      JdbcExecutor jExec = srk.getJdbcExecutor();

      int key = jExec.execute("SELECT PRICINGRATEINVENTORYID FROM PRICINGRATEINVENTORY WHERE  " +
                              " PRICINGPROFILEID = " + pricingProfileId +
                              " AND INDEXEXPIRYDATE is null");
      while(jExec.next(key))
      {
        id = jExec.getInt(key, 1);
        break;
      }
      jExec.closeData(key);
    }
    catch(Exception e)
    {
      logger.error("@RateAdminHandler.getPendingPriceInventoryId Exception at getting inventoryId "+e);		//#DG594 improve loging
    }
    return id;
  }

  private int getUserAccessTypeId(int userprofileid, int pageid)
  {
    try
    {
      String theSQL =
        "SELECT USERPAGEACCESSEXCEPTION.ACCESSTYPEID FROM "
        + " USERPAGEACCESSEXCEPTION WHERE PAGEID = " + pageid + " AND USERPROFILEID = "
        + userprofileid;

      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(theSQL);
      int result = -1;

      while (jExec.next(key))
      {
        result = jExec.getInt(key, 1);
        break;
      }

      jExec.closeData(key);

      return result;
    }
    catch (Exception e)
    {
      setStandardFailMessage();
      logger.error(
        "Exception RAH@getUserAccessTypeId: Problem encountered in getting accessTypeId ");
      logger.error(e);		//#DG594 improve loging
    }
    return -1;
  }

  private boolean isUserEditAccessType()
  {
    return true;
  }
  //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

	public void setExpiryDateForAllCurrentRates(SessionResourceKit srk, int pricingProfileId, GregorianCalendar inpDate, int currPriceInventoryId)
	{
		int id = - 1;

		try
		{
            //#DG498 pending rates should be updated
			JdbcExecutor jExec = srk.getJdbcExecutor();
            String baseSql = "SELECT PRICINGRATEINVENTORYID FROM PRICINGRATEINVENTORY WHERE  " +
                              " PRICINGPROFILEID = " + pricingProfileId +
                              " AND INDEXEFFECTIVEDATE <= " + DBA.sqlStringFrom(inpDate.getTime()) +
                  " AND PRICINGRATEINVENTORYID <> " + currPriceInventoryId;
            int key = jExec.execute(baseSql +" AND indexexpirydate is null ");

			PricingRateInventory lastInventory;
      // IMPORTANT!! In should be just minutes.
			//inpDate.add(inpDate.MINUTE, - 1);
			Date expiryDate = inpDate.getTime();
            // if empty retrieve the latest
            if ( jExec.getRowCount(key) <= 0) {
              key = jExec.execute("SELECT PRICINGRATEINVENTORYID FROM ( "
                     +baseSql +" ORDER BY indexexpirydate DESC ) where ROWNUM <= 1");
            }
            //#DG498 end
			while(jExec.next(key))
			{
				id = jExec.getInt(key, 1);
				lastInventory = new PricingRateInventory(srk, id);
				lastInventory.setIndexExpiryDate(expiryDate);
				lastInventory.ejbStore();
				logger.debug("BILLY ==> Setting PRICINGRATEINVENTORYID = " + id + " ExpiryDate = " + expiryDate);
			}
			jExec.closeData(key);
		}
		catch(Exception e)
		{
			logger.error("@RateAdminHandler.setExpiryDateForAllCurrentRates Exception: " + e.toString());
		}
	}

	public int getPriceInventoryId(SessionResourceKit srk, int pricingProfileId, java.util.Date inpDate, int currPriceInventoryId)
	{
		int id = - 1;

		try
		{
			JdbcExecutor jExec = srk.getJdbcExecutor();
			int key = jExec.execute("SELECT PRICINGRATEINVENTORYID FROM PRICINGRATEINVENTORY WHERE  " +
                              " PRICINGPROFILEID = " + pricingProfileId + " AND INDEXEFFECTIVEDATE <= " +
                              DBA.sqlStringFrom(inpDate) + " AND PRICINGRATEINVENTORYID <> " +
                              currPriceInventoryId + " ORDER BY INDEXEFFECTIVEDATE DESC");
			while(jExec.next(key))
			{
				id = jExec.getInt(key, 1);
				break;
			}
			jExec.closeData(key);
		}
		catch(Exception e)
		{
			logger.error("@RateAdminHandler.getPriceInventoryId Exception at getting inventoryId "+e);		//#DG594 improve loging
		}
		return id;
	}

	public int getPendingPriceInventoryId(SessionResourceKit srk, int pricingProfileId)
	{
		int id = - 1;

		try
		{
			JdbcExecutor jExec = srk.getJdbcExecutor();

			//Compare date up to Minutes ==> set Seconds to 00
			Date theDate = new Date();
			theDate.setSeconds(00);
			int key = jExec.execute("SELECT PRICINGRATEINVENTORYID FROM PRICINGRATEINVENTORY WHERE  " +
                              " PRICINGPROFILEID = " + pricingProfileId + " AND INDEXEFFECTIVEDATE >= "
                              + DBA.sqlStringFrom(theDate) + " ORDER BY INDEXEFFECTIVEDATE DESC");
			while(jExec.next(key))
			{
				id = jExec.getInt(key, 1);
				break;
			}
			jExec.closeData(key);
		}
		catch(Exception e)
		{
			logger.error("@RateAdminHandler.getPendingPriceInventoryId Exception at getting inventoryId "+e);		//#DG594 improve loging
		}
		return id;
	}

	private void enableRate(PageEntry pg, Hashtable pst, SessionResourceKit srk)
		throws Exception
	{
		changeRate(pg, pst, srk, Mc.PRICING_STATUS_ACTIVE);
	}


	//////////////////////////////////////////////////////////////////////
	// if User comfirms the decision to turn rate status to 'Disabled'
	public void handleOnDialogButton()
	{
		logger.debug("--- RateAdminHandler ---> handleOnDialogButton");

		PageEntry pg = getTheSessionState().getCurrentPage();

		if (pg.getPageCondition3() == false)
		{
			return;
		}

		SessionResourceKit srk = getSessionResourceKit();

		try
		{
			srk.beginTransaction();
			Hashtable pst = pg.getPageStateTable();
			if (pg.getPageCondition4() == true)
			{
				//change the Rate Status	to 'Disabled'
				disableRate(pg, pst, srk);
				setEffectDateToCurDate(pg, pst, srk);
			}
			else
			{
				//Insert New Rate Record
				PassiveMessage pm = runAllIMBusinessRules(pg, srk);
				if (pm != null)
				{
					pm.setGenerate(true);
					getTheSessionState().setPasMessage(pm);

					//setActiveMessageToAlert(Sc.FUNDING_ACTIVITY_ERROR,ActiveMsgFactory.ISCUSTOMCONFIRM);
					return;
				}

        //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
        Object chosenBaseIndexId = (Object) pst.get("chosenBaseIndexId");
        int chosenBaseIndexIdVal = com.iplanet.jato.util.TypeConverter.asInt(chosenBaseIndexId);
        int baseIndexFromScreen = com.iplanet.jato.util.TypeConverter.asInt(getCurrNDPage().getDisplayFieldValue("cbBaseIndex"));

    logger.debug("--- RAH@handleOnDialogButton::ChosenBaseIndexIdVal: " + chosenBaseIndexIdVal);
    logger.debug("--- RAH@handleOnDialogButton::ChosenBaseIndexIdFromScreen: " + getCurrNDPage().getDisplayFieldValue("cbBaseIndex"));
    logger.debug("--- RAH@handleOnDialogButton::PgCond6: " + pg.getPageCondition6());

        int pricingUpdateType = getMtgRatePrimeRateRelation(pg.getPageCondition6(),
                                                            chosenBaseIndexIdVal,
                                                            baseIndexFromScreen);

        int pricingProfileId = ((Double)(pst.get("chosenProfileId"))).intValue();
logger.debug("RAH@HandleOnSubmit::ProfileId: " + pricingProfileId);

				insertRateRecord(pg,
                         pst,
                         srk,
                         new java.util.GregorianCalendar(),
                         pricingUpdateType,
                         pricingProfileId,
                         0.00, //TEMP
                         false);
        //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//
			}
			resetAllConditions(pg);

			//For main Rate Administration page: refresh data because of updating rate status
			getPageCursorInfo(pg.getParentPage(), "DEFAULT").setRefresh(true);
			navigateToNextPage();
			srk.commitTransaction();
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Problem encountered processing Ok User Dialog Button of the Update Pricing Rate Screen");
			logger.error(e.toString());
      //logger.error("The Stack Dump : " + StringUtil.stack2string(e));
		}
	}

	///////////////////////////////////////////////////////////////////////////
	private void disableRate(PageEntry pg, Hashtable pst, SessionResourceKit srk)
		throws Exception
	{
		changeRate(pg, pst, srk, Mc.PRICING_STATUS_DISABLED);
	}

	private void changeRate(PageEntry pg, Hashtable pst, SessionResourceKit srk, int status)
		throws Exception
	{
		int profileIdVal = ((Double)(pst.get("chosenProfileId"))).intValue();

		PricingProfile profile = new PricingProfile(srk, profileIdVal);

    //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    //--Ticket#1736--18July2005--start--//
    // TeaserDiscount and TeaserTerm fields are moved to the PricingRateInventory table.
    // No need to update mtgProd!!
    //--Ticket#1736--18July2005--end--//
    //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

		profile.setPricingStatusId(status);
		profile.setRateDisabledDate(new Date());
		profile.ejbStore();

	}

	// Set effective date for disabled status to current date.
	private void setEffectDateToCurDate(PageEntry pg, Hashtable pst, SessionResourceKit srk)
		throws Exception
	{
		int inventoryIdVal = ((Double)(pst.get("chosenInventoryId"))).intValue();

		PricingRateInventory pricingInventory = new PricingRateInventory(srk, inventoryIdVal);
		pricingInventory.setIndexEffectiveDate(new Date());
		pricingInventory.ejbStore();
	}

	///////////////////////////////////////////////////////////////////////////
	public void enableRateUpdate()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		SessionResourceKit srk = getSessionResourceKit();

		try
		{
			srk.beginTransaction();
			Hashtable pst = pg.getPageStateTable();
			enableRate(pg, pst, srk);
			resetAllConditions(pg);

			//For main Rate Administration page: refresh data because of updating rate status
			getPageCursorInfo(pg.getParentPage(), "DEFAULT").setRefresh(true);
			navigateToNextPage();
			srk.commitTransaction();
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Problem encountered making Rate Enable");
			logger.error(e);		//#DG594 improve loging
		}
	}

	/////////////////////////////////////////////////////////////////////////
	private void resetAllConditions(PageEntry pg)
	{
		logger.debug("--- RateAdminHandler ---> resetAllConditions");
		pg.setPageCondition3(false);
		pg.setPageCondition4(false);
    //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    pg.setPageCondition5(true);
    pg.setPageCondition6(false);
    //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//
	}

	public boolean generateUpdateButton()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();

    int rowindex = com.iplanet.jato.util.TypeConverter.asInt(getCurrNDPage().getDisplayFieldValue("Repeated1/hdRowNdx"));
logger.debug("VLAD ==> RateAdm@generateUpdateButton - RowIdx = " + rowindex);

		int [ ] iStatusId =(int [ ]) pst.get("iStatusId");

logger.debug("VLAD ==> RateAdm@generateUpdateButton - StatusIdArraySize = " + iStatusId.length);

    int statusid = iStatusId [ rowindex - 1 ];

logger.debug("VLAD ==> RateAdm@generateUpdateButton - StatusId = " + statusid);

		logger.debug("VLAD ==> RateAdm@generateUpdateButton - RowIdx = " + rowindex +
		      " statusid = " + statusid + " iStatusId.length = " + iStatusId.length);
		if (statusid == Mc.PRICING_STATUS_DISABLED)
		{
			// need to switch the history button a little bit to the right, this could be
			// fixed by creating a blank image with the same size of the hidden update button.
			//field.setHtmlText("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

			return false;
		}
		return true;
	}


	///////////////////////////////////////////////////////////////////

	public PassiveMessage runAllIMBusinessRules(PageEntry pg, SessionResourceKit srk)
	{
		PassiveMessage pm = new PassiveMessage();
		BusinessRuleExecutor brExec = new BusinessRuleExecutor();

		try
		{
			brExec.BREValidator(getTheSessionState(), pg, srk, pm, "IM-%");
			logger.trace("--T--> @RateAdminHandler.runIM001BusinessRules: IM rules");
			if (pm.getNumMessages() > 0) return pm;
		}
		catch(Exception e)
		{
			logger.error("Problem encountered during funding, @RateAdminHandler.runIM001BusinessRules");
			logger.error(e);
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
		}
		return null;
	}

  //--> Added by Billy 05Sept2002
  public void checkAndSyncDOWithCursorInfo(RequestHandlingTiledViewBase theRepeat)
	{
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
    setPageFlag(pg);
 		Hashtable pst = pg.getPageStateTable();
 	  PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");
    try
    {
      //--Release2.1--//
      //--> Sync it in any case. -- By Billy 17Dec2002
      if(this.isRateAdmin)
        ((pgInterestRateAdminRepeated1TiledView)theRepeat).setCurrentDisplayOffset(tds.getFirstDisplayRowNumber() - 1);
      else
        ((pgInterestRateHistoryRepeated1TiledView)theRepeat).setCurrentDisplayOffset(tds.getFirstDisplayRowNumber() - 1);
      //}
    }
    catch(Exception mce)
    {
      logger.debug("RateAdminHandler@checkAndSyncDOWithCursorInfo::Exception: " + mce);
    }
	}

  //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
  public void checkAndSyncDOWithPrimeRateCursor(RequestHandlingTiledViewBase theRepeat)
  {
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
    setPageFlag(pg);
    Hashtable pst = pg.getPageStateTable();
    PageCursorInfo tds2 =(PageCursorInfo) pst.get("RepeatedPrimeIndex");

    try
    {
      if(this.isRateAdmin)
        ((pgInterestRateAdminRepeatedPrimeIndexTiledView)theRepeat).setCurrentDisplayOffset(tds2.getFirstDisplayRowNumber() - 1);
      else
        ((pgInterestRateAdminRepeatedPrimeIndexTiledView)theRepeat).setCurrentDisplayOffset(tds2.getFirstDisplayRowNumber() - 1);
    }
    catch(Exception mce)
    {
      logger.debug("RAH@checkAndSyncDOWithPrimeRateCursor::Exception: " + mce);
    }
  }
  //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//
}

