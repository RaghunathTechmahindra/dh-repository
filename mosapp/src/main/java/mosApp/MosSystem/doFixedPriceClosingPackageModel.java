/*
 * @(#)doFixedPriceClosingPackageModel.java    2006-1-26
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package mosApp.MosSystem;

import java.math.BigDecimal;
import java.sql.Timestamp;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 * doFixedPriceClosingPackageModel - 
 *
 * @version   1.0 2006-1-26
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface doFixedPriceClosingPackageModel
    extends QueryModel, SelectQueryModel {

    /**
     * fields bound name definition.
     */
    public static final String FIELD_DFFPCPDEALID = "dfFPCPDealId";
    public static final String FIELD_DFFPCPCOPYID = "dfFPCPCopyId";
    public static final String FIELD_DFFPCPREQUESTSTATUS =
        "dfFPCPRequestStatus";
    public static final String FIELD_DFFPCPREQUESTSTATUSDATE =
        "dfFPCPRequestStatusDate";
    public static final String FIELD_DFFPCPCONTACTFIRSTNAME =
        "dfFPCPContactFirstName";
    public static final String FIELD_DFFPCPCONTACTLASTNAME =
        "dfFPCPContactLastName";
    public static final String FIELD_DFFPCPCONTACTEMAIL =
        "dfFPCPContactEmail";
    public static final String FIELD_DFFPCPCONTACTCOMMENTS =
        "dfFPCPContactComments";
    public static final String FIELD_DFFPCPCONTACTWORKPHONEAREA =
        "dfFPCPContactWorkPhoneArea";
    public static final String FIELD_DFFPCPCONTACTWORKPHONENUM =
        "dfFPCPContactWorkPhoneNum";
    public static final String FIELD_DFFPCPCONTACTWORKPHONEEXT =
        "dfFPCPContactWorkPhoneExt";
    public static final String FIELD_DFFPCPCONTACTCELLPHONEAREA =
        "dfFPCPContactCellPhoneArea";
    public static final String FIELD_DFFPCPCONTACTCELLPHONENUM =
        "dfFPCPContactCellPhoneNum";
    public static final String FIELD_DFFPCPCONTACTHOMEPHONEAREA =
        "dfFPCPContactHomePhoneArea";
    public static final String FIELD_DFFPCPCONTACTHOMEPHONENUM =
        "dfFPCPContactHomePhoneNum";

    /**
     * get getter and setter definition.
     */
    public BigDecimal getDfFPCPDealId();
    public void setDfFPCPDealId(BigDecimal id);

    public BigDecimal getDfFPCPCopyId();
    public void setDfFPCPCopyId(BigDecimal id);

    public String getDfFPCPRequestStatus();
    public void setDfFPCPRequestStatus(String status);

    public Timestamp getDfFPCPRequestStatusDate();
    public void setDfFPCPRequestStatusDate(Timestamp date);

    public String getDfFPCPContactFirstName();
    public void setDfFPCPContactFirstName(String value);

    public String getDfFPCPContactLastName();
    public void setDfFPCPContactLastName(String value);

    public String getDfFPCPContactEmail();
    public void setDfFPCPContactEmail(String value);

    public String getDfFPCPContactComments();
    public void setDfFPCPContactComments(String value);

    public String getDfFPCPContactWorkPhoneArea();
    public void setDfFPCPContactWorkPhoneArea(String value);

    public String getDfFPCPContactWorkPhoneNum();
    public void setDfFPCPContactWorkPhoneNum(String value);

    public String getDfFPCPContactWorkPhoneExt();
    public void setDfFPCPContactWorkPhoneExt(String value);

    public String getDfFPCPContactCellPhoneArea();
    public void setDfFPCPContactCellPhoneArea(String value);

    public String getDfFPCPContactCellPhoneNum();
    public void setDfFPCPContactCellPhoneNum(String value);

    public String getDfFPCPContactHomePhoneArea();
    public void setDfFPCPContactHomePhoneArea(String value);

    public String getDfFPCPContactHomePhoneNum();
    public void setDfFPCPContactHomePhoneNum(String value);
}
