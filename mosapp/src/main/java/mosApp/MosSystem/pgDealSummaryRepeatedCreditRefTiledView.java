package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgDealSummaryRepeatedCreditRefTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealSummaryRepeatedCreditRefTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(2);
		setPrimaryModelClass( doDetailViewCreditRefsModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStADCreditReferenceType().setValue(CHILD_STADCREDITREFERENCETYPE_RESET_VALUE);
		getStADReferenceDesc().setValue(CHILD_STADREFERENCEDESC_RESET_VALUE);
		getStADInsitutionName().setValue(CHILD_STADINSITUTIONNAME_RESET_VALUE);
		getStADTimeWithReference().setValue(CHILD_STADTIMEWITHREFERENCE_RESET_VALUE);
		getStADAccountNumber().setValue(CHILD_STADACCOUNTNUMBER_RESET_VALUE);
		getStADCurrentBalance().setValue(CHILD_STADCURRENTBALANCE_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STADCREDITREFERENCETYPE,StaticTextField.class);
		registerChild(CHILD_STADREFERENCEDESC,StaticTextField.class);
		registerChild(CHILD_STADINSITUTIONNAME,StaticTextField.class);
		registerChild(CHILD_STADTIMEWITHREFERENCE,StaticTextField.class);
		registerChild(CHILD_STADACCOUNTNUMBER,StaticTextField.class);
		registerChild(CHILD_STADCURRENTBALANCE,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDetailViewCreditRefsModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		// The following code block was migrated from the RepeatedCreditRef_onBeforeDisplayEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.setupNumOfRepeatedCreditRef(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STADCREDITREFERENCETYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewCreditRefsModel(),
				CHILD_STADCREDITREFERENCETYPE,
				doDetailViewCreditRefsModel.FIELD_DFCREDITREFERENCETYPE,
				CHILD_STADCREDITREFERENCETYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STADREFERENCEDESC))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewCreditRefsModel(),
				CHILD_STADREFERENCEDESC,
				doDetailViewCreditRefsModel.FIELD_DFREFERENCEDESC,
				CHILD_STADREFERENCEDESC_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STADINSITUTIONNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewCreditRefsModel(),
				CHILD_STADINSITUTIONNAME,
				doDetailViewCreditRefsModel.FIELD_DFINSTITUTIONNAME,
				CHILD_STADINSITUTIONNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STADTIMEWITHREFERENCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewCreditRefsModel(),
				CHILD_STADTIMEWITHREFERENCE,
				doDetailViewCreditRefsModel.FIELD_DFTIMEWITHREFERENCE,
				CHILD_STADTIMEWITHREFERENCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STADACCOUNTNUMBER))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewCreditRefsModel(),
				CHILD_STADACCOUNTNUMBER,
				doDetailViewCreditRefsModel.FIELD_DFACCOUNTNUMBER,
				CHILD_STADACCOUNTNUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STADCURRENTBALANCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewCreditRefsModel(),
				CHILD_STADCURRENTBALANCE,
				doDetailViewCreditRefsModel.FIELD_DFCURRENTBALANCE,
				CHILD_STADCURRENTBALANCE_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStADCreditReferenceType()
	{
		return (StaticTextField)getChild(CHILD_STADCREDITREFERENCETYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStADReferenceDesc()
	{
		return (StaticTextField)getChild(CHILD_STADREFERENCEDESC);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStADInsitutionName()
	{
		return (StaticTextField)getChild(CHILD_STADINSITUTIONNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStADTimeWithReference()
	{
		return (StaticTextField)getChild(CHILD_STADTIMEWITHREFERENCE);
	}


	/**
	 *
	 *
	 */
	public String endStADTimeWithReferenceDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stADTimeWithReference_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setMonthsToYearsMonths(event.getContent());
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStADAccountNumber()
	{
		return (StaticTextField)getChild(CHILD_STADACCOUNTNUMBER);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStADCurrentBalance()
	{
		return (StaticTextField)getChild(CHILD_STADCURRENTBALANCE);
	}


	/**
	 *
	 *
	 */
	public doDetailViewCreditRefsModel getdoDetailViewCreditRefsModel()
	{
		if (doDetailViewCreditRefs == null)
			doDetailViewCreditRefs = (doDetailViewCreditRefsModel) getModel(doDetailViewCreditRefsModel.class);
		return doDetailViewCreditRefs;
	}


	/**
	 *
	 *
	 */
	public void setdoDetailViewCreditRefsModel(doDetailViewCreditRefsModel model)
	{
			doDetailViewCreditRefs = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STADCREDITREFERENCETYPE="stADCreditReferenceType";
	public static final String CHILD_STADCREDITREFERENCETYPE_RESET_VALUE="";
	public static final String CHILD_STADREFERENCEDESC="stADReferenceDesc";
	public static final String CHILD_STADREFERENCEDESC_RESET_VALUE="";
	public static final String CHILD_STADINSITUTIONNAME="stADInsitutionName";
	public static final String CHILD_STADINSITUTIONNAME_RESET_VALUE="";
	public static final String CHILD_STADTIMEWITHREFERENCE="stADTimeWithReference";
	public static final String CHILD_STADTIMEWITHREFERENCE_RESET_VALUE="";
	public static final String CHILD_STADACCOUNTNUMBER="stADAccountNumber";
	public static final String CHILD_STADACCOUNTNUMBER_RESET_VALUE="";
	public static final String CHILD_STADCURRENTBALANCE="stADCurrentBalance";
	public static final String CHILD_STADCURRENTBALANCE_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDetailViewCreditRefsModel doDetailViewCreditRefs=null;
  private DealSummaryHandler handler=new DealSummaryHandler();

}

