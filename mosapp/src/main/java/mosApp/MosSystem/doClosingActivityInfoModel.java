package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doClosingActivityInfoModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public String getDfCity();

	
	/**
	 * 
	 * 
	 */
	public void setDfCity(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfProvinceAbbreviation();

	
	/**
	 * 
	 * 
	 */
	public void setDfProvinceAbbreviation(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPostalFSALDU();

	
	/**
	 * 
	 * 
	 */
	public void setDfPostalFSALDU(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAddressLine1();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddressLine1(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAddressLine2();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddressLine2(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPhoneNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfPhoneNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfFaxNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfFaxNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmailAddress();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmailAddress(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfContactId();

	
	/**
	 * 
	 * 
	 */
	public void setDfContactId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAddrId();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddrId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPhoneNumberExtension();

	
	/**
	 * 
	 * 
	 */
	public void setDfPhoneNumberExtension(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFCITY="dfCity";
	public static final String FIELD_DFPROVINCEABBREVIATION="dfProvinceAbbreviation";
	public static final String FIELD_DFPOSTALFSALDU="dfPostalFSALDU";
	public static final String FIELD_DFADDRESSLINE1="dfAddressLine1";
	public static final String FIELD_DFADDRESSLINE2="dfAddressLine2";
	public static final String FIELD_DFPHONENUMBER="dfPhoneNumber";
	public static final String FIELD_DFFAXNUMBER="dfFaxNumber";
	public static final String FIELD_DFEMAILADDRESS="dfEmailAddress";
	public static final String FIELD_DFCONTACTID="dfContactId";
	public static final String FIELD_DFADDRID="dfAddrId";
	public static final String FIELD_DFPHONENUMBEREXTENSION="dfPhoneNumberExtension";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

