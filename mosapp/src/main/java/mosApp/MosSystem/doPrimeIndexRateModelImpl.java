package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.picklist.BXResources;

import com.basis100.log.*;


/**
 *
 *
 *
 */
//--Ticket#781--XD_ARM/VRM--19Apr2005--//
public class doPrimeIndexRateModelImpl extends QueryModelBase
	implements doPrimeIndexRateModel
{
	/**
	 *
	 *
	 */
	public doPrimeIndexRateModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);
		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);
		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);
		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);
		setFieldSchema(FIELD_SCHEMA);
		initialize();
	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}

	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

    logger = SysLog.getSysLogger("DOPIRM");
    logger.debug("VLAD ===> DOPIRM@SQLBeforeExecute: " + sql);

		return sql;

	}

	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //To override all the Description fields by populating from BXResource
    String defaultInstanceStateName = getRequestContext().getModelManager().getDefaultModelInstanceName(
                                      SessionStateModel.class);

    SessionStateModelImpl theSessionState = (SessionStateModelImpl) getRequestContext().getModelManager().getModel(
                                             SessionStateModel.class,
                                             defaultInstanceStateName,
                                             true
                                             );
    int languageId = theSessionState.getLanguageId();
	int institutionId = theSessionState.getDealInstitutionId();

    while (this.next())
    {
        this.setDfPrimeIndexName(BXResources.getPickListDescription(
                                institutionId, "PRIMEINDEXRATEPROFILE",
                                this.getDfPrimeIndexRateProfileId().intValue(),
                                languageId)
                                );
	   }
	}

	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfEffectiveDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFEFFECTIVEDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfEffectiveDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFEFFECTIVEDATE,value);
	}

  /**
   *
   *
   */
  public java.math.BigDecimal getDfPrimeIndexRateProfileId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFPRIMEINDEXRATEPROFILEID);
  }

  /**
   *
   *
   */
  public void setDfPrimeIndexRateProfileId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFPRIMEINDEXRATEPROFILEID,value);
  }

  /**
   *
   *
   */
  public Double getDfPrimeIndexRate()
  {
    return (Double)getValue(FIELD_DFPRIMEINDEXRATE);
  }

  /**
   *
   *
   */
  public void setDfPrimeIndexRate(Double value)
  {
    setValue(FIELD_DFPRIMEINDEXRATE,value);
  }

    /**
     *
     *
     */
    public String getDfPrimeIndexName()
    {
      return (String)getValue(FIELD_DFPRIMEINDEXNAME);
    }

    /**
     *
     *
     */
    public void setDfPrimeIndexName(String value)
    {
      setValue(FIELD_DFPRIMEINDEXNAME,value);
    }

    /**
     *
     *
     */
    public String getDfPrimeIndexDescription()
    {
      return (String)getValue(FIELD_DFPRIMEINDEXDESCRIPTION);
    }

    /**
     *
     *
     */
    public void setDfPrimeIndexDescription(String value)
    {
      setValue(FIELD_DFPRIMEINDEXDESCRIPTION,value);
    }

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  public static final String SELECT_SQL_TEMPLATE="SELECT ALL PRIMEINDEXRATEPROFILE.PRIMEINDEXNAME, " +
    // IMPORTANT TODO: change the PRIMEINDEXRATEPROFILE.PRIMEINDEXDESCRIPTION to Id
    // selection from the PrimeIndexInventory table

                                                  "MIN(PRIMEINDEXRATEPROFILE.PRIMEINDEXDESCRIPTION) MINPRIMEINDEXDESCRIPTION, " +
                                                  "MIN(PRIMEINDEXRATEINVENTORY.PRIMEINDEXEFFDATE) MINPRIMEINDEXEFFDATE, " +
                                                  "MIN(PRIMEINDEXRATEINVENTORY.PRIMEINDEXRATE) MINPRIMEINDEXRATE, " +
                                                  "MIN(PRIMEINDEXRATEINVENTORY.PRIMEINDEXRATEPROFILEID) MINPRIMEINDEXRATEPROFILEID " +

                                                  "FROM PRIMEINDEXRATEPROFILE, PRIMEINDEXRATEINVENTORY " +
                                                  "__WHERE__  GROUP BY PRIMEINDEXRATEPROFILE.PRIMEINDEXNAME " +
                                                  "ORDER BY PRIMEINDEXRATEPROFILE.PRIMEINDEXNAME ASC ";

	public static final String MODIFYING_QUERY_TABLE_NAME="PRIMEINDEXRATEPROFILE, PRIMEINDEXRATEINVENTORY";

	public static final String STATIC_WHERE_CRITERIA=" (PRIMEINDEXRATEPROFILE.PRIMEINDEXRATEPROFILEID  =  PRIMEINDEXRATEINVENTORY.PRIMEINDEXRATEPROFILEID) " +
                                                   " AND PRIMEINDEXRATEPROFILE.PRIMEINDEXRATEPROFILEID <> 0" +
                                                   " AND PRIMEINDEXRATEPROFILE.PRIMEINDEXDISDATE is null" +
                                                   " AND PRIMEINDEXRATEINVENTORY.PRIMEINDEXEXPDATE is null";

	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();

  public static final String QUALIFIED_COLUMN_DFPRIMEINDEXRATEPROFILEID="PRIMEINDEXRATEINVENTORY.MINPRIMEINDEXRATEPROFILEID";
  public static final String COLUMN_DFPRIMEINDEXRATEPROFILEID="MINPRIMEINDEXRATEPROFILEID";

  public static final String QUALIFIED_COLUMN_DFPRIMEINDEXNAME="PRIMEINDEXRATEPROFILE.PRIMEINDEXNAME";
  public static final String COLUMN_DFPRIMEINDEXNAME="PRIMEINDEXNAME";

  public static final String QUALIFIED_COLUMN_DFPRIMEINDEXRATE="PRIMEINDEXRATEINVENTORY.MINPRIMEINDEXRATE";
  public static final String COLUMN_DFPRIMEINDEXRATE="MINPRIMEINDEXRATE";

  public static final String QUALIFIED_COLUMN_DFPRIMEINDEXDESCRIPTION="PRIMEINDEXRATEPROFILE.MINPRIMEINDEXDESCRIPTION";
  public static final String COLUMN_DFPRIMEINDEXDESCRIPTION="MINPRIMEINDEXDESCRIPTION";

  public static final String QUALIFIED_COLUMN_DFEFFECTIVEDATE="PRIMEINDEXRATEINVENTORY.MINPRIMEINDEXEFFDATE";
  public static final String COLUMN_DFEFFECTIVEDATE="MINPRIMEINDEXEFFDATE";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////


	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEFFECTIVEDATE,
				COLUMN_DFEFFECTIVEDATE,
				QUALIFIED_COLUMN_DFEFFECTIVEDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"MIN(PRIMEINDEXRATEINVENTORY.PRIMEINDEXEFFDATE)",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				"MIN(PRIMEINDEXRATEINVENTORY.PRIMEINDEXEFFDATE)"));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFPRIMEINDEXRATEPROFILEID,
        COLUMN_DFPRIMEINDEXRATEPROFILEID,
        QUALIFIED_COLUMN_DFPRIMEINDEXRATEPROFILEID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "MIN(PRIMEINDEXRATEINVENTORY.PRIMEINDEXRATEPROFILEID)",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        "MIN(PRIMEINDEXRATEINVENTORY.PRIMEINDEXRATEPROFILEID)"));

      FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
        FIELD_DFPRIMEINDEXRATE,
        COLUMN_DFPRIMEINDEXRATE,
        QUALIFIED_COLUMN_DFPRIMEINDEXRATE,
        Double.class,
        false,
        true,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "MIN(PRIMEINDEXRATEINVENTORY.PRIMEINDEXRATE)",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        "MIN(PRIMEINDEXRATEINVENTORY.PRIMEINDEXRATE)"));

      FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
          FIELD_DFPRIMEINDEXNAME,
          COLUMN_DFPRIMEINDEXNAME,
          QUALIFIED_COLUMN_DFPRIMEINDEXNAME,
          String.class,
          false,
          false,
          QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
          "",
          QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
          ""));

      FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
          FIELD_DFPRIMEINDEXDESCRIPTION,
          COLUMN_DFPRIMEINDEXDESCRIPTION,
          QUALIFIED_COLUMN_DFPRIMEINDEXDESCRIPTION,
          String.class,
          false,
          false,
          QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
          "MIN(PRIMEINDEXRATEPROFILE.PRIMEINDEXDESCRIPTION)",
          QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
          "MIN(PRIMEINDEXRATEPROFILE.PRIMEINDEXDESCRIPTION)"));
	}
}

