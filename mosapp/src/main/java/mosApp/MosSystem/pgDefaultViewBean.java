package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;
import java.text.*;

import com.basis100.picklist.BXResources;

/**
 *
 *
 *
 */
public class pgDefaultViewBean extends ViewBeanBase
	implements ViewBean
{
	/**
	 *
	 *
	 */
	public pgDefaultViewBean()
	{
		super(PAGE_NAME);
		setDefaultDisplayURL(DEFAULT_DISPLAY_URL);

		registerChildren();

		initialize();

	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STATICTEXT1))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STATICTEXT1,
				CHILD_STATICTEXT1,
				CHILD_STATICTEXT1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STINSTITUTION))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STINSTITUTION,
				CHILD_STINSTITUTION,
				CHILD_STINSTITUTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBRAND))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBRAND,
				CHILD_STBRAND,
				CHILD_STBRAND_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUSERNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STUSERNAME,
				CHILD_STUSERNAME,
				CHILD_STUSERNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUSERROLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STUSERROLE,
				CHILD_STUSERROLE,
				CHILD_STUSERROLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STDATE,
				CHILD_STDATE,
				CHILD_STDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUSERID))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STUSERID,
				CHILD_STUSERID,
				CHILD_STUSERID_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStaticText1().setValue(CHILD_STATICTEXT1_RESET_VALUE);
		getStInstitution().setValue(CHILD_STINSTITUTION_RESET_VALUE);
		getStBrand().setValue(CHILD_STBRAND_RESET_VALUE);
		getStUserName().setValue(CHILD_STUSERNAME_RESET_VALUE);
		getStUserRole().setValue(CHILD_STUSERROLE_RESET_VALUE);
		getStDate().setValue(CHILD_STDATE_RESET_VALUE);
		getStUserID().setValue(CHILD_STUSERID_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STATICTEXT1,StaticTextField.class);
		registerChild(CHILD_STINSTITUTION,StaticTextField.class);
		registerChild(CHILD_STBRAND,StaticTextField.class);
		registerChild(CHILD_STUSERNAME,StaticTextField.class);
		registerChild(CHILD_STUSERROLE,StaticTextField.class);
		registerChild(CHILD_STDATE,StaticTextField.class);
		registerChild(CHILD_STUSERID,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		super.beginDisplay(event);
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics this_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	/**
	 *
	 *
	 */
	public StaticTextField getStaticText1()
	{
		return (StaticTextField)getChild(CHILD_STATICTEXT1);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStInstitution()
	{
		return (StaticTextField)getChild(CHILD_STINSTITUTION);
	}


	/**
	 *
	 *
	 */
	public boolean beginStInstitutionDisplay(ChildDisplayEvent event)
	{
		Object value = getStInstitution().getValue();
		return true;

		// The following code block was migrated from the stInstitution_onBeforeDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		////CSpHtml.sendMessage("<p>stInstitution_onBeforeDisplayEvent");
		SessionState theSessionObj =(SessionState) getSession().getAttribute(US_THE_SESSION);

		if (theSessionObj == null)
		{
			setDisplayFieldValue("stInstitution", new String("NULL"));
		}
		else
		{
			setDisplayFieldValue("stInstitution", new String(theSessionObj.getInstitutionName()));
		}

		return;
		*/
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBrand()
	{
		return (StaticTextField)getChild(CHILD_STBRAND);
	}


	/**
	 *
	 *
	 */
	public boolean beginStBrandDisplay(ChildDisplayEvent event)
	{
		Object value = getStBrand().getValue();
		return true;

		// The following code block was migrated from the stBrand_onBeforeDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		//CSpHtml.sendMessage("<p>stBrand_onBeforeDisplayEvent");
		SessionState theSessionObj =(SessionState) getSession().getAttribute(US_THE_SESSION
		/$"usTheSession"$/
		);

		if (theSessionObj == null)
		{
			setDisplayFieldValue("stBrand", new String("NULL"));
		}
		else
		{
			setDisplayFieldValue("stBrand", new String(theSessionObj.getBrandName()));
		}

		return;
		*/
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUserName()
	{
		return (StaticTextField)getChild(CHILD_STUSERNAME);
	}


	/**
	 *
	 *
	 */
	public boolean beginStUserNameDisplay(ChildDisplayEvent event)
	{
		Object value = getStUserName().getValue();
		return true;

		// The following code block was migrated from the stUserName_onBeforeDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		//CSpHtml.sendMessage("<p>stUserName_onBeforeDisplayEvent");
		SessionState theSessionObj =(SessionState) getSession().getAttribute(US_THE_SESSION);

		if (theSessionObj == null)
		{
			setDisplayFieldValue("stUserName", new String("NULL"));
		}
		else
		{
			setDisplayFieldValue("stUserName", new String(theSessionObj.getSessionUserName()));
		}

		return;
		*/
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUserRole()
	{
		return (StaticTextField)getChild(CHILD_STUSERROLE);
	}


	/**
	 *
	 *
	 */
	public boolean beginStUserRoleDisplay(ChildDisplayEvent event)
	{
		Object value = getStUserRole().getValue();
		return true;

		// The following code block was migrated from the stUserRole_onBeforeDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		//CSpHtml.sendMessage("<p>stUserRole_onBeforeDisplayEvent");
		SessionState theSessionObj =(SessionState) getSession().getAttribute(US_THE_SESSION);

		if (theSessionObj == null)
		{
			setDisplayFieldValue("stUserRole", new String("NULL"));
		}
		else
		{
			setDisplayFieldValue("stUserRole", new String(theSessionObj.getSessionUserRole()));
		}

		return;
		*/
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDate()
	{
		return (StaticTextField)getChild(CHILD_STDATE);
	}


	/**
	 *
	 *
	 */
	public boolean beginStDateDisplay(ChildDisplayEvent event)
	{
		Object value = getStDate().getValue();
		return true;

		// The following code block was migrated from the stDate_onBeforeDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		//CSpHtml.sendMessage("<p>stDate_onBeforeDisplayEvent");
		SessionState theSessionObj =(SessionState) getSession().getAttribute(US_THE_SESSION);

		if (theSessionObj == null)
		{
			setDisplayFieldValue("stDate", new String("NULL"));
		}
		else
		{
			setDisplayFieldValue("stDate", new String(theSessionObj.getSessionDateStr()));
		}

		return;
		*/
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUserID()
	{
		return (StaticTextField)getChild(CHILD_STUSERID);
	}


	/**
	 *
	 *
	 */
	public boolean beginStUserIDDisplay(ChildDisplayEvent event)
	{
		Object value = getStUserID().getValue();
		return true;

		// The following code block was migrated from the stUserID_onBeforeDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		//CSpHtml.sendMessage("<p>stUserID_onBeforeDisplayEvent");
		SessionState theSessionObj =(SessionState) getSession().getAttribute(US_THE_SESSION
		/$"usTheSession"$/
		);

		if (theSessionObj == null)
		{
			setDisplayFieldValue("stUserID", new String("NULL"));
		}
		else
		{
			setDisplayFieldValue("stUserID", new String("" + theSessionObj.getSessionUserId()));
		}

		return;
		*/
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String PAGE_NAME="pgDefault";
	public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgDefault.jsp";
	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STATICTEXT1="StaticText1";
	public static final String CHILD_STATICTEXT1_RESET_VALUE="Default page";
	public static final String CHILD_STINSTITUTION="stInstitution";
	public static final String CHILD_STINSTITUTION_RESET_VALUE="";
	public static final String CHILD_STBRAND="stBrand";
	public static final String CHILD_STBRAND_RESET_VALUE="";
	public static final String CHILD_STUSERNAME="stUserName";
	public static final String CHILD_STUSERNAME_RESET_VALUE="";
	public static final String CHILD_STUSERROLE="stUserRole";
	public static final String CHILD_STUSERROLE_RESET_VALUE="";
	public static final String CHILD_STDATE="stDate";
	public static final String CHILD_STDATE_RESET_VALUE="";
	public static final String CHILD_STUSERID="stUserID";
	public static final String CHILD_STUSERID_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	// MigrationToDo : Migrate custom member
	// public static final String US_THE_SESSION="usTheSession";
	// MigrationToDo : Migrate custom member
	// public static final String US_SAVED_PAGES="usSavedPages";
	// MigrationToDo : Migrate custom member
	// public static final String COMBO_BOX_NAME="cbPageNames";
	// MigrationToDo : Migrate custom member
	// public static final String TB_DEAL_ID="tbDealId";

}

