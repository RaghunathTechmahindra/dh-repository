package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doGetActualClosingDateModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfActualClosingDate();


	/**
	 *
	 *
	 */
	public void setDfActualClosingDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public String getDfServicingMortgNum();


	/**
	 *
	 *
	 */
	public void setDfServicingMortgNum(String value);


	/**
	 *
	 *
	 */
	public String getDfBankABANumber();


	/**
	 *
	 *
	 */
	public void setDfBankABANumber(String value);


	/**
	 *
	 *
	 */
	public String getDfBankAccountNumber();


	/**
	 *
	 *
	 */
	public void setDfBankAccountNumber(String value);


	/**
	 *
	 *
	 */
	public String getDfBankName();


	/**
	 *
	 *
	 */
	public void setDfBankName(String value);

	//--> Ticket#127 : Change request for BMO
  //--> Add check box to indicate if the Broker Commission paid or not
  //--> By Billy 25Nov2003
  public String getDfBrokerCommPaid();
  public void setDfBrokerCommPaid(String value);
  //==================================================================


	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFACTUALCLOSINGDATE="dfActualClosingDate";
	public static final String FIELD_DFSERVICINGMORTGNUM="dfServicingMortgNum";
	public static final String FIELD_DFBANKABANUMBER="dfBankABANumber";
	public static final String FIELD_DFBANKACCOUNTNUMBER="dfBankAccountNumber";
	public static final String FIELD_DFBANKNAME="dfBankName";
	//--> Ticket#127 : Change request for BMO
  //--> Add check box to indicate if the Broker Commission paid or not
  //--> By Billy 25Nov2003
  public static final String FIELD_DFBROKERCOMMPAID="dfBrokerCommPaid";
  //==================================================================



	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

