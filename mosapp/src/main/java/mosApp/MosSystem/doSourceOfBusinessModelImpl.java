package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doSourceOfBusinessModelImpl extends QueryModelBase
	implements doSourceOfBusinessModel
{
	/**
	 *
	 *
	 */
	public doSourceOfBusinessModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    ////logger = SysLog.getSysLogger("DOSOBM");
    ////logger.debug("DOSOBM@afterExecute::SQLTemplate: " + this.getSelectSQL());
    ////logger.debug("DOSOBM@afterExecute::Size: " + this.getSize());

    //// To override all the Description fields by populating from BXResource
    //// Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

    int languageId = theSessionState.getLanguageId();

    //// The new partyTypeId field should be created in order to pass proper id
    //// to the BXResources. Translation must be done only if a party already+
    //// assigned to the deal. Otherwise just display page blank.
    while(this.next())
    {
      //// Source Business Category.
      this.setDfSBPCategoryDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
        "SOURCEOFBUSINESSCATEGORY", this.getDfSBPCategoryId().intValue(), languageId));

      //// Profile Status.
      this.setDfSBProfileStatusDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
        "PROFILESTATUS", this.getDfSBPStatusId().intValue(), languageId));

      //// Province
      this.setDfSBPProvinceName(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
        "PROVINCE", this.getDfSBPProvinceName(), languageId));

      //---------------- FXLink Phase II ------------------//
      // get SystemType description
      this.setDfSBPSystemTypeDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
        "SYSTEMTYPE", this.getDfSBPSystemTypeDesc(), languageId));
      //=====================================================

      //========================================================
      //--> Ticket#139 : BMO - Ability to prioritize Broker (SOB)
      //--> Add new field to indicate the Priority of the SOB
      //--> By Billy 28Nov2003
      // get SOB Priority Desc.
      this.setDfSBPPriorityDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
        "SOURCEOFBUSINESSPRIORITY", this.getDfSBPPriorityDesc(), languageId));
      //========================================================
      //--DJ_PREFCOMMETHOD_CR--start--//
      this.setDfPreferredDeliveryMethod(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
        "PREFERREDDELIVERYMETHOD", this.getDfPreferredDeliveryMethod(), languageId));
      //--DJ_PREFCOMMETHOD_CR--end--//
    }

   //Reset Location
    this.beforeFirst();

	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSBPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfSBProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSBPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPShortName()
	{
		return (String)getValue(FIELD_DFSBPSHORTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPShortName(String value)
	{
		setValue(FIELD_DFSBPSHORTNAME,value);
	}

  //--Release2.1--start//
  //// New field to provide the Profile Status translation.
	/**
	 *
	 *
	 */
	public String getDfSBProfileStatusDesc()
	{
		return (String)getValue(FIELD_DFSBPROFILESTATUSDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfSBProfileStatusDesc(String value)
	{
		setValue(FIELD_DFSBPROFILESTATUSDESC,value);
	}
  //--Release2.1--end//

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPStatusId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSBPSTATUSID);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPStatusId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSBPSTATUSID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPCompFactor()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSBPCOMPFACTOR);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPCompFactor(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSBPCOMPFACTOR,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPClientId()
	{
		return (String)getValue(FIELD_DFSBPCLIENTID);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPClientId(String value)
	{
		setValue(FIELD_DFSBPCLIENTID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPAlternativeId()
	{
		return (String)getValue(FIELD_DFSBPALTERNATIVEID);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPAlternativeId(String value)
	{
		setValue(FIELD_DFSBPALTERNATIVEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPServLen()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSBPSERVLEN);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPServLen(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSBPSERVLEN,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPCategoryId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSBPCATEGORYID);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPCategoryId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSBPCATEGORYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPTierLevel()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSBPTIERLEVEL);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPTierLevel(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSBPTIERLEVEL,value);
	}


	/**
	 *
	 *
	 */
  //---------------- FXLink Phase II ------------------//
  //--> Rename DFSBPMAILBOXNUM ==> DFSBPSYSTEMCODE
	public String getDfSBPSystemCode()
	{
		return (String)getValue(FIELD_DFSBPSYSTEMCODE);
	}

	/**
	 *
	 *
	 */
	public void setDfSBPSystemCode(String value)
	{
		setValue(FIELD_DFSBPSYSTEMCODE,value);
	}
  //=====================================================

	/**
	 *
	 *
	 */
	public String getDfSBPFirstName()
	{
		return (String)getValue(FIELD_DFSBPFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPFirstName(String value)
	{
		setValue(FIELD_DFSBPFIRSTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPMidInit()
	{
		return (String)getValue(FIELD_DFSBPMIDINIT);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPMidInit(String value)
	{
		setValue(FIELD_DFSBPMIDINIT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPLastName()
	{
		return (String)getValue(FIELD_DFSBPLASTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPLastName(String value)
	{
		setValue(FIELD_DFSBPLASTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPPhone()
	{
		return (String)getValue(FIELD_DFSBPPHONE);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPPhone(String value)
	{
		setValue(FIELD_DFSBPPHONE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPFax()
	{
		return (String)getValue(FIELD_DFSBPFAX);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPFax(String value)
	{
		setValue(FIELD_DFSBPFAX,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPEmail()
	{
		return (String)getValue(FIELD_DFSBPEMAIL);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPEmail(String value)
	{
		setValue(FIELD_DFSBPEMAIL,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPCity()
	{
		return (String)getValue(FIELD_DFSBPCITY);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPCity(String value)
	{
		setValue(FIELD_DFSBPCITY,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPProvinceId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSBPPROVINCEID);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPProvinceId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSBPPROVINCEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBP_FSA()
	{
		return (String)getValue(FIELD_DFSBP_FSA);
	}


	/**
	 *
	 *
	 */
	public void setDfSBP_FSA(String value)
	{
		setValue(FIELD_DFSBP_FSA,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBP_LDU()
	{
		return (String)getValue(FIELD_DFSBP_LDU);
	}


	/**
	 *
	 *
	 */
	public void setDfSBP_LDU(String value)
	{
		setValue(FIELD_DFSBP_LDU,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPAddrLine1()
	{
		return (String)getValue(FIELD_DFSBPADDRLINE1);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPAddrLine1(String value)
	{
		setValue(FIELD_DFSBPADDRLINE1,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPAddrLine2()
	{
		return (String)getValue(FIELD_DFSBPADDRLINE2);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPAddrLine2(String value)
	{
		setValue(FIELD_DFSBPADDRLINE2,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPFirmId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSBPFIRMID);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPFirmId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSBPFIRMID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPUserId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSBPUSERID);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPUserId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSBPUSERID,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfSBPUserDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFSBPUSERDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPUserDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFSBPUSERDATE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPPhoneExt()
	{
		return (String)getValue(FIELD_DFSBPPHONEEXT);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPPhoneExt(String value)
	{
		setValue(FIELD_DFSBPPHONEEXT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPStatusDesc()
	{
		return (String)getValue(FIELD_DFSBPSTATUSDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPStatusDesc(String value)
	{
		setValue(FIELD_DFSBPSTATUSDESC,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPCategoryDesc()
	{
		return (String)getValue(FIELD_DFSBPCATEGORYDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPCategoryDesc(String value)
	{
		setValue(FIELD_DFSBPCATEGORYDESC,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPProvinceName()
	{
		return (String)getValue(FIELD_DFSBPPROVINCENAME);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPProvinceName(String value)
	{
		setValue(FIELD_DFSBPPROVINCENAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfNotes()
	{
		return (String)getValue(FIELD_DFNOTES);
	}


	/**
	 *
	 *
	 */
	public void setDfNotes(String value)
	{
		setValue(FIELD_DFNOTES,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSOBRegionId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSOBREGIONID);
	}


	/**
	 *
	 *
	 */
	public void setDfSOBRegionId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSOBREGIONID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSOBRegionDesc()
	{
		return (String)getValue(FIELD_DFSOBREGIONDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfSOBRegionDesc(String value)
	{
		setValue(FIELD_DFSOBREGIONDESC,value);
	}

  //=================================================
  //--> CRF#37 :: Source/Firm to Group routing for TD
  //=================================================
  //--> Added new fields to handle Source to Group association
  //--> By Billy 19Aug2003
  public java.math.BigDecimal getDfSBPGroupId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSBPGROUPID);
	}

	public void setDfSBPGroupId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSBPGROUPID,value);
	}

	public java.sql.Timestamp getDfSBPGroupDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFSBPGROUPDATE);
	}

	public void setDfSBPGroupDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFSBPGROUPDATE,value);
	}
  //========================================================

  //---------------- FXLink Phase II ------------------//
  //--> By Billy 06Nov2003
  //--> Add new fields for SystemType
  public java.math.BigDecimal getDfSBPSystemTypeId()
  {
		return (java.math.BigDecimal)getValue(FIELD_DFSBPSYSTEMTYPEID);
	}

	public void setDfSBPSystemTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSBPSYSTEMTYPEID,value);
	}

  public String getDfSBPSystemTypeDesc()
  {
		return (String)getValue(FIELD_DFSBPSYSTEMTYPEDESC);
	}

	public void setDfSBPSystemTypeDesc(String value)
  {
		setValue(FIELD_DFSBPSYSTEMTYPEDESC,value);
	}
  //====================================================

  //========================================================
  //--> Ticket#139 : BMO - Ability to prioritize Broker (SOB)
  //--> Add new field to indicate the Priority of the SOB
  //--> By Billy 28Nov2003
  public java.math.BigDecimal getDfSBPPriorityId()
  {
		return (java.math.BigDecimal)getValue(FIELD_DFSBPPRIORITYID);
	}

	public void setDfSBPPriorityId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSBPPRIORITYID,value);
	}

  public String getDfSBPPriorityDesc()
  {
		return (String)getValue(FIELD_DFSBPPRIORITYDESC);
	}

	public void setDfSBPPriorityDesc(String value)
  {
		setValue(FIELD_DFSBPPRIORITYDESC,value);
	}
  //====================================================
  //--DJ_PREFCOMMETHOD_CR--start--//
  /**
   *
   *
   */
  public String getDfPreferredDeliveryMethod()
  {
    return (String)getValue(FIELD_DFPREFERREDDELIVERYMETHOD);
  }

  /**
   *
   *
   */
  public void setDfPreferredDeliveryMethod(String value)
  {
    setValue(FIELD_DFPREFERREDDELIVERYMETHOD,value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfPreferredDeliveryMethodId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFPREFERREDDELIVERYMETHODID);
  }

  /**
   *
   *
   */
  public void setDfPreferredDeliveryMethodId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFPREFERREDDELIVERYMETHODID,value);
  }
  //--DJ_PREFCOMMETHOD_CR--end--//
  public java.math.BigDecimal getDfInstitutionId() {
      return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
    }

    public void setDfInstitutionId(java.math.BigDecimal value) {
      setValue(FIELD_DFINSTITUTIONID, value);
    }
    
    
    public String getDfLicenseNumber()
    {
  		return (String)getValue(FIELD_DFLICENSENUMBER);
  	}

  	public void setDfLicenseNumber(String value)
    {
  		setValue(FIELD_DFLICENSENUMBER,value);
  	}
  	
	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  public SysLogger logger;

	public static final String DATA_SOURCE_NAME="jdbc/orcl";

  ////"to_char(PROFILESTATUS.PROFILESTATUSID) PROFILESTATUSID_STR, " +

	////public static final String SELECT_SQL_TEMPLATE="SELECT ALL SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPROFILEID, SOURCEOFBUSINESSPROFILE.SOBPSHORTNAME, SOURCEOFBUSINESSPROFILE.PROFILESTATUSID, SOURCEOFBUSINESSPROFILE.COMPENSATIONFACTOR, SOURCEOFBUSINESSPROFILE.SOBPBUSINESSID, SOURCEOFBUSINESSPROFILE.ALTERNATIVEID, SOURCEOFBUSINESSPROFILE.LENGTHOFSERVICE, SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSCATEGORYID, SOURCEOFBUSINESSPROFILE.TIERLEVEL, SOURCEOFBUSINESSMAILBOX.SOURCESYSTEMMAILBOXNBR, CONTACT.CONTACTFIRSTNAME, CONTACT.CONTACTMIDDLEINITIAL, CONTACT.CONTACTLASTNAME, CONTACT.CONTACTPHONENUMBER, CONTACT.CONTACTFAXNUMBER, CONTACT.CONTACTEMAILADDRESS, ADDR.CITY, ADDR.PROVINCEID, ADDR.POSTALFSA, ADDR.POSTALLDU, ADDR.ADDRESSLINE1, ADDR.ADDRESSLINE2, SOURCETOFIRMASSOC.SOURCEFIRMPROFILEID, SOURCETOFIRMASSOC.EFFECTIVEDATE, SOURCETOUSERASSOC.USERPROFILEID, SOURCETOUSERASSOC.EFFECTIVEDATE, CONTACT.CONTACTPHONENUMBEREXTENSION, PROFILESTATUS.PSDESCRIPTION, SOURCEOFBUSINESSCATEGORY.SOBCDESCRIPTION, PROVINCE.PROVINCENAME, SOURCEOFBUSINESSPROFILE.NOTES, SOURCEOFBUSINESSPROFILE.SOBREGIONID, SOBREGION.SOBREGIONDESCRIPTION FROM SOURCEOFBUSINESSPROFILE, CONTACT, SOURCEOFBUSINESSMAILBOX, ADDR, SOURCETOFIRMASSOC, SOURCETOUSERASSOC, PROFILESTATUS, SOURCEOFBUSINESSCATEGORY, PROVINCE, SOBREGION  __WHERE__  ";
	//=================================================
  //--> CRF#37 :: Source/Firm to Group routing for TD
  //=================================================
  //--> Added new fields to handle Source to Group association
  //--> By Billy 19Aug2003
  //---------------- FXLink Phase II ------------------//
  //--> 1) Modified to use SOB.sourceFirmProfileId instead of SOURCETOFIRMASSOC table
  //--> 2) Modified to use SOB.sourceOfBusinessCode instead of SOURCEOFBUSINESSMAILBOX table
  //--> 3) Added new filed : systemTypeId
  //--> By Billy 06Nov2003
  //--> Some fixes : to elimitate soem table joints
  //--> By Billy 07Nov2003
  public static final String SELECT_SQL_TEMPLATE=
  "SELECT ALL SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPROFILEID, " +
  //"PROFILESTATUS.PROFILESTATUSID, " +
  "SOURCEOFBUSINESSPROFILE.SOBPSHORTNAME, SOURCEOFBUSINESSPROFILE.PROFILESTATUSID, " +
  "SOURCEOFBUSINESSPROFILE.COMPENSATIONFACTOR, SOURCEOFBUSINESSPROFILE.SOBPBUSINESSID, " +
  "SOURCEOFBUSINESSPROFILE.ALTERNATIVEID, SOURCEOFBUSINESSPROFILE.LENGTHOFSERVICE, " +
  "SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSCATEGORYID, SOURCEOFBUSINESSPROFILE.TIERLEVEL, " +
  //"SOURCEOFBUSINESSMAILBOX.SOURCESYSTEMMAILBOXNBR, CONTACT.CONTACTFIRSTNAME, " +
  "SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSCODE, CONTACT.CONTACTFIRSTNAME, " +
  //======================================================================================
  "CONTACT.CONTACTMIDDLEINITIAL, CONTACT.CONTACTLASTNAME, CONTACT.CONTACTPHONENUMBER, " +
  "CONTACT.CONTACTFAXNUMBER, CONTACT.CONTACTEMAILADDRESS, ADDR.CITY, " +
  "ADDR.PROVINCEID, ADDR.POSTALFSA, ADDR.POSTALLDU, ADDR.ADDRESSLINE1, " +
  //"ADDR.ADDRESSLINE2, SOURCETOFIRMASSOC.SOURCEFIRMPROFILEID, " +
  "ADDR.ADDRESSLINE2, SOURCEOFBUSINESSPROFILE.SOURCEFIRMPROFILEID, " +
  //======================================================================
  "SOURCETOUSERASSOC.USERPROFILEID, " +
  "SOURCETOUSERASSOC.EFFECTIVEDATE EFFECTIVEDATE_USER, CONTACT.CONTACTPHONENUMBEREXTENSION, " +
  "to_char(SOURCEOFBUSINESSPROFILE.PROFILESTATUSID) PROFILESTATUSID_STR, " +
  "to_char(SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSCATEGORYID) SOURCEOFBUSINESSCATEGORYID_STR, " +
  "to_char(ADDR.PROVINCEID) PROVINCEID_STR, SOURCEOFBUSINESSPROFILE.NOTES, " +
  "SOURCEOFBUSINESSPROFILE.SOBREGIONID, SOBREGION.SOBREGIONDESCRIPTION, " +
  "SOURCETOGROUPASSOC.GROUPPROFILEID, SOURCETOGROUPASSOC.EFFECTIVEDATE EFFECTIVEDATE_GROUP, " +
  //======================================================================
  "SOURCEOFBUSINESSPROFILE.SYSTEMTYPEID, to_char(SOURCEOFBUSINESSPROFILE.SYSTEMTYPEID) SYSTEMTYPEID_STR, " +
  //======================================================================
  //========================================================
  //--> Ticket#139 : BMO - Ability to prioritize Broker (SOB)
  //--> Add new field to indicate the Priority of the SOB
  //--> By Billy 28Nov2003
  "SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPRIORITYID, to_char(SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPRIORITYID) SOURCEOFBUSINESSPRIORITYID_STR, " +
  "SOURCEOFBUSINESSPROFILE.SOBLICENSEREGISTRATIONNUMBER, " +
  //======================================================================
  //--DJ_PREFCOMMETHOD_CR--start--//
  "to_char(PREFERREDDELIVERYMETHOD.PREFERREDDELIVERYMETHODID) PREFERREDDELIVERYMETHODID_STR, " +
  "CONTACT.PREFERREDDELIVERYMETHODID,SOURCEOFBUSINESSPROFILE.INSTITUTIONPROFILEID " +
  //--DJ_PREFCOMMETHOD_CR--end--//
  "FROM SOURCEOFBUSINESSPROFILE, CONTACT, ADDR, SOURCETOUSERASSOC, " +
  "SOBREGION, SOURCETOGROUPASSOC, " +
  //--DJ_PREFCOMMETHOD_CR--start--//
  "PREFERREDDELIVERYMETHOD " +
  //--DJ_PREFCOMMETHOD_CR--end--//
  " __WHERE__  ";

	////public static final String MODIFYING_QUERY_TABLE_NAME="SOURCEOFBUSINESSPROFILE, CONTACT, SOURCEOFBUSINESSMAILBOX, ADDR, SOURCETOFIRMASSOC, SOURCETOUSERASSOC, PROFILESTATUS, SOURCEOFBUSINESSCATEGORY, PROVINCE, SOBREGION";
	//---------------- FXLink Phase II ------------------//
  //--> 1) Modified to use SOB.sourceFirmProfileId instead of SOURCETOFIRMASSOC table
  //--> 2) Modified to use SOB.sourceOfBusinessCode instead of SOURCEOFBUSINESSMAILBOX table
  //--> 3) Added new filed : systemTypeId
  //--> By Billy 06Nov2003
  public static final String MODIFYING_QUERY_TABLE_NAME=
  "SOURCEOFBUSINESSPROFILE, CONTACT, " +
  "ADDR, SOURCETOUSERASSOC, " +
  "SOBREGION, SOURCETOGROUPASSOC, " +
  //=========================================================================================
  //--DJ_PREFCOMMETHOD_CR--start--//
  "PREFERREDDELIVERYMETHOD ";
  //--DJ_PREFCOMMETHOD_CR--end--//

	////public static final String STATIC_WHERE_CRITERIA=" ((SOURCEOFBUSINESSPROFILE.CONTACTID  =  CONTACT.CONTACTID (+) AND SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPROFILEID  =  SOURCEOFBUSINESSMAILBOX.SOURCEOFBUSINESSPROFILEID(+) AND CONTACT.ADDRID =  ADDR.ADDRID (+) AND ADDR.PROVINCEID  =  PROVINCE.PROVINCEID (+) AND SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPROFILEID  =  SOURCETOUSERASSOC.SOURCEOFBUSINESSPROFILEID(+) AND SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPROFILEID  =  SOURCETOFIRMASSOC.SOURCEOFBUSINESSPROFILEID(+) AND SOURCEOFBUSINESSPROFILE.PROFILESTATUSID  =  PROFILESTATUS.PROFILESTATUSID (+) AND SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSCATEGORYID  =  SOURCEOFBUSINESSCATEGORY.SOURCEOFBUSINESSCATEGORYID (+) AND SOURCEOFBUSINESSPROFILE.SOBREGIONID = SOBREGION.SOBREGIONID (+)) AND SOURCETOFIRMASSOC.STATUS (+) = 'A')";
	//---------------- FXLink Phase II ------------------//
  //--> 1) Modified to use SOB.sourceFirmProfileId instead of SOURCETOFIRMASSOC table
  //--> 2) Modified to use SOB.sourceOfBusinessCode instead of SOURCEOFBUSINESSMAILBOX table
  //--> 3) Added new filed : systemTypeId
  //--> By Billy 06Nov2003
  public static final String STATIC_WHERE_CRITERIA=
  " SOURCEOFBUSINESSPROFILE.CONTACTID  =  CONTACT.CONTACTID (+) " +
  " AND SOURCEOFBUSINESSPROFILE.INSTITUTIONPROFILEID  =  CONTACT.INSTITUTIONPROFILEID (+) " +
  "AND CONTACT.ADDRID =  ADDR.ADDRID (+) " +
  "AND CONTACT.INSTITUTIONPROFILEID =  ADDR.INSTITUTIONPROFILEID (+) " +
  "AND SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPROFILEID  =  SOURCETOUSERASSOC.SOURCEOFBUSINESSPROFILEID(+) " +
  "AND SOURCEOFBUSINESSPROFILE.INSTITUTIONPROFILEID  =  SOURCETOUSERASSOC.INSTITUTIONPROFILEID(+) " +
  "AND SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPROFILEID  =  SOURCETOGROUPASSOC.SOURCEOFBUSINESSPROFILEID(+) " +
  "AND SOURCEOFBUSINESSPROFILE.INSTITUTIONPROFILEID  =  SOURCETOGROUPASSOC.INSTITUTIONPROFILEID(+) " +
  "AND SOURCEOFBUSINESSPROFILE.SOBREGIONID = SOBREGION.SOBREGIONID (+) " +
  "AND SOURCEOFBUSINESSPROFILE.INSTITUTIONPROFILEID = SOBREGION.INSTITUTIONPROFILEID (+) " +
  //--DJ_PREFCOMMETHOD_CR--start--//
  "AND (CONTACT.PREFERREDDELIVERYMETHODID  =  PREFERREDDELIVERYMETHOD.PREFERREDDELIVERYMETHODID) ";
  //--DJ_PREFCOMMETHOD_CR--end--//

	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFSBPROFILEID="SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPROFILEID";
	public static final String COLUMN_DFSBPROFILEID="SOURCEOFBUSINESSPROFILEID";
	public static final String QUALIFIED_COLUMN_DFSBPSHORTNAME="SOURCEOFBUSINESSPROFILE.SOBPSHORTNAME";
	public static final String COLUMN_DFSBPSHORTNAME="SOBPSHORTNAME";
	public static final String QUALIFIED_COLUMN_DFSBPSTATUSID="SOURCEOFBUSINESSPROFILE.PROFILESTATUSID";
	public static final String COLUMN_DFSBPSTATUSID="PROFILESTATUSID";
	public static final String QUALIFIED_COLUMN_DFSBPCOMPFACTOR="SOURCEOFBUSINESSPROFILE.COMPENSATIONFACTOR";
	public static final String COLUMN_DFSBPCOMPFACTOR="COMPENSATIONFACTOR";
	public static final String QUALIFIED_COLUMN_DFSBPCLIENTID="SOURCEOFBUSINESSPROFILE.SOBPBUSINESSID";
	public static final String COLUMN_DFSBPCLIENTID="SOBPBUSINESSID";
	public static final String QUALIFIED_COLUMN_DFSBPALTERNATIVEID="SOURCEOFBUSINESSPROFILE.ALTERNATIVEID";
	public static final String COLUMN_DFSBPALTERNATIVEID="ALTERNATIVEID";
	public static final String QUALIFIED_COLUMN_DFSBPSERVLEN="SOURCEOFBUSINESSPROFILE.LENGTHOFSERVICE";
	public static final String COLUMN_DFSBPSERVLEN="LENGTHOFSERVICE";
	public static final String QUALIFIED_COLUMN_DFSBPCATEGORYID="SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSCATEGORYID";
	public static final String COLUMN_DFSBPCATEGORYID="SOURCEOFBUSINESSCATEGORYID";
	public static final String QUALIFIED_COLUMN_DFSBPTIERLEVEL="SOURCEOFBUSINESSPROFILE.TIERLEVEL";
	public static final String COLUMN_DFSBPTIERLEVEL="TIERLEVEL";
	public static final String QUALIFIED_COLUMN_DFSBPFIRSTNAME="CONTACT.CONTACTFIRSTNAME";
	public static final String COLUMN_DFSBPFIRSTNAME="CONTACTFIRSTNAME";
	public static final String QUALIFIED_COLUMN_DFSBPMIDINIT="CONTACT.CONTACTMIDDLEINITIAL";
	public static final String COLUMN_DFSBPMIDINIT="CONTACTMIDDLEINITIAL";
	public static final String QUALIFIED_COLUMN_DFSBPLASTNAME="CONTACT.CONTACTLASTNAME";
	public static final String COLUMN_DFSBPLASTNAME="CONTACTLASTNAME";
	public static final String QUALIFIED_COLUMN_DFSBPPHONE="CONTACT.CONTACTPHONENUMBER";
	public static final String COLUMN_DFSBPPHONE="CONTACTPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFSBPFAX="CONTACT.CONTACTFAXNUMBER";
	public static final String COLUMN_DFSBPFAX="CONTACTFAXNUMBER";
	public static final String QUALIFIED_COLUMN_DFSBPEMAIL="CONTACT.CONTACTEMAILADDRESS";
	public static final String COLUMN_DFSBPEMAIL="CONTACTEMAILADDRESS";
	public static final String QUALIFIED_COLUMN_DFSBPCITY="ADDR.CITY";
	public static final String COLUMN_DFSBPCITY="CITY";
	public static final String QUALIFIED_COLUMN_DFSBPPROVINCEID="ADDR.PROVINCEID";
	public static final String COLUMN_DFSBPPROVINCEID="PROVINCEID";
	public static final String QUALIFIED_COLUMN_DFSBP_FSA="ADDR.POSTALFSA";
	public static final String COLUMN_DFSBP_FSA="POSTALFSA";
	public static final String QUALIFIED_COLUMN_DFSBP_LDU="ADDR.POSTALLDU";
	public static final String COLUMN_DFSBP_LDU="POSTALLDU";
	public static final String QUALIFIED_COLUMN_DFSBPADDRLINE1="ADDR.ADDRESSLINE1";
	public static final String COLUMN_DFSBPADDRLINE1="ADDRESSLINE1";
	public static final String QUALIFIED_COLUMN_DFSBPADDRLINE2="ADDR.ADDRESSLINE2";
	public static final String COLUMN_DFSBPADDRLINE2="ADDRESSLINE2";
	public static final String QUALIFIED_COLUMN_DFSBPUSERID="SOURCETOUSERASSOC.USERPROFILEID";
	public static final String COLUMN_DFSBPUSERID="USERPROFILEID";
	public static final String QUALIFIED_COLUMN_DFSBPUSERDATE="SOURCETOUSERASSOC.EFFECTIVEDATE";
	public static final String COLUMN_DFSBPUSERDATE="EFFECTIVEDATE_USER";
	public static final String QUALIFIED_COLUMN_DFSBPPHONEEXT="CONTACT.CONTACTPHONENUMBEREXTENSION";
	public static final String COLUMN_DFSBPPHONEEXT="CONTACTPHONENUMBEREXTENSION";

  //--Release2.1--//
	////public static final String QUALIFIED_COLUMN_DFSBPSTATUSDESC="PROFILESTATUS.PSDESCRIPTION";
	////public static final String COLUMN_DFSBPSTATUSDESC="PSDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFSBPSTATUSDESC="SOURCEOFBUSINESSCATEGORY.PROFILESTATUSID_STR";
	public static final String COLUMN_DFSBPSTATUSDESC="PROFILESTATUSID_STR";

	public static final String QUALIFIED_COLUMN_DFSBPCATEGORYDESC="SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSCATEGORYID_STR";
	public static final String COLUMN_DFSBPCATEGORYDESC="SOURCEOFBUSINESSCATEGORYID_STR";

  //--Release2.1--//
	public static final String QUALIFIED_COLUMN_DFSBPPROVINCENAME="ADDR.PROVINCEID_STR";
	public static final String COLUMN_DFSBPPROVINCENAME="PROVINCEID_STR";

	public static final String QUALIFIED_COLUMN_DFNOTES="SOURCEOFBUSINESSPROFILE.NOTES";
	public static final String COLUMN_DFNOTES="NOTES";
	public static final String QUALIFIED_COLUMN_DFSOBREGIONID="SOURCEOFBUSINESSPROFILE.SOBREGIONID";
	public static final String COLUMN_DFSOBREGIONID="SOBREGIONID";
	public static final String QUALIFIED_COLUMN_DFSOBREGIONDESC="SOBREGION.SOBREGIONDESCRIPTION";
	public static final String COLUMN_DFSOBREGIONDESC="SOBREGIONDESCRIPTION";

  //--Release2.1--//
	public static final String QUALIFIED_COLUMN_DFSBPROFILESTATUSDESC="SOURCEOFBUSINESSCATEGORY.PROFILESTATUSID_STR";
	public static final String COLUMN_DFSBPROFILESTATUSDESC="PROFILESTATUSID_STR";

  //=================================================
  //--> CRF#37 :: Source/Firm to Group routing for TD
  //=================================================
  //--> Added new fields to handle Source to Group association
  //--> By Billy 19Aug2003
  public static final String QUALIFIED_COLUMN_DFSBPGROUPID="SOURCETOGROUPASSOC.GROUPPROFILEID";
	public static final String COLUMN_DFSBPGROUPID="GROUPPROFILEID";
	public static final String QUALIFIED_COLUMN_DFSBPGROUPDATE="SOURCETOGROUPASSOC.EFFECTIVEDATE";
	public static final String COLUMN_DFSBPGROUPDATE="EFFECTIVEDATE_GROUP";
  //=================================================

  //---------------- FXLink Phase II ------------------//
  //--> 1) Modified to use SOB.sourceFirmProfileId instead of SOURCETOFIRMASSOC table
  //--> 2) Modified to use SOB.sourceOfBusinessCode instead of SOURCEOFBUSINESSMAILBOX table
  //--> 3) Added new filed : systemTypeId
  //--> By Billy 06Nov2003
  //--> Rename DFSBPMAILBOXNUM ==> DFSBPSYSTEMCODE
  //public static final String QUALIFIED_COLUMN_DFSBPMAILBOXNUM="SOURCEOFBUSINESSMAILBOX.SOURCESYSTEMMAILBOXNBR";
	//public static final String COLUMN_DFSBPMAILBOXNUM="SOURCESYSTEMMAILBOXNBR";
  public static final String QUALIFIED_COLUMN_DFSBPSYSTEMCODE="SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSCODE";
	public static final String COLUMN_DFSBPSYSTEMCODE="SOURCEOFBUSINESSCODE";

  //--> Modified to use SOB.sourceFirmProfileId instead of SOURCETOFIRMASSOC table
  public static final String QUALIFIED_COLUMN_DFSBPFIRMID="SOURCEOFBUSINESSPROFILE.SOURCEFIRMPROFILEID";
	public static final String COLUMN_DFSBPFIRMID="SOURCEFIRMPROFILEID";
  //--> Removed Effective Date
	//public static final String QUALIFIED_COLUMN_DFSBPFIRMDATE="SOURCETOFIRMASSOC.EFFECTIVEDATE";
	//public static final String COLUMN_DFSBPFIRMDATE="EFFECTIVEDATE_FIRM";

  //--> Add new fields for SystemType
  public static final String QUALIFIED_COLUMN_DFSBPSYSTEMTYPEID="SOURCEOFBUSINESSPROFILE.SYSTEMTYPEID";
	public static final String COLUMN_DFSBPSYSTEMTYPEID="SYSTEMTYPEID";
  public static final String QUALIFIED_COLUMN_DFSBPSYSTEMTYPEDESC="SOURCEOFBUSINESSPROFILE.SYSTEMTYPEID_STR";
	public static final String COLUMN_DFSBPSYSTEMTYPEDESC="SYSTEMTYPEID_STR";
  //========================================================

  //========================================================
  //--> Ticket#139 : BMO - Ability to prioritize Broker (SOB)
  //--> Add new field to indicate the Priority of the SOB
  //--> By Billy 28Nov2003
  public static final String QUALIFIED_COLUMN_DFSBPPPRIORITYID="SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPRIORITYID";
	public static final String COLUMN_DFSBPPPRIORITYID="SOURCEOFBUSINESSPRIORITYID";
  public static final String QUALIFIED_COLUMN_DFSBPPPRIORITYDESC="SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPRIORITYID_STR";
	public static final String COLUMN_DFSBPPPRIORITYDESC="SOURCEOFBUSINESSPRIORITYID_STR";
  //========================================================

  //--DJ_PREFCOMMETHOD_CR--start--//
	public static final String QUALIFIED_COLUMN_DFPREFERREDDELIVERYMETHOD="PREFERREDDELIVERYMETHOD.PREFERREDDELIVERYMETHODID_STR";
	public static final String COLUMN_DFPREFERREDDELIVERYMETHOD="PREFERREDDELIVERYMETHODID_STR";

  public static final String QUALIFIED_COLUMN_DFPREFERREDDELIVERYMETHODID="CONTACT.PREFERREDDELIVERYMETHODID";
	public static final String COLUMN_DFPREFERREDDELIVERYMETHODID="PREFERREDDELIVERYMETHODID";
  //--DJ_PREFCOMMETHOD_CR--end--//
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "SOURCEOFBUSINESSPROFILE.INSTITUTIONPROFILEID";
  public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
  
    public static final String QUALIFIED_COLUMN_DFLICENSENUMBER="SOURCEOFBUSINESSPROFILE.SOBLICENSEREGISTRATIONNUMBER";
	public static final String COLUMN_DFLICENSENUMBER="SOBLICENSEREGISTRATIONNUMBER";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPROFILEID,
				COLUMN_DFSBPROFILEID,
				QUALIFIED_COLUMN_DFSBPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPSHORTNAME,
				COLUMN_DFSBPSHORTNAME,
				QUALIFIED_COLUMN_DFSBPSHORTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPSTATUSID,
				COLUMN_DFSBPSTATUSID,
				QUALIFIED_COLUMN_DFSBPSTATUSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPCOMPFACTOR,
				COLUMN_DFSBPCOMPFACTOR,
				QUALIFIED_COLUMN_DFSBPCOMPFACTOR,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPCLIENTID,
				COLUMN_DFSBPCLIENTID,
				QUALIFIED_COLUMN_DFSBPCLIENTID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPALTERNATIVEID,
				COLUMN_DFSBPALTERNATIVEID,
				QUALIFIED_COLUMN_DFSBPALTERNATIVEID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPSERVLEN,
				COLUMN_DFSBPSERVLEN,
				QUALIFIED_COLUMN_DFSBPSERVLEN,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPCATEGORYID,
				COLUMN_DFSBPCATEGORYID,
				QUALIFIED_COLUMN_DFSBPCATEGORYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPTIERLEVEL,
				COLUMN_DFSBPTIERLEVEL,
				QUALIFIED_COLUMN_DFSBPTIERLEVEL,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    //---------------- FXLink Phase II ------------------//
    //--> Rename DFSBPMAILBOXNUM ==> DFSBPSYSTEMCODE
    //--> By Billy 07Nov2003
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPSYSTEMCODE,
				COLUMN_DFSBPSYSTEMCODE,
				QUALIFIED_COLUMN_DFSBPSYSTEMCODE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    //=====================================================

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPFIRSTNAME,
				COLUMN_DFSBPFIRSTNAME,
				QUALIFIED_COLUMN_DFSBPFIRSTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPMIDINIT,
				COLUMN_DFSBPMIDINIT,
				QUALIFIED_COLUMN_DFSBPMIDINIT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPLASTNAME,
				COLUMN_DFSBPLASTNAME,
				QUALIFIED_COLUMN_DFSBPLASTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPPHONE,
				COLUMN_DFSBPPHONE,
				QUALIFIED_COLUMN_DFSBPPHONE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPFAX,
				COLUMN_DFSBPFAX,
				QUALIFIED_COLUMN_DFSBPFAX,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPEMAIL,
				COLUMN_DFSBPEMAIL,
				QUALIFIED_COLUMN_DFSBPEMAIL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPCITY,
				COLUMN_DFSBPCITY,
				QUALIFIED_COLUMN_DFSBPCITY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPPROVINCEID,
				COLUMN_DFSBPPROVINCEID,
				QUALIFIED_COLUMN_DFSBPPROVINCEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBP_FSA,
				COLUMN_DFSBP_FSA,
				QUALIFIED_COLUMN_DFSBP_FSA,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBP_LDU,
				COLUMN_DFSBP_LDU,
				QUALIFIED_COLUMN_DFSBP_LDU,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPADDRLINE1,
				COLUMN_DFSBPADDRLINE1,
				QUALIFIED_COLUMN_DFSBPADDRLINE1,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPADDRLINE2,
				COLUMN_DFSBPADDRLINE2,
				QUALIFIED_COLUMN_DFSBPADDRLINE2,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPFIRMID,
				COLUMN_DFSBPFIRMID,
				QUALIFIED_COLUMN_DFSBPFIRMID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    //---------------- FXLink Phase II ------------------//
    //--> Removed Effective Date
    //--> By Billy 07Nov2003
    /*
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPFIRMDATE,
				COLUMN_DFSBPFIRMDATE,
				QUALIFIED_COLUMN_DFSBPFIRMDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    */

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPUSERID,
				COLUMN_DFSBPUSERID,
				QUALIFIED_COLUMN_DFSBPUSERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPUSERDATE,
				COLUMN_DFSBPUSERDATE,
				QUALIFIED_COLUMN_DFSBPUSERDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPPHONEEXT,
				COLUMN_DFSBPPHONEEXT,
				QUALIFIED_COLUMN_DFSBPPHONEEXT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPSTATUSDESC,
				COLUMN_DFSBPSTATUSDESC,
				QUALIFIED_COLUMN_DFSBPSTATUSDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPCATEGORYDESC,
				COLUMN_DFSBPCATEGORYDESC,
				QUALIFIED_COLUMN_DFSBPCATEGORYDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPPROVINCENAME,
				COLUMN_DFSBPPROVINCENAME,
				QUALIFIED_COLUMN_DFSBPPROVINCENAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNOTES,
				COLUMN_DFNOTES,
				QUALIFIED_COLUMN_DFNOTES,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOBREGIONID,
				COLUMN_DFSOBREGIONID,
				QUALIFIED_COLUMN_DFSOBREGIONID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOBREGIONDESC,
				COLUMN_DFSOBREGIONDESC,
				QUALIFIED_COLUMN_DFSOBREGIONDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

     //--Release2.1--//
     //// New field to translate Profile Status Description.
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPROFILESTATUSDESC,
				COLUMN_DFSBPROFILESTATUSDESC,
				QUALIFIED_COLUMN_DFSBPROFILESTATUSDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    //=================================================
    //--> CRF#37 :: Source/Firm to Group routing for TD
    //=================================================
    //--> Added new fields to handle Source to Group association
    //--> By Billy 19Aug2003
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPGROUPID,
				COLUMN_DFSBPGROUPID,
				QUALIFIED_COLUMN_DFSBPGROUPID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPGROUPDATE,
				COLUMN_DFSBPGROUPDATE,
				QUALIFIED_COLUMN_DFSBPGROUPDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    //===========================================================

    //---------------- FXLink Phase II ------------------//
    //--> Add new fields for SystemType
    //--> By Billy 07Nov2003
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPSYSTEMTYPEID,
				COLUMN_DFSBPSYSTEMTYPEID,
				QUALIFIED_COLUMN_DFSBPSYSTEMTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPSYSTEMTYPEDESC,
				COLUMN_DFSBPSYSTEMTYPEDESC,
				QUALIFIED_COLUMN_DFSBPSYSTEMTYPEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLICENSENUMBER,
				COLUMN_DFLICENSENUMBER,
				QUALIFIED_COLUMN_DFLICENSENUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    
    //=======================================================
    //--> Ticket#139 : BMO - Ability to prioritize Broker (SOB)
    //--> Add new field to indicate the Priority of the SOB
    //--> By Billy 28Nov2003
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPPRIORITYID,
				COLUMN_DFSBPPPRIORITYID,
				QUALIFIED_COLUMN_DFSBPPPRIORITYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPPRIORITYDESC,
				COLUMN_DFSBPPPRIORITYDESC,
				QUALIFIED_COLUMN_DFSBPPPRIORITYDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    //=======================================================
    //--DJ_PREFCOMMETHOD_CR--start--//
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPREFERREDDELIVERYMETHOD,
				COLUMN_DFPREFERREDDELIVERYMETHOD,
				QUALIFIED_COLUMN_DFPREFERREDDELIVERYMETHOD,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPREFERREDDELIVERYMETHODID,
				COLUMN_DFPREFERREDDELIVERYMETHODID,
				QUALIFIED_COLUMN_DFPREFERREDDELIVERYMETHODID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    //--DJ_PREFCOMMETHOD_CR--end--//
        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                  FIELD_DFINSTITUTIONID,
                  COLUMN_DFINSTITUTIONID,
                  QUALIFIED_COLUMN_DFINSTITUTIONID,
                  java.math.BigDecimal.class,
                  false,
                  false,
                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                  "",
                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                  ""));
	}
}

