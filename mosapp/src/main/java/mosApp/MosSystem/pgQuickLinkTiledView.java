package mosApp.MosSystem;

import java.util.ArrayList;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.DefaultModel;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;


import com.iplanet.jato.view.html.Button;

import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

/**
 * <p>Title: pgQuickLinkTiledView</p>
 * <p>Description: TiledView Class for repeated quick link section on Quick Link Screen</p>
 *
 * @author 
 * @version 1.0 
 *
 * 
 */
public class pgQuickLinkTiledView extends RequestHandlingTiledViewBase
implements TiledView, RequestHandler {

	// The logger
	private final static Logger logger = LoggerFactory.getLogger(pgQuickLinkTiledView.class);
	
	/**
	 * <p>constructor</p>
	 */
	public pgQuickLinkTiledView(View parent, String name) {
		super(parent, name);
		setMaxDisplayTiles(pgQuickLinksViewBean.MAXCOMPTILE_SIZE);
		setPrimaryModelClass(doQuickLinkModel.class);
		registerChildren();
		initialize();
	}

	/**
	 * <p>initializer</p>
	 */
	protected void initialize() {
	}

	/**
	 * <p>createChild</p>
	 * <p>Description: createChild method for Jato Framework</p>
	 * 
	 * @param name:String - child name
	 * @return View 
	 */
	protected View createChild(String name) {

		if (name.equals(CHILD_HDQID)) {
			HiddenField child 
			= new HiddenField(this,
					getdoQuickLinkModel(),
					CHILD_HDQID,
					doQuickLinkModel.FIELD_DFQUICKLINKID,
					CHILD_HDQID_RESET_VALUE,
					null);
			return child;
		} else if (name.equals(CHILD_HDSORTID)) {
			HiddenField child 
			= new HiddenField(this,
					getdoQuickLinkModel(),
					CHILD_HDSORTID,
					doQuickLinkModel.FIELD_DFSORTORDER,
					CHILD_HDSORTID_RESET_VALUE,
					null);
			return child;  
		}  else if (name.equals(CHILD_STQUICKLINKLIST)) {
			StaticTextField child 
			= new StaticTextField(this,
					new DefaultModel(),
					CHILD_STQUICKLINKLIST,
					CHILD_STQUICKLINKLIST,
					CHILD_STQUICKLINKLIST_RESET_VALUE,
					null);
			return child;
		} else if (name.equals(CHILD_TBDESCRIPTIONENGLISH)) {
			TextField child 
			= new TextField(this,
					getdoQuickLinkModel(),
					CHILD_TBDESCRIPTIONENGLISH,
					doQuickLinkModel.FIELD_DFLINKNAMEENGLISH,
					CHILD_TBDESCRIPTIONENGLISH_RESET_VALUE,
					null);
			return child;
		} else if (name.equals(CHILD_TBDESCRIPTIONFRENCH)){
			TextField child 
			= new TextField(this,
					getdoQuickLinkModel(),
					CHILD_TBDESCRIPTIONFRENCH,
					doQuickLinkModel.FIELD_DFLINKNAMEFRENCH,
					CHILD_TBDESCRIPTIONFRENCH_RESET_VALUE,
					null);            
			return child;
		} else if (name.equals(CHILD_TBURLADDRESS)) {
			TextField child =
				new TextField(this, getdoQuickLinkModel(),
						CHILD_TBURLADDRESS,
						doQuickLinkModel.FIELD_URLLINK,
						CHILD_TBURLADDRESS_RESET_VALUE, null);
			return child;
		}  else if (name.equals(CHILD_BTADD)) {
			Button child = new Button(
					this, getDefaultModel(), CHILD_BTADD,
					CHILD_BTADD, CHILD_BTADD_RESET_VALUE,
					null);
			return child;
		} else if (name.equals(CHILD_BTTESTLINK)) {
			Button child = new Button(
					this, getDefaultModel(), CHILD_BTTESTLINK,
					CHILD_BTTESTLINK, CHILD_BTDTESTLINK_RESET_VALUE,
					null);
			return child;
		}  else if (name.equals(CHILD_BTCLEAR)) {
			Button child = new Button(
					this, getDefaultModel(), CHILD_BTCLEAR,
					CHILD_BTCLEAR, CHILD_BTCLEAR_RESET_VALUE,
					null);
			return child;
		} else if (name.equals(CHILD_BTUP)) {
			Button child 
			= new Button(this,
					getDefaultModel(),
					CHILD_BTUP,
					CHILD_BTUP,
					CHILD_BTUP,
					null);
			return child;
		} else if (name.equals(CHILD_BTDOWN)) {
			Button child 
			= new Button(this,
					getDefaultModel(),
					CHILD_BTDOWN,
					CHILD_BTDOWN,
					CHILD_BTDOWN,
					null);
			return child;
		}
		else throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}

	/**
	 * <p>
	 * registerChildren
	 * </p>
	 * <p>
	 * Description: registerChildren method for Jato Framework
	 * </p>
	 * 
	 * @param
	 * @return
	 */
	protected void registerChildren() {

		registerChild(CHILD_HDQID, HiddenField.class);
		registerChild(CHILD_HDSORTID, HiddenField.class);
		registerChild(CHILD_TBDESCRIPTIONENGLISH, TextField.class);
		registerChild(CHILD_STQUICKLINKLIST, StaticTextField.class);
		registerChild(CHILD_TBDESCRIPTIONFRENCH, TextField.class);
		registerChild(CHILD_TBURLADDRESS, TextField.class);
		registerChild(CHILD_BTADD, Button.class);
		registerChild(CHILD_BTTESTLINK, Button.class);
		registerChild(CHILD_BTCLEAR, Button.class);
		registerChild(CHILD_BTUP, Button.class);
		registerChild(CHILD_BTDOWN, Button.class);

	}

	/**
	 * <p>resetChildren</p>
	 * <p>Description: resetChildren method for Jato Framework</p>
	 * 
	 * @param 
	 * @return
	 */
	public void resetChildren() {

		super.resetChildren();

		getHdQId().setValue(CHILD_HDQID_RESET_VALUE);
		getHdSortId().setValue(CHILD_HDSORTID_RESET_VALUE);
		getTbDescriptionEnglish().setValue(CHILD_TBDESCRIPTIONENGLISH_RESET_VALUE);
		getStQuickLinkList().setValue(CHILD_STQUICKLINKLIST_RESET_VALUE);
		getTbDescriptionFrench().setValue(CHILD_TBDESCRIPTIONFRENCH_RESET_VALUE);
		getTbUrlAddress().setValue(CHILD_TBDESCRIPTIONFRENCH_RESET_VALUE);
		getBtAdd().setValue(CHILD_BTADD_RESET_VALUE);
		getBtTestLink().setValue(CHILD_BTDTESTLINK_RESET_VALUE);
		getBtClear().setValue(CHILD_BTCLEAR_RESET_VALUE);
		getBtUp().setValue(CHILD_BTUP_RESET_VALUE);
		getBtDown().setValue(CHILD_BTDOWN_RESET_VALUE);
	}

	/**
	 * <p>
	 * nextTile
	 * </p>
	 * <p>
	 * Description: display next mortgage section tiled view
	 * </p>
	 */
	public boolean nextTile() throws ModelControlException {

		boolean movedToRow = super.nextTile();
		
		if (movedToRow) {
			QuickLinksHandler handler = (QuickLinksHandler) this.handler
					.cloneSS();
			handler.pageGetState(this.getParentViewBean());
			handler.setQuickLinkDisplayFields(getTileIndex(), true);
			handler.pageSaveState();
		} else {

			handler.pageGetState(this.getParentViewBean());
			doQuickLinkModelImpl theObj = (doQuickLinkModelImpl) getdoQuickLinkModel();

			if (theObj.getNumRows() < pgQuickLinksViewBean.MAXCOMPTILE_SIZE) {				
				super.setTileIndex(super.getTileIndex() + 1);
				handler.setQuickLinkDisplayFields(getTileIndex(), false);
				handler.pageSaveState();
				return true;
			}
		}
		return movedToRow;
	}

	
	
	
	public HiddenField getHdQId() {
		return (HiddenField) getChild(CHILD_HDQID);
	}

	public HiddenField getHdSortId() {
		return (HiddenField) getChild(CHILD_HDSORTID);
	}

	public StaticTextField getStQuickLinkList() {
		return (StaticTextField) getChild(CHILD_STQUICKLINKLIST);
	}	
	
	public StaticTextField getTbDescriptionEnglish() {
		return (StaticTextField) getChild(CHILD_TBDESCRIPTIONENGLISH);
	}

	public TextField getTbDescriptionFrench() {
		return (TextField) getChild(CHILD_TBDESCRIPTIONFRENCH);
	}

	public TextField getTbUrlAddress() {
		return (TextField) getChild(CHILD_TBURLADDRESS);
	}

	public Button getBtAdd() {
		return (Button) getChild(CHILD_BTADD);
	}

	public Button getBtTestLink() {
		return (Button) getChild(CHILD_BTTESTLINK);
	}

	public Button getBtClear() {
		return (Button) getChild(CHILD_BTCLEAR);
	}

	public Button getBtUp() {
		return (Button) getChild(CHILD_BTUP);
	}

	public Button getBtDown() {
		return (Button) getChild(CHILD_BTDOWN);
	}
    

	

	/**
	 * <p>getWebActionModels</p>
	 * <p>Description: get Auto executed models</p>
	 *
	 * @param int: executionType
	 * @return Model[]
	 */
	public Model[] getWebActionModels(int executionType) {
		List modelList = new ArrayList();
		switch (executionType) {
		case MODEL_TYPE_RETRIEVE:
			;
			break;

		case MODEL_TYPE_UPDATE:
			;
			break;

		case MODEL_TYPE_DELETE:
			;
			break;

		case MODEL_TYPE_INSERT:
			;
			break;

		case MODEL_TYPE_EXECUTE:
			;
			break;
		}
		return (Model[]) modelList.toArray(new Model[0]);
	}

	/**
	 * <p>beginDisplay</p>
	 * <p>Description: beginDisplay method for Jato Framework</p>
	 * 
	 * @param event: DisplayEvent
	 * @return
	 */
	public void beginDisplay(DisplayEvent event) throws ModelControlException {

		QuickLinksHandler handler 
		= (QuickLinksHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

		if (getPrimaryModel() == null) {
			throw new ModelControlException("Primary model is null");
		}

		super.beginDisplay(event);
		resetTileIndex();
	}

	/**
	 * <p>beforeModelExecutes</p>
	 * <p>Description: beforeModelExecutes method for Jato Framework</p>
	 * 
	 * @param model: Model
	 * @param executionContext: int
	 * @return boolean
	 */
	public boolean beforeModelExecutes(Model model, int executionContext) {
		return super.beforeModelExecutes(model, executionContext);
	}

	/**
	 * <p>afterModelExecutes</p>
	 * <p>Description: beforeModelExecutes method for Jato Framework</p>
	 * 
	 * @param model: Model
	 * @param executionContext: int
	 * @return
	 */
	public void afterModelExecutes(Model model, int executionContext) {
		super.afterModelExecutes(model, executionContext);
	}

	/**
	 * <p>afterModelExecutes</p>
	 * <p>Description: afterModelExecutes method for Jato Framework</p>
	 * 
	 * @param executionContext: int
	 * @return
	 */
	public void afterAllModelsExecute(int executionContext) {
		super.afterAllModelsExecute(executionContext);
	}

	/**
	 * <p>onModelError</p>     * 
	 * <p>Description: onModelError method for Jato Framework</p>
	 * 
	 * @param model: Model
	 * @param executionContext: int
	 * @param exception: ModelControlException
	 * @return
	 */
	public void onModelError(Model model, int executionContext,
			ModelControlException exception) throws ModelControlException {
		super.onModelError(model, executionContext, exception);
	}

	public void setdoComponentOverDraftModel( doQuickLinkModel model) {
		doQuickLinkModelInstance = model;
	}

	public doQuickLinkModel getdoQuickLinkModel() {
		if (doQuickLinkModelInstance == null) {
			doQuickLinkModelInstance = 
				(doQuickLinkModel) getModel(doQuickLinkModel.class);
		}
		return doQuickLinkModelInstance;
	}

	// //////////////////////////////////////////////////////////////////////////////
	// Class variables
	// //////////////////////////////////////////////////////////////////////////////


	public static final String CHILD_HDQID = "hdQuickLinkId";
	public static final String CHILD_HDQID_RESET_VALUE = "";

	public static final String CHILD_HDSORTID = "hdSortId";
	public static final String CHILD_HDSORTID_RESET_VALUE = "";

	public static final String CHILD_TBDESCRIPTIONENGLISH = "tbDescriptionEnglish";
	public static final String CHILD_TBDESCRIPTIONENGLISH_RESET_VALUE = "";

	public static final String CHILD_STQUICKLINKLIST = "stQuicklinkList";
	public static final String CHILD_STQUICKLINKLIST_RESET_VALUE = "";

	
	public static final String CHILD_TBDESCRIPTIONFRENCH = "tbDescriptionFrench";
	public static final String CHILD_TBDESCRIPTIONFRENCH_RESET_VALUE = "";

	public static final String CHILD_TBURLADDRESS = "tbUrlAddress";
	public static final String CHILD_TBURLADDRESS_RESET_VALUE = "";

	public static final String CHILD_BTADD = "btAdd";
	public static final String CHILD_BTADD_RESET_VALUE = "";

	public static final String CHILD_BTTESTLINK = "btTestLink";
	public static final String CHILD_BTDTESTLINK_RESET_VALUE = "";

	public static final String CHILD_BTCLEAR = "btClear";
	public static final String CHILD_BTCLEAR_RESET_VALUE = "";

	public static final String CHILD_BTUP = "btUp";
	public static final String CHILD_BTUP_RESET_VALUE = "";

	public static final String CHILD_BTDOWN = "btDown";
	public static final String CHILD_BTDOWN_RESET_VALUE = "";
	
	
	// //////////////////////////////////////////////////////////////////////////
	// Instance variables
	// //////////////////////////////////////////////////////////////////////////

	private doQuickLinkModel doQuickLinkModelInstance = null;
	private QuickLinksHandler handler = new QuickLinksHandler();

}
