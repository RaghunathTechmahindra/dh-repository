package mosApp.MosSystem;

import java.sql.SQLException;

import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doGetRateCodeModelImpl extends QueryModelBase
	implements doGetRateCodeModel
{
	/**
	 *
	 *
	 */
	public doGetRateCodeModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{

	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPricingProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPRICINGPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPricingProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPRICINGPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfRateCode()
	{
		return (String)getValue(FIELD_DFRATECODE);
	}


	/**
	 *
	 *
	 */
	public void setDfRateCode(String value)
	{
		setValue(FIELD_DFRATECODE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPricingRateInventoryId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPRICINGRATEINVENTORYID);
	}


	/**
	 *
	 *
	 */
	public void setDfPricingRateInventoryId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPRICINGRATEINVENTORYID,value);
	}

    public java.math.BigDecimal getDfInstitutionId() {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
      }

      public void setDfInstitutionId(java.math.BigDecimal value) {
        setValue(FIELD_DFINSTITUTIONID, value);
      }


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL PRICINGPROFILE.PRICINGPROFILEID, PRICINGPROFILE.RATECODE, PRICINGRATEINVENTORY.PRICINGRATEINVENTORYID,PRICINGPROFILE.INSTITUTIONPROFILEID FROM PRICINGPROFILE, PRICINGRATEINVENTORY  __WHERE__  ";
	
	public static final String MODIFYING_QUERY_TABLE_NAME="PRICINGPROFILE, PRICINGRATEINVENTORY";
	
	public static final String STATIC_WHERE_CRITERIA=" (PRICINGPROFILE.PRICINGPROFILEID  =  PRICINGRATEINVENTORY.PRICINGPROFILEID) AND (PRICINGPROFILE.INSTITUTIONPROFILEID  =  PRICINGRATEINVENTORY.INSTITUTIONPROFILEID)";
	
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFPRICINGPROFILEID="PRICINGPROFILE.PRICINGPROFILEID";
	public static final String COLUMN_DFPRICINGPROFILEID="PRICINGPROFILEID";
	public static final String QUALIFIED_COLUMN_DFRATECODE="PRICINGPROFILE.RATECODE";
	public static final String COLUMN_DFRATECODE="RATECODE";
	public static final String QUALIFIED_COLUMN_DFPRICINGRATEINVENTORYID="PRICINGRATEINVENTORY.PRICINGRATEINVENTORYID";
	public static final String COLUMN_DFPRICINGRATEINVENTORYID="PRICINGRATEINVENTORYID";

    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "PRICINGPROFILE.INSTITUTIONPROFILEID";
      public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRICINGPROFILEID,
				COLUMN_DFPRICINGPROFILEID,
				QUALIFIED_COLUMN_DFPRICINGPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFRATECODE,
				COLUMN_DFRATECODE,
				QUALIFIED_COLUMN_DFRATECODE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRICINGRATEINVENTORYID,
				COLUMN_DFPRICINGRATEINVENTORYID,
				QUALIFIED_COLUMN_DFPRICINGRATEINVENTORYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	      FIELD_SCHEMA.addFieldDescriptor(
	                new QueryFieldDescriptor(
	                  FIELD_DFINSTITUTIONID,
	                  COLUMN_DFINSTITUTIONID,
	                  QUALIFIED_COLUMN_DFINSTITUTIONID,
	                  java.math.BigDecimal.class,
	                  false,
	                  false,
	                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	                  "",
	                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	                  ""));
	}

}

