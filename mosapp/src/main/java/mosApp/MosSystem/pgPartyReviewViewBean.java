package mosApp.MosSystem;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;

import mosApp.SQLConnectionManagerImpl;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.view.CommandFieldDescriptor;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.ViewBeanBase;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HREF;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

import com.filogix.express.web.jato.ExpressViewBeanBase; 
/**
 *
 *
 *
 */
public class pgPartyReviewViewBean extends ExpressViewBeanBase

{
  /**
   *
   *
   */
  public pgPartyReviewViewBean()
  {
    super(PAGE_NAME);
    setDefaultDisplayURL(DEFAULT_DISPLAY_URL);

    registerChildren();

    initialize();

  }

  /**
   *
   *
   */
  protected void initialize()
  {
  }

  ////////////////////////////////////////////////////////////////////////////////
  // Child manipulation methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   * createChild
   *
   * @param name String <br>
   * 	Name of the child the method will create
   * @return View : The result of createChild<br>
   * @version ?? <br>
   * Date: 05/31/2006 <br>
   * Author: NBC/PP Implementation Team <br>
   * Change: <br>
   * 	Added check for creating a StaticTextField for Location
   *
   */
  protected View createChild(String name)
  {
	  View superReturn = super.createChild(name);  
	  if (superReturn != null) {  
	  return superReturn;  
	  } else if (name.equals(CHILD_TBDEALID)) 
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBDEALID,
        CHILD_TBDEALID,
        CHILD_TBDEALID_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_CBPAGENAMES))
    {
      ComboBox child = new ComboBox(this,
        getDefaultModel(),
        CHILD_CBPAGENAMES,
        CHILD_CBPAGENAMES,
        CHILD_CBPAGENAMES_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("Choose a Page");
      child.setOptions(cbPageNamesOptions);
      return child;
    }
    else
    if(name.equals(CHILD_BTPROCEED))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTPROCEED,
        CHILD_BTPROCEED,
        CHILD_BTPROCEED_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF1))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF1,
        CHILD_HREF1,
        CHILD_HREF1_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF2))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF2,
        CHILD_HREF2,
        CHILD_HREF2_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF3))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF3,
        CHILD_HREF3,
        CHILD_HREF3_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF4))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF4,
        CHILD_HREF4,
        CHILD_HREF4_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF5))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF5,
        CHILD_HREF5,
        CHILD_HREF5_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF6))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF6,
        CHILD_HREF6,
        CHILD_HREF6_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF7))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF7,
        CHILD_HREF7,
        CHILD_HREF7_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF8))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF8,
        CHILD_HREF8,
        CHILD_HREF8_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF9))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF9,
        CHILD_HREF9,
        CHILD_HREF9_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HREF10))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_HREF10,
        CHILD_HREF10,
        CHILD_HREF10_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_STPAGELABEL))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPAGELABEL,
        CHILD_STPAGELABEL,
        CHILD_STPAGELABEL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STCOMPANYNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STCOMPANYNAME,
        CHILD_STCOMPANYNAME,
        CHILD_STCOMPANYNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STTODAYDATE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STTODAYDATE,
        CHILD_STTODAYDATE,
        CHILD_STTODAYDATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STUSERNAMETITLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STUSERNAMETITLE,
        CHILD_STUSERNAMETITLE,
        CHILD_STUSERNAMETITLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STDEALID))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STDEALID,
        doDealSummarySnapShotModel.FIELD_DFAPPLICATIONID,
        CHILD_STDEALID_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STBORRFIRSTNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STBORRFIRSTNAME,
        CHILD_STBORRFIRSTNAME,
        CHILD_STBORRFIRSTNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STDEALSTATUS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STDEALSTATUS,
        doDealSummarySnapShotModel.FIELD_DFSTATUSDESCR,
        CHILD_STDEALSTATUS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STDEALSTATUSDATE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STDEALSTATUSDATE,
        doDealSummarySnapShotModel.FIELD_DFSTATUSDATE,
        CHILD_STDEALSTATUSDATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STSOURCEFIRM))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STSOURCEFIRM,
        doDealSummarySnapShotModel.FIELD_DFSFSHORTNAME,
        CHILD_STSOURCEFIRM_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STSOURCE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STSOURCE,
        doDealSummarySnapShotModel.FIELD_DFSOBPSHORTNAME,
        CHILD_STSOURCE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STLOB))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STLOB,
        doDealSummarySnapShotModel.FIELD_DFLOBDESCR,
        CHILD_STLOB_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STDEALTYPE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STDEALTYPE,
        doDealSummarySnapShotModel.FIELD_DFDEALTYPEDESCR,
        CHILD_STDEALTYPE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STDEALPURPOSE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STDEALPURPOSE,
        doDealSummarySnapShotModel.FIELD_DFLOANPURPOSEDESCR,
        CHILD_STDEALPURPOSE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPURCHASEPRICE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STPURCHASEPRICE,
        doDealSummarySnapShotModel.FIELD_DFTOTALPURCHASEPRICE,
        CHILD_STPURCHASEPRICE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMTTERM))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STPMTTERM,
        doDealSummarySnapShotModel.FIELD_DFPAYMENTTERMDESCR,
        CHILD_STPMTTERM_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STESTCLOSINGDATE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STESTCLOSINGDATE,
        doDealSummarySnapShotModel.FIELD_DFESTCLOSINGDATE,
        CHILD_STESTCLOSINGDATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_BTWORKQUEUELINK))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTWORKQUEUELINK,
        CHILD_BTWORKQUEUELINK,
        CHILD_BTWORKQUEUELINK_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_CHANGEPASSWORDHREF))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_CHANGEPASSWORDHREF,
        CHILD_CHANGEPASSWORDHREF,
        CHILD_CHANGEPASSWORDHREF_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_BTTOOLHISTORY))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTTOOLHISTORY,
        CHILD_BTTOOLHISTORY,
        CHILD_BTTOOLHISTORY_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_BTTOONOTES))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTTOONOTES,
        CHILD_BTTOONOTES,
        CHILD_BTTOONOTES_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_BTTOOLSEARCH))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTTOOLSEARCH,
        CHILD_BTTOOLSEARCH,
        CHILD_BTTOOLSEARCH_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_BTTOOLLOG))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTTOOLLOG,
        CHILD_BTTOOLLOG,
        CHILD_BTTOOLLOG_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_STERRORFLAG))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STERRORFLAG,
        CHILD_STERRORFLAG,
        CHILD_STERRORFLAG_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_DETECTALERTTASKS))
    {
      HiddenField child = new HiddenField(this,
        getDefaultModel(),
        CHILD_DETECTALERTTASKS,
        CHILD_DETECTALERTTASKS,
        CHILD_DETECTALERTTASKS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STTOTALLOANAMOUNT))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STTOTALLOANAMOUNT,
        doDealSummarySnapShotModel.FIELD_DFTOTALLOANAMT,
        CHILD_STTOTALLOANAMOUNT_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_BTPREVTASKPAGE))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTPREVTASKPAGE,
        CHILD_BTPREVTASKPAGE,
        CHILD_BTPREVTASKPAGE_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_STPREVTASKPAGELABEL))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPREVTASKPAGELABEL,
        CHILD_STPREVTASKPAGELABEL,
        CHILD_STPREVTASKPAGELABEL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_BTNEXTTASKPAGE))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTNEXTTASKPAGE,
        CHILD_BTNEXTTASKPAGE,
        CHILD_BTNEXTTASKPAGE_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_STNEXTTASKPAGELABEL))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STNEXTTASKPAGELABEL,
        CHILD_STNEXTTASKPAGELABEL,
        CHILD_STNEXTTASKPAGELABEL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STTASKNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STTASKNAME,
        CHILD_STTASKNAME,
        CHILD_STTASKNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_SESSIONUSERID))
    {
      HiddenField child = new HiddenField(this,
        getDefaultModel(),
        CHILD_SESSIONUSERID,
        CHILD_SESSIONUSERID,
        CHILD_SESSIONUSERID_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPARTYTYPE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STPARTYTYPE,
        doPartySearchModel.FIELD_DFPARTYTYPEDESCRIPTION,
        CHILD_STPARTYTYPE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMGENERATE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMGENERATE,
        CHILD_STPMGENERATE,
        CHILD_STPMGENERATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMHASTITLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMHASTITLE,
        CHILD_STPMHASTITLE,
        CHILD_STPMHASTITLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMHASINFO))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMHASINFO,
        CHILD_STPMHASINFO,
        CHILD_STPMHASINFO_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMHASTABLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMHASTABLE,
        CHILD_STPMHASTABLE,
        CHILD_STPMHASTABLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMHASOK))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMHASOK,
        CHILD_STPMHASOK,
        CHILD_STPMHASOK_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMTITLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMTITLE,
        CHILD_STPMTITLE,
        CHILD_STPMTITLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMINFOMSG))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMINFOMSG,
        CHILD_STPMINFOMSG,
        CHILD_STPMINFOMSG_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMONOK))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMONOK,
        CHILD_STPMONOK,
        CHILD_STPMONOK_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMMSGS))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMMSGS,
        CHILD_STPMMSGS,
        CHILD_STPMMSGS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPMMSGTYPES))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPMMSGTYPES,
        CHILD_STPMMSGTYPES,
        CHILD_STPMMSGTYPES_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMGENERATE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMGENERATE,
        CHILD_STAMGENERATE,
        CHILD_STAMGENERATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMHASTITLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMHASTITLE,
        CHILD_STAMHASTITLE,
        CHILD_STAMHASTITLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMHASINFO))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMHASINFO,
        CHILD_STAMHASINFO,
        CHILD_STAMHASINFO_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMHASTABLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMHASTABLE,
        CHILD_STAMHASTABLE,
        CHILD_STAMHASTABLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMTITLE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMTITLE,
        CHILD_STAMTITLE,
        CHILD_STAMTITLE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMINFOMSG))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMINFOMSG,
        CHILD_STAMINFOMSG,
        CHILD_STAMINFOMSG_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMMSGS))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMMSGS,
        CHILD_STAMMSGS,
        CHILD_STAMMSGS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMMSGTYPES))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMMSGTYPES,
        CHILD_STAMMSGTYPES,
        CHILD_STAMMSGTYPES_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMDIALOGMSG))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMDIALOGMSG,
        CHILD_STAMDIALOGMSG,
        CHILD_STAMDIALOGMSG_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAMBUTTONSHTML))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAMBUTTONSHTML,
        CHILD_STAMBUTTONSHTML,
        CHILD_STAMBUTTONSHTML_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STINCLUDEDSSSTART))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STINCLUDEDSSSTART,
        CHILD_STINCLUDEDSSSTART,
        CHILD_STINCLUDEDSSSTART_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STINCLUDEDSSEND))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STINCLUDEDSSEND,
        CHILD_STINCLUDEDSSEND,
        CHILD_STINCLUDEDSSEND_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_BTOK))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTOK,
        CHILD_BTOK,
        CHILD_BTOK_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_STPARTYSTATUS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STPARTYSTATUS,
        doPartySearchModel.FIELD_DFPARTYPROFILESTATUSDESC,
        CHILD_STPARTYSTATUS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STFAXNUMBER))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STFAXNUMBER,
        CHILD_STFAXNUMBER,
        CHILD_STFAXNUMBER_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPARTYCOMPANYNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STPARTYCOMPANYNAME,
        doPartySearchModel.FIELD_DFCOMPANY,
        CHILD_STPARTYCOMPANYNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STCITY))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STCITY,
        doPartySearchModel.FIELD_DFCITY,
        CHILD_STCITY_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPROVINCE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STPROVINCE,
        doPartySearchModel.FIELD_DFPROVINCENAME,
        CHILD_STPROVINCE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPARTYFIRSTNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STPARTYFIRSTNAME,
        doPartySearchModel.FIELD_DFCONTACTFIRSTNAME,
        CHILD_STPARTYFIRSTNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPARTYMIDDLEINITIAL))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STPARTYMIDDLEINITIAL,
        doPartySearchModel.FIELD_DFCONTACTINTITIAL,
        CHILD_STPARTYMIDDLEINITIAL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPARTYLASTNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STPARTYLASTNAME,
        doPartySearchModel.FIELD_DFCONTACTLASTNAME,
        CHILD_STPARTYLASTNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STWORKPHONENUMBER))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STWORKPHONENUMBER,
        CHILD_STWORKPHONENUMBER,
        CHILD_STWORKPHONENUMBER_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STEMAIL))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STEMAIL,
        doPartySearchModel.FIELD_DFCONTACTEMAIL,
        CHILD_STEMAIL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STADDRESSLINE1))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STADDRESSLINE1,
        doPartySearchModel.FIELD_DFADDRESSLINE1,
        CHILD_STADDRESSLINE1_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STADDRESSLINE2))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STADDRESSLINE2,
        doPartySearchModel.FIELD_DFADDRESSLINE2,
        CHILD_STADDRESSLINE2_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPOSTALCODEFSA))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STPOSTALCODEFSA,
        doPartySearchModel.FIELD_DFPOSTALFSA,
        CHILD_STPOSTALCODEFSA_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPOSTALCODELDU))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STPOSTALCODELDU,
        doPartySearchModel.FIELD_DFPOSTALLDU,
        CHILD_STPOSTALCODELDU_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STCLIENTNUMBER))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STCLIENTNUMBER,
        doPartySearchModel.FIELD_DFCLIENTNUMBER,
        CHILD_STCLIENTNUMBER_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPARTYSHORTNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STPARTYSHORTNAME,
        doPartySearchModel.FIELD_DFPARTYSHORTNAME,
        CHILD_STPARTYSHORTNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_HDPARTYID))
    {
      HiddenField child = new HiddenField(this,
        getdoPartySearchModel(),
        CHILD_HDPARTYID,
        doPartySearchModel.FIELD_DFPARTYID,
        CHILD_HDPARTYID_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_HDPARTYTYPEID))
    {
      HiddenField child = new HiddenField(this,
        getdoPartySearchModel(),
        CHILD_HDPARTYTYPEID,
        doPartySearchModel.FIELD_DFPARTYTYPE,
        CHILD_HDPARTYTYPEID_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_BTMODIFYPARTY))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTMODIFYPARTY,
        CHILD_BTMODIFYPARTY,
        CHILD_BTMODIFYPARTY_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HDPARTYSTATUSID))
    {
      HiddenField child = new HiddenField(this,
        getdoPartySearchModel(),
        CHILD_HDPARTYSTATUSID,
        doPartySearchModel.FIELD_DFPARTYSTATUS,
        CHILD_HDPARTYSTATUSID_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STSPECIALFEATURE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealSummarySnapShotModel(),
        CHILD_STSPECIALFEATURE,
        doDealSummarySnapShotModel.FIELD_DFSPECIALFEATURE,
        CHILD_STSPECIALFEATURE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBNOTES))
    {
      TextField child = new TextField(this,
        getdoPartySearchModel(),
        CHILD_TBNOTES,
        doPartySearchModel.FIELD_DFNOTES,
        CHILD_TBNOTES_RESET_VALUE,
        null);
      return child;
    }
    //// Hidden button to trigger the ActiveMessage 'fake' submit button
    //// via the new BX ActMessageCommand class
    else
    if(name.equals(CHILD_BTACTMSG))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTACTMSG,
        CHILD_BTACTMSG,
        CHILD_BTACTMSG_RESET_VALUE,
        new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));
      return child;
    }
    //--Release2.1--//
    //// New link to toggle language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new session
    //// language id throughout the all modulus.
    else
    if(name.equals(CHILD_TOGGLELANGUAGEHREF))
    {
      HREF child = new HREF(
        this,
        getDefaultModel(),
        CHILD_TOGGLELANGUAGEHREF,
        CHILD_TOGGLELANGUAGEHREF,
        CHILD_TOGGLELANGUAGEHREF_RESET_VALUE,
        null);
      return child;
    }
    //--DJ_PT_CR--start--//
    else
    if(name.equals(CHILD_STBRANCHTRANSITNUMCMHC))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STBRANCHTRANSITNUMCMHC,
        doPartySearchModel.FIELD_DFBRANCHTRANSITNUMCMHC,
        CHILD_STBRANCHTRANSITNUMCMHC_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STBRANCHTRANSITNUMGE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STBRANCHTRANSITNUMGE,
        doPartySearchModel.FIELD_DFBRANCHTRANSITNUMGE,
        CHILD_STBRANCHTRANSITNUMGE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPARTYNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STPARTYNAME,
        doPartySearchModel.FIELD_DFPARTYNAME,
        CHILD_STPARTYNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPARTYSHORTNAMELABEL))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPARTYSHORTNAMELABEL,
        CHILD_STPARTYSHORTNAMELABEL,
        CHILD_STPARTYSHORTNAMELABEL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STCLIENTNUMBERLABEL))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STCLIENTNUMBERLABEL,
        CHILD_STCLIENTNUMBERLABEL,
        CHILD_STCLIENTNUMBERLABEL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STINCLUDEBRANCHTRANSITINFOSTART))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STINCLUDEBRANCHTRANSITINFOSTART,
        CHILD_STINCLUDEBRANCHTRANSITINFOSTART,
        CHILD_STINCLUDEBRANCHTRANSITINFOSTART_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STINCLUDEBRANCHTRANSITINFOEND))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STINCLUDEBRANCHTRANSITINFOEND,
        CHILD_STINCLUDEBRANCHTRANSITINFOEND,
        CHILD_STINCLUDEBRANCHTRANSITINFOEND_RESET_VALUE,
        null);
      return child;
    }
    //--DJ_PT_CR--end--//
    //--DJ_PREFCOMMETHOD_CR--start--//
    else
    if(name.equals(CHILD_STPREFDELIVERYMETHOD))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STPREFDELIVERYMETHOD,
        doPartySearchModel.FIELD_DFPREFERREDDELIVERYMETHOD,
        CHILD_STPREFDELIVERYMETHOD_RESET_VALUE,
        null);
      return child;
    }
    //--DJ_PREFCOMMETHOD_CR--end--//
    //--> Ticket#369 :: Change for Cervus :: added New Bank info for Party
    //--> By Billy 21May2004
    else
    if(name.equals(CHILD_STBANKTRANSITNUM))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STBANKTRANSITNUM,
        doPartySearchModel.FIELD_DFBANKTRANSITNUM,
        CHILD_STBANKTRANSITNUM_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STBANKACCNUM))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STBANKACCNUM,
        doPartySearchModel.FIELD_DFBANKACCNUM,
        CHILD_STBANKACCNUM_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STBANKNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getdoPartySearchModel(),
        CHILD_STBANKNAME,
        doPartySearchModel.FIELD_DFBANKNAME,
        CHILD_STBANKNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STINCLUDEBANKINFOSTART))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STINCLUDEBANKINFOSTART,
        CHILD_STINCLUDEBANKINFOSTART,
        CHILD_STINCLUDEBANKINFOSTART_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STINCLUDEBANKINFOEND))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STINCLUDEBANKINFOEND,
        CHILD_STINCLUDEBANKINFOEND,
        CHILD_STINCLUDEBANKINFOEND_RESET_VALUE,
        null);
      return child;
    }
    //====================================================================
    //***** NBC/PP Implementation Team - Version 1.1 - START *****//
    else
        if(name.equals(CHILD_STLOCATION))
        {
          StaticTextField child = new StaticTextField(this,
            getdoPartySearchModel(),
            CHILD_STLOCATION,
            doPartySearchModel.FIELD_DFLOCATION,
            CHILD_STLOCATION_RESET_VALUE,
            null);
          return child;
        }    
    //*****  NBC/PP Implementation Team - Version 1.1 - END *****//
    else
    {
      throw new IllegalArgumentException("Invalid child name [" + name + "]");
    }
  }

  /**
   * resetChildren
   *
   * @param none
   * @return none
   * @version ?? <br>
   * Date: 05/31/2006<br>
   * Author: NBC/PP Implementation Team <br>
   * Change: <br>
   * 	Added Location static text
   */
  public void resetChildren()
  {
	super.resetChildren(); 
	getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
    getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
    getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
    getHref1().setValue(CHILD_HREF1_RESET_VALUE);
    getHref2().setValue(CHILD_HREF2_RESET_VALUE);
    getHref3().setValue(CHILD_HREF3_RESET_VALUE);
    getHref4().setValue(CHILD_HREF4_RESET_VALUE);
    getHref5().setValue(CHILD_HREF5_RESET_VALUE);
    getHref6().setValue(CHILD_HREF6_RESET_VALUE);
    getHref7().setValue(CHILD_HREF7_RESET_VALUE);
    getHref8().setValue(CHILD_HREF8_RESET_VALUE);
    getHref9().setValue(CHILD_HREF9_RESET_VALUE);
    getHref10().setValue(CHILD_HREF10_RESET_VALUE);
    getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
    getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
    getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
    getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
    getStDealId().setValue(CHILD_STDEALID_RESET_VALUE);
    getStBorrFirstName().setValue(CHILD_STBORRFIRSTNAME_RESET_VALUE);
    getStDealStatus().setValue(CHILD_STDEALSTATUS_RESET_VALUE);
    getStDealStatusDate().setValue(CHILD_STDEALSTATUSDATE_RESET_VALUE);
    getStSourceFirm().setValue(CHILD_STSOURCEFIRM_RESET_VALUE);
    getStSource().setValue(CHILD_STSOURCE_RESET_VALUE);
    getStLOB().setValue(CHILD_STLOB_RESET_VALUE);
    getStDealType().setValue(CHILD_STDEALTYPE_RESET_VALUE);
    getStDealPurpose().setValue(CHILD_STDEALPURPOSE_RESET_VALUE);
    getStPurchasePrice().setValue(CHILD_STPURCHASEPRICE_RESET_VALUE);
    getStPmtTerm().setValue(CHILD_STPMTTERM_RESET_VALUE);
    getStEstClosingDate().setValue(CHILD_STESTCLOSINGDATE_RESET_VALUE);
    getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
    getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
    getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
    getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
    getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
    getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
    getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
    getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
    getStTotalLoanAmount().setValue(CHILD_STTOTALLOANAMOUNT_RESET_VALUE);
    getBtPrevTaskPage().setValue(CHILD_BTPREVTASKPAGE_RESET_VALUE);
    getStPrevTaskPageLabel().setValue(CHILD_STPREVTASKPAGELABEL_RESET_VALUE);
    getBtNextTaskPage().setValue(CHILD_BTNEXTTASKPAGE_RESET_VALUE);
    getStNextTaskPageLabel().setValue(CHILD_STNEXTTASKPAGELABEL_RESET_VALUE);
    getStTaskName().setValue(CHILD_STTASKNAME_RESET_VALUE);
    getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
    getStPartyType().setValue(CHILD_STPARTYTYPE_RESET_VALUE);
    getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
    getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
    getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
    getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
    getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
    getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
    getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
    getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
    getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
    getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
    getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
    getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
    getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
    getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
    getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
    getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
    getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
    getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
    getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
    getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
    getStIncludeDSSstart().setValue(CHILD_STINCLUDEDSSSTART_RESET_VALUE);
    getStIncludeDSSend().setValue(CHILD_STINCLUDEDSSEND_RESET_VALUE);
    getBtOK().setValue(CHILD_BTOK_RESET_VALUE);
    getStPartyStatus().setValue(CHILD_STPARTYSTATUS_RESET_VALUE);
    getStFaxNumber().setValue(CHILD_STFAXNUMBER_RESET_VALUE);
    getStPartyCompanyName().setValue(CHILD_STPARTYCOMPANYNAME_RESET_VALUE);
    getStCity().setValue(CHILD_STCITY_RESET_VALUE);
    getStProvince().setValue(CHILD_STPROVINCE_RESET_VALUE);
    getStPartyFirstName().setValue(CHILD_STPARTYFIRSTNAME_RESET_VALUE);
    getStPartyMiddleInitial().setValue(CHILD_STPARTYMIDDLEINITIAL_RESET_VALUE);
    getStPartyLastName().setValue(CHILD_STPARTYLASTNAME_RESET_VALUE);
    getStWorkPhoneNumber().setValue(CHILD_STWORKPHONENUMBER_RESET_VALUE);
    getStEmail().setValue(CHILD_STEMAIL_RESET_VALUE);
    getStAddressLine1().setValue(CHILD_STADDRESSLINE1_RESET_VALUE);
    getStAddressLine2().setValue(CHILD_STADDRESSLINE2_RESET_VALUE);
    getStPostalCodeFSA().setValue(CHILD_STPOSTALCODEFSA_RESET_VALUE);
    getStPostalCodeLDU().setValue(CHILD_STPOSTALCODELDU_RESET_VALUE);
    getStClientNumber().setValue(CHILD_STCLIENTNUMBER_RESET_VALUE);
    getStPartyShortName().setValue(CHILD_STPARTYSHORTNAME_RESET_VALUE);
    getHdPartyId().setValue(CHILD_HDPARTYID_RESET_VALUE);
    getHdPartyTypeId().setValue(CHILD_HDPARTYTYPEID_RESET_VALUE);
    getBtModifyParty().setValue(CHILD_BTMODIFYPARTY_RESET_VALUE);
    getHdPartyStatusId().setValue(CHILD_HDPARTYSTATUSID_RESET_VALUE);
    getStSpecialFeature().setValue(CHILD_STSPECIALFEATURE_RESET_VALUE);
    getTbNotes().setValue(CHILD_TBNOTES_RESET_VALUE);
    //// new hidden button to activate 'fake' submit button from the ActiveMessage class.
    getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);

    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
    getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);
    //--DJ_PT_CR--start--//
    getStBranchTransitNumCMHC().setValue(CHILD_STBRANCHTRANSITNUMCMHC_RESET_VALUE);
    getStBranchTransitNumGE().setValue(CHILD_STBRANCHTRANSITNUMGE_RESET_VALUE);
    getStPartyName().setValue(CHILD_STPARTYNAME_RESET_VALUE);
    getStPartyShortName().setValue(CHILD_STPARTYSHORTNAMELABEL_RESET_VALUE);
    getStClientNumberLabel().setValue(CHILD_STCLIENTNUMBERLABEL_RESET_VALUE);
    getStIncludeBranchTransitInfoStart().setValue(CHILD_STINCLUDEBRANCHTRANSITINFOSTART_RESET_VALUE);
    getStIncludeBranchTransitInfoEnd().setValue(CHILD_STINCLUDEBRANCHTRANSITINFOEND_RESET_VALUE);
    //--DJ_PT_CR--end--//
    //--DJ_PREFCOMMETHOD_CR--start--//
    getStPrefDeliveryMethod().setValue(CHILD_STPREFDELIVERYMETHOD_RESET_VALUE);
    //--DJ_PREFCOMMETHOD_CR--end--//
    //--> Ticket#369 :: Change for Cervus :: added New Bank info for Party
    //--> By Billy 21May2004
    getStBankTransitNum().setValue(CHILD_STBANKTRANSITNUM_RESET_VALUE);
    getStBankAccNum().setValue(CHILD_STBANKACCNUM_RESET_VALUE);
    getStBankName().setValue(CHILD_STBANKNAME_RESET_VALUE);
    getStIncludeBankInfoStart().setValue(CHILD_STINCLUDEBANKINFOSTART_RESET_VALUE);
    getStIncludeBankInfoEnd().setValue(CHILD_STINCLUDEBANKINFOEND_RESET_VALUE);
    //====================================================================
    //***** Change By NBC/PP Implementation Team - Version 1.1 - START ****//
    getStLocation().setValue(CHILD_STLOCATION);
    //***** Change By NBC/PP Implementation Team - Version 1.1 - END ****//    
  }

  /**
   * registerChildren
   *
   * @param none
   * @return none
   * @version ?? <br>
   * Date: 05/31/2006<br>
   * Author: NBC/PP Implementation Team <br>
   * Change: <br>
   * 	Added Location static text
   *
   */
  protected void registerChildren()
  {
	super.registerChildren(); 
	registerChild(CHILD_TBDEALID, TextField.class);
    registerChild(CHILD_CBPAGENAMES, ComboBox.class);
    registerChild(CHILD_BTPROCEED, Button.class);
    registerChild(CHILD_HREF1, HREF.class);
    registerChild(CHILD_HREF2, HREF.class);
    registerChild(CHILD_HREF3, HREF.class);
    registerChild(CHILD_HREF4, HREF.class);
    registerChild(CHILD_HREF5, HREF.class);
    registerChild(CHILD_HREF6, HREF.class);
    registerChild(CHILD_HREF7, HREF.class);
    registerChild(CHILD_HREF8, HREF.class);
    registerChild(CHILD_HREF9, HREF.class);
    registerChild(CHILD_HREF10, HREF.class);
    registerChild(CHILD_STPAGELABEL, StaticTextField.class);
    registerChild(CHILD_STCOMPANYNAME, StaticTextField.class);
    registerChild(CHILD_STTODAYDATE, StaticTextField.class);
    registerChild(CHILD_STUSERNAMETITLE, StaticTextField.class);
    registerChild(CHILD_STDEALID, StaticTextField.class);
    registerChild(CHILD_STBORRFIRSTNAME, StaticTextField.class);
    registerChild(CHILD_STDEALSTATUS, StaticTextField.class);
    registerChild(CHILD_STDEALSTATUSDATE, StaticTextField.class);
    registerChild(CHILD_STSOURCEFIRM, StaticTextField.class);
    registerChild(CHILD_STSOURCE, StaticTextField.class);
    registerChild(CHILD_STLOB, StaticTextField.class);
    registerChild(CHILD_STDEALTYPE, StaticTextField.class);
    registerChild(CHILD_STDEALPURPOSE, StaticTextField.class);
    registerChild(CHILD_STPURCHASEPRICE, StaticTextField.class);
    registerChild(CHILD_STPMTTERM, StaticTextField.class);
    registerChild(CHILD_STESTCLOSINGDATE, StaticTextField.class);
    registerChild(CHILD_BTWORKQUEUELINK, Button.class);
    registerChild(CHILD_CHANGEPASSWORDHREF, HREF.class);
    registerChild(CHILD_BTTOOLHISTORY, Button.class);
    registerChild(CHILD_BTTOONOTES, Button.class);
    registerChild(CHILD_BTTOOLSEARCH, Button.class);
    registerChild(CHILD_BTTOOLLOG, Button.class);
    registerChild(CHILD_STERRORFLAG, StaticTextField.class);
    registerChild(CHILD_DETECTALERTTASKS, HiddenField.class);
    registerChild(CHILD_STTOTALLOANAMOUNT, StaticTextField.class);
    registerChild(CHILD_BTPREVTASKPAGE, Button.class);
    registerChild(CHILD_STPREVTASKPAGELABEL, StaticTextField.class);
    registerChild(CHILD_BTNEXTTASKPAGE, Button.class);
    registerChild(CHILD_STNEXTTASKPAGELABEL, StaticTextField.class);
    registerChild(CHILD_STTASKNAME, StaticTextField.class);
    registerChild(CHILD_SESSIONUSERID, HiddenField.class);
    registerChild(CHILD_STPARTYTYPE, StaticTextField.class);
    registerChild(CHILD_STPMGENERATE, StaticTextField.class);
    registerChild(CHILD_STPMHASTITLE, StaticTextField.class);
    registerChild(CHILD_STPMHASINFO, StaticTextField.class);
    registerChild(CHILD_STPMHASTABLE, StaticTextField.class);
    registerChild(CHILD_STPMHASOK, StaticTextField.class);
    registerChild(CHILD_STPMTITLE, StaticTextField.class);
    registerChild(CHILD_STPMINFOMSG, StaticTextField.class);
    registerChild(CHILD_STPMONOK, StaticTextField.class);
    registerChild(CHILD_STPMMSGS, StaticTextField.class);
    registerChild(CHILD_STPMMSGTYPES, StaticTextField.class);
    registerChild(CHILD_STAMGENERATE, StaticTextField.class);
    registerChild(CHILD_STAMHASTITLE, StaticTextField.class);
    registerChild(CHILD_STAMHASINFO, StaticTextField.class);
    registerChild(CHILD_STAMHASTABLE, StaticTextField.class);
    registerChild(CHILD_STAMTITLE, StaticTextField.class);
    registerChild(CHILD_STAMINFOMSG, StaticTextField.class);
    registerChild(CHILD_STAMMSGS, StaticTextField.class);
    registerChild(CHILD_STAMMSGTYPES, StaticTextField.class);
    registerChild(CHILD_STAMDIALOGMSG, StaticTextField.class);
    registerChild(CHILD_STAMBUTTONSHTML, StaticTextField.class);
    registerChild(CHILD_STINCLUDEDSSSTART, StaticTextField.class);
    registerChild(CHILD_STINCLUDEDSSEND, StaticTextField.class);
    registerChild(CHILD_BTOK, Button.class);
    registerChild(CHILD_STPARTYSTATUS, StaticTextField.class);
    registerChild(CHILD_STFAXNUMBER, StaticTextField.class);
    registerChild(CHILD_STPARTYCOMPANYNAME, StaticTextField.class);
    registerChild(CHILD_STCITY, StaticTextField.class);
    registerChild(CHILD_STPROVINCE, StaticTextField.class);
    registerChild(CHILD_STPARTYFIRSTNAME, StaticTextField.class);
    registerChild(CHILD_STPARTYMIDDLEINITIAL, StaticTextField.class);
    registerChild(CHILD_STPARTYLASTNAME, StaticTextField.class);
    registerChild(CHILD_STWORKPHONENUMBER, StaticTextField.class);
    registerChild(CHILD_STEMAIL, StaticTextField.class);
    registerChild(CHILD_STADDRESSLINE1, StaticTextField.class);
    registerChild(CHILD_STADDRESSLINE2, StaticTextField.class);
    registerChild(CHILD_STPOSTALCODEFSA, StaticTextField.class);
    registerChild(CHILD_STPOSTALCODELDU, StaticTextField.class);
    registerChild(CHILD_STCLIENTNUMBER, StaticTextField.class);
    registerChild(CHILD_STPARTYSHORTNAME, StaticTextField.class);
    registerChild(CHILD_HDPARTYID, HiddenField.class);
    registerChild(CHILD_HDPARTYTYPEID, HiddenField.class);
    registerChild(CHILD_BTMODIFYPARTY, Button.class);
    registerChild(CHILD_HDPARTYSTATUSID, HiddenField.class);
    registerChild(CHILD_STSPECIALFEATURE, StaticTextField.class);
    registerChild(CHILD_TBNOTES, TextField.class);
    //// New hidden button implementation.
    registerChild(CHILD_BTACTMSG, Button.class);

    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
    registerChild(CHILD_TOGGLELANGUAGEHREF, HREF.class);
    //--DJ_PT_CR--start--//
    registerChild(CHILD_STBRANCHTRANSITNUMCMHC, StaticTextField.class);
    registerChild(CHILD_STBRANCHTRANSITNUMGE, StaticTextField.class);
    registerChild(CHILD_STPARTYNAME, StaticTextField.class);
    registerChild(CHILD_STPARTYSHORTNAMELABEL, StaticTextField.class);
    registerChild(CHILD_STCLIENTNUMBERLABEL, StaticTextField.class);
    registerChild(CHILD_STINCLUDEBRANCHTRANSITINFOSTART, StaticTextField.class);
    registerChild(CHILD_STINCLUDEBRANCHTRANSITINFOEND, StaticTextField.class);
    //--DJ_PT_CR--end--//
    //--DJ_PREFCOMMETHOD_CR--start--//
    registerChild(CHILD_STPREFDELIVERYMETHOD, StaticTextField.class);
    //--DJ_PREFCOMMETHOD_CR--end--//
    //--> Ticket#369 :: Change for Cervus :: added New Bank info for Party
    //--> By Billy 21May2004
    registerChild(CHILD_STBANKTRANSITNUM, StaticTextField.class);
    registerChild(CHILD_STBANKACCNUM, StaticTextField.class);
    registerChild(CHILD_STBANKNAME, StaticTextField.class);
    registerChild(CHILD_STINCLUDEBANKINFOSTART, StaticTextField.class);
    registerChild(CHILD_STINCLUDEBANKINFOSTART, StaticTextField.class);
    //====================================================================
    //***** Change By NBC/PP Implementation Team - Version 1.1 - START ****//
    registerChild(CHILD_STLOCATION, StaticTextField.class);
    //***** Change By NBC/PP Implementation Team - Version 1.1 - END ****//    
  }

  ////////////////////////////////////////////////////////////////////////////////
  // Model management methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  public Model[] getWebActionModels(int executionType)
  {
    List modelList = new ArrayList();
    switch(executionType)
    {
      case MODEL_TYPE_RETRIEVE:
        modelList.add(getdoDealSummarySnapShotModel()); ;
        modelList.add(getdoPartySearchModel()); ;
        break;

      case MODEL_TYPE_UPDATE:
        ;
        break;

      case MODEL_TYPE_DELETE:
        ;
        break;

      case MODEL_TYPE_INSERT:
        ;
        break;

      case MODEL_TYPE_EXECUTE:
        ;
        break;
    }
    return(Model[])modelList.toArray(new Model[0]);
  }

  ////////////////////////////////////////////////////////////////////////////////
  // View flow control methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  public void beginDisplay(DisplayEvent event) throws ModelControlException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();
    handler.pageGetState(this);

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By John 05Dec2002
    //Setup Options for cbPageNames
    //cbPageNamesOptions..populate(getRequestContext());
	int langId = handler.getTheSessionState().getLanguageId();
    cbPageNamesOptions.populate(getRequestContext(), langId);

    //Setup NoneSelected Label for cbPageNames
    CHILD_CBPAGENAMES_NONSELECTED_LABEL =
      BXResources.getGenericMsg("CBPAGENAMES_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    //Check if the cbPageNames is already created
    if(getCbPageNames() != null)
    {
      getCbPageNames().setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);

      //=====================================

    }
    handler.pageSaveState();
    super.beginDisplay(event);
  }

  /**
   *
   *
   */
  public boolean beforeModelExecutes(Model model, int executionContext)
  {

    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.pageGetState(this);

    handler.setupBeforePageGeneration();

    handler.pageSaveState();

    // This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
    return super.beforeModelExecutes(model, executionContext);

  }

  /**
   *
   *
   */
  public void afterModelExecutes(Model model, int executionContext)
  {

    // This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
    super.afterModelExecutes(model, executionContext);

  }

  /**
   *
   *
   */
  public void afterAllModelsExecute(int executionContext)
  {

    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.pageGetState(this);

    handler.populatePageDisplayFields();

    handler.pageSaveState();

    // This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
    super.afterAllModelsExecute(executionContext);
  }

  /**
   *
   *
   */
  public void onModelError(Model model, int executionContext, ModelControlException exception) throws ModelControlException
  {

    // This is the analog of NetDynamics this_onDataObjectErrorEvent
    super.onModelError(model, executionContext, exception);

  }

  /**
   *
   *
   */
  public TextField getTbDealId()
  {
    return(TextField)getChild(CHILD_TBDEALID);
  }

  /**
   *
   *
   */
  public ComboBox getCbPageNames()
  {
    return(ComboBox)getChild(CHILD_CBPAGENAMES);
  }

  /**
   *
   *
   */
  //--DJ_Ticket661--start--//
  //Modified to sort by PageLabel based on the selected language
  static class CbPageNamesOptionList extends GotoOptionList
 {
    /**
     *
     *
     */
    CbPageNamesOptionList() {

    }


    public void populate(RequestContext rc) {
      Connection c = null;
      try {
        clear();
        SelectQueryModel m = null;

        SelectQueryExecutionContext eContext = new
          SelectQueryExecutionContext(
          (Connection)null,

          DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,

          DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null) {
          m = new doPageNameLabelModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else {
          m = (SelectQueryModel)rc.getModelManager().getModel(doPageNameLabelModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        // Sort the results from DataObject
        TreeMap sorted = new TreeMap();
        while (m.next()) {
          Object dfPageLabel =
            m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
          String label = (dfPageLabel == null ? "" : dfPageLabel.toString());
          Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
          String value = (dfPageId == null ? "" : dfPageId.toString());
          String[] theVal = new String[2];
          theVal[0] = label;
          theVal[1] = value;
          sorted.put(label, theVal);
        }

        // Set the sorted list to the optionlist
        Iterator theList = sorted.values().iterator();
        String[] theVal = new String[2];
        while (theList.hasNext()) {
          theVal = (String[]) (theList.next());
          add(theVal[0], theVal[1]);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      finally {
        try {
          if (c != null)
            c.close();
        }
        catch (SQLException ex) {
              // ignore
        }
      }
    }
 }
 //--DJ_Ticket661--end--//

  /**
   *
   *
   */
  public void handleBtProceedRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleGoPage();

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public Button getBtProceed()
  {
    return(Button)getChild(CHILD_BTPROCEED);
  }

  /**
   *
   *
   */
  public void handleHref1Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(0);

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public HREF getHref1()
  {
    return(HREF)getChild(CHILD_HREF1);
  }

  /**
   *
   *
   */
  public void handleHref2Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(1);

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public HREF getHref2()
  {
    return(HREF)getChild(CHILD_HREF2);
  }

  /**
   *
   *
   */
  public void handleHref3Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(2);

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public HREF getHref3()
  {
    return(HREF)getChild(CHILD_HREF3);
  }

  /**
   *
   *
   */
  public void handleHref4Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(3);

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public HREF getHref4()
  {
    return(HREF)getChild(CHILD_HREF4);
  }

  /**
   *
   *
   */
  public void handleHref5Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(4);

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public HREF getHref5()
  {
    return(HREF)getChild(CHILD_HREF5);
  }

  /**
   *
   *
   */
  public void handleHref6Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(5);

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public HREF getHref6()
  {
    return(HREF)getChild(CHILD_HREF6);
  }

  /**
   *
   *
   */
  public void handleHref7Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(6);

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public HREF getHref7()
  {
    return(HREF)getChild(CHILD_HREF7);
  }

  /**
   *
   *
   */
  public void handleHref8Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(7);

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public HREF getHref8()
  {
    return(HREF)getChild(CHILD_HREF8);
  }

  /**
   *
   *
   */
  public void handleHref9Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(8);

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public HREF getHref9()
  {
    return(HREF)getChild(CHILD_HREF9);
  }

  /**
   *
   *
   */
  public void handleHref10Request(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(9);

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public HREF getHref10()
  {
    return(HREF)getChild(CHILD_HREF10);
  }

  //--Release2.1--start//
  //// Two new methods providing the business logic for the new link to toggle language.
  //// When touched this link should reload the page in opposite language (french versus english)
  //// and set this new session value.
  /**
   *
   *
   */
  public void handleToggleLanguageHrefRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleToggleLanguage();

    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getToggleLanguageHref()
  {
    return(HREF)getChild(CHILD_TOGGLELANGUAGEHREF);
  }

  //--Release2.1--end//

  /**
   *
   *
   */
  public StaticTextField getStPageLabel()
  {
    return(StaticTextField)getChild(CHILD_STPAGELABEL);
  }

  /**
   *
   *
   */
  public StaticTextField getStCompanyName()
  {
    return(StaticTextField)getChild(CHILD_STCOMPANYNAME);
  }

  /**
   *
   *
   */
  public StaticTextField getStTodayDate()
  {
    return(StaticTextField)getChild(CHILD_STTODAYDATE);
  }

  /**
   *
   *
   */
  public StaticTextField getStUserNameTitle()
  {
    return(StaticTextField)getChild(CHILD_STUSERNAMETITLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStDealId()
  {
    return(StaticTextField)getChild(CHILD_STDEALID);
  }

  /**
   *
   *
   */
  public StaticTextField getStBorrFirstName()
  {
    return(StaticTextField)getChild(CHILD_STBORRFIRSTNAME);
  }

  /**
   *
   *
   */
  public StaticTextField getStDealStatus()
  {
    return(StaticTextField)getChild(CHILD_STDEALSTATUS);
  }

  /**
   *
   *
   */
  public StaticTextField getStDealStatusDate()
  {
    return(StaticTextField)getChild(CHILD_STDEALSTATUSDATE);
  }

  /**
   *
   *
   */
  public StaticTextField getStSourceFirm()
  {
    return(StaticTextField)getChild(CHILD_STSOURCEFIRM);
  }

  /**
   *
   *
   */
  public StaticTextField getStSource()
  {
    return(StaticTextField)getChild(CHILD_STSOURCE);
  }

  /**
   *
   *
   */
  public StaticTextField getStLOB()
  {
    return(StaticTextField)getChild(CHILD_STLOB);
  }

  /**
   *
   *
   */
  public StaticTextField getStDealType()
  {
    return(StaticTextField)getChild(CHILD_STDEALTYPE);
  }

  /**
   *
   *
   */
  public StaticTextField getStDealPurpose()
  {
    return(StaticTextField)getChild(CHILD_STDEALPURPOSE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPurchasePrice()
  {
    return(StaticTextField)getChild(CHILD_STPURCHASEPRICE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmtTerm()
  {
    return(StaticTextField)getChild(CHILD_STPMTTERM);
  }

  /**
   *
   *
   */
  public StaticTextField getStEstClosingDate()
  {
    return(StaticTextField)getChild(CHILD_STESTCLOSINGDATE);
  }

  /**
   *
   *
   */
  public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleDisplayWorkQueue();

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public Button getBtWorkQueueLink()
  {
    return(Button)getChild(CHILD_BTWORKQUEUELINK);
  }

  /**
   *
   *
   */
  public void handleChangePasswordHrefRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleChangePassword();

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public HREF getChangePasswordHref()
  {
    return(HREF)getChild(CHILD_CHANGEPASSWORDHREF);
  }

  /**
   *
   *
   */
  public void handleBtToolHistoryRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleDisplayDealHistory();

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public Button getBtToolHistory()
  {
    return(Button)getChild(CHILD_BTTOOLHISTORY);
  }

  /**
   *
   *
   */
  public void handleBtTooNotesRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleDisplayDealNotes();

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public Button getBtTooNotes()
  {
    return(Button)getChild(CHILD_BTTOONOTES);
  }

  /**
   *
   *
   */
  public void handleBtToolSearchRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleDealSearch();

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public Button getBtToolSearch()
  {
    return(Button)getChild(CHILD_BTTOOLSEARCH);
  }

  /**
   *
   *
   */
  public void handleBtToolLogRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleSignOff();

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public Button getBtToolLog()
  {
    return(Button)getChild(CHILD_BTTOOLLOG);
  }

  /**
   *
   *
   */
  public StaticTextField getStErrorFlag()
  {
    return(StaticTextField)getChild(CHILD_STERRORFLAG);
  }

  /**
   *
   *
   */
  public HiddenField getDetectAlertTasks()
  {
    return(HiddenField)getChild(CHILD_DETECTALERTTASKS);
  }

  /**
   *
   *
   */
  public StaticTextField getStTotalLoanAmount()
  {
    return(StaticTextField)getChild(CHILD_STTOTALLOANAMOUNT);
  }

  /**
   *
   *
   */
  public void handleBtPrevTaskPageRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handlePrevTaskPage();

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public Button getBtPrevTaskPage()
  {
    return(Button)getChild(CHILD_BTPREVTASKPAGE);
  }

  /**
   *
   *
   */
  public String endBtPrevTaskPageDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btPrevTaskPage_onBeforeHtmlOutputEvent method
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.pageGetState(this);

    boolean rc = handler.generatePrevTaskPage();

    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }

  }

  /**
   *
   *
   */
  public StaticTextField getStPrevTaskPageLabel()
  {
    return(StaticTextField)getChild(CHILD_STPREVTASKPAGELABEL);
  }

  /**
   *
   *
   */
  public String endStPrevTaskPageLabelDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the stPrevTaskPageLabel_onBeforeHtmlOutputEvent method
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.pageGetState(this);

    boolean rc = handler.generatePrevTaskPage();

    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public void handleBtNextTaskPageRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleNextTaskPage();

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public Button getBtNextTaskPage()
  {
    return(Button)getChild(CHILD_BTNEXTTASKPAGE);
  }

  /**
   *
   *
   */
  public String endBtNextTaskPageDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btNextTaskPage_onBeforeHtmlOutputEvent method
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.pageGetState(this);

    boolean rc = handler.generateNextTaskPage();

    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }

  }

  /**
   *
   *
   */
  public StaticTextField getStNextTaskPageLabel()
  {
    return(StaticTextField)getChild(CHILD_STNEXTTASKPAGELABEL);
  }

  /**
   *
   *
   */
  public String endStNextTaskPageLabelDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the stNextTaskPageLabel_onBeforeHtmlOutputEvent method
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.pageGetState(this);

    boolean rc = handler.generateNextTaskPage();

    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }

  }

  /**
   *
   *
   */
  public StaticTextField getStTaskName()
  {
    return(StaticTextField)getChild(CHILD_STTASKNAME);
  }

  /**
   *
   *
   */
  public String endStTaskNameDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the stTaskName_onBeforeHtmlOutputEvent method
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.pageGetState(this);

    boolean rc = handler.generateTaskName();

    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public HiddenField getSessionUserId()
  {
    return(HiddenField)getChild(CHILD_SESSIONUSERID);
  }

  /**
   *
   *
   */
  public StaticTextField getStPartyType()
  {
    return(StaticTextField)getChild(CHILD_STPARTYTYPE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmGenerate()
  {
    return(StaticTextField)getChild(CHILD_STPMGENERATE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmHasTitle()
  {
    return(StaticTextField)getChild(CHILD_STPMHASTITLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmHasInfo()
  {
    return(StaticTextField)getChild(CHILD_STPMHASINFO);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmHasTable()
  {
    return(StaticTextField)getChild(CHILD_STPMHASTABLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmHasOk()
  {
    return(StaticTextField)getChild(CHILD_STPMHASOK);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmTitle()
  {
    return(StaticTextField)getChild(CHILD_STPMTITLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmInfoMsg()
  {
    return(StaticTextField)getChild(CHILD_STPMINFOMSG);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmOnOk()
  {
    return(StaticTextField)getChild(CHILD_STPMONOK);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmMsgs()
  {
    return(StaticTextField)getChild(CHILD_STPMMSGS);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmMsgTypes()
  {
    return(StaticTextField)getChild(CHILD_STPMMSGTYPES);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmGenerate()
  {
    return(StaticTextField)getChild(CHILD_STAMGENERATE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmHasTitle()
  {
    return(StaticTextField)getChild(CHILD_STAMHASTITLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmHasInfo()
  {
    return(StaticTextField)getChild(CHILD_STAMHASINFO);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmHasTable()
  {
    return(StaticTextField)getChild(CHILD_STAMHASTABLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmTitle()
  {
    return(StaticTextField)getChild(CHILD_STAMTITLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmInfoMsg()
  {
    return(StaticTextField)getChild(CHILD_STAMINFOMSG);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmMsgs()
  {
    return(StaticTextField)getChild(CHILD_STAMMSGS);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmMsgTypes()
  {
    return(StaticTextField)getChild(CHILD_STAMMSGTYPES);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmDialogMsg()
  {
    return(StaticTextField)getChild(CHILD_STAMDIALOGMSG);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmButtonsHtml()
  {
    return(StaticTextField)getChild(CHILD_STAMBUTTONSHTML);
  }

  /**
   *
   *
   */
  public StaticTextField getStIncludeDSSstart()
  {
    return(StaticTextField)getChild(CHILD_STINCLUDEDSSSTART);
  }

  /**
   *
   *
   */
  public StaticTextField getStIncludeDSSend()
  {
    return(StaticTextField)getChild(CHILD_STINCLUDEDSSEND);
  }

  /**
   *
   *
   */
  public void handleBtOKRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    //handler.handleOkStandard();
    handler.handleCancelStandard();

    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtOK()
  {
    return(Button)getChild(CHILD_BTOK);
  }

  /**
   *
   *
   */
  public StaticTextField getStPartyStatus()
  {
    return(StaticTextField)getChild(CHILD_STPARTYSTATUS);
  }

  /**
   *
   *
   */
  public StaticTextField getStFaxNumber()
  {
    return(StaticTextField)getChild(CHILD_STFAXNUMBER);
  }

  /**
   *
   *
   */
  public StaticTextField getStPartyCompanyName()
  {
    return(StaticTextField)getChild(CHILD_STPARTYCOMPANYNAME);
  }

  /**
   *
   *
   */
  public StaticTextField getStCity()
  {
    return(StaticTextField)getChild(CHILD_STCITY);
  }

  /**
   *
   *
   */
  public StaticTextField getStProvince()
  {
    return(StaticTextField)getChild(CHILD_STPROVINCE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPartyFirstName()
  {
    return(StaticTextField)getChild(CHILD_STPARTYFIRSTNAME);
  }

  /**
   *
   *
   */
  public StaticTextField getStPartyMiddleInitial()
  {
    return(StaticTextField)getChild(CHILD_STPARTYMIDDLEINITIAL);
  }

  /**
   *
   *
   */
  public StaticTextField getStPartyLastName()
  {
    return(StaticTextField)getChild(CHILD_STPARTYLASTNAME);
  }

  /**
   *
   *
   */
  public StaticTextField getStWorkPhoneNumber()
  {
    return(StaticTextField)getChild(CHILD_STWORKPHONENUMBER);
  }

  /**
   *
   *
   */
  public StaticTextField getStEmail()
  {
    return(StaticTextField)getChild(CHILD_STEMAIL);
  }

  /**
   *
   *
   */
  public StaticTextField getStAddressLine1()
  {
    return(StaticTextField)getChild(CHILD_STADDRESSLINE1);
  }

  /**
   *
   *
   */
  public StaticTextField getStAddressLine2()
  {
    return(StaticTextField)getChild(CHILD_STADDRESSLINE2);
  }

  /**
   *
   *
   */
  public StaticTextField getStPostalCodeFSA()
  {
    return(StaticTextField)getChild(CHILD_STPOSTALCODEFSA);
  }

  /**
   *
   *
   */
  public StaticTextField getStPostalCodeLDU()
  {
    return(StaticTextField)getChild(CHILD_STPOSTALCODELDU);
  }

  /**
   *
   *
   */
  public StaticTextField getStClientNumber()
  {
    return(StaticTextField)getChild(CHILD_STCLIENTNUMBER);
  }

  /**
   *
   *
   */
  public StaticTextField getStPartyShortName()
  {
    return(StaticTextField)getChild(CHILD_STPARTYSHORTNAME);
  }

  /**
   *
   *
   */
  public HiddenField getHdPartyId()
  {
    return(HiddenField)getChild(CHILD_HDPARTYID);
  }

  /**
   *
   *
   */
  public HiddenField getHdPartyTypeId()
  {
    return(HiddenField)getChild(CHILD_HDPARTYTYPEID);
  }

  /**
   *
   *
   */
  public void handleBtModifyPartyRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleAddParty(true);

    handler.postHandlerProtocol();

  }

  /**
   *
   *
   */
  public Button getBtModifyParty()
  {
    return(Button)getChild(CHILD_BTMODIFYPARTY);
  }

  /**
   *
   *
   */
  public String endBtModifyPartyDisplay(ChildContentDisplayEvent event)
  {

    // The following code block was migrated from the btModifyParty_onBeforeHtmlOutputEvent method
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.pageGetState(this);

    String rc = handler.handleViewModifyPartyButton(event);

    handler.pageSaveState();

    return rc;

  }

  /**
   *
   *
   */
  public HiddenField getHdPartyStatusId()
  {
    return(HiddenField)getChild(CHILD_HDPARTYSTATUSID);
  }

  /**
   *
   *
   */
  public StaticTextField getStSpecialFeature()
  {
    return(StaticTextField)getChild(CHILD_STSPECIALFEATURE);
  }

  /**
   *
   *
   */
  public TextField getTbNotes()
  {
    return(TextField)getChild(CHILD_TBNOTES);
  }

  //--DJ_PT_CR--start--//
  /**
   *
   *
   */
  public StaticTextField getStPartyName()
  {
    return(StaticTextField)getChild(CHILD_STPARTYNAME);
  }

  /**
   *
   *
   */
  public StaticTextField getStBranchTransitNumCMHC()
  {
    return(StaticTextField)getChild(CHILD_STBRANCHTRANSITNUMCMHC);
  }

  /**
   *
   *
   */
  public StaticTextField getStBranchTransitNumGE()
  {
    return(StaticTextField)getChild(CHILD_STBRANCHTRANSITNUMGE);
  }

  /**
   *
   *
   */
  public StaticTextField getStClientNumberLabel()
  {
    return(StaticTextField)getChild(CHILD_STCLIENTNUMBERLABEL);
  }

  /**
   *
   *
   */
  public StaticTextField getStIncludeBranchTransitInfoStart()
  {
    return(StaticTextField)getChild(CHILD_STINCLUDEBRANCHTRANSITINFOSTART);
  }

  /**
   *
   *
   */
  public StaticTextField getStIncludeBranchTransitInfoEnd()
  {
    return(StaticTextField)getChild(CHILD_STINCLUDEBRANCHTRANSITINFOEND);
  }

  //--DJ_PT_CR--end--//

  //--DJ_PREFCOMMETHOD_CR--start--//
  /**
   *
   *
   */
  public StaticTextField getStPrefDeliveryMethod()
  {
    return(StaticTextField)getChild(CHILD_STPREFDELIVERYMETHOD);
  }

  //--DJ_PREFCOMMETHOD_CR--end--//
  /**
  * getStLocation
  * 	Returns the StaticTextField for location
  * 
  * @param none
  * @return StaticTextField
  * @version 1.0 (Initial Version - 31 May 2006)
  */
  public StaticTextField getStLocation()
  {
	  return(StaticTextField)getChild(CHILD_STLOCATION);
  }

  /**
   *
   *
   */
  public doDealSummarySnapShotModel getdoDealSummarySnapShotModel()
  {
    if(doDealSummarySnapShot == null)
    {
      doDealSummarySnapShot = (doDealSummarySnapShotModel)getModel(doDealSummarySnapShotModel.class);
    }
    return doDealSummarySnapShot;
  }

  /**
   *
   *
   */
  public void setdoDealSummarySnapShotModel(doDealSummarySnapShotModel model)
  {
    doDealSummarySnapShot = model;
  }

  /**
   *
   *
   */
  public doPartySearchModel getdoPartySearchModel()
  {
    if(doPartySearch == null)
    {
      doPartySearch = (doPartySearchModel)getModel(doPartySearchModel.class);
    }
    return doPartySearch;
  }

  /**
   *
   *
   */
  public void setdoPartySearchModel(doPartySearchModel model)
  {
    doPartySearch = model;
  }

  //// New hidden button for the ActiveMessage 'fake' submit button.
  public Button getBtActMsg()
  {
    return(Button)getChild(CHILD_BTACTMSG);
  }

  //// Populate previous links new set of methods to populate Href display Strings.
  public String endHref1Display(ChildContentDisplayEvent event)
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 0);
    handler.pageSaveState();

    return rc;
  }

  public String endHref2Display(ChildContentDisplayEvent event)
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 1);
    handler.pageSaveState();

    return rc;
  }

  public String endHref3Display(ChildContentDisplayEvent event)
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 2);
    handler.pageSaveState();

    return rc;
  }

  public String endHref4Display(ChildContentDisplayEvent event)
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 3);
    handler.pageSaveState();

    return rc;
  }

  public String endHref5Display(ChildContentDisplayEvent event)
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 4);
    handler.pageSaveState();

    return rc;
  }

  public String endHref6Display(ChildContentDisplayEvent event)
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 5);
    handler.pageSaveState();

    return rc;
  }

  public String endHref7Display(ChildContentDisplayEvent event)
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 6);
    handler.pageSaveState();

    return rc;
  }

  public String endHref8Display(ChildContentDisplayEvent event)
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 7);
    handler.pageSaveState();

    return rc;
  }

  public String endHref9Display(ChildContentDisplayEvent event)
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 8);
    handler.pageSaveState();

    return rc;
  }

  public String endHref10Display(ChildContentDisplayEvent event)
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();
    handler.pageGetState(this);
    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 9);
    handler.pageSaveState();

    return rc;
  }

  //// This method overrides the getDefaultURL() framework JATO method and it is located
  //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
  //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
  //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
  //// The full method is still in PHC base class. It should care the
  //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
  public String getDisplayURL()
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();
    handler.pageGetState(this);

    String url = getDefaultDisplayURL();

    int languageId = handler.theSessionState.getLanguageId();

    //// Call the language specific URL (business delegation done in the BXResource).
    if(url != null && !url.trim().equals("") && !url.trim().equals("/"))
    {
      url = BXResources.getBXUrl(url, languageId);
    }
    else
    {
      url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
    }

    return url;
  }

  //--> Ticket#369 :: Change for Cervus :: added New Bank info for Party
  //--> By Billy 21May2004
  public StaticTextField getStBankTransitNum()
  {
    return(StaticTextField)getChild(CHILD_STBANKTRANSITNUM);
  }

  public StaticTextField getStBankAccNum()
  {
    return(StaticTextField)getChild(CHILD_STBANKACCNUM);
  }

  public StaticTextField getStBankName()
  {
    return(StaticTextField)getChild(CHILD_STBANKNAME);
  }

  public StaticTextField getStIncludeBankInfoStart()
  {
    return(StaticTextField)getChild(CHILD_STINCLUDEBANKINFOSTART);
  }

  public StaticTextField getStIncludeBankInfoEnd()
  {
    return(StaticTextField)getChild(CHILD_STINCLUDEBANKINFOEND);
  }

  //=====================================================================

  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////
  // Custom Methods - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////


  public void handleActMessageOK(String[] args)
  {
    PartySearchHandler handler = (PartySearchHandler)this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleActMessageOK(args);

    handler.postHandlerProtocol();

  }

  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////

  public static final String PAGE_NAME = "pgPartyReview";

  //// It is a variable now (see explanation in the getDisplayURL() method above.
  ////public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgPartyReview.jsp";
  public static Map FIELD_DESCRIPTORS;
  public static final String CHILD_TBDEALID = "tbDealId";
  public static final String CHILD_TBDEALID_RESET_VALUE = "";
  public static final String CHILD_CBPAGENAMES = "cbPageNames";
  public static final String CHILD_CBPAGENAMES_RESET_VALUE = "";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 05Dec2002
  //private static CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  private CbPageNamesOptionList cbPageNamesOptions = new CbPageNamesOptionList();

  //Added the Variable for NonSelected Label
  private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";

  //==================================================================

  public static final String CHILD_BTPROCEED = "btProceed";
  public static final String CHILD_BTPROCEED_RESET_VALUE = "Proceed";
  public static final String CHILD_HREF1 = "Href1";
  public static final String CHILD_HREF1_RESET_VALUE = "";
  public static final String CHILD_HREF2 = "Href2";
  public static final String CHILD_HREF2_RESET_VALUE = "";
  public static final String CHILD_HREF3 = "Href3";
  public static final String CHILD_HREF3_RESET_VALUE = "";
  public static final String CHILD_HREF4 = "Href4";
  public static final String CHILD_HREF4_RESET_VALUE = "";
  public static final String CHILD_HREF5 = "Href5";
  public static final String CHILD_HREF5_RESET_VALUE = "";
  public static final String CHILD_HREF6 = "Href6";
  public static final String CHILD_HREF6_RESET_VALUE = "";
  public static final String CHILD_HREF7 = "Href7";
  public static final String CHILD_HREF7_RESET_VALUE = "";
  public static final String CHILD_HREF8 = "Href8";
  public static final String CHILD_HREF8_RESET_VALUE = "";
  public static final String CHILD_HREF9 = "Href9";
  public static final String CHILD_HREF9_RESET_VALUE = "";
  public static final String CHILD_HREF10 = "Href10";
  public static final String CHILD_HREF10_RESET_VALUE = "";
  public static final String CHILD_STPAGELABEL = "stPageLabel";
  public static final String CHILD_STPAGELABEL_RESET_VALUE = "";
  public static final String CHILD_STCOMPANYNAME = "stCompanyName";
  public static final String CHILD_STCOMPANYNAME_RESET_VALUE = "";
  public static final String CHILD_STTODAYDATE = "stTodayDate";
  public static final String CHILD_STTODAYDATE_RESET_VALUE = "";
  public static final String CHILD_STUSERNAMETITLE = "stUserNameTitle";
  public static final String CHILD_STUSERNAMETITLE_RESET_VALUE = "";
  public static final String CHILD_STDEALID = "stDealId";
  public static final String CHILD_STDEALID_RESET_VALUE = "";
  public static final String CHILD_STBORRFIRSTNAME = "stBorrFirstName";
  public static final String CHILD_STBORRFIRSTNAME_RESET_VALUE = "";
  public static final String CHILD_STDEALSTATUS = "stDealStatus";
  public static final String CHILD_STDEALSTATUS_RESET_VALUE = "";
  public static final String CHILD_STDEALSTATUSDATE = "stDealStatusDate";
  public static final String CHILD_STDEALSTATUSDATE_RESET_VALUE = "";
  public static final String CHILD_STSOURCEFIRM = "stSourceFirm";
  public static final String CHILD_STSOURCEFIRM_RESET_VALUE = "";
  public static final String CHILD_STSOURCE = "stSource";
  public static final String CHILD_STSOURCE_RESET_VALUE = "";
  public static final String CHILD_STLOB = "stLOB";
  public static final String CHILD_STLOB_RESET_VALUE = "";
  public static final String CHILD_STDEALTYPE = "stDealType";
  public static final String CHILD_STDEALTYPE_RESET_VALUE = "";
  public static final String CHILD_STDEALPURPOSE = "stDealPurpose";
  public static final String CHILD_STDEALPURPOSE_RESET_VALUE = "";
  public static final String CHILD_STPURCHASEPRICE = "stPurchasePrice";
  public static final String CHILD_STPURCHASEPRICE_RESET_VALUE = "";
  public static final String CHILD_STPMTTERM = "stPmtTerm";
  public static final String CHILD_STPMTTERM_RESET_VALUE = "";
  public static final String CHILD_STESTCLOSINGDATE = "stEstClosingDate";
  public static final String CHILD_STESTCLOSINGDATE_RESET_VALUE = "";
  public static final String CHILD_BTWORKQUEUELINK = "btWorkQueueLink";
  public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE = " ";
  public static final String CHILD_CHANGEPASSWORDHREF = "changePasswordHref";
  public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE = "";
  public static final String CHILD_BTTOOLHISTORY = "btToolHistory";
  public static final String CHILD_BTTOOLHISTORY_RESET_VALUE = " ";
  public static final String CHILD_BTTOONOTES = "btTooNotes";
  public static final String CHILD_BTTOONOTES_RESET_VALUE = " ";
  public static final String CHILD_BTTOOLSEARCH = "btToolSearch";
  public static final String CHILD_BTTOOLSEARCH_RESET_VALUE = " ";
  public static final String CHILD_BTTOOLLOG = "btToolLog";
  public static final String CHILD_BTTOOLLOG_RESET_VALUE = " ";
  public static final String CHILD_STERRORFLAG = "stErrorFlag";
  public static final String CHILD_STERRORFLAG_RESET_VALUE = "N";
  public static final String CHILD_DETECTALERTTASKS = "detectAlertTasks";
  public static final String CHILD_DETECTALERTTASKS_RESET_VALUE = "";
  public static final String CHILD_STTOTALLOANAMOUNT = "stTotalLoanAmount";
  public static final String CHILD_STTOTALLOANAMOUNT_RESET_VALUE = "";
  public static final String CHILD_BTPREVTASKPAGE = "btPrevTaskPage";
  public static final String CHILD_BTPREVTASKPAGE_RESET_VALUE = " ";
  public static final String CHILD_STPREVTASKPAGELABEL = "stPrevTaskPageLabel";
  public static final String CHILD_STPREVTASKPAGELABEL_RESET_VALUE = "";
  public static final String CHILD_BTNEXTTASKPAGE = "btNextTaskPage";
  public static final String CHILD_BTNEXTTASKPAGE_RESET_VALUE = " ";
  public static final String CHILD_STNEXTTASKPAGELABEL = "stNextTaskPageLabel";
  public static final String CHILD_STNEXTTASKPAGELABEL_RESET_VALUE = "";
  public static final String CHILD_STTASKNAME = "stTaskName";
  public static final String CHILD_STTASKNAME_RESET_VALUE = "";
  public static final String CHILD_SESSIONUSERID = "sessionUserId";
  public static final String CHILD_SESSIONUSERID_RESET_VALUE = "";
  public static final String CHILD_STPARTYTYPE = "stPartyType";
  public static final String CHILD_STPARTYTYPE_RESET_VALUE = "";
  public static final String CHILD_STPMGENERATE = "stPmGenerate";
  public static final String CHILD_STPMGENERATE_RESET_VALUE = "";
  public static final String CHILD_STPMHASTITLE = "stPmHasTitle";
  public static final String CHILD_STPMHASTITLE_RESET_VALUE = "";
  public static final String CHILD_STPMHASINFO = "stPmHasInfo";
  public static final String CHILD_STPMHASINFO_RESET_VALUE = "";
  public static final String CHILD_STPMHASTABLE = "stPmHasTable";
  public static final String CHILD_STPMHASTABLE_RESET_VALUE = "";
  public static final String CHILD_STPMHASOK = "stPmHasOk";
  public static final String CHILD_STPMHASOK_RESET_VALUE = "";
  public static final String CHILD_STPMTITLE = "stPmTitle";
  public static final String CHILD_STPMTITLE_RESET_VALUE = "";
  public static final String CHILD_STPMINFOMSG = "stPmInfoMsg";
  public static final String CHILD_STPMINFOMSG_RESET_VALUE = "";
  public static final String CHILD_STPMONOK = "stPmOnOk";
  public static final String CHILD_STPMONOK_RESET_VALUE = "";
  public static final String CHILD_STPMMSGS = "stPmMsgs";
  public static final String CHILD_STPMMSGS_RESET_VALUE = "";
  public static final String CHILD_STPMMSGTYPES = "stPmMsgTypes";
  public static final String CHILD_STPMMSGTYPES_RESET_VALUE = "";
  public static final String CHILD_STAMGENERATE = "stAmGenerate";
  public static final String CHILD_STAMGENERATE_RESET_VALUE = "";
  public static final String CHILD_STAMHASTITLE = "stAmHasTitle";
  public static final String CHILD_STAMHASTITLE_RESET_VALUE = "";
  public static final String CHILD_STAMHASINFO = "stAmHasInfo";
  public static final String CHILD_STAMHASINFO_RESET_VALUE = "";
  public static final String CHILD_STAMHASTABLE = "stAmHasTable";
  public static final String CHILD_STAMHASTABLE_RESET_VALUE = "";
  public static final String CHILD_STAMTITLE = "stAmTitle";
  public static final String CHILD_STAMTITLE_RESET_VALUE = "";
  public static final String CHILD_STAMINFOMSG = "stAmInfoMsg";
  public static final String CHILD_STAMINFOMSG_RESET_VALUE = "";
  public static final String CHILD_STAMMSGS = "stAmMsgs";
  public static final String CHILD_STAMMSGS_RESET_VALUE = "";
  public static final String CHILD_STAMMSGTYPES = "stAmMsgTypes";
  public static final String CHILD_STAMMSGTYPES_RESET_VALUE = "";
  public static final String CHILD_STAMDIALOGMSG = "stAmDialogMsg";
  public static final String CHILD_STAMDIALOGMSG_RESET_VALUE = "";
  public static final String CHILD_STAMBUTTONSHTML = "stAmButtonsHtml";
  public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE = "";
  public static final String CHILD_STINCLUDEDSSSTART = "stIncludeDSSstart";
  public static final String CHILD_STINCLUDEDSSSTART_RESET_VALUE = "";
  public static final String CHILD_STINCLUDEDSSEND = "stIncludeDSSend";
  public static final String CHILD_STINCLUDEDSSEND_RESET_VALUE = "";
  public static final String CHILD_BTOK = "btOK";
  public static final String CHILD_BTOK_RESET_VALUE = "OK";
  public static final String CHILD_STPARTYSTATUS = "stPartyStatus";
  public static final String CHILD_STPARTYSTATUS_RESET_VALUE = "";
  public static final String CHILD_STFAXNUMBER = "stFaxNumber";
  public static final String CHILD_STFAXNUMBER_RESET_VALUE = "";
  public static final String CHILD_STPARTYCOMPANYNAME = "stPartyCompanyName";
  public static final String CHILD_STPARTYCOMPANYNAME_RESET_VALUE = "";
  public static final String CHILD_STCITY = "stCity";
  public static final String CHILD_STCITY_RESET_VALUE = "";
  public static final String CHILD_STPROVINCE = "stProvince";
  public static final String CHILD_STPROVINCE_RESET_VALUE = "";
  public static final String CHILD_STPARTYFIRSTNAME = "stPartyFirstName";
  public static final String CHILD_STPARTYFIRSTNAME_RESET_VALUE = "";
  public static final String CHILD_STPARTYMIDDLEINITIAL = "stPartyMiddleInitial";
  public static final String CHILD_STPARTYMIDDLEINITIAL_RESET_VALUE = "";
  public static final String CHILD_STPARTYLASTNAME = "stPartyLastName";
  public static final String CHILD_STPARTYLASTNAME_RESET_VALUE = "";
  public static final String CHILD_STWORKPHONENUMBER = "stWorkPhoneNumber";
  public static final String CHILD_STWORKPHONENUMBER_RESET_VALUE = "";
  public static final String CHILD_STEMAIL = "stEmail";
  public static final String CHILD_STEMAIL_RESET_VALUE = "";
  public static final String CHILD_STADDRESSLINE1 = "stAddressLine1";
  public static final String CHILD_STADDRESSLINE1_RESET_VALUE = "";
  public static final String CHILD_STADDRESSLINE2 = "stAddressLine2";
  public static final String CHILD_STADDRESSLINE2_RESET_VALUE = "";
  public static final String CHILD_STPOSTALCODEFSA = "stPostalCodeFSA";
  public static final String CHILD_STPOSTALCODEFSA_RESET_VALUE = "";
  public static final String CHILD_STPOSTALCODELDU = "stPostalCodeLDU";
  public static final String CHILD_STPOSTALCODELDU_RESET_VALUE = "";
  public static final String CHILD_STCLIENTNUMBER = "stClientNumber";
  public static final String CHILD_STCLIENTNUMBER_RESET_VALUE = "";
  public static final String CHILD_STPARTYSHORTNAME = "stPartyShortName";
  public static final String CHILD_STPARTYSHORTNAME_RESET_VALUE = "";
  public static final String CHILD_HDPARTYID = "hdPartyId";
  public static final String CHILD_HDPARTYID_RESET_VALUE = "";
  public static final String CHILD_HDPARTYTYPEID = "hdPartyTypeId";
  public static final String CHILD_HDPARTYTYPEID_RESET_VALUE = "";
  public static final String CHILD_BTMODIFYPARTY = "btModifyParty";
  public static final String CHILD_BTMODIFYPARTY_RESET_VALUE = "Custom";
  public static final String CHILD_HDPARTYSTATUSID = "hdPartyStatusId";
  public static final String CHILD_HDPARTYSTATUSID_RESET_VALUE = "";
  public static final String CHILD_STSPECIALFEATURE = "stSpecialFeature";
  public static final String CHILD_STSPECIALFEATURE_RESET_VALUE = "";
  public static final String CHILD_TBNOTES = "tbNotes";
  public static final String CHILD_TBNOTES_RESET_VALUE = "";

  //// New hidden button implementing the 'fake' one from the ActiveMessage class.
  public static final String CHILD_BTACTMSG = "btActMsg";
  public static final String CHILD_BTACTMSG_RESET_VALUE = "ActMsg";

  //--Release2.1--//
  //// New link to toggle the language. When touched this link should reload the
  //// page in opposite language (french versus english) and set this new language
  //// session id throughout all modulus.
  public static final String CHILD_TOGGLELANGUAGEHREF = "toggleLanguageHref";
  public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE = "";

  //--DJ_PT_CR--start--//
  public static final String CHILD_STBRANCHTRANSITNUMCMHC = "stBranchTransitNumCMHC";
  public static final String CHILD_STBRANCHTRANSITNUMCMHC_RESET_VALUE = "";

  public static final String CHILD_STBRANCHTRANSITNUMGE = "stBranchTransitNumGE";
  public static final String CHILD_STBRANCHTRANSITNUMGE_RESET_VALUE = "";

  public static final String CHILD_STPARTYNAME = "stPartyName";
  public static final String CHILD_STPARTYNAME_RESET_VALUE = "";

  public static final String CHILD_STPARTYSHORTNAMELABEL = "stPartyShortNameLabel";
  public static final String CHILD_STPARTYSHORTNAMELABEL_RESET_VALUE = "";

  public static final String CHILD_STCLIENTNUMBERLABEL = "stClientNumberLabel";
  public static final String CHILD_STCLIENTNUMBERLABEL_RESET_VALUE = "";

  public static final String CHILD_STINCLUDEBRANCHTRANSITINFOSTART = "stIncludeBranchTransitInfoStart";
  public static final String CHILD_STINCLUDEBRANCHTRANSITINFOSTART_RESET_VALUE = "";

  public static final String CHILD_STINCLUDEBRANCHTRANSITINFOEND = "stIncludeBranchTransitInfoEnd";
  public static final String CHILD_STINCLUDEBRANCHTRANSITINFOEND_RESET_VALUE = "";

  //--DJ_PT_CR--end--//

  //--DJ_PREFCOMMETHOD_CR--start--//
  public static final String CHILD_STPREFDELIVERYMETHOD = "stPrefDeliveryMethod";
  public static final String CHILD_STPREFDELIVERYMETHOD_RESET_VALUE = "";

  //--DJ_PREFCOMMETHOD_CR--end--//

  //--> Ticket#369 :: Change for Cervus :: added New Bank info for Party
  //--> By Billy 21May2004
  public static final String CHILD_STBANKTRANSITNUM = "stBankTransitNum";
  public static final String CHILD_STBANKTRANSITNUM_RESET_VALUE = "";
  public static final String CHILD_STBANKACCNUM = "stBankAccNum";
  public static final String CHILD_STBANKACCNUM_RESET_VALUE = "";
  public static final String CHILD_STBANKNAME = "stBankName";
  public static final String CHILD_STBANKNAME_RESET_VALUE = "";
  public static final String CHILD_STINCLUDEBANKINFOSTART = "stIncludeBankInfoStart";
  public static final String CHILD_STINCLUDEBANKINFOSTART_RESET_VALUE = "";
  public static final String CHILD_STINCLUDEBANKINFOEND = "stIncludeBankInfoEnd";
  public static final String CHILD_STINCLUDEBANKINFOEND_RESET_VALUE = "";
  //====================================================================
  //***** Change by NBC/PP Implementation Team - Version 1.1 - START *****//
  public static final String CHILD_STLOCATION = "stLocation";
  public static final String CHILD_STLOCATION_RESET_VALUE = "";
  //***** Change by NBC/PP Implementation Team - Version 1.1 - END *****//  

  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////

  private doDealSummarySnapShotModel doDealSummarySnapShot = null;
  private doPartySearchModel doPartySearch = null;
  protected String DEFAULT_DISPLAY_URL = "/mosApp/MosSystem/pgPartyReview.jsp";

  ////////////////////////////////////////////////////////////////////////////
  // Custom Members - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////

  // MigrationToDo : Migrate custom member
  private PartySearchHandler handler = new PartySearchHandler();

}
