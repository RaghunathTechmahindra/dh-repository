package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doApplicantEmploymentIncomeSelectModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfEmploymentHistoryId();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmploymentHistoryId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfContactId();

	
	/**
	 * 
	 * 
	 */
	public void setDfContactId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfContactCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfContactCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAddressId();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddressId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAddrCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddrCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIncomeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIncomeCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmployerName();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmployerName(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfEmploymentStatusId();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmploymentStatusId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfEmploymentTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmploymentTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmployerPhoneNo();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmployerPhoneNo(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfContactPhoneNoExtension();

	
	/**
	 * 
	 * 
	 */
	public void setDfContactPhoneNoExtension(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmployerFaxNo();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmployerFaxNo(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmployerEmailAddress();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmployerEmailAddress(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmployerAddressLine1();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmployerAddressLine1(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmployerCity();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmployerCity(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfProvinceId();

	
	/**
	 * 
	 * 
	 */
	public void setDfProvinceId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmployerPostalFSA();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmployerPostalFSA(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmployerPostalLDU();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmployerPostalLDU(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmployerAddressLine2();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmployerAddressLine2(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIndustrySectorId();

	
	/**
	 * 
	 * 
	 */
	public void setDfIndustrySectorId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfOccupationId();

	
	/**
	 * 
	 * 
	 */
	public void setDfOccupationId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfJobTittle();

	
	/**
	 * 
	 * 
	 */
	public void setDfJobTittle(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTimeAtJob();

	
	/**
	 * 
	 * 
	 */
	public void setDfTimeAtJob(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIncomeTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfIncomeDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIncomePeriodId();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomePeriodId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIncomeAmount();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeAmount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfIncludeInGDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncludeInGDS(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPercentIncludedInGDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfPercentIncludedInGDS(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfIncludeInTDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncludeInTDS(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPercentIncludedInTDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfPercentIncludedInTDS(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfJobTitleID();

	
	/**
	 * 
	 * 
	 */
	public void setDfJobTitleID(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFEMPLOYMENTHISTORYID="dfEmploymentHistoryId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFCONTACTID="dfContactId";
	public static final String FIELD_DFCONTACTCOPYID="dfContactCopyId";
	public static final String FIELD_DFADDRESSID="dfAddressId";
	public static final String FIELD_DFADDRCOPYID="dfAddrCopyId";
	public static final String FIELD_DFINCOMEID="dfIncomeId";
	public static final String FIELD_DFINCOMECOPYID="dfIncomeCopyId";
	public static final String FIELD_DFEMPLOYERNAME="dfEmployerName";
	public static final String FIELD_DFEMPLOYMENTSTATUSID="dfEmploymentStatusId";
	public static final String FIELD_DFEMPLOYMENTTYPEID="dfEmploymentTypeId";
	public static final String FIELD_DFEMPLOYERPHONENO="dfEmployerPhoneNo";
	public static final String FIELD_DFCONTACTPHONENOEXTENSION="dfContactPhoneNoExtension";
	public static final String FIELD_DFEMPLOYERFAXNO="dfEmployerFaxNo";
	public static final String FIELD_DFEMPLOYEREMAILADDRESS="dfEmployerEmailAddress";
	public static final String FIELD_DFEMPLOYERADDRESSLINE1="dfEmployerAddressLine1";
	public static final String FIELD_DFEMPLOYERCITY="dfEmployerCity";
	public static final String FIELD_DFPROVINCEID="dfProvinceId";
	public static final String FIELD_DFEMPLOYERPOSTALFSA="dfEmployerPostalFSA";
	public static final String FIELD_DFEMPLOYERPOSTALLDU="dfEmployerPostalLDU";
	public static final String FIELD_DFEMPLOYERADDRESSLINE2="dfEmployerAddressLine2";
	public static final String FIELD_DFINDUSTRYSECTORID="dfIndustrySectorId";
	public static final String FIELD_DFOCCUPATIONID="dfOccupationId";
	public static final String FIELD_DFJOBTITTLE="dfJobTittle";
	public static final String FIELD_DFTIMEATJOB="dfTimeAtJob";
	public static final String FIELD_DFINCOMETYPEID="dfIncomeTypeId";
	public static final String FIELD_DFINCOMEDESC="dfIncomeDesc";
	public static final String FIELD_DFINCOMEPERIODID="dfIncomePeriodId";
	public static final String FIELD_DFINCOMEAMOUNT="dfIncomeAmount";
	public static final String FIELD_DFINCLUDEINGDS="dfIncludeInGDS";
	public static final String FIELD_DFPERCENTINCLUDEDINGDS="dfPercentIncludedInGDS";
	public static final String FIELD_DFINCLUDEINTDS="dfIncludeInTDS";
	public static final String FIELD_DFPERCENTINCLUDEDINTDS="dfPercentIncludedInTDS";
	public static final String FIELD_DFJOBTITLEID="dfJobTitleID";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

