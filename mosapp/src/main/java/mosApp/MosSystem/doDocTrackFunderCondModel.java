package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doDocTrackFunderCondModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public String getDfDocumentLabel();


	/**
	 *
	 *
	 */
	public void setDfDocumentLabel(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDocStatusId();


	/**
	 *
	 *
	 */
	public void setDfDocStatusId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfDocDueDate();


	/**
	 *
	 *
	 */
	public void setDfDocDueDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public String getDfByPass();


	/**
	 *
	 *
	 */
	public void setDfByPass(String value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfStatusChangeDate();


	/**
	 *
	 *
	 */
	public void setDfStatusChangeDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public String getDfResponsibility();


	/**
	 *
	 *
	 */
	public void setDfResponsibility(String value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfRequestDate();


	/**
	 *
	 *
	 */
	public void setDfRequestDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public String getDfStatus();


	/**
	 *
	 *
	 */
	public void setDfStatus(String value);


	/**
	 *
	 *
	 */
	public String getDfDocumentText();


	/**
	 *
	 *
	 */
	public void setDfDocumentText(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDocTrackId();


	/**
	 *
	 *
	 */
	public void setDfDocTrackId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRoleId();


	/**
	 *
	 *
	 */
	public void setDfRoleId(java.math.BigDecimal value);




	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDOCUMENTLABEL="dfDocumentLabel";
	public static final String FIELD_DFDOCSTATUSID="dfDocStatusId";
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFDOCDUEDATE="dfDocDueDate";
	public static final String FIELD_DFBYPASS="dfByPass";
	public static final String FIELD_DFSTATUSCHANGEDATE="dfStatusChangeDate";
	public static final String FIELD_DFRESPONSIBILITY="dfResponsibility";
	public static final String FIELD_DFREQUESTDATE="dfRequestDate";
	public static final String FIELD_DFSTATUS="dfStatus";
	public static final String FIELD_DFDOCUMENTTEXT="dfDocumentText";
	//================================================================
	//CLOB Performance Enhancement June 2010
	public static final String FIELD_DFDOCUMENTTEXTLITE="dfDocumentTextLite";
	//================================================================
	public static final String FIELD_DFDOCTRACKID="dfDocTrackId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFROLEID="dfRoleId";

	//--Release2.1--//
  //// Added LanguagePreferenceId DataField
	public static final String FIELD_DFLANGUAGEPREFERENCEID="dfLanguagePrederenceId";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

