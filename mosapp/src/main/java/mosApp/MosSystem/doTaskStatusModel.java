package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doTaskStatusModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public Integer getMOS_TASKSTATUS_TASKSTATUSID();

	
	/**
	 * 
	 * 
	 */
	public void setMOS_TASKSTATUS_TASKSTATUSID(Integer value);

	
	/**
	 * 
	 * 
	 */
	public String getMOS_TASKSTATUS_TSDESCRIPTION();

	
	/**
	 * 
	 * 
	 */
	public void setMOS_TASKSTATUS_TSDESCRIPTION(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_MOS_TASKSTATUS_TASKSTATUSID="MOS_TASKSTATUS_TASKSTATUSID";
	public static final String FIELD_MOS_TASKSTATUS_TSDESCRIPTION="MOS_TASKSTATUS_TSDESCRIPTION";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

