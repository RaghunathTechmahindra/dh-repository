package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doUWSourceInformationModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfSourceBusinessProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfSourceBusinessProfileId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSourceName();

	
	/**
	 * 
	 * 
	 */
	public void setDfSourceName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSourceFirm();

	
	/**
	 * 
	 * 
	 */
	public void setDfSourceFirm(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSourceAddress1();

	
	/**
	 * 
	 * 
	 */
	public void setDfSourceAddress1(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSourceCity();

	
	/**
	 * 
	 * 
	 */
	public void setDfSourceCity(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSourceProvince();

	
	/**
	 * 
	 * 
	 */
	public void setDfSourceProvince(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSourceContactPhone();

	
	/**
	 * 
	 * 
	 */
	public void setDfSourceContactPhone(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSourceContactPhoneExt();

	
	/**
	 * 
	 * 
	 */
	public void setDfSourceContactPhoneExt(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSourceContactFax();

	
	/**
	 * 
	 * 
	 */
	public void setDfSourceContactFax(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFSOURCEBUSINESSPROFILEID="dfSourceBusinessProfileId";
	public static final String FIELD_DFSOURCENAME="dfSourceName";
	public static final String FIELD_DFSOURCEFIRM="dfSourceFirm";
	public static final String FIELD_DFSOURCEADDRESS1="dfSourceAddress1";
	public static final String FIELD_DFSOURCECITY="dfSourceCity";
	public static final String FIELD_DFSOURCEPROVINCE="dfSourceProvince";
	public static final String FIELD_DFSOURCECONTACTPHONE="dfSourceContactPhone";
	public static final String FIELD_DFSOURCECONTACTPHONEEXT="dfSourceContactPhoneExt";
	public static final String FIELD_DFSOURCECONTACTFAX="dfSourceContactFax";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

