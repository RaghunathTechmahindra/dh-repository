package mosApp.MosSystem;

/**
 * 23/May/2006 DVG #DG426 #3242  apostrophe quadruples when used in notes
 */


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;

import org.apache.commons.lang.StringUtils;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.aag.AAGuideline;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.commitment.CCM;
import com.basis100.deal.conditions.ConditionRegExParser;
import com.basis100.deal.docprep.Dc;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealNotes;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.SourceFirmProfilePK;
import com.basis100.deal.security.ActiveMessage;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.security.DealStatusManager;
import com.basis100.deal.util.BusinessCalendar;
import com.basis100.deal.util.DBA;
import com.basis100.deal.validation.DocBusinessRuleExecutor;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.WFEExternalTaskCompletion;
import com.basis100.workflow.WFTaskExecServices;
import com.filogix.express.email.EmailSender;
import com.filogix.externallinks.condition.management.ConditionUpdateRequester;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.externallinks.services.ExchangeFolderCreator;
import com.filogix.util.Xc;
import com.iplanet.jato.view.DisplayField;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.RadioButtonGroup;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

////SYNCADD
//==================================================================================
// Note :
//
//  PageCondition1 : true - indicate redisplay of screen
//  PageState1  : Store the Broker Note input on screen
//  PageState2  : Store Servicing Number inout on screen
//==================================================================================
//// Hide approve option. If user comes to this screen from GoTo menu or UW screen
//// this section should be hidden.
//===================================================================================
//
//  PageCondition2 : true - indicates hiding of the approval section.
//===================================================================================
//
//  PageCondition3 : true - indicates that user responded to "do you want to generate decline letter?" dialog
//===================================================================================
//
//  PageCondition4 : true - indicates that user wants the decline letter to be generated
//===================================================================================
//
//  PageCondition5 : true - same value as local variable autoMIRequest in finalApprovalActivities() but available at a page context
//===================================================================================

/**
 *
 *
 */
public class DealResolutionHandler extends PageHandlerCommon implements
        Cloneable, Sc
{
    //public static final String[] RES_Received_LABELS={ "Approve Deal", "Hold,
    // Pending Information", "Deny", "Collapse Deal"};
    public static final int[] RES_Received_STATUS = {
            Mc.DEAL_APPROVAL_RECOMMENDED, 0, Mc.DEAL_DENIED, Mc.DEAL_COLLAPSED };

    //private static final String[] CONST_MONTH_NAMES={ "", "January",
    // "February", "March", "April", "May", "June", "July", "August",
    // "September", "October", "November", "December"};
    //private static final int[] CONST_MONTH_VALUES={ 0, 1, 2, 3, 4, 5, 6, 7,
    // 8, 9, 10, 11, 12};
    // general purpose
    public static final int UNDEFINED = -1;

    // resolution selection
    public static final int APPROVAL = 1;

    public static final int HOLD = 2;

    public static final int DENY = 3;

    public static final int COLLAPSE = 4;

    // approval options
    public static final int APPROVE = 0;

    public static final int APPROVE_WITH_CHANGE = 1;

    public static final int APPROVE_REQUEST_EXTERNAL_APPROVAL = 2;

    public static final int APPROVE_EXTERNAL_APPROVAL_GRANTED = 3;

    // hold options
    public static final int HOLDFORDAYS = 5;

    public static final int HOLDUNTILDATE = 6;

    //--DocCentral_FolderCreate--21Sep--2004--end--//
    public final static String DEAL_FOLDER_DOC_CENTRAL_CREATION_FAILED = "Deal Folder creation failed in DocCentral";

    /**
     *
     *
     */
    public DealResolutionHandler cloneSS()
    {
        return (DealResolutionHandler) super.cloneSafeShallow();
    }

    /**
     *
     *
     */
    public void populatePageDisplayFields()
    {
        PageEntry pg = theSessionState.getCurrentPage();

        Hashtable pst = pg.getPageStateTable();

        ViewBean thePage = getCurrNDPage();

        populatePageShellDisplayFields();

        // Quick Link Menu display
        displayQuickLinkMenu();

        populateTaskNavigator(pg);

        populatePageDealSummarySnapShot();

        populatePreviousPagesLinks();

        //// Data validation API.
        try
        {

            String dealResolValidPath = Sc.DVALID_DEAL_RESOLUTION_PROPS_FILE_NAME;

            //logger.debug("DRH@dealResolJSDataValid: " + dealResolValidPath);
            JSValidationRules dealResolValidObj = new JSValidationRules(
                    dealResolValidPath, logger, thePage);
            dealResolValidObj.attachJSValidationRules();
        } catch (Exception e)
        {
            logger
                    .error("Exception DRH@PopulatePageDisplayFields: Problem encountered in JS data validation API."
                            + e.getMessage());
        }

        populateResolutionAndDenialOptions(thePage);

        // populate the Broker Note which stored in PageState1
        thePage.setDisplayFieldValue("tbBrokerNoteText", new String(pg
                .getPageState1()));

        ////SYNCADD.
        // Populate the Servicing Number which stored in PageState2 -- By Billy
        // 10June2002
        thePage.setDisplayFieldValue("tbServMortgNum", new String(pg
                .getPageState2()));
    }

    /**
     *
     *
     */
    public void setupBeforePageGeneration()
    {
        PageEntry pg = theSessionState.getCurrentPage();

        if (pg.getSetupBeforeGenerationCalled() == true)
        {
            return;
        }

        pg.setSetupBeforeGenerationCalled(true);

        // -- //
        setupDealSummarySnapShotDO(pg);

        //SYNCADD.
        // Populate Servicing Number -- By Billy 10June2002
        // Note : set it to PageState2 ==> will populate in RowDispaly handler
        if ((pg.getPageCondition1() == false)
                && (isDisplayServicingNumField() == true))
        {
            try
            {
                String tmpStr = (new Deal(srk, null, pg.getPageDealId(), pg
                        .getPageDealCID()).getServicingMortgageNumber());

                if (tmpStr != null)
                {
                    pg.setPageState2(tmpStr);
                } else
                {
                    pg.setPageState2("");
                }
            } catch (Exception e)
            {
                pg.setPageState2("");
            }

            pg.setPageCondition1(true);
        }
    }

    /**
     *
     *
     */
    public void handleDealResolutionSubmit()
    {
        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            int resolutionstatus = this.getResolutionStatus();

            if (resolutionstatus == UNDEFINED)
            {
                setActiveMessageToAlert(BXResources.getSysMsg(
                        "DEAL_RESOLUTION_MISSING_RESOLUTION", theSessionState
                                .getLanguageId()),
                        ActiveMsgFactory.ISCUSTOMCONFIRM);

                return;
            }

            if (resolutionstatus == APPROVAL)
            {
                handleApproval(getApprovalType());
                
                ConditionRegExParser conditionRegExParser = new ConditionRegExParser(srk);
                PageEntry pg = theSessionState.getCurrentPage();
                Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
                conditionRegExParser.createAllMissingDocumentVerbiage(theSessionState.getLanguageId(), deal);

                return;
            }

            if (resolutionstatus == HOLD)
            {
                handleHold(getHoldPendingInformation());

                return;
            }

            if (resolutionstatus == DENY)
            {
                handleDeny(getDenialStatus(), false);

                return;
            }

            if (resolutionstatus == COLLAPSE)
            {
                handleCollapse(false);

                return;
            }

            throw new Exception("Unexpected Resolution option = "
                    + resolutionstatus);
        } catch (Exception e)
        {
            srk.cleanTransaction();

            SysLogger logger = srk.getSysLogger();
            logger
                    .error("Exception enxountered @DealResolution.handleDealResolutionSubmit()");
            logger.error(e);
            setStandardFailMessage();
        }
    }

    private void generateDeclineLetter(Deal deal) {
      // call docprep to create decline letter
      try
      {
        DocumentRequest dr = DocumentRequest.requestDocByProfile(srk, deal,
            Dc.DOCUMENT_GENERAL_TYPE_DECLINED_MTG_LETTER);
      }
      catch (Exception e)
      {
        srk.cleanTransaction();
        logger.error("Exception encountered @DealResolution.generateDeclineLetter() - while generating decline letter");
        logger.error(e);
        setStandardFailMessage();
      }

      logger.debug("DRH@generateDeclineLetter.  finished calling docprep for DL");
    }

    private void generateRateBonusLetter(Deal deal) {
        // call docprep to create decline letter
        try
        {
          DocumentRequest dr = DocumentRequest.requestDocByProfile(srk, deal,
              Dc.DOCUMENT_GENERAL_TYPE_RATE_BONUS_MTG_LETTER);
        }
        catch (Exception e)
        {
          srk.cleanTransaction();
          logger.error("Exception encountered @DealResolution.generateRateBonusLetter() - " +
                "while generating RateBonus letter");
          logger.error(e);
          setStandardFailMessage();
        }

        logger.debug("DRH@generateRateBonusLetter.  finished calling docprep for DL");
      }
    
    /**
     *
     *
     */
    public void handleCustomActMessageOk(String[] args)
    {
        Deal deal = null;

        PageEntry pg = theSessionState.getCurrentPage();

        SessionResourceKit srk = getSessionResourceKit();

        SysLogger logger = srk.getSysLogger();

        if (args[0].equals("E"))
        {
            // user OKs escalate approval
            try
            {
                DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);
                srk.beginTransaction();

                // adopt transactional copy (if one)
                standardAdoptTxCopy(pg, true);
                deal = DBA
                        .getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

                int oldStatus = deal.getStatusId();
                DBA.recordDealStatus(srk, deal, Mc.DEAL_APPROVAL_RECOMMENDED,
                        -1, -1);

                // if status changed ==> write DealHistory logging
                if (oldStatus != deal.getStatusId())
                {
                    String msgForStatus = hLog.statusChange(oldStatus,
                            "Deal Resolution");
                    hLog.log(deal.getDealId(), deal.getCopyId(), msgForStatus,
                            Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), deal
                                    .getStatusId());
                }

                //// SYNCADD.
                // Update Servicing Number based on Parameter setting -- by
                // Billy 10June2002
                if (isDisplayServicingNumField() == true)
                {
                    ////String theSrvNum =
                    // getCurrNDPage().getDisplayFieldStringValue("tbServMortgNum");
                    TextField tbTheSrvNum = (TextField) getCurrNDPage()
                            .getDisplayField("tbServMortgNum");
                    String theSrvNum = (String) tbTheSrvNum.getValue();

                    //logger.debug("DRH@handleCustomActMessageOk::SrvNum: " +
                    // theSrvNum);
                    deal.setServicingMortgageNumber(theSrvNum);
                }

                ////===============================================
                deal.ejbStore();

                // get ext. completion object used with approval recommended
                WFEExternalTaskCompletion extObj = (WFEExternalTaskCompletion) (pg
                        .getPageStateTable()).get("EXTTASKCOMPLETIONOBJ");
                workflowTrigger(0, srk, pg, extObj);
                setupForDealNotes(pg, BXResources.getSysMsg(
                        "DEAL_RESOULTION_ENTER_APPROVAL_NOTE", theSessionState
                                .getLanguageId()));
                srk.commitTransaction();
                navigateToNextPage(true);
            } catch (Exception e)
            {
                srk.cleanTransaction();
                logger
                        .error("Exception encountered @DealResolution.handleCustomActMessageOk() - while escalating approval");
                logger.error(e);
                setStandardFailMessage();
            }

            return;
        }

        if (args[0].equals("C"))
        {
            try
            {
                handleCollapse(true);
            } catch (Exception e)
            {
                srk.cleanTransaction();
                logger
                        .error("Exception encountered @DealResolution.handleCustomActMessageOk() - while collasping deal");
                logger.error(e);
                setStandardFailMessage();
            }

            return;
        }

        if (args[0].equals("D"))
        {
            try
            {
                // denial reason id saved in Page Entry criteria id (1)
                handleDeny(pg.getPageCriteriaId1(), true);
            } catch (Exception e)
            {
                srk.cleanTransaction();
                logger
                        .error("Exception encountered @DealResolution.handleCustomActMessageOk() - while denying deal");
                logger.error(e);
                setStandardFailMessage();
            }

            return;
        }

        // called after user responds OK to "do you want to generate decline letter" dialog.
        if (args[0].equals("DLY") || args[0].equals("DLN")) {
          // do not display the dialog again
          pg.setPageCondition3(true);

          // the user wants the decline letter to be generated
          if (args[0].equals("DLY")) {
            pg.setPageCondition4(true);
            logger.debug("DRH@handleCustomActMessageOk.  User clicked OK on DL dialog ");
          }
          else {
            pg.setPageCondition4(false);
            logger.debug("DRH@handleCustomActMessageOk.  User clicked Cancel on DL dialog ");
          }
          try {
            // in the case of a decline, generate the decline letter here
            if (getResolutionStatus() == DENY) {
              // the user clicked OK in the dialog
              if (pg.getPageCondition4()) {
                deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

//              Ticket #4483
//              Check if action allowed
                CCM ccm = new CCM(srk);
                if (!ccm.checkIfActionAllowed(deal))
                {
                    setActiveMessageToAlert(BXResources.getSysMsg(
                            "CCM_ACTION_NOT_ALLOWED_MI_CHECK_FAILED", theSessionState
                                    .getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);

                    return;
                }
                else
                {
                    generateDeclineLetter(deal);
                }
              }

              // call handleDeny to complete the decline code
              // denial reason id saved in Page Entry criteria id (1)
              handleDeny(pg.getPageCriteriaId1(), true);
            }
            // approval
            else {
              // the letter is generated in finalApprovalActivities, after the status has been changed
              finalApprovalActivities(true, false, srk, pg);
            }
          }
          catch(Exception e)
          {
            srk.cleanTransaction();
            logger.error("Exception encountered @DealResolution.handleCustomActMessageOk() - while generating decline letter");
            logger.error(e);
            setStandardFailMessage();
          }
        }

        /*
        // Commented this section as we are not showing MI Request pending message after deal approval - Ticket 250
        if (args[0].equals("M") || args[0].equals("N"))
        {
            // User answered auto Submit MI Request on approve?
            boolean autoMI = args[0].equals("M");
            // this has the same value as autoMI but at the page level.  autoMI is at this method level
            pg.setPageCondition5(autoMI);

            try
            {
                finalApprovalActivities(true, false, srk, pg);
            } catch (Exception e)
            {
                srk.cleanTransaction();
                logger
                        .error("Exception encountered @DealResolution.handleCustomActMessageOk() - while approve deal (after auto MI dialog)");
                logger.error(e);
                setStandardFailMessage();
            }
            return;
        }*/

        //--CervusPhaseII--start--//
        ViewBean thePage = getCurrNDPage();

        if (args[0].equals("OVERRIDE_DEAL_LOCK_YES"))
        {
            logger
                    .trace("CRH@handleCustomActMessageOk_REJECT_COND_YES: Override deal lock");
            provideOverrideDealLockActivity(pg, srk, thePage);
            return;
        }

        if (args[0].equals("OVERRIDE_DEAL_LOCK_NO"))
        {
            logger
                    .trace("CRH@handleCustomActMessageOk_REJECT_COND_NO: Return to previous screen");

            theSessionState.setActMessage(null);
            theSessionState.overrideDealLock(true);
            handleCancelStandard(false);

            return;
        }
        //--CervusPhaseII--13Nov2004--end--//
    }

    /**
     * Set the active message to the "Do you want to generate decline letter" dialog.
     * The JATO framework will automatically call handleCustomActMessageOk() if the user clicks Ok
     * @param deal Deal
     */
    private void declineLetterSetup()
    {
        // Begin 18Nov2005 -- Decline Letter changes
         //Ticket #4138
        setActiveMessageToAlert(BXResources.getSysMsg("DEAL_RESOULTION_GENERATE_DECLINE_LETTER_CONFIRM",
                theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMDIALOG2, "DLY", "DLN");
        logger.debug("DRH@declineLetterSetup.  finished setting active message for DL dialog");
        // End -- Decline Letter changes
    }

    /**
     *
     *
     */
    private void handleApproval(int approvalType) throws Exception
    {
        logger.trace("--T--> @DealResolutionHandler.handleApproval");

        if ((approvalType < APPROVE)
                || (approvalType > APPROVE_EXTERNAL_APPROVAL_GRANTED))
        {
            setActiveMessageToAlert(BXResources.getSysMsg(
                    "DEAL_RESOLUTION_APPROVAL_MISSING_APPROVAL_TYPE",
                    theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMCONFIRM);

            return;
        }

        PageEntry pg = theSessionState.getCurrentPage();

        SessionResourceKit srk = getSessionResourceKit();

        Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

        SysLogger logger = srk.getSysLogger();

        DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);

        String msg = new String("");

        int cs = deal.getStatusId();

        DealStatusManager dsm = DealStatusManager.getInstance(srk);

        int statusCat = dsm.getStatusCategory(cs, srk);
        ////logger.debug("DRH@handleApproval::Stamp1");
        if ((statusCat != Mc.DEAL_STATUS_CATEGORY_DECISION)
                || (cs == Mc.DEAL_APPROVED))
        {
            setActiveMessageToAlert(
                    BXResources
                            .getSysMsg(
                                    "DEAL_RESOLUTION_APPROVAL_OPTION_NOT_ALLOWED_STATUS_CATEGORY",
                                    theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMCONFIRM);

            return;
        }

        //
        //  APPROVE
        //
        
        if ((approvalType == APPROVE)
                && !((cs == Mc.DEAL_EXTERNAL_APPROVAL_REVIEW)
                        || (cs == Mc.DEAL_EXTERNAL_APPROVAL_REQUESTED) || (cs == Mc.DEAL_APPROVE_WITH_CHANGES)))
        {

            //-- ========== SCR#859 begins ========== --//
            //-- by Neil on Feb 16, 2005
            // BMO wants save the screen before running business rule checking.
            if (isDisplayServicingNumField() == true)
            {
                ///String theSrvNum = getCurrNDPage().getDisplayFieldStringValue("tbServMortgNum");
                TextField tbTheSrvNum = (TextField) getCurrNDPage().getDisplayField("tbServMortgNum");
                String theSrvNum = (String) tbTheSrvNum.getValue();
                //logger.debug("DRH@handleApproval::SrvNum: " + theSrvNum);
                deal.setServicingMortgageNumber(theSrvNum);
            }
            deal.ejbStore();
            //-- ========== SCR#859 ends ========== --//
            AAGuideline aag = new AAGuideline(srk);

            ////logger.debug("DRH@handleApproval::Stamp2");
            int newCs = aag.qualifyApproval(theSessionState, pg, srk, deal);
            
            ////logger.debug("DRH@handleApproval::Stamp2_NewCs: " + newCs);
            switch (newCs)
            {
            case -1:

                // can't be approved
                ////logger.debug("DRH@handleApproval::Stamp2_1");
                setActiveMessageToAlert(aag.getUserDialog());

                ////logger.debug("DRH@handleApproval::Stamp2_1_1");
                return;

            case Mc.DEAL_APPROVAL_RECOMMENDED:

                // must escalate
                ActiveMessage am = aag.getUserDialog();
                am.setResponseIdentifier("E");
                setActiveMessageToAlert(am);

                // save the external task completion object (used if user OKs
                // escalation)
                (pg.getPageStateTable()).put("EXTTASKCOMPLETIONOBJ", aag
                        .getExtTaskCompletionObj());

                ////logger.debug("DRH@handleApproval::Stamp2_2");
                return;

            case Mc.DEAL_APPROVED:
              // only generate the decline letter confirmation in this case

              // proceed to final approval
              ////logger.debug("DRH@handleApproval::Stamp2_3_1");
              finalApprovalActivities(false, false, srk, pg);

              ////logger.debug("DRH@handleApproval::Stamp2_3_2");
              return;

            case Mc.DEAL_EXTERNAL_APPROVAL_REVIEW:

                ////logger.debug("DRH@handleApproval::Stamp2_4_1");
                srk.beginTransaction();

                // adopt transactional copy (if one)
                standardAdoptTxCopy(pg, true);
                deal = DBA
                        .getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

                int oldStatus = deal.getStatusId();
                DBA.recordDealStatus(srk, deal, newCs, -1, -1);

                // if status changed ==> write DealHistory logging
                if (oldStatus != deal.getStatusId())
                {
                    String msgForStatus = hLog.statusChange(oldStatus,
                            "Deal Resolution");
                    hLog.log(deal.getDealId(), deal.getCopyId(), msgForStatus,
                            Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), deal
                                    .getStatusId());
                }

                ////logger.debug("DRH@handleApproval::Stamp2_4_2");
                CCM ccm = new CCM(srk);
                ccm.lockScenarios(deal);

                //-- ========== SCR#859 begins ========== --//
                //-- by Neil on Feb 16, 2005
                // BMO wants save screen before running business rule checking.
                // This part of code moves up.
                //
                //////SYNCADD.
                //// Update Servicing Number based on Parameter setting -- by
                //// Billy 10June2002
                //if (isDisplayServicingNumField() == true)
                //{
                //    ///String theSrvNum =
                //    // getCurrNDPage().getDisplayFieldStringValue("tbServMortgNum");
                //    TextField tbTheSrvNum = (TextField) getCurrNDPage()
                //            .getDisplayField("tbServMortgNum");
                //    String theSrvNum = (String) tbTheSrvNum.getValue();
                //
                //    //logger.debug("DRH@handleApproval::SrvNum: " + theSrvNum);
                //    deal.setServicingMortgageNumber(theSrvNum);
                //}
                //////===============================================
                //deal.ejbStore();
                //-- ========== SCR#859 ends ========== --//
                workflowTrigger(2, srk, pg, null);

                ////logger.debug("DRH@handleApproval::Stamp2_4_3");
                // add Broker note -- New requirement -- By Billy 13July2001
                createBrokerNote(pg, srk);
                setupForDealNotes(pg, BXResources.getSysMsg(
                        "DEAL_RESOULTION_ENTER_APPROVAL_NOTE", theSessionState
                                .getLanguageId()));
                srk.commitTransaction();

                navigateToNextPage(true);

                ////logger.debug("DRH@handleApproval::Stamp2_4_4");
                return;

            default:

                // no other valid status codes
                throw new Exception("Unexpected (next) deal status (" + newCs
                        + ") returned from AAG, dealId=" + pg.getPageDealId()
                        + ", copyId=" + pg.getPageDealCID());
            }
        } else if ((approvalType == APPROVE)
                && (cs == Mc.DEAL_EXTERNAL_APPROVAL_REVIEW))
        {
          logger.debug("DRH@handleApproval.  skip decline letter conf.  deal external approval review.");

          // don't display the decline letter confirmation
          pg.setPageCondition3(true);
          pg.setPageCondition4(false);

          // proceed to final approval

          finalApprovalActivities(false, false, srk, pg);

            return;
        } else if ((approvalType == APPROVE)
                && (cs == Mc.DEAL_APPROVE_WITH_CHANGES))
        {
            ////logger.debug("DRH@handleApproval::Stamp3");
            srk.beginTransaction();

            // adopt transactional copy (if one)
            standardAdoptTxCopy(pg, true);
            deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

            int oldStatus = deal.getStatusId();
            DBA.recordDealStatus(srk, deal, Mc.DEAL_REVIEW_APPROVED_CHANGES,
                    -1, -1);

            // if status changed ==> write DealHistory logging
            if (oldStatus != deal.getStatusId())
            {
                String msgForStatus = hLog.statusChange(oldStatus,
                        "Deal Resolution");
                hLog.log(deal.getDealId(), deal.getCopyId(), msgForStatus,
                        Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), deal
                                .getStatusId());
            }

            //// SYNCADD.
            // Update Servicing Number based on Parameter setting -- by Billy
            // 10June2002
            if (isDisplayServicingNumField() == true)
            {
                ////String theSrvNum =
                // getCurrNDPage().getDisplayFieldStringValue("tbServMortgNum");
                TextField tbTheSrvNum = (TextField) getCurrNDPage()
                        .getDisplayField("tbServMortgNum");
                String theSrvNum = (String) tbTheSrvNum.getValue();

                //logger.debug("DRH@handleApproval::SrvNum: " + theSrvNum);
                deal.setServicingMortgageNumber(theSrvNum);
            }

            ////===============================================
            deal.ejbStore();
            workflowTrigger(2, srk, pg, null);

            // add Broker note -- New requirement -- By Billy 13July2001
            createBrokerNote(pg, srk);
            setupForDealNotes(pg, BXResources.getSysMsg(
                    "DEAL_RESOULTION_ENTER_CHANGES", theSessionState
                            .getLanguageId()));
            srk.commitTransaction();
            navigateToNextPage(true);

            return;
        } else if ((approvalType == APPROVE)
                && (cs == Mc.DEAL_EXTERNAL_APPROVAL_REQUESTED))
        {
            setActiveMessageToAlert(BXResources.getSysMsg(
                    "DEAL_RESOLUTION_APPROVAL_EAR_OPTION_REQUIRED",
                    theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMCONFIRM);

            return;
        }

        //
        //  APPROVE_WITH_CHANGE
        //
        else if ((approvalType == APPROVE_WITH_CHANGE)
                && (cs == Mc.DEAL_EXTERNAL_APPROVAL_REQUESTED))
        {
            ////logger.debug("DRH@handleApproval::Stamp4");
            srk.beginTransaction();

            // adopt transactional copy (if one)
            standardAdoptTxCopy(pg, true);
            deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

            //deal history record
            int oldStatus = deal.getStatusId();
            deal = DBA.recordDealStatus(srk, deal, Mc.DEAL_RECEIVED,
                    Mc.DEAL_RES_CONDITION_NO_CONDITION, -1);

            // Create History Log if Status changed
            if (oldStatus != deal.getStatusId())
            {
                msg = hLog.statusChange(oldStatus, "Deal Resolution");
                hLog.log(deal.getDealId(), deal.getCopyId(), msg,
                        Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), deal
                                .getStatusId());
            }

            CCM ccm = new CCM(srk);

            // basically transform recommedned scenarion to gold and cleanup
            // (like a commitment
            // reversal)
            ccm.adoptRecommendedScenario(deal, 0);
            ccm.cancelCommitment(deal);

            //// SYNCADD.
            // Update Servicing Number based on Parameter setting -- by Billy
            // 10June2002
            if (isDisplayServicingNumField() == true)
            {
                ////String theSrvNum =
                // getCurrNDPage().getDisplayFieldStringValue("tbServMortgNum");
                TextField tbTheSrvNum = (TextField) getCurrNDPage()
                        .getDisplayField("tbServMortgNum");
                String theSrvNum = (String) tbTheSrvNum.getValue();

                //logger.debug("DRH@handleApproval::SrvNum: " + theSrvNum);
                deal.setServicingMortgageNumber(theSrvNum);
            }

            deal.ejbStore();

            ////===============================================
            workflowTrigger(2, srk, pg, null);

            // add Broker note -- New requirement -- By Billy 13July2001
            createBrokerNote(pg, srk);
            setupForDealNotes(pg, BXResources.getSysMsg(
                    "DEAL_RESOULTION_ENTER_EXT_LENDER_RELEASE", theSessionState
                            .getLanguageId()));
            srk.commitTransaction();
            navigateToNextPage(true);

            return;
        } else if ((approvalType == APPROVE_WITH_CHANGE)
                && ((cs == Mc.DEAL_HIGHER_APPROVAL_REQUESTED) || (cs == Mc.DEAL_REVIEW_APPROVED_CHANGES)))
        {
            srk.beginTransaction();

            // adopt transactional copy (if one)
            standardAdoptTxCopy(pg, true);
            deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

            int oldStatus = deal.getStatusId();
            DBA.recordDealStatus(srk, deal, Mc.DEAL_APPROVE_WITH_CHANGES, -1,
                    -1);

            // if status changed ==> write DealHistory logging
            if (oldStatus != deal.getStatusId())
            {
                String msgForStatus = hLog.statusChange(oldStatus,
                        "Deal Resolution");
                hLog.log(deal.getDealId(), deal.getCopyId(), msgForStatus,
                        Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), deal
                                .getStatusId());
            }

            deal.ejbStore();
            workflowTrigger(2, srk, pg, null);

            // add Broker note -- New requirement -- By Billy 13July2001
            createBrokerNote(pg, srk);
            setupForDealNotes(pg, BXResources.getSysMsg(
                    "DEAL_RESOULTION_ENTER_APPROVE_WITH_CHANGES_NOTE",
                    theSessionState.getLanguageId()));
            srk.commitTransaction();
            navigateToNextPage(true);

            return;
        } else if (approvalType == APPROVE_WITH_CHANGE)
        {
            setActiveMessageToAlert(BXResources.getSysMsg(
                    "DEAL_RESOLUTION_APPROVE_WITH_CHANGE_NOT_ALLOWED",
                    theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMCONFIRM);

            return;
        }

        //
        //  APPROVE_REQUEST_EXTERNAL_APPROVAL
        //
        else if ((approvalType == APPROVE_REQUEST_EXTERNAL_APPROVAL)
                && (cs == Mc.DEAL_EXTERNAL_APPROVAL_REVIEW))
        {
            ////logger.debug("DRH@handleApproval::Stamp5");
            srk.beginTransaction();

            // adopt transactional copy (if one)
            standardAdoptTxCopy(pg, true);
            deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

            int oldStatus = deal.getStatusId();
            DBA.recordDealStatus(srk, deal,
                    Mc.DEAL_EXTERNAL_APPROVAL_REQUESTED, -1, -1);

            // if status changed ==> write DealHistory logging
            if (oldStatus != deal.getStatusId())
            {
                String msgForStatus = hLog.statusChange(oldStatus,
                        "Deal Resolution");
                hLog.log(deal.getDealId(), deal.getCopyId(), msgForStatus,
                        Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), deal
                                .getStatusId());
            }

            //// SYNCADD.
            // Update Servicing Number based on Parameter setting -- by Billy
            // 10June2002
            if (isDisplayServicingNumField() == true)
            {
                ////String theSrvNum =
                // getCurrNDPage().getDisplayFieldStringValue("tbServMortgNum");
                TextField tbTheSrvNum = (TextField) getCurrNDPage()
                        .getDisplayField("tbServMortgNum");
                String theSrvNum = (String) tbTheSrvNum.getValue();

                //logger.debug("DRH@handleApproval::SrvNum: " + theSrvNum);
                deal.setServicingMortgageNumber(theSrvNum);
            }

            ////===============================================
            deal.ejbStore();
            workflowTrigger(2, srk, pg, null);

            // add Broker note -- New requirement -- By Billy 13July2001
            createBrokerNote(pg, srk);
            setupForDealNotes(pg, BXResources.getSysMsg(
                    "DEAL_RESOULTION_ENTER_EXT_LENDER_APPROVAL_REQUEST",
                    theSessionState.getLanguageId()));
            srk.commitTransaction();
            navigateToNextPage(true);

            return;
        } else if (approvalType == APPROVE_REQUEST_EXTERNAL_APPROVAL)
        {
            setActiveMessageToAlert(
                    BXResources
                            .getSysMsg(
                                    "DEAL_RESOLUTION_APPROVE_REQUEST_EXTERNAL_APPROVAL_NOT_ALLOWED",
                                    theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMCONFIRM);

            return;
        }

        //
        //  APPROVE_EXTERNAL_APPROVAL_GRANTED
        //
        else if ((approvalType == APPROVE_EXTERNAL_APPROVAL_GRANTED)
                && (cs == Mc.DEAL_EXTERNAL_APPROVAL_REQUESTED))
        {
          logger.debug("DRH@handleApproval.  skip decline letter conf.  deal external approval requested.");

          // don't display the decline letter confirmation
          pg.setPageCondition3(true);
          pg.setPageCondition4(false);

          finalApprovalActivities(false, false, srk, pg);

          return;
        } else if (approvalType == APPROVE_EXTERNAL_APPROVAL_GRANTED)
        {
            setActiveMessageToAlert(
                    BXResources
                            .getSysMsg(
                                    "DEAL_RESOLUTION_APPROVE_EXTERNAL_APPROVAL_GRANTED_NOT_ALLOWED",
                                    theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMCONFIRM);

            return;
        }

        ////logger.debug("DRH@handleApproval::Stamp6:EndOfMethod");
        // unspecified handling
        throw new Exception("Unspecified actions for Approval Type = "
                + approvalType + ", Deal Status = " + cs + ", for dealId = "
                + pg.getPageDealId() + ", copyId = " + pg.getPageDealCID());
    }

    /**
     *
     *
     */
    private void handleCollapse(boolean confirmed) throws Exception
    {
        PageEntry pg = theSessionState.getCurrentPage();
        SessionResourceKit srk = getSessionResourceKit();
        SysLogger logger = srk.getSysLogger();
        DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);
        String msg = " ";

        logger.trace("--T--> @DealResolutionHandler.handleCollapse");

        Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
        int cs = deal.getStatusId();
        DealStatusManager dsm = DealStatusManager.getInstance(srk);
        int statusCat = dsm.getStatusCategory(cs, srk);

        // retrieve the old statusCategery
        //int statusCatOld = statusCat;

        if (statusCat == Mc.DEAL_STATUS_CATEGORY_FINAL_DISPOSITION)
        {
            setActiveMessageToAlert(
                    BXResources
                            .getSysMsg(
                                    "DEAL_RESOLUTION_COLLASPE_OPTION_NOT_ALLOWED_STATUS_CATEGORY",
                                    theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMCONFIRM);

            return;
        }

        // Check if action allowed
        CCM ccm = new CCM(srk);

        if (!ccm.checkIfActionAllowed(deal))
        {
            setActiveMessageToAlert(BXResources.getSysMsg(
                    "CCM_ACTION_NOT_ALLOWED_MI_CHECK_FAILED", theSessionState
                            .getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);

            return;
        }

        if (confirmed == false)
        {
            // dialog to confirm collaspe
            setActiveMessageToAlert(BXResources.getSysMsg(
                    "DEAL_RESOLUTION_COLLASPE_CONFIRM_DIALOG", theSessionState
                            .getLanguageId()), ActiveMsgFactory.ISCUSTOMDIALOG,
                    "C");

            return;
        }

        srk.beginTransaction();

        ccm.collaspeDeal(deal);

        //// Place requests for #118 (71), #47 (63) in case of DJ client.
        if (PropertiesCache.getInstance().getProperty(
                theSessionState.getDealInstitutionId(),
                "com.basis100.conditions.isdjdocumentgeneration", "N").equals(
                "Y"))
        {
            //// 1. Produce DJ #118 (LoanApplicationCancelledNotice) letter.
            logger
                    .debug("VLAD ===> TEST_Before :: Before send the requestLoanApplicationCancelled Letter!!");
            DocumentRequest.requestLoanApplicationCancelled(srk, deal);
            logger
                    .debug("VLAD ===> TEST_After :: Sent the requestLoanApplicationCancelled Letter!!");

            //// 2. Produce DJ #47 (TransferredMortgageFile) document (part of
            // logic
            //// when it should be done from the DealResolution screen)
            //// and place requests for #47 (63) with attachments: 119(72) and
            // 120(73)
            //// and new unconditional #53 (74).
            logger
                    .debug("VLAD ===> TEST_Before :: Before send the requestTransferredMtgFile Letter and attachmnts!!");

            //DocumentRequest.requestTransferredMtgFile(srk, deal);
            //DocumentRequest.requestCreditApplication(srk, deal);
            //DocumentRequest.requestCreditAnalysisForm(srk, deal);
            //DocumentRequest.requestMtgFinancingApprovalAddOn(srk, deal);
            //// New request from Product to send the documents wrapped up into
            // the bundles:
            //// new 1047 bundles is:
            //// #47 (Cover Letter & Coupon) + #120 + #119
            //// new 1053 bundle is:
            //// #53 (Cover Letter) + #114 + #115 + #116.
            //// These two new requests are cancelled.
            ////DocumentRequest.requestTransferredMtgFileBundle(srk, deal);
            ////DocumentRequest.requestMtgFinancingApprovalAndOnBundle(srk,
            // deal);
            logger
                    .debug("VLAD ===> TEST_After :: Sent the requestTransferredMtgFile Letter and attachmnts!!");
        }

        //--DJ_DT_CR--end--//
        //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
        //--> By Billy 09Jan2004
        //--> Reset Hold Reason if decision made
        deal.setHoldReasonId(0);

        //=============================================================
        //--> Ticket#158 : BMO retain Mortgage Servicing Number for Deny and
        // Collapsing
        //--> Moved the Mortgage Servicing Number out from the Approval Section
        //--> By Billy 13Jan2004
        if (isDisplayServicingNumField() == true)
        {
            String theSrvNum = (String) getCurrNDPage().getDisplayFieldValue(
                    "tbServMortgNum");
            deal.setServicingMortgageNumber(theSrvNum);
        }

        //==============================================================================
        deal.ejbStore();

        //logger.debug("BILLY ===> Before calling workflowTrigger !!!");
        workflowTrigger(2, srk, pg, null);

        //logger.debug("BILLY ===> After calling workflowTrigger !!!");
        // add Broker note -- New requirement -- By Billy 13July2001
        createBrokerNote(pg, srk);
        // Ticket FIX FXP31918 - Commented unused variables
        //int systemTypeId = deal.getSystemTypeId();

        //String systemTypeDesc = PicklistData.getDescription("SystemType",
          //      systemTypeId);

        //-- FXLink Phase II --//
        //--> Send if deal.ChannelMedia = "E"
        //--> By Billy 01Dec2003
        if ((deal.getChannelMedia() != null)
                && deal.getChannelMedia().trim().equals("E"))
        //if ((systemTypeDesc.trim().equals("MORTY") ||
        // systemTypeDesc.trim().equals("ECNI")) &&
        // deal.getChannelMedia().trim().equals("E") &&
        // deal.getSourceSystemMailBoxNBR() != null)
        //==================================
        {
            //trigger automateed response to Broker System and update deal
            // hsitory
            DocumentRequest.requestDenial(srk, deal);

            //log to deal hidtory logger
            msg = hLog.sourceSystemReponse(deal.getSourceOfBusinessProfileId());
            hLog.log(deal.getDealId(), deal.getCopyId(), msg,
                    Mc.DEAL_TX_TYPE_INTERFACE, srk.getExpressState().getUserProfileId());
        }

        //--> Ticket#371 :: Change for Cervus :: Send Upload when Collapse
        //--> By Billy 27May2004
        ccm.sendServicingUpload(srk, deal, Mc.SERVICING_UPLOAD_TYPE_COLLAPSE,
                theSessionState.getLanguageId());

        //================================================================
        //Fix for ticket 60 AIG UG
        if (srk.isInTransaction()) {
            srk.commitTransaction();
        }

        setupForDealNotes(pg, BXResources.getSysMsg(
                "DEAL_RESOULTION_ENTER_COLLASPE_NOTE", theSessionState
                        .getLanguageId()));

        //--DJ_DT_CR--start--//
        //// Place request for document #47 (63) in case of DJ client
        // unconditionally.
        if (PropertiesCache.getInstance().getProperty(
                theSessionState.getDealInstitutionId(),
                "com.basis100.conditions.isdjdocumentgeneration", "N").equals(
                "Y"))
        {
            logger
                    .debug("VLAD ===> TEST_Before :: Before send the requestDeclinedMtgLetter Letter and attachmnts!!");

            //// Finally, along with the #57 (67) should be sent. These two new
            // requests are cancelled.
            ////DocumentRequest.requestTransferredMtgFileBundle(srk, deal);
            ////DocumentRequest.requestMtgFinancingApprovalAndOnBundle(srk,
            // deal);
            
//          Ticket #4242
            if(deal.getStatusId() == Mc.DEAL_DENIED)
              DocumentRequest.requestDeclinedMtgLetter(srk, deal);
            else
              DocumentRequest.requestRateBonusMtgLetter(srk, deal);
            
            logger
                    .debug("VLAD ===> TEST_After :: Sent the requestDeclinedMtgLetter Letter and attachmnts!!");
        }

        //--DJ_DT_CR--end--//
        navigateToNextPage(true);
    }

    /**
     *
     *
     */
    private void handleDeny(int denyReasonId, boolean confirmed)
            throws Exception
    {
        PageEntry pg = theSessionState.getCurrentPage();
        SessionResourceKit srk = getSessionResourceKit();
        Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
        
        //4483
        CCM _ccm = new CCM(srk);
        if (!_ccm.checkIfActionAllowed(deal))
        {
            setActiveMessageToAlert(BXResources.getSysMsg(
                    "DEAL_RESOLUTION_DENY_OPTION_NOT_ALLOWED_MI_STATUS_CATEGORY", theSessionState
                            .getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);

            return;
        }
        
        SysLogger logger = srk.getSysLogger();
        DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);
        String msg = " ";

        logger
                .trace("--T--> @DealResolutionHandler.handleDeny(), denyReasonId= "
                        + denyReasonId);

        int cs = deal.getStatusId();
        DealStatusManager dsm = DealStatusManager.getInstance(srk);
        int statusCat = dsm.getStatusCategory(cs, srk);
        if (statusCat > Mc.DEAL_STATUS_CATEGORY_DECISION)
        {
            setActiveMessageToAlert(BXResources.getSysMsg(
                    "DEAL_RESOLUTION_DENY_OPTION_NOT_ALLOWED_STATUS_CATEGORY",
                    theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMCONFIRM);

            return;
        }

        if (denyReasonId < 0)
        {
            setActiveMessageToAlert(BXResources.getSysMsg(
                    "DEAL_RESOLUTION_DENY_MISSING_DENY_REASON", theSessionState
                            .getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);

            return;
        }

        if (confirmed == false)
        {
            // dialog to confirm deny - save deny reason in page entry
            pg.setPageCriteriaId1(denyReasonId);
            setActiveMessageToAlert(BXResources.getSysMsg(
                   "DEAL_RESOLUTION_DENY_CONFIRM_DIALOG", theSessionState
                           .getLanguageId()), ActiveMsgFactory.ISCUSTOMDIALOG,
                              "D");
            // page condition 3 = has decline letter confirmation been displayed and responded to by user?
            pg.setPageCondition3(false);

            return;
        }
        else
        {
          // Begin 18Nov2005 -- Decline Letter changes
          // has the user already responded to the "do you want to generate decline letter" dialog?
          if (!pg.getPageCondition3())
          {
            // only do if decline reason if credit history
            if (denyReasonId == Mc.DENIAL_REASON_CREDIT_HISTORY)
            {
                String prop = PropertiesCache.getInstance().
                    getProperty(theSessionState.getDealInstitutionId(), 
                                "mosApp.MosSystem.dealresolution.declineletter", "N");
                if (prop.equals("Y")) {
                    declineLetterSetup();
                    return;
                }
            }
          }
          // End -- Decline Letter changes
        }

        if ((denyReasonId == Mc.DEAL_DENY_EXTERNAL_LENDER_REFUSED)
                && (cs == Mc.DEAL_EXTERNAL_APPROVAL_REQUESTED))
        {
            srk.beginTransaction();

            /*
             * logger.trace("--T--> @DealResolutionHandler.handleDeny(),
             * denyReason: DEAL_DENY_EXTERNAL_LENDER_REFUSED" + ", dealStatusId =
             * DEAL_EXTERNAL_APPROVAL_REQUESTED");
             */
            // adopt transactional copy (if one)
            standardAdoptTxCopy(pg, true);

            // deal history record
            int oldStatus = deal.getStatusId();
            deal = DBA.recordDealStatus(srk, pg.getPageDealId(), pg
                    .getPageDealCID(), Mc.DEAL_RECEIVED,
                    Mc.DEAL_RES_CONDITION_NO_CONDITION);

            // Create History Log if Status changed
            if (oldStatus != deal.getStatusId())
            {
                msg = hLog.statusChange(oldStatus, "Deal Resolution");
                hLog.log(deal.getDealId(), deal.getCopyId(), msg,
                        Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), deal
                                .getStatusId());
            }

            // basically transform recommedned scenarion to gold and cleanup
            // (like a commitment
            // reversal)
            CCM ccm = new CCM(srk);
            ccm.adoptRecommendedScenario(deal, 0);
            ccm.cleanupForDeniedOrCollasped(deal);

            //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
            //--> By Billy 09Jan2004
            //--> Reset Hold Reason if decision made
            deal.setHoldReasonId(0);

            //--> Ticket#158 : BMO retain Mortgage Servicing Number for Deny
            // and Collapsing
            //--> Moved the Mortgage Servicing Number out from the Approval
            // Section
            //--> By Billy 13Jan2004
            if (isDisplayServicingNumField() == true)
            {
                String theSrvNum = (String) getCurrNDPage()
                        .getDisplayFieldValue("tbServMortgNum");
                deal.setServicingMortgageNumber(theSrvNum);
            }

            //==============================================================================
            deal.ejbStore();

            workflowTrigger(2, srk, pg, null);
            srk.commitTransaction();
            setupForDealNotes(pg, BXResources.getSysMsg(
                    "DEAL_RESOULTION_ENTER_EXT_LENDER_RELEASE", theSessionState
                            .getLanguageId()));
            navigateToNextPage(true);

            return;
        }

        if ((denyReasonId == Mc.DEAL_DENY_EXTERNAL_LENDER_REFUSED)
                && (cs != Mc.DEAL_EXTERNAL_APPROVAL_REQUESTED))
        {
            setActiveMessageToAlert(BXResources.getSysMsg(
                    "DEAL_RESOLUTION_DENY_REASON_CAN_NOT_BE_USED",
                    theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMCONFIRM);

            return;
        }

        // Check if action allowed
        CCM ccm = new CCM(srk);

        if (!ccm.checkIfActionAllowed(deal))
        {
            setActiveMessageToAlert(BXResources.getSysMsg(
                    "CCM_ACTION_NOT_ALLOWED_MI_CHECK_FAILED", theSessionState
                            .getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);

            return;
        }

        srk.beginTransaction();

        ccm.denyDeal(deal, denyReasonId);

        //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
        //--> By Billy 09Jan2004
        //--> Reset Hold Reason if decision made
        deal.setHoldReasonId(0);
        
        /***** FXP 27058 start *****/
        if (isDisplayServicingNumField() == true)
        {
            String theSrvNum = (String) getCurrNDPage()
                    .getDisplayFieldValue("tbServMortgNum");
            deal.setServicingMortgageNumber(theSrvNum);
        }
        /***** FXP 27058 end *****/
        
        /***** FXP 27058 start *****/
        if (isDisplayServicingNumField() == true)
        {
            String theSrvNum = (String) getCurrNDPage()
                    .getDisplayFieldValue("tbServMortgNum");
            deal.setServicingMortgageNumber(theSrvNum);
        }
        /***** FXP 27058 end *****/
        
        //=============================================================
        deal.ejbStore();

        workflowTrigger(2, srk, pg, null);

        //close all outstanding tasks on the deal
        // add Broker note -- New requirement -- By Billy 13July2001
        createBrokerNote(pg, srk);
        // Ticket FIX FXP31918 - Commented unused variables
        // trigger automated response to Broker System and update deal hsitory
        //int systemTypeId = deal.getSystemTypeId();

        // String systemTypeDesc = PicklistData.getDescription("SystemType",
        //        systemTypeId);

        //DA-DEC-UC-2 starts
        //-- FXLink Phase II --//
        //--> Send if deal.ChannelMedia = "E"
        //--> By Billy 01Dec2003
        //if ((deal.getChannelMedia() != null)
        //        && deal.getChannelMedia().trim().equals("E"))
        //if ((systemTypeDesc.trim().equals("MORTY") ||
        // systemTypeDesc.trim().equals("ECNI")) &&
        // deal.getChannelMedia().trim().equals("E") &&
        // deal.getSourceSystemMailBoxNBR() != null)
        //==================================
        //{
            //trigger automateed response to Broker System and update deal
            // hsitory
            //DocumentRequest.requestDenial(srk, deal);

            //log to deal history logger
            //msg = hLog.sourceSystemReponse(deal.getSourceOfBusinessProfileId());
            //hLog.log(deal.getDealId(), deal.getCopyId(), msg,
            //        Mc.DEAL_TX_TYPE_INTERFACE, srk.getExpressState().getUserProfileId());
        //}
        
        
        try {
            MasterDeal masterDeal = new MasterDeal(srk, null, deal.getDealId());
            if (masterDeal.getFXLinkSchemaId() == MasterDeal.FXLINKSCHEMA_FCX_V_1_0) {       	
               // ESBOutboundQueue queue = new ESBOutboundQueue(srk);
               // queue.create(queue.createPrimaryKey(), ServiceConst.SERVICE_TYPE_LOANDECISION_UPDATE, deal.getDealId(), deal.getSystemTypeId(), srk.getExpressState().getUserProfileId());
               // queue.setServiceSubTypeid(ServiceConst.SERVICE_SUBTYPE_LOANDECISION_DENIAL);
               // queue.ejbStore();
            	
            	// For Manual Deal originated from Deal Entry screen will have channel Media as 'M' 
		 	    // Block updating the Deal event status,Condition update status and deal decision for deals created from express. 
		 	    // FXP32829 - Dec 06 2011 - FSD updated S:\Project Repository\CMMI Active Projects\Express 4.2 
            	// Release\4 BA - PA Baselines\Data Alignment version v1.14 and use case DA-DEC-UC-3
		        if(deal.getChannelMedia() != null && deal.getChannelMedia().trim().equalsIgnoreCase(Xc.CHANNEL_MEDIA_M )) {
		        	
		        	if (srk.isInTransaction()) {
		                 srk.commitTransaction();
		        	}    
		        	setupForDealNotes(pg, BXResources.getSysMsg(
		                     "DEAL_RESOULTION_ENTER_DENY", theSessionState.getLanguageId()));
		             navigateToNextPage(true);
		             return;
		        }
		        
		        
        		 //checks DOC Business rule before sending FCX denial and also sending email
        		 DocBusinessRuleExecutor dbre = new DocBusinessRuleExecutor(srk);
        		 
        		 if( dbre.evaluateRequest(
        		 		deal,Dc.DOCUMENT_GENERAL_TYPE_DEAL_DENIAL).isCreate()){
        			 	
       	          		ESBOutboundQueue queue = new ESBOutboundQueue(srk);
       	          		queue.create(queue.createPrimaryKey(), ServiceConst.SERVICE_TYPE_LOANDECISION_UPDATE, deal.getDealId(), deal.getSystemTypeId(), srk.getExpressState().getUserProfileId());
       	          		queue.setServiceSubTypeid(ServiceConst.SERVICE_SUBTYPE_LOANDECISION_DENIAL);
       	          		queue.ejbStore();
        		 }
            } else {
                if ((deal.getChannelMedia() != null) && deal.getChannelMedia().trim().equals("E")) {
                	DocumentRequest.requestDenial(srk, deal);
                    msg = hLog.sourceSystemReponse(deal.getSourceOfBusinessProfileId());
                    hLog.log(deal.getDealId(), deal.getCopyId(), msg, Mc.DEAL_TX_TYPE_INTERFACE, srk.getExpressState().getUserProfileId());
                }
            }
        } catch (Exception e) {
            EmailSender.sendAlertEmail("Deal Resolution", e);
            throw e;
        }
        //DA-DEC-UC-2 ends

        //--> Ticket#371 :: Change for Cervus :: Send Upload when Deny
        //--> By Billy 27May2004
        ccm.sendServicingUpload(srk, deal, Mc.SERVICING_UPLOAD_TYPE_DENY,
                theSessionState.getLanguageId());

        //================================================================
        //--DJ_DT_CR--start--//
        //// Place request for document #47 (63) in case of DJ client
        // unconditionally.
        if (PropertiesCache.getInstance().getProperty(
                theSessionState.getDealInstitutionId(), 
                "com.basis100.conditions.isdjdocumentgeneration", "N").equals(
                "Y"))
        {
            //// 1. Produce DJ #47 (TransferredMortgageFile) document (part of
            // logic
            //// when it should be done from the DealResolution screen)
            //// and place requests for #47 (63) with attachments: 119(72) and
            // 120(73).
            //// and new unconditional #53 (74).
            logger
                    .debug("VLAD ===> TEST_Before :: Before send the requestDeclinedMtgLetter Letter and attachmnts!!");

            //DocumentRequest.requestTransferredMtgFile(srk, deal);
            //DocumentRequest.requestCreditApplication(srk, deal);
            //DocumentRequest.requestCreditAnalysisForm(srk, deal);
            //DocumentRequest.requestMtgFinancingApprovalAddOn(srk, deal);
            //// New request from Product to send the documents wrapped up into
            // the bundles:
            //// new 1047 bundles is:
            //// #47 (Cover Letter & Coupon) + #120 + #120 + #119 + #114 + #115
            // + #116
            //// new 1053 bundle is:
            //// #53 (Cover Letter) + #114 + #115 + #116.
            //// Note, that there is some overlapping here, but client wants to
            // get these
            //// documents in bundles.
            //// Finally, along with the #57 (67) should be sent. These two new
            // requests are cancelled.
            ////DocumentRequest.requestTransferredMtgFileBundle(srk, deal);
            ////DocumentRequest.requestMtgFinancingApprovalAndOnBundle(srk,
            // deal);
            DocumentRequest.requestDeclinedMtgLetter(srk, deal);
            logger
                    .debug("VLAD ===> TEST_After :: Sent the requestDeclinedMtgLetter Letter and attachmnts!!");
        }

        //--DJ_DT_CR--end--//
        //Fix for ticket 60 AIG UG
        if (srk.isInTransaction()) {
                srk.commitTransaction();
        }       

        setupForDealNotes(pg, BXResources.getSysMsg(
                "DEAL_RESOULTION_ENTER_DENY", theSessionState.getLanguageId()));
    
        navigateToNextPage(true);
    }

    /**
     *
     *
     */
    private void handleHold(int holdOption) throws Exception
    {
        PageEntry pg = theSessionState.getCurrentPage();
        SessionResourceKit srk = getSessionResourceKit();
        Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
        SysLogger logger = srk.getSysLogger();
        DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);
        String msg = " ";

        //DealHistory hLog = new DealHistory(srk);
        logger
                .trace("--T--> @DealResolutionHandler.handleHold() Enter!! HoldOption: "
                        + holdOption);

        int cs = deal.getStatusId();
        DealStatusManager dsm = DealStatusManager.getInstance(srk);
        int statusCat = dsm.getStatusCategory(cs, srk);

        //// SYNCADD.
        // Bug fix by Billy to check if statusCat >
        // Mc.DEAL_STATUS_CATEGORY_DECISION
        //    before was statusCat != Mc.DEAL_STATUS_CATEGORY_DECISION
        // -- 15July2002
        if (statusCat > Mc.DEAL_STATUS_CATEGORY_DECISION)
        {
            setActiveMessageToAlert(BXResources.getSysMsg(
                    "DEAL_RESOLUTION_HOLD_OPTION_NOT_ALLOWED_STATUS_CATEGORY",
                    theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMCONFIRM);

            logger
                    .trace("--T--> @DealResolutionHandler.handleHold---statusCat != Mc.DEAL_STATUS_CATEGORY_DECISION");

            return;
        }

        if (holdOption == UNDEFINED)
        {
            setActiveMessageToAlert(BXResources.getSysMsg(
                    "DEAL_RESOLUTION_HOLD_MISSING_HOLD_OPTION", theSessionState
                            .getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);

            logger
                    .trace("--T--> @DealResolutionHandler.handleHold---UNDEFINED");

            return;
        }
        
        //5.0 FXP27305
		TextField tbTheNoteText = (TextField) getCurrNDPage().getDisplayField("tbBrokerNoteText");
		if (StringUtils.isEmpty(tbTheNoteText.getValue().toString())) {

			setActiveMessageToAlert(BXResources.getSysMsg(
					"DEAL_RESOLUTION_HOLD_MISSING_BROKER_NOTE",
					theSessionState.getLanguageId()),
					ActiveMsgFactory.ISCUSTOMCONFIRM);
			return;
		}

        //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
        //--> By Billy 06Jan2004
        //--> Add handle for hold for Hours and Mins
        long timeToHoldInSecs = 0;
        Date holdDate = null;
        Date curDate = new Date();
        Calendar newDueDate = Calendar.getInstance();
        newDueDate.setTime(curDate);

        if (holdOption == HOLDFORDAYS)
        {
            // get Hold for no days
            int noOfDays = getHoldDurationFor("tbHoldForDays");
            logger.trace("Inside handleHold()---HOLDFORDAYS: noOfDays = "
                    + noOfDays);

            // get Hold for no hours
            int noOfHrs = getHoldDurationFor("tbHoldForHrs");
            logger.trace("Inside handleHold()---HOLDFORDAYS: noOfHrs = "
                    + noOfHrs);

            // get Hold for no Mins
            int noOfMins = getHoldDurationFor("tbHoldForMins");
            logger.trace("Inside handleHold()---HOLDFORDAYS: noOfMins = "
                    + noOfMins);

            // Calc for the time in Seconds to hold
            timeToHoldInSecs = (noOfDays
                    * (new BusinessCalendar(srk)
                            .getBusinessDayDurationInMinutes()) * 60)
                    + (noOfHrs * 60 * 60) + (noOfMins * 60);
            logger
                    .trace("Inside handleHold()---HOLDFORDAYS: timeToHoldInSecs = "
                            + timeToHoldInSecs);

            if (timeToHoldInSecs <= 0)
            {
                setActiveMessageToAlert(BXResources.getSysMsg(
                        "DEAL_RESOLUTION_HOLD_INVALID_NUMBER_OF_DAYS",
                        theSessionState.getLanguageId()),
                        ActiveMsgFactory.ISCUSTOMCONFIRM);

                logger
                        .trace("--T--> @DealResolutionHandler.handleHold()---timeToHoldInSecs <= 0");

                return;
            }

            //log to dealHistory -- Comment it out; not make sence -- By Billy
            // 16May2001
            //Log to history --> By Billy 07Jan2004
            srk.beginTransaction();
            msg = "Deal was held for " + noOfDays + " days, " + noOfHrs
                    + " hours and " + noOfMins + " mins";
            hLog.log(deal.getDealId(), deal.getCopyId(), msg,
                    Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId());
            srk.commitTransaction();
        } else if (holdOption == HOLDUNTILDATE)
        {
            holdDate = getHoldDurationDateSelected();

            logger.trace("Inside handleHold5---HOLDUNTILDATE:  holdDate = "
                    + holdDate.toString());

            if (validHoldDate(holdDate) == false)
            {
                setActiveMessageToAlert(BXResources.getSysMsg(
                        "DEAL_RESOLUTION_HOLD_INVALID_HOLD_DATE",
                        theSessionState.getLanguageId()),
                        ActiveMsgFactory.ISCUSTOMCONFIRM);

                logger
                        .trace("--T--> @DealResolutionHandler.handleHold()---validHoldDate(holdDate) == false");

                return;
            }

            //Log to history --> By Billy 07Jan2004
            srk.beginTransaction();
            msg = "Deal was held until "
                    + new SimpleDateFormat("dd MMM yyyy").format(holdDate);
            hLog.log(deal.getDealId(), deal.getCopyId(), msg,
                    Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId());
            srk.commitTransaction();
        }

        //===================================================================
        // notify workflow engine to extend open tasks
        try
        {
            WFTaskExecServices tes = new WFTaskExecServices(
                    (PageHandlerCommon) this, pg);

            srk.beginTransaction();

            tes.extendOpenTasks(false, srk, pg, -1, theSessionState.getDealInstitutionId(), 
                                holdDate, timeToHoldInSecs,
                    -1, -1, -1 );

            //      -- Moved down by Neil on Jan/28/2005
            //      // add Broker note -- New requirement -- By Billy 13July2001
            //      createBrokerNote(pg, srk);
            //
            //      //--> New requirement for Release2.2
            //      //--> Send Pending Message to Broker -- By Billy 23Jan2003
            //      if
            // (PropertiesCache.getInstance().getProperty("com.basis100.deal.hold.sendpendingmsg",
            // "Y")
            //            .equals("Y"))
            //      {
            //        DocumentRequest.requestPendingMessage(srk, deal);
            //
            //        //=========================================================
            //      }
            //
            //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
            //--> By Billy 06Jan2004
            //--> Get and set Hold reason if selected
            int holdReasonId = getHoldReasonId();
            if (holdReasonId > 0)
            {
                deal.setHoldReasonId(holdReasonId);
            }

            //--> Ticket#158 : BMO retain Mortgage Servicing Number for Deny
            // and Collapsing
            //--> Moved the Mortgage Servicing Number out from the Approval
            // Section
            //--> By Billy 13Jan2004
            if (isDisplayServicingNumField() == true)
            {
                String theSrvNum = (String) getCurrNDPage()
                        .getDisplayFieldValue("tbServMortgNum");
                deal.setServicingMortgageNumber(theSrvNum);
            }

            //==============================================================================
            deal.ejbStore();

            //==============================================================================
            // add Broker note -- New requirement -- By Billy 13July2001
            createBrokerNote(pg, srk);
            
            //--> New requirement for Release2.2
            //--> Send Pending Message to Broker -- By Billy 23Jan2003
            if (PropertiesCache.getInstance().getProperty(
                    theSessionState.getDealInstitutionId(),
                    "com.basis100.deal.hold.sendpendingmsg", "Y").equals("Y"))
            {
            	//DA-DEC-UC-3 starts
            	//DocumentRequest.requestPendingMessage(srk, deal);
            	try {
            		MasterDeal masterDeal = new MasterDeal(srk, null, deal.getDealId());
            		if (masterDeal.getFXLinkSchemaId() == MasterDeal.FXLINKSCHEMA_FCX_V_1_0) {    
            			
            			// For Manual Deal originated from Deal Entry screen will have channel Media as 'M' 
        	     	    // Block updating the Deal event status,Condition update status and deal decision for deals created from express.. 
        	     	    // FXP32829 - Dec 06 2011 - FSD S:\Project Repository\CMMI Active Projects\Express 4.2 Release\4 BA - 
            			// PA Baselines\Data Alignment FSD updated version v1.14 and use case DA-DEC-UC-3
            			
        	            if(deal.getChannelMedia() != null && deal.getChannelMedia().trim().equalsIgnoreCase(Xc.CHANNEL_MEDIA_M )) {
        	            	srk.commitTransaction();
        	            	
        	            	setupForDealNotes(pg, BXResources.getSysMsg(
        	                        "DEAL_RESOULTION_ENTER_DEAL_EXTENDED_NOTE", theSessionState
        	                                .getLanguageId()));
        	                navigateToNextPage(true);
        	                return;
        	            }
            			//FXP27305, Nov 23, 2009: check doc business rule for holding -- start
            			DocBusinessRuleExecutor dbre = new DocBusinessRuleExecutor(srk);
            			if( dbre.evaluateRequest(
            					deal,Dc.DOCUMENT_GENERAL_TYPE_PENDING_MESSAGE).isCreate()){
            				
               	          	ESBOutboundQueue queue = new ESBOutboundQueue(srk);
            				queue.create(queue.createPrimaryKey(), 
            						ServiceConst.SERVICE_TYPE_LOANDECISION_UPDATE, deal.getDealId(), 
            						deal.getSystemTypeId(), srk.getExpressState().getUserProfileId());
            				queue.setServiceSubTypeid(ServiceConst.SERVICE_SUBTYPE_LOANDECISION_HOLD);
            				queue.ejbStore();
            			}
            			//FXP27305 -- end
            		} else {
            			DocumentRequest.requestPendingMessage(srk, deal);
            		}
            	} catch (Exception e) {
            		EmailSender.sendAlertEmail("Deal Resolution", e);
            		throw e;
            	}
            	//DA-DEC-UC-3 ends
            }
            //==============================================================================

            //-- ========== SCR#750 begins ========== --//
            //-- by Neil on Jan/27/2005
            //-- wrap the switch with if.
            if (PropertiesCache.getInstance().getProperty(
                    theSessionState.getDealInstitutionId(),
                    "com.basis100.conditions.isdjdocumentgeneration", "N")
                    .equals("Y"))
            {
                //--DJ_HR_CR004--start--//
                //// Send required document based on HoldReason option.
                switch (holdReasonId)
                {
                case Mc.HOLD_REASON_REFERRED_EXTERNAL:
                    logger
                            .debug("VLAD ===> TEST_Before :: Before send the requestMtgSubmittedForDecision Letter!!");

                    //// New request from Product to send the documents wrapped
                    // up into the bundles:
                    //// new 1050 bundle is:
                    //// #50 (Cover Letter) + #119 + #120.
                    ////DocumentRequest.requestMtgSubmittedForDecision(srk,
                    // deal);
                    DocumentRequest.requestMtgSubmittedForDecisionBundle(srk,
                            deal);

                    //// Upon producing #50 DJ wants to see attachments as of
                    // #119 and #120.
                    ////DocumentRequest.requestCreditApplication(srk, deal);
                    ////DocumentRequest.requestCreditAnalysisForm(srk, deal);
                    //--Ticket#656--start--//
                    // New change request from DJ: generate the DealSummaryLite
                    // except #119.
                    // Note that #119 is not a part of 1047 bundle.
                    DocumentRequest.requestDealSummaryDJ(srk, deal);

                    //--Ticket#656--end--//
                    logger
                            .debug("VLAD ===> TEST_After :: Sent the requestConditionalMtgApproval Letter with Attachments!!");

                    break;

                case Mc.HOLD_REASON_WAITING_FOR_CREDIT_EXPERIENCE:
                    logger
                            .debug("VLAD ===> TEST_Before :: Before send the requestCreditExperience Letter!!");
                    DocumentRequest.requestCreditExperience(srk, deal);
                    logger
                            .debug("VLAD ===> TEST_After :: Sent requestCreditExperience Letter!!");

                    break;

                default:
                    break;
                }
                //--DJ_HR_CR004--end--//
                logger.debug("==>by Neil: notification = yes in switch block");
            }
            //-- ========== SCR#750 ends ========== --//

            //-- ========== SCR#750 begins ========== --//
            //-- by Neil on Dec/22/2004
            //-- only if HOLDREASON.NOTIFICATION is 'Y',
            //-- generates the pending message.
            // get the NOTIFICATION value
            String theSQL = " SELECT " + "NOTIFICATION " + "FROM "
                    + "HOLDREASON " + "WHERE " + "HOLDREASONID = "
                    + holdReasonId;
            logger.debug("==>by Neil: holdReason = " + holdReasonId);
            logger.debug("==>by Neil: sql = " + theSQL);
            String isNotification = "N";

            JdbcExecutor jExec = srk.getJdbcExecutor();
            int key = jExec.execute(theSQL);

            while (jExec.next(key))
            {
                isNotification = jExec.getString(key, "NOTIFICATION");
                logger
                        .debug("@DealResolutionHandler.handleHod: Notification = '"
                                + isNotification + "'");
            }

            jExec.closeData(key);

            if (isNotification.equalsIgnoreCase("Y"))
            {
                logger.debug("==>by Neil: notification = yes");
                // Trigger workflow
                workflowTrigger(2, srk, pg, null);
            }

            //-- ========== SCR#750 ends ========== --//

            //=============================================================================
            srk.commitTransaction();

            setupForDealNotes(pg, BXResources.getSysMsg(
                    "DEAL_RESOULTION_ENTER_DEAL_EXTENDED_NOTE", theSessionState
                            .getLanguageId()));
            navigateToNextPage(true);
        } catch (Exception e)
        {
            //String Msg = "Exception encountered @DealResolution.handleHold(): timeToHoldInSecs="
            //        + timeToHoldInSecs
            //        + ", date="
            //        + holdDate
            //        + ", dealId="
            //        + deal.getDealId() + ", copyId=" + deal.getCopyId();
            logger.error(msg);
            throw new Exception(msg);
        }
    }

    /**
     * Navigate user to Deal Notes screen after resolution processed
     *
     * @param pe
     *            DOCUMENT ME!
     * @param messageToUser
     *            DOCUMENT ME!
     */
    private void setupForDealNotes(PageEntry pe, String messageToUser)
    {
        // providing copy id since know to point to non-tx copy at this point!
        PageEntry pgEntry = setupMainPagePageEntry(Sc.PGNM_DEAL_NOTES_ID, pe
                .getPageDealId(), -1, theSessionState.getDealInstitutionId());

        // New required by User to Skip displaying the message -- By Billy
        // 13July2001
        //if (messageToUser != null)
        //          setActiveMessageToAlert(messageToUser,
        // ActiveMsgFactory.ISCUSTOMCONFIRM);

        pgEntry.setPageCondition5(pe.getPageCondition5());
        
        getSavedPages().setNextPage(pgEntry);
    }

    /**
     *
     *
     */
    private void finalApprovalActivities(boolean confirmed, boolean autoMIReq,
            SessionResourceKit srk, PageEntry pg) throws Exception
    {
        ////logger.debug("DRH@finalApprovalActivities::Stamp1");
        // if uncomfirmed check if MI required but not yet requested ... if so
        // dialog user to auto request MI as
        // part of approval activity (dialog response will trigger confirming
        // call).
        Deal deal = null;

        CCM ccm = new CCM(srk);
        //Check if display MI Request Message from Parameter -- By BILLY
        // 11April2002
        if ((confirmed == false)
                && PropertiesCache.getInstance().getProperty(
                        theSessionState.getDealInstitutionId(),
                        "com.basis100.deal.approval.mirequestmsg", "Y").equals(
                        "Y"))
        {
            deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
            
            // Ticket 250 - modified condition to display MI request message
            if (deal.getSpecialFeatureId() != Mc.SPECIAL_FEATURE_PRE_APPROVAL  && deal.getMIStatusId() == Mc.MI_STATUS_BLANK)
            {
            	pg.setPageCondition5(true);
            }
        }
        // Begin 18Nov2005 -- Decline Letter changes
        // add decline letter dialog only if there's a premium on the rate
        // pg page condition 3 is only set in the case approval status (screen pick-list) = approve and
        // if AAG rules run, nothing should be triggered.
        if (!pg.getPageCondition3())
        {
          pg.setPageCondition4(false);
          deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
          if (deal.getPremium() > 0)
          {  String prop = PropertiesCache.getInstance().
                 getProperty(theSessionState.getDealInstitutionId(),
                             "mosApp.MosSystem.dealresolution.declineletter", "N");
             if (prop.equals("Y")) {
                 declineLetterSetup();
                 return;
             }
          }
        }
        // End Decline Letter changes

        // -- //
        int finalApproverId = srk.getExpressState().getUserProfileId();

        String msg = " ";

        ////logger.debug("DRH@finalApprovalActivities::Stamp3");
        DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);

        srk.beginTransaction();

        standardAdoptTxCopy(pg, true);

        deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

        int oldStatus = deal.getStatusId();

        String msgForStatus = "";

        deal = DBA.recordDealStatus(srk, deal, Mc.DEAL_APPROVED, -1, -1);

        ////logger.debug("DRH@finalApprovalActivities::Stamp4");
        // Create History Log if Status changed
        if (oldStatus != deal.getStatusId())
        {
            ////logger.debug("DRH@finalApprovalActivities::Stamp5");
            msgForStatus = hLog.statusChange(oldStatus, "Deal Resolution");
            hLog.log(deal.getDealId(), deal.getCopyId(), msgForStatus,
                    Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), deal
                            .getStatusId());
        }

        deal.setFinalApproverId(finalApproverId);

        CalcMonitor dcm = CalcMonitor.getMonitor(srk);

        ////logger.debug("DRH@finalApprovalActivities::Stamp6");
        deal.setCalcMonitor(dcm);

        ////logger.debug("DRH@finalApprovalActivities::Stamp7");
        // -- //
        msg = hLog.getFinalApproverLogMessage(srk.getExpressState().getUserProfileId(), srk);

        ////logger.debug("DRH@finalApprovalActivities::Stamp8");
        ccm.issueCommitment(deal, autoMIReq, theSessionState.getLanguageId());

        deal.ejbStore();

        /***** FXP31423 = EXP500999 start *****/
        logger.debug("DRH@handleApproval.  Beginning to insert condition update request.");
        ConditionUpdateRequester requester = new ConditionUpdateRequester(srk);
        int result = requester.insertConditionUpdateRequest(deal.getDealId(), false);
        if(result < -1) {
            logger.debug("DRH@handleApproval.  Failed to insert condition update request.");
        }
        else {
            logger.debug("DRH@handleApproval. Inserted condition update request successfully.");
        }                
        /***** FXP31423 = EXP500999 end  *****/
        
        // register offer immediately (later may wait confirmation of offer
        // delivery)
        oldStatus = deal.getStatusId();

        ////logger.debug("DRH@finalApprovalActivities::Stamp9");
        if (deal.getSpecialFeatureId() == Mc.SPECIAL_FEATURE_PRE_APPROVAL)
        {

            ////logger.debug("DRH@finalApprovalActivities::Stamp10_1");
            //msg = hLog.getFinalApproverLogMessage(srk.getUserProfileId(),
            // srk);
            DBA.recordDealStatus(srk, deal, Mc.DEAL_PRE_APPROVAL_OFFERED);

            // pre-approval ...
            // Create History Log if Status changed
            if (oldStatus != deal.getStatusId())
            {

                msgForStatus = hLog.statusChange(oldStatus, "Deal Resolution");
                hLog.log(deal.getDealId(), deal.getCopyId(), msgForStatus,
                        Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), deal
                                .getStatusId());
            }

            hLog.log(pg.getPageDealId(), pg.getPageDealCID(), msg,
                    Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), deal
                            .getStatusId());
        } else
        {

            ////logger.debug("DRH@finalApprovalActivities::Stamp10_2");
            //msg = hLog.getFinalApproverLogMessage(srk.getUserProfileId(),
            // srk);
            DBA.recordDealStatus(srk, deal, Mc.DEAL_COMMIT_OFFERED);

            // Create History Log if Status changed
            if (oldStatus != deal.getStatusId())
            {
                msgForStatus = hLog.statusChange(oldStatus, "Deal Resolution");
                hLog.log(deal.getDealId(), deal.getCopyId(), msgForStatus,
                        Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), deal
                                .getStatusId());
            }

            hLog.log(pg.getPageDealId(), pg.getPageDealCID(), msg,
                    Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), deal
                            .getStatusId());
        }

        ////logger.debug("DRH@finalApprovalActivities::Stamp11");
        //// SYNCADD.
        // Update Servicing Number based on Parameter setting -- by Billy
        // 10June2002
        if (isDisplayServicingNumField() == true)
        {
            ////String theSrvNum =
            // getCurrNDPage().getDisplayFieldStringValue("tbServMortgNum");
            TextField tbTheSrvNum = (TextField) getCurrNDPage()
                    .getDisplayField("tbServMortgNum");
            String theSrvNum = (String) tbTheSrvNum.getValue();

            //logger.debug("DRH@handleApproval::SrvNum: " + theSrvNum);
            deal.setServicingMortgageNumber(theSrvNum);
        }

        ////===============================================
        //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
        //--> By Billy 09Jan2004
        //--> Reset Hold Reason if decision made
        deal.setHoldReasonId(0);

        //=============================================================
        deal.ejbStore();

        ////logger.debug("DRH@finalApprovalActivities::Stamp12");
        doCalculation(dcm);

        workflowTrigger(2, srk, pg, null);

        //***** Change by NBC/PP Implementation Team - Start *****//
        // check if Floated Down Rate task will be created
        LenderProfile dealLender = deal.getLenderProfile();
        boolean rfdSupported = false;
        if (dealLender != null)
        {
             rfdSupported = dealLender.isSupportRateFloatDown();
        }

        MtgProd mtgprod = deal.getMtgProd();
        boolean validRfd = false;
        if (mtgprod != null)
        {
            
            validRfd = mtgprod.isValidRateFloatDown();
        }
        
        boolean taskCreated = false;
        if (deal.getFloatDownTaskCreatedFlag() != null)
        {
            taskCreated = deal.getFloatDownTaskCreatedFlag().equalsIgnoreCase("Y");           
        }

        boolean taskCompleted = false;
        if (deal.getFloatDownCompletedFlag() != null)
        {
            taskCompleted = deal.getFloatDownCompletedFlag().equalsIgnoreCase("Y");            
        }

        if (rfdSupported && 
                !taskCreated &&
                !taskCompleted &&
                deal.getRateGuaranteePeriod() > 0 &&
                validRfd)
        {
            deal.setFloatDownTaskCreatedFlag("Y");
            deal.ejbStore();
        }
        //***** Change by NBC/PP Implementation Team - End *****//
        
        ////logger.debug("DRH@finalApprovalActivities::Stamp13");
        // add Broker note -- New requirement -- By Billy 13July2001
        createBrokerNote(pg, srk);

        ////logger.debug("DRH@finalApprovalActivities::Stamp13_1");
        setupForDealNotes(pg, BXResources.getSysMsg(
                "DEAL_RESOULTION_ENTER_APPROVAL_NOTE", theSessionState
                        .getLanguageId()));

        ////logger.debug("DRH@finalApprovalActivities::Stamp14");
        srk.commitTransaction();

        // generate the decline letter
        // this must be done after the status change
        if (pg.getPageCondition4())
        {
            generateRateBonusLetter(deal);
        }

        ////logger.debug("DRH@finalApprovalActivities::Stamp15");
        //--Exchange Lender Folder Creation 2008-12-23//
        // If lender picks up option create DocCentral deal folder on 'Approval'

        // (Settig in DCCONFIG table) this activity should be processed on
        // success
        // of any previous one.
//        DocCentralInterface dci = new DocCentralInterface(srk);

        //don't create folders for manual deals - FXP29987
        if (deal.getSystemTypeId() != Mc.SYSTEM_TYPE_MANUAL_ENTRY)
        {
            ExchangeFolderCreator dci = new ExchangeFolderCreator(srk);
            int fldrIndicator = dci.getFolderCreationIndicator();

            if (fldrIndicator == Mc.CREATE_DOC_CENTRAL_DEAL_FOLDER_ON_APPROVAL || 
	                fldrIndicator == Mc.CREATE_DOC_CENTRAL_DEAL_FOLDER_ON_APPROVAL_2    )
	        {
	            String folderIdentificator = createDocCentralFolder(srk, deal, dci);
	            logger
	                    .debug("DRH@finalApprovalActivities::Stamp16_BeforeHitServer");
	
	            if (folderIdentificator
	                    .equals(DEAL_FOLDER_DOC_CENTRAL_CREATION_FAILED))
	            {
	                setActiveMessageToAlert(BXResources.getSysMsg(
	                        "DEAL_FOLDER_DOC_CENTRAL_CREATION_FAILED",
	                        theSessionState.getLanguageId()),
	                        ActiveMsgFactory.ISCUSTOMCONFIRM);
	            } else
	            {
	                String foldermsg = BXResources.getSysMsg(
	                        "DEAL_FOLDER_DOC_CENTRAL_CREATED", theSessionState
	                                .getLanguageId());
	                foldermsg += folderIdentificator;
	                setActiveMessageToAlert(foldermsg,
	                        ActiveMsgFactory.ISCUSTOMCONFIRM);
	            }
	        }
        }

        ////logger.debug("DRH@finalApprovalActivities::Stamp16_AfterDocCentralCreation");
        //--DocCentral_FolderCreate--21Sep--2004--end--//
        navigateToNextPage(true);
    }

    //return which deal resolution resolve type has been selected
    private int getResolutionStatus()
    {
        ////CSpRadioButtonsGroup rbxApprove =(CSpRadioButtonsGroup)
        // getCurrNDPage().getDisplayField("*rbApproveDeal");
        RadioButtonGroup rbxApprove = (RadioButtonGroup) getCurrNDPage()
                .getDisplayField("rbApproveDeal");
        String approveDeal = (String) rbxApprove.getValue();

        //logger.debug("DRH@getResolutionStatus::rbApproveValue: " +
        // approveDeal);
        ////if (rbxApprove.getSelectedIndex() != - 1)
        if (!approveDeal.equals("") && (rbxApprove != null))
        {
            //logger.debug("DRH@getResolutionStatus::Approval mode");
            return APPROVAL;
        }

        ////CSpRadioButtonsGroup rbxHold =(CSpRadioButtonsGroup)
        // getCurrNDPage().getDisplayField("*rbHoldPendingInfo");
        RadioButtonGroup rbxHold = (RadioButtonGroup) getCurrNDPage()
                .getDisplayField("rbHoldPendingInfo");
        String holdInfo = (String) rbxHold.getValue();

        //logger.debug("DRH@getResolutionStatus::rbHoldValue: " + holdInfo);
        ////if (rbxHold.getSelectedIndex() != - 1)
        if (!holdInfo.equals("") && (rbxHold != null))
        {
            //logger.debug("DRH@getResolutionStatus::Hold mode");
            return HOLD;
        }

        ////CSpRadioButtonsGroup rbxDeny =(CSpRadioButtonsGroup)
        // getCurrNDPage().getDisplayField("*rbDeny");
        RadioButtonGroup rbxDeny = (RadioButtonGroup) getCurrNDPage()
                .getDisplayField("rbDeny");
        String denyInfo = (String) getCurrNDPage().getDisplayFieldValue(
                "rbDeny");

        //logger.debug("DRH@getResolutionStatus::rbDenyValue: " + denyInfo);
        ////if (rbxDeny.getSelectedIndex() != - 1)
        if (!denyInfo.equals("") && (rbxDeny != null))
        {
            //logger.debug("DRH@getResolutionStatus::Deny mode");
            return DENY;
        }

        ////CSpRadioButtonsGroup rbxCollapse =(CSpRadioButtonsGroup)
        // getCurrNDPage().getDisplayField("*rbCollapseDeal");
        RadioButtonGroup rbxCollapse = (RadioButtonGroup) getCurrNDPage()
                .getDisplayField("rbCollapseDeal");
        String collapseInfo = (String) getCurrNDPage().getDisplayFieldValue(
                "rbCollapseDeal");

        //logger.debug("DRH@getResolutionStatus::rbxCollapse: " +
        // collapseInfo);
        ////if (rbxCollapse.getSelectedIndex() != - 1)
        if (!collapseInfo.equals("") && (rbxCollapse != null))
        {
            //logger.debug("DRH@getResolutionStatus::Collapse mode");
            return COLLAPSE;
        }

        return UNDEFINED;
    }

    //return what type of hold duration has been selected
    private int getHoldPendingInformation()
    {
        ////CSpRadioButtonsGroup rbxHoldDays =(CSpRadioButtonsGroup)
        // getCurrNDPage().getDisplayField("*rbHoldFor");
        ////String rbxHoldDays = (String)
        // getCurrNDPage().getDisplayFieldValue("rbHoldFor");
        RadioButtonGroup rbxHoldDays = (RadioButtonGroup) getCurrNDPage()
                .getDisplayField("rbHoldFor");
        String holdDays = (String) rbxHoldDays.getValue();

        //logger.debug("DRH@getHoldPendingInformation::rbxHoldDays: " +
        // holdDays);
        ////if (rbxHoldDays.getSelectedIndex() != - 1)
        if (!holdDays.equals("") && (rbxHoldDays != null))
        {
            //logger.debug("DRH@getHoldPendingInformation::HoldDays mode");
            return HOLDFORDAYS;
        }

        ////CSpRadioButtonsGroup rbxHoldUntilDate =(CSpRadioButtonsGroup)
        // getCurrNDPage().getDisplayField("*rbHoldUntil");
        RadioButtonGroup rbxHoldUntilDate = (RadioButtonGroup) getCurrNDPage()
                .getDisplayField("rbHoldUntil");
        String holdUntil = (String) rbxHoldUntilDate.getValue();

        //logger.debug("DRH@getHoldPendingInformation::rbxHoldUntilDate: " +
        // holdUntil);
        ////if (rbxHoldUntilDate.getSelectedIndex() != - 1)
        if (!holdUntil.equals("") && (rbxHoldUntilDate != null))
        {
            //logger.debug("DRH@getHoldPendingInformation::HoldUntil mode");
            return HOLDUNTILDATE;
        }

        return UNDEFINED;
    }

    /**
     *
     *
     */
    private int getHoldDurationFor(String fieldName)
    {
        String sHoldForDay = (String) getCurrNDPage().getDisplayFieldValue(
                fieldName);

        //logger.debug("DRH@getHoldDurationForNoDays::HoldForDay: " +
        // sHoldForDay);
        int holdFor = 0;

        try
        {
            //// Wrong auto-conversion.
            holdFor = (new Integer(sHoldForDay)).intValue();

            //logger.trace("DRH@getHoldDurationForNoDays: holdForDay = " +
            // holdForDay);
        } catch (Exception e)
        {
            holdFor = 0;
        }

        return holdFor;
    }

    /**
     *
     *
     */
    private java.util.Date getHoldDurationDateSelected()
    {
        //get the month
        ComboBox cbxHoldUntilMonth = (ComboBox) getCurrNDPage()
                .getDisplayField("cbHoldUntilMonth");
        String sHoldUntilMonth = cbxHoldUntilMonth.getValue().toString();

        //logger.debug("DRH@getHoldDurationDateSelected::Month: " +
        // sHoldUntilMonth);
        int month = -1;

        try
        {
            ////month
            // =((Integer)TypeConverter.asInt(cbxHoldUntilMonth.getSelected().getValue()));
            month = (new Integer(sHoldUntilMonth)).intValue();

            //logger.trace("DRH@getHoldDurationDateSelected: month = " +
            // month);
        } catch (Exception e)
        {
            month = -1;
        }

        //get the day
        ////TextField tbHoldUntilDay =(TextField)
        // getCurrNDPage().getDisplayFieldValue("tbHoldUntilDay");
        TextField tbHoldUntilDay = (TextField) getCurrNDPage().getDisplayField(
                "tbHoldUntilDay");
        String sHoldUntilDay = (String) tbHoldUntilDay.getValue();

        //logger.debug("DRH@getHoldDurationDateSelected::Day: " +
        // sHoldUntilDay);
        int day = -1;

        try
        {
            ////day =(int)TypeConverter.asInt(tbHoldUntilDay);
            day = (new Integer(sHoldUntilDay)).intValue();

            //logger.trace("DRH@getHoldDurationDateSelected: day = " + day);
        } catch (Exception e)
        {
            day = -1;
        }

        //get the year
        ////TextField tbHoldUntilYear =(TextField)
        // getCurrNDPage().getDisplayFieldValue("tbHoldUntilYear");
        TextField tbHoldUntilYear = (TextField) getCurrNDPage()
                .getDisplayField("tbHoldUntilYear");
        String sHoldUntilYear = (String) tbHoldUntilYear.getValue();

        //logger.debug("DRH@getHoldDurationDateSelected::Year: " +
        // sHoldUntilYear);
        int year = -1;

        try
        {
            ///year =(int)TypeConverter.asInt(tbHoldUntilYear);
            year = (new Integer(sHoldUntilYear)).intValue();

            //logger.trace("DRH@getHoldDurationDateSelected: year = " + year);
        } catch (Exception e)
        {
            year = -1;
        }

        //construct the Date
        Date holdUntilDate = new Date((year - 1900), (month - 1), day);

        //logger.trace("DRH@getHoldDurationDateSelected: holdUntilDate = "
        //               + holdUntilDate.toString());
        return holdUntilDate;
    }

    /**
     *
     *
     */
    private boolean validHoldDate(Date dueDate)
    {
        // ensure hold date is in the future...
        Date curDate = new Date();

        if (curDate.after(dueDate))
        {
            return false;
        } else
        {
            return true;
        }
    }

    /**
     *
     *
     */
    private int getApprovalType()
    {
        ComboBox cbxApprovalType = (ComboBox) getCurrNDPage().getDisplayField(
                "cbApprovalType");
        String sApprovalType = cbxApprovalType.getValue().toString();

        //logger.debug("DRH@getApprovalType::ApprovalType: " + sApprovalType);
        int apptype = -1;

        try
        {
            ////apptype
            // =((Integer)TypeConverter.asInt(cbxApprovalType.getSelected().getValue()));
            apptype = (new Integer(sApprovalType)).intValue();

            //logger.debug("DRH@getApprovalType::ApprovalIndex: " + apptype);
        } catch (Exception e)
        {
            apptype = -1;
        }

        return apptype;
    }

    //get denial status or denial reason
    private int getDenialStatus()
    {
        ComboBox cbxDenialOptions = (ComboBox) getCurrNDPage().getDisplayField(
                "cbDenialOptions");
        String sDenialOptions = cbxDenialOptions.getValue().toString();

        int denyOption = -1;

        try
        {
            ////denyOption
            // =((Integer)TypeConverter.asInt(cbxDenialOptions.getSelected().getValue()));
            denyOption = (new Integer(sDenialOptions)).intValue();
            logger
                    .debug("DealResolutionhandler--getDenialStatus(), denyOption = "
                            + denyOption);
        } catch (Exception e)
        {
            denyOption = -1;
        }

        return denyOption;
    }

    /**
     *
     *
     */
    private void populateResolutionAndDenialOptions(ViewBean NDPage)
    {
        PageEntry pg = theSessionState.getCurrentPage();

        SessionResourceKit srk = getSessionResourceKit();

        Deal deal = null;

        ////boolean hideApproveOption = true;
        pg.setPageCondition2(true);

        // hide approve option if:
        //      1) not here via Underwriter Worksheet
        //      2) deal status is not one of decision category
        Hashtable pst = pg.getPageStateTable();

        if (pst != null)
        {
            Object o = pst.get("RESOLVE_FROM_UWORKSHEET");

            if (o != null)
            {
                // viewing recommended scenario
                try
                {
                    deal = DBA.getDeal(srk, pg.getPageDealId(), pg
                            .getPageDealCID());

                    int status = deal.getStatusId();
                    DealStatusManager dsm = DealStatusManager.getInstance(srk);
                    int statusCategory = dsm.getStatusCategory(deal
                            .getStatusId(), srk);

                    if ((statusCategory == Mc.DEAL_STATUS_CATEGORY_DECISION)
                            && (status != Mc.DEAL_APPROVED))
                    {
                        ////hideApproveOption = false;
                        pg.setPageCondition2(false);
                    }
                } catch (Exception e)
                {
                    ;
                }
            }
        }

        //boolean denyAvailable = true;

        //--Release2.1--//
        //--> By John 26Nov2002
        //  Grab text from BXResource
        //  public static final String[] RES_Received_LABELS=
        //{ "Approve Deal", "Hold, Pending Information", "Deny", "Collapse
        // Deal"};
        String[] res_labels = new String[4];
        int index = 0;

        res_labels[index++] = BXResources.getSysMsg("DEAL_RES_APPROVE_DEAL",
                theSessionState.getLanguageId());
        res_labels[index++] = BXResources.getSysMsg(
                "DEAL_RES_HOLD_PENDING_INFO", getTheSessionState()
                        .getLanguageId());
        res_labels[index++] = BXResources.getSysMsg("DEAL_RES_DENY",
                theSessionState.getLanguageId());
        res_labels[index++] = BXResources.getSysMsg("DEAL_RES_COLLAPSE_DEAL",
                theSessionState.getLanguageId());

        int[] res_status = RES_Received_STATUS;

        // resolution options ....
        RadioButtonGroup rbxResolutionOptions = (RadioButtonGroup) NDPage
                .getDisplayField("rbApproveDeal");
        String approveDeal = (String) rbxResolutionOptions.getValue();

        //logger.debug("DRH@populateResolutionAndDenialOptions::approveDeal: "
        // + approveDeal);
        //// Obsolete ND call.
        ////rbxResolutionOptions.removeAllChildren(true);
        OptionList rbxResolutionOptionsList = ((pgDealResolutionViewBean) NDPage).rbApproveDealOptions;

        ////rbxResolutionOptions.addSelectable(res_labels [ 0 ], new
        // Integer(res_status [ 0 ]));
        rbxResolutionOptionsList.clear();
        rbxResolutionOptionsList.add(res_labels[0],
                (new Integer(res_status[0])).toString());

        StaticTextField stxApprovalTypeLabel = (StaticTextField) NDPage
                .getDisplayField("stApprovalTypeLabel");

        //logger.debug("DRH@populateResolutionAndDenialOptions::ApprovalTypeLabel:
        // " + stxApprovalTypeLabel);
        //// SYNCADD.
        // Added ServicingNumber text box -- by Billy 10June2002
        DisplayField stxApprovalServNumLabel = (DisplayField) NDPage
                .getDisplayField("stApprovalServicingNumLabel");

        DisplayField tbServMortgNum = (DisplayField) NDPage
                .getDisplayField("tbServMortgNum");

        //======================================================
        ////if (hideApproveOption == true)
        if (pg.getPageCondition2() == true) //// hide Approve option.
        {
            //logger.debug("DRH@populateResolutionAndDenialOptions::Hide
            // Approve Option");
            setDisplayApproveOption(pg, NDPage);
            rbxResolutionOptionsList.clear();
        }

        //--> Ticket#158 : BMO retain Mortgage Servicing Number for Deny and
        // Collapsing
        //--> Moved the Mortgage Servicing Number out from the Approval Section
        //--> By Billy 13Jan2004
        dispServNumberConditional(pg, NDPage);

        //=============================================================================
        RadioButtonGroup rbxHoldPendingInfo = (RadioButtonGroup) NDPage
                .getDisplayField("rbHoldPendingInfo");
        String holdPendingInfo = (String) rbxHoldPendingInfo.getValue();

        //logger.debug("DRH@populateResolutionAndDenialOptions::holdPendingInfo:
        // " + holdPendingInfo);
        OptionList rbxHoldPendingInfoList = ((pgDealResolutionViewBean) NDPage).rbHoldPendingInfoOptions;

        ////rbxHoldPendingInfo.addSelectable(res_labels [ 1 ], new
        // Integer(res_status [ 1 ]));
        rbxHoldPendingInfoList.clear();
        rbxHoldPendingInfoList.add(res_labels[1], (new Integer(res_status[1]))
                .toString());

        //// Obsolete ND call.
        ////rbxHoldPendingInfo.unselectAll();
        RadioButtonGroup rbxDenyOptions = (RadioButtonGroup) NDPage
                .getDisplayField("rbDeny");
        String deny = (String) rbxDenyOptions.getValue();

        //logger.debug("DRH@populateResolutionAndDenialOptions::Deny: " +
        // deny);
        //// Obsolete ND call.
        ////rbxDeny.removeAllChildren(true);
        OptionList rbxDenyList = ((pgDealResolutionViewBean) NDPage).rbDenyOptions;

        ////rbxDeny.addSelectable(res_labels [ 2 ], new Integer(res_status [ 2
        // ]));
        rbxDenyList.clear();
        rbxDenyList.add(res_labels[2], (new Integer(res_status[2])).toString());

        //// Obsolete ND call.
        ////rbxDeny.unselectAll();
        ////CSpRadioButtonsGroup rbxCollapseDeal =(CSpRadioButtonsGroup)
        // NDPage.getDisplayField("*rbCollapseDeal");
        RadioButtonGroup rbxCollapseDealOptions = (RadioButtonGroup) NDPage
                .getDisplayField("rbCollapseDeal");
        String collapse = (String) rbxCollapseDealOptions.getValue();

        //logger.debug("DRH@populateResolutionAndDenialOptions::Collapse: " +
        // collapse);
        //// Obsolete ND call.
        ////rbxCollapseDeal.removeAllChildren(true);
        OptionList rbxCollapseDealList = ((pgDealResolutionViewBean) NDPage).rbCollapseDealOptions;

        ////rbxCollapseDeal.addSelectable(res_labels [ 3 ], new
        // Integer(res_status [ 3 ]));
        rbxCollapseDealList.clear();
        rbxCollapseDealList.add(res_labels[3], (new Integer(res_status[3]))
                .toString());

        //// Obsolete ND call.
        ////rbxCollapseDeal.unselectAll();
        // hold options
        ////CSpRadioButtonsGroup rbxHoldFor =(CSpRadioButtonsGroup)
        // NDPage.getDisplayField("*rbHoldFor");
        RadioButtonGroup rbxHoldForOptions = (RadioButtonGroup) NDPage
                .getDisplayField("rbHoldFor");

        ////logger.debug("DRH@populateResolutionAndDenialOptions::LabelForNonSelect:
        // " + rbxHoldForOptions.getLabelForNoneSelected());
        rbxHoldForOptions.setMultiSelect(false);

        String holdFor = (String) rbxHoldForOptions.getValue();

        ////logger.debug("DRH@populateResolutionAndDenialOptions::HoldFor: " +
        // holdFor);
        //// Obsolete ND call.
        ////rbxHoldFor.removeAllChildren(true);
        OptionList rbxHoldForList = ((pgDealResolutionViewBean) NDPage).rbHoldForOptions;

        ////rbxHoldFor.addSelectable("Hold for", new Integer(1));
        rbxHoldForList.clear();

        //--Release2.1--//
        //--> By John 26Nov2002
        //rbxHoldForList.add("Hold for", (new Integer(1)).toString());
        rbxHoldForList.add(BXResources.getSysMsg("DEAL_RES_HOLD_FOR",
                theSessionState.getLanguageId()), (new Integer(1)).toString());

        //--------------//
        //// Obsolete ND call.
        ////rbxHoldFor.unselectAll();
        RadioButtonGroup rbxHoldUntilOptions = (RadioButtonGroup) NDPage
                .getDisplayField("rbHoldUntil");
        rbxHoldUntilOptions.setMultiSelect(false);

        String holdUntil = (String) rbxHoldUntilOptions.getValue();

        ////logger.debug("DRH@populateResolutionAndDenialOptions::HoldUntil: "
        // + holdUntil);
        //// Obsolete ND call.
        ////rbxHoldUntil.removeAllChildren(true);
        OptionList rbxHoldUntilList = ((pgDealResolutionViewBean) NDPage).rbHoldUntilOptions;

        ////rbxHoldUntil.addSelectable("Hold Until", new Integer(2));
        rbxHoldUntilList.clear();

        //--Release2.1--//
        //--> By John 26Nov2002
        //rbxHoldUntilList.add("Hold Until", (new Integer(2)).toString());
        rbxHoldUntilList.add(BXResources.getSysMsg("DEAL_RES_HOLD_UNTIL",
                theSessionState.getLanguageId()), (new Integer(2)).toString());

        //--------------//
        //// Obsolete ND call.
        ////rbxHoldUntil.unselectAll();
        // months
        //ComboBox cbxHoldUntilMonth =(ComboBox)
        // NDPage.getDisplayField("cbHoldUntilMonth");
        //// Obsolete ND call
        ////cbxHoldUntilMonth.removeAllChildren(true);
        //int lim = CONST_MONTH_NAMES.length;
        //logger.debug("DRH@populateResolutionAndDenialOptions::Limit: " +
        // lim);
        ////OptionList cbxHoldUntilMonth = new OptionList();
        //--Release2.1--//
        //--> By John 26Nov2002
        this.fillupMonths("cbHoldUntilMonth");

        /*
         * String [] labels = new String[lim]; String [] values = new
         * String[lim];
         *
         * for(int j = 0; j < lim; j ++) {
         * ///cbxHoldUntilMonth.addSelectable(CONST_MONTH_NAMES [ j ], new
         * Integer(CONST_MONTH_VALUES [ j ]));
         * logger.debug("DRH@populateResolutionAndDenialOptions::Label[" + j +
         * "]: " + CONST_MONTH_NAMES [ j ]);
         * logger.debug("DRH@populateResolutionAndDenialOptions::Value[" + j +
         * "]: " + new Integer(CONST_MONTH_VALUES [ j ]));
         *
         * labels[j] = CONST_MONTH_NAMES [ j ]; values[j] = (new
         * Integer(j)).toString(); } //// Obsolete ND call.
         * ////cbxHoldUntilMonth.unselectAll(); OptionList holdUntilMonthOption =
         * ((pgDealResolutionViewBean) NDPage).cbHoldUntilMonthOptions;
         *
         * //// BXTODO: test this clearance. holdUntilMonthOption.clear();
         *
         * holdUntilMonthOption.setOptions(labels, values);
         */

        // denial reasons ...
        ComboBox cbxDenialOptions = (ComboBox) NDPage
                .getDisplayField("cbDenialOptions");

        //// Obsolete ND call.
        ////cbxDenialOptions.removeAllChildren(true);
        //// Manual population of the Sort Combobox.
        //--Release2.1--//
        //  Population handled in pgDealResolutionViewBean
        //--> By John 26Nov2002
        /*
         * OptionList cbxDenialOptionsOptions = ((pgDealResolutionViewBean)
         * NDPage).cbDenialOptionsOptions;
         *
         * try { JdbcExecutor jExec = getSessionResourceKit().getJdbcExecutor();
         * String sql = "Select DRDESCRIPTION, DENIALREASONID from DENIALREASON ";
         * if (denyAvailable == false) sql = sql + "WHERE DENIALREASONID = " +
         * deal.getDenialReasonId(); else sql = sql + "order by DENIALREASONID";
         * int key = jExec.execute(sql);
         *
         * for(; jExec.next(key); ) {
         * ////cbxDenialOptions.addSelectable(jExec.getString(key, 1), new
         * Integer(jExec.getInt(key, 2)));
         * cbxDenialOptionsOptions.add(jExec.getString(key, 1), (new
         * Integer(jExec.getInt(key, 2))).toString()); } jExec.closeData(key); }
         * catch(Exception e) { //// Obsolete ND call
         * ////cbxDenialOptions.removeAllChildren(true);
         * ////cbxDenialOptions.addSelectable("N/A", new Integer(0));
         *
         * cbxDenialOptionsOptions.clear(); cbxDenialOptionsOptions.add("N/A",
         * (new Integer(0)).toString()); }
         */
        //==================================================================
        //// Obsolete ND call
        ////cbxDenialOptions.clear();
        ////cbxDenialOptions.select(0);
        cbxDenialOptions.setValue((new Integer(0)).toString(), true);

        // approval type combo box ...
        ComboBox cbxApprovalType = (ComboBox) NDPage
                .getDisplayField("cbApprovalType");

        //// Obsolete ND call.
        ////cbxApprovalType.removeAllChildren(true);
        //// Manual population of the Sort Combobox.
        OptionList cbxApprovalTypeOptions = ((pgDealResolutionViewBean) NDPage).cbApprovalTypeOptions;
        cbxApprovalTypeOptions.clear();

        try
        {
            JdbcExecutor jExec = getSessionResourceKit().getJdbcExecutor();
            String sql = "Select ATDESCRIPTION, APPROVALTYPEID from APPROVALTYPE ORDER BY APPROVALTYPEID ";
            int key = jExec.execute(sql);

            for (; jExec.next(key);)
            {
                ////cbxApprovalType.addSelectable(jExec.getString(key, 1), new
                // Integer(jExec.getInt(key, 2)));
                //--Release2.1--start//
                //// Now it should be taken from the BXResources.
                ////cbxApprovalTypeOptions.add(jExec.getString(key, 1), (new
                // Integer(jExec.getInt(key, 2))).toString());
                String label = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(),
                        "APPROVALTYPE", jExec.getInt(key, 2), theSessionState
                                .getLanguageId());

                String value = (new Integer(jExec.getInt(key, 2))).toString();
                cbxApprovalTypeOptions.add(label, value);
            }

            jExec.closeData(key);
        } catch (Exception exc)
        {
            //// Obsolete ND call
            ////cbxApprovalType.removeAllChildren(true);
            ////cbxApprovalType.addSelectable("N/A", new Integer(0));
            cbxApprovalTypeOptions.add("N/A", (new Integer(0)).toString());
        }

        //// Obsolete ND call
        ////cbxApprovalType.clear();
        ////cbxApprovalType.select(0);
        cbxApprovalType.setValue((new Integer(0)).toString(), true);

        ////if (hideApproveOption == true)
        if (pg.getPageCondition2() == true)
        {
            //// Obsolete ND call. Finish, now is commented to test the submit
            // business logic.
            //// This is done before, re-group into one call.
            ////logger.debug("DRH@populateResolutionAndDenialOptions::HideApproveOpt?
            // " + hideApproveOption);
            setDisplayApproveOption(pg, NDPage);

            ////cbxApprovalType.setVisible(false);
            cbxApprovalTypeOptions.clear();
        }
    }

    /**
     *
     *
     */
    private void setDisplayApproveOption(PageEntry pg, ViewBean thePage)
    {
        //// Set Mask to make the filed not dispalyed
        String suppressStart = "<!--";
        String suppressEnd = "-->";

        if (pg.getPageCondition2() == false)
        {
            //// Unset mask to display the field
            thePage.setDisplayFieldValue("stIncludeApproveOptionStart",
                    new String(""));
            thePage.setDisplayFieldValue("stIncludeApproveOptionEnd",
                    new String(""));

            return;
        } else if (pg.getPageCondition2() == true) //// suppress
        {
            thePage.setDisplayFieldValue("stIncludeApproveOptionStart",
                    new String(suppressStart));
            thePage.setDisplayFieldValue("stIncludeApproveOptionEnd",
                    new String(suppressEnd));
        }

        return;
    }

    //--> Ticket#158 : BMO retain Mortgage Servicing Number for Deny and
    // Collapsing
    //--> Moved the Mortgage Servicing Number out from the Approval Section
    //--> By Billy 13Jan2004
    private void dispServNumberConditional(PageEntry pg, ViewBean thePage)
    {
        //// Set Mask to make the filed not dispalyed
        String suppressStart = "<!--";
        String suppressEnd = "-->";

        if (isDisplayServicingNumField() == true)
        {
            //// Unset mask to display the field
            thePage.setDisplayFieldValue("stIncludeSrvcNumberStart",
                    new String(""));
            thePage.setDisplayFieldValue("stIncludeSrvcNumberEnd", new String(
                    ""));

            return;
        } else if (isDisplayServicingNumField() == false) //// suppress
        {
            thePage.setDisplayFieldValue("stIncludeSrvcNumberStart",
                    new String(suppressStart));
            thePage.setDisplayFieldValue("stIncludeSrvcNumberEnd", new String(
                    suppressEnd));
        }

        return;
    }

    /**
     *
     *
     */
    public void handleToUWSheet()
    {
        PageEntry pg = theSessionState.getCurrentPage();

        // Not necessary to pass the Coypid ==> same as re-entry of the UW by
        // GOTO button
        PageEntry pgUW = setupMainPagePageEntry(Sc.PGNM_UNDERWRITER_WORKSHEET,
                pg.getPageDealId(), -1, theSessionState.getDealInstitutionId());

        getSavedPages().setNextPage(pgUW);

        navigateToNextPage(true);

        return;
    }

    /**
     *
     *
     */
    public void createBrokerNote(PageEntry pe, SessionResourceKit srk)
            throws Exception
    {
        ////TextField tbTheNoteText = (TextField)
        // getCurrNDPage().getDisplayFieldValue("tbBrokerNoteText");
        TextField tbTheNoteText = (TextField) getCurrNDPage().getDisplayField(
                "tbBrokerNoteText");

        String theNoteText = tbTheNoteText.getValue().toString();

        ////logger.debug("DRH@CreateBrokerNote::TheNoteText: " + theNoteText);
        // check if any text input
        if ((theNoteText == null) || theNoteText.trim().equals(""))
        {
            return;
        }

        // insert note
        int dealId = pe.getPageDealId();

        int userId = theSessionState.getSessionUserId();

        DealPK dealPk = new DealPK(dealId, 1);

        // only needed to pass deal id
        DealNotes dealNotes = new DealNotes(srk);

        //#DG426 String noteText = StringUtil.makeQuoteSafe(theNoteText);

        dealNotes.create(dealPk, dealId,
                Sc.DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS, theNoteText, null,
                userId);
    }

    /**
     *
     *
     */
    public void saveData(PageEntry pg, boolean calledInTransaction)
    {
        // get Broker note text and store in page state 1
        String msgText = ((String) getCurrNDPage().getDisplayFieldValue(
                "tbBrokerNoteText")).toString();

        //logger.debug("DRH@saveData::msgText: " + msgText);
        pg.setPageState1(msgText);

        //// SYNCADD.
        // get Servicing Number and store in PageState2
        msgText = ((String) getCurrNDPage().getDisplayFieldValue(
                "tbServMortgNum")).toString();

        pg.setPageState2(msgText);
    }

    /**
     * SYNCADD.
     *
     * @return DOCUMENT ME!
     */
    private boolean isDisplayServicingNumField()
    {
        return (PropertiesCache.getInstance().getProperty(
                 theSessionState.getDealInstitutionId(),
                 "com.basis100.deal.approval.displayservnumfield", "N")
                .equals("Y"));
    }

    //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
    //--> By Billy 09Jan2004
    // Method to get Hold Reason ID
    private int getHoldReasonId()
    {
        String sHoldReason = (String) getCurrNDPage().getDisplayFieldValue(
                "cbHoldReason");
        int holdReasonId = -1;

        try
        {
            holdReasonId = (new Integer(sHoldReason)).intValue();
        } catch (Exception e)
        {
            holdReasonId = -1;
        }

        return holdReasonId;
    }

    protected String createDocCentralFolder(SessionResourceKit srk, Deal deal,
            ExchangeFolderCreator dci)

    {
        String ret = DEAL_FOLDER_DOC_CENTRAL_CREATION_FAILED;

        try
        {
            int folderIndicator = dci.getFolderCreationIndicator();
            String folderIdentifier = "";
            logger.debug("DRH@createDocCentralFolder::FolderIndicator: "
                    + folderIndicator);

            if ((folderIndicator == Mc.DO_NOT_CREATE_DOC_CENTRAL_DEAL_FOLDER)
                    || (folderIndicator == Mc.CREATE_DOC_CENTRAL_DEAL_FOLDER_ON_SUBMISSION  ) )
            {
                // do nothing, in the second case folder creation is postponed
                // until deal approval.
                logger
                        .trace("DRH@createDocCentralFolder::Do not create Doc Central Deal folder");
            } else if (folderIndicator == Mc.CREATE_DOC_CENTRAL_DEAL_FOLDER_ON_APPROVAL  ) {
                // Check first whether the folder for this deal is created or
                // not.
                logger
                        .trace("DRH@createDocCentralFolder::Create Doc Central Deal folder");

                // Just sanity check, it should not be here at the moment.
                folderIdentifier = "";

                logger.debug("DRH@createDocCentralFolder::DealFoundId: "
                        + deal.getDealId());
                logger.debug("DRH@createDocCentralFolder::DealFoundCopyId: "
                        + deal.getCopyId());
                logger.debug("DRH@createDocCentralFolder::DealFoundCopyType: "
                        + deal.getCopyType());
                logger.debug("DRH@createDocCentralFolder::FolderExists?: "
                        + dci.folderExists(deal));

                if (!dci.folderExists(deal))
                {
                    folderIdentifier = dci.createFolder(deal);

                    String sfpIdentifier = deal.getSourceFirmProfile()
                            .getSourceFirmName();
                    logger
                            .debug("DRH@createDocCentralFolder::sobIdentifierToDocCentral: "
                                    + sfpIdentifier);

                    int sfpId = deal.getSourceFirmProfileId();

                    SourceFirmProfile SFPBean = new SourceFirmProfile(srk)
                            .findByPrimaryKey(new SourceFirmProfilePK(sfpId));

                    logger
                            .debug("DRH@createDocCentralFolder::sobIdentifierToDocCentralFromBean: "
                                    + SFPBean.getSourceFirmName());

                    // Create record in DealHistory table and form the return
                    // Folder Identifier.
                    if ((folderIdentifier != null)
                            && !folderIdentifier.trim().equals(""))
                    {
                        DealHistoryLogger hLog = DealHistoryLogger
                                .getInstance(srk);
                        String msg = hLog.docCentralFolderCreationInfo(SFPBean,
                                folderIdentifier);
                        hLog
                                .log(
                                        deal.getDealId(),
                                        deal.getCopyId(),
                                        msg,
                                        Mc.DEAL_TX_TYPE_DOCCENTRAL_FOLDER_CREATION,
                                        srk.getExpressState().getUserProfileId(), deal
                                                .getStatusId());

                        ret = folderIdentifier;
                    } else
                    {
                        ret = DEAL_FOLDER_DOC_CENTRAL_CREATION_FAILED;
                    }
                }
                // Catherne, 14-Jul-05, fix for #1517 ----- begin -----------------
                // do not return DEAL_FOLDER_DOC_CENTRAL_CREATION_FAILED
                else {
                  ret = dci.getFolderCode(deal);
                }
                // Catherne, 14-Jul-05, fix for #1517 ----- end -----------------
            }
        } catch (Exception ex)
        {
            String msg = "DRH@createDocCentralFolder::NOTE: Failed to create folder "
                    + " Deal: " + deal.getDealId();
            logger.error(msg);

            //// Add this function later
            ////handler.logDocCentralFolderFailure(srk, beanPK, SFPBean,
            // folderIdentifier);
        }

        logger.debug("DRH@createDocCentralFolder::FolderIdentifier: " + ret);

        return ret;
    }

    //--DocCentral_FolderCreate--21Sep--2004--end--//
    
    
    public void handleOKSpecial() {

        SessionResourceKit srk = getSessionResourceKit();
        try {

            PageEntry pg = theSessionState.getCurrentPage();
            workflowTrigger(2, srk, pg, null);
            navigateToNextPage(true);

        } catch (Exception e) {
            SysLogger logger = srk.getSysLogger();
            logger.error("Exception @DealResolution.getSessionResourceKit()");
            logger.error(e);
            setStandardFailMessage();
        }
    }
    
    public boolean isSpecialWorkflowTrigerEnable(){
        
        // property
        String key = PropertiesCache.getInstance().getProperty(-1, 
                "com.filogix.dealresolution.viewonly.workflowtriger","N");
        if("N".equals(key)) return false;
        
        // only in view-only mode
        if(theSessionState.getCurrentPage().isEditable()) return false;
        
        // only for administrators
        switch (getTheSessionState().getSessionUserType()) {
        case Mc.USER_TYPE_JR_ADMIN:
        case Mc.USER_TYPE_ADMIN:
        case Mc.USER_TYPE_SR_ADMIN:
            return true;
        default:
            return false;
        }
        
    }

}
