package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgIWorkQueuerpPirorityCountTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgIWorkQueuerpPirorityCountTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doPirorityCountModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStPriorityLabel().setValue(CHILD_STPRIORITYLABEL_RESET_VALUE);
		getStPriorityCount().setValue(CHILD_STPRIORITYCOUNT_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STPRIORITYLABEL,StaticTextField.class);
		registerChild(CHILD_STPRIORITYCOUNT,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoPirorityCountModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
          //----SCR#1147--start--23Mar2005--//
          boolean movedToRow = super.nextTile();
          if (movedToRow)
          {
            int rowNum = this.getTileIndex();

            //logger.debug("IWQPCTV@nextTile::rowNum: " + rowNum);
            iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
            handler.pageGetState(this.getParentViewBean());

            try
            {
              handler.setTaskStatusWithHoldReason();
            }
            catch (Exception e)
            {
              e.printStackTrace();
            }

            handler.populatedRepeatedFields(1, rowNum);
            handler.pageSaveState();
          }
          //----SCR#1147--start--23Mar2005--//

          return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STPRIORITYLABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPirorityCountModel(),
				CHILD_STPRIORITYLABEL,
				doPirorityCountModel.FIELD_DFPRIORITYDESC,
				CHILD_STPRIORITYLABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPRIORITYCOUNT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPirorityCountModel(),
				CHILD_STPRIORITYCOUNT,
				doPirorityCountModel.FIELD_DFPRIORITYCOUNT,
				CHILD_STPRIORITYCOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPriorityLabel()
	{
		return (StaticTextField)getChild(CHILD_STPRIORITYLABEL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPriorityCount()
	{
		return (StaticTextField)getChild(CHILD_STPRIORITYCOUNT);
	}


	/**
	 *
	 *
	 */
	public doPirorityCountModel getdoPirorityCountModel()
	{
		if (doPirorityCount == null)
			doPirorityCount = (doPirorityCountModel) getModel(doPirorityCountModel.class);
		return doPirorityCount;
	}


	/**
	 *
	 *
	 */
	public void setdoPirorityCountModel(doPirorityCountModel model)
	{
			doPirorityCount = model;
	}


      	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
        private iWorkQueueHandler handler = new iWorkQueueHandler();

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STPRIORITYLABEL="stPriorityLabel";
	public static final String CHILD_STPRIORITYLABEL_RESET_VALUE="";
	public static final String CHILD_STPRIORITYCOUNT="stPriorityCount";
	public static final String CHILD_STPRIORITYCOUNT_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doPirorityCountModel doPirorityCount=null;

}

