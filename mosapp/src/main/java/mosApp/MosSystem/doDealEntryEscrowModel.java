package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doDealEntryEscrowModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfEscrowPaymentId();

	
	/**
	 * 
	 * 
	 */
	public void setDfEscrowPaymentId(java.math.BigDecimal value);

	
    /**
     * 
     * 
     */
    public java.math.BigDecimal getDfCopyId();

    
    /**
     * 
     * 
     */
    public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEscrowPaymentDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfEscrowPaymentDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfEscrowPayment();

	
	/**
	 * 
	 * 
	 */
	public void setDfEscrowPayment(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfEscrowTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfEscrowTypeId(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFESCROWPAYMENTID="dfEscrowPaymentId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFESCROWPAYMENTDESC="dfEscrowPaymentDesc";
	public static final String FIELD_DFESCROWPAYMENT="dfEscrowPayment";
	public static final String FIELD_DFESCROWTYPEID="dfEscrowTypeId";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

