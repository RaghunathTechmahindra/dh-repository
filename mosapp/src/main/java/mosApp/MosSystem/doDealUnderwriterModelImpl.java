package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;

/**
 *
 *
 *
 */
public class doDealUnderwriterModelImpl extends QueryModelBase
	implements doDealUnderwriterModel
{
	/**
	 *
	 *
	 */
	public doDealUnderwriterModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfUnderwriterId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFUNDERWRITERID);
	}


	/**
	 *
	 *
	 */
	public void setDfUnderwriterId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFUNDERWRITERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfUnderwriter()
	{
		return (String)getValue(FIELD_DFUNDERWRITER);
	}


	/**
	 *
	 *
	 */
	public void setDfUnderwriter(String value)
	{
		setValue(FIELD_DFUNDERWRITER,value);
	}

	/**
	 * MetaData object test method to verify the SYNTHETIC new
	 * field for the reogranized SQL_TEMPLATE query.
	 *
	 */
  /**
	public void setResultSet(ResultSet value)
	{
  		logger = SysLog.getSysLogger("METADATA");

  		try
  		{
        super.setResultSet(value);
   			if( value != null)
   			{
       			ResultSetMetaData metaData = value.getMetaData();
       			int columnCount = metaData.getColumnCount();
       			logger.debug("===============================================");
            logger.debug("Testing MetaData Object for new synthetic fields for" +
                          " doDealUnderwriterModel");
       			logger.debug("NumberColumns: " + columnCount);
         		for(int i = 0; i < columnCount ; i++)
          		{
                 	logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
          		}
          		logger.debug("================================================");
      		}
  		}
  		catch (SQLException e)
  		{
    		    logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
  		}
 	}
  **/

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	////public static final String SELECT_SQL_TEMPLATE="SELECT ALL USERPROFILE.USERPROFILEID, CONTACT.COPYID, CONTACT.CONTACTLASTNAME || ' ' || SUBSTR(CONTACT.CONTACTFIRSTNAME,0,1 ) Underwriter FROM CONTACT, USERPROFILE  __WHERE__  ";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL USERPROFILE.USERPROFILEID, CONTACT.COPYID, CONTACT.CONTACTLASTNAME || ' ' || SUBSTR(CONTACT.CONTACTFIRSTNAME,0,1 ) SYNTHTETICUNDERWRITER FROM CONTACT, USERPROFILE  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="CONTACT, USERPROFILE";
	public static final String STATIC_WHERE_CRITERIA=" (((CONTACT.CONTACTID  =  USERPROFILE.CONTACTID AND CONTACT.INSTITUTIONPROFILEID  =  USERPROFILE.INSTITUTIONPROFILEID)) AND CONTACT.COPYID = 1)";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFUNDERWRITERID="USERPROFILE.USERPROFILEID";
	public static final String COLUMN_DFUNDERWRITERID="USERPROFILEID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="CONTACT.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	////public static final String QUALIFIED_COLUMN_DFUNDERWRITER=".";
	////public static final String COLUMN_DFUNDERWRITER="";
	public static final String QUALIFIED_COLUMN_DFUNDERWRITER="CONTACT.SYNTHTETICUNDERWRITER";
	public static final String COLUMN_DFUNDERWRITER="SYNTHTETICUNDERWRITER";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUNDERWRITERID,
				COLUMN_DFUNDERWRITERID,
				QUALIFIED_COLUMN_DFUNDERWRITERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		////FIELD_SCHEMA.addFieldDescriptor(
		////	new QueryFieldDescriptor(
		////		FIELD_DFUNDERWRITER,
		////		COLUMN_DFUNDERWRITER,
		////		QUALIFIED_COLUMN_DFUNDERWRITER,
		////		String.class,
		////		false,
		////		false,
		////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		////		"",
		////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		////		""));

    //// Adjusted FieldDescriptor for the ComputedColumn to fix the iMT tool bug.
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUNDERWRITER,
				COLUMN_DFUNDERWRITER,
				QUALIFIED_COLUMN_DFUNDERWRITER,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"CONTACT.CONTACTLASTNAME || ' ' || SUBSTR(CONTACT.CONTACTFIRSTNAME,0,1 )",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"CONTACT.CONTACTLASTNAME || ' ' || SUBSTR(CONTACT.CONTACTFIRSTNAME,0,1 )"));

	}

}

