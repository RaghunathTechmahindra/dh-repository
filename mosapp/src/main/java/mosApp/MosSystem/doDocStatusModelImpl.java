package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;


/**
 *
 *
 *
 */
public class doDocStatusModelImpl extends QueryModelBase
	implements doDocStatusModel
{
	/**
	 *
	 *
	 */
	public doDocStatusModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDocStatusId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDOCSTATUSID);
	}


	/**
	 *
	 *
	 */
	public void setDfDocStatusId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDOCSTATUSID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDocStatusDesc()
	{
		return (String)getValue(FIELD_DFDOCSTATUSDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfDocStatusDesc(String value)
	{
		setValue(FIELD_DFDOCSTATUSDESC,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL DOCUMENTSTATUS.DOCUMENTSTATUSID, DOCUMENTSTATUS.DOCUMENTSTATUSDESCRIPTION FROM DOCUMENTSTATUS  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="DOCUMENTSTATUS";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDOCSTATUSID="DOCUMENTSTATUS.DOCUMENTSTATUSID";
	public static final String COLUMN_DFDOCSTATUSID="DOCUMENTSTATUSID";
	public static final String QUALIFIED_COLUMN_DFDOCSTATUSDESC="DOCUMENTSTATUS.DOCUMENTSTATUSDESCRIPTION";
	public static final String COLUMN_DFDOCSTATUSDESC="DOCUMENTSTATUSDESCRIPTION";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOCSTATUSID,
				COLUMN_DFDOCSTATUSID,
				QUALIFIED_COLUMN_DFDOCSTATUSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOCSTATUSDESC,
				COLUMN_DFDOCSTATUSDESC,
				QUALIFIED_COLUMN_DFDOCSTATUSDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

