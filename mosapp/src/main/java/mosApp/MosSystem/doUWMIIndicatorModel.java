package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 * WARNING - BJH Nov 20, 2002 : Dropped usage - no longer necessary
 *
 */
public interface doUWMIIndicatorModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIIndicatorId();


	/**
	 *
	 *
	 */
	public void setDfMIIndicatorId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfMIIndicatorDescription();


	/**
	 *
	 *
	 */
	public void setDfMIIndicatorDescription(String value);




	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFMIINDICATORID="dfMIIndicatorId";
	public static final String FIELD_DFMIINDICATORDESCRIPTION="dfMIIndicatorDescription";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

