package mosApp.MosSystem;

import java.util.Hashtable;

import MosSystem.Sc;

import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.view.TiledView;

/**
 *
 *
 */
public class TransactionHistoryHandler extends PageHandlerCommon
	implements Cloneable, Sc
{
	/**
	 *
	 *
	 */
	public TransactionHistoryHandler cloneSS()
	{
		return(TransactionHistoryHandler) super.cloneSafeShallow();

	}


	//This method is used to populate the fields in the header
	//with the appropriate information

	public void populatePageDisplayFields()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds =(PageCursorInfo) pst.get("Repeated1");

		try
		{
			populatePageShellDisplayFields();

			// Quick Link Menu display
			displayQuickLinkMenu();
			populateTaskNavigator(pg);
			populatePageDealSummarySnapShot();
			populatePreviousPagesLinks();
			revisePrevNextButtonGeneration(pg, tds);
			getCurrNDPage().setDisplayFieldValue("stBeginLine", new Integer(tds.getFirstDisplayRowNumber()));
			getCurrNDPage().setDisplayFieldValue("stEndLine", new Integer(tds.getLastDisplayRowNumber()));
			getCurrNDPage().setDisplayFieldValue("stTotalLines", new Integer(tds.getTotalRows()));
		}
		catch(Exception e)
		{
			logger.error("Exception @TransactionhistoryHandler.populatePageDisplayFields()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}

	}


	//
	// Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
	//

	public void setupBeforePageGeneration()
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			if (pg.getSetupBeforeGenerationCalled() == true) return;
			pg.setSetupBeforeGenerationCalled(true);

			setupDealSummarySnapShotDO(pg);

			QueryModelBase theDO =(QueryModelBase)
          (RequestManager.getRequestContext().getModelManager().getModel(doTransactionHistoryModel.class));
			theDO.clearUserWhereCriteria();
			theDO.addUserWhereCriterion("dfDealId", "=", new String("" + pg.getPageDealId()));

      PageCursorInfo tds = getPageCursorInfo(pg, "Repeated1");
			Hashtable pst = pg.getPageStateTable();
			theDO.executeSelect(null);
			int numRows = theDO.getSize();

			//			logger.trace("!!!!!!!!!NUM_ROWS=" + numRows);
			if (tds == null)
			{
				String voName = "Repeated1";
				int perPage = ((TiledView)(getCurrNDPage().getChild(voName))).getMaxDisplayTiles();
				tds = new PageCursorInfo(voName, voName, perPage, numRows);
				tds.setRefresh(true);
				pst.put(tds.getName(), tds);
			}
			else
			{
				tds.setTotalRows(numRows);
			}
		}
		catch(Exception e)
		{
			logger.error("Exception @TransactionhistoryHandler.setupBeforePageGeneration()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}

	}


	/**
	 *
	 *
	 */
	public void handleForwardButton()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();

		try
		{
			PageCursorInfo tds =(PageCursorInfo) pst.get("Repeated1");
			//int numTransactionHistories = 0;
			handleForwardBackwardCommon(pg, tds, - 1, true);
		}
		catch(Exception e)
		{
			logger.error("Exception @TransactionhistoryHandler.handleForwardButton()");
			logger.error(e);
			setStandardFailMessage();
		}
	}


	/**
	 *
	 *
	 */
	public void handleBackwardButton()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		try
		{
			PageCursorInfo tds =(PageCursorInfo) pst.get("Repeated1");
			//int numTransactionHistories = 0;
			handleForwardBackwardCommon(pg, tds, - 1, false);
		}
		catch(Exception e)
		{
			logger.error("Exception @TransactionhistoryHandler.handleBackwardButton()");
			logger.error(e);
			setStandardFailMessage();
		}
	}


	/**
	 *
	 *
	 */
	public void populateTransactionHistoryLineNumber(int rowIndex)
	{
    getCurrNDPage().setDisplayFieldValue("Repeated1/stLineNumber", new String("" + rowIndex));
    getCurrNDPage().setDisplayFieldValue("Repeated1/stDivId", new String("details" + rowIndex));
	}


	/**
	 *
	 *
	 */
	public void handleSubmit()
	{
		handleCancelStandard();
	}


	/**
	 *
	 *
	 */
	public void handleCancel()
	{
		handleCancelStandard();
	}

}

