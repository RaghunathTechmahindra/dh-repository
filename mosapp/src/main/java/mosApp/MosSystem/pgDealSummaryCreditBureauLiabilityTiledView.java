package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgDealSummaryCreditBureauLiabilityTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealSummaryCreditBureauLiabilityTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(2);
		setPrimaryModelClass( doDetailViewCBLiabilitiesModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStCBType().setValue(CHILD_STCBTYPE_RESET_VALUE);
		getStCBLiabilityDesc().setValue(CHILD_STCBLIABILITYDESC_RESET_VALUE);
		getStCBTotalAmount().setValue(CHILD_STCBTOTALAMOUNT_RESET_VALUE);
		getStCBMonthlyPayment().setValue(CHILD_STCBMONTHLYPAYMENT_RESET_VALUE);
		getStCBLiabilityGDS().setValue(CHILD_STCBLIABILITYGDS_RESET_VALUE);
		getStCBLiabilityTDS().setValue(CHILD_STCBLIABILITYTDS_RESET_VALUE);
		getStCBLiabilityPayOffDesc().setValue(CHILD_STCBLIABILITYPAYOFFDESC_RESET_VALUE);
		getStCreditBureauIndicator().setValue(CHILD_STCREDITBUREAUINDICATOR_RESET_VALUE);
	    getStCreditBureauLiabilityLimit().setValue(CHILD_STCREDITBUREAULIABILITYLIMIT_RESET_VALUE);
        getStMaturityDate().setValue(CHILD_STMATURITYDATE_RESET_VALUE);
	}


	/**
	 * 
	 * 
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STCBTYPE,StaticTextField.class);
		registerChild(CHILD_STCBLIABILITYDESC,StaticTextField.class);
		registerChild(CHILD_STCBTOTALAMOUNT,StaticTextField.class);
		registerChild(CHILD_STCBMONTHLYPAYMENT,StaticTextField.class);
		registerChild(CHILD_STCBLIABILITYGDS,StaticTextField.class);
		registerChild(CHILD_STCBLIABILITYTDS,StaticTextField.class);
		registerChild(CHILD_STCBLIABILITYPAYOFFDESC,StaticTextField.class);
        registerChild(CHILD_STCREDITBUREAUINDICATOR,StaticTextField.class);
        registerChild(CHILD_STCREDITBUREAULIABILITYLIMIT,StaticTextField.class);
        registerChild(CHILD_STMATURITYDATE, StaticTextField.class);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDetailViewCBLiabilitiesModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		// The following code block was migrated from the CreditBureauLiability_onBeforeDisplayEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.setupNumOfRepeatedCBLiability(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}

		return movedToRow;

	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STCBTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewCBLiabilitiesModel(),
				CHILD_STCBTYPE,
				doDetailViewCBLiabilitiesModel.FIELD_DFLIABILITYTYPE,
				CHILD_STCBTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCBLIABILITYDESC))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewCBLiabilitiesModel(),
				CHILD_STCBLIABILITYDESC,
				doDetailViewCBLiabilitiesModel.FIELD_DFLIABILITYDESC,
				CHILD_STCBLIABILITYDESC_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCBTOTALAMOUNT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewCBLiabilitiesModel(),
				CHILD_STCBTOTALAMOUNT,
				doDetailViewCBLiabilitiesModel.FIELD_DFLIABILITYAMOUNT,
				CHILD_STCBTOTALAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCBMONTHLYPAYMENT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewCBLiabilitiesModel(),
				CHILD_STCBMONTHLYPAYMENT,
				doDetailViewCBLiabilitiesModel.FIELD_DFLIABILITYMONTHLYPAYMENT,
				CHILD_STCBMONTHLYPAYMENT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCBLIABILITYGDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewCBLiabilitiesModel(),
				CHILD_STCBLIABILITYGDS,
				doDetailViewCBLiabilitiesModel.FIELD_DFPERCENTINCLUDEDINGDS,
				CHILD_STCBLIABILITYGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCBLIABILITYTDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewCBLiabilitiesModel(),
				CHILD_STCBLIABILITYTDS,
				doDetailViewCBLiabilitiesModel.FIELD_DFPERCENTINCLUDEDINTDS,
				CHILD_STCBLIABILITYTDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCBLIABILITYPAYOFFDESC))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewCBLiabilitiesModel(),
				CHILD_STCBLIABILITYPAYOFFDESC,
				doDetailViewCBLiabilitiesModel.FIELD_DFLIABILITYPAYOFFDESC,
				CHILD_STCBLIABILITYPAYOFFDESC_RESET_VALUE,
				null);
			return child;
		}
		if (name.equals(CHILD_STCREDITBUREAUINDICATOR))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewCBLiabilitiesModel(),
				CHILD_STCREDITBUREAUINDICATOR,
				doDetailViewCBLiabilitiesModel.FIELD_DFCREDITBUREAUINDICATOR,
				CHILD_STCREDITBUREAUINDICATOR_RESET_VALUE,
				null);
			return child;
		}
        else if (name.equals(CHILD_STCREDITBUREAULIABILITYLIMIT)) {
            TextField child = new TextField(this,
            		getdoDetailViewCBLiabilitiesModel(),
                CHILD_STCREDITBUREAULIABILITYLIMIT,
                doDetailViewCBLiabilitiesModel.FIELD_DFCREDITBUREAULIABILITYLIMIT,
                CHILD_STCREDITBUREAULIABILITYLIMIT_RESET_VALUE,
                null);
            return child;
        }
        else if (name.equals(CHILD_STMATURITYDATE)) {
			StaticTextField child = new StaticTextField(this,
					getdoDetailViewCBLiabilitiesModel(), CHILD_STMATURITYDATE,
					doDetailViewCBLiabilitiesModel.FIELD_DFMATURITYDATE,
					CHILD_STMATURITYDATE_RESET_VALUE, null);

			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCBType()
	{
		return (StaticTextField)getChild(CHILD_STCBTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCBLiabilityDesc()
	{
		return (StaticTextField)getChild(CHILD_STCBLIABILITYDESC);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCBTotalAmount()
	{
		return (StaticTextField)getChild(CHILD_STCBTOTALAMOUNT);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCBMonthlyPayment()
	{
		return (StaticTextField)getChild(CHILD_STCBMONTHLYPAYMENT);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCBLiabilityGDS()
	{
		return (StaticTextField)getChild(CHILD_STCBLIABILITYGDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCBLiabilityTDS()
	{
		return (StaticTextField)getChild(CHILD_STCBLIABILITYTDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCBLiabilityPayOffDesc()
	{
		return (StaticTextField)getChild(CHILD_STCBLIABILITYPAYOFFDESC);
	}

    /**
     * 
     * @return
     */
    public StaticTextField getStCreditBureauIndicator() {

        return (StaticTextField) getChild(CHILD_STCREDITBUREAUINDICATOR);
    }
    
    /**
     * 
     * 
     */
    public TextField getStCreditBureauLiabilityLimit() {
        return (TextField) getChild(CHILD_STCREDITBUREAULIABILITYLIMIT);
    }
    
    /**
    *
    *
    */
    public StaticTextField getStMaturityDate()
    {
        return (StaticTextField) getChild(CHILD_STMATURITYDATE);
    }
    
	public String endStCreditBureauIndicatorDisplay(ChildContentDisplayEvent event)
	{
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setYesOrNo(event.getContent());
		handler.pageSaveState();

		return rc;
	}
	/**
	 *
	 *
	 */
	public doDetailViewCBLiabilitiesModel getdoDetailViewCBLiabilitiesModel()
	{
		if (doDetailViewCBLiabilities == null)
			doDetailViewCBLiabilities = (doDetailViewCBLiabilitiesModel) getModel(doDetailViewCBLiabilitiesModel.class);
		return doDetailViewCBLiabilities;
	}


	/**
	 *
	 *
	 */
	public void setdoDetailViewCBLiabilitiesModel(doDetailViewCBLiabilitiesModel model)
	{
			doDetailViewCBLiabilities = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STCBTYPE="stCBType";
	public static final String CHILD_STCBTYPE_RESET_VALUE="";
	public static final String CHILD_STCBLIABILITYDESC="stCBLiabilityDesc";
	public static final String CHILD_STCBLIABILITYDESC_RESET_VALUE="";
	public static final String CHILD_STCBTOTALAMOUNT="stCBTotalAmount";
	public static final String CHILD_STCBTOTALAMOUNT_RESET_VALUE="";
	public static final String CHILD_STCBMONTHLYPAYMENT="stCBMonthlyPayment";
	public static final String CHILD_STCBMONTHLYPAYMENT_RESET_VALUE="";
	public static final String CHILD_STCBLIABILITYGDS="stCBLiabilityGDS";
	public static final String CHILD_STCBLIABILITYGDS_RESET_VALUE="";
	public static final String CHILD_STCBLIABILITYTDS="stCBLiabilityTDS";
	public static final String CHILD_STCBLIABILITYTDS_RESET_VALUE="";
	public static final String CHILD_STCBLIABILITYPAYOFFDESC="stCBLiabilityPayOffDesc";
	public static final String CHILD_STCBLIABILITYPAYOFFDESC_RESET_VALUE="";
    public static final String CHILD_STCREDITBUREAUINDICATOR="stCreditBureauIndicator";
    public static final String CHILD_STCREDITBUREAUINDICATOR_RESET_VALUE="";
    public static final String CHILD_STCREDITBUREAULIABILITYLIMIT="stCreditBureauLiabilityLimit";
    public static final String CHILD_STCREDITBUREAULIABILITYLIMIT_RESET_VALUE="";
    public static final String CHILD_STMATURITYDATE = "stMaturityDate";
    public static final String CHILD_STMATURITYDATE_RESET_VALUE = "";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDetailViewCBLiabilitiesModel doDetailViewCBLiabilities=null;
  private DealSummaryHandler handler=new DealSummaryHandler();

}

