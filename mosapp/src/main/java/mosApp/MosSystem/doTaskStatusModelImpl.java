package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doTaskStatusModelImpl extends QueryModelBase
	implements doTaskStatusModel
{
	/**
	 *
	 *
	 */
	public doTaskStatusModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public Integer getMOS_TASKSTATUS_TASKSTATUSID()
	{
		return (Integer)getValue(FIELD_MOS_TASKSTATUS_TASKSTATUSID);
	}


	/**
	 *
	 *
	 */
	public void setMOS_TASKSTATUS_TASKSTATUSID(Integer value)
	{
		setValue(FIELD_MOS_TASKSTATUS_TASKSTATUSID,value);
	}


	/**
	 *
	 *
	 */
	public String getMOS_TASKSTATUS_TSDESCRIPTION()
	{
		return (String)getValue(FIELD_MOS_TASKSTATUS_TSDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setMOS_TASKSTATUS_TSDESCRIPTION(String value)
	{
		setValue(FIELD_MOS_TASKSTATUS_TSDESCRIPTION,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL TASKSTATUS.TASKSTATUSID, TASKSTATUS.TSDESCRIPTION FROM TASKSTATUS  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="TASKSTATUS";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_MOS_TASKSTATUS_TASKSTATUSID="TASKSTATUS.TASKSTATUSID";
	public static final String COLUMN_MOS_TASKSTATUS_TASKSTATUSID="TASKSTATUSID";
	public static final String QUALIFIED_COLUMN_MOS_TASKSTATUS_TSDESCRIPTION="TASKSTATUS.TSDESCRIPTION";
	public static final String COLUMN_MOS_TASKSTATUS_TSDESCRIPTION="TSDESCRIPTION";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_MOS_TASKSTATUS_TASKSTATUSID,
				COLUMN_MOS_TASKSTATUS_TASKSTATUSID,
				QUALIFIED_COLUMN_MOS_TASKSTATUS_TASKSTATUSID,
				Integer.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_MOS_TASKSTATUS_TSDESCRIPTION,
				COLUMN_MOS_TASKSTATUS_TSDESCRIPTION,
				QUALIFIED_COLUMN_MOS_TASKSTATUS_TSDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

