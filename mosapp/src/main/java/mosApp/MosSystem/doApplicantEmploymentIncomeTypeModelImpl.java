package mosApp.MosSystem;

import java.sql.SQLException;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doApplicantEmploymentIncomeTypeModelImpl extends QueryModelBase
	implements doApplicantEmploymentIncomeTypeModel
{
	/**
	 *
	 *
	 */
	public doApplicantEmploymentIncomeTypeModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 21Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
	int institutionId = theSessionState.getDealInstitutionId();
    while(this.next())
    {
      //Convert INCOMETYPE Descriptions
      this.setMOS_INCOMETYPE_ITDESCRIPTION(BXResources.getPickListDescription(
          institutionId, "INCOMETYPE", this.getMOS_INCOMETYPE_INCOMETYPEID().intValue(), languageId));
    }

   //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getMOS_INCOMETYPE_INCOMETYPEID()
	{
		return (java.math.BigDecimal)getValue(FIELD_MOS_INCOMETYPE_INCOMETYPEID);
	}


	/**
	 *
	 *
	 */
	public void setMOS_INCOMETYPE_INCOMETYPEID(java.math.BigDecimal value)
	{
		setValue(FIELD_MOS_INCOMETYPE_INCOMETYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getMOS_INCOMETYPE_ITDESCRIPTION()
	{
		return (String)getValue(FIELD_MOS_INCOMETYPE_ITDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setMOS_INCOMETYPE_ITDESCRIPTION(String value)
	{
		setValue(FIELD_MOS_INCOMETYPE_ITDESCRIPTION,value);
	}


	/**
	 *
	 *
	 */
	public String getMOS_INCOMETYPE_EMPLOYMENTRELATED()
	{
		return (String)getValue(FIELD_MOS_INCOMETYPE_EMPLOYMENTRELATED);
	}


	/**
	 *
	 *
	 */
	public void setMOS_INCOMETYPE_EMPLOYMENTRELATED(String value)
	{
		setValue(FIELD_MOS_INCOMETYPE_EMPLOYMENTRELATED,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getMOS_INCOMETYPE_GDSINCLUSION()
	{
		return (java.math.BigDecimal)getValue(FIELD_MOS_INCOMETYPE_GDSINCLUSION);
	}


	/**
	 *
	 *
	 */
	public void setMOS_INCOMETYPE_GDSINCLUSION(java.math.BigDecimal value)
	{
		setValue(FIELD_MOS_INCOMETYPE_GDSINCLUSION,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getMOS_INCOMETYPE_TDSINCLUSION()
	{
		return (java.math.BigDecimal)getValue(FIELD_MOS_INCOMETYPE_TDSINCLUSION);
	}


	/**
	 *
	 *
	 */
	public void setMOS_INCOMETYPE_TDSINCLUSION(java.math.BigDecimal value)
	{
		setValue(FIELD_MOS_INCOMETYPE_TDSINCLUSION,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  public static final String SELECT_SQL_TEMPLATE=
    "SELECT DISTINCT INCOMETYPE.INCOMETYPEID, INCOMETYPE.ITDESCRIPTION, "+
    "INCOMETYPE.EMPLOYMENTRELATED, INCOMETYPE.GDSINCLUSION, INCOMETYPE.TDSINCLUSION "+
    "FROM INCOMETYPE  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="INCOMETYPE";
	public static final String STATIC_WHERE_CRITERIA=" INCOMETYPE.EMPLOYMENTRELATED = 'Y' ";
    public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_MOS_INCOMETYPE_INCOMETYPEID="INCOMETYPE.INCOMETYPEID";
	public static final String COLUMN_MOS_INCOMETYPE_INCOMETYPEID="INCOMETYPEID";
	public static final String QUALIFIED_COLUMN_MOS_INCOMETYPE_ITDESCRIPTION="INCOMETYPE.ITDESCRIPTION";
	public static final String COLUMN_MOS_INCOMETYPE_ITDESCRIPTION="ITDESCRIPTION";
	public static final String QUALIFIED_COLUMN_MOS_INCOMETYPE_EMPLOYMENTRELATED="INCOMETYPE.EMPLOYMENTRELATED";
	public static final String COLUMN_MOS_INCOMETYPE_EMPLOYMENTRELATED="EMPLOYMENTRELATED";
	public static final String QUALIFIED_COLUMN_MOS_INCOMETYPE_GDSINCLUSION="INCOMETYPE.GDSINCLUSION";
	public static final String COLUMN_MOS_INCOMETYPE_GDSINCLUSION="GDSINCLUSION";
	public static final String QUALIFIED_COLUMN_MOS_INCOMETYPE_TDSINCLUSION="INCOMETYPE.TDSINCLUSION";
	public static final String COLUMN_MOS_INCOMETYPE_TDSINCLUSION="TDSINCLUSION";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_MOS_INCOMETYPE_INCOMETYPEID,
				COLUMN_MOS_INCOMETYPE_INCOMETYPEID,
				QUALIFIED_COLUMN_MOS_INCOMETYPE_INCOMETYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_MOS_INCOMETYPE_ITDESCRIPTION,
				COLUMN_MOS_INCOMETYPE_ITDESCRIPTION,
				QUALIFIED_COLUMN_MOS_INCOMETYPE_ITDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_MOS_INCOMETYPE_EMPLOYMENTRELATED,
				COLUMN_MOS_INCOMETYPE_EMPLOYMENTRELATED,
				QUALIFIED_COLUMN_MOS_INCOMETYPE_EMPLOYMENTRELATED,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_MOS_INCOMETYPE_GDSINCLUSION,
				COLUMN_MOS_INCOMETYPE_GDSINCLUSION,
				QUALIFIED_COLUMN_MOS_INCOMETYPE_GDSINCLUSION,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_MOS_INCOMETYPE_TDSINCLUSION,
				COLUMN_MOS_INCOMETYPE_TDSINCLUSION,
				QUALIFIED_COLUMN_MOS_INCOMETYPE_TDSINCLUSION,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

