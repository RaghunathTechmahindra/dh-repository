package mosApp.MosSystem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.html.StaticTextField;

/**
 * Title: pgDealSummaryComponentLocTiledView
 * <p>
 * Description:This view bean relates the pgDealSummary.jsp.
 * @author MCM Impl Team.
 * @version 1.0 10-June-2008 XS_16.7 Initial version
 * @version 1.1 23-June-2008 XS_16.14 Added New Fields for display LOC information.
 * @version 1.2 17-July-2008 XS_2.8 Added nextTile(boolean) to avoid confliction
 * @version 1.3 05-Aug-2008 XS_16.21 - nextTile() - Added Internationalization support for generic messages.
 * @version 1.4 14-Oct-2008 Bugfix FXP22958 Mapping Changed for MI Premium Amount
 */
public class pgDealSummaryComponentLocTiledView extends
RequestHandlingTiledViewBase implements TiledView, RequestHandler
{

    public static Map FIELD_DESCRIPTORS;

    public static final String CHILD_STCOMPONENTTYPE = "stComponentType";

    public static final String CHILD_STCOMPONENTTYPE_RESET_VALUE = "";

    public static final String CHILD_STPRODUCTNAME = "stProductName";

    public static final String CHILD_STPRODUCTNAME_RESET_VALUE = "";

    public static final String CHILD_STAMOUNT = "stAmount";

    public static final String CHILD_STAMOUNT_RESET_VALUE = "";

    public static final String CHILD_STINCLUDEMIPREMIUM = "stIncludeMiPremium";

    public static final String CHILD_STINCLUDEMIPREMIUM_RESET_VALUE = "";

    public static final String CHILD_STPAYMENTAMOUNT = "stPaymentAmount";

    public static final String CHILD_STPAYMENTAMOUNT_RESET_VALUE = "";

    public static final String CHILD_STALLOWTAXESCROW = "stAllowTaxEscrow";

    public static final String CHILD_STALLOWTAXESCROW_RESET_VALUE = "";

    public static final String CHILD_STNETRATE = "stNetRate";

    public static final String CHILD_STNETRATE_RESET_VALUE = "";

    public static final String CHILD_STDISCOUNT = "stDiscount";

    public static final String CHILD_STDISCOUNT_RESET_VALUE = "";

    public static final String CHILD_STPREMIUM = "stPremium";

    public static final String CHILD_STPREMIUM_RESET_VALUE = "";

    public static final String CHILD_STBUYDOWNRATE = "stBuyDownRate";

    public static final String CHILD_STBUYDOWNRATE_RESET_VALUE = "";

    /** ****************** MCM Impl XS_16.14 STARTS****************************************** */

    public static final String CHILD_STCOMMISSIONCODE = "stCommissionCode";

    public static final String CHILD_STCOMMISSIONCODE_RESET_VALUE = "";

    public static final String CHILD_STEXISTINGACCOUNTREFERENCE = "stExistingAccountReference";

    public static final String CHILD_STEXISTINGACCOUNTREFERENCE_RESET_VALUE = "";

    public static final String CHILD_STALLOCATETAXESCROW = "stAllocateTaxEscrow";

    public static final String CHILD_STALLOCATETAXESCROW_RESET_VALUE = "";
    
    public static final String CHILD_STLOCNETRATE = "stLocNetRate";

    public static final String CHILD_STLOCNETRATE_RESET_VALUE = "";
    
    public static final String CHILD_STLOCAMOUNT = "stLocAmount";

    public static final String CHILD_STLOCAMOUNT_RESET_VALUE = "";

    public static final String CHILD_STREPAYMENTTYPE = "stRepaymentType";

    public static final String CHILD_STREPAYMENTTYPE_RESET_VALUE = "";

    public static final String CHILD_STFIRSTPAYMENTDATE = "stFirstPaymentDate";

    public static final String CHILD_STFIRSTPAYMENTDATE_RESET_VALUE = "";

    public static final String CHILD_STTAXESCROW = "stTaxEscrow";

    public static final String CHILD_STTAXESCROW_RESET_VALUE = "";

    public static final String CHILD_STMIPREMIUM = "stMIPremium";

    public static final String CHILD_STMIPREMIUM_RESET_VALUE = "";

    public static final String CHILD_STALLOCATEMIPREMIUM = "stAllocateMIPremium";

    public static final String CHILD_STALLOCATEMIPREMIUM_RESET_VALUE = "";

    public static final String CHILD_STTOTALPAYMENT = "stTotalPayment";

    public static final String CHILD_STTOTALPAYMENT_RESET_VALUE = "";

    public static final String CHILD_STTOTALLOCAMOUNT = "stTotalLOCAmount";

    public static final String CHILD_STTOTALLOCAMOUNT_RESET_VALUE = "";

    public static final String CHILD_STEXISTINGACCOUNT = "stExistingAccount";

    public static final String CHILD_STEXISTINGACCOUNT_RESET_VALUE = "";

    public static final String CHILD_STINTERESTONLYPAYMENT = "stInterestOnlyPayment";

    public static final String CHILD_STINTERESTONLYPAYMENT_RESET_VALUE = "";

    public static final String CHILD_STHOLDBACKAMOUNT = "stHoldbackAmount";

    public static final String CHILD_STHOLDBACKAMOUNT_RESET_VALUE = "";

    public static final String CHILD_STPAYMENTFREQUENCY = "stPaymentFrequency";

    public static final String CHILD_STPAYMENTFREQUENCY_RESET_VALUE = "";

    public static final String CHILD_STADDITIONALDETAILS = "stAdditionalDetails";

    public static final String CHILD_STADDITIONALDETAILS_RESET_VALUE = "";
    
    public static final String CHILD_STPOSTEDRATE = "stPostedRate";
    
    public static final String CHILD_STPOSTEDRATE_RESET_VALUE = "";
   
    /** ****************** MCM Impl XS_16.14 ENDS******************************************** */

    /**
     * Models used by this class to display information.
     */
    private doComponentLocModel doComponentLocModel = null;

    /**
     * Handler class used by this class to display information.
     */
    private DealSummaryHandler handler = new DealSummaryHandler();

    /**
     * This Constructor registers the children and sets the default URL.
     * @param View
     *            object in which this tile is a part of
     * @param name
     *            of this tiled view
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    public pgDealSummaryComponentLocTiledView(View parent, String name)
    {
        super(parent, name);
        setMaxDisplayTiles(100);
        setPrimaryModelClass(doComponentLocModel.class);
        registerChildren();
        initialize();

    }

    /**
     * Initialize.
     */
    protected void initialize()
    {

    }

    /**
     * Description: This method resets the children which are fields on the
     * corresponding JSP
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     * @version 1.1 24-June-2008 XS_16.14
     *              Added necessary changes required for the new tiled view for loc infomation section.
     */
    public void resetChildren()
    {

        getStComponentType().setValue(CHILD_STCOMPONENTTYPE_RESET_VALUE);
        getStProductName().setValue(CHILD_STPRODUCTNAME_RESET_VALUE);
        getStAmount().setValue(CHILD_STAMOUNT_RESET_VALUE);
        getStIncludeMIPremium().setValue(CHILD_STINCLUDEMIPREMIUM_RESET_VALUE);
        getStPaymentAmount().setValue(CHILD_STPAYMENTAMOUNT_RESET_VALUE);
        getStAllowTaxEscrow().setValue(CHILD_STALLOWTAXESCROW_RESET_VALUE);
        getStNetRate().setValue(CHILD_STNETRATE_RESET_VALUE);
        getStDiscount().setValue(CHILD_STDISCOUNT_RESET_VALUE);
        getStPremium().setValue(CHILD_STPREMIUM_RESET_VALUE);
        getStBuyDownRate().setValue(CHILD_STBUYDOWNRATE_RESET_VALUE);

        /** ******************MCM Impl Team XS_16.14 changes starts****************************************** */

        getStCommissionCode().setValue(CHILD_STCOMMISSIONCODE_RESET_VALUE);
        getStExistingAccountReference().setValue(CHILD_STEXISTINGACCOUNTREFERENCE_RESET_VALUE);
        getStAllocateTaxEscrow().setValue(CHILD_STALLOCATETAXESCROW_RESET_VALUE);
        getStLocNetRate().setValue(CHILD_STLOCNETRATE_RESET_VALUE);
        getStLocAmount().setValue(CHILD_STLOCAMOUNT_RESET_VALUE);
        getStRepaymentType().setValue(CHILD_STREPAYMENTTYPE_RESET_VALUE);
        getStFirstPaymentDate().setValue(CHILD_STFIRSTPAYMENTDATE_RESET_VALUE);
        getStTaxEscrow().setValue(CHILD_STTAXESCROW_RESET_VALUE);
        getStMIPremium().setValue(CHILD_STMIPREMIUM_RESET_VALUE);
        getStAllocateMIPremium().setValue(CHILD_STALLOCATEMIPREMIUM_RESET_VALUE);
        getStTotalPayment().setValue(CHILD_STTOTALPAYMENT_RESET_VALUE);
        getStTotalLOCAmount().setValue(CHILD_STTOTALLOCAMOUNT_RESET_VALUE);
        getStExistingAccount().setValue(CHILD_STEXISTINGACCOUNT_RESET_VALUE);
        getStInterestOnlyPayment().setValue(CHILD_STINTERESTONLYPAYMENT_RESET_VALUE);
        getStHoldbackAmount().setValue(CHILD_STHOLDBACKAMOUNT_RESET_VALUE);
        getStPaymentFrequency().setValue(CHILD_STPAYMENTFREQUENCY_RESET_VALUE);
        getStAdditionalDetails().setValue(CHILD_STADDITIONALDETAILS_RESET_VALUE);
        getStPostedRate().setValue(CHILD_STPOSTEDRATE_RESET_VALUE);
       
        /** ******************MCM Impl Team XS_16.14 changes ends******************************************** */

    }

    /**
     * Description: This method registers the children which are fields on the
     * corresponding JSP.
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     * @version 1.1 24-June-2008 XS_16.14
     *              Added necessary changes required for the new tiled view for LOC infomation section.
     */
    protected void registerChildren()
    {

        registerChild(CHILD_STCOMPONENTTYPE, StaticTextField.class);
        registerChild(CHILD_STPRODUCTNAME, StaticTextField.class);
        registerChild(CHILD_STAMOUNT, StaticTextField.class);
        registerChild(CHILD_STINCLUDEMIPREMIUM, StaticTextField.class);
        registerChild(CHILD_STPAYMENTAMOUNT, StaticTextField.class);
        registerChild(CHILD_STALLOWTAXESCROW, StaticTextField.class);
        registerChild(CHILD_STNETRATE, StaticTextField.class);
        registerChild(CHILD_STDISCOUNT, StaticTextField.class);
        registerChild(CHILD_STPREMIUM, StaticTextField.class);
        registerChild(CHILD_STBUYDOWNRATE, StaticTextField.class);


        /** ******************MCM Impl Team XS_16.14 changes starts****************************************** */

        registerChild(CHILD_STCOMMISSIONCODE, StaticTextField.class);
        registerChild(CHILD_STEXISTINGACCOUNTREFERENCE, StaticTextField.class);
        registerChild(CHILD_STALLOCATETAXESCROW, StaticTextField.class);
        registerChild(CHILD_STLOCNETRATE, StaticTextField.class);
        registerChild(CHILD_STLOCAMOUNT, StaticTextField.class);
        registerChild(CHILD_STREPAYMENTTYPE, StaticTextField.class);
        registerChild(CHILD_STFIRSTPAYMENTDATE, StaticTextField.class);
        registerChild(CHILD_STTAXESCROW, StaticTextField.class);
        registerChild(CHILD_STMIPREMIUM, StaticTextField.class);
        registerChild(CHILD_STALLOCATEMIPREMIUM, StaticTextField.class);
        registerChild(CHILD_STTOTALLOCAMOUNT, StaticTextField.class);
        registerChild(CHILD_STTOTALPAYMENT, StaticTextField.class);
        registerChild(CHILD_STEXISTINGACCOUNT, StaticTextField.class);
        registerChild(CHILD_STINTERESTONLYPAYMENT, StaticTextField.class);
        registerChild(CHILD_STHOLDBACKAMOUNT, StaticTextField.class);
        registerChild(CHILD_STPAYMENTFREQUENCY, StaticTextField.class);
        registerChild(CHILD_STADDITIONALDETAILS, StaticTextField.class);
        registerChild(CHILD_STPOSTEDRATE, StaticTextField.class);
       
        /** ******************MCM Impl Team XS_16.14 changes ends******************************************** */

    }

    /**
     * Description: Adds all models to the model list
     * <p>.
     * @param executionType
     *            the execution type
     * @return model[]- List of models used by this viewbean
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    public Model[] getWebActionModels(int executionType)
    {

        List modelList = new ArrayList();

        switch (executionType)
        {

        case MODEL_TYPE_RETRIEVE:
            modelList.add(getDoComponentLocModel());
            ;

            break;

        case MODEL_TYPE_UPDATE:
            ;

            break;

        case MODEL_TYPE_DELETE:
            ;

            break;

        case MODEL_TYPE_INSERT:
            ;

            break;

        case MODEL_TYPE_EXECUTE:
            ;

            break;

        }

        return (Model[]) modelList.toArray(new Model[0]);

    }

    /**
     * Description:This method creates the handler objects, saves the page state
     * and also populates the combo box fields. Also resets the tile index.
     * <p>
     * @param event -
     *            DisplayEvent which triggered this call.
     * @return void
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    public void beginDisplay(DisplayEvent event) throws ModelControlException
    {

        if (getPrimaryModel() == null)
        {

            throw new ModelControlException("Primary model is null");

        }

        super.beginDisplay(event);
        resetTileIndex();

    }

    /**
     * Description: This method moves the control from current tile to next
     * tile.
     * @return boolean - true, if the tile has moved. False otherwise.
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     * @version 1.1 24-June-2008 method has been update to display the flag value.
     * @version 1.2 05-Aug-2008 XS_16.21 - Added Internationalization support for generic messages.
     */
    public boolean nextTile() throws ModelControlException
    {

        boolean movedToRow = super.nextTile();
        doComponentLocModelImpl theObj =(doComponentLocModelImpl) this.getDoComponentLocModel();
        
        //***************MCM Impl team changes starts - XS_16.21***************************/
        String defaultInstanceStateName = getRequestContext().getModelManager()
        .getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl) getRequestContext()
        .getModelManager().getModel(SessionStateModel.class,
                defaultInstanceStateName, true);
        int languageId = theSessionState.getLanguageId();
        //***************MCM Impl team changes ends - XS_16.21***************************/
        
        if (movedToRow) {
            //set Allocate property Tax String
           //***************XS_16.21-MCM Impl team changes starts - Added BXResources for generic message for localization support*/
           if("Y".equals(theObj.getDfPropertyTaxFlag()))
               setDisplayFieldValue(CHILD_STALLOWTAXESCROW,  BXResources.getGenericMsg("YES_LABEL",languageId));
           else{
               setDisplayFieldValue(CHILD_STALLOWTAXESCROW,  BXResources.getGenericMsg("NO_LABEL",languageId));
           }
           //Display Allocate MI premium flag
           if("Y".equals(theObj.getDfMiAllocateFlag()))
               setDisplayFieldValue(CHILD_STINCLUDEMIPREMIUM,  BXResources.getGenericMsg("YES_LABEL",languageId));
           else
               setDisplayFieldValue(CHILD_STINCLUDEMIPREMIUM,BXResources.getGenericMsg("NO_LABEL",languageId));
           /** ******************XS_16.14 STARTS****************************************** */
           // Display Existing Account Indicator flag
           if("Y".equals(theObj.getDfExistingAccountIndicator()))
               setDisplayFieldValue(CHILD_STEXISTINGACCOUNT,  BXResources.getGenericMsg("YES_LABEL",languageId));
           else
               setDisplayFieldValue(CHILD_STEXISTINGACCOUNT,BXResources.getGenericMsg("NO_LABEL",languageId));
           /** ******************XS_16.14 ENDS****************************************** */
           //***************XS_16.21 - MCM Impl team changes ends  ***************************/
        }

        return movedToRow;

    }

    /**
     * <p>nextTile</p>
     * <p> to be calles from child to skip this tiled nextTile to avoid confliction
     * @param skipThisTile
     * @return
     * @throws ModelControlException
     * 
     * @version 1.0 July 17, 2008 MCM Team for XS 2.8
     */
    protected boolean nextTile (boolean skipThisTile) throws ModelControlException {
        
        if (skipThisTile) {
            return super.nextTile();
        } else return nextTile();
    }

    /**
     * Description: Creates the children represented by fields on the JSP.
     * @param name -
     *            String name of the child
     * @return View - JATO field representing the child
     * @exception IllegalArgumentException
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     * @version 1.1 24-June-2008 XS_16.14
     *              Added necessary changes required for the new tiled view for LOC infomation section.
     */
    protected View createChild(String name)
    {

        if (name.equals(CHILD_STCOMPONENTTYPE))
        {

            StaticTextField child = new StaticTextField(this,
                    getDoComponentLocModel(), CHILD_STCOMPONENTTYPE,
                    doComponentLocModel.FIELD_DFCOMPONENTTYPE,
                    CHILD_STCOMPONENTTYPE_RESET_VALUE, null);

            return child;

        }
        else
            if (name.equals(CHILD_STPRODUCTNAME))
            {

                StaticTextField child = new StaticTextField(this,
                        getDoComponentLocModel(), CHILD_STPRODUCTNAME,
                        doComponentLocModel.FIELD_DFPRODUCTNAME,
                        CHILD_STPRODUCTNAME_RESET_VALUE, null);

                return child;

            }
            else
                if (name.equals(CHILD_STAMOUNT))
                {

                    StaticTextField child = new StaticTextField(this,
                            getDoComponentLocModel(), CHILD_STAMOUNT,
                            doComponentLocModel.FIELD_DFTOTALLOCAMOUNT,
                            CHILD_STAMOUNT_RESET_VALUE, null);

                    return child;

                }
                else
                    if (name.equals(CHILD_STINCLUDEMIPREMIUM))
                    {

                        StaticTextField child = new StaticTextField(this,
                                getDoComponentLocModel(),
                                CHILD_STINCLUDEMIPREMIUM,
                                doComponentLocModel.FIELD_DFMIALLOCAGTEFLAG,
                                CHILD_STINCLUDEMIPREMIUM_RESET_VALUE, null);

                        return child;

                    }
                    else
                        if (name.equals(CHILD_STPAYMENTAMOUNT))
                        {

                            StaticTextField child = new StaticTextField(
                                    this,
                                    getDoComponentLocModel(),
                                    CHILD_STPAYMENTAMOUNT,
                                    doComponentLocModel.FIELD_DFTOTALPAYMENTAMOUNT,
                                    CHILD_STPAYMENTAMOUNT_RESET_VALUE, null);

                            return child;

                        }
                        else
                            if (name.equals(CHILD_STALLOWTAXESCROW))
                            {

                                StaticTextField child = new StaticTextField(
                                        this,
                                        getDoComponentLocModel(),
                                        CHILD_STALLOWTAXESCROW,
                                        doComponentLocModel.FIELD_DFPROPERTYTAXALLOCATEFLAG,
                                        CHILD_STALLOWTAXESCROW_RESET_VALUE,
                                        null);

                                return child;

                            }
                            else
                                if (name.equals(CHILD_STNETRATE))
                                {

                                    StaticTextField child = new StaticTextField(
                                            this,
                                            getDoComponentLocModel(),
                                            CHILD_STNETRATE,
                                            doComponentLocModel.FIELD_DFNETINTRESTRATE,
                                            CHILD_STNETRATE_RESET_VALUE, null);

                                    return child;

                                }
                                else
                                    if (name.equals(CHILD_STDISCOUNT))
                                    {

                                        StaticTextField child = new StaticTextField(
                                                this,
                                                getDoComponentLocModel(),
                                                CHILD_STDISCOUNT,
                                                doComponentLocModel.FIELD_DFDISCOUNT,
                                                CHILD_STDISCOUNT_RESET_VALUE,
                                                null);

                                        return child;

                                    }
                                    else
                                        if (name.equals(CHILD_STPREMIUM))
                                        {

                                            StaticTextField child = new StaticTextField(
                                                    this,
                                                    getDoComponentLocModel(),
                                                    CHILD_STPREMIUM,
                                                    doComponentLocModel.FIELD_DFPREMIUM,
                                                    CHILD_STPREMIUM_RESET_VALUE,
                                                    null);

                                            return child;

                                        }
                                        else
                                            if (name
                                                    .equals(CHILD_STBUYDOWNRATE))
                                            {

                                                StaticTextField child = new StaticTextField(
                                                        this,
                                                        getDoComponentLocModel(),
                                                        CHILD_STBUYDOWNRATE,
                                                        CHILD_STBUYDOWNRATE,
                                                        CHILD_STBUYDOWNRATE_RESET_VALUE,
                                                        null);

                                                return child;

                                            }
        /** ******************MCM Impl Team XS_16.14 changes starts****************************************** */
                                         else
                                              if (name.equals(CHILD_STCOMMISSIONCODE))
                                              {

                                                  StaticTextField child = new StaticTextField(this,
                                                          getDoComponentLocModel(), CHILD_STCOMMISSIONCODE,
                                                          doComponentLocModel.FIELD_DFCOMMISSIONCODE,
                                                          CHILD_STCOMMISSIONCODE_RESET_VALUE, null);

                                                  return child;

                                              }
                                           else
                                              if (name.equals(CHILD_STEXISTINGACCOUNTREFERENCE))
                                              {

                                                  StaticTextField child = new StaticTextField(this,
                                                          getDoComponentLocModel(), CHILD_STEXISTINGACCOUNTREFERENCE,
                                                          doComponentLocModel.FIELD_DFEXISTINGACCOUNTNUMBER,
                                                          CHILD_STEXISTINGACCOUNTREFERENCE_RESET_VALUE, null);

                                                  return child;

                                              }
                                            else
                                              if (name.equals(CHILD_STALLOCATETAXESCROW))
                                              {

                                                  StaticTextField child = new StaticTextField(this,
                                                          getDoComponentLocModel(), CHILD_STALLOCATETAXESCROW,
                                                          doComponentLocModel.FIELD_DFPROPERTYTAXALLOCATEFLAG,
                                                          CHILD_STALLOCATETAXESCROW_RESET_VALUE, null);

                                                  return child;

                                              }
                                           else
                                              if (name.equals(CHILD_STLOCNETRATE))
                                              {

                                                  StaticTextField child = new StaticTextField(this,
                                                          getDoComponentLocModel(), CHILD_STLOCNETRATE,
                                                          doComponentLocModel.FIELD_DFNETRATE,
                                                          CHILD_STLOCNETRATE_RESET_VALUE, null);

                                                  return child;

                                              }
                                           else
                                              if (name.equals(CHILD_STLOCAMOUNT))
                                              {
    
                                                  StaticTextField child = new StaticTextField(this,
                                                          getDoComponentLocModel(), CHILD_STLOCAMOUNT,
                                                          doComponentLocModel.FIELD_DFLOCAMOUNT,
                                                          CHILD_STLOCAMOUNT_RESET_VALUE, null);
    
                                                  return child;
    
                                              }
                                           else
                                              if (name.equals(CHILD_STREPAYMENTTYPE))
                                              {

                                                  StaticTextField child = new StaticTextField(this,
                                                          getDoComponentLocModel(), CHILD_STREPAYMENTTYPE,
                                                          doComponentLocModel.FIELD_DFREPAYMENTTYPE,
                                                          CHILD_STREPAYMENTTYPE_RESET_VALUE, null);

                                                  return child;

                                              }
                                           else
                                              if (name.equals(CHILD_STFIRSTPAYMENTDATE))
                                              {
  
                                                  StaticTextField child = new StaticTextField(this,
                                                          getDoComponentLocModel(), CHILD_STFIRSTPAYMENTDATE,
                                                          doComponentLocModel.FIELD_DFFIRSTPAYMENTDATE,
                                                          CHILD_STFIRSTPAYMENTDATE_RESET_VALUE, null);
  
                                                  return child;
  
                                              }
                                           else
                                              if (name.equals(CHILD_STTAXESCROW))
                                              {

                                                  StaticTextField child = new StaticTextField(this,
                                                          getDoComponentLocModel(), CHILD_STTAXESCROW,
                                                          doComponentLocModel.FIELD_DFDEALSUMMARYTAXESCROWAMOUNT,
                                                          CHILD_STTAXESCROW_RESET_VALUE, null);

                                                  return child;

                                              }
                                           else
                                              if (name.equals(CHILD_STMIPREMIUM))
                                              {

                                                  StaticTextField child = new StaticTextField(this,
                                                          getDoComponentLocModel(), CHILD_STMIPREMIUM,
                                                          doComponentLocModel.FIELD_DFDEALSUMMARYMIPREMIUMAMOUNT,
                                                          CHILD_STMIPREMIUM_RESET_VALUE, null);

                                                  return child;

                                               }
                                            else
                                                if (name.equals(CHILD_STALLOCATEMIPREMIUM))
                                                {
  
                                                    StaticTextField child = new StaticTextField(this,
                                                            getDoComponentLocModel(), CHILD_STALLOCATEMIPREMIUM,
                                                            doComponentLocModel.FIELD_DFMIALLOCAGTEFLAG,
                                                            CHILD_STALLOCATEMIPREMIUM_RESET_VALUE, null);
  
                                                    return child;
  
                                                }
                                             else
                                                if (name.equals(CHILD_STTOTALPAYMENT))
                                                {

                                                    StaticTextField child = new StaticTextField(this,
                                                            getDoComponentLocModel(), CHILD_STTOTALPAYMENT,
                                                            doComponentLocModel.FIELD_DFTOTALPAYMENTAMOUNT,
                                                            CHILD_STTOTALPAYMENT_RESET_VALUE, null);

                                                    return child;

                                                }
                                             else
                                                if (name.equals(CHILD_STTOTALLOCAMOUNT))
                                                {

                                                    StaticTextField child = new StaticTextField(this,
                                                            getDoComponentLocModel(), CHILD_STTOTALLOCAMOUNT,
                                                            doComponentLocModel.FIELD_DFTOTALLOCAMOUNT,
                                                            CHILD_STTOTALLOCAMOUNT_RESET_VALUE, null);

                                                    return child;

                                                }
                                             else
                                                if (name.equals(CHILD_STEXISTINGACCOUNT))
                                                {

                                                    StaticTextField child = new StaticTextField(this,
                                                            getDoComponentLocModel(), CHILD_STEXISTINGACCOUNT,
                                                            doComponentLocModel.FIELD_DFEXISTINGACCOUNTINDICATOR,
                                                            CHILD_STEXISTINGACCOUNT_RESET_VALUE, null);

                                                    return child;

                                                }
                                             else
                                                if (name.equals(CHILD_STINTERESTONLYPAYMENT))
                                                {

                                                    StaticTextField child = new StaticTextField(this,
                                                            getDoComponentLocModel(), CHILD_STINTERESTONLYPAYMENT,
                                                            doComponentLocModel.FIELD_DFPANDIPAYMENTAMOUNT,
                                                            CHILD_STINTERESTONLYPAYMENT_RESET_VALUE, null);

                                                    return child;

                                                }
                                             else
                                                if (name.equals(CHILD_STHOLDBACKAMOUNT))
                                                {

                                                    StaticTextField child = new StaticTextField(this,
                                                            getDoComponentLocModel(), CHILD_STHOLDBACKAMOUNT,
                                                            doComponentLocModel.FIELD_DFADVANCEHOLD,
                                                            CHILD_STHOLDBACKAMOUNT_RESET_VALUE, null);

                                                    return child;

                                                 }
                                             else
                                                if (name.equals(CHILD_STPAYMENTFREQUENCY))
                                                {

                                                    StaticTextField child = new StaticTextField(this,
                                                            getDoComponentLocModel(), CHILD_STPAYMENTFREQUENCY,
                                                            doComponentLocModel.FIELD_DFPAYMENTFREQUENCY,
                                                            CHILD_STPAYMENTFREQUENCY_RESET_VALUE, null);

                                                    return child;

                                                }
                                             else
                                                if (name.equals(CHILD_STADDITIONALDETAILS))
                                                {
                  
                                                    StaticTextField child = new StaticTextField(this,
                                                            getDoComponentLocModel(), CHILD_STADDITIONALDETAILS,
                                                            doComponentLocModel.FIELD_DFADDITIONALINFORMATION,
                                                            CHILD_STADDITIONALDETAILS_RESET_VALUE, null);
                  
                                                    return child;
                  
                                                }
                                             else
                                                if (name.equals(CHILD_STPOSTEDRATE))
                                                {
                      
                                                    StaticTextField child = new StaticTextField(this,
                                                            getDoComponentLocModel(), CHILD_STPOSTEDRATE,
                                                            doComponentLocModel.FIELD_DFPOSTEDRATE,
                                                            CHILD_STPOSTEDRATE_RESET_VALUE, null);
                      
                                                    return child;
                      
                                                }
        /********************MCM Impl Team XS_16.14 changes ends****************************************** */
                                              else
                                                {

                                                    throw new IllegalArgumentException(
                                                            "Invalid child name [" + name + "]");

                                                }

    }

    /**
     * Getters and setters for the display fields starts
     */
    public StaticTextField getStDiscount()
    {

        return (StaticTextField) getChild(CHILD_STDISCOUNT);

    }

    public StaticTextField getStPremium()
    {

        return (StaticTextField) getChild(CHILD_STPREMIUM);

    }

    public StaticTextField getStBuyDownRate()
    {

        return (StaticTextField) getChild(CHILD_STBUYDOWNRATE);

    }

    public StaticTextField getStComponentType()
    {

        return (StaticTextField) getChild(CHILD_STCOMPONENTTYPE);

    }

    public StaticTextField getStProductName()
    {

        return (StaticTextField) getChild(CHILD_STPRODUCTNAME);

    }

    public StaticTextField getStIncludeMIPremium()
    {

        return (StaticTextField) getChild(CHILD_STINCLUDEMIPREMIUM);

    }

    public StaticTextField getStPaymentAmount()
    {

        return (StaticTextField) getChild(CHILD_STPAYMENTAMOUNT);

    }

    public StaticTextField getStAllowTaxEscrow()
    {

        return (StaticTextField) getChild(CHILD_STALLOWTAXESCROW);

    }

    public StaticTextField getStNetRate()
    {

        return (StaticTextField) getChild(CHILD_STNETRATE);

    }

    public StaticTextField getStAmount()
    {

        return (StaticTextField) getChild(CHILD_STAMOUNT);

    }
    /**
     * Getters and setters for the display fields ends.
     */
    
    /**
     * Getters and setters XS_16.14 for the display fields Starts.
     */

    public StaticTextField getStCommissionCode()
    {

        return (StaticTextField) getChild(CHILD_STCOMMISSIONCODE);

    }

    public StaticTextField getStExistingAccountReference()
    {

        return (StaticTextField) getChild(CHILD_STEXISTINGACCOUNTREFERENCE);

    }

    public StaticTextField getStAllocateTaxEscrow()
    {

        return (StaticTextField) getChild(CHILD_STALLOCATETAXESCROW);

    }
    
    public StaticTextField getStLocNetRate()
    {

        return (StaticTextField) getChild(CHILD_STLOCNETRATE);

    }
    public StaticTextField getStLocAmount()
    {

        return (StaticTextField) getChild(CHILD_STLOCAMOUNT);

    }

    public StaticTextField getStRepaymentType()
    {

        return (StaticTextField) getChild(CHILD_STREPAYMENTTYPE);

    }

    public StaticTextField getStFirstPaymentDate()
    {

        return (StaticTextField) getChild(CHILD_STFIRSTPAYMENTDATE);

    }
    public StaticTextField getStTaxEscrow()
    {

        return (StaticTextField) getChild(CHILD_STTAXESCROW);

    }
    public StaticTextField getStMIPremium()
    {

        return (StaticTextField) getChild(CHILD_STMIPREMIUM);

    }
    public StaticTextField getStAllocateMIPremium()
    {

        return (StaticTextField) getChild(CHILD_STALLOCATEMIPREMIUM);

    }
    public StaticTextField getStTotalPayment()
    {

        return (StaticTextField) getChild(CHILD_STTOTALPAYMENT);

    }
    public StaticTextField getStTotalLOCAmount()
    {

        return (StaticTextField) getChild(CHILD_STTOTALLOCAMOUNT);

    }
    public StaticTextField getStExistingAccount()
    {

        return (StaticTextField) getChild(CHILD_STEXISTINGACCOUNT);

    }
    public StaticTextField getStInterestOnlyPayment()
    {

        return (StaticTextField) getChild(CHILD_STINTERESTONLYPAYMENT);

    }
    public StaticTextField getStHoldbackAmount()
    {

        return (StaticTextField) getChild(CHILD_STHOLDBACKAMOUNT);

    }
    public StaticTextField getStPaymentFrequency()
    {

        return (StaticTextField) getChild(CHILD_STPAYMENTFREQUENCY);

    }
    public StaticTextField getStAdditionalDetails()
    {

        return (StaticTextField) getChild(CHILD_STADDITIONALDETAILS);

    }
     public StaticTextField getStPostedRate()
    {

        return (StaticTextField) getChild(CHILD_STPOSTEDRATE);

    }

    /**
     * Getters and setters XS_16.14  for the display fields ends.
     */

    /**
     * <p>
     * Description: This method returns the model of type getDoComponentLocModel
     * used by this class. If the model has not been set, an instance is
     * created.
     * </p>
     * @returns getDoComponentLocModel - the model used by this view
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    public doComponentLocModel getDoComponentLocModel()
    {

        if (doComponentLocModel == null)
        {

            doComponentLocModel = (doComponentLocModel) getModel(doComponentLocModel.class);

        }

        return doComponentLocModel;

    }

}
