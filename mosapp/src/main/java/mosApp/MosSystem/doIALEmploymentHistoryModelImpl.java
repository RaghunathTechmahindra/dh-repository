package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doIALEmploymentHistoryModelImpl extends QueryModelBase
	implements doIALEmploymentHistoryModel
{
	/**
	 *
	 *
	 */
	public doIALEmploymentHistoryModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfOccupationId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFOCCUPATIONID);
	}


	/**
	 *
	 *
	 */
	public void setDfOccupationId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFOCCUPATIONID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIndustrySectorId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINDUSTRYSECTORID);
	}


	/**
	 *
	 *
	 */
	public void setDfIndustrySectorId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINDUSTRYSECTORID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEmploymentHistoryId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFEMPLOYMENTHISTORYID);
	}


	/**
	 *
	 *
	 */
	public void setDfEmploymentHistoryId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFEMPLOYMENTHISTORYID,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL EMPLOYMENTHISTORY.BORROWERID, EMPLOYMENTHISTORY.OCCUPATIONID, EMPLOYMENTHISTORY.INDUSTRYSECTORID, EMPLOYMENTHISTORY.EMPLOYMENTHISTORYID FROM EMPLOYMENTHISTORY  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="EMPLOYMENTHISTORY";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="EMPLOYMENTHISTORY.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFOCCUPATIONID="EMPLOYMENTHISTORY.OCCUPATIONID";
	public static final String COLUMN_DFOCCUPATIONID="OCCUPATIONID";
	public static final String QUALIFIED_COLUMN_DFINDUSTRYSECTORID="EMPLOYMENTHISTORY.INDUSTRYSECTORID";
	public static final String COLUMN_DFINDUSTRYSECTORID="INDUSTRYSECTORID";
	public static final String QUALIFIED_COLUMN_DFEMPLOYMENTHISTORYID="EMPLOYMENTHISTORY.EMPLOYMENTHISTORYID";
	public static final String COLUMN_DFEMPLOYMENTHISTORYID="EMPLOYMENTHISTORYID";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFOCCUPATIONID,
				COLUMN_DFOCCUPATIONID,
				QUALIFIED_COLUMN_DFOCCUPATIONID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINDUSTRYSECTORID,
				COLUMN_DFINDUSTRYSECTORID,
				QUALIFIED_COLUMN_DFINDUSTRYSECTORID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYMENTHISTORYID,
				COLUMN_DFEMPLOYMENTHISTORYID,
				QUALIFIED_COLUMN_DFEMPLOYMENTHISTORYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

