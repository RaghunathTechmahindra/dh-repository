package mosApp.MosSystem;

import com.basis100.deal.util.StringUtil;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.DisplayField;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.html.StaticTextField;

import static mosApp.MosSystem.doDealNotesInfoModel.FIELD_DFDEALNOTESCATEGORY;
import static mosApp.MosSystem.doDealNotesInfoModel.FIELD_DFUSERNAME;
import static mosApp.MosSystem.doDealNotesInfoModel.FIELD_DFDEALNOTESDATE;
import static mosApp.MosSystem.doDealNotesInfoModel.FIELD_DFDEALID;
import static mosApp.MosSystem.doDealNotesInfoModel.FIELD_DFLANGUAGEPREFERENCEID;
import static mosApp.MosSystem.doDealNotesInfoModel.FIELD_DFDEALNOTESTEXT;
import static MosSystem.Mc.LANGUAGE_UNKNOW;

/**
 * This class is added for FXP27053 -- Deal Notes Lazy Load 
 */
public class pgDealNotes2TiledView extends RequestHandlingTiledViewBase {

	public static final String CHILD_STNOTECATEGORY1 = "stNoteCategory1";
	public static final String CHILD_STNOTECATEGORY1_RESET_VALUE = "";
	public static final String CHILD_STNOTETEXT1 = "stNoteText1";
	public static final String CHILD_STNOTETEXT1_RESET_VALUE = "";
	public static final String CHILD_STUSERNAME1 = "stUserName1";
	public static final String CHILD_STUSERNAME1_RESET_VALUE = "";
	public static final String CHILD_STNOTEDATE1 = "stNoteDate1";
	public static final String CHILD_STNOTEDATE1_RESET_VALUE = "";

	private doDealNotesInfoModel doDealNotesInfoModel;

	public pgDealNotes2TiledView(View parent, String name) {
		super(parent, name);
		setMaxDisplayTiles(1000);
		setPrimaryModelClass(doDealNotesInfoModel.class);

		registerChild(CHILD_STNOTECATEGORY1, StaticTextField.class);
		registerChild(CHILD_STNOTETEXT1, StaticTextField.class);
		registerChild(CHILD_STUSERNAME1, StaticTextField.class);
		registerChild(CHILD_STNOTEDATE1, StaticTextField.class);
	}

	public Model[] getWebActionModels(int executionType) {
		switch (executionType) {
		case MODEL_TYPE_RETRIEVE:
			return new Model[] { getDoDealNotesInfoModel() };
		}
		return new Model[0];
	}

	protected View createChild(String name) {
		if (name.equals(CHILD_STNOTECATEGORY1)) {
			StaticTextField child = new StaticTextField(this, getDoDealNotesInfoModel(), CHILD_STNOTECATEGORY1, FIELD_DFDEALNOTESCATEGORY, CHILD_STNOTECATEGORY1_RESET_VALUE, null);
			return child;
		} else if (name.equals(CHILD_STNOTETEXT1)) {
			StaticTextField child = new StaticTextField(this, getDoDealNotesInfoModel(), CHILD_STNOTETEXT1, FIELD_DFDEALNOTESTEXT, CHILD_STNOTETEXT1_RESET_VALUE, null);
			return child;
		} else if (name.equals(CHILD_STUSERNAME1)) {
			StaticTextField child = new StaticTextField(this, getDoDealNotesInfoModel(), CHILD_STUSERNAME1, FIELD_DFUSERNAME, CHILD_STUSERNAME1_RESET_VALUE, null);
			return child;
		} else if (name.equals(CHILD_STNOTEDATE1)) {
			StaticTextField child = new StaticTextField(this, getDoDealNotesInfoModel(), CHILD_STNOTEDATE1, FIELD_DFDEALNOTESDATE, CHILD_STNOTEDATE1_RESET_VALUE, null);
			return child;
		}
		return super.createChild(name);
	}

	public void resetChildren() {
		((DisplayField) this.getChild(CHILD_STNOTECATEGORY1)).setValue(CHILD_STNOTECATEGORY1_RESET_VALUE);
		((DisplayField) this.getChild(CHILD_STNOTETEXT1)).setValue(CHILD_STNOTETEXT1_RESET_VALUE);
		((DisplayField) this.getChild(CHILD_STUSERNAME1)).setValue(CHILD_STUSERNAME1_RESET_VALUE);
		((DisplayField) this.getChild(CHILD_STNOTEDATE1)).setValue(CHILD_STNOTEDATE1_RESET_VALUE);
	}

	public doDealNotesInfoModel getDoDealNotesInfoModel() {
		if (doDealNotesInfoModel == null)
			doDealNotesInfoModel = (doDealNotesInfoModel) getModel(doDealNotesInfoModel.class);
		return doDealNotesInfoModel;
	}

	public boolean beforeModelExecutes(Model model, int executionContext) {
		if (model instanceof doDealNotesInfoModel) {
			pgDealNotes2ViewBean parent = (pgDealNotes2ViewBean) this.getParent();

			doDealNotesInfoModel model_ = (doDealNotesInfoModel) model;
			model_.clearUserWhereCriteria();
			model_.addUserWhereCriterion(FIELD_DFDEALID, "=", parent.dealId);
			model_.addUserWhereCriterion(FIELD_DFLANGUAGEPREFERENCEID, "in", "(" + parent.languageId + "," + LANGUAGE_UNKNOW + ")");
		}
		return super.beforeModelExecutes(model, executionContext);
	}

	public String endStNoteText1Display(ChildContentDisplayEvent event) throws ModelControlException {
		return StringUtil.convertToHtmString(event.getContent());
	}

}
