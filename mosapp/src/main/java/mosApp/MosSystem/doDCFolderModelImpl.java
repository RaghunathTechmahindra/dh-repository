package mosApp.MosSystem;

import java.math.BigDecimal;

import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.model.sql.QueryFieldSchema;

import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.ModelControlException;
import com.basis100.resources.*;

import java.sql.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.filogix.externallinks.framework.ServiceConst;;

public class doDCFolderModelImpl extends QueryModelBase implements
	doDCFolderModel {

    //The logger
    private static Log _log = LogFactory.getLog(doDCFolderModelImpl.class);

    // static definitions;
    public static final String DATA_SOURCE_NAME = "jdbc/orcl";
    // select SQL template.
    public static final String SELECT_SQL_TEMPLATE =
        "SELECT ALL " +
        "DEAL.DEALID, " +
        "DEAL.SOURCEAPPLICATIONID, " +
        "DCFOLDER.DCFOLDERCODE, " +
        "DCFOLDER.DATECREATED, " +
        "DCFOLDER.DCFOLDERSTATUSCODE, " +
        "DCFOLDER.DCFOLDERSTATUSDESCRIPTION, " +
        "DCFOLDER.INSTITUTIONPROFILEID " +
        "FROM " +
        "DEAL, DCFOLDER " +
        "__WHERE__ ORDER BY DCFOLDER.DCFOLDERCODE DESC";


    // the query table name.
    public static final String MODIFYING_QUERY_TABLE_NAME =
        "DEAL, DCFOLDER";
    // the static query criteria.
    public static final String STATIC_WHERE_CRITERIA =
        "(DEAL.INSTITUTIONPROFILEID = DCFOLDER.INSTITUTIONPROFILEID) AND " +
        "(DEAL.SOURCEAPPLICATIONID = DCFOLDER.SOURCEAPPLICATIONID)";
    
    // the query field schema.
    public static final QueryFieldSchema FIELD_SCHEMA = 
        new QueryFieldSchema();

    //column definition for each field.
    public final static String QUALIFIED_COLUMN_DFDEALID = "DEAL.DEALID";
    public final static String COLUMN_DFDEALID = "DEALID";
    
    public final static String QUALIFIED_COLUMN_DFSOURCEAPPLICATIONID = "DEAL.SOURCEAPPLICATIONID";
    public final static String COLUMN_DFSOURCEAPPLICATIONID = "SOURCEAPPLICATIONID";
    
    public final static String QUALIFIED_COLUMN_DFDCFOLDERCODE = "DCFOLDER.DCFOLDERCODE";
    public final static String COLUMN_DFDCFOLDERCODE = "DCFOLDERCODE";

    public final static String QUALIFIED_COLUMN_DFDATECREATED = "DCFOLDER.DATECREATED";
    public final static String COLUMN_DFDATECREATED = "DATECREATED";
    
    public final static String QUALIFIED_COLUMN_DFDCFOLDERSTATUSCODE = "DCFOLDER.DCFOLDERSTATUSCODE";
    public final static String COLUMN_DFDCFOLDERSTATUSCODE = "DCFOLDERSTATUSCODE";

    public static final String QUALIFIED_COLUMN_DFDCFOLDERSTATUSDESCRIPTION =
        "DCFOLDER.DCFOLDERSTATUSDESCRIPTION";
    public static final String COLUMN_DFDCFOLDERSTATUSDESCRIPTION =
        "DCFOLDERSTATUSDESCRIPTION";

    public static final String QUALIFIED_COLUMN_DFINSTITUTIONPROFILEID =
        "DCFOLDER.INSTITUTIONPROFILEID";
    public static final String COLUMN_DFINSTITUTIONPROFILEID =
        "INSTITUTIONPROFILEID";

    
    // build the relationship between the column definition and the bound field
    // name, which are defined in the interface.
    static {
        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                	FIELD_DFDEALID,
                	COLUMN_DFDEALID,
                    QUALIFIED_COLUMN_DFDEALID,
                    BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
            FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                	FIELD_DFSOURCEAPPLICATIONID,
                    COLUMN_DFSOURCEAPPLICATIONID,
                    QUALIFIED_COLUMN_DFSOURCEAPPLICATIONID,
                    String.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
            FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                	FIELD_DFDCFOLDERCODE,
                    COLUMN_DFDCFOLDERCODE,
                    QUALIFIED_COLUMN_DFDCFOLDERCODE,
                    String.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
			FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                	FIELD_DFDATECREATED,
                    COLUMN_DFDATECREATED,
                    QUALIFIED_COLUMN_DFDATECREATED,
                    Timestamp.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
            FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFDCFOLDERSTATUSCODE,
                    COLUMN_DFDCFOLDERSTATUSCODE,
                    QUALIFIED_COLUMN_DFDCFOLDERSTATUSCODE,
                    String.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );

            FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFDCFOLDERSTATUSDESCRIPTION,
                    COLUMN_DFDCFOLDERSTATUSDESCRIPTION,
                    QUALIFIED_COLUMN_DFDCFOLDERSTATUSDESCRIPTION,
                    BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFINSTITUTIONPROFILEID,
                        COLUMN_DFINSTITUTIONPROFILEID,
                        QUALIFIED_COLUMN_DFINSTITUTIONPROFILEID,
                        Timestamp.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );

            
    }
    
    /**
     * Constructor function
     */
    public doDCFolderModelImpl() {

        super();
        setDataSourceName(DATA_SOURCE_NAME);
        setSelectSQLTemplate(SELECT_SQL_TEMPLATE);
        setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);
        setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);
        setFieldSchema(FIELD_SCHEMA);
        initialize();
    }

    /**
     * initializing.  do nothing for now.
     */
    public void initialize() {}

    /**
     * before execute the SQL.
     */
    protected String beforeExecute(ModelExecutionContext context,
                                   int queryType, String sql)
        throws ModelControlException {

        SysLog.trace(this.getClass(), "SQL ********** before Execute: " + sql);
        
        return sql;
    }

    /**
     * after execute, we need change some language sensitive value for the
     * result.
     */
    protected void afterExecute(ModelExecutionContext context, int queryType)
        throws ModelControlException 
    {
/*
    	String s = getDfDCFolderCode();
    	if (s == null)
    		setDfDCFolderCode(new String("-1"));
    	beforeFirst();*/
    }
    
    public BigDecimal getDfDealId()
    {
    	return (BigDecimal) getValue(FIELD_DFDEALID);
    }
    public void setDfDealId(BigDecimal id)
    {
    	setValue(FIELD_DFDEALID, id);
    }
    
    public String getDfSourceApplicationId()
    {
    	return (String) getValue(FIELD_DFSOURCEAPPLICATIONID);
    }
    public void setDfSourceApplicationId(String sourceApp)
    {
    	setValue(FIELD_DFSOURCEAPPLICATIONID, sourceApp);
    }

    public String getDfDCFolderCode()
    {
    	return (String) getValue(FIELD_DFDCFOLDERCODE);
    }
    public void setDfDCFolderCode(String DCFolderCode)
    {
    	setValue(FIELD_DFDCFOLDERCODE, DCFolderCode);
    }
	public Timestamp getDfDateCreated()
    {
    	return (Timestamp) getValue(FIELD_DFDATECREATED);
    }
    public void setDfDateCreated(Timestamp dateCreated)
    {
    	setValue(FIELD_DFDATECREATED, dateCreated);
    }
    
    public String getDfDCFolderStatusCode()
    {
    	return (String) getValue(FIELD_DFDCFOLDERSTATUSCODE);
    }
    public void setDfDCFolderStatusCode(String statusCode)
    {
    	setValue(FIELD_DFDCFOLDERSTATUSCODE, statusCode);
    }
    
    public String getDfDCFolderStatusDescription()
    {
    	return (String) getValue(FIELD_DFDCFOLDERSTATUSDESCRIPTION);
    }
    public void setDfDCFolderStatusDescription(String statusDescription)
    {
    	setValue(FIELD_DFDCFOLDERSTATUSDESCRIPTION, statusDescription);
    }
    
    public int getDfInstitutionProfileId()
    {
    	return (Integer) getValue(FIELD_DFINSTITUTIONPROFILEID);
    }
    public void setDfInstitutionProfileId(int institutionProfileId)
    {
    	setValue(FIELD_DFINSTITUTIONPROFILEID, institutionProfileId);
    }
}
