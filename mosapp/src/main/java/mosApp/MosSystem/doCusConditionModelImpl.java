package mosApp.MosSystem;

/**
 * 11/Oct/2007 DVG #DG638 LEN217180: Malcolm Whitton Condition Text exceeds 4000 characters 
 */

import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.SQLException;

import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.log.SysLog;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doCusConditionModelImpl extends QueryModelBase
	implements doCusConditionModel
{
	/**
	 *
	 *
	 */
	public doCusConditionModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
		//#DG638 reset clob as string		
		if( queryType != QUERY_TYPE_SELECT)
			return;
		while(next())    {
			String condText;
			String defCondText;
	  		//================================================================
		  	// CLOB Optimization by Phil: 25May2010
			//Getting cus condition.
			if(getValue(FIELD_DFCUSCONDITIONLITE) != null) {
				condText = (String)getValue(FIELD_DFCUSCONDITIONLITE);
			} else {
				try {
					Clob condTextObj = (Clob)getValue(FIELD_DFCUSCONDITION);
					condText = TypeConverter.stringFromClob(condTextObj);
				}
				catch (Exception e) {
					SysLog.error(getClass(), StringUtil.stack2string(e), "CusConIM");
					condText = null;
				}
			}
			//Getting def condition.
			BigDecimal defTextLength_bd = (BigDecimal)getValue(FIELD_DFDEFCONDITION_LENGTH);
			int defTextLength = (defTextLength_bd == null)?0:defTextLength_bd.intValue();
			if(defTextLength <= 4000) { // Retrieve casted VARCHAR
				defCondText = (String)getValue(FIELD_DFDEFCONDITION_VARCHAR);
				setDfDefCondition(defCondText);
			} else { // Retrieve CLOB
				try {				
					Clob defCondTextObj = (Clob)getValue(FIELD_DFDEFCONDITION);
					defCondText = TypeConverter.stringFromClob(defCondTextObj);
					
				}
				catch (Exception e) {
					SysLog.error(getClass(), StringUtil.stack2string(e), "CusConIM");
					defCondText = null;
				}
			}
			//================================================================
			setDfCusCondition(condText);
			setDfDefCondition(defCondText);
		}
		//Reset Location
		beforeFirst();
		//#DG638 end		
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCusCondition()
	{
		return (String)getValue(FIELD_DFCUSCONDITION);
	}


	/**
	 *
	 *
	 */
	public void setDfCusCondition(String value)
	{
		setValue(FIELD_DFCUSCONDITION,value);
	}
	
	/**
	 *
	 *
	 */
	public String getDfDefCondition()
	{
		return (String)getValue(FIELD_DFDEFCONDITION);
	}


	/**
	 *
	 *
	 */
	public void setDfDefCondition(String value)
	{
		setValue(FIELD_DFDEFCONDITION,value);
	}
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfConditionId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCONDITIONID);
	}

	/**
	 *
	 *
	 */
	public void setDfConditionId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCONDITIONID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDocumentLabel()
	{
		return (String)getValue(FIELD_DFDOCUMENTLABEL);
	}


	/**
	 *
	 *
	 */
	public void setDfDocumentLabel(String value)
	{
		setValue(FIELD_DFDOCUMENTLABEL,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAction()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFACTION);
	}


	/**
	 *
	 *
	 */
	public void setDfAction(java.math.BigDecimal value)
	{
		setValue(FIELD_DFACTION,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfResponsibility()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFRESPONSIBILITY);
	}


	/**
	 *
	 *
	 */
	public void setDfResponsibility(java.math.BigDecimal value)
	{
		setValue(FIELD_DFRESPONSIBILITY,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCusDocTrackId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCUSDOCTRACKID);
	}


	/**
	 *
	 *
	 */
	public void setDfCusDocTrackId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCUSDOCTRACKID,value);
	}

  //--Release2.1--//
  //--> Added LanguagePreferenceId DataField
  public java.math.BigDecimal getDfLanguagePrederenceId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLANGUAGEPREFERENCEID);
	}

	public void setDfLanguagePrederenceId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLANGUAGEPREFERENCEID,value);
	}
	
	public String getDfDisplayDefault() {
		return (String)getValue(FIELD_DFDISPLAYDEFAULT);
	}
	
	public boolean getDfDisplayDefaultBool() {
		return "Y".equalsIgnoreCase((String)getValue(FIELD_DFDISPLAYDEFAULT));
	}

	public void setDfDisplayDefault(String value) {
		setValue(FIELD_DFDISPLAYDEFAULT,value);
		
	}
	
	public String getDfUseDefaultCondition() {
		return (String)getValue(FIELD_DFUSEDEFAULT);
	}

	public void setDfUseDefaultCondition(String value) {
		setValue(FIELD_DFUSEDEFAULT,value);
		
	}


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //--> Added new LanguageId datafield in order to setup Language Criteria when page displayed
  //--> By Billy 26Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL DOCUMENTTRACKING.DEALID, DOCUMENTTRACKING.COPYID, DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXT, DOCUMENTTRACKINGVERBIAGE.DOCUMENTLABEL, DOCUMENTTRACKING.DOCUMENTSTATUSID, DOCUMENTTRACKING.DOCUMENTRESPONSIBILITYROLEID, DOCUMENTTRACKING.DOCUMENTTRACKINGID FROM DOCUMENTTRACKING, DOCUMENTTRACKINGVERBIAGE  __WHERE__  ORDER BY DOCUMENTTRACKING.DOCUMENTTRACKINGID  ASC";
	//CLOB Performance Enhancement June 2010
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL DOCUMENTTRACKING.DEALID, DOCUMENTTRACKING.COPYID, "+
    "DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXT, DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXTLITE, DOCUMENTTRACKINGVERBIAGE.DOCUMENTLABEL, "+
    "DOCUMENTTRACKING.DOCUMENTSTATUSID, DOCUMENTTRACKING.DOCUMENTRESPONSIBILITYROLEID, "+
    //--> Added LanguagePreferenceId
    "DOCUMENTTRACKING.DOCUMENTTRACKINGID, DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID, "+
    "CONDITIONVERBIAGE.CONDITIONTEXT, length(CONDITIONVERBIAGE.CONDITIONTEXT) CONDITIONTEXT_LENGTH, " + 
    "CAST(SUBSTR(CONDITIONVERBIAGE.CONDITIONTEXT,1,4000) as VARCHAR(4000)) CONDITIONTEXT_VARCHAR ," + 
    "DOCUMENTTRACKING.CONDITIONID, " +
    "DOCUMENTTRACKING.USEDEFAULTCHECKED, DOCUMENTTRACKING.DISPLAYDEFAULT " + 
    "FROM DOCUMENTTRACKING, DOCUMENTTRACKINGVERBIAGE, CONDITIONVERBIAGE  "+
    "__WHERE__  "+
    "ORDER BY DOCUMENTTRACKING.DOCUMENTTRACKINGID  ASC";

  public static final String MODIFYING_QUERY_TABLE_NAME="DOCUMENTTRACKING, DOCUMENTTRACKINGVERBIAGE";
	//--Release2.1--//
  //--> Take out the HardCoded Criteria, DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID = 0
  //public static final String STATIC_WHERE_CRITERIA=" (((DOCUMENTTRACKINGVERBIAGE.COPYID  =  DOCUMENTTRACKING.COPYID) AND (DOCUMENTTRACKINGVERBIAGE.DOCUMENTTRACKINGID  =  DOCUMENTTRACKING.DOCUMENTTRACKINGID)) AND DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID = 0 AND DOCUMENTTRACKING.DOCUMENTTRACKINGSOURCEID = 2) ";
	public static final String STATIC_WHERE_CRITERIA=
    " (((DOCUMENTTRACKINGVERBIAGE.COPYID  =  DOCUMENTTRACKING.COPYID) AND "+
    "(DOCUMENTTRACKINGVERBIAGE.DOCUMENTTRACKINGID  =  DOCUMENTTRACKING.DOCUMENTTRACKINGID) AND " +
    "(DOCUMENTTRACKING.CONDITIONID = CONDITIONVERBIAGE.CONDITIONID) AND " +
    "(DOCUMENTTRACKING.INSTITUTIONPROFILEID = DOCUMENTTRACKINGVERBIAGE.INSTITUTIONPROFILEID)) AND "  +
    "(CONDITIONVERBIAGE.INSTITUTIONPROFILEID = DOCUMENTTRACKING.INSTITUTIONPROFILEID) AND " +
    "(DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID = CONDITIONVERBIAGE.LANGUAGEPREFERENCEID) AND" + 
    "(DOCUMENTTRACKING.DOCUMENTTRACKINGSOURCEID = 2)) ";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="DOCUMENTTRACKING.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DOCUMENTTRACKING.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFCUSCONDITION="DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXT";
	public static final String COLUMN_DFCUSCONDITION="DOCUMENTTEXT";
	//================================================================
	//CLOB Performance Enhancement June 2010
	public static final String QUALIFIED_COLUMN_DFCUSCONDITIONLITE="DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXTLITE";
	public static final String COLUMN_DFCUSCONDITIONLITE="DOCUMENTTEXTLITE";
	//================================================================
	public static final String QUALIFIED_COLUMN_DFDOCUMENTLABEL="DOCUMENTTRACKINGVERBIAGE.DOCUMENTLABEL";
	public static final String COLUMN_DFDOCUMENTLABEL="DOCUMENTLABEL";
	public static final String QUALIFIED_COLUMN_DFACTION="DOCUMENTTRACKING.DOCUMENTSTATUSID";
	public static final String COLUMN_DFACTION="DOCUMENTSTATUSID";
	public static final String QUALIFIED_COLUMN_DFRESPONSIBILITY="DOCUMENTTRACKING.DOCUMENTRESPONSIBILITYROLEID";
	public static final String COLUMN_DFRESPONSIBILITY="DOCUMENTRESPONSIBILITYROLEID";
	public static final String QUALIFIED_COLUMN_DFCUSDOCTRACKID="DOCUMENTTRACKING.DOCUMENTTRACKINGID";
	public static final String COLUMN_DFCUSDOCTRACKID="DOCUMENTTRACKINGID";
	//--Release2.1--//
  //--> Added LanguagePreferenceId DataField
  public static final String QUALIFIED_COLUMN_DFLANGUAGEPREFERENCEID="DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID";
	public static final String COLUMN_DFLANGUAGEPREFERENCEID="LANGUAGEPREFERENCEID";
	//4.3 GR
	//Added condition text for default condition.
	public static final String QUALIFIED_COLUMN_DFDEFCONDITION = "CONDITIONVERBIAGE.CONDITIONTEXT";
	public static final String COLUMN_DFDEFCONDITION = "CONDITIONTEXT";
	//================================================================
	//CLOB Performance Enhancement June 2010
	public static final String QUALIFIED_COLUMN_DFDEFCONDITION_VARCHAR="CONDITIONVERBIAGE.CONDITIONTEXT_VARCHAR";
	public static final String COLUMN_DFDEFCONDITION_VARCHAR="CONDITIONTEXT_VARCHAR";
	public static final String QUALIFIED_COLUMN_DFDEFCONDITION_LENGTH="CONDITIONVERBIAGE.CONDITIONTEXT_LENGTH";
	public static final String COLUMN_DFDEFCONDITION_LENGTH="CONDITIONTEXT_LENGTH";
	//================================================================
	public static final String QUALIFIED_COLUMN_DFCONDITIONID = "DOCUMENTTRACKING.CONDITIONID";
	public static final String COLUMN_DFCONDITIONID = "CONDITIONID";
	public static final String COLUMN_DFUSEDEFAULT = "USEDEFAULTCHECKED";
	public static final String QUALIFIED_COLUMN_DFUSEDEFAULT = "DOCUMENTTRACKING.USEDEFAULTCHECKED";
	public static final String COLUMN_DFDISPLAYDEFAULT = "DISPLAYDEFAULT";
	public static final String QUALIFIED_COLUMN_DFDISPLAYDEFAULT = "DOCUMENTTRACKING.DISPLAYDEFAULT";



	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCUSCONDITION,
				COLUMN_DFCUSCONDITION,
				QUALIFIED_COLUMN_DFCUSCONDITION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		//================================================================
		//CLOB Performance Enhancement June 2010
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFCUSCONDITIONLITE,
					COLUMN_DFCUSCONDITIONLITE,
					QUALIFIED_COLUMN_DFCUSCONDITIONLITE,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));

		//================================================================

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOCUMENTLABEL,
				COLUMN_DFDOCUMENTLABEL,
				QUALIFIED_COLUMN_DFDOCUMENTLABEL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFACTION,
				COLUMN_DFACTION,
				QUALIFIED_COLUMN_DFACTION,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFRESPONSIBILITY,
				COLUMN_DFRESPONSIBILITY,
				QUALIFIED_COLUMN_DFRESPONSIBILITY,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCUSDOCTRACKID,
				COLUMN_DFCUSDOCTRACKID,
				QUALIFIED_COLUMN_DFCUSDOCTRACKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    //--Release2.1--//
    //--> Added LanguagePreferenceId DataField
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLANGUAGEPREFERENCEID,
				COLUMN_DFLANGUAGEPREFERENCEID,
				QUALIFIED_COLUMN_DFLANGUAGEPREFERENCEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    
    //4.3 GR
	FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEFCONDITION,
				COLUMN_DFDEFCONDITION,
				QUALIFIED_COLUMN_DFDEFCONDITION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
	//================================================================
	//CLOB Performance Enhancement June 2010
	FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEFCONDITION_VARCHAR,
				COLUMN_DFDEFCONDITION_VARCHAR,
				QUALIFIED_COLUMN_DFDEFCONDITION_VARCHAR,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
	
	//CLOB Performance Enhancement June 2010
	FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEFCONDITION_LENGTH,
				COLUMN_DFDEFCONDITION_LENGTH,
				QUALIFIED_COLUMN_DFDEFCONDITION_LENGTH,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
	//================================================================
	
	FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONDITIONID,
				COLUMN_DFCONDITIONID,
				QUALIFIED_COLUMN_DFCONDITIONID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
	
	FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSEDEFAULT,
				COLUMN_DFUSEDEFAULT,
				QUALIFIED_COLUMN_DFUSEDEFAULT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
	
	FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDISPLAYDEFAULT,
				COLUMN_DFDISPLAYDEFAULT,
				QUALIFIED_COLUMN_DFDISPLAYDEFAULT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

