package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

import com.basis100.picklist.BXResources;
import com.filogix.express.web.jato.ExpressViewBeanBase; 
/**
 *
 *
 *
 */
public class pgPartySearchViewBean extends ExpressViewBeanBase

{
	/**
	 *
	 *
	 */
	public pgPartySearchViewBean()
	{
		super(PAGE_NAME);
		setDefaultDisplayURL(DEFAULT_DISPLAY_URL);

		registerChildren();

		initialize();

	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		View superReturn = super.createChild(name);  
		if (superReturn != null) {  
		return superReturn;  
		} else if (name.equals(CHILD_TBDEALID)) 
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBDEALID,
				CHILD_TBDEALID,
				CHILD_TBDEALID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPAGENAMES))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES_RESET_VALUE,
				null);

      //--Release2.1--//
      //Modified to set NonSelected Label from CHILD_CBPAGENAMES_NONSELECTED_LABEL
      //--> By John 05Dec2002
      child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
      //==========================================================================

			child.setOptions(cbPageNamesOptions);
			return child;
		}
		else
		if (name.equals(CHILD_BTPROCEED))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPROCEED,
				CHILD_BTPROCEED,
				CHILD_BTPROCEED_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF1))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF1,
				CHILD_HREF1,
				CHILD_HREF1_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF2))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF2,
				CHILD_HREF2,
				CHILD_HREF2_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF3))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF3,
				CHILD_HREF3,
				CHILD_HREF3_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF4))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF4,
				CHILD_HREF4,
				CHILD_HREF4_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF5))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF5,
				CHILD_HREF5,
				CHILD_HREF5_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF6))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF6,
				CHILD_HREF6,
				CHILD_HREF6_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF7))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF7,
				CHILD_HREF7,
				CHILD_HREF7_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF8))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF8,
				CHILD_HREF8,
				CHILD_HREF8_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF9))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF9,
				CHILD_HREF9,
				CHILD_HREF9_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF10))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF10,
				CHILD_HREF10,
				CHILD_HREF10_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCOMPANYNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTODAYDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUSERNAMETITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALID))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALID,
				doDealSummarySnapShotModel.FIELD_DFAPPLICATIONID,
				CHILD_STDEALID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBORRFIRSTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBORRFIRSTNAME,
				CHILD_STBORRFIRSTNAME,
				CHILD_STBORRFIRSTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALSTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALSTATUS,
				doDealSummarySnapShotModel.FIELD_DFSTATUSDESCR,
				CHILD_STDEALSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALSTATUSDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALSTATUSDATE,
				doDealSummarySnapShotModel.FIELD_DFSTATUSDATE,
				CHILD_STDEALSTATUSDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSOURCEFIRM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STSOURCEFIRM,
				doDealSummarySnapShotModel.FIELD_DFSFSHORTNAME,
				CHILD_STSOURCEFIRM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSOURCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STSOURCE,
				doDealSummarySnapShotModel.FIELD_DFSOBPSHORTNAME,
				CHILD_STSOURCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLOB))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STLOB,
				doDealSummarySnapShotModel.FIELD_DFLOBDESCR,
				CHILD_STLOB_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALTYPE,
				doDealSummarySnapShotModel.FIELD_DFDEALTYPEDESCR,
				CHILD_STDEALTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALPURPOSE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALPURPOSE,
				doDealSummarySnapShotModel.FIELD_DFLOANPURPOSEDESCR,
				CHILD_STDEALPURPOSE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPURCHASEPRICE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STPURCHASEPRICE,
				doDealSummarySnapShotModel.FIELD_DFTOTALPURCHASEPRICE,
				CHILD_STPURCHASEPRICE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMTTERM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STPMTTERM,
				doDealSummarySnapShotModel.FIELD_DFPAYMENTTERMDESCR,
				CHILD_STPMTTERM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STESTCLOSINGDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STESTCLOSINGDATE,
				doDealSummarySnapShotModel.FIELD_DFESTCLOSINGDATE,
				CHILD_STESTCLOSINGDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTSUBMIT))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSUBMIT,
				CHILD_BTSUBMIT,
				CHILD_BTSUBMIT_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTWORKQUEUELINK))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_CHANGEPASSWORDHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLHISTORY))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOONOTES))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLSEARCH))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLLOG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STERRORFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_DETECTALERTTASKS))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTOTALLOANAMOUNT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STTOTALLOANAMOUNT,
				doDealSummarySnapShotModel.FIELD_DFTOTALLOANAMT,
				CHILD_STTOTALLOANAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTCANCEL))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTCANCEL,
				CHILD_BTCANCEL,
				CHILD_BTCANCEL_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTPREVTASKPAGE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPREVTASKPAGE,
				CHILD_BTPREVTASKPAGE,
				CHILD_BTPREVTASKPAGE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPREVTASKPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPREVTASKPAGELABEL,
				CHILD_STPREVTASKPAGELABEL,
				CHILD_STPREVTASKPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTNEXTTASKPAGE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTNEXTTASKPAGE,
				CHILD_BTNEXTTASKPAGE,
				CHILD_BTNEXTTASKPAGE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STNEXTTASKPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STNEXTTASKPAGELABEL,
				CHILD_STNEXTTASKPAGELABEL,
				CHILD_STNEXTTASKPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTASKNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTASKNAME,
				CHILD_STTASKNAME,
				CHILD_STTASKNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_SESSIONUSERID))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBPARTYSHORTNAME))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBPARTYSHORTNAME,
				CHILD_TBPARTYSHORTNAME,
				CHILD_TBPARTYSHORTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPARTYTYPE))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBPARTYTYPE,
				CHILD_CBPARTYTYPE,
				CHILD_CBPARTYTYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbPartyTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TBCOMPANY))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBCOMPANY,
				CHILD_TBCOMPANY,
				CHILD_TBCOMPANY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBCITY))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBCITY,
				CHILD_TBCITY,
				CHILD_TBCITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPROVINCE))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBPROVINCE,
				CHILD_CBPROVINCE,
				CHILD_CBPROVINCE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbProvinceOptions);
			return child;
		}
		else
		if (name.equals(CHILD_BTBACKWARDBUTTON))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTBACKWARDBUTTON,
				CHILD_BTBACKWARDBUTTON,
				CHILD_BTBACKWARDBUTTON_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTFORWARDBUTTON))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTFORWARDBUTTON,
				CHILD_BTFORWARDBUTTON,
				CHILD_BTFORWARDBUTTON_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_ROWSDISPLAYED))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_ROWSDISPLAYED,
				CHILD_ROWSDISPLAYED,
				CHILD_ROWSDISPLAYED_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_REPEATED1))
		{
			pgPartySearchRepeated1TiledView child = new pgPartySearchRepeated1TiledView(this,
				CHILD_REPEATED1);
			return child;
		}
		else
		if (name.equals(CHILD_STPMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASOK,
				CHILD_STPMHASOK,
				CHILD_STPMHASOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMTITLE,
				CHILD_STPMTITLE,
				CHILD_STPMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMONOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMONOK,
				CHILD_STPMONOK,
				CHILD_STPMONOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGS,
				CHILD_STPMMSGS,
				CHILD_STPMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMTITLE,
				CHILD_STAMTITLE,
				CHILD_STAMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGS,
				CHILD_STAMMSGS,
				CHILD_STAMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMDIALOGMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMBUTTONSHTML))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STINCLUDEDSSSTART))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STINCLUDEDSSSTART,
				CHILD_STINCLUDEDSSSTART,
				CHILD_STINCLUDEDSSSTART_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STINCLUDEDSSEND))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STINCLUDEDSSEND,
				CHILD_STINCLUDEDSSEND,
				CHILD_STINCLUDEDSSEND_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTADDPARTY))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTADDPARTY,
				CHILD_BTADDPARTY,
				CHILD_BTADDPARTY_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_TBPHONENUMBER1))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBPHONENUMBER1,
				CHILD_TBPHONENUMBER1,
				CHILD_TBPHONENUMBER1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBPHONENUMBER2))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBPHONENUMBER2,
				CHILD_TBPHONENUMBER2,
				CHILD_TBPHONENUMBER2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBPHONENUMBER3))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBPHONENUMBER3,
				CHILD_TBPHONENUMBER3,
				CHILD_TBPHONENUMBER3_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBFAXNUMBER1))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBFAXNUMBER1,
				CHILD_TBFAXNUMBER1,
				CHILD_TBFAXNUMBER1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBFAXNUMBER2))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBFAXNUMBER2,
				CHILD_TBFAXNUMBER2,
				CHILD_TBFAXNUMBER2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBFAXNUMBER3))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBFAXNUMBER3,
				CHILD_TBFAXNUMBER3,
				CHILD_TBFAXNUMBER3_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSPECIALFEATURE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STSPECIALFEATURE,
				doDealSummarySnapShotModel.FIELD_DFSPECIALFEATURE,
				CHILD_STSPECIALFEATURE_RESET_VALUE,
				null);
			return child;
		}
    //// Hidden button to trigger the ActiveMessage 'fake' submit button
    //// via the new BX ActMessageCommand class
    else
    if (name.equals(CHILD_BTACTMSG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTACTMSG,
				CHILD_BTACTMSG,
				CHILD_BTACTMSG_RESET_VALUE,
				new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));
				return child;
    }
    //--Release2.1--//
    //// New link to toggle language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new session
    //// language id throughout the all modulus.
		else
		if (name.equals(CHILD_TOGGLELANGUAGEHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF_RESET_VALUE,
				null);
				return child;
    }
    //--DJ_PT_CR--start//
    //// The new textfield providing search by Client# or Branch Transit#
		else
		if (name.equals(CHILD_TBCLIENTNUM))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBCLIENTNUM,
				CHILD_TBCLIENTNUM,
				CHILD_TBCLIENTNUM_RESET_VALUE,
				null);
			return child;
		}
    //--DJ_PT_CR--end//
	//***** Change by NBC/PP Impl. Team - Version 1.1 - Start *****//
		// New textfield providing search by Location
		else
		if (name.equals(CHILD_TBLOCATION))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBLOCATION,
				CHILD_TBLOCATION,
				CHILD_TBLOCATION_RESET_VALUE,
				null);
			return child;
		}
	//***** Change by NBC/PP Impl. Team - Version 1.1 - End *****//
		
		
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		super.resetChildren(); 
		getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
		getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
		getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
		getHref1().setValue(CHILD_HREF1_RESET_VALUE);
		getHref2().setValue(CHILD_HREF2_RESET_VALUE);
		getHref3().setValue(CHILD_HREF3_RESET_VALUE);
		getHref4().setValue(CHILD_HREF4_RESET_VALUE);
		getHref5().setValue(CHILD_HREF5_RESET_VALUE);
		getHref6().setValue(CHILD_HREF6_RESET_VALUE);
		getHref7().setValue(CHILD_HREF7_RESET_VALUE);
		getHref8().setValue(CHILD_HREF8_RESET_VALUE);
		getHref9().setValue(CHILD_HREF9_RESET_VALUE);
		getHref10().setValue(CHILD_HREF10_RESET_VALUE);
		getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
		getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
		getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
		getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
		getStDealId().setValue(CHILD_STDEALID_RESET_VALUE);
		getStBorrFirstName().setValue(CHILD_STBORRFIRSTNAME_RESET_VALUE);
		getStDealStatus().setValue(CHILD_STDEALSTATUS_RESET_VALUE);
		getStDealStatusDate().setValue(CHILD_STDEALSTATUSDATE_RESET_VALUE);
		getStSourceFirm().setValue(CHILD_STSOURCEFIRM_RESET_VALUE);
		getStSource().setValue(CHILD_STSOURCE_RESET_VALUE);
		getStLOB().setValue(CHILD_STLOB_RESET_VALUE);
		getStDealType().setValue(CHILD_STDEALTYPE_RESET_VALUE);
		getStDealPurpose().setValue(CHILD_STDEALPURPOSE_RESET_VALUE);
		getStPurchasePrice().setValue(CHILD_STPURCHASEPRICE_RESET_VALUE);
		getStPmtTerm().setValue(CHILD_STPMTTERM_RESET_VALUE);
		getStEstClosingDate().setValue(CHILD_STESTCLOSINGDATE_RESET_VALUE);
		getBtSubmit().setValue(CHILD_BTSUBMIT_RESET_VALUE);
		getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
		getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
		getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
		getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
		getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
		getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
		getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
		getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
		getStTotalLoanAmount().setValue(CHILD_STTOTALLOANAMOUNT_RESET_VALUE);
		getBtCancel().setValue(CHILD_BTCANCEL_RESET_VALUE);
		getBtPrevTaskPage().setValue(CHILD_BTPREVTASKPAGE_RESET_VALUE);
		getStPrevTaskPageLabel().setValue(CHILD_STPREVTASKPAGELABEL_RESET_VALUE);
		getBtNextTaskPage().setValue(CHILD_BTNEXTTASKPAGE_RESET_VALUE);
		getStNextTaskPageLabel().setValue(CHILD_STNEXTTASKPAGELABEL_RESET_VALUE);
		getStTaskName().setValue(CHILD_STTASKNAME_RESET_VALUE);
		getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
		getTbPartyShortName().setValue(CHILD_TBPARTYSHORTNAME_RESET_VALUE);
		getCbPartyType().setValue(CHILD_CBPARTYTYPE_RESET_VALUE);
		getTbCompany().setValue(CHILD_TBCOMPANY_RESET_VALUE);
		getTbCity().setValue(CHILD_TBCITY_RESET_VALUE);
		getCbProvince().setValue(CHILD_CBPROVINCE_RESET_VALUE);
		getBtBackwardButton().setValue(CHILD_BTBACKWARDBUTTON_RESET_VALUE);
		getBtForwardButton().setValue(CHILD_BTFORWARDBUTTON_RESET_VALUE);
		getRowsDisplayed().setValue(CHILD_ROWSDISPLAYED_RESET_VALUE);
		getRepeated1().resetChildren();
		getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
		getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
		getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
		getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
		getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
		getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
		getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
		getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
		getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
		getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
		getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
		getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
		getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
		getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
		getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
		getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
		getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
		getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
		getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
		getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
		getStIncludeDSSstart().setValue(CHILD_STINCLUDEDSSSTART_RESET_VALUE);
		getStIncludeDSSend().setValue(CHILD_STINCLUDEDSSEND_RESET_VALUE);
		getBtAddParty().setValue(CHILD_BTADDPARTY_RESET_VALUE);
		getTbPhoneNumber1().setValue(CHILD_TBPHONENUMBER1_RESET_VALUE);
		getTbPhoneNumber2().setValue(CHILD_TBPHONENUMBER2_RESET_VALUE);
		getTbPhoneNumber3().setValue(CHILD_TBPHONENUMBER3_RESET_VALUE);
		getTbFaxNumber1().setValue(CHILD_TBFAXNUMBER1_RESET_VALUE);
		getTbFaxNumber2().setValue(CHILD_TBFAXNUMBER2_RESET_VALUE);
		getTbFaxNumber3().setValue(CHILD_TBFAXNUMBER3_RESET_VALUE);
		getStSpecialFeature().setValue(CHILD_STSPECIALFEATURE_RESET_VALUE);
    //// new hidden button to activate 'fake' submit button from the ActiveMessage class.
    getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
		getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);
    //--DJ_PT_CR--start//
    //// The new textfield providing search by Client# or Branch Transit#
    getTbClientNum().setValue(CHILD_TBCLIENTNUM_RESET_VALUE);
	//***** Change by NBC/PP Impl. Team - Version 1.1 - Start *****//
	// New textfield providing search by Location
    	getTbLocation().setValue(CHILD_TBLOCATION_RESET_VALUE);
    //***** Change by NBC/PP Impl. Team - Version 1.1 - End *****//
    
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		super.registerChildren(); 
		registerChild(CHILD_TBDEALID,TextField.class);
		registerChild(CHILD_CBPAGENAMES,ComboBox.class);
		registerChild(CHILD_BTPROCEED,Button.class);
		registerChild(CHILD_HREF1,HREF.class);
		registerChild(CHILD_HREF2,HREF.class);
		registerChild(CHILD_HREF3,HREF.class);
		registerChild(CHILD_HREF4,HREF.class);
		registerChild(CHILD_HREF5,HREF.class);
		registerChild(CHILD_HREF6,HREF.class);
		registerChild(CHILD_HREF7,HREF.class);
		registerChild(CHILD_HREF8,HREF.class);
		registerChild(CHILD_HREF9,HREF.class);
		registerChild(CHILD_HREF10,HREF.class);
		registerChild(CHILD_STPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STCOMPANYNAME,StaticTextField.class);
		registerChild(CHILD_STTODAYDATE,StaticTextField.class);
		registerChild(CHILD_STUSERNAMETITLE,StaticTextField.class);
		registerChild(CHILD_STDEALID,StaticTextField.class);
		registerChild(CHILD_STBORRFIRSTNAME,StaticTextField.class);
		registerChild(CHILD_STDEALSTATUS,StaticTextField.class);
		registerChild(CHILD_STDEALSTATUSDATE,StaticTextField.class);
		registerChild(CHILD_STSOURCEFIRM,StaticTextField.class);
		registerChild(CHILD_STSOURCE,StaticTextField.class);
		registerChild(CHILD_STLOB,StaticTextField.class);
		registerChild(CHILD_STDEALTYPE,StaticTextField.class);
		registerChild(CHILD_STDEALPURPOSE,StaticTextField.class);
		registerChild(CHILD_STPURCHASEPRICE,StaticTextField.class);
		registerChild(CHILD_STPMTTERM,StaticTextField.class);
		registerChild(CHILD_STESTCLOSINGDATE,StaticTextField.class);
		registerChild(CHILD_BTSUBMIT,Button.class);
		registerChild(CHILD_BTWORKQUEUELINK,Button.class);
		registerChild(CHILD_CHANGEPASSWORDHREF,HREF.class);
		registerChild(CHILD_BTTOOLHISTORY,Button.class);
		registerChild(CHILD_BTTOONOTES,Button.class);
		registerChild(CHILD_BTTOOLSEARCH,Button.class);
		registerChild(CHILD_BTTOOLLOG,Button.class);
		registerChild(CHILD_STERRORFLAG,StaticTextField.class);
		registerChild(CHILD_DETECTALERTTASKS,HiddenField.class);
		registerChild(CHILD_STTOTALLOANAMOUNT,StaticTextField.class);
		registerChild(CHILD_BTCANCEL,Button.class);
		registerChild(CHILD_BTPREVTASKPAGE,Button.class);
		registerChild(CHILD_STPREVTASKPAGELABEL,StaticTextField.class);
		registerChild(CHILD_BTNEXTTASKPAGE,Button.class);
		registerChild(CHILD_STNEXTTASKPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STTASKNAME,StaticTextField.class);
		registerChild(CHILD_SESSIONUSERID,HiddenField.class);
		registerChild(CHILD_TBPARTYSHORTNAME,TextField.class);
		registerChild(CHILD_CBPARTYTYPE,ComboBox.class);
		registerChild(CHILD_TBCOMPANY,TextField.class);
		registerChild(CHILD_TBCITY,TextField.class);
		registerChild(CHILD_CBPROVINCE,ComboBox.class);
		registerChild(CHILD_BTBACKWARDBUTTON,Button.class);
		registerChild(CHILD_BTFORWARDBUTTON,Button.class);
		registerChild(CHILD_ROWSDISPLAYED,StaticTextField.class);
		registerChild(CHILD_REPEATED1,pgPartySearchRepeated1TiledView.class);
		registerChild(CHILD_STPMGENERATE,StaticTextField.class);
		registerChild(CHILD_STPMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STPMHASINFO,StaticTextField.class);
		registerChild(CHILD_STPMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STPMHASOK,StaticTextField.class);
		registerChild(CHILD_STPMTITLE,StaticTextField.class);
		registerChild(CHILD_STPMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STPMONOK,StaticTextField.class);
		registerChild(CHILD_STPMMSGS,StaticTextField.class);
		registerChild(CHILD_STPMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMGENERATE,StaticTextField.class);
		registerChild(CHILD_STAMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STAMHASINFO,StaticTextField.class);
		registerChild(CHILD_STAMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STAMTITLE,StaticTextField.class);
		registerChild(CHILD_STAMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STAMMSGS,StaticTextField.class);
		registerChild(CHILD_STAMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMDIALOGMSG,StaticTextField.class);
		registerChild(CHILD_STAMBUTTONSHTML,StaticTextField.class);
		registerChild(CHILD_STINCLUDEDSSSTART,StaticTextField.class);
		registerChild(CHILD_STINCLUDEDSSEND,StaticTextField.class);
		registerChild(CHILD_BTADDPARTY,Button.class);
		registerChild(CHILD_TBPHONENUMBER1,TextField.class);
		registerChild(CHILD_TBPHONENUMBER2,TextField.class);
		registerChild(CHILD_TBPHONENUMBER3,TextField.class);
		registerChild(CHILD_TBFAXNUMBER1,TextField.class);
		registerChild(CHILD_TBFAXNUMBER2,TextField.class);
		registerChild(CHILD_TBFAXNUMBER3,TextField.class);
		registerChild(CHILD_STSPECIALFEATURE,StaticTextField.class);
    //// New hidden button implementation.
    registerChild(CHILD_BTACTMSG,Button.class);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
		registerChild(CHILD_TOGGLELANGUAGEHREF,HREF.class);
    //--DJ_PT_CR--start//
    //// The new textfield providing search by Client# or Branch Transit#
    registerChild(CHILD_TBCLIENTNUM,TextField.class);
	//***** Change by NBC/PP Impl. Team - Version 1.1 - Start *****//
	// New textfield providing search by Location
    	registerChild(CHILD_TBLOCATION,TextField.class);
    //***** Change by NBC/PP Impl. Team - Version 1.1 - End *****//
    
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDealSummarySnapShotModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
    PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By John 05Dec2002
    //Setup Options for cbPageNames
    //cbPageNamesOptions..populate(getRequestContext());
	int langId = handler.getTheSessionState().getLanguageId();
    cbPageNamesOptions.populate(getRequestContext(), langId);

    //Setup NoneSelected Label for cbPageNames
    CHILD_CBPAGENAMES_NONSELECTED_LABEL =
    BXResources.getGenericMsg("CBPAGENAMES_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    //Check if the cbPageNames is already created
    if(getCbPageNames() != null)
      getCbPageNames().setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);

    cbPartyTypeOptions.populate(getRequestContext());
    cbProvinceOptions.populate(getRequestContext());
    //=====================================

    handler.pageSaveState();
    super.beginDisplay(event);
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		handler.setupBeforePageGeneration();

		handler.pageSaveState();

		// This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		handler.populatePageDisplayFields();

		handler.pageSaveState();

		// This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics this_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	/**
	 *
	 *
	 */
	public TextField getTbDealId()
	{
		return (TextField)getChild(CHILD_TBDEALID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPageNames()
	{
		return (ComboBox)getChild(CHILD_CBPAGENAMES);
	}

	/**
	 *
	 *
	 */
  //--DJ_Ticket661--start--//
  //Modified to sort by PageLabel based on the selected language
  static class CbPageNamesOptionList extends GotoOptionList
 {
    /**
     *
     *
     */
    CbPageNamesOptionList() {

    }


    public void populate(RequestContext rc) {
      Connection c = null;
      try {
        clear();
        SelectQueryModel m = null;

        SelectQueryExecutionContext eContext = new
          SelectQueryExecutionContext(
          (Connection)null,

          DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,

          DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null) {
          m = new doPageNameLabelModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else {
          m = (SelectQueryModel)rc.getModelManager().getModel(doPageNameLabelModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        // Sort the results from DataObject
        TreeMap sorted = new TreeMap();
        while (m.next()) {
          Object dfPageLabel =
            m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
          String label = (dfPageLabel == null ? "" : dfPageLabel.toString());
          Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
          String value = (dfPageId == null ? "" : dfPageId.toString());
          String[] theVal = new String[2];
          theVal[0] = label;
          theVal[1] = value;
          sorted.put(label, theVal);
        }

        // Set the sorted list to the optionlist
        Iterator theList = sorted.values().iterator();
        String[] theVal = new String[2];
        while (theList.hasNext()) {
          theVal = (String[]) (theList.next());
          add(theVal[0], theVal[1]);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      finally {
        try {
          if (c != null)
            c.close();
        }
        catch (SQLException ex) {
              // ignore
        }
      }
    }
 }
 //--DJ_Ticket661--end--//


	/**
	 *
	 *
	 */
	public void handleBtProceedRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleGoPage();

		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtProceed()
	{
		return (Button)getChild(CHILD_BTPROCEED);
	}


	/**
	 *
	 *
	 */
	public void handleHref1Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(0);

		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref1()
	{
		return (HREF)getChild(CHILD_HREF1);
	}


	/**
	 *
	 *
	 */
	public void handleHref2Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(1);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref2()
	{
		return (HREF)getChild(CHILD_HREF2);
	}


	/**
	 *
	 *
	 */
	public void handleHref3Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(2);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref3()
	{
		return (HREF)getChild(CHILD_HREF3);
	}


	/**
	 *
	 *
	 */
	public void handleHref4Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(3);

		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref4()
	{
		return (HREF)getChild(CHILD_HREF4);
	}


	/**
	 *
	 *
	 */
	public void handleHref5Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(4);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref5()
	{
		return (HREF)getChild(CHILD_HREF5);
	}


	/**
	 *
	 *
	 */
	public void handleHref6Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(5);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref6()
	{
		return (HREF)getChild(CHILD_HREF6);
	}


	/**
	 *
	 *
	 */
	public void handleHref7Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(6);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref7()
	{
		return (HREF)getChild(CHILD_HREF7);
	}


	/**
	 *
	 *
	 */
	public void handleHref8Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(7);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref8()
	{
		return (HREF)getChild(CHILD_HREF8);
	}


	/**
	 *
	 *
	 */
	public void handleHref9Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(8);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref9()
	{
		return (HREF)getChild(CHILD_HREF9);
	}


	/**
	 *
	 *
	 */
	public void handleHref10Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(9);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref10()
	{
		return (HREF)getChild(CHILD_HREF10);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCompanyName()
	{
		return (StaticTextField)getChild(CHILD_STCOMPANYNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTodayDate()
	{
		return (StaticTextField)getChild(CHILD_STTODAYDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUserNameTitle()
	{
		return (StaticTextField)getChild(CHILD_STUSERNAMETITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealId()
	{
		return (StaticTextField)getChild(CHILD_STDEALID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBorrFirstName()
	{
		return (StaticTextField)getChild(CHILD_STBORRFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealStatus()
	{
		return (StaticTextField)getChild(CHILD_STDEALSTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealStatusDate()
	{
		return (StaticTextField)getChild(CHILD_STDEALSTATUSDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSourceFirm()
	{
		return (StaticTextField)getChild(CHILD_STSOURCEFIRM);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSource()
	{
		return (StaticTextField)getChild(CHILD_STSOURCE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLOB()
	{
		return (StaticTextField)getChild(CHILD_STLOB);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealType()
	{
		return (StaticTextField)getChild(CHILD_STDEALTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealPurpose()
	{
		return (StaticTextField)getChild(CHILD_STDEALPURPOSE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPurchasePrice()
	{
		return (StaticTextField)getChild(CHILD_STPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmtTerm()
	{
		return (StaticTextField)getChild(CHILD_STPMTTERM);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEstClosingDate()
	{
		return (StaticTextField)getChild(CHILD_STESTCLOSINGDATE);
	}


	/**
	 *
	 *
	 */
	public void handleBtSubmitRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handlePerformSearch();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtSubmit()
	{
		return (Button)getChild(CHILD_BTSUBMIT);
	}


	/**
	 *
	 *
	 */
	public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayWorkQueue();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtWorkQueueLink()
	{
		return (Button)getChild(CHILD_BTWORKQUEUELINK);
	}


	/**
	 *
	 *
	 */
	public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleChangePassword();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getChangePasswordHref()
	{
		return (HREF)getChild(CHILD_CHANGEPASSWORDHREF);
	}

  //--Release2.1--start//
  //// Two new methods providing the business logic for the new link to toggle language.
  //// When touched this link should reload the page in opposite language (french versus english)
  //// and set this new session value.
	/**
	 *
	 *
	 */
	public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this, true);

		handler.handleToggleLanguage();

		handler.postHandlerProtocol();
	}

	/**
	 *
	 *
	 */
	public HREF getToggleLanguageHref()
	{
		return (HREF)getChild(CHILD_TOGGLELANGUAGEHREF);
	}

  //--Release2.1--end//
	/**
	 *
	 *
	 */
	public void handleBtToolHistoryRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayDealHistory();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtToolHistory()
	{
		return (Button)getChild(CHILD_BTTOOLHISTORY);
	}


	/**
	 *
	 *
	 */
	public void handleBtTooNotesRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayDealNotes();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtTooNotes()
	{
		return (Button)getChild(CHILD_BTTOONOTES);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolSearchRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDealSearch();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtToolSearch()
	{
		return (Button)getChild(CHILD_BTTOOLSEARCH);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolLogRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleSignOff();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtToolLog()
	{
		return (Button)getChild(CHILD_BTTOOLLOG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStErrorFlag()
	{
		return (StaticTextField)getChild(CHILD_STERRORFLAG);
	}


	/**
	 *
	 *
	 */
	public HiddenField getDetectAlertTasks()
	{
		return (HiddenField)getChild(CHILD_DETECTALERTTASKS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTotalLoanAmount()
	{
		return (StaticTextField)getChild(CHILD_STTOTALLOANAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void handleBtCancelRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleCancelStandard();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtCancel()
	{
		return (Button)getChild(CHILD_BTCANCEL);
	}


	/**
	 *
	 *
	 */
	public void handleBtPrevTaskPageRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handlePrevTaskPage();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtPrevTaskPage()
	{
		return (Button)getChild(CHILD_BTPREVTASKPAGE);
	}


	/**
	 *
	 *
	 */
	public String endBtPrevTaskPageDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btPrevTaskPage_onBeforeHtmlOutputEvent method
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generatePrevTaskPage();

		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPrevTaskPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPREVTASKPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public String endStPrevTaskPageLabelDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stPrevTaskPageLabel_onBeforeHtmlOutputEvent method
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generatePrevTaskPage();

		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public void handleBtNextTaskPageRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleNextTaskPage();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtNextTaskPage()
	{
		return (Button)getChild(CHILD_BTNEXTTASKPAGE);
	}


	/**
	 *
	 *
	 */
	public String endBtNextTaskPageDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btNextTaskPage_onBeforeHtmlOutputEvent method
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generateNextTaskPage();

		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";

	}

	/**
	 *
	 *
	 */
   //// SYNCADD. New event added, test it.
  public String endBtAddPartyDisplay(ChildContentDisplayEvent event)
 	{

 		// The following code block was migrated from the btAddParty_onBeforeHtmlOutputEvent method
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		String rc = handler.handleViewModifyPartyButton(event);

		handler.pageSaveState();

    return rc;

  }

	/**
	 *
	 *
	 */
	public StaticTextField getStNextTaskPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STNEXTTASKPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public String endStNextTaskPageLabelDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stNextTaskPageLabel_onBeforeHtmlOutputEvent method
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generateNextTaskPage();

		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTaskName()
	{
		return (StaticTextField)getChild(CHILD_STTASKNAME);
	}


	/**
	 *
	 *
	 */
	public String endStTaskNameDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stTaskName_onBeforeHtmlOutputEvent method
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generateTaskName();

		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public HiddenField getSessionUserId()
	{
		return (HiddenField)getChild(CHILD_SESSIONUSERID);
	}


	/**
	 *
	 *
	 */
	public TextField getTbPartyShortName()
	{
		return (TextField)getChild(CHILD_TBPARTYSHORTNAME);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPartyType()
	{
		return (ComboBox)getChild(CHILD_CBPARTYTYPE);
	}

	/**
	 *
	 *
	 */
	static class CbPartyTypeOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbPartyTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
            (SessionStateModelImpl)rc.getModelManager().getModel(
                            SessionStateModel.class,
                            defaultInstanceStateName,
                            true);

        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),"PARTYTYPE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];

        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex)
      {
				ex.printStackTrace();
			}
		}
	}



	/**
	 *
	 *
	 */
	public TextField getTbCompany()
	{
		return (TextField)getChild(CHILD_TBCOMPANY);
	}


	/**
	 *
	 *
	 */
	public TextField getTbCity()
	{
		return (TextField)getChild(CHILD_TBCITY);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbProvince()
	{
		return (ComboBox)getChild(CHILD_CBPROVINCE);
	}

	/**
	 *
	 *
	 */
	static class CbProvinceOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbProvinceOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
            (SessionStateModelImpl)rc.getModelManager().getModel(
                            SessionStateModel.class,
                            defaultInstanceStateName,
                            true);

        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),"PROVINCE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];

        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex)
      {
				ex.printStackTrace();
			}
		}
	}



	/**
	 *
	 *
	 */
	public void handleBtBackwardButtonRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

    getPgPartySearchRepeated1TiledView().handleWebAction(WebActions.ACTION_PREV);

		handler.handleBackwardButton();

		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtBackwardButton()
	{
		return (Button)getChild(CHILD_BTBACKWARDBUTTON);
	}


	/**
	 *
	 *
	 */
	public String endBtBackwardButtonDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btBackwardButton_onBeforeHtmlOutputEvent method
    /**
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		////boolean rc=((doRowGeneratorModelImpl)(getRepeated1().getdoRowGeneratorModel())).hasPreviousResults();
    boolean rc = handler.generateBackwardButtonStd();

		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
    **/

    return event.getContent();

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		int rc = handler.generateBackwardButtonStd();

		handler.pageSaveState();

		return rc;
		*/
	}

  ///////////
  //// BACK/FORWARD JATO MVC IMPLEMENTATION BASED OF THE
  //// (FIRST/NEST/PREVIOUS/LAST NETDYNAMICS PATTERN.
  ///////////

  /**
  *
  *
  */
  public boolean beginBtForwardButtonDisplay(ChildDisplayEvent event)  ////BTFORWARDBUTTON
      throws IOException
  {
      boolean result=((doPartySearchModelImpl)getPgPartySearchRepeated1TiledView()
          .getdoPartySearchModel()).hasMoreResults();

      return result;
  }


  public pgPartySearchRepeated1TiledView getPgPartySearchRepeated1TiledView()
  {
      return (pgPartySearchRepeated1TiledView)getChild(CHILD_REPEATED1);
  }


  public boolean beginBtBackwardButtonDisplay(ChildDisplayEvent event)  ////BTBACKWARDBUTTON
      throws IOException
  {
      boolean result=((doPartySearchModelImpl)getPgPartySearchRepeated1TiledView().getdoPartySearchModel()).hasPreviousResults();
      return result;
  }

  private void renderDisabledButton(ChildDisplayEvent event, String name, String value)
      throws IOException
  {
      // This demonstates how to use the supplied display event to output
      // directly to the JSP output stream
      ((JspChildDisplayEvent)event).getPageContext().getOut().println(
          "<input type=\"button\" name=\""+getChild(name).getQualifiedName()+
          "\" value=\""+value+"\" disabled>");
  }


	/**
	 *
	 *
	 */
	public void handleBtForwardButtonRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

    getRepeated1().handleWebAction(WebActions.ACTION_NEXT);

		handler.handleForwardButton();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtForwardButton()
	{
		return (Button)getChild(CHILD_BTFORWARDBUTTON);
	}


	/**
	 *
	 *
	 */
	public String endBtForwardButtonDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btForwardButton_onBeforeHtmlOutputEvent method
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generateForwardButtonStd();

		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public StaticTextField getRowsDisplayed()
	{
		return (StaticTextField)getChild(CHILD_ROWSDISPLAYED);
	}


	/**
	 *
	 *
	 */
	public pgPartySearchRepeated1TiledView getRepeated1()
	{
		return (pgPartySearchRepeated1TiledView)getChild(CHILD_REPEATED1);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STPMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STPMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasOk()
	{
		return (StaticTextField)getChild(CHILD_STPMHASOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STPMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmOnOk()
	{
		return (StaticTextField)getChild(CHILD_STPMONOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STAMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STAMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmDialogMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMDIALOGMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmButtonsHtml()
	{
		return (StaticTextField)getChild(CHILD_STAMBUTTONSHTML);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIncludeDSSstart()
	{
		return (StaticTextField)getChild(CHILD_STINCLUDEDSSSTART);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIncludeDSSend()
	{
		return (StaticTextField)getChild(CHILD_STINCLUDEDSSEND);
	}


	/**
	 *
	 *
	 */
	public void handleBtAddPartyRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleAddParty(false);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtAddParty()
	{
		return (Button)getChild(CHILD_BTADDPARTY);
	}


	/**
	 *
	 *
	 */
	public TextField getTbPhoneNumber1()
	{
		return (TextField)getChild(CHILD_TBPHONENUMBER1);
	}


	/**
	 *
	 *
	 */
	public TextField getTbPhoneNumber2()
	{
		return (TextField)getChild(CHILD_TBPHONENUMBER2);
	}


	/**
	 *
	 *
	 */
	public TextField getTbPhoneNumber3()
	{
		return (TextField)getChild(CHILD_TBPHONENUMBER3);
	}


	/**
	 *
	 *
	 */
	public TextField getTbFaxNumber1()
	{
		return (TextField)getChild(CHILD_TBFAXNUMBER1);
	}


	/**
	 *
	 *
	 */
	public TextField getTbFaxNumber2()
	{
		return (TextField)getChild(CHILD_TBFAXNUMBER2);
	}


	/**
	 *
	 *
	 */
	public TextField getTbFaxNumber3()
	{
		return (TextField)getChild(CHILD_TBFAXNUMBER3);
	}

  //--DJ_PT_CR--start//
	/**
	 *
	 *
	 */
	public TextField getTbClientNum()
	{
		return (TextField)getChild(CHILD_TBCLIENTNUM);
	}
  //--DJ_PT_CR--end//

	/**
	 *
	 *
	 */
	public StaticTextField getStSpecialFeature()
	{
		return (StaticTextField)getChild(CHILD_STSPECIALFEATURE);
	}

	/**
	 * getTbLocation
	 * 	Returns TextField representing the Location text field 
	 * 
	 * @param none
	 * 
	 * @return TextField : represents Location text field on page
	 * @author NBC/PP Implementation Team
	 *
	 */
	public TextField getTbLocation()
	{
		return (TextField)getChild(CHILD_TBLOCATION);
	}

	/**
	 *
	 *
	 */
	public doDealSummarySnapShotModel getdoDealSummarySnapShotModel()
	{
		if (doDealSummarySnapShot == null)
			doDealSummarySnapShot = (doDealSummarySnapShotModel) getModel(doDealSummarySnapShotModel.class);
		return doDealSummarySnapShot;
	}


	/**
	 *
	 *
	 */
	public void setdoDealSummarySnapShotModel(doDealSummarySnapShotModel model)
	{
			doDealSummarySnapShot = model;
	}

  //// New hidden button for the ActiveMessage 'fake' submit button.
  public Button getBtActMsg()
	{
		return (Button)getChild(CHILD_BTACTMSG);
	}

  //// Populate previous links new set of methods to populate Href display Strings.
  public String endHref1Display(ChildContentDisplayEvent event)
	{
    PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 0);
		handler.pageSaveState();

    return rc;
  }
  public String endHref2Display(ChildContentDisplayEvent event)
	{
    PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 1);
		handler.pageSaveState();

    return rc;
  }
  public String endHref3Display(ChildContentDisplayEvent event)
	{
    PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 2);
		handler.pageSaveState();

    return rc;
  }
  public String endHref4Display(ChildContentDisplayEvent event)
	{
    PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 3);
		handler.pageSaveState();

    return rc;
  }
  public String endHref5Display(ChildContentDisplayEvent event)
	{
    PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 4);
		handler.pageSaveState();

    return rc;
  }
  public String endHref6Display(ChildContentDisplayEvent event)
	{
    PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 5);
		handler.pageSaveState();

    return rc;
  }
  public String endHref7Display(ChildContentDisplayEvent event)
	{
    PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 6);
		handler.pageSaveState();

    return rc;
  }
  public String endHref8Display(ChildContentDisplayEvent event)
	{
    PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 7);
		handler.pageSaveState();

    return rc;
  }
  public String endHref9Display(ChildContentDisplayEvent event)
	{
    PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 8);
		handler.pageSaveState();

    return rc;
  }
  public String endHref10Display(ChildContentDisplayEvent event)
	{
    PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 9);
		handler.pageSaveState();

    return rc;
  }

  //// This method overrides the getDefaultURL() framework JATO method and it is located
  //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
  //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
  //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
  //// The full method is still in PHC base class. It should care the
  //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
  public String getDisplayURL()
  {
       PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();
       handler.pageGetState(this);

       String url=getDefaultDisplayURL();

       int languageId = handler.theSessionState.getLanguageId();

       //// Call the language specific URL (business delegation done in the BXResource).
       if(url != null && !url.trim().equals("") && !url.trim().equals("/")) {
          url = BXResources.getBXUrl(url, languageId);
       }
       else {
          url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
       }

      return url;
  }

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////



	//]]SPIDER_EVENT<btCancel_onWebEvent>

  public void handleActMessageOK(String[] args)
	{
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		handler.preHandlerProtocol(this);

		handler.handleActMessageOK(args);

		handler.postHandlerProtocol();

	}

	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String PAGE_NAME="pgPartySearch";
  //// It is a variable now (see explanation in the getDisplayURL() method above.
	////public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgPartySearch.jsp";
	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_TBDEALID="tbDealId";
	public static final String CHILD_TBDEALID_RESET_VALUE="";
	public static final String CHILD_CBPAGENAMES="cbPageNames";
	public static final String CHILD_CBPAGENAMES_RESET_VALUE="";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 05Dec2002
  //private static CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  private CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  //Added the Variable for NonSelected Label
  private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";
  //==================================================================

	public static final String CHILD_BTPROCEED="btProceed";
	public static final String CHILD_BTPROCEED_RESET_VALUE="Proceed";
	public static final String CHILD_HREF1="Href1";
	public static final String CHILD_HREF1_RESET_VALUE="";
	public static final String CHILD_HREF2="Href2";
	public static final String CHILD_HREF2_RESET_VALUE="";
	public static final String CHILD_HREF3="Href3";
	public static final String CHILD_HREF3_RESET_VALUE="";
	public static final String CHILD_HREF4="Href4";
	public static final String CHILD_HREF4_RESET_VALUE="";
	public static final String CHILD_HREF5="Href5";
	public static final String CHILD_HREF5_RESET_VALUE="";
	public static final String CHILD_HREF6="Href6";
	public static final String CHILD_HREF6_RESET_VALUE="";
	public static final String CHILD_HREF7="Href7";
	public static final String CHILD_HREF7_RESET_VALUE="";
	public static final String CHILD_HREF8="Href8";
	public static final String CHILD_HREF8_RESET_VALUE="";
	public static final String CHILD_HREF9="Href9";
	public static final String CHILD_HREF9_RESET_VALUE="";
	public static final String CHILD_HREF10="Href10";
	public static final String CHILD_HREF10_RESET_VALUE="";
	public static final String CHILD_STPAGELABEL="stPageLabel";
	public static final String CHILD_STPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STCOMPANYNAME="stCompanyName";
	public static final String CHILD_STCOMPANYNAME_RESET_VALUE="";
	public static final String CHILD_STTODAYDATE="stTodayDate";
	public static final String CHILD_STTODAYDATE_RESET_VALUE="";
	public static final String CHILD_STUSERNAMETITLE="stUserNameTitle";
	public static final String CHILD_STUSERNAMETITLE_RESET_VALUE="";
	public static final String CHILD_STDEALID="stDealId";
	public static final String CHILD_STDEALID_RESET_VALUE="";
	public static final String CHILD_STBORRFIRSTNAME="stBorrFirstName";
	public static final String CHILD_STBORRFIRSTNAME_RESET_VALUE="";
	public static final String CHILD_STDEALSTATUS="stDealStatus";
	public static final String CHILD_STDEALSTATUS_RESET_VALUE="";
	public static final String CHILD_STDEALSTATUSDATE="stDealStatusDate";
	public static final String CHILD_STDEALSTATUSDATE_RESET_VALUE="";
	public static final String CHILD_STSOURCEFIRM="stSourceFirm";
	public static final String CHILD_STSOURCEFIRM_RESET_VALUE="";
	public static final String CHILD_STSOURCE="stSource";
	public static final String CHILD_STSOURCE_RESET_VALUE="";
	public static final String CHILD_STLOB="stLOB";
	public static final String CHILD_STLOB_RESET_VALUE="";
	public static final String CHILD_STDEALTYPE="stDealType";
	public static final String CHILD_STDEALTYPE_RESET_VALUE="";
	public static final String CHILD_STDEALPURPOSE="stDealPurpose";
	public static final String CHILD_STDEALPURPOSE_RESET_VALUE="";
	public static final String CHILD_STPURCHASEPRICE="stPurchasePrice";
	public static final String CHILD_STPURCHASEPRICE_RESET_VALUE="";
	public static final String CHILD_STPMTTERM="stPmtTerm";
	public static final String CHILD_STPMTTERM_RESET_VALUE="";
	public static final String CHILD_STESTCLOSINGDATE="stEstClosingDate";
	public static final String CHILD_STESTCLOSINGDATE_RESET_VALUE="";
	public static final String CHILD_BTSUBMIT="btSubmit";
	public static final String CHILD_BTSUBMIT_RESET_VALUE=" ";
	public static final String CHILD_BTWORKQUEUELINK="btWorkQueueLink";
	public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE=" ";
	public static final String CHILD_CHANGEPASSWORDHREF="changePasswordHref";
	public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE="";
	public static final String CHILD_BTTOOLHISTORY="btToolHistory";
	public static final String CHILD_BTTOOLHISTORY_RESET_VALUE=" ";
	public static final String CHILD_BTTOONOTES="btTooNotes";
	public static final String CHILD_BTTOONOTES_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLSEARCH="btToolSearch";
	public static final String CHILD_BTTOOLSEARCH_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLLOG="btToolLog";
	public static final String CHILD_BTTOOLLOG_RESET_VALUE=" ";
	public static final String CHILD_STERRORFLAG="stErrorFlag";
	public static final String CHILD_STERRORFLAG_RESET_VALUE="N";
	public static final String CHILD_DETECTALERTTASKS="detectAlertTasks";
	public static final String CHILD_DETECTALERTTASKS_RESET_VALUE="";
	public static final String CHILD_STTOTALLOANAMOUNT="stTotalLoanAmount";
	public static final String CHILD_STTOTALLOANAMOUNT_RESET_VALUE="";
	public static final String CHILD_BTCANCEL="btCancel";
	public static final String CHILD_BTCANCEL_RESET_VALUE=" ";
	public static final String CHILD_BTPREVTASKPAGE="btPrevTaskPage";
	public static final String CHILD_BTPREVTASKPAGE_RESET_VALUE=" ";
	public static final String CHILD_STPREVTASKPAGELABEL="stPrevTaskPageLabel";
	public static final String CHILD_STPREVTASKPAGELABEL_RESET_VALUE="";
	public static final String CHILD_BTNEXTTASKPAGE="btNextTaskPage";
	public static final String CHILD_BTNEXTTASKPAGE_RESET_VALUE=" ";
	public static final String CHILD_STNEXTTASKPAGELABEL="stNextTaskPageLabel";
	public static final String CHILD_STNEXTTASKPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STTASKNAME="stTaskName";
	public static final String CHILD_STTASKNAME_RESET_VALUE="";
	public static final String CHILD_SESSIONUSERID="sessionUserId";
	public static final String CHILD_SESSIONUSERID_RESET_VALUE="";
	public static final String CHILD_TBPARTYSHORTNAME="tbPartyShortName";
	public static final String CHILD_TBPARTYSHORTNAME_RESET_VALUE="";
	public static final String CHILD_CBPARTYTYPE="cbPartyType";
	public static final String CHILD_CBPARTYTYPE_RESET_VALUE="";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 05Dec2002
  //private static CbPartyTypeOptionList cbPartyTypeOptions=new CbPartyTypeOptionList();
  private CbPartyTypeOptionList cbPartyTypeOptions=new CbPartyTypeOptionList();
  //==================================================================

  public static final String CHILD_TBCOMPANY="tbCompany";
	public static final String CHILD_TBCOMPANY_RESET_VALUE="";
	public static final String CHILD_TBCITY="tbCity";
	public static final String CHILD_TBCITY_RESET_VALUE="";
	public static final String CHILD_CBPROVINCE="cbProvince";
	public static final String CHILD_CBPROVINCE_RESET_VALUE="";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 05Dec2002
  //private static CbProvinceOptionList cbProvinceOptions=new CbProvinceOptionList();
  private CbProvinceOptionList cbProvinceOptions=new CbProvinceOptionList();
  //==================================================================

  public static final String CHILD_BTBACKWARDBUTTON="btBackwardButton";
	public static final String CHILD_BTBACKWARDBUTTON_RESET_VALUE="BackwardButton";
	public static final String CHILD_BTFORWARDBUTTON="btForwardButton";
	public static final String CHILD_BTFORWARDBUTTON_RESET_VALUE="ForwardButton";
	public static final String CHILD_ROWSDISPLAYED="rowsDisplayed";
	public static final String CHILD_ROWSDISPLAYED_RESET_VALUE="";
	public static final String CHILD_REPEATED1="Repeated1";
	public static final String CHILD_STPMGENERATE="stPmGenerate";
	public static final String CHILD_STPMGENERATE_RESET_VALUE="";
	public static final String CHILD_STPMHASTITLE="stPmHasTitle";
	public static final String CHILD_STPMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STPMHASINFO="stPmHasInfo";
	public static final String CHILD_STPMHASINFO_RESET_VALUE="";
	public static final String CHILD_STPMHASTABLE="stPmHasTable";
	public static final String CHILD_STPMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STPMHASOK="stPmHasOk";
	public static final String CHILD_STPMHASOK_RESET_VALUE="";
	public static final String CHILD_STPMTITLE="stPmTitle";
	public static final String CHILD_STPMTITLE_RESET_VALUE="";
	public static final String CHILD_STPMINFOMSG="stPmInfoMsg";
	public static final String CHILD_STPMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STPMONOK="stPmOnOk";
	public static final String CHILD_STPMONOK_RESET_VALUE="";
	public static final String CHILD_STPMMSGS="stPmMsgs";
	public static final String CHILD_STPMMSGS_RESET_VALUE="";
	public static final String CHILD_STPMMSGTYPES="stPmMsgTypes";
	public static final String CHILD_STPMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMGENERATE="stAmGenerate";
	public static final String CHILD_STAMGENERATE_RESET_VALUE="";
	public static final String CHILD_STAMHASTITLE="stAmHasTitle";
	public static final String CHILD_STAMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STAMHASINFO="stAmHasInfo";
	public static final String CHILD_STAMHASINFO_RESET_VALUE="";
	public static final String CHILD_STAMHASTABLE="stAmHasTable";
	public static final String CHILD_STAMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STAMTITLE="stAmTitle";
	public static final String CHILD_STAMTITLE_RESET_VALUE="";
	public static final String CHILD_STAMINFOMSG="stAmInfoMsg";
	public static final String CHILD_STAMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STAMMSGS="stAmMsgs";
	public static final String CHILD_STAMMSGS_RESET_VALUE="";
	public static final String CHILD_STAMMSGTYPES="stAmMsgTypes";
	public static final String CHILD_STAMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMDIALOGMSG="stAmDialogMsg";
	public static final String CHILD_STAMDIALOGMSG_RESET_VALUE="";
	public static final String CHILD_STAMBUTTONSHTML="stAmButtonsHtml";
	public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE="";
	public static final String CHILD_STINCLUDEDSSSTART="stIncludeDSSstart";
	public static final String CHILD_STINCLUDEDSSSTART_RESET_VALUE="";
	public static final String CHILD_STINCLUDEDSSEND="stIncludeDSSend";
	public static final String CHILD_STINCLUDEDSSEND_RESET_VALUE="";
	public static final String CHILD_BTADDPARTY="btAddParty";
	public static final String CHILD_BTADDPARTY_RESET_VALUE="addPty";
	public static final String CHILD_TBPHONENUMBER1="tbPhoneNumber1";
	public static final String CHILD_TBPHONENUMBER1_RESET_VALUE="";
	public static final String CHILD_TBPHONENUMBER2="tbPhoneNumber2";
	public static final String CHILD_TBPHONENUMBER2_RESET_VALUE="";
	public static final String CHILD_TBPHONENUMBER3="tbPhoneNumber3";
	public static final String CHILD_TBPHONENUMBER3_RESET_VALUE="";
	public static final String CHILD_TBFAXNUMBER1="tbFaxNumber1";
	public static final String CHILD_TBFAXNUMBER1_RESET_VALUE="";
	public static final String CHILD_TBFAXNUMBER2="tbFaxNumber2";
	public static final String CHILD_TBFAXNUMBER2_RESET_VALUE="";
	public static final String CHILD_TBFAXNUMBER3="tbFaxNumber3";
	public static final String CHILD_TBFAXNUMBER3_RESET_VALUE="";
	public static final String CHILD_STSPECIALFEATURE="stSpecialFeature";
	public static final String CHILD_STSPECIALFEATURE_RESET_VALUE="";
  //// New hidden button implementing the 'fake' one from the ActiveMessage class.
  public static final String CHILD_BTACTMSG="btActMsg";
	public static final String CHILD_BTACTMSG_RESET_VALUE="ActMsg";
  //--Release2.1--//
  //// New link to toggle the language. When touched this link should reload the
  //// page in opposite language (french versus english) and set this new language
  //// session id throughout all modulus.
	public static final String CHILD_TOGGLELANGUAGEHREF="toggleLanguageHref";
	public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE="";
  //--DJ_PT_CR--start//
  //// The new textfield providing search by Client# or Branch Transit#
	public static final String CHILD_TBCLIENTNUM="tbClientNum";
	public static final String CHILD_TBCLIENTNUM_RESET_VALUE="";
	//***** Change by NBC/PP Impl. Team - Version 1.1 - Start *****//
	// New textfield providing search by Location
	public static final String CHILD_TBLOCATION="tbLocation";
	public static final String CHILD_TBLOCATION_RESET_VALUE="";
    //***** Change by NBC/PP Impl. Team - Version 1.1 - End *****//

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDealSummarySnapShotModel doDealSummarySnapShot=null;
	protected String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgPartySearch.jsp";


	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	// MigrationToDo : Migrate custom member
	private PartySearchHandler handler=new PartySearchHandler();

}

