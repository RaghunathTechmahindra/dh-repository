package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doPickConditionModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfConditionId();

	
	/**
	 * 
	 * 
	 */
	public void setDfConditionId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfDescription();

	
	/**
	 * 
	 * 
	 */
	public void setDfDescription(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFCONDITIONID="dfConditionId";
	public static final String FIELD_DFDESCRIPTION="dfDescription";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

