package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.picklist.BXResources;
import com.basis100.log.*;

/**
 *
 *
 *
 */
public class doDisclosureSelectModelImpl extends QueryModelBase
        implements doDisclosureSelectModel
{
        /**
         *
         *
         */
        public doDisclosureSelectModelImpl()
        {
                super();
                setDataSourceName(DATA_SOURCE_NAME);

                setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

                setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

                setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

                setFieldSchema(FIELD_SCHEMA);

                initialize();

        }


        /**
         *
         *
         */
        public void initialize()
        {
        }


        ////////////////////////////////////////////////////////////////////////////////
        // NetDynamics-migrated events
        ////////////////////////////////////////////////////////////////////////////////
        /**
         *
         *
         */
        protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
                throws ModelControlException
        {

                // TODO: Migrate specific onBeforeExecute code
                return sql;

        }


        /**
         *
         *
         */
        protected void afterExecute(ModelExecutionContext context, int queryType)
                throws ModelControlException
        {
    //--Release2.1--//
    ////To override all the Description fields by populating from BXResource.
    ////Get the Language ID from the SessionStateModel.

/*
    logger = SysLog.getSysLogger("DODTOM");
    logger.debug("DODTOM@afterExecute::Size: " + this.getSize());
    logger.debug("DODTOM@afterExecute::SQLTemplate: " + this.getSelectSQL());

    String defaultInstanceStateName =
                        getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);

    SessionStateModelImpl theSessionState =
                                          (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                           SessionStateModel.class,
                                           defaultInstanceStateName,
                                           true);

    int languageId = theSessionState.getLanguageId();

    while(this.next())
    {
      //// Convert Condition Responsibility Role Description.
      this.setDfResponsibility(BXResources.getPickListDescription("CONDITIONRESPONSIBILITYROLE", this.getDfRoleId().intValue(), languageId));

      //// Convert Document Status Description.
      this.setDfStatus(BXResources.getPickListDescription("DOCUMENTSTATUS", this.getDfStatus(), languageId));

    }

    ////Reset Location
    this.beforeFirst();
*/
        }

        protected void onDatabaseError(ModelExecutionContext context, int queryType,
                                       SQLException exception) {
        }

        public java.math.BigDecimal getDfDisclosureQuestionId()
        {
                return (java.math.BigDecimal)getValue(FIELD_DFDISCLOSUREQUESTIONID);
        }

        public void setDfDisclosureQuestionId(java.math.BigDecimal value)
        {
                setValue(FIELD_DFDISCLOSUREQUESTIONID,value);
        }

        public java.math.BigDecimal getDfDocumentTypeId()
        {
                return (java.math.BigDecimal)getValue(FIELD_DFDOCUMENTTYPEID);
        }

        public void setDfDocumentTypeId(java.math.BigDecimal value)
        {
                setValue(FIELD_DFDOCUMENTTYPEID,value);
        }

        public java.math.BigDecimal getDfDealId()
        {
                return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
        }

        public void setDfDealId(java.math.BigDecimal value)
        {
                setValue(FIELD_DFDEALID,value);
        }

        public String getDfQuestionCode()
        {
                return (String)getValue(FIELD_DFQUESTIONCODE);
        }

        public void setDfQuestionCode(String value)
        {
                setValue(FIELD_DFQUESTIONCODE,value);
        }

        public String getDfIsResourceBundleKey()
        {
                return (String)getValue(FIELD_DFISRESOURCEBUNDLEKEY);
        }

        public void setDfIsResourceBundleKey(String value)
        {
                setValue(FIELD_DFISRESOURCEBUNDLEKEY,value);
        }

        public String getDfResponse()
        {
                return (String)getValue(FIELD_DFRESPONSE);
        }

        public void setDfResponse(String value)
        {
                setValue(FIELD_DFRESPONSE,value);
        }
        
        //cancel btn
        public String getDfInstr()
        {
            return (String)getValue(FIELD_DF);
        } 
        //cancel btn
        public void setDfInstr(String value)
        {
            setValue(FIELD_DF, value);
        }

        ////////////////////////////////////////////////////////////////////////////
        // Custom Methods - Require Manual Migration
        ////////////////////////////////////////////////////////////////////////////





        ////////////////////////////////////////////////////////////////////////////
        // Class variables
        ////////////////////////////////////////////////////////////////////////////

        public SysLogger logger;

        ////////////////////////////////////////////////////////////////////////////////
        // Class variables
        ////////////////////////////////////////////////////////////////////////////////

        public static final String DATA_SOURCE_NAME="jdbc/orcl";

        public static final String SELECT_SQL_TEMPLATE="SELECT ALL DISCLOSUREQUESTION.DISCLOSUREQUESTIONID, " +
            "DISCLOSUREQUESTION.DOCUMENTTYPEID, DISCLOSUREQUESTION.DEALID, " +
            "DISCLOSUREQUESTION.QUESTIONCODE, DISCLOSUREQUESTION.ISRESOURCEBUNDLEKEY, " +
            "DISCLOSUREQUESTION.RESPONSE " +
            "FROM DISCLOSUREQUESTION " +
            "__WHERE__  ORDER BY DISCLOSUREQUESTION.QUESTIONCODE  ASC";

        public static final String MODIFYING_QUERY_TABLE_NAME = "DISCLOSUREQUESTION";

        public static final String STATIC_WHERE_CRITERIA = " ";

        public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
        public static final String QUALIFIED_COLUMN_DFDISCLOSUREQUESTIONID="DISCLOSUREQUESTION.DISCLOSUREQUESTIONID";
        public static final String COLUMN_DFDISCLOSUREQUESTIONID="DISCLOSUREQUESTIONID";
        public static final String QUALIFIED_COLUMN_DFDOCUMENTTYPEID="DISCLOSUREQUESTION.DOCUMENTTYPEID";
        public static final String COLUMN_DFDOCUMENTTYPEID="DOCUMENTTYPEID";
        public static final String QUALIFIED_COLUMN_DFDEALID="DISCLOSUREQUESTION.DEALID";
        public static final String COLUMN_DFDEALID="DEALID";
        public static final String QUALIFIED_COLUMN_DFQUESTIONCODE="DISCLOSUREQUESTION.QUESTIONCODE";
        public static final String COLUMN_DFQUESTIONCODE="QUESTIONCODE";
        public static final String QUALIFIED_COLUMN_DFISRESOURCEBUNDLEKEY="DISCLOSUREQUESTION.ISRESOURCEBUNDLEKEY";
        public static final String COLUMN_DFISRESOURCEBUNDLEKEY="ISRESOURCEBUNDLEKEY";
        public static final String QUALIFIED_COLUMN_DFRESPONSE="DISCLOSUREQUESTION.RESPONSE";
        public static final String COLUMN_DFRESPONSE="RESPONSE";
        
        //cancel btn
        public static final String QUALIFIED_COLUMN_DF = ".";	
        public static final String COLUMN_DF = "";


        ////////////////////////////////////////////////////////////////////////////
        // Instance variables
        ////////////////////////////////////////////////////////////////////////////


        ////////////////////////////////////////////////////////////////////////////
        // Custom Members - Require Manual Migration
        ////////////////////////////////////////////////////////////////////////////

        static {

          FIELD_SCHEMA.addFieldDescriptor(
              new QueryFieldDescriptor(
                  FIELD_DFDISCLOSUREQUESTIONID,
                  COLUMN_DFDISCLOSUREQUESTIONID,
                  QUALIFIED_COLUMN_DFDISCLOSUREQUESTIONID,
                  java.math.BigDecimal.class,
                  false,
                  false,
                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                  "",
                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                  ""));

          FIELD_SCHEMA.addFieldDescriptor(
              new QueryFieldDescriptor(
                  FIELD_DFDOCUMENTTYPEID,
                  COLUMN_DFDOCUMENTTYPEID,
                  QUALIFIED_COLUMN_DFDOCUMENTTYPEID,
                  java.math.BigDecimal.class,
                  false,
                  false,
                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                  "",
                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                  ""));

          FIELD_SCHEMA.addFieldDescriptor(
              new QueryFieldDescriptor(
                  FIELD_DFDEALID,
                  COLUMN_DFDEALID,
                  QUALIFIED_COLUMN_DFDEALID,
                  java.math.BigDecimal.class,
                  false,
                  false,
                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                  "",
                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                  ""));

          FIELD_SCHEMA.addFieldDescriptor(
              new QueryFieldDescriptor(
                  FIELD_DFQUESTIONCODE,
                  COLUMN_DFQUESTIONCODE,
                  QUALIFIED_COLUMN_DFQUESTIONCODE,
                  String.class,
                  false,
                  false,
                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                  "",
                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                  ""));

          FIELD_SCHEMA.addFieldDescriptor(
              new QueryFieldDescriptor(
                  FIELD_DFISRESOURCEBUNDLEKEY,
                  COLUMN_DFISRESOURCEBUNDLEKEY,
                  QUALIFIED_COLUMN_DFISRESOURCEBUNDLEKEY,
                  String.class,
                  false,
                  false,
                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                  "",
                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                  ""));

          FIELD_SCHEMA.addFieldDescriptor(
              new QueryFieldDescriptor(
                  FIELD_DFRESPONSE,
                  COLUMN_DFRESPONSE,
                  QUALIFIED_COLUMN_DFRESPONSE,
                  String.class,
                  false,
                  false,
                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                  "",
                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                  ""));

        }

}

