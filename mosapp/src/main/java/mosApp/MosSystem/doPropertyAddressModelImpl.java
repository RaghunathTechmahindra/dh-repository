package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doPropertyAddressModelImpl extends QueryModelBase
	implements doPropertyAddressModel
{
	/**
	 *
	 *
	 */
	public doPropertyAddressModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyStreetTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYSTREETTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyStreetTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYSTREETTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStreetTypeDescription()
	{
		return (String)getValue(FIELD_DFSTREETTYPEDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfStreetTypeDescription(String value)
	{
		setValue(FIELD_DFSTREETTYPEDESCRIPTION,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyStreetDirectionId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYSTREETDIRECTIONID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyStreetDirectionId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYSTREETDIRECTIONID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStreetDirectionDescription()
	{
		return (String)getValue(FIELD_DFSTREETDIRECTIONDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfStreetDirectionDescription(String value)
	{
		setValue(FIELD_DFSTREETDIRECTIONDESCRIPTION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyAddressLine2()
	{
		return (String)getValue(FIELD_DFPROPERTYADDRESSLINE2);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyAddressLine2(String value)
	{
		setValue(FIELD_DFPROPERTYADDRESSLINE2,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyStreetNumber()
	{
		return (String)getValue(FIELD_DFPROPERTYSTREETNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyStreetNumber(String value)
	{
		setValue(FIELD_DFPROPERTYSTREETNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyStreetName()
	{
		return (String)getValue(FIELD_DFPROPERTYSTREETNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyStreetName(String value)
	{
		setValue(FIELD_DFPROPERTYSTREETNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPrimaryPropertyFlag()
	{
		return (String)getValue(FIELD_DFPRIMARYPROPERTYFLAG);
	}


	/**
	 *
	 *
	 */
	public void setDfPrimaryPropertyFlag(String value)
	{
		setValue(FIELD_DFPRIMARYPROPERTYFLAG,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL PROPERTY.DEALID, PROPERTY.PROPERTYID, PROPERTY.COPYID, PROPERTY.STREETTYPEID, STREETTYPE.STDESCRIPTION, PROPERTY.STREETDIRECTIONID, STREETDIRECTION.SDDESCRIPTION, PROPERTY.PROPERTYADDRESSLINE2, PROPERTY.PROPERTYSTREETNUMBER, PROPERTY.PROPERTYSTREETNAME, PROPERTY.PRIMARYPROPERTYFLAG FROM PROPERTY, STREETTYPE, STREETDIRECTION  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="PROPERTY, STREETTYPE, STREETDIRECTION";
	public static final String STATIC_WHERE_CRITERIA=" (PROPERTY.STREETTYPEID  =  STREETTYPE.STREETTYPEID) AND (PROPERTY.STREETDIRECTIONID  =  STREETDIRECTION.STREETDIRECTIONID)";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFPROPERTYDEALID="PROPERTY.DEALID";
	public static final String COLUMN_DFPROPERTYDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYID="PROPERTY.PROPERTYID";
	public static final String COLUMN_DFPROPERTYID="PROPERTYID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYCOPYID="PROPERTY.COPYID";
	public static final String COLUMN_DFPROPERTYCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYSTREETTYPEID="PROPERTY.STREETTYPEID";
	public static final String COLUMN_DFPROPERTYSTREETTYPEID="STREETTYPEID";
	public static final String QUALIFIED_COLUMN_DFSTREETTYPEDESCRIPTION="STREETTYPE.STDESCRIPTION";
	public static final String COLUMN_DFSTREETTYPEDESCRIPTION="STDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFPROPERTYSTREETDIRECTIONID="PROPERTY.STREETDIRECTIONID";
	public static final String COLUMN_DFPROPERTYSTREETDIRECTIONID="STREETDIRECTIONID";
	public static final String QUALIFIED_COLUMN_DFSTREETDIRECTIONDESCRIPTION="STREETDIRECTION.SDDESCRIPTION";
	public static final String COLUMN_DFSTREETDIRECTIONDESCRIPTION="SDDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFPROPERTYADDRESSLINE2="PROPERTY.PROPERTYADDRESSLINE2";
	public static final String COLUMN_DFPROPERTYADDRESSLINE2="PROPERTYADDRESSLINE2";
	public static final String QUALIFIED_COLUMN_DFPROPERTYSTREETNUMBER="PROPERTY.PROPERTYSTREETNUMBER";
	public static final String COLUMN_DFPROPERTYSTREETNUMBER="PROPERTYSTREETNUMBER";
	public static final String QUALIFIED_COLUMN_DFPROPERTYSTREETNAME="PROPERTY.PROPERTYSTREETNAME";
	public static final String COLUMN_DFPROPERTYSTREETNAME="PROPERTYSTREETNAME";
	public static final String QUALIFIED_COLUMN_DFPRIMARYPROPERTYFLAG="PROPERTY.PRIMARYPROPERTYFLAG";
	public static final String COLUMN_DFPRIMARYPROPERTYFLAG="PRIMARYPROPERTYFLAG";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYDEALID,
				COLUMN_DFPROPERTYDEALID,
				QUALIFIED_COLUMN_DFPROPERTYDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYID,
				COLUMN_DFPROPERTYID,
				QUALIFIED_COLUMN_DFPROPERTYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYCOPYID,
				COLUMN_DFPROPERTYCOPYID,
				QUALIFIED_COLUMN_DFPROPERTYCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYSTREETTYPEID,
				COLUMN_DFPROPERTYSTREETTYPEID,
				QUALIFIED_COLUMN_DFPROPERTYSTREETTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTREETTYPEDESCRIPTION,
				COLUMN_DFSTREETTYPEDESCRIPTION,
				QUALIFIED_COLUMN_DFSTREETTYPEDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYSTREETDIRECTIONID,
				COLUMN_DFPROPERTYSTREETDIRECTIONID,
				QUALIFIED_COLUMN_DFPROPERTYSTREETDIRECTIONID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTREETDIRECTIONDESCRIPTION,
				COLUMN_DFSTREETDIRECTIONDESCRIPTION,
				QUALIFIED_COLUMN_DFSTREETDIRECTIONDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYADDRESSLINE2,
				COLUMN_DFPROPERTYADDRESSLINE2,
				QUALIFIED_COLUMN_DFPROPERTYADDRESSLINE2,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYSTREETNUMBER,
				COLUMN_DFPROPERTYSTREETNUMBER,
				QUALIFIED_COLUMN_DFPROPERTYSTREETNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYSTREETNAME,
				COLUMN_DFPROPERTYSTREETNAME,
				QUALIFIED_COLUMN_DFPROPERTYSTREETNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIMARYPROPERTYFLAG,
				COLUMN_DFPRIMARYPROPERTYFLAG,
				QUALIFIED_COLUMN_DFPRIMARYPROPERTYFLAG,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

