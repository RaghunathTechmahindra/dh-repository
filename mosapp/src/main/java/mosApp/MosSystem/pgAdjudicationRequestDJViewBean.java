package mosApp.MosSystem;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.StaticTextField;

/**
 * <p>Title: pgAdjudicationRequestDJViewBean.java </p>
 *
 * <p>Description:DJ implementation of the adjudication request screen</p>
 *
 * <p>Copyright: </p>
 *
 * <p>Company: filogix</p>
 *
 * @author Vlad Ivanov
 * @version 1.0
 */
public class pgAdjudicationRequestDJViewBean extends pgAdjudicationRequestViewBean
{
      private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";

	/**
	 *
	 *
	 */
	public pgAdjudicationRequestDJViewBean()
	{
		super(PAGE_NAME);
		setDefaultDisplayURL(DISPLAY_URL);
                setDefaultDisplayUrl(DISPLAY_URL);
		registerChildren();
	}

          protected void initialize() {
          }

          ////////////////////////////////////////////////////////////////////////////////
          // Child manipulation methods
          ////////////////////////////////////////////////////////////////////////////////
          protected View createChild(String name)
          {
            View childFromParent = super.createChild(name);

            if (childFromParent != null) {
              return childFromParent;
            }
            else if(name.equals(CHILD_STREQUESTSTATUS))
            {
             StaticTextField child = new StaticTextField(this,
               //TODO: define specific model here
               getDefaultModel(),
               CHILD_STREQUESTSTATUS,
               //TODO: bind model here
               CHILD_STREQUESTSTATUS,
               CHILD_STREQUESTSTATUS_RESET_VALUE,
               null);
             return child;
            }
            else if(name.equals(CHILD_STREQUESTDATE))
            {
             StaticTextField child = new StaticTextField(this,
               //TODO: define specific model here
               getDefaultModel(),
               CHILD_STREQUESTDATE,
               //TODO: bind model here
               CHILD_STREQUESTDATE,
               CHILD_STREQUESTDATE_RESET_VALUE,
               null);
             return child;
            }
            else if(name.equals(CHILD_STRESPONSEDATE))
            {
             StaticTextField child = new StaticTextField(this,
               //TODO: define specific model here
               getDefaultModel(),
               CHILD_STRESPONSEDATE,
               //TODO: bind model here
               CHILD_STRESPONSEDATE,
               CHILD_STRESPONSEDATE_RESET_VALUE,
               null);
             return child;
            }
            else if(name.equals(CHILD_STCREDITRESPONSE))
            {
             StaticTextField child = new StaticTextField(this,
               //TODO: define specific model here
               getDefaultModel(),
               CHILD_STCREDITRESPONSE,
               //TODO: bind model here
               CHILD_STCREDITRESPONSE,
               CHILD_STCREDITRESPONSE_RESET_VALUE,
               null);
             return child;
            }
            else if(name.equals(CHILD_STERRORS))
            {
             StaticTextField child = new StaticTextField(this,
               //TODO: define specific model here
               getDefaultModel(),
               CHILD_STERRORS,
               //TODO: bind model here
               CHILD_STERRORS,
               CHILD_STERRORS_RESET_VALUE,
               null);
             return child;
            }
            else
              throw new IllegalArgumentException("Invalid child name [" + name + "]");
          }

          public void resetChildren()
          {
            super.resetChildren();

            getStRequestStatus().setValue(CHILD_STREQUESTSTATUS_RESET_VALUE);
            getStRequestDate().setValue(CHILD_STREQUESTDATE_RESET_VALUE);
            getStResponseDate().setValue(CHILD_STRESPONSEDATE_RESET_VALUE);
            getStCreditResponse().setValue(CHILD_STCREDITRESPONSE_RESET_VALUE);
            getStErrors().setValue(CHILD_STERRORS_RESET_VALUE);
          }


          /**
           *
           *
           */
          protected void registerChildren()
          {
            super.registerChildren();

            registerChild(CHILD_STREQUESTSTATUS,StaticTextField.class);
            registerChild(CHILD_STREQUESTDATE,StaticTextField.class);
            registerChild(CHILD_STRESPONSEDATE,StaticTextField.class);
            registerChild(CHILD_STCREDITRESPONSE,StaticTextField.class);
            registerChild(CHILD_STERRORS,StaticTextField.class);
          }

          public void beginDisplay(DisplayEvent event) throws ModelControlException
          {
            super.beginDisplay(event);
          }

          public boolean beforeModelExecutes(Model model, int executionContext) {
            AdjudicationRequestHandler handler = this.handler.cloneSS();

            handler.pageGetState(this);
            handler.setupBeforePageGeneration();
            handler.pageSaveState();

            // This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
            return super.beforeModelExecutes(model, executionContext);
          }

          public void afterModelExecutes(Model model, int executionContext)
          {
            // This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
            super.afterModelExecutes(model, executionContext);
          }

          public void afterAllModelsExecute(int executionContext) {
            AdjudicationRequestHandler handler = this.handler.cloneSS();

            handler.pageGetState(this);
            handler.populatePageDisplayFields();
            handler.pageSaveState();

            // This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
            super.afterAllModelsExecute(executionContext);
          }

          public void onModelError(Model model, int executionContext, ModelControlException exception)
                  throws ModelControlException
          {

            // This is the analog of NetDynamics this_onDataObjectErrorEvent
            super.onModelError(model, executionContext, exception);

          }

          public void handleBtProceedRequest(RequestInvocationEvent event)
                  throws ServletException, IOException
          {

            AdjudicationRequestHandler handler = this.handler.cloneSS();
            handler.preHandlerProtocol(this);
            handler.handleGoPage();
            handler.postHandlerProtocol();

          }

          //// This method overrides the getDefaultURL() framework JATO method and it is located
          //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
          //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
          //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
          //// The full method is still in PHC base class. It should care the
          //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
          public String getDisplayURL() {
            AdjudicationRequestHandler handler = (AdjudicationRequestHandler)this.
                handler.cloneSS();
            handler.pageGetState(this);

            String url = getDefaultDisplayURL();

            int languageId = handler.theSessionState.getLanguageId();

            //// Call the language specific URL (business delegation done in the BXResource).
            if (url != null && !url.trim().equals("") && !url.trim().equals("/")) {
              url = BXResources.getBXUrl(url, languageId);
            }
            else {
              url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
            }

            return url;
          }

    public StaticTextField getStRequestStatus() {
      return (StaticTextField) getChild(CHILD_STREQUESTSTATUS);
    }

    public StaticTextField getStRequestDate()
    {
      return (StaticTextField)getChild(CHILD_STREQUESTDATE);
    }

    public StaticTextField getStResponseDate()
    {
      return (StaticTextField)getChild(CHILD_STRESPONSEDATE);
    }

    public StaticTextField getStCreditResponse()
    {
      return (StaticTextField)getChild(CHILD_STCREDITRESPONSE);
    }

    public StaticTextField getStErrors()
    {
      return (StaticTextField)getChild(CHILD_STERRORS);
    }

          ////////////////////////////////////////////////////////////////////////////
          // Obsolete Netdynamics Events - Require Manual Migration
          ////////////////////////////////////////////////////////////////////////////


          ////////////////////////////////////////////////////////////////////////////
          // Custom Methods - Require Manual Migration
          ////////////////////////////////////////////////////////////////////////////

          public void handleActMessageOK(String[] args) {
            AdjudicationRequestHandler handler = this.handler.cloneSS();
            handler.preHandlerProtocol(this);
            handler.handleActMessageOK(args);
            handler.postHandlerProtocol();
          }


          ////////////////////////////////////////////////////////////////////////////////
          // Class variables
          ////////////////////////////////////////////////////////////////////////////////
          public static final String PAGE_NAME = "pgAdjudicationRequestDJ";
          //// It is a variable now (see explanation in the getDisplayURL() method above.
          public static Map FIELD_DESCRIPTORS;

          public static final String CHILD_STREQUESTSTATUS = "stRequestStatus";
          public static final String CHILD_STREQUESTSTATUS_RESET_VALUE = "";

          public static final String CHILD_STREQUESTDATE = "stRequestDate";
          public static final String CHILD_STREQUESTDATE_RESET_VALUE = "";

          public static final String CHILD_STRESPONSEDATE = "stResponseDate";
          public static final String CHILD_STRESPONSEDATE_RESET_VALUE = "";

          public static final String CHILD_STCREDITRESPONSE = "stCreditResponse";
          public static final String CHILD_STCREDITRESPONSE_RESET_VALUE = "";

          public static final String CHILD_STERRORS = "stErrors";
          public static final String CHILD_STERRORS_RESET_VALUE = "";

          ////////////////////////////////////////////////////////////////////////////
          // Instance variables
          ////////////////////////////////////////////////////////////////////////////

          private String DISPLAY_URL="/mosApp/MosSystem/pgAdjudicationRequestDJ.jsp";
}

