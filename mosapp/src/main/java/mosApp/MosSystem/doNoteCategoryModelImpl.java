package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doNoteCategoryModelImpl extends QueryModelBase
	implements doNoteCategoryModel
{
	/**
	 *
	 *
	 */
	public doNoteCategoryModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public String getDfDealNotesCategory()
	{
		return (String)getValue(FIELD_DFDEALNOTESCATEGORY);
	}


	/**
	 *
	 *
	 */
	public void setDfDealNotesCategory(String value)
	{
		setValue(FIELD_DFDEALNOTESCATEGORY,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealNotesCategoryID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALNOTESCATEGORYID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealNotesCategoryID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALNOTESCATEGORYID,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL DEALNOTESCATEGORY.CATEGORYNAME, DEALNOTESCATEGORY.DEALNOTESCATEGORYID FROM DEALNOTESCATEGORY  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="DEALNOTESCATEGORY";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALNOTESCATEGORY="DEALNOTESCATEGORY.CATEGORYNAME";
	public static final String COLUMN_DFDEALNOTESCATEGORY="CATEGORYNAME";
	public static final String QUALIFIED_COLUMN_DFDEALNOTESCATEGORYID="DEALNOTESCATEGORY.DEALNOTESCATEGORYID";
	public static final String COLUMN_DFDEALNOTESCATEGORYID="DEALNOTESCATEGORYID";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALNOTESCATEGORY,
				COLUMN_DFDEALNOTESCATEGORY,
				QUALIFIED_COLUMN_DFDEALNOTESCATEGORY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALNOTESCATEGORYID,
				COLUMN_DFDEALNOTESCATEGORYID,
				QUALIFIED_COLUMN_DFDEALNOTESCATEGORYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

