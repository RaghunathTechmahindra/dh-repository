package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.basis100.picklist.BXResources;

import java.text.*;

/**
 *
 *
 *
 */
 //--Release2.1--//
 //TODO : This model is not necessary and should be eliminated later
 //--> By Billy 18Nov2002
public class doDetailViewEmpJobTitleModelImpl extends QueryModelBase
	implements doDetailViewEmpJobTitleModel
{
	/**
	 *
	 *
	 */
	public doDetailViewEmpJobTitleModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 18Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
	int institutionId = theSessionState.getDealInstitutionId();
    while(this.next())
    {
      //Convert CREDITREFTYPE Desc.
      this.setDfJobTitleDesc(BXResources.getPickListDescription(
          institutionId, "JOBTITLE", this.getDfJobTitleId().intValue(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfJobTitleId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFJOBTITLEID);
	}


	/**
	 *
	 *
	 */
	public void setDfJobTitleId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFJOBTITLEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfJobTitleDesc()
	{
		return (String)getValue(FIELD_DFJOBTITLEDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfJobTitleDesc(String value)
	{
		setValue(FIELD_DFJOBTITLEDESC,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  //--Release2.1--//
  //Not need to modify the SQL and the Multilingual Desc. is populated in afterExecute() method
  //--> By Billy 18Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  public static final String SELECT_SQL_TEMPLATE="SELECT ALL JOBTITLE.JOBTITLEID, JOBTITLE.JOBTITLEDESCRIPTION FROM JOBTITLE  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="JOBTITLE";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFJOBTITLEID="JOBTITLE.JOBTITLEID";
	public static final String COLUMN_DFJOBTITLEID="JOBTITLEID";
	public static final String QUALIFIED_COLUMN_DFJOBTITLEDESC="JOBTITLE.JOBTITLEDESCRIPTION";
	public static final String COLUMN_DFJOBTITLEDESC="JOBTITLEDESCRIPTION";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	//[[SPIDER_EVENTS BEGIN

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFJOBTITLEID,
				COLUMN_DFJOBTITLEID,
				QUALIFIED_COLUMN_DFJOBTITLEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFJOBTITLEDESC,
				COLUMN_DFJOBTITLEDESC,
				QUALIFIED_COLUMN_DFJOBTITLEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

