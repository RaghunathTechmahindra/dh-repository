package mosApp.MosSystem;

/**
 * <p>Title: PageStateHandler</p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.0
 * * @version 1.2  <br>
 * Date: 07/04/006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *  Added 3 new methods isRFDSupported,isRGPForDealAndProductGreaterThanZero,
 *getRateFloatDownTypeForDeal and edited one method [navigateToNextPage]for handling RFD new screen UI <br>
 *
 * @version 1.3  <br>
 * Date: 04-August-2008 <br>
 * Story:XS_9.2 <br>
 * Author: MCM Impl Team<br>
 * Change:  Added new method isAnyComponentEligibleForRFD to check if any components are RFD eligible.
 * 			Modified navigateToNextPage(..) to add a method call for isAnyComponentEligibleForRFD(..)
 */

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.security.ActiveMessage;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.security.DLM;
import com.basis100.deal.security.DealEditControl;
import com.basis100.deal.security.PDC;
import com.basis100.deal.security.PLM;
import com.basis100.deal.security.PropertyProvinceAC;
import com.basis100.deal.util.BusinessCalendar;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.entity.Page;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.Model;

/**
 * 
 *  
 */
////public class PageStateHandler extends Object
//// implements Sc
public class PageStateHandler extends SavedPagesModelImpl implements
        SavedPagesModel, Model, Sc, Serializable {
    
    private final static Log _log = 
        LogFactory.getLog(PageStateHandler.class);
    
    private PageHandlerCommon phc;

    protected HttpSession session;

    /**
     * 
     *  
     */
    public PageStateHandler(PageHandlerCommon phc)
    {
        this.phc = phc;

    }

    public PageStateHandler()
    {
        super();
    }

    public PageStateHandler(HttpSession session)
    {
        super();
        this.session = session;
    }

    /**
     * navigateToNextPage
     *
     * @param o
     *
     * @return vid Date: 07/04/006 <br>
     *         Author: NBC/PP Implementation Team <br>
     *         Change: <br>
     *         Added functionality to get access to the RFD screen <br>
     *         
     * Date: 04-August-2008 <br>
     * Story:XS_9.2 <br>
     * Author: MCM Impl Team<br>
     * 			Change: <br>
     * 			Modified to add a method call for isAnyComponentEligibleForRFD(..)
     */
    
    ////public void navigateToNextPage(SessionState theSession, SavedPages
    // savedPages, SessionResourceKit srk)
    public void navigateToNextPage(SessionStateModel theSessionState,
            SavedPagesModel savedPages, SessionResourceKit srk)
    throws IllegalArgumentException
    {
        SysLogger logger = srk.getSysLogger();
        logger.debug("@navigateToNextPage: {implementation version} :: set [theSession, savedPages, srk] = ["
                + (theSessionState != null)
                + ", "
                + (savedPages != null) + ", " + (srk != null) + "]");

        try
        {
            PageEntry currPage = theSessionState.getCurrentPage();

            setStandardNextPageIfNotSet(srk, currPage, savedPages);

            // if no next page then simply exit (should never happen ... but
            // ...)
            if (savedPages.isNextPageSet() == false)
            {
                logger.error("@navigateToNextPage: no next page set - unexpected but ignored");
                return;
            }


            PageEntry nextPage = savedPages.getNextPage();
            logger.debug("@navigateToNextPage: next page (id/Label) = ("
                    + nextPage.getPageId() + ", " + nextPage.getPageLabel()
                    + ")");

            //3.3 ML 
            _log.debug("before calling enforceInstitutionId @navigateToNextPage 1");
            phc.enforceInstitutionId(nextPage);
            _log.debug("after calling enforceInstitutionId @navigateToNextPage 1");
            
            // trigger underwriter worksheet special entry requirements
            if (nextPage.getPageId() == Mc.PGNM_UNDERWRITER_WORKSHEET)
            {
                ////if (underwriterWorksheetSpecial(theSession, nextPage, srk)
                // == false) return;
                boolean isUWSpecial = false;
                try
                {
                    isUWSpecial = underwriterWorksheetSpecial(theSessionState,
                            nextPage, srk);
                } catch (Exception e)
                {
                    logger.error("PSH@navigateToNextPage:Exception: " + e);
                    logger.error(e);
                }

                if (isUWSpecial == false)
                    return;                    

                // alert message already set in this case
            }

            // ***** Change by NBC Impl. Team - Version 1.1 - Start *****//

            if (nextPage.getPageId() == Mc.PGNM_RATE_FLOAT_DOWN) {
                int cid = nextPage.getPageDealCID();
                logger
                .debug("@isRGPForDealAndProductGreaterThanZero= cid***** "
                        + cid);
                int rateFloatDownType = getRateFloatDownTypeForDeal(nextPage,
                        srk);
                int isRGPForDealAndProductGreaterThanZero = isRGPForDealAndProductGreaterThanZero(
                        nextPage, srk);
                int isRFDSupported = isRFDSupported(srk);
                
                boolean isRFDSupportedForLender = isRFDSupportedForLender(nextPage, srk);
                
                boolean showAMLMessage =false;
                
//              ***************MCM Impl team changes Starts - XS_9.2 - Version 1.3 *******************/                
                boolean pefromRFD = false;
                if(isRFDSupported != 0 && isRFDSupportedForLender)
                {
	                boolean dealIsRFDElible = false;
	                boolean componentsEligibleForRFD = false;
	                
	                
	                
	                //If mosApp.MosSystem.RFDSupport != 'N' and Prodcut and Deal RGP > 0 && Rate Float Down Type != 'No Float Down' 
	                //then set deal as RFD eligible
	                if(isRGPForDealAndProductGreaterThanZero > 0 && rateFloatDownType != 1)
	                {
	                	dealIsRFDElible = true;
	                }
	                
	                //Check if any components are eligible for RFD
	                componentsEligibleForRFD = isAnyComponentEligibleForRFD(nextPage, srk);
	                
	                //If either Deal or any components eligible for RFD , set the flag                
	                if(dealIsRFDElible || componentsEligibleForRFD)
	                {
	                	pefromRFD = true;
	                }
                }
                //If Deal and Components are not eligible for RFD,display the error message
                if(pefromRFD == false)
                {
                	showAMLMessage=true;
                }
                
//              ***************MCM Impl team changes Ends - XS_9.2 - Version 1.3 *******************/                 
                if (showAMLMessage) {
                    ActiveMessage am = ActiveMsgFactory.getActiveMessage(
                            ActiveMsgFactory.ISCUSTOMCONFIRM, theSessionState
                            .getLanguageId());
                    am.setDialogMsg(BXResources.getSysMsg(
                            "PAGE_DENIED_DEFAULT", theSessionState
                            .getLanguageId()));
                    theSessionState.setActMessage(am);

                    return;
                }

            }

            // ***** Change by NBC Impl. Team - Version 1.1 - End*****//
            
            boolean backwardNavigation = false;
            if(currPage.getPageId() != Mc.PGNM_TASK_REASSIGN)
                backwardNavigation = (nextPage == currPage.getParentPage());
            boolean nextIsSubPage = (nextPage.isSubPage() == true);

            // get page entity for next page to access properties
            Page nextPgDetails = null;

            try
            {
                PDC pdc = PDC.getInstance();
                nextPgDetails = pdc.getPage(nextPage.getPageId());                

            } catch (Exception e)
            {
                logger.error("PSH@navigateToNextPage:Exception: " + e);
                logger.error(e);
            }

            Deal deal = null;

//          moved from later in this part to be userd to determin propety province for desclosure            
//          but only need to check if the nextPage is DealPage
            if(nextPage.isDealPage())
            {
                // if copy id not set determine automatically
                int cid = nextPage.getPageDealCID();
                if (cid == -1)
                {
                    try
                    {                	  
                        cid = phc.standardCopySelection(srk, nextPage);
                    } catch (Exception exc)
                    {
                        logger.error("PSH@navigateToNextPage:Exception: " + exc);
                        logger.error(exc);
                    }
                }

                // get the deal - for access to status code and other details
                deal = null;

                try
                {
                    deal = new Deal(srk, null);
                    if (nextPage.getPageDealId() > 0)
                        deal.findByPrimaryKey(new DealPK(nextPage.getPageDealId(), cid));
                } catch (Exception ex)
                {
                    logger.error("PSH@navigateToNextPage:Exception: " + ex);
                    logger.error(ex);
                }
            }

            int accessByPropertyProvince = Sc.PAGE_ACCESS_UNASSIGNED;
            int accessTypeUser = 0;

            if (backwardNavigation == false) {
                // check if the next page is disclosure page,
                // if so determin which province
                // if nextPage is Disclosure page, then nextPage.isDealPage and deal is not null
                if (nextPage != null && nextPage.getPageName() != null 
                        && nextPage.getPageName().indexOf("Disclosure") >= 0 )
                {
                    try
                    {
                        accessByPropertyProvince = determineDisclosureAccess
                        (deal, theSessionState, savedPages, nextPgDetails, nextPage, currPage, srk);

                        switch( accessByPropertyProvince )
                        {
                        case Sc.PAGE_ACCESS_EDIT:
                            logger.debug("@navigateToNextPage :: page is editable by province  ");
                            break;

                        case Sc.PAGE_ACCESS_VIEW_ONLY:
                            logger.debug("@navigateToNextPage :: page is editable by province  ");
                            break;

                        case Sc.PAGE_ACCESS_NOT_FOUND:

                            phc.setActiveMessageToAlert
                            (BXResources.getSysMsg("PAGE_DENIED_DESCLOURE", theSessionState.getLanguageId()),
                                    ActiveMsgFactory.ISCUSTOMCONFIRM );

                            // add extra comment
                            // Access to the screen is denied. no disclosuer document is currently availabe for the selected province
                            savedPages.setNextPage(null);
                            return;
                        default:
                            phc.setActiveMessageToAlert(ActiveMsgFactory.ISPAGEDENIEDMSG );
                        savedPages.setNextPage(null);
                        return;
                        }

                    }
                    catch(Exception e)
                    {
                        logger.error("PSH@navigateToNextPage:Exception: " + e);
                        logger.error(e);
                    }
                }

                // determine access by user type
                // forward navigation - determine acess by user type
                
                if (nextPage.getPageId() == Mc.PGNM_QUICKLINK_ADMININISTRATION) {
                   // Check URL access for particular institution
                	PropertiesCache cache = PropertiesCache.getInstance();
            		String quickLinkEnabledForInstitution = (cache.getProperty(srk.getExpressState().getUserInstitutionId(),
            				"com.filogix.ingestion.quicklinks.enabled", "Y"));
                	if("N".equalsIgnoreCase(quickLinkEnabledForInstitution)) {
	                	ActiveMessage am = ActiveMsgFactory.getActiveMessage(
	                            ActiveMsgFactory.ISCUSTOMCONFIRM, theSessionState
	                            .getLanguageId());
	                    am.setDialogMsg(BXResources.getSysMsg(
	                            "PAGE_DENIED_DEFAULT", theSessionState
	                            .getLanguageId()));
	                    theSessionState.setActMessage(am);
	                    savedPages.setNextPage(null);
	                    return;
                	}
                }
                
                try
                {
                    accessTypeUser = determineUserTypeAccess(theSessionState,
                            savedPages, nextPgDetails, nextPage, currPage, srk);
                } catch (Exception exp)
                {
                    logger.error("PSH@navigateToNextPage:Exception: " + exp);
                    logger.error(exp);
                }
                logger
                .debug("@navigateToNextPage :: forward navigation : access by user type = "
                        + accessTypeUser);

                // ensure access (user type) allowed
                if (accessTypeUser == Sc.PAGE_ACCESS_DISALLOWED)
                {
                    savedPages.setNextPage(null);
                    return;

                    // active message (already) set in this case
                }
            } else
            {
                // backward navigation - as is (sub-page activity can not have
                // changed!)
                accessTypeUser = (nextPage.isEditable()) ? Sc.PAGE_ACCESS_EDIT
                        : Sc.PAGE_ACCESS_VIEW_ONLY;
                logger
                .debug("@navigateToNextPage :: backward navigation : access by user type = "
                        + accessTypeUser);
            }

            // if non-deal page (sub or not) we obviously do not qualify by deal
            // status - so
            // we are essentially finished (but ensure view only if indficated
            // by page properties
            if (nextPage.isDealPage() == false)
            {
                // redundant check for (non-deal) sub-page but no matter!
                nextPage.setEditable(accessTypeUser == Sc.PAGE_ACCESS_EDIT);
                
                if(theSessionState.getCurrentPage() != null		//FXP21509 ML  
                		&& theSessionState.getCurrentPage().getPageId() > 0
                		&& theSessionState.getCurrentPage().getDealInstitutionId() >= 0) 
                {
                	int currentPageInstProfId = theSessionState.getCurrentPage().getDealInstitutionId();

                	_log.debug("before calling enforceInstitutionId @navigateToNextPage 2");

                	phc.enforceInstitutionId(theSessionState.getCurrentPage()); //FXP21249 ML
                	cleanCurrentPage(theSessionState, savedPages, srk,
                			backwardNavigation || nextIsSubPage);
                	phc.enforceInstitutionId(nextPage);
                	
                	//FXP28199: Clear session state variable if user is not going to a page in the list.
                    if(!displayMIAlert(currentPageInstProfId, nextPage.getPageId())) {
                    	theSessionState.setMiResponseExpected(null);
                    }
                    //End of FXP28199

                	_log.debug("after calling enforceInstitutionId @navigateToNextPage 2");
                }
                // off we go ...
                
                
                // PLM - Page edit Control applicable to Admin pages
                if (nextPgDetails.getPageLockOnEdit())
                {
                	 try
                     {
                         PLM plm = PLM.getInstance();
                         
                         if(plm.isLocked(nextPage.getPageId(), nextPage.getPageMosUserId(), srk) == true)
                         {
                        	  String alertMsg = BXResources.getSysMsg(
                                    "PAGE_LOCKED_BY_USER",
                                    theSessionState.getLanguageId())
                                    + " : "
                                    + plm.getPageLockedUserDetails(nextPage.getPageId(), nextPage.getPageMosUserId(), srk)
                                    + " at '"
                                    +  plm.getPageLockTimeStamp(nextPage.getPageId(), nextPage.getPageMosUserId(), srk)
                                        + "' : "
                                    + BXResources.getSysMsg("PAGE_LOCKED_FOR_EDIT",
                                            theSessionState.getLanguageId());
                        	  
                        	   phc.setActiveMessageToAlert(alertMsg,
                                     ActiveMsgFactory.ISCUSTOMCONFIRM);
                             logger.debug("@navigateToNextPage: page locked by another user - access denied");
                             savedPages.setNextPage(null);
                             return;
                         } else  {
                         plm.lock(nextPage.getPageId(), nextPage.getPageMosUserId(), srk, RequestManager.getRequest().getSession().getId().toUpperCase());
                         logger.error("PSH@@navigateToNextPage()   Lock on page: "+nextPage.getPageId()+ " User: " +
                        		 nextPage.getPageMosUserId() + "Institution Id " + srk.getExpressState().getUserInstitutionId() +
                        		 "session id" + RequestManager.getRequest().getSession().getId().toUpperCase());
                         }
                     } catch (Exception e) {
                         logger.error("PSH@navigateToNextPage -Lock on page::Exception: " + e);
                         logger.error(e);
                     }
                }
                
                standardNextPageActions(theSessionState, savedPages, nextPage,
                        srk, backwardNavigation, nextIsSubPage);
                return;
            }

            //
            // deal page ...
            //
            // Ensure we have the copy id for the deal (obtain gold copy id if
            // not) ...
            if (backwardNavigation == true)
            {
                if (nextPage.isDealPage() == true)
                {
                    nextPage.setPageDealCID(currPage.getPageDealCID());

                    // Also populate the modified flag ==> Bug fix by BILLY
                    // 14Nov2001
                    if (nextPage.isModified() == false)
                        nextPage.setModified(currPage.isModified());
                    // ==============================================================
                }
            }

            // sub-page adopt copy id of parent page (if necessary and possible)
            if (nextIsSubPage == true && nextPage.getPageDealCID() == -1)
            {
                int pCid = nextPage.getParentPage().getPageDealCID();
                if (pCid > 0)
                    nextPage.setPageDealCID(pCid);
            }

//          moved above to check disclosuer page 
//          // if copy id not set determine automatically
//          int cid = nextPage.getPageDealCID();
//          if (cid == -1)
//          {
//          try
//          {
//          cid = phc.standardCopySelection(srk, nextPage);
//          } catch (Exception exc)
//          {
//          logger.error("PSH@navigateToNextPage:Exception: " + exc);
//          }
//          }

//          // get the deal - for access to status code and other details
//          Deal deal = null;

//          try
//          {
//          deal = new Deal(srk, null);
//          deal
//          .findByPrimaryKey(new DealPK(nextPage.getPageDealId(),
//          cid));
//          } catch (Exception ex)
//          {
//          logger.error("PSH@navigateToNextPage:Exception: " + ex);
//          }

            // check deal status access permission ...
            int accessTypeStatus = 0;

            if (backwardNavigation == true)
            {
                // backward navigation - as is (sub-page activity can not have
                // changed!)
                accessTypeStatus = (nextPage.isEditable()) ? Sc.PAGE_ACCESS_EDIT
                        : Sc.PAGE_ACCESS_VIEW_ONLY;
            } else
            {
                // forward navigation ...
                ////accessTypeStatus = phc.accessByDealStatus(deal, nextPage,
                // theSession, savedPages, srk, accessTypeUser, true);
                try
                {
                    // changed by midori for disclousre
                    // acturally in phc.accessByDealStatus(,,, accessTypeUser,,)
                    // was not used
//                  accessTypeStatus = phc.accessByDealStatus(deal, nextPage,
//                  theSessionState, savedPages, srk, accessTypeUser,
//                  true);
                    accessTypeStatus = phc.accessByDealStatus(deal, nextPage,
                            theSessionState, savedPages, srk, Sc.PAGE_ACCESS_EDIT,
                            true);

                } catch (Exception exc)
                {
                    logger.error("PSH@navigateToNextPage:Exception: " + exc);
                    logger.error(exc);
                }

                logger
                .debug("@navigateToNextPage: forward navigation : deal status access type="
                        + accessTypeStatus);
                if (!(accessTypeStatus == Sc.PAGE_ACCESS_VIEW_ONLY || accessTypeStatus == Sc.PAGE_ACCESS_EDIT))
                {
                    savedPages.setNextPage(null);
                    return;

                    // active message (already) set in this case
                }
            }


            // access mode is weaker of page and status access
            int accessType = Sc.PAGE_ACCESS_EDIT;
            // at this point, accessByPropertyProvince is Sc.PAGE_ACCESS_EDIT
            if (accessTypeUser == Sc.PAGE_ACCESS_VIEW_ONLY
                    || accessTypeStatus == Sc.PAGE_ACCESS_VIEW_ONLY)
                accessType = Sc.PAGE_ACCESS_VIEW_ONLY;

            // accessByPropertyProvince is stronger than userType
            if (accessByPropertyProvince == Sc.PAGE_ACCESS_EDIT
                    || accessByPropertyProvince == Sc.PAGE_ACCESS_VIEW_ONLY)
                accessType = accessByPropertyProvince;

            // ensure view only for locked scenario
            if (isLockedScenario(deal, nextPage) == true)
                accessType = Sc.PAGE_ACCESS_VIEW_ONLY;

            // if view only proceed immediately
            if (accessType == Sc.PAGE_ACCESS_VIEW_ONLY)
            {
            	nextPage.setEditable(false);
            	if(theSessionState.getCurrentPage() != null		//FXP21509 ML  
            			&& theSessionState.getCurrentPage().getPageId() > 0
            			&& theSessionState.getCurrentPage().getDealInstitutionId() >= 0) 
            	{ 
            		_log.debug("before calling enforceInstitutionId @navigateToNextPage 3");
            		phc.enforceInstitutionId(theSessionState.getCurrentPage()); //FXP21249 ML
            		cleanCurrentPage(theSessionState, savedPages, srk,
            				backwardNavigation || nextIsSubPage);
            		phc.enforceInstitutionId(nextPage);
            		_log.debug("after calling enforceInstitutionId @navigateToNextPage 3");
            	}
            	// off we go ...
            	standardNextPageActions(theSessionState, savedPages, nextPage,
            			srk, backwardNavigation, nextIsSubPage);
            	return;
            }


            // edit mode ... check for lock on deal in question
            nextPage.setEditable(true);
            boolean lockRequired = false;
            DLM dlm = null;
            try
            {
                dlm = DLM.getInstance();
            } catch (Exception ex)
            {
                logger.error("PSH@navigateToNextPage:Exception: " + ex);
                logger.error(ex);
            }
            ////boolean currentLockPresent = dlm.isLocked(deal.getDealId(),
            // theSession.getSessionUserId());
            //---------> Cervus II : Section 5.2.2 Begins <---------//

            //--> Adjust to handle Deal Lock Override
            //--> By Neil : Nov/08/2004
            //// Vlad. It should be 0 by default for any unlocked deal.
            int currentLockStatus = 0;
            try
            {
                currentLockStatus = dlm.getLockStatus(deal.getDealId(),
                        theSessionState.getSessionUserId(), srk);

                logger .debug("=====> VLAD : PSH@navigateToNextPage : currentLockStatus = " + currentLockStatus);
            } catch (Exception ex)
            {
                logger.error("@navigateToNextPage : Exception = "
                        + ex.getMessage());
            }

            if ((currentLockStatus == DLM.NOT_LOCKED)
                    || (currentLockStatus == DLM.LOCKED_BY_SELF))
            {
                logger .debug("=====> VLAD : PSH@navigateToNextPage : DLM.NOT_LOCKED phase");

                // LOCKED_BY_SELF is treated as NOT_LOCKED
                // determine if lock required
                if (nextPgDetails.getDealLockOnEdit() == true)
                {
                    lockRequired = true;
                }
            } else if (currentLockStatus == DLM.LOCKED_BUT_OVERRIDABLE)
            {
                logger
                .debug("=====> VLAD : PSH@navigateToNextPage : DLM.LOCKED_BUT_OVERRIDABLE phase");
                phc.setActiveMessageToAlert(BXResources.getSysMsg(
                        "OVERRIDE_DEAL_LOCK", theSessionState.getLanguageId()),
                        ActiveMsgFactory.ISCUSTOMDIALOG2,
                        "OVERRIDE_DEAL_LOCK_YES", "OVERRIDE_DEAL_LOCK_NO");
            } else if (currentLockStatus == DLM.LOCKED_AND_UNOVERRIDABLE)
            {
                logger
                .debug("=====> VLAD : PSH@navigateToNextPage : DLM.LOCKED_AND_UNOVERRDIABLE phase");
                if (nextPgDetails.getAllowEditWhenLocked() == false)
                {
                    // deal locked and not allow to proceed when locked (normal
                    // case!)
                    //--Ticket#541--18Aug2004--start--//
                    String lockerName = "";
                    String lockedTime = null;
                    try
                    {
                        int dealId = deal.getDealId();
                        int userProfileId = dlm.getUserIdOfLockedDeal(deal
                                .getDealId(), srk);
                        
                        lockerName = getUserName(userProfileId, nextPage.getDealInstitutionId(), srk);
                        lockedTime = getLockedTime(userProfileId, dealId, dlm,
                                srk);
                    } catch (Exception e)
                    {
                        logger
                        .debug("@navigateToNextPage: user information about deal locked is denied."
                                + e.getMessage());
                    }

                    String alertMsg = BXResources.getSysMsg(
                            "PAGE_DENIED_DEAL_LOCKED_FIRST_PART",
                            theSessionState.getLanguageId())
                            + " : "
                            + lockerName
                            + " at "
                            + lockedTime
                            + " : "
                            + BXResources.getSysMsg("PAGE_DENIED_DEAL_LOCKED",
                                    theSessionState.getLanguageId());
                    //--Ticket#541--18Aug2004--end--//

                    phc.setActiveMessageToAlert(alertMsg,
                            ActiveMsgFactory.ISCUSTOMCONFIRM);
                    logger
                    .debug("@navigateToNextPage: deal locked by another user - access denied");
                    savedPages.setNextPage(null);
                    return;
                }
            }
            //---------> Cervus II : Section 5.2.2 Ends <---------//

            //--> Bug fix :: this should be done before locking the deal
            //--> As the cleanCurrentPage will unlock deallock some how that
            // caused problem.
            //--> By Billy 02Feb2004
            if(theSessionState.getCurrentPage() != null		//FXP21509 ML  
            		&& theSessionState.getCurrentPage().getPageId() > 0
            		&& theSessionState.getCurrentPage().getDealInstitutionId() >= 0) 
            { 
            	_log.debug("before calling enforceInstitutionId @navigateToNextPage 4");
            	phc.enforceInstitutionId(theSessionState.getCurrentPage()); //FXP21249 ML
            	cleanCurrentPage(theSessionState, savedPages, srk,
            			backwardNavigation || nextIsSubPage);
            	phc.enforceInstitutionId(nextPage);
            	_log.debug("after calling enforceInstitutionId @navigateToNextPage 4");
            }
            //==========================================================

            //--> Should place Deal Lock immediately after checking
            //--> BUG fix by Billy 30July2003
            //  setup lock on deal if required
            if (lockRequired == true)
            {
                logger
                .debug("@navigateToNextPage: deal lock required, place lock");

                try
                {
                    String miResponse = "N";
                    
                    if (("" + nextPage.getPageDealId() + "_" + nextPage.getDealInstitutionId()) .equalsIgnoreCase(theSessionState.getMiResponseExpected())){
                        miResponse = "Y";
                    } else {
                        theSessionState.setMiResponseExpected(null);
                    }
                    
                    dlm.lock( nextPage.getPageDealId(), nextPage.getPageMosUserId(), srk,
                        RequestManager.getRequest().getSession().getId().toUpperCase(), miResponse);
                    
                } catch (Exception eeex)
                {
                    logger.error("PSH@navigateToNextPage:Exception: " + eeex);
                    logger.error(eeex);
                }

            }
            //===================================================

            // Transactional copy management. When returning to parent page
            // (from sub-page) typically not
            // is activity necessary - i.e. if sub used tx copy it should have
            // been adopted or dropped before
            // getting here. However, there are rare occasions when an expected
            // transactional copy (upon
            // return to parent) does not exist. For example the parent page may
            // have adopted (or dropped)
            // its own transaction copy before navigating to the sub-page; e.g.
            // to guarantee changes in the
            // sub-page are persistent, not dependend upon submit from parent
            // page. In this case there will be
            // no transactional copy upon return.

            DealEditControl dec = theSessionState.getDec();
            if ((backwardNavigation == false)
                    || (backwardNavigation == true && ((dec == null) || (dec != null && dec
                            .isTxCopy() == false))))
            {
                nextPage.setTxCopyLevel(-1);

                // Check for Deal Entry
                if (nextPage.isDealEntry() == true)
                {
                    logger
                    .debug("@navigateToNextPage: (new) deal entry - special case , setup dec for new (tx copy) deal");

                    // special case - a new deal has been created in the
                    // database - by definition the deal is transactional
                    // until page submit
                    try
                    {
                        dec = new DealEditControl(nextPage.getPageDealId(),
                                nextPage.getPageDealCID(), true, srk);
                    } catch (Exception ex)
                    {
                        logger.error("PSH@navigateToNextPage:Exception: " + ex);
                        logger.error(ex);
                    }

                    theSessionState.setDec(dec);
                    nextPage.setTxCopyLevel(dec.getNumTxCopies());
                } else
                {
                    logger.debug("@navigateToNextPage: to editable deal page");

                    // setup transactional copy if necessary
                    if (nextPgDetails.getEditViaTxCopy() == true)
                    {
                        logger
                        .debug("@navigateToNextPage: requires tx copy - create");
                        dec = theSessionState.getDec();
                        if (dec != null)
                        {
                            // existing edit control object - create new copy
                            try
                            {
                                logger
                                .debug("Performance Check :: @navigateToNextPage: before calling dec.newCopy");
                                dec.newCopy(srk);
                                logger
                                .debug("Performance Check :: @navigateToNextPage: after calling dec.newCopy");
                            } catch (Exception ex)
                            {
                                logger.error("PSH@navigateToNextPage:Exception: " + ex);
                                logger.error(ex);
                            }
                        } else
                        {
                            // create and set new edit control object
                            try
                            {
                                logger
                                .debug("Performance Check :: @navigateToNextPage: before calling new DealEditControl");
                                dec = new DealEditControl(nextPage
                                        .getPageDealId(), nextPage
                                        .getPageDealCID(), srk);
                                logger
                                .debug("Performance Check :: @navigateToNextPage: after calling new DealEditControl");
                            } catch (Exception eex)
                            {
                                logger.error("PSH@navigateToNextPage:Exception: " + eex);
                                logger.error(eex);
                            }
                            theSessionState.setDec(dec);
                        }

                        // set current page to new copy id
                        nextPage.setPageDealCID(dec.getCID());
                        nextPage.setTxCopyLevel(dec.getNumTxCopies());
                    }
                }
            }

            // off we go ...
            standardNextPageActions(theSessionState, savedPages, nextPage, srk,
                    backwardNavigation, nextIsSubPage);
        } catch (IllegalArgumentException e)
        {
            phc.setStandardFailMessage();

            // show the page navigation info.
            ////String navMsg = " PID=" + savedPages.getNextPage().getPageId()
            // + ", Sub=" +(savedPages.getNextPage().isSubPage() ? "Yes" : "No
            // ") + ", Back=" +((savedPages.getNextPage() ==
            // theSession.getCurrentPage().getParentPage()) ? "Yes" : "No ") +
            // ", Access=" +(savedPages.getNextPage().isEditable() ? "Edit" :
            // "View") + ", Page=" + savedPages.getNextPage().getPageLabel();

            PageEntry currPage = theSessionState.getCurrentPage();

            String navMsg = " PID="
                + savedPages.getNextPage().getPageId()
                + ", Sub="
                + (savedPages.getNextPage().isSubPage() ? "Yes" : "No ")
                + ", Back="
                + ((savedPages.getNextPage() == currPage.getParentPage()) ? "Yes"
                        : "No ") + ", Access="
                        + (savedPages.getNextPage().isEditable() ? "Edit" : "View")
                        + ", Page=" + savedPages.getNextPage().getPageLabel();
            srk.getSysLogger().error(
                    "Exception @naigateToNextPage() -- " + navMsg);
            srk.getSysLogger().error(e);
            savedPages.setNextPage(null);
            throw e;
        }

    }

    /**
     * Determine whether the pageId is in the list of pages that should display MI Alert.
     * FXP28199.
     * @param deal
     * @param pageId
     * @return true if pageId is in the list, false otherwise.
     */
    private boolean displayMIAlert(int institutionProfileId, int pageId) {
		//Getting the list of pages.
    	String pageListStr = PropertiesCache.getInstance().getProperty
		(institutionProfileId, "com.filogix.express.mialert.pageid", null);
    	String pageIdStr = String.valueOf(pageId);
    	if (null != pageListStr) {
        	String[] pageListArr = pageListStr.split(",");
        	for(int i = 0; i < pageListArr.length; i++) {
        		if(pageIdStr.equals(pageListArr[i].trim())) { //In the list.
        			return true;
        		}
        	}
        }
		return false;
	}

	/**
     * 
     *  
     */
    private int determineUserTypeAccess(SessionStateModel theSessionState,
            SavedPagesModel savedPages, Page nextPgDetails, PageEntry nextPage,
            PageEntry currPage, SessionResourceKit srk) throws Exception
    {
        int access = 0;
        
        if (nextPage.isSubPage() == false)
            access = phc.accessByPage(nextPage, theSessionState, savedPages,
                    srk, true);
        else
        {
            // navigating to sub-page - set current page as parent of sub [next]
            // ===================================================================
            // Modified by Billy to prevent navigating to itself -- 07Aug2001
            //  Alert : may need to confirm with Bernard if there is any side
            // effect !!!

            /*
             * It used to be : nextPage.setParentPage(currPage);
             */

            if (nextPage.getPageId() != currPage.getPageId())
            {
                nextPage.setParentPage(currPage);
            } else
            {
                // Preserve the Parent Page -- Otherwise, cannot navigate back
                // to parent
                nextPage.setParentPage(currPage.getParentPage());
            }
            // ============ End of Changes
            // =======================================

            if (nextPage.isDealPage() == true) {
                // deal page - inherited from parent page
                if (nextPage.getPageId() == Mc.PGNM_TASK_REASSIGN)
                    access = Sc.PAGE_ACCESS_EDIT;
                else access = (currPage.isEditable() == false) ? Sc.PAGE_ACCESS_VIEW_ONLY
                        : Sc.PAGE_ACCESS_EDIT;
            }
            else
                // non-deal page - get access by user type (just like for a main
                // page!)
                access = phc.accessByPage(nextPage, theSessionState,
                        savedPages, srk, true);
        }

        // now ensure access not edit if page definition is view
        if (access == Sc.PAGE_ACCESS_EDIT
                && nextPgDetails.getEditable() == false)
            access = Sc.PAGE_ACCESS_VIEW_ONLY;

        return access;

    }

    private int determineDisclosureAccess(Deal deal, SessionStateModel theSessionState,
            SavedPagesModel savedPages, Page nextPgDetails, PageEntry nextPage,
            PageEntry currPage, SessionResourceKit srk) throws Exception
    {
        int access = 0;

        if (nextPage.isSubPage() == false) {
            //            return phc.accessByPropertyProvince(deal, nextPage,
            // theSessionState, savedPages,
            //                                                        srk, Sc.PAGE_ACCESS_EDIT, true);

            PropertyProvinceAC ppam = new PropertyProvinceAC(deal);
            return ppam.accessDisclosure(theSessionState);
        } else {
            // navigating to sub-page - set current page as parent of sub [next]
            // ===================================================================
            // Modified by Billy to prevent navigating to itself -- 07Aug2001
            //  Alert : may need to confirm with Bernard if there is any side
            // effect !!!

            /*
             * It used to be : nextPage.setParentPage(currPage);
             */

            if (nextPage.getPageId() != currPage.getPageId())
            {
                nextPage.setParentPage(currPage);
            } else
            {
                // Preserve the Parent Page -- Otherwise, cannot navigate back
                // to parent
                nextPage.setParentPage(currPage.getParentPage());
            }
            // ============ End of Changes
            // =======================================

            if (nextPage.isDealPage() == true)
                // deal page - inherited from parent page
                access = (currPage.isEditable() == false) ? Sc.PAGE_ACCESS_VIEW_ONLY
                        : Sc.PAGE_ACCESS_EDIT;
            else
                // non-deal page - get access by user type (just like for a main
                // page!)
                access = phc.accessByPage(nextPage, theSessionState,
                        savedPages, srk, true);
        }

        return access;        
    }
    /**
     * 
     *  
     */
    private boolean isLockedScenario(Deal deal, PageEntry pg)
    {
        if (deal == null)
            return false;

        // precaution ...
        String str = deal.getCopyType();

        // simple locked scenario test ...
        if (str != null && str.trim().equals("S"))
        {
            str = deal.getScenarioLocked();
            if (str != null && str.trim().equals("Y"))
                return true;
        }

        // if page is underwriter worksheet consider locked if pointing to gold
        // (e.g. viewable in post-decision mode)
        if (str != null && str.trim().equals("G"))
        {
            if (pg.getPageId() == Mc.PGNM_UNDERWRITER_WORKSHEET)
                return true;
        }

        return false;

    }

    /**
     * 
     *  
     */
    public boolean underwriterWorksheetSpecial(
            SessionStateModel theSessionState, PageEntry nextPage,
            SessionResourceKit srk) throws Exception
    {
        SysLogger logger = srk.getSysLogger();
        ////PageEntry nextPage = savedPages.getNextPage();

        // going to UW - check for first time entry (e.g. vs. return from
        // sub-page)
        int cid = nextPage.getPageDealCID();

        if (cid != -1)
            return true;

        // true "entry" condition - e.g. getting here from GO or first encounter
        // in a task's page linkage ....
        // check for deal locked - simply deny entry to underwriter worksheet if
        // locked or incomplete
        // since thinks get too complicated otherwise
        DLM dlm = DLM.getInstance();

        ////boolean currentLockPresent = dlm.isLocked(nextPage.getPageDealId(),
        // theSession.getSessionUserId());

        // Here should get the userId from SessionState instead
        //--> Fixed by BILLY 04July2002

        //---------> Cervus II : Section 5.2.2 Begins <----------//

        //--> Adjust to handle Deal Lock Override
        //--> By Neil : Nov/08/2004
        //int currentLockStatus = -1;
        //// Vlad. It should be 0 by default for any unlocked deal.
        int currentLockStatus = 0;

        int dealId = nextPage.getPageDealId();
        int currentUserId = theSessionState.getSessionUserId();
        logger
                .debug("=====> By Neil : @underwriterWorksheetSpecial : dealId = "
                        + dealId);
        logger
                .debug("=====> By Neil : @underwriterWorksheetSpecial : currentUserId = "
                        + currentUserId);

        /**
         * boolean currentLockPresent = dlm.isLocked(nextPage.getPageDealId(),
         * theSessionState.getSessionUserId(), srk); logger.debug( "=====> VLAD :
         * PSH@underwriterWorksheetSpecial :CurrentLockPresent: " +
         * currentLockPresent);
         */

        try
        {
            currentLockStatus = dlm.getLockStatus(dealId, currentUserId, srk);
        } catch (Exception ex)
        {
            logger.error("@underwriterWorksheetSpecial : Exception = "
                    + ex.getMessage());
        }
        logger
                .debug("=====> By Neil : @underwriterWorksheetSpecial : currentLockStatus = "
                        + currentLockStatus);
        switch (currentLockStatus)
        {
        case DLM.NOT_LOCKED:
            logger
                    .debug("=====> VLAD : PSH@@underwriterWorksheetSpecial:: DLM.NOT_LOCKED phase");
            break;

        case DLM.LOCKED_BY_SELF:
            logger
                    .debug("=====> VLAD : PSH@@underwriterWorksheetSpecial:: DLM.LOCKED_BY_SELF phase");
            break;

        case DLM.LOCKED_BUT_OVERRIDABLE:
            logger
                    .debug("=====> VLAD : PSH@@underwriterWorksheetSpecial:: DLM.LOCKED_BUT_OVERRIDABLE phase");

            phc.setActiveMessageToAlert(BXResources.getSysMsg(
                    "OVERRIDE_DEAL_LOCK", theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMDIALOG2, "OVERRIDE_DEAL_LOCK_YES",
                    "OVERRIDE_DEAL_LOCK_NO");

            break;

        case DLM.LOCKED_AND_UNOVERRIDABLE:
            logger
                    .debug("=====> VLAD : PSH@@underwriterWorksheetSpecial:: DLM.LOCKED_AND_UNOVERRIDABLE phase");
            // locked and not overridable
            logger
                    .debug("@navigateToNextPage.underwriterWorksheetSpecial: deal locked, cannot enter");

            //--Ticket#541--18Aug2004--start--//
            // get the dealId - for access to userProfileId and other details
            int userProfileId = -1;
            String lockedTime = null;
            String lockerName = "";
            //          int dealId = nextPage.getPageDealId();
            try
            {
                userProfileId = dlm.getUserIdOfLockedDeal(dealId, srk);
                // Get locker name.
                lockerName = getUserName(userProfileId, nextPage.getDealInstitutionId(), srk);
                // Get lockedTime
                lockedTime = getLockedTime(userProfileId, dealId, dlm, srk);

                logger
                        .trace("@navigateToNextPage:underwriterWorksheetSpecial:LockedTime: "
                                + lockedTime);
                logger
                        .trace("@navigateToNextPage:underwriterWorksheetSpecial:LockerName: "
                                + lockerName);
            } catch (Exception e)
            {
                logger
                        .debug("@navigateToNextPage: user information about deal locked is denied."
                                + e.getMessage());
            }

            String alertMsg = BXResources.getSysMsg(
                    "PAGE_DENIED_DEAL_LOCKED_FIRST_PART", theSessionState
                            .getLanguageId())
                    + " : "
                    + lockerName
                    + " at "
                    + lockedTime
                    + " : "
                    + BXResources.getSysMsg("PAGE_DENIED_DEAL_LOCKED",
                            theSessionState.getLanguageId());

            //--Ticket#541--18Aug2004--end--//
            phc.setActiveMessageToAlert(alertMsg,
                    ActiveMsgFactory.ISCUSTOMCONFIRM);

            return false;
        //              break;

        default:
            break;
        }

        //---------> Cervus II : Section 5.2.2 Ends <----------//
        // check access allowed by user type
        int accessTypeUser = phc.accessByPage(nextPage, theSessionState, phc
                .getSavedPages(), srk, true);

        if (accessTypeUser == Sc.PAGE_ACCESS_DISALLOWED)
        {
            logger
                    .debug("@navigateToNextPage.underwriterWorksheetSpecial: user access not allowed (user type)");
            return false;
        }

        // --- //
        // set focus scenario
        Deal dealScenario = new Deal(srk, null);

        dealScenario.setSilentMode(true);

        // no log errors for benign finder exceptions
        boolean scenarioLocated = false;

        // get recommended scenario (if one)
        try
        {
            dealScenario = dealScenario.findByRecommendedScenario(new DealPK(
                    nextPage.getPageDealId(), -1), false);
            scenarioLocated = true;
        } catch (Exception e)
        {
            ;
        }

        // get first unlocked scenario (if necessary and one)
        if (scenarioLocated == false)
        {
            try
            {
                dealScenario = dealScenario.findByUnlockedScenario(new DealPK(
                        nextPage.getPageDealId(), -1));
                scenarioLocated = true;
                logger
                        .debug("@navigateToNextPage.underwriterWorksheetSpecial: unlocked sceanrio located.");
            } catch (Exception e)
            {
                ;
            }
        }

        dealScenario.setSilentMode(false);

        // end quite mode
        logger
                .debug("@navigateToNextPage.underwriterWorksheetSpecial: recommended scenario located="
                        + scenarioLocated);

        MasterDeal md = new MasterDeal(srk, null);

        Deal deal = new Deal(srk, null);

        try
        {
            md = md
                    .findByPrimaryKey(new MasterDealPK(nextPage.getPageDealId()));
            cid = md.getGoldCopyId();
            deal = new Deal(srk, null);
            deal = deal.findByPrimaryKey(new DealPK(nextPage.getPageDealId(),
                    md.getGoldCopyId()));
        } catch (Exception e)
        {
            logger
                    .error("@navigateToNextPage.underwriterWorksheetSpecial: error locating gold copy of deal");
            throw new Exception("Underwriter Worksheet entry exception");
        }

        // check for incomplete status - access denied if so ...
        if (deal.getStatusId() == Mc.DEAL_INCOMPLETE)
        {
            logger
                    .debug("@navigateToNextPage.underwriterWorksheetSpecial: user access not allowed (deal incomplete)");
            phc.setActiveMessageToAlert(BXResources.getSysMsg(
                    "PAGE_DENIED_DEFAULT", theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMCONFIRM);
            return false;
        }

        //ensure selected deal's underwriterID is not 0 or null
        // to synchronize with the ingestion process for this deal.
        // If this id is 0 the access to the UWorksheet page must
        // be closed otherwise this record will never be updated with the
        // information incoming from the ingestion.
        int underwriterID = deal.getUnderwriterUserId();

        if (underwriterID == 0)
        {
            phc.setActiveMessageToAlert(BXResources.getSysMsg(
                    "UNDERWRITER_USER_ID_HAS_NOT_BEEN_ASSIGNED",
                    theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMCONFIRM);
            return false;
        }

        boolean statusChanged = false;
        // determine if status transition to in underwriting required
        if (deal.getStatusId() == Mc.DEAL_RECEIVED)
        {
            // go to in underwriting if user is underwriter
            try
            {
                logger
                        .debug("@navigateToNextPage.underwriterWorksheetSpecial: transition to in underwriting deal status");
                deal.setStatusId(Mc.DEAL_IN_UNDEREWRITING);
                deal.setStatusDate(new java.util.Date());
                deal.ejbStore();
                statusChanged = true;
                if (scenarioLocated)
                {
                    dealScenario.setStatusId(Mc.DEAL_IN_UNDEREWRITING);
                    dealScenario.setStatusDate(new java.util.Date());
                    dealScenario.ejbStore();
                }

                //Add log to deal history -- Billy 16May2001
                DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);
                String msg = hLog
                        .statusChange(Mc.DEAL_RECEIVED, "Underwriting");
                hLog.log(deal.getDealId(), deal.getCopyId(), msg,
                        Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId());

                /// workflowTrigger(0, srk, nextPage, null); // likely to
                // affect tasks
            } catch (Exception e)
            {
                throw new Exception(
                        "Underwriter Worksheet entry - transition to in underwriting failure");
            }
        }

        // create scenario (if necessary)
        if (deal.getStatusId() == Mc.DEAL_IN_UNDEREWRITING
                && scenarioLocated == false)
        {
            // make scenario based on the deal
            logger
                    .debug("@navigateToNextPage.underwriterWorksheetSpecial: create new scenario");
            try
            {
                //--> To subpress all JDBC logs
                logger.setJdbcTraceOn(false);
                int scenarioCid = md.copy();
                logger.setJdbcTraceRestore();
                logger.debug("New scenario created :: copyid = " + scenarioCid);
                //=============================
                dealScenario = dealScenario.findByPrimaryKey(new DealPK(deal
                        .getDealId(), scenarioCid));
                dealScenario.setCopyType("S");
                dealScenario.setScenarioRecommended("N");
                dealScenario.setScenarioLocked("N");
                dealScenario.setScenarioDescription(createScenarioDescription(
                        theSessionState, deal, true));
                dealScenario.setScenarioNumber(md.nextScenarioNumber());
                dealScenario.ejbStore();
                md.ejbStore();
                scenarioLocated = true;
            } catch (Exception e)
            {
                throw new Exception(
                        "Underwriter Worksheet entry - scenario creation failure");
            }
        }

        // locate recommedned again, this time permitting gold copy
        if (scenarioLocated == false)
        {
            try
            {
                dealScenario.setSilentMode(true);
                dealScenario = dealScenario.findByRecommendedScenario(
                        new DealPK(nextPage.getPageDealId(), -1), true);
                scenarioLocated = true;
                logger
                        .debug("@navigateToNextPage.underwriterWorksheetSpecial:  recommended sceanrio (gold) located.");
            } catch (Exception e)
            {
                ;
            }
        }

        // locate highest scenario number (if no unlocked and could not create)
        if (scenarioLocated == false)
        {
            try
            {
                dealScenario.setSilentMode(true);
                dealScenario = dealScenario.findByHighestScenario(new DealPK(
                        nextPage.getPageDealId(), -1));
                scenarioLocated = true;
                logger
                        .debug("@navigateToNextPage.underwriterWorksheetSpecial: sceanrio located - highest (locked) scenario.");
            } catch (Exception e)
            {
                ;
            }
        }

        // set copy id in page entry
        if (scenarioLocated == false)
        {
            throw new Exception(
                    "@navigateToNextPage.underwriterWorksheetSpecial: could not determine locate view scenario");
        } else
        {
            // set from default (focus) scenario
            nextPage.setPageDealCID(dealScenario.getCopyId());

            // page conditon 3 == true if in usderwriting status
            nextPage
                    .setPageCondition3(dealScenario.getStatusId() == Mc.DEAL_IN_UNDEREWRITING);

            // this is new type=4 for DealStatus Update - Express4.2GR
            // not call workflow but insert record in to ESBOutboundQueue table
            if (statusChanged)
                phc.workflowTrigger(4, srk, nextPage, null); 
        }

        return true;

    }

    /**
     * getLockedTime
     * 
     * @param userProfileId
     * @param dealId
     * @param dlm
     * @param srk
     * @return
     * @throws Exception
     * @author Neil at Nov/08/2004
     */
    private String getLockedTime(int userProfileId, int dealId, DLM dlm,
            SessionResourceKit srk) throws Exception
    {
        String lockedTime = dlm.getLockedTime(new Integer(dealId).toString(),
                new Integer(userProfileId).toString(), srk);

        if (lockedTime == null)
        {
            // no lock found
            String errMsg = "Lock record not exists for DEALID = " + dealId
                    + " AND USERPROFILEID = " + userProfileId;
            throw new Exception("@navigateToNextPage : Exception = " + errMsg);
        }

        return lockedTime;
    }

    /**
     * 
     *  
     */
    public String createScenarioDescription(SessionStateModel theSessionState,
            Deal deal, boolean gold)
    {

        String tsString = getScenFormatDate(deal.getSessionResourceKit(),
                theSessionState.getSessionUserTimeZone());

        if (gold == true)
        {
            return "Based on Deal on " + tsString;
        }

        return "Based on Scenario " + deal.getScenarioNumber() + " on "
                + tsString;

    }

    private static String datePattern = "yyyy-MM-dd HH:mm:ss.SSS";

    /**
     * 
     *  
     */
    private String getScenFormatDate(SessionResourceKit srk,
            int callerTimeZoneId)
    {

        /*
         * Calendar cal = Calendar.getInstance();
         * 
         * String ss = months[cal.get(Calendar.MONTH)] + " " +
         * parseSingleDigit(cal.get(Calendar.DATE)) + " " +
         * cal.get(Calendar.YEAR) + " " + "at " + cal.get(Calendar.HOUR_OF_DAY) +
         * ":" + parseSingleDigit(cal.get(Calendar.MINUTE));
         */
        // get string for current date - then convert to caller timezone
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

        String atDate = sdf.format(new java.util.Date());

        BusinessCalendar bcal = new BusinessCalendar(srk);

        String ss = bcal.tzDisplay(atDate, callerTimeZoneId,
                "yyyy-MM-dd HH:mm:ss.SSS", "MMM dd yyyy 'at' HH:mm");

        return ss + " " + bcal.getTimeZoneId(callerTimeZoneId);

    }

    private static String[] months =
    { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct",
            "Nov", "Dec" };

    /**
     * 
     *  
     */
    private String parseSingleDigit(int di)
    {
        if (di < 10)
            return ("0" + di);

        return ("" + di);

    }

    /**
     * 
     *  
     */
    private String parseAmPm(int ampmid)
    {
        if (ampmid == 0)
            return "AM";

        return "PM";

    }

    /**
     * 
     *  
     */
    private void cleanCurrentPage(SessionStateModel theSessionState,
            SavedPagesModel savedPages, SessionResourceKit srk,
            boolean goingToOrFromSubPage) throws IllegalArgumentException
    {
        SysLogger logger = srk.getSysLogger();

        // cases here current page not cleaned
        //
        //  1. going to sub-page
        //  2. going to parent of sub-page
        if (goingToOrFromSubPage)
        {
            logger
                    .debug("@navigateToNextPage.cleanCurrentPage: to or from sub-page, do nothing.");
            return;
        }

        PageEntry pe = theSessionState.getCurrentPage();

        // check current page editable deal page
        if ((pe.isEditable() == true && pe.isDealPage() == true)
                || pe.isDealEntry())
        {
            logger
                    .debug("@navigateToNextPage.cleanCurrentPage: editable deal page or (new) deal entry");

            // remove lock (if necessary)
            //
            // NOTICE
            // -------
            //
            // The astute programmer will have noticed that the current
            // arrangement
            // does not allow for the condition where a sub-page establishes a
            // deal lock where the parent page has not - it is assumed that such
            // behavior is not necessary - i.e. if a lock is required on a
            // sub-page
            // the same will be true for the main page.
            logger
                    .debug("@navigateToNextPage.cleanCurrentPage: remove tx copy (if one) - or the deal if is (unsubmitted) deal ");
            DLM dlm = null;
            try
            {
                dlm = DLM.getInstance();
                /***** 4.3 GR MI response alert *****/
                String miResponse = dlm.getMIResponseExpected(srk, pe
                        .getPageDealId(), pe.getPageMosUserId());
                if ("Y".equalsIgnoreCase(miResponse))
                    theSessionState.setMiResponseExpected("" + pe.getPageDealId() + "_" + pe.getDealInstitutionId());
                /***** 4.3 GR MI response alert *****/
                dlm.unlock(pe.getPageDealId(), pe.getPageMosUserId(), srk, true);
            } catch (Exception e)
            {
                logger.error("PSH@navigateToNextPage:Exception: " + e);
                logger.error(e);
            }

            // free transactional deal copies (if any)
            ////DealEditControl dec = theSession.getDec();
            DealEditControl dec = theSessionState.getDec();
            if (dec == null)
                return;
            if (pe.isDealEntry())
            {
                logger
                        .debug("@navigateToNextPage.cleanCurrentPage: (new) deal entry - remove the deal)");
                MasterDeal md = null;
                try
                {
                    md = new MasterDeal(srk, null, pe.getPageDealId());
                } catch (Exception ex)
                {
                    logger.error("PSH@navigateToNextPage:Exception: " + ex);
                    logger.error(ex);
                }
                srk.setInterimAuditOn(false);
                try
                {
                    md.removeDeal();
                } catch (Exception e)
                {
                    logger.error("PSH@navigateToNextPage:Exception: " + e);
                    logger.error(e);
                }
                srk.restoreAuditOn();
                ////theSession.setDec(null);
                theSessionState.setDec(null);
                pe.setPageDealCID(-1);
                return;
            }
            logger
                    .debug("@navigateToNextPage.cleanCurrentPage: removing tx copies");
            //// TODO. This is a very temp comment to compile the util class.
            //// The com/basis100/... classes should be recompiled to reflect
            // the
            //// modifing of MosSystem package to mosApp.MosSystem. This
            // modification
            //// is an essential feature of JATO.

            try
            {
                dec.dropToTxLevel(0, srk, pe);
            } catch (Exception ex)
            {
                logger.error("PSH@navigateToNextPage:Exception: " + ex);
                logger.error(ex);
            }
            pe.setPageDealCID(dec.getCID());
            theSessionState.setDec(null);
        }
        
        Page currentPgDetails = null;

        try
        {
            PDC pdc = PDC.getInstance();
            currentPgDetails = pdc.getPage(pe.getPageId());                

        } catch (Exception e)
        {
            logger.error("PSH@navigateToNextPage:Exception: " + e);
            logger.error(e);
        }
        	
        // PLM - Restricts single user to edit the Admin pages 
        if (currentPgDetails.getPageLockOnEdit())
        {
        	 try
             {
                 PLM plm = PLM.getInstance();
                 
                 plm.unlock(pe.getPageId(), pe.getPageMosUserId(), srk, true, RequestManager.getRequest().getSession().getId().toUpperCase());
                 logger.debug("PSH@navigateToNextPage - Lock removed for  Page Id: "+pe.getPageId()+ " User: " + pe.getPageMosUserId() +
                		 "Institution Id " + srk.getExpressState().getUserInstitutionId() + " Session Id:" +
                		 RequestManager.getRequest().getSession().getId().toUpperCase());
             } catch (Exception e) {
                 logger.error("PSH@navigateToNextPage:Exception: " + e);
                 logger.error(e);
             }
        }
    }

    /**
     * 
     *  
     */
    private void setStandardNextPageIfNotSet(SessionResourceKit srk,
            PageEntry currPage, SavedPagesModel savedPages)
    {
        SysLogger logger = srk.getSysLogger();
        //logger.debug("PSH@setStandardNextPageIfNotSet::start");
        //logger.debug("PSH@setStandardNextPageIfNotSet::CurrPage is subPage? "
        // + currPage.isSubPage());
        //logger.debug("PSH@setStandardNextPageIfNotSet::Is nextPageSet? " +
        // savedPages.isNextPageSet());

        if (currPage != null && currPage.isSubPage() == true)
        {
            // must go back to parent from subpage (unless going to further sub)
            if (savedPages.isNextPageSet() == true)
            {
                logger.debug("PageStateHandler@)setStandardNextPageIfNotSet curr page is sub and next page set case");
                PageEntry nextPage = savedPages.getNextPage();
                logger.debug("NextPage from PageStateHandler@)setStandardNextPageIfNotSet: "
                                + nextPage);
                logger.debug("Is nextPage SubPage? " + nextPage.isSubPage());

                if (nextPage.isSubPage() == true)
                    return;

                // then ok ... continue on down the line.
            }
            savedPages.setNextPage(currPage.getParentPage());
        }

        if (savedPages.isNextPageSet() == false)
        {
            logger.debug("PageStateHandler@)setStandardNextPageIfNotSet next page IS NOT set case");
            if (currPage.isTaskNavigateMode() == true
                    && currPage.getNextPE() != null)
            {
                savedPages.setNextPage(currPage.getNextPE());

                ////PageEntry nextPE = currPage.getNextPE();
                ////logger.debug("NextPE page: " + nextPE);
            } else if (savedPages.getWorkQueue() != null
                    && savedPages.getWorkQueue().getPageId() > 0)
            {
                savedPages.setNextPageAsWorkQueue();
            } else if (savedPages.getSearchPage() != null)
            {
                savedPages.setNextPageAsDealSearch();
            }
        }

    }

    /**
     * 
     *  
     */
    public void standardNextPageActions(SessionStateModel theSessionState,
            SavedPagesModel savedPages, PageEntry nextPage,
            SessionResourceKit srk, boolean backwardNavigation,
            boolean nextIsSubPage)
    {
        SysLogger logger = srk.getSysLogger();

        // log the page navigation
        String navMsg = "Navigation: PID=" + nextPage.getPageId() + ", Sub="
                + (nextIsSubPage ? "Yes" : "No ") + ", Back="
                + (backwardNavigation ? "Yes" : "No ") + ", Access="
                + (nextPage.isEditable() ? "Edit" : "View") + ", Page="
                + nextPage.getPageLabel();
        if (nextPage.isDealPage() == true)
            navMsg += ", Deal=" + nextPage.getPageDealId() + ", CID="
                    + nextPage.getPageDealCID();

        logger.info(navMsg);
        logger.debug("PSH@standardNextPageActions::is the next page SubPage? "
                + nextPage.isSubPage());

        if (nextPage.isSubPage() == true)
        {
            theSessionState.setCurrentPage(nextPage);
            logger
                    .debug("PSH@standardNextPageActions::I am after setCurrentPage");
            savedPages.setNextPage(null);
            logger
                    .debug("PSH@standardNextPageActions::I am after set next page null");

            return;
        }

        PageEntry currPage = theSessionState.getCurrentPage();
        logger.debug("PSH@standardNextPageActions::CurrPageName: "
                + currPage.getPageName());

        if (currPage != null)
        {
            if (savedPages.isBackPageSet() == false)
            {
                savedPages.setBackPage(currPage);
      	  	}
            savedPages.addToPreviousPages(currPage);
        }

        theSessionState.setCurrentPage(nextPage);

        //nextPage.show("PSH@standardNextPageActions::Show nextPage", logger);
        savedPages.setNextPage(null);
        //logger.debug("PSH@standardNextPageActions::PreviousPagesQueue: " +
        // savedPages.getPreviousPagesQueue());
        savedPages.getPreviousPagesQueue().removeIfQueued(theSessionState);
        savedPages.revisePreviousPagesLinks();
        //logger.debug("I finished PageStateH@standardNextPageActions::AMSize:
        // " + theSessionState.getActMessage().getMsgs().size());
    }

    //--Ticket#541--18Aug2004--start--//
    /* this is never called
    private String getUserName(SessionResourceKit srk)
    {
        int userprofileid = srk.getUserProfileId();
        String username = "";

        try
        {
            UserProfile up = new UserProfile(srk)
                    .findByPrimaryKey(new UserProfileBeanPK(userprofileid));

            username = up.getUserFullName();
        } catch (Exception e)
        {
            SysLogger logger = srk.getSysLogger();
            logger.error("Exception occurred @handleSubmitStandard()");
            logger.error(e);
        }

        return username;
    }
    */
    
    private String getUserName(int userprofileid, int institutionId, SessionResourceKit srk)
    {
        String username = "";

        try
        {        	
            UserProfile up = new UserProfile(srk)
                    .findByPrimaryKey(new UserProfileBeanPK(userprofileid, institutionId));
            username = up.getUserFullName();
        } catch (Exception e)
        {
            SysLogger logger = srk.getSysLogger();
            logger.error("Exception occurred @handleSubmitStandard()");
            logger.error(e);
        }

        return username;
    }
    //--Ticket#541--18Aug2004--end--/

    /**
     * getRateFloatDownTypeForDeal
     *
     * @param PageEntry,SessionResourceKit
     *
     *
     * @return int : the result of getRateFloatDownTypeForDeal
     *
     * Date: 07/04/006 <br>
     * Author: NBC/PP Implementation Team <br>
     * Change: <br>
     * This is a new method to handle conditions - access to RFD screen Show RFD
     * screen only when RFD is supported <br>
     */
    public int getRateFloatDownTypeForDeal(PageEntry nextPage,
            SessionResourceKit srk) {
        SysLogger logger = srk.getSysLogger();
        int type = 0;
        int copyId = nextPage.getPageDealCID();

        if (copyId == -1) {

            try {

                copyId = phc.standardCopySelection(srk, nextPage);

            } catch (Exception exc) {
                logger
                        .error("@getRateFloatDownTypeForDeal - default copy Id could not be fetched"
                                + exc);
            }
        }
        Deal deal = null;

        try {

            deal = new Deal(srk, null);
            deal.findByPrimaryKey(new DealPK(nextPage.getPageDealId(), copyId));

            type = deal.getRateFloatDownTypeId();

        } catch (Exception ex) {
            logger
                    .error("@getRateFloatDownTypeForDeal cannot get the deal from the database"
                            + ex);
        }

        return type;
    }

    /**
     * isRGPForDealAndProductGreaterThanZero
     *
     * @param PageEntry,SessionResourceKit
     *
     *
     * @return int : the result of isRGPForDealAndProductGreaterThanZero
     *
     * Date: 07/04/006 <br>
     * Author: NBC/PP Implementation Team <br>
     * Change: <br>
     * This is a new method to handle conditions - access to RFD screen Show RFD
     * screen only when Deal and product RGP is greater than 0 <br>
     */
    public int isRGPForDealAndProductGreaterThanZero(PageEntry nextPage,
            SessionResourceKit srk) {
        SysLogger logger = srk.getSysLogger();
        int dealRGP = 0;
        int productRGP = 0;
        int returnVal = 0;
        int copyId = nextPage.getPageDealCID();

        if (copyId == -1) {
            try {

                copyId = phc.standardCopySelection(srk, nextPage);

            } catch (Exception exc) {
                logger
                        .error("@isRGPForDealAndProductGreaterThanZero - default copy Id could not be fetched"
                                + exc);
            }
        }
        Deal deal = null;

        try {

            deal = new Deal(srk, null);
            deal.findByPrimaryKey(new DealPK(nextPage.getPageDealId(), copyId));
            dealRGP = deal.getRateGuaranteePeriod();

            productRGP = deal.getMtgProd().getRateGuaranteePeriod();

            if (dealRGP > 0 && productRGP > 0) {
                returnVal = 1;

            }

        } catch (Exception ex) {
            logger
                    .error("@isRGPForDealAndProductGreaterThanZero - deal details could not be fetched"
                            + ex);
        }

        return returnVal;
    }

    /**
     * isRFDSupported
     *
     * @param SessionResourceKit
     *
     *
     * @return int : the result of isRFDSupported
     *
     * Date: 07/04/006 <br>
     * Author: NBC/PP Implementation Team <br>
     * Change: <br>
     * This is a new method to handle conditions - access to RFD screen Show RFD
     * screen only when RFD is supported by system <br>
     */
    public int isRFDSupported(SessionResourceKit srk) {
        SysLogger logger = srk.getSysLogger();

        int returnVal = 1;

        try {
            if ((PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(),
                    "mosApp.MosSystem.RFDSupport", "N")).equals("N")) {
                returnVal = 0;

            }
        } catch (Exception exc) {
            logger.error("@isRFDSupported cannot read mossys properties" + exc);
        }

        return returnVal;
    }
    
    
    /**
     * isAnyComponentEligibleForRFD
     *
     * @param PageEntry,SessionResourceKit
     *
     *
     * @return int : the result of isAnyComponentEligibleForRFD
     *
     * Author: MCM Implementation Team <br>
     * @version 1.0 XS_9.2 01-August-2008 Initial Version
     * Change: <br>
     * This method is to check if any components are RFD eligible.
     */
    public boolean isAnyComponentEligibleForRFD(PageEntry nextPage,
            SessionResourceKit srk) {
    	
        SysLogger logger = srk.getSysLogger();
        boolean isAnyComponentEligibleForRFD = false;
        int copyId = nextPage.getPageDealCID();
        int productRGP = 0;
        int componentRGP = 0;
        int RFDTypeId=0;
        if (copyId == -1) 
        {
            try 
            {
                copyId = phc.standardCopySelection(srk, nextPage);

            } catch (Exception exc) {
                logger
                        .error("@isAnyComponentEligibleForRFD - default copy Id could not be fetched"
                                + exc);
            }
        }
        
        try 
        {
        	Deal deal = new Deal(srk, null);
            deal.findByPrimaryKey(new DealPK(nextPage.getPageDealId(), copyId));
            //Get all the components
            Collection components = deal.getComponents();
        	Iterator itComponents = components.iterator();
        	
        	//For each component
            while (itComponents.hasNext())
            {
            	Component component = (Component) itComponents.next();
            	 //If the component is a Mortgage Component
            	if(component.getComponentTypeId() == Mc.COMPONENT_TYPE_MORTGAGE)
            	{
            		ComponentMortgage componentMortgage = component.getComponentMortgage();
            		
            		//Get the product for the Component
                	MtgProd mtgprod = new MtgProd(srk,null,component.getMtgProdId());
                	//Read Product RGP and Rate Float Down Type
                	productRGP = mtgprod.getRateGuaranteePeriod();
                	RFDTypeId = mtgprod.getRateFloatDownTypeId();
                	
                	//Read Component RGP
                	componentRGP = componentMortgage.getRateGuaranteePeriod();
                	
                	//Check if this component is eligible for RFD
                	if( productRGP > 0 && RFDTypeId != Mc.RATE_FLOAT_DOWN_TYPE_DEFAULT && componentRGP > 0){
                		isAnyComponentEligibleForRFD = true;
                		break;
                	}
            	}	
            	
            }//End of while

        } catch (Exception ex) {
            logger
                    .error("@isAnyComponentEligibleForRFD - deal details could not be fetched"
                            + ex);
        }

        return isAnyComponentEligibleForRFD;
    }    

    /**
     * isRFDSupportedForLender
     *
     * @param PageEntry,SessionResourceKit
     *
     *
     * @return int : the result of isRFDSupportedForLender
     *
     * Author: MCM Implementation Team <br>
     * @version 1.0 XS_9.2 01-August-2008 Initial Version
     * Change: <br>
     * This method is to check if Lender supports RFD.
     */
    public boolean isRFDSupportedForLender(PageEntry nextPage,
            SessionResourceKit srk) {
    	
        SysLogger logger = srk.getSysLogger();
        boolean isRFDSupportedForLender = false;
        int copyId = nextPage.getPageDealCID();
        if (copyId == -1) 
        {
            try 
            {
                copyId = phc.standardCopySelection(srk, nextPage);

            } catch (Exception exc) {
                logger
                        .error("@isRFDSupportedForLender - default copy Id could not be fetched"
                                + exc);
            }
        }
        
        try 
        {
        	Deal deal = new Deal(srk, null);
            deal.findByPrimaryKey(new DealPK(nextPage.getPageDealId(), copyId));
            if(deal.getLenderProfile().getSupportRateFloatDown().equalsIgnoreCase("Y"))
            {
            	isRFDSupportedForLender=true;
            }

        } catch (Exception ex) {
            logger
                    .error("@isRFDSupportedForLender - deal details could not be fetched"
                            + ex);
        }

        return isRFDSupportedForLender;
    }    
}

