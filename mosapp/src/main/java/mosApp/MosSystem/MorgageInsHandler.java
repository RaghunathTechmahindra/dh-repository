package mosApp.MosSystem;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.commitment.CCM;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.miprocess.MIProcessHandler;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.security.DealStatusManager;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.util.DBA;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.services.mi.MIResponseProcessHelper;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.DisplayField;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.TextField;


/**
 *
 *
 */
public class MorgageInsHandler extends PageHandlerCommon
implements Cloneable, Sc
{
	private final static Logger logger = LoggerFactory.getLogger(MorgageInsHandler.class);

	public static final String MI_REVIEW_OK_TO_NEXT_SCN="B";
	public static final String MI_REVIEW_OK_TO_PLACE_REQUEST="C";
	public static final String MI_REVIEW_OK_TO_CANCEL_REQUEST="D";

	public MorgageInsHandler cloneSS()
	{
		return(MorgageInsHandler) super.cloneSafeShallow();
	}

	//This method is used to populate the fields in the header
	//with the appropriate information
	public void populatePageDisplayFields()
	{
		PageEntry pg = theSessionState.getCurrentPage();
		ViewBean thePage = getCurrNDPage();

		//      Ticket #4113
		DisplayField df = ((pgMortgageInsViewBean)thePage).getDisplayField("stLOCInterestOnlyMaturityDate");
		if(df != null & df.getValue() != null)
		{
			String val = (String) df.getValue().toString();
			if(val.indexOf("1969-12-31") > -1)
			{
				thePage.setDisplayFieldValue("stLOCInterestOnlyMaturityDate", "");
			}
		}

		//	  Ticket #4115
		DisplayField _locAmort = ((pgMortgageInsViewBean)thePage).getDisplayField("stLOCAmortizationMonths");
		if(_locAmort != null & _locAmort.getValue() != null)
		{
			String val = _locAmort.getValue().toString();
			if(val.equals("0"))
			{
				thePage.setDisplayFieldValue("stLOCAmortizationMonths", "");
			}
		}

		populatePageShellDisplayFields();

		// Quick Link Menu display
		displayQuickLinkMenu();

		populateTaskNavigator(pg);

		populatePageDealSummarySnapShot();

		populatePreviousPagesLinks();

		// ***** Change by NBC Impl. Team - Bug#1679 - Start *****//
		if(isForceProgessAdvance(getSessionResourceKit())){
			thePage.setDisplayFieldValue("hdProgressAdvanceHidden","Y");
		}
		// ***** Change by NBC Impl. Team - Bug#1679 - End *****//
	}

	//
	// Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
	//

	public void setupBeforePageGeneration()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		if (pg.getSetupBeforeGenerationCalled() == true) return;

		pg.setSetupBeforeGenerationCalled(true);


		// -- //
		setupDealSummarySnapShotDO(pg);
		int dealId = pg.getPageDealId();
		int copyId = pg.getPageDealCID();

		if (dealId <= 0)
		{
			setStandardFailMessage();
			logger.error("Exception @MorgageInsHandler.setupBeforePageGenerationProblem: invalid dealId (" + dealId + ") , Copy Id (" + copyId + ")");
			return;
		}

		//5.0 MI -- start
		try {
			SessionResourceKit srk = getSessionResourceKit();
			Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
			if (deal.getMortgageInsurerId() != 0)
			{
				MIResponseProcessHelper.processPendingAsyncMIResponse(srk, deal);

				//notify users that MI premium is changed
				if(displayAMLForUpdatedMIPremium(deal)) return;
			}
			
		} catch (Exception e) {
			logger.error("Exception at setupBeforePageGeneration", e);
			setStandardFailMessage();
		}
		//5.0 MI -- end

		// -- //
		// check for mi status indicating respopnse received from MI carrier - an unreviewed response ...
		MIProcessHandler theMIProcess = MIProcessHandler.getInstance();

		theMIProcess.checkMIResponse(dealId, copyId, getSessionResourceKit(), this, pg);
		Integer cspDealId = new Integer(dealId);
		Integer cspCopyId = new Integer(copyId);

		// set criteria for data objects
		doMIReviewModelImpl doview = (doMIReviewModelImpl)
		(RequestManager.getRequestContext().getModelManager().getModel(doMIReviewModel.class));

		doview.clearUserWhereCriteria();
		doview.addUserWhereCriterion("dfDealId", "=", cspDealId);
		doview.addUserWhereCriterion("dfCopyId", "=", cspCopyId);

		try
		{
			ResultSet rs = doview.executeSelect(null);

			if(rs == null || doview.getSize() < 0)
			{
				logger.debug("MIH@setupBeforePageGeneration::No row returned!!");
				//getTheSession().setDialogAlertMessage("Wrong Deal No For Query..");
				setActiveMessageToAlert("Wrong Deal No For Query..", ActiveMsgFactory.ISCUSTOMCONFIRM);
				logger.error("Problem encountered querying MI Insurance - invalid dealId (" + dealId + ") , Copy Id (" + copyId + ")");
				return;
			}
		}
		catch (ModelControlException mce)
		{
			logger.error("MIH@setupBeforePageGeneration::MCEException: " + mce);
		}

		catch (SQLException sqle)
		{
			logger.error("MIH@setupBeforePageGeneration::SQLException: " + sqle);
		}

	}



	public void handleMortgageInsuranceSubmit()
	{
		try
		{
			// Run MI Process
			MIProcessHandler theMIProcess = MIProcessHandler.getInstance();
			SessionResourceKit srk = getSessionResourceKit();
			PageEntry pg = theSessionState.getCurrentPage();



			// Save changes MI data into deal table
			if (updateForMortgageInsurance(pg)) {

				Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());

				//stop execution if user is trying to send unnecessary cancel request
				
				if((deal.getMIIndicatorId() == Mc.MI_INDICATOR_UW_WAIVED || deal.getMIIndicatorId() == Mc.MI_INDICATOR_NOT_REQUIRED) 
						&& needToStopSendingCancel(deal))
				{

					setActiveMessageToAlert(
							BXResources.getSysMsg("MI_LRU_PREQUAL_ERROR_MSG", 
									theSessionState.getLanguageId()),
									ActiveMsgFactory.ISCUSTOMCONFIRM);
					return;
				}
				
				//QC393 UC2 Akternate flow 3:: if the deal was approved before do not show popup error dialog.
				if(deal.getMIStatusId() == Mc.MI_STATUS_APPROVAL_RECEIVED || deal.getMIStatusId() == Mc.MI_STATUS_APPROVED || 
						deal.getMIStatusId() == Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED) {
					setActiveMessageToAlert(BXResources.getSysMsg("MI_REVIEW_RCMSTATUS_CHANGE_PENDING_MSG", theSessionState.getLanguageId()),
							ActiveMsgFactory.ISCUSTOMCONFIRM);
				}
				//QC-Ticket-490-Start
				else 
				if ((deal.getMIIndicatorId() == Mc.MI_INDICATOR_UW_WAIVED || deal.getMIIndicatorId() == Mc.MI_INDICATOR_NOT_REQUIRED) &&
					(deal.getMIStatusId() == Mc.MI_STATUS_INITIAL_REQUEST_PENDING || deal.getMIStatusId() == Mc.MI_STATUS_INITIAL_REQUEST_SUBMITTED))
				{
					setActiveMessageToAlert(BXResources.getSysMsg("MI_REVIEW_RCMSTATUS_NOT_REQUIRED_OPEN_REQ_MSG", theSessionState.getLanguageId()),
							ActiveMsgFactory.ISCUSTOMDIALOG_MI_REQUEST, MI_REVIEW_OK_TO_CANCEL_REQUEST);
					return;
				}
				//QC-Ticket-490-End				
				else {
					// validation
					PassiveMessage pm = theMIProcess.validateMIAttributes(deal, theSessionState, pg, srk);

					//QC361 show error on hard OR soft stops.
					if (pm.getNumMessages() > 0)//(pm.getCritical() == true)
					{
						pm.setGenerate(true);
						theSessionState.setPasMessage(pm);

						if (deal.getMIIndicatorId() == Mc.MII_NOT_REQUIRED || deal.getMIIndicatorId() == Mc.MII_UW_WAIVED){
							setActiveMessageToAlert(BXResources.getSysMsg("MI_VALIDATION_CANCEL_ERROR_MSG", theSessionState.getLanguageId()),
									ActiveMsgFactory.ISCUSTOMCONFIRM);
						} else {
							//5.0 MI, QC146, change to send request immediately after clicking OK button,  
							setActiveMessageToAlert(BXResources.getSysMsg("MI_VALIDATION_ERROR_MSG", theSessionState.getLanguageId()),
									ActiveMsgFactory.ISCUSTOMDIALOG_MI_REQUEST, MI_REVIEW_OK_TO_PLACE_REQUEST);
						}
						return;
					}
				}

				// no validate problems - determine recommended communication status and produce appropriate dialog for user
				getRecommendMIStatusAndDisplayNextDialog(theMIProcess, deal, srk);

			} else {
				// if no message set (no eror encountered) then away we go ...
				if (theSessionState.isActMessageSet() == false) navigateToNextPage(true);
			}

		} catch(Exception e) {
			logger.error("Exception @MorgageInsHandler.handleMortgageInsuranceSubmit()", e);
			setStandardFailMessage();
		}
	}





	/**
	 *
	 *
	 */
	private boolean updateForMortgageInsurance(PageEntry pg)
	{
		int theDealId = pg.getPageDealId();

		int theCopyId = pg.getPageDealCID();

		ViewBean thePage = getCurrNDPage();


		// Get the values to be updated from Screen
		int theMIIndicator = readIntValueFromCombobox(thePage, "cbMIIndicator");

		TextField MIExistingPolicy =(TextField) thePage.getDisplayField("txMIExistingPolicy");
		String theMIExistingPolicy = (String) MIExistingPolicy.getValue();

		int theMIType = readIntValueFromCombobox(thePage, "cbMIType");

		int theMIInsurer = readIntValueFromCombobox(thePage, "cbMIInsurer");

		int theProgressAdvanceTypeId = readIntValueFromCombobox(thePage, "cbProgressAdvanceType");


		String theMIUpfront = (String) thePage.getDisplayFieldValue("rbMIUpfront");


		int theMIPayor =  readIntValueFromCombobox(thePage, "cbMIPayor");

		TextField MIComments =(TextField) thePage.getDisplayField("txMIComments");
		String theMIComments = (String) MIComments.getValue();

		double theMIPremium = 0;

		try
		{
			HiddenField MIPremium =(HiddenField) thePage.getDisplayField("hdMIPremium");
			String sMIPremium = MIPremium.getValue().toString();
			logger.debug("MIH@updateForMortgageInsurance::MIPremium: " + sMIPremium);

			theMIPremium = (new Double(sMIPremium).doubleValue());
		}
		catch(Exception e)
		{
			logger.debug("MIH@updateForMortgageInsurance::Exception getting MIPremium value", e);
		}

		// Additional fields for CMHN modifications -- By BILLY 12Feb2002
		// Changed txMIPreQCertNum ==> stMIPreQCertNum ViewOnly -- By BILLY 08April2002
		//String theMIPreQCertNum = thePage.getDisplayFieldStringValue("txMIPreQCertNum");
		//--BMO_MI_CR--//
		// Changed back to textbox for BMO (see below).
		//========================================================================================
		////String rbMIRUIntervention = thePage.getDisplayFieldStringValue("rbMIRUIntervention");
		String rbMIRUIntervention = (String) thePage.getDisplayFieldValue("rbMIRUIntervention");
		logger.debug("MIH@updateForMortgageInsurance::rbMIRUIntervention: " + rbMIRUIntervention);




		//  MI additions
		String theProgressAdvanceInspectionBy = (String) thePage.getDisplayFieldValue("rbProgressAdvanceInspectionBy");
		logger.debug("MIH@updateForMortgageInsurance::progressAdvanceInspectionBy: " + theProgressAdvanceInspectionBy);

		String theSelfDirectedRRSP = (String) thePage.getDisplayFieldValue("rbSelfDirectedRRSP");
		logger.debug("MIH@updateForMortgageInsurance::selfDirectedRRSP: " + theSelfDirectedRRSP);

		String theRequestStandardService = (String) thePage.getDisplayFieldValue("rbRequestStandardService");
		logger.debug("MIH@updateForMortgageInsurance::requestStandardService: " + theRequestStandardService);

		int theLOCRepaymentType = 0;
		try
		{
			ComboBox cbLOCRepaymentType = (ComboBox) thePage.getDisplayField("cbLOCRepaymentType");
			String strLOCRepaymentType = cbLOCRepaymentType.getValue().toString();
			logger.debug("MIH@updateForMortgageInsurance::LOCRepaymentType: " + strLOCRepaymentType);

			theLOCRepaymentType = (new Integer(strLOCRepaymentType)).intValue();

		}
		catch(Exception e)
		{
			logger.error("MIH@updateForMortgageInsurance::Exception getting cbLOCRepaymentType value", e);
		}

		String theCMHCProdTrackerId = "";
		try
		{
			TextField CMHCProdTrackerId =(TextField) thePage.getDisplayField("txCMHCProductTrackerIdentifier");

			// fixed for 4080 - Midori -- start
			if (CMHCProdTrackerId.getValue() != null){
				theCMHCProdTrackerId =  CMHCProdTrackerId.getValue().toString();
			}   
			//      fixed for 4080 - Midori -- end

			logger.debug("MIH@updateForMortgageInsurance::CMHCProdTrackerId: " + theCMHCProdTrackerId);}
		catch(Exception e)
		{
			logger.error("MIH@updateForMortgageInsurance::Exception getting CMHCProdTrackerId value", e);
		}

		try
		{
			SessionResourceKit srk = getSessionResourceKit();
			CalcMonitor dcm = CalcMonitor.getMonitor(srk);

			// Get the corresponding Deal Entity
			Deal theDeal = new Deal(srk, dcm, theDealId, theCopyId);
			srk.beginTransaction();

			// Update the MI fields
			theDeal.setMIIndicatorId(theMIIndicator);
			theDeal.setMIExistingPolicyNumber(theMIExistingPolicy);
			theDeal.setMITypeId(theMIType);
			theDeal.setMortgageInsurerId(theMIInsurer);
			// SEAN DJ SPEC-Progress Advance Type July 22, 2005: update.
			theDeal.setProgressAdvanceTypeId(theProgressAdvanceTypeId);
			// SEAN DJ SPEC-PAT END
			theDeal.setMIUpfront(theMIUpfront);
			theDeal.setMIPayorId(theMIPayor);
			theDeal.setMIComments(theMIComments);

			//MI additions
			theDeal.setProgressAdvanceInspectionBy(theProgressAdvanceInspectionBy);
			theDeal.setSelfDirectedRRSP(theSelfDirectedRRSP);
			theDeal.setRequestStandardService(theRequestStandardService);
			theDeal.setLocRepaymentTypeId(theLOCRepaymentType);
			theDeal.setCmhcProductTrackerIdentifier(theCMHCProdTrackerId);


			logger.debug("MIH@updateForMortgageInsurance::MIPremium_BeforeDealSetting: " + theMIPremium);
			theDeal.setMIPremiumAmount(theMIPremium);

			// Additional fields for CMHN modifications -- By BILLY 12Feb2002
			// Changed txMIPreQCertNum ==> stMIPreQCertNum ViewOnly -- By BILLY 08April2002
			//--BMO_MI_CR--//
			// Changed back for BMO (see below).
			//theDeal.setPreQualificationMICertNum(theMIPreQCertNum);
			//=============================================================================
			theDeal.setMIRUIntervention(rbMIRUIntervention);

			//===================================================================
			// ZR:NOTE: this trace line has been added so we can monitor the amount
			// of MI premium.
			//===================================================================
			logger.debug("Setting the MI premium amount from MorgageInsHandler :  Deal ID is : " + theDealId + " and copyId is :" + theCopyId + " and MI Premium Amount is : " + theMIPremium);

			//===============================================================
			// New fields to handle MIPolicyNum problem :
			//  the number lost if user cancel MI and re-send again later.
			// -- Set the MIPolicyNo based on the Insurer the user picked
			// -- Modified by Billy 18July2003
			if(theDeal.getMortgageInsurerId() == Sc.MI_INSURER_CMHC) {
				//--> Set to CMHC Policy Number
				theDeal.setMIPolicyNumber(theDeal.getMIPolicyNumCMHC());
			} else if(theDeal.getMortgageInsurerId() == Sc.MI_INSURER_GE) {
				//--> Set to GE Policy Number
				theDeal.setMIPolicyNumber(theDeal.getMIPolicyNumGE());
			}
			else if(theDeal.getMortgageInsurerId() == Sc.MI_INSURER_AIG_UG) {
				theDeal.setMIPolicyNumber(theDeal.getMIPolicyNumAIGUG());
			}
			else if(theDeal.getMortgageInsurerId() == Sc.MI_INSURER_PMI) {
				theDeal.setMIPolicyNumber(theDeal.getMIPolicyNumPMI());
			}

			theDeal.ejbStore();
			dcm.calc();
			srk.commitTransaction();
			return true;
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Exception @MorgageInsHandler.updateForMortgageInsurance()", e);
			setStandardFailMessage();
			return false;
		}

	}


	public void handleCustomActMessageOk(String[] args)
	{
		logger.trace("--T--> MIH@handleCustomActMessageOk: " +((args != null && args [ 0 ] != null) ? args [ 0 ] : "No args"));

		ViewBean thePage = getCurrNDPage();

		PageEntry pg = theSessionState.getCurrentPage();
		SessionResourceKit srk = getSessionResourceKit();
		CalcMonitor dcm = CalcMonitor.getMonitor(srk);

		//--CervusPhaseII--start--//
		//ViewBean thePage = getCurrNDPage();
		if (args [ 0 ].equals("OVERRIDE_DEAL_LOCK_YES"))
		{
			logger.trace("MIH@handleCustomActMessageOk_REJECT_COND_YES: Override deal lock");
			provideOverrideDealLockActivity(pg, srk, thePage);
			return;
		}

		if (args [ 0 ].equals("OVERRIDE_DEAL_LOCK_NO"))
		{
			logger.trace("MIH@handleCustomActMessageOk_REJECT_COND_NO: Return to previous screen");

			theSessionState.setActMessage(null);
			theSessionState.overrideDealLock(true);
			handleCancelStandard(false);

			return;
		}
		//--CervusPhaseII--end--//

		try
		{
			Deal theDeal = new Deal(srk, dcm, pg.getPageDealId(), pg.getPageDealCID());
			MIProcessHandler theMIProcess = MIProcessHandler.getInstance();

			// Check if comming from UW screen (i.e. PageCondition1 == true)
			//    then populate the ScenarioNumber
			// Otherwise pass 'R' -- inform to use the Recommended ScenarioNumber
			String theSNumber = "R";
			if (pg.getPageCondition1())
			{
				// Propulate the current ScenarioNumber
				theSNumber = theDeal.getScenarioNumber() + "";
			}
			logger.debug("BILLY ==> The ScenarioNumber passing to MIProcessHandler = " + theSNumber);
			logger.debug("BILLY ==> @handleCustomActMessageOk Args[0] = " + args [ 0 ]);

			if (args [ 0 ].equals(MI_REVIEW_OK_TO_NEXT_SCN))
			{
				// User pressed OK on MI Status Confirm (Next Screen) Dialog
				//  -- Display Next Screen
				srk.beginTransaction();
				standardAdoptTxCopy(pg, true);

				// ensure mi status is blank for manual MI
				Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
				if (deal.getMIStatusId() != Mc.MI_STATUS_BLANK)
				{
					if (theMIProcess.isManualMIType(deal))
					{
						deal.setMIStatusId(Mc.MI_STATUS_BLANK);
						//temp
						logger.debug("MIH@MI_REVIEW_OK_TO_NEXT_SCN::stamp: set to blank");
						deal.ejbStore();
					}
				}
				srk.commitTransaction();
				navigateToNextPage(true);
				return;
			}
			if (args [ 0 ].equals(MI_REVIEW_OK_TO_PLACE_REQUEST))
			{
				// User pressed OK on MI Status Confirm (Place Request) Dialog
				// 5.0 MI removed code for CMHC/GE
				return;
			}
			if (args [ 0 ].equals(MI_REVIEW_OK_TO_CANCEL_REQUEST))
			{
				// User pressed OK on MI Status Confirm (Cancel Request) Dialog
				//  -- run Cancel MI request and display next screen
				// Check if action allowed
				CCM ccm = new CCM(srk);
				if (! ccm.checkIfActionAllowed(theDeal))
				{
					setActiveMessageToAlert(BXResources.getSysMsg(
							"CCM_ACTION_NOT_ALLOWED_MI_CHECK_FAILED", 
							theSessionState.getLanguageId()),
							ActiveMsgFactory.ISCUSTOMCONFIRM);
					return;
				}
				// 5.0 MI removed code for CMHC/GE
				return;
			}
			//4.3GR, Update Total Loan Amount -- start
			if (args [ 0 ].equals(MI_PREMIUM_UPDATED))
			{
				//this method is to called by only UW and DE screen.
				theSessionState.setActMessage(null);
				handleOKOnAMLForUpdatedMIPremium();
				return;
			}
			//4.3GR, Update Total Loan Amount -- end
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Exception encountered @MorgageInsHandler.handleCustomActMessageOk()", e);
			setStandardFailMessage();
		}

	}


	private void getRecommendMIStatusAndDisplayNextDialog(MIProcessHandler theMIProcess, Deal theDeal, SessionResourceKit srk)
	throws Exception
	{
		int theRcmStatus = 0; // default hard-stop (see below)

		theRcmStatus = theMIProcess.determineRcmCommStatus(theDeal, srk);
		logger.debug("VLAD ==>InsideBXP: " + theRcmStatus);

		// Check for the returned status and pop up Dialog box based on it
		switch(theRcmStatus)
		{
		case MIProcessHandler.C_I_CHANGE_NOT_AVAILABLE :
			setActiveMessageToAlert(BXResources.getSysMsg("MI_REVIEW_RCMSTATUS_CHANGE_REQ_NOT_AVALIABLE_MSG", theSessionState.getLanguageId()),
					ActiveMsgFactory.ISCUSTOMCONFIRM);
			break;
		case MIProcessHandler.C_I_NOT_AVAILABLE :
			setActiveMessageToAlert(BXResources.getSysMsg("MI_REVIEW_RCMSTATUS_NOT_AVALIABLE_MSG", theSessionState.getLanguageId()),
					ActiveMsgFactory.ISCUSTOMDIALOG, MI_REVIEW_OK_TO_NEXT_SCN);
			break;
		case MIProcessHandler.C_I_NOT_AVAILABLE_OPEN_REQUEST :
			setActiveMessageToAlert(BXResources.getSysMsg("MI_REVIEW_RCMSTATUS_NOT_AVALIABLE_OPEN_REQ_MSG", theSessionState.getLanguageId()),
					ActiveMsgFactory.ISCUSTOMDIALOG_MI_REQUEST, MI_REVIEW_OK_TO_CANCEL_REQUEST);
			break;
		case MIProcessHandler.C_I_NOT_REQUIRED :
			setActiveMessageToAlert(BXResources.getSysMsg("MI_REVIEW_RCMSTATUS_NOT_REQUIRED_MSG", theSessionState.getLanguageId()),
					ActiveMsgFactory.ISCUSTOMDIALOG, MI_REVIEW_OK_TO_NEXT_SCN);
			break;
		case MIProcessHandler.C_I_NOT_REQUIRED_OPEN_REQUEST :
			setActiveMessageToAlert(BXResources.getSysMsg("MI_REVIEW_RCMSTATUS_NOT_REQUIRED_OPEN_REQ_MSG", theSessionState.getLanguageId()),
					ActiveMsgFactory.ISCUSTOMDIALOG_MI_REQUEST, MI_REVIEW_OK_TO_CANCEL_REQUEST);
			break;
		case MIProcessHandler.C_I_NOT_ENOUGH_INFORMATION :
			setActiveMessageToAlert(BXResources.getSysMsg("MI_REVIEW_RCMSTATUS_NOT_ENOUGH_INFO_MSG", theSessionState.getLanguageId()),
					ActiveMsgFactory.ISCUSTOMDIALOG, MI_REVIEW_OK_TO_NEXT_SCN);
			break;
		case MIProcessHandler.C_I_INITIAL_REQUEST_PENDING :
			setActiveMessageToAlert(BXResources.getSysMsg("MI_REVIEW_RCMSTATUS_INIT_REQ_PENDING_MSG", theSessionState.getLanguageId()),
					ActiveMsgFactory.ISCUSTOMDIALOG_MI_REQUEST, MI_REVIEW_OK_TO_PLACE_REQUEST);
			break;
		case MIProcessHandler.C_I_ERRORS_CORRECTED_AND_PENDING :
		case MIProcessHandler.C_I_ERRORS_CORRECTED_AND_PENDING_OMIRF_M :
			setActiveMessageToAlert(BXResources.getSysMsg("MI_REVIEW_RCMSTATUS_ERROR_CORR_PENDING_MSG", theSessionState.getLanguageId()),
					ActiveMsgFactory.ISCUSTOMDIALOG_MI_REQUEST, MI_REVIEW_OK_TO_PLACE_REQUEST);
			break;
		case MIProcessHandler.C_I_CHANGES_PENDING :
			setActiveMessageToAlert(BXResources.getSysMsg("MI_REVIEW_RCMSTATUS_CHANGE_PENDING_MSG", theSessionState.getLanguageId()),
					ActiveMsgFactory.ISCUSTOMDIALOG_MI_REQUEST, MI_REVIEW_OK_TO_PLACE_REQUEST);
			break;
		default :
			// Hard stop -- most likely problem exist !!
			setStandardFailMessage();
			break;
		}

	}


	// Method to check if allowed Deal Modification for PostDecision
	public boolean displayToDealModButton()
	{

		// Check if review Only
		if (displaySubmitButton() == false)
			return false;


		// Othwise, displayed if deal status category = Post-Decision
		PageEntry pg = theSessionState.getCurrentPage();

		Deal deal = null;

		try
		{
			deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
			DealStatusManager dsm = DealStatusManager.getInstance(srk);
			if (dsm.getStatusCategory(deal.getStatusId(), srk) == Mc.DEAL_STATUS_CATEGORY_POST_DECISION)
				return true;
		}
		catch(Exception e)
		{
			return false;

			// should never happen ...
		}

		return false;

	}


	public void handleToDealMod()
	{
		PageEntry pg = theSessionState.getCurrentPage();


		// Save changes MI data into deal table
		if (updateForMortgageInsurance(pg))
		{
			// Unlock the deal
			try
			{
				(new DecisionModificationHandler(this)).perfromDecisionModificationRequest(Mc.DM_NON_CRITICAL_NO_DOC_PREP, pg, srk, false);

				// Set Sub-page to Deal Modification
				PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_DEAL_MODIFICATION);

				// Make sure not to display Post-Decision Dailogue
				pgEntry.setPageCondition4(true);

				// Set to run MI rules when submit
				pgEntry.setPageCondition6(true);

				getSavedPages().setNextPage(pgEntry);

				// Debug by BILLY -- need to set the Tx copy level to 1, otherwise, the Tx copy will lost after submit
				//  when retruning from DealMod -- BILLY 08Dec2001
				if (pg.getTxCopyLevel() == - 1)
				{
					pg.setTxCopyLevel(1);
				}
			}
			catch(Exception e)
			{
				logger.error("Exception @MorgageInsHandler.handleToDealMod()", e);
				setStandardFailMessage();
			}
		}


		// if no message set (no eror encountered) then away we go ...
		////if (getTheSession().isActMessageSet() == false)
		if (theSessionState.isActMessageSet() == false)
		{
			navigateToNextPage(true);
		}

	}

	// ***** Change by NBC Impl. Team - Bug#1679 - Start *****//
	/**
	 * isForceProgessAdvance 
	 * Method used to verify if ForceProgressAdvance flag is enabled/disabled
	 * @param srk SessionResourceKit object to be used for logging
	 * @return boolean : true if the property is set to 'Y' else false
	 */	
	public boolean isForceProgessAdvance(SessionResourceKit srk) {
		boolean returnVal = false;
		try {
			//			PropertiesCache.addPropertiesFile(Config
			//					.getProperty("SYS_PROPERTIES_LOCATION")
			//					+ "mossys.properties");
			if (PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(),
					"com.basis100.system.forceprogressadvancetype", "N").trim().equalsIgnoreCase("Y")) {
				returnVal = true;
			}
		} catch (Exception e) {
			logger.error("MIH@isForceProgressAdvance cannot read mossys properties" + e.getMessage(), e);
		}
		return returnVal;
	}
	// ***** Change by NBC Impl. Team - Bug#1679 - End *****//



	/**
	 * Triger workflow when leaving the MI screen
	 * ticket: FXP20282 
	 */
	public void handleWorkFlow() {
		PageEntry pg = theSessionState.getCurrentPage();
		try {
			srk.beginTransaction();
			workflowTrigger(2, srk, pg, null);
			srk.commitTransaction();
		} catch (Exception e) {
			srk.freeResources();
			logger.error("Exception @MorgageInsHandler.handleWorkflow()", e);
		}
	}


	/**
	 * read int value from combo box on the screen
	 * 
	 * @param thePage
	 * @param fieldName
	 * @return selected value
	 * @since 5.0
	 */
	private int readIntValueFromCombobox(ViewBean thePage, String fieldName) {

		int value = 0;
		ComboBox combobox = (ComboBox) thePage.getDisplayField(fieldName);
		String strValue = combobox.getValue().toString();

		try {
			value = Integer.parseInt(strValue);
		} catch (NumberFormatException e) {
			logger.error("failed to parse value: " + strValue + " for the field: " + fieldName, e);
		}

		return value;
	}

	/**
	 * checks if the operation of submit button can proceed to save data and
	 * show next AML. the purpose of checking this is that when MIIndicator is 4
	 * or 5, it is not real MI, but it is assessment. for assessment,
	 * MIProviders doesn't require follow-up cancel request. so, if MIIndicator
	 * is already 4 or 5, and if user is trying to send cancel, this method will
	 * return true. when this method returns true, Express should say
	 * "you don't have to do that" kind of thing.
	 * 
	 * @return true - Express has to stop users from sending MI request
	 * @since 5.0
	 */
	private boolean needToStopSendingCancel(Deal deal) {

		//this check is only for CMHC/GE for now
		if(deal.getMortgageInsurerId() == Mc.MI_INSURER_AIGUG) return false;

		//when user is trying to send a cancel request, check if the action is allowed
		/*if (deal.getMIIndicatorId() != Mc.MI_INDICATOR_NOT_REQUIRED 
				&& deal.getMIIndicatorId() != Mc.MI_INDICATOR_UW_WAIVED){
			//this is not cancel request, OK to go on.
			return false;
		}*/
		/*
		//QC-Ticket-490-Start
		if (deal.getMIStatusId() == Mc.MI_STATUS_BLANK ||
				deal.getMIStatusId() == Mc.MI_STATUS_INITIAL_REQUEST_PENDING ||
				deal.getMIStatusId() == Mc.MI_STATUS_INITIAL_REQUEST_SUBMITTED) {
			return false;
		}
		//QC-Ticket-490-End		
*/
		if (!(deal.getMIIndicatorId() == Mc.MI_INDICATOR_UW_WAIVED || deal.getMIIndicatorId() == Mc.MI_INDICATOR_NOT_REQUIRED))
			return false;
		
		/**
		 * <pre>
		 * [Action allowed?]
		 * 
		 * Horizontal: MI Status
		 * Vertical  : previousMIIndicatorId
		 * 
		 *     0   3   4   5   6   9   10  11  12  13  14  15  16  17  18  19  20  23
		 * 0   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
		 * 1   -   -   Y   N   Y   N   Y   N   N   N   N   Y   Y   N   N   N   Y   Y
		 * 2   -   N   Y   N   Y   N   Y   N   N   N   N   Y   Y   N   N   N   Y   Y
		 * 3   N   N   N   N   N   N   N   N   N   N   N   Y   Y   N   N   N   N   N
		 * 4   N   N   N   N   N   N   N   N   N   N   N   N   N   N   N   N   N   N
		 * 5   N   N   N   N   N   N   N   N   N   N   N   N   N   N   N   N   N   N
		 * </pre>
		 */

		//if previous MI indicatorid is 4, 5, always stop it.
		switch (deal.getPreviousMIIndicatorId()) {
		case MI_INDICATOR_NOT_REQUIRED:// 0
			//NOT OK to send cancel
			return true;
		case MI_INDICATOR_REQUIRED_STD_GUIDELINES: // 1
		case MI_INDICATOR_UW_REQUIRED: // 2

			switch (deal.getMIStatusId()) {
			case MI_STATUS_CRITICAL_ERRORS_FOUND_BY_INSURER://4
			case MI_STATUS_ERRORS_RECEIVED_BY_INSURER: //6
			case MI_STATUS_MANUAL_UNDERWRITING_REQUIRED: //10
			case MI_STATUS_APPROVAL_RECEIVED: //15
			case MI_STATUS_APPROVED: //16
			case MI_STATUS_RISKED: //20
			case MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED: //23
			case MI_STATUS_INITIAL_REQUEST_SUBMITTED: // 2
			case MI_STATUS_INITIAL_REQUEST_PENDING: // 1
				//OK to send cancel
				return false;
			default:
				//NOT OK to send cancel
				return true;
			}
		case MI_INDICATOR_UW_WAIVED: // 3

			switch (deal.getMIStatusId()) {
			case MI_STATUS_APPROVAL_RECEIVED: //15
			case MI_STATUS_APPROVED: //16
				//OK to send cancel
				return false;
			default:
				//NOT OK to send cancel
				return true;
			}
		case MI_INDICATOR_APPLICATION_FOR_LOAN_ASSESSMENT:// 4
		case MI_INDICATOR_PRE_QUALIFICATION:// 5
		default:
			//NOT OK to send cancel
			return true;
		}
	}

}

