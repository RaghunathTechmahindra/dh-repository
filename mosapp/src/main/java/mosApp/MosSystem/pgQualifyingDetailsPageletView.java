package mosApp.MosSystem;

import java.util.ArrayList;
import java.util.List;


import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.ContainerView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBeanBase;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.html.StaticTextField;

/**
 * <p> Title: pgQualifyingDetailsPageletView </p>
 *
 * <p> Description: This view bean used to display Qualifying Details from POS </p>
 *
 * @author MCM Impl team
 * @version 1.0 06-JUN-2008 XS_2.13 Initial Version 
 * @version 1.1 12-JUN-2008 XS_16.9 
 * - Added DEFAULT_DISPLAY_URL_DEALSUMMARY,pgQualifyingDetailsPageletView Constructor for DealSummary
 * -
 */
public class pgQualifyingDetailsPageletView extends ViewBeanBase implements
ContainerView 
{
    protected String DEFAULT_DISPLAY_URL = "/mosApp/MosSystem/pgQualifyingDetailsPagelet.jsp";
    protected String DEFAULT_DISPLAY_URL_DEALSUMMARY = "/mosApp/MosSystem/pgQualifyingDetailsPageletSummary.jsp";

    public static final String CHILD_STRATE = "stRate";
    public static final String CHILD_STRATE_RESET_VALUE = "";

    public static final String CHILD_STCOMPOUNDINGPERIOD = "stCompoundingPeriod";
    public static final String CHILD_STCOMPOUNDINGPERIOD_RESET_VALUE = "";

    public static final String CHILD_STAMORTIZATIONYRS = "stAmortizationYrs";
    public static final String CHILD_STAMORTIZATIONYRS_RESET_VALUE = "";

    public static final String CHILD_STAMORTIZATIONMTHS = "stAmortizationMths";
    public static final String CHILD_STAMORTIZATIONMTHS_RESET_VALUE = "";

    public static final String CHILD_STREPAYMENTTYPE = "stRepaymentType";
    public static final String CHILD_STREPAYMENTTYPE_RESET_VALUE = "";

    public static final String CHILD_STPAYMENT = "stPayment";
    public static final String CHILD_STPAYMENT_RESET_VALUE = "";

    public static final String CHILD_STGDS = "stGDS";
    public static final String CHILD_STGDS_RESET_VALUE = "";

    public static final String CHILD_STTDS = "stTDS";
    public static final String CHILD_STTDS_RESET_VALUE = "";

    public static final String CHILD_STQUALIFYINGDETAILSSECTIONSTART = "stQualifyingDetailsSectionStart";
    public static final String CHILD_STQUALIFYINGDETAILSSECTIONSTART_RESET_VALUE = "";

    public static final String CHILD_STQUALIFYINGDETAILSSECTIONEND = "stQualifyingDetailsSectionEnd";
    public static final String CHILD_STQUALIFYINGDETAILSSECTIONEND_RESET_VALUE = "";

    public static final String CHILD_STQUALIFYINGDETAILSFIELDSSTART = "stQualifyingDetailsFieldsStart";
    public static final String CHILD_STQUALIFYINGDETAILSFIELDSSTART_RESET_VALUE = "";

    public static final String CHILD_STQUALIFYINGDETAILSFIELDSEND = "stQualifyingDetailsFieldsEnd";
    public static final String CHILD_STQUALIFYINGDETAILSFIELDSEND_RESET_VALUE = "";

    public static final String CHILD_STQUALIFYINGDETAILSMESSAGESTART = "stQualifyingDetailsMessageStart";
    public static final String CHILD_STQUALIFYINGDETAILSMESSAGESTART_RESET_VALUE = "";

    public static final String CHILD_STQUALIFYINGDETAILSMESSAGEEND = "stQualifyingDetailsMessageEnd";
    public static final String CHILD_STQUALIFYINGDETAILSMESSAGEEND_RESET_VALUE = "";



    private doQualifyingDetailsModel qualifyingDetailsModel = null;
    private QualifyingDetailsHandler handler = new QualifyingDetailsHandler();

    private DealHandlerCommon parentDealHandler = null;
    private PageHandlerCommon parentPageHandler = null;

    /**
     * <p>Description:This constructor, sets the page name, registers the
     * children and sets the default URL.</p>
     *
     * @version 1.0 XS_2.13 Initial Version
     *  
     */
    public pgQualifyingDetailsPageletView(View parent, String name, DealHandlerCommon parentHandler)
    {
        super(parent, name);

        setDefaultDisplayURL(DEFAULT_DISPLAY_URL);

        this.parentDealHandler = parentHandler;

        registerChildren();

        initialize();
    } 

    /**
     * <p>Description:This constructor, sets the page name, registers the
     * children and sets the default URL.</p>
     *
     * @version 1.1 XS_16.9 Initial Version
     *  
     */
    public pgQualifyingDetailsPageletView(View parent, String name, PageHandlerCommon parentHandler)
    {
        super(parent, name);

        setDefaultDisplayURL(DEFAULT_DISPLAY_URL_DEALSUMMARY);

        this.parentPageHandler = parentHandler;

        registerChildren();

        initialize();
    } 

    /**
     * <p>Description: This Constructor does nothing.</p>
     *
     * @version 1.0 XS_2.13 Initial Version
     *  
     */
    protected void initialize() {
    }

    /**
     * <p>Description: This method resets the children which are fields on the pgQualifyingDetailsPagelet.jsp
     * @param
     * @return void
     * @version 1.0 06-JUN-2008 XS_2.13 Initial Version
     */
    public void resetChildren() {

        getStRate().setValue(CHILD_STRATE_RESET_VALUE);
        getStCompoundingPeriod().setValue(CHILD_STCOMPOUNDINGPERIOD_RESET_VALUE);
        getStAmortizationYrs().setValue(CHILD_STAMORTIZATIONYRS_RESET_VALUE);
        getStAmortizationMths().setValue(CHILD_STAMORTIZATIONMTHS_RESET_VALUE);
        getStRepaymentType().setValue(CHILD_STREPAYMENTTYPE_RESET_VALUE);
        getStPayment().setValue(CHILD_STPAYMENT_RESET_VALUE);
        getStGDS().setValue(CHILD_STGDS_RESET_VALUE);
        getStTDS().setValue(CHILD_STTDS_RESET_VALUE);

        getStQualifyingDetailsSectionStart().setValue(CHILD_STQUALIFYINGDETAILSSECTIONSTART_RESET_VALUE);
        getStQualifyingDetailsSectionEnd().setValue(CHILD_STQUALIFYINGDETAILSSECTIONEND_RESET_VALUE);
        getStQualifyingDetailsFieldsStart().setValue(CHILD_STQUALIFYINGDETAILSFIELDSSTART_RESET_VALUE);
        getStQualifyingDetailsFieldsEnd().setValue(CHILD_STQUALIFYINGDETAILSFIELDSEND_RESET_VALUE);
        getStQualifyingDetailsMessageStart().setValue(CHILD_STQUALIFYINGDETAILSMESSAGESTART_RESET_VALUE);
        getStQualifyingDetailsMessageEnd().setValue(CHILD_STQUALIFYINGDETAILSMESSAGEEND_RESET_VALUE);

    }

    /**
     * <p>Description: This method registers the children which are fields on the pgQualifyingDetailsPagelet.jsp
     * @param
     * @return void
     * @version 1.0 06-JUN-2008 XS_2.13 Initial Version
     */
    protected void registerChildren() {

        registerChild(CHILD_STRATE, StaticTextField.class);
        registerChild(CHILD_STCOMPOUNDINGPERIOD, StaticTextField.class);
        registerChild(CHILD_STAMORTIZATIONYRS, StaticTextField.class);
        registerChild(CHILD_STAMORTIZATIONMTHS, StaticTextField.class);
        registerChild(CHILD_STREPAYMENTTYPE, StaticTextField.class);
        registerChild(CHILD_STPAYMENT, StaticTextField.class);
        registerChild(CHILD_STGDS, StaticTextField.class);
        registerChild(CHILD_STTDS, StaticTextField.class);

        registerChild(CHILD_STQUALIFYINGDETAILSSECTIONSTART, StaticTextField.class);
        registerChild(CHILD_STQUALIFYINGDETAILSSECTIONEND, StaticTextField.class);
        registerChild(CHILD_STQUALIFYINGDETAILSFIELDSSTART, StaticTextField.class);
        registerChild(CHILD_STQUALIFYINGDETAILSFIELDSEND, StaticTextField.class);
        registerChild(CHILD_STQUALIFYINGDETAILSMESSAGESTART, StaticTextField.class);
        registerChild(CHILD_STQUALIFYINGDETAILSMESSAGEEND, StaticTextField.class);

    }

    /**
     * <p>Description: Adds all models to the model list
     * @param executionType int
     * @returns Model[] - List of models used by this viewbean
     * @version 1.0 06-JUN-2008 XS_2.13 Initial Version
     *  
     */
    public Model[] getWebActionModels(int executionType)
    {
        List modelList = new ArrayList();
        switch (executionType)
        {
        case MODEL_TYPE_RETRIEVE:
            ;
            break;

        case MODEL_TYPE_UPDATE:
            ;
            break;

        case MODEL_TYPE_DELETE:
            ;
            break;

        case MODEL_TYPE_INSERT:
            ;
            break;

        case MODEL_TYPE_EXECUTE:
            ;
            break;
        }
        return (Model[]) modelList.toArray(new Model[0]);
    }

    /**
     * <p>Description: This method creates the handler objects and saves the
     * page state</p>
     * @param DisplayEvent event
     * @exception ModelControlException
     * @version 1.0 06-JUN-2008 XS_2.13 Initial Version
     *  
     */
    public void beginDisplay(DisplayEvent event) throws ModelControlException {

        QualifyingDetailsHandler handler = (QualifyingDetailsHandler) this.handler.cloneSS();
        handler.pageGetState(this.getParentViewBean());

        handler.setupBeforePageGeneration();
        super.beginDisplay(event);
        handler.pageSaveState();

    }


    /**
     * <p>
     * Description: This method does preparation for the page by calling
     * beforeModelExecutes() of super class.
     * </p>
     * @version 1.0 06-JUN-2008 XS_2.13 Initial Version
     */
    public boolean beforeModelExecutes(Model model, int executionContext)
    {
        return super.beforeModelExecutes(model, executionContext);
    }

    /**
     * <p>
     * Description: This method calls the afterModelExecutes method of the super
     * class.
     * </p>
     * @version 1.0 06-JUN-2008 XS_2.13 Initial Version
     */
    public void afterModelExecutes(Model model, int executionContext)
    {

        super.afterModelExecutes(model, executionContext);

    }

    /**
     * <p>
     * Description: This method displays the fields on the page after executing
     * the model.
     * </p>
     * @version 1.0 06-JUN-2008 XS_2.13 Initial Version
     */
    public void afterAllModelsExecute(int executionContext)
    {
        super.afterAllModelsExecute(executionContext);
    }
    /**
     * <p>Description: This method calls the onModelError method of the super class.
     *
     * @params model - Model that called this method
     * @params executionContext - integer that represents the context
     * @params exception - ModelControlException that is thrown from the model
     *
     * @version 1.0 06-JUN-2008 XS_2.13 Initial Version
     */
    public void onModelError(Model model, int executionContext,
            ModelControlException exception) throws ModelControlException
            {

        super.onModelError(model, executionContext, exception);

            }

    /**
     * <p>Description: Creates the children represented by fields on the pgQualifyingDetailsPagelet.jsp.jsp</p>
     *
     * @param name - String name of the child 
     * @return View
     * @version 1.0 06-JUN-2008 XS_2.13 Initial Version
     */
    protected View createChild(String name) {
        if (name.equals(CHILD_STRATE))
        {
            StaticTextField child = new StaticTextField(
                    this,
                    getdoQualifyingDetailsModel(),
                    CHILD_STRATE,
                    doQualifyingDetailsModel.FIELD_DFQUALIFYRATE,
                    CHILD_STRATE_RESET_VALUE,
                    null);

            return child;
        }
        else
            if (name.equals(CHILD_STCOMPOUNDINGPERIOD))
            {
                StaticTextField child = new StaticTextField(
                        this,
                        getdoQualifyingDetailsModel(),
                        CHILD_STCOMPOUNDINGPERIOD,
                        doQualifyingDetailsModel.FIELD_DFINTERESTCOMPOUNDINGID,
                        CHILD_STCOMPOUNDINGPERIOD_RESET_VALUE,
                        null);

                return child;
            }
            else
                if (name.equals(CHILD_STAMORTIZATIONYRS))
                {
                    StaticTextField child = new StaticTextField(this,
                            getDefaultModel(),
                            CHILD_STAMORTIZATIONYRS,
                            CHILD_STAMORTIZATIONYRS,
                            CHILD_STAMORTIZATIONYRS_RESET_VALUE,
                            null);
                    return child;
                }
                else
                    if (name.equals(CHILD_STAMORTIZATIONMTHS))
                    {
                        StaticTextField child = new StaticTextField(this,
                                getDefaultModel(),
                                CHILD_STAMORTIZATIONMTHS,
                                CHILD_STAMORTIZATIONMTHS,
                                CHILD_STAMORTIZATIONMTHS_RESET_VALUE,
                                null);
                        return child;
                    }
                    else
                        if (name.equals(CHILD_STREPAYMENTTYPE))
                        {
                            StaticTextField child = new StaticTextField(
                                    this,
                                    getdoQualifyingDetailsModel(),
                                    CHILD_STREPAYMENTTYPE,
                                    doQualifyingDetailsModel.FIELD_DFREPAYMENTTYPEID,
                                    CHILD_STREPAYMENTTYPE_RESET_VALUE,
                                    null);

                            return child;
                        }
                        else
                            if (name.equals(CHILD_STPAYMENT))
                            {
                                StaticTextField child = new StaticTextField(
                                        this,
                                        getdoQualifyingDetailsModel(),
                                        CHILD_STPAYMENT,
                                        doQualifyingDetailsModel.FIELD_DFPANDIPAYMENTAMOUNTQUALIFY,
                                        CHILD_STPAYMENT_RESET_VALUE,
                                        null);

                                return child;
                            }
                            else
                                if (name
                                        .equals(CHILD_STGDS))
                                {
                                    StaticTextField child = new StaticTextField(
                                            this,
                                            getdoQualifyingDetailsModel(),
                                            CHILD_STGDS,
                                            doQualifyingDetailsModel.FIELD_DFQUALIFYGDS,
                                            CHILD_STGDS_RESET_VALUE,
                                            null);

                                    return child;
                                }
                                else
                                    if (name.equals(CHILD_STTDS))
                                    {
                                        StaticTextField child = new StaticTextField(
                                                this,
                                                getdoQualifyingDetailsModel(),
                                                CHILD_STTDS,
                                                doQualifyingDetailsModel.FIELD_DFQUALIFYTDS,
                                                CHILD_STTDS_RESET_VALUE,
                                                null);

                                        return child;
                                    }
                                    else
                                        if (name.equals(CHILD_STQUALIFYINGDETAILSSECTIONSTART))
                                        {
                                            StaticTextField child = new StaticTextField(
                                                    this,
                                                    getDefaultModel(),
                                                    CHILD_STQUALIFYINGDETAILSSECTIONSTART,
                                                    CHILD_STQUALIFYINGDETAILSSECTIONSTART,
                                                    CHILD_STQUALIFYINGDETAILSSECTIONSTART_RESET_VALUE,
                                                    null);

                                            return child;
                                        }
                                        else
                                            if (name.equals(CHILD_STQUALIFYINGDETAILSSECTIONEND))
                                            {
                                                StaticTextField child = new StaticTextField(
                                                        this,
                                                        getDefaultModel(),
                                                        CHILD_STQUALIFYINGDETAILSSECTIONEND,
                                                        CHILD_STQUALIFYINGDETAILSSECTIONEND,
                                                        CHILD_STQUALIFYINGDETAILSSECTIONEND_RESET_VALUE,
                                                        null);

                                                return child;
                                            }
                                            else
                                                if (name.equals(CHILD_STQUALIFYINGDETAILSFIELDSSTART))
                                                {
                                                    StaticTextField child = new StaticTextField(
                                                            this,
                                                            getDefaultModel(),
                                                            CHILD_STQUALIFYINGDETAILSFIELDSSTART,
                                                            CHILD_STQUALIFYINGDETAILSFIELDSSTART,
                                                            CHILD_STQUALIFYINGDETAILSFIELDSSTART_RESET_VALUE,
                                                            null);

                                                    return child;
                                                }
                                                else
                                                    if (name.equals(CHILD_STQUALIFYINGDETAILSFIELDSEND))
                                                    {
                                                        StaticTextField child = new StaticTextField(
                                                                this,
                                                                getDefaultModel(),
                                                                CHILD_STQUALIFYINGDETAILSFIELDSEND,
                                                                CHILD_STQUALIFYINGDETAILSFIELDSEND,
                                                                CHILD_STQUALIFYINGDETAILSFIELDSEND_RESET_VALUE,
                                                                null);

                                                        return child;
                                                    }

                                                    else
                                                        if (name.equals(CHILD_STQUALIFYINGDETAILSMESSAGESTART))
                                                        {
                                                            StaticTextField child = new StaticTextField(
                                                                    this,
                                                                    getDefaultModel(),
                                                                    CHILD_STQUALIFYINGDETAILSMESSAGESTART,
                                                                    CHILD_STQUALIFYINGDETAILSMESSAGESTART,
                                                                    CHILD_STQUALIFYINGDETAILSMESSAGESTART_RESET_VALUE,
                                                                    null);

                                                            return child;
                                                        }
        if (name.equals(CHILD_STQUALIFYINGDETAILSMESSAGEEND))
        {
            StaticTextField child = new StaticTextField(
                    this,
                    getDefaultModel(),
                    CHILD_STQUALIFYINGDETAILSMESSAGEEND,
                    CHILD_STQUALIFYINGDETAILSMESSAGEEND,
                    CHILD_STQUALIFYINGDETAILSMESSAGEEND_RESET_VALUE,
                    null);

            return child;
        }
        else 
        {
            throw new IllegalArgumentException("Invalid child name [" + name + "]");
        }

    }

    /**
     * <p>Description: This method returns the model of type doQualifyingDetailsModel used by this class.
     * If the model has not been set, an instance is created.</p>
     *
     * @returns doQualifyingDetailsModel - the model used by this view
     *
     * @version 1.0 06-JUN-2008 XS_2.13 Initial Version
     */
    public doQualifyingDetailsModel getdoQualifyingDetailsModel() {

        if (qualifyingDetailsModel == null)
            qualifyingDetailsModel = (doQualifyingDetailsModel) getModel(doQualifyingDetailsModel.class);
        return qualifyingDetailsModel;
    }
    /**
     * <p>Description: This method sets the model of type doQualifyingDetailsModel used by this class</p>
     *
     * @param model - the doQualifyingDetailsModel model used by this view
     *
     * @version 1.0 06-JUN-2008 XS_2.13 Initial Version
     */
    public void setdoQualifyingDetailsModel(doQualifyingDetailsModel model) {
        qualifyingDetailsModel = model;
    }
    /**
     * Description: Setters and gettes for the new fields
     * @version 1.0 06-JUN-2008 XS_2.13 Initial Version
     * 
     */
    public StaticTextField getStRate()
    {
        return (StaticTextField) getChild(CHILD_STRATE);
    }


    public StaticTextField getStAmortizationYrs()
    {
        return (StaticTextField) getChild(CHILD_STAMORTIZATIONYRS);
    }
    public StaticTextField getStAmortizationMths()
    {
        return (StaticTextField) getChild(CHILD_STAMORTIZATIONMTHS);
    }

    public StaticTextField getStCompoundingPeriod()
    {
        return (StaticTextField) getChild(CHILD_STCOMPOUNDINGPERIOD);
    }

    public StaticTextField getStRepaymentType()
    {
        return (StaticTextField) getChild(CHILD_STREPAYMENTTYPE);
    }

    public StaticTextField getStPayment()
    {
        return (StaticTextField) getChild(CHILD_STPAYMENT);
    }

    public StaticTextField getStGDS()
    {
        return (StaticTextField) getChild(CHILD_STGDS);
    }


    public StaticTextField getStTDS()
    {
        return (StaticTextField) getChild(CHILD_STTDS);
    }


    public StaticTextField getStQualifyingDetailsSectionStart()
    {
        return (StaticTextField) getChild(CHILD_STQUALIFYINGDETAILSSECTIONSTART);
    }


    public StaticTextField getStQualifyingDetailsSectionEnd()
    {
        return (StaticTextField) getChild(CHILD_STQUALIFYINGDETAILSSECTIONEND);
    }


    public StaticTextField getStQualifyingDetailsFieldsStart()
    {
        return (StaticTextField) getChild(CHILD_STQUALIFYINGDETAILSFIELDSSTART);
    }

    public StaticTextField getStQualifyingDetailsFieldsEnd()
    {
        return (StaticTextField) getChild(CHILD_STQUALIFYINGDETAILSFIELDSEND);
    }


    public StaticTextField getStQualifyingDetailsMessageStart()
    {
        return (StaticTextField) getChild(CHILD_STQUALIFYINGDETAILSMESSAGESTART);
    }

    public StaticTextField getStQualifyingDetailsMessageEnd()
    {
        return (StaticTextField) getChild(CHILD_STQUALIFYINGDETAILSMESSAGEEND);
    }


}
