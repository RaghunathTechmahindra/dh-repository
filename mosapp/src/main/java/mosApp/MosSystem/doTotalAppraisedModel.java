package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doTotalAppraisedModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalPurchasePrice();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalPurchasePrice(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalActualAppraisedValue();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalActualAppraisedValue(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalEstimatedAppraisedValue();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalEstimatedAppraisedValue(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFTOTALPURCHASEPRICE="dfTotalPurchasePrice";
	public static final String FIELD_DFTOTALACTUALAPPRAISEDVALUE="dfTotalActualAppraisedValue";
	public static final String FIELD_DFTOTALESTIMATEDAPPRAISEDVALUE="dfTotalEstimatedAppraisedValue";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

