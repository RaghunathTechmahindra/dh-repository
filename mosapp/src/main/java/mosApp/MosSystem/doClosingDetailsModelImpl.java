package mosApp.MosSystem;

import java.sql.SQLException;

import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doClosingDetailsModelImpl extends QueryModelBase
    implements doClosingDetailsModel
{
    /**
     *
     *
     */
    public doClosingDetailsModelImpl()
    {
        super();
        setDataSourceName(DATA_SOURCE_NAME);

        setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

        setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

        setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

        setFieldSchema(FIELD_SCHEMA);

        initialize();

    }


    /**
     *
     *
     */
    public void initialize()
    {
    }


    ////////////////////////////////////////////////////////////////////////////////
    // NetDynamics-migrated events
    ////////////////////////////////////////////////////////////////////////////////
    /**
     *
     *
     */
    protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
        throws ModelControlException
    {

        // TODO: Migrate specific onBeforeExecute code
        return sql;

    }


    /**
     *
     *
     */
    protected void afterExecute(ModelExecutionContext context, int queryType)
        throws ModelControlException
    {
    }


    /**
     *
     *
     */
    protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
    {
    }


    /**
     *
     *
     */
    public java.math.BigDecimal getDfDealId()
    {
        return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
    }


    /**
     *
     *
     */
    public void setDfDealId(java.math.BigDecimal value)
    {
        setValue(FIELD_DFDEALID,value);
    }


    /**
     *
     *
     */
    public java.math.BigDecimal getDfCopyId()
    {
        return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
    }


    /**
     *
     *
     */
    public void setDfCopyId(java.math.BigDecimal value)
    {
        setValue(FIELD_DFCOPYID,value);
    }


    /**
     *
     *
     */
    public java.sql.Timestamp getDfEstimatedClosingDate()
    {
        return (java.sql.Timestamp)getValue(FIELD_DFESTIMATEDCLOSINGDATE);
    }


    /**
     *
     *
     */
    public void setDfEstimatedClosingDate(java.sql.Timestamp value)
    {
        setValue(FIELD_DFESTIMATEDCLOSINGDATE,value);
    }


    /**
     *
     *
     */
    public java.sql.Timestamp getDfInterestAdjustmentDate()
    {
        return (java.sql.Timestamp)getValue(FIELD_DFINTERESTADJUSTMENTDATE);
    }


    /**
     *
     *
     */
    public void setDfInterestAdjustmentDate(java.sql.Timestamp value)
    {
        setValue(FIELD_DFINTERESTADJUSTMENTDATE,value);
    }


    /**
     *
     *
     */
    public java.math.BigDecimal getDfIADNumberOfDays()
    {
        return (java.math.BigDecimal)getValue(FIELD_DFIADNUMBEROFDAYS);
    }


    /**
     *
     *
     */
    public void setDfIADNumberOfDays(java.math.BigDecimal value)
    {
        setValue(FIELD_DFIADNUMBEROFDAYS,value);
    }


    /**
     *
     *
     */
    public java.sql.Timestamp getDfFirstPaymentDate()
    {
        return (java.sql.Timestamp)getValue(FIELD_DFFIRSTPAYMENTDATE);
    }


    /**
     *
     *
     */
    public void setDfFirstPaymentDate(java.sql.Timestamp value)
    {
        setValue(FIELD_DFFIRSTPAYMENTDATE,value);
    }


    /**
     *
     *
     */
    public java.sql.Timestamp getDfFirstPaymentDateMonthly()
    {
        return (java.sql.Timestamp)getValue(FIELD_DFFIRSTPAYMENTDATEMONTHLY);
    }


    /**
     *
     *
     */
    public void setDfFirstPaymentDateMonthly(java.sql.Timestamp value)
    {
        setValue(FIELD_DFFIRSTPAYMENTDATEMONTHLY,value);
    }


    /**
     *
     *
     */
    public java.sql.Timestamp getDfMaturityDate()
    {
        return (java.sql.Timestamp)getValue(FIELD_DFMATURITYDATE);
    }


    /**
     *
     *
     */
    public void setDfMaturityDate(java.sql.Timestamp value)
    {
        setValue(FIELD_DFMATURITYDATE,value);
    }


    /**
     *
     *
     */
    public java.math.BigDecimal getDfPerDiemInterest()
    {
        return (java.math.BigDecimal)getValue(FIELD_DFPERDIEMINTEREST);
    }


    /**
     *
     *
     */
    public void setDfPerDiemInterest(java.math.BigDecimal value)
    {
        setValue(FIELD_DFPERDIEMINTEREST,value);
    }


    /**
     *
     *
     */
    public java.math.BigDecimal getDfNetInterestRate()
    {
        return (java.math.BigDecimal)getValue(FIELD_DFNETINTERESTRATE);
    }


    /**
     *
     *
     */
    public void setDfNetInterestRate(java.math.BigDecimal value)
    {
        setValue(FIELD_DFNETINTERESTRATE,value);
    }


    /**
     *
     *
     */
    public java.math.BigDecimal getDfTotalLoanBridgeAmount()
    {
        return (java.math.BigDecimal)getValue(FIELD_DFTOTALLOANBRIDGEAMOUNT);
    }


    /**
     *
     *
     */
    public void setDfTotalLoanBridgeAmount(java.math.BigDecimal value)
    {
        setValue(FIELD_DFTOTALLOANBRIDGEAMOUNT,value);
    }


    /**
     *
     *
     */
    public java.math.BigDecimal getDfIADInterest()
    {
        return (java.math.BigDecimal)getValue(FIELD_DFIADINTEREST);
    }


    /**
     *
     *
     */
    public void setDfIADInterest(java.math.BigDecimal value)
    {
        setValue(FIELD_DFIADINTEREST,value);
    }


    /**
     *
     *
     */
    public String getDfInterestCompoundDesc()
    {
        return (String)getValue(FIELD_DFINTERESTCOMPOUNDDESC);
    }


    /**
     *
     *
     */
    public void setDfInterestCompoundDesc(String value)
    {
        setValue(FIELD_DFINTERESTCOMPOUNDDESC,value);
    }


    /**
     *
     *
     */
    public java.math.BigDecimal getDfAdvanceHoldBack()
    {
        return (java.math.BigDecimal)getValue(FIELD_DFADVANCEHOLDBACK);
    }


    /**
     *
     *
     */
    public void setDfAdvanceHoldBack(java.math.BigDecimal value)
    {
        setValue(FIELD_DFADVANCEHOLDBACK,value);
    }


    /**
     *
     *
     */
    public java.math.BigDecimal getDfPaymantFreqId()
    {
        return (java.math.BigDecimal)getValue(FIELD_DFPAYMANTFREQID);
    }


    /**
     *
     *
     */
    public void setDfPaymantFreqId(java.math.BigDecimal value)
    {
        setValue(FIELD_DFPAYMANTFREQID,value);
    }




    ////////////////////////////////////////////////////////////////////////////
    // Obsolete Netdynamics Events - Require Manual Migration
    ////////////////////////////////////////////////////////////////////////////





    ////////////////////////////////////////////////////////////////////////////
    // Custom Methods - Require Manual Migration
    ////////////////////////////////////////////////////////////////////////////





    ////////////////////////////////////////////////////////////////////////////
    // Class variables
    ////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////
    // Class variables
    ////////////////////////////////////////////////////////////////////////////////

    public static final String DATA_SOURCE_NAME="jdbc/orcl";
    public static final String SELECT_SQL_TEMPLATE="SELECT ALL DEAL.DEALID, DEAL.COPYID, DEAL.ESTIMATEDCLOSINGDATE, DEAL.INTERIMINTERESTADJUSTMENTDATE, DEAL.IADNUMBEROFDAYS, DEAL.FIRSTPAYMENTDATE, DEAL.FIRSTPAYMENTDATEMONTHLY, DEAL.MATURITYDATE, DEAL.PERDIEMINTERESTAMOUNT, DEAL.NETINTERESTRATE, DEAL.TOTALLOANBRIDGEAMOUNT, DEAL.INTERIMINTERESTAMOUNT, INTERESTCOMPOUND.INTERESTCOMPOUNDDESCRIPTION, DEAL.ADVANCEHOLD, DEAL.PAYMENTFREQUENCYID FROM DEAL, LENDERPROFILE, INTERESTCOMPOUND  __WHERE__  ";
    public static final String MODIFYING_QUERY_TABLE_NAME="DEAL, LENDERPROFILE, INTERESTCOMPOUND";
    public static final String STATIC_WHERE_CRITERIA=
        " DEAL.LENDERPROFILEID  =  LENDERPROFILE.LENDERPROFILEID (+) AND LENDERPROFILE.INTERESTCOMPOUNDINGID  =  INTERESTCOMPOUND.INTERESTCOMPOUNDINGID (+)"
        + " AND DEAL.INSTITUTIONPROFILEID  =  LENDERPROFILE.INSTITUTIONPROFILEID";
    public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
    public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
    public static final String COLUMN_DFDEALID="DEALID";
    public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
    public static final String COLUMN_DFCOPYID="COPYID";
    public static final String QUALIFIED_COLUMN_DFESTIMATEDCLOSINGDATE="DEAL.ESTIMATEDCLOSINGDATE";
    public static final String COLUMN_DFESTIMATEDCLOSINGDATE="ESTIMATEDCLOSINGDATE";
    public static final String QUALIFIED_COLUMN_DFINTERESTADJUSTMENTDATE="DEAL.INTERIMINTERESTADJUSTMENTDATE";
    public static final String COLUMN_DFINTERESTADJUSTMENTDATE="INTERIMINTERESTADJUSTMENTDATE";
    public static final String QUALIFIED_COLUMN_DFIADNUMBEROFDAYS="DEAL.IADNUMBEROFDAYS";
    public static final String COLUMN_DFIADNUMBEROFDAYS="IADNUMBEROFDAYS";
    public static final String QUALIFIED_COLUMN_DFFIRSTPAYMENTDATE="DEAL.FIRSTPAYMENTDATE";
    public static final String COLUMN_DFFIRSTPAYMENTDATE="FIRSTPAYMENTDATE";
    public static final String QUALIFIED_COLUMN_DFFIRSTPAYMENTDATEMONTHLY="DEAL.FIRSTPAYMENTDATEMONTHLY";
    public static final String COLUMN_DFFIRSTPAYMENTDATEMONTHLY="FIRSTPAYMENTDATEMONTHLY";
    public static final String QUALIFIED_COLUMN_DFMATURITYDATE="DEAL.MATURITYDATE";
    public static final String COLUMN_DFMATURITYDATE="MATURITYDATE";
    public static final String QUALIFIED_COLUMN_DFPERDIEMINTEREST="DEAL.PERDIEMINTERESTAMOUNT";
    public static final String COLUMN_DFPERDIEMINTEREST="PERDIEMINTERESTAMOUNT";
    public static final String QUALIFIED_COLUMN_DFNETINTERESTRATE="DEAL.NETINTERESTRATE";
    public static final String COLUMN_DFNETINTERESTRATE="NETINTERESTRATE";
    public static final String QUALIFIED_COLUMN_DFTOTALLOANBRIDGEAMOUNT="DEAL.TOTALLOANBRIDGEAMOUNT";
    public static final String COLUMN_DFTOTALLOANBRIDGEAMOUNT="TOTALLOANBRIDGEAMOUNT";
    public static final String QUALIFIED_COLUMN_DFIADINTEREST="DEAL.INTERIMINTERESTAMOUNT";
    public static final String COLUMN_DFIADINTEREST="INTERIMINTERESTAMOUNT";
    public static final String QUALIFIED_COLUMN_DFINTERESTCOMPOUNDDESC="INTERESTCOMPOUND.INTERESTCOMPOUNDDESCRIPTION";
    public static final String COLUMN_DFINTERESTCOMPOUNDDESC="INTERESTCOMPOUNDDESCRIPTION";
    public static final String QUALIFIED_COLUMN_DFADVANCEHOLDBACK="DEAL.ADVANCEHOLD";
    public static final String COLUMN_DFADVANCEHOLDBACK="ADVANCEHOLD";
    public static final String QUALIFIED_COLUMN_DFPAYMANTFREQID="DEAL.PAYMENTFREQUENCYID";
    public static final String COLUMN_DFPAYMANTFREQID="PAYMENTFREQUENCYID";




    ////////////////////////////////////////////////////////////////////////////
    // Instance variables
    ////////////////////////////////////////////////////////////////////////////





    ////////////////////////////////////////////////////////////////////////////
    // Custom Members - Require Manual Migration
    ////////////////////////////////////////////////////////////////////////////

    static
    {

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFDEALID,
                COLUMN_DFDEALID,
                QUALIFIED_COLUMN_DFDEALID,
                java.math.BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFCOPYID,
                COLUMN_DFCOPYID,
                QUALIFIED_COLUMN_DFCOPYID,
                java.math.BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFESTIMATEDCLOSINGDATE,
                COLUMN_DFESTIMATEDCLOSINGDATE,
                QUALIFIED_COLUMN_DFESTIMATEDCLOSINGDATE,
                java.sql.Timestamp.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFINTERESTADJUSTMENTDATE,
                COLUMN_DFINTERESTADJUSTMENTDATE,
                QUALIFIED_COLUMN_DFINTERESTADJUSTMENTDATE,
                java.sql.Timestamp.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFIADNUMBEROFDAYS,
                COLUMN_DFIADNUMBEROFDAYS,
                QUALIFIED_COLUMN_DFIADNUMBEROFDAYS,
                java.math.BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFFIRSTPAYMENTDATE,
                COLUMN_DFFIRSTPAYMENTDATE,
                QUALIFIED_COLUMN_DFFIRSTPAYMENTDATE,
                java.sql.Timestamp.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFFIRSTPAYMENTDATEMONTHLY,
                COLUMN_DFFIRSTPAYMENTDATEMONTHLY,
                QUALIFIED_COLUMN_DFFIRSTPAYMENTDATEMONTHLY,
                java.sql.Timestamp.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFMATURITYDATE,
                COLUMN_DFMATURITYDATE,
                QUALIFIED_COLUMN_DFMATURITYDATE,
                java.sql.Timestamp.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFPERDIEMINTEREST,
                COLUMN_DFPERDIEMINTEREST,
                QUALIFIED_COLUMN_DFPERDIEMINTEREST,
                java.math.BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFNETINTERESTRATE,
                COLUMN_DFNETINTERESTRATE,
                QUALIFIED_COLUMN_DFNETINTERESTRATE,
                java.math.BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFTOTALLOANBRIDGEAMOUNT,
                COLUMN_DFTOTALLOANBRIDGEAMOUNT,
                QUALIFIED_COLUMN_DFTOTALLOANBRIDGEAMOUNT,
                java.math.BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFIADINTEREST,
                COLUMN_DFIADINTEREST,
                QUALIFIED_COLUMN_DFIADINTEREST,
                java.math.BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFINTERESTCOMPOUNDDESC,
                COLUMN_DFINTERESTCOMPOUNDDESC,
                QUALIFIED_COLUMN_DFINTERESTCOMPOUNDDESC,
                String.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFADVANCEHOLDBACK,
                COLUMN_DFADVANCEHOLDBACK,
                QUALIFIED_COLUMN_DFADVANCEHOLDBACK,
                java.math.BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFPAYMANTFREQID,
                COLUMN_DFPAYMANTFREQID,
                QUALIFIED_COLUMN_DFPAYMANTFREQID,
                java.math.BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

    }

}

