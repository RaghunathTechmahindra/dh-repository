package mosApp.MosSystem;

import java.io.*;
import java.math.BigDecimal;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.basis100.picklist.BXResources;

/**
 *
 *
 *
 */
public class doUserAdminGetBranchesModelImpl extends QueryModelBase
	implements doUserAdminGetBranchesModel
{
	/**
	 *
	 *
	 */
	public doUserAdminGetBranchesModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 06Dec2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert Branch name.
        this.setDfBranchName(BXResources.getPickListDescription(this.getDfInstitutionProfileId().intValue(),
                "BRANCHPROFILE", this.getDfBranchProfileId().intValue(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBranchProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBRANCHPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfBranchProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBRANCHPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBranchName()
	{
		return (String)getValue(FIELD_DFBRANCHNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfBranchName(String value)
	{
		setValue(FIELD_DFBRANCHNAME,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRegionProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFREGIONPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfRegionProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFREGIONPROFILEID,value);
	}



    public BigDecimal getDfInstitutionProfileId() {

        return (java.math.BigDecimal)getValue(FIELD_DFINSTITUTIONPROFILEID);
    }


    public void setDfInstitutionProfileId(BigDecimal value) {

        setValue(FIELD_DFINSTITUTIONPROFILEID, value);
    }

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL BRANCHPROFILE.BRANCHPROFILEID, BRANCHPROFILE.BRANCHNAME, "+
    "BRANCHPROFILE.INSTITUTIONPROFILEID, BRANCHPROFILE.REGIONPROFILEID "+
    "FROM BRANCHPROFILE  "+
    "__WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="BRANCHPROFILE";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBRANCHPROFILEID="BRANCHPROFILE.BRANCHPROFILEID";
	public static final String COLUMN_DFBRANCHPROFILEID="BRANCHPROFILEID";
	public static final String QUALIFIED_COLUMN_DFBRANCHNAME="BRANCHPROFILE.BRANCHNAME";
	public static final String COLUMN_DFBRANCHNAME="BRANCHNAME";
	public static final String QUALIFIED_COLUMN_DFREGIONPROFILEID="BRANCHPROFILE.REGIONPROFILEID";
	public static final String COLUMN_DFREGIONPROFILEID="REGIONPROFILEID";
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONPROFILEID="BRANCHPROFILE.INSTITUTIONPROFILEID";
    public static final String COLUMN_DFINSTITUTIONPROFILEID="INSTITUTIONPROFILEID";



	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRANCHPROFILEID,
				COLUMN_DFBRANCHPROFILEID,
				QUALIFIED_COLUMN_DFBRANCHPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRANCHNAME,
				COLUMN_DFBRANCHNAME,
				QUALIFIED_COLUMN_DFBRANCHNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREGIONPROFILEID,
				COLUMN_DFREGIONPROFILEID,
				QUALIFIED_COLUMN_DFREGIONPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFINSTITUTIONPROFILEID,
                    COLUMN_DFINSTITUTIONPROFILEID,
                    QUALIFIED_COLUMN_DFINSTITUTIONPROFILEID,
                    java.math.BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""));

	}

}

