package mosApp.MosSystem;

import java.math.BigDecimal;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 * Title: doQuickLinkModel
 * <p>
 * Description: This is the interface class for doQuickLinkModelImpl
 * used to retrieve data from quicklinks table.
 * @author 
 * @version 1.0 
 * 
 * 
 */
public interface doQuickLinkModel extends QueryModel, SelectQueryModel
{

    public static final String FIELD_DFQUICKLINKID = "dfQuickLinkId";
    public static final String FIELD_DFLINKNAMEENGLISH = "dfLinkNameEnglish";
    public static final String FIELD_DFLINKNAMEFRENCH = "dfLinkNameFrench";
    public static final String FIELD_URLLINK = "dfUrlLink";
    public static final String FIELD_DFBRANCHPROFILEID = "dfBranchProfileId";
    public static final String FIELD_DFINSTITUTIONPROFILEID = "dfInstitutionProfileId";
    public static final String FIELD_DFSORTORDER = "dfSortOrder";
    public static final String FIELD_DFFAVICON = "dfFavicon";

   
    /**
     * This method declaration returns the value of field QuickLinkId.
     * @return BigDecimal QuickLinkId from the model.
     */
    public BigDecimal getDfQuickLinkId();

    /**
     * This method declaration sets the value of field QuickLinkId
     * @param value -
     *            new value of the field
     */
    public void setDfQuickLinkId(BigDecimal value);

    /**
     * This method declaration returns the value of field LinkNameEnglish.
     * @return BigDecimal LinkNameEnglish from the model.
     */
    public String getDfLinkNameEnglish();

    /**
     * This method declaration sets the value of field LinkNameEnglish
     * @param value -
     *            new value of the field
     */
    public void setDfLinkNameEnglish(String value);
    
    /**
     * This method declaration returns the value of field LinkNameFrench.
     * @return BigDecimal LinkNameFrench from the model.
     */
    public String getDfLinkNameFrench();

    /**
     * This method declaration sets the value of field LinkNameFrench
     * @param value -
     *            new value of the field
     */
    public void setDfLinkNameFrench(String value);
    
    /**
     * This method declaration returns the value of field UrlLink.
     * @return BigDecimal UrlLink from the model.
     */
    public String getDfUrlLink();

    /**
     * This method declaration sets the value of field UrlLink
     * @param value -
     *            new value of the field
     */
    public void setDfUrlLink(String value);
    
    /**
     * This method declaration returns the value of field InstitutionId.
     * @return BigDecimal InstitutionId from the model.
     */
    public BigDecimal getDfInstitutionProfileId();

    /**
     * This method declaration sets the value of field InstitutionId
     * @param value -
     *            new InstitutionId.
     */
    public void setDfBranchProfileId(BigDecimal value);

    /**
     * This method declaration returns the value of field branchProfileId.
     * @return BigDecimal branchProfileId from the model.
     */
    public BigDecimal getDfBranchProfileId();

    /**
     * This method declaration sets the value of field branchProfileId
     * @param value -
     *            new InstitutionId.
     */
    public void setDfInstitutionProfileId(BigDecimal value);
    
    /**
     * This method declaration returns the value of field SortOrder.
     * @return BigDecimal SortOrder from the model
     */
    public BigDecimal getDfSortOrder();

    /**
     * This method declaration sets the SortOrder.
     * @param value
     *            the new SortOrder
     */
    public void setDfSortOrder(BigDecimal value);

    /**
     * This method declaration returns the value of field Favicon.
     * @return String Favicon from the model
     */
    public String getDfFavicon();

    /**
     * This method declaration sets the dfFavicon.
     * @param value
     *            the new Favicon
     */
    public void setDfFavicon(String value);

}
