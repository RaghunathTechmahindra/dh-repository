package mosApp.MosSystem;

import java.text.*;
import java.util.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.security.*;
import com.basis100.deal.pk.*;
import com.basis100.workflow.*;
import com.basis100.workflow.entity.*;
import com.basis100.workflow.pk.*;
import com.basis100.deal.calc.*;
import com.basis100.deal.util.*;
import com.basis100.deal.validation.*;
import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.jdbcservices.jdbcexecutor.*;
import com.basis100.util.date.*;
import com.basis100.picklist.BXResources;
import com.basis100.deal.docrequest.DocumentRequest;

import mosApp.*;
import MosSystem.*;

import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.html.*;
import com.iplanet.jato.view.event.*;

/**
 *
 *
 */
public class AppraisalHandlerDJ
  extends PageHandlerCommon
  implements Cloneable, Sc
{
  /**
   *
   *
   */
  public AppraisalHandlerDJ cloneSS()
  {
    return(AppraisalHandlerDJ)super.cloneSafeShallow();

  }

  /////////////////////////////////////////////////////////////////////
  //This method is used to populate the fields in the header
  //with the appropriate information

  public void populatePageDisplayFields()
  {
    PageEntry pg = getTheSessionState().getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    populatePageShellDisplayFields();
    populateTaskNavigator(pg);
    populatePageDealSummarySnapShot();
    populatePreviousPagesLinks();
    setJSValidation();
  }

  /////////////////////////////////////////////////////////////////////

  public void setJSValidation()
  {
    // Validate # of Dependants
    HtmlDisplayFieldBase df = null;
    String theExtraHtml = null;

    ViewBean thePage = getCurrNDPage();

    df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbEstimatedAppraisal");
    theExtraHtml = "onBlur=\"isFieldDecimal(2);\"";
    df.setExtraHtml(theExtraHtml);

    df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbActualAppraisal");
    theExtraHtml = "onBlur=\"isFieldDecimal(2);\"";
    
    // Add by Anna for PMI2 -- set Actual Appraisal Field as read only
	// under the condition: 1.From GoTo button/Task/Link History
	// 2.The Deal is Mortgage Insured. 3.MI type coded as Extended Payload = 'Y'
	try
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		SessionResourceKit srk = getSessionResourceKit();
		Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
		if(!(getTheSessionState().getCurrentPage().isSubPage())&&(deal.getMortgageInsurerId()!=0)&&
				(DealCalcUtil.checkIfMIPremiumIsExtended(deal)))			 
		{
			// 1. Customize color
		    String changeColorStyle = new String("style=\"background-color:d1ebff\"" + " style=\"border-bottom: hidden d1ebff 0\"" + " style=\"border-left: hidden d1ebff 0\"" + " style=\"border-right: hidden d1ebff 0\"" + " style=\"border-top: hidden d1ebff 0\"" + " style=\"font-weight: bold\"");

		    // 2. Disable edition
		    String disableEdition = new String(" disabled");
		    
		    theExtraHtml = theExtraHtml + changeColorStyle + disableEdition;
		}	
	}catch(Exception e)
	{
		logger.error("Exception @AppraisalReviewHandler.setJSValidation()");
		logger.error(e);
		setStandardFailMessage();
		return;
	}
	// End	
    df.setExtraHtml(theExtraHtml);

    // Set the compare date to today
    //  Validate Month
    df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/cbMonths");
    theExtraHtml =
      "onBlur=\"isFieldValidDate('tbYear', 'cbMonths', 'tbDay');\"";
    df.setExtraHtml(theExtraHtml);

    //  Validate Day
    df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbDay");
    theExtraHtml = "onBlur=\"if(isFieldInIntRange(1, 31)) " +
      "isFieldValidDate('tbYear', 'cbMonths', 'tbDay');\"";
    df.setExtraHtml(theExtraHtml);

    // Validate Year
    df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbYear");
    theExtraHtml = "onBlur=\"if(isFieldInIntRange(1800, 2100)) " +
      "isFieldValidDate('tbYear', 'cbMonths', 'tbDay');\"";
    df.setExtraHtml(theExtraHtml);

    //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
    //--> By Billy 22Dec2003
    // JS for Work Phone # fields
    df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactWorkPhoneNum1");
    theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
    df.setExtraHtml(theExtraHtml);

    df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactWorkPhoneNum2");
    theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
    df.setExtraHtml(theExtraHtml);

    df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactWorkPhoneNum3");
    theExtraHtml = "onBlur=\"isFieldInteger(4);\"";
    df.setExtraHtml(theExtraHtml);

    // JS for Work Phone # Extention
    df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactWorkPhoneNumExt");
    theExtraHtml = "onBlur=\"isFieldInteger(0);\"";
    df.setExtraHtml(theExtraHtml);

    // JS for Cell Phone # fields
    df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactCellPhoneNum1");
    theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
    df.setExtraHtml(theExtraHtml);

    df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactCellPhoneNum2");
    theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
    df.setExtraHtml(theExtraHtml);

    df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactCellPhoneNum3");
    theExtraHtml = "onBlur=\"isFieldInteger(4);\"";
    df.setExtraHtml(theExtraHtml);

    // JS for Cell Phone # fields
    df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactHomePhoneNum1");
    theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
    df.setExtraHtml(theExtraHtml);

    df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactHomePhoneNum2");
    theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
    df.setExtraHtml(theExtraHtml);

    df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAOContactHomePhoneNum3");
    theExtraHtml = "onBlur=\"isFieldInteger(4);\"";
    df.setExtraHtml(theExtraHtml);
    //==============================================================
  }

  /////////////////////////////////////////////////////////////////////
  //
  // Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
  //
  public void setupBeforePageGeneration()
  {
    PageEntry pg = getTheSessionState().getCurrentPage();
    if(pg.getSetupBeforeGenerationCalled() == true)
    {
      return;
    }
    pg.setSetupBeforeGenerationCalled(true);

    // -- //
    setupDealSummarySnapShotDO(pg);

    // criteria for main properties population
    QueryModelBase theDO = (QueryModelBase)
      (RequestManager.getRequestContext().getModelManager().getModel(
      doAppraisalPropertyModel.class));

    theDO.clearUserWhereCriteria();
    theDO.addUserWhereCriterion("dfDealId", "=",
      new String("" + pg.getPageDealId()));
    theDO.addUserWhereCriterion("dfCopyId", "=",
      new String("" + pg.getPageDealCID()));

    PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");
    Hashtable pst = pg.getPageStateTable();

    if(tds == null)
    {
      // first invocation!
      Vector propIds = new Vector();
      pst.put("PROPIDS", propIds);
      Vector aDates = new Vector();
      pst.put("ADATES", aDates);

      // get and store property ids, etc
      int numRows = 0;
      try
      {
        theDO.executeSelect(null);
        numRows = theDO.getSize();
        logger.debug(
          "--- AppraiserReviewHandler.setupBeforePageGeneration ---> :: numRows = " +
          numRows);
      }
      catch(Exception ex)
      {
        logger.error("Exception @AppraisalHandler.setupBeforePageGeneration - entry to page will be disallowed.");
        logger.error(ex);
        numRows = 0;
      }

      // store state info
      logger.debug(
        "--- AppraiserReviewHandler.setupBeforePageGeneration ---> :: Store State Info. **************************** ");
      java.util.Date dt = null;
      try
      {
        while(theDO.next())
        {
          propIds.addElement(theDO.getValue(doAppraisalPropertyModel.FIELD_DFPROPERTYID).toString());
          logger.debug("--- AppraisalReviewHandler ---> :: propertyid = " +
            theDO.getValue(doAppraisalPropertyModel.FIELD_DFPROPERTYID).toString());
          dt = (java.util.Date)(theDO.getValue(doAppraisalPropertyModel.FIELD_DFAPPRAISALDATE));
          aDates.addElement(dt);
          logger.debug("--- AppraisalReviewHandler ---> :: adate = " + dt);
        }
      }
      catch(Exception e)
      {
        logger.error("@AppraisalHandler..setupBeforePageGeneration : Error when getting data from Model");
        logger.error(e.toString());
      }

      String voName = "Repeated1";
      int perPage = ((TiledView)(getCurrNDPage().getChild(voName))).
        getMaxDisplayTiles();

      // set up and save task display state object
      tds = new PageCursorInfo("DEFAULT", voName, perPage, propIds.size());
      tds.setRefresh(true);
      pst.put(tds.getName(), tds);

      // setup to generate no entry (active) message when there are no properties
      if(propIds.size() <= 0)
      {
        setActiveMessageToAlert(BXResources.getSysMsg(
          "APPRAISAL_REVIEW_CANNOT_ENTER", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMLEAVEPAGE);
      }
    }

    // --- //
    theDO = (QueryModelBase)
      (RequestManager.getRequestContext().getModelManager().getModel(
      doTotalAppraisedModel.class));
    theDO.clearUserWhereCriteria();
    theDO.addUserWhereCriterion("dfDealId", "=",
      new String("" + pg.getPageDealId()));
    theDO.addUserWhereCriterion("dfCopyId", "=",
      new String("" + pg.getPageDealCID()));
  }

  /////////////////////////////////////////////////////////////////////
  public void displayAppraiser(int rowNdx)
  {
    String propertyId = new String("");
    Date appraisalDate = null;

    try
    {
      PageEntry pg = getTheSessionState().getCurrentPage();
      ViewBean currNDPage = getCurrNDPage();
      Hashtable pst = pg.getPageStateTable();
      int dealid = pg.getPageDealId();
      //int copyid = pg.getPageDealCID();
      Vector propIds = new Vector();
      Vector aDates = new Vector();

      //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
      //--> By Billy 18Dec2003
      //Set display for the Phone #s
      doAppraisalPropertyModelImpl theObj =(doAppraisalPropertyModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doAppraisalPropertyModel.class));
      setPhoneNoDisplayField(theObj,
        "Repeated1/tbAOContactWorkPhoneNum1",
        "Repeated1/tbAOContactWorkPhoneNum2",
        "Repeated1/tbAOContactWorkPhoneNum3",
        "dfAOContactWorkPhoneNum", rowNdx);
      setPhoneNoDisplayField(theObj,
        "Repeated1/tbAOContactCellPhoneNum1",
        "Repeated1/tbAOContactCellPhoneNum2",
        "Repeated1/tbAOContactCellPhoneNum3",
        "dfAOContactCellPhoneNum", rowNdx);
      setPhoneNoDisplayField(theObj,
        "Repeated1/tbAOContactHomePhoneNum1",
        "Repeated1/tbAOContactHomePhoneNum2",
        "Repeated1/tbAOContactHomePhoneNum3",
        "dfAOContactHomePhoneNum", rowNdx);
      //===================================================================

      /////////////////////////////////////////////////////
      Object obj = pst.get("PROPIDS");
      if(obj instanceof Vector)
      {
        propIds = (Vector)obj;

        // check for no row case
      }
      if(propIds.size() == 0)
      {
        return;
      }
      obj = pst.get("ADATES");
      if(obj instanceof Vector)
      {
        aDates = (Vector)obj;
      }
      logger.debug("--- AppraiserReviewHandler.displayAppraiser ---> :: rowIndex = " + rowNdx);

      /////////////////////////////////////////////////////
      //ComboBox comboBox =(ComboBox)(getCurrNDPage().getDisplayField("Repeated1/cbMonths"));
      fillupMonths("Repeated1/cbMonths");

      obj = aDates.elementAt(rowNdx);
      if(obj instanceof Date)
      {
        appraisalDate = (Date)obj;
      }
      if(appraisalDate != null)
      {
        getCurrNDPage().setDisplayFieldValue("Repeated1/cbMonths",
          new String("" +
          (appraisalDate.getMonth() + 1)));

        //--> The following is a bug -- By Billy 09Sept2002
        int year = appraisalDate.getYear() + 1900;
        //-->if (year < 30) year += 2000;
        //-->if (year > 100) year += 1900;
        //================================================

        logger.debug(
          "--- AppraiserReviewHandler.displayAppraiser ---> :: appraisalDate.year = " +
          year
          + "appraisalDate.month = " + (appraisalDate.getMonth() + 1)
          + "appraisalDate.day = " + appraisalDate.getDate());
        getCurrNDPage().setDisplayFieldValue("Repeated1/tbYear",
          new String("" + year));
        getCurrNDPage().setDisplayFieldValue("Repeated1/tbDay",
          new String("" +
          appraisalDate.getDate()));
      }
      //--> Fill the default values if appraisalDate = null
      //--> By Billy 09Sept2002
      else
      {
        logger.debug("--- AppraiserReviewHandler.displayAppraiser ---> appraisalDate = null :: Set all defaults for Date");
        getCurrNDPage().setDisplayFieldValue("Repeated1/cbMonths",
          new String("0"));
        getCurrNDPage().setDisplayFieldValue("Repeated1/tbYear", new String(""));
        getCurrNDPage().setDisplayFieldValue("Repeated1/tbDay", new String(""));
      }
      //===================================================

      QueryModelBase appraiserSelectDO = (QueryModelBase)
        (RequestManager.getRequestContext().getModelManager().getModel(
        doAppraiserSelectModel.class));
      appraiserSelectDO.clearUserWhereCriteria();
      appraiserSelectDO.addUserWhereCriterion("dfDealId", "=", new Integer(dealid));

      obj = propIds.elementAt(rowNdx);
      if(obj instanceof String)
      {
        propertyId = (String)obj;
      }
      logger.debug("--- AppraiserReviewHandler.displayAppraiser ---> :: propertyId.toString() = " + propertyId.toString());
      appraiserSelectDO.addUserWhereCriterion("dfPropertyId", "=", new String(propertyId));
      appraiserSelectDO.executeSelect(null);

      if(appraiserSelectDO.getSize() <= 0)
      {
        pst.put("APPRAISER_THIS_ROW", "N");
        logger.debug(
          "--- AppraiserReviewHandler.displayAppraiser ---> :: No records returned. ");
        String noVal = new String("");

        // Added new fields -- Billy 13March2002
        getCurrNDPage().setDisplayFieldValue("Repeated1/stAppraiserShortName",
          noVal);
        getCurrNDPage().setDisplayFieldValue("Repeated1/stAppraiserCompanyName",
          noVal);
        getCurrNDPage().setDisplayFieldValue("Repeated1/tbAppraiserNotes",
          noVal);
        //============================================================================
        getCurrNDPage().setDisplayFieldValue("Repeated1/stAppraiserName", noVal);
        getCurrNDPage().setDisplayFieldValue("Repeated1/stAppraiserAddress",
          noVal);
        getCurrNDPage().setDisplayFieldValue("Repeated1/stPhone", noVal);
        getCurrNDPage().setDisplayFieldValue("Repeated1/stFax", noVal);
        getCurrNDPage().setDisplayFieldValue("Repeated1/stEmail", noVal);
        return;

        // no appraiser currently set
      }
      logger.debug(
        "--- AppraiserReviewHandler.displayAppraiser ---> :: records returned. ");
      pst.put("APPRAISER_THIS_ROW", "Y");

      // Added new fields -- Billy 13March2002
      getCurrNDPage().setDisplayFieldValue("Repeated1/stAppraiserShortName",
        appraiserSelectDO.getValue(
        "dfAppraiserShortName"));
      getCurrNDPage().setDisplayFieldValue("Repeated1/stAppraiserCompanyName",
        appraiserSelectDO.getValue(
        "dfAppraiserCompanyName"));
      getCurrNDPage().setDisplayFieldValue("Repeated1/tbAppraiserNotes",
        appraiserSelectDO.getValue(
        "dfAppraiserNotes"));
      //============================================================================
      getCurrNDPage().setDisplayFieldValue("Repeated1/stAppraiserAddress",
        appraiserSelectDO.getValue(
        "dfAppraiserAddress"));
      getCurrNDPage().setDisplayFieldValue("Repeated1/stFax",
        appraiserSelectDO.getValue("dfFax"));
      getCurrNDPage().setDisplayFieldValue("Repeated1/stEmail",
        appraiserSelectDO.getValue("dfEmail"));
      getCurrNDPage().setDisplayFieldValue("Repeated1/stAppraiserName",
        appraiserSelectDO.getValue(
        "dfAppraiserName"));
      getCurrNDPage().setDisplayFieldValue("Repeated1/stPhone",
        appraiserSelectDO.getValue(
        "dfAppraiserPhoneNumber"));
      Object extension = appraiserSelectDO.getValue("dfAppraiserExtension");
      if(extension != null && extension.toString().trim().length() != 0)
      {
        getCurrNDPage().setDisplayFieldValue("Repeated1/stAppraiserExtension",
          new
          String("x" + extension.toString()));
      }
    }
    catch(Exception e)
    {
      setStandardFailMessage();
      logger.error("Exception @AppraisalHandler.displayAppraiser: Problem encountered locating appraiser, propertyId=<" +
        propertyId + ">");
      logger.error(e);
      return;
    }

    return;

  }

  /////////////////////////////////////////////////////////////////////
  public boolean setAddAppraiserStatus()
  {
    // use (row scope) page state variable set by displayAppraiser() [triggered on
    // a row event] to communicate if an appraiser is selected for the current row.
    PageEntry pg = getTheSessionState().getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    boolean haveAppraiser = false;
    String flag = (String)pst.get("APPRAISER_THIS_ROW");

    if(flag != null && flag.equals("Y"))
    {
      haveAppraiser = true;

    }
    if(haveAppraiser == false)
    {
      return true;
    }
    // none - display the 'add' button
    else
    {
      return false;
    }
  }

  ///////////////////////////////////////////////////////////////////////////
  public boolean setChangeAppraiserStatus()
  {
    // use (row scope) page state variable set by displayAppraiser() [triggered on
    // a row event] to communicate if an appraiser is selected for the current row.
    PageEntry pg = getTheSessionState().getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    boolean haveAppraiser = false;
    String flag = (String)pst.get("APPRAISER_THIS_ROW");

    if(flag != null && flag.equals("Y"))
    {
      haveAppraiser = true;

    }
    if(haveAppraiser == false)
    {

// none - hide the 'change' button
      return false;
    }
    else
    {
      return true;
    }
  }

  /////////////////////////////////////////////////////////////////////
  public void saveData(PageEntry pg, boolean calledInTransaction)
  {
    logger.debug("@AppraisalHandler.saveData():: saveData IN");

    int i = 0;

    SessionResourceKit srk = getSessionResourceKit();

    try
    {
      CalcMonitor dcm = CalcMonitor.getMonitor(srk);
      Hashtable pst = pg.getPageStateTable();
      PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");
      srk.beginTransaction();

      // update entities
      int propertiesOnPage = tds.getNumberOfDisplayRowsOnPage();
      for(i = 0; i < propertiesOnPage; ++i)
      {
        logger.debug(
          "@AppraisalHandler.saveData():: calling updateProperty for property " + i);
        updateProperty(i, pg, srk, dcm);
      }
      dcm.calc();
      srk.commitTransaction();
      if(srk.getModified())
      {
        pg.setModified(true);
      }
    }
    catch(Exception e)
    {
      srk.cleanTransaction();
      setActiveMessageToAlert(BXResources.getSysMsg(
        "TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
      logger.error("Exception @AppraisalHandler.SaveData, deal Id = " +
        pg.getPageDealId() + ", copy Id = " + pg.getPageDealCID() +
        ", line number = " + i);
      logger.error(e);
      return;
    }

  }

  /////////////////////////////////////////////////////////////////////
  private void updateProperty(int rowNdx, PageEntry pg, SessionResourceKit srk, CalcMonitor dcm) throws Exception
  {
    int month = 0;
    int day = 0;
    int year = 0;
    int propertyId = 0;

    try
    {
      Hashtable pst = pg.getPageStateTable();
      Vector propIds = (Vector)pst.get("PROPIDS");
      Vector aDates = (Vector)pst.get("ADATES");
      propertyId = com.iplanet.jato.util.TypeConverter.asInt(propIds.elementAt(rowNdx), 0);
      logger.debug("@AppraisalHandler.updateProperty():: propertyId = " + propertyId);
      logger.debug("@AppraisalHandler.updateProperty():: copyid = " + pg.getPageDealCID());
      Property pp = new Property(srk, dcm, propertyId, pg.getPageDealCID());

      // read and set properties
      // Appraisal Date
      boolean dateBad = true;

      month = com.iplanet.jato.util.TypeConverter.asInt(getRepeatedField("Repeated1/cbMonths", rowNdx), -1) - 1;
      if(month >= 0)
      {
        day = com.iplanet.jato.util.TypeConverter.asInt(getRepeatedField("Repeated1/tbDay", rowNdx), -1);
        if(day > 0)
        {
          year = com.iplanet.jato.util.TypeConverter.asInt(getRepeatedField(
            "Repeated1/tbYear", rowNdx), -1);
          if(year > 0)
          {
            dateBad = false;
          }
        }
      }
      aDates.set(rowNdx, null);
      logger.debug("updateProperty -- the date :: year=" + year + " month=" +
        month + " day=" + day);
      if(dateBad == false)
      {
        GregorianCalendar calendar = (GregorianCalendar)GregorianCalendar.
          getInstance();
        calendar.set(year, month, day);
        pp.setAppraisalDateAct(calendar.getTime());
        aDates.set(rowNdx, calendar.getTime());
      }

      pp.setAppraisalSourceId(
        com.iplanet.jato.util.TypeConverter.asInt(getRepeatedField(
        "Repeated1/cbAppraisalSource", rowNdx), 0));

      pp.setEstimatedAppraisalValue(com.iplanet.jato.util.TypeConverter.asDouble(getRepeatedField(
        "Repeated1/tbEstimatedAppraisal", rowNdx), 0));

      pp.setActualAppraisalValue(com.iplanet.jato.util.TypeConverter.asDouble(getRepeatedField(
        "Repeated1/tbActualAppraisal", rowNdx), 0));

      //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
      //--> By Billy 22Dec2003
      // Check if Appraisal Order supported
      if(isSupportAppraisalOrder())
        updateAppraisalOrder(rowNdx, pg, srk, dcm, pp);
      //=================================================================

      pp.ejbStore();
    }
    catch(Exception ex)
    {
      setActiveMessageToAlert(BXResources.getSysMsg(
        "TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
      logger.error("Exception @AppraisalHandler.updateProperty: deal Id = " +
        pg.getPageDealId() + ", copy Id = " + pg.getPageDealCID() +
        ", property Id = " + propertyId);
      logger.error(ex);
      throw new Exception("@updateProperty");
    }
  }

  ///////////////////////////////////////////////////////////////////////////
  public void changeAppraiser(int rowNdx, String buttonName)
  {
    //	Pass PartyTypeId, PropertyId into the Deal Summary Screen
    //	Navigate to the Deal Summary Screen
    try
    {
      PageEntry pg = getTheSessionState().getCurrentPage();
      Hashtable pst = pg.getPageStateTable();
      Vector propIds = (Vector)pst.get("PROPIDS");
      logger.trace("--T--> @AppraiserHandler.changeAppraiser:");
      PageEntry pgEntry = null;
      if(buttonName.equals("btChangeAppraiser"))
      {
        pgEntry = setupSubPagePageEntry(pg, Sc.PGNM_PARTY_SEARCH, true);
      }
      else if(buttonName.equals("btAddAppraiser"))
      {
        pgEntry = setupSubPagePageEntry(pg, Sc.PGNM_PARTY_SEARCH, true);
      }
      else
      {
        throw new Exception("");
      }

      Hashtable subPst = pgEntry.getPageStateTable();
      subPst.put(PartySearchHandler.PROPERTY_ID_FILTER_PASSED_PARM,
        (String)propIds.elementAt(rowNdx));
      subPst.put(PartySearchHandler.PARTY_TYPE_FILTER_PASSED_PARM,
        "" + Sc.PARTY_TYPE_APPRAISER);
      logger.trace(
        "--T--> @AppraiserHandler.changeAppraiser: passing: propertyId=" +
        ((String)subPst.get(PartySearchHandler.
        PROPERTY_ID_FILTER_PASSED_PARM)) +
        ", partyTypeId=" +
        ((String)subPst.get(PartySearchHandler.PARTY_TYPE_FILTER_PASSED_PARM)));
      getSavedPages().setNextPage(pgEntry);
      navigateToNextPage();
    }
    catch(Exception ex)
    {
      setActiveMessageToAlert(BXResources.getSysMsg(
        "TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
      logger.error("Exception @AppraisalHandler.changeAppraiser: while navigating to the Party Summary/Search subpage");
      logger.error(ex);
    }
  }

  /////////////////////////////////////////////////////////////////////
  public void handleAppraiserSubmit()
  {
    try
    {
      PageEntry pg = getTheSessionState().getCurrentPage();
      srk.beginTransaction();
      standardAdoptTxCopy(pg, true);
      srk.commitTransaction();

      // #2370 - Catherine, 4-Nov-05 --- start -----------------------
      try {
        logger.debug("--> PartySearchHandler.handleAddPartySubmit() -- Calling Workflow");
        workflowTrigger(2, srk, pg, null);
      }
      catch(Exception e)
      {
        logger.error("Exception @AppraisalHandler.handleAppraiserSubmit():");
        logger.error(e);
        setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMCONFIRM);
        return;
      }
      // #2370 - Catherine, 4-Oct-05 --- end -----------------------
      
      navigateToNextPage(true);
      return;
    }
    catch(Exception e)
    {
      srk.cleanTransaction();
      logger.error(
        "Exception @AppraisalHandler.handleAppraiserSubmit: while Submiting");
      logger.error(e);
    }
    //  exception was encountered ... navigate away from the page (including away from parent page if sub)
    navigateAwayFromFromPage(true);
  }

  /////////////////////////////////////////////////////////////////////

  //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
  //--> By Billy 22Dec2003
  // method to check if Appraisal Order is supported or not
  public boolean isSupportAppraisalOrder()
  {
    if((PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),"com.basis100.appraisalreview.supportappraisalorder", "N")).equals("N"))
      return false;
    else
      return true;
  }

  // method to check if Appraisal Order is supported
  public String getIsSupportAppraisalOrderStr()
  {
    if(isSupportAppraisalOrder())
      return "true";
    else
      return "false";
  }

  // Method to update or create Appraisal Order
  private void updateAppraisalOrder(int rowNdx, PageEntry pg, SessionResourceKit srk, CalcMonitor dcm, Property p)
    throws Exception
  {
    // check if Appraisal Order exist
    AppraisalOrder theAO = p.getAppraisalOrder();
    if(theAO == null)
    {
      // Create the Appraisal Order record
      theAO = new AppraisalOrder(srk, dcm).create((PropertyPK)(p.getPk()));
    }

    // Update Appraisal Order data from input fields
    theAO.setUseBorrowerInfo(getRepeatedField("Repeated1/chUseBorrowerInfo", rowNdx));
    // Update Name of Property Owner
    theAO.setPropertyOwnerName(getRepeatedField("Repeated1/tbPropertyOwnerName", rowNdx));
    // Update Contact Name
    theAO.setContactName(getRepeatedField("Repeated1/tbAOContactName", rowNdx));
    // Update Contact Work Phone Number
    theAO.setContactWorkPhoneNum(this.phoneJoin(
      getRepeatedField("Repeated1/tbAOContactWorkPhoneNum1", rowNdx),
      getRepeatedField("Repeated1/tbAOContactWorkPhoneNum2", rowNdx),
      getRepeatedField("Repeated1/tbAOContactWorkPhoneNum3", rowNdx)
      ));
    theAO.setContactWorkPhoneNumExt(getRepeatedField("Repeated1/tbAOContactWorkPhoneNumExt", rowNdx));
    // Update Contact Cell Phone Number
    theAO.setContactCellPhoneNum(this.phoneJoin(
      getRepeatedField("Repeated1/tbAOContactCellPhoneNum1", rowNdx),
      getRepeatedField("Repeated1/tbAOContactCellPhoneNum2", rowNdx),
      getRepeatedField("Repeated1/tbAOContactCellPhoneNum3", rowNdx)
      ));
    // Update Contact Home Phone Number
    theAO.setContactHomePhoneNum(this.phoneJoin(
      getRepeatedField("Repeated1/tbAOContactHomePhoneNum1", rowNdx),
      getRepeatedField("Repeated1/tbAOContactHomePhoneNum2", rowNdx),
      getRepeatedField("Repeated1/tbAOContactHomePhoneNum3", rowNdx)
      ));
    // Update Comments to Appraiser
    theAO.setCommentsForAppraiser(getRepeatedField("Repeated1/tbCommentsForAppraiser", rowNdx));

    // check if need to update Appraisal Order status
    if(isSubmitAndSendButton == true)
    {
      // set status to Submitted
      theAO.setAppraisalStatusId(1);
    }

    theAO.ejbStore();
  }

  public void handleSubmitAndSendAOReq()
  {
    try
    {
      PageEntry pg = getTheSessionState().getCurrentPage();
      Deal deal = null;

      srk.beginTransaction();
      standardAdoptTxCopy(pg, true);

      deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

      //--DJ_#548_Ticket--start--//
      SessionResourceKit srk = getSessionResourceKit();

      // Get the corresponding Deal Entity
      ////Deal theDeal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());

      // validation
      PassiveMessage pm = validateAppraisalRules(deal, theSessionState, pg, srk);
      //logger.debug("AH@handleSubmitAndSendAOReq::PM#ofMsgs: " + pm.getNumMessages());
      //logger.debug("AH@handleSubmitAndSendAOReq::isCritical? " + pm.getCritical());

      if (pm.getCritical() == true)
      {
        pm.setGenerate(true);
        theSessionState.setPasMessage(pm);

        setActiveMessageToAlert(BXResources.getSysMsg("MI_VALIDATION_CANCEL_ERROR_MSG", theSessionState.getLanguageId()),
              ActiveMsgFactory.ISCUSTOMCONFIRM);

        return;
      }

      // no validate problems - determine and produce document for appraiser
      //--DJ_#548_Ticket--start--//
      //--DJ_DT_CR--start--//
      logger.debug("VLAD ===> TEST_Before :: Before send the requestInstructionToAppraiser!!");
      DocumentRequest.requestInstructionToAppraiser(srk, deal);
      logger.debug("VLAD ===> TEST_After :: Sent requestInstructionToAppraiser!!");
      //--DJ_DT_CR--end--//

      //--Ticket#652--start--//
      // Trigger workflow if Appraisal Rules are OK.
      workflowTrigger(2, srk, pg, null);
      //--Ticket#652--start--//

      srk.commitTransaction();
      navigateToNextPage(true);
      return;
    }
    catch(Exception e)
    {
      srk.cleanTransaction();
      logger.error(
        "Exception @AppraisalHandler.handleAppraiserSubmit: while Submiting");
      logger.error(e);
    }
    //  exception was encountered ... navigate away from the page (including away from parent page if sub)
    navigateAwayFromFromPage(true);
  }

  // This is only used for the Submit and send button to indicate if to send AO req.
  private boolean isSubmitAndSendButton = false;
  public void preHandlerProtocolSpecial(ViewBean currNDPage)
  {
    logger = SysLog.getSysLogger("@AppraisalHandler.preHandlerProtocolSpecial");
    isSubmitAndSendButton = true;
    preHandlerProtocol(currNDPage, false);
    isSubmitAndSendButton = false;
  }

  //--DJ_#548_Ticket--start--//
  public PassiveMessage validateAppraisalRules(Deal deal, SessionStateModelImpl theSession, PageEntry pe, SessionResourceKit srk) throws Exception
  {
      PassiveMessage       pm     = new PassiveMessage();
      BusinessRuleExecutor brExec = new BusinessRuleExecutor();

      // local scope rules ...
      int lim = 0;
      Object [] objs = null;

      // property rules
      try
      {
          Collection properties = deal.getProperties();
          lim = properties.size();
          objs = properties.toArray();
      }
      catch (Exception e) { lim = 0; } // assume no properties

      for (int i = 0; i < lim; ++i)
      {
          Property prop = (Property)objs[i];
          brExec.setCurrentProperty(prop.getPropertyId());
          brExec.setCurrentBorrower(-1);

          PassiveMessage pmProp = brExec.BREValidator(theSession, pe, srk, null, "AR-%");

          if (pmProp != null)
          {
            pm.addSeparator();
            pm.addMsg(BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_PROPERTY_PROBLEM", theSessionState.getLanguageId())  +
                      prop.formPropertyAddress(), PassiveMessage.INFO);
            pm.addAllMessages(pmProp);
            pm.addSeparator();
          }
      }

      if (pm.getNumMessages() > 0)
      {
          String tmpMsg = BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_DEAL_PROBLEM", theSessionState.getLanguageId())
            + "<br>" + "(Deal #: " + deal.getDealId() + ", " + "Scenario #: " + deal.getScenarioNumber() + ")";
          pm.setInfoMsg(tmpMsg);
          return pm;
      }

      return pm;
  }
  //--DJ_#548_Ticket--end--//
  
  
  

}
