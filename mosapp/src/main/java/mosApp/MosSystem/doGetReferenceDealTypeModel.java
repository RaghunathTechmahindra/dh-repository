package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doGetReferenceDealTypeModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfReferenceDealTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfReferenceDealTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfReferenceDealTypeDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfReferenceDealTypeDesc(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFREFERENCEDEALTYPEID="dfReferenceDealTypeId";
	public static final String FIELD_DFREFERENCEDEALTYPEDESC="dfReferenceDealTypeDesc";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

