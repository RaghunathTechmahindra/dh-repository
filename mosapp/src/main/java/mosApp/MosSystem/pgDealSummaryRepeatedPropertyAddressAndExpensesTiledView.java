package mosApp.MosSystem;

import java.util.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgDealSummaryRepeatedPropertyAddressAndExpensesTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealSummaryRepeatedPropertyAddressAndExpensesTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doDetailViewPropertiesModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStEXPrimaryProperty().setValue(CHILD_STEXPRIMARYPROPERTY_RESET_VALUE);
		getStEXStreetNumber().setValue(CHILD_STEXSTREETNUMBER_RESET_VALUE);
		getStEXCity().setValue(CHILD_STEXCITY_RESET_VALUE);
		getStEXProvince().setValue(CHILD_STEXPROVINCE_RESET_VALUE);
		getStEXStreetName().setValue(CHILD_STEXSTREETNAME_RESET_VALUE);
		getStEXPosCode1().setValue(CHILD_STEXPOSCODE1_RESET_VALUE);
		getStEXPosCode2().setValue(CHILD_STEXPOSCODE2_RESET_VALUE);
		getRepeatedPropertyExpenses().resetChildren();
		getStBeginHidePropExpensesSection().setValue(CHILD_STBEGINHIDEPROPEXPENSESSECTION_RESET_VALUE);
		getStEndHidePropExpensesSection().setValue(CHILD_STENDHIDEPROPEXPENSESSECTION_RESET_VALUE);
		getStExStreetType().setValue(CHILD_STEXSTREETTYPE_RESET_VALUE);
		getStExStreetDirection().setValue(CHILD_STEXSTREETDIRECTION_RESET_VALUE);
		getStExAddressLine2().setValue(CHILD_STEXADDRESSLINE2_RESET_VALUE);
		getStPropertyAddressUnitNumber().setValue(CHILD_STPROPERTYADDRESSUNITNUMBER_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STEXPRIMARYPROPERTY,StaticTextField.class);
		registerChild(CHILD_STEXSTREETNUMBER,StaticTextField.class);
		registerChild(CHILD_STEXCITY,StaticTextField.class);
		registerChild(CHILD_STEXPROVINCE,StaticTextField.class);
		registerChild(CHILD_STEXSTREETNAME,StaticTextField.class);
		registerChild(CHILD_STEXPOSCODE1,StaticTextField.class);
		registerChild(CHILD_STEXPOSCODE2,StaticTextField.class);
		registerChild(CHILD_REPEATEDPROPERTYEXPENSES,pgDealSummaryRepeatedPropertyExpensesTiledView.class);
		registerChild(CHILD_STBEGINHIDEPROPEXPENSESSECTION,StaticTextField.class);
		registerChild(CHILD_STENDHIDEPROPEXPENSESSECTION,StaticTextField.class);
		registerChild(CHILD_STEXSTREETTYPE,StaticTextField.class);
		registerChild(CHILD_STEXSTREETDIRECTION,StaticTextField.class);
		registerChild(CHILD_STEXADDRESSLINE2,StaticTextField.class);
		registerChild(CHILD_STPROPERTYADDRESSUNITNUMBER,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDetailViewPropertiesModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		// The following code block was migrated from the RepeatedPropertyAddressAndExpenses_onBeforeDisplayEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.setupNumOfRepeatedPropertyAddressAndExpenses(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// The following code block was migrated from the RepeatedPropertyAddressAndExpenses_onBeforeRowDisplayEvent method
      DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();

      handler.pageGetState(this.getParentViewBean());
      handler.setExpenses(this.getTileIndex());
      handler.pageSaveState();
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STEXPRIMARYPROPERTY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STEXPRIMARYPROPERTY,
				doDetailViewPropertiesModel.FIELD_DFPRIMARYPROPERTY,
				CHILD_STEXPRIMARYPROPERTY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STEXSTREETNUMBER))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STEXSTREETNUMBER,
				doDetailViewPropertiesModel.FIELD_DFSTREETNUMBER,
				CHILD_STEXSTREETNUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STEXCITY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STEXCITY,
				doDetailViewPropertiesModel.FIELD_DFCITY,
				CHILD_STEXCITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STEXPROVINCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STEXPROVINCE,
				doDetailViewPropertiesModel.FIELD_DFPROVINCE,
				CHILD_STEXPROVINCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STEXSTREETNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STEXSTREETNAME,
				doDetailViewPropertiesModel.FIELD_DFSTREETNAME,
				CHILD_STEXSTREETNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STEXPOSCODE1))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STEXPOSCODE1,
				doDetailViewPropertiesModel.FIELD_DFPOSTALFSA,
				CHILD_STEXPOSCODE1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STEXPOSCODE2))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STEXPOSCODE2,
				doDetailViewPropertiesModel.FIELD_DFPOSTALLDU,
				CHILD_STEXPOSCODE2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_REPEATEDPROPERTYEXPENSES))
		{
			pgDealSummaryRepeatedPropertyExpensesTiledView child = new pgDealSummaryRepeatedPropertyExpensesTiledView(this,
				CHILD_REPEATEDPROPERTYEXPENSES);
			return child;
		}
		else
		if (name.equals(CHILD_STBEGINHIDEPROPEXPENSESSECTION))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBEGINHIDEPROPEXPENSESSECTION,
				CHILD_STBEGINHIDEPROPEXPENSESSECTION,
				CHILD_STBEGINHIDEPROPEXPENSESSECTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STENDHIDEPROPEXPENSESSECTION))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STENDHIDEPROPEXPENSESSECTION,
				CHILD_STENDHIDEPROPEXPENSESSECTION,
				CHILD_STENDHIDEPROPEXPENSESSECTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STEXSTREETTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STEXSTREETTYPE,
				doDetailViewPropertiesModel.FIELD_DFSTREETTYPE,
				CHILD_STEXSTREETTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STEXSTREETDIRECTION))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STEXSTREETDIRECTION,
				doDetailViewPropertiesModel.FIELD_DFSTREETDIRECTION,
				CHILD_STEXSTREETDIRECTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STEXADDRESSLINE2))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STEXADDRESSLINE2,
				doDetailViewPropertiesModel.FIELD_DFADDRESSLINE,
				CHILD_STEXADDRESSLINE2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYADDRESSUNITNUMBER))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPROPERTYADDRESSUNITNUMBER,
				doDetailViewPropertiesModel.FIELD_DFPROPERTYUNITNUMBER,
				CHILD_STPROPERTYADDRESSUNITNUMBER_RESET_VALUE,
				null);
			return child;
		}
		
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEXPrimaryProperty()
	{
		return (StaticTextField)getChild(CHILD_STEXPRIMARYPROPERTY);
	}


	/**
	 *
	 *
	 */
	public String endStEXPrimaryPropertyDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stEXPrimaryProperty_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setYesOrNo(event.getContent());
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEXStreetNumber()
	{
		return (StaticTextField)getChild(CHILD_STEXSTREETNUMBER);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEXCity()
	{
		return (StaticTextField)getChild(CHILD_STEXCITY);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEXProvince()
	{
		return (StaticTextField)getChild(CHILD_STEXPROVINCE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEXStreetName()
	{
		return (StaticTextField)getChild(CHILD_STEXSTREETNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEXPosCode1()
	{
		return (StaticTextField)getChild(CHILD_STEXPOSCODE1);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEXPosCode2()
	{
		return (StaticTextField)getChild(CHILD_STEXPOSCODE2);
	}


	/**
	 *
	 *
	 */
	public pgDealSummaryRepeatedPropertyExpensesTiledView getRepeatedPropertyExpenses()
	{
		return (pgDealSummaryRepeatedPropertyExpensesTiledView)getChild(CHILD_REPEATEDPROPERTYEXPENSES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBeginHidePropExpensesSection()
	{
		return (StaticTextField)getChild(CHILD_STBEGINHIDEPROPEXPENSESSECTION);
	}


	/**
	 *
	 *
	 */
	public String endStBeginHidePropExpensesSectionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stBeginHidePropExpensesSection_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.generateOrHideBeginPropExpSect();
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEndHidePropExpensesSection()
	{
		return (StaticTextField)getChild(CHILD_STENDHIDEPROPEXPENSESSECTION);
	}


	/**
	 *
	 *
	 */
	public String endStEndHidePropExpensesSectionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stEndHidePropExpensesSection_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.generateOrHideEndPropExpSect();
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStExStreetType()
	{
		return (StaticTextField)getChild(CHILD_STEXSTREETTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStExStreetDirection()
	{
		return (StaticTextField)getChild(CHILD_STEXSTREETDIRECTION);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStExAddressLine2()
	{
		return (StaticTextField)getChild(CHILD_STEXADDRESSLINE2);
	}


	
	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyAddressUnitNumber()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYADDRESSUNITNUMBER);
	}
	/**
	 *
	 *
	 */
	public doDetailViewPropertiesModel getdoDetailViewPropertiesModel()
	{
		if (doDetailViewProperties == null)
			doDetailViewProperties = (doDetailViewPropertiesModel) getModel(doDetailViewPropertiesModel.class);
		return doDetailViewProperties;
	}


	/**
	 *
	 *
	 */
	public void setdoDetailViewPropertiesModel(doDetailViewPropertiesModel model)
	{
			doDetailViewProperties = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STEXPRIMARYPROPERTY="stEXPrimaryProperty";
	public static final String CHILD_STEXPRIMARYPROPERTY_RESET_VALUE="";
	public static final String CHILD_STEXSTREETNUMBER="stEXStreetNumber";
	public static final String CHILD_STEXSTREETNUMBER_RESET_VALUE="";
	public static final String CHILD_STEXCITY="stEXCity";
	public static final String CHILD_STEXCITY_RESET_VALUE="";
	public static final String CHILD_STEXPROVINCE="stEXProvince";
	public static final String CHILD_STEXPROVINCE_RESET_VALUE="";
	public static final String CHILD_STEXSTREETNAME="stEXStreetName";
	public static final String CHILD_STEXSTREETNAME_RESET_VALUE="";
	public static final String CHILD_STEXPOSCODE1="stEXPosCode1";
	public static final String CHILD_STEXPOSCODE1_RESET_VALUE="";
	public static final String CHILD_STEXPOSCODE2="stEXPosCode2";
	public static final String CHILD_STEXPOSCODE2_RESET_VALUE="";
	public static final String CHILD_REPEATEDPROPERTYEXPENSES="RepeatedPropertyExpenses";
	public static final String CHILD_STBEGINHIDEPROPEXPENSESSECTION="stBeginHidePropExpensesSection";
	public static final String CHILD_STBEGINHIDEPROPEXPENSESSECTION_RESET_VALUE="";
	public static final String CHILD_STENDHIDEPROPEXPENSESSECTION="stEndHidePropExpensesSection";
	public static final String CHILD_STENDHIDEPROPEXPENSESSECTION_RESET_VALUE="";
	public static final String CHILD_STEXSTREETTYPE="stExStreetType";
	public static final String CHILD_STEXSTREETTYPE_RESET_VALUE="";
	public static final String CHILD_STEXSTREETDIRECTION="stExStreetDirection";
	public static final String CHILD_STEXSTREETDIRECTION_RESET_VALUE="";
	public static final String CHILD_STEXADDRESSLINE2="stExAddressLine2";
	public static final String CHILD_STEXADDRESSLINE2_RESET_VALUE="";
	
	public static final String CHILD_STPROPERTYADDRESSUNITNUMBER="stPropertyAddressUnitNumber";
	public static final String CHILD_STPROPERTYADDRESSUNITNUMBER_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDetailViewPropertiesModel doDetailViewProperties=null;
  private DealSummaryHandler handler=new DealSummaryHandler();

}

