package mosApp.MosSystem;

import java.util.Hashtable;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.filogix.express.legacy.mosapp.ISavedPagesModel;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.BeanAdapterModel;

/**
 *
 *
 */
//--> This may cause memory leak if we store the HTTPSession the SessionState Object
//--> As the SessionModel will holding a reference to the HTTPSession then when we invalide
//--> the session it will not go away when GC clicks off.
//--> Modified by Billy 11April2003
//public class SavedPagesModelImpl extends SessionModel
public class SavedPagesModelImpl extends BeanAdapterModel
	implements Sc, SavedPagesModel, ISavedPagesModel
 {

	/**
	 *
	 *
	 */
	public SavedPagesModelImpl()
	{
		super();
		initPreviousPagesLinkText();
	}

	/**
	 *
	 *
	 */
  //--> This may cause memory leak if we store the HTTPSession the SessionState Object
  //--> Commented out by Billy 11April2003
  /*
	public SavedPagesModelImpl(HttpSession session)
	{
	    super();
	   	this.session = session;
	}
  */

	/**
	 *
	 *
	 */
	////public SavedPagesModelImpl()
	////{
	////	initPreviousPagesLinkText();
        ////
	////}


	/**
	 *
	 *
	 */
	public SavedPagesModelImpl(int queueSizeMax)
	{
		if (queueSizeMax == QUEUE_SIZE_MAX)
		{
			return;
		}

		this.queueSizeMax = queueSizeMax;

		previousPagesQueue = new PageQueue(queueSizeMax);

		previousPagesLinkText = new String [ queueSizeMax ];

		previousPagesLinkFlag = new boolean [ queueSizeMax ];

		initPreviousPagesLinkText();

	}


	/**
	 *
	 *
	 */
	public void addToPreviousPages(PageEntry pe)
	{

		// check for navigation to self
		PageEntry nextPg = getNextPage();

		if (nextPg != null && pe != null)
		{
			if (pe.getPageId() == nextPg.getPageId() && pe.getPageDealId() == nextPage.getPageDealId() 
			        && pe.getDealInstitutionId() == nextPage.getDealInstitutionId())
			{
				// enough to consider as navigation to self
				return;
			}
		}

		pe.setPageDealCID(- 1);

		pe.setModified(false);

		previousPagesQueue.add(pe);

	}


	/**
	 *
	 *
	 */
	public void setNextPageAsPreviousPage(int queueNdx)
	{
		setNextPage(previousPagesQueue.get(queueNdx));

		previousPagesQueue.remove(queueNdx);

	}


	/**
	 *
	 *
	 */
	public void setNextPageAsWorkQueue()
	{
		setNextPage(getWorkQueue());

	}


	/**
	 *
	 *
	 */
	public void setNextPageAsWorkQueue(boolean refresh)
	{
		PageEntry iworkqueue = getWorkQueue();

		Hashtable pagest = iworkqueue.getPageStateTable();

		if (pagest != null)
		{
			pagest.put("REFRESH", new Boolean(refresh));
		}

		setNextPage(iworkqueue);

	}


	/**
	 *
	 *
	 */
	public void setNextPageAsBackPage()
	{
		setNextPage(getBackPage());

	}


	/**
	 *
	 *
	 */
	public void setNextPageAsDealSearch()
	{
		setNextPage(getSearchPage());

	}


	/**
	 *
	 *
	 */
  //--> New method to clear all cached PEs -- By Billy 22April2003
  public void clear()
  {
	workQueue = null;
    searchPage=null;
    backPage=null;
    nextPage=null;
    clearPageSetFlags();
  }

	public void clearPageSetFlags()
	{
		nextPageSet = false;
		backPageSet = false;
	}


	/**
	 *
	 *
	 */
	public void revisePreviousPagesLinks()
	{
		int allowedQueueSize = previousPagesQueue.getQueueSize();

		//logger = SysLog.getSysLogger("SVDPGS");

		//--Release2.1--//
		////Get the Language ID from the SessionStateModel
		String defaultInstanceStateName =
			RequestManager.getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);

		SessionStateModelImpl theSessionState = (SessionStateModelImpl)RequestManager.getRequestContext().getModelManager().getModel(
				SessionStateModel.class,
				defaultInstanceStateName,
				true);

		// Fix by Billy to prevent Queue Size and LinkText size out of sync. -- 18July2001
		for(int i = allowedQueueSize;
		i < queueSizeMax;
		i ++)
		{
			previousPagesLinkFlag [ i ] = false;
			previousPagesLinkText [ i ] = "";
		}

		PageEntry pg = null;

		String linkText = "";

		String dealAppId = "";

		for(int i = 0;
		i < allowedQueueSize;
		i ++)
		{
			previousPagesLinkFlag [ i ] = true;
			pg = previousPagesQueue.get(i);
			if (pg == null)
			{
				continue;
			}
			//--Release2.1--start//
			int languageId = theSessionState.getLanguageId();
			int institutionId = theSessionState.getDealInstitutionId();
			if (institutionId < 0)
				institutionId = theSessionState.getUserInstitutionId();

			//// Except the page name the pageId shoud be extracted and passed to the
			//// BXResources to obtain the language independent page name.
			////linkText = i + 1 + ". " + BXResources.getPickListDescription("PAGELABEL", pg.getPageId(), languageId);
			//--DJ_LDI_CR--start--//
			//// In order to display or not to Life&Disability Insurance page.

			//// No Life&Disability Insurance screen.
			if (PropertiesCache.getInstance().getProperty(-1, "com.basis100.ldinsurance.displaylifedisabilitymodule", "N").equals("N"))
			{
				linkText = i + 1 + ". " + BXResources.getPickListDescription(institutionId, "PAGELABEL", pg.getPageId(), languageId);
			}
			//// Give an access to the Life&Disability Insurance screen.
			else if (PropertiesCache.getInstance().getProperty(-1, "com.basis100.ldinsurance.displaylifedisabilitymodule", "N").equals("Y"))
			{
				linkText = i + 1 + ". " + BXResources.getPickListDescription(institutionId, "DJPAGELABEL", pg.getPageId(), languageId);
			}
			//--DJ_LDI_CR--end--//
			//--Release2.1--end//

			dealAppId = pg.getPageDealApplicationId();
			if ((dealAppId != null) &&(! dealAppId.equals("")))
			{
				//--Release2.1--start//
				String dealLabel = BXResources.getGenericMsg("DEAL_LABEL",  theSessionState.getLanguageId());
				linkText += ", " + dealLabel + dealAppId;
				//--Release2.1--end//
			}

			//ML - add institution name on previous screen label 
			if (pg.getDealInstitutionId() >= 0 && theSessionState.isMultiAccess())  {
				linkText += ", " + BXResources.getInstitutionName(pg.getDealInstitutionId());
			}

			previousPagesLinkText [ i ] = linkText;
		}

	}


	/**
	 *
	 *
	 */
	private void initPreviousPagesLinkText()
	{
		for(int i = 0;
		i < queueSizeMax;
		i ++)
		{
			previousPagesLinkText [ i ] = "";
		}

	}


	/**
	 *
	 *
	 */
	public boolean isNextPageSet()
	{
		return nextPageSet;

	}


	/**
	 *
	 *
	 */
	public boolean isBackPageSet()
	{
		return backPageSet;

	}


	/**
	 *
	 *
	 */
	public PageEntry getBackPage()
	{
		return backPage;

	}


	/**
	 *
	 *
	 */
	public PageEntry getNextPage()
	{
		return nextPage;

	}


	/**
	 *
	 *
	 */
	public boolean getPreviousPagesLinkFlag(int position)
	{
		if (position >= queueSizeMax)
		{
			return false;
		}

		return previousPagesLinkFlag [ position ];

	}


	/**
	 *
	 *
	 */
	public String getPreviousPagesLinkText(int position)
	{
		if (position >= queueSizeMax)
		{
			return "";
		}

		return previousPagesLinkText [ position ];

	}


	/**
	 *
	 *
	 */
	public PageQueue getPreviousPagesQueue()
	{
		return previousPagesQueue;

	}


	/**
	 *
	 *
	 */
	public PageEntry getWorkQueue()
	{
		return workQueue;

	}


	/**
	 *
	 *
	 */
	public PageEntry getSearchPage()
	{
		return searchPage;

	}


	/**
	 *
	 *
	 */
	////public void setBackPage(PageEntry aBackPage)
	////{
	////	int pId = aBackPage.getPageId();

	////	if (pId == 0 || pId == Mc.PGNM_SIGN_ON_ID || pId == Mc.PGNM_SIGNOFF_ID || pId == Mc.PGNM_CHANGE_PASSWORD_ID) return;

	////	backPage = aBackPage;

	////	if (backPage != null) backPageSet = true;
	////	else backPageSet = false;
        ////
	////}

	public void setBackPage(PageEntry aBackPage)
	{
    //logger = SysLog.getSysLogger("SVDPGS");
    //logger.debug("SPMODEL@setBackPage:: Start");

    int pId = aBackPage.getPageId();
    //logger.debug("SPMODEL@setBackPage::PageId: " + pId);

		if (pId == 0 || pId == Mc.PGNM_SIGN_ON_ID || pId == Mc.PGNM_SIGNOFF_ID || pId == Mc.PGNM_CHANGE_PASSWORD_ID) return;

		backPage = aBackPage;

		if (backPage != null) backPageSet = true;
		else backPageSet = false;


    //logger.debug("SignOnHangler@storeNextPageAsBackPage:: " + backPage.getPageName());

    //// temp to test only.
    ////savedPages.show(logger);
	}

	/**
	 *
	 *
	 */
	public void setNextPage(PageEntry aPageEntry)
	{
                //// This log activity brings the exception.
                ////logger = SysLog.getSysLogger("SAVEDPGS");

                ////logger.debug("SPMI@setNextPage::PageEntryNameIn: " + aPageEntry.getPageName());
                ////logger.debug("SPMI@setNextPage::PageEntryMosUserIdIn: " + aPageEntry.getPageMosUserId());
                ////logger.debug("SPMI@setNextPage::PageEntryMosUserTypeIn: " + aPageEntry.getPageMosUserTypeId());
                ////logger.debug("SPMI@setNextPage::PageEntryDealIdIn: " + aPageEntry.getPageDealId());
                ////logger.debug("SPMI@setNextPage::PageEntryCopyIdIn: " + aPageEntry.getPageDealCID());
                ////logger.debug("======================================");

                nextPage = aPageEntry;

		            if (nextPage != null) nextPageSet = true;
		            else nextPageSet = false;

                ////logger.debug("SPMI@setNextPage::setNextPage: " + nextPage.getPageName());
                ////logger.debug("SPMI@setNextPage::PageEntryMosUserIdNextPage: " + nextPage.getPageMosUserId());
                ////logger.debug("SPMI@setNextPage::PageEntryMosUserTypeNextPage: " + nextPage.getPageMosUserTypeId());
                ////logger.debug("SPMI@setNextPage::PageEntryDealIdNextPage: " + nextPage.getPageDealId());
                ////logger.debug("SPMI@setNextPage::PageEntryCopyIdNextPage: " + nextPage.getPageDealCID());

                ////logger.debug("SPMI@setNextPage::I finished.");

	}


	/**
	 *
	 *
	 */
	public void setWorkQueue(PageEntry aWorkQueue)
	{
		workQueue = aWorkQueue;

	}


	/**
	 *
	 *
	 */
	public void setSearchPage(PageEntry aSearchPage)
	{
		searchPage = aSearchPage;

	}


	/**
	 *
	 *
	 */
	public void show(SysLogger logger)
	{
		logger.debug("\nSavedPages::\n");

		logger.debug("\nSavedPages:: nextPageSet = " + nextPageSet);

		logger.debug("\nSavedPages:: backPageSet = " + backPageSet);

		if (nextPage == null) logger.debug("\nSavedPages:: Next Page : IS NULL");
		else nextPage.show("\nSavedPages:: Next Page", logger);

		if (backPage == null) logger.debug("\nSavedPages:: Back Page : IS NULL");
		else backPage.show("\nSavedPages:: Back Page", logger);

		if (workQueue == null) logger.debug("\nSavedPages:: Work Queue : IS NULL");
		else workQueue.show("\nSavedPages:: Work Queue", logger);

		if (searchPage == null) logger.debug("\nSavedPages:: Search Page : IS NULL");
		else searchPage.show("\nSavedPages:: Search Page", logger);

	}

	////////////////////////////////////////////////////////////////////////////////
	// Member variables
	////////////////////////////////////////////////////////////////////////////////
	private static final int QUEUE_SIZE_MAX=10;
	private PageEntry workQueue;
	private PageEntry searchPage=null;
	private boolean nextPageSet;
	private boolean backPageSet;
	private PageEntry backPage;
	private PageEntry nextPage;
	private boolean[] previousPagesLinkFlag=new boolean[QUEUE_SIZE_MAX];
	private String[] previousPagesLinkText=new String[QUEUE_SIZE_MAX];
	private PageQueue previousPagesQueue=new PageQueue(QUEUE_SIZE_MAX);
	private int queueSizeMax=QUEUE_SIZE_MAX;

  //--> This may cause memory leak if we store the HTTPSession the SessionState Object
  //--> Commented out by Billy 11April2003
	//private HttpSession session;
  //public SysLogger logger;

}