package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doPirorityCountModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public String getDfPriorityDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfPriorityDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public Integer getDfPriorityCount();

	
	/**
	 * 
	 * 
	 */
	public void setDfPriorityCount(Integer value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getMOS_PRIORITY_PRIORITYID();

	
	/**
	 * 
	 * 
	 */
	public void setMOS_PRIORITY_PRIORITYID(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFPRIORITYDESC="dfPriorityDesc";
	public static final String FIELD_DFPRIORITYCOUNT="dfPriorityCount";
	public static final String FIELD_MOS_PRIORITY_PRIORITYID="MOS_PRIORITY_PRIORITYID";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

