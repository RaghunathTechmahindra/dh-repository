package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;
import com.basis100.picklist.BXResources;

/**
 *
 *
 *
 */
public class doMWQPickListModelImpl extends QueryModelBase
	implements doMWQPickListModel
{
	/**
	 *
	 *
	 */
	public doMWQPickListModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    ////logger = SysLog.getSysLogger("DOMWQPLM");
    ////logger.debug("DOMWQPLM@afterExecute::SQLTemplate: " + this.getSelectSQL());

    //// 6. Override all the Description fields (see #1-5 below in SQL_TEMPLATE section
    //// and populate them from the BXResource based on the Language ID from the SessionStateModel.
    String defaultInstanceStateName =
			              getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState = (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                            SessionStateModel.class,
                                            defaultInstanceStateName,
                                            true);

    int languageId = theSessionState.getLanguageId();

    while(this.next())
    {
      // Convert Task Labels (using the new field DFTASKID).
        this.setDfTASKNAME(BXResources.getPickListDescription(theSessionState.getUserInstitutionId(),
          "TASK741", this.getDfTASKID().intValue(), languageId));

      //Convert Task Status Descriptions
      this.setDfDescription(BXResources.getPickListDescription(theSessionState.getUserInstitutionId(),
          "TASKSTATUS", this.getDfDescription(), languageId));

      //Convert Priority Descriptions
      this.setDfPriorityDescription(BXResources.getPickListDescription(theSessionState.getUserInstitutionId(),
          "PRIORITY", this.getDfPriorityDescription(), languageId));

      //Convert TimeMilestone Descriptions
      this.setDfTMDescription(BXResources.getPickListDescription(theSessionState.getUserInstitutionId(),
          "TIMEMILESTONE", this.getDfTMDescription(), languageId));
    }

   //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public String getDfDescription()
	{
		return (String)getValue(FIELD_DFDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfDescription(String value)
	{
		setValue(FIELD_DFDESCRIPTION,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAssignTaskId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFASSIGNTASKID);
	}


	/**
	 *
	 *
	 */
	public void setDfAssignTaskId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFASSIGNTASKID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfTMDescription()
	{
		return (String)getValue(FIELD_DFTMDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfTMDescription(String value)
	{
		setValue(FIELD_DFTMDESCRIPTION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfTASKNAME()
	{
		return (String)getValue(FIELD_DFTASKNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfTASKNAME(String value)
	{
		setValue(FIELD_DFTASKNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPriorityDescription()
	{
		return (String)getValue(FIELD_DFPRIORITYDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfPriorityDescription(String value)
	{
		setValue(FIELD_DFPRIORITYDESCRIPTION,value);
	}

	//--Release2.1--//
  //// 6. Add new fiels taskId to maintain the translation via the BXResoure.
  /**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTASKID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTASKID);
	}


	/**
	 *
	 *
	 */
	public void setDfTASKID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTASKID,value);
	}



	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //// 1. Replace all Descriptions from the tables included into the PickList with
  //// their joins' counterparts in the SQL_TEMPLATE.
  //// 2. Convert all these new fragments (IDs) into the string format.
  //// 3. Remove all joins which include the tables from the PickList.
  //// 4. Remove all PickList tables from the SQL_TEMPLATE.
  //// 5. Redefine the QUALIFIED_COLUMN_DFPRIORITYDESCRIPTION fields follow the #1,2.
  //// 6. Add new fiels taskId to maintain the translation via the BXResoure.
  //// 7. Repopulate the PickList Description fields manually in the afterExecute method (see above).
  public static final String DATA_SOURCE_NAME="jdbc/orcl";

  //--Release2.1--//
  //// 1. Replace all Descriptions from the tables included into the PickList with
  //// their joins' counterparts in the SQL_TEMPLATE.
  //// 2. Convert all these new fragments (IDs) into the string format plus new
  //// taskId field to maintain task translation.
	////public static final String SELECT_SQL_TEMPLATE="SELECT ALL TASKSTATUS.TSDESCRIPTION,
  ////ASSIGNEDTASKSWORKQUEUE.ASSIGNEDTASKSWORKQUEUEID, TIMEMILESTONE.TMDESCRIPTION,
  ////WORKFLOWTASK.TASKNAME, PRIORITY.PDESCRIPTION FROM TASKSTATUS,
  ////ASSIGNEDTASKSWORKQUEUE, TIMEMILESTONE, WORKFLOWTASK, PRIORITY  __WHERE__  ";
	public static final String SELECT_SQL_TEMPLATE=
  "SELECT ALL to_char(ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID) TASKSTATUSID_STR, ASSIGNEDTASKSWORKQUEUE.TASKID," +
  "ASSIGNEDTASKSWORKQUEUE.ASSIGNEDTASKSWORKQUEUEID, " +
  "to_char(ASSIGNEDTASKSWORKQUEUE.TIMEMILESTONEID) TIMEMILESTONEID_STR, " +
  "WORKFLOWTASK.TASKNAME, to_char(ASSIGNEDTASKSWORKQUEUE.PRIORITYID) PRIORITYID_STR " +
  "FROM ASSIGNEDTASKSWORKQUEUE, WORKFLOWTASK " +
  "__WHERE__  ";

  //--Release2.1--//
  //// 4. Remove all PickList tables from the SQL_TEMPLATE.
  ////public static final String MODIFYING_QUERY_TABLE_NAME="TASKSTATUS, ASSIGNEDTASKSWORKQUEUE,
  ////TIMEMILESTONE, WORKFLOWTASK, PRIORITY";
  public static final String MODIFYING_QUERY_TABLE_NAME=
  "ASSIGNEDTASKSWORKQUEUE, WORKFLOWTASK";

  //--Release2.1--//
  //// 3. Remove all joins which include the tables from the PickList.
	////public static final String STATIC_WHERE_CRITERIA="
  ////(TASKSTATUS.TASKSTATUSID  =  ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID)
  ////AND (ASSIGNEDTASKSWORKQUEUE.TIMEMILESTONEID  =  TIMEMILESTONE.TIMEMILESTONEID)
  ////AND (ASSIGNEDTASKSWORKQUEUE.WORKFLOWID  =  WORKFLOWTASK.WORKFLOWID)
  ////AND (ASSIGNEDTASKSWORKQUEUE.TASKID  =  WORKFLOWTASK.TASKID)
  ////AND (ASSIGNEDTASKSWORKQUEUE.PRIORITYID  =  PRIORITY.PRIORITYID)";
	public static final String STATIC_WHERE_CRITERIA=
  "(ASSIGNEDTASKSWORKQUEUE.WORKFLOWID  =  WORKFLOWTASK.WORKFLOWID) AND " +
  "(ASSIGNEDTASKSWORKQUEUE.INSTITUTIONPROFILEID  =  WORKFLOWTASK.INSTITUTIONPROFILEID) AND " +
  "(ASSIGNEDTASKSWORKQUEUE.TASKID  =  WORKFLOWTASK.TASKID) ";

	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();

  //--Release2.1--//
  //// 5. Redefine the QUALIFIED_COLUMN_DFPRIORITYDESCRIPTION fields follow the #1,2.
	////public static final String QUALIFIED_COLUMN_DFDESCRIPTION="TASKSTATUS.TSDESCRIPTION";
	////public static final String COLUMN_DFDESCRIPTION="TSDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFDESCRIPTION="ASSIGNEDTASKSWORKQUEUE.TASKSTATUSID_STR";
	public static final String COLUMN_DFDESCRIPTION="TASKSTATUSID_STR";

	public static final String QUALIFIED_COLUMN_DFASSIGNTASKID="ASSIGNEDTASKSWORKQUEUE.ASSIGNEDTASKSWORKQUEUEID";
	public static final String COLUMN_DFASSIGNTASKID="ASSIGNEDTASKSWORKQUEUEID";

  //--Release2.1--//
  //// 5. Redefine the QUALIFIED_COLUMN_DFPRIORITYDESCRIPTION fields follow the #1,2.
	////public static final String QUALIFIED_COLUMN_DFTMDESCRIPTION="TIMEMILESTONE.TMDESCRIPTION";
	////public static final String COLUMN_DFTMDESCRIPTION="TMDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFTMDESCRIPTION="ASSIGNEDTASKSWORKQUEUE.TIMEMILESTONEID_STR";
	public static final String COLUMN_DFTMDESCRIPTION="TIMEMILESTONEID_STR";

	public static final String QUALIFIED_COLUMN_DFTASKNAME="WORKFLOWTASK.TASKNAME";
	public static final String COLUMN_DFTASKNAME="TASKNAME";

  //--Release2.1--//
  //// 5. Redefine the QUALIFIED_COLUMN_DFPRIORITYDESCRIPTION fields follow the #1,2.
	////public static final String QUALIFIED_COLUMN_DFPRIORITYDESCRIPTION="PRIORITY.PDESCRIPTION";
	////public static final String COLUMN_DFPRIORITYDESCRIPTION="PDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFPRIORITYDESCRIPTION="ASSIGNEDTASKSWORKQUEUE.PRIORITYID_STR";
	public static final String COLUMN_DFPRIORITYDESCRIPTION="PRIORITYID_STR";

  //--Release2.1--//
  //// 6. Add new fiels taskId to maintain the translation via the BXResoure.
	public static final String QUALIFIED_COLUMN_DFTASKID="ASSIGNEDTASKSWORKQUEUE.TASKID";
	public static final String COLUMN_DFTASKID="TASKID";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;




	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDESCRIPTION,
				COLUMN_DFDESCRIPTION,
				QUALIFIED_COLUMN_DFDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFASSIGNTASKID,
				COLUMN_DFASSIGNTASKID,
				QUALIFIED_COLUMN_DFASSIGNTASKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTMDESCRIPTION,
				COLUMN_DFTMDESCRIPTION,
				QUALIFIED_COLUMN_DFTMDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTASKNAME,
				COLUMN_DFTASKNAME,
				QUALIFIED_COLUMN_DFTASKNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIORITYDESCRIPTION,
				COLUMN_DFPRIORITYDESCRIPTION,
				QUALIFIED_COLUMN_DFPRIORITYDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

   	//--Release2.1--//
    //// 6. Add new fiels taskId to maintain the translation via the BXResoure.
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTASKID,
				COLUMN_DFTASKID,
				QUALIFIED_COLUMN_DFTASKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

