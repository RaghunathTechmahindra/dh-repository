package mosApp.MosSystem;

import java.sql.SQLException;

import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doViewAppraiserInfoModelImpl extends QueryModelBase
	implements doViewAppraiserInfoModel
{
	/**
	 *
	 *
	 */
	public doViewAppraiserInfoModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{

	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAppraiserContactId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFAPPRAISERCONTACTID);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserContactId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFAPPRAISERCONTACTID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAppraiserFirstName()
	{
		return (String)getValue(FIELD_DFAPPRAISERFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserFirstName(String value)
	{
		setValue(FIELD_DFAPPRAISERFIRSTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAppraiserInitial()
	{
		return (String)getValue(FIELD_DFAPPRAISERINITIAL);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserInitial(String value)
	{
		setValue(FIELD_DFAPPRAISERINITIAL,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAppraiserLastName()
	{
		return (String)getValue(FIELD_DFAPPRAISERLASTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserLastName(String value)
	{
		setValue(FIELD_DFAPPRAISERLASTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAppraiserPhoneNumber()
	{
		return (String)getValue(FIELD_DFAPPRAISERPHONENUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserPhoneNumber(String value)
	{
		setValue(FIELD_DFAPPRAISERPHONENUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAppraiserFaxNumber()
	{
		return (String)getValue(FIELD_DFAPPRAISERFAXNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserFaxNumber(String value)
	{
		setValue(FIELD_DFAPPRAISERFAXNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAppraiserPhoneExtension()
	{
		return (String)getValue(FIELD_DFAPPRAISERPHONEEXTENSION);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserPhoneExtension(String value)
	{
		setValue(FIELD_DFAPPRAISERPHONEEXTENSION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAppraiserAddr1()
	{
		return (String)getValue(FIELD_DFAPPRAISERADDR1);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserAddr1(String value)
	{
		setValue(FIELD_DFAPPRAISERADDR1,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAppraiserAddr2()
	{
		return (String)getValue(FIELD_DFAPPRAISERADDR2);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserAddr2(String value)
	{
		setValue(FIELD_DFAPPRAISERADDR2,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAppraiserCity()
	{
		return (String)getValue(FIELD_DFAPPRAISERCITY);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserCity(String value)
	{
		setValue(FIELD_DFAPPRAISERCITY,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAppraiserPostFSA()
	{
		return (String)getValue(FIELD_DFAPPRAISERPOSTFSA);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserPostFSA(String value)
	{
		setValue(FIELD_DFAPPRAISERPOSTFSA,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAppraiserPostLDU()
	{
		return (String)getValue(FIELD_DFAPPRAISERPOSTLDU);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserPostLDU(String value)
	{
		setValue(FIELD_DFAPPRAISERPOSTLDU,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAppraiserPartytypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFAPPRAISERPARTYTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfAppraiserPartytypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFAPPRAISERPARTYTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


    public java.math.BigDecimal getDfInstitutionId() {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
      }

      public void setDfInstitutionId(java.math.BigDecimal value) {
        setValue(FIELD_DFINSTITUTIONID, value);
      }

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  public static final String SELECT_SQL_TEMPLATE=
    "SELECT DISTINCT CONTACT.CONTACTID, CONTACT.CONTACTFIRSTNAME, CONTACT.CONTACTMIDDLEINITIAL, "+
    "CONTACT.CONTACTLASTNAME, CONTACT.CONTACTPHONENUMBER, CONTACT.CONTACTFAXNUMBER, "+
    "CONTACT.CONTACTPHONENUMBEREXTENSION, ADDR.ADDRESSLINE1, ADDR.ADDRESSLINE2, "+
    "ADDR.CITY, ADDR.POSTALFSA, ADDR.POSTALLDU, PARTYPROFILE.PARTYTYPEID, "+
    "PROPERTY.DEALID, PROPERTY.PROPERTYID, PROPERTY.COPYID,PARTYDEALASSOC.INSTITUTIONPROFILEID FROM PARTYDEALASSOC, "+
    "PARTYPROFILE, PROPERTY, CONTACT, ADDR  __WHERE__  ";

  public static final String MODIFYING_QUERY_TABLE_NAME="PARTYDEALASSOC, PARTYPROFILE, PROPERTY, CONTACT, ADDR";
	
  public static final String STATIC_WHERE_CRITERIA="( ( (PARTYDEALASSOC.PARTYPROFILEID  =  PARTYPROFILE.PARTYPROFILEID)  AND  (PARTYDEALASSOC.DEALID  =  PROPERTY.DEALID)  AND  (PARTYDEALASSOC.PROPERTYID  =  PROPERTY.PROPERTYID)  AND  (PARTYPROFILE.CONTACTID  =  CONTACT.CONTACTID)  AND  (CONTACT.ADDRID  =  ADDR.ADDRID)  AND    (PARTYDEALASSOC.INSTITUTIONPROFILEID  =  PARTYPROFILE.INSTITUTIONPROFILEID)  AND  (PARTYDEALASSOC.INSTITUTIONPROFILEID  =  PROPERTY.INSTITUTIONPROFILEID)  AND  (PARTYPROFILE.INSTITUTIONPROFILEID  =  CONTACT.INSTITUTIONPROFILEID)  AND  (CONTACT.INSTITUTIONPROFILEID  =  ADDR.INSTITUTIONPROFILEID) ) AND PARTYPROFILE.PARTYTYPEID = 51 )";

	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFAPPRAISERCONTACTID="CONTACT.CONTACTID";
	public static final String COLUMN_DFAPPRAISERCONTACTID="CONTACTID";
	public static final String QUALIFIED_COLUMN_DFAPPRAISERFIRSTNAME="CONTACT.CONTACTFIRSTNAME";
	public static final String COLUMN_DFAPPRAISERFIRSTNAME="CONTACTFIRSTNAME";
	public static final String QUALIFIED_COLUMN_DFAPPRAISERINITIAL="CONTACT.CONTACTMIDDLEINITIAL";
	public static final String COLUMN_DFAPPRAISERINITIAL="CONTACTMIDDLEINITIAL";
	public static final String QUALIFIED_COLUMN_DFAPPRAISERLASTNAME="CONTACT.CONTACTLASTNAME";
	public static final String COLUMN_DFAPPRAISERLASTNAME="CONTACTLASTNAME";
	public static final String QUALIFIED_COLUMN_DFAPPRAISERPHONENUMBER="CONTACT.CONTACTPHONENUMBER";
	public static final String COLUMN_DFAPPRAISERPHONENUMBER="CONTACTPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFAPPRAISERFAXNUMBER="CONTACT.CONTACTFAXNUMBER";
	public static final String COLUMN_DFAPPRAISERFAXNUMBER="CONTACTFAXNUMBER";
	public static final String QUALIFIED_COLUMN_DFAPPRAISERPHONEEXTENSION="CONTACT.CONTACTPHONENUMBEREXTENSION";
	public static final String COLUMN_DFAPPRAISERPHONEEXTENSION="CONTACTPHONENUMBEREXTENSION";
	public static final String QUALIFIED_COLUMN_DFAPPRAISERADDR1="ADDR.ADDRESSLINE1";
	public static final String COLUMN_DFAPPRAISERADDR1="ADDRESSLINE1";
	public static final String QUALIFIED_COLUMN_DFAPPRAISERADDR2="ADDR.ADDRESSLINE2";
	public static final String COLUMN_DFAPPRAISERADDR2="ADDRESSLINE2";
	public static final String QUALIFIED_COLUMN_DFAPPRAISERCITY="ADDR.CITY";
	public static final String COLUMN_DFAPPRAISERCITY="CITY";
	public static final String QUALIFIED_COLUMN_DFAPPRAISERPOSTFSA="ADDR.POSTALFSA";
	public static final String COLUMN_DFAPPRAISERPOSTFSA="POSTALFSA";
	public static final String QUALIFIED_COLUMN_DFAPPRAISERPOSTLDU="ADDR.POSTALLDU";
	public static final String COLUMN_DFAPPRAISERPOSTLDU="POSTALLDU";
	public static final String QUALIFIED_COLUMN_DFAPPRAISERPARTYTYPEID="PARTYPROFILE.PARTYTYPEID";
	public static final String COLUMN_DFAPPRAISERPARTYTYPEID="PARTYTYPEID";
	public static final String QUALIFIED_COLUMN_DFDEALID="PROPERTY.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYID="PROPERTY.PROPERTYID";
	public static final String COLUMN_DFPROPERTYID="PROPERTYID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="PROPERTY.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";

    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "PROPERTYEXPENSE.INSTITUTIONPROFILEID";
      public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERCONTACTID,
				COLUMN_DFAPPRAISERCONTACTID,
				QUALIFIED_COLUMN_DFAPPRAISERCONTACTID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERFIRSTNAME,
				COLUMN_DFAPPRAISERFIRSTNAME,
				QUALIFIED_COLUMN_DFAPPRAISERFIRSTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERINITIAL,
				COLUMN_DFAPPRAISERINITIAL,
				QUALIFIED_COLUMN_DFAPPRAISERINITIAL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERLASTNAME,
				COLUMN_DFAPPRAISERLASTNAME,
				QUALIFIED_COLUMN_DFAPPRAISERLASTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERPHONENUMBER,
				COLUMN_DFAPPRAISERPHONENUMBER,
				QUALIFIED_COLUMN_DFAPPRAISERPHONENUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERFAXNUMBER,
				COLUMN_DFAPPRAISERFAXNUMBER,
				QUALIFIED_COLUMN_DFAPPRAISERFAXNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERPHONEEXTENSION,
				COLUMN_DFAPPRAISERPHONEEXTENSION,
				QUALIFIED_COLUMN_DFAPPRAISERPHONEEXTENSION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERADDR1,
				COLUMN_DFAPPRAISERADDR1,
				QUALIFIED_COLUMN_DFAPPRAISERADDR1,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERADDR2,
				COLUMN_DFAPPRAISERADDR2,
				QUALIFIED_COLUMN_DFAPPRAISERADDR2,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERCITY,
				COLUMN_DFAPPRAISERCITY,
				QUALIFIED_COLUMN_DFAPPRAISERCITY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERPOSTFSA,
				COLUMN_DFAPPRAISERPOSTFSA,
				QUALIFIED_COLUMN_DFAPPRAISERPOSTFSA,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERPOSTLDU,
				COLUMN_DFAPPRAISERPOSTLDU,
				QUALIFIED_COLUMN_DFAPPRAISERPOSTLDU,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPRAISERPARTYTYPEID,
				COLUMN_DFAPPRAISERPARTYTYPEID,
				QUALIFIED_COLUMN_DFAPPRAISERPARTYTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYID,
				COLUMN_DFPROPERTYID,
				QUALIFIED_COLUMN_DFPROPERTYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	      FIELD_SCHEMA.addFieldDescriptor(
	                new QueryFieldDescriptor(
	                  FIELD_DFINSTITUTIONID,
	                  COLUMN_DFINSTITUTIONID,
	                  QUALIFIED_COLUMN_DFINSTITUTIONID,
	                  java.math.BigDecimal.class,
	                  false,
	                  false,
	                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	                  "",
	                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	                  ""));
	}

}

