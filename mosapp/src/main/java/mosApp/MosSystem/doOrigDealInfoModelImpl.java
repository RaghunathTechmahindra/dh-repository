package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doOrigDealInfoModelImpl extends QueryModelBase
	implements doOrigDealInfoModel
{
	/**
	 *
	 *
	 */
	public doOrigDealInfoModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
		// SEAN Ticket #1786 July 29, 2005: get the description based on the language id.
		//Get the Language ID from the SessionStateModel
		String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
		SessionStateModelImpl theSessionState =
			(SessionStateModelImpl) getRequestContext().
			getModelManager().getModel(SessionStateModel.class,
									   defaultInstanceStateName,
									   true);
		int languageId = theSessionState.getLanguageId();
		while(this.next()) {
			// convert the status.
			this.setDfStatusDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),"STATUS", this.getDfStatusDesc(),
																	languageId));
		}
		// SEAN Ticket #1786 END.
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStatusDesc()
	{
		return (String)getValue(FIELD_DFSTATUSDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfStatusDesc(String value)
	{
		setValue(FIELD_DFSTATUSDESC,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSourceApplicationId()
	{
		return (String)getValue(FIELD_DFSOURCEAPPLICATIONID);
	}


	/**
	 *
	 *
	 */
	public void setDfSourceApplicationId(String value)
	{
		setValue(FIELD_DFSOURCEAPPLICATIONID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSOBProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSOBPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfSOBProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSOBPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfApplicationDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFAPPLICATIONDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfApplicationDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFAPPLICATIONDATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfUnderwriterProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFUNDERWRITERPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfUnderwriterProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFUNDERWRITERPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAdministratorProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFADMINISTRATORPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfAdministratorProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFADMINISTRATORPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfFunderProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFFUNDERPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfFunderProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFFUNDERPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSOBFirstName()
	{
		return (String)getValue(FIELD_DFSOBFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfSOBFirstName(String value)
	{
		setValue(FIELD_DFSOBFIRSTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSOBLastName()
	{
		return (String)getValue(FIELD_DFSOBLASTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfSOBLastName(String value)
	{
		setValue(FIELD_DFSOBLASTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSOBPhoneNumber()
	{
		return (String)getValue(FIELD_DFSOBPHONENUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfSOBPhoneNumber(String value)
	{
		setValue(FIELD_DFSOBPHONENUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSOBFaxNumber()
	{
		return (String)getValue(FIELD_DFSOBFAXNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfSOBFaxNumber(String value)
	{
		setValue(FIELD_DFSOBFAXNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSOBPhoneExt()
	{
		return (String)getValue(FIELD_DFSOBPHONEEXT);
	}


	/**
	 *
	 *
	 */
	public void setDfSOBPhoneExt(String value)
	{
		setValue(FIELD_DFSOBPHONEEXT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfNewDealId()
	{
		return (String)getValue(FIELD_DFNEWDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfNewDealId(String value)
	{
		setValue(FIELD_DFNEWDEALID,value);
	}

    public java.math.BigDecimal getDfInstitutionId() {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
      }

      public void setDfInstitutionId(java.math.BigDecimal value) {
        setValue(FIELD_DFINSTITUTIONID, value);
      }


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	// SEAN Ticket #1786 July 29, 2005: update the SQL template.
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL DEAL.DEALID, DEAL.COPYID, STATUS.STATUSDESCRIPTION, DEAL.SOURCEAPPLICATIONID, DEAL.SOURCEOFBUSINESSPROFILEID, DEAL.APPLICATIONDATE, DEAL.UNDERWRITERUSERID, DEAL.ADMINISTRATORID, DEAL.FUNDERPROFILEID, CONTACT.CONTACTFIRSTNAME, CONTACT.CONTACTLASTNAME, CONTACT.CONTACTPHONENUMBER, CONTACT.CONTACTFAXNUMBER, CONTACT.CONTACTPHONENUMBEREXTENSION, DEAL.REFERENCEDEALNUMBER, " +
		"to_char(STATUS.STATUSID) STATUSID_STR, DEAL.INSTITUTIONPROFILEID " +  
		"FROM DEAL, SOURCEOFBUSINESSPROFILE, CONTACT, STATUS  __WHERE__  ";
	// SEAN Ticket #1786 ENDD
	public static final String MODIFYING_QUERY_TABLE_NAME="DEAL, SOURCEOFBUSINESSPROFILE, CONTACT, STATUS";

	public static final String STATIC_WHERE_CRITERIA=" (DEAL.SOURCEOFBUSINESSPROFILEID  =  SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPROFILEID) AND (DEAL.INSTITUTIONPROFILEID  =  SOURCEOFBUSINESSPROFILE.INSTITUTIONPROFILEID) AND (SOURCEOFBUSINESSPROFILE.CONTACTID  =  CONTACT.CONTACTID) AND (SOURCEOFBUSINESSPROFILE.INSTITUTIONPROFILEID  =  CONTACT.INSTITUTIONPROFILEID) AND (DEAL.STATUSID  =  STATUS.STATUSID)";
	
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	// SEAN Ticket #1786 July 29, 2005: update the column define.
	public static final String QUALIFIED_COLUMN_DFSTATUSDESC="STATUS.STATUSID_STR";
	public static final String COLUMN_DFSTATUSDESC="STATUSID_STR";
	// SEAN Ticket #1786 END
	public static final String QUALIFIED_COLUMN_DFSOURCEAPPLICATIONID="DEAL.SOURCEAPPLICATIONID";
	public static final String COLUMN_DFSOURCEAPPLICATIONID="SOURCEAPPLICATIONID";
	public static final String QUALIFIED_COLUMN_DFSOBPROFILEID="DEAL.SOURCEOFBUSINESSPROFILEID";
	public static final String COLUMN_DFSOBPROFILEID="SOURCEOFBUSINESSPROFILEID";
	public static final String QUALIFIED_COLUMN_DFAPPLICATIONDATE="DEAL.APPLICATIONDATE";
	public static final String COLUMN_DFAPPLICATIONDATE="APPLICATIONDATE";
	public static final String QUALIFIED_COLUMN_DFUNDERWRITERPROFILEID="DEAL.UNDERWRITERUSERID";
	public static final String COLUMN_DFUNDERWRITERPROFILEID="UNDERWRITERUSERID";
	public static final String QUALIFIED_COLUMN_DFADMINISTRATORPROFILEID="DEAL.ADMINISTRATORID";
	public static final String COLUMN_DFADMINISTRATORPROFILEID="ADMINISTRATORID";
	public static final String QUALIFIED_COLUMN_DFFUNDERPROFILEID="DEAL.FUNDERPROFILEID";
	public static final String COLUMN_DFFUNDERPROFILEID="FUNDERPROFILEID";
	public static final String QUALIFIED_COLUMN_DFSOBFIRSTNAME="CONTACT.CONTACTFIRSTNAME";
	public static final String COLUMN_DFSOBFIRSTNAME="CONTACTFIRSTNAME";
	public static final String QUALIFIED_COLUMN_DFSOBLASTNAME="CONTACT.CONTACTLASTNAME";
	public static final String COLUMN_DFSOBLASTNAME="CONTACTLASTNAME";
	public static final String QUALIFIED_COLUMN_DFSOBPHONENUMBER="CONTACT.CONTACTPHONENUMBER";
	public static final String COLUMN_DFSOBPHONENUMBER="CONTACTPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFSOBFAXNUMBER="CONTACT.CONTACTFAXNUMBER";
	public static final String COLUMN_DFSOBFAXNUMBER="CONTACTFAXNUMBER";
	public static final String QUALIFIED_COLUMN_DFSOBPHONEEXT="CONTACT.CONTACTPHONENUMBEREXTENSION";
	public static final String COLUMN_DFSOBPHONEEXT="CONTACTPHONENUMBEREXTENSION";
	public static final String QUALIFIED_COLUMN_DFNEWDEALID="DEAL.REFERENCEDEALNUMBER";
	public static final String COLUMN_DFNEWDEALID="REFERENCEDEALNUMBER";

    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "DEAL.INSTITUTIONPROFILEID";
  public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTATUSDESC,
				COLUMN_DFSTATUSDESC,
				QUALIFIED_COLUMN_DFSTATUSDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOURCEAPPLICATIONID,
				COLUMN_DFSOURCEAPPLICATIONID,
				QUALIFIED_COLUMN_DFSOURCEAPPLICATIONID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOBPROFILEID,
				COLUMN_DFSOBPROFILEID,
				QUALIFIED_COLUMN_DFSOBPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPLICATIONDATE,
				COLUMN_DFAPPLICATIONDATE,
				QUALIFIED_COLUMN_DFAPPLICATIONDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUNDERWRITERPROFILEID,
				COLUMN_DFUNDERWRITERPROFILEID,
				QUALIFIED_COLUMN_DFUNDERWRITERPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADMINISTRATORPROFILEID,
				COLUMN_DFADMINISTRATORPROFILEID,
				QUALIFIED_COLUMN_DFADMINISTRATORPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFUNDERPROFILEID,
				COLUMN_DFFUNDERPROFILEID,
				QUALIFIED_COLUMN_DFFUNDERPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOBFIRSTNAME,
				COLUMN_DFSOBFIRSTNAME,
				QUALIFIED_COLUMN_DFSOBFIRSTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOBLASTNAME,
				COLUMN_DFSOBLASTNAME,
				QUALIFIED_COLUMN_DFSOBLASTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOBPHONENUMBER,
				COLUMN_DFSOBPHONENUMBER,
				QUALIFIED_COLUMN_DFSOBPHONENUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOBFAXNUMBER,
				COLUMN_DFSOBFAXNUMBER,
				QUALIFIED_COLUMN_DFSOBFAXNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOBPHONEEXT,
				COLUMN_DFSOBPHONEEXT,
				QUALIFIED_COLUMN_DFSOBPHONEEXT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNEWDEALID,
				COLUMN_DFNEWDEALID,
				QUALIFIED_COLUMN_DFNEWDEALID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                  FIELD_DFINSTITUTIONID,
                  COLUMN_DFINSTITUTIONID,
                  QUALIFIED_COLUMN_DFINSTITUTIONID,
                  java.math.BigDecimal.class,
                  false,
                  false,
                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                  "",
                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                  ""));
	}

}

