package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.CheckBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.StaticTextField;


/**
 *
 *
 *
 */
public class pgDocTrackingRepeated3TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDocTrackingRepeated3TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(100);
		setPrimaryModelClass( doDocTrackFunderCondModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStFunderDocumentLabel().setValue(CHILD_STFUNDERDOCUMENTLABEL_RESET_VALUE);
		getCkFunderDocStatus().setValue(CHILD_CKFUNDERDOCSTATUS_RESET_VALUE);
		getStFunderDocStatusDate().setValue(CHILD_STFUNDERDOCSTATUSDATE_RESET_VALUE);
		getStFunderByPass().setValue(CHILD_STFUNDERBYPASS_RESET_VALUE);
		getStFunderDueDate().setValue(CHILD_STFUNDERDUEDATE_RESET_VALUE);
		getStFunderResponsibility().setValue(CHILD_STFUNDERRESPONSIBILITY_RESET_VALUE);
		getHdFunderDocTrackId().setValue(CHILD_HDFUNDERDOCTRACKID_RESET_VALUE);
		getHdFunderCopyId().setValue(CHILD_HDFUNDERCOPYID_RESET_VALUE);
		getHdFunderRoleId().setValue(CHILD_HDFUNDERROLEID_RESET_VALUE);
		getHdFunderRequestDate().setValue(CHILD_HDFUNDERREQUESTDATE_RESET_VALUE);
		getHdFunderStatusDate().setValue(CHILD_HDFUNDERSTATUSDATE_RESET_VALUE);
		getHdFunderDocStatus().setValue(CHILD_HDFUNDERDOCSTATUS_RESET_VALUE);
		getHdFunderDocText().setValue(CHILD_HDFUNDERDOCTEXT_RESET_VALUE);
		getHdFunderResponsibility().setValue(CHILD_HDFUNDERRESPONSIBILITY_RESET_VALUE);
		getHdFunderDocumentLabel().setValue(CHILD_HDFUNDERDOCUMENTLABEL_RESET_VALUE);
		getBtFunderDetails().setValue(CHILD_BTFUNDERDETAILS_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STFUNDERDOCUMENTLABEL,StaticTextField.class);
		registerChild(CHILD_CKFUNDERDOCSTATUS,CheckBox.class);
		registerChild(CHILD_STFUNDERDOCSTATUSDATE,StaticTextField.class);
		registerChild(CHILD_STFUNDERBYPASS,StaticTextField.class);
		registerChild(CHILD_STFUNDERDUEDATE,StaticTextField.class);
		registerChild(CHILD_STFUNDERRESPONSIBILITY,StaticTextField.class);
		registerChild(CHILD_HDFUNDERDOCTRACKID,HiddenField.class);
		registerChild(CHILD_HDFUNDERCOPYID,HiddenField.class);
		registerChild(CHILD_HDFUNDERROLEID,HiddenField.class);
		registerChild(CHILD_HDFUNDERREQUESTDATE,HiddenField.class);
		registerChild(CHILD_HDFUNDERSTATUSDATE,HiddenField.class);
		registerChild(CHILD_HDFUNDERDOCSTATUS,HiddenField.class);
		registerChild(CHILD_HDFUNDERDOCTEXT,HiddenField.class);
		registerChild(CHILD_HDFUNDERRESPONSIBILITY,HiddenField.class);
		registerChild(CHILD_HDFUNDERDOCUMENTLABEL,HiddenField.class);
		registerChild(CHILD_BTFUNDERDETAILS,Button.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
//				modelList.add(getdoDocTrackFunderCondModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
  public boolean nextTile()
    throws ModelControlException
  {
    // This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
    boolean movedToRow = super.nextTile();

    if (movedToRow)
    {
      int rowNum = getTileIndex();
      boolean ret = true;

      DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();
      handler.pageGetState(this.getParentViewBean());

      logger = SysLog.getSysLogger("pgDTR3TV");

      // Get Data Object
      doDocTrackFunderCondModelImpl theObj =(doDocTrackFunderCondModelImpl) this.getdoDocTrackFunderCondModel();

      if (theObj.getSize() > 0)
      {
        setDisplayFieldValue(CHILD_CKFUNDERDOCSTATUS,
                             CHILD_CKFUNDERDOCSTATUS_RESET_VALUE);

        ////logger.debug("pgDTR3TV@nextTile::chFunderValue_After[ " + rowNum + "]: "
        ////            + getCkFunderDocStatus().getValue().toString());

        handler.populateDisplayRowFunder(rowNum, "Repeated3", theObj);
      }
      else ret = false;

      handler.pageSaveState();

      return ret;
    }
    return movedToRow;
  }


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STFUNDERDOCUMENTLABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDocTrackFunderCondModel(),
				CHILD_STFUNDERDOCUMENTLABEL,
				doDocTrackFunderCondModel.FIELD_DFDOCUMENTLABEL,
				CHILD_STFUNDERDOCUMENTLABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CKFUNDERDOCSTATUS))
		{
			CheckBox child = new CheckBox(this,
				////getDefaultModel(),
        getdoDocTrackFunderCondModel(),
				CHILD_CKFUNDERDOCSTATUS,
				CHILD_CKFUNDERDOCSTATUS,
				"T","F",
				false,
				null);

			return child;
		}
		else
		if (name.equals(CHILD_STFUNDERDOCSTATUSDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDocTrackFunderCondModel(),
				CHILD_STFUNDERDOCSTATUSDATE,
				doDocTrackFunderCondModel.FIELD_DFSTATUSCHANGEDATE,
				CHILD_STFUNDERDOCSTATUSDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STFUNDERBYPASS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STFUNDERBYPASS,
				CHILD_STFUNDERBYPASS,
				CHILD_STFUNDERBYPASS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STFUNDERDUEDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDocTrackFunderCondModel(),
				CHILD_STFUNDERDUEDATE,
				doDocTrackFunderCondModel.FIELD_DFDOCDUEDATE,
				CHILD_STFUNDERDUEDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STFUNDERRESPONSIBILITY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDocTrackFunderCondModel(),
				CHILD_STFUNDERRESPONSIBILITY,
				doDocTrackFunderCondModel.FIELD_DFRESPONSIBILITY,
				CHILD_STFUNDERRESPONSIBILITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDFUNDERDOCTRACKID))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackFunderCondModel(),
				CHILD_HDFUNDERDOCTRACKID,
				doDocTrackFunderCondModel.FIELD_DFDOCTRACKID,
				CHILD_HDFUNDERDOCTRACKID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDFUNDERCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackFunderCondModel(),
				CHILD_HDFUNDERCOPYID,
				doDocTrackFunderCondModel.FIELD_DFCOPYID,
				CHILD_HDFUNDERCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDFUNDERROLEID))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackFunderCondModel(),
				CHILD_HDFUNDERROLEID,
				doDocTrackFunderCondModel.FIELD_DFROLEID,
				CHILD_HDFUNDERROLEID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDFUNDERREQUESTDATE))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackFunderCondModel(),
				CHILD_HDFUNDERREQUESTDATE,
				doDocTrackFunderCondModel.FIELD_DFREQUESTDATE,
				CHILD_HDFUNDERREQUESTDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDFUNDERSTATUSDATE))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackFunderCondModel(),
				CHILD_HDFUNDERSTATUSDATE,
				doDocTrackFunderCondModel.FIELD_DFDOCDUEDATE,
				CHILD_HDFUNDERSTATUSDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDFUNDERDOCSTATUS))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackFunderCondModel(),
				CHILD_HDFUNDERDOCSTATUS,
				doDocTrackFunderCondModel.FIELD_DFDOCSTATUSID,
				CHILD_HDFUNDERDOCSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDFUNDERDOCTEXT))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackFunderCondModel(),
				CHILD_HDFUNDERDOCTEXT,
				doDocTrackFunderCondModel.FIELD_DFDOCUMENTTEXT,
				CHILD_HDFUNDERDOCTEXT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDFUNDERRESPONSIBILITY))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackFunderCondModel(),
				CHILD_HDFUNDERRESPONSIBILITY,
				doDocTrackFunderCondModel.FIELD_DFRESPONSIBILITY,
				CHILD_HDFUNDERRESPONSIBILITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDFUNDERDOCUMENTLABEL))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackFunderCondModel(),
				CHILD_HDFUNDERDOCUMENTLABEL,
				doDocTrackFunderCondModel.FIELD_DFDOCUMENTLABEL,
				CHILD_HDFUNDERDOCUMENTLABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTFUNDERDETAILS))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTFUNDERDETAILS,
				CHILD_BTFUNDERDETAILS,
				CHILD_BTFUNDERDETAILS_RESET_VALUE,
				null);
				return child;

		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStFunderDocumentLabel()
	{
		return (StaticTextField)getChild(CHILD_STFUNDERDOCUMENTLABEL);
	}


	/**
	 *
	 *
	 */
	public CheckBox getCkFunderDocStatus()
	{
		return (CheckBox)getChild(CHILD_CKFUNDERDOCSTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStFunderDocStatusDate()
	{
		return (StaticTextField)getChild(CHILD_STFUNDERDOCSTATUSDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStFunderByPass()
	{
		return (StaticTextField)getChild(CHILD_STFUNDERBYPASS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStFunderDueDate()
	{
		return (StaticTextField)getChild(CHILD_STFUNDERDUEDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStFunderResponsibility()
	{
		return (StaticTextField)getChild(CHILD_STFUNDERRESPONSIBILITY);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdFunderDocTrackId()
	{
		return (HiddenField)getChild(CHILD_HDFUNDERDOCTRACKID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdFunderCopyId()
	{
		return (HiddenField)getChild(CHILD_HDFUNDERCOPYID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdFunderRoleId()
	{
		return (HiddenField)getChild(CHILD_HDFUNDERROLEID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdFunderRequestDate()
	{
		return (HiddenField)getChild(CHILD_HDFUNDERREQUESTDATE);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdFunderStatusDate()
	{
		return (HiddenField)getChild(CHILD_HDFUNDERSTATUSDATE);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdFunderDocStatus()
	{
		return (HiddenField)getChild(CHILD_HDFUNDERDOCSTATUS);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdFunderDocText()
	{
		return (HiddenField)getChild(CHILD_HDFUNDERDOCTEXT);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdFunderResponsibility()
	{
		return (HiddenField)getChild(CHILD_HDFUNDERRESPONSIBILITY);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdFunderDocumentLabel()
	{
		return (HiddenField)getChild(CHILD_HDFUNDERDOCUMENTLABEL);
	}


	/**
	 *
	 *
	 */
	public void handleBtFunderDetailsRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this.getParentViewBean());

		handler.navigateToDetail(DocTrackingHandler.SECTION_FUNDER, handler.getRowNdxFromWebEventMethod(event));

		handler.postHandlerProtocol();

		// The following code block was migrated from the btFunderDetails_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.navigateToDetail(DocTrackingHandler.SECTION_FUNDER, handler.getRowNdxFromWebEventMethod(event));

		handler.postHandlerProtocol();

		return;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtFunderDetails()
	{
		return (Button)getChild(CHILD_BTFUNDERDETAILS);
	}


	/**
	 *
	 *
	 */
	public doDocTrackFunderCondModel getdoDocTrackFunderCondModel()
	{
		if (doDocTrackFunderCond == null)
			doDocTrackFunderCond = (doDocTrackFunderCondModel) getModel(doDocTrackFunderCondModel.class);
		return doDocTrackFunderCond;
	}


	/**
	 *
	 *
	 */
	public void setdoDocTrackFunderCondModel(doDocTrackFunderCondModel model)
	{
			doDocTrackFunderCond = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STFUNDERDOCUMENTLABEL="stFunderDocumentLabel";
	public static final String CHILD_STFUNDERDOCUMENTLABEL_RESET_VALUE="";
	public static final String CHILD_CKFUNDERDOCSTATUS="ckFunderDocStatus";
	public static final String CHILD_CKFUNDERDOCSTATUS_RESET_VALUE="F";
	public static final String CHILD_STFUNDERDOCSTATUSDATE="stFunderDocStatusDate";
	public static final String CHILD_STFUNDERDOCSTATUSDATE_RESET_VALUE="";
	public static final String CHILD_STFUNDERBYPASS="stFunderByPass";
	public static final String CHILD_STFUNDERBYPASS_RESET_VALUE="";
	public static final String CHILD_STFUNDERDUEDATE="stFunderDueDate";
	public static final String CHILD_STFUNDERDUEDATE_RESET_VALUE="";
	public static final String CHILD_STFUNDERRESPONSIBILITY="stFunderResponsibility";
	public static final String CHILD_STFUNDERRESPONSIBILITY_RESET_VALUE="";
	public static final String CHILD_HDFUNDERDOCTRACKID="hdFunderDocTrackId";
	public static final String CHILD_HDFUNDERDOCTRACKID_RESET_VALUE="";
	public static final String CHILD_HDFUNDERCOPYID="hdFunderCopyId";
	public static final String CHILD_HDFUNDERCOPYID_RESET_VALUE="";
	public static final String CHILD_HDFUNDERROLEID="hdFunderRoleId";
	public static final String CHILD_HDFUNDERROLEID_RESET_VALUE="";
	public static final String CHILD_HDFUNDERREQUESTDATE="hdFunderRequestDate";
	public static final String CHILD_HDFUNDERREQUESTDATE_RESET_VALUE="";
	public static final String CHILD_HDFUNDERSTATUSDATE="hdFunderStatusDate";
	public static final String CHILD_HDFUNDERSTATUSDATE_RESET_VALUE="";
	public static final String CHILD_HDFUNDERDOCSTATUS="hdFunderDocStatus";
	public static final String CHILD_HDFUNDERDOCSTATUS_RESET_VALUE="";
	public static final String CHILD_HDFUNDERDOCTEXT="hdFunderDocText";
	public static final String CHILD_HDFUNDERDOCTEXT_RESET_VALUE="";
	public static final String CHILD_HDFUNDERRESPONSIBILITY="hdFunderResponsibility";
	public static final String CHILD_HDFUNDERRESPONSIBILITY_RESET_VALUE="";
	public static final String CHILD_HDFUNDERDOCUMENTLABEL="hdFunderDocumentLabel";
	public static final String CHILD_HDFUNDERDOCUMENTLABEL_RESET_VALUE="";
	public static final String CHILD_BTFUNDERDETAILS="btFunderDetails";
	public static final String CHILD_BTFUNDERDETAILS_RESET_VALUE=" ";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDocTrackFunderCondModel doDocTrackFunderCond=null;
	private DocTrackingHandler handler=new DocTrackingHandler();
  public SysLogger logger;


}

