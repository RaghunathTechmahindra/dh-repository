package mosApp.MosSystem;

import java.sql.SQLException;

import MosSystem.Mc;

import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 * <p>Title: doAdjudicationResponseBNCModelImpl</p>
 *
 * <p>Description: Implementation class for doAdjudicationResponseBNCModel</p>
 *
 * <p> Copyright: Filogix Inc. (c) 2006 </p>
 *
 * <p> Company: Filogix Inc.</p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.0
 */
public class doAdjudicationResponseBNCModelImpl extends QueryModelBase
	implements doAdjudicationResponseBNCModel
{
	/**
	 *
	 *
	 */
	public doAdjudicationResponseBNCModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

//   logger = SysLog.getSysLogger("AdjudicationResponseBNCModel");
//   logger.debug("AdjudicationResponseBNCModel@afterExecute::Size: " + sql);

		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
//	    logger = SysLog.getSysLogger("AdjudicationResponseBNCModel");
//	    logger.debug("AdjudicationResponseBNCModel@afterExecute::Size: " + this.getSize());
//	    logger.debug("AdjudicationResponseBNCModel@afterExecute::SQLTemplate: " + this.getSelectSQL());

	    //--Release2.1--//
	    //To override all the Description fields by populating from BXResource
	    //Get the Language ID from the SessionStateModel
	    String defaultInstanceStateName =
				getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
	    SessionStateModelImpl theSessionState =
	        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
	                        SessionStateModel.class,
	                        defaultInstanceStateName,
	                        true);
	    int languageId = theSessionState.getLanguageId();

	    if (this.getSize() == 0)
	    {
	    	// Request Status - set as not submitted
	    	this.setDfRequestStatusDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
	    			"REQUESTSTATUS", "1", languageId));
	    }

	    while(this.next())
	    {
	      //  Request status
	      if (this.getDfRequestStatusDesc().equals("0"))
	      {
	    	  // set as not submitted
	    	this.setDfRequestStatusDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
	    			"REQUESTSTATUS", "1", languageId));
	      } else {
	    	this.setDfRequestStatusDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
		    		"REQUESTSTATUS", this.getDfRequestStatusDesc(), languageId));
	      }

	      // Adjudication Status
	      if (this.getDfAdjudicationDesc() != null)
	      {
	      this.setDfAdjudicationDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
	    			  "ADJUDICATIONSTATUS", this.getDfAdjudicationDesc(), languageId));
	      }

    	  // Credit Bureau
	      if (this.getDfGlobalUsedCreditBureauName() != null)
	      {
    	  this.setDfGlobalUsedCreditBureauName(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
    			  "CREDITBUREAUNAME", this.getDfGlobalUsedCreditBureauName(), languageId));
	      }

	      // Primary Applicant Risk
	      if (this.getDfPrimaryApplicantRisk() != null)
	      {
		      this.setDfPrimaryApplicantRisk(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
		  	      "RISKTYPE", this.getDfPrimaryApplicantRisk(), languageId));
	      }

	      // Secondary Applicant Risk
	      if (this.getDfSecondaryApplicantRisk() != null)
	      {
		      this.setDfSecondaryApplicantRisk(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
		  	      "RISKTYPE", this.getDfSecondaryApplicantRisk(), languageId));
	      }


	      // Market Risk
	      if (this.getDfMarketRisk() != null)
	      {
		      this.setDfMarketRisk(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
		  	      "RISKTYPE", this.getDfMarketRisk(), languageId));
	      }


	      // Neighbourhood Risk
	      if (this.getDfNeighbourhoodRisk() != null)
	      {
		      this.setDfNeighbourhoodRisk(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
		  	      "RISKTYPE", this.getDfNeighbourhoodRisk(), languageId));
	      }

	      // Property Risk
		  if (this.getDfPropertyRisk() != null)
		  {
		      this.setDfPropertyRisk(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
		  	      "RISKTYPE", this.getDfPropertyRisk(), languageId));
		  }

	    }

	    // reset location
	    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfResponseId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFRESPONSEID);
	}

	/**
	 *
	 *
	 */
	public void setDfResponseId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFRESPONSEID, value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRequestId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFREQUESTID);
	}

	/**
	 *
	 *
	 */
	public void setDfRequestId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFREQUESTID, value);
	}


	/**
	 *
	 *
	 */
	public String getDfResponseDate()
	{
		return (String)getValue(FIELD_DFRESPONSEDATE);
	}

	/**
	 *
	 *
	 */
	public void setDfResponseDate(String value)
	{
		setValue(FIELD_DFRESPONSEDATE, value);
	}


	/**
	 *
	 *
	 */
	public String getDfRequestDate()
	{
		return (String)getValue(FIELD_DFREQUESTDATE);
	}

	/**
	 *
	 *
	 */
	public void setDfRequestDate(String value)
	{
		setValue(FIELD_DFREQUESTDATE, value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRequestStatusId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFREQUESTSTATUSID);
	}

	/**
	 *
	 *
	 */
	public void setDfRequestStatusId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFREQUESTSTATUSID, value);
	}


	/**
	 *
	 *
	 */
	public String getDfRequestStatusDesc()
	{
		return (String)getValue(FIELD_DFREQUESTSTATUSDESC);
	}

	/**
	 *
	 *
	 */
	public void setDfRequestStatusDesc(String value)
	{
		setValue(FIELD_DFREQUESTSTATUSDESC, value);
	}



	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfGlobalUsedCreditBureauNameId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFGLOBALUSEDCREDITBUREAUNAMEID);
	}

	/**
	 *
	 *
	 */
	public void setDfGlobalUsedCreditBureauNameId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFGLOBALUSEDCREDITBUREAUNAMEID, value);
	}


	/**
	 *
	 *
	 */
	public String getDfGlobalUsedCreditBureauName()
	{
		return (String)getValue(FIELD_DFGLOBALUSEDCREDITBUREAUNAME);
	}

	/**
	 *
	 *
	 */
	public void setDfGlobalUsedCreditBureauName(String value)
	{
		setValue(FIELD_DFGLOBALUSEDCREDITBUREAUNAME, value);
	}


	/**
	 *
	 *
	 */
	public String getDfGlobalCreditBureauScore()
	{
		return (String)getValue(FIELD_DFGLOBALCREDITBUREAUSCORE);
	}

	/**
	 *
	 *
	 */
	public void setDfGlobalCreditBureauScore(String value)
	{
		setValue(FIELD_DFGLOBALCREDITBUREAUSCORE, value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfGlobalRiskRating()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFGLOBALRISKRATING);
	}

	/**
	 *
	 *
	 */
	public void setDfGlobalRiskRating(java.math.BigDecimal value)
	{
		setValue(FIELD_DFGLOBALRISKRATING, value);
	}


	/**
	 *
	 *
	 */
	public String getDfGlobalInternalScore()
	{
		return (String)getValue(FIELD_DFGLOBALINTERNALSCORE);
	}

	/**
	 *
	 *
	 */
	public void setDfGlobalInternalScore(String value)
	{
		setValue(FIELD_DFGLOBALINTERNALSCORE, value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAdjudicationStatusId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFADJUDICATIONSTATUSID);
	}

	/**
	 *
	 *
	 */
	public void setDfAdjudicationStatusId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFADJUDICATIONSTATUSID, value);
	}


	/**
	 *
	 *
	 */
	public String getDfAdjudicationDesc()
	{
		return (String)getValue(FIELD_DFADJUDICATIONDESC);
	}

	/**
	 *
	 *
	 */
	public void setDfAdjudicationDesc(String value)
	{
		setValue(FIELD_DFADJUDICATIONDESC, value);
	}




	/**
	 *
	 *
	 */
	public String getDfProductCode()
	{
		return (String)getValue(FIELD_DFPRODUCTCODE);
	}

	/**
	 *
	 *
	 */
	public void setDfProductCode(String value)
	{
		setValue(FIELD_DFPRODUCTCODE, value);
	}


	/**
	 *
	 *
	 */
	public String getDfMIDecision()
	{
		return (String)getValue(FIELD_DFMIDECISION);
	}

	/**
	 *
	 *
	 */
	public void setDfMIDecision(String value)
	{
		setValue(FIELD_DFMIDECISION, value);
	}


	/**
	 *
	 *
	 */
	public String getDfAmountInsured()
	{
		return (String)getValue(FIELD_DFAMOUNTINSURED);
	}

	/**
	 *
	 *
	 */
	public void setDfAmountInsured(String value)
	{
		setValue(FIELD_DFAMOUNTINSURED, value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPrimaryApplicantRiskId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPRIMARYAPPLICANTRISKID);
	}

	/**
	 *
	 *
	 */
	public void setDfPrimaryApplicantRiskId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPRIMARYAPPLICANTRISKID, value);
	}


	/**
	 *
	 *
	 */
	public String getDfPrimaryApplicantRisk()
	{
		return (String)getValue(FIELD_DFPRIMARYAPPLICANTRISK);
	}

	/**
	 *
	 *
	 */
	public void setDfPrimaryApplicantRisk(String value)
	{
		setValue(FIELD_DFPRIMARYAPPLICANTRISK, value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSecondaryApplicantRiskId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSECONDARYAPPLICANTRISKID);
	}

	/**
	 *
	 *
	 */
	public void setDfSecondaryApplicantRiskId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSECONDARYAPPLICANTRISKID, value);
	}


	/**
	 *
	 *
	 */
	public String getDfSecondaryApplicantRisk()
	{
		return (String)getValue(FIELD_DFSECONDARYAPPLICANTRISK);
	}

	/**
	 *
	 *
	 */
	public void setDfSecondaryApplicantRisk(String value)
	{
		setValue(FIELD_DFSECONDARYAPPLICANTRISK, value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMarketRiskId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMARKETRISKID);
	}

	/**
	 *
	 *
	 */
	public void setDfMarketRiskId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMARKETRISKID, value);
	}


	/**
	 *
	 *
	 */
	public String getDfMarketRisk()
	{
		return (String)getValue(FIELD_DFMARKETRISK);
	}

	/**
	 *
	 *
	 */
	public void setDfMarketRisk(String value)
	{
		setValue(FIELD_DFMARKETRISK, value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyRiskId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYRISKID);
	}

	/**
	 *
	 *
	 */
	public void setDfPropertyRiskId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYRISKID, value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyRisk()
	{
		return (String)getValue(FIELD_DFPROPERTYRISK);
	}

	/**
	 *
	 *
	 */
	public void setDfPropertyRisk(String value)
	{
		setValue(FIELD_DFPROPERTYRISK, value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNeighbourhoodRiskId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFNEIGHBOURHOODRISKID);
	}

	/**
	 *
	 *
	 */
	public void setDfNeighbourhoodRiskId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFNEIGHBOURHOODRISKID, value);
	}


	/**
	 *
	 *
	 */
	public String getDfNeighbourhoodRisk()
	{
		return (String)getValue(FIELD_DFNEIGHBOURHOODRISK);
	}

	/**
	 *
	 *
	 */
	public void setDfNeighbourhoodRisk(String value)
	{
		setValue(FIELD_DFNEIGHBOURHOODRISK, value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfGlobalNetWorth()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFGLOBALNETWORTH);
	}

	/**
	 *
	 *
	 */
	public void setDfGlobalNetWorth(java.math.BigDecimal value)
	{
		setValue(FIELD_DFGLOBALNETWORTH, value);
	}


    public java.math.BigDecimal getDfInstitutionId() {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
      }

      public void setDfInstitutionId(java.math.BigDecimal value) {
        setValue(FIELD_DFINSTITUTIONID, value);
      }
	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.
  public static final String DATA_SOURCE_NAME="jdbc/orcl";
  public static final String SELECT_SQL_TEMPLATE=
	  "SELECT DISTINCT REQUEST.REQUESTID, REQUEST.REQUESTDATE, REQUEST.REQUESTSTATUSID, DEAL.DEALID, DEAL.COPYID, " +
	  "RESPONSE.RESPONSEID, TO_CHAR(REQUEST.REQUESTSTATUSID) REQUESTSTATUSID_STR, RESPONSE.RESPONSEDATE, " +
	  "TO_CHAR(BNCADJUDICATIONRESPONSE.GLOBALUSEDCREDITBUREAUNAMEID) GLOBALUSEDCBNAMEID_STR, " +
	  "BNCADJUDICATIONRESPONSE.GLOBALUSEDCREDITBUREAUNAMEID, ADJUDICATIONRESPONSE.GLOBALCREDITBUREAUSCORE, " +
	  "TO_CHAR(ADJUDICATIONRESPONSE.GLOBALRISKRATING) GLOBALRISKRATING_STR, " +
	  "ADJUDICATIONRESPONSE.GLOBALRISKRATING, ADJUDICATIONRESPONSE.GLOBALINTERNALSCORE, " +
	  "TO_CHAR(ADJUDICATIONRESPONSE.ADJUDICATIONSTATUSID) ADJUDICATIONSTATUSID_STR, " +
	  "ADJUDICATIONRESPONSE.ADJUDICATIONSTATUSID, BNCADJUDICATIONRESPONSE.PRODUCTCODE, " +
	  "BNCADJUDICATIONRESPONSE.MIDECISION, BNCADJUDICATIONRESPONSE.AMOUNTINSURED, " +
	  "TO_CHAR(BNCADJUDICATIONRESPONSE.PRIMARYAPPLICANTRISKID) PRIMARYAPPLICANTRISKID_STR, " +
	  "BNCADJUDICATIONRESPONSE.PRIMARYAPPLICANTRISKID, TO_CHAR(BNCADJUDICATIONRESPONSE.SECONDARYAPPLICANTRISKID) SECONDARYAPPLICANTRISKID_STR, " +
	  "BNCADJUDICATIONRESPONSE.SECONDARYAPPLICANTRISKID, TO_CHAR(BNCADJUDICATIONRESPONSE.MARKETRISKID) MARKETRISKID_STR, " +
	  "BNCADJUDICATIONRESPONSE.MARKETRISKID, TO_CHAR(BNCADJUDICATIONRESPONSE.PROPERTYRISKID) PROPERTYRISKID_STR, " +
	  "BNCADJUDICATIONRESPONSE.PROPERTYRISKID, TO_CHAR(BNCADJUDICATIONRESPONSE.NEIGHBOURHOODRISKID) NEIGHBOURHOODRISKID_STR, " +
	  "BNCADJUDICATIONRESPONSE.NEIGHBOURHOODRISKID, BNCADJUDICATIONRESPONSE.GLOBALNETWORTH, " +
	  "REQUEST.INSTITUTIONPROFILEID "+
	  "FROM REQUEST " +
	  "LEFT OUTER JOIN DEAL ON REQUEST.DEALID = DEAL.DEALID " +
	  "LEFT OUTER JOIN RESPONSE ON REQUEST.REQUESTID = RESPONSE.REQUESTID " +
	  "LEFT OUTER JOIN ADJUDICATIONRESPONSE ON RESPONSE.RESPONSEID = ADJUDICATIONRESPONSE.RESPONSEID " +
	  "LEFT OUTER JOIN BNCADJUDICATIONRESPONSE ON ADJUDICATIONRESPONSE.RESPONSEID = BNCADJUDICATIONRESPONSE.RESPONSEID " +
	  "__WHERE__  ORDER BY REQUEST.REQUESTDATE DESC, DEAL.COPYID DESC ";



  public static final String MODIFYING_QUERY_TABLE_NAME="REQUEST"; //ADJUDICATIONRESPONSE, RESPONSE, BNCADJUDICATIONRESPONSE, DEAL, REQUEST ";

  // Ticket 1193: Removed the WHERE clause for REQUEST.COPYID = DEAL.COPYID to fix
  public static final String STATIC_WHERE_CRITERIA= " REQUEST.DEALID = DEAL.DEALID " +
	  // SCR#1305 - start
	  " AND REQUEST.COPYID = DEAL.COPYID AND REQUEST.INSTITUTIONPROFILEID = DEAL.INSTITUTIONPROFILEID " +
	  // SCR#1305 - end
  " AND REQUEST.SERVICEPRODUCTID = " + Mc.SERVICE_PRODUCT_ID_GCD + " ";

	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();

	public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFRESPONSEID="RESPONSE.RESPONSEID";
	public static final String COLUMN_DFRESPONSEID="RESPONSEID";
	public static final String QUALIFIED_COLUMN_DFREQUESTID="REQUEST.REQUESTID";
	public static final String COLUMN_DFREQUESTID="REQUESTID";
	public static final String QUALIFIED_COLUMN_DFRESPONSEDATE="RESPONSE.RESPONSEDATE";
	public static final String COLUMN_DFRESPONSEDATE="RESPONSEDATE";
	public static final String QUALIFIED_COLUMN_DFREQUESTDATE="REQUEST.REQUESTDATE";
	public static final String COLUMN_DFREQUESTDATE="REQUESTDATE";
	public static final String QUALIFIED_COLUMN_DFREQUESTSTATUSID="REQUEST.REQUESTSTATUSID";
	public static final String COLUMN_DFREQUESTSTATUSID="REQUESTSTATUSID";
	public static final String QUALIFIED_COLUMN_DFREQUESTSTATUSDESC="REQUEST.REQUESTSTATUSID_STR";
	public static final String COLUMN_DFREQUESTSTATUSDESC="REQUESTSTATUSID_STR";
	public static final String QUALIFIED_COLUMN_DFGLOBALUSEDCREDITBUREAUNAMEID="BNCADJUDICATIONRESPONSE.GLOBALUSEDCREDITBUREAUNAMEID";
	public static final String COLUMN_DFGLOBALUSEDCREDITBUREAUNAMEID="GLOBALUSEDCREDITBUREAUNAMEID";
	public static final String QUALIFIED_COLUMN_DFGLOBALUSEDCREDITBUREAUNAME="BNCADJUDICATIONRESPONSE.GLOBALUSEDCBNAMEID_STR";
	public static final String COLUMN_DFGLOBALUSEDCREDITBUREAUNAME="GLOBALUSEDCBNAMEID_STR";
	public static final String QUALIFIED_COLUMN_DFGLOBALCREDITBUREAUSCORE="ADJUDICATIONRESPONSE.GLOBALCREDITBUREAUSCORE";
	public static final String COLUMN_DFGLOBALCREDITBUREAUSCORE="GLOBALCREDITBUREAUSCORE";
	public static final String QUALIFIED_COLUMN_DFGLOBALRISKRATING="BADJUDICATIONRESPONSE.GLOBALRISKRATING";
	public static final String COLUMN_DFGLOBALRISKRATING="GLOBALRISKRATING";
	public static final String QUALIFIED_COLUMN_DFGLOBALINTERNALSCORE="ADJUDICATIONRESPONSE.GLOBALINTERNALSCORE";
	public static final String COLUMN_DFGLOBALINTERNALSCORE="GLOBALINTERNALSCORE";
	public static final String QUALIFIED_COLUMN_DFADJUDICATIONSTATUSID="ADJUDICATIONRESPONSE.ADJUDICATIONSTATUSID";
	public static final String COLUMN_DFADJUDICATIONSTATUSID="ADJUDICATIONSTATUSID";
	public static final String QUALIFIED_COLUMN_DFADJUDICATIONDESC="ADJUDICATIONRESPONSE.ADJUDICATIONSTATUSID_STR";
	public static final String COLUMN_DFADJUDICATIONDESC="ADJUDICATIONSTATUSID_STR";
	public static final String QUALIFIED_COLUMN_DFPRODUCTCODE="BNCADJUDICATIONRESPONSE.PRODUCTCODE";
	public static final String COLUMN_DFPRODUCTCODE="PRODUCTCODE";
	public static final String QUALIFIED_COLUMN_DFMIDECISION="BNCADJUDICATIONRESPONSE.MIDECISION";
	public static final String COLUMN_DFMIDECISION="MIDECISION";
	public static final String QUALIFIED_COLUMN_DFAMOUNTINSURED="BNCADJUDICATIONRESPONSE.AMOUNTINSURED";
	public static final String COLUMN_DFAMOUNTINSURED="AMOUNTINSURED";
	public static final String QUALIFIED_COLUMN_DFPRIMARYAPPLICANTRISKID="BNCADJUDICATIONRESPONSE.PRIMARYAPPLICANTRISKID";
	public static final String COLUMN_DFPRIMARYAPPLICANTRISKID="PRIMARYAPPLICANTRISKID";
	public static final String QUALIFIED_COLUMN_DFPRIMARYAPPLICANTRISK="BNCADJUDICATIONRESPONSE.PRIMARYAPPLICANTRISKID_STR";
	public static final String COLUMN_DFPRIMARYAPPLICANTRISK="PRIMARYAPPLICANTRISKID_STR";
	public static final String QUALIFIED_COLUMN_DFSECONDARYAPPLICANTRISKID="BNCADJUDICATIONRESPONSE.SECONDARYAPPLICANTRISKID";
	public static final String COLUMN_DFSECONDARYAPPLICANTRISKID="SECONDARYAPPLICANTRISKID";
	public static final String QUALIFIED_COLUMN_DFSECONDARYAPPLICANTRISK="BNCADJUDICATIONRESPONSE.SECONDARYAPPLICANTRISKID_STR";
	public static final String COLUMN_DFSECONDARYAPPLICANTRISK="SECONDARYAPPLICANTRISKID_STR";
	public static final String QUALIFIED_COLUMN_DFMARKETRISKID="BNCADJUDICATIONRESPONSE.MARKETRISKID";
	public static final String COLUMN_DFMARKETRISKID="MARKETRISKID";
	public static final String QUALIFIED_COLUMN_DFMARKETRISK="BNCADJUDICATIONRESPONSE.MARKETRISKID_STR";
	public static final String COLUMN_DFMARKETRISK="MARKETRISKID_STR";
	public static final String QUALIFIED_COLUMN_DFPROPERTYRISKID="BNCADJUDICATIONRESPONSE.PROPERTYRISKID";
	public static final String COLUMN_DFPROPERTYRISKID="PROPERTYRISKID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYRISK="BNCADJUDICATIONRESPONSE.PROPERTYRISKID_STR";
	public static final String COLUMN_DFPROPERTYRISK="PROPERTYRISKID_STR";
	public static final String QUALIFIED_COLUMN_DFNEIGHBOURHOODRISKID="BNCADJUDICATIONRESPONSE.NEIGHBOURHOODRISKID";
	public static final String COLUMN_DFNEIGHBOURHOODRISKID="NEIGHBOURHOODRISKID";
	public static final String QUALIFIED_COLUMN_DFNEIGHBOURHOODRISK="BNCADJUDICATIONRESPONSE.NEIGHBOURHOODRISKID_STR";
	public static final String COLUMN_DFNEIGHBOURHOODRISK="NEIGHBOURHOODRISKID_STR";
	public static final String QUALIFIED_COLUMN_DFGLOBALNETWORTH="BNCADJUDICATIONRESPONSE.GLOBALNETWORTH";
	public static final String COLUMN_DFGLOBALNETWORTH="GLOBALNETWORTH";
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "PROPERTYEXPENSE.INSTITUTIONPROFILEID";
      public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";



	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFDEALID,
					COLUMN_DFDEALID,
					QUALIFIED_COLUMN_DFDEALID,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFCOPYID,
					COLUMN_DFCOPYID,
					QUALIFIED_COLUMN_DFCOPYID,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFRESPONSEID,
				COLUMN_DFRESPONSEID,
				QUALIFIED_COLUMN_DFRESPONSEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFREQUESTID,
				COLUMN_DFREQUESTID,
				QUALIFIED_COLUMN_DFREQUESTID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFRESPONSEDATE,
				COLUMN_DFRESPONSEDATE,
				QUALIFIED_COLUMN_DFRESPONSEDATE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFREQUESTDATE,
				COLUMN_DFREQUESTDATE,
				QUALIFIED_COLUMN_DFREQUESTDATE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFREQUESTSTATUSID,
				COLUMN_DFREQUESTSTATUSID,
				QUALIFIED_COLUMN_DFREQUESTSTATUSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFREQUESTSTATUSDESC,
				COLUMN_DFREQUESTSTATUSDESC,
				QUALIFIED_COLUMN_DFREQUESTSTATUSDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFGLOBALUSEDCREDITBUREAUNAMEID,
				COLUMN_DFGLOBALUSEDCREDITBUREAUNAMEID,
				QUALIFIED_COLUMN_DFGLOBALUSEDCREDITBUREAUNAMEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFGLOBALUSEDCREDITBUREAUNAME,
				COLUMN_DFGLOBALUSEDCREDITBUREAUNAME,
				QUALIFIED_COLUMN_DFGLOBALUSEDCREDITBUREAUNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFGLOBALCREDITBUREAUSCORE,
				COLUMN_DFGLOBALCREDITBUREAUSCORE,
				QUALIFIED_COLUMN_DFGLOBALCREDITBUREAUSCORE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFGLOBALRISKRATING,
				COLUMN_DFGLOBALRISKRATING,
				QUALIFIED_COLUMN_DFGLOBALRISKRATING,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFGLOBALINTERNALSCORE,
				COLUMN_DFGLOBALINTERNALSCORE,
				QUALIFIED_COLUMN_DFGLOBALINTERNALSCORE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFADJUDICATIONSTATUSID,
				COLUMN_DFADJUDICATIONSTATUSID,
				QUALIFIED_COLUMN_DFADJUDICATIONSTATUSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFADJUDICATIONDESC,
				COLUMN_DFADJUDICATIONDESC,
				QUALIFIED_COLUMN_DFADJUDICATIONDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFPRODUCTCODE,
				COLUMN_DFPRODUCTCODE,
				QUALIFIED_COLUMN_DFPRODUCTCODE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFMIDECISION,
				COLUMN_DFMIDECISION,
				QUALIFIED_COLUMN_DFMIDECISION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFAMOUNTINSURED,
				COLUMN_DFAMOUNTINSURED,
				QUALIFIED_COLUMN_DFAMOUNTINSURED,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFPRIMARYAPPLICANTRISKID,
				COLUMN_DFPRIMARYAPPLICANTRISKID,
				QUALIFIED_COLUMN_DFPRIMARYAPPLICANTRISKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFPRIMARYAPPLICANTRISK,
				COLUMN_DFPRIMARYAPPLICANTRISK,
				QUALIFIED_COLUMN_DFPRIMARYAPPLICANTRISK,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFSECONDARYAPPLICANTRISKID,
				COLUMN_DFSECONDARYAPPLICANTRISKID,
				QUALIFIED_COLUMN_DFSECONDARYAPPLICANTRISKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFSECONDARYAPPLICANTRISK,
				COLUMN_DFSECONDARYAPPLICANTRISK,
				QUALIFIED_COLUMN_DFSECONDARYAPPLICANTRISK,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFMARKETRISKID,
				COLUMN_DFMARKETRISKID,
				QUALIFIED_COLUMN_DFMARKETRISKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFMARKETRISK,
				COLUMN_DFMARKETRISK,
				QUALIFIED_COLUMN_DFMARKETRISK,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFPROPERTYRISKID,
				COLUMN_DFPROPERTYRISKID,
				QUALIFIED_COLUMN_DFPROPERTYRISKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFPROPERTYRISK,
				COLUMN_DFPROPERTYRISK,
				QUALIFIED_COLUMN_DFPROPERTYRISK,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFNEIGHBOURHOODRISKID,
				COLUMN_DFNEIGHBOURHOODRISKID,
				QUALIFIED_COLUMN_DFNEIGHBOURHOODRISKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFNEIGHBOURHOODRISK,
				COLUMN_DFNEIGHBOURHOODRISK,
				QUALIFIED_COLUMN_DFNEIGHBOURHOODRISK,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFGLOBALNETWORTH,
				COLUMN_DFGLOBALNETWORTH,
				QUALIFIED_COLUMN_DFGLOBALNETWORTH,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	        FIELD_SCHEMA.addFieldDescriptor(
	                new QueryFieldDescriptor(
	                  FIELD_DFINSTITUTIONID,
	                  COLUMN_DFINSTITUTIONID,
	                  QUALIFIED_COLUMN_DFINSTITUTIONID,
	                  java.math.BigDecimal.class,
	                  false,
	                  false,
	                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	                  "",
	                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	                  ""));
	}

}

