package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 * Description: doComponentInfoModel is related with JSP properties.
 * 
 * @author: MCM Impl Team.
 * @version 1.0 2008-6-11
 */
public interface doComponentInfoModel extends QueryModel, SelectQueryModel {

    // Class variables
    public static final String FIELD_DFTOTALAMOUNT = "dfTotalAmount";

    public static final String FIELD_DFCOPYID = "dfCopyId";

    public static final String FIELD_DFDEALID = "dfDealId";

    public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";

    public static final String FIELD_DFTOTALCASHBACKAMOUNT = "dfTotalCashBackAmount";

    public static final String FIELD_DFTOTALLOANAMOUNT = "dfTotalLoanAmount";

    /**
     * This method declaration returns a value of java.math.BigDecimal
     * 
     * @return
     */
    public java.math.BigDecimal getDfTotalAmount();

    /**
     * This method declaration sets the value of java.math.BigDecimal
     * 
     * @param value
     */
    public void setDfTotalAmount(java.math.BigDecimal value);

    /**
     * This method declaration returns a value of java.math.BigDecimal
     * 
     * @return
     */
    public java.math.BigDecimal getDfTotalCashBackAmount();

    /**
     * This method declaration sets the value of java.math.BigDecimal
     * 
     * @param value
     */
    public void setDfTotalCashBackAmount(java.math.BigDecimal value);

    /**
     * This method declaration returns a value of java.math.BigDecimal
     * 
     * @return
     */
    public java.math.BigDecimal getDfCopyId();

    /**
     * This method declaration sets the value of java.math.BigDecimal
     * 
     * @param value
     */
    public void setDfCopyId(java.math.BigDecimal value);

    /**
     * This method declaration returns a value of java.math.BigDecimal
     * 
     * @return
     */
    public java.math.BigDecimal getDfDealId();

    /**
     * This method declaration sets the value of java.math.BigDecimal
     * 
     * @param value
     */
    public void setDfDealId(java.math.BigDecimal value);

    /**
     * This method declaration returns a value of java.math.BigDecimal
     * 
     * @return
     */
    public java.math.BigDecimal getDfInstitutionId();

    /**
     * This method declaration sets the value of java.math.BigDecimal
     * 
     * @param value
     */
    public void setDfInstitutionId(java.math.BigDecimal value);

    /**
     * This method declaration returns a value of java.math.BigDecimal
     * 
     * @return
     */
    public java.math.BigDecimal getDfTotalLoanAmount();

    /**
     * This method declaration sets the value of java.math.BigDecimal
     * 
     * @param value
     */
    public void setDfTotalLoanAmount(java.math.BigDecimal value);
}