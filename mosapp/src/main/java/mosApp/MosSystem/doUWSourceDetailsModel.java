package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doUWSourceDetailsModel extends QueryModel, SelectQueryModel
{
  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////
  public static final String FIELD_DFSOBPSHORTNAME = "dfSobpShortName";
  public static final String FIELD_DFSOURCEFIRMNAME = "dfSourceFirmName";
  public static final String FIELD_DFADDRESSLINE1 = "dfAddressLine1";
  public static final String FIELD_DFCITY = "dfCity";
  public static final String FIELD_DFCONTACTFAXNUMBER = "dfContactFaxNumber";
  public static final String FIELD_DFCONTACTPHONENUMBER =
    "dfContactPhoneNumber";
  public static final String FIELD_DFPROVINCEABBR = "dfProvinceAbbr";
  public static final String FIELD_DFDEALID = "dfDealId";
  //public static final String FIELD_DFCOPYID = "dfCopyId";
  public static final String FIELD_DFFULLADDRESS = "dfFullAddress";
  public static final String FIELD_DFPHONENOEXTENTION = "dfPhoneNoExtention";
  public static final String FIELD_DFCONTACTFIRSTNAME = "dfContactFirstName";
  public static final String FIELD_DFCONTACTLASTNAME = "dfContactLastName";

  public static final String FIELD_DFLICENSENUMBER = "dfLicenseNumber";
  
  //-- ========== DJ#725: begins ========== --//
  //-- By Neil : Nov/29/2004
  // E-mail
  public static final String FIELD_DFCONTACTEMAILADDRESS =
    "dfContactEmailAddress";

  // Status
  public static final String FIELD_DFPSDESCRIPTION = "dfPsDescription";

  // System Type
  public static final String FIELD_DFSYSTEMTYPEDESCRIPTION =
    "dfSystemTypeDescription";

  //-- ========== DJ#725: ends ========== --//

  /***** FXP23815 add profileStatusId *****/
  public static final String FIELD_DFPROFILESTATUSID = "dfProfileStatusId";

  //Submission Agent
  public static final String FIELD_DFSOURCEOFBUSINESSPROFILEID = "dfSourceOfBusinessProfileId";
  public static final String FIELD_DFSORTORDER = "dfSortOrder";

  
  /**
   *  FXP23815 profileStatusID
   */
  public String getDfProfileStatusId();

  /**
   *  FXP23815 profileStatusID
   */
  public void setDfProfileStatusId(String value);

  /**
   *
   *
   */
  public String getDfSobpShortName();

  /**
   *
   *
   */
  public void setDfSobpShortName(String value);

  /**
   *
   *
   */
  public String getDfSourceFirmName();

  /**
   *
   *
   */
  public void setDfSourceFirmName(String value);

  /**
   *
   *
   */
  public String getDfAddressLine1();

  /**
   *
   *
   */
  public void setDfAddressLine1(String value);

  /**
   *
   *
   */
  public String getDfCity();

  /**
   *
   *
   */
  public void setDfCity(String value);

  /**
   *
   *
   */
  public String getDfContactFaxNumber();

  /**
   *
   *
   */
  public void setDfContactFaxNumber(String value);

  /**
   *
   *
   */
  public String getDfContactPhoneNumber();

  /**
   *
   *
   */
  public void setDfContactPhoneNumber(String value);

  /**
   *
   *
   */
  public String getDfProvinceAbbr();

  /**
   *
   *
   */
  public void setDfProvinceAbbr(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfDealId();

  /**
   *
   *
   */
  public void setDfDealId(java.math.BigDecimal value);

  /**
   *
   *
   */
  //public java.math.BigDecimal getDfCopyId();

  /**
   *
   *
   */
  //public void setDfCopyId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfFullAddress();

  /**
   *
   *
   */
  public void setDfFullAddress(String value);

  /**
   *
   *
   */
  public String getDfPhoneNoExtention();

  /**
   *
   *
   */
  public void setDfPhoneNoExtention(String value);

  /**
   *
   *
   */
  public String getDfContactFirstName();

  /**
   *
   *
   */
  public void setDfContactFirstName(String value);

  /**
   *
   *
   */
  public String getDfContactLastName();

  /**
   *
   *
   */
  public void setDfContactLastName(String value);

  
  /**
   * @return
   */
  public String getDfLicenseNumber();
  
  
  //-- ========== DJ#725: begins ========== --//
  //-- By Neil : Nov/29/2004

  /**
   * @return
   */
  public String getDfContactEmailAddress();

  /**
   * @return
   */
  public String getDfPsDescription();

  /**
   * @return
   */
  public String getDfSystemTypeDescription();

  /**
   * @param
   */
  public void setDfLicenseNumber(String value);
  
  /**
   * @param
   */
  public void setDfContactEmailAddress(String value);

  /**
   * @param
   */
  public void setDfPsDescription(String value);

  /**
   * @param
   */
  public void setDfSystemTypeDescription(String value);

  public java.math.BigDecimal getDfSourceOfBusinessProfileId();
  
  public void setDfSourceOfBusinessProfileId(java.math.BigDecimal value);
    
  public java.math.BigDecimal getDfSortOrder();
  
  public void setDfSortOrder(java.math.BigDecimal value);
  
  //-- ========== DJ#725: Ends ========== --//
  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////
}
