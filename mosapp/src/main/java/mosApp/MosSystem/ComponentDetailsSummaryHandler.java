package mosApp.MosSystem;

import MosSystem.Sc;

import com.iplanet.jato.RequestManager;
import com.iplanet.jato.view.ViewBean;

import java.util.Hashtable;


/**
 * <p>
 * Title: ComponentDetailsSummaryHandler.java
 * </p>
 * Description: This class handles the manipulation of data including display of data for Component Details Summary Screen.
 * </p>
 * </p>
 * @author MCM Impl Team
 * @version 1.0 13-June-2008 XS_16.12 Initial Version
 * @version 1.1 17-June-2008 XS_16.15 Updated setupBeforePageGeneration() for formating Actual Payment Term
 * @version 1.1 24-June-2008 XS_16.13 Updated setupBeforePageGeneration() for formating Actual Payment Term
 */
public class ComponentDetailsSummaryHandler extends PageHandlerCommon
    implements Cloneable, Sc {

    /**
     * <p>
     * Description: This Constructor returns the instance of this class.
     * </p>
     * @version 1.0 13-June-2008 XS_16.12 Initial version
     */
    public ComponentDetailsSummaryHandler cloneSS () {

        return (ComponentDetailsSummaryHandler) super.cloneSafeShallow();

    }

    /**
     * <p>
     * Description: This method is used to populate the fields in the header
     * with the appropriate information
     * </p>
     * @version 1.0 13-June-2008 XS_16.12 Initial version
     */
    public void populatePageDisplayFields () {

        PageEntry pg = theSessionState.getCurrentPage();
        Hashtable pst = pg.getPageStateTable();
        PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");
        populatePageShellDisplayFields();
        populateTaskNavigator(pg);
        populatePageDealSummarySnapShot();
        populatePreviousPagesLinks();
        displayDSSConditional(pg, getCurrNDPage());
    }

    /**
     * <p>
     * Description: This method initializes the model for display data
     * </p>
     * @version 1.0 13-June-2008 XS_16.12 Initial version
     * @version 1.1 17-June-2008 XS_16.15 Updated setupBeforePageGeneration() for formating Actual Payment Term field
     */
    public void setupBeforePageGeneration () {

        PageEntry pg = theSessionState.getCurrentPage();

        if (pg.getSetupBeforeGenerationCalled() == true) {

            return;
        }
        pg.setSetupBeforeGenerationCalled(true);
        setupDealSummarySnapShotDO(pg);
    
    }

    /**
     *
     *
     */
    private void displayDSSConditional (PageEntry pg, ViewBean thePage) {

        String suppressStart = "<!--";

        String suppressEnd = "//-->";

        if ((pg != null) && (pg.getPageCondition1() == false)) {

            logger.debug("--T--> displayDSSConditional :: stIncludeDSSstart and stIncludeDSSend allow DSS");
            thePage.setDisplayFieldValue("stIncludeDSSstart", new String(""));
            thePage.setDisplayFieldValue("stIncludeDSSend", new String(""));

            return;

        }

        // suppress
        logger.debug("--T--> displayDSSConditional :: stIncludeDSSstart and stIncludeDSSend suppress DSS");

        thePage.setDisplayFieldValue("stIncludeDSSstart",
                                     new String(suppressStart));

        thePage.setDisplayFieldValue("stIncludeDSSend", new String(suppressEnd));

    }

    /**
     *
     *
     *
     */
    public void handleForwardButton () {

        PageEntry pg = theSessionState.getCurrentPage();

        Hashtable pst = pg.getPageStateTable();

        PageCursorInfo tds = (PageCursorInfo) pst.get("DEFAULT");

        handleForwardBackwardCommon(pg, tds, -1, true);

        return;

    }

    /**
     *
     *
     */
    public void handleBackwardButton () {

        PageEntry pg = theSessionState.getCurrentPage();

        Hashtable pst = pg.getPageStateTable();

        PageCursorInfo tds = (PageCursorInfo) pst.get("DEFAULT");

        handleForwardBackwardCommon(pg, tds, -1, false);

        return;

    }

}
