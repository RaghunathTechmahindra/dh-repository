package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgIALReviewRepeatedEmployerTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgIALReviewRepeatedEmployerTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doIALEmploymentHistoryModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getCbOccupation().setValue(CHILD_CBOCCUPATION_RESET_VALUE);
		getCbIndustry().setValue(CHILD_CBINDUSTRY_RESET_VALUE);
		getHdEmpBorrowerId().setValue(CHILD_HDEMPBORROWERID_RESET_VALUE);
		getHdEmployerId().setValue(CHILD_HDEMPLOYERID_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_CBOCCUPATION,ComboBox.class);
		registerChild(CHILD_CBINDUSTRY,ComboBox.class);
		registerChild(CHILD_HDEMPBORROWERID,HiddenField.class);
		registerChild(CHILD_HDEMPLOYERID,HiddenField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoIALEmploymentHistoryModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}

		return movedToRow;

	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_CBOCCUPATION))
		{
			ComboBox child = new ComboBox( this,
				getdoIALEmploymentHistoryModel(),
				CHILD_CBOCCUPATION,
				doIALEmploymentHistoryModel.FIELD_DFOCCUPATIONID,
				CHILD_CBOCCUPATION_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbOccupationOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBINDUSTRY))
		{
			ComboBox child = new ComboBox( this,
				getdoIALEmploymentHistoryModel(),
				CHILD_CBINDUSTRY,
				doIALEmploymentHistoryModel.FIELD_DFINDUSTRYSECTORID,
				CHILD_CBINDUSTRY_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbIndustryOptions);
			return child;
		}
		else
		if (name.equals(CHILD_HDEMPBORROWERID))
		{
			HiddenField child = new HiddenField(this,
				getdoIALEmploymentHistoryModel(),
				CHILD_HDEMPBORROWERID,
				doIALEmploymentHistoryModel.FIELD_DFBORROWERID,
				CHILD_HDEMPBORROWERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDEMPLOYERID))
		{
			HiddenField child = new HiddenField(this,
				getdoIALEmploymentHistoryModel(),
				CHILD_HDEMPLOYERID,
				doIALEmploymentHistoryModel.FIELD_DFEMPLOYMENTHISTORYID,
				CHILD_HDEMPLOYERID_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbOccupation()
	{
		return (ComboBox)getChild(CHILD_CBOCCUPATION);
	}

	/**
	 *
	 *
	 */
	static class CbOccupationOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbOccupationOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
			Connection c = null;
      Statement query = null;
			try {
				clear();
				if(rc == null) {
					c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
				}
				else {
					c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
				}

				query = c.createStatement();
				ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);

				while(results.next()) {
					Object OCCUPATIONID = results.getObject("OCCUPATIONID");
					Object OCCUPATION_ODESCRIPTION = results.getObject("ODESCRIPTION");

					String label = (OCCUPATION_ODESCRIPTION == null?"":OCCUPATION_ODESCRIPTION.toString());

					String value = (OCCUPATIONID == null?"":OCCUPATIONID.toString());

					add(label, value);
				}
        results.close();
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			finally {
				try {
					if(c != null) c.close();
          if(query != null) query.close();
				}
				catch (SQLException  ex) {
					// ignore
				}
			}
		}
		private static String FETCH_DATA_STATEMENT="SELECT OCCUPATION.ODESCRIPTION, OCCUPATION.OCCUPATIONID FROM OCCUPATION";

	}



	/**
	 *
	 *
	 */
	public ComboBox getCbIndustry()
	{
		return (ComboBox)getChild(CHILD_CBINDUSTRY);
	}

	/**
	 *
	 *
	 */
	static class CbIndustryOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbIndustryOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
			Connection c = null;
      Statement query = null;
			try {
				clear();
				if(rc == null) {
					c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
				}
				else {
					c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
				}

				query = c.createStatement();
				ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);

				while(results.next()) {
					Object INDUSTRYSECTOR_ISDESCRIPTION = results.getObject("ISDESCRIPTION");
					Object INDUSTRYSECTORID = results.getObject("INDUSTRYSECTORID");

					String label = (INDUSTRYSECTOR_ISDESCRIPTION == null?"":INDUSTRYSECTOR_ISDESCRIPTION.toString());

					String value = (INDUSTRYSECTORID == null?"":INDUSTRYSECTORID.toString());

					add(label, value);
				}
        results.close();
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			finally {
				try {
					if(c != null) c.close();
          if(query != null) query.close();
				}
				catch (SQLException  ex) {
					// ignore
				}
			}
		}
		private static String FETCH_DATA_STATEMENT="SELECT INDUSTRYSECTOR.ISDESCRIPTION, INDUSTRYSECTOR.INDUSTRYSECTORID FROM INDUSTRYSECTOR";

	}



	/**
	 *
	 *
	 */
	public HiddenField getHdEmpBorrowerId()
	{
		return (HiddenField)getChild(CHILD_HDEMPBORROWERID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdEmployerId()
	{
		return (HiddenField)getChild(CHILD_HDEMPLOYERID);
	}


	/**
	 *
	 *
	 */
	public doIALEmploymentHistoryModel getdoIALEmploymentHistoryModel()
	{
		if (doIALEmploymentHistory == null)
			doIALEmploymentHistory = (doIALEmploymentHistoryModel) getModel(doIALEmploymentHistoryModel.class);
		return doIALEmploymentHistory;
	}


	/**
	 *
	 *
	 */
	public void setdoIALEmploymentHistoryModel(doIALEmploymentHistoryModel model)
	{
			doIALEmploymentHistory = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_CBOCCUPATION="cbOccupation";
	public static final String CHILD_CBOCCUPATION_RESET_VALUE="";
	private CbOccupationOptionList cbOccupationOptions=new CbOccupationOptionList();
	public static final String CHILD_CBINDUSTRY="cbIndustry";
	public static final String CHILD_CBINDUSTRY_RESET_VALUE="";
	private CbIndustryOptionList cbIndustryOptions=new CbIndustryOptionList();
	public static final String CHILD_HDEMPBORROWERID="hdEmpBorrowerId";
	public static final String CHILD_HDEMPBORROWERID_RESET_VALUE="";
	public static final String CHILD_HDEMPLOYERID="hdEmployerId";
	public static final String CHILD_HDEMPLOYERID_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doIALEmploymentHistoryModel doIALEmploymentHistory=null;

}

