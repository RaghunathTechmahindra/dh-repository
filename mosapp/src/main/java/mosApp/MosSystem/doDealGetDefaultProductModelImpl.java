package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doDealGetDefaultProductModelImpl extends QueryModelBase
	implements doDealGetDefaultProductModel
{
	/**
	 *
	 *
	 */
	public doDealGetDefaultProductModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMtgProdId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMTGPRODID);
	}


	/**
	 *
	 *
	 */
	public void setDfMtgProdId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMTGPRODID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDefaultProduct()
	{
		return (String)getValue(FIELD_DFDEFAULTPRODUCT);
	}


	/**
	 *
	 *
	 */
	public void setDfDefaultProduct(String value)
	{
		setValue(FIELD_DFDEFAULTPRODUCT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLenderProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLENDERPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfLenderProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLENDERPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDefaultLender()
	{
		return (String)getValue(FIELD_DFDEFAULTLENDER);
	}


	/**
	 *
	 *
	 */
	public void setDfDefaultLender(String value)
	{
		setValue(FIELD_DFDEFAULTLENDER,value);
	}

	/**
	 * MetaData object test method to verify the SQL_TEMPLATE query.
	 *
	 */
    /**
        public void setResultSet(ResultSet value)
        {
            logger = SysLog.getSysLogger("METADATA");

            try
            {
                super.setResultSet(value);
                if( value != null)
                {
                    ResultSetMetaData metaData = value.getMetaData();
                    int columnCount = metaData.getColumnCount();
                    logger.debug("===============================================");
                    logger.debug("Testing MetaData Object for new synthetic fields for" +
	                 	            " doDealGetDefaultProductModel");
                    logger.debug("NumberColumns: " + columnCount);
                    for(int i = 0; i < columnCount ; i++)
                    {
                        logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
                    }
                    logger.debug("================================================");
                  }
              }
              catch (SQLException e)
              {
                  logger.debug("Class: " + getClass().getName());
                  logger.debug("SQLException: " + e);
              }
        }
     **/
	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE
        ="SELECT distinct MTGPRODLENDERINVESTORASSOC.MTGPRODID, " +
                "MTGPRODLENDERINVESTORASSOC.DEFAULTPRODUCT, LENDERPROFILE.LENDERPROFILEID, " +
                "LENDERPROFILE.DEFAULTLENDER FROM MTGPROD, MTGPRODLENDERINVESTORASSOC, " +
                "LENDERPROFILE  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="MTGPROD, MTGPRODLENDERINVESTORASSOC, LENDERPROFILE";
	public static final String STATIC_WHERE_CRITERIA
        =" (MTGPROD.MTGPRODID  =  MTGPRODLENDERINVESTORASSOC.MTGPRODID) " +
                " AND (MTGPRODLENDERINVESTORASSOC.LENDERPROFILEID  =  LENDERPROFILE.LENDERPROFILEID) " + 
                " AND (MTGPROD.INSTITUTIONPROFILEID = MTGPRODLENDERINVESTORASSOC.INSTITUTIONPROFILEID) " +
                " AND (MTGPROD.INSTITUTIONPROFILEID = LENDERPROFILE.INSTITUTIONPROFILEID) ";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFMTGPRODID="MTGPRODLENDERINVESTORASSOC.MTGPRODID";
	public static final String COLUMN_DFMTGPRODID="MTGPRODID";
	public static final String QUALIFIED_COLUMN_DFDEFAULTPRODUCT="MTGPRODLENDERINVESTORASSOC.DEFAULTPRODUCT";
	public static final String COLUMN_DFDEFAULTPRODUCT="DEFAULTPRODUCT";
	public static final String QUALIFIED_COLUMN_DFLENDERPROFILEID="LENDERPROFILE.LENDERPROFILEID";
	public static final String COLUMN_DFLENDERPROFILEID="LENDERPROFILEID";
	public static final String QUALIFIED_COLUMN_DFDEFAULTLENDER="LENDERPROFILE.DEFAULTLENDER";
	public static final String COLUMN_DFDEFAULTLENDER="DEFAULTLENDER";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMTGPRODID,
				COLUMN_DFMTGPRODID,
				QUALIFIED_COLUMN_DFMTGPRODID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEFAULTPRODUCT,
				COLUMN_DFDEFAULTPRODUCT,
				QUALIFIED_COLUMN_DFDEFAULTPRODUCT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLENDERPROFILEID,
				COLUMN_DFLENDERPROFILEID,
				QUALIFIED_COLUMN_DFLENDERPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEFAULTLENDER,
				COLUMN_DFDEFAULTLENDER,
				QUALIFIED_COLUMN_DFDEFAULTLENDER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

