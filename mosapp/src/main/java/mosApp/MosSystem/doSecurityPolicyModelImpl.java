package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doSecurityPolicyModelImpl extends QueryModelBase
	implements doSecurityPolicyModel
{
	/**
	 *
	 *
	 */
	public doSecurityPolicyModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSECURITYPOLICYID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSECURITYPOLICYID);
	}


	/**
	 *
	 *
	 */
	public void setDfSECURITYPOLICYID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSECURITYPOLICYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSECPOLDESCRIPTION()
	{
		return (String)getValue(FIELD_DFSECPOLDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfSECPOLDESCRIPTION(String value)
	{
		setValue(FIELD_DFSECPOLDESCRIPTION,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfVALUE()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfVALUE(java.math.BigDecimal value)
	{
		setValue(FIELD_DFVALUE,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfCHANGEDATE()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFCHANGEDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfCHANGEDATE(java.sql.Timestamp value)
	{
		setValue(FIELD_DFCHANGEDATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCHANGEDBYUSERID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCHANGEDBYUSERID);
	}


	/**
	 *
	 *
	 */
	public void setDfCHANGEDBYUSERID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCHANGEDBYUSERID,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL SECURITYPOLICY.SECURITYPOLICYID, SECURITYPOLICY.SECPOLDESCRIPTION, SECURITYPOLICY.VALUE, SECURITYPOLICY.CHANGEDATE, SECURITYPOLICY.CHANGEDBYUSERID FROM SECURITYPOLICY  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="SECURITYPOLICY";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFSECURITYPOLICYID="SECURITYPOLICY.SECURITYPOLICYID";
	public static final String COLUMN_DFSECURITYPOLICYID="SECURITYPOLICYID";
	public static final String QUALIFIED_COLUMN_DFSECPOLDESCRIPTION="SECURITYPOLICY.SECPOLDESCRIPTION";
	public static final String COLUMN_DFSECPOLDESCRIPTION="SECPOLDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFVALUE="SECURITYPOLICY.VALUE";
	public static final String COLUMN_DFVALUE="VALUE";
	public static final String QUALIFIED_COLUMN_DFCHANGEDATE="SECURITYPOLICY.CHANGEDATE";
	public static final String COLUMN_DFCHANGEDATE="CHANGEDATE";
	public static final String QUALIFIED_COLUMN_DFCHANGEDBYUSERID="SECURITYPOLICY.CHANGEDBYUSERID";
	public static final String COLUMN_DFCHANGEDBYUSERID="CHANGEDBYUSERID";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSECURITYPOLICYID,
				COLUMN_DFSECURITYPOLICYID,
				QUALIFIED_COLUMN_DFSECURITYPOLICYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSECPOLDESCRIPTION,
				COLUMN_DFSECPOLDESCRIPTION,
				QUALIFIED_COLUMN_DFSECPOLDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFVALUE,
				COLUMN_DFVALUE,
				QUALIFIED_COLUMN_DFVALUE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCHANGEDATE,
				COLUMN_DFCHANGEDATE,
				QUALIFIED_COLUMN_DFCHANGEDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCHANGEDBYUSERID,
				COLUMN_DFCHANGEDBYUSERID,
				QUALIFIED_COLUMN_DFCHANGEDBYUSERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

