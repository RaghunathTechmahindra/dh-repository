package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doBorrowerEntryAssetUpdateModelImpl extends QueryModelBase
	implements doBorrowerEntryAssetUpdateModel
{
	/**
	 *
	 *
	 */
	public doBorrowerEntryAssetUpdateModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAssetId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFASSETID);
	}


	/**
	 *
	 *
	 */
	public void setDfAssetId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFASSETID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAssetDescription()
	{
		return (String)getValue(FIELD_DFASSETDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfAssetDescription(String value)
	{
		setValue(FIELD_DFASSETDESCRIPTION,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAssetValue()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFASSETVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfAssetValue(java.math.BigDecimal value)
	{
		setValue(FIELD_DFASSETVALUE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIncludingGDS()
	{
		return (String)getValue(FIELD_DFINCLUDINGGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfIncludingGDS(String value)
	{
		setValue(FIELD_DFINCLUDINGGDS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIncludingTDS()
	{
		return (String)getValue(FIELD_DFINCLUDINGTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfIncludingTDS(String value)
	{
		setValue(FIELD_DFINCLUDINGTDS,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="";
	public static final String MODIFYING_QUERY_TABLE_NAME="ASSET";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="ASSET.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFASSETID="ASSET.ASSETID";
	public static final String COLUMN_DFASSETID="ASSETID";
	public static final String QUALIFIED_COLUMN_DFASSETDESCRIPTION="ASSET.ASSETDESCRIPTION";
	public static final String COLUMN_DFASSETDESCRIPTION="ASSETDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFASSETVALUE="ASSET.ASSETVALUE";
	public static final String COLUMN_DFASSETVALUE="ASSETVALUE";
	public static final String QUALIFIED_COLUMN_DFINCLUDINGGDS="ASSET.INCLUDEINGDS";
	public static final String COLUMN_DFINCLUDINGGDS="INCLUDEINGDS";
	public static final String QUALIFIED_COLUMN_DFINCLUDINGTDS="ASSET.INCLUDEINTDS";
	public static final String COLUMN_DFINCLUDINGTDS="INCLUDEINTDS";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				true,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFASSETID,
				COLUMN_DFASSETID,
				QUALIFIED_COLUMN_DFASSETID,
				java.math.BigDecimal.class,
				true,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFASSETDESCRIPTION,
				COLUMN_DFASSETDESCRIPTION,
				QUALIFIED_COLUMN_DFASSETDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFASSETVALUE,
				COLUMN_DFASSETVALUE,
				QUALIFIED_COLUMN_DFASSETVALUE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCLUDINGGDS,
				COLUMN_DFINCLUDINGGDS,
				QUALIFIED_COLUMN_DFINCLUDINGGDS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCLUDINGTDS,
				COLUMN_DFINCLUDINGTDS,
				QUALIFIED_COLUMN_DFINCLUDINGTDS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

