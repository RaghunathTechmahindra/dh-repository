package mosApp.MosSystem;

/**
 * 11/Oct/2007 DVG #DG638 LEN217180: Malcolm Whitton Condition Text exceeds 4000 characters 
 */

import java.sql.Clob;
import java.sql.SQLException;

import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doDocTrackingCusConditionModelImpl extends QueryModelBase
	implements doDocTrackingCusConditionModel
{
	/**
	 *
	 *
	 */
	public doDocTrackingCusConditionModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    logger = SysLog.getSysLogger("DODTCCM");
    logger.debug("DODTCCM@afterExecute::Size: " + this.getSize());
    logger.debug("DODTCCM@afterExecute::SQLTemplate: " + this.getSelectSQL());
		//#DG638 reset clob as string		
		if( queryType != QUERY_TYPE_SELECT)
			return;
		while(next())    {
			String condText;
	  		//================================================================
	  		//CLOB Performance Enhancement June 2010
			Object docTextVarchar = getValue(FIELD_DFDOCUMENTTEXTLITE);
			if( docTextVarchar != null) {
				condText = (String)docTextVarchar;
			} else {
		  		try {
		  			Clob condTextObj = (Clob)getValue(FIELD_DFCUSCONDITION);
		  			condText = TypeConverter.stringFromClob(condTextObj);
		  		}
		  		catch (Exception e) {
		  			SysLog.error(getClass(), StringUtil.stack2string(e), "DTCusIM");
		  			condText = null;
		  		}
			}
			//================================================================
			setDfCusCondition(condText);
		}
		//Reset Location
		beforeFirst();
		//#DG638 end		
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCusCondition()
	{
		return (String)getValue(FIELD_DFCUSCONDITION);
	}


	/**
	 *
	 *
	 */
	public void setDfCusCondition(String value)
	{
		setValue(FIELD_DFCUSCONDITION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDocumentLabel()
	{
		return (String)getValue(FIELD_DFDOCUMENTLABEL);
	}


	/**
	 *
	 *
	 */
	public void setDfDocumentLabel(String value)
	{
		setValue(FIELD_DFDOCUMENTLABEL,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAction()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFACTION);
	}


	/**
	 *
	 *
	 */
	public void setDfAction(java.math.BigDecimal value)
	{
		setValue(FIELD_DFACTION,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfResponsibility()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFRESPONSIBILITY);
	}


	/**
	 *
	 *
	 */
	public void setDfResponsibility(java.math.BigDecimal value)
	{
		setValue(FIELD_DFRESPONSIBILITY,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCusDocTrackId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCUSDOCTRACKID);
	}


	/**
	 *
	 *
	 */
	public void setDfCusDocTrackId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCUSDOCTRACKID,value);
	}

	/**
	 *
	 *
	 */
	public String getDfByPass()
	{
		return (String)getValue(FIELD_DFBYPASS);
	}

	/**
	 *
	 *
	 */
	public void setDfByPass(String value)
	{
		setValue(FIELD_DFBYPASS,value);
	}

  //--Release2.1--//
  //--> Added LanguagePreferenceId DataField
  public java.math.BigDecimal getDfLanguagePrederenceId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLANGUAGEPREFERENCEID);
	}

	public void setDfLanguagePrederenceId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLANGUAGEPREFERENCEID,value);
	}


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //// Added new LanguageId datafield in order to setup Language Criteria when page displayed
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL DOCUMENTTRACKING.DEALID, DOCUMENTTRACKING.COPYID, DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXT, DOCUMENTTRACKINGVERBIAGE.DOCUMENTLABEL, DOCUMENTTRACKING.DOCUMENTSTATUSID, DOCUMENTTRACKING.DOCUMENTRESPONSIBILITYROLEID, DOCUMENTTRACKING.DOCUMENTTRACKINGID FROM DOCUMENTTRACKING, DOCUMENTTRACKINGVERBIAGE  __WHERE__  ORDER BY DOCUMENTTRACKING.DOCUMENTTRACKINGID  ASC";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT DOCUMENTTRACKING.DEALID, DOCUMENTTRACKING.COPYID, "+   
    "DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXT, DOCUMENTTRACKINGVERBIAGE.DOCUMENTLABEL, "+
    //================================================================
    //CLOB Performance Enhancement June 2010
    "DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXTLITE, "+
    //================================================================
    "DOCUMENTTRACKING.DOCUMENTSTATUSID, DOCUMENTTRACKING.DOCUMENTRESPONSIBILITYROLEID, "+
    //--> Added LanguagePreferenceId, StApprovalByPass
    "DOCUMENTTRACKING.DOCUMENTTRACKINGID, DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID, "+
    "CONDITION.STAPPROVALBYPASS " +
    "FROM CONDITION, DOCUMENTTRACKING, DOCUMENTTRACKINGVERBIAGE, DOCUMENTSTATUS "+
    "__WHERE__  "+
    "ORDER BY DOCUMENTTRACKING.DOCUMENTTRACKINGID  ASC";

  public static final String MODIFYING_QUERY_TABLE_NAME="DOCUMENTTRACKING, DOCUMENTTRACKINGVERBIAGE";
	//--Release2.1--//
  //// IMPORTANT!!!
  /////"DOCUMENTSTATUS.DOCUMENTSTATUSID = 0 AND " +
  //// Take out the HardCoded Criteria, DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID = 0
  //public static final String STATIC_WHERE_CRITERIA=" (((DOCUMENTTRACKINGVERBIAGE.COPYID  =  DOCUMENTTRACKING.COPYID) AND (DOCUMENTTRACKINGVERBIAGE.DOCUMENTTRACKINGID  =  DOCUMENTTRACKING.DOCUMENTTRACKINGID)) AND DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID = 0 AND DOCUMENTTRACKING.DOCUMENTTRACKINGSOURCEID = 2) ";
	public static final String STATIC_WHERE_CRITERIA=
	    "DOCUMENTTRACKING.INSTITUTIONPROFILEID = DOCUMENTTRACKINGVERBIAGE.INSTITUTIONPROFILEID " + 
        " AND DOCUMENTTRACKING.DOCUMENTTRACKINGID = DOCUMENTTRACKINGVERBIAGE.DOCUMENTTRACKINGID " + 
	    " AND DOCUMENTTRACKING.COPYID = DOCUMENTTRACKINGVERBIAGE.COPYID " + 
        " AND DOCUMENTTRACKING.INSTITUTIONPROFILEID = CONDITION.INSTITUTIONPROFILEID " + 
        " AND DOCUMENTTRACKING.CONDITIONID = CONDITION.CONDITIONID " +
        " AND DOCUMENTSTATUS.DOCUMENTSTATUSID = DOCUMENTTRACKING.DOCUMENTSTATUSID " +    
        " AND DOCUMENTSTATUS.DOCUMENTSTATUSID = 0 " +
        " AND DOCUMENTTRACKING.DOCUMENTTRACKINGSOURCEID = 2 ";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="DOCUMENTTRACKING.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DOCUMENTTRACKING.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFCUSCONDITION="DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXT";
	public static final String COLUMN_DFCUSCONDITION="DOCUMENTTEXT";
	//================================================================
	//CLOB Performance Enhancement June 2010
	public static final String QUALIFIED_COLUMN_DFDOCUMENTTEXTLITE="DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXTLITE";
	public static final String COLUMN_DFDOCUMENTTEXTLITE="DOCUMENTTEXTLITE";
	//================================================================

	public static final String QUALIFIED_COLUMN_DFDOCUMENTLABEL="DOCUMENTTRACKINGVERBIAGE.DOCUMENTLABEL";
	public static final String COLUMN_DFDOCUMENTLABEL="DOCUMENTLABEL";
	public static final String QUALIFIED_COLUMN_DFACTION="DOCUMENTTRACKING.DOCUMENTSTATUSID";
	public static final String COLUMN_DFACTION="DOCUMENTSTATUSID";
	public static final String QUALIFIED_COLUMN_DFRESPONSIBILITY="DOCUMENTTRACKING.DOCUMENTRESPONSIBILITYROLEID";
	public static final String COLUMN_DFRESPONSIBILITY="DOCUMENTRESPONSIBILITYROLEID";
	public static final String QUALIFIED_COLUMN_DFCUSDOCTRACKID="DOCUMENTTRACKING.DOCUMENTTRACKINGID";
	public static final String COLUMN_DFCUSDOCTRACKID="DOCUMENTTRACKINGID";
	//--Release2.1--//
  //--> Added LanguagePreferenceId DataField
  public static final String QUALIFIED_COLUMN_DFLANGUAGEPREFERENCEID="DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID";
	public static final String COLUMN_DFLANGUAGEPREFERENCEID="LANGUAGEPREFERENCEID";
	public static final String QUALIFIED_COLUMN_DFBYPASS="CONDITION.STAPPROVALBYPASS";
	public static final String COLUMN_DFBYPASS="STAPPROVALBYPASS";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCUSCONDITION,
				COLUMN_DFCUSCONDITION,
				QUALIFIED_COLUMN_DFCUSCONDITION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		//================================================================
		//CLOB Performance Enhancement June 2010
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFDOCUMENTTEXTLITE,
					COLUMN_DFDOCUMENTTEXTLITE,
					QUALIFIED_COLUMN_DFDOCUMENTTEXTLITE,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
		//================================================================

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOCUMENTLABEL,
				COLUMN_DFDOCUMENTLABEL,
				QUALIFIED_COLUMN_DFDOCUMENTLABEL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFACTION,
				COLUMN_DFACTION,
				QUALIFIED_COLUMN_DFACTION,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFRESPONSIBILITY,
				COLUMN_DFRESPONSIBILITY,
				QUALIFIED_COLUMN_DFRESPONSIBILITY,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCUSDOCTRACKID,
				COLUMN_DFCUSDOCTRACKID,
				QUALIFIED_COLUMN_DFCUSDOCTRACKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    //--Release2.1--//
    //--> Added LanguagePreferenceId DataField
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLANGUAGEPREFERENCEID,
				COLUMN_DFLANGUAGEPREFERENCEID,
				QUALIFIED_COLUMN_DFLANGUAGEPREFERENCEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBYPASS,
				COLUMN_DFBYPASS,
				QUALIFIED_COLUMN_DFBYPASS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
	}

}

