package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doUserAdminGetGroupsModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfGroupProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfGroupProfileId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfGroupName();

	
	/**
	 * 
	 * 
	 */
	public void setDfGroupName(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBranchProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBranchProfileId(java.math.BigDecimal value);
	
	  public java.math.BigDecimal getDfInstitutionId();
	  public void setDfInstitutionId(java.math.BigDecimal id);
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFGROUPPROFILEID="dfGroupProfileId";
	public static final String FIELD_DFGROUPNAME="dfGroupName";
	public static final String FIELD_DFBRANCHPROFILEID="dfBranchProfileId";
	 public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

