package mosApp.MosSystem;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 * Title: doQuickLinkModelImpl
 * <p>
 * Description: Implementation of doQuickLinkModel Interface.
 * @author 
 * @version 1.0 
 * 
 * 
 * 
 */
public class doQuickLinkModelImpl extends QueryModelBase implements
	doQuickLinkModel
{
	private final static Logger  logger = LoggerFactory.getLogger(doQuickLinkModelImpl.class);
	
	public static final String DATA_SOURCE_NAME = "jdbc/orcl";

    public static final String SELECT_SQL_TEMPLATE = "SELECT QUICKLINKS.QUICKLINKID, QUICKLINKS.LINKNAMEENGLISH, QUICKLINKS.LINKNAMEFRENCH,"
    		+ " QUICKLINKS.URLLINK,QUICKLINKS.BRANCHPROFILEID, QUICKLINKS.INSTITUTIONPROFILEID, QUICKLINKS.SORTORDER, QUICKLINKS.FAVICON "
    		+ " FROM QUICKLINKS "
    		+ "__WHERE__  ORDER BY QUICKLINKS.SORTORDER ASC";
       
    public static final String MODIFYING_QUERY_TABLE_NAME = "QUICKLINKS";
   
    public static final String STATIC_WHERE_CRITERIA = "";
        	
        
    public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();

    //  code starts-here    
    public static final String QUALIFIED_COLUMN_DFQUICKLINKID = "QUICKLINKS.QUICKLINKID";
    public static final String COLUMN_DFQUICKLINKID = "QUICKLINKID";

    public static final String QUALIFIED_COLUMN_DFLINKNAMEENGLISH = "QUICKLINKS.LINKNAMEENGLISH";
    public static final String COLUMN_DFLINKNAMEENGLISH = "LINKNAMEENGLISH";

    public static final String QUALIFIED_COLUMN_DFLINKNAMEFRENCH = "QUICKLINKS.LINKNAMEFRENCH";
    public static final String COLUMN_DFLINKNAMEFRENCH = "LINKNAMEFRENCH";

    public static final String QUALIFIED_COLUMN_DFURLLINK = "QUICKLINKS.URLLINK";
    public static final String COLUMN_DFURLLINK = "URLLINK";

    public static final String QUALIFIED_COLUMN_DFBRANCHPROFILEID = "QUICKLINKS.BRANCHPROFILEID";
    public static final String COLUMN_DFBRANCHPROFILEID = "BRANCHPROFILEID";

    public static final String QUALIFIED_COLUMN_DFINSTITUTIONPROFILEID = "QUICKLINKS.INSTITUTIONPROFILEID";
    public static final String COLUMN_DFINSTITUTIONPROFILEID = "INSTITUTIONPROFILEID";

    public static final String QUALIFIED_COLUMN_DFSORTORDER = "QUICKLINKS.SORTORDER";
    public static final String COLUMN_DFSORTORDER = "SORTORDER";

    public static final String QUALIFIED_COLUMN_DFFAVICON = "QUICKLINKS.FAVICON";
    public static final String COLUMN_DFFAVICON = "FAVICON";

 

    static
    {

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
        		FIELD_DFQUICKLINKID, COLUMN_DFQUICKLINKID,
        		QUALIFIED_COLUMN_DFQUICKLINKID, java.math.BigDecimal.class,
                false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
        		FIELD_DFLINKNAMEENGLISH, COLUMN_DFLINKNAMEENGLISH, QUALIFIED_COLUMN_DFLINKNAMEENGLISH,
        		String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
        		FIELD_DFLINKNAMEFRENCH, COLUMN_DFLINKNAMEFRENCH, QUALIFIED_COLUMN_DFLINKNAMEFRENCH,
        		String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
        		FIELD_URLLINK, COLUMN_DFURLLINK,
        		QUALIFIED_COLUMN_DFURLLINK,
        		String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
        		FIELD_DFBRANCHPROFILEID, COLUMN_DFBRANCHPROFILEID,
        		QUALIFIED_COLUMN_DFBRANCHPROFILEID, java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
        		FIELD_DFINSTITUTIONPROFILEID, COLUMN_DFINSTITUTIONPROFILEID,
        		QUALIFIED_COLUMN_DFINSTITUTIONPROFILEID, java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
        		FIELD_DFSORTORDER, COLUMN_DFSORTORDER,
        		QUALIFIED_COLUMN_DFSORTORDER,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
        		FIELD_DFFAVICON, COLUMN_DFFAVICON,
        		QUALIFIED_COLUMN_DFFAVICON, String.class,
                false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));   
        
    }

    /**
     * Instantiates a new do quick link model impl.
     */
    public doQuickLinkModelImpl()
    {
        super();
        setDataSourceName(DATA_SOURCE_NAME);

        setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

        setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

        setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

        setFieldSchema(FIELD_SCHEMA);

        initialize();

    }

    /**
     * Initialize.
     */
    public void initialize()
    {

    }

    /**
     * BeforeExecute
     * @param context
     *            the context
     * @param queryType
     *            the query type
     * @param sql
     *            the sql
     * @return list of model model[]
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    protected String beforeExecute(ModelExecutionContext context,
            int queryType, String sql) throws ModelControlException
    {

        return sql;

    }

    /**
     * This method is used to handle any display logic after executing the
     * model..
     * @param context
     *            the context
     * @param queryType
     *            the query type
     * @return list of model model[]
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    protected void afterExecute(ModelExecutionContext context, int queryType)
            throws ModelControlException
    {
        // Reset Location
        this.beforeFirst();

    }

    /**
     * OnDatabaseError.
     * @param context
     *            the context
     * @param queryType
     *            the query type
     * @param exception
     *            the exception
     * @return list of model model[]
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    protected void onDatabaseError(ModelExecutionContext context,
            int queryType, SQLException exception)
    {
    	logger.error(this.getClass() + ".onDatabaseError(): Database error details"  + exception.getMessage());
    }

    
    
    public java.math.BigDecimal getDfQuickLinkId()
    {
        return (java.math.BigDecimal) getValue(FIELD_DFQUICKLINKID);

    }

    public void setDfQuickLinkId(java.math.BigDecimal value)
    {

        setValue(FIELD_DFQUICKLINKID, value);

    }
    
    public String getDfLinkNameEnglish()
    {
        return (String) getValue(FIELD_DFLINKNAMEENGLISH);

    }

    public void setDfLinkNameEnglish(String value)
    {

        setValue(FIELD_DFLINKNAMEENGLISH, value);

    }
    
    public String getDfLinkNameFrench()
    {
        return (String) getValue(FIELD_DFLINKNAMEFRENCH);

    }

    public void setDfLinkNameFrench(String value)
    {

        setValue(FIELD_DFLINKNAMEFRENCH, value);

    }
    
    public String getDfUrlLink()
    {
        return (String) getValue(FIELD_URLLINK);

    }

    public void setDfUrlLink(String value)
    {

        setValue(FIELD_URLLINK, value);

    }
    
    public java.math.BigDecimal getDfBranchProfileId()
    {
        return (java.math.BigDecimal) getValue(FIELD_DFBRANCHPROFILEID);

    }

    public void setDfBranchProfileId(java.math.BigDecimal value)
    {

        setValue(FIELD_DFBRANCHPROFILEID, value);

    }
    
    public java.math.BigDecimal getDfInstitutionProfileId()
    {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONPROFILEID);

    }

    public void setDfInstitutionProfileId(java.math.BigDecimal value)
    {

        setValue(FIELD_DFINSTITUTIONPROFILEID, value);

    }

    public java.math.BigDecimal getDfSortOrder()
    {
        return (java.math.BigDecimal) getValue(FIELD_DFSORTORDER);

    }

    public void setDfSortOrder(java.math.BigDecimal value)
    {

        setValue(FIELD_DFSORTORDER, value);

    }

    public String getDfFavicon()
    {
        return (String) getValue(FIELD_DFFAVICON);

    }

    public void setDfFavicon(String value)
    {

        setValue(FIELD_DFFAVICON, value);

    }
}
